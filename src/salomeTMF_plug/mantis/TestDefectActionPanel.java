package salomeTMF_plug.mantis;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class TestDefectActionPanel extends JPanel implements ActionListener{
	JButton editDefectButton ;
	JButton viewLinkButton ;
	
	DefectPanel pDefectPanel;
	MantisPlugin pMantisPlugin;
	
	TestDefectActionPanel(MantisPlugin pMantisPlugin){
		super(new FlowLayout());
		this.pMantisPlugin = pMantisPlugin;
		initActionPanel();
	}
	
	void setDefectPanel(DefectPanel pDefectPanel){
		this.pDefectPanel = pDefectPanel;
	}
	
	void initActionPanel(){
		editDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Editer"));
		editDefectButton.addActionListener(this);
		
		viewLinkButton = new JButton(Language.getInstance().getText("Liason"));
		viewLinkButton.addActionListener(this);
		
		add(editDefectButton);
		add(viewLinkButton);
		
	}
	
	public void actionPerformed(ActionEvent e){
		 if (e.getSource().equals(editDefectButton)){
			editDefectPerformed();
		} else  if (e.getSource().equals(viewLinkButton)){
			viewLinkPerformed();
		}
	}
	
	void editDefectPerformed(){
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			DefectView pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pDefectWrapper, pMantisPlugin, true);
			try {
				if (pDefectView.isDoingModification()){
					pDefectPanel.reloadData();
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	void viewLinkPerformed(){
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			new ViewLinkDialog(pDefectWrapper, pMantisPlugin, DataModel.getCurrentTest());
		}
	}
}
