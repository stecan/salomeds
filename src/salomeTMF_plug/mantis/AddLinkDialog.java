package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.ActionDetailsResultMouseListener;
import org.objectweb.salome_tmf.ihm.models.AutomaticTestResultMouseListener;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;
import org.objectweb.salome_tmf.ihm.models.TreeRenderer;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;



public class AddLinkDialog extends JDialog implements DataConstants, ActionListener, TreeSelectionListener {

	protected TestTreeModel campTreeModel;
	JTree testTree;
	DefaultMutableTreeNode testSelectedNode;
	DefaultMutableTreeNode temporaryChosenRootNode;

	JTabbedPane detailPanel;
	JPanel buttonPanel;
	//JButton cancelButton;
	JButton validateButton;

	JPanel emptyPanel;

	JTextArea descriptionArea;
	JScrollPane campaignPanel;

	JPanel executionPanel;
	JLabel envLabel;
	JLabel dateLabel;
	JLabel datelastExec;


	JPanel resExexPanel;

	ExecutionResult currentExecResult;
	MyTableModel testResultTableModel;
	JTable testResultTable;
	TableSorter  sorter;
	ListSelectionModel rowSM;
	JButton linkDefectButton;
	JButton removelinkDefectButton;
	JButton editDefectButton;
	JButton addDefectButton;
	JLabel executionName ;
	DefectPanel pDefectPanel;

	JPanel infoDefect;
	JLabel idDefect;
	JLabel resumeDefect;
	JLabel priorityDefect;
	JLabel stateDefect;
	JButton editDefect;

	DefectWrapper defectToLink;
	MantisPlugin pMantisPlugin;
	Hashtable defectsWrapperCache;

	boolean doModification = false;
	public AddLinkDialog(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin) {

		super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
		defectToLink = pDefectWrapper;
		this.pMantisPlugin = pMantisPlugin;
		int t_x = 1024 - 100;
		int t_y = 768 - 50;
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice[] gs = ge.getScreenDevices();
			GraphicsDevice gd = gs[0];
			GraphicsConfiguration[] gc = gd.getConfigurations();
			Rectangle r = gc[0].getBounds();
			t_x = r.width - 100;
			t_y = r.height - 50 ;
		} catch(Exception E){

		}

		TreeRenderer testRenderer = new TreeRenderer();
		DefaultMutableTreeNode treeRoot = new DefaultMutableTreeNode("Selection");
		temporaryChosenRootNode = new DefaultMutableTreeNode(treeRoot.getUserObject());
		DefaultMutableTreeNode testRoot = SalomeTMFPanels.getCampaignDynamicTree().getRoot();
		DefaultMutableTreeNode testRootClone = (DefaultMutableTreeNode)testRoot.clone();

		testTree = new JTree();
		campTreeModel = new TestTreeModel(testRootClone, testTree, null);
		testTree.setModel(campTreeModel);
		testTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		for (int j = 0; j < testRoot.getChildCount(); j++) {
	        DefaultMutableTreeNode childNode = (DefaultMutableTreeNode)testRoot.getChildAt(j);
	        Campaign campaign = (Campaign)childNode.getUserObject();
	        if (campaign.containsExecutionResultInModel()){
	        	addObject(testRootClone, campaign, true);
	        }
	    }

		testTree.setCellRenderer(testRenderer);
		testTree.addTreeSelectionListener(this);

		// Executions de chaque campagne
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) campTreeModel
				.getRoot();
		for (int j = 0; j < root.getChildCount(); j++) {
			DefaultMutableTreeNode campNode = (DefaultMutableTreeNode) root
					.getChildAt(j);
			if (campNode.getUserObject() instanceof Campaign) {
				campNode.removeAllChildren();
				Campaign camp = (Campaign) campNode.getUserObject();
				ArrayList execList = camp.getExecutionListFromModel();
				if (!execList.isEmpty()) {
					Iterator it = execList.iterator();
					while (it.hasNext()) {
						Execution exec = (Execution) it.next();
						ArrayList execResList = exec.getExecutionResultListFromModel();
						if (execResList.size()>0){
							DefaultMutableTreeNode execNode  = addObject(campNode, exec, true);
							Iterator it2 = execResList.iterator();
							while (it2.hasNext()) {
								ExecutionResult resExec = (ExecutionResult) it2.next();
								addObject(execNode, resExec, true);
							}
						}
					}
				}
			}
		}






		detailPanel = new JTabbedPane();
		detailPanel.setSize(new Dimension(t_x*3/2, t_y/10*8));

		emptyPanel = new JPanel();
		//emptyPanel.setSize(new Dimension(t_x/2, t_y/10*8));
		detailPanel.add(emptyPanel, "Details");

		descriptionArea = new JTextArea();
		campaignPanel = new JScrollPane(descriptionArea);
		descriptionArea.setEditable(false);
		campaignPanel.setPreferredSize(new Dimension(t_x*3/2, t_y/10*8));


		/* Execution */
		executionPanel = new JPanel();
		executionPanel.setLayout(new BoxLayout(executionPanel, BoxLayout.Y_AXIS));
		envLabel = new JLabel();
		dateLabel= new JLabel();
		datelastExec= new JLabel();
		executionPanel.add(envLabel);
		executionPanel.add(dateLabel);
		executionPanel.add(datelastExec);

		/* Resultat d'execution */
		testResultTableModel = new MyTableModel();
        testResultTable = new JTable();
        executionName = new JLabel();
        executionName.setFont(new Font(null, Font.BOLD,18));

        testResultTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Famille"));
        testResultTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Suite"));
        testResultTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Test"));
        testResultTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Resultats"));
        sorter = new TableSorter(testResultTableModel);
        testResultTable.setModel(sorter);
        sorter.setTableHeader(testResultTable.getTableHeader());
        //testResultTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        testResultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane tablePane = new JScrollPane(testResultTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());


        rowSM = testResultTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting())
                    return;

                int selectedRowIndex = testResultTable.getSelectedRow();
                if (selectedRowIndex != -1 && testResultTableModel.getRowCount() > 0) {
                    Test test = DataModel.getCurrentProject().getTestFromModel(
                    (String)sorter.getValueAt(selectedRowIndex, 0),
                    (String)sorter.getValueAt(selectedRowIndex, 1),
                    (String)sorter.getValueAt(selectedRowIndex, 2)
                    );
                    DataModel.setTestObservedInExecution(test);
                    DataModel.setCurrentExecutionTestResult(currentExecResult.getExecutionTestResultFromModel(test));
                    if (defectToLink!=null){
                    	linkDefectButton.setEnabled(true);
                    	removelinkDefectButton.setEnabled(true);
                    	editDefectButton.setEnabled(true);
                    	addDefectButton.setEnabled(true);
                    } else {
                    	removelinkDefectButton.setEnabled(true);
                    	editDefectButton.setEnabled(true);
                    	addDefectButton.setEnabled(true);
                    }
                    updateDefectPanel(true);
                } else {
                    linkDefectButton.setEnabled(false);
                    removelinkDefectButton.setEnabled(false);
                	editDefectButton.setEnabled(false);
                	addDefectButton.setEnabled(false);
                    updateDefectPanel(false);
                }
            }
        });
        // Gestion de la souris
        testResultTable.addMouseListener(new ActionDetailsResultMouseListener());
        testResultTable.addMouseListener(new AutomaticTestResultMouseListener());

        linkDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Lier_a_lannomalie"));
        linkDefectButton.setEnabled(false);
        linkDefectButton.addActionListener(this);

        removelinkDefectButton =  new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Supprimer_lien"));
        removelinkDefectButton.setEnabled(false);
        removelinkDefectButton.addActionListener(this);

        editDefectButton =  new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Editer"));
        editDefectButton.setEnabled(false);
        editDefectButton.addActionListener(this);

        addDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Ajouter"));
        addDefectButton.setEnabled(false);
        addDefectButton.addActionListener(this);

        JPanel upPart = new JPanel(new BorderLayout());
        upPart.add(executionName, BorderLayout.NORTH);

        JPanel allView = new JPanel(new BorderLayout());

        allView.add(upPart, BorderLayout.NORTH);
        allView.add(tablePane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(linkDefectButton);
        buttonPanel.add(removelinkDefectButton);
        buttonPanel.add(addDefectButton);
        buttonPanel.add(editDefectButton);


        pDefectPanel = new DefectPanel(true,pMantisPlugin, buttonPanel, null);


        resExexPanel = new JPanel();
        resExexPanel.setLayout(new BoxLayout(resExexPanel, BoxLayout.Y_AXIS));
        resExexPanel.add(allView);
        //Ici les annomalies
        resExexPanel.add(pDefectPanel);
        //resExexPanel.add(buttonPanel);



        JScrollPane testScrollPane = new JScrollPane(testTree);
		testScrollPane.setBorder(BorderFactory.createTitledBorder("Executions"));
		testScrollPane.setPreferredSize(new Dimension(t_x/3, t_y/10*8));

		/*JPanel windowPanel = new JPanel();
		windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
		windowPanel.add(testScrollPane);
		windowPanel.add(Box.createRigidArea(new Dimension(20, 50)));
		windowPanel.add(detailPanel);
		windowPanel.setPreferredSize(new Dimension(t_x, t_y/10*8));
		*/



		JSplitPane windowPanel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				testScrollPane, detailPanel);
		windowPanel.setPreferredSize(new Dimension(t_x, t_y/10*8));
		//windowPanel.setDividerLocation(t_x/3);
		/*if (Api.isIDE_DEV()){
			windowPanel.setDividerLocation(3);
		} else {
			windowPanel.setDividerLocation(t_x*3/2);
		}*/
		//add(splitPane);

		buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

		/*cancelButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Annuler"));
		cancelButton.addActionListener(this);*/
		validateButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Valider"));
		validateButton.addActionListener(this);
		//buttonPanel.add(cancelButton);
		buttonPanel.add(validateButton);


		if (defectToLink != null){
			infoDefect = new JPanel(new GridLayout(1,5));
			infoDefect.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),salomeTMF_plug.mantis.languages.Language.getInstance().getText("Anomalie_selectione")));
			idDefect = new JLabel("id : " + defectToLink.getId());
			resumeDefect = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Resume_______") + " " + defectToLink.getResume());
			String str_value = (String) pMantisPlugin.priorityByID.get(Integer.valueOf(""+defectToLink.getPriority()));
			priorityDefect = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Priority") + " : " + str_value);
			str_value = (String) pMantisPlugin.statusByID.get(Integer.valueOf(""+defectToLink.getStatus()));
			stateDefect =  new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Etat") + " : " + str_value);
			editDefect = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Editer"));
			editDefect.addActionListener(this);

			infoDefect.add(idDefect);
			infoDefect.add(resumeDefect);
			infoDefect.add(priorityDefect);
			infoDefect.add(stateDefect);
			infoDefect.add(editDefect);

		}

		Container contentPaneFrame = this.getContentPane();
		if (defectToLink != null){
			contentPaneFrame.add(infoDefect, BorderLayout.NORTH);
		}
		contentPaneFrame.add(windowPanel, BorderLayout.CENTER);
		contentPaneFrame.add(buttonPanel, BorderLayout.SOUTH);

		this.setTitle(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Lier"));
		this.setSize(new Dimension(t_x, t_y));
		/*this.pack();
		this.setLocationRelativeTo(this.getParent());
		this.setVisible(true);
		 */
		centerScreen();
	} // Fin du constructeur ExecChooser

	
	 void centerScreen() {
		  Dimension dim = getToolkit().getScreenSize();
		  this.pack();
		  Rectangle abounds = getBounds();
		  setLocation((dim.width - abounds.width) / 2,
		      (dim.height - abounds.height) / 2);	 
	      this.setVisible(true); 
		  requestFocus();
	}

	/**
	 * Methode d'ajout d'un noeud dans l'arbre sous le parent.
	 * @param parent le parent
	 * @param child le noeud a ajouter
	 * @param shouldBeVisible visible ou non
	 * @return le nouveau noeud de l'arbre
	 */
	public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
			Object child, boolean shouldBeVisible) {

		DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
		if (parent == null) {
			parent = temporaryChosenRootNode;
		}
		// Insertion du noeud
		campTreeModel.insertNodeInto(childNode, parent, parent.getChildCount());
		// on s'assure que le noeud est visible
		if (shouldBeVisible) {
			testTree.scrollPathToVisible(new TreePath(childNode.getPath()));
		}
		return childNode;
	} // Fin de la classe addObject/3


	void setEmptyPanel(){
		detailPanel.removeAll();
		detailPanel.add(emptyPanel, "Details");
		//detailPanel.repaint();
	}

	void setCampaignPanel(Campaign pCamp){
		descriptionArea.setText(pCamp.getDescriptionFromModel());
		detailPanel.removeAll();
		detailPanel.add(campaignPanel, org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Campagne"));
	}

	void setExecutionPanel(Execution pExec){
		detailPanel.removeAll();
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		envLabel.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Environnement") + " : " + pExec.getEnvironmentFromModel().getNameFromModel()) ;
		dateLabel.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Date_de_creation") +  " : " + formater.format(pExec.getCreationDateFromModel()).toString());
		Date pDate = pExec.getLastDateFromModel();
		datelastExec.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Derniere_Execution") + " : " + formater.format(pDate).toString());
		detailPanel.add(executionPanel, org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Execution"));
	}

	void setResExecutionPanel(ExecutionResult pExecutionResult){
		executionName.setText(pExecutionResult.getNameFromModel());
		detailPanel.removeAll();
		currentExecResult = pExecutionResult;
		DataModel.setObervedExecutionResult(pExecutionResult);
		DataModel.setObservedExecution(pExecutionResult.getExecution());
		initTestResults(pExecutionResult);
		detailPanel.add(resExexPanel, org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Execution"));
	}

	 public void initTestResults(ExecutionResult executionResult) {
		 testResultTableModel.clearTable();
	        Test[] pTabTest =  executionResult.getTestOrderedFromModel();
	        for (int j = 0; j < pTabTest.length ; j++){
	            Test test = pTabTest[j];
	            ArrayList data = new ArrayList();
	            data.add(test.getTestListFromModel().getFamilyFromModel().toString());
	            data.add(test.getTestListFromModel().toString());
	            data.add(test.toString());
	            ImageIcon image = Tools.getActionStatusIcon(executionResult.getTestResultStatusFromModel(test));
	            if (image == null) {
	                data.add(Tools.createAppletImageIcon(IHMConstants.PATH_TO_GOON_ICON, ""));
	            } else {
	                data.add(Tools.getActionStatusIcon(executionResult.getTestResultStatusFromModel(test)));
	            }
	            currentExecResult = executionResult;
	            testResultTableModel.addRow(data);

	        }
	    } // Fin de la m?thode initTestResults/1
/*************************** Listener ***********************************/

	public void actionPerformed(ActionEvent e){
		if (e.getSource().equals(linkDefectButton)){
			LinkDefectPerformed();
		}else if (editDefect != null && e.getSource().equals(editDefect)){
			editDefectPermed(defectToLink);
		}else if (e.getSource().equals(validateButton)){
			dispose();
		} else if (e.getSource().equals(editDefectButton)) {
			DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
			editDefectPermed(pDefectWrapper);
		} else if (e.getSource().equals(removelinkDefectButton)) {
			removelinkDefectPerformed();
		} else if (e.getSource().equals(addDefectButton)){
			addDefectPerformed();
		}
	}

	void addDefectPerformed(){
		Execution pExecution = null;
		ExecutionTestResult pExecutionTestResult = null;
		try {
			pExecutionTestResult = DataModel.getCurrentExecutionTestResult();
			pExecution = DataModel.getObservedExecutionResult().getExecution();
		}catch (Exception e1){
			e1.printStackTrace();
		}
		DefectView pDefectView = null;
		if (pExecution != null){
			pDefectView = new DefectView(this, pMantisPlugin, pExecution.getEnvironmentFromModel().getNameFromModel());
		} else {
			pDefectView = new DefectView(this, pMantisPlugin);
		}
		try {
			if (pDefectView.isDoingModification()){
				DefectWrapper pDefectWrapper = pDefectView.getDefectWrapper();
				if (pDefectWrapper != null){
					Attachment pAttch = pMantisPlugin.makeAttachement(pDefectWrapper.getId());
					pExecutionTestResult.addAttachementInDBAndModel(pAttch);
					doModification = true;
					defectsWrapperCache.put(new Integer(pDefectWrapper.getId()), pDefectWrapper);
					pDefectPanel.loadData(defectsWrapperCache);
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	void removelinkDefectPerformed() {
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper != null){
			if (pMantisPlugin.deleteConfirme(Language.getInstance().getText("Le_lien"))){
				try  {
					int defectID = pDefectWrapper.getId();
					ExecutionTestResult pExecutionTestResult = DataModel.getCurrentExecutionTestResult();
					HashMap attacMap = pExecutionTestResult.getAttachmentMapFromModel();
					Set keysSet = attacMap.keySet();
					boolean trouve = false;
					Iterator iter = keysSet.iterator();
					while (!trouve &&  iter.hasNext()) {
						Object attachName =  iter.next();
						Attachment pAttach = (Attachment) attacMap.get(attachName);
						int bugid = pMantisPlugin.getIDofBug(pAttach);
						if (bugid == defectID){
							pExecutionTestResult.deleteAttachementInDBAndModel(pAttach);
							defectsWrapperCache.remove(new Integer(bugid));
							pDefectPanel.loadData(defectsWrapperCache);
							trouve = true;
						}
					}
				} catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
	}

	void updateDefectPanel(boolean load){
		if (defectsWrapperCache == null){
			defectsWrapperCache = new Hashtable();
		}
		defectsWrapperCache.clear();
		if (!load){
			pDefectPanel.loadData(defectsWrapperCache);
			return;

		}
		ExecutionTestResult pExecutionTestResult = DataModel.getCurrentExecutionTestResult();

		//Hashtable defectsWrapper = new Hashtable();
		if (pExecutionTestResult != null){
			HashMap attacMap = pExecutionTestResult.getAttachmentMapFromModel();
			Set keysSet = attacMap.keySet();
			for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
				try {
					Object attachName =  iter.next();
					Attachment pAttach = (Attachment) attacMap.get(attachName);
					int bugid = pMantisPlugin.getIDofBug(pAttach);
					DefectWrapper pDefectWrapper = null;
					if (bugid != -1){
						pDefectWrapper = pMantisPlugin.getDefectInfo(bugid, false);
					}
					if (pDefectWrapper != null){
						defectsWrapperCache.put(new Integer(bugid), pDefectWrapper);
					}
				} catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		pDefectPanel.loadData(defectsWrapperCache);


		/*try {
			Hashtable allDefects = pMantisPlugin.getDefectsOfProject(false);
			Enumeration enumLikeddefect = linkedDefects.keys();
			while (enumLikeddefect.hasMoreElements()){
				Integer key = (Integer) enumLikeddefect.nextElement();
				allDefects.remove(key);
			}
			pDefectPanel.loadData(allDefects);
		} catch(Exception e){
			e.printStackTrace();
		}*/
	}

	void editDefectPermed(DefectWrapper pDefectWrapper){
		//DefectWrapper pDefectWrapper  = getSelectedDefect();
		if (pDefectWrapper !=null){
			DefectView pDefectView = new DefectView(this, pDefectWrapper, pMantisPlugin, true);

			if (pDefectView.isDoingModification()){
				doModification = true;
				if (defectToLink != null && defectToLink.getId() == pDefectWrapper.getId()) {
					idDefect.setText("id : " + defectToLink.getId());
					resumeDefect.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Resume_______") + " " + defectToLink.getResume());
					String str_value = (String) pMantisPlugin.priorityByID.get(Integer.valueOf(""+defectToLink.getPriority()));
					priorityDefect.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Priority") + " : " + str_value);
					str_value = (String) pMantisPlugin.statusByID.get(Integer.valueOf(""+defectToLink.getStatus()));
					stateDefect.setText(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Etat") + " : " + str_value);
				}
				pDefectPanel.loadData(defectsWrapperCache);
			}
		}
	}

	void LinkDefectPerformed() {
		if (defectToLink !=null){
			int bug_id = defectToLink.getId();
			if (bug_id >= 0){
				Integer id = new Integer(bug_id);
				DefectWrapper pDefectwrapper = (DefectWrapper) defectsWrapperCache.get(id);
				if (pDefectwrapper == null){ /* If the link is not present */
					try {
						Attachment pAttch = pMantisPlugin.makeAttachement(bug_id);
						ExecutionTestResult	pExecutionTestResult =  DataModel.getCurrentExecutionTestResult();
						pExecutionTestResult.addAttachementInDBAndModel(pAttch);
						defectsWrapperCache.put(id, defectToLink);
						pDefectPanel.reloadData();
					} catch (Exception e){
						e.printStackTrace();
					}
				}
			}
		}

	}

	boolean isDoingModification(){
		return doModification;
	}

	public void valueChanged(TreeSelectionEvent e) {
		DefaultMutableTreeNode selectNode = (DefaultMutableTreeNode) testTree.getSelectionPath().getLastPathComponent();
        if (selectNode != null) {
            Object selectObject = selectNode.getUserObject();
        	if (selectObject instanceof Campaign){
        		setCampaignPanel((Campaign) selectObject);
        	} else if (selectObject instanceof Execution){
        		setExecutionPanel((Execution) selectObject);
        	} else if (selectObject instanceof ExecutionResult){
        		System.out.println("ExecutionResult" + selectObject);
        		setResExecutionPanel((ExecutionResult) selectObject);
        	} else {
        		setEmptyPanel();
        	}
        }
	}
}
