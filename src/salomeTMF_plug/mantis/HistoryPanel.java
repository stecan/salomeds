package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.HistoryWrapper;

public class HistoryPanel extends JPanel{

	MyTableModel historyTableModel;
    TableSorter  sorter ;
    JTable historyTable;
    MantisPlugin pMantisPlugin;
    
     HistoryPanel(MantisPlugin pMantisPlugin, Vector history){
    	super();
		setLayout(new BorderLayout());
    	this.pMantisPlugin = pMantisPlugin;
		historyTable = new JTable();
    	historyTableModel = new MyTableModel();
    	historyTableModel.addColumnNameAndColumn("Date");
    	historyTableModel.addColumnNameAndColumn(Language.getInstance().getText("User")); //Produit
    	historyTableModel.addColumnNameAndColumn(Language.getInstance().getText("Field")); //Produit
    	historyTableModel.addColumnNameAndColumn(Language.getInstance().getText("Change")); //Etat
		

		sorter = new TableSorter(historyTableModel);
		historyTable.setModel(sorter);
        sorter.setTableHeader(historyTable.getTableHeader());
        historyTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane defectsTableScrollPane = new JScrollPane(historyTable);
        add(defectsTableScrollPane, BorderLayout.CENTER);
        loadData(history);
    }
     
     void loadData(Vector history){
    	if (history != null){
    		int size =  history.size();
    		for (int i = 0; i < size ; i++){
    			HistoryWrapper pHistoryWrapper = (HistoryWrapper) history.elementAt(i);
    			ArrayList data = new ArrayList();
    			data.add(pHistoryWrapper.getDate());
    			data.add(pHistoryWrapper.getUsername());
    			data.add(pHistoryWrapper.getField());
    			data.add(pHistoryWrapper.getChange(pMantisPlugin));
    			historyTableModel.addRow(data);
    		}
    	}
     }
}
