package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;

import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.main.IBugJDialog;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class DefectViewPanel extends JPanel implements ActionListener {

	/**
	 * Label de l'utilisateur
	 */
	JLabel userNameLabel;

	/**
	 * Label du Pojet
	 */
	JLabel projectLabel;

	/**
	 * label du logo
	 */
	JLabel logoLabel;

	/**
	 * Label pour la plateforme
	 */
	JLabel platformLabel;

	/**
	 * Label pour l'OS
	 */
	JLabel osLabel;

	/**
	 * Label pour les priorites
	 */
	JLabel priorityLabel;

	/**
	 * Label pour l'importance
	 */
	JLabel severityLabel;

	/**
	 * Label pour le produit
	 */
	JLabel environnementLabel;

	/**
	 * Label pour le destinataire
	 */
	JLabel assignedToLabel;

	/**
	 * Label pour l'url
	 */
	JLabel urlLabel;

	/**
	 * Label pour le resume
	 */
	JLabel summaryLabel;

	/**
	 * Label pour la description
	 */
	JLabel descriptionLabel;

	/**
	 * Label pour le status
	 */
	JLabel statusLabel;

	/**
	 * Label pour la reproductibilite
	 */
	JLabel reproducibilityLabel;
	/**
	 * Label pour la resolution
	 */
	JLabel resolutionLabel;

	/**
	 * Modele pour la comboBox de la plateforme
	 */
	DefaultComboBoxModel assignedToComboBoxModel;

	/**
	 * Modele pour la comboBox de la plateforme
	 */
	DefaultComboBoxModel platformComboBoxModel;

	/**
	 * Modele pour la comboBox des priorites
	 */
	DefaultComboBoxModel priorityComboBoxModel;

	/**
	 * Modele pour la comboBox de l'OS
	 */
	DefaultComboBoxModel osComboBoxModel;

	/**
	 * Modele pour la comboBox de l'importance
	 */
	DefaultComboBoxModel severityComboBoxModel;

	/**
	 * Modele pour la comboBox de status
	 */
	DefaultComboBoxModel statusComboBoxModel;

	/**
	 * Modele pour la comboBox de reproducibility
	 */
	DefaultComboBoxModel reproducibilityComboBoxModel;

	/**
	 * Modele pour la comboBox de resolution
	 */
	DefaultComboBoxModel resolutionComboBoxModel;

	/**
	 * ComboBox pour les environnement
	 */
	JComboBox comboEnvironnement;

	/**
	 * Bouton d'envoi
	 */
	JButton commitButton;

	/**
	 * Bouton de modification
	 */
	JButton modifyButton;

	/**
	 * Bouton de Visualisation
	 */
	// JButton viewButton;

	/**
	 * Bouton d'annulation
	 */
	JButton cancelButton;

	/**
	 * Champ texte pour le destinataire
	 */
	JTextField assignedToTextField;

	/**
	 * Champ texte pour le resume
	 */
	JTextField summaryTextField;

	/**
	 * Champ texte pour la description
	 */
	JTextArea descriptionArea;
	JScrollPane descriptionScrollPane;
	/**
	 * La liste des plateformes
	 */
	JComboBox platformComboBox;

	/**
	 * La liste des OS
	 */
	JComboBox osComboBox;

	/**
	 * La liste des priorites
	 */
	JComboBox priorityComboBox;

	/**
	 * La liste des importances
	 */
	JComboBox severityComboBox;

	/**
	 * La liste des destinataires
	 */
	JComboBox assignedToComboBox;

	/**
	 * La liste des status
	 */
	JComboBox statusToComboBox;

	/**
	 * La liste des reproductibilites
	 */
	JComboBox reproducibilityComboBox;

	/**
	 * La liste des resolutions
	 */
	JComboBox resolutionComboBox;

	MantisPlugin pMantisPlugin;
	DefectWrapper pDefectWrapper;

	boolean isEditView;

	/* Les Panels */
	JPanel infoPanel;
	JPanel statePanel;
	JPanel descriptionPanel;
	JPanel buttonPanel;

	boolean haveDoModification = false;

	IBugJDialog pIBugJDialog;

	/**
	 * Add Defect with information about test execution
	 *
	 * @param owner
	 * @param execResult
	 * @param executionTestResult
	 * @param actionName
	 * @param actionDesc
	 * @param actionAwatedRes
	 * @param actionEffectiveRes
	 * @param pMantisPlugin
	 */
	public DefectViewPanel(IBugJDialog pIBugJDialog,
			ExecutionResult execResult,
			ExecutionTestResult executionTestResult, String actionName,
			String actionDesc, String actionAwatedRes,
			String actionEffectiveRes, MantisPlugin pMantisPlugin) {
		super();
		this.pMantisPlugin = pMantisPlugin;
		this.pIBugJDialog = pIBugJDialog;
		isEditView = false;
		initComponent(isEditView);
		if (execResult != null && executionTestResult != null) {
			fillDefaultInformation(execResult, executionTestResult, actionName,
					actionDesc, actionAwatedRes, actionEffectiveRes);
		}
		projectLabel.setText(projectLabel.getText() + " : "
				+ DataModel.getCurrentProject().getNameFromModel());
		userNameLabel.setText(userNameLabel.getText() + " : "
				+ DataModel.getCurrentUser().getLoginFromModel());
		initPanel(isEditView);
		makePanel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
				.getText("Ajouter_un_bug_dans")
				+ " Mantis");
	}

	/**
	 * Add defect from empty description
	 *
	 * @param owner
	 * @param pMantisPlugin
	 */
	public DefectViewPanel(IBugJDialog pIBugJDialog, MantisPlugin pMantisPlugin) {
		super();
		this.pMantisPlugin = pMantisPlugin;
		this.pIBugJDialog = pIBugJDialog;
		isEditView = false;
		initComponent(isEditView);

		projectLabel.setText(projectLabel.getText() + " : "
				+ DataModel.getCurrentProject().getNameFromModel());
		userNameLabel.setText(userNameLabel.getText() + " : "
				+ DataModel.getCurrentUser().getLoginFromModel());

		initPanel(isEditView);
		makePanel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
				.getText("Ajouter_un_bug_dans")
				+ " Mantis");
	}

	/**
	 * Add defect with environement restriction
	 *
	 * @param owner
	 * @param pMantisPlugin
	 * @param envRestriction
	 */
	public DefectViewPanel(IBugJDialog pIBugJDialog,
			MantisPlugin pMantisPlugin, String envRestriction) {
		super();
		this.pMantisPlugin = pMantisPlugin;
		this.pIBugJDialog = pIBugJDialog;
		isEditView = false;
		initComponent(isEditView);

		projectLabel.setText(projectLabel.getText() + " : "
				+ DataModel.getCurrentProject().getNameFromModel());
		userNameLabel.setText(userNameLabel.getText() + " : "
				+ DataModel.getCurrentUser().getLoginFromModel());
		applyEnvRestriction(envRestriction);
		initPanel(isEditView);
		makePanel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
				.getText("Ajouter_un_bug_dans")
				+ " Mantis");
	}

	/**
	 * Edit existing defect
	 *
	 * @param owner
	 * @param pDefectWrapper
	 * @param pMantisPlugin
	 */
	public DefectViewPanel(IBugJDialog pIBugJDialog,
			DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin) {
		super();
		this.pMantisPlugin = pMantisPlugin;
		this.pIBugJDialog = pIBugJDialog;
		this.pDefectWrapper = pDefectWrapper;
		isEditView = true;
		initComponent(isEditView);
		projectLabel.setText(projectLabel.getText() + " : "
				+ DataModel.getCurrentProject().getNameFromModel());
		fillDefect(pDefectWrapper);
		initPanel(isEditView);
		makePanel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
				.getText("Ajouter_un_bug_dans")
				+ " Mantis");

	}

	void makePanel(String title) {
		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(statePanel, BorderLayout.NORTH);
		mainPanel.add(descriptionPanel, BorderLayout.CENTER);
		mainPanel.add(buttonPanel, BorderLayout.SOUTH);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(infoPanel);
		add(mainPanel);

	}

	void initComponent(boolean isEditView) {

		/* Les Infos */
		userNameLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Utilisateurs")
						+ " : ");
		projectLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Projet")
						+ " : ");
		logoLabel = new JLabel();
		logoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/salomeTMF_plug/mantis/resources/mantis_logo.gif")));

		/* Les ComboBox */
		osLabel = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language
				.getInstance().getText("OS_"));
		osComboBoxModel = new DefaultComboBoxModel();
		Vector osNames = pMantisPlugin.getBugOSList();
		if (osNames != null) {
			for (int i = 0; i < osNames.size(); i++) {
				osComboBoxModel.addElement(osNames.elementAt(i));
			}
		}
		osComboBox = new JComboBox(osComboBoxModel);
		osComboBox.setEditable(pMantisPlugin.isEditableOS());

		priorityLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Priorite__"));
		priorityComboBoxModel = new DefaultComboBoxModel();
		Vector prioritiesNames = pMantisPlugin.priority_values;
		if (prioritiesNames != null) {
			for (int i = 0; i < prioritiesNames.size(); i++) {
				priorityComboBoxModel.addElement(prioritiesNames.elementAt(i));
			}
		}
		priorityComboBox = new JComboBox(priorityComboBoxModel);

		platformLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Plateforme__"));
		platformComboBoxModel = new DefaultComboBoxModel();
		Vector platformsNames = pMantisPlugin.getBugPlateformList();
		if (platformsNames != null) {
			for (int i = 0; i < platformsNames.size(); i++) {
				platformComboBoxModel.addElement(platformsNames.elementAt(i));
			}
		}
		platformComboBox = new JComboBox(platformComboBoxModel);
		platformComboBox.setEditable(pMantisPlugin.isEditablePlateForme());

		severityLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Importance__"));
		severityComboBoxModel = new DefaultComboBoxModel();
		Vector severityNames = pMantisPlugin.severity_values;
		if (severityNames != null) {
			for (int i = 0; i < severityNames.size(); i++) {
				severityComboBoxModel.addElement(severityNames.elementAt(i));
			}
		}
		severityComboBox = new JComboBox(severityComboBoxModel);
		severityComboBox.setEditable(false);

		assignedToLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Destinataire__"));
		assignedToComboBoxModel = new DefaultComboBoxModel();
		Vector assignedToVector = pMantisPlugin.getBugTrackerAllUsers();
		if (assignedToVector != null) {
			for (int i = 0; i < assignedToVector.size(); i++) {
				assignedToComboBoxModel.addElement(assignedToVector.get(i));
			}
		}
		assignedToComboBox = new JComboBox(assignedToComboBoxModel);
		assignedToComboBox.setEditable(false);

		environnementLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Environnement")
						+ " :");
		ArrayList listOfEnv = DataModel.getCurrentProject()
				.getEnvironmentListFromModel();
		Vector choiceEnv = new Vector();
		int size = listOfEnv.size();
		for (int i = 0; i < size; i++) {
			choiceEnv.add(((Environment) listOfEnv.get(i)).getNameFromModel());
		}
		choiceEnv.add("___NO_ENV___");
		comboEnvironnement = new JComboBox(choiceEnv);
		comboEnvironnement.setEditable(false);

		statusLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Statut")
						+ " : ");
		statusComboBoxModel = new DefaultComboBoxModel();
		Vector statusNames = pMantisPlugin.status_values;
		if (statusNames != null) {
			for (int i = 0; i < statusNames.size(); i++) {
				statusComboBoxModel.addElement(statusNames.elementAt(i));
			}
		}
		statusToComboBox = new JComboBox(statusComboBoxModel);
		statusToComboBox.setEditable(false);

		reproducibilityLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("reproducibility"));
		reproducibilityComboBoxModel = new DefaultComboBoxModel();
		Vector reproducibilityNames = pMantisPlugin.reproductibility_values;
		for (int i = 0; i < reproducibilityNames.size(); i++) {
			reproducibilityComboBoxModel
					.addElement(reproducibilityNames.get(i));
		}
		reproducibilityComboBox = new JComboBox(reproducibilityComboBoxModel);

		resolutionLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("resolution"));
		resolutionComboBoxModel = new DefaultComboBoxModel();
		Vector resolutionNames = pMantisPlugin.resolution_values;
		for (int i = 0; i < resolutionNames.size(); i++) {
			resolutionComboBoxModel.addElement(resolutionNames.get(i));
		}
		resolutionComboBox = new JComboBox(resolutionComboBoxModel);

		/* Les Zones de textes */
		summaryLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Resume_______"));
		summaryTextField = new JTextField();

		descriptionLabel = new JLabel(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Description__"));
		descriptionArea = new JTextArea();
		descriptionScrollPane = new JScrollPane(descriptionArea);
		descriptionScrollPane.setPreferredSize(new Dimension(500, 200));

		/* Les Boutons */
		commitButton = new JButton(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Envoyer"));
		commitButton.addActionListener(this);
		modifyButton = new JButton(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Modifier"));
		modifyButton.addActionListener(this);
		// viewButton = new
		// JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Visualiser"));
		cancelButton = new JButton(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Annuler"));
		cancelButton.addActionListener(this);

	}

	void initPanel(boolean isEditView) {
		/************* Diposotion *****************/
		infoPanel = new JPanel(new GridLayout(2, 2));
		infoPanel.add(logoLabel);
		infoPanel.add(new JLabel());
		infoPanel.add(projectLabel);
		infoPanel.add(userNameLabel);

		if (isEditView) {
			statePanel = new JPanel(new GridLayout(4, 5));
			/* Ligne 1 */
			statePanel.add(severityLabel);
			statePanel.add(priorityLabel);
			statePanel.add(reproducibilityLabel);
			statePanel.add(environnementLabel);
			statePanel.add(statusLabel);

			/* Ligne 2 */
			statePanel.add(severityComboBox);
			statePanel.add(priorityComboBox);
			statePanel.add(reproducibilityComboBox);
			statePanel.add(comboEnvironnement);
			statePanel.add(statusToComboBox);

			/* Ligne 3 */
			statePanel.add(resolutionLabel);
			statePanel.add(assignedToLabel);
			statePanel.add(platformLabel);
			statePanel.add(osLabel);
			statePanel.add(new JLabel(""));
			/* Ligne 4 */
			statePanel.add(resolutionComboBox);
			statePanel.add(assignedToComboBox);
			statePanel.add(platformComboBox);
			statePanel.add(osComboBox);

		} else {
			statePanel = new JPanel(new GridLayout(4, 4));
			/* Ligne 1 */
			statePanel.add(severityLabel);
			statePanel.add(priorityLabel);
			statePanel.add(reproducibilityLabel);
			statePanel.add(environnementLabel);

			/* Ligne 2 */
			statePanel.add(severityComboBox);
			statePanel.add(priorityComboBox);
			statePanel.add(reproducibilityComboBox);
			statePanel.add(comboEnvironnement);

			/* Ligne 3 */

			statePanel.add(new JLabel(""));
			statePanel.add(assignedToLabel);
			statePanel.add(platformLabel);
			statePanel.add(osLabel);

			/* Ligne 4 */
			statePanel.add(new JLabel(""));
			statePanel.add(assignedToComboBox);
			statePanel.add(platformComboBox);
			statePanel.add(osComboBox);
		}

		descriptionPanel = new JPanel(new BorderLayout());

		JPanel resumePanel1 = new JPanel();
		JPanel resumePanel = new JPanel();

		resumePanel1.setLayout(new BoxLayout(resumePanel1, BoxLayout.X_AXIS));
		resumePanel1.add(summaryLabel);

		resumePanel1.add(summaryTextField);
		resumePanel.setLayout(new BoxLayout(resumePanel, BoxLayout.Y_AXIS));
		resumePanel.add(Box.createRigidArea(new Dimension(10, 10)));
		resumePanel.add(resumePanel1);
		resumePanel.add(Box.createRigidArea(new Dimension(10, 10)));

		JPanel textPanel = new JPanel(new BorderLayout());
		textPanel.add(descriptionLabel, BorderLayout.NORTH);
		textPanel.add(descriptionScrollPane, BorderLayout.CENTER);

		descriptionPanel.add(resumePanel, BorderLayout.NORTH);
		descriptionPanel.add(textPanel, BorderLayout.CENTER);

		buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		if (isEditView) {
			buttonPanel.add(modifyButton);
			buttonPanel.add(cancelButton);
		} else {
			buttonPanel.add(commitButton);
			buttonPanel.add(cancelButton);
		}
	}

	void applyEnvRestriction(String envName) {
		int size = comboEnvironnement.getItemCount();
		int i = 0;
		boolean trouve = false;
		if (envName != null) {
			while (i < size && !trouve) {
				String value = ((String) comboEnvironnement.getItemAt(i))
						.trim();
				if (value.equals(envName)) {
					trouve = true;
					comboEnvironnement.setSelectedIndex(i);
					if (!(value.equals("___NO_ENV___") || value.equals(""))) {
						comboEnvironnement.setEnabled(false);
					}
				}
				i++;
			}
		}
		if (!trouve) {
			comboEnvironnement.addItem(envName);
			comboEnvironnement.setSelectedItem(envName);
			comboEnvironnement.setEnabled(false);
		}
	}

	void fillDefect(DefectWrapper pDefectWrapper) {
		/* On peut pas changer l'env si != "" ou noenv */
		String user = pDefectWrapper.getUser();
		userNameLabel.setText(userNameLabel.getText() + " : " + user);

		boolean trouve = false;
		int size = 0;
		int i = 0;
		String valueToSearch;
		/* Severite */
		valueToSearch = (String) pMantisPlugin.severityByID.get(Integer
				.valueOf("" + pDefectWrapper.getSeverity()));
		valueToSearch = valueToSearch.trim();
		size = severityComboBox.getItemCount();
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) severityComboBox.getItemAt(i)).trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					severityComboBox.setSelectedIndex(i);
				}
				i++;
			}

		}

		/* Priority */
		valueToSearch = (String) pMantisPlugin.priorityByID.get(Integer
				.valueOf("" + pDefectWrapper.getPriority()));
		valueToSearch = valueToSearch.trim();
		size = priorityComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) priorityComboBox.getItemAt(i)).trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					priorityComboBox.setSelectedIndex(i);
				}
				i++;
			}

		}

		/* Repreductability */
		valueToSearch = (String) pMantisPlugin.reproductibilityByID.get(Integer
				.valueOf("" + pDefectWrapper.getReproducibility()));
		valueToSearch = valueToSearch.trim();
		size = reproducibilityComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) reproducibilityComboBox.getItemAt(i))
						.trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					reproducibilityComboBox.setSelectedIndex(i);
				}
				i++;
			}

		}

		/* Environnement */
		valueToSearch = (String) pDefectWrapper.getEnvironement();
		if (valueToSearch == null) {
			valueToSearch = "";
		}
		valueToSearch = valueToSearch.trim();
		size = comboEnvironnement.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) comboEnvironnement.getItemAt(i))
						.trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					comboEnvironnement.setSelectedIndex(i);
					if (!(value.equals("___NO_ENV___") || value.equals(""))) {
						comboEnvironnement.setEnabled(false);
					}
				}
				i++;
			}
		}
		if (!trouve) {
			comboEnvironnement.addItem(valueToSearch);
			comboEnvironnement.setSelectedItem(valueToSearch);
		}

		/* Status */
		valueToSearch = (String) pMantisPlugin.statusByID.get(Integer
				.valueOf("" + pDefectWrapper.getStatus()));
		valueToSearch = valueToSearch.trim();
		size = statusToComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) statusToComboBox.getItemAt(i)).trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					statusToComboBox.setSelectedIndex(i);
				}
				i++;
			}
		}

		/* Resolution */
		valueToSearch = (String) pMantisPlugin.resolutionByID.get(Integer
				.valueOf("" + pDefectWrapper.getResolution()));
		valueToSearch = valueToSearch.trim();
		size = resolutionComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) resolutionComboBox.getItemAt(i))
						.trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					resolutionComboBox.setSelectedIndex(i);
				}
				i++;
			}
		}

		/* Recipiant */
		valueToSearch = pDefectWrapper.getRecipient();
		if (valueToSearch == null) {
			valueToSearch = "";
		}
		valueToSearch = valueToSearch.trim();
		assignedToComboBox.addItem("");
		size = assignedToComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) assignedToComboBox.getItemAt(i))
						.trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					assignedToComboBox.setSelectedIndex(i);
				}
				i++;
			}
		}

		/* Plateforme */
		valueToSearch = pDefectWrapper.getPlateforme();
		if (valueToSearch == null) {
			valueToSearch = "";
		}
		valueToSearch = valueToSearch.trim();
		size = platformComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) platformComboBox.getItemAt(i)).trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					platformComboBox.setSelectedIndex(i);
				}
				i++;
			}
		}
		if (!trouve) {
			platformComboBox.addItem(valueToSearch);
			platformComboBox.setSelectedItem(valueToSearch);
		}

		/* OS */
		valueToSearch = pDefectWrapper.getOs();
		if (valueToSearch == null) {
			valueToSearch = "";
		}
		valueToSearch = valueToSearch.trim();
		size = osComboBox.getItemCount();
		i = 0;
		trouve = false;
		if (valueToSearch != null) {
			while (i < size && !trouve) {
				String value = ((String) osComboBox.getItemAt(i)).trim();
				if (value.equals(valueToSearch)) {
					trouve = true;
					osComboBox.setSelectedIndex(i);
				}
				i++;
			}
		}
		if (!trouve) {
			osComboBox.addItem(valueToSearch);
			osComboBox.setSelectedItem(valueToSearch);
		}

		/* Resume */
		summaryTextField.setText(pDefectWrapper.getResume());

		/* Details */
		descriptionArea.setText(pDefectWrapper.getDescription());
	}

	void fillDefaultInformation(ExecutionResult execResult,
			ExecutionTestResult executionTestResult, String actionName,
			String actionDesc, String actionAwatedRes, String actionEffectiveRes) {
		Test testInExec = executionTestResult.getTestFromModel();
		Execution exec = execResult.getExecution();
		Environment environment = exec.getEnvironmentFromModel();

		// Convert HTML description to text for a test
		String testHtmlDesc = testInExec.getDescriptionFromModel();
		String testTextDesc = "";

		EditorKit kit = new HTMLEditorKit();
		Document doc = kit.createDefaultDocument();
		// The Document class does not yet handle charset's properly.
		doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
		try {
			// Create a reader on the HTML content.
			Reader rd = new StringReader(testHtmlDesc);
			// Parse the HTML.
			kit.read(rd, doc, 0);
			testTextDesc = doc.getText(0, doc.getLength());
		} catch (Exception e) {
			e.printStackTrace();
		}

		String bugDescription = "";
		bugDescription = bugDescription
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("__Campagne_")
				+ exec.getCampagneFromModel().getNameFromModel()
				+ "\n"
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("__Execution_")
				+ exec.getNameFromModel()
				+ "\n"
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("__Test_famille_")
				+ testInExec.getTestListFromModel().getFamilyFromModel()
						.getNameFromModel()
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("_suite_")
				+ testInExec.getTestListFromModel().getNameFromModel()
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("_test_")
				+ testInExec.getNameFromModel()
				+ "]"
				+ "\n"
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("__Description_du_test_") + testTextDesc
				+ "\n";

		DataSet dataSet = exec.getDataSetFromModel();
		String dataSetStr = "\n- "
				+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Jeu_de_donnees") + " : "
				+ dataSet.getNameFromModel() + "\n";
		Set dataSetKeysSet = dataSet.getParametersHashMapFromModel().keySet();
		// Environment env =
		// PluginsTools.getCurrentCampExecution().getEnvironmentFromModel();
		if (!dataSetKeysSet.isEmpty()) {
			for (Iterator iter = dataSetKeysSet.iterator(); iter.hasNext();) {
				String paramName = ((String) iter.next()).trim();
				String value = dataSet.getParameterValueFromModel(paramName)
						.trim();
				if (value != null && !value.equals("")) {
					if (value.startsWith(DataConstants.PARAM_VALUE_FROM_ENV)) {
						if (environment != null) {
							value = environment.getParameterValue(paramName);
						}
					}
					dataSetStr += "\t" + paramName + " = " + value + "\n";
				}

			}
			bugDescription += dataSetStr + "\n";
		}

		if (actionName != null) {
			bugDescription += org.objectweb.salome_tmf.ihm.languages.Language
					.getInstance().getText("_Action")
					+ actionName
					+ "\n"
					+ org.objectweb.salome_tmf.ihm.languages.Language
							.getInstance().getText("_Action_desc")
					+ actionDesc
					+ "\n"
					+ org.objectweb.salome_tmf.ihm.languages.Language
							.getInstance().getText("_Action_res_attendu")
					+ actionAwatedRes
					+ "\n"
					+ org.objectweb.salome_tmf.ihm.languages.Language
							.getInstance().getText("_Action_res_effectif")
					+ actionEffectiveRes;
		}

		String bugAdditionalDesc = pMantisPlugin.getAdditionalBugDesc();
		if ((bugAdditionalDesc != null) && (bugAdditionalDesc != "")) {
			bugDescription += bugAdditionalDesc;
		}
		descriptionArea.setText(bugDescription);
		/* Env */
		String str_env = environment.getNameFromModel().trim();
		comboEnvironnement.removeAllItems();
		comboEnvironnement.addItem(str_env);
		comboEnvironnement.setEditable(false);
		comboEnvironnement.setEnabled(false);
	}

	/*************************** Listener ***********************************/

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(cancelButton)) {
			haveDoModification = false;
			// dispose();
			pIBugJDialog.onCancelPerformed();
		} else if (e.getSource().equals(commitButton)) {
			commitPerformed();
		} else if (e.getSource().equals(modifyButton)) {
			modifyPerformed();
		}
	}

	void makeDefectWrapper() {
		boolean modification = true;
		if (pDefectWrapper == null) {
			pDefectWrapper = new DefectWrapper();
			modification = false;
		}
		String str_value;
		Integer id;

		pDefectWrapper.setDescription(descriptionArea.getText());
		str_value = ((String) comboEnvironnement.getSelectedItem()).trim();
		if (str_value.equals("")) {
			str_value = "___NO_ENV___";
		}
		pDefectWrapper.setEnvironement(str_value);

		try {
			str_value = ((String) osComboBox.getSelectedItem()).trim();
		} catch (Exception e) {
			str_value = "";
		}
		pDefectWrapper.setOs(str_value);

		try {
			str_value = ((String) platformComboBox.getSelectedItem()).trim();
		} catch (Exception e) {
			str_value = "";
		}
		pDefectWrapper.setPlateforme(str_value);

		str_value = ((String) priorityComboBox.getSelectedItem()).trim();
		id = (Integer) pMantisPlugin.priorityByValues.get(str_value);
		pDefectWrapper.setPriority(id.intValue());

		if (assignedToComboBox.getSelectedItem() != null) {
			str_value = ((String) assignedToComboBox.getSelectedItem()).trim();
			pDefectWrapper.setRecipient(str_value);
		}

		str_value = ((String) reproducibilityComboBox.getSelectedItem()).trim();
		id = (Integer) pMantisPlugin.reproductibilityByValues.get(str_value);
		pDefectWrapper.setReproducibility(id.intValue());

		if (modification == true) {
			/* Only on modification */
			str_value = ((String) resolutionComboBox.getSelectedItem()).trim();
			id = (Integer) pMantisPlugin.resolutionByValues.get(str_value);
			pDefectWrapper.setResolution(id.intValue());
		}

		str_value = summaryTextField.getText();
		pDefectWrapper.setResume(str_value);

		str_value = ((String) severityComboBox.getSelectedItem()).trim();
		id = (Integer) pMantisPlugin.severityByValues.get(str_value);
		pDefectWrapper.setSeverity(id.intValue());

		str_value = ((String) statusToComboBox.getSelectedItem()).trim();
		id = (Integer) pMantisPlugin.statusByValues.get(str_value);
		pDefectWrapper.setStatus(id.intValue());

		pDefectWrapper.setUrl(null);
		if (modification == false) {
			pDefectWrapper.setUser(DataModel.getCurrentUser()
					.getLoginFromModel());
		}
	}

	void modifyPerformed() {
		haveDoModification = false;
		makeDefectWrapper();
		if (pDefectWrapper != null) {
			try {
				pMantisPlugin.modifyBug(pDefectWrapper);
				haveDoModification = true;
			} catch (Exception e) {
				pDefectWrapper = null;
				e.printStackTrace();
			}
		}
		// dispose();
		pIBugJDialog.onModifyPerformed();
	}

	void commitPerformed() {
		haveDoModification = false;
		String summary = summaryTextField.getText().trim();
		if ((summary == null) || (summary.equals(""))) {
			JOptionPane.showMessageDialog(this,
					org.objectweb.salome_tmf.ihm.languages.Language
							.getInstance().getText("Must_fill_summary"),
					org.objectweb.salome_tmf.ihm.languages.Language
							.getInstance().getText("Erreur_"),
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		makeDefectWrapper();
		int id = -1;
		if (pDefectWrapper != null) {
			try {
				id = pMantisPlugin.addBug(pDefectWrapper);
				pDefectWrapper.setId(id);
				haveDoModification = true;
			} catch (Exception e) {
				pDefectWrapper = null;
				e.printStackTrace();
			}
		}
		if (id != -1) {
			pIBugJDialog.onCommitPerformed(pMantisPlugin.makeAttachement(id));
		} else {
			pIBugJDialog.onCommitPerformed(null);
		}
	}

	public boolean isDoingModification() {
		return haveDoModification;
	}
}
