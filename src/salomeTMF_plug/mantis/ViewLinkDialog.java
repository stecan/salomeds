package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;

import javax.swing.JDialog;

import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class ViewLinkDialog extends JDialog{

	MantisPlugin pMantisPlugin;
	ViewLinkPanel pViewLinkPanel;
	
	ViewLinkDialog(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		super(SalomeTMFContext.getInstance().getSalomeFrame(),true);
		setModal(true);
		this.pMantisPlugin = pMantisPlugin;
		pViewLinkPanel = new ViewLinkPanel(this, pDefectWrapper, pMantisPlugin);
		makeDialog(Language.getInstance().getText("Liason"));
	}
	
	ViewLinkDialog(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin, Test pTest){
		super(SalomeTMFContext.getInstance().getSalomeFrame(),true);
		setModal(true);
		this.pMantisPlugin = pMantisPlugin;
		pViewLinkPanel = new ViewLinkPanel(this, pDefectWrapper, pMantisPlugin, pTest);
		makeDialog(Language.getInstance().getText("Liason"));
	}
	

	void makeDialog(String title){
		int t_x = 1024 - 100;
		int t_y = 768 - 50;
		int t_x2 = 1024;
		int t_y2 = 768;
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice[] gs = ge.getScreenDevices();
			GraphicsDevice gd = gs[0];
			GraphicsConfiguration[] gc = gd.getConfigurations();
			Rectangle r = gc[0].getBounds();
			t_x = r.width - 100;
			t_y = r.height -50 ;
			t_x2 = r.width;
			t_y2 = r.height ;
		} catch(Exception E){
			
		}
		
		int seq_x = t_x/6;
		int seq_y = t_y/6;
		
		Container contentPane = this.getContentPane(); 
		contentPane.setLayout(new BorderLayout());
		contentPane.add(pViewLinkPanel, BorderLayout.CENTER);
		pViewLinkPanel.setPreferredSize(new Dimension(seq_x*4,seq_x*2));
		this.setTitle(title);
		setSize(seq_x*4*4, seq_y*3);
		/*this.pack();
		this.setLocationRelativeTo(this.getParent()); 
		this.setVisible(true);*/
		centerScreen();
	}
	
	void centerScreen() {
		  Dimension dim = getToolkit().getScreenSize();
		  this.pack();
		  Rectangle abounds = getBounds();
		  setLocation((dim.width - abounds.width) / 2,
		      (dim.height - abounds.height) / 2);	 
	      this.setVisible(true); 
		  requestFocus();
	}
}
