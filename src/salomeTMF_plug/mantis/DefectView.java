package salomeTMF_plug.mantis;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JTabbedPane;

import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.ihm.main.IBugJDialog;

import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;



public class DefectView extends JDialog implements  IBugJDialog{ 
	
	boolean haveDoModification = false;
	boolean viewLink = false;
	
	
	DefectViewPanel pDefectViewPanel; 
	DefectPanel pLinkDefectPanel;
	HistoryPanel pHistoryPanel;
	ViewLinkPanel pViewLinkExecPanel;
	
	
	JTabbedPane mainPane;
	/**
	 * Add Defect with information about test execution
	 * @param owner
	 * @param execResult
	 * @param executionTestResult
	 * @param actionName
	 * @param actionDesc
	 * @param actionAwatedRes
	 * @param actionEffectiveRes
	 * @param pMantisPlugin
	 */
	public DefectView(Dialog owner, ExecutionResult execResult, ExecutionTestResult executionTestResult,
			String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes, MantisPlugin pMantisPlugin){
		super(owner,true);
		pDefectViewPanel = new DefectViewPanel(this, execResult, executionTestResult, actionName, actionDesc, actionAwatedRes, actionEffectiveRes, pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	
	public DefectView(Frame owner, ExecutionResult execResult, ExecutionTestResult executionTestResult,
			String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes, MantisPlugin pMantisPlugin){
		super(owner,true);
		pDefectViewPanel = new DefectViewPanel(this, execResult, executionTestResult, actionName, actionDesc, actionAwatedRes, actionEffectiveRes, pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	
	
	/**
	 * Add defect from empty description
	 * @param owner
	 * @param pMantisPlugin
	 */
	public DefectView(Dialog owner, MantisPlugin pMantisPlugin){
		super(owner,true);
		setModal(true);
		pDefectViewPanel = new DefectViewPanel(this,  pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	
	public DefectView(Frame owner, MantisPlugin pMantisPlugin){
		super(owner,true);
		setModal(true);
		pDefectViewPanel = new DefectViewPanel(this,  pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	
	/**
	 * Add defect with environement restriction
	 * @param owner
	 * @param pMantisPlugin
	 * @param envRestriction
	 */
	public DefectView(Dialog owner, MantisPlugin pMantisPlugin, String envRestriction){
		super(owner,true);
		setModal(true);
		pDefectViewPanel = new DefectViewPanel(this,  pMantisPlugin, envRestriction);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	public DefectView(Frame owner, MantisPlugin pMantisPlugin, String envRestriction){
		super(owner,true);
		setModal(true);
		pDefectViewPanel = new DefectViewPanel(this,  pMantisPlugin, envRestriction);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
	}
	
	/**
	 * Edit existing defect
	 * @param owner
	 * @param pDefectWrapper
	 * @param pMantisPlugin
	 */
	/*public DefectView(Dialog owner, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		super(owner,true);
		pDefectViewPanel = new DefectViewPanel(this,  pDefectWrapper, pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
		
	}
	public DefectView(Frame owner, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		super(owner,true);
		pDefectViewPanel = new DefectViewPanel(this,  pDefectWrapper, pMantisPlugin);
		makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
		
	}*/
	/**
	 * Edit existing defect
	 * @param owner
	 * @param pDefectWrapper
	 * @param pMantisPlugin
	 */
	public DefectView(Dialog owner, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin, boolean viewLink){
		super(owner,true);
		this.viewLink = viewLink;
		if (viewLink){
			makeViewLinkPanel(pDefectWrapper, pMantisPlugin);
			makeHistoryPanel(pDefectWrapper, pMantisPlugin);
			makeViewLinkExecPanel(pDefectWrapper, pMantisPlugin);
		}
		pDefectViewPanel = new DefectViewPanel(this,  pDefectWrapper, pMantisPlugin);
		//makeDialog(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Ajouter_un_bug_dans")+ " Mantis");	
		if (pDefectWrapper != null){
			makeDialog(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Visualiser_bugs")+ " id : " + pDefectWrapper.getId());	
		} else {
			makeDialog(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Visualiser_bugs"));	
		}
	}
	public DefectView(Frame owner, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin, boolean viewLink){
		super(owner,true);
		this.viewLink = viewLink;
		if (viewLink){
			makeViewLinkPanel(pDefectWrapper, pMantisPlugin);
			makeHistoryPanel(pDefectWrapper, pMantisPlugin);
			makeViewLinkExecPanel(pDefectWrapper, pMantisPlugin);
		}
		pDefectViewPanel = new DefectViewPanel(this,  pDefectWrapper, pMantisPlugin);
		if (pDefectWrapper != null){
			makeDialog(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Visualiser_bugs")+ " id : " + pDefectWrapper.getId());	
		} else {
			makeDialog(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Visualiser_bugs"));	
		}
	}
	
	void makeViewLinkPanel(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		Hashtable defectsLinked;
		try {
			defectsLinked = pMantisPlugin.getDefectLink(pDefectWrapper);
			LinkWithDefectsPanel pLinkWithDefectsPanel = new LinkWithDefectsPanel(this, pDefectWrapper, pMantisPlugin);
			pLinkDefectPanel = new DefectPanel(true,pMantisPlugin, pLinkWithDefectsPanel, null);
			pLinkWithDefectsPanel.setDefectPanel(pLinkDefectPanel);
			pLinkDefectPanel.loadData(defectsLinked);
		} catch (Exception e){
			e.printStackTrace();
			pLinkDefectPanel = null;
		}
	}
	
	void makeHistoryPanel(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin) {
		Vector bugHistories = null;
		try {
			bugHistories = pMantisPlugin.getBugHistory(pDefectWrapper);
		} catch(Exception e){
			e.printStackTrace();
		}
		
		pHistoryPanel = new HistoryPanel(pMantisPlugin, bugHistories);
		
	}
	
	void makeViewLinkExecPanel(DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		pViewLinkExecPanel = new ViewLinkPanel(this,pDefectWrapper, pMantisPlugin);
	}
	
	void makeDialog(String title){
		int t_x = 1024 - 100;
		int t_y = 768/3*2 - 50;
		try {
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice[] gs = ge.getScreenDevices();
			GraphicsDevice gd = gs[0];
			GraphicsConfiguration[] gc = gd.getConfigurations();
			Rectangle r = gc[0].getBounds();
			t_x = r.width - 100;
			t_y = r.height/3*2 -50 ;
		} catch(Exception E){
			
		}
		
		Container contentPane = this.getContentPane(); 
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		if (viewLink && pLinkDefectPanel != null){
			mainPane = new JTabbedPane();
			mainPane.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Anomalie"),pDefectViewPanel);
			mainPane.add(salomeTMF_plug.mantis.languages.Language.getInstance().getText("lien_anomalie"),pLinkDefectPanel);
			mainPane.add(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Liason"),pViewLinkExecPanel);
			mainPane.add(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Issue_History"),pHistoryPanel);
			contentPane.add(mainPane);
		} else {
			contentPane.add(pDefectViewPanel);
		}
		this.setTitle(title);
		setSize(t_x, t_y);
		/* this.pack();
		 this.setLocationRelativeTo(this.getParent()); 
		 this.setVisible(true);
		 */
		centerScreen();
	}
	
	 void centerScreen() {
		  Dimension dim = getToolkit().getScreenSize();
		  this.pack();
		  Rectangle abounds = getBounds();
		  setLocation((dim.width - abounds.width) / 2,
		      (dim.height - abounds.height) / 2);	 
	      this.setVisible(true); 
		  requestFocus();
	}
	
	boolean isDoingModification(){
		return pDefectViewPanel.isDoingModification();
	}
	
	DefectWrapper getDefectWrapper(){
		return pDefectViewPanel.pDefectWrapper;
	}
	
	public void onViewPerformed(){
		dispose();
	}
	
	public void onModifyPerformed(){
		dispose();
	}
	
	public void onCancelPerformed(){
		dispose();
	}
	
    public void onCommitPerformed(Attachment bugURL){
		dispose();
	}
}
