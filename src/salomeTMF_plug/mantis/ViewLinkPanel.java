package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.main.ActionDetailsView;
import org.objectweb.salome_tmf.ihm.main.AttachmentViewWindow;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;

import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class ViewLinkPanel extends JPanel implements ActionListener,  ListSelectionListener{
	
	
	MyTableModel execTestTableModel;
    TableSorter  sorter ;
    JTable execTestTable;
    JScrollPane execTestTableScrollPane;
	Vector ligneTbable;
	
	
	JButton viewCampButton; 
	JButton viewTestButton; 
	JButton viewExecTestButton; 
	JButton closeButton; 
	JPanel buttonsPanel;
	
	JDialog pDialog;
	MantisPlugin pMantisPlugin;
	boolean viewTest = true ; // else view resexectest
	ViewLinkPanel(JDialog pDialog, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		this.pDialog = pDialog;
		this.pMantisPlugin = pMantisPlugin;
		ligneTbable = new Vector();
		viewTest = true;
		initComponent();
		loadData(pDefectWrapper, null);
	}
	
	ViewLinkPanel(JDialog pDialog, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin, Test pTes){
		this.pDialog = pDialog;
		this.pMantisPlugin = pMantisPlugin;
		ligneTbable = new Vector();
		viewTest = false;
		initComponent();
		loadData(pDefectWrapper, pTes);
	}
	
	
	void initComponent(){
		execTestTable = new JTable();
		
		execTestTableModel = new MyTableModel();
		execTestTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Environnements")); //Produit
		execTestTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Campagnes")); //Produit
		execTestTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Executions") + "/" + org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Resultats"));
		execTestTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Familles") + "/" + org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Suites")); //Produit
		execTestTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tests")); //Etat
		
		sorter = new TableSorter(execTestTableModel);
		execTestTable.setModel(sorter);
        sorter.setTableHeader(execTestTable.getTableHeader());
        execTestTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        execTestTableScrollPane = new JScrollPane(execTestTable);
        ListSelectionModel rowSM = execTestTable.getSelectionModel();
    	rowSM.addListSelectionListener(this);
        
        
    	closeButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Fermer") );
    	closeButton.addActionListener(this);
    	
    	viewCampButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Visualiser") + " "+ org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Campagne"));
        viewCampButton.addActionListener(this);
    	viewCampButton.setEnabled(false);
    	
    	viewTestButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Visualiser") + " " + org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Test"));	
    	viewTestButton.addActionListener(this);
    	viewTestButton.setEnabled(false);
    	
    	viewExecTestButton  = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Visualiser") + " " + org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Executions"));
    	viewExecTestButton.addActionListener(this);
    	viewExecTestButton.setEnabled(false);
    	
    	if (viewTest){
    		buttonsPanel = new JPanel(new GridLayout(1,4));
    		buttonsPanel.add(closeButton);
        	buttonsPanel.add(viewTestButton);
        	buttonsPanel.add(viewCampButton);
        	buttonsPanel.add(viewExecTestButton);
        	
    	} else {
    		buttonsPanel = new JPanel(new GridLayout(1,3));
    		buttonsPanel.add(closeButton);
        	buttonsPanel.add(viewCampButton);
        	buttonsPanel.add(viewExecTestButton);
    	}
    	
    	
    	 setLayout(new BorderLayout());
        
         add(execTestTableScrollPane, BorderLayout.CENTER);
         add(buttonsPanel, BorderLayout.SOUTH);
	}
	
	void loadData(DefectWrapper pDefectWrapper, Test pTest){
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		
		for (int i=0; i <  listOfCampaign.size(); i++){
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j=0; j <  listOfExecution.size(); j++){
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec.getExecutionResultListFromModel();
				for (int k=0; k <  listOfExecutionRes.size(); k++){
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes.get(k);
					Test[] tests = pExecutionResult.getTestOrderedFromModel();
					for (int l = 0; l < tests.length ; l++){
						if (canTest(pTest, tests[l])){
							ExecutionTestResult pExecutionTestResult = pExecutionResult.getExecutionTestResultFromModel(tests[l]); 
							if (isLinkToDefect(pExecutionTestResult, pDefectWrapper )){
								
								ArrayList dataList = new ArrayList();
								
								dataList.add(pExec.getEnvironmentFromModel().getNameFromModel());
								dataList.add(pCamp.getNameFromModel());
								dataList.add(pExec.getNameFromModel() + "/" + pExecutionResult.getNameFromModel() );
								dataList.add(tests[l].getTestListFromModel().getFamilyFromModel().getNameFromModel() + "/" + tests[l].getTestListFromModel().getNameFromModel() );
								dataList.add(tests[l].getNameFromModel());
								execTestTableModel.addRow(dataList);
								
								ArrayList dataList2 = new ArrayList();
								dataList2.add(pExec.getEnvironmentFromModel());
								dataList2.add(pCamp);
								dataList2.add(pExec);
								dataList2.add(pExecutionResult);
								dataList2.add(tests[l]);
								ligneTbable.add(dataList2);
								
							}
						}
					}
				}
			}
		}
	}
	
	boolean canTest(Test filtredTest ,Test pTest ){
		if (filtredTest == null){
			return true;
		}
		if (filtredTest.getIdBdd() == pTest.getIdBdd()){
				return true;
		}
		return false;
	}
	
	boolean isLinkToDefect(ExecutionTestResult pExecutionTestResult, DefectWrapper pDefectWrapper){
		Hashtable defectsWrapper = new Hashtable();
		if (pExecutionTestResult != null){
			HashMap attacMap = pExecutionTestResult.getAttachmentMapFromModel();
			Set keysSet = attacMap.keySet();
			for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
				try {
					Object attachName =  iter.next();
					Attachment pAttach = (Attachment) attacMap.get(attachName);
					int bugid = pMantisPlugin.getIDofBug(pAttach);
					if (bugid == pDefectWrapper.getId()){
						return true;
					}
				} catch(Exception ex){
					ex.printStackTrace();
				}
			}
		}
		return false;
	}
	
	/********************* Listener *************************/
	public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) {
        	return;
        }
        
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
            //no rows are selected
        	viewCampButton.setEnabled(false);
        	viewTestButton.setEnabled(false);
        	viewExecTestButton.setEnabled(false);
        } else {
            //int selectedRow = lsm.getMinSelectionIndex();
        	viewCampButton.setEnabled(true);
        	viewTestButton.setEnabled(true);
        	viewExecTestButton.setEnabled(true);
            //selectedRow is selected
        }
	}
	
	
	public void actionPerformed(ActionEvent e){
		if (e.getSource().equals(viewCampButton)){
			viewCampPerformed(e);
		} else if (e.getSource().equals(viewTestButton)){	
			viewTestPerformed(e);
		}else if (e.getSource().equals(viewExecTestButton)){
			viewExecTestPerformed(e);
		} else if (e.getSource().equals(closeButton)){
			pDialog.dispose();
		}
	}
	
	void viewCampPerformed(ActionEvent e){
		int selectedRowIndex = execTestTable.getSelectedRow();
		
		if (selectedRowIndex != -1) {
			int index =  sorter.modelIndex(selectedRowIndex);
			if (ligneTbable.size() >=  index){
				try {
					ArrayList pArrayList = (ArrayList) ligneTbable.elementAt(index);
					Campaign pCamp = (Campaign) pArrayList.get(1);
					pDialog.dispose();
					DataModel.view(pCamp);
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		}
	}
	
	void viewExecTestPerformed(ActionEvent e){
		int selectedRowIndex = execTestTable.getSelectedRow();
		
		if (selectedRowIndex != -1) {
			int index =  sorter.modelIndex(selectedRowIndex);
			if (ligneTbable.size() >=  index){
				try {
					ArrayList pArrayList = (ArrayList) ligneTbable.elementAt(index);
					Test pTest = (Test) pArrayList.get(4);
					ExecutionResult lastExecutionResult = (ExecutionResult) pArrayList.get(3);
					DataModel.setCurrentExecutionTestResult(lastExecutionResult.getExecutionTestResultFromModel(pTest));
					DataModel.setObervedExecutionResult(lastExecutionResult);
					pDialog.dispose();
					if (pTest instanceof ManualTest){
						new ActionDetailsView((ManualTest)pTest, lastExecutionResult.getExecution(), lastExecutionResult, lastExecutionResult.getExecutionTestResultFromModel(pTest));
					} else {
						new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(), DataConstants.EXECUTION_RESULT_TEST, DataModel.getCurrentExecutionTestResult());	
					}
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		}	
	}
	
	
	void viewTestPerformed(ActionEvent e){
		int selectedRowIndex = execTestTable.getSelectedRow();
		
		if (selectedRowIndex != -1) {
			int index =  sorter.modelIndex(selectedRowIndex);
			if (ligneTbable.size() >=  index){
				try {
					ArrayList pArrayList = (ArrayList) ligneTbable.elementAt(index);
					Test pTest = (Test) pArrayList.get(4);
					pDialog.dispose();
					DataModel.view(pTest);
				} catch (Exception ex){
					ex.printStackTrace();
				}
			}
		}
	}
}
