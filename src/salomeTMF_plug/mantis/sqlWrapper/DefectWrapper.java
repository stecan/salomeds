package salomeTMF_plug.mantis.sqlWrapper;


public class DefectWrapper {
	String environement; //OK = version in Mantis
	String user; //OK = username in Mantis
	String plateforme; //OK = platform in Mantis
	String os; //OK = os in Mantis
	int priority; //OK = priority in Mantis
	int severity; //OK = severity in Mantis
	int status; //OK = status in Mantis
	int reproducibility; // Ok = reproducibility in Mantis
	int resolution; // OK = resolution in Mantis
	String recipient; //Assigned to via username in Mantis
	String url =""; //NONE
	String resume; //OK = summary in Mantis
	String description; //Ok = description In Mantis
	int id ;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEnvironement() {
		return environement;
	}
	public void setEnvironement(String environement) {
		this.environement = environement;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getPlateforme() {
		return plateforme;
	}
	public void setPlateforme(String plateforme) {
		this.plateforme = plateforme;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public int getReproducibility() {
		return reproducibility;
	}
	public void setReproducibility(int reproducibility) {
		this.reproducibility = reproducibility;
	}
	public int getResolution() {
		return resolution;
	}
	public void setResolution(int resolution) {
		this.resolution = resolution;
	}
	public String getResume() {
		return resume;
	}
	public void setResume(String resume) {
		this.resume = resume;
	}
	public int getSeverity() {
		return severity;
	}
	public void setSeverity(int severity) {
		this.severity = severity;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
}
