package salomeTMF_plug.mantis.sqlWrapper;

import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.User;

public interface MantisConnector {

	public void initConnector(String driver, String url_db, String user, String pwd) throws Exception ;
	
	/**
	 * 
	 * @param user_login
	 * @return ID of user_login in Mantis or -1 if not exist
	 * @throws Exception
	 */
	public int getUserID(String user_login) throws Exception;
	
	/**
	 * 
	 * @param user_login
	 * @param bugProjectID
	 * @return
	 * @throws Exception
	 */
	public int getUserIDInProject(String user_login, int bugProjectID) throws Exception;
	
	/**
	 * 
	 * @param project_name
	 * @return ID of project_name in Mantis or -1 if not exist
	 * @throws Exception
	 */
	public int getProjectID(String project_name) throws Exception;
	
	/**
	 * 
	 * @param bugUsrID
	 * @param bugProjectID
	 * @return the acces level of the user bugUsrID in the project bugProjectID or -1 if not exist
	 * @throws Exception
	 */
	public int getUserAccesLevel(int bugUsrID, int bugProjectID)  throws Exception ;
	

	/**
	 * Add user in the mantis with access_level = viewer
	 * @param pUser
	 * @param bugProjectID
	 * @return the new user id in Mantis
	 * @throws Exception
	 */
	public int addUser(User pUser) throws Exception;
	
	/**
	 * Add project in mantis
	 * @param project
	 * @return the new id of the project
	 * @throws Exception
	 */
	public int addProject(Project project) throws Exception ;
	
	
	/**
	 * Add user in the mantis project with specified access_level
	 * @param bugUsrID
	 * @param bugProjectID
	 * @param access_level
	 * @throws Exception
	 */
	public void addUserInProject(int bugUsrID, int bugProjectID, int access_level) throws Exception;

	/**
	 * Add default environnement name ___NO_ENV___ in the specified Mantis project
	 * @param bugProjectID
	 * @throws Exception
	 */
	public void addDefaultEnvToProject(int bugProjectID) throws Exception ;
	
	/**
	 * Add Component in Mantis
	 * @param name
	 * @param description
	 * @param bugProjectID
	 * @throws Exception
	 */
	public void addEnvironment(String name, String description, int bugProjectID) throws Exception ;
	
	/**
	 * Add Component in Mantis
	 * @param environment
	 * @param bugProjectID
	 * @throws Exception
	 */
	public void addEnvironment(Environment environment, int bugProjectID) throws Exception;
	
	
	/**
	 * Add relation between defect bugSource <--> bugDest
	 * @param bugSource
	 * @param bugDest
	 * @throws Exception
	 */
	public void addDefectLink(int bugUsrID, int bugSource, int bugDest) throws Exception ;
	
	/**
	 * delete relation between defect bugSource <--> bugDest
	 * @param bugSource
	 * @param bugDest
	 * @throws Exception
	 */
	public void deleteDefectLink(int userID, int bugSource, int bugDest) throws Exception;
	
	/**
	 * Update component name old_component with new_component and description (and reference)
	 * @param old_component
	 * @param new_component
	 * @param description
	 * @param bugProjectID
	 * @throws Exception
	 */
	public void updateEnvironment(String old_component, String new_component, String description, int bugProjectID) throws Exception;
	
	
	public boolean isExistEnv(int bugProjectID, String envName)  throws Exception ;
	
	/**
	 * Delete Component name environment and reference in Mantis DB
	 * @param environment
	 * @param bugProjectID
	 * @throws Exception
	 */
	public void deleteEnvironment(String environment, int bugProjectID) throws Exception ;
	
	
	public int addDefect(int bugUsrID, int bugProjectID, int assigned_to_ID, String  long_desc,  String url_attach,  String short_desc, String bug_OS, 
			int bug_priority, String bug_platform, int bug_reproductibility, int bug_severity, String component) throws Exception ;
	
		
	public void updateDefect(int bugUsrID, int bugProjectID,  int bugID, int id_assigned_to, String long_desc, int id_bug_severity, int id_bug_satus, String short_desc, String bug_OS, 
			int id_bug_priority, String bug_platform, String bug_env, int id_bug_reproducibility, int id_bug_resolution) throws Exception;
	
	public DefectWrapper getDefectInfo(int bugID) throws Exception;
	
	public Hashtable getProjectDefects(int bugProjectID) throws Exception;
	
	/**
	 * Get a hashtable of defectwrapper representing all defects linked with bugID in the project bugProjectID
	 * @param bugProjectID
	 * @param bugID
	 * @return
	 * @throws Exception
	 */
	public Hashtable getDefectLink(int bugProjectID, int bugID) throws Exception ;
	
	public Vector getBugTrackerAllUsers(int bugProjectID ) ;
	
	public void suspend()  throws Exception ;
	
	/**
	 * Delete a bug (and all reference) from Mantis DB
	 * @param bugProjectID
	 * @param bugID
	 * @throws Exception
	 */
	public void deleteDefect(int bugProjectID ,  int bugID) throws Exception ;
	
	/**
	 * Get a vector of HistoryWrapper represented bug History
	 * @param bugProjectID
	 * @param bugID
	 * @return
	 * @throws Exception
	 */
	public Vector getBugHistory(int bugProjectID ,  int bugID) throws Exception ;
	
}
