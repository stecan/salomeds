/*
* SalomeTMF is a Test Management Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
*
* Contact: 	mikael.marche@rd.francetelecom.com
* 			faycal.sougrati@francetelecom.com
*/

package salomeTMF_plug.mantis.sqlWrapper;

import java.util.Random;

import org.objectweb.salome_tmf.api.sql.IDataBase;

public class SQLUtils {
	
	static IDataBase iDB = null;
	static int idTrans = -1;
	static Random idCalcul = new java.util.Random(1000000);
	
	static void initSQLUtils(IDataBase idb) throws Exception{
		if (idb == null){
			throw new Exception("[Bugzilla SQLUtils] Database is null");
		}
		iDB = idb;
	}
	
	static synchronized  int beginTrans() throws Exception {
		if (idTrans == -1) {
			idTrans = idCalcul.nextInt();
			return idTrans;	
		}
		return -1;
	}
	
	static public void commitTrans(int id) throws Exception {
		if (idTrans != -1) {
			if (id == idTrans) {
				idTrans = -1;
				iDB.commit();
			}
		}
	}
	
	static synchronized  public void rollBackTrans(int id) throws Exception{
		if (idTrans != -1) {
			if (id == idTrans) {
				idTrans = -1;
				iDB.rollback();
			}
		}
	}

}
