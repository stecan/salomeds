package salomeTMF_plug.mantis.sqlWrapper;

import java.sql.Date;
import java.text.SimpleDateFormat;

import salomeTMF_plug.mantis.MantisPlugin;
import salomeTMF_plug.mantis.languages.Language;

public class HistoryWrapper {
	int id;
	Date pDate;
	String username;
	String field_name ;
	String old_value ;
	String new_value;
	int code;

	/* For to String */
	String field;
	String change;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getField_name() {
		return field_name;
	}
	public void setField_name(String field_name) {
		this.field_name = field_name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNew_value() {
		return new_value;
	}
	public void setNew_value(String new_value) {
		this.new_value = new_value;
	}
	public String getOld_value() {
		return old_value;
	}
	public void setOld_value(String old_value) {
		this.old_value = old_value;
	}
	public Date getPDate() {
		return pDate;
	}
	public void setPDate(Date date) {
		pDate = date;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}


	public String getDate(){
		 SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		 return formater.format(pDate).toString();
	}

	public String getField(){
		String ret = "";
		if (code == 0){
			ret = field_name;
		} else if (code == 1){
			ret = Language.getInstance().getText("New"); //local
		} else if (code == 18){
			ret = Language.getInstance().getText("Relationship_added"); //local
		}else if (code == 19){
			ret = Language.getInstance().getText("Relationship_deleted"); //local
		} else {
			/* Not yet Implemented */
			ret = field_name;
		}
		return ret;
	}

	public String getChange(MantisPlugin pMantisPlugin){
		String ret = "";
		if (code == 0){
			if (field_name.equals("handler_id")){
				ret = old_value  + " => " + new_value;
			} else if (field_name.equals("status")){
				Integer I_old_value = new Integer(old_value);
				Integer I_new_value = new Integer(new_value);
				ret = pMantisPlugin.statusByID.get(I_old_value) +  " => " + pMantisPlugin.statusByID.get(I_new_value);
			}else if (field_name.equals("priority")){
				Integer I_old_value = new Integer(old_value);
				Integer I_new_value = new Integer(new_value);
				ret = pMantisPlugin.priorityByID.get(I_old_value) +  " => " + pMantisPlugin.priorityByID.get(I_new_value);
			}else if (field_name.equals("severity")){
				Integer I_old_value = new Integer(old_value);
				Integer I_new_value = new Integer(new_value);
				ret = pMantisPlugin.severityByID.get(I_old_value) +  " => " + pMantisPlugin.severityByID.get(I_new_value);
			}else if (field_name.equals("reproducibility")){
				Integer I_old_value = new Integer(old_value);
				Integer I_new_value = new Integer(new_value);
				ret = pMantisPlugin.reproductibilityByID.get(I_old_value) +  " => " + pMantisPlugin.reproductibilityByID.get(I_new_value);
			}else if (field_name.equals("resolution")){
				Integer I_old_value = new Integer(old_value);
				Integer I_new_value = new Integer(new_value);
				ret = pMantisPlugin.resolutionByID.get(I_old_value) +  " => " + pMantisPlugin.resolutionByID.get(I_new_value);
			}else {
				ret = old_value  + " => " + new_value;
			}
		} else if (code == 1){
			ret = ""; //local
		} else if (code == 18){
			ret = Language.getInstance().getText("related_to") + " " +new_value ; //local
		}else if (code == 19){
			ret = Language.getInstance().getText("related_to") + " " +new_value; //local
		} else {
			/* Not yet Implemented */
			ret = old_value  + " => " + new_value;
		}
		return ret;
	}
	/*if (code == 0){
		//Update
		 SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		formater.format // toString
	}*/
}
