package salomeTMF_plug.mantis.sqlWrapper;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.User;

public class SQLMantis implements MantisConnector {

	IDataBase iDB = null;
	Properties sql_prop = null;
	final String MANTIS_STMT_FILE = "/salomeTMF_plug/mantis/resources/sql/Mantis_Stmts.properties";

	boolean connected = false;

	static int RELATION_RELATED_TO = 1;
	static int RELATION_PARENT_OFF = 2;
	static int RELATION_CHILD_OF = 3;
	static int RELATION_DUPLICATE_OF = 4;

	public void initConnector(String driver, String url_db, String user,
			String pwd) throws Exception {
		ISQLObjectFactory iSQL_OF = Api.getISQLObjectFactory();
		if (iDB == null) {
			iDB = iSQL_OF.getInstanceOfDataBase(driver);
			iDB.open(url_db, user, pwd);
			SQLUtils.initSQLUtils(iDB);
		}
		// Mantis properties file for SQL statements
		sql_prop = Util.getPropertiesFile(getClass().getResource(
				MANTIS_STMT_FILE));
		connected = true;
	}

	public int getUserID(String user_login) throws Exception {
		int bugUsrID = -1;
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		ResultSet stmtRes = null;
		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectUsr"));
		prep.setString(1, user_login);
		stmtRes = prep.executeQuery();
		if (stmtRes.next()) {
			Util.log("[MantisPlugin] Mantis connexion with user : "
					+ user_login);
			bugUsrID = stmtRes.getInt("id");
		} else {
			Util
					.log("[MantisPlugin] Current user in Salome doesn't exist in mantis DB : "
							+ user_login);
		}
		return bugUsrID;
	}

	public int getUserIDInProject(String user_login, int bugProjectID)
			throws Exception {
		int bugUsrID = -1;
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		ResultSet stmtRes = null;
		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectUsrProject"));
		prep.setString(1, user_login);
		prep.setInt(2, bugProjectID);
		stmtRes = prep.executeQuery();
		if (stmtRes.next()) {
			Util.log("[MantisPlugin] Mantis connexion with user : "
					+ user_login);
			bugUsrID = stmtRes.getInt("id");
		} else {
			Util
					.log("[MantisPlugin] Current user in Salome doesn't exist in mantis DB : "
							+ user_login);
		}
		return bugUsrID;
	}

	public int getProjectID(String project_name) throws Exception {
		int bugProjectID = -1;
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectProject"));
		prep.setString(1, project_name);
		ResultSet stmtRes2 = prep.executeQuery();
		if (stmtRes2.next()) {
			bugProjectID = stmtRes2.getInt("id");
		}
		return bugProjectID;
	}

	public int getUserAccesLevel(int bugUsrID, int bugProjectID)
			throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		int access_level = -1;
		if ((bugUsrID != -1) && (bugProjectID != -1)) {
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectProjectUser"));
			prep.setInt(1, bugProjectID);
			prep.setInt(2, bugUsrID);
			ResultSet stmtRes3 = prep.executeQuery();
			if (stmtRes3.next()) {
				access_level = stmtRes3.getInt("access_level");
				// isCurrentUserExistsInProject = true;
			}
		}
		return access_level;
	}

	public int addUser(User pUser) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		// Adding the user to Mantis
		int nbTrans = -1;
		PreparedStatement prep = null;
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new java.util.Date()); // gives the current
														// date in
														// MySQL-acceptable
														// format.
		String cookieName = pUser.getLoginFromModel() + "_"
				+ new java.util.Date().getTime();

		int bugUsrID = -1;
		// Add user to mantis DB with access_level = viewer
		try {
			nbTrans = SQLUtils.beginTrans();
			prep = iDB.prepareStatement(sql_prop.getProperty("AddNewBugUser"));
			prep.setString(1, pUser.getLoginFromModel());
			prep.setString(2, pUser.getEmailFromModel());
			prep.setString(3, pUser.getPasswordFromDB());
			prep.setLong(4, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setLong(5, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setInt(6, 1);
			prep.setInt(7, 10); // 0 : NONE 10 ://viewer
			prep.setInt(8, 0);
			prep.setString(9, pUser.getFirstNameFromModel() + " "
					+ pUser.getLastNameFromModel());
			prep.setString(10, cookieName);
			prep.executeUpdate();

			ResultSet stmtRes = null;
			prep = iDB.prepareStatement(sql_prop.getProperty("SelectUsr"));
			prep.setString(1, pUser.getLoginFromModel());
			stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				bugUsrID = stmtRes.getInt("id");
			}
			if (bugUsrID == -1) {
				throw new Exception("User no found");
			}
			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}

		/*
		 * User ID try { ResultSet stmtRes = null; prep =
		 * iDB.prepareStatement(sql_prop.getProperty("SelectUsr"));
		 * prep.setString(1, pUser.getLoginFromModel()); stmtRes =
		 * prep.executeQuery(); if (stmtRes.next()) { bugUsrID =
		 * stmtRes.getInt("id"); } } catch (Exception e) { e.printStackTrace();
		 * throw e; }
		 */

		// Default user preferences
		try {
			nbTrans = SQLUtils.beginTrans();
			prep = iDB.prepareStatement(sql_prop
					.getProperty("AddDefaultUserPref"));
			prep.setInt(1, bugUsrID);
			prep.executeUpdate();
			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
		return bugUsrID;
	}

	public int addProject(Project project) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		int bugProjectID = -1;
		// Adding Salome TMF project to Mantis DB
		int nbTrans = -1;
		PreparedStatement prep;
		try {
			nbTrans = SQLUtils.beginTrans();
			prep = iDB.prepareStatement(sql_prop.getProperty("AddNewProject"));
			prep.setString(1, project.getNameFromModel());
			prep.setString(2, "");
			prep.setString(3, project.getDescriptionFromModel());
			prep.executeUpdate();

			// Get project ID
			prep = iDB.prepareStatement(sql_prop.getProperty("SelectProject"));
			prep.setString(1, project.getNameFromModel());
			ResultSet stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				bugProjectID = stmtRes.getInt("id");
			}

			if (bugProjectID == -1) {
				throw new Exception("Project no found");
			}

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}

		return bugProjectID;

	}

	public void addUserInProject(int bugUsrID, int bugProjectID,
			int access_level) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (access_level <= -1) {
			access_level = 70;// manager
		}
		/*
		 * if(currentProject.getAdministratorWrapperFromDB().getLogin().equals(
		 * currentUser.getLoginFromModel())) { access_level = 90; // admin }
		 */
		int nbTrans = -1;
		try {
			nbTrans = SQLUtils.beginTrans();
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("AddUserToProject"));
			prep.setInt(1, bugProjectID);
			prep.setInt(2, bugUsrID);
			prep.setInt(3, access_level);
			prep.executeUpdate();
			SQLUtils.commitTrans(nbTrans);
			Util.log("[MantisPlugin] Add User id=" + bugUsrID
					+ " to project id=" + bugProjectID + " with access level="
					+ access_level);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void addDefaultEnvToProject(int bugProjectID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		// Adding default environment
		int nbTrans = -1;
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new java.util.Date()); // gives the current
														// date in
														// MySQL-acceptable
														// format.
		try {
			nbTrans = SQLUtils.beginTrans();
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("AddNewEnvironment"));
			prep.setInt(1, bugProjectID);
			prep.setString(2, "___NO_ENV___");
//			prep.setString(3, date);
			prep.setLong(3, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setString(4, "[SALOME_DEFAULT_ENVIRONMENT]");
			prep.executeUpdate();
			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void addEnvironment(String name, String description, int bugProjectID)
			throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}

		int nbTrans = -1;
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new java.util.Date()); // gives you the current
														// date in
														// MySQL-acceptable
														// format.
		try {
			nbTrans = SQLUtils.beginTrans();
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("AddNewEnvironment"));
			prep.setInt(1, bugProjectID);
			prep.setString(2, name.trim());
			// prep.setString(3,date);
			prep.setLong(3, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setString(4, description);
			prep.executeUpdate();
			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void addEnvironment(Environment environment, int bugProjectID)
			throws Exception {
		addEnvironment(environment.getNameFromModel(), environment
				.getDescriptionFromModel(), bugProjectID);
	}

	public void addDefectLink(int userID, int bugSource, int bugDest)
			throws Exception {

		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (userID <= 0) {
			throw new Exception("User Id is not valid");
		}

		if (bugSource <= 0) {
			throw new Exception("Source Id is not valid");
		}

		if (bugDest <= 0) {
			throw new Exception("Destination Id is not valid");
		}
		int nbTrans = -1;
		try {
			nbTrans = SQLUtils.beginTrans();
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("AddLinkedDefect"));
			prep.setInt(1, bugSource);
			prep.setInt(2, bugDest);
			prep.setInt(3, RELATION_RELATED_TO);
			prep.executeUpdate();

			addBugHistory(userID, bugSource, 18, "", "1", "" + bugDest);
			addBugHistory(userID, bugDest, 18, "", "1", "" + bugSource);

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void updateEnvironment(String old_component, String new_component,
			String description, int bugProjectID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		int nbTrans = -1;
		PreparedStatement prep;
		try {
			nbTrans = SQLUtils.beginTrans();

			prep = iDB.prepareStatement(sql_prop
					.getProperty("UpdateEnvironment"));
			prep.setString(1, new_component);
			prep.setString(2, description);
			prep.setInt(3, bugProjectID);
			prep.setString(4, old_component);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop
					.getProperty("UpdateEnvironmentForBugs"));
			prep.setString(1, new_component.trim());
			prep.setInt(2, bugProjectID);
			prep.setString(3, old_component.trim());
			prep.executeUpdate();

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void deleteEnvironment(String environment, int bugProjectID)
			throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		int nbTrans = -1;
		PreparedStatement prep;

		try {
			nbTrans = SQLUtils.beginTrans();
			prep = iDB.prepareStatement(sql_prop
					.getProperty("UpdateEnvironmentForBugs"));
			prep.setString(1, "___NO_ENV___");
			prep.setInt(2, bugProjectID);
			prep.setString(3, environment);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop
					.getProperty("DeleteEnvironment"));
			prep.setInt(1, bugProjectID);
			prep.setString(2, environment);
			prep.executeUpdate();

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void deleteDefectLink(int userID, int bugSource, int bugDest)
			throws Exception {

		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}

		if (userID <= 0) {
			throw new Exception("User Id is not valid");
		}
		if (bugSource <= 0) {
			throw new Exception("Source Id is not valid");
		}

		if (bugDest <= 0) {
			throw new Exception("Destination Id is not valid");
		}
		int nbTrans = -1;
		try {
			nbTrans = SQLUtils.beginTrans();
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("DeleteLinkedDefect"));
			prep.setInt(1, bugSource);
			prep.setInt(2, bugDest);
			prep.setInt(3, RELATION_RELATED_TO);
			int val = prep.executeUpdate();
			if (val > 0) {
				addBugHistory(userID, bugSource, 19, "", "1", "" + bugDest);
				addBugHistory(userID, bugDest, 19, "", "1", "" + bugSource);
			}

			prep = iDB.prepareStatement(sql_prop
					.getProperty("DeleteLinkedDefect"));
			prep.setInt(2, bugSource);
			prep.setInt(1, bugDest);
			prep.setInt(3, RELATION_RELATED_TO);
			val = prep.executeUpdate();
			if (val > 0) {
				addBugHistory(userID, bugSource, 19, "", "1", "" + bugDest);
				addBugHistory(userID, bugDest, 19, "", "1", "" + bugSource);
			}

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public boolean isExistEnv(int bugProjectID, String envName)
			throws Exception {
		if (!(bugProjectID > 0)) {
			return false;
		}
		boolean ret = true;
		try {
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectEnvironment"));
			prep.setInt(1, bugProjectID);
			prep.setString(2, envName.trim());
			ResultSet stmtRes = prep.executeQuery();
			if (!stmtRes.next()) {
				ret = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

	public int addDefect(int bugUsrID, int bugProjectID, int assigned_to_ID,
			String long_desc, String url_attach, String short_desc,
			String bug_OS, int bug_priority, String bug_platform,
			int bug_reproductibility, int bug_severity, String component)
			throws Exception {

		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		if (bugUsrID <= 0) {
			throw new Exception("User Id is not valid");
		}
		if (assigned_to_ID <= 0) {
			throw new Exception("assigned user Id is not valid");
		}
		int bug_id = -1;
		PreparedStatement prep;
		int nbTrans = -1;
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(new java.util.Date()); // gives you the current
														// date in
														// MySQL-acceptable
														// format.

		try {
			nbTrans = SQLUtils.beginTrans();

			/* Adding bug description */
			prep = iDB.prepareStatement(sql_prop.getProperty("AddBugDesc"));
			prep.setString(1, long_desc);
			prep.setString(2, bugUsrID + "_" + date);
			prep.executeUpdate();

			/* Selection of bug decription ID */
			int bug_desc_id = -1;
			prep = iDB.prepareStatement(sql_prop.getProperty("SelectBugDesc"));
			prep.setString(1, bugUsrID + "_" + date);
			ResultSet stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				bug_desc_id = stmtRes.getInt("id");
			}

			/* Adding the bug */
			if ((url_attach != null) && (!url_attach.equals(""))) {
				short_desc += " (URL = " + url_attach + ")";
			}
			if ((bug_OS == null) || (bug_OS.equals("")))
				bug_OS = " ";

			if ((short_desc == null) || (short_desc.equals("")))
				short_desc = " ";

			prep = iDB.prepareStatement(sql_prop.getProperty("AddBug"));
			prep.setInt(1, bugProjectID);
			prep.setInt(2, bugUsrID);
			prep.setInt(3, assigned_to_ID);
			prep.setInt(4, bug_priority);
			prep.setInt(5, bug_severity);
			prep.setInt(6, bug_reproductibility);
			prep.setLong(7, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setLong(8, Calendar.getInstance().getTime().getTime() / 1000);
			prep.setInt(9, bug_desc_id);
			prep.setString(10, bug_OS);
			prep.setString(11, bug_platform);
			prep.setString(12, short_desc);
			prep.setString(13, component);
			prep.executeUpdate();

			/* Get BugID */
			prep = iDB.prepareStatement(sql_prop.getProperty("SelectBugID"));
			prep.setString(1, bugUsrID + "_" + date);

			stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				bug_id = stmtRes.getInt("id");
				addBugHistory(bugUsrID, bug_id, 1, "", "", "");
				Util.log("[MantisPlugin]Add bug id = " + bug_id);
			}

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}

		return bug_id;
	}

	public void updateDefect(int bugUsrID, int bugProjectID, int bugID,
			int id_assigned_to, String long_desc, int id_bug_severity,
			int id_bug_satus, String short_desc, String bug_OS,
			int id_bug_priority, String bug_platform, String bug_env,
			int id_bug_reproducibility, int id_bug_resolution) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		if (bugUsrID <= 0) {
			throw new Exception("User Id is not valid");
		}
		if (id_assigned_to <= 0) {
			throw new Exception("assigned user Id is not valid");
		}

		int nbTrans = -1;
		int bug_desc_id = -1;
		try {
			nbTrans = SQLUtils.beginTrans();
			/* Selection of bug decription ID */
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectBugDesc2"));
			prep.setInt(1, bugID);
			ResultSet stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				bug_desc_id = stmtRes.getInt("id");
			}

			/* Get Old Information */
			String oldplateforme = bug_platform; // OK
			String oldos = bug_OS; // OK
			int oldpriority = id_bug_priority; // OK
			int oldseverity = id_bug_severity; // OK
			int oldstatus = id_bug_satus; // OK
			int oldreproducibility = id_bug_reproducibility;
			int oldresolution = id_bug_resolution;
			int oldrecipient = id_assigned_to;
			String oldresume = short_desc; // OK
			String olddescription = long_desc; // OK
			prep = iDB.prepareStatement(sql_prop.getProperty("SelectBugInfo"));
			prep.setInt(1, bugID);
			stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				oldplateforme = stmtRes.getString("platform");
				oldos = stmtRes.getString("os");
				oldpriority = stmtRes.getInt("priority");
				oldseverity = stmtRes.getInt("severity");
				oldstatus = stmtRes.getInt("status");
				oldreproducibility = stmtRes.getInt("reproducibility");
				oldresolution = stmtRes.getInt("resolution");
				oldresume = stmtRes.getString("summary");
				olddescription = stmtRes.getString("description");
			}
			prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectBugHandler"));
			prep.setInt(1, bugID);
			stmtRes = prep.executeQuery();
			if (stmtRes.next()) {
				oldrecipient = stmtRes.getInt("id");
			}

			/* Update bug description */
			if (bug_desc_id > 0) {
				prep = iDB.prepareStatement(sql_prop
						.getProperty("UpdateBugDesc"));
				prep.setString(1, long_desc);
				prep.setInt(2, bug_desc_id);
				prep.executeUpdate();
			}

			/* Update bug information */
			prep = iDB.prepareStatement(sql_prop.getProperty("UpdateBugInfo"));
			prep.setInt(1, id_assigned_to); // OK
			prep.setInt(2, id_bug_priority); // OK
			prep.setInt(3, id_bug_severity); // OK
			prep.setInt(4, id_bug_satus); // OK
			prep.setInt(5, id_bug_reproducibility);
			prep.setInt(6, id_bug_resolution);
			prep.setString(7, bug_OS); // OK
			prep.setString(8, bug_platform); // OK
			prep.setString(9, bug_env); // OK
			prep.setString(10, short_desc); // OK
			prep.setInt(11, bugID); // OK
			prep.executeUpdate();

			/* Add bug history */
			if (oldrecipient != id_assigned_to) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "handler_id", ""
							+ oldrecipient, "" + id_assigned_to);
				} catch (Exception e) {
				}
			}
			if (oldpriority != id_bug_priority) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "priority", ""
							+ oldpriority, "" + id_bug_priority);
				} catch (Exception e) {
				}
			}
			if (oldseverity != id_bug_severity) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "severity", ""
							+ oldseverity, "" + id_bug_severity);
				} catch (Exception e) {
				}
			}
			if (oldstatus != id_bug_satus) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "status", "" + oldstatus,
							"" + id_bug_satus);
				} catch (Exception e) {
				}
			}
			if (oldreproducibility != id_bug_reproducibility) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "reproducibility", ""
							+ oldreproducibility, "" + id_bug_reproducibility);
				} catch (Exception e) {
				}
			}
			if (oldresolution != id_bug_resolution) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "resolution", ""
							+ oldresolution, "" + id_bug_resolution);
				} catch (Exception e) {
				}
			}
			if (!olddescription.equals(long_desc)) {
				try {
					addBugHistory(bugUsrID, bugID, 6, "", "", "");
				} catch (Exception e) {
				}
			}
			if (!oldresume.equals(short_desc)) {
				try {
					addBugHistory(bugUsrID, bugID, 0, "summary", oldresume,
							short_desc);
				} catch (Exception e) {
				}
			}

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception E) {
			E.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw E;
		}

	}

	public DefectWrapper getDefectInfo(int bugID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugID <= 0) {
			throw new Exception("Bug Id is not valid");
		}
		DefectWrapper pDefectWrapper = new DefectWrapper();

		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectBugInfo"));
		prep.setInt(1, bugID);
		ResultSet stmtRes = prep.executeQuery();
		if (stmtRes.next()) {
			pDefectWrapper.setId(stmtRes.getInt("mantis_bug_table.id"));
			pDefectWrapper.setEnvironement(stmtRes.getString("version"));
			pDefectWrapper.setUser(stmtRes.getString("username"));
			pDefectWrapper.setPlateforme(stmtRes.getString("platform"));
			pDefectWrapper.setOs(stmtRes.getString("os"));
			// priority = getBugPriority(new
			// Integer(stmtRes.getInt("priority")));
			pDefectWrapper.setPriority(stmtRes.getInt("priority"));
			// severity = getBugSeverity(new
			// Integer(stmtRes.getInt("severity")));
			pDefectWrapper.setSeverity(stmtRes.getInt("severity"));
			// status = getBugStatus(new Integer(stmtRes.getInt("status")));
			pDefectWrapper.setStatus(stmtRes.getInt("status"));
			// reproducibility = getBugReproducibility(new
			// Integer(stmtRes.getInt("reproducibility")));
			pDefectWrapper
					.setReproducibility(stmtRes.getInt("reproducibility"));
			// resolution = getBugResolution(new
			// Integer(stmtRes.getInt("resolution")));
			pDefectWrapper.setResolution(stmtRes.getInt("resolution"));
			pDefectWrapper.setResume(stmtRes.getString("summary"));
			pDefectWrapper.setDescription(stmtRes.getString("description"));
		} else {
			throw new Exception("No bug");
		}
		prep = iDB.prepareStatement(sql_prop.getProperty("SelectBugHandler"));
		prep.setInt(1, bugID);
		stmtRes = prep.executeQuery();
		if (stmtRes.next()) {
			pDefectWrapper.setRecipient(stmtRes.getString("username"));
		} else {
			throw new Exception("No bug");
		}
		return pDefectWrapper;
	}

	public Hashtable getProjectDefects(int bugProjectID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		Hashtable listDefectWrapper = new Hashtable();
		Hashtable menbers = getBugTrackerUsers(bugProjectID);

		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectBugOfProject"));
		prep.setInt(1, bugProjectID);
		ResultSet stmtRes = prep.executeQuery();
		while (stmtRes.next()) {
			DefectWrapper pDefectWrapper = new DefectWrapper();
			pDefectWrapper.setEnvironement(stmtRes.getString("version")); // OK
			pDefectWrapper.setId(stmtRes.getInt("id"));
			int userID = stmtRes.getInt("reporter_id"); // OK
			pDefectWrapper.setUser((String) menbers.get(new Integer(userID)));
			pDefectWrapper.setPlateforme(stmtRes.getString("platform")); // OK
			pDefectWrapper.setOs(stmtRes.getString("os")); // OK
			pDefectWrapper.setPriority(stmtRes.getInt("priority")); // OK
			pDefectWrapper.setSeverity(stmtRes.getInt("severity"));// OK
			pDefectWrapper.setStatus(stmtRes.getInt("status")); // OK
			pDefectWrapper
					.setReproducibility(stmtRes.getInt("reproducibility")); // OK
			pDefectWrapper.setResolution(stmtRes.getInt("resolution")); // ok
			pDefectWrapper.setResume(stmtRes.getString("summary")); // OK
			pDefectWrapper.setDescription(stmtRes.getString("description")); // OK
			int handlerID = stmtRes.getInt("handler_id"); // OK
			pDefectWrapper.setRecipient((String) menbers.get(new Integer(
					handlerID)));
			listDefectWrapper.put(new Integer(pDefectWrapper.getId()),
					pDefectWrapper);
		}
		return listDefectWrapper;
	}

	public Hashtable getDefectLink(int bugProjectID, int bugID)
			throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		if (bugID <= 0) {
			throw new Exception("defect Id is not valid");
		}
		Hashtable listDefectWrapper = new Hashtable();
		Hashtable menbers = getBugTrackerUsers(bugProjectID);

		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectLinkedDefect"));
		prep.setInt(1, bugID);
		ResultSet stmtRes = prep.executeQuery();

		while (stmtRes.next()) {
			DefectWrapper pDefectWrapper = new DefectWrapper();
			pDefectWrapper.setEnvironement(stmtRes.getString("version")); // OK
			pDefectWrapper.setId(stmtRes.getInt("mantis_bug_table.id"));
			int userID = stmtRes.getInt("reporter_id"); // OK
			pDefectWrapper.setUser((String) menbers.get(new Integer(userID)));
			pDefectWrapper.setPlateforme(stmtRes.getString("platform")); // OK
			pDefectWrapper.setOs(stmtRes.getString("os")); // OK
			pDefectWrapper.setPriority(stmtRes.getInt("priority")); // OK
			pDefectWrapper.setSeverity(stmtRes.getInt("severity"));// OK
			pDefectWrapper.setStatus(stmtRes.getInt("status")); // OK
			pDefectWrapper
					.setReproducibility(stmtRes.getInt("reproducibility")); // OK
			pDefectWrapper.setResolution(stmtRes.getInt("resolution")); // ok
			pDefectWrapper.setResume(stmtRes.getString("summary")); // OK
			pDefectWrapper.setDescription(stmtRes.getString("description")); // OK
			int handlerID = stmtRes.getInt("handler_id"); // OK
			pDefectWrapper.setRecipient((String) menbers.get(new Integer(
					handlerID)));
			listDefectWrapper.put(new Integer(pDefectWrapper.getId()),
					pDefectWrapper);
		}

		prep = iDB
				.prepareStatement(sql_prop.getProperty("SelectLinkedDefect2"));
		prep.setInt(1, bugID);
		stmtRes = prep.executeQuery();
		while (stmtRes.next()) {
			DefectWrapper pDefectWrapper = new DefectWrapper();
			pDefectWrapper.setEnvironement(stmtRes.getString("version")); // OK
			pDefectWrapper.setId(stmtRes.getInt("mantis_bug_table.id"));
			int userID = stmtRes.getInt("reporter_id"); // OK
			pDefectWrapper.setUser((String) menbers.get(new Integer(userID)));
			pDefectWrapper.setPlateforme(stmtRes.getString("platform")); // OK
			pDefectWrapper.setOs(stmtRes.getString("os")); // OK
			pDefectWrapper.setPriority(stmtRes.getInt("priority")); // OK
			pDefectWrapper.setSeverity(stmtRes.getInt("severity"));// OK
			pDefectWrapper.setStatus(stmtRes.getInt("status")); // OK
			pDefectWrapper
					.setReproducibility(stmtRes.getInt("reproducibility")); // OK
			pDefectWrapper.setResolution(stmtRes.getInt("resolution")); // ok
			pDefectWrapper.setResume(stmtRes.getString("summary")); // OK
			pDefectWrapper.setDescription(stmtRes.getString("description")); // OK
			int handlerID = stmtRes.getInt("handler_id"); // OK
			pDefectWrapper.setRecipient((String) menbers.get(new Integer(
					handlerID)));
			listDefectWrapper.put(new Integer(pDefectWrapper.getId()),
					pDefectWrapper);
		}

		return listDefectWrapper;
	}

	public Vector getBugTrackerAllUsers(int bugProjectID) {
		if (!(bugProjectID > 0)) {
			return new Vector();
		}
		Vector res = null;
		try {
			ResultSet stmtRes;
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectAllLoginFromProject"));
			prep.setInt(1, bugProjectID);
			stmtRes = prep.executeQuery();
			res = new Vector();
			while (stmtRes.next()) {
				res.add(stmtRes.getString("username"));
			}
		} catch (Exception E) {
			E.printStackTrace();
		}
		return res;
	}

	public Hashtable getBugTrackerUsers(int bugProjectID) {
		if (!(bugProjectID > 0)) {
			return new Hashtable();
		}
		Hashtable res = null;
		try {
			ResultSet stmtRes;
			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("SelectAllLoginFromProject"));
			prep.setInt(1, bugProjectID);
			stmtRes = prep.executeQuery();
			res = new Hashtable();
			while (stmtRes.next()) {
				res.put(new Integer(stmtRes.getInt("id")), stmtRes
						.getString("username"));
			}
		} catch (Exception E) {
			res = null;
			E.printStackTrace();
		}
		return res;
	}

	public void suspend() throws Exception {
		iDB.close();
		iDB = null;
		connected = false;
	}

	/*************************************************************************************************************/

	void addBugHistory(int bugUsrID, int bug_id, int type, String field,
			String oldVal, String newVal) throws Exception {
		PreparedStatement prep;
		int nbTrans = -1;
		try {
			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String date = sdf.format(new java.util.Date());
			nbTrans = SQLUtils.beginTrans();
			prep = iDB.prepareStatement(sql_prop.getProperty("AddBugHistory"));
			prep.setInt(1, bugUsrID);
			prep.setInt(2, bug_id);
			prep.setLong(3, Calendar.getInstance().getTime().getTime() / 1000);
//			prep.setString(3, date);
			prep.setString(4, field);
			prep.setString(5, oldVal);
			prep.setString(6, newVal);
			prep.setInt(7, type);
			prep.executeUpdate();
			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public void deleteDefect(int bugProjectID, int bugID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		if (bugID <= 0) {
			throw new Exception("defect Id is not valid");
		}
		int nbTrans = -1;
		try {
			nbTrans = SQLUtils.beginTrans();

			PreparedStatement prep = iDB.prepareStatement(sql_prop
					.getProperty("DeleteLinkedDefectSource"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop
					.getProperty("DeleteLinkedDefectDest"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteBugNote"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteMonitor"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteBugFile"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteHistory"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteBugText"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			prep = iDB.prepareStatement(sql_prop.getProperty("DeleteDefect"));
			prep.setInt(1, bugID);
			prep.executeUpdate();

			SQLUtils.commitTrans(nbTrans);
		} catch (Exception e) {
			e.printStackTrace();
			SQLUtils.rollBackTrans(nbTrans);
			throw e;
		}
	}

	public Vector getBugHistory(int bugProjectID, int bugID) throws Exception {
		if (!connected) {
			throw new Exception(
					"Connector JDBC is not connected to the mantis database");
		}
		if (bugProjectID <= 0) {
			throw new Exception("Project Id is not valid");
		}
		if (bugID <= 0) {
			throw new Exception("defect Id is not valid");
		}
		Vector listOfHistory = new Vector();
		Hashtable menbers = getBugTrackerUsers(bugProjectID);

		PreparedStatement prep = iDB.prepareStatement(sql_prop
				.getProperty("SelectDefectHistory"));
		prep.setInt(1, bugID);
		ResultSet stmtRes = prep.executeQuery();
		while (stmtRes.next()) {
			int userID = stmtRes.getInt("user_id");
			String user = (String) menbers.get(new Integer(userID));
			int code = stmtRes.getInt("type");
			Date pDate = stmtRes.getDate("date_modified");
			String field_name = stmtRes.getString("field_name");
			String old_value = stmtRes.getString("old_value");
			String new_value = stmtRes.getString("new_value");
			HistoryWrapper pHistoryWrapper = new HistoryWrapper();

			pHistoryWrapper.setId(stmtRes.getInt("id"));
			pHistoryWrapper.setCode(code);
			pHistoryWrapper.setUsername(user);
			pHistoryWrapper.setField_name(field_name.trim());
			pHistoryWrapper.setOld_value(old_value.trim());
			pHistoryWrapper.setNew_value(new_value.trim());
			pHistoryWrapper.setPDate(pDate);
			listOfHistory.add(pHistoryWrapper);
		}

		return listOfHistory;
	}
}
