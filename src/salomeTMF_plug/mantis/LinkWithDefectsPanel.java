package salomeTMF_plug.mantis;

import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class LinkWithDefectsPanel extends JPanel implements ActionListener{
	
	JButton linkDefectButton;
	JButton editDefectButton ;
	JButton deleteLinkButton ;
	
	DefectPanel pDefectPanel;
	MantisPlugin pMantisPlugin;
	
	Dialog jDialog;
	DefectWrapper sourceDefectWrapper;
	
	LinkWithDefectsPanel(Dialog jDialog, DefectWrapper pDefectWrapper, MantisPlugin pMantisPlugin){
		super(new GridLayout(1, 3));
		this.pMantisPlugin = pMantisPlugin;
		sourceDefectWrapper = pDefectWrapper;
		this.jDialog = jDialog;
		initActionPanel();
	}
	
	void setDefectPanel(DefectPanel pDefectPanel){
		this.pDefectPanel = pDefectPanel;
	}
	
	void initActionPanel(){
		linkDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Lier_a_annomalie"));
		linkDefectButton.addActionListener(this);
		editDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Editer"));
		editDefectButton.addActionListener(this);
		deleteLinkButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Supprimer_lien"));
		deleteLinkButton.addActionListener(this);
		
		add(editDefectButton);
		add(deleteLinkButton);
		add(linkDefectButton);
	}
	
	public void actionPerformed(ActionEvent e){
		 if (e.getSource().equals(editDefectButton)){
			editDefectPerformed();
		} else if (e.getSource().equals(deleteLinkButton)){
			deleteDefectPerformed();
		}else if (e.getSource().equals(linkDefectButton)){
			linkPerformed();
		}
	}
	
	
	void linkPerformed(){
		pDefectPanel.defectsWrapper.put(new Integer(sourceDefectWrapper.getId()), sourceDefectWrapper);
		LinkDefectDialog pLinkDefectDialog = new LinkDefectDialog(jDialog, pMantisPlugin, pDefectPanel.defectsWrapper, null );
		pDefectPanel.defectsWrapper.remove(new Integer(sourceDefectWrapper.getId()));
		DefectWrapper pDefectWrapper =  pLinkDefectDialog.getSelectedDefectWrapper();
		if (pDefectWrapper != null){
			try {
				pMantisPlugin.addDefectLink(sourceDefectWrapper, pDefectWrapper);
				pDefectPanel.defectsWrapper.put(new Integer(pDefectWrapper.getId()), pDefectWrapper);
				pDefectPanel.reloadData();
			} catch (Exception e){
				e.printStackTrace();
			}
			}
			
		}
	
	
	void editDefectPerformed(){
		//PAS DE LINK
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			new DefectView(jDialog, pDefectWrapper, pMantisPlugin, false);
			pDefectPanel.reloadData();
		}
		
	}
	
	void deleteDefectPerformed(){
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			try {
				if (pMantisPlugin.deleteConfirme(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Le_lien"))){
					pMantisPlugin.deleteDefectLink(sourceDefectWrapper, pDefectWrapper);
					pDefectPanel.defectsWrapper.remove(new Integer(pDefectWrapper.getId()));
					pDefectPanel.reloadData();
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
}
