/*
* SalomeTMF is a Test Management Framework
* Copyright (C) 2005 France Telecom R&D
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
* @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
*
* Contact: 	mikael.marche@rd.francetelecom.com
* 			faycal.sougrati@francetelecom.com
*/

package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class DefectPanel extends JPanel implements  ActionListener{


	/** Fitre **/
	JPanel panelFiltre;
	JLabel labelRapporter; //Rapporteur
	JComboBox comboRapporter; //On Maxi

	JLabel labelOwner; //Assigne a
	JComboBox comboOwner; //On Maxi

	JLabel labelProduit; //Produit (environnement)
	JComboBox comboProduit; //On Maxi-Mini

	JLabel labelSeverite; //Severite
	//Hashtable severityByID;
	JComboBox comboSeverite; //On Maxi-Mini

	JLabel labelPriorite; //Priorite
	//Hashtable prioriteByID;
	JComboBox comboPriorite; //On Maxi-Mini

	JLabel labelDiagnostic; //Diagnostic
	//Hashtable diagnosticByID;
	JComboBox comboDiagnostic; //On Maxi

	JLabel labelState; //Etat
	//Hashtable stateByID;
	Vector choiceState;
	JComboBox comboState; //On Maxi

	JLabel labelHideSate; // Cacher le statut:
	JComboBox comboHideSate;

	JLabel labelResume;
	JTextField filtreResume;
	JCheckBox checkStartWith;
	JCheckBox checkContyent;
	//JButton applyFiltreButton;

	//Date : TODO

	/** Table **/
	JPanel defectsPanel;
    MyTableModel defectsTableModel;
    TableSorter  sorter ;
    JTable defectsTable;
	Vector ligneTbable;
	Map colorState;


	/** Action **/
	JPanel actionPanel;
	JPanel pPanelActionDefects;
	JButton newDefectButton ;
	JButton editDefectButton ;
	JButton viewLinkButton ;
	JButton addLinkButton ;
	JButton deleteButton;

	boolean isMiniSize = false;
	MantisPlugin pMantisPlugin;

	Hashtable defectsWrapper;
	boolean canReload = false;
	boolean isIndependant = false;
	public DefectPanel(boolean miniSize, MantisPlugin pMantisPlugin, JPanel _pPanelActionDefects, String envRestrictionName) {
		super();
		setLayout(new BorderLayout());
		isMiniSize = miniSize;
		this.pMantisPlugin = pMantisPlugin;
		if (_pPanelActionDefects == null){
			pPanelActionDefects = this;
			initActionPanel();
		} else {
			try {
				pPanelActionDefects = _pPanelActionDefects;
				actionPanel =  _pPanelActionDefects;
			} catch (Exception e){
				e.printStackTrace();
				pPanelActionDefects = this;
				initActionPanel();
			}
		}
		initFiltrePanel();
		if (envRestrictionName != null) {
			applyEnvRestriction(envRestrictionName);
		}
		initTablePanel();

		add(panelFiltre, BorderLayout.NORTH);
		add(defectsPanel, BorderLayout.CENTER);
		add(actionPanel, BorderLayout.SOUTH);
		canReload = true;

	}

	/**
	 * @covers SFD_ForgeORTF_ANO_AFF_000030 - �2.4.1
	 * @covers EDF-2.4.1
	 */
	void initFiltrePanel(){
		//PASSER pas last ID et test null
		panelFiltre = new JPanel();
		/* On Maxi-Mini */
		labelProduit = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Environnement") + " :"); //Produit (environnement)
		ArrayList listOfEnv = DataModel.getCurrentProject().getEnvironmentListFromModel();
		Vector choiceEnv = new Vector();
		choiceEnv.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
		choiceEnv.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Aucun"));
		int size = listOfEnv.size();
		for (int i = 0 ; i < size ; i++){
			choiceEnv.add(((Environment)listOfEnv.get(i)).getNameFromModel());
		}
		comboProduit = new JComboBox(choiceEnv);
		comboProduit.addActionListener(this);
		comboProduit.setEditable(false);

		/* comboSeverite; On Maxi-Mini */
		labelSeverite = new JLabel(Language.getInstance().getText("Severite") + " :");
		Vector choiceSeverite = new Vector();
		choiceSeverite.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
		size = pMantisPlugin.severity_values.size();
		for (int i = 0; i < size ; i++){
			choiceSeverite.add( pMantisPlugin.severity_values.elementAt(i));
		}
		comboSeverite = new JComboBox(choiceSeverite);
		comboSeverite.addActionListener(this);
		comboSeverite.setEditable(false);


		/*comboPriorite; On Maxi-Mini*/
		labelPriorite = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Priority") + " :"); //Priorite
		Vector choicePriorite = new Vector();
		choicePriorite.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
		size = pMantisPlugin.priority_values.size();
		for (int i = 0; i < size ; i++){
			choicePriorite.add( pMantisPlugin.priority_values.elementAt(i));
		}
		comboPriorite = new JComboBox(choicePriorite);
		comboPriorite.addActionListener(this);
		comboPriorite.setEditable(false);

		choiceState = new Vector();
		choiceState.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
		size = pMantisPlugin.status_values.size();
		//20100108 - D�but modification Forge ORTF V1.0.0
//		int incColor = 0;
//		int incColor2 = 0;
//		if (size > 2){
//			incColor = 255 / (size -2);
//			incColor2 = 150 / (size -2);
//		}
//		int indexColor = 0;
//		int indexColor2 = 0;
//		Color pColor = new Color(255,0,0);
		colorState = new HashMap();
		for (int i = 0; i < size ; i++){
			String values = (String) pMantisPlugin.status_values.elementAt(i);
			Integer id = (Integer) pMantisPlugin.statusByValues.get(values);
			choiceState.add(values);
			if(values != null)
				colorState.put(id, pMantisPlugin.statusColorTable.get(values));
//			if (i == 0){
//				//colorState.put(id, new Color(0,150,255));
//				colorState.put(id, new Color(0,150,0));
//			}else if (i < size-1 ){
//				indexColor = indexColor + incColor;
//				indexColor2 = indexColor2 + incColor2;
//				//pColor = new Color( 255, indexColor, indexColor2);
//				pColor = new Color( 0, indexColor, indexColor2);
//				colorState.put(id, pColor);
//			} else {
//				//colorState.put(id, Color.GREEN);
//				colorState.put(id, Color.CYAN);
//			}
		}
		//20100108 - Fin modification Forge ORTF V1.0.0

		if (isMiniSize){
			panelFiltre.setLayout(new GridLayout(2, 3));
			panelFiltre.add(labelProduit);
			panelFiltre.add(labelSeverite);
			panelFiltre.add(labelPriorite);
			panelFiltre.add(comboProduit);
			panelFiltre.add(comboSeverite);
			panelFiltre.add(comboPriorite);
		} else {
			labelRapporter = new JLabel(Language.getInstance().getText("Rapporteur") + " :"); //Rapporteur
			//comboRapporter; //On Maxi
			Vector choiceRapporter = pMantisPlugin.getBugTrackerAllUsers();
			choiceRapporter.add(0, org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
			comboRapporter = new JComboBox(choiceRapporter);
			comboRapporter.addActionListener(this);
			comboRapporter.setEditable(false);

			labelOwner = new JLabel(Language.getInstance().getText("Destinataire") + " :"); //Assigne a
			//comboOwner; //On Maxi
			Vector choiceOwner = pMantisPlugin.getBugTrackerAllUsers();
			choiceOwner.add(0, org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
			comboOwner = new JComboBox(choiceOwner);
			comboOwner.addActionListener(this);
			comboOwner.setEditable(false);

			labelDiagnostic = new JLabel(Language.getInstance().getText("Diagnostic") + " :"); //Diagnostic
			//comboDiagnostic; //On Maxi


			Vector choiceDiagnostic = new Vector();
			choiceDiagnostic.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
			size = pMantisPlugin.resolution_values.size();
			for (int i = 0; i < size ; i++){
				choiceDiagnostic.add( pMantisPlugin.resolution_values.elementAt(i));
			}
			comboDiagnostic = new JComboBox(choiceDiagnostic);
			comboDiagnostic.addActionListener(this);
			comboDiagnostic.setEditable(false);

			labelState = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Etat") + " :"); //Etat
			//JComboBox comboState; //On Maxi
			/*choiceState = new Vector();
			choiceState.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Tout"));
			size = pMantisPlugin.status_values.size();
			int incColor = 0;
			int incColor2 = 0;
			if (size > 2){
				incColor = 255 / (size -2);
				incColor2 = 150 / (size -2);
			}
			int indexColor = 0;
			int indexColor2 = 0;
			Color pColor = new Color(255,0,0);
			colorState = new Hashtable();
			for (int i = 0; i < size ; i++){
				String values = (String) pMantisPlugin.status_values.elementAt(i);
				Integer id = (Integer) pMantisPlugin.statusByValues.get(values);
				choiceState.add( pMantisPlugin.status_values.elementAt(i));
				if (i == 0){
					colorState.put(id, new Color(0,150,255));
				}else if (i < size-1 ){
					indexColor = indexColor + incColor;
					indexColor2 = indexColor2 + incColor2;
					pColor = new Color( 255, indexColor, indexColor2);
					colorState.put(id, pColor);
				} else {
					colorState.put(id, Color.GREEN);
				}
			}*/
			comboState = new JComboBox(choiceState);
			comboState.addActionListener(this);
			comboState.setEditable(false);

			labelHideSate = new JLabel(Language.getInstance().getText("Cacher_le_statut") + " :");
			Vector choiceHideSate = new Vector();
			size =  choiceState.size();
			choiceHideSate.add(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Aucun"));
			String str2add =  Language.getInstance().getText("et_supp");
			for (int i =1 ; i < size ; i++){
				choiceHideSate.add(choiceState.elementAt(i) + " " + str2add);
			}
			comboHideSate = new JComboBox(choiceHideSate);
			comboHideSate.addActionListener(this);
			comboHideSate.setEditable(false);

			labelResume = new JLabel(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Resume_______"));
			filtreResume = new JTextField(150);
			checkStartWith = new JCheckBox(Language.getInstance().getText("Start_With"));
			checkStartWith.setSelected(true);
			checkContyent = new JCheckBox(Language.getInstance().getText("Content"));

			checkStartWith.addItemListener(new ItemListener() {
	            public void itemStateChanged(ItemEvent e) {
	                if (checkStartWith.isSelected()){
	                	checkContyent.setSelected(false);
	                } else {
	                	checkContyent.setSelected(true);
	                }
	            }
			});
			checkContyent.addItemListener(new ItemListener() {
	            public void itemStateChanged(ItemEvent e) {
	                if (checkContyent.isSelected()){
	                	checkStartWith.setSelected(false);
	                }else {
	                	checkStartWith.setSelected(true);
	                }
	            }
			});
			filtreResume.addKeyListener(new KeyListener() {
	            public void keyTyped(KeyEvent keyEvent)  {
	                reloadData();
	            }
	            public void keyPressed(KeyEvent keyEvent) {}
	            public void keyReleased(KeyEvent keyEvent) {}

			});


			panelFiltre.setLayout(new GridLayout(5, 4));
			panelFiltre.add(labelProduit);
			panelFiltre.add(labelSeverite);
			panelFiltre.add(labelPriorite);
			panelFiltre.add(labelState);

			panelFiltre.add(comboProduit);
			panelFiltre.add(comboSeverite);
			panelFiltre.add(comboPriorite);
			panelFiltre.add(comboState);

			panelFiltre.add(labelDiagnostic);
			panelFiltre.add(labelRapporter);
			panelFiltre.add(labelOwner);
			panelFiltre.add(labelHideSate);

			panelFiltre.add(comboDiagnostic);
			panelFiltre.add(comboRapporter);
			panelFiltre.add(comboOwner);
			panelFiltre.add(comboHideSate);

			JPanel resumePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
			resumePanel.add(labelResume);
			resumePanel.add(filtreResume);

			JPanel checkPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
			checkPanel.add(checkStartWith);
			checkPanel.add(checkContyent);
			//checkPanel.add(applyFiltreButton);
			panelFiltre.add(filtreResume);
			panelFiltre.add(checkPanel);

			panelFiltre.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),"Filtre"));

		}

	}

	void initTablePanel(){
		defectsTable = new JTable();
		defectsTableModel = new MyTableModel();
		if (isMiniSize){
			defectsTableModel.addColumnNameAndColumn("id");
			defectsTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Environnement")); //Produit
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resume")); //Produit
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Severite")); //Etat
		} else {
			defectsTableModel.addColumnNameAndColumn("id");
			defectsTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Environnement")); //Produit
			//defectsTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Etat")); //Etat
			defectsTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Priority")); //Etat
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resume")); //Produit
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Severite")); //Etat
			defectsTableModel.addColumnNameAndColumn(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("resolution"));
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Rapporteur"));
			defectsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Destinataire"));

		}


		sorter = new TableSorter(defectsTableModel);
        defectsTable.setModel(sorter);
        sorter.setTableHeader(defectsTable.getTableHeader());
        defectsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane defectsTableScrollPane = new JScrollPane(defectsTable);
        defectsTable.getColumnModel().getColumn(0).setWidth(20);
        try {
    		//reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(reqCovered));
        	defectsTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new StateTableCellRenderer());
    	} catch (Exception  e){
    		e.printStackTrace();
    	}

    	defectsTable.addMouseListener(new MouseAdapter() {
    		public void mouseClicked(MouseEvent e) {
    			if (e.getClickCount()>1){
    				if (isIndependant){
    					editDefectPerformed();
    				}
    			}
    		}
    	});

    	int size = choiceState.size();
        JPanel statePanel = new JPanel();
        statePanel.setLayout(new GridLayout(1, size -1 ));
        for (int i = 1 ; i < size ; i++){
        	String state = (String) choiceState.elementAt(i);
        	JLabel pLabel = new JLabel(state);
        	Integer id = (Integer) pMantisPlugin.statusByValues.get(state);
        	Color pColor = (Color) colorState.get(id);

        	pLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
         	pLabel.setBackground(pColor);
        	//pLabel.setForeground(pColor);
        	pLabel.setOpaque(true);
        	statePanel.add(pLabel);
        }
        defectsPanel = new JPanel();
        defectsPanel.setLayout(new BorderLayout());
        defectsPanel.add(defectsTableScrollPane, BorderLayout.CENTER);
        defectsPanel.add(statePanel, BorderLayout.SOUTH);

	}

	void initActionPanel(){
		isIndependant = true;
		actionPanel = new JPanel(new GridLayout(1, 5));
		newDefectButton = new JButton(Language.getInstance().getText("Ajouter"));
		newDefectButton.addActionListener(this);
		editDefectButton = new JButton(Language.getInstance().getText("Editer"));
		editDefectButton.addActionListener(this);
		viewLinkButton = new JButton(Language.getInstance().getText("Liason"));
		viewLinkButton.addActionListener(this);
		addLinkButton = new JButton(Language.getInstance().getText("Lier"));
		addLinkButton.addActionListener(this);

		deleteButton  = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Supprimer"));
		deleteButton.addActionListener(this);
		/*if (pMantisPlugin.access_level < 70){
			deleteButton.setEnabled(false);
		}*/
		actionPanel.add(newDefectButton);
		actionPanel.add(editDefectButton);
		actionPanel.add(viewLinkButton);
		actionPanel.add(addLinkButton);
		actionPanel.add(deleteButton);
	}

	void applyEnvRestriction(String envName){
		int size = comboProduit.getItemCount();
		int i = 0;
		boolean trouve = false;
		if (envName != null){
			while (i < size && !trouve){
				String value = ((String)comboProduit.getItemAt(i)).trim();
				if (value.equals(envName)){
					trouve = true;
					comboProduit.setSelectedIndex(i);
					/*if (!(value.equals("___NO_ENV___") || value.equals("")  )){
						comboProduit.setEnabled(false);
					}*/
				}
				i++;
			}
		}
	}

	void loadData(Hashtable hashDefectsWrapper){
		if (hashDefectsWrapper == null){
			return;
		}
		/* HASH : ID - > DefectWrapper */
		defectsWrapper = hashDefectsWrapper;
		reloadData();
	}

	void reloadData(){
		if (defectsWrapper == null || canReload == false){
			return;
		}
		Enumeration listKey = defectsWrapper.keys();
		defectsTableModel.clearTable();
		while (listKey.hasMoreElements()){
			Integer ID = (Integer) listKey.nextElement();
			DefectWrapper pDefectWrapper = (DefectWrapper) defectsWrapper.get(ID);
			if (isFiltred(pDefectWrapper)){
				ArrayList data = new ArrayList();
				if (isMiniSize){
					data.add(""+pDefectWrapper.getId());
					data.add(pDefectWrapper.getEnvironement());
					data.add(pDefectWrapper.getResume());
					int severity = pDefectWrapper.getSeverity();
					String str_severity = (String) pMantisPlugin.severityByID.get(Integer.valueOf(""+severity));
					data.add(str_severity);
				} else {
					data.add(""+pDefectWrapper.getId());
					data.add(pDefectWrapper.getEnvironement());

					/*int state = pDefectWrapper.getStatus();
					String str_state = (String) pMantisPlugin.statusByID.get(Integer.valueOf(state));
					data.add(str_state);
					*/
					int priority = pDefectWrapper.getPriority();
					String str_priority = (String) pMantisPlugin.priorityByID.get(Integer.valueOf(""+priority));
					data.add(str_priority);
//
					data.add(pDefectWrapper.getResume());
					int severity = pDefectWrapper.getSeverity();
					String str_severity = (String) pMantisPlugin.severityByID.get(Integer.valueOf(""+severity));
					data.add(str_severity);
					int resolution = pDefectWrapper.getResolution();
					String str_resolution =  (String) pMantisPlugin.resolutionByID.get(Integer.valueOf(""+resolution));
					data.add(str_resolution);
					data.add(pDefectWrapper.getUser());
					data.add(pDefectWrapper.getRecipient());
					//"id","Environnement", "priorite", "Resume", "Severite", "resolution", "Rapporteur", "Destinataire";
				}
				defectsTableModel.addRow(data);
			}
		}
		setColumnSize(defectsTable);
	}

	private boolean isFiltred(DefectWrapper pDefectWrapper){
		int envSelected = comboProduit.getSelectedIndex();
		if (envSelected > 0){
			String filtredEnv = (String) comboProduit.getSelectedItem();
			String env = pDefectWrapper.getEnvironement().trim();
			if (envSelected>1){
				if (!env.equals(filtredEnv)){
					return false;
				}
			} else {
				//No env or vide
				if (!( (env.equals("")) || (env.equals("___NO_ENV___")) )){
					return false;
				}
			}
		}

		int severityselected = comboSeverite.getSelectedIndex();
		if (severityselected > 0){
			String filtredSeverity = (String) comboSeverite.getSelectedItem();
			int severity = pDefectWrapper.getSeverity();
			String str_seveiry = (String) pMantisPlugin.severityByID.get(Integer.valueOf(""+severity));
			if (!filtredSeverity.equals(str_seveiry)){
				return false;
			}
		}

		int priorityselected = comboPriorite.getSelectedIndex();
		if (priorityselected > 0){
			String filtredPriority = (String) comboPriorite.getSelectedItem();
			int priority = pDefectWrapper.getPriority();
			String str_priority = (String) pMantisPlugin.priorityByID.get(Integer.valueOf(""+priority));
			if (!filtredPriority.equals(str_priority)){
				return false;
			}
		}

		if (!isMiniSize){
			int stateselected = comboState.getSelectedIndex();
			if (stateselected > 0){
				String filtredState = (String) comboState.getSelectedItem();
				int state = pDefectWrapper.getStatus();
				String str_state = (String) pMantisPlugin.statusByID.get(Integer.valueOf(""+state));
				if (!filtredState.equals(str_state)){
					return false;
				}
			}


			int diagnosticselected = comboDiagnostic.getSelectedIndex();
			if (diagnosticselected > 0){
				String filtredState = (String) comboDiagnostic.getSelectedItem();
				int res = pDefectWrapper.getResolution();
				String str_res = (String) pMantisPlugin.resolutionByID.get(Integer.valueOf(""+res));
				if (!filtredState.equals(str_res)){
					return false;
				}
			}


			int rapporter_selected = comboRapporter.getSelectedIndex();
			if (rapporter_selected > 0){
				String filtredRapporter = (String) comboRapporter.getSelectedItem();
				String str_rapporter= (String) pDefectWrapper.getUser();
				if (!filtredRapporter.equals(str_rapporter)){
					return false;
				}
			}

			int owner_selected = comboOwner.getSelectedIndex();
			if (owner_selected > 0){
				String filtredOwner = (String) comboOwner.getSelectedItem();
				String str_owner = (String) pDefectWrapper.getRecipient();
				System.out.println("Owner is " + str_owner);
				if (!filtredOwner.equals(str_owner)){
					return false;
				}
			}

			int hidesate_selected = comboHideSate.getSelectedIndex();
			if (hidesate_selected > 0){
				String filtredState = (String) comboState.getItemAt(hidesate_selected);
				int state = pDefectWrapper.getStatus();
				int stateFiltre = ((Integer) pMantisPlugin.statusByValues.get(filtredState)).intValue();
				if (!(state <= stateFiltre)){
					return false;
				}
			}
			String filtre = filtreResume.getText().trim();
			if (!filtre.equals("")){
				String sumary = pDefectWrapper.getResume().trim();
				if (checkStartWith.isSelected()){
					if (!sumary.startsWith(filtre)){
						return false;
					}
				}

				if (checkContyent.isSelected()){
					filtre= ".*"+filtre+".*";
					if (!sumary.matches(filtre)){
						return false;
					}
				}
			}
		}
		return true;
	}

	public void updateDefects(Hashtable defects){

	}
	/**************************** Listener ***********************************/
	public void actionPerformed(ActionEvent e){
		if (e.getSource().equals(newDefectButton)){
			newDefectPerformed();
		} else if (e.getSource().equals(editDefectButton)){
			editDefectPerformed();
		} else if (e.getSource().equals(deleteButton)){
			deletePerformed();
		} else if (e.getSource().equals(viewLinkButton)){
			viewLinkPerformed();
		} else if (e.getSource().equals(addLinkButton)){
			 addLinkPerformed();
		} else if (e.getSource().equals(comboProduit)){
			reloadData();
		} else if (e.getSource().equals(comboSeverite)){
			reloadData();
		} else if (e.getSource().equals(comboPriorite)){
			reloadData();
		} else if (e.getSource().equals(comboState)){
			reloadData();
		} else if (e.getSource().equals(comboDiagnostic)){
			reloadData();
		} else if (e.getSource().equals(comboOwner)){
			reloadData();
		} else if (e.getSource().equals(comboRapporter)){
			reloadData();
		} else if (e.getSource().equals(comboHideSate)){
			reloadData();
		}
	}

	void addLinkPerformed(){
		DefectWrapper pDefectWrapper  = getSelectedDefect();
		AddLinkDialog pAddLinkDialog = new AddLinkDialog(pDefectWrapper, pMantisPlugin);
		if (pAddLinkDialog.isDoingModification()){
			try {
				loadData(pMantisPlugin.getDefectsOfProject(true));
			} catch (Exception e){
				e.printStackTrace();
			}
		}else {
			reloadData();
		}
	}

	void viewLinkPerformed(){
		DefectWrapper pDefectWrapper  = getSelectedDefect();
		if (pDefectWrapper !=null){
			new ViewLinkDialog(pDefectWrapper, pMantisPlugin);
		}
	}


	void deletePerformed() {
		DefectWrapper pDefectWrapper  = getSelectedDefect();
		if (pDefectWrapper != null){
			if (pMantisPlugin.access_level < 70) {
				JOptionPane.showMessageDialog(  SalomeTMFContext.getInstance().getSalomeFrame(),
	                    Language.getInstance().getText("Access_level_insuffisant2"),
	                    "Error!",
	                    JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (pMantisPlugin.deleteConfirme( Language.getInstance().getText("Anomalie_selectione"))){
				try {
					pMantisPlugin.deleteDefect(pDefectWrapper);
					//deleteref
					defectsWrapper.remove(new Integer (pDefectWrapper.getId()));
					reloadData();
				} catch (Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	/*void applyFiltrePerformed() {
		String filtre = filtreResume.getText().trim();
		if (filtre.equals("")){
			return;
		}
	}*/

	void newDefectPerformed(){
		//new AskNewBug(pMantisPlugin, false,  "", "", "", "");
		DefectView pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pMantisPlugin);
		try {
			if (pDefectView.isDoingModification()){
				loadData(pMantisPlugin.getDefectsOfProject(true));
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	void editDefectPerformed(){
		DefectWrapper pDefectWrapper  = getSelectedDefect();
		if (pDefectWrapper !=null){
			DefectView pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pDefectWrapper, pMantisPlugin, true);

			try {
				if (pDefectView.isDoingModification()){
					loadData(pMantisPlugin.getDefectsOfProject(true));
					//reloadData();
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	DefectWrapper getSelectedDefect(){
		DefectWrapper pDefectWrapper = null;
		if (defectsWrapper == null){
			return null;
		}
		int selectedRowIndex = defectsTable.getSelectedRow();
		if (selectedRowIndex != -1) {
			String str_id = (String)sorter.getValueAt(selectedRowIndex,0);
			str_id = str_id.trim();
			Integer id = Integer.valueOf(str_id);
			pDefectWrapper = (DefectWrapper) defectsWrapper.get(id);
			/* System.out.println(id);*/
		}
		return pDefectWrapper;

	}
	/********************* Render ******************/
	 /**
	    * redefine size of table columns
	    */
	   public void setColumnSize(JTable table){
	       try {
		   FontMetrics fm = table.getFontMetrics(table.getFont());
	       for (int i = 0 ; i < table.getColumnCount() ; i++)
	       {
	           int max = 0;
	           for (int j = 0 ; j < table.getRowCount() ; j++)
	           {
	        	  int taille = fm.stringWidth((String)table.getValueAt(j,i));
	              if (taille > max)
	                   max = taille;
	           }
	           String nom = (String)table.getColumnModel().getColumn(i).getIdentifier();
	           int taille = fm.stringWidth(nom);
	           if (taille > max)
	                  max = taille;
	          table.getColumnModel().getColumn(i).setPreferredWidth(max+10);
	       }
	       } catch (Exception e){

	       }
	   }


	public class StateTableCellRenderer  extends DefaultTableCellRenderer {
		Color colorToUse = null;

		public Component getTableCellRendererComponent
		(JTable table, Object value, boolean isSelected,
				boolean hasFocus, int row, int column) {

			//super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			try {
				if (column == 0){
					Integer ID = Integer.valueOf((String) value);
					DefectWrapper pDefectWrapper = (DefectWrapper) defectsWrapper.get(ID);
					int state = pDefectWrapper.getStatus();
					colorToUse = (Color) colorState.get(Integer.valueOf(""+state));
				}
				if (colorToUse != null){
					if (isSelected){
						setBorder(BorderFactory.createLineBorder(Color.BLACK));
					}
		         	setBackground(colorToUse);
		        	setForeground(Color.BLACK);
		        	//setOpaque(false);
		        	setText((String) value);
				}

				//super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

			} catch (Exception e){
				e.printStackTrace();
			}
			return this;
		}
	}
}
