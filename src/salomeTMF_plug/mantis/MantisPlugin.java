/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: 	mikael.marche@rd.francetelecom.com
 * 			faycal.sougrati@francetelecom.com
 */

package salomeTMF_plug.mantis;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import org.dom4j.Document;
import org.dom4j.Element;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.Plugin;
import org.java.plugin.PluginDescriptor;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Mail;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.SalomeEvent;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.main.IBugJDialog;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.Common;
import org.objectweb.salome_tmf.plugins.core.XMLLoaderPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLWriterPlugin;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;
import salomeTMF_plug.mantis.sqlWrapper.MantisConnector;
import salomeTMF_plug.mantis.sqlWrapper.SQLMantis;
import salomeTMF_plug.requirements.ReqPlugin;

public class MantisPlugin extends Plugin implements BugTracker, Observer,
		XMLPrinterPlugin, Common {

	// JApplet applet = null;
	User currentUser = null;
	Project currentProject = null;
	Properties cfg_prop = null;
	static String mantis_host = "";
	// ...IDataBase iDB = null;
	// ...Properties sql_prop = null;
	boolean isCurrentUserExistsInProject = false;
	int bugUsrID = -1;
	int bugProjectID = -1;

	final String MANTIS_CFG_PATH = "/plugins/mantis/cfg/";
	final String MANTIS_CFG_FILE = "CfgMantis.properties";
	// ...final String MANTIS_STMT_FILE
	// ="/salomeTMF_plug/mantis/resources/sql/Mantis_Stmts.properties";
	final String MANTIS_SUB_MENU = "Mantis";
	final static String ATTACH_DESC = "[MANTIS_ATTACH]";
	final static boolean AUTOADD = true;

	/*
	 * final String BLOCKER_STR = "block"; final String MAJOR_STR = "major";
	 * final String CRITICAL_STR = "crash";
	 */
	final int DEFAULT_CORRECTED_VALUE = 90;

	Hashtable priorityByValues;
	public Hashtable priorityByID;
	Vector priority_values;

	Hashtable severityByValues;
	public Hashtable severityByID;
	Vector severity_values;

	Hashtable statusByValues;
	public Hashtable statusByID;
	Vector status_values;
	Map statusColorTable;

	Hashtable reproductibilityByValues;
	public Hashtable reproductibilityByID;
	Vector reproductibility_values;

	Hashtable resolutionByValues;
	public Hashtable resolutionByID;
	Vector resolution_values;

	int access_level = 0;
	/*
	 * 0 = VIEWER
	 *
	 * 70 = MANAGER 90 = ADMIN of MANTIS
	 */

	static boolean suspended = false;

	/****** MantisConnector *****/
	MantisConnector pMantisConnector;
	DefectPanel pDefectPanel1;
	DefectPanel pDefectPanel2;
	DefectPanel pDefectPanel3;

	DefectPanel pMTestDefectPanel;
	DefectPanel pATestDefectPanel;
	JTabbedPane pMTestJTabbedPane;
	JTabbedPane pATestJTabbedPane;
	JTree salomeDynamicTree;

	static JTabbedPane salomeMainTabs;
	JTabbedPane pJTabbedPane2;
	JTabbedPane pJTabbedPane3;
	Hashtable defectsCache;

	private boolean mandatoryImport = false;
	private boolean mandatoryExport = false;

	public MantisPlugin(PluginManager arg0, PluginDescriptor arg1) {
		super(arg0, arg1);
	}

	protected void doStart() throws Exception {
		// TODO Auto-generated method stub

	}

	protected void doStop() throws Exception {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void initBugTracker(Object iPlugObject) throws Exception {

		// applet = (JApplet)iPlugObject;
		currentUser = PluginsTools.getCurrentUser();
		currentProject = PluginsTools.getCurrentProject();

		currentProject.registerObserver(this);

		// Opens mantis connexion
		try {
			URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
			String url_txt = _urlBase.toString();
			url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
			URL urlBase = new URL(url_txt);
			// Mantis configuration file for DB connexion
			String unixProjectName = Api.getUnixProjectName();
			java.net.URL url_cfg;
			if (unixProjectName == null) {
				url_cfg = new java.net.URL(urlBase + MANTIS_CFG_PATH
						+ MANTIS_CFG_FILE);
			} else {
				url_cfg = new java.net.URL(urlBase + MANTIS_CFG_PATH
						+ unixProjectName + "_" + MANTIS_CFG_FILE);
			}
			String mantisCfg = null;
			try {
				Util.log("Try to find cfg file: " + url_cfg);
				cfg_prop = Util.getPropertiesFile(url_cfg);
			} catch (Exception e) {
				Util.log("Failed to find cfg file.");
				if (unixProjectName == null) {
					mantisCfg = MANTIS_CFG_PATH + MANTIS_CFG_FILE;

				} else {
					mantisCfg = MANTIS_CFG_PATH + unixProjectName + "_"
							+ MANTIS_CFG_FILE;
				}
				cfg_prop = Util.getPropertiesFile(mantisCfg);
				Util.log("Get cfg file with relative path.");
			}

			String driver = cfg_prop.getProperty("JDBC_DRIVER");
			String url_db = cfg_prop.getProperty("MANTIS_URL_DB");
			String user = cfg_prop.getProperty("MANTIS_USER_LOGIN");
			String pwd = cfg_prop.getProperty("MANTIS_USER_PWD");
			InputStream in;
			try {
				in = url_cfg.openStream();
			} catch (Exception e) {
				in = Util.class.getResourceAsStream(mantisCfg);
			}
			byte[] bytes = new byte[1024];
			in.read(bytes);
			String confString = new String(bytes, "ISO-8859-1");
			int begin = confString.indexOf("MANTIS_USER_PWD=");
			int size = "MANTIS_USER_PWD=".length() + begin;
			// String pwd = confString.substring(size, size +16);

			// Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			// SecretKeySpec keySpec = new SecretKeySpec(Api.getCryptKey()
			// .getBytes(), "AES");
			// IvParameterSpec ivSpec = new IvParameterSpec("fedcba9876543210"
			// .getBytes());
			// cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
			// pwd = new
			// String(cipher.doFinal(pwd.getBytes("ISO-8859-1"))).trim();

			// if (!Api.isIDE_DEV()) {
			// try {
			// pwd = org.objectweb.salome_tmf.api.MD5paswd
			// .decryptString(pwd);
			// } catch (Exception e) {
			// Util.log("[MantisPlugin] Clear pwd used for Mantis");
			// }
			// }

			mantis_host = cfg_prop.getProperty("MANTIS_URL_HTTP");
			Util.log("[MantisPlugin] MANTIS open database : " + ", " + driver
					+ ", " + url_db + ", " + user);
			if (!mantis_host.endsWith("/"))
				mantis_host = mantis_host + "/";

			/*** Create an Instance of Mantis Connector **/
			/** To be changed with classname getInstance readed from properties **/
			pMantisConnector = (MantisConnector) new SQLMantis();

			pMantisConnector.initConnector(driver, url_db, user, pwd);
			/*
			 * ISQLObjectFactory iSQL_OF = Api.getISQLObjectFactory(); if (iDB
			 * == null){ iDB = iSQL_OF.getInstanceOfDataBase(driver);
			 * iDB.open(url_db,user,pwd); SQLUtils.initSQLUtils(iDB); } //
			 * Mantis properties file for SQL statements sql_prop =
			 * Util.getPropertiesFile(getClass().getResource(MANTIS_STMT_FILE));
			 */

			// Set bug Project
			bugProjectID = pMantisConnector.getProjectID(currentProject
					.getNameFromModel());
			if (bugProjectID < 1 && AUTOADD) {
				/* Add current project in Mantis */
				addProject_impl(currentProject);
			}

			// Set bug user
			String user_login = currentUser.getLoginFromModel();
			bugUsrID = pMantisConnector.getUserID(user_login);
			if (bugUsrID < 1 && AUTOADD) {
				/* Add current user in Mantis */
				addUser_impl(currentUser);
			}

			access_level = pMantisConnector.getUserAccesLevel(bugUsrID,
					bugProjectID);
			if (access_level != -1) {
				isCurrentUserExistsInProject = true;
			} else {
				if (AUTOADD) {
					addUserInProject_impl();
				} else {
					access_level = 0;
				}
			}

			// Init Mantis Fields
			initMantisFields();
			pDefectPanel1 = new DefectPanel(false, this, null, null);
			salomeMainTabs.addTab("Plug-in Mantis", pDefectPanel1);
			suspended = false;
			salomeMainTabs.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (salomeMainTabs.getSelectedComponent().equals(
							pDefectPanel1)) {
						if (bugProjectID > 0) {
							try {
								pDefectPanel1.loadData(pMantisConnector
										.getProjectDefects(bugProjectID));
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
						System.out.println("MANTIS");
					}
				}
			});

			TestDefectActionPanel pATestDefectActionPanel = new TestDefectActionPanel(
					this);
			pATestDefectPanel = new DefectPanel(true, this,
					pATestDefectActionPanel, null);
			pATestDefectActionPanel.setDefectPanel(pATestDefectPanel);
			pATestJTabbedPane.addTab("Plug-in Mantis", pATestDefectPanel);
			pATestJTabbedPane.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (pATestJTabbedPane.getSelectedComponent().equals(
							pATestDefectPanel)) {
						if (bugProjectID > 0) {
							try {
								loadDefectTest(DataModel.getCurrentTest(),
										pATestDefectPanel);
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
						System.out.println("MANTISA");
					}
				}
			});

			TestDefectActionPanel pMTestDefectActionPanel = new TestDefectActionPanel(
					this);
			pMTestDefectPanel = new DefectPanel(true, this,
					pMTestDefectActionPanel, null);
			pMTestDefectActionPanel.setDefectPanel(pMTestDefectPanel);
			pMTestJTabbedPane.addTab("Plug-in Mantis", pMTestDefectPanel);
			pMTestJTabbedPane.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (pMTestJTabbedPane.getSelectedComponent().equals(
							pMTestDefectPanel)) {
						if (bugProjectID > 0) {
							try {
								loadDefectTest(DataModel.getCurrentTest(),
										pMTestDefectPanel);
							} catch (Exception ex) {
								ex.printStackTrace();
							}
						}
						System.out.println("MANTISM");
					}
				}
			});

		} catch (Exception E) {
			E.printStackTrace();
			SalomeTMFContext.getInstance().showMessage(
					Language.getInstance().getText("Probleme_config"),
					Language.getInstance().getText("Plugin_non_charge"),
					JOptionPane.WARNING_MESSAGE);
			throw E;
		}

	}

	/**
	 * @return true if projet, and user are stored in other DB (not in salome
	 *         DB)
	 */
	public boolean isIndependantTracker() {
		return true;
	}

	/**
	 * @return true if the tracker support ICAL/QSCORE
	 */
	public boolean isSuportIndicators() {
		return true;
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public boolean isUserExistsInBugDB() {
		return isCurrentUserExistsInProject;
	}

	public void addUser(User user) throws Exception {
		if (bugProjectID < 1) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"creation_projet_avant_creation_utilisateur"), Language
					.getInstance().getText("Information..."),
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		if (isCurrentUserExistsInProject) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"L_utilisateur_")
					+ currentUser.getLoginFromModel()
					+ Language.getInstance()
							.getText("_existe_deja_dans_Mantis"), Language
					.getInstance().getText("Information"),
					JOptionPane.INFORMATION_MESSAGE);
		} else {

			// Adding the user to Mantis
			/*
			 * int bugUsrID =
			 * pMantisConnector.getUserID(user.getLoginFromModel()); if
			 * (bugUsrID == -1){ bugUsrID = pMantisConnector.addUser(user); } //
			 * Add user to mantis project int access_l = 70; // manager if
			 * (currentProject
			 * .getAdministratorWrapperFromDB().getLogin().equals(
			 * currentUser.getLoginFromModel())) { access_l = 90; // admin }
			 *
			 * pMantisConnector.addUserInProject(bugUsrID, bugProjectID,
			 * access_l);
			 *
			 *
			 * isCurrentUserExistsInProject = true; access_level = access_l;
			 */

			addUser_impl(user);
			access_level = addUserInProject_impl();

			JOptionPane
					.showMessageDialog(
							SalomeTMFContext.getInstance().getSalomeFrame(),
							Language.getInstance().getText("L_utilisateur_")
									+ currentUser.getLoginFromModel()
									+ Language
											.getInstance()
											.getText(
													"_a_ete_cree_dans_Mantis_avec_succes")
									+ Language
											.getInstance()
											.getText(
													"vous_devez_quitter_Salome_et_vous_reconnecter_pour")
									+ Language
											.getInstance()
											.getText(
													"acceder_aux_fonctionnalites_concernees"),
							Language.getInstance().getText("Information..."),
							JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void addUser_impl(User user) throws Exception {
		bugUsrID = pMantisConnector.getUserID(user.getLoginFromModel());
		if (bugUsrID == -1) {
			bugUsrID = pMantisConnector.addUser(user);
		}
	}

	private int addUserInProject_impl() throws Exception {
		int access_l = 70; // manager
		if (currentProject.getAdministratorWrapperFromDB().getLogin().equals(
				currentUser.getLoginFromModel())) {
			access_l = 90; // admin
		}

		pMantisConnector.addUserInProject(bugUsrID, bugProjectID, access_l);
		isCurrentUserExistsInProject = true;
		return access_l;
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void addProject(Project project) throws Exception {
		if (bugProjectID > 0) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Le_projet_")
					+ currentProject.getNameFromModel()
					+ Language.getInstance()
							.getText("_existe_deja_dans_Mantis"), Language
					.getInstance().getText("Information"),
					JOptionPane.INFORMATION_MESSAGE);
		} else {
			// Adding Salome TMF project to Mantis DB
			/*
			 * bugProjectID = pMantisConnector.addProject(currentProject) ; if
			 * (bugProjectID != -1){
			 * pMantisConnector.addDefaultEnvToProject(bugProjectID); }
			 */
			addProject_impl(currentProject);
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Le_projet_")
					+ currentProject.getNameFromModel()
					+ Language.getInstance().getText(
							"_a_ete_cree_dans_Mantis_avec_succes"), Language
					.getInstance().getText("Information..."),
					JOptionPane.INFORMATION_MESSAGE);
		}
	}

	private void addProject_impl(Project project) throws Exception {
		bugProjectID = pMantisConnector.addProject(project);
		if (bugProjectID != -1) {
			pMantisConnector.addDefaultEnvToProject(bugProjectID);
		}
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void addEnvironment(Environment environment) throws Exception {
		if (!(bugProjectID > 0)) {
			return;
		}
		pMantisConnector.addEnvironment(environment, bugProjectID);
	}

	void addEnvironment(String envName) throws Exception {
		if (!(bugProjectID > 0)) {
			return;
		}
		Environment environment = DataModel.getCurrentProject()
				.getEnvironmentFromModel(envName);
		if (environment == null) {
			pMantisConnector.addEnvironment(envName, "", bugProjectID);
		} else {
			addEnvironment(environment);
		}
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void updateEnvironment(String old_component, String new_component,
			String description) throws Exception {
		if (!(bugProjectID > 0)) {
			return;
		}
		pMantisConnector.updateEnvironment(old_component, new_component,
				description, bugProjectID);
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void deleteEnvironment(String environment) throws Exception {
		if (!(bugProjectID > 0)) {
			return;
		}
		pMantisConnector.deleteEnvironment(environment, bugProjectID);
	}

	public JPanel getBugViewPanel(IBugJDialog pIBugJDialog, String actionName,
			String actionDesc, String actionAwatedRes, String actionEffectiveRes) {
		ExecutionResult execResult = DataModel.getObservedExecutionResult();
		ExecutionTestResult executionTestResult = DataModel
				.getCurrentExecutionTestResult();
		return new DefectViewPanel(pIBugJDialog, execResult,
				executionTestResult, actionName, actionDesc, actionAwatedRes,
				actionEffectiveRes, this);
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public Attachment addBug(String long_desc, String assigned_to,
			String url_attach, String bug_severity, String short_desc,
			String bug_OS, String bug_priority, String bug_platform,
			String bug_reproductibility, Execution pExec,
			ExecutionResult pExecRes, Test ptest) throws Exception {

		if (access_level <= 10) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Access_level_insuffisant"), "Error!",
					JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}
		if (!(bugProjectID > 0)) {
			JOptionPane
					.showMessageDialog(SalomeTMFContext.getInstance()
							.getSalomeFrame(), Language.getInstance().getText(
							"Must_create_project"), "Error!",
							JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}

		UrlAttachment bugURL = null;

		// If bug environment doesn't exist, we create it!
		Environment env = pExec.getEnvironmentFromModel();
		if (!pMantisConnector.isExistEnv(bugProjectID, env.getNameFromModel())) {
			addEnvironment(env);
		}

		// Assigned to ID init
		int assigned_to_ID = -1;
		try {
			assigned_to_ID = pMantisConnector.getUserIDInProject(assigned_to,
					bugProjectID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (assigned_to_ID == -1) {
			assigned_to_ID = bugUsrID;
			assigned_to = currentUser.getLoginFromModel();
		}
		if ((url_attach != null) && (!url_attach.equals(""))) {
			short_desc += " (URL = " + url_attach + ")";
		}
		if ((bug_OS == null) || (bug_OS.equals(""))) {
			bug_OS = " ";
		}
		if ((short_desc == null) || (short_desc.equals(""))) {
			short_desc = " ";
		}
		int int_bug_priority;
		int int_bug_severity;
		int int_bug_reproductibility;

		if ((bug_priority == null) || (bug_priority == "")) {
			int_bug_priority = 10;
		} else {
			int_bug_priority = ((Integer) priorityByValues.get(bug_priority))
					.intValue();
		}
		if ((bug_severity == null) || (bug_severity == "")) {
			int_bug_severity = 10;
		} else {
			int_bug_severity = ((Integer) severityByValues.get(bug_severity))
					.intValue();
		}
		if ((bug_reproductibility == null) || (bug_reproductibility == "")) {
			int_bug_reproductibility = 10;
		} else {
			int_bug_reproductibility = ((Integer) reproductibilityByValues
					.get(bug_reproductibility)).intValue();
		}

		int bug_id = pMantisConnector.addDefect(bugUsrID, bugProjectID,
				assigned_to_ID, long_desc, url_attach, short_desc, bug_OS,
				int_bug_priority, bug_platform, int_bug_reproductibility,
				int_bug_severity, env.getNameFromModel());

		String url_res = mantis_host + "view_salome_bug_page.php?project_id="
				+ URLEncoder.encode("" + bugProjectID) + "&bug_id="
				+ URLEncoder.encode("" + bug_id) + "&user_id="
				+ URLEncoder.encode("" + bugUsrID);
		bugURL = new UrlAttachment(url_res, MantisPlugin.ATTACH_DESC);

		return bugURL;

	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	int addBug(DefectWrapper pDefectWrapper) throws Exception {

		if (access_level <= 10) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Access_level_insuffisant"), "Error!",
					JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}
		if (!(bugProjectID > 0)) {
			JOptionPane
					.showMessageDialog(SalomeTMFContext.getInstance()
							.getSalomeFrame(), Language.getInstance().getText(
							"Must_create_project"), "Error!",
							JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}

		UrlAttachment bugURL = null;

		// If bug environment doesn't exist, we create it!
		String env = pDefectWrapper.getEnvironement();
		if (env == null || env.equals("")) {
			env = "___NO_ENV___";
		}
		if (!pMantisConnector.isExistEnv(bugProjectID, env)) {
			addEnvironment(env);
		}

		// Assigned to ID init
		int assigned_to_ID = -1;
		try {
			assigned_to_ID = pMantisConnector.getUserIDInProject(pDefectWrapper
					.getRecipient(), bugProjectID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (assigned_to_ID == -1) {
			assigned_to_ID = bugUsrID;
		}

		int int_bug_priority = pDefectWrapper.getPriority();
		int int_bug_severity = pDefectWrapper.getSeverity();
		int int_bug_reproductibility = pDefectWrapper.getReproducibility();
		String long_desc = pDefectWrapper.getDescription();
		String short_desc = pDefectWrapper.getResume();
		String bug_OS = pDefectWrapper.getOs();
		String bug_platform = pDefectWrapper.getPlateforme();

		int bug_id = pMantisConnector.addDefect(bugUsrID, bugProjectID,
				assigned_to_ID, long_desc, null, short_desc, bug_OS,
				int_bug_priority, bug_platform, int_bug_reproductibility,
				int_bug_severity, env);

		/*
		 * String url_res = mantis_host + "view_salome_bug_page.php?project_id="
		 * + URLEncoder.encode (""+bugProjectID) + "&bug_id=" +
		 * URLEncoder.encode(""+bug_id) + "&user_id=" + URLEncoder.encode
		 * (""+bugUsrID); bugURL = new
		 * UrlAttachment(url_res,MantisPlugin.ATTACH_DESC);
		 */
		pDefectWrapper.setId(bug_id);
		sendMail(pDefectWrapper, "added", false);
		return bug_id;

	}

	void modifyBug(DefectWrapper pDefectWrapper) throws Exception {

		if (access_level <= 10) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Access_level_insuffisant"), "Error!",
					JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}
		int id = pDefectWrapper.getId();
		if (!(bugProjectID > 0) || id < 1) {
			JOptionPane
					.showMessageDialog(SalomeTMFContext.getInstance()
							.getSalomeFrame(), Language.getInstance().getText(
							"Must_create_project"), "Error!",
							JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}

		int id_assigned_to = -1;
		try {
			id_assigned_to = pMantisConnector.getUserIDInProject(pDefectWrapper
					.getRecipient(), bugProjectID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (id_assigned_to == -1) {
			id_assigned_to = bugUsrID;
		}

		int id_bug_severity = pDefectWrapper.getSeverity();
		int id_bug_satus = pDefectWrapper.getStatus();
		int id_bug_priority = pDefectWrapper.getPriority();
		int id_bug_reproducibility = pDefectWrapper.getReproducibility();
		int id_bug_resolution = pDefectWrapper.getResolution();
		String short_desc = pDefectWrapper.getResume();
		String long_desc = pDefectWrapper.getDescription();
		String bug_OS = pDefectWrapper.getOs();
		String bug_platform = pDefectWrapper.getPlateforme();
		String env = pDefectWrapper.getEnvironement();
		if (env == null || env.equals("")) {
			env = "___NO_ENV___";
		}
		if (!pMantisConnector.isExistEnv(bugProjectID, env)) {
			addEnvironment(env);
		}

		pMantisConnector.updateDefect(bugUsrID, bugProjectID, id,
				id_assigned_to, long_desc, id_bug_severity, id_bug_satus,
				short_desc, bug_OS, id_bug_priority, bug_platform, env,
				id_bug_reproducibility, id_bug_resolution);

		sendMail(pDefectWrapper, "updated", true);
	}

	private void sendMail(DefectWrapper pDefectWrapper, String action,
			boolean isUpdate) {
		try {
			String to[] = new String[1];
			String userFrom = "";
			if (isUpdate) {
				userFrom = pDefectWrapper.getUser();
			} else {
				userFrom = pDefectWrapper.getRecipient();
			}
			Vector<UserWrapper> listOfUser = currentProject
					.getAllUsersWrapper();
			boolean trouve = false;
			int index = 0;
			int taille = listOfUser.size();
			while (!trouve && index < taille) {
				UserWrapper pUserWrapper = listOfUser.elementAt(index);
				if (pUserWrapper.getLogin().equals(userFrom)) {
					to[0] = pUserWrapper.getEmail();
					trouve = true;
				}
				index++;
			}
			if (trouve == true && !to[0].equals("")) {
				// String recipients[ ], String subject, String message , String
				// from
				Mail.postMail(to, "[" + currentProject.getNameFromModel()
						+ "] Mantis defect " + pDefectWrapper.getId() + " is "
						+ action, pDefectWrapper.getResume() + "\n"
						+ pDefectWrapper.getDescription(), currentUser
						.getEmailFromModel());
			}
		} catch (Exception e) {

		}
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void modifyBug(Attachment pAttach, String long_desc,
			String assigned_to, String url_attach, String bug_severity,
			String bug_satus, String short_desc, String bug_OS,
			String bug_priority, String bug_platform, String bug_env,
			String bug_reproducibility, String bug_resolution) throws Exception {

		if (access_level <= 10) {
			JOptionPane.showMessageDialog(SalomeTMFContext.getInstance()
					.getSalomeFrame(), Language.getInstance().getText(
					"Access_level_insuffisant"), "Error!",
					JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}
		int id = getIDofBug(pAttach);
		if (!(bugProjectID > 0) || id < 1) {
			JOptionPane
					.showMessageDialog(SalomeTMFContext.getInstance()
							.getSalomeFrame(), Language.getInstance().getText(
							"Must_create_project"), "Error!",
							JOptionPane.ERROR_MESSAGE);
			throw new Exception("[Mantis] Project not found");
		}

		int id_assigned_to = -1;
		try {
			id_assigned_to = pMantisConnector.getUserIDInProject(assigned_to,
					bugProjectID);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (id_assigned_to == -1) {
			id_assigned_to = bugUsrID;
			assigned_to = currentUser.getLoginFromModel();
		}

		// int id_assigned_to = bugUsrID;

		int id_bug_severity = ((Integer) severityByValues.get(bug_severity))
				.intValue();
		int id_bug_satus = ((Integer) statusByValues.get(bug_satus)).intValue();
		int id_bug_priority = ((Integer) priorityByValues.get(bug_priority))
				.intValue();
		int id_bug_reproducibility = ((Integer) reproductibilityByValues
				.get(bug_reproducibility)).intValue();
		int id_bug_resolution = ((Integer) resolutionByValues
				.get(bug_resolution)).intValue();

		pMantisConnector.updateDefect(bugUsrID, bugProjectID, id,
				id_assigned_to, long_desc, id_bug_severity, id_bug_satus,
				short_desc, bug_OS, id_bug_priority, bug_platform, bug_env,
				id_bug_reproducibility, id_bug_resolution);
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void showBug(Attachment attach) {
		int bug_id = getIDofBug(attach);
		if (bug_id < 1) {
			showBugInBugTracker(attach);
		}
		/*
		 * String environement; //OK String user; //OK String plateforme; //OK
		 * String os; //OK String priority; //OK String severity; //OK String
		 * status; //OK String reproducibility; String resolution; String
		 * recipient; String url =""; //NONE String resume; //OK String
		 * description; //OK
		 */
		try {
			Integer id = new Integer(bug_id);
			DefectWrapper pDefectWrapper = null;
			if (defectsCache != null) {
				pDefectWrapper = (DefectWrapper) defectsCache.get(id);
			}
			if (pDefectWrapper == null) {
				defectsCache = pMantisConnector.getProjectDefects(bugProjectID);
				pDefectWrapper = (DefectWrapper) defectsCache.get(id);
			}
			new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(),
					pDefectWrapper, this, true);
			/*
			 * DefectWrapper pDefectWrapper=
			 * pMantisConnector.getDefectInfo(bug_id); environement =
			 * pDefectWrapper.getEnvironement(); user =
			 * pDefectWrapper.getUser(); plateforme =
			 * pDefectWrapper.getPlateforme(); os = pDefectWrapper.getOs();
			 * priority = getBugPriority(new
			 * Integer(pDefectWrapper.getPriority())); severity =
			 * getBugSeverity(new Integer(pDefectWrapper.getSeverity())); status
			 * = getBugStatus(new Integer(pDefectWrapper.getStatus()));
			 * reproducibility = getBugReproducibility(new
			 * Integer(pDefectWrapper.getReproducibility())); resolution =
			 * getBugResolution(new Integer(pDefectWrapper.getResolution()));
			 * resume = pDefectWrapper.getResume(); description =
			 * pDefectWrapper.getDescription(); recipient =
			 * pDefectWrapper.getRecipient(); //new AskNewBug(this, true,
			 * attach, environement, user, plateforme, os, priority, severity,
			 * status,reproducibility,resolution, recipient, url, resume,
			 * description);
			 */
		} catch (Exception e) {
			e.printStackTrace();
			showBugInBugTracker(attach);
		}
	}

	/**
	 * Return the bug ID of the defect representing by pAttachment
	 *
	 * @param pAttachment
	 * @return
	 */
	public String getBugID(Attachment pAttachment) {
		String res = pAttachment.getNameFromModel();
		int debut = res.lastIndexOf("bug_id=");
		if (debut > 0) {
			int fin = res.indexOf("&", debut);
			if (fin > 0) {
				try {
					res = res.substring(debut + 7, fin);
					int bugID = Integer.parseInt(res);
					DefectWrapper pDefectWrapper = getDefectInfo(bugID, false);
					if (pDefectWrapper == null) {
						res = null;
					}
				} catch (Exception e) {

				}
			}
		}

		return res;
	}

	int getIDofBug(Attachment pAttachment) {
		int id = -1;

		String res = pAttachment.getNameFromModel();
		String desc = pAttachment.getDescriptionFromModel();
		if (!desc.equals(ATTACH_DESC)) {
			return id;
		}
		int debut = res.lastIndexOf("bug_id=");
		if (debut > 0) {
			int fin = res.indexOf("&", debut);
			if (fin > 0) {
				try {
					res = res.substring(debut + 7, fin).trim();
					id = Integer.parseInt(res);
				} catch (Exception e) {
				}
			}
		}

		return id;
	}

	UrlAttachment makeAttachement(int bug_id) {
		String url_res = mantis_host + "view_salome_bug_page.php?project_id="
				+ URLEncoder.encode("" + bugProjectID) + "&bug_id="
				+ URLEncoder.encode("" + bug_id) + "&user_id="
				+ URLEncoder.encode("" + bugUsrID);
		return new UrlAttachment(url_res, MantisPlugin.ATTACH_DESC);
	}

	public void showBugInBugTracker(Attachment attach) {
		if (attach == null || !(attach instanceof UrlAttachment))
			return;
		try {
			URL newUrl = new URL(attach.getNameFromModel());
			SalomeTMFContext.getBaseIHM().showDocument(newUrl,
					getBugTrackerName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public void showEnvironmentBugs(Environment env) {
		if (!(bugProjectID > 0)) {
			return;
		}
		String url_res = mantis_host + "view_salome_bug_page.php?project_id="
				+ URLEncoder.encode("" + bugProjectID) + "&version_name="
				+ URLEncoder.encode(env.getNameFromModel()) + "&user_id="
				+ URLEncoder.encode("" + bugUsrID);
		try {
			// applet.getAppletContext().showDocument(new URL(url_res),
			// getBugTrackerName());
			SalomeTMFContext.getBaseIHM().showDocument(new URL(url_res),
					getBugTrackerName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Vector getBugHistory(DefectWrapper pDefectWrapper) throws Exception {
		Vector bugHistories = pMantisConnector.getBugHistory(bugProjectID,
				pDefectWrapper.getId());
		return bugHistories;
	}

	Hashtable getDefectLink(DefectWrapper pDefectWrapper) throws Exception {
		Hashtable defectsLinked;
		defectsLinked = pMantisConnector.getDefectLink(bugProjectID,
				pDefectWrapper.getId());
		if (defectsLinked == null) {
			return new Hashtable();
		}
		return defectsLinked;
	}

	void addDefectLink(DefectWrapper pDefectSource,
			DefectWrapper pDefectDestination) throws Exception {
		pMantisConnector.addDefectLink(bugUsrID, pDefectSource.getId(),
				pDefectDestination.getId());
	}

	void deleteDefectLink(DefectWrapper pDefectSource,
			DefectWrapper pDefectDestination) throws Exception {
		pMantisConnector.deleteDefectLink(bugUsrID, pDefectSource.getId(),
				pDefectDestination.getId());
	}

	void deleteDefect(DefectWrapper pDefectSource) throws Exception {
		pMantisConnector.deleteDefect(bugProjectID, pDefectSource.getId());

		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec
						.getExecutionResultListFromModel();
				for (int k = 0; k < listOfExecutionRes.size(); k++) {
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
							.get(k);
					Test[] tests = pExecutionResult.getTestOrderedFromModel();
					for (int l = 0; l < tests.length; l++) {
						ExecutionTestResult pExecutionTestResult = pExecutionResult
								.getExecutionTestResultFromModel(tests[l]);
						deleteTestLinkDefect(pExecutionTestResult,
								pDefectSource);
					}
				}
			}
		}
		sendMail(pDefectSource, "deleted", true);
	}

	Hashtable getDefectsOfProject(boolean forceReload) throws Exception {
		if (forceReload == false && defectsCache != null) {
			return defectsCache;
		}
		defectsCache = pMantisConnector.getProjectDefects(bugProjectID);
		return defectsCache;
	}

	DefectWrapper getDefectInfo(int id_bug, boolean forceReload)
			throws Exception {
		DefectWrapper pDefectWrapper = null;
		if (id_bug < 0) {
			return pDefectWrapper;
		}
		Integer id = new Integer(id_bug);
		if (forceReload == false && defectsCache != null) {
			pDefectWrapper = (DefectWrapper) defectsCache.get(id);

		}
		if (pDefectWrapper == null) {
			getDefectsOfProject(true);
			pDefectWrapper = (DefectWrapper) defectsCache.get(id);
		}
		return pDefectWrapper;
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public int getEnvNbBugs(Environment env, boolean onlyCorretedBugs) {
		int res = 0;
		if ((!cfg_prop.containsKey("MAJOR_BUG_LIST"))
				|| ((!cfg_prop.containsKey("CLOSED_BUG_LIST")))) {
			res = getEnvironmentNbBugs(env, CRITICAL, onlyCorretedBugs)
					+ getEnvironmentNbBugs(env, MAJOR, onlyCorretedBugs);
		} else {
			try {
				Hashtable allDefectsByID = pMantisConnector
						.getProjectDefects(bugProjectID);
				Enumeration allDefects = allDefectsByID.elements();
				String matchEnvName = env.getNameFromModel().trim();
				String major_list = cfg_prop.getProperty("MAJOR_BUG_LIST");
				String[] major_members = major_list.split("[,]");
				String closed_list = cfg_prop.getProperty("CLOSED_BUG_LIST");
				String[] closed_members = closed_list.split("[,]");
				while (allDefects.hasMoreElements()) {
					DefectWrapper pDefectWrapper = (DefectWrapper) allDefects
							.nextElement();
					String envName = pDefectWrapper.getEnvironement().trim();
					if (envName.equals(matchEnvName)) {
						int severity = pDefectWrapper.getSeverity();
						boolean matchSeverity = false;
						int i = 0;
						while (!matchSeverity && i < major_members.length) {
							int major_int = Integer.parseInt(major_members[i]
									.trim());
							if (severity == major_int) {
								matchSeverity = true;
							}
							i++;
						}
						if (matchSeverity) {
							if (!onlyCorretedBugs) {
								res++;
							} else {
								boolean isCorected = false;
								int correction = pDefectWrapper.getStatus();
								int j = 0;
								while (!isCorected && i < closed_members.length) {
									int close_int = Integer
											.parseInt(closed_members[j].trim());
									if (correction == close_int) {
										isCorected = true;
										res++;
									}
									j++;
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		/*
		 * try { String major_list = cfg_prop.getProperty("MAJOR_BUG_LIST");
		 * String[] major_members = major_list.split("[,]"); for (int i=0 ;
		 * i<major_members.length ; i++) { int major_int =
		 * Integer.parseInt(major_members[i].trim()); ResultSet stmtRes;
		 * PreparedStatement prep; if (!onlyCorretedBugs) { prep =
		 * iDB.prepareStatement(sql_prop.getProperty("SelectBugsBySeverity"));
		 * prep.setInt(1,major_int); prep.setString(2,env.getNameFromModel());
		 * prep.setInt(3,bugProjectID); stmtRes = prep.executeQuery();
		 * while(stmtRes.next()) { res++; } } else { String closed_list =
		 * cfg_prop.getProperty("CLOSED_BUG_LIST"); String[] closed_members =
		 * closed_list.split("[,]"); for (int j=0 ; j<closed_members.length ;
		 * j++) { int closed_int = Integer.parseInt(closed_members[j].trim());
		 * prep =
		 * iDB.prepareStatement(sql_prop.getProperty("SelectCorretedBugsBySeverity"
		 * )); prep.setInt(1,closed_int); prep.setInt(2,major_int);
		 * prep.setString(3,env.getNameFromModel());
		 * prep.setInt(4,bugProjectID); stmtRes = prep.executeQuery();
		 * while(stmtRes.next()) { res++; } } }
		 *
		 * } } catch (Exception e) { e.printStackTrace(); } }
		 */
		return res;
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public int getEnvironmentNbBugs(Environment env, int severity,
			boolean onlyCorretedBugs) {
		int res = 0;
		int severity_value = 0;

		Vector severities_int = new Vector(severityByValues.values());
		int critical = 80;
		int major = 70;
		int major_tmp = 70;
		int closed = DEFAULT_CORRECTED_VALUE;

		if (cfg_prop.containsKey("BUG_CRITICAL_INT")) {
			critical = Integer.parseInt(cfg_prop
					.getProperty("BUG_CRITICAL_INT"));
		} else {
			critical = ((Integer) severities_int.elementAt(0)).intValue();
			major_tmp = ((Integer) severities_int.elementAt(0)).intValue();
			for (int i = 1; i < severities_int.size(); i++) {
				int tmp = ((Integer) severities_int.elementAt(i)).intValue();
				if (critical < tmp) {
					major_tmp = critical;
					critical = tmp;
				}
			}
		}

		if (cfg_prop.containsKey("BUG_MAJOR_INT")) {
			major = Integer.parseInt(cfg_prop.getProperty("BUG_MAJOR_INT"));
		} else {
			major = major_tmp;
		}

		Util.log("[MantisPlugin]getEnvironmentNbBugs() critical = " + critical
				+ " and major = " + major);

		if (severity == CRITICAL) {
			severity_value = critical;
		} else if (severity == MAJOR) {
			severity_value = major;
		}

		if (cfg_prop.containsKey("BUG_CLOSED_INT")) {
			closed = Integer.parseInt(cfg_prop.getProperty("BUG_CLOSED_INT"));
		}

		try {
			Hashtable allDefectsByID = pMantisConnector
					.getProjectDefects(bugProjectID);
			Enumeration allDefects = allDefectsByID.elements();
			String matchEnvName = env.getNameFromModel().trim();
			while (allDefects.hasMoreElements()) {
				DefectWrapper pDefectWrapper = (DefectWrapper) allDefects
						.nextElement();
				String envName = pDefectWrapper.getEnvironement().trim();
				if (envName.equals(matchEnvName)) {
					int severity_v = pDefectWrapper.getSeverity();
					// boolean matchSeverity = false;
					// int i = 0;
					if (severity_v == severity_value) {
						if (!onlyCorretedBugs) {
							res++;
						} else {
							// boolean isCorected = false;
							int correction = pDefectWrapper.getStatus();
							if (closed == correction) {
								res++;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		/*
		 * try{ ResultSet stmtRes; PreparedStatement prep; if (onlyCorretedBugs)
		 * { prep =
		 * iDB.prepareStatement(sql_prop.getProperty("SelectCorretedBugsBySeverity"
		 * )); prep.setInt(1,closed); prep.setInt(2,severity_value);
		 * prep.setString(3,env.getNameFromModel());
		 * prep.setInt(4,bugProjectID); stmtRes = prep.executeQuery();
		 * while(stmtRes.next()) { res++; } } else { prep =
		 * iDB.prepareStatement(sql_prop.getProperty("SelectBugsBySeverity"));
		 * prep.setInt(1,severity_value);
		 * prep.setString(2,env.getNameFromModel());
		 * prep.setInt(3,bugProjectID); stmtRes = prep.executeQuery();
		 * while(stmtRes.next()) { res++; } } }catch(Exception E){
		 * E.printStackTrace(); }
		 */

		return res;
	}

	public Vector getBugTrackerAllUsers() {
		return pMantisConnector.getBugTrackerAllUsers(bugProjectID);
	}

	/**
	 * @see org.objectweb.salome_tmf.plugins.core.BugTracker
	 */
	public String getBugTrackerName() {
		return MANTIS_SUB_MENU;
	}

	/**
	 * Get bug tracker attachment description
	 *
	 * @return bug tracker name
	 */
	public String getBugTrackerAttachDesc() {
		return ATTACH_DESC;
	}

	public void update(Observable o, Object arg) {
		try {
			if (arg instanceof SalomeEvent) {
				SalomeEvent pSalomeEvent = (SalomeEvent) arg;
				int code = pSalomeEvent.getCode();
				if (code == ApiConstants.INSERT_ENVIRONMENT) {
					Environment env = (Environment) pSalomeEvent.getArg();
					Util
							.log("[MantisPlugin]dispach event INSERT_ENVIRONMENT env = "
									+ env.getNameFromModel());
					if (!pMantisConnector.isExistEnv(bugProjectID, env
							.getNameFromModel())) {
						addEnvironment(env);
					}
				} else if (code == ApiConstants.UPDATE_ENVIRONMENT) {
					String oldName = (String) pSalomeEvent.getOldValue();
					String newName = (String) pSalomeEvent.getNewValue();
					Util
							.log("[MantisPlugin]dispach event UPDATE_ENVIRONMENT old env = "
									+ oldName + " - new env = " + newName);
					if (pMantisConnector.isExistEnv(bugProjectID, oldName))
						updateEnvironment(oldName, newName, "[SALOME_ENV]");
				} else if (code == ApiConstants.DELETE_ENVIRONMENT) {
					Environment env = (Environment) pSalomeEvent.getArg();
					Util
							.log("[MantisPlugin]dispach event DELETE_ENVIRONMENT env = "
									+ env.getNameFromModel());
					if (pMantisConnector.isExistEnv(bugProjectID, env
							.getNameFromModel()))
						deleteEnvironment(env.getNameFromModel());
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public Vector getBugOSList() {
		return null;
	}

	public Vector getBugPriorityList() {
		Vector list = new Vector();

		for (Enumeration e = priorityByValues.keys(); e.hasMoreElements();) {
			list.add((String) e.nextElement());
		}

		return list;
	}

	String getBugPriority(Integer i) {
		String res = null;

		for (Enumeration e = priorityByValues.keys(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			Integer value = (Integer) priorityByValues.get(key);
			if (value.equals(i)) {
				return key;
			}
		}
		return res;
	}

	public Vector getBugPlateformList() {
		return null;
	}

	public Vector getBugSeverityList() {
		Vector list = new Vector();

		for (Enumeration e = severityByValues.keys(); e.hasMoreElements();) {
			list.add((String) e.nextElement());
		}

		return list;
	}

	String getBugSeverity(Integer i) {
		String res = null;

		for (Enumeration e = severityByValues.keys(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			Integer value = (Integer) severityByValues.get(key);
			if (value.equals(i)) {
				return key;
			}
		}
		return res;
	}

	public Vector getBugTrackerStatusList() {
		Vector list = new Vector();

		for (Enumeration e = statusByValues.keys(); e.hasMoreElements();) {
			list.add((String) e.nextElement());
		}

		return list;
	}

	String getBugStatus(Integer i) {
		String res = null;

		for (Enumeration e = statusByValues.keys(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			Integer value = (Integer) statusByValues.get(key);
			if (value.equals(i)) {
				return key;
			}
		}
		return res;
	}

	String getBugReproducibility(Integer i) {
		String res = null;

		for (Enumeration e = reproductibilityByValues.keys(); e
				.hasMoreElements();) {
			String key = (String) e.nextElement();
			Integer value = (Integer) reproductibilityByValues.get(key);
			if (value.equals(i)) {
				return key;
			}
		}
		return res;
	}

	String getBugResolution(Integer i) {
		String res = null;

		for (Enumeration e = resolutionByValues.keys(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			Integer value = (Integer) resolutionByValues.get(key);
			if (value.equals(i)) {
				return key;
			}
		}
		return res;
	}

	public boolean isEditableOS() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isEditablePlateForme() {
		// TODO Auto-generated method stub
		return true;
	}

	private void initMantisFields() {
		// init priority list
		priorityByValues = new Hashtable();
		priorityByID = new Hashtable();
		priority_values = new Vector();
		initMantisPriorities();

		// init severity list
		severityByValues = new Hashtable();
		severityByID = new Hashtable();
		severity_values = new Vector();
		initMantisSeverities();

		// init status list
		statusByValues = new Hashtable();
		statusByID = new Hashtable();
		status_values = new Vector();
		statusColorTable = new HashMap();
		initMantisStatus();

		// init reproductibility list
		reproductibilityByValues = new Hashtable();
		reproductibilityByID = new Hashtable();
		reproductibility_values = new Vector();
		initMantisReproductibilities();

		// init resolution list
		resolutionByValues = new Hashtable();
		resolutionByID = new Hashtable();
		resolution_values = new Vector();
		initMantisResolutions();

	}

	private void initMantisPriorities() {
		if (priorityByValues == null) {
			priorityByValues = new Hashtable();
			priorityByID = new Hashtable();
			priority_values = new Vector();
		} else {
			priorityByValues.clear();
			priorityByID.clear();
			priority_values.clear();
		}
		if (cfg_prop.containsKey("MANTIS_PRIORITIES_LIST")) {
			try {
				String list = cfg_prop.getProperty("MANTIS_PRIORITIES_LIST");
				String[] members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						priorityByValues.put(parts[1], Integer
								.valueOf(parts[0]));
						priorityByID.put(Integer.valueOf(parts[0]), parts[1]);
						priority_values.add(parts[1]);
					} else {
						initDefaultPriorities();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			initDefaultPriorities();
		}
	}

	private void initDefaultPriorities() {
		if (priorityByValues == null) {
			priorityByValues = new Hashtable();
			priorityByID = new Hashtable();
			priority_values = new Vector();
		} else {
			priorityByValues = new Hashtable();
			priorityByID = new Hashtable();
			priority_values = new Vector();
		}
		priorityByValues.put("none", new Integer(10));
		priorityByValues.put("low", new Integer(20));
		priorityByValues.put("normal", new Integer(30));
		priorityByValues.put("high", new Integer(40));
		priorityByValues.put("urgent", new Integer(50));
		priorityByValues.put("immediate", new Integer(60));

		priorityByID.put(new Integer(10), "none");
		priorityByID.put(new Integer(20), "low");
		priorityByID.put(new Integer(30), "normal");
		priorityByID.put(new Integer(40), "high");
		priorityByID.put(new Integer(50), "urgent");
		priorityByID.put(new Integer(60), "immediate");

		priority_values.add("none");
		priority_values.add("low");
		priority_values.add("normal");
		priority_values.add("high");
		priority_values.add("urgent");
		priority_values.add("immediate");
	}

	private void initMantisSeverities() {
		if (severityByValues == null) {
			severityByValues = new Hashtable();
			severityByID = new Hashtable();
			severity_values = new Vector();
		}
		severityByValues.clear();
		severityByID.clear();
		severity_values.clear();
		if (cfg_prop.containsKey("MANTIS_SEVERITIES_LIST")) {
			try {
				String list = cfg_prop.getProperty("MANTIS_SEVERITIES_LIST");
				String[] members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						severityByValues.put(parts[1], Integer
								.valueOf(parts[0]));
						severityByID.put(Integer.valueOf(parts[0]), parts[1]);
						severity_values.add(parts[1]);
					} else {
						initDefaultSeverities();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			initDefaultSeverities();
		}
	}

	private void initDefaultSeverities() {
		if (severityByValues == null) {
			severityByValues = new Hashtable();
			severityByID = new Hashtable();
			severity_values = new Vector();
		} else {
			severityByValues.clear();
			severityByID.clear();
			severity_values.clear();
		}
		severityByValues.put("feature", new Integer(10));
		severityByValues.put("trivial", new Integer(20));
		severityByValues.put("text", new Integer(30));
		severityByValues.put("tweak", new Integer(40));
		severityByValues.put("minor", new Integer(50));
		severityByValues.put("major", new Integer(60));
		severityByValues.put("crash", new Integer(70));
		severityByValues.put("block", new Integer(80));

		severityByID.put(new Integer(10), "feature");
		severityByID.put(new Integer(20), "trivial");
		severityByID.put(new Integer(30), "text");
		severityByID.put(new Integer(40), "tweak");
		severityByID.put(new Integer(50), "minor");
		severityByID.put(new Integer(60), "major");
		severityByID.put(new Integer(70), "crash");
		severityByID.put(new Integer(80), "block");

		severity_values.add("feature");
		severity_values.add("trivial");
		severity_values.add("text");
		severity_values.add("tweak");
		severity_values.add("minor");
		severity_values.add("major");
		severity_values.add("crash");
		severity_values.add("block");
	}

	private void initMantisStatus() {
		if (statusByValues == null) {
			statusByValues = new Hashtable();
			statusByID = new Hashtable();
			status_values = new Vector();
			statusColorTable = new HashMap();
		} else {
			statusByValues.clear();
			statusByID.clear();
			status_values.clear();
			statusColorTable.clear();
		}
		if (cfg_prop.containsKey("MANTIS_STATUS_LIST")
				&& cfg_prop.containsKey("MANTIS_STATUS_COLOR_LIST")) {
			try {
				String list = cfg_prop.getProperty("MANTIS_STATUS_LIST");
				String[] members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						statusByValues.put(parts[1], Integer.valueOf(parts[0]));
						statusByID.put(Integer.valueOf(parts[0]), parts[1]);
						status_values.add(parts[1]);
					} else {
						initDefaultStatus();
					}
				}
				list = cfg_prop.getProperty("MANTIS_STATUS_COLOR_LIST");
				members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						statusColorTable.put(parts[0], Color.decode(parts[1]));
					} else {
						initDefaultStatus();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				initDefaultStatus();
			}
		} else {
			initDefaultStatus();
		}
	}

	private void initDefaultStatus() {
		if (statusByValues == null) {
			statusByValues = new Hashtable();
			statusByID = new Hashtable();
			status_values = new Vector();
			statusColorTable = new HashMap();
		} else {
			statusByValues.clear();
			statusByID.clear();
			status_values.clear();
			statusColorTable.clear();
		}
		statusByValues.put("new", new Integer(10));
		statusByValues.put("feedback", new Integer(20));
		statusByValues.put("acknowledged", new Integer(30));
		statusByValues.put("confirmed", new Integer(40));
		statusByValues.put("assigned", new Integer(50));
		statusByValues.put("resolved", new Integer(60));
		statusByValues.put("closed", new Integer(70));

		statusByID.put(new Integer(10), "new");
		statusByID.put(new Integer(20), "feedback");
		statusByID.put(new Integer(30), "acknowledged");
		statusByID.put(new Integer(40), "confirmed");
		statusByID.put(new Integer(50), "assigned");
		statusByID.put(new Integer(60), "resolved");
		statusByID.put(new Integer(70), "closed");

		status_values.add("new");
		status_values.add("feedback");
		status_values.add("acknowledged");
		status_values.add("confirmed");
		status_values.add("assigned");
		status_values.add("resolved");
		status_values.add("closed");

		statusColorTable.put("new", new Color(255, 160, 160));
		statusColorTable.put("feedback", new Color(255, 80, 168));
		statusColorTable.put("acknowledged", new Color(255, 216, 80));
		statusColorTable.put("confirmed", new Color(255, 255, 176));
		statusColorTable.put("assigned", new Color(200, 200, 255));
		statusColorTable.put("resolved", new Color(204, 238, 221));
		statusColorTable.put("closed", new Color(232, 232, 232));
	}

	private void initMantisReproductibilities() {
		if (reproductibilityByValues == null) {
			reproductibilityByValues = new Hashtable();
			reproductibilityByID = new Hashtable();
			reproductibility_values = new Vector();
		} else {
			reproductibilityByValues.clear();
			reproductibilityByID.clear();
			reproductibility_values.clear();
		}

		reproductibilityByValues = new Hashtable();
		if (cfg_prop.containsKey("MANTIS_REPRODUCTIBILITIES_LIST")) {
			try {
				String list = cfg_prop
						.getProperty("MANTIS_REPRODUCTIBILITIES_LIST");
				String[] members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						reproductibilityByValues.put(parts[1], Integer
								.valueOf(parts[0]));
						reproductibilityByID.put(Integer.valueOf(parts[0]),
								parts[1]);
						reproductibility_values.add(parts[1]);
					} else {
						initDefaultReproductibilities();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			initDefaultReproductibilities();
		}
	}

	private void initDefaultReproductibilities() {
		if (reproductibilityByValues == null) {
			reproductibilityByValues = new Hashtable();
			reproductibilityByID = new Hashtable();
			reproductibility_values = new Vector();
		} else {
			reproductibilityByValues.clear();
			reproductibilityByID.clear();
			reproductibility_values.clear();
		}
		reproductibilityByValues.put("always", new Integer(10));
		reproductibilityByValues.put("sometimes", new Integer(30));
		reproductibilityByValues.put("random", new Integer(50));
		reproductibilityByValues.put("have not tried", new Integer(70));
		reproductibilityByValues.put("unable to duplicate", new Integer(90));
		reproductibilityByValues.put("N/A", new Integer(100));

		reproductibilityByID.put(new Integer(10), "always");
		reproductibilityByID.put(new Integer(30), "sometimes");
		reproductibilityByID.put(new Integer(50), "random");
		reproductibilityByID.put(new Integer(70), "have not tried");
		reproductibilityByID.put(new Integer(90), "unable to duplicate");
		reproductibilityByID.put(new Integer(100), "N/A");

		reproductibility_values.add("always");
		reproductibility_values.add("sometimes");
		reproductibility_values.add("random");
		reproductibility_values.add("have not tried");
		reproductibility_values.add("unable to duplicate");
		reproductibility_values.add("N/A");
	}

	private void initMantisResolutions() {
		if (resolutionByValues == null) {
			resolutionByValues = new Hashtable();
			resolutionByID = new Hashtable();
			resolution_values = new Vector();
		} else {
			resolutionByValues.clear();
			resolutionByID.clear();
			resolution_values.clear();
		}
		if (cfg_prop.containsKey("MANTIS_RESOLUTIONS_LIST")) {
			try {
				String list = cfg_prop.getProperty("MANTIS_RESOLUTIONS_LIST");
				String[] members = list.split("[,]");
				for (int i = 0; i < members.length; i++) {
					String member = members[i];
					String[] parts = member.split("[:]");
					if (parts.length == 2) {
						resolutionByValues.put(parts[1], Integer
								.valueOf(parts[0]));
						resolutionByID.put(Integer.valueOf(parts[0]), parts[1]);
						resolution_values.add(parts[1]);
					} else {
						initDefaultResolutions();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			initDefaultResolutions();
		}
	}

	private void initDefaultResolutions() {
		if (resolutionByValues == null) {
			resolutionByValues = new Hashtable();
			resolutionByID = new Hashtable();
			resolution_values = new Vector();
		} else {
			resolutionByValues.clear();
			resolutionByID.clear();
			resolution_values.clear();
		}
		resolutionByValues.put("open", new Integer(10));
		resolutionByValues.put("fixed", new Integer(20));
		resolutionByValues.put("reopened", new Integer(30));
		resolutionByValues.put("unable to duplicate", new Integer(40));
		resolutionByValues.put("not fixable", new Integer(50));
		resolutionByValues.put("duplicate", new Integer(60));
		resolutionByValues.put("not a bug", new Integer(70));
		resolutionByValues.put("suspended", new Integer(80));
		resolutionByValues.put("wont fix", new Integer(90));

		resolutionByID.put(new Integer(10), "open");
		resolutionByID.put(new Integer(20), "fixed");
		resolutionByID.put(new Integer(30), "reopened");
		resolutionByID.put(new Integer(40), "unable to duplicate");
		resolutionByID.put(new Integer(50), "not fixable");
		resolutionByID.put(new Integer(60), "duplicate");
		resolutionByID.put(new Integer(70), "not a bug");
		resolutionByID.put(new Integer(80), "suspended");
		resolutionByID.put(new Integer(90), "wont fix");

		resolution_values.add("open");
		resolution_values.add("fixed");
		resolution_values.add("reopened");
		resolution_values.add("unable to duplicate");
		resolution_values.add("not fixable");
		resolution_values.add("duplicate");
		resolution_values.add("not a bug");
		resolution_values.add("suspended");
		resolution_values.add("wont fix");
	}

	public Vector getBugTrackerReproductibilityList() {
		Vector list = new Vector();

		for (Enumeration e = reproductibilityByValues.keys(); e
				.hasMoreElements();) {
			list.add((String) e.nextElement());
		}

		return list;
	}

	public Vector getBugTrackerResolutionList() {
		Vector list = new Vector();

		for (Enumeration e = resolutionByValues.keys(); e.hasMoreElements();) {
			list.add((String) e.nextElement());
		}

		return list;
	}

	public boolean isUsesBugReproducibily() {
		return true;
	}

	public boolean isUsesBugResolution() {
		// TODO Auto-generated method stub
		return true;
	}

	public String getAdditionalBugDesc() {
		String res = "";
		if (cfg_prop.containsKey("BUG_DESC_ADD_FIELDS")) {
			String fieldsList = cfg_prop.getProperty("BUG_DESC_ADD_FIELDS");
			String[] fields = fieldsList.split("[,]");
			for (int i = 0; i < fields.length; i++) {
				String field = fields[i].trim();
				if (cfg_prop.containsKey(field)) {
					res += "\n- " + cfg_prop.getProperty(field) + " : ";
				}
			}
		}
		return res;
	}

	// / LIVE ////

	public void suspend() {
		try {
			pMantisConnector.suspend();
			// iDB.close();
			// iDB = null;
			suspended = true;
		} catch (Exception e) {

		}
	}

	/*
	 * public void resume() { ISQLObjectFactory iSQL_OF =
	 * Api.getISQLObjectFactory(); iDB = iSQL_OF.getInstanceOfDataBase(driver);
	 * iDB.open(url_db,user,pwd); SQLUtils.initSQLUtils(iDB); }
	 */

	public JPanel getStateColorPanel() {
		int size = pDefectPanel1.choiceState.size();
		JPanel statePanel = new JPanel();
		statePanel.setLayout(new GridLayout(1, size - 1));
		for (int i = 1; i < size; i++) {
			String state = (String) pDefectPanel1.choiceState.elementAt(i);
			JLabel pLabel = new JLabel(state);
			Integer id = (Integer) statusByValues.get(state);
			Color pColor = (Color) pDefectPanel1.colorState.get(id);

			pLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
			pLabel.setBackground(pColor);
			// pLabel.setForeground(pColor);
			pLabel.setOpaque(true);
			statePanel.add(pLabel);
		}
		return statePanel;
	}

	public Color getStateDefect(Attachment pAttachment) {
		Color pColor = null;
		int id = getIDofBug(pAttachment);
		if (id >= 0) {
			try {
				DefectWrapper pDefectWrapper = getDefectInfo(id, false);
				if (pDefectWrapper != null) {
					int state = pDefectWrapper.getStatus();
					pColor = (Color) pDefectPanel1.colorState.get(new Integer(
							state));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return pColor;
	}

	public boolean isOpenDefect(Attachment pAttachment) {
		Color pColor = null;
		int id = getIDofBug(pAttachment);
		if (id >= 0) {
			try {
				DefectWrapper pDefectWrapper = getDefectInfo(id, false);
				if (pDefectWrapper != null) {
					int state = pDefectWrapper.getStatus();
					pColor = (Color) pDefectPanel1.colorState.get(new Integer(
							state));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (pColor.equals(new Color(232, 232, 232)))
			return false;
		return true;
	}

	public JPanel getDefectPanelForTests(Vector testSelected) {
		TestDefectActionPanel pTestDefectActionPanel = new TestDefectActionPanel(
				this);
		DefectPanel pTestDefectPanel = new DefectPanel(true, this,
				pTestDefectActionPanel, null);
		pTestDefectActionPanel.setDefectPanel(pTestDefectPanel);
		loadDefectTest(testSelected, pTestDefectPanel);
		return pTestDefectPanel;
	}

	/********************************* Common ********************************************/
	public void init(Object ihm) {

	}

	public boolean isActivableInTestToolsMenu() {
		return false;
	}

	public boolean isActivableInCampToolsMenu() {
		return false;
	}

	public boolean isActivableInDataToolsMenu() {
		return false;
	}

	public boolean usesOtherUIComponents() {
		return true;
	}

	public void activatePluginInTestToolsMenu(JMenu testToolsMenu) {
	}

	public void activatePluginInCampToolsMenu(JMenu campToolsMenu) {
	}

	public void activatePluginInDataToolsMenu(JMenu dataToolsMenu) {
	}

	public Vector getUsedUIComponents() {
		Vector uiComponentsUsed = new Vector();
		uiComponentsUsed.add(UICompCst.MAIN_TABBED_PANE);
		uiComponentsUsed.add(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS);
		uiComponentsUsed.add(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS);
		uiComponentsUsed.add(UICompCst.CAMPAIGN_WORKSPACE_PANEL_FOR_TABS);
		uiComponentsUsed.add(UICompCst.CAMP_EXECUTION_RESULTS_BUTTONS_PANEL);
		uiComponentsUsed
				.add(UICompCst.MANUAL_TEST_EXECUTION_RESULT_DETAILS_TAB);
		uiComponentsUsed.add(UICompCst.MANUAL_EXECUTION_TAB);
		uiComponentsUsed.add(UICompCst.TEST_DYNAMIC_TREE);
		return uiComponentsUsed;
	}

	public void activatePluginInStaticComponent(Integer uiCompCst) {
		if (uiCompCst.equals(UICompCst.MAIN_TABBED_PANE)) {
			salomeMainTabs = (JTabbedPane) SalomeTMFContext.getInstance()
					.getUIComponent(uiCompCst);
		} else if (uiCompCst
				.equals(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS)) {
			pMTestJTabbedPane = (JTabbedPane) SalomeTMFContext.getInstance()
					.getUIComponent(uiCompCst);
		} else if (uiCompCst
				.equals(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS)) {
			pATestJTabbedPane = (JTabbedPane) SalomeTMFContext.getInstance()
					.getUIComponent(uiCompCst);
		} else if (uiCompCst.equals(UICompCst.TEST_DYNAMIC_TREE)) {
			DynamicTree pDynamicTree = (DynamicTree) SalomeTMFContext
					.getInstance().getUIComponent(uiCompCst);
			setTestTree(pDynamicTree);
		}
	}

	public void activatePluginInDynamicComponent(Integer uiCompCst) {
		if (uiCompCst
				.equals(UICompCst.MANUAL_TEST_EXECUTION_RESULT_DETAILS_TAB)) {
			pJTabbedPane2 = (JTabbedPane) SalomeTMFContext.getInstance()
					.getUIComponent(uiCompCst);

			ExecResDefectActionPanel pExecResDefectActionPanel = new ExecResDefectActionPanel(
					this, true);
			pDefectPanel2 = new DefectPanel(true, this,
					pExecResDefectActionPanel, null);
			pExecResDefectActionPanel.setDefectPanel(pDefectPanel2);

			pJTabbedPane2.addTab("Plug-in Mantis", pDefectPanel2);
			pJTabbedPane2.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (pJTabbedPane2.getSelectedComponent().equals(
							pDefectPanel2)) {
						if (bugProjectID > 0) {

							/* make hashofdefect */
							ExecutionTestResult pExecutionTestResult = DataModel
									.getCurrentExecutionTestResult();
							// pExecResDefectActionPanel.setExecutionResult(pExecutionTestResult);

							Hashtable defectsWrapper = new Hashtable();
							if (pExecutionTestResult != null) {
								HashMap attacMap = pExecutionTestResult
										.getAttachmentMapFromModel();
								Set keysSet = attacMap.keySet();
								for (Iterator iter = keysSet.iterator(); iter
										.hasNext();) {
									try {
										Object attachName = iter.next();
										Attachment pAttach = (Attachment) attacMap
												.get(attachName);
										int bugid = getIDofBug(pAttach);
										DefectWrapper pDefectWrapper = null;

										if (bugid != -1) {
											// pDefectWrapper =
											// pMantisConnector.getDefectInfo(bugid);
											pDefectWrapper = getDefectInfo(
													bugid, false);
										}
										if (pDefectWrapper != null) {
											defectsWrapper.put(new Integer(
													bugid), pDefectWrapper);
										}
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								}
							}
							pDefectPanel2.loadData(defectsWrapper);
						}
						System.out.println("MANTIS");
					}
				}
			});
		} else if (uiCompCst.equals(UICompCst.MANUAL_EXECUTION_TAB)) {
			pJTabbedPane3 = (JTabbedPane) SalomeTMFContext.getInstance()
					.getUIComponent(uiCompCst);

			ExecResDefectActionPanel pExecResDefectActionPanel = new ExecResDefectActionPanel(
					this, true);
			pDefectPanel3 = new DefectPanel(true, this,
					pExecResDefectActionPanel, null);
			pExecResDefectActionPanel.setDefectPanel(pDefectPanel3);

			pJTabbedPane3.addTab("Plug-in Mantis", pDefectPanel3);
			pJTabbedPane3.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if (pJTabbedPane3.getSelectedComponent().equals(
							pDefectPanel3)) {
						if (bugProjectID > 0) {

							/* make hashofdefect */
							ExecutionTestResult pExecutionTestResult = DataModel
									.getCurrentExecutionTestResult();
							// pExecResDefectActionPanel.setExecutionResult(pExecutionTestResult);

							Hashtable defectsWrapper = new Hashtable();
							if (pExecutionTestResult != null) {
								HashMap attacMap = pExecutionTestResult
										.getAttachmentMapFromModel();
								Set keysSet = attacMap.keySet();
								for (Iterator iter = keysSet.iterator(); iter
										.hasNext();) {
									try {
										Object attachName = iter.next();
										Attachment pAttach = (Attachment) attacMap
												.get(attachName);
										int bugid = getIDofBug(pAttach);
										DefectWrapper pDefectWrapper = null;
										if (bugid != -1) {
											// pDefectWrapper =
											// pMantisConnector.getDefectInfo(bugid);
											pDefectWrapper = getDefectInfo(
													bugid, false);
										}
										if (pDefectWrapper != null) {
											defectsWrapper.put(new Integer(
													bugid), pDefectWrapper);
										}
									} catch (Exception ex) {
										ex.printStackTrace();
									}
								}
							}
							pDefectPanel3.loadData(defectsWrapper);
						}
						// System.out.println("MANTIS");
					}
				}
			});
		}

	}

	public boolean isFreezable() {
		return false;
	}

	public void freeze() {
	}

	public void unFreeze() {
	}

	public boolean isFreezed() {
		return false;
	}

	/**
	 * Called by Salome when all plugin are initialized and when Salome-tmf
	 * started
	 *
	 * @param commonExtensions
	 *            a ExtensionPoint of all plugin available of type common
	 * @param testDriverExtensions
	 *            a ExtensionPoint of all plugin available of type testDriver
	 * @param scriptEngineExtensions
	 *            a ExtensionPoint of all plugin available of type scriptEngine
	 * @param bugTrackerExtensions
	 *            a ExtensionPoint of all plugin available of type
	 *            bugTrackerEngine
	 */
	public void allPluginActived(ExtensionPoint commonExtensions,
			ExtensionPoint testDriverExtensions,
			ExtensionPoint scriptEngineExtensions,
			ExtensionPoint bugTrackerExtensions) {

	}

	public void allPluginActived(ExtensionPoint commonExtensions,
			ExtensionPoint testDriverExtensions,
			ExtensionPoint scriptEngineExtensions,
			ExtensionPoint bugTrackerExtensions,
			ExtensionPoint statistiquesExtensions) {

	}

	public void setTestTree(DynamicTree pDynamicTree) {
		salomeDynamicTree = pDynamicTree.getTree();
		salomeDynamicTree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode) salomeDynamicTree
						.getLastSelectedPathComponent();
				if (node == null)
					return;
				Object nodeInfo = node.getUserObject();
				/* React to the node selection. */
				if ((nodeInfo instanceof ManualTest)) {
					if (pMTestJTabbedPane != null) {
						if (pMTestJTabbedPane.getSelectedComponent().equals(
								pMTestDefectPanel)) {
							System.out.println("pMTestDefectPanel");
							loadDefectTest((Test) nodeInfo, pMTestDefectPanel);
						}
					}
				} else if ((nodeInfo instanceof AutomaticTest)) {
					if (pATestJTabbedPane != null) {
						if (pATestJTabbedPane.getSelectedComponent().equals(
								pATestDefectPanel)) {
							System.out.println("pATestDefectPanel");
							loadDefectTest((Test) nodeInfo, pATestDefectPanel);
						}
					}
				}
			}
		});
	}

	void loadDefectTest(Test pTest, DefectPanel pDefectPanel) {
		Hashtable pDefectWrappers = new Hashtable();
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec
						.getExecutionResultListFromModel();
				for (int k = 0; k < listOfExecutionRes.size(); k++) {
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
							.get(k);
					Test[] tests = pExecutionResult.getTestOrderedFromModel();
					for (int l = 0; l < tests.length; l++) {
						if (pTest.getIdBdd() == tests[l].getIdBdd()) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							addTestLinkDefect(pExecutionTestResult,
									pDefectWrappers);
						}
					}
				}
			}
		}
		pDefectPanel.loadData(pDefectWrappers);
	}

	void loadDefectTest(Vector pTests, DefectPanel pDefectPanel) {
		Hashtable pDefectWrappers = new Hashtable();
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec
						.getExecutionResultListFromModel();
				for (int k = 0; k < listOfExecutionRes.size(); k++) {
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
							.get(k);
					Test[] tests = pExecutionResult.getTestOrderedFromModel();
					for (int l = 0; l < tests.length; l++) {
						if (isFiltred(pTests, tests[l])) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							addTestLinkDefect(pExecutionTestResult,
									pDefectWrappers);
						}
					}
				}
			}
		}
		pDefectPanel.loadData(pDefectWrappers);
	}

	boolean isFiltred(Vector testsList, Test pTest) {
		int size = testsList.size();
		int i = 0;
		boolean filtred = false;
		while (!filtred && i < size) {
			Test ptempTest = (Test) testsList.elementAt(i);
			if (ptempTest.getIdBdd() == pTest.getIdBdd()) {
				filtred = true;
			}
			i++;
		}
		return filtred;
	}

	void addTestLinkDefect(ExecutionTestResult pExecutionTestResult,
			Hashtable pDefectWrappers) {
		// Hashtable defectsWrapper = new Hashtable();
		if (pExecutionTestResult != null) {
			HashMap attacMap = pExecutionTestResult.getAttachmentMapFromModel();
			Set keysSet = attacMap.keySet();
			for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
				try {
					Object attachName = iter.next();
					Attachment pAttach = (Attachment) attacMap.get(attachName);
					int bugid = getIDofBug(pAttach);
					DefectWrapper pDefectWrapper = getDefectInfo(bugid, false);
					if (pDefectWrapper != null) {
						pDefectWrappers.put(new Integer(bugid), pDefectWrapper);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	void deleteTestLinkDefect(ExecutionTestResult pExecutionTestResult,
			DefectWrapper pDefectSource) {

		if (pExecutionTestResult != null) {
			HashMap attacMap = pExecutionTestResult
					.getCopyOfAttachmentMapFromModel();
			Set keysSet = attacMap.keySet();
			for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
				try {
					Object attachName = iter.next();
					Attachment pAttach = (Attachment) attacMap.get(attachName);
					int bugid = getIDofBug(pAttach);
					DefectWrapper pDefectWrapper = getDefectInfo(bugid, false);
					if (pDefectWrapper != null && pDefectSource != null
							&& pDefectSource.getId() == pDefectWrapper.getId()) {
						pExecutionTestResult
								.deleteAttachementInDBAndModel(pAttach);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	/************************ Confirmation ***************************/

	boolean deleteConfirme(String quoi) {
		Object[] options = {
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Oui"),
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Non") };
		int choice = -1;

		choice = SalomeTMFContext.getInstance().askQuestion(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("confimation_suppression2")
						+ " " + quoi + " ?",
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Attention_"), JOptionPane.WARNING_MESSAGE,
				options);

		if (choice == JOptionPane.YES_OPTION) {
			return true;
		} else {
			return false;
		}
	}

	/********************** XML ******************************/
	/************** Option Panel *************/
	JCheckBox exportDefectBox;
	JPanel exportDefectPanel;

	public JPanel getExportOptionPanel() {
		exportDefectBox = new JCheckBox(Language.getInstance().getText(
				"Exporter_les_bugs"));
		exportDefectBox.setSelected(true);
		exportDefectPanel = new JPanel();
		exportDefectPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		exportDefectPanel.add(exportDefectBox);
		exportDefectPanel.setName("Mantis");
		return exportDefectPanel;
	}

	public JPanel getImportOptionPanel() {
		return null;
	}

	boolean doExport() {
		if (mandatoryExport)
			return true;
		if (exportDefectBox == null || !exportDefectBox.isSelected()) {
			return false;
		}
		return true;
	}

	/************** EXPORT *****************/

	void writeXMLDefect(Element defectsElem, DefectWrapper pDefectWrapper,
			XMLWriterPlugin pXMLWriter) {
		if (pDefectWrapper == null) {
			return;
		}
		Element defectElem = defectsElem.addElement("Defect");
		int bugID = pDefectWrapper.getId();
		defectElem.addAttribute("id", "Bug_" + bugID);
		defectElem.addAttribute("summary", ""
				+ pDefectWrapper.getResume().replaceAll("\n", "\\\\n"));
		defectElem.addAttribute("reporter", pDefectWrapper.getUser());
		defectElem.addAttribute("handler", pDefectWrapper.getRecipient());

		Element priorityElem = defectElem.addElement("Priority");
		priorityElem.addAttribute("id", "" + pDefectWrapper.getPriority());
		priorityElem.addAttribute("value", (String) priorityByID
				.get(new Integer(pDefectWrapper.getPriority())));

		Element severityElem = defectElem.addElement("Severity");
		severityElem.addAttribute("id", "" + pDefectWrapper.getSeverity());
		severityElem.addAttribute("value", (String) severityByID
				.get(new Integer(pDefectWrapper.getSeverity())));

		Element statusElem = defectElem.addElement("Status");
		statusElem.addAttribute("id", "" + pDefectWrapper.getStatus());
		statusElem.addAttribute("value", (String) statusByID.get(new Integer(
				pDefectWrapper.getStatus())));

		Element reproducibilityElem = defectElem.addElement("Reproducibility");
		reproducibilityElem.addAttribute("id", ""
				+ pDefectWrapper.getReproducibility());
		reproducibilityElem.addAttribute("value", (String) reproductibilityByID
				.get(new Integer(pDefectWrapper.getReproducibility())));

		Element resolutionElem = defectElem.addElement("Resolution");
		resolutionElem.addAttribute("id", "" + pDefectWrapper.getResolution());
		resolutionElem.addAttribute("value", (String) resolutionByID
				.get(new Integer(pDefectWrapper.getResolution())));

		Element infoElem = defectElem.addElement("Info");
		infoElem.addAttribute("environment", pDefectWrapper.getEnvironement());
		infoElem.addAttribute("platform", pDefectWrapper.getPlateforme());
		infoElem.addAttribute("os", pDefectWrapper.getOs());

		defectElem.addElement("Description").setText(
				pDefectWrapper.getDescription().replaceAll("\n", "\\\\n"));
		Element attachsElem = defectElem.addElement("MantisURL");
		String url_res = mantis_host + "view_salome_bug_page.php?project_id="
				+ URLEncoder.encode("" + bugProjectID) + "&bug_id="
				+ URLEncoder.encode("" + bugID) + "&user_id="
				+ URLEncoder.encode("" + bugUsrID);
		attachsElem.addAttribute("url", url_res);

		Element defectsRelations = defectElem.addElement("DefectRelations");
		try {
			Hashtable defectRelation = pMantisConnector.getDefectLink(
					bugProjectID, pDefectWrapper.getId());
			Enumeration defectRelationEnum = defectRelation.elements();
			while (defectRelationEnum.hasMoreElements()) {
				DefectWrapper pDefect = (DefectWrapper) defectRelationEnum
						.nextElement();
				Element defectsRelation = defectsRelations
						.addElement("DefectRelation");
				defectsRelation.addAttribute("ref", "Bug_" + pDefect.getId());
			}
		} catch (Exception e) {

		}
	}

	void writeXMLDefectLink(Element pElem, ExecutionResult pExecutionResult,
			ExecutionTestResult pExecTestRes, XMLWriterPlugin pXMLWriter,
			Vector addedBugs) {
		HashMap attachs = pExecTestRes.getAttachmentMapFromModel();
		Iterator ita = attachs.values().iterator();
		Element defectsElem = null;
		while (ita != null && ita.hasNext()) {
			Attachment at = (Attachment) ita.next();
			if (at instanceof UrlAttachment) {
				UrlAttachment uat = (UrlAttachment) at;
				if (uat.getDescriptionFromModel().equals(ATTACH_DESC)) {
					int bugID = getIDofBug(at);
					if (defectsCache != null) {
						DefectWrapper pDefectWrapper = (DefectWrapper) defectsCache
								.get(new Integer(bugID));
						if (pDefectWrapper != null) {
							String id_bug = "Bug_" + pDefectWrapper.getId();
							if (!addedBugs.contains(id_bug)) {
								defectsElem = pElem.addElement("DefectsLink");
								defectsElem.addAttribute("defect_manager",
										"Mantis");
								defectsElem.addAttribute("host", mantis_host);
								defectsElem.addAttribute("ref", id_bug);
								addedBugs.add(id_bug);
								Element tempElem;
								if (pExecutionResult != null) {
									Execution pExec = pExecutionResult
											.getExecution();
									Environment pEnv = pExec
											.getEnvironmentFromModel();
									Campaign pCamp = pExec
											.getCampagneFromModel();

									tempElem = defectsElem
											.addElement("CampaignRef");
									tempElem.addAttribute("id", "Camp_"
											+ pCamp.getIdBdd());
									tempElem.addAttribute("name", pCamp
											.getNameFromModel());

									tempElem = defectsElem
											.addElement("EnvironmentRef");
									tempElem.addAttribute("id", "Env_"
											+ pEnv.getIdBdd());
									tempElem.addAttribute("name", pEnv
											.getNameFromModel());

									tempElem = defectsElem
											.addElement("ExecutionRef");
									tempElem.addAttribute("id", "ExecCamp_"
											+ pExec.getIdBdd());
									tempElem.addAttribute("name", pExec
											.getNameFromModel());

									tempElem = defectsElem
											.addElement("ExecutionResultRef");
									tempElem.addAttribute("id", "ResExecCamp_"
											+ pExecutionResult.getIdBdd());
									tempElem
											.addAttribute("name",
													pExecutionResult
															.getNameFromModel());

									/*
									 * tempElem =
									 * defectsElem.addElement("ExecutionTestResult"
									 * ); tempElem.addAttribute("id",
									 * ""+pExecTestRes.getIdBdd() );
									 * tempElem.addAttribute("name",
									 * pExecTestRes.getNameFromModel());
									 *
									 * tempElem =
									 * defectsElem.addElement("Test");
									 * tempElem.addAttribute("id",
									 * ""+pExecTestRes
									 * .getTestFromModel().getIdBdd() );
									 * tempElem.addAttribute("name",
									 * pExecTestRes
									 * .getTestFromModel().getNameFromModel());
									 * tempElem
									 * .addAttribute("execution_test_id",
									 * ""+pExecTestRes.getIdBdd());
									 */
								}
								tempElem = defectsElem.addElement("TestRef");
								tempElem.addAttribute("ref", "Test_"
										+ pExecTestRes.getTestFromModel()
												.getIdBdd());
								tempElem.addElement("Nom").setText(
										pExecTestRes.getTestFromModel()
												.getNameFromModel());
								tempElem.addAttribute("execution_test_id",
										"TestRes_" + pExecTestRes.getIdBdd());
								writeXMLDefect(defectsElem, pDefectWrapper,
										pXMLWriter);
							}
						}
					}
				}
			}
		}
	}

	// Hashtable alldefect ;
	public void addXMLElement2Root(Element rootElement,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/* Reload defect cache */
		try {
			getDefectsOfProject(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* Test Plan */
	public void addXMLElement2Project(Element projectElement, Project pProject,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		if (!doExport()) {
			return;
		}
		if (pMantisConnector != null && bugProjectID > 0) {
			try {
				Element defectsElem = projectElement.addElement("Defects");
				defectsElem.addAttribute("defect_manager", "Mantis");
				defectsElem.addAttribute("host", mantis_host);
				getDefectsOfProject(true);
				// alldefect = pMantisConnector.getProjectDefects(bugProjectID);
				Enumeration enumDefect = defectsCache.elements();
				while (enumDefect.hasMoreElements()) {
					DefectWrapper pDefectWrapper = (DefectWrapper) enumDefect
							.nextElement();
					writeXMLDefect(defectsElem, pDefectWrapper, pXMLWriter);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void addXMLElement2Family(Element familyElement, Family pFamily,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/*
		 * if (alldefect == null) return; HashMap attachs =
		 * pFamily.getAttachmentMapFromModel(); Iterator ita =
		 * attachs.values().iterator(); Element defectsElem = null; while
		 * (ita.hasNext()){ Attachment at = (Attachment)ita.next(); if (at
		 * instanceof UrlAttachment){ UrlAttachment uat = (UrlAttachment)at; if
		 * (uat.getDescriptionFromModel().equals(ATTACH_DESC)){ int bugID =
		 * getIDofBug(at); DefectWrapper pDefectWrapper =
		 * (DefectWrapper)alldefect.get(new Integer(bugID)); if (defectsElem ==
		 * null){ defectsElem = familyElement.addElement("DefectsLink");
		 * defectsElem.addAttribute("defect_manager", "Mantis");
		 * defectsElem.addAttribute("host", mantis_host); }
		 * writeXMLDefect(defectsElem, pDefectWrapper, pXMLWriter); } } }
		 */
	}

	public void addXMLElement2Suite(Element suiteElement, TestList ptestList,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/*
		 * if (alldefect == null) return; HashMap attachs =
		 * ptestList.getAttachmentMapFromModel(); Iterator ita =
		 * attachs.values().iterator(); Element defectsElem = null; while
		 * (ita.hasNext()){ Attachment at = (Attachment)ita.next(); if (at
		 * instanceof UrlAttachment){ UrlAttachment uat = (UrlAttachment)at; if
		 * (uat.getDescriptionFromModel().equals(ATTACH_DESC)){ int bugID =
		 * getIDofBug(at); DefectWrapper pDefectWrapper =
		 * (DefectWrapper)alldefect.get(new Integer(bugID)); if (defectsElem ==
		 * null){ defectsElem = suiteElement.addElement("DefectsLink");
		 * defectsElem.addAttribute("defect_manager", "Mantis");
		 * defectsElem.addAttribute("host", mantis_host); }
		 * writeXMLDefect(defectsElem, pDefectWrapper, pXMLWriter); } } }
		 */
	}

	public void addXMLElement2Test(Element testElement, Test pTest,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		if (!doExport()) {
			return;
		}
		// Hashtable pDefectWrappers = new Hashtable();
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		Vector addedBugs = new Vector();
		Element defectsElem = testElement.addElement("DefectsLinks");
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec
						.getExecutionResultListFromModel();
				for (int k = 0; k < listOfExecutionRes.size(); k++) {
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
							.get(k);
					Test[] tests = pExecutionResult.getTestOrderedFromModel();
					for (int l = 0; l < tests.length; l++) {
						if (pTest.getIdBdd() == tests[l].getIdBdd()) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							writeXMLDefectLink(defectsElem, pExecutionResult,
									pExecutionTestResult, pXMLWriter, addedBugs);
							// return; /*TO DO : remplace with while */
						}
					}
				}
			}
		}

	}

	// void addXMLElement2ValuedParameter(Element paramElement, Test pTest,
	// Parameter pParam);
	public void addXMLElement2Action(Element testElement, Action pAction,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/* NOT YET IMPLEMENTED */
	}

	/* Campaing plan */
	public void addXMLElement2Campaign(Element campaignElement,
			Campaign pcampaign, XMLWriterPlugin pXMLWriter, String pathAttach) {
		/*
		 * if (defectsCache == null) return; HashMap attachs =
		 * pcampaign.getAttachmentMapFromModel(); Iterator ita =
		 * attachs.values().iterator(); Element defectsElem = null; while
		 * (ita.hasNext()){ Attachment at = (Attachment)ita.next(); if (at
		 * instanceof UrlAttachment){ UrlAttachment uat = (UrlAttachment)at; if
		 * (uat.getDescriptionFromModel().equals(ATTACH_DESC)){ int bugID =
		 * getIDofBug(at); DefectWrapper pDefectWrapper =
		 * (DefectWrapper)defectsCache.get(new Integer(bugID)); if (defectsElem
		 * == null){ defectsElem = campaignElement.addElement("DefectsLink");
		 * defectsElem.addAttribute("defect_manager", "Mantis");
		 * defectsElem.addAttribute("host", mantis_host); }
		 * writeXMLDefect(defectsElem, pDefectWrapper, pXMLWriter); } } }
		 */
	}

	public void addXMLElement2Execution(Element execElement,
			Execution pExecution, XMLWriterPlugin pXMLWriter, String pathAttach) {
		if (!doExport()) {
			return;
		}

		// Hashtable pDefectWrappers = new Hashtable();
		Project pProject = DataModel.getCurrentProject();
		Vector addedBugs = new Vector();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				// defectsElem = pElem.addElement("DefectsLink");
				if (pExec.getIdBdd() == pExecution.getIdBdd()) {
					Element defectsElem = execElement
							.addElement("DefectsLinks");
					ArrayList listOfExecutionRes = pExec
							.getExecutionResultListFromModel();
					for (int k = 0; k < listOfExecutionRes.size(); k++) {
						ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
								.get(k);
						Test[] tests = pExecutionResult
								.getTestOrderedFromModel();
						for (int l = 0; l < tests.length; l++) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							writeXMLDefectLink(defectsElem, pExecutionResult,
									pExecutionTestResult, pXMLWriter, addedBugs);
						}
					}
					return; /* TO DO : remplace with while */
				}

			}
		}
	}

	public void addXMLElement2ResExecution(Element resExecElement,
			ExecutionResult pExecRes, XMLWriterPlugin pXMLWriter,
			String pathAttach) {
		if (!doExport()) {
			return;
		}
		Project pProject = DataModel.getCurrentProject();
		Vector addedBugs = new Vector();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				ArrayList listOfExecutionRes = pExec
						.getExecutionResultListFromModel();
				for (int k = 0; k < listOfExecutionRes.size(); k++) {
					ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
							.get(k);
					if (pExecutionResult.getIdBdd() == pExecRes.getIdBdd()) {
						Element defectsElem = resExecElement
								.addElement("DefectsLinks");
						Test[] tests = pExecutionResult
								.getTestOrderedFromModel();
						for (int l = 0; l < tests.length; l++) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							writeXMLDefectLink(defectsElem, pExecutionResult,
									pExecutionTestResult, pXMLWriter, addedBugs);
						}
						return; /* TO DO : remplace with while */
					}
				}
			}
		}
	}

	public void addXMLElement2ResTestExecution(Element resTestElem,
			ExecutionTestResult pExecTestRes, Test pTest,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		if (!doExport()) {
			return;
		}

		Element defectsElem = resTestElem.addElement("DefectsLinks");
		writeXMLDefectLink(defectsElem, null, pExecTestRes, pXMLWriter,
				new Vector());

	}

	public void addXMLElement2DataSet(Element dataSetElement, DataSet pDataSet,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/* NOT YET IMPLEMENTED */
	}

	/* Data */
	public void addXMLElement2Parameter(Element paramElement, Parameter pParam,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		/* NOT YET IMPLEMENTED */
	}

	public void addXMLElement2Environment(Element envElement, Environment pEnv,
			XMLWriterPlugin pXMLWriter, String pathAttach) {
		if (!doExport()) {
			return;
		}
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		Element defectsElem = envElement.addElement("DefectsLinks");
		Vector addedBugs = new Vector();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				if (pExec.getEnvironmentFromModel().getIdBdd() == pEnv
						.getIdBdd()) {
					ArrayList listOfExecutionRes = pExec
							.getExecutionResultListFromModel();
					for (int k = 0; k < listOfExecutionRes.size(); k++) {
						ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
								.get(k);
						Test[] tests = pExecutionResult
								.getTestOrderedFromModel();
						for (int l = 0; l < tests.length; l++) {
							ExecutionTestResult pExecutionTestResult = pExecutionResult
									.getExecutionTestResultFromModel(tests[l]);
							writeXMLDefectLink(defectsElem, pExecutionResult,
									pExecutionTestResult, pXMLWriter, addedBugs);
						}

					}
				}
			}
		}

		/*
		 * if (defectsCache == null) return; Enumeration enumDefect =
		 * defectsCache.elements(); Element defectsElem = null; while
		 * (enumDefect.hasMoreElements()){ DefectWrapper pDefectWrapper =
		 * (DefectWrapper)enumDefect.nextElement(); if
		 * (pDefectWrapper.getEnvironement().equals(pEnv.getNameFromModel())){
		 * if (defectsElem == null){ defectsElem =
		 * envElement.addElement("DefectsLink");
		 * defectsElem.addAttribute("defect_manager", "Mantis");
		 * defectsElem.addAttribute("host", mantis_host); }
		 * writeXMLDefect(defectsElem, pDefectWrapper, pXMLWriter); } }
		 */
	}

	/**************** IMPORT *****************/
	/**
	 * Called first before updateMethode if isSupOption = true
	 */
	public void manageDelete(Document doc, XMLLoaderPlugin pXMLLoader)
			throws Exception {
		/* DO NOTHINK */
	}

	/* Test Plan */
	public void updateProjectFromXML(Document doc, boolean isSupOption,
			Project project, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateFamilyFromXML(Element familyElement, Family pFamily,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateSuiteFromXML(Element suiteElement, TestList pSuite,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateTestFromXML(Element testElement, Test pTest,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateActionFromXML(Element actionElement, Action pAction,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	/* Campaing plan */
	public void updateCampaignFromXML(Element campaignElement,
			Campaign pcampaign, boolean isSupOption, XMLLoaderPlugin pXMLLoader)
			throws Exception {
		/* DO NOTHINK */
	}

	public void updateExecutionFromXML(Element execElement,
			Execution pExecution, boolean isSupOption,
			XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateResExecutionFromXML(Element resExecElement,
			ExecutionResult pExecRes, boolean isSupOption,
			XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateDataSetFromXML(Element dataSetElement, DataSet pDataSet,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	/* Data */
	public void updateParameterFromXML(Element paramElement, Parameter pParam,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	public void updateEnvironmentFromXML(Element envElement, Environment pEnv,
			boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
		/* DO NOTHINK */
	}

	/**
	 *
	 * Called last after import
	 */
	public void refreshNewData() {
		/* DO NOTHINK */
	}

	/****************
	 * XSLT ******************
	 *
	 * @param dynamicMode
	 *            true if generated HTML report is the results' report, false
	 *            otherwise
	 * @param multiFrame
	 *            true if the generated HTML report is in multi-frame mode,
	 *            false otherwise
	 */
	public File getXSLToImport(boolean dynamicMode, boolean multiFrame,
			boolean htmlFormat) {
		String _urlBase = SalomeTMFContext.getInstance().getUrlBase()
				.toString();
		String url_txt = _urlBase.substring(0, _urlBase.lastIndexOf("/"));
		String xsl = "";
		if (dynamicMode) {
			if (htmlFormat) {
				if (multiFrame) {
					xsl = "/plugins/mantis/xsl/frameDynaDefects.xsl";
				} else {
					xsl = "/plugins/mantis/xsl/dynaDefects.xsl";
				}
			} else
				xsl = "/plugins/mantis/xsl/dynaDefectsDocBook.xsl";
		} else {// staticMode
			if (htmlFormat) {
				if (multiFrame) {
					xsl = "/plugins/mantis/xsl/frameDefects.xsl";
				} else {
					xsl = "/plugins/mantis/xsl/defects.xsl";
				}
			} else
				xsl = "/plugins/mantis/xsl/defectsDocBook.xsl";
		}
		String temporaryFilePath = System.getProperties().getProperty(
				"java.io.tmpdir")
				+ File.separator + "defectsXSLT.xsl";
		try {
			URL xsltURL = new URL(url_txt + xsl);
			Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
		} catch (Exception e) {
			Tools.writeFile(ReqPlugin.class.getResourceAsStream("/salome/"
					+ xsl), temporaryFilePath);
		}
		File xsltFile = new File(temporaryFilePath);
		return xsltFile;
	}

	/**
	 * Get the translation file for the XSLT transformation
	 *
	 * @return the XML file for translation
	 */
	public File getTranslationFile() {
		String _urlBase = SalomeTMFContext.getInstance().getUrlBase()
				.toString();
		String url_txt = _urlBase.substring(0, _urlBase.lastIndexOf("/"));
		String translate = "/plugins/mantis/xsl/translate.xml";
		String temporaryFilePath = System.getProperties().getProperty(
				"java.io.tmpdir")
				+ File.separator + "requirementsTranslate.xml";
		try {
			URL translateURL = new URL(url_txt + translate);
			Tools.writeFile(translateURL.openStream(), temporaryFilePath);
		} catch (Exception e) {
			Tools.writeFile(ReqPlugin.class.getResourceAsStream("/salome/"
					+ translate), temporaryFilePath);
		}
		File translateFile = new File(temporaryFilePath);
		return translateFile;
	}

	public int getQsScoreDefectForTest(Test test, Environment env) {
		int res = 3;

		Vector defects = getOpenDefectWrappersListForTestAndEnv(test, env);
		if ((defects != null) && (defects.size() != 0)) {
			int i = 0;
			while ((i < defects.size()) && (res > 0)) {
				DefectWrapper defect = (DefectWrapper) defects.elementAt(i);
				int pri = defect.getPriority();
				int sev = defect.getSeverity();
				int qs_tmp = 3;
				if (pri >= 40) {
					qs_tmp = 0;
				} else if (pri == 30) {
					if (sev >= 30) {
						qs_tmp = 0;
					} else {
						qs_tmp = 1;
					}
				} else if (pri == 20) {
					if (sev >= 40) {
						qs_tmp = 1;
					} else {
						qs_tmp = 2;
					}
				}
				if (qs_tmp < res) {
					res = qs_tmp;
				}
				i++;
			}
		}

		return res;
	}

	Vector getOpenDefectWrappersListForTestAndEnv(Test test, Environment env) {

		Vector pDefectWrappers = new Vector();
		Project pProject = DataModel.getCurrentProject();
		ArrayList listOfCampaign = pProject.getCampaignListFromModel();
		for (int i = 0; i < listOfCampaign.size(); i++) {
			Campaign pCamp = (Campaign) listOfCampaign.get(i);
			ArrayList listOfExecution = pCamp.getExecutionListFromModel();
			for (int j = 0; j < listOfExecution.size(); j++) {
				Execution pExec = (Execution) listOfExecution.get(j);
				if (pExec.getEnvironmentFromModel().equals(env)) {
					ArrayList listOfExecutionRes = pExec
							.getExecutionResultListFromModel();
					for (int k = 0; k < listOfExecutionRes.size(); k++) {
						ExecutionResult pExecutionResult = (ExecutionResult) listOfExecutionRes
								.get(k);
						Test[] tests = pExecutionResult
								.getTestOrderedFromModel();
						for (int l = 0; l < tests.length; l++) {
							if (test.getIdBdd() == tests[l].getIdBdd()) {
								ExecutionTestResult pExecutionTestResult = pExecutionResult
										.getExecutionTestResultFromModel(tests[l]);
								if (pExecutionTestResult != null) {
									HashMap attacMap = pExecutionTestResult
											.getAttachmentMapFromModel();
									Set keysSet = attacMap.keySet();
									for (Iterator iter = keysSet.iterator(); iter
											.hasNext();) {
										try {
											Object attachName = iter.next();
											Attachment pAttach = (Attachment) attacMap
													.get(attachName);
											int bugid = getIDofBug(pAttach);
											DefectWrapper pDefectWrapper = getDefectInfo(
													bugid, false);
											if ((pDefectWrapper != null)
													&& (pDefectWrapper
															.getStatus() != 90)) {
												pDefectWrappers
														.add(pDefectWrapper);
											}
										} catch (Exception ex) {
											ex.printStackTrace();
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return pDefectWrappers;
	}

	public void setExport(boolean b) {
		mandatoryExport = b;
	}

	public void setImport(boolean b) {
		mandatoryImport = b;
	}

	public String getChapterTitleInReport() {
		if (doExport()) {
			return Language.getInstance().getText("Anomalies");
		} else {
			return null;
		}
	}

	public String getParameterName(String chapterName) {
		if (chapterName.equals(Language.getInstance().getText("Anomalies"))) {
			return "anomalies";
		}
		return null;
	}

	public String getName(Attachment pAttachment) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getPriority(Attachment pAttachment) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getStatus(Attachment pAttachment) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getType(Attachment pAttachment) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getBugKey(Attachment pAttachment) {
		return getBugID(pAttachment);
	}

}