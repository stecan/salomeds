package salomeTMF_plug.mantis;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;

public class LinkDefectDialog extends JDialog implements ActionListener {

	JButton commitButton;
	JButton cancelButton;

	DefectWrapper selectDefectWrapper;

	MantisPlugin pMantisPlugin;
	DefectPanel pDefectPanel;

	public LinkDefectDialog(MantisPlugin pMantisPlugin,
			Hashtable linkedDefects, String envRestrictionName) {
		super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
		setModal(true);
		this.pMantisPlugin = pMantisPlugin;
		initComponent(envRestrictionName, linkedDefects);
		makeDialog(Language.getInstance().getText("Lier_a_annomalie"));
	}

	public LinkDefectDialog(Dialog parent, MantisPlugin pMantisPlugin,
			Hashtable linkedDefects, String envRestrictionName) {
		super(parent, true);
		setModal(true);
		this.pMantisPlugin = pMantisPlugin;
		initComponent(envRestrictionName, linkedDefects);
		makeDialog(Language.getInstance().getText("Lier_a_annomalie"));

	}

	void initComponent(String envRestrictionName, Hashtable linkedDefects) {
		selectDefectWrapper = null;
		commitButton = new JButton(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Valider"));
		commitButton.addActionListener(this);
		cancelButton = new JButton(
				org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
						.getText("Annuler"));
		cancelButton.addActionListener(this);
		JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonPanel.add(cancelButton);
		buttonPanel.add(commitButton);
		pDefectPanel = new DefectPanel(false, pMantisPlugin, buttonPanel,
				envRestrictionName);
		try {
			Hashtable allDefects = pMantisPlugin.getDefectsOfProject(false);
			Enumeration enumLikeddefect = linkedDefects.keys();
			while (enumLikeddefect.hasMoreElements()) {
				Integer key = (Integer) enumLikeddefect.nextElement();
				allDefects.remove(key);
			}
			pDefectPanel.loadData(allDefects);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DefectWrapper getSelectedDefectWrapper() {
		return selectDefectWrapper;
	}

	void makeDialog(String title) {
		int t_x = 1024 - 100;
		int t_y = 768 - 50;
		int t_x2 = 1024;
		int t_y2 = 768;
		try {
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			GraphicsDevice[] gs = ge.getScreenDevices();
			GraphicsDevice gd = gs[0];
			GraphicsConfiguration[] gc = gd.getConfigurations();
			Rectangle r = gc[0].getBounds();
			t_x = r.width - 100;
			t_y = r.height - 50;
			t_x2 = r.width;
			t_y2 = r.height;
		} catch (Exception E) {

		}

		Container contentPane = this.getContentPane();
		contentPane.setLayout(new BorderLayout());
		contentPane.add(pDefectPanel, BorderLayout.CENTER);
		pDefectPanel.setSize(new Dimension(t_x, t_y));
		this.setTitle(title);
		setSize(t_x, t_y);
		/*
		 * this.pack(); this.setLocationRelativeTo(this.getParent());
		 * this.setVisible(true);
		 */
		centerScreen();
	}

	void centerScreen() {
		Dimension dim = getToolkit().getScreenSize();
		this.pack();
		Rectangle abounds = getBounds();
		setLocation((dim.width - abounds.width) / 2,
				(dim.height - abounds.height) / 2);
		this.setVisible(true);
		requestFocus();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(cancelButton)) {
			selectDefectWrapper = null;
			dispose();
		} else if (e.getSource().equals(commitButton)) {
			selectDefectWrapper = pDefectPanel.getSelectedDefect();
			dispose();
		}
	}
}
