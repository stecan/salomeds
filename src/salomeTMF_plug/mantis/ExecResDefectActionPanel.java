package salomeTMF_plug.mantis;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.mantis.languages.Language;
import salomeTMF_plug.mantis.sqlWrapper.DefectWrapper;


public class ExecResDefectActionPanel extends JPanel implements ActionListener{

	
	JButton newDefectButton ;
	JButton linkDefectButton;
	JButton editDefectButton ;
	JButton deleteButton ;
	
	DefectPanel pDefectPanel;
	MantisPlugin pMantisPlugin;
	ExecutionTestResult pExecutionTestResult;
	Execution pExecution; 
	boolean workOnModel;
	ExecResDefectActionPanel(MantisPlugin pMantisPlugin, boolean workOnModel){
		super(new GridLayout(1, 3));
		this.pMantisPlugin = pMantisPlugin;
		this.workOnModel = workOnModel;
		initActionPanel();
	}
	
	void setDefectPanel(DefectPanel pDefectPanel){
		this.pDefectPanel = pDefectPanel;
	}
	
	void setExecutionResult(ExecutionTestResult pExecutionTestResult){
		this.pExecutionTestResult = pExecutionTestResult;
	}
	void setExecution(Execution pExecution){
		this.pExecution = pExecution;
	}
	void initActionPanel(){
		newDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Ajouter"));
		newDefectButton.addActionListener(this);
		linkDefectButton = new JButton(Language.getInstance().getText("Lier_a_annomalie"));
		linkDefectButton.addActionListener(this);
		editDefectButton = new JButton(salomeTMF_plug.mantis.languages.Language.getInstance().getText("Editer"));
		editDefectButton.addActionListener(this);
		deleteButton = new JButton(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Supprimer"));
		deleteButton.addActionListener(this);
		add(newDefectButton);
		add(linkDefectButton);
		add(editDefectButton);
		add(deleteButton);
	}
	
	public void actionPerformed(ActionEvent e){
		if (e.getSource().equals(newDefectButton)){
			newDefectPerformed();
		} else if (e.getSource().equals(editDefectButton)){
			editDefectPerformed();
		} else if (e.getSource().equals(deleteButton)){
			deleteDefectPerformed();
		}else if (e.getSource().equals(linkDefectButton)){
			linkPerformed();
		}
	}
	
	void linkPerformed(){
		String envRestrictionName = null;
		if (pExecution == null){
			try {
			pExecution = DataModel.getObservedExecutionResult().getExecution();
			envRestrictionName = pExecution.getEnvironmentFromModel().getNameFromModel();
			}catch (Exception e1){
				e1.printStackTrace();
			}
		} else {
			envRestrictionName = pExecution.getEnvironmentFromModel().getNameFromModel();
		}
		LinkDefectDialog pLinkDefectDialog = new LinkDefectDialog(pMantisPlugin, pDefectPanel.defectsWrapper, envRestrictionName );
		DefectWrapper pDefectWrapper =  pLinkDefectDialog.getSelectedDefectWrapper();
		if (pDefectWrapper != null){
			try {
				Attachment pAttch = pMantisPlugin.makeAttachement(pDefectWrapper.getId());
				//if (pExecutionTestResult == null){
					pExecutionTestResult =  DataModel.getCurrentExecutionTestResult();
				//}
				if (workOnModel){
					pExecutionTestResult.addAttachementInModel(pAttch);
				} else {
					pExecutionTestResult.addAttachementInDBAndModel(pAttch);
				}
				pDefectPanel.defectsWrapper.put(new Integer(pDefectWrapper.getId()), pDefectWrapper);
				pDefectPanel.reloadData();
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	void newDefectPerformed(){
		
		if (pExecution == null){
			try {
			pExecution = DataModel.getObservedExecutionResult().getExecution();
			}catch (Exception e1){
				e1.printStackTrace();
			}
		}
		//if (pExecutionTestResult == null){
			pExecutionTestResult =  DataModel.getCurrentExecutionTestResult();
		//}
		DefectView pDefectView = null;
		if (DataModel.getObservedExecutionResult() != null && pExecutionTestResult != null){
			pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), DataModel.getObservedExecutionResult(), pExecutionTestResult,
					null, null, null, null, pMantisPlugin);
		} else if (pExecution != null){
			pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pMantisPlugin, pExecution.getEnvironmentFromModel().getNameFromModel());
			
		} else {
			pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pMantisPlugin);
		}
		try {
			if (pDefectView.isDoingModification()){
				DefectWrapper pDefectWrapper = pDefectView.getDefectWrapper();
				if (pDefectWrapper != null){
					Attachment pAttch = pMantisPlugin.makeAttachement(pDefectWrapper.getId());
					
					if (workOnModel){
						pExecutionTestResult.addAttachementInModel(pAttch);
					} else {
						pExecutionTestResult.addAttachementInDBAndModel(pAttch);
					}
					pDefectPanel.defectsWrapper.put(new Integer(pDefectWrapper.getId()), pDefectWrapper);
					pDefectPanel.reloadData();
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	void editDefectPerformed(){
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			DefectView pDefectView = new DefectView(SalomeTMFContext.getInstance().getSalomeFrame(), pDefectWrapper, pMantisPlugin, true);
			try {
				if (pDefectView.isDoingModification()){
					pDefectPanel.reloadData();
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	void deleteDefectPerformed(){
		DefectWrapper pDefectWrapper  = pDefectPanel.getSelectedDefect();
		if (pDefectWrapper !=null){
			if (pMantisPlugin.deleteConfirme(Language.getInstance().getText("Le_lien"))){	
				int defectID = pDefectWrapper.getId();
				//if (pExecutionTestResult == null){
					pExecutionTestResult =  DataModel.getCurrentExecutionTestResult();
				//}
				HashMap attacMap = pExecutionTestResult.getAttachmentMapFromModel();
				Set keysSet = attacMap.keySet();
				boolean trouve = false;
				Iterator iter = keysSet.iterator();
				while (!trouve &&  iter.hasNext()) {
					try {
						Object attachName =  iter.next();
						Attachment pAttach = (Attachment) attacMap.get(attachName);
						int bugid = pMantisPlugin.getIDofBug(pAttach);
						if (bugid == defectID){
							if (workOnModel){
								pExecutionTestResult.deleteAttachmentInModel(pAttach);
							} else {
								pExecutionTestResult.deleteAttachementInDBAndModel(pAttach);
							}
							pDefectPanel.defectsWrapper.remove(new Integer(bugid));
							pDefectPanel.reloadData();
							trouve = true;
						}
					} catch(Exception ex){
						ex.printStackTrace();
					}
				}
			}		
		}
	}
}
