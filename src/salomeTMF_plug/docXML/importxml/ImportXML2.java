/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.docXML.importxml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JTable;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.databaseSQL.SQLEngine;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.admin.Administration;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.TestDriver;
import org.objectweb.salome_tmf.plugins.core.XMLLoaderPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.common.CreateProjectDialog;
import salomeTMF_plug.docXML.languages.Language;

public class ImportXML2 implements ApiConstants, IHMConstants, XMLLoaderPlugin {

    private ArrayList<Family> familySelectionList;
    private ArrayList<TestList> suiteSelectionList;
    private ArrayList<Test> testSelectionList;

    private IPlugObject pIPlugObject;
    private AdminVTData pAdminVTData;
    private Project project;
    private Vector<XMLPrinterPlugin> listXMLPlugin = new Vector<XMLPrinterPlugin>();

    private Document doc;
    private String dirXml;

    private ImportDialog2 idialog;
    private CreateProjectDialog cdialog;

    private boolean cancelled = false;
    private boolean initSelection;
    private boolean importCampaign = false;
    private boolean attachmentException = false;
    private boolean newProject = false;
    private Vector<AutomaticTest> automatedTest2Update = new Vector<AutomaticTest>();
    private Vector<Test> savedTests = new Vector<Test>();

    public ImportXML2(JDialog dialog, Document document,
                      ArrayList<Family> familySelectionList,
                      ArrayList<TestList> suiteSelectionList,
                      ArrayList<Test> testSelectionList, AdminVTData adminVTData,
                      IPlugObject plugObject) {
        super();

        this.familySelectionList = familySelectionList;
        this.suiteSelectionList = suiteSelectionList;
        this.testSelectionList = testSelectionList;
        this.pIPlugObject = plugObject;
        this.doc = document;
        this.pAdminVTData = adminVTData;

        Vector<Extension> listExtXMLPlugin = pIPlugObject
            .getXMLPrintersExtension();
        for (Extension pXMLExt : listExtXMLPlugin) {
            JPFManager pJPFManager = pIPlugObject.getPluginManager();
            try {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager
                    .activateExtension(pXMLExt);
                if (!listXMLPlugin.contains(pXMLPrinterPlugin)) {
                    listXMLPlugin.add(pXMLPrinterPlugin);
                }
            } catch (Exception e) {
            }
        }

        if (dialog instanceof ImportDialog2) {
            idialog = (ImportDialog2) dialog;
        } else {
            cdialog = (CreateProjectDialog) dialog;
        }
    }

    /**
     * Launch the import process
     *
     * @throws Exception
     */
    public void importInProject() throws Exception {
        project = DataModel.getCurrentProject(); // Add for V2
        DataModel.reloadFromBase(false);

        if (idialog.isSupprElement() && !cancelled) {
            projectDeletions();
            testsDeletions();
            campaignsDeletions();

            /***** Plugin manage XML delete for Project *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                pXMLPrinterPlugin.manageDelete(doc, this);
            }
        }

        coreImport();

        DataModel.reloadFromBase(true);

        /***** Plugin manage XML refresh *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            pXMLPrinterPlugin.refreshNewData();
        }
        /***** (END) Plugin manage XML refresh *****/

    }

    /**
     * Launch the import process from the createDialog in Administration of
     * Salome
     *
     * @throws Exception
     */
    public void importInNewProject() throws Exception {
        newProject = true;
        ArrayList<Object> data = new ArrayList<Object>();
        project = cdialog.getNewProject();
        if (project != null && !cancelled) {
            if (Api.isConnected()) {
                try {
                    project.addInDB();
                    project.setUserInModel(project.getAdministratorFromModel());

                    data.add(Tools.createAppletImageIcon(PATH_TO_PROJECT_ICON,
                                                         ""));
                    data.add(project.getNameFromModel().trim());
                    data.add(project.getAdministratorFromModel()
                             .getLoginFromModel());
                    data.add(project.getCreationDateFromModel().toString());
                    data.add(project.getDescriptionFromModel());
                    JTable projectTable = (JTable) Administration
                        .getUIComponent(UICompCst.ADMIN_PROJECT_MANAGEMENT_TABLE);
                    ((MyTableModel) projectTable.getModel()).addRow(data);
                    project.setUserInModel(project.getAdministratorFromModel());
                    pAdminVTData.addProjectInModel(project);
                    pAdminVTData.addAdminToProjectInModel(project
                                                          .getAdministratorFromModel(), project);
                    projectTable.getColumnModel().getColumn(0).setMaxWidth(18);
                } catch (Exception exception) {
                    cdialog
                        .setErrorMessage(cdialog.getErrorMessage()
                                         + Language
                                         .getInstance()
                                         .getText(
                                                  "Probleme_lors_de_la_creation_du_nouveau_projet"));
                    cdialog.showErrorMessage();
                    setAnnule(true);
                }
            }
        }

        if (!cancelled) {
            SQLEngine.setSpecialAllow(true);
            coreImport();
            SQLEngine.setSpecialAllow(false);
        }
    }

    /**
     * Delete attachments of project which are in database and not in XML
     * document.
     */
    private void projectDeletions() throws Exception {
        Element projectElem = (Element) doc.selectSingleNode("//ProjetVT");
        attachmentDeletions(projectElem, project);
    }

    /**
     * Delete elements of test plan which are in database and not in XML
     * document
     *
     * @throws Exception
     */
    private void testsDeletions() throws Exception {
        // Deletion of project parameters
        parameterDeletion();
        if (cancelled) {
            throw new Exception("[ImportAlgo->suppressionParameter]");
        }

        // Deletion of environments
        environmentDeletion();
        if (cancelled) {
            throw new Exception("[ImportAlgo->suppressionEnvironnement]");
        }

        // deletion of families
        familyDeletion();
        if (cancelled) {
            throw new Exception("[ImportAlgo->suppressionFamily]");
        }
    }

    /**
     * Delete elements of campaigns which are in database and not in XML
     * document
     *
     * @throws Exception
     */
    private void campaignsDeletions() throws Exception {
        ArrayList<Campaign> campList = project.getCampaignListFromModel();
        ArrayList<Campaign> campToRemove = new ArrayList<Campaign>();
        List<Element> campListXML = doc.selectNodes("//CampagneTest");
        Element campElem = null;
        Iterator<Campaign> itCamp = campList.iterator();
        while (itCamp.hasNext() && !cancelled) {
            Campaign camp = (Campaign) itCamp.next();
            boolean belong = false;
            Iterator<Element> itCampXML = campListXML.iterator();
            while (itCampXML.hasNext() && !belong) {
                campElem = itCampXML.next();
                if (campElem.elementText("Nom").equals(camp.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                campToRemove.add(camp);
            } else {
                // suppression des jeux de donnees
                dataSetDeletion(campElem, camp);

                // suppression des attachements des campagnes
                attachmentDeletions(campElem, camp);

                // suppression Tests
                testInCampDeletion(campElem, camp);

                // suppression Executions -> suppression resultats d'execution
                execInCampDeletion(campElem, camp);
            }
        }
        Iterator<Campaign> itCampRm = campToRemove.iterator();
        while (itCampRm.hasNext() && !cancelled) {
            Campaign campRm = itCampRm.next();
            try {
                project.deleteCampaignInDBAndModel(campRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                throw e;
            }
        }
    }

    /**
     * Delete project parameters are in project database and not in XML
     * document.
     *
     * @return true if there was an error during deletion
     */
    private void parameterDeletion() throws Exception {
        Hashtable<String, Parameter> paramSet = project
            .getParameterSetFromModel();
        Enumeration<Parameter> itParamProjet = paramSet.elements();

        while (itParamProjet.hasMoreElements() && !cancelled) {
            Parameter param = itParamProjet.nextElement();
            List<Element> listParamXML = doc.selectNodes("//Param");
            boolean belong = false;
            for (Element paramElem : listParamXML) {
                if (paramElem.elementText("Nom").equals(
                                                        param.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                try {
                    project.deleteParamInDBndModel(param);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                    cancelled = true;
                }
            }
        }
    }

    /**
     * Delete project environment and associated data are in project database
     * and not in XML document
     *
     * @return true if there was an error during deletion
     */
    private void environmentDeletion() throws Exception {
        // environments deletion
        List<Element> envList = doc.selectNodes("//Environnement");
        ArrayList<Environment> envInBDD = project.getEnvironmentListFromModel();
        Iterator<Environment> itEnv = envInBDD.iterator();

        while (itEnv.hasNext() && !cancelled) {
            Environment env = itEnv.next();
            Iterator<Element> itEnvXML = envList.iterator();
            Element envElem = null;
            boolean belong = false;
            while (itEnvXML.hasNext() && !belong) {
                envElem = (Element) itEnvXML.next();
                if (envElem.elementText("Nom").equals(env.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                try {
                    project.deleteEnvironmentInDBandModel(env);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                    cancelled = true;
                }
            } else {
                // delete parameters with value in environment
                List<Element> paramEnvListXML = envElem
                    .selectNodes("ValeurParams/ValeurParam");
                Hashtable<Parameter, String> paramEnvList = env
                    .getParametersHashTableFromModel();
                Enumeration<Parameter> enumEnv = paramEnvList.keys();
                while (enumEnv.hasMoreElements()) {
                    belong = false;
                    Parameter paramEnv = enumEnv.nextElement();
                    Iterator<Element> itParamEnvXML = paramEnvListXML
                        .iterator();
                    while (itParamEnvXML.hasNext()) {
                        Element paramElem = (Element) itParamEnvXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (paramEnv.getNameFromModel().equals(nomParam)) {
                            belong = true;
                        }
                    }
                    if (!belong) {
                        try {
                            env.deleteDefParamInDBAndModel(paramEnv);
                        } catch (Exception e) {
                            Tools.ihmExceptionView(e);
                        }
                    }
                }
                // suppression du script de l'environnement
                Script script = env.getInitScriptFromModel();
                if (script != null) {
                    if (envElem.element("Script") == null) {
                        try {
                            env.deleteScriptInDBAndModel();
                        } catch (Exception e) {
                            Tools.ihmExceptionView(e);
                        }
                    }
                }
                // suppression des attachements de l'environnement
                attachmentDeletions(envElem, env);
            }
        }
    }

    /**
     * Delete families and associated elements are in project database and not
     * in XML document.
     *
     * @return
     */
    private void familyDeletion() throws Exception {
        // suppression des familles
        ArrayList<Family> familleList = project.getFamilyListFromModel();
        ArrayList<Family> familyToRemove = new ArrayList<Family>();
        List<Element> familleListXML = doc.selectNodes("//Famille");
        Element famElem = null;
        Iterator<Family> itFamily = familleList.iterator();
        while (itFamily.hasNext() && !cancelled) {
            Family family = itFamily.next();
            boolean belong = false;
            Iterator<Element> itFamXML = familleListXML.iterator();
            while (itFamXML.hasNext() && !belong) {
                famElem = (Element) itFamXML.next();
                if (famElem.elementText("Nom")
                    .equals(family.getNameFromModel())) {
                    if (isInitSelection()) {
                        boolean selectFamily = false;
                        for (Family selectedFamily : familySelectionList) {
                            if (selectedFamily.getNameFromModel().equals(
                                                                         family.getNameFromModel())) {
                                selectFamily = true;
                            }
                        }
                        if (selectFamily) {
                            belong = true;
                        }
                    } else {
                        belong = true;
                    }
                }
            }
            if (!belong) {
                familyToRemove.add(family);
            } else {
                //
                attachmentDeletions(famElem, family);
                // la famille appartient au document XML, gestion des suites
                suiteDeletion(famElem, family);
            }
        }
        Iterator<Family> itFamRm = familyToRemove.iterator();
        while (itFamRm.hasNext() && !cancelled) {
            Family family = itFamRm.next();
            try {
                project.deleteFamilyInDBAndModel(family);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                cancelled = true;
            }
        }
    }

    /**
     * Delete suites and associated elements are in project database and not in
     * XML document.
     *
     * @return
     */
    private void suiteDeletion(Element famElem, Family pFamily)
        throws Exception {
        // gestion des suites
        boolean belong = false;
        ArrayList<TestList> suiteList = pFamily.getSuiteListFromModel();
        ArrayList<TestList> suiteToRemove = new ArrayList<TestList>();
        Element suiteElem = null;
        List<Element> suiteListXML = famElem
            .selectNodes("SuiteTests/SuiteTest");
        Iterator<TestList> itSuite = suiteList.iterator();
        while (itSuite.hasNext() && !cancelled) {
            TestList suite = itSuite.next();
            belong = false;
            Iterator<Element> itSuiteXML = suiteListXML.iterator();
            while (itSuiteXML.hasNext() && !belong) {
                suiteElem = itSuiteXML.next();
                if (suiteElem.elementText("Nom").equals(
                                                        suite.getNameFromModel())) {
                    if (isInitSelection()) {
                        Iterator<TestList> itSuiteSelect = suiteSelectionList
                            .iterator();
                        boolean selectionneSuite = false;
                        while (itSuiteSelect.hasNext()) {
                            TestList suiteSelect = (TestList) itSuiteSelect
                                .next();
                            if (suiteSelect.getNameFromModel().equals(
                                                                      suite.getNameFromModel())
                                && suiteSelect
                                .getFamilyFromModel()
                                .getNameFromModel()
                                .equals(
                                        suite.getFamilyFromModel()
                                        .getNameFromModel())) {
                                selectionneSuite = true;
                            }
                        }
                        if (selectionneSuite) {
                            belong = true;
                        }
                    } else {
                        belong = true;
                    }
                }
            }
            if (!belong) {
                suiteToRemove.add(suite);
            } else {
                // gestion de la suppression des attachements de la suite
                attachmentDeletions(suiteElem, suite);
                // on gere la suppression des tests appartenant a la suite
                testDeletion(suiteElem, suite);
            }
        }
        Iterator<TestList> itSuiteRm = suiteToRemove.iterator();
        while (itSuiteRm.hasNext() && !cancelled) {
            TestList suite = (TestList) itSuiteRm.next();
            try {
                project.deleteTestListInDBandModel(suite);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                cancelled = true;
            }
        }
    }

    /**
     * Delete tests and associated elements are in project database and not in
     * XML document.
     *
     * @return
     */
    private void testDeletion(Element suiteElem, TestList suite)
        throws Exception {
        boolean belong = false;
        ArrayList<Test> testList = suite.getTestListFromModel();
        ArrayList<Test> testToRm = new ArrayList<Test>();
        Iterator<Test> itTest = testList.iterator();
        while (itTest.hasNext() && !cancelled) {
            Test test = itTest.next();
            belong = false;
            Element testElem = null;
            if (suiteElem != null) {
                Iterator<Element> itTestXML = suiteElem.selectNodes(
                                                                    "Tests/Test").iterator();
                while (itTestXML.hasNext() && !belong) {
                    testElem = itTestXML.next();
                    if (testElem.elementText("Nom").equals(
                                                           test.getNameFromModel())) {
                        if (isInitSelection()) {
                            Iterator<Test> itTestSelect = testSelectionList
                                .iterator();
                            boolean selectTest = false;
                            while (itTestSelect.hasNext()) {
                                Test testSelect = itTestSelect.next();
                                if (testSelect.getNameFromModel().equals(
                                                                         test.getNameFromModel())
                                    && testSelect
                                    .getTestListFromModel()
                                    .getNameFromModel()
                                    .equals(
                                            test
                                            .getTestListFromModel()
                                            .getNameFromModel())
                                    && testSelect
                                    .getTestListFromModel()
                                    .getFamilyFromModel()
                                    .getNameFromModel()
                                    .equals(
                                            test
                                            .getTestListFromModel()
                                            .getFamilyFromModel()
                                            .getNameFromModel())) {
                                    selectTest = true;
                                }
                            }
                            if (selectTest) {
                                belong = true;
                            }
                        } else {
                            belong = true;
                        }
                    }
                }
            }
            if (!belong) {
                testToRm.add(test);
            } else {
                // gestion des parametres
                ArrayList<Parameter> paramTestList = test
                    .getParameterListFromModel();
                List<Element> paramTestXML = testElem
                    .selectNodes("ParamsT/ParamT");
                ArrayList<Parameter> paramTestToRemove = new ArrayList<Parameter>();
                Iterator<Parameter> itParamTest = paramTestList.iterator();
                while (itParamTest.hasNext()) {
                    Parameter param = itParamTest.next();
                    belong = false;
                    Iterator<Element> itParamTestXML = paramTestXML.iterator();
                    while (itParamTestXML.hasNext()) {
                        Element paramElem = itParamTestXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (param.getNameFromModel().equals(nomParam)) {
                            belong = true;
                        }
                    }
                    if (!belong) {
                        paramTestToRemove.add(param);
                    }
                }
                Iterator<Parameter> itParamRm = paramTestToRemove.iterator();
                while (itParamRm.hasNext()) {
                    Parameter param = itParamRm.next();
                    try {
                        test.deleteUseParamInDBAndModel(param);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                // gestion des attachements
                attachmentDeletions(testElem, test);

                // si test automatique gestion du script
                if (test instanceof AutomaticTest) {
                    Script script = ((AutomaticTest) test).getScriptFromModel();
                    if (script != null) {
                        Element scriptElem = (Element) testElem
                            .selectSingleNode("TestAuto/Script");
                        if (scriptElem == null
                            || !scriptElem.attributeValue("nom").equals(
                                                                        script.getNameFromModel())) {
                            try {
                                ((AutomaticTest) test)
                                    .deleteScriptInDBAndModel();
                            } catch (Exception e) {
                                Tools.ihmExceptionView(e);
                            }
                        }
                    }
                } else {
                    // si test manuel gestion des actions
                    actionDeletion(testElem, (ManualTest) test);
                }
            }
        }
        Iterator<Test> itTestRm = testToRm.iterator();
        while (itTestRm.hasNext() && !cancelled) {
            Test test = (Test) itTestRm.next();
            try {
                suite.deleteTestInDBAndModel(test);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                cancelled = true;
            }
        }
    }

    /**
     * Delete actions of manual test and associated elements are in project
     * database and not in XML document.
     *
     * @param testElem
     *            concerned XML element
     * @param test
     *            manual test in DB
     */
    private void actionDeletion(Element testElem, ManualTest test)
        throws Exception {
        ArrayList<Action> actionList = test.getActionListFromModel(false);
        List<Element> actionListXML = testElem
            .selectNodes("TestManuel/ActionTest");
        ArrayList<Action> actionToRemove = new ArrayList<Action>();
        boolean belong;
        Iterator<Action> itAction = actionList.iterator();
        while (itAction.hasNext()) {
            Action action = itAction.next();
            Element actionElem = null;
            belong = false;
            Iterator<Element> itActionXML = actionListXML.iterator();
            while (itActionXML.hasNext() && !belong) {
                actionElem = (Element) itActionXML.next();
                if (actionElem.elementText("Nom").equals(
                                                         action.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                actionToRemove.add(action);
            } else {
                // gestion des parametres des actions
                Hashtable<String, Parameter> actionParam = action
                    .getParameterHashSetFromModel();
                Enumeration<Parameter> itParamAction = actionParam.elements();
                ArrayList<Parameter> paramActionToRemove = new ArrayList<Parameter>();
                while (itParamAction.hasMoreElements()) {
                    Parameter param = itParamAction.nextElement();
                    belong = false;
                    Iterator<Element> itParamActionXML = actionElem
                        .selectNodes("ParamsT/ParamT").iterator();
                    while (itParamActionXML.hasNext()) {
                        Element paramElem = itParamActionXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (param.getNameFromModel().equals(nomParam)) {
                            belong = true;
                        }
                    }
                    if (!belong) {
                        paramActionToRemove.add(param);
                    }
                }
                Iterator<Parameter> itParamActionRm = paramActionToRemove
                    .iterator();
                while (itParamActionRm.hasNext()) {
                    Parameter param = itParamActionRm.next();
                    try {
                        action.deleteUseParamInDBAndModel(param);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                // gestion des attachements des actions
                attachmentDeletions(actionElem, action);
            }

        }
        Iterator<Action> itActionRm = actionToRemove.iterator();
        while (itActionRm.hasNext()) {
            Action actionRm = itActionRm.next();
            try {
                test.deleteActionInDBModel(actionRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                cancelled = true;
            }
        }
    }

    /**
     * Delete attachments of the given element which are in database and not in
     * the XML document
     *
     * @param pDocElem
     *            XML element
     * @param pWithAttachment
     *            element with attachment in DB
     */
    private void attachmentDeletions(Element pDocElem,
                                     WithAttachment pWithAttachment) throws Exception {
        boolean belong;
        List<Node> urlAttachementList = pDocElem
            .selectNodes("Attachements/UrlAttachement");
        List<Node> fileAttachementList = pDocElem
            .selectNodes("Attachements/FileAttachement");
        HashMap<String, Attachment> envMap = pWithAttachment
            .getAttachmentMapFromModel();
        ArrayList<Attachment> envMapToRemove = new ArrayList<Attachment>();
        Iterator<Attachment> itWithAttachmentMap = envMap.values().iterator();
        while (itWithAttachmentMap.hasNext()) {
            belong = false;
            Attachment attach = itWithAttachmentMap.next();
            if (attach instanceof UrlAttachment) {
                UrlAttachment pUrlAttachment = (UrlAttachment) attach;
                Iterator<Node> itWithAttachmentUrlAttachXML = urlAttachementList
                    .iterator();
                while (itWithAttachmentUrlAttachXML.hasNext()
                       && belong == false) {
                    Element urlElem = (Element) itWithAttachmentUrlAttachXML
                        .next();
                    if (pUrlAttachment.getNameFromModel().equals(
                                                                 urlElem.attributeValue("url"))) {
                        belong = true;
                    }
                }
            } else {
                FileAttachment pFileAttachment = (FileAttachment) attach;
                Iterator<Node> itEnvFileAttachXML = fileAttachementList
                    .iterator();
                while (itEnvFileAttachXML.hasNext() && belong == false) {
                    Element fileElem = (Element) itEnvFileAttachXML.next();
                    if (pFileAttachment.getNameFromModel().equals(
                                                                  fileElem.attributeValue("nom"))) {
                        belong = true;
                    }
                }
            }
            if (!belong) {
                envMapToRemove.add(attach);
            }

        }
        for (Attachment attach : envMapToRemove) {
            try {
                pWithAttachment.deleteAttachementInDBAndModel(attach);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Delete data sets of the campaign which are in database and not in the XML
     * document
     *
     * @param pDocElem
     *            XML element
     * @param pWithAttachment
     *            element with attachment in DB
     */
    private void dataSetDeletion(Element campElem, Campaign camp) {
        ArrayList<DataSet> datasetList = camp.getDataSetListFromModel();
        ArrayList<DataSet> datasetToRemove = new ArrayList<DataSet>();
        List<Element> datatsetListXML = campElem
            .selectNodes("JeuxDonnees/JeuDonnees");
        Element jeuElem = null;
        Iterator<DataSet> itJeu = datasetList.iterator();
        boolean belong;
        while (itJeu.hasNext()) {
            DataSet jeu = itJeu.next();
            belong = false;
            Iterator<Element> itJeuXML = datatsetListXML.iterator();
            while (itJeuXML.hasNext()) {
                jeuElem = itJeuXML.next();
                if (jeuElem.elementText("Nom").equals(jeu.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                datasetToRemove.add(jeu);
            }
        }
        Iterator<DataSet> itDatasetRm = datasetToRemove.iterator();
        while (itDatasetRm.hasNext()) {
            DataSet jeuRm = itDatasetRm.next();
            try {
                camp.deleteDataSetInDBAndModel(jeuRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Supprime les tests d'une campagne qui sont presents dans le projet mais
     * pas dans le documents
     *
     * @param campElem
     * @param camp
     */
    private void testInCampDeletion(Element campElem, Campaign camp)
        throws Exception {
        ArrayList<Test> testList = camp.getTestListFromModel();
        ArrayList<Test> testToRemove = new ArrayList<Test>();
        List<Element> testListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef");
        Element testElem = null;
        Iterator<Test> itTest = testList.iterator();
        boolean belong;
        while (itTest.hasNext()) {
            Test test = itTest.next();
            String suiteName = test.getTestListFromModel().getNameFromModel();
            String familyName = test.getTestListFromModel()
                .getFamilyFromModel().getNameFromModel();
            belong = false;
            Iterator<Element> itTestXML = testListXML.iterator();
            while (itTestXML.hasNext() && !belong) {
                testElem = itTestXML.next();
                String nomSuite = ((Element) testElem
                                   .selectSingleNode("ancestor::SuiteTestRef[1]"))
                    .elementText("Nom");
                String nomFamille = ((Element) testElem
                                     .selectSingleNode("ancestor::FamilleRef[1]"))
                    .elementText("Nom");
                String nomTest = testElem.elementText("Nom");
                if (test.getNameFromModel().equals(nomTest)
                    && suiteName.equals(nomSuite)
                    && familyName.equals(nomFamille)) {
                    belong = true;
                }
            }
            if (!belong) {
                testToRemove.add(test);
            }
        }
        Iterator<Test> itTestRm = testToRemove.iterator();
        while (itTestRm.hasNext()) {
            Test testRm = (Test) itTestRm.next();
            try {
                camp.deleteTestFromCampInDBAndModel(testRm, true);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Delete executions which are in database and not in XML document
     *
     * @param campElem
     * @param camp
     */
    void execInCampDeletion(Element campElem, Campaign camp) throws Exception {
        ArrayList<Execution> execList = camp.getExecutionListFromModel();
        ArrayList<Execution> execToRemove = new ArrayList<Execution>();
        List<Node> execListXML = campElem
            .selectNodes("ExecCampTests/ExecCampTest");
        Element execElem = null;
        Iterator<Execution> itExec = execList.iterator();
        boolean belong;
        while (itExec.hasNext() && !cancelled) {
            Execution exec = itExec.next();
            String execName = exec.getNameFromModel();
            belong = false;
            Iterator<Node> itExecXML = execListXML.iterator();
            while (itExecXML.hasNext() && !belong) {
                execElem = (Element) itExecXML.next();
                String nomExec = execElem.elementText("Nom");
                if (nomExec.equals(execName)) {
                    belong = true;
                }
            }
            if (!belong) {
                execToRemove.add(exec);
            } else {
                // suppression des resultats d'execution
                ArrayList<ExecutionResult> resExecList = exec
                    .getExecutionResultListFromModel();
                ArrayList<ExecutionResult> resExecToRemove = new ArrayList<ExecutionResult>();
                List<Node> resExecListXML = execElem
                    .selectNodes("ResulExecCampTests/ResulExecCampTest");
                Element resExecElem = null;
                Iterator<ExecutionResult> itResExec = resExecList.iterator();
                while (itResExec.hasNext()) {
                    ExecutionResult resExec = itResExec.next();
                    String resExecName = resExec.getNameFromModel();
                    belong = false;
                    Iterator<Node> itResExecXML = resExecListXML.iterator();
                    while (itResExecXML.hasNext() && !belong) {
                        resExecElem = (Element) itResExecXML.next();
                        String nomResExec = resExecElem.elementText("Nom");
                        if (nomResExec.equals(resExecName)) {
                            belong = true;
                        }
                    }
                    if (!belong) {
                        resExecToRemove.add(resExec);
                    }
                }
                Iterator<ExecutionResult> itResExecRm = resExecToRemove
                    .iterator();
                while (itResExecRm.hasNext()) {
                    ExecutionResult resExecRm = itResExecRm.next();
                    try {
                        exec.deleteExecutionResultInDBAndModel(resExecRm);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
        Iterator<Execution> itExecRm = execToRemove.iterator();
        while (itExecRm.hasNext() && !cancelled) {
            Execution execRm = itExecRm.next();
            try {
                camp.deleteExecutionInDBAndModel(execRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Method to manage the import process
     */
    private void coreImport() {

        try {

            /***** Plugin update globale data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateProjectFromXML(doc, idialog
                                                           .isSupprElement(), project, this);
                } else {
                    pXMLPrinterPlugin.updateProjectFromXML(doc, false, project,
                                                           this);
                }
            }
            /***** (END) Plugin update globale data *****/
            // Attachements du projet
            updateProjectAttachement();
            // Parametres du projet
            updateProjectParameter();
            // Environnement du projet
            updateProjectEnvironment();
            // Arbre des test
            updateFamily();
            if (importCampaign && !cancelled) {
                updateCampaigns();
            }
            updatePlugin();

        } catch (Exception e) {
            Tools.ihmExceptionView(e);
            cancelled = true;
            return;
        }
    }

    /**
     * Update attachments of project
     */
    private void updateProjectAttachement() throws Exception {
        Element projectElem = (Element) doc.selectSingleNode("//ProjetVT");
        updateElementAttachement(projectElem, project, false);
    }

    /**
     * Update parameters of project
     *
     * @throws Exception
     */
    private void updateProjectParameter() throws Exception {
        ArrayList<Node> paramList = (ArrayList<Node>) doc
            .selectNodes("//Param");
        Iterator<Node> itParam = paramList.iterator();
        while (itParam.hasNext() && !cancelled) {
            Element paramElem = (Element) itParam.next();
            String nomParam = paramElem.elementText("Nom");
            String descriptionParam = (paramElem.elementText("Description") == null) ? ""
                : paramElem.elementText("Description");
            descriptionParam = descriptionParam.replaceAll("\\\\n", "\n");
            Parameter newParamter = new Parameter(nomParam, descriptionParam);
            boolean existe = false;
            Parameter par = null;
            Enumeration<Parameter> itParam2 = project
                .getParameterSetFromModel().elements();
            while (itParam2.hasMoreElements() && !existe) {
                par = itParam2.nextElement();
                if (par.getNameFromModel().equals(nomParam)) {
                    existe = true;
                }
            }

            if (!existe) {
                // Ajout du nouveau parametre
                project.addParameterToDBAndModel(newParamter);
                par = newParamter;
            } else {
                // Modification du parametre
                par.updateDescriptionInDBAndModel(descriptionParam);
            }
            /***** Plugin update parameter data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateParameterFromXML(paramElem, par,
                                                             idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateParameterFromXML(paramElem, par,
                                                             false, this);
                }
            }
            /***** (END) Plugin update parameter data *****/
        }
    }

    /**
     * Add environment in project if it does not exist in database, if it
     * already exists, update this environment
     *
     * @throws Exception
     */
    private void updateProjectEnvironment() throws Exception {
        // Environnements du projet
        Element paramsElem = (Element) doc.selectSingleNode("//Params");
        ArrayList<Node> envList = (ArrayList<Node>) doc
            .selectNodes("//Environnement");
        ArrayList<Environment> envInBDD = project.getEnvironmentListFromModel();
        Iterator<Node> itEnv = envList.iterator();
        while (itEnv.hasNext() && !cancelled) {
            Element envElem = (Element) itEnv.next();
            String nomEnv = envElem.elementText("Nom");
            String descriptionEnv = (envElem.elementText("Description") == null) ? ""
                : envElem.elementText("Description");
            descriptionEnv = descriptionEnv.replaceAll("\\\\n", "\n");
            Iterator<Environment> itEnvBDD = envInBDD.iterator();
            Environment env = null;
            boolean existeEnv = false;
            while (itEnvBDD.hasNext() && !existeEnv) {
                env = itEnvBDD.next();
                if (env.getNameFromModel().equals(nomEnv)) {
                    existeEnv = true;
                }
            }
            boolean newEnv = false;
            if (!existeEnv) {
                env = null;
                env = new Environment(nomEnv, descriptionEnv);
                project.addEnvironmentInDBAndModel(env);
                newEnv = true;
            } else {
                env.updateInDBAndModel(nomEnv, descriptionEnv);
            }
            // Valeurs des parametres
            List<Node> paramEnvList = envElem
                .selectNodes("ValeurParams/ValeurParam");
            Iterator<Node> itParamEnv = paramEnvList.iterator();
            while (itParamEnv.hasNext()) {
                Element paramElem = (Element) itParamEnv.next();
                String valeurParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                    .attributeValue("valeur")
                    : "";
                String id_paramDoc = paramElem.attributeValue("ref");
                Element paramRootElem = (Element) paramsElem
                    .selectSingleNode("Param[@id_param = '" + id_paramDoc
                                      + "']");
                String nomParam = paramRootElem.elementText("Nom");
                // String descriptionParam =
                // (paramRootElem.elementText("Description") ==
                // null)?"":paramRootElem.elementText("Description");
                boolean existe = false;
                Parameter paramEnv = null;
                if (env != null && newEnv == false) {
                    Hashtable<Parameter, String> paramEnvInBDD = env
                        .getParametersHashTableFromModel();
                    Enumeration<Parameter> enumParamEnv = paramEnvInBDD.keys();
                    while (enumParamEnv.hasMoreElements() && !existe) {
                        paramEnv = enumParamEnv.nextElement();
                        if (nomParam.equals(paramEnv.getNameFromModel())) {
                            existe = true;
                        }
                    }
                }
                if (!existe) {
                    paramEnv = project.getParameterFromModel(nomParam);
                    if (paramEnv != null) {
                        env.addParameterValueInDBModel(paramEnv, valeurParam);
                    }
                } else {
                    env.addParameterValueInDBModel(paramEnv, valeurParam);
                }
            }
            // Ajout ou mise a jour du script de l'environnement
            if (envElem.element("Script") != null) {
                String classpath = envElem.element("Script").elementText(
                                                                         "Classpath");
                String argScript = envElem.element("Script").elementText(
                                                                         "ArgScript");
                String type = envElem.element("Script").attributeValue("type");
                String dirScript = envElem.element("Script").attributeValue(
                                                                            "dir");
                dirScript = restorePath(dirScript);
                File fScript = new File(dirXml + File.separator + dirScript);

                Script pScritp = new Script(fScript.getName(), "");
                pScritp.setTypeInModel(type);
                pScritp.setScriptExtensionInModel(classpath);
                pScritp.updatePlugArgInModel(argScript);
                try {
                    if (newEnv == true) {
                        env.addScriptInDBAndModel(pScritp, fScript);
                    } else {
                        env.deleteScriptInDBAndModel();
                        env.addScriptInDBAndModel(pScritp, fScript);
                    }
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            }
            updateElementAttachement(envElem, env, newEnv);

            /***** Plugin update Env data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateEnvironmentFromXML(envElem, env,
                                                               idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateEnvironmentFromXML(envElem, env,
                                                               false, this);
                }
            }
            /***** (END) Plugin update Env data *****/
        }
    }

    /**
     * Add family if it does not exist in database, update it otherwise
     *
     * @throws Exception
     */
    private void updateFamily() throws Exception {
        ArrayList<Node> familyList = (ArrayList<Node>) doc
            .selectNodes("//Famille");
        ArrayList<Family> familleListModel = project.getFamilyListFromModel();
        if (familyList != null) {
            Iterator<Node> it = familyList.iterator();
            while (it.hasNext() && !cancelled) {
                Element familyElem = (Element) it.next();
                String familyName = familyElem.elementText("Nom");
                String familyDescription = (familyElem
                                            .elementText("Description") == null) ? "" : familyElem
                    .elementText("Description");
                if (familyElem.element("Description") != null
                    && familyElem.element("Description")
                    .attribute("isHTML") != null
                    && familyElem.element("Description").attributeValue(
                                                                        "isHTML").equals("true")) {
                    familyDescription = giveHTMLDescription(familyElem);
                } else {
                    familyDescription = familyDescription.replaceAll("\\\\n",
                                                                     "<br>");
                    familyDescription = "<html><head></head><body>"
                        + familyDescription + "</body></html>";
                }
                boolean selectionne = false;
                if (initSelection) {
                    Iterator<Family> itFamily = familySelectionList.iterator();
                    while (itFamily.hasNext()) {
                        Family family = itFamily.next();
                        if (familyName.equals(family.getNameFromModel())) {
                            selectionne = true;
                        }
                    }
                }
                if (!initSelection || selectionne) {
                    boolean appartient = false;
                    Family fam = null;
                    boolean newFamily = false;
                    Iterator<Family> itFam = familleListModel.iterator();
                    while (itFam.hasNext() && !appartient) {
                        fam = itFam.next();
                        if (fam.getNameFromModel().equals(familyName)) {
                            appartient = true;
                        }
                    }
                    try {
                        if (!appartient) {
                            fam = new Family(familyName, familyDescription);
                            project.addFamilyInDBAndModel(fam);
                            newFamily = true;
                        } else {
                            fam.updateDescriptionInModel(familyDescription);
                        }

                        // updatesuite
                        updateElementAttachement(familyElem, fam, newFamily);

                        /***** Plugin update FamilyData data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateFamilyFromXML(
                                                                      familyElem, fam, idialog
                                                                      .isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateFamilyFromXML(
                                                                      familyElem, fam, false, this);
                            }
                        }
                        /***** (END) Plugin update FamilyData data *****/

                        updateSuite(familyElem, fam, newFamily);

                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    /**
     *
     * @param familyElem
     * @param pFamily
     * @param newFamily
     * @throws Exception
     */
    private void updateSuite(Element familyElem, Family pFamily,
                             boolean newFamily) throws Exception {
        ArrayList<Node> suiteList = (ArrayList<Node>) familyElem
            .selectNodes(".//SuiteTest");
        if (suiteList != null) {
            Iterator<Node> it2 = suiteList.iterator();
            while (it2.hasNext() && !cancelled) {
                Element suiteElem = (Element) it2.next();
                String suiteName = suiteElem.elementText("Nom");
                String suiteDescription = (suiteElem.elementText("Description") == null) ? ""
                    : suiteElem.elementText("Description");
                if (suiteElem.element("Description") != null
                    && suiteElem.element("Description").attribute("isHTML") != null
                    && suiteElem.element("Description").attributeValue(
                                                                       "isHTML").equals("true")) {
                    suiteDescription = giveHTMLDescription(suiteElem);
                } else {
                    suiteDescription = suiteDescription.replaceAll("\\\\n",
                                                                   "<br>");
                    suiteDescription = "<html><head></head><body>"
                        + suiteDescription + "</body></html>";
                }
                // boolean erreur = false;
                boolean selectionneSuite = false;
                if (isInitSelection()) {
                    Iterator<TestList> itSuite = suiteSelectionList.iterator();
                    while (itSuite.hasNext()) {
                        TestList suite = itSuite.next();
                        if (suiteName.equals(suite.getNameFromModel())) {
                            selectionneSuite = true;
                        }
                    }
                }
                boolean newSuite = false;
                if (!isInitSelection() || selectionneSuite) {
                    boolean appartient = false;
                    TestList suite = null;
                    if (pFamily != null && newFamily == false) {
                        ArrayList<TestList> suiteTestList = pFamily
                            .getSuiteListFromModel();
                        Iterator<TestList> itSuite = suiteTestList.iterator();
                        while (itSuite.hasNext() && !appartient) {
                            suite = itSuite.next();
                            if (suite.getNameFromModel().equals(suiteName)) {
                                appartient = true;
                            }
                        }
                    }

                    try {
                        if (!appartient) {
                            suite = new TestList(suiteName, suiteDescription);
                            pFamily.addTestListInDBAndModel(suite);
                            newSuite = true;
                        } else {
                            suite.updateInDBAndModel(suiteName,
                                                     suiteDescription);
                        }
                        // gerer les attachements
                        updateElementAttachement(suiteElem, suite, newSuite);

                        /***** Plugin update Suite Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateSuiteFromXML(suiteElem,
                                                                     suite, idialog.isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateSuiteFromXML(suiteElem,
                                                                     suite, false, this);
                            }
                        }
                        /***** (END) Plugin update Suite Data *****/

                        // les tests
                        updateTests(suiteElem, pFamily, suite, newFamily,
                                    newSuite);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    /**
     * Manage insertion and update of tests in database from XML file
     *
     * @param suiteElem
     * @param fam
     * @param suite
     * @param newFamily
     * @param newSuite
     */
    private void updateTests(Element suiteElem, Family fam, TestList suite,
                             boolean newFamily, boolean newSuite) throws Exception {
        List<Element> testList = suiteElem.selectNodes(".//Test");
        if (testList == null) {
            return;
        }

        Iterator<Element> it = testList.iterator();
        while (it.hasNext() && !cancelled) {
            try {
                Element testElem = it.next();
                String testName = testElem.elementText("Nom");
                boolean newTest = false;
                boolean selectionneTest = false;
                if (isInitSelection()) {
                    Iterator<Test> itTest = testSelectionList.iterator();
                    while (itTest.hasNext()) {
                        Test test = (Test) itTest.next();
                        if (testName.equals(test.getNameFromModel())) {
                            selectionneTest = true;
                        }
                    }
                }
                if (!isInitSelection() || selectionneTest) {
                    // Verification de l'existence ou non du test
                    boolean appartient = false;
                    Test test = null;
                    if (suite != null) {
                        ArrayList<Test> testListBDD = suite
                            .getTestListFromModel();
                        Iterator<Test> itTest = testListBDD.iterator();
                        while (itTest.hasNext() && !appartient) {
                            test = itTest.next();
                            if (test.getNameFromModel().equals(testName)) {
                                appartient = true;
                            }
                        }
                    }
                    // recuperation des donnees dans le document
                    String loginConceptor = "";
                    // User pUser;
                    if (!appartient) {
                        loginConceptor = getLogin(testElem);
                        if (loginConceptor == null) {
                            if (!newProject) {
                                loginConceptor = DataModel.getCurrentUser()
                                    .getLoginFromModel();
                            } else {
                                loginConceptor = project
                                    .getAdministratorFromModel()
                                    .getLoginFromModel();
                            }
                        }
                    } else {
                        loginConceptor = test.getConceptorLoginFromModel();
                    }
                    String testDescription = (testElem
                                              .elementText("Description") == null) ? ""
                        : testElem.elementText("Description");
                    if (testElem.element("Description") != null
                        && testElem.element("Description").attribute(
                                                                     "isHTML") != null
                        && testElem.element("Description").attributeValue(
                                                                          "isHTML").equals("true")) {
                        testDescription = giveHTMLDescription(testElem);
                    } else {
                        testDescription = testDescription.replaceAll("\\\\n",
                                                                     "<br>");
                        testDescription = "<html><head></head><body>"
                            + testDescription + "</body></html>";
                    }

                    String testType;
                    String plug_ext;
                    if (testElem.element("TestAuto") == null) {
                        testType = "MANUAL";
                        plug_ext = "";
                    } else {
                        testType = "AUTOMATED";
                        String plug = testElem.element("TestAuto")
                            .attributeValue("plug_ext");
                        if (plug != null) {
                            plug_ext = plug;
                        } else {
                            plug_ext = "";
                        }
                    }
                    if (!appartient) {

                        newTest = true;
                        if (testType.equals("AUTOMATED")) {
                            test = new AutomaticTest(testName, testDescription,
                                                     plug_ext);
                        } else {
                            test = new ManualTest(testName, testDescription);
                        }
                        test.setConceptorLoginInModel(loginConceptor);
                        suite.addTestInDBAndModel(test);

                        updateElementAttachement(testElem, test, newTest);

                        updateParametresTest(testElem, test);

                        /***** Plugin update Test Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, idialog.isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, false, this);
                            }
                        }
                        /***** (END) Plugin update Test Data *****/

                        if (testType == "AUTOMATED") {
                            updateTestScript(testElem, (AutomaticTest) test);
                            automatedTest2Update.add((AutomaticTest) test);
                        } else {
                            ajouterActions(testElem, (ManualTest) test);
                        }

                    } else { // test already present in DB
                        boolean noupdate = true;
                        Test testDB = project.getTestFromModel(fam
                                                               .getNameFromModel(), suite.getNameFromModel(),
                                                               testName);
                        boolean isDifferent = detecterModif(fam, suite,
                                                            testElem);
                        if (!idialog.isKeepOriginalOption() && isDifferent) {
                            Test testToUpdate = null;
                            if (idialog.isKeepOriginalAndImportOption()
                                || idialog.isKeepOriginalAndUpdateOption()) {
                                String newName = "save_" + testName;
                                int i = 0;
                                while (Test.isInBase(suite, newName)) {
                                    newName = newName + "_" + i;
                                    i++;
                                }
                                testDB.updateInDBAndModel(newName, testDB
                                                          .getDescriptionFromModel());
                                savedTests.add(testDB);
                                if (idialog.isKeepOriginalAndUpdateOption()) {
                                    if (testDB instanceof ManualTest) {
                                        testToUpdate = ManualTest
                                            .createCopyInDBAndModel(
                                                                    (ManualTest) testDB,
                                                                    testName);
                                    } else if (testDB instanceof AutomaticTest) {
                                        testToUpdate = AutomaticTest
                                            .createCopyInDBAndModel(
                                                                    (AutomaticTest) testDB,
                                                                    testName);
                                    }
                                }
                            } else if (idialog.isEraseOriginalAndImport()) {
                                project.deleteTestInDBandModel(testDB);
                            }
                            if (idialog.isKeepOriginalAndImportOption()
                                || idialog.isEraseOriginalAndImport()) {
                                // import test from XML
                                newTest = true;
                                if (testType.equals("AUTOMATED")) {
                                    test = new AutomaticTest(testName,
                                                             testDescription, plug_ext);
                                } else {
                                    test = new ManualTest(testName,
                                                          testDescription);
                                }
                                test.setConceptorLoginInModel(loginConceptor);
                                suite.addTestInDBAndModel(test);

                                updateElementAttachement(testElem, test,
                                                         newTest);

                                updateParametresTest(testElem, test);

                                /***** Plugin update Test Data *****/
                                int size = listXMLPlugin.size();
                                for (int i = 0; i < size; i++) {
                                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                        .elementAt(i);
                                    if (idialog != null) {
                                        pXMLPrinterPlugin
                                            .updateTestFromXML(
                                                               testElem,
                                                               test,
                                                               idialog
                                                               .isSupprElement(),
                                                               this);
                                    } else {
                                        pXMLPrinterPlugin.updateTestFromXML(
                                                                            testElem, test, false, this);
                                    }
                                }
                                /***** (END) Plugin update Test Data *****/

                                if (testType == "AUTOMATED") {
                                    updateTestScript(testElem,
                                                     (AutomaticTest) test);
                                    automatedTest2Update
                                        .add((AutomaticTest) test);
                                } else {
                                    ajouterActions(testElem, (ManualTest) test);
                                }
                                noupdate = false;
                            } else if (idialog.isKeepOriginalAndUpdateOption()
                                       || idialog.isUpdateOriginalOption()) {
                                if (idialog.isUpdateOriginalOption()) {
                                    testToUpdate = testDB;
                                }
                                // update testToUpdate
                                testToUpdate.updateInModel(testName,
                                                           testDescription);
                                updateElementAttachement(testElem,
                                                         testToUpdate, newTest);
                                updateParametresTest(testElem, testToUpdate);
                                /***** Plugin update Test Data *****/
                                for (XMLPrinterPlugin pXMLPrinterPlugin : listXMLPlugin) {
                                    pXMLPrinterPlugin.updateTestFromXML(
                                                                        testElem, testToUpdate, idialog
                                                                        .isSupprElement(), this);
                                }
                                /***** (END) Plugin update Test Data *****/
                                if (testType == "AUTOMATED") {
                                    updateTestScript(testElem,
                                                     (AutomaticTest) testToUpdate);
                                    Util.log("[ImportDialog] Add test auto "
                                             + testToUpdate.getIdBdd());
                                    automatedTest2Update
                                        .add((AutomaticTest) testToUpdate);
                                } else {
                                    ManualTest manualTest = (ManualTest) testToUpdate;
                                    ArrayList<Action> actionList = manualTest
                                        .getActionListFromModel(false);
                                    ArrayList<String> actionToRemove = new ArrayList<String>();
                                    for (Action action : actionList) {
                                        if (!actionToRemove.contains(action
                                                                     .getNameFromModel()))
                                            actionToRemove.add(action
                                                               .getNameFromModel());
                                    }
                                    for (String actionName : actionToRemove) {
                                        manualTest
                                            .deleteActionInDBModel(manualTest
                                                                   .getActionFromModel(actionName));
                                    }
                                    ajouterActions(testElem,
                                                   (ManualTest) testToUpdate);
                                }
                                noupdate = false;
                            }
                        }
                        if (noupdate) {/*
                                        * Si aucune modification sur l'element
                                        * on appelle les plugins
                                        */

                            /***** Plugin update Test Data *****/
                            for (XMLPrinterPlugin pXMLPrinterPlugin : listXMLPlugin) {
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    testDB, idialog.isSupprElement(), this);
                            }
                            /***** (END) Plugin update Test Data *****/
                        }
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Dans le cas ou le test etait deja dans la BD, ajout ou mise a jour du
     * script
     *
     * @param familyName
     *            nom de la famille auquelle appartient le test
     * @param suiteName
     *            nom de la suite auquelle appartient le test
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void updateTestScript(Element testElem, AutomaticTest test)
        throws Exception {
        Element scriptElem = (Element) testElem
            .selectSingleNode("TestAuto/Script");
        try {
            if (scriptElem != null) {
                String classpath = scriptElem.elementText("Classpath");
                String argScript = scriptElem.elementText("ArgScript");
                String type = scriptElem.attributeValue("type");
                String dirScript = scriptElem.attributeValue("dir");
                dirScript = restorePath(dirScript);
                File fScript = new File(dirXml + File.separator + dirScript);

                Script pScritp = new Script(fScript.getName(), "");
                pScritp.setTypeInModel(type);
                pScritp.setScriptExtensionInModel(classpath);
                pScritp.updatePlugArgInModel(argScript);

                if (test != null) {
                    if (test.getScriptFromModel() != null) {
                        test.deleteScriptInDBAndModel();
                    }
                }
                test.addScriptInDBAndModel(pScritp, fScript);
            } else {
                if (test.getScriptFromModel() != null) {
                    test.deleteScriptInDBAndModel();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            idialog.setErrorMessage(idialog.getErrorMessage()
                                    + Language.getInstance().getText(
                                                                     "Probleme_lors_de_la_suppression_d_un_script"));
            idialog.showErrorMessage();
        }
    }

    /**
     * On cherche a savoir si le test du projet possede des differences avec le
     * test present dans le fichier XML, ces differences generant un conflit
     *
     * @param familyName
     *            nom de la famille auquelle appartient le test
     * @param suiteName
     *            nom de la suite auquelle appartient le test
     * @param testElem
     *            element test du document XML
     * @return true si reel conflit, false sinon
     */
    public boolean detecterModif(Family pFamily, TestList suite,
                                 Element testElem) throws Exception {
        // on verifie que tous les parametres qui sont dans le document d'import
        // sont dans le projet
        String testName = testElem.elementText("Nom");
        Test test = project.getTestFromModel(pFamily.getNameFromModel(), suite
                                             .getNameFromModel(), testName);
        List<Element> paramsImport = testElem.selectNodes("ParamsT/ParamT/Nom");
        ArrayList<Parameter> paramsProjet = test.getParameterListFromModel();
        Iterator<Element> itImport = paramsImport.iterator();
        while (itImport.hasNext()) {
            String nomParamImport = ((Element) itImport.next()).getText();
            boolean appartient = false;
            Iterator<Parameter> itParamProjet = paramsProjet.iterator();
            while (itParamProjet.hasNext()) {
                Parameter param = itParamProjet.next();
                if (param.getNameFromModel().equals(nomParamImport)) {
                    appartient = true;
                }
            }
            if (!appartient) {
                return true;
            }
        }
        String descriptionXML = "";
        Element descElem = testElem.element("Description");
        if (descElem != null && descElem.hasContent()) {
            descriptionXML = testElem.element("Description").asXML();
            descriptionXML = getTextFromChildren(testElem
                                                 .element("Description"), "");
        }
        String descriptionDB = test.getDescriptionFromModel();
        if (descriptionDB != null && !descriptionDB.equals("")) {
            descriptionDB = descriptionDB.replaceAll("<br>", "<br/>");
            if (!descriptionDB.startsWith("<html>")) {
                descriptionDB = "<html>" + descriptionDB + "</html>";
            }
            Document document = DocumentHelper.parseText(descriptionDB);
            descriptionDB = getTextFromChildren(document.getRootElement(), "");
        }
        if (descriptionXML == null)
            descriptionXML = "";
        if (descriptionDB == null)
            descriptionDB = "";
        if (!descriptionXML.equals(descriptionDB)) {
            return true;
        }
        // on verifie que tous les parametres qui sont dans le projet sont bien
        // dans le document d'import
        Iterator<Parameter> itProj = paramsProjet.iterator();
        while (itProj.hasNext()) {
            Parameter param = itProj.next();
            boolean appartient = false;
            Iterator<Element> itDonnees = paramsImport.iterator();
            while (itDonnees.hasNext()) {
                String nomParam = (itDonnees.next()).getText();
                if (nomParam.equals(param.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                return true;
            }
        }
        // Verification des actions si parametres ok
        if (test instanceof ManualTest) {
            // detection de modifications dans les actions
            ManualTest testManuel = (ManualTest) test;
            List<Element> actionList = testElem.selectNodes(".//ActionTest");
            Iterator<Element> itAction = actionList.iterator();
            while (itAction.hasNext()) {
                Element actionElem = itAction.next();
                String nomAction = actionElem.elementText("Nom");
                String descriptionAction = (actionElem
                                            .elementText("Description") == null) ? "" : actionElem
                    .elementText("Description");
                String resulAttendu = (actionElem.elementText("ResultAttendu") == null) ? ""
                    : actionElem.elementText("ResultAttendu");
                descriptionAction = descriptionAction.replaceAll("\\\\n", "\n");
                resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");

                Action pAction = testManuel.getActionFromModel(nomAction);
                if (pAction == null) {
                    return true;
                } else {
                    if (!pAction.getDescriptionFromModel().trim().equals(
                                                                         descriptionAction)
                        || !pAction.getAwaitedResultFromModel().trim()
                        .equals(resulAttendu)) {
                        return true;
                    }
                    if (existModifInParamsAction(actionElem, pAction)) {
                        return true;
                    }
                }
            }

            // verifier que toutes les actions qui sont dans le projet sont dans
            // le fichier XML
            ArrayList<Action> actionsProjet = testManuel
                .getActionListFromModel(false);
            for (Action action : actionsProjet) {
                boolean appartient = false;
                Iterator<Element> itDonnees = actionList.iterator();
                while (itDonnees.hasNext()) {
                    Element actionElem = itDonnees.next();
                    String nomAction = actionElem.elementText("Nom");
                    if (action.getNameFromModel().equals(nomAction)) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    return true;
                }
            }
        }

        // verification des attachements
        List<Element> urlAttachementList = testElem
            .selectNodes("Attachements/UrlAttachement");
        HashMap<String, Attachment> envMap = test.getAttachmentMapFromModel();
        // verifier que tous les url attachements qui sont dans le XML sont
        // aussi dans la DB
        for (Element urlElem : urlAttachementList) {
            String url = urlElem.attributeValue("url");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof UrlAttachment)
                    && ((UrlAttachment) attach).getNameFromModel().equals(
                                                                          url)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verifier que tous les url attachements qui sont dans la DB sont aussi
        // dans le XML
        for (Attachment attach : envMap.values()) {
            if (attach instanceof UrlAttachment) {
                boolean belong = false;
                String url = ((UrlAttachment) attach).getNameFromModel();
                for (Element urlElem : urlAttachementList) {
                    if (url.equals(urlElem.attributeValue("url"))) {
                        belong = true;
                    }
                }
                if (!belong) {
                    return true;
                }
            }
        }

        // verification que tous les file attachments qui sont dans le XML sont
        // aussi dans la DB
        List<Element> fileAttachmentList = testElem
            .selectNodes("Attachements/FileAttachement");
        for (Element fileElem : fileAttachmentList) {
            String nom = fileElem.attributeValue("nom");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof FileAttachment)
                    && ((FileAttachment) attach).getNameFromModel().equals(
                                                                           nom)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verification que tous les file attachment qui sont dans la bd sont
        // aussi dans le document XML
        for (Attachment attach : envMap.values()) {
            String nomBD = ((FileAttachment) attach).getNameFromModel();
            boolean belong = false;
            for (Element fileElem : fileAttachmentList) {
                if (nomBD.equals(fileElem.attributeValue("nom"))) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }

        // verification des scripts des tests automatiques
        if (test instanceof AutomaticTest) {
            AutomaticTest testAuto = (AutomaticTest) test;
            Script script = testAuto.getScriptFromModel();
            if (script != null) {
                // calculate md5 from file from DB
                Element scriptElem = (Element) testElem
                    .selectSingleNode("TestAuto/Script");
                if (scriptElem == null) {
                    return true;
                }
                File tempScriptFile = script.getFileFromDB(null);
                File fromXMLScriptFile = new File(dirXml + File.separator
                                                  + scriptElem.attributeValue("dir"));
                String md5DBScript = getDigest(tempScriptFile);
                String md5XMLScript = getDigest(fromXMLScriptFile);
                if (!md5DBScript.equals(md5XMLScript)) {
                    return true;
                }
                tempScriptFile.delete();
            } else {
                Element scriptElem = (Element) testElem
                    .selectSingleNode("TestAuto/Script");
                if (scriptElem != null) {
                    return true;
                }
            }
        }
        return false;
    }

    private String getTextFromChildren(Element testElem, String txt) {
        String text = txt + testElem.getTextTrim();
        Iterator<Element> it = testElem.elementIterator();
        while (it.hasNext()) {
            Element elem = it.next();
            text += getTextFromChildren(elem, text);
        }
        return text;
    }

    /**
     * Gives MD5 of the given file
     *
     * @param f
     * @return
     */
    public String getDigest(File f) {
        String digest = null;
        if (f != null) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(f);
                digest = Digest.runMD5(fis);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return digest;
    }

    /**
     * Methode qui evalue si il y a une difference entre les parametres d'une
     * action dans le document XML et ceux de cette meme action dans le projet
     * Salome
     *
     * @param actionElem
     *            element du document DOM4J contenant l'action
     * @param action
     *            action dans le modele de SalomeTMF
     * @return
     */
    public boolean existModifInParamsAction(Element actionElem, Action action)
        throws Exception {
        List<Element> paramsImport = actionElem
            .selectNodes("ParamsT/ParamT/Nom");
        Hashtable<String, Parameter> paramsProjet = action
            .getParameterHashSetFromModel();
        // on verifie que tous les parametres qui sont dans le document d'import
        // sont bien dans le projet
        for (Element paramElem : paramsImport) {
            String nomParamImport = paramElem.getText();
            if (action.getParameterFromModel(nomParamImport) == null) {
                return true;
            }
        }
        // on verifie que tous les parametres qui sont dans le projet sont bien
        // dans le document d'import
        Enumeration<Parameter> itProj = paramsProjet.elements();
        while (itProj.hasMoreElements()) {
            Parameter param = itProj.nextElement();
            boolean appartient = false;
            for (Element paramElem : paramsImport) {
                String nomParam = paramElem.getText();
                if (nomParam.equals(param.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                return true;
            }
        }
        return false;
    }

    /**
     * Dans le cas ou le test etait deja dans la BD, ajout des parametres
     *
     * @param test
     *            le test concerne
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void updateParametresTest(Element testElem, Test test)
        throws Exception {
        // on ajoute les parametres que si ils n'existent pas deja dans la bd
        List<Element> paramList = testElem.selectNodes("ParamsT/ParamT/Nom");
        Iterator<Element> itParam = paramList.iterator();
        while (itParam.hasNext()) {
            String nomParam = (itParam.next()).getText();
            boolean appartient = false;
            Parameter param = null;
            if (test != null) {
                ArrayList<Parameter> paramListBDD = test
                    .getParameterListFromModel();
                Iterator<Parameter> itparam = paramListBDD.iterator();
                while (itparam.hasNext() && !appartient) {
                    param = itparam.next();
                    if (param.getNameFromModel().equals(nomParam)) {
                        appartient = true;
                    }
                }
            }
            try {
                if (!appartient) {
                    param = project.getParameterFromModel(nomParam);
                    if (param != null) {
                        test.setUseParamInDBAndModel(param);
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    private String getLogin(Element pElem) throws Exception {
        String login = pElem.element("Concepteur").elementText("Login");
        if (project.containUser(login) != -1) {
            return login;
        } else {
            return null;
        }
    }

    /**
     * Transform description in XML in description compatible with HTML
     * description
     *
     * @param elem
     * @return
     */
    private String giveHTMLDescription(Element elem) {
        String description = elem.element("Description").asXML();
        description = description.substring(27, description
                                            .indexOf("</Description>"));
        description = description.replaceAll("<br/>", "<br>");
        description = "<html><head></head><body>" + description
            + "</body></html>";
        return description;
    }

    void updatePlugin() throws Exception {
        // gestion des drivers des scripts des tests automatiques
        for (AutomaticTest test : automatedTest2Update) {
            try {
                TestDriver driver = null;
                if (idialog != null) {
                    driver = ((AutomaticTest) test).ActivateExtention(
                                                                      (Extension) pIPlugObject
                                                                      .getAssociatedExtension(test
                                                                                              .getExtensionFromModel()),
                                                                      pIPlugObject.getUrlBase(), pIPlugObject
                                                                      .getPluginManager());
                }
                if (test.getScriptFromModel() != null && driver != null) {
                    File file = ((AutomaticTest) test).getTestScriptFromDB();
                    if (file != null) {
                        driver.updateTestScriptFromImport(file
                                                          .getAbsolutePath(), (AutomaticTest) test);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                idialog
                    .setErrorMessage(idialog.getErrorMessage()
                                     + Language
                                     .getInstance()
                                     .getText(
                                              "Probleme_lors_de_la_mise_a_jour_des_drivers_des_scripts"));
                idialog.showErrorMessage();
            }
        }
    }

    /**
     * Update attachments of an element
     */
    public void updateElementAttachement(Element element, WithAttachment data,
                                         boolean isNewElement) {
        try {
            // URL
            List<Node> urlAttachementList = element
                .selectNodes("Attachements/UrlAttachement");
            Iterator<Node> itEnvUrlAttach = urlAttachementList.iterator();
            while (itEnvUrlAttach.hasNext()) {
                Element urlElem = (Element) itEnvUrlAttach.next();
                String url = urlElem.attributeValue("url");
                String description = (urlElem.elementText("Description") == null) ? ""
                    : urlElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (isNewElement == false) {
                    HashMap<String, Attachment> envMap = data
                        .getAttachmentMapFromModel();
                    Iterator<Attachment> itEnvAttachs = envMap.values()
                        .iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = itEnvAttachs.next();
                        if ((attach instanceof UrlAttachment)
                            && ((UrlAttachment) attach).getNameFromModel()
                            .equals(url)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    data.addAttachementInDBAndModel(new UrlAttachment(url,
                                                                      description));
                } else {
                    ((UrlAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }
            }
            // fichiers
            List<Node> envFileAttachementList = element
                .selectNodes("Attachements/FileAttachement");
            Iterator<Node> itEnvFileAttach = envFileAttachementList.iterator();
            while (itEnvFileAttach.hasNext()) {
                Element fileElem = (Element) itEnvFileAttach.next();
                String dirAtt = fileElem.attributeValue("dir");
                String nom = fileElem.attributeValue("nom");
                dirAtt = restorePath(dirAtt);
                File f = new File(dirXml + File.separator + dirAtt);
                String description = (fileElem.elementText("Description") == null) ? ""
                    : fileElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (isNewElement == false) {
                    HashMap<String, Attachment> envMap = data
                        .getAttachmentMapFromModel();
                    Iterator<Attachment> itEnvAttachs = envMap.values()
                        .iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = (Attachment) itEnvAttachs.next();
                        if ((attach instanceof FileAttachment)
                            && ((FileAttachment) attach).getNameFromModel()
                            .equals(nom)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    attach = new FileAttachment(f, description);
                    data.addAttachementInDBAndModel(attach);
                } else {
                    ((FileAttachment) attach).updateInDBAndModel(f);
                    ((FileAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (!attachmentException) {
                idialog
                    .setErrorMessage(idialog.getErrorMessage()
                                     + Language
                                     .getInstance()
                                     .getText(
                                              "Probleme_lors_de_la_recuperation_des_attachements"));
                attachmentException = true;
            }
            idialog.showErrorMessage();
        }
    }

    /**
     * Dans le cas ou le test n'etait pas deja dans la BD, ajout des actions
     *
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void ajouterActions(Element testElem, ManualTest test)
        throws Exception {
        List<Element> actionList = testElem.selectNodes(".//ActionTest");
        for (Element actionElem : actionList) {
            String nomAction = actionElem.elementText("Nom");
            String description = (actionElem.elementText("Description") == null) ? ""
                : actionElem.elementText("Description");
            String resulAttendu = (actionElem.elementText("ResultAttendu") == null) ? ""
                : actionElem.elementText("ResultAttendu");
            description = description.replaceAll("\\\\n", "\n");
            resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");

            Action newAction = new Action(test, nomAction, description);
            newAction.setAwaitedResultInModel(resulAttendu);
            test.addActionInDBAndModel(newAction);

            ajouterAttachements(actionElem, newAction);
            ajoutParametresActions(actionElem, newAction);

            /***** Plugin update Action Data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateActionFromXML(actionElem,
                                                          newAction, idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateActionFromXML(actionElem,
                                                          newAction, false, this);
                }
            }
            /***** (END) Plugin update Action Data *****/
        }
    }

    /**
     * Dans le cas ou attachableElement n'etait pas deja dans la BD, ajout des
     * attachements
     *
     * @param elem
     *            : element qui represente le WithAttachment dans le document
     *            XML
     * @param attachableElement
     */
    public void ajouterAttachements(Element elem,
                                    WithAttachment attachableElement) throws Exception {
        List<Node> urlAttachementList = elem
            .selectNodes("Attachements/UrlAttachement");
        Iterator<Node> itUrlAttach = urlAttachementList.iterator();
        while (itUrlAttach.hasNext()) {
            Element urlElem = (Element) itUrlAttach.next();
            String url = urlElem.attributeValue("url");
            try {
                new URL(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
                continue;
            }
            String description = (urlElem.elementText("Description") == null) ? ""
                : urlElem.elementText("Description");
            description = description.replaceAll("\\\\n", "\n");
            UrlAttachment pUrlAttachment = new UrlAttachment(url, description);
            try {
                attachableElement.addAttachementInDBAndModel(pUrlAttachment);
            } catch (Exception e1) {
                Tools.ihmExceptionView(e1);
            }
        }
        List<Node> testFileAttachementList = elem
            .selectNodes("Attachements/FileAttachement");
        Iterator<Node> itFileAttach = testFileAttachementList.iterator();
        while (itFileAttach.hasNext()) {
            Element fileElem = (Element) itFileAttach.next();
            String dirAtt = fileElem.attributeValue("dir");
            dirAtt = restorePath(dirAtt);
            File f = new File(dirXml + File.separator + dirAtt);
            String description = (fileElem.elementText("Description") == null) ? ""
                : fileElem.elementText("Description");
            description = description.replaceAll("\\\\n", "\n");
            FileAttachment pFileAttachment = new FileAttachment(f, description);
            try {
                attachableElement.addAttachementInDBAndModel(pFileAttachment);
            } catch (Exception e2) {
                Tools.ihmExceptionView(e2);
            }
        }
    }

    /**
     * Si l'action existait dans la BD, ajout ou mise a jour des parametres
     *
     * @param pAction
     *            identifiant de l'action
     * @param actionElem
     *            element action du document comportant les donnees
     */
    public void ajoutParametresActions(Element actionElem, Action pAction)
        throws Exception {
        List<Node> paramActionList = actionElem
            .selectNodes("ParamsT/ParamT/Nom");
        Iterator<Node> itParamAction = paramActionList.iterator();
        while (itParamAction.hasNext()) {
            String nomParamImport = ((Element) itParamAction.next()).getText();
            boolean appartient = false;
            if (pAction.getParameterFromModel(nomParamImport) != null) {
                appartient = true;
            }
            if (!appartient) {
                Parameter param;
                Test pTest = pAction.getTest();
                param = pTest.getUsedParameterFromModel(nomParamImport);
                if (param != null) {
                    pAction.setUseParamInDBAndModel(param);
                } else {
                    param = project.getParameterFromModel(nomParamImport);
                    if (param != null) {
                        pTest.setUseParamInDBAndModel(param);
                        pAction.setUseParamInDBAndModel(param);
                    } else {
                        // AIE
                    }
                }
            }
        }
    }

    /**
     * Update list of campaigns with information from the XML file
     */
    private void updateCampaigns() {
        ArrayList<Campaign> campList = project.getCampaignListFromModel();
        List<Element> campListXML = doc.selectNodes("//CampagneTest");
        Iterator<Element> itCampXML = campListXML.iterator();
        while (itCampXML.hasNext() && !cancelled) {
            try {
                Element campElem = (Element) itCampXML.next();
                String campName = campElem.elementText("Nom");
                String campDescription = (campElem.elementText("Description") == null) ? ""
                    : campElem.elementText("Description");
                if (campElem.element("Description") != null
                    && campElem.element("Description").attribute("isHTML") != null
                    && campElem.element("Description").attributeValue(
                                                                      "isHTML").equals("true")) {
                    campDescription = giveHTMLDescription(campElem);
                } else {
                    campDescription = campDescription.replaceAll("\\\\n",
                                                                 "<br>");
                    campDescription = "<html><head></head><body>"
                        + campDescription + "</body></html>";
                }
                boolean appartient = false;
                Campaign campaign = null;
                Iterator<Campaign> itCamp = campList.iterator();
                while (itCamp.hasNext() && !appartient) {
                    campaign = (Campaign) itCamp.next();
                    if (campElem.elementText("Nom").equals(
                                                           campaign.getNameFromModel())) {
                        appartient = true;
                    }
                    Util.log("[gestionDesCampagnes] compare "
                             + campaign.getNameFromModel() + " avec "
                             + campElem.elementText("Nom") + " result = "
                             + appartient);
                }
                if (!appartient) {

                    Campaign camp = new Campaign(campName, campDescription);
                    project.addCampaignInDBandModel(camp);

                    // ajout des attachements
                    ajouterAttachements(campElem, camp);
                    /***** Plugin update Campaign Data *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        if (idialog != null) {
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    camp, idialog.isSupprElement(), this);
                        } else {
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    camp, false, this);
                        }
                    }
                    /***** (END) Plugin update Campaing Data *****/
                    // ajout des jeux de donnees
                    ajouterJeuxDonnees(campElem, camp);
                    // ajout des tests associees a la campagne
                    ajoutTestsToCamp(campElem, camp);
                    // ajout des executions
                    ajouterExecutions(campElem, camp);
                } else { // the campaign is in DB
                    if (!idialog.isKeepOriginalOption()) {
                        // option is keepOriginalAndImport
                        // if some tests are copied, the campaign is in
                        // conflict, keep a copy and import the new campaign
                        boolean keepCopy = mustKeepCopy(campaign, campElem);
                        if (keepCopy) {
                            // change name of original campaign in "save_..."
                            String newName = "save_" + campName;
                            int i = 0;
                            while (Campaign.isInBase(project, newName)) {
                                newName = newName + "_" + i;
                                i++;
                            }
                            campaign.updateInDBAndModel(newName, campaign
                                                        .getDescriptionFromModel());
                            // import campaign from XML
                            Campaign camp = new Campaign(campName,
                                                         campDescription);
                            project.addCampaignInDBandModel(camp);

                            // ajout des attachements
                            ajouterAttachements(campElem, camp);
                            /***** Plugin update Campaign Data *****/
                            int size = listXMLPlugin.size();
                            for (int j = 0; j < size; j++) {
                                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                    .elementAt(j);
                                if (idialog != null) {
                                    pXMLPrinterPlugin.updateCampaignFromXML(
                                                                            campElem, camp, idialog
                                                                            .isSupprElement(), this);
                                } else {
                                    pXMLPrinterPlugin.updateCampaignFromXML(
                                                                            campElem, camp, false, this);
                                }
                            }
                            /***** (END) Plugin update Campaing Data *****/
                            // ajout des jeux de donnees
                            ajouterJeuxDonnees(campElem, camp);
                            // ajout des tests associees a la campagne
                            ajoutTestsToCamp(campElem, camp);
                            // ajout des executions
                            ajouterExecutions(campElem, camp);
                        } else {
                            // execution level
                            List<Element> execList = campElem
                                .selectNodes("ExecCampTests/ExecCampTest");
                            Iterator<Element> itExec = execList.iterator();
                            while (itExec.hasNext() && !cancelled) {
                                Element execElem = itExec.next();
                                String execName = execElem.elementText("Nom");
                                boolean belong = false;
                                ArrayList<Execution> execCampList = campaign
                                    .getExecutionListFromModel();
                                Iterator<Execution> itExecCamp = execCampList
                                    .iterator();
                                Execution exec = null;
                                while (itExecCamp.hasNext() && !belong) {
                                    exec = itExecCamp.next();
                                    if (execName
                                        .equals(exec.getNameFromModel())) {
                                        belong = true;
                                    }
                                }
                                if (!belong) {
                                    ajouterExecution(execElem, campaign);
                                } else {
                                    if (!idialog.isKeepOriginalOption()) {
                                        // search differences
                                        boolean keepCopyExec = mustKeepCopy(
                                                                            exec, execElem);
                                        if (keepCopyExec) {
                                            String newName = "save_" + execName;
                                            int i = 0;
                                            while (Execution.isInBase(campaign,
                                                                      newName)) {
                                                newName = newName + "_" + i;
                                                i++;
                                            }
                                            exec
                                                .updateInDBAndModel(
                                                                    newName,
                                                                    exec
                                                                    .getDescriptionFromModel());
                                            ajouterExecution(execElem, campaign);
                                        } else {
                                            List<Element> resExecList = execElem
                                                .selectNodes("ResulExecCampTests/ResulExecCampTest");
                                            Iterator<Element> itResExec = resExecList
                                                .iterator();
                                            while (itResExec.hasNext()
                                                   && !cancelled) {
                                                Element resExecElem = itResExec
                                                    .next();
                                                String resExecName = resExecElem
                                                    .elementText("Nom");
                                                boolean belong2 = false;
                                                ArrayList<ExecutionResult> resExecutionList = exec
                                                    .getExecutionResultListFromModel();
                                                Iterator<ExecutionResult> itResExecution = resExecutionList
                                                    .iterator();
                                                ExecutionResult resExec = null;
                                                while (itResExecution.hasNext()
                                                       && !belong2) {
                                                    resExec = itResExecution
                                                        .next();
                                                    if (resExecName
                                                        .equals(resExec
                                                                .getNameFromModel())) {
                                                        belong = true;
                                                    }
                                                }
                                                if (!belong) {
                                                    // add execution result
                                                    ajouterResulExec(
                                                                     resExecElem, exec);
                                                } else {
                                                    if (!idialog
                                                        .isKeepOriginalOption()) {
                                                        // search differences
                                                        boolean keepCopyResExec = mustKeepCopy(
                                                                                               resExec,
                                                                                               resExecElem);
                                                        if (keepCopyResExec) {
                                                            /*
                                                             * String newName =
                                                             * "save_"
                                                             * +resExecName; int
                                                             * i = 0; while
                                                             * (containsResExec
                                                             * (exec, newName))
                                                             * { newName =
                                                             * newName + "_" +
                                                             * i; i++; }
                                                             * resExec.
                                                             * updateInDBAndModel
                                                             * (newName,
                                                             * resExec.
                                                             * getDescriptionFromModel
                                                             * ());
                                                             */
                                                            // TODO implement
                                                            // updateInDBAndModel
                                                            ajouterResulExec(
                                                                             resExecElem,
                                                                             exec);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /*
     * private boolean containsResExec(Execution exec, String newName) {
     * ArrayList<ExecutionResult> resExecList =
     * exec.getExecutionResultListFromModel(); for (ExecutionResult resExec :
     * resExecList) { if (newName.equals(resExec.getNameFromModel())) { return
     * true; } } return false; }
     */

    /**
     * Si la campagne n'existait pas deja dans la BD, ajout des jeux de donnees
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param id_camp
     *            identifiant de la campagne ajoutee dans la base de donnees
     */
    private void ajouterJeuxDonnees(Element campElem, Campaign pCamp)
        throws Exception {
        List<Element> jeuListXML = campElem
            .selectNodes("JeuxDonnees/JeuDonnees");
        for (Element jeuElem : jeuListXML) {
            String nomJeu = jeuElem.elementText("Nom");
            String description = jeuElem.elementText("Description");
            if (description == null) {
                description = "";
            } else {
                description = description.replaceAll("\\\\n", "\n");
            }
            DataSet pDataSet = new DataSet(nomJeu, description);
            pCamp.addDataSetInDBAndModel(pDataSet);

            // ajout du jeu de donnees au modele
            // ajout des parametres values du jeu de donnees
            List<Element> paramList = jeuElem
                .selectNodes("ValeurParams/ValeurParam");
            for (Element paramElem : paramList) {
                String valParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                    .attributeValue("valeur")
                    : "";
                String nomParam = paramElem.elementText("Nom");

                Parameter param = project.getParameterFromModel(nomParam);
                pDataSet.addParamValueToDBAndModel(valParam, param);
            }

            /***** Plugin update DataSet *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateDataSetFromXML(jeuElem, pDataSet,
                                                           idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateDataSetFromXML(jeuElem, pDataSet,
                                                           false, this);
                }
            }
            /***** (END) Plugin update DataSet *****/
        }
    }

    private void ajoutTestsToCamp(Element campElem, Campaign pCamp)
        throws Exception {
        List<Element> familyListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef");
        for (Element famElem : familyListXML) {
            String nomFamille = famElem.elementText("Nom");
            Family family = project.getFamilyFromModel(nomFamille);
            List<Node> suiteListXML = famElem
                .selectNodes("SuiteTestsCamp/SuiteTestRef");
            Iterator<Node> itSuite = suiteListXML.iterator();
            while (itSuite.hasNext()) {
                Element suiteElem = (Element) itSuite.next();
                String nomSuite = suiteElem.elementText("Nom");
                TestList testList = family.getTestListInModel(nomSuite);
                List<Node> testListXML = suiteElem
                    .selectNodes("TestsCamp/TestRef");
                Iterator<Node> itTest = testListXML.iterator();
                while (itTest.hasNext()) {
                    Element testElem = (Element) itTest.next();
                    String nomTest = testElem.elementText("Nom");
                    int userID = -1;
                    String loginAssigned = testElem
                        .attributeValue("loginAssigned");
                    if (loginAssigned != null && !loginAssigned.equals("")) {
                        userID = project.containUser(loginAssigned);
                    }
                    if (userID == -1) {
                        if (idialog != null) {
                            // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                            // id_exec, DataModel.getCurrentUser().getLogin(),
                            // description, statut, resultat);
                            userID = DataModel.getCurrentUser().getIdBdd();
                        } else {
                            // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                            // id_exec,
                            // cdialog.getNewProject().getAdministrator().getLogin(),
                            // description, statut, resultat);
                            userID = project.getAdministratorFromModel()
                                .getIdBdd();
                        }
                    }
                    Test pTest = testList.getTestFromModel(nomTest);
                    pCamp.importTestInDBAndModel(pTest, userID);
                }
            }
        }
    }

    /**
     * Methode qui ajoute les executions aux campagnes qui n'etait pas au
     * prealable dans la BD
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param id_camp
     *            identifiant de la campagne dans la BD
     */
    public void ajouterExecutions(Element campElem, Campaign pCamp)
        throws Exception {
        List<Element> listExecXML = campElem
            .selectNodes("ExecCampTests/ExecCampTest");
        for (Element execElem : listExecXML) {
            ajouterExecution(execElem, pCamp);
        }
    }

    /**
     * Methode qui ajoute une execution a une campagne
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param id_camp
     *            identifiant de la campagne dans la BD
     */
    private void ajouterExecution(Element execElem, Campaign pCamp) {
        String nomExec = execElem.elementText("Nom");
        String description = (execElem.elementText("Description") != null) ? execElem
            .elementText("Description")
            : "";
        description = description.replaceAll("\\\\n", "\n");
        Execution pExecution = new Execution(nomExec, description);

        Element jeuElem = execElem.element("JeuDonneesEx");
        DataSet pDataSet = null;
        if (jeuElem != null) {
            String nomJeu = jeuElem.elementText("Nom");
            // id_jeu = ConnectionData.getCampTestSelect().getDataSetId(nomJeu,
            // campName);
            pDataSet = pCamp.getDataSetFromModel(nomJeu);
        } else {
            pDataSet = new DataSet(ApiConstants.EMPTY_NAME, "");
        }
        pExecution.updateDatasetInModel(pDataSet);

        Element envElem = execElem.element("EnvironnementEx");
        String nomEnvXML = envElem.elementText("Nom");

        Environment pEnvironement = project.getEnvironmentFromModel(nomEnvXML);
        pExecution.updateEnvInModel(pEnvironement);

        User pUser = null;
        if (idialog != null) {
            pUser = DataModel.getCurrentUser();
        } else {
            pUser = project.getAdministratorFromModel();
        }
        try {
            pCamp.addExecutionInDBAndModel(pExecution, pUser);

            // Ajout des scripts
            Element scriptPre = (Element) execElem
                .selectSingleNode("Script[1]");
            if (scriptPre != null) {
                ajouterScriptToExec(scriptPre, pExecution);
            }
            Element scriptPost = (Element) execElem
                .selectSingleNode("Script[2]");
            if (scriptPost != null) {
                ajouterScriptToExec(scriptPost, pExecution);
            }

            // ajouter les attachements
            ajouterAttachements(execElem, pExecution);

            /***** Plugin update Execution Data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateExecutionFromXML(execElem,
                                                             pExecution, idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateExecutionFromXML(execElem,
                                                             pExecution, false, this);
                }
            }
            /***** (END) Plugin update Execution Data *****/

            // ajouter les resultats d'executions
            ajouterResulExecs(execElem, pExecution);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }

    }

    /**
     * Methode qui ajoute un script a une execution
     *
     * @param script
     *            element script du document XML
     * @param id_exec
     *            identifiant de l'execution
     */
    public void ajouterScriptToExec(Element script, Execution pExecution)
        throws Exception {
        String classpath = script.elementText("Classpath");
        String argScript = script.elementText("ArgScript");
        String type = script.attributeValue("type");
        String dirScript = script.attributeValue("dir");
        dirScript = restorePath(dirScript);
        File fScript = new File(dirXml + File.separator + dirScript);

        Script pScritp = new Script(fScript.getName(), "");
        pScritp.setTypeInModel(type);
        pScritp.setScriptExtensionInModel(classpath);
        pScritp.updatePlugArgInModel(argScript);
        try {
            pExecution.addScriptInDBAndModel(pScritp, fScript);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    /**
     * Methode qui rajoute les resultats d'execution a une campagne
     *
     * @param execElem
     *            element representant l'execution d'une campagne dans le
     *            document XML
     * @param id_exec
     *            identifiant de l'execution dans la BD
     * @param campName
     *            nom de la campagne qui contient l'execution
     */
    public void ajouterResulExecs(Element execElem, Execution pExecution)
        throws Exception {
        List<Element> resulExecListXML = execElem
            .selectNodes("ResulExecCampTests/ResulExecCampTest");
        for (Element rexecElem : resulExecListXML) {
            ajouterResulExec(rexecElem, pExecution);

        }
    }

    /**
     * Methode qui rajoute un resultat d'execution a une execution
     *
     * @param rexecElem
     *            element representant le resultat d'execution d'une campagne
     *            dans le document XML
     * @param pExecution
     *            execution dans la DB
     */
    private void ajouterResulExec(Element rexecElem, Execution pExecution)
        throws Exception {

        String nom = rexecElem.elementText("Nom");
        String description = (rexecElem.elementText("Description") != null) ? rexecElem
            .elementText("Description")
            : "";
        description = description.replaceAll("\\\\n", "\n");
        String resultat = rexecElem.attributeValue("statut");
        Element resExecElem = rexecElem.element("ResulExecs");
        User pUser = null;
        if (idialog != null) {
            pUser = DataModel.getCurrentUser();
        } else {
            pUser = project.getAdministratorFromModel();
        }
        ExecutionResult pExecutionResult = new ExecutionResult(nom,
                                                               description, pExecution);
        pExecutionResult.setExecutionStatusInModel(resultat);
        pExecution.addExecutionResultInDBAndModel(pExecutionResult, pUser);

        ajouterAttachements(rexecElem, pExecutionResult);
        if (resExecElem != null) {
            List<Element> execCasList = resExecElem.selectNodes("ResulExec");
            for (Element execCasElem : execCasList) {
                String res = execCasElem.attributeValue("res");
                Element refTest = execCasElem.element("RefTest");
                String nomTest = refTest.elementText("NomTest");
                String nomSuite = refTest.elementText("NomSuite");
                String nomFamille = refTest.elementText("NomFamille");

                Test pTest = project.getTestFromModel(nomFamille, nomSuite,
                                                      nomTest);
                ExecutionTestResult pExecutionTestResult = pExecutionResult
                    .initTestResultStatusInModel(pTest, res, 0, pExecution
                                                 .getCampagneFromModel());
                if (!pExecutionTestResult.isInBase()) {
                    pExecutionResult.addExecTestResultInDB(pTest);
                }
                ajouterAttachements(execCasElem, pExecutionTestResult);

            }
        }

        List<Element> resActionList = rexecElem
            .selectNodes("ResulActionTests/ResulActionTest");
        for (Element resAction : resActionList) {
            Element refAction = resAction.element("RefAction");
            String nomAction = refAction.elementText("NomAction");
            String nomTest = refAction.elementText("NomTest");
            String nomSuite = refAction.elementText("NomSuite");
            String nomFamille = refAction.elementText("NomFamille");

            Test pTest = project
                .getTestFromModel(nomFamille, nomSuite, nomTest);
            Action pAction = ((ManualTest) pTest).getActionFromModel(nomAction);
            ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) pExecutionResult
                .getExecutionTestResultFromModel(pTest);
            if (!pManualExecutionResult.isInBase()) {
                pExecutionResult.addExecTestResultInDB((ManualTest) pTest);
            }
            String resultatAction = resAction.attributeValue("res");
            if (resultatAction.equals("NonRenseigne")) {
                resultatAction = "";
            }
            String descAction = (resAction.elementText("Description") == null) ? ""
                : resAction.elementText("Description");
            String resulAttendu = (resAction.elementText("ResultAttendu") == null) ? ""
                : resAction.elementText("ResultAttendu");
            String effectiveAttendu = (resAction.elementText("ResulEffectif") == null) ? ""
                : resAction.elementText("ResulEffectif");

            descAction = descAction.replaceAll("\\\\n", "\n");
            resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");
            effectiveAttendu = effectiveAttendu.replaceAll("\\\\n", "\n");

            // ConnectionData.getCampTestInsert().addResExecActionUsingID(id_exec_res,
            // id_test, id_action, resultatAction, descAction, resulAttendu);

            pManualExecutionResult.addStatusForActionInModel(pAction,
                                                             resultatAction);
            pManualExecutionResult.addEffectivResultInModel(pAction,
                                                            effectiveAttendu);
            pManualExecutionResult.addDescriptionResultInModel(pAction,
                                                               descAction);
            pManualExecutionResult.addAwaitedResultInModel(pAction,
                                                           resulAttendu);
            pExecutionResult.addExecActionResultInDB(pTest, pAction);
            // pManualExecutionResult.addActionResultInDB(pAction);

        }

        /***** Plugin update ExecutionResult Data *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (idialog != null) {
                pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                            pExecutionResult, idialog.isSupprElement(), this);
            } else {
                pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                            pExecutionResult, false, this);
            }
        }
        /***** (END) Plugin update ExecutionResult Data *****/
    }

    /**
     * Method which detects if the campaign in database must be copied or not.
     * The campaign in database must be copied if it contains tests which have
     * been saved in the test plan or it does not contain the same tests.
     *
     * @param campaign
     *            campaign in DB
     * @param campElem
     *            campaign in XML document
     * @return
     */
    public boolean mustKeepCopy(Campaign campaign, Element campElem)
        throws Exception {
        // tests list
        ArrayList<Test> testList = campaign.getTestListFromModel();
        List<Element> testListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef");
        for (Test test : testList) {
            if (savedTests.contains(test)) {
                return true;
            }
            boolean belong = false;
            Iterator<Element> itTestListXML = testListXML.iterator();
            while (itTestListXML.hasNext() && !belong) {
                Element testRef = itTestListXML.next();
                String nomTest = testRef.elementText("Nom");
                String nomSuite = ((Element) testRef
                                   .selectSingleNode("ancestor::SuiteTestRef[1]"))
                    .elementText("Nom");
                String nomFamille = ((Element) testRef
                                     .selectSingleNode("ancestor::FamilleRef[1]"))
                    .elementText("Nom");
                String suiteName = test.getTestListFromModel()
                    .getNameFromModel();
                String familyName = test.getTestListFromModel()
                    .getFamilyFromModel().getNameFromModel();
                if (test.getNameFromModel().equals(nomTest)
                    && suiteName.equals(nomSuite)
                    && familyName.equals(nomFamille)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        if (testListXML.size() != testList.size()) {
            return true;
        }
        for (Element testRef : testListXML) {
            String nomTest = testRef.elementText("Nom");
            String nomSuite = ((Element) testRef
                               .selectSingleNode("ancestor::SuiteTestRef[1]"))
                .elementText("Nom");
            String nomFamille = ((Element) testRef
                                 .selectSingleNode("ancestor::FamilleRef[1]"))
                .elementText("Nom");
            boolean appartient = false;
            Iterator<Test> itTestList = testList.iterator();
            while (itTestList.hasNext() && !appartient) {
                Test test = itTestList.next();
                String suiteName = test.getTestListFromModel()
                    .getNameFromModel();
                String familyName = test.getTestListFromModel()
                    .getFamilyFromModel().getNameFromModel();
                if (test.getNameFromModel().equals(nomTest)
                    && suiteName.equals(nomSuite)
                    && familyName.equals(nomFamille)) {
                    appartient = true;
                }
            }
            if (!appartient) {
                return true;
            }
        }

        // description
        String descriptionXML = "";
        Element descElem = campElem.element("Description");
        if (descElem != null && descElem.hasContent()) {
            descriptionXML = campElem.element("Description").asXML();
            descriptionXML = getTextFromChildren(campElem
                                                 .element("Description"), "");
        }
        String descriptionDB = campaign.getDescriptionFromModel();
        if (descriptionDB != null && !descriptionDB.equals("")) {
            descriptionDB = descriptionDB.replaceAll("<br>", "<br/>");
            if (!descriptionDB.startsWith("<html>")) {
                descriptionDB = "<html>" + descriptionDB + "</html>";
            }
            Document document = DocumentHelper.parseText(descriptionDB);
            descriptionDB = getTextFromChildren(document.getRootElement(), "");
        }
        if (descriptionDB == null)
            descriptionDB = "";
        if (!descriptionXML.equals(descriptionDB)) {
            return true;
        }

        // attachments
        List<Element> urlAttachementList = campElem
            .selectNodes("Attachements/UrlAttachement");
        HashMap<String, Attachment> envMap = campaign
            .getAttachmentMapFromModel();
        // verifier que tous les url attachements qui sont dans le XML sont
        // aussi dans la DB
        for (Element urlElem : urlAttachementList) {
            String url = urlElem.attributeValue("url");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof UrlAttachment)
                    && ((UrlAttachment) attach).getNameFromModel().equals(
                                                                          url)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verifier que tous les url attachements qui sont dans la DB sont aussi
        // dans le XML
        for (Attachment attach : envMap.values()) {
            if (attach instanceof UrlAttachment) {
                boolean belong = false;
                String url = ((UrlAttachment) attach).getNameFromModel();
                for (Element urlElem : urlAttachementList) {
                    if (url.equals(urlElem.attributeValue("url"))) {
                        belong = true;
                    }
                }
                if (!belong) {
                    return true;
                }
            }
        }

        // verification que tous les file attachments qui sont dans le XML sont
        // aussi dans la DB
        List<Element> fileAttachmentList = campElem
            .selectNodes("Attachements/FileAttachement");
        for (Element fileElem : fileAttachmentList) {
            String nom = fileElem.attributeValue("nom");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof FileAttachment)
                    && ((FileAttachment) attach).getNameFromModel().equals(
                                                                           nom)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verification que tous les file attachment qui sont dans la bd sont
        // aussi dans le document XML
        for (Attachment attach : envMap.values()) {
            String nomBD = ((FileAttachment) attach).getNameFromModel();
            boolean belong = false;
            for (Element fileElem : fileAttachmentList) {
                if (nomBD.equals(fileElem.attributeValue("nom"))) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }

        // data sets
        List<Element> dataSetList = campElem
            .selectNodes("JeuxDonnees/JeuDonnees");
        for (Element dataSetElem : dataSetList) {
            String dataSetName = dataSetElem.elementText("Nom");
            ArrayList<DataSet> datasets = campaign.getDataSetListFromModel();
            boolean belong = false;
            Iterator<DataSet> itDataSet = datasets.iterator();
            while (itDataSet.hasNext() && !belong) {
                DataSet dataSet = itDataSet.next();
                if (dataSetName.equals(dataSet.getNameFromModel())) {
                    belong = true;
                    String dataSetDescription = dataSetElem
                        .elementText("Description");
                    if (!dataSetDescription.equals(dataSet
                                                   .getDescriptionFromModel())) {
                        return true;
                    }
                    List<Element> paramList = dataSetElem
                        .selectNodes("ValeurParams/ValeurParam");
                    for (Element paramElem : paramList) {
                        String paramName = paramElem.elementText("Nom");
                        boolean belong2 = false;
                        HashMap<String, String> paramHashMap = dataSet
                            .getParametersHashMapFromModel();
                        Iterator<String> itparam = paramHashMap.keySet()
                            .iterator();
                        while (itparam.hasNext() && !belong2) {
                            String paramNom = itparam.next();
                            if (paramNom.equals(paramName)) {
                                belong2 = true;
                                String paramValue = paramElem
                                    .attributeValue("valeur");
                                String paramValeur = paramHashMap.get(paramNom);
                                if (paramValue != null
                                    && !paramValue.equals(paramValeur)) {
                                    return true;
                                }
                            }
                        }
                        if (!belong2) {
                            return true;
                        }
                    }
                }
            }
            if (!belong) {
                return true;
            }
        }
        ArrayList<DataSet> datasets = campaign.getDataSetListFromModel();
        for (DataSet dataSet : datasets) {
            boolean belong = false;
            Iterator<Element> itDataSet = dataSetList.iterator();
            while (itDataSet.hasNext() && !belong) {
                Element dataSetElem = itDataSet.next();
                String dataSetName = dataSetElem.elementText("Nom");
                if (dataSetName.equals(dataSet.getNameFromModel())) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method which detects if the execution in database must be copied or not.
     *
     * @param exec
     *            execution in DB
     * @param execElem
     *            execution in XML document
     * @return
     */
    private boolean mustKeepCopy(Execution exec, Element execElem)
        throws Exception {
        // attachments
        List<Element> urlAttachementList = execElem
            .selectNodes("Attachements/UrlAttachement");
        HashMap<String, Attachment> envMap = exec.getAttachmentMapFromModel();
        // verifier que tous les url attachements qui sont dans le XML sont
        // aussi dans la DB
        for (Element urlElem : urlAttachementList) {
            String url = urlElem.attributeValue("url");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof UrlAttachment)
                    && ((UrlAttachment) attach).getNameFromModel().equals(
                                                                          url)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verifier que tous les url attachements qui sont dans la DB sont aussi
        // dans le XML
        for (Attachment attach : envMap.values()) {
            if (attach instanceof UrlAttachment) {
                boolean belong = false;
                String url = ((UrlAttachment) attach).getNameFromModel();
                for (Element urlElem : urlAttachementList) {
                    if (url.equals(urlElem.attributeValue("url"))) {
                        belong = true;
                    }
                }
                if (!belong) {
                    return true;
                }
            }
        }

        // verification que tous les file attachments qui sont dans le XML sont
        // aussi dans la DB
        List<Element> fileAttachmentList = execElem
            .selectNodes("Attachements/FileAttachement");
        for (Element fileElem : fileAttachmentList) {
            String nom = fileElem.attributeValue("nom");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof FileAttachment)
                    && ((FileAttachment) attach).getNameFromModel().equals(
                                                                           nom)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verification que tous les file attachment qui sont dans la bd sont
        // aussi dans le document XML
        for (Attachment attach : envMap.values()) {
            String nomBD = ((FileAttachment) attach).getNameFromModel();
            boolean belong = false;
            for (Element fileElem : fileAttachmentList) {
                if (nomBD.equals(fileElem.attributeValue("nom"))) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }

        // dataset
        Element jeuDonneesElem = execElem.element("JeuDonneesEx");
        if (jeuDonneesElem != null) {
            String idExec = jeuDonneesElem.attributeValue("ref");
            if (idExec != null) {
                Element datasetElem = (Element) doc.getRootElement()
                    .selectSingleNode(
                                      "//JeuDonnees[@id_jeu='" + idExec + "']");
                String datasetName = datasetElem.elementText("Nom");
                if (exec.getDataSetFromModel() == null
                    || exec.getDataSetFromModel() != null
                    && !datasetName.equals(exec.getDataSetFromModel()
                                           .getNameFromModel())) {
                    return true;
                }
            }
        }

        // environment
        String idEnv = execElem.element("EnvironnementEx")
            .attributeValue("ref");
        Element envElem = (Element) doc.getRootElement().selectSingleNode(
                                                                          "//Environnement[@idEnv='" + idEnv + "']");
        String envName = envElem.elementText("Nom");
        if (!envName.equals(exec.getEnvironmentFromModel().getNameFromModel())) {
            return true;
        }

        // pre-script
        Element preScriptElem = (Element) execElem
            .selectSingleNode("Script[@type='PRE_SCRIPT']");
        if (preScriptElem != null) {
            Script preScript = exec.getPreScriptFromModel();
            if (preScript == null) {
                return true;
            } else {// compare md5
                File tempScriptFile = preScript.getFileFromDB(null);
                File fromXMLScriptFile = new File(dirXml + File.separator
                                                  + preScriptElem.attributeValue("dir"));
                String md5DBScript = getDigest(tempScriptFile);
                String md5XMLScript = getDigest(fromXMLScriptFile);
                if (!md5DBScript.equals(md5XMLScript)) {
                    return true;
                }
                tempScriptFile.delete();
            }
        }

        // post-script
        Element postScriptElem = (Element) execElem
            .selectSingleNode("Script[@type='POST_SCRIPT']");
        if (postScriptElem != null) {
            Script postScript = exec.getPostScriptFromModel();
            if (postScript == null) {
                return true;
            } else {// compare md5
                File tempScriptFile = postScript.getFileFromDB(null);
                File fromXMLScriptFile = new File(dirXml + File.separator
                                                  + preScriptElem.attributeValue("dir"));
                String md5DBScript = getDigest(tempScriptFile);
                String md5XMLScript = getDigest(fromXMLScriptFile);
                if (!md5DBScript.equals(md5XMLScript)) {
                    return true;
                }
                tempScriptFile.delete();
            }
        }

        return false;
    }

    /**
     * Method which detects if the execution result in database must be copied
     * or not.
     *
     * @param resExec
     *            execution result in DB
     * @param resExecElem
     *            execution result in XML document
     * @return
     */
    private boolean mustKeepCopy(ExecutionResult resExec, Element resExecElem)
        throws Exception {
        // testeur
        String testeur = resExecElem.elementText("Testeur");
        if (testeur != null && !testeur.equals(resExec.getTesterFromModel())) {
            return true;
        }

        // attachements
        List<Element> urlAttachementList = resExecElem
            .selectNodes("Attachements/UrlAttachement");
        HashMap<String, Attachment> envMap = resExec
            .getAttachmentMapFromModel();
        // verifier que tous les url attachements qui sont dans le XML sont
        // aussi dans la DB
        for (Element urlElem : urlAttachementList) {
            String url = urlElem.attributeValue("url");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof UrlAttachment)
                    && ((UrlAttachment) attach).getNameFromModel().equals(
                                                                          url)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verifier que tous les url attachements qui sont dans la DB sont aussi
        // dans le XML
        for (Attachment attach : envMap.values()) {
            if (attach instanceof UrlAttachment) {
                boolean belong = false;
                String url = ((UrlAttachment) attach).getNameFromModel();
                for (Element urlElem : urlAttachementList) {
                    if (url.equals(urlElem.attributeValue("url"))) {
                        belong = true;
                    }
                }
                if (!belong) {
                    return true;
                }
            }
        }

        // verification que tous les file attachments qui sont dans le XML sont
        // aussi dans la DB
        List<Element> fileAttachmentList = resExecElem
            .selectNodes("Attachements/FileAttachement");
        for (Element fileElem : fileAttachmentList) {
            String nom = fileElem.attributeValue("nom");
            boolean belong = false;
            for (Attachment attach : envMap.values()) {
                if ((attach instanceof FileAttachment)
                    && ((FileAttachment) attach).getNameFromModel().equals(
                                                                           nom)) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }
        // verification que tous les file attachment qui sont dans la bd sont
        // aussi dans le document XML
        for (Attachment attach : envMap.values()) {
            String nomBD = ((FileAttachment) attach).getNameFromModel();
            boolean belong = false;
            for (Element fileElem : fileAttachmentList) {
                if (nomBD.equals(fileElem.attributeValue("nom"))) {
                    belong = true;
                }
            }
            if (!belong) {
                return true;
            }
        }

        // resulExec
        List<Element> resulExecList = resExecElem
            .selectNodes("ResulExecs/ResulExec");
        for (Element resulExec : resulExecList) {
            String idResulExecString = resulExec.attributeValue("refTest");
            int idResulExec = new Integer(idResulExecString.substring(5))
                .intValue();
            Test test = project.getTestFromModel(idResulExec);
            ExecutionTestResult testResult = resExec
                .getExecutionTestResultFromModel(test);
            String res = resulExec.attributeValue("res");
            String status = testResult.getStatusFromModel();
            if (res != null
                && (res.equals("PASSED") || res.equals("FAILED") || res
                    .equals("INCONCLUSIF"))) {
                if (!res.equals(status)) {
                    return true;
                }
            } else if ((res == null || res.equals("") || res
                        .equals("NonRenseigne"))
                       && !status.equals("")) {
                return true;
            }
        }

        // resultActionTest
        List<Element> resulActionTestList = resExecElem
            .selectNodes("ResulActionTests/ResulActionTest");
        for (Element resulActionElem : resulActionTestList) {
            String idActionString = resulActionElem.attributeValue("refAction");
            String actionName = resulActionElem.element("RefAction")
                .elementText("NomAction");
            Element actionElem = (Element) doc
                .getRootElement()
                .selectSingleNode(
                                  "//ActionTest[@id_action='" + idActionString + "']");
            Element testElem = actionElem.getParent().getParent();
            String idTestString = testElem.attributeValue("id_test");
            int idTest = new Integer(idTestString.substring(5)).intValue();
            Test test = project.getTestFromModel(idTest);
            if (test instanceof ManualTest) {
                Action action = ((ManualTest) test)
                    .getActionFromModel(actionName);
                ExecutionTestResult pExecutionTestResult = resExec
                    .getExecutionTestResultFromModel(test);
                if (pExecutionTestResult instanceof ManualExecutionResult) {
                    ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) pExecutionTestResult;
                    HashMap<Action, String> actionMap = pManualExecutionResult
                        .getActionsMapInModel();
                    String resBD = actionMap.get(action);
                    String resXML = resulActionElem.attributeValue("res");
                    if (resXML != null
                        && (resXML.equals("PASSED")
                            || resXML.equals("FAILED") || resXML
                            .equals("INCONCLUSIF"))) {
                        if (!resXML.equals(resBD)) {
                            return true;
                        }
                    } else if ((resXML == null || resXML.equals("") || resXML
                                .equals("NonRenseigne"))
                               && !resBD.equals("")) {
                        return true;
                    }
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }
        return false;
    }

    private String restorePath(String path) throws Exception {
        if (File.separator.equals("\\")) {
            // Unix2Windows
            return path.replace('/', '\\');
        } else {
            return path.replace('\\', '/');
        }
    }

    /**
     * Cancel the current task
     *
     * @param b
     */
    public void setAnnule(boolean b) {
        cancelled = b;
    }

    /**
     * Verify if the task was cancelled
     *
     * @return
     */
    public boolean isAnnule() {
        return cancelled;
    }

    /**
     * Set the field defines if campaigns are also imported
     *
     * @param selected
     */
    public void setImportCampaign(boolean importCampaign) {
        this.importCampaign = importCampaign;
    }

    /**
     * Set the directory where the XML file is
     *
     * @param dirXml
     */
    public void setDirXml(String dirXml) {
        this.dirXml = dirXml;
    }

    /**
     * Return true if the list of selected tests was initialized
     *
     * @return
     */
    public boolean isInitSelection() {
        return initSelection;
    }

    /**
     * True in parameter if the test selection has been already initialized
     *
     * @param initSelection
     */
    public void setInitSelection(boolean initSelection) {
        this.initSelection = initSelection;
    }
}
