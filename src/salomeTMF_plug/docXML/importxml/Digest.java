/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.docXML.importxml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.MessageDigest;

/**
 *
 * @author Aurore PENAULT
 * @since JDK5.0
 */
public class Digest {

    private static final int SIZE = 16384;
    private static final String base = "0123456789abcdef";

    /**
     * Compute the MD5 digest of a file from a FileInputStream
     * @param fis a FileInputStream
     * @return MD5 as a String
     */
    public static String runMD5(FileInputStream fis) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("MD5");
            byte buffer[] = new byte[SIZE];
            while(true) {
                int l=fis.read(buffer);
                if (l== -1 ) break;
                sha1.update(buffer,0,l);
            }
            fis.close();
            byte digest [] = sha1.digest();
            StringBuffer buf = new StringBuffer();
            for(int i=0; i < digest.length; i++) {
                buf.append(base.charAt((digest[i] >> 4) & 0xf));
                buf.append(base.charAt(digest[i] & 0xf));
            }
            String md5 = buf.toString();;
            return md5;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Compute the MD5 digest of a file from a FileInputStream
     * @param fis a FileInputStream
     * @return MD5 as a String
     * @throws FileNotFoundException if the file does not exist, 
     * is a directory rather than a regular file, or for some other 
     * reason cannot be opened for reading. 
     */
    public static String runMD5(File f) throws FileNotFoundException {
        FileInputStream fis = new FileInputStream(f);
        return Digest.runMD5(fis);
    }

}
