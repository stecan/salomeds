/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */


package  salomeTMF_plug.docXML.importxml;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.common.ImportTestChooser;
import salomeTMF_plug.docXML.languages.Language;

public class ImportDialog extends JDialog{

    private static String fs = System.getProperties().getProperty("file.separator");

    JCheckBox importTestBox;

    JButton testSelection;
    JLabel tousTests;
    boolean selectionDesTests = false;

    String xmlFile;
    JLabel sauvLabel;
    JTextField sauvTF;
    JButton sauvButton;

    JButton valider;
    JButton annuler;

    JCheckBox suppr;

    JCheckBox supprActionCheck;

    JLabel conflitLabel;
    JRadioButton neRienFaire;
    JRadioButton creerCopie;
    JRadioButton miseAJour;

    JProgressBar progress;

    private boolean creer_copy = false;
    private boolean majPossible = false;
    private boolean supprElement = false;
    private boolean supprAction = false;

    String errorMessage = "";

    boolean initSelection = false;
    ArrayList<TestList> suiteSelectionList;
    ArrayList<Family> familySelectionList;
    ArrayList<Test> testSelectionList;
    DefaultMutableTreeNode chosenRoot;

    ImportXML traitement;

    Thread t;
    protected IPlugObject pIPlugObject;

    Vector<JPanel> panelsPluginsForImport = new Vector<JPanel>();

    /** Creates a new instance of Import */
    public ImportDialog(IPlugObject iPlugObject) throws Exception {
        super(SalomeTMFContext.getInstance().getSalomeFrame(),true);
        this.pIPlugObject = iPlugObject;

        createComponents();
    }

    public void createComponents() {

        Vector<Extension> listExtXMLPlugin = pIPlugObject.getXMLPrintersExtension();
        for (Extension pXMLExt : listExtXMLPlugin) {
            JPFManager pJPFManager =  pIPlugObject.getPluginManager();
            try {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager.activateExtension(pXMLExt);
                JPanel pPanel = pXMLPrinterPlugin.getImportOptionPanel();
                if (pPanel != null && !panelsPluginsForImport.contains(pPanel)){
                    panelsPluginsForImport.add(pPanel);
                }
            } catch (Exception e){

            }
        }

        suiteSelectionList = new ArrayList<TestList>();
        familySelectionList = new ArrayList<Family>();
        testSelectionList = new ArrayList<Test>();

        importTestBox = new JCheckBox(Language.getInstance().getText("Importer_uniquement_les_tests"));
        importTestBox.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    try {
                        if (!importTestBox.isSelected()){
                            testSelection.setEnabled(false);
                        }else{
                            testSelection.setEnabled(true);
                        }
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });

        importTestBox.setSelected(true);

        testSelection = new JButton(Language.getInstance().getText("Selection_des_tests"));
        testSelection.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    xmlFile = sauvTF.getText();
                    if (xmlFile==null || xmlFile.equals("")){
                        JOptionPane.showMessageDialog(
                                                      ImportDialog.this,
                                                      Language.getInstance().getText("Avant_de_selectionner_les_tests_a_importer_veuillez_indiquer_le_fichier_XML_a_utiliser"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    } else {
                        File xmlFileFile = new File(xmlFile);
                        if (!xmlFileFile.exists()) {
                            JOptionPane.showMessageDialog(
                                                          ImportDialog.this,
                                                          Language.getInstance().getText("Vous_devez_entrer_un_nom_de_fichier_existant_pour_le_document_d_import"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                        } else {
                            Document doc = null;
                            try{
                                doc = xmlParser(xmlFile);
                            }catch (Exception ex){
                                ex.printStackTrace();
                                errorMessage+=Language.getInstance().getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
                                showErrorMessage();
                            }
                            try {
                                if (initSelection){
                                    new ImportTestChooser(ImportDialog.this, doc, true);
                                }else{
                                    new ImportTestChooser(ImportDialog.this, doc, false);
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
                                showErrorMessage();
                            }
                        }
                    }
                }
            });
        tousTests = new JLabel(Language.getInstance().getText("_Par_defaut_tous_les_tests_sont_importes_"));
        tousTests.setFont(new Font(null, Font.ITALIC, 12));

        sauvLabel = new JLabel(Language.getInstance().getText("Fichier_XML_"));
        sauvTF = new JTextField(50);
        sauvButton = new JButton(Language.getInstance().getText("Choisir"));
        sauvButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance().getText("Fichier_XML____xml_"),".xml"));
                        int returnVal =  fileChooser.showDialog(ImportDialog.this, Language.getInstance().getText("Selectionner"));
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                            xmlFile = fileChooser.getSelectedFile().getAbsolutePath();
                            if (xmlFile.indexOf(".")!=-1){
                                if (!xmlFile.substring(xmlFile.lastIndexOf(".")).equals(".xml")){
                                    xmlFile+=".xml";
                                }
                            }else{
                                xmlFile+=".xml";
                            }
                            sauvTF.setText(xmlFile);
                        }
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });

        valider = new JButton(Language.getInstance().getText("OK"));
        valider.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    xmlFile = sauvTF.getText();
                    if (xmlFile==null || xmlFile.equals("")){
                        JOptionPane.showMessageDialog(
                                                      ImportDialog.this,
                                                      Language.getInstance().getText("Vous_devez_entrer_un_nom_de_fichier_pour_le_document_d_import"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    } else {
                        File xmlFileFile = new File(xmlFile);
                        if (!xmlFileFile.exists()) {
                            JOptionPane.showMessageDialog(
                                                          ImportDialog.this,
                                                          Language.getInstance().getText("Vous_devez_entrer_un_nom_de_fichier_existant_pour_le_document_d_import"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                        } else {
                            t = new Thread() {
                                    public void run() {
                                        try {
                                            String dirXml = xmlFile.substring(0, xmlFile.lastIndexOf(fs));
                                            if (creerCopie.isSelected()) {
                                                creer_copy = true;
                                            } else if (miseAJour.isSelected()) {
                                                majPossible = true;
                                            }
                                            if (supprActionCheck.isSelected()) {
                                                supprAction = true;
                                            }
                                            if (suppr.isSelected()){
                                                supprElement = true;
                                            }
                                            if(sauvTF.getText().equals("")){
                                                JOptionPane.showMessageDialog(
                                                                              ImportDialog.this,
                                                                              Language.getInstance().getText("Vous_devez_entrez_un_nom_de_fichier_pour_le_document_genere"),
                                                                              Language.getInstance().getText("Erreur_"),
                                                                              JOptionPane.ERROR_MESSAGE);
                                            }else{
                                                progress.setIndeterminate(true);
                                                try{
                                                    traitement = new ImportXML(ImportDialog.this, xmlParser(xmlFile), familySelectionList, suiteSelectionList, testSelectionList, null, pIPlugObject);
                                                }catch (Exception e){
                                                    e.printStackTrace();
                                                    errorMessage+=Language.getInstance().getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
                                                    showErrorMessage();
                                                    traitement.setAnnule(true);
                                                }
                                                traitement.setImportOnlyTests(importTestBox.isSelected());
                                                traitement.setDirXml(dirXml);
                                                traitement.setSelectionDesTests(isInitSelection());
                                                traitement.setImportOnlyTests(importTestBox.isSelected());
                                                /*
                                                  if(ext != null){
                                                  traitement.setImportRequirement(importReqBox.isSelected());
                                                  }
                                                */
                                                traitement.importInProject();
                                                if (!traitement.isAnnule()){
                                                    if (errorMessage.equals("")){
                                                        progress.setVisible(false);
                                                        JOptionPane.showMessageDialog( ImportDialog.this,
                                                                                       Language.getInstance().getText("L_import_s_est_terminee_avec_succes"),
                                                                                       Language.getInstance().getText("Information"),
                                                                                       JOptionPane.INFORMATION_MESSAGE);
                                                        ImportDialog.this.dispose();
                                                    }else{
                                                        progress.setVisible(false);
                                                        JOptionPane.showMessageDialog( ImportDialog.this,
                                                                                       errorMessage,
                                                                                       Language.getInstance().getText("Erreur"),
                                                                                       JOptionPane.ERROR_MESSAGE);
                                                        ImportDialog.this.dispose();
                                                    }
                                                }else if (!errorMessage.equals("")){
                                                    progress.setVisible(false);
                                                    JOptionPane.showMessageDialog( ImportDialog.this,
                                                                                   errorMessage,
                                                                                   Language.getInstance().getText("Erreur"),
                                                                                   JOptionPane.ERROR_MESSAGE);
                                                    ImportDialog.this.dispose();
                                                }
                                            }
                                        } catch (Exception ex) {
                                            Tools.ihmExceptionView(ex);
                                        }
                                    }
                                };
                            t.start();
                        }
                    }
                }
            });

        annuler = new JButton(Language.getInstance().getText("Annuler"));
        annuler.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (traitement != null){
                            traitement.setAnnule(true);
                        }
                        ImportDialog.this.dispose();
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });

        JPanel importTestPanel = new JPanel();
        importTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        importTestPanel.add(importTestBox);

        JPanel selectTestPanel = new JPanel();
        selectTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        selectTestPanel.add(testSelection);
        selectTestPanel.add(tousTests);

        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new BoxLayout(selectPanel, BoxLayout.Y_AXIS));
        selectPanel.setBorder(BorderFactory.createTitledBorder(""));
        selectPanel.add(importTestPanel);
        selectPanel.add(selectTestPanel);

        JPanel sauvLabelPanel = new JPanel(new FlowLayout (FlowLayout.LEFT));
        sauvLabelPanel.add(sauvLabel);
        JPanel sauvTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvTFPanel.add(sauvTF);
        JPanel sauvButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvButtonPanel.add(sauvButton);

        JPanel sauvPanel = new JPanel();
        sauvPanel.setLayout(new BoxLayout(sauvPanel, BoxLayout.Y_AXIS));
        sauvPanel.setBorder(BorderFactory.createTitledBorder(""));
        sauvPanel.add(sauvLabelPanel);
        sauvPanel.add(sauvTFPanel);
        sauvPanel.add(sauvButtonPanel);

        JPanel buttons = new JPanel();
        buttons.add(valider);
        buttons.add(annuler);

        progress = new JProgressBar();
        JPanel progression = new JPanel(new FlowLayout(FlowLayout.CENTER));
        progression.add(progress);

        JPanel commonPanel = new JPanel();
        commonPanel.setLayout(new BoxLayout(commonPanel, BoxLayout.Y_AXIS));
        commonPanel.add(buttons);
        commonPanel.add(Box.createRigidArea(new Dimension(1, 5)));
        commonPanel.add(progression);

        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(Box.createVerticalStrut(10));
        page.add(sauvPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(selectPanel);

        suppr = new JCheckBox(Language.getInstance().getText("Supprimer_les_elements_qui_ne_sont_pas_dans_le_document_XML"));

        supprActionCheck = new JCheckBox(Language.getInstance().getText("Supprimer_les_informations_des_tests_absentes_dans_le_document_XML"));

        conflitLabel = new JLabel(Language.getInstance().getText("En_cas_de_conflit__"));
        neRienFaire = new JRadioButton(Language.getInstance().getText("Conserver_l_original_et_ne_rien_faire_de_plus"));
        creerCopie = new JRadioButton(Language.getInstance().getText("Conserver_l_original_et_faire_une_copie_des_nouvelles_donnees_en_prefixant_par_copy_"));
        creerCopie.setSelected(true);
        miseAJour = new JRadioButton(Language.getInstance().getText("Mettre_a_jour_les_elements_possibles_description_et_attachements_des_tests_script_des_tests_automatiques_attachements_des_actions_"));
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(neRienFaire);
        buttonGroup.add(creerCopie);
        buttonGroup.add(miseAJour);

        JPanel options = new JPanel();
        options.setLayout(new BoxLayout(options, BoxLayout.Y_AXIS));
        options.add(suppr);
        options.add(Box.createVerticalStrut(10));
        options.add(supprActionCheck);
        options.add(Box.createVerticalStrut(10));
        options.add(conflitLabel);
        options.add(Box.createVerticalStrut(10));
        options.add(creerCopie);
        options.add(Box.createVerticalStrut(10));
        options.add(miseAJour);
        options.add(Box.createVerticalStrut(10));
        options.add(neRienFaire);

        JTabbedPane onglets = new JTabbedPane();
        onglets.addTab(Language.getInstance().getText("Principal"), page);
        onglets.addTab(Language.getInstance().getText("Options"), options);
        /**** Ajout des tabs des plugins ****/
        int size2 = panelsPluginsForImport.size();
        for (int i = 0; i < size2; i++){
            JPanel pPanel = (JPanel) panelsPluginsForImport.elementAt(i);
            String name = pPanel.getName();
            onglets.addTab(name, pPanel);
        }

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(onglets, BorderLayout.CENTER);
        contentPaneFrame.add(commonPanel, BorderLayout.SOUTH);

        this.setTitle(Language.getInstance().getText("Import_a_partir_d_un_fichier_XML"));
        //this.setLocation(400,100);
        centerScreen();
    }
        
    protected void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);         
        this.setVisible(true); 
        requestFocus();
    }

    /**
     * Methode qui parse le document XML dont le chemin est indique dans "path"
     * @param path
     * @return un document DOM4J
     * @throws Exception
     */
    public Document xmlParser(String path) throws Exception{
        SAXReader reader = new SAXReader(false);
        Document doc = reader.read(new FileInputStream(new File(path)));
        return doc;
    }

    /**
     * Methode qui affiche les messages d'erreur
     * @return
     */
    public void showErrorMessage(){
        JOptionPane.showMessageDialog(
                                      ImportDialog.this,
                                      errorMessage,
                                      Language.getInstance().getText("Erreur_"),
                                      JOptionPane.ERROR_MESSAGE);
        ImportDialog.this.dispose();
    }

    /**
     * @return Returns the initSelection.
     */
    public boolean isInitSelection() {
        return initSelection;
    }
    /**
     * @param initSelection The initSelection to set.
     */
    public void setInitSelection(boolean initSelection) {
        this.initSelection = initSelection;
    }
    /**
     * @return Returns the suiteList.
     */
    public ArrayList<TestList> getSuiteSelectionList() {
        return suiteSelectionList;
    }
    /**
     * @param suiteList The suiteList to set.
     */
    public void setSuiteSelectionList(ArrayList<TestList> suiteSelectionList) {
        this.suiteSelectionList = suiteSelectionList;
    }
    /**
     * @return Returns the familyList.
     */
    public ArrayList<Family> getFamilySelectionList() {
        return familySelectionList;
    }
    /**
     * @param familyList The familyList to set.
     */
    public void setFamilySelectionList(ArrayList<Family> familySelectionList) {
        this.familySelectionList = familySelectionList;
    }
    /**
     * @return Returns the testList.
     */
    public ArrayList<Test> getTestSelectionList() {
        return testSelectionList;
    }
    /**
     * @param testList The testList to set.
     */
    public void setTestSelectionList(ArrayList<Test> testSelectionList) {
        this.testSelectionList = testSelectionList;
    }
    /**
     * @return Returns the chosenRoot.
     */
    public DefaultMutableTreeNode getChosenRoot() {
        return chosenRoot;
    }
    /**
     * @param chosenRoot The chosenRoot to set.
     */
    public void setChosenRoot(DefaultMutableTreeNode chosenRoot) {
        this.chosenRoot = chosenRoot;
    }
    /**
     * @return Returns the selectionDesTests.
     */
    public boolean isSelectionDesTests() {
        return selectionDesTests;
    }
    /**
     * @param selectionDesTests The selectionDesTests to set.
     */
    public void setSelectionDesTests(boolean selectionDesTests) {
        this.selectionDesTests = selectionDesTests;
    }

    /**
     * @return Returns the supprAction.
     */
    public boolean isSupprAction() {
        return supprAction;
    }
    /**
     * @return Returns the creer_copy.
     */
    public boolean isCreer_copy() {
        return creer_copy;
    }
    /**
     * @return Returns the majPossible.
     */
    public boolean isMajPossible() {
        return majPossible;
    }
    /**
     * @return Returns the supprElement.
     */
    public boolean isSupprElement() {
        return supprElement;
    }
    /**
     * @return Returns the errorMessage.
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * @param errorMessage The errorMessage to set.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
