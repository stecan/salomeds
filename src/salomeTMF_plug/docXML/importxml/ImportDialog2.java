/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.docXML.importxml;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.common.ImportTestChooser2;
import salomeTMF_plug.docXML.languages.Language;

/**
 * To import data from XML file
 * @author vapu8214
 * @since JDK5.0
 */
public class ImportDialog2 extends JDialog {
        
    protected IPlugObject pIPlugObject;
    private Vector<JPanel> panelsPluginsForImport = new Vector<JPanel>();

    private ArrayList<Family> familySelectionList;
    private ArrayList<TestList> suiteSelectionList;
    private ArrayList<Test> testSelectionList;
    private boolean initSelection = false;
    private DefaultMutableTreeNode chosenRoot;
    
    private JLabel sauvLabel;
    private JTextField sauvTF;
    private JButton sauvButton;
    
    private JCheckBox importCampBox;
    private JButton testSelection;
    private JLabel tousTests;

    private JCheckBox suppr;
    private JLabel conflitLabel;
    private JRadioButton keepOriginal;
    private JRadioButton keepOriginalAndImport;
    private JRadioButton keepOriginalAndUpdate;
    private JRadioButton updateOriginal;
    private JRadioButton eraseOriginalAndImport;
        
    private JButton valider;
    private JButton annuler;
        
    private JProgressBar progress;
        
    private String xmlFile;
    private String errorMessage = "";
    private boolean selectionDesTests = false;
    private ImportXML2 traitement;
    
    /** 
     * Creates a new instance of ImportDialog2 
     */
    public ImportDialog2(IPlugObject iPlugObject) throws Exception {
        super(SalomeTMFContext.getInstance().getSalomeFrame(),true);
        this.pIPlugObject = iPlugObject;
        createComponents();
    }
    
    /**
     * Create components of the dialog box
     */
    private void createComponents(){
        
        Vector<Extension> listExtXMLPlugin = pIPlugObject.getXMLPrintersExtension();
        for (Extension pXMLExt : listExtXMLPlugin) {
            JPFManager pJPFManager =  pIPlugObject.getPluginManager();
            try {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager.activateExtension(pXMLExt);
                JPanel pPanel = pXMLPrinterPlugin.getImportOptionPanel();
                if (pPanel != null && !panelsPluginsForImport.contains(pPanel)) {
                    panelsPluginsForImport.add(pPanel);
                }
            } catch (Exception e) {}
        }
        
        familySelectionList = new ArrayList<Family>();
        suiteSelectionList = new ArrayList<TestList>();
        testSelectionList = new ArrayList<Test>();
        
        sauvLabel = new JLabel(Language.getInstance().getText("Fichier_XML_"));
        sauvTF = new JTextField(50);
        sauvButton = new JButton(Language.getInstance().getText("Choisir"));
        sauvButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance().
										getText("Fichier_XML____xml_"),".xml"));
                        int returnVal =  fileChooser.showDialog(ImportDialog2.this, 
								Language.getInstance().getText("Selectionner"));
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
                            xmlFile = fileChooser.getSelectedFile().getAbsolutePath();
                            if (xmlFile.indexOf(".")!=-1){
                                if (!xmlFile.substring(xmlFile.lastIndexOf(".")).equals(".xml")){
                                    xmlFile+=".xml";
                                }
                            }else{
                                xmlFile+=".xml";
                            }
                            sauvTF.setText(xmlFile);
                        }
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });
        
        importCampBox = new JCheckBox(Language.getInstance().getText("Importer_les_campagnes"));
        importCampBox.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    try {
                        if (importCampBox.isSelected()){
                            testSelection.setEnabled(false);
                            keepOriginalAndUpdate.setEnabled(false);
                            updateOriginal.setEnabled(false);
                            eraseOriginalAndImport.setEnabled(false);
                            if (!keepOriginal.isSelected() && !keepOriginalAndImport.isSelected()) {
                                keepOriginal.setSelected(true);
                            }
                        }else{
                            testSelection.setEnabled(true);
                            keepOriginalAndUpdate.setEnabled(true);
                            updateOriginal.setEnabled(true);
                            eraseOriginalAndImport.setEnabled(true);
                        }
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });
        importCampBox.setSelected(false);

        testSelection = new JButton(Language.getInstance().getText("Selection_des_tests"));
        testSelection.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (xmlFile==null || xmlFile.equals("")){
                        JOptionPane.
			    showMessageDialog(
					      ImportDialog2.this,
					      Language.getInstance().
					      getText("Avant_de_selectionner_les_tests_a_importer_veuillez_indiquer_le_fichier_XML_a_utiliser"),
					      Language.getInstance().getText("Erreur_"),
					      JOptionPane.ERROR_MESSAGE);
                    }else{
                        Document doc = null;
                        try{
                            doc = xmlParser(xmlFile);
                        }catch (Exception ex){
                            ex.printStackTrace();
                            errorMessage+=Language.getInstance().
				getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
                            showErrorMessage();
                        }
                        try {
                            if (initSelection){
                                new ImportTestChooser2(ImportDialog2.this, doc, true);
                            }else{
                                new ImportTestChooser2(ImportDialog2.this, doc, false);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
                            showErrorMessage();
                        }
                    }
                }
            });
        tousTests = new JLabel(Language.getInstance().getText("_Par_defaut_tous_les_tests_sont_importes_"));
        tousTests.setFont(new Font(null, Font.ITALIC, 12));
        
        suppr = new JCheckBox(Language.getInstance().getText("Supprimer_les_elements_qui_ne_sont_pas_dans_le_document_XML"));
        conflitLabel = new JLabel(Language.getInstance().getText("En_cas_de_conflit__"));
        keepOriginal = new JRadioButton(Language.getInstance().getText("Conserver_l_original"));
        keepOriginalAndImport = new JRadioButton(Language.getInstance().getText("Garder_une_copie_et_importer_le_nouvel_element"));
        keepOriginalAndUpdate = new JRadioButton(Language.getInstance()
						 .getText("Garder_un_copie_et_mettre_a_jour_une_copie_de_l_original_avec_l_element_importe"));
        updateOriginal = new JRadioButton(Language.getInstance().getText("Mettre_a_jour_l_original_avec_l_element_importe"));
        eraseOriginalAndImport = new JRadioButton(Language.getInstance().getText("Remplacer_l_original_par_l_element_importe"));
        keepOriginal.setSelected(true);
        
        ButtonGroup conflictGroup = new ButtonGroup();
        conflictGroup.add(keepOriginal);
        conflictGroup.add(keepOriginalAndImport);
        conflictGroup.add(keepOriginalAndUpdate);
        conflictGroup.add(updateOriginal);
        conflictGroup.add(eraseOriginalAndImport);
         
        valider = new JButton(Language.getInstance().getText("OK"));
        valider.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (xmlFile==null || xmlFile.equals("")){
                        JOptionPane.showMessageDialog(
                                                      ImportDialog2.this,
                                                      Language.getInstance().getText("Vous_devez_entrer_un_nom_de_fichier_pour_le_document_d_import"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    } else {
                        Thread t = new Thread() {
                                public void run() {
                                    try {
                                        String dirXml = xmlFile.substring(0, xmlFile.lastIndexOf(File.separator));
                                        if(sauvTF.getText().equals("")) {
                                            JOptionPane.showMessageDialog(
                                                                          ImportDialog2.this,
                                                                          Language.getInstance().getText("Vous_devez_entrez_un_nom_de_fichier_pour_le_document_genere"),
                                                                          Language.getInstance().getText("Erreur_"),
                                                                          JOptionPane.ERROR_MESSAGE);
                                        } else {
                                            progress.setIndeterminate(true);
                                            try{
                                                traitement = new ImportXML2(ImportDialog2.this, xmlParser(xmlFile), 
                                                                            familySelectionList, suiteSelectionList, testSelectionList, 
                                                                            null, pIPlugObject);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                errorMessage+=Language.getInstance().getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
                                                showErrorMessage();
                                                traitement.setAnnule(true);
                                            }
                                            traitement.setImportCampaign(importCampBox.isSelected());
                                            traitement.setDirXml(dirXml);
                                            traitement.setInitSelection(isInitSelection());
                                            traitement.importInProject();
                                            if (!traitement.isAnnule()){
                                                if (errorMessage.equals("")){
                                                    progress.setVisible(false);
                                                    JOptionPane.showMessageDialog( ImportDialog2.this,
                                                                                   Language.getInstance().getText("L_import_s_est_terminee_avec_succes"),
                                                                                   Language.getInstance().getText("Information"),
                                                                                   JOptionPane.INFORMATION_MESSAGE);
                                                    ImportDialog2.this.dispose();
                                                }else{
                                                    progress.setVisible(false);
                                                    JOptionPane.showMessageDialog( ImportDialog2.this,
                                                                                   errorMessage,
                                                                                   Language.getInstance().getText("Erreur"),
                                                                                   JOptionPane.ERROR_MESSAGE);
                                                    ImportDialog2.this.dispose();
                                                }
                                            }else if (!errorMessage.equals("")){
                                                progress.setVisible(false);
                                                JOptionPane.showMessageDialog( ImportDialog2.this,
                                                                               errorMessage,
                                                                               Language.getInstance().getText("Erreur"),
                                                                               JOptionPane.ERROR_MESSAGE);
                                                ImportDialog2.this.dispose();
                                            }
                                        }
                                    } catch (Exception ex) {
                                        Tools.ihmExceptionView(ex);
                                    }
                                }
                            };
                        t.start();
                    }
                }
            });

        annuler = new JButton(Language.getInstance().getText("Annuler"));
        annuler.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (traitement != null){
                            traitement.setAnnule(true);
                        }
                        ImportDialog2.this.dispose();
                    } catch (Exception ex) {
                        Tools.ihmExceptionView(ex);
                    }
                }
            });
        
        progress = new JProgressBar();
        
        JPanel sauvLabelPanel = new JPanel(new FlowLayout (FlowLayout.LEFT));
        sauvLabelPanel.add(sauvLabel);
        JPanel sauvTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvTFPanel.add(sauvTF);
        JPanel sauvButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvButtonPanel.add(sauvButton);
        
        JPanel sauvPanel = new JPanel();
        sauvPanel.setLayout(new BoxLayout(sauvPanel, BoxLayout.Y_AXIS));
        sauvPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Document_d_import")));
        sauvPanel.add(sauvLabelPanel);
        sauvPanel.add(sauvTFPanel);
        sauvPanel.add(sauvButtonPanel);
        
        JPanel importTestPanel = new JPanel();
        importTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        importTestPanel.add(importCampBox);

        JPanel selectTestPanel = new JPanel();
        selectTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        selectTestPanel.add(testSelection);
        selectTestPanel.add(tousTests);

        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new BoxLayout(selectPanel, BoxLayout.Y_AXIS));
        selectPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Elements_a_importer")));
        selectPanel.add(importTestPanel);
        selectPanel.add(selectTestPanel);
        for (JPanel pPanel : panelsPluginsForImport) {
            selectPanel.add(pPanel);
        }
        
        JPanel supprOptionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        supprOptionPanel.add(suppr);
        JPanel conflictPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictPanel.add(conflitLabel);
        JPanel conflictOption1Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictOption1Panel.add(Box.createHorizontalStrut(10));
        conflictOption1Panel.add(keepOriginal);
        JPanel conflictOption2Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictOption2Panel.add(Box.createHorizontalStrut(10));
        conflictOption2Panel.add(keepOriginalAndImport);
        JPanel conflictOption3Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictOption3Panel.add(Box.createHorizontalStrut(10));
        conflictOption3Panel.add(keepOriginalAndUpdate);
        JPanel conflictOption4Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictOption4Panel.add(Box.createHorizontalStrut(10));
        conflictOption4Panel.add(updateOriginal);
        JPanel conflictOption5Panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        conflictOption5Panel.add(Box.createHorizontalStrut(10));
        conflictOption5Panel.add(eraseOriginalAndImport);
        
        JPanel optionsPanel = new JPanel();
        optionsPanel.setLayout(new BoxLayout(optionsPanel, BoxLayout.Y_AXIS));
        optionsPanel.setBorder(new TitledBorder("Options"));
        optionsPanel.add(supprOptionPanel);
        optionsPanel.add(Box.createVerticalStrut(10));
        optionsPanel.add(conflictPanel);
        optionsPanel.add(conflictOption1Panel);
        optionsPanel.add(conflictOption2Panel);
        optionsPanel.add(conflictOption3Panel);
        optionsPanel.add(conflictOption4Panel);
        optionsPanel.add(conflictOption5Panel);
        
        JPanel buttons = new JPanel();
        buttons.add(valider);
        buttons.add(annuler);
        
        JPanel progression = new JPanel(new FlowLayout(FlowLayout.CENTER));
        progression.add(progress);
        
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(Box.createVerticalStrut(10));
        page.add(sauvPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(selectPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(optionsPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(buttons);
        page.add(Box.createRigidArea(new Dimension(1, 5)));
        page.add(progression);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);

        setTitle(Language.getInstance().getText("Import_a_partir_d_un_fichier_XML"));
        centerScreen();         
    }
    
    /**
     * Center and show the dialog
     */
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);         
        this.setVisible(true); 
        requestFocus();
    }
    
    /**
     * Methode qui parse le document XML dont le chemin est indique dans "path"
     * @param path
     * @return un document DOM4J
     * @throws Exception
     */
    public Document xmlParser(String path) throws Exception{
        SAXReader reader = new SAXReader(false);
        Document doc = reader.read(new FileInputStream(new File(path)));
        return doc;
    }
    
    /**
     * Methode qui affiche les messages d'erreur
     * @return
     */
    public void showErrorMessage(){
        JOptionPane.showMessageDialog(
                                      ImportDialog2.this,
                                      errorMessage,
                                      Language.getInstance().getText("Erreur_"),
                                      JOptionPane.ERROR_MESSAGE);
        ImportDialog2.this.dispose();
    }
    
    /**
     * @return Returns the initSelection.
     */
    public boolean isInitSelection() {
        return initSelection;
    }
    
    /**
     * @param initSelection The initSelection to set.
     */
    public void setInitSelection(boolean initSelection) {
        this.initSelection = initSelection;
    }

    /**
     * Get the root of the tree of selected tests
     * @return
     */
    public DefaultMutableTreeNode getChosenRoot() {
        return chosenRoot;
    }

    public boolean isSelectionDesTests() {
        return selectionDesTests;
    }

    public void setSelectionDesTests(boolean selectionDesTests) {
        this.selectionDesTests = selectionDesTests;
    }

    public void setFamilySelectionList(ArrayList<Family> familySelectionList) {
        this.familySelectionList = familySelectionList;
    }

    public void setSuiteSelectionList(ArrayList<TestList> suiteSelectionList) {
        this.suiteSelectionList = suiteSelectionList;
    }

    public void setTestSelectionList(ArrayList<Test> testSelectionList) {
        this.testSelectionList = testSelectionList;
    }

    public void setChosenRoot(DefaultMutableTreeNode chosenRoot) {
        this.chosenRoot = chosenRoot;
    }

    public boolean isSupprElement() {
        return suppr.isSelected();
    }
        
    public boolean isKeepOriginalOption() {
        return keepOriginal.isSelected();
    }
        
    public boolean isKeepOriginalAndImportOption() {
        return keepOriginalAndImport.isSelected();
    }
        
    public boolean isKeepOriginalAndUpdateOption() {
        return keepOriginalAndUpdate.isSelected();
    }
        
    public boolean isUpdateOriginalOption() {
        return updateOriginal.isSelected();
    }
        
    public boolean isEraseOriginalAndImport() {
        return eraseOriginalAndImport.isSelected();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
