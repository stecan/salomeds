package salomeTMF_plug.docXML.importxml;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JTable;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.admin.Administration;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.TestDriver;
import org.objectweb.salome_tmf.plugins.core.XMLLoaderPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.common.CreateProjectDialog;
import salomeTMF_plug.docXML.languages.Language;

public class ImportXML implements ApiConstants, IHMConstants, XMLLoaderPlugin {

    private static String fs = System.getProperties().getProperty(
                                                                  "file.separator");
    private Document doc;
    private ImportDialog idialog;
    private CreateProjectDialog cdialog;

    ArrayList<TestList> suiteSelectionList;
    ArrayList<Family> familySelectionList;
    ArrayList<Test> testSelectionList;

    Project project;

    String dirXml;

    private boolean selectionDesTests;
    private boolean importOnlyTests;
    private boolean annule = false;
    private boolean newProject = false;

    ISQLProject pISQLProject;

    AdminVTData pAdminVTData;

    Vector<Test> automatedTest2Update;
    IPlugObject pIPlugObject;

    // ImportXMLReq pImportReq;

    Vector<XMLPrinterPlugin> listXMLPlugin = new Vector<XMLPrinterPlugin>();

    /**
     * Affichage des exception lors de l'import des attachements
     */
    boolean attachmentExecption;

    public ImportXML(JDialog dialog, Document doc,
                     ArrayList<Family> familySList, ArrayList<TestList> suiteSList,
                     ArrayList<Test> testSList, AdminVTData adminVTData,
                     IPlugObject pIPlugObject) throws Exception {
        super();
        this.doc = doc;
        this.pIPlugObject = pIPlugObject;
        Vector<Extension> listExtXMLPlugin = pIPlugObject
            .getXMLPrintersExtension();
        for (Extension pXMLExt : listExtXMLPlugin) {
            JPFManager pJPFManager = pIPlugObject.getPluginManager();
            try {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager
                    .activateExtension(pXMLExt);
                if (!listXMLPlugin.contains(pXMLPrinterPlugin)) {
                    listXMLPlugin.add(pXMLPrinterPlugin);
                }
            } catch (Exception e) {

            }
        }
        suiteSelectionList = suiteSList;
        familySelectionList = familySList;
        testSelectionList = testSList;
        pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        pAdminVTData = adminVTData;
        attachmentExecption = false;
        if (dialog instanceof ImportDialog) {
            idialog = (ImportDialog) dialog;
        } else {
            cdialog = (CreateProjectDialog) dialog;
        }
    }

    /**
     * Supprime les parametres presents dans le projet mais non dans le
     * documents
     *
     * @throws Exception
     */
    boolean suppressionParameter() throws Exception {
        // Suppression des parametres du projet
        Hashtable<String, Parameter> paramSet = project
            .getParameterSetFromModel();
        Enumeration<Parameter> itParamProjet = paramSet.elements();

        while (itParamProjet.hasMoreElements() && !annule) {
            Parameter param = itParamProjet.nextElement();
            Iterator<Node> itParamXML = doc.selectNodes("//Param").iterator();
            boolean appartient = false;
            while (itParamXML.hasNext()) {
                Element paramElem = (Element) itParamXML.next();
                if (paramElem.elementText("Nom").equals(
                                                        param.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                try {
                    project.deleteParamInDBndModel(param);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                    annule = true;
                }
            }
        }
        return annule;
    }

    /**
     * Supprime les attachements de pWithAttachment qui sont presents dans le
     * projet mais pas dans le documents
     *
     * @param pDocElem
     * @param pWithAttachment
     * @return
     */
    boolean suppressionAttachment(Element pDocElem,
                                  WithAttachment pWithAttachment) throws Exception {
        // suppression des attachements de l'environnement
        boolean appartient;
        List<Node> urlAttachementList = pDocElem
            .selectNodes("Attachements/UrlAttachement");
        List<Node> fileAttachementList = pDocElem
            .selectNodes("Attachements/FileAttachement");
        HashMap<String, Attachment> envMap = pWithAttachment
            .getAttachmentMapFromModel();
        ArrayList<Attachment> envMapToRemove = new ArrayList<Attachment>();
        Iterator<Attachment> itWithAttachmentMap = envMap.values().iterator();
        while (itWithAttachmentMap.hasNext()) {
            appartient = false;
            Attachment attach = itWithAttachmentMap.next();

            if (attach instanceof UrlAttachment) {
                UrlAttachment pUrlAttachment = (UrlAttachment) attach;
                Iterator<Node> itWithAttachmentUrlAttachXML = urlAttachementList
                    .iterator();
                while (itWithAttachmentUrlAttachXML.hasNext()
                       && appartient == false) {
                    Element urlElem = (Element) itWithAttachmentUrlAttachXML
                        .next();
                    if (pUrlAttachment.getNameFromModel().equals(
                                                                 urlElem.attributeValue("url"))) {
                        appartient = true;
                    }
                }
            } else {
                FileAttachment pFileAttachment = (FileAttachment) attach;
                Iterator<Node> itEnvFileAttachXML = fileAttachementList
                    .iterator();
                while (itEnvFileAttachXML.hasNext() && appartient == false) {
                    Element fileElem = (Element) itEnvFileAttachXML.next();
                    if (pFileAttachment.getNameFromModel().equals(
                                                                  fileElem.attributeValue("nom"))) {
                        appartient = true;
                    }
                }
            }
            if (!appartient) {
                envMapToRemove.add(attach);
            }

        }
        Iterator<Attachment> itMapRm = envMapToRemove.iterator();
        while (itMapRm.hasNext()) {
            Attachment attach = (Attachment) itMapRm.next();
            try {
                pWithAttachment.deleteAttachementInDBAndModel(attach);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                // annule = true;
            }
        }
        return annule;
    }

    /**
     * Supprime les environnements qui sont presents dans le projet mais pas
     * dans le documents
     *
     * @return
     */
    boolean suppressionEnvironnement() throws Exception {
        // suppressions des environnements
        ArrayList<Element> envList = (ArrayList<Element>) doc
            .selectNodes("//Environnement");
        ArrayList<Environment> envInBDD = project.getEnvironmentListFromModel();
        Iterator<Environment> itEnv = envInBDD.iterator();
        while (itEnv.hasNext() && !annule) {
            Environment env = itEnv.next();
            Iterator<Element> itEnvXML = envList.iterator();
            Element envElem = null;
            boolean appartient = false;
            while (itEnvXML.hasNext() && !appartient) {
                envElem = (Element) itEnvXML.next();
                if (envElem.elementText("Nom").equals(env.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                try {
                    project.deleteEnvironmentInDBandModel(env);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                    annule = true;
                }
            } else {
                // suppression parametres values dans l'environnement
                // int idEnv = env.getIdBdd();
                List<Node> paramEnvListXML = envElem
                    .selectNodes("ValeurParams/ValeurParam");
                // ArrayList paramsToRemove = new ArrayList();
                Hashtable<Parameter, String> paramEnvList = env
                    .getParametersHashTableFromModel();
                Enumeration<Parameter> enumEnv = paramEnvList.keys();
                while (enumEnv.hasMoreElements()) {
                    appartient = false;
                    Parameter paramEnv = enumEnv.nextElement();
                    Iterator<Node> itParamEnvXML = paramEnvListXML.iterator();
                    while (itParamEnvXML.hasNext()) {
                        Element paramElem = (Element) itParamEnvXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (paramEnv.getNameFromModel().equals(nomParam)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        try {
                            env.deleteDefParamInDBAndModel(paramEnv);
                        } catch (Exception e) {
                            Tools.ihmExceptionView(e);
                            // annule = true;
                        }
                    }
                }
                // suppression du script de l'environnement
                Script script = env.getInitScriptFromModel();
                if (script != null) {
                    if (envElem.element("Script") == null) {
                        try {
                            env.deleteScriptInDBAndModel();
                        } catch (Exception e) {
                            Tools.ihmExceptionView(e);
                            // annule = true;
                        }
                    }
                }
                // suppression des attachements de l'environnement
                suppressionAttachment(envElem, env);
            }
        }
        return annule;
    }

    /**
     * Methode qui supprime les actions d'un test manuel
     *
     * @param test
     *            le test manuel concerne
     * @param testElem
     *            l'element du document XML qui correspond au test
     */
    boolean supprimerActions(Element testElem, ManualTest test)
        throws Exception {
        ArrayList<Action> actionList = test.getActionListFromModel(false);
        List<Node> actionListXML = testElem
            .selectNodes("TestManuel/ActionTest");
        ArrayList<Action> actionToRemove = new ArrayList<Action>();
        boolean appartient;
        for (Action action : actionList) {
            Element actionElem = null;
            appartient = false;
            Iterator<Node> itActionXML = actionListXML.iterator();
            while (itActionXML.hasNext() && !appartient) {
                actionElem = (Element) itActionXML.next();
                if (actionElem.elementText("Nom").equals(
                                                         action.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                actionToRemove.add(action);
            } else {
                // gestion des parametres des actions
                Hashtable<String, Parameter> actionParam = action
                    .getParameterHashSetFromModel();
                Enumeration<Parameter> itParamAction = actionParam.elements();
                ArrayList<Parameter> paramActionToRemove = new ArrayList<Parameter>();
                while (itParamAction.hasMoreElements()) {
                    Parameter param = (Parameter) itParamAction.nextElement();
                    appartient = false;
                    Iterator<Node> itParamActionXML = actionElem.selectNodes(
                                                                             "ParamsT/ParamT").iterator();
                    while (itParamActionXML.hasNext()) {
                        Element paramElem = (Element) itParamActionXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (param.getNameFromModel().equals(nomParam)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        paramActionToRemove.add(param);
                    }
                }
                for (Parameter param : paramActionToRemove) {
                    try {
                        action.deleteUseParamInDBAndModel(param);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                // gestion des attachements des actions
                suppressionAttachment(actionElem, action);
            }

        }
        for (Action actionRm : actionToRemove) {
            try {
                test.deleteActionInDBModel(actionRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                annule = true;
                return annule;
            }
        }
        return annule;
    }

    /**
     * Supprime les tests qui sont presents dans le projet mais pas dans le
     * documents
     *
     * @param suiteElem
     * @param suite
     * @return
     */
    boolean suppressionTest(Element suiteElem, TestList suite) throws Exception {
        boolean appartient = false;
        ArrayList<Test> testList = suite.getTestListFromModel();
        ArrayList<Test> testToRm = new ArrayList<Test>();
        Iterator<Test> itTest = testList.iterator();
        while (itTest.hasNext() && !annule) {
            Test test = itTest.next();
            appartient = false;
            Element testElem = null;
            if (suiteElem != null) {
                Iterator<Node> itTestXML = suiteElem.selectNodes("Tests/Test")
                    .iterator();
                while (itTestXML.hasNext() && !appartient) {
                    testElem = (Element) itTestXML.next();
                    if (testElem.elementText("Nom").equals(
                                                           test.getNameFromModel())) {
                        if (isSelectionDesTests()) {
                            Iterator<Test> itTestSelect = testSelectionList
                                .iterator();
                            boolean selectionneTest = false;
                            while (itTestSelect.hasNext()) {
                                Test testSelect = (Test) itTestSelect.next();
                                if (testSelect.getNameFromModel().equals(
                                                                         test.getNameFromModel())
                                    && testSelect
                                    .getTestListFromModel()
                                    .getNameFromModel()
                                    .equals(
                                            test
                                            .getTestListFromModel()
                                            .getNameFromModel())
                                    && testSelect
                                    .getTestListFromModel()
                                    .getFamilyFromModel()
                                    .getNameFromModel()
                                    .equals(
                                            test
                                            .getTestListFromModel()
                                            .getFamilyFromModel()
                                            .getNameFromModel())) {
                                    selectionneTest = true;
                                }
                            }
                            if (selectionneTest) {
                                appartient = true;
                            }
                        } else {
                            appartient = true;
                        }
                    }
                }
            }
            if (!appartient) {
                testToRm.add(test);
            } else {
                // gestion des parametres
                ArrayList<Parameter> paramTestList = test
                    .getParameterListFromModel();
                List<Node> paramTestXML = testElem
                    .selectNodes("ParamsT/ParamT");
                ArrayList<Parameter> paramTestToRemove = new ArrayList<Parameter>();
                for (Parameter param : paramTestList) {
                    appartient = false;
                    Iterator<Node> itParamTestXML = paramTestXML.iterator();
                    while (itParamTestXML.hasNext()) {
                        Element paramElem = (Element) itParamTestXML.next();
                        String nomParam = paramElem.elementText("Nom");
                        if (param.getNameFromModel().equals(nomParam)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        paramTestToRemove.add(param);
                    }
                }
                for (Parameter param : paramTestToRemove) {
                    try {
                        test.deleteUseParamInDBAndModel(param);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                // gestion des attachements
                suppressionAttachment(testElem, test);

                // si test automatique gestion du script
                if (test instanceof AutomaticTest) {
                    Script script = ((AutomaticTest) test).getScriptFromModel();
                    if (script != null) {
                        Element scriptElem = (Element) testElem
                            .selectSingleNode("TestAuto/Script");
                        if (scriptElem == null
                            || !scriptElem.attributeValue("nom").equals(
                                                                        script.getNameFromModel())) {
                            try {
                                ((AutomaticTest) test)
                                    .deleteScriptInDBAndModel();
                            } catch (Exception e) {
                                Tools.ihmExceptionView(e);
                            }
                        }
                    }
                } else {
                    if (idialog.isSupprAction()) {
                        // si test manuel gestion des actions
                        supprimerActions(testElem, (ManualTest) test);
                    }
                }
            }
        }
        Iterator<Test> itTestRm = testToRm.iterator();
        while (itTestRm.hasNext() && !annule) {
            Test test = itTestRm.next();
            try {
                suite.deleteTestInDBAndModel(test);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                annule = true;
                return annule;
            }
        }
        return annule;
    }

    /**
     * Supprime les suites qui sont presents dans le projet mais pas dans le
     * documents
     *
     * @param famElem
     * @param pFamily
     * @return
     */
    boolean suppressionSuite(Element famElem, Family pFamily) throws Exception {
        // gestion des suites
        boolean appartient = false;
        ArrayList<TestList> suiteList = pFamily.getSuiteListFromModel();
        ArrayList<TestList> suiteToRemove = new ArrayList<TestList>();
        Element suiteElem = null;
        List<Node> suiteListXML = famElem.selectNodes("SuiteTests/SuiteTest");
        Iterator<TestList> itSuite = suiteList.iterator();
        while (itSuite.hasNext() && !annule) {
            TestList suite = itSuite.next();
            appartient = false;
            Iterator<Node> itSuiteXML = suiteListXML.iterator();
            while (itSuiteXML.hasNext() && !appartient) {
                suiteElem = (Element) itSuiteXML.next();
                if (suiteElem.elementText("Nom").equals(
                                                        suite.getNameFromModel())) {
                    if (isSelectionDesTests()) {
                        Iterator<TestList> itSuiteSelect = suiteSelectionList
                            .iterator();
                        boolean selectionneSuite = false;
                        while (itSuiteSelect.hasNext()) {
                            TestList suiteSelect = itSuiteSelect.next();
                            if (suiteSelect.getNameFromModel().equals(
                                                                      suite.getNameFromModel())
                                && suiteSelect
                                .getFamilyFromModel()
                                .getNameFromModel()
                                .equals(
                                        suite.getFamilyFromModel()
                                        .getNameFromModel())) {
                                selectionneSuite = true;
                            }
                        }
                        if (selectionneSuite) {
                            appartient = true;
                        }
                    } else {
                        appartient = true;
                    }
                }
            }
            if (!appartient) {
                suiteToRemove.add(suite);
            } else {
                // gestion de la suppression des attachements de la suite
                suppressionAttachment(suiteElem, suite);
                // on gere la suppression des tests appartenant a la suite
                suppressionTest(suiteElem, suite);
            }
        }
        Iterator<TestList> itSuiteRm = suiteToRemove.iterator();
        while (itSuiteRm.hasNext() && !annule) {
            TestList suite = itSuiteRm.next();
            try {
                project.deleteTestListInDBandModel(suite);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                annule = true;
                return annule;
            }
        }
        return annule;
    }

    /**
     * Supprime les familles qui sont presents dans le projet mais pas dans le
     * documents
     *
     * @return
     */
    boolean suppressionFamily() throws Exception {
        // suppression des familles
        ArrayList<Family> familleList = project.getFamilyListFromModel();
        ArrayList<Family> familleToRemove = new ArrayList<Family>();
        List<Node> familleListXML = doc.selectNodes("//Famille");
        Element famElem = null;
        Iterator<Family> itFamily = familleList.iterator();
        while (itFamily.hasNext() && !annule) {
            Family family = itFamily.next();
            boolean appartient = false;
            Iterator<Node> itFamXML = familleListXML.iterator();
            while (itFamXML.hasNext() && !appartient) {
                famElem = (Element) itFamXML.next();
                if (famElem.elementText("Nom")
                    .equals(family.getNameFromModel())) {
                    if (isSelectionDesTests()) {
                        Iterator<Family> itFamilleSelect = familySelectionList
                            .iterator();
                        boolean selectionneFamille = false;
                        while (itFamilleSelect.hasNext()) {
                            Family familleSelect = itFamilleSelect.next();
                            if (familleSelect.getNameFromModel().equals(
                                                                        family.getNameFromModel())) {
                                selectionneFamille = true;
                            }
                        }
                        if (selectionneFamille) {
                            appartient = true;
                        }
                    } else {
                        appartient = true;
                    }
                }
            }
            if (!appartient) {
                familleToRemove.add(family);
            } else {
                //
                suppressionAttachment(famElem, family);
                // la famille appartient au document XML, gestion des suites
                suppressionSuite(famElem, family);
            }
        }
        Iterator<Family> itFamRm = familleToRemove.iterator();
        while (itFamRm.hasNext() && !annule) {
            Family family = itFamRm.next();
            try {
                project.deleteFamilyInDBAndModel(family);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                annule = true;
                return annule;
            }
        }
        return annule;
    }

    /**
     * Methode qui permet de supprimer les elements qui sont dans le projet mais
     * pas dans le document XML
     */
    public void gestionDesSuppressionsTests() throws Exception {
        // Suppression des parametres du projet
        if (suppressionParameter()) {
            throw new Exception("[ImportAlgo->suppressionParameter]");
        }

        // suppressions des environnements
        if (suppressionEnvironnement()) {
            throw new Exception("[ImportAlgo->suppressionEnvironnement]");
        }

        // suppression des familles
        if (suppressionFamily()) {
            throw new Exception("[ImportAlgo->suppressionFamily]");
        }
    }

    /**
     * Supprime les jeux de donnees qui sont presents dans le projet mais pas
     * dans le documents
     *
     * @param campElem
     * @param camp
     */
    void suppressionDataSet(Element campElem, Campaign camp) throws Exception {
        ArrayList<DataSet> jeuxList = camp.getDataSetListFromModel();
        ArrayList<DataSet> jeuxToRemove = new ArrayList<DataSet>();
        List<Node> jeuxListXML = campElem.selectNodes("JeuxDonnees/JeuDonnees");
        Element jeuElem = null;
        Iterator<DataSet> itJeu = jeuxList.iterator();
        boolean appartient;
        while (itJeu.hasNext()) {
            DataSet jeu = itJeu.next();
            appartient = false;
            Iterator<Node> itJeuXML = jeuxListXML.iterator();
            while (itJeuXML.hasNext()) {
                jeuElem = (Element) itJeuXML.next();
                if (jeuElem.elementText("Nom").equals(jeu.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                jeuxToRemove.add(jeu);
            }
        }
        Iterator<DataSet> itJeuRm = jeuxToRemove.iterator();
        while (itJeuRm.hasNext()) {
            DataSet jeuRm = (DataSet) itJeuRm.next();
            try {
                camp.deleteDataSetInDBAndModel(jeuRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Supprime les tests d'une campagne qui sont presents dans le projet mais
     * pas dans le documents
     *
     * @param campElem
     * @param camp
     */
    void suppressionTestInCamp(Element campElem, Campaign camp)
        throws Exception {
        ArrayList<Test> testList = camp.getTestListFromModel();
        ArrayList<Test> testToRemove = new ArrayList<Test>();
        List<Node> testListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef");
        Element testElem = null;
        Iterator<Test> itTest = testList.iterator();
        boolean appartient;
        while (itTest.hasNext()) {
            Test test = itTest.next();
            String suiteName = test.getTestListFromModel().getNameFromModel();
            String familyName = test.getTestListFromModel()
                .getFamilyFromModel().getNameFromModel();
            appartient = false;
            Iterator<Node> itTestXML = testListXML.iterator();
            while (itTestXML.hasNext() && !appartient) {
                testElem = (Element) itTestXML.next();
                String nomSuite = ((Element) testElem
                                   .selectSingleNode("ancestor::SuiteTestRef[1]"))
                    .elementText("Nom");
                String nomFamille = ((Element) testElem
                                     .selectSingleNode("ancestor::FamilleRef[1]"))
                    .elementText("Nom");
                String nomTest = testElem.elementText("Nom");
                if (test.getNameFromModel().equals(nomTest)
                    && suiteName.equals(nomSuite)
                    && familyName.equals(nomFamille)) {
                    appartient = true;
                }
            }
            if (!appartient) {
                testToRemove.add(test);
            }
        }
        Iterator<Test> itTestRm = testToRemove.iterator();
        while (itTestRm.hasNext()) {
            Test testRm = (Test) itTestRm.next();
            try {
                camp.deleteTestFromCampInDBAndModel(testRm, true);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Supprime les executions d'une campagne qui sont presents dans le projet
     * mais pas dans le documents
     *
     * @param campElem
     * @param camp
     */
    void suppressionExecInCamp(Element campElem, Campaign camp)
        throws Exception {
        ArrayList<Execution> execList = camp.getExecutionListFromModel();
        ArrayList<Execution> execToRemove = new ArrayList<Execution>();
        List<Node> execListXML = campElem
            .selectNodes("ExecCampTests/ExecCampTest");
        Element execElem = null;
        Iterator<Execution> itExec = execList.iterator();
        boolean appartient;
        while (itExec.hasNext() && !annule) {
            Execution exec = itExec.next();
            String execName = exec.getNameFromModel();
            appartient = false;
            Iterator<Node> itExecXML = execListXML.iterator();
            while (itExecXML.hasNext() && !appartient) {
                execElem = (Element) itExecXML.next();
                String nomExec = execElem.elementText("Nom");
                if (nomExec.equals(execName)) {
                    appartient = true;
                }
            }
            if (!appartient) {
                execToRemove.add(exec);
            } else {
                // suppression des resultats d'execution
                ArrayList<ExecutionResult> resExecList = exec
                    .getExecutionResultListFromModel();
                ArrayList<ExecutionResult> resExecToRemove = new ArrayList<ExecutionResult>();
                List<Node> resExecListXML = execElem
                    .selectNodes("ResulExecCampTests/ResulExecCampTest");
                Element resExecElem = null;
                Iterator<ExecutionResult> itResExec = resExecList.iterator();
                while (itResExec.hasNext()) {
                    ExecutionResult resExec = itResExec.next();
                    String resExecName = resExec.getNameFromModel();
                    appartient = false;
                    Iterator<Node> itResExecXML = resExecListXML.iterator();
                    while (itResExecXML.hasNext() && !appartient) {
                        resExecElem = (Element) itResExecXML.next();
                        String nomResExec = resExecElem.elementText("Nom");
                        if (nomResExec.equals(resExecName)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        resExecToRemove.add(resExec);
                    }
                }
                Iterator<ExecutionResult> itResExecRm = resExecToRemove
                    .iterator();
                while (itResExecRm.hasNext()) {
                    ExecutionResult resExecRm = itResExecRm.next();
                    try {
                        exec.deleteExecutionResultInDBAndModel(resExecRm);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
        Iterator<Execution> itExecRm = execToRemove.iterator();
        while (itExecRm.hasNext() && !annule) {
            Execution execRm = itExecRm.next();
            try {
                camp.deleteExecutionInDBAndModel(execRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Supprime les campagnes qui sont presents dans le projet mais pas dans le
     * documents
     *
     * @throws Exception
     */
    public void gestionDesSuppressionsCampagnes() throws Exception {
        ArrayList<Campaign> campList = project.getCampaignListFromModel();
        ArrayList<Campaign> campToRemove = new ArrayList<Campaign>();
        List<Node> campListXML = doc.selectNodes("//CampagneTest");
        Element campElem = null;
        Iterator<Campaign> itCamp = campList.iterator();
        while (itCamp.hasNext() && !annule) {
            Campaign camp = itCamp.next();
            boolean appartient = false;
            Iterator<Node> itCampXML = campListXML.iterator();
            while (itCampXML.hasNext() && !appartient) {
                campElem = (Element) itCampXML.next();
                if (campElem.elementText("Nom").equals(camp.getNameFromModel())) {
                    appartient = true;
                }
            }
            if (!appartient) {
                campToRemove.add(camp);
            } else {
                // suppression des jeux de donnees
                suppressionDataSet(campElem, camp);

                // suppression des attachements des campagnes
                suppressionAttachment(campElem, camp);

                // suppression Tests
                suppressionTestInCamp(campElem, camp);

                // suppression Executions -> suppression resultats d'execution
                suppressionExecInCamp(campElem, camp);
            }
        }
        Iterator<Campaign> itCampRm = campToRemove.iterator();
        while (itCampRm.hasNext() && !annule) {
            Campaign campRm = itCampRm.next();
            try {
                project.deleteCampaignInDBAndModel(campRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
                throw e;
            }
        }
    }

    /**
     * Methode qui supprime les campagnes lorsque l'utilisateur ne veut importer
     * que les tests
     */
    public void suppressionsCampagnes() throws Exception {
        ArrayList<Campaign> campList = project.getCampaignListFromModel();
        ArrayList<Campaign> campToRemove = new ArrayList<Campaign>();
        Iterator<Campaign> itCamp = campList.iterator();
        while (itCamp.hasNext() && !annule) {
            Campaign camp = (Campaign) itCamp.next();
            campToRemove.add(camp);
        }
        Iterator<Campaign> itCampRm = campToRemove.iterator();
        while (itCampRm.hasNext() && !annule) {
            Campaign campRm = (Campaign) itCampRm.next();
            try {
                project.deleteCampaignInDBAndModel(campRm);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    public void gestionDesSuppressionsProject() throws Exception {
        List<Node> projectList = doc.selectNodes("//ProjetVT");
        Element projElem = null;
        Iterator<Node> itProj = projectList.iterator();
        while (itProj.hasNext()) {
            projElem = (Element) itProj.next();
            suppressionAttachment(projElem, project);
        }
    }

    /************************* UPADATE ***************************************************/

    void updateProjectAttachement() throws Exception {
        ArrayList<Node> projectList = (ArrayList<Node>) doc
            .selectNodes("//ProjetVT");
        Iterator<Node> itProject = projectList.iterator();
        while (itProject.hasNext()) {
            Element projectElem = (Element) itProject.next();
            updateElementAttachement(projectElem, project, false);
        }
    }

    void updateProjectParameter() throws Exception {
        ArrayList<Node> paramList = (ArrayList<Node>) doc
            .selectNodes("//Param");
        Iterator<Node> itParam = paramList.iterator();
        while (itParam.hasNext() && !annule) {
            Element paramElem = (Element) itParam.next();
            String nomParam = paramElem.elementText("Nom");
            String descriptionParam = (paramElem.elementText("Description") == null) ? ""
                : paramElem.elementText("Description");
            descriptionParam = descriptionParam.replaceAll("\\\\n", "\n");
            Parameter newParamter = new Parameter(nomParam, descriptionParam);
            boolean existe = false;
            Parameter par = null;
            Enumeration<Parameter> itParam2 = project
                .getParameterSetFromModel().elements();
            while (itParam2.hasMoreElements() && !existe) {
                par = itParam2.nextElement();
                if (par.getNameFromModel().equals(nomParam)) {
                    existe = true;
                }
            }

            if (!existe) {
                // Ajout du nouveau parametre
                project.addParameterToDBAndModel(newParamter);
                par = newParamter;
            } else {
                // Modification du parametre
                par.updateDescriptionInDBAndModel(descriptionParam);
            }
            /***** Plugin update parameter data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateParameterFromXML(paramElem, par,
                                                             idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateParameterFromXML(paramElem, par,
                                                             false, this);
                }
            }
            /***** (END) Plugin update parameter data *****/
        }
    }

    /*
     * String getParentElement(Hashtable docReqById, Integer id_reqParent){
     *
     * Element reqElem = (Element)docReqById.get(id_reqParent); }
     */

    void updateProjectEnvironment() throws Exception {
        // Environnements du projet
        Element paramsElem = (Element) doc.selectSingleNode("//Params");
        ArrayList<Node> envList = (ArrayList<Node>) doc
            .selectNodes("//Environnement");
        ArrayList<Environment> envInBDD = project.getEnvironmentListFromModel();
        Iterator<Node> itEnv = envList.iterator();
        while (itEnv.hasNext() && !annule) {
            Element envElem = (Element) itEnv.next();
            String nomEnv = envElem.elementText("Nom");
            String descriptionEnv = (envElem.elementText("Description") == null) ? ""
                : envElem.elementText("Description");
            descriptionEnv = descriptionEnv.replaceAll("\\\\n", "\n");
            Iterator<Environment> itEnvBDD = envInBDD.iterator();
            Environment env = null;
            boolean existeEnv = false;
            while (itEnvBDD.hasNext() && !existeEnv) {
                env = itEnvBDD.next();
                if (env.getNameFromModel().equals(nomEnv)) {
                    existeEnv = true;
                }
            }
            boolean newEnv = false;
            if (!existeEnv) {
                env = null;
                env = new Environment(nomEnv, descriptionEnv);
                project.addEnvironmentInDBAndModel(env);
                newEnv = true;
            } else {
                env.updateInDBAndModel(nomEnv, descriptionEnv);
            }
            // Valeurs des parametres
            List<Node> paramEnvList = envElem
                .selectNodes("ValeurParams/ValeurParam");
            Iterator<Node> itParamEnv = paramEnvList.iterator();
            while (itParamEnv.hasNext()) {
                Element paramElem = (Element) itParamEnv.next();
                String valeurParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                    .attributeValue("valeur")
                    : "";
                String id_paramDoc = paramElem.attributeValue("ref");
                Element paramRootElem = (Element) paramsElem
                    .selectSingleNode("Param[@id_param = '" + id_paramDoc
                                      + "']");
                String nomParam = paramRootElem.elementText("Nom");
                // String descriptionParam =
                // (paramRootElem.elementText("Description") ==
                // null)?"":paramRootElem.elementText("Description");
                boolean existe = false;
                Parameter paramEnv = null;
                if (env != null && newEnv == false) {
                    Hashtable<Parameter, String> paramEnvInBDD = env
                        .getParametersHashTableFromModel();
                    Enumeration<Parameter> enumParamEnv = paramEnvInBDD.keys();
                    while (enumParamEnv.hasMoreElements() && !existe) {
                        paramEnv = enumParamEnv.nextElement();
                        if (nomParam.equals(paramEnv.getNameFromModel())) {
                            existe = true;
                        }
                    }
                }
                if (!existe) {
                    paramEnv = project.getParameterFromModel(nomParam);
                    if (paramEnv != null) {
                        env.addParameterValueInDBModel(paramEnv, valeurParam);
                    }
                } else {
                    env.addParameterValueInDBModel(paramEnv, valeurParam);
                }
            }
            // Ajout ou mise a jour du script de l'environnement
            if (envElem.element("Script") != null) {
                String classpath = envElem.element("Script").elementText(
                                                                         "Classpath");
                String argScript = envElem.element("Script").elementText(
                                                                         "ArgScript");
                String type = envElem.element("Script").attributeValue("type");
                String dirScript = envElem.element("Script").attributeValue(
                                                                            "dir");
                dirScript = restorePath(dirScript);
                File fScript = new File(dirXml + fs + dirScript);

                Script pScritp = new Script(fScript.getName(), "");
                pScritp.setTypeInModel(type);
                pScritp.setScriptExtensionInModel(classpath);
                pScritp.updatePlugArgInModel(argScript);
                try {
                    if (newEnv == true) {
                        env.addScriptInDBAndModel(pScritp, fScript);
                    } else {
                        env.deleteScriptInDBAndModel();
                        env.addScriptInDBAndModel(pScritp, fScript);
                    }
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            }
            updateElementAttachement(envElem, env, newEnv);

            /***** Plugin update Env data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateEnvironmentFromXML(envElem, env,
                                                               idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateEnvironmentFromXML(envElem, env,
                                                               false, this);
                }
            }
            /***** (END) Plugin update Env data *****/
        }
    }

    public void updateElementAttachement(Element envElem,
                                         WithAttachment simpleElement, boolean newElement) {
        try {
            // URL
            List<Node> urlAttachementList = envElem
                .selectNodes("Attachements/UrlAttachement");
            Iterator<Node> itEnvUrlAttach = urlAttachementList.iterator();
            while (itEnvUrlAttach.hasNext()) {
                Element urlElem = (Element) itEnvUrlAttach.next();
                String url = urlElem.attributeValue("url");
                try {
                    new URL(url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    continue;
                }
                String description = (urlElem.elementText("Description") == null) ? ""
                    : urlElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (newElement == false) {
                    HashMap<String, Attachment> envMap = simpleElement
                        .getAttachmentMapFromModel();
                    Iterator<Attachment> itEnvAttachs = envMap.values()
                        .iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = itEnvAttachs.next();
                        if ((attach instanceof UrlAttachment)
                            && ((UrlAttachment) attach).getNameFromModel()
                            .equals(url)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    simpleElement.addAttachementInDBAndModel(new UrlAttachment(
                                                                               url, description));
                } else {
                    ((UrlAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }
            }
            // fichiers
            List<Node> envFileAttachementList = envElem
                .selectNodes("Attachements/FileAttachement");
            Iterator<Node> itEnvFileAttach = envFileAttachementList.iterator();
            while (itEnvFileAttach.hasNext()) {
                Element fileElem = (Element) itEnvFileAttach.next();
                String dirAtt = fileElem.attributeValue("dir");
                String nom = fileElem.attributeValue("nom");
                dirAtt = restorePath(dirAtt);
                File f = new File(dirXml + fs + dirAtt);
                String description = (fileElem.elementText("Description") == null) ? ""
                    : fileElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (newElement == false) {
                    HashMap<String, Attachment> envMap = simpleElement
                        .getAttachmentMapFromModel();
                    Iterator<Attachment> itEnvAttachs = envMap.values()
                        .iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = (Attachment) itEnvAttachs.next();
                        if ((attach instanceof FileAttachment)
                            && ((FileAttachment) attach).getNameFromModel()
                            .equals(nom)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    attach = new FileAttachment(f, description);
                    simpleElement.addAttachementInDBAndModel(attach);
                } else {
                    ((FileAttachment) attach).updateInDBAndModel(f);
                    ((FileAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            if (!attachmentExecption) {
                idialog
                    .setErrorMessage(idialog.getErrorMessage()
                                     + Language
                                     .getInstance()
                                     .getText(
                                              "Probleme_lors_de_la_recuperation_des_attachements"));
                attachmentExecption = true;
            }
            idialog.showErrorMessage();
        }
    }

    /**
     * Dans le cas ou le test etait deja dans la BD, ajout des parametres
     *
     * @param test
     *            le test concerne
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void updateParametresTest(Element testElem, Test test)
        throws Exception {
        // on ajoute les parametres que si ils n'existent pas deja dans la bd
        List<Node> paramList = testElem.selectNodes("ParamsT/ParamT/Nom");
        Iterator<Node> itParam = paramList.iterator();
        while (itParam.hasNext()) {
            String nomParam = ((Element) itParam.next()).getText();
            boolean appartient = false;
            Parameter param = null;
            if (test != null) {
                ArrayList<Parameter> paramListBDD = test
                    .getParameterListFromModel();
                Iterator<Parameter> itparam = paramListBDD.iterator();
                while (itparam.hasNext() && !appartient) {
                    param = itparam.next();
                    if (param.getNameFromModel().equals(nomParam)) {
                        appartient = true;
                    }
                }
            }
            try {
                if (!appartient) {
                    param = project.getParameterFromModel(nomParam);
                    if (param != null) {
                        test.setUseParamInDBAndModel(param);
                    } else {
                        // AIE
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Methode qui verifie que tous les parametres d'un test manuel sont bien
     * utilises, sinon elle les supprime
     *
     * @param test
     *            le test manuel concerne
     */
    public void verifUtilisationParams(ManualTest test) throws Exception {
        ArrayList<Parameter> listParams = test.getParameterListFromModel();
        ArrayList<Parameter> paramToRemove = new ArrayList<Parameter>();
        Iterator<Parameter> itParam = listParams.iterator();
        while (itParam.hasNext()) {
            Parameter param = itParam.next();
            boolean appartient = false;
            ArrayList<Action> listActions = test.getActionListFromModel(false);
            Iterator<Action> itAction = listActions.iterator();
            while (itAction.hasNext()) {
                Action action = itAction.next();
                Hashtable<String, Parameter> paramActionSet = action
                    .getParameterHashSetFromModel();
                Enumeration<Parameter> itParamAction = paramActionSet
                    .elements();
                while (itParamAction.hasMoreElements()) {
                    Parameter paramAction = itParamAction.nextElement();
                    if (param.getNameFromModel().equals(
                                                        paramAction.getNameFromModel())) {
                        appartient = true;
                    }
                }
            }
            if (!appartient) {
                paramToRemove.add(param);
            }
        }
        if (!paramToRemove.isEmpty()) {
            Iterator<Parameter> paramRm = paramToRemove.iterator();
            while (paramRm.hasNext()) {
                Parameter param = paramRm.next();
                try {
                    // test.deleteUseParamFromDB(param);
                    test.deleteUseParamInDBAndModel(param);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            }
        }
    }

    /**
     * Dans le cas ou le test etait deja dans la BD, ajout ou mise a jour du
     * script
     *
     * @param familyName
     *            nom de la famille auquelle appartient le test
     * @param suiteName
     *            nom de la suite auquelle appartient le test
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void updateTestScript(Element testElem, AutomaticTest test)
        throws Exception {
        Element scriptElem = (Element) testElem
            .selectSingleNode("TestAuto/Script");
        try {
            if (scriptElem != null) {
                String classpath = scriptElem.elementText("Classpath");
                String argScript = scriptElem.elementText("ArgScript");
                String type = scriptElem.attributeValue("type");
                String dirScript = scriptElem.attributeValue("dir");
                dirScript = restorePath(dirScript);
                File fScript = new File(dirXml + fs + dirScript);
                // Script fileScript = null;

                Script pScritp = new Script(fScript.getName(), "");
                pScritp.setTypeInModel(type);
                pScritp.setScriptExtensionInModel(classpath);
                pScritp.updatePlugArgInModel(argScript);

                if (test != null) {
                    if (test.getScriptFromModel() != null) {
                        test.deleteScriptInDBAndModel();
                    }

                }
                test.addScriptInDBAndModel(pScritp, fScript);

            } else {
                if (test.getScriptFromModel() != null) {
                    test.deleteScriptInDBAndModel();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            idialog.setErrorMessage(idialog.getErrorMessage()
                                    + Language.getInstance().getText(
                                                                     "Probleme_lors_de_la_suppression_d_un_script"));
            idialog.showErrorMessage();
        }
    }

    /**
     * Gerer l'insertion, la mise a jour des tests lors de l'import de donnees
     *
     * @param id_famille
     *            identifiant de la famille auquelle appartiennent les tests
     * @param suite
     *            suite auquel appartiennent les tests
     * @param id_suite
     *            identifiant de la suite auquelle les tests appartiennent
     * @param suiteName
     *            , nom de la suite qui contient les test
     * @param familyName
     *            nom de la famille qui contien les tests
     * @param testList
     *            liste des tests dans le document
     */
    /*
     * public void updateTests(Element suiteElem, Family fam, TestList suite,
     * boolean newFamily, boolean newSuite) throws Exception { //ArrayList
     * testAuto, ArrayList<Node> testList =
     * (ArrayList<Node>)suiteElem.selectNodes(".//Test"); if (testList == null){
     * return; }
     *
     * Iterator<Node> it = testList.iterator(); while (it.hasNext() && !annule){
     * try { Element testElem = (Element)it.next(); String testName =
     * testElem.elementText("Nom"); boolean newTest = false; boolean
     * selectionneTest = false; if (isSelectionDesTests()){ Iterator<Test>
     * itTest = testSelectionList.iterator(); while (itTest.hasNext()){ Test
     * test = (Test)itTest.next(); if
     * (testName.equals(test.getNameFromModel())){ selectionneTest = true; } } }
     * if (!isSelectionDesTests() || selectionneTest){ //Verification de
     * l'existence ou non du test boolean appartient = false; Test test = null;
     * if (suite != null){ ArrayList<Test> testListBDD =
     * suite.getTestListFromModel(); Iterator<Test> itTest =
     * testListBDD.iterator(); while (itTest.hasNext() && !appartient){ test =
     * itTest.next(); if (test.getNameFromModel().equals(testName)){ appartient
     * = true; } } } //recuperation des donnees dans le document String
     * loginConceptor=""; //User pUser; if (!appartient){ loginConceptor =
     * getLogin(testElem); if (loginConceptor == null) { if (!newProject){
     * loginConceptor = DataModel.getCurrentUser().getLoginFromModel(); } else {
     * loginConceptor = project.getAdministratorFromModel().getLoginFromModel();
     * } } }else{ loginConceptor = test.getConceptorLoginFromModel(); } String
     * testDescription = (testElem.elementText("Description") ==
     * null)?"":testElem.elementText("Description"); if
     * (testElem.element("Description")!= null &&
     * testElem.element("Description").attribute("isHTML")!= null &&
     * testElem.element("Description").attributeValue("isHTML").equals("true"))
     * { testDescription = giveHTMLDescription(testElem); } else {
     * testDescription = testDescription.replaceAll("\\\\n", "<br>");
     * testDescription =
     * "<html><head></head><body>"+testDescription+"</body></html>"; }
     *
     * String testType; String plug_ext; if (testElem.element("TestAuto") ==
     * null){ testType = "MANUAL"; plug_ext = ""; }else{ testType = "AUTOMATED";
     * String plug = testElem.element("TestAuto").attributeValue("plug_ext"); if
     * (plug!=null){ plug_ext = plug; }else{ plug_ext = ""; } } if
     * (!appartient){ newTest = true; if (testType.equals("AUTOMATED")){ test =
     * new AutomaticTest(testName, testDescription, plug_ext); } else { test =
     * new ManualTest(testName, testDescription); }
     * test.setConceptorLoginInModel(loginConceptor);
     * suite.addTestInDBAndModel(test);
     *
     * updateElementAttachement(testElem, test, newTest);
     *
     * updateParametresTest(testElem, test);
     *//***** Plugin update Test Data *****/
    /*
     * int size = listXMLPlugin.size(); for (int i = 0; i < size; i++){
     * XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin)
     * listXMLPlugin.elementAt(i); if (idialog != null) {
     * pXMLPrinterPlugin.updateTestFromXML(testElem, test,
     * idialog.isSupprElement(), this); } else {
     * pXMLPrinterPlugin.updateTestFromXML(testElem, test, false, this); } }
     *//***** (END) Plugin update Test Data *****/
    /*
     *
     * if (testType == "AUTOMATED"){ updateTestScript(testElem, (AutomaticTest)
     * test); int [] tab = {fam.getIdBdd(), suite.getIdBdd(), test.getIdBdd()};
     * testAuto.add(tab); automatedTest2Update.add(test); }else{
     * ajouterActions(testElem, (ManualTest) test); }
     *
     *
     * }else{ Le test est deja present boolean noupadte = true; Verification de
     * l'existence ou non d'un conflit (i.e: si le test est deja execute)
     * boolean isConflit = false; boolean isExisteModif = false;
     * ArrayList<Campaign> campaignList = project.getCampaignOfTest(test); if
     * (!campaignList.isEmpty()){ Iterator<Campaign> itCamp =
     * campaignList.iterator(); while (itCamp.hasNext() && !isConflit ){
     * Campaign camp = itCamp.next(); if
     * (!camp.getExecutionListFromModel().isEmpty()){ isConflit = true; } } } if
     * (isConflit){ isExisteModif = detecterModif(fam, suite, testElem); } if
     * (isConflit && isExisteModif && idialog.isCreer_copy()){ //creation d'un
     * nouveau test avec nom precede de "copy_" lorsqu'il y a un conflit //Test
     * testExiste = TestPlanData.getTest(familyName, suiteName,
     * "copy_"+testName); String newName = "copy_"+testName ; Test testExiste =
     * project.getTestFromModel(fam.getNameFromModel(),
     * suite.getNameFromModel(), newName); if (testExiste != null){ int num = 1;
     * while (testExiste!= null && num < 101 ){ newName = "copy_"+ num
     * +testName; testExiste = project.getTestFromModel(fam.getNameFromModel(),
     * suite.getNameFromModel(), "copy_"+newName); num++; } if (testExiste !=
     * null){ suite.deleteTestInDBAndModel(testExiste); } }
     *
     * newTest = true; if (testType.equals("AUTOMATED")){ test = new
     * AutomaticTest(newName, testDescription, plug_ext); } else { test = new
     * ManualTest(newName, testDescription); }
     * test.setConceptorLoginInModel(DataModel
     * .getCurrentUser().getLoginFromModel()); suite.addTestInDBAndModel(test);
     *
     * ajouterAttachements(testElem, test); updateParametresTest(testElem,
     * test);
     *//***** Plugin update Test Data *****/
    /*
     * int size = listXMLPlugin.size(); for (int i = 0; i < size; i++){
     * XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin)
     * listXMLPlugin.elementAt(i); pXMLPrinterPlugin.updateTestFromXML(testElem,
     * test, idialog.isSupprElement(), this); }
     *//***** (END) Plugin update Test Data *****/
    /*
     *
     * if (testType == "AUTOMATED"){ updateTestScript(testElem, (AutomaticTest)
     * test ); Util.log("[ImportDialog] Add test auto " + test); //int [] tab =
     * {fam.getIdBdd(), suite.getIdBdd(), test.getIdBdd()}; //testAuto.add(tab);
     * automatedTest2Update.add(test); }else{ ajouterActions(testElem,
     * (ManualTest)test); } noupadte = false; } if (!isConflit){ //on effectue
     * toutes les mises a jour necessaires test.updateInDBAndModel(testName,
     * testDescription);
     *
     * updateElementAttachement(testElem, test, newTest);
     *
     * updateParametresTest(testElem, test);
     *//***** Plugin update Test Data *****/
    /*
     * int size = listXMLPlugin.size(); for (int i = 0; i < size; i++){
     * XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin)
     * listXMLPlugin.elementAt(i); pXMLPrinterPlugin.updateTestFromXML(testElem,
     * test, idialog.isSupprElement(), this); }
     *//***** (END) Plugin update Test Data *****/
    /*
     *
     * if (testType == "AUTOMATED"){ updateTestScript(testElem,
     * (AutomaticTest)test); //int [] tab = {fam.getIdBdd(), suite.getIdBdd(),
     * test.getIdBdd()}; //testAuto.add(tab); automatedTest2Update.add(test);
     * }else{ if (idialog.isSupprAction()){ supprimerActions(testElem,
     * (ManualTest)test); verifUtilisationParams((ManualTest)test); }
     * updateAction(testElem, (ManualTest)test, false); } noupadte = false; } if
     * (isConflit && isExisteModif && idialog.isMajPossible()){ //dans ce cas,
     * il y a conflit et on doit faire la mise a jour des elements qui sont
     * possibles (Attachements tests et actions, description test, script tests
     * automatiques) test.updateInModel(testName, testDescription);
     * updateElementAttachement(testElem, test, newTest);
     *//***** Plugin update Test Data *****/
    /*
     * int size = listXMLPlugin.size(); for (int i = 0; i < size; i++){
     * XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin)
     * listXMLPlugin.elementAt(i); pXMLPrinterPlugin.updateTestFromXML(testElem,
     * test, idialog.isSupprElement(), this); }
     *//***** (END) Plugin update Test Data *****/
    /*
     * if (testType == "AUTOMATED"){ updateTestScript(testElem,
     * (AutomaticTest)test); Util.log("[ImportDialog] Add test auto " +
     * test.getIdBdd()); //int [] tab = {fam.getIdBdd(), suite.getIdBdd(),
     * test.getIdBdd()}; //testAuto.add(tab); automatedTest2Update.add(test);
     * }else{ if (idialog.isSupprAction()){ supprimerActions(testElem,
     * (ManualTest)test); verifUtilisationParams((ManualTest)test); }
     * updateAction(testElem, (ManualTest)test, true); } noupadte = false; } if
     * (noupadte) { Si aucune modification sur l'element on appele les plugins{
     * /***** Plugin update Test Data **** int size = listXMLPlugin.size(); for
     * (int i = 0; i < size; i++){ XMLPrinterPlugin pXMLPrinterPlugin =
     * (XMLPrinterPlugin) listXMLPlugin.elementAt(i);
     * pXMLPrinterPlugin.updateTestFromXML(testElem, test,
     * idialog.isSupprElement(), this); }
     *//***** (END) Plugin update Test Data *****/
    /*
     * } } } } catch (Exception e){ Tools.ihmExceptionView(e); } } }
     */

    /**
     * Gerer l'insertion, la mise a jour des tests lors de l'import de donnees
     *
     * @param id_famille
     *            identifiant de la famille auquelle appartiennent les tests
     * @param suite
     *            suite auquel appartiennent les tests
     * @param id_suite
     *            identifiant de la suite auquelle les tests appartiennent
     * @param suiteName
     *            , nom de la suite qui contient les test
     * @param familyName
     *            nom de la famille qui contien les tests
     * @param testList
     *            liste des tests dans le document
     */
    public void updateTests(Element suiteElem, Family fam, TestList suite,
                            boolean newFamily, boolean newSuite) throws Exception {
        // ArrayList testAuto,
        ArrayList<Node> testList = (ArrayList<Node>) suiteElem
            .selectNodes(".//Test");
        if (testList == null) {
            return;
        }

        Iterator<Node> it = testList.iterator();
        while (it.hasNext() && !annule) {
            try {
                Element testElem = (Element) it.next();
                String testName = testElem.elementText("Nom");
                boolean newTest = false;
                boolean selectionneTest = false;
                if (isSelectionDesTests()) {
                    Iterator<Test> itTest = testSelectionList.iterator();
                    while (itTest.hasNext()) {
                        Test test = (Test) itTest.next();
                        if (testName.equals(test.getNameFromModel())) {
                            selectionneTest = true;
                        }
                    }
                }
                if (!isSelectionDesTests() || selectionneTest) {
                    // Verification de l'existence ou non du test
                    boolean appartient = false;
                    Test test = null;
                    if (suite != null) {
                        ArrayList<Test> testListBDD = suite
                            .getTestListFromModel();
                        Iterator<Test> itTest = testListBDD.iterator();
                        while (itTest.hasNext() && !appartient) {
                            test = itTest.next();
                            if (test.getNameFromModel().equals(testName)) {
                                appartient = true;
                            }
                        }
                    }
                    // recuperation des donnees dans le document
                    String loginConceptor = "";
                    // User pUser;
                    if (!appartient) {
                        loginConceptor = getLogin(testElem);
                        if (loginConceptor == null) {
                            if (!newProject) {
                                loginConceptor = DataModel.getCurrentUser()
                                    .getLoginFromModel();
                            } else {
                                loginConceptor = project
                                    .getAdministratorFromModel()
                                    .getLoginFromModel();
                            }
                        }
                    } else {
                        loginConceptor = test.getConceptorLoginFromModel();
                    }
                    String testDescription = (testElem
                                              .elementText("Description") == null) ? ""
                        : testElem.elementText("Description");
                    if (testElem.element("Description") != null
                        && testElem.element("Description").attribute(
                                                                     "isHTML") != null
                        && testElem.element("Description").attributeValue(
                                                                          "isHTML").equals("true")) {
                        testDescription = giveHTMLDescription(testElem);
                    } else {
                        testDescription = testDescription.replaceAll("\\\\n",
                                                                     "<br>");
                        testDescription = "<html><head></head><body>"
                            + testDescription + "</body></html>";
                    }

                    String testType;
                    String plug_ext;
                    if (testElem.element("TestAuto") == null) {
                        testType = "MANUAL";
                        plug_ext = "";
                    } else {
                        testType = "AUTOMATED";
                        String plug = testElem.element("TestAuto")
                            .attributeValue("plug_ext");
                        if (plug != null) {
                            plug_ext = plug;
                        } else {
                            plug_ext = "";
                        }
                    }
                    if (!appartient) {
                        newTest = true;
                        if (testType.equals("AUTOMATED")) {
                            test = new AutomaticTest(testName, testDescription,
                                                     plug_ext);
                        } else {
                            test = new ManualTest(testName, testDescription);
                        }
                        test.setConceptorLoginInModel(loginConceptor);
                        suite.addTestInDBAndModel(test);

                        updateElementAttachement(testElem, test, newTest);

                        updateParametresTest(testElem, test);

                        /***** Plugin update Test Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, idialog.isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, false, this);
                            }
                        }
                        /***** (END) Plugin update Test Data *****/

                        if (testType == "AUTOMATED") {
                            updateTestScript(testElem, (AutomaticTest) test);
                            /*
                             * int [] tab = {fam.getIdBdd(), suite.getIdBdd(),
                             * test.getIdBdd()}; testAuto.add(tab);
                             */
                            automatedTest2Update.add(test);
                        } else {
                            ajouterActions(testElem, (ManualTest) test);
                        }

                    } else { /* Le test est deja present */
                        boolean noupadte = true;
                        boolean isExisteModif = false;
                        isExisteModif = detecterModif(fam, suite, testElem);
                        if (isExisteModif && idialog.isCreer_copy()) {
                            // creation d'un nouveau test avec nom precede de
                            // "copy_" lorsqu'il y a un conflit
                            // Test testExiste =
                            // TestPlanData.getTest(familyName, suiteName,
                            // "copy_"+testName);
                            String newName = "copy_" + testName;
                            Test testExiste = project.getTestFromModel(fam
                                                                       .getNameFromModel(), suite
                                                                       .getNameFromModel(), newName);
                            if (testExiste != null) {
                                int num = 1;
                                while (testExiste != null && num < 101) {
                                    newName = "copy_" + num + testName;
                                    testExiste = project.getTestFromModel(fam
                                                                          .getNameFromModel(), suite
                                                                          .getNameFromModel(), "copy_"
                                                                          + newName);
                                    num++;
                                }
                                if (testExiste != null) {
                                    suite.deleteTestInDBAndModel(testExiste);
                                }
                            }

                            newTest = true;
                            if (testType.equals("AUTOMATED")) {
                                test = new AutomaticTest(newName,
                                                         testDescription, plug_ext);
                            } else {
                                test = new ManualTest(newName, testDescription);
                            }
                            testName = newName;
                            test.setConceptorLoginInModel(DataModel
                                                          .getCurrentUser().getLoginFromModel());
                            suite.addTestInDBAndModel(test);

                            ajouterAttachements(testElem, test);
                            updateParametresTest(testElem, test);

                            /***** Plugin update Test Data *****/
                            int size = listXMLPlugin.size();
                            for (int i = 0; i < size; i++) {
                                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                    .elementAt(i);
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, idialog.isSupprElement(), this);
                            }
                            /***** (END) Plugin update Test Data *****/

                            if (testType == "AUTOMATED") {
                                updateTestScript(testElem, (AutomaticTest) test);
                                Util
                                    .log("[ImportDialog] Add test auto "
                                         + test);
                                // int [] tab = {fam.getIdBdd(),
                                // suite.getIdBdd(), test.getIdBdd()};
                                // testAuto.add(tab);
                                automatedTest2Update.add(test);
                            } else {
                                ajouterActions(testElem, (ManualTest) test);
                            }
                            noupadte = false;
                        }
                        // on effectue toutes les mises a jour necessaires
                        test.updateInDBAndModel(testName, testDescription);

                        updateElementAttachement(testElem, test, newTest);

                        updateParametresTest(testElem, test);

                        /***** Plugin update Test Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            pXMLPrinterPlugin.updateTestFromXML(testElem, test,
                                                                idialog.isSupprElement(), this);
                        }
                        /***** (END) Plugin update Test Data *****/

                        if (testType == "AUTOMATED") {
                            updateTestScript(testElem, (AutomaticTest) test);
                            // int [] tab = {fam.getIdBdd(), suite.getIdBdd(),
                            // test.getIdBdd()};
                            // testAuto.add(tab);
                            automatedTest2Update.add(test);
                        } else {
                            if (idialog.isSupprAction()) {
                                supprimerActions(testElem, (ManualTest) test);
                                verifUtilisationParams((ManualTest) test);
                            }
                            updateAction(testElem, (ManualTest) test, false);
                        }
                        noupadte = false;
                        if (isExisteModif && idialog.isMajPossible()) {
                            // dans ce cas, il y a conflit et on doit faire la
                            // mise a jour des elements qui sont possibles
                            // (Attachements tests et actions, description test,
                            // script tests automatiques)
                            test.updateInModel(testName, testDescription);
                            updateElementAttachement(testElem, test, newTest);
                            /***** Plugin update Test Data *****/
                            size = listXMLPlugin.size();
                            for (int i = 0; i < size; i++) {
                                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                    .elementAt(i);
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, idialog.isSupprElement(), this);
                            }
                            /***** (END) Plugin update Test Data *****/
                            if (testType == "AUTOMATED") {
                                updateTestScript(testElem, (AutomaticTest) test);
                                Util.log("[ImportDialog] Add test auto "
                                         + test.getIdBdd());
                                // int [] tab = {fam.getIdBdd(),
                                // suite.getIdBdd(), test.getIdBdd()};
                                // testAuto.add(tab);
                                automatedTest2Update.add(test);
                            } else {
                                if (idialog.isSupprAction()) {
                                    supprimerActions(testElem,
                                                     (ManualTest) test);
                                    verifUtilisationParams((ManualTest) test);
                                }
                                updateAction(testElem, (ManualTest) test, true);
                            }
                            noupadte = false;
                        }
                        if (noupadte) {/*
                                        * Si aucune modification sur l'element
                                        * on appele les plugins{ /***** Plugin
                                        * update Test Data ****
                                        */
                            size = listXMLPlugin.size();
                            for (int i = 0; i < size; i++) {
                                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                    .elementAt(i);
                                pXMLPrinterPlugin.updateTestFromXML(testElem,
                                                                    test, idialog.isSupprElement(), this);
                            }
                            /***** (END) Plugin update Test Data *****/
                        }
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    void updateSuite(Element familyElem, Family pFamily, boolean newFamily)
        throws Exception {
        ArrayList<Node> suiteList = (ArrayList<Node>) familyElem
            .selectNodes(".//SuiteTest");
        if (suiteList != null) {
            Iterator<Node> it2 = suiteList.iterator();
            while (it2.hasNext() && !annule) {
                Element suiteElem = (Element) it2.next();
                String suiteName = suiteElem.elementText("Nom");
                String suiteDescription = (suiteElem.elementText("Description") == null) ? ""
                    : suiteElem.elementText("Description");
                if (suiteElem.element("Description") != null
                    && suiteElem.element("Description").attribute("isHTML") != null
                    && suiteElem.element("Description").attributeValue(
                                                                       "isHTML").equals("true")) {
                    suiteDescription = giveHTMLDescription(suiteElem);
                } else {
                    suiteDescription = suiteDescription.replaceAll("\\\\n",
                                                                   "<br>");
                    suiteDescription = "<html><head></head><body>"
                        + suiteDescription + "</body></html>";
                }
                // boolean erreur = false;
                boolean selectionneSuite = false;
                if (isSelectionDesTests()) {
                    Iterator<TestList> itSuite = suiteSelectionList.iterator();
                    while (itSuite.hasNext()) {
                        TestList suite = itSuite.next();
                        if (suiteName.equals(suite.getNameFromModel())) {
                            selectionneSuite = true;
                        }
                    }
                }
                boolean newSuite = false;
                if (!isSelectionDesTests() || selectionneSuite) {
                    boolean appartient = false;
                    TestList suite = null;
                    if (pFamily != null && newFamily == false) {
                        ArrayList<TestList> suiteTestList = pFamily
                            .getSuiteListFromModel();
                        Iterator<TestList> itSuite = suiteTestList.iterator();
                        while (itSuite.hasNext() && !appartient) {
                            suite = itSuite.next();
                            if (suite.getNameFromModel().equals(suiteName)) {
                                appartient = true;
                            }
                        }
                    }

                    try {
                        if (!appartient) {
                            suite = new TestList(suiteName, suiteDescription);
                            pFamily.addTestListInDBAndModel(suite);
                            newSuite = true;
                        } else {
                            suite.updateInDBAndModel(suiteName,
                                                     suiteDescription);
                        }
                        // gerer les attachements
                        updateElementAttachement(suiteElem, suite, newSuite);

                        /***** Plugin update Suite Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateSuiteFromXML(suiteElem,
                                                                     suite, idialog.isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateSuiteFromXML(suiteElem,
                                                                     suite, false, this);
                            }
                        }
                        /***** (END) Plugin update Suite Data *****/

                        // les tests
                        updateTests(suiteElem, pFamily, suite, newFamily,
                                    newSuite);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    void updateFamily() throws Exception {
        // ArrayList testAuto = new ArrayList();
        ArrayList<Node> familyList = (ArrayList<Node>) doc
            .selectNodes("//Famille");
        ArrayList<Family> familleListModel = project.getFamilyListFromModel();
        if (familyList != null) {
            Iterator<Node> it = familyList.iterator();
            while (it.hasNext() && !annule) {
                Element familyElem = (Element) it.next();
                String familyName = familyElem.elementText("Nom");
                String familyDescription = (familyElem
                                            .elementText("Description") == null) ? "" : familyElem
                    .elementText("Description");
                if (familyElem.element("Description") != null
                    && familyElem.element("Description")
                    .attribute("isHTML") != null
                    && familyElem.element("Description").attributeValue(
                                                                        "isHTML").equals("true")) {
                    familyDescription = giveHTMLDescription(familyElem);
                } else {
                    familyDescription = familyDescription.replaceAll("\\\\n",
                                                                     "<br>");
                    familyDescription = "<html><head></head><body>"
                        + familyDescription + "</body></html>";
                }
                boolean selectionne = false;
                if (isSelectionDesTests()) {
                    Iterator<Family> itFamily = familySelectionList.iterator();
                    while (itFamily.hasNext()) {
                        Family family = itFamily.next();
                        if (familyName.equals(family.getNameFromModel())) {
                            selectionne = true;
                        }
                    }
                }
                if (!isSelectionDesTests() || selectionne) {
                    boolean appartient = false;
                    Family fam = null;
                    boolean newFamily = false;
                    Iterator<Family> itFam = familleListModel.iterator();
                    while (itFam.hasNext() && !appartient) {
                        fam = itFam.next();
                        if (fam.getNameFromModel().equals(familyName)) {
                            appartient = true;
                        }
                    }
                    try {
                        if (!appartient) {
                            fam = new Family(familyName, familyDescription);

                            project.addFamilyInDBAndModel(fam);

                            newFamily = true;
                        } else {
                            fam.updateDescriptionInModel(familyDescription);
                        }

                        // updatesuite
                        updateElementAttachement(familyElem, fam, newFamily);

                        /***** Plugin update FamilyData data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (idialog != null) {
                                pXMLPrinterPlugin.updateFamilyFromXML(
                                                                      familyElem, fam, idialog
                                                                      .isSupprElement(), this);
                            } else {
                                pXMLPrinterPlugin.updateFamilyFromXML(
                                                                      familyElem, fam, false, this);
                            }
                        }
                        /***** (END) Plugin update FamilyData data *****/

                        updateSuite(familyElem, fam, newFamily);

                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    private String giveHTMLDescription(Element elem) {
        String description = elem.element("Description").asXML();
        description = description.substring(27, description
                                            .indexOf("</Description>"));
        description = description.replaceAll("<br/>", "<br>");
        description = "<html><head></head><body>" + description
            + "</body></html>";
        return description;
    }

    /**
     * Dans le cas ou le test etait pas deja dans la BD, ajout ou mise a jour
     * des actions
     *
     * @param test
     *            le test qui contient les actions
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     * @param justeMAJAttachements
     *            si oui, autorise uniquement la mise a jour des attachements
     *            des actions
     */
    public void updateAction(Element testElem, ManualTest test,
                             boolean justeMAJAttachements) throws Exception {
        if (idialog.isSupprAction() && !idialog.isSupprElement()) {
            supprimerActions(testElem, (ManualTest) test);
            // verification que tous les parametres du test sont toujours
            // utilises

        }
        List<Node> actionList = testElem.selectNodes(".//ActionTest");
        Iterator<Node> itAction = actionList.iterator();
        while (itAction.hasNext()) {
            Element actionElem = (Element) itAction.next();
            String nomAction = actionElem.elementText("Nom");
            String description = (actionElem.elementText("Description") == null) ? ""
                : actionElem.elementText("Description");
            String resulAttendu = (actionElem.elementText("ResultAttendu") == null) ? ""
                : actionElem.elementText("ResultAttendu");
            description = description.replaceAll("\\\\n", "\n");
            resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");
            boolean appartient = false;
            Action action = null;
            if (test != null) {
                ArrayList<Action> actionListBDD = ((ManualTest) test)
                    .getActionListFromModel(false);
                Iterator<Action> itActionBD = actionListBDD.iterator();
                while (itActionBD.hasNext() && !appartient) {
                    action = itActionBD.next();
                    if (action.getNameFromModel().equals(nomAction)) {
                        appartient = true;
                    }
                }
            }
            if (!appartient && !justeMAJAttachements) {

                action = new Action(test, nomAction, description);
                action.setAwaitedResultInModel(resulAttendu);
                try {
                    test.addActionInDBAndModel(action);
                    ajouterAttachements(actionElem, action);
                    ajoutParametresActions(actionElem, action);
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            } else {
                if (!justeMAJAttachements) {
                    try {
                        action.updateInDBAndModel(nomAction, description,
                                                  resulAttendu);
                        ajoutParametresActions(actionElem, action);
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                updateElementAttachement(actionElem, action, false);
            }
            /***** Plugin update Action Data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                pXMLPrinterPlugin.updateActionFromXML(actionElem, action,
                                                      idialog.isSupprElement(), this);
            }
            /***** (END) Plugin update Action Data *****/
        }
    }

    /**
     * Ajout des tests si ils ne sont pas deja dans la campagne
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param campaign
     *            campagne du modele
     */
    public void updateTestsToCamp(Element campElem, Campaign campaign)
        throws Exception {
        List<Node> familyListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef");
        Iterator<Node> itFam = familyListXML.iterator();
        while (itFam.hasNext()) {
            Element famElem = (Element) itFam.next();
            String nomFamille = famElem.elementText("Nom");
            Family family = project.getFamilyFromModel(nomFamille);
            List<Node> suiteListXML = famElem
                .selectNodes("SuiteTestsCamp/SuiteTestRef");
            Iterator<Node> itSuite = suiteListXML.iterator();
            while (itSuite.hasNext()) {
                Element suiteElem = (Element) itSuite.next();
                String nomSuite = suiteElem.elementText("Nom");
                TestList testList = family.getTestListInModel(nomSuite);
                List<Node> testListXML = suiteElem
                    .selectNodes("TestsCamp/TestRef");
                Iterator<Node> itTest = testListXML.iterator();
                while (itTest.hasNext()) {
                    try {
                        Element testElem = (Element) itTest.next();
                        String nomTest = testElem.elementText("Nom");
                        int userID = -1;
                        String loginAssigned = testElem
                            .attributeValue("loginAssigned");
                        if (loginAssigned != null && !loginAssigned.equals("")) {
                            userID = project.containUser(loginAssigned);
                        }
                        if (userID == -1) {
                            if (idialog != null) {
                                // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                                // id_exec,
                                // DataModel.getCurrentUser().getLogin(),
                                // description, statut, resultat);
                                userID = DataModel.getCurrentUser().getIdBdd();
                            } else {
                                // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                                // id_exec,
                                // cdialog.getNewProject().getAdministrator().getLogin(),
                                // description, statut, resultat);
                                userID = project.getAdministratorFromModel()
                                    .getIdBdd();
                            }
                        }
                        Test test = testList.getTestFromModel(nomTest);
                        if (!campaign.getTestListFromModel().contains(test)) {
                            campaign.importTestInDBAndModel(test, userID);
                        }
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    /**
     * Ajout ou mise a jour des jeux de donnees d'une campagne donnee
     *
     * @param campElem
     * @param campaign
     * @return liste des jeux de donnees qui ont ete modifies
     */
    public ArrayList<String> updateJeuxDonnees(Element campElem,
                                               Campaign campaign) throws Exception {
        ArrayList<String> listJeuModif = new ArrayList<String>();
        List<Node> jeuListXML = campElem.selectNodes("JeuxDonnees/JeuDonnees");
        Iterator<Node> itJeu = jeuListXML.iterator();
        while (itJeu.hasNext()) {
            try {
                Element jeuElem = (Element) itJeu.next();
                String nomJeu = jeuElem.elementText("Nom");
                String description = jeuElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                // verification de l'appartenance ou non du jeu de donnees a la
                // campagne
                boolean appartient = false;
                ArrayList<DataSet> dataSetList = campaign
                    .getDataSetListFromModel();
                Iterator<DataSet> itSet = dataSetList.iterator();
                DataSet dataSet = null;
                while (itSet.hasNext() && !appartient) {
                    dataSet = itSet.next();
                    if (dataSet.getNameFromModel().equals(nomJeu)) {
                        appartient = true;
                    }
                }
                boolean different = false;
                if (appartient) {
                    // verifier que toutes les valeurs des parametres sont les
                    // memes sinon creer un "jeu'Bis'", les deux jeux de donnees
                    // ayant le meme nom
                    List<Node> paramtestList = jeuElem
                        .selectNodes("ValeurParams/ValeurParam");
                    Iterator<Node> itParamtestJeu = paramtestList.iterator();
                    while (itParamtestJeu.hasNext()) {
                        Element paramElem = (Element) itParamtestJeu.next();
                        String valParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                            .attributeValue("valeur")
                            : "";
                        String nomParam = paramElem.elementText("Nom");
                        HashMap<String, String> paramMap = dataSet
                            .getParametersHashMapFromModel();
                        if (!paramMap.containsKey(nomParam)) {
                            different = true;
                        } else {
                            String valInJeu = (paramMap.get(nomParam) != null) ? paramMap
                                .get(nomParam)
                                : "";
                            if (!valInJeu.equals(valParam)) {
                                different = true;
                            }
                        }
                    }
                    if (different) {
                        listJeuModif.add(dataSet.getNameFromModel());
                    }
                }

                if (!appartient || different) {
                    // int id_jeu = 0;

                    if (!appartient) {
                        dataSet = new DataSet(nomJeu, description);
                        campaign.addDataSetInDBAndModel(dataSet);
                    } else {
                        dataSet = new DataSet(nomJeu + "_Bis", description);
                        campaign.addDataSetInDBAndModel(dataSet);
                    }
                    // ajout des parametres values du jeu de donnees
                    List<Node> paramList = jeuElem
                        .selectNodes("ValeurParams/ValeurParam");
                    Iterator<Node> itParamJeu = paramList.iterator();
                    while (itParamJeu.hasNext()) {
                        Element paramElem = (Element) itParamJeu.next();
                        String valParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                            .attributeValue("valeur")
                            : "";
                        String nomParam = paramElem.elementText("Nom");
                        Parameter param = DataModel.getCurrentProject()
                            .getParameterFromModel(nomParam);
                        dataSet.addParamValueToDBAndModel(valParam, param);
                    }
                }
                /***** Plugin update DataSet *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    pXMLPrinterPlugin.updateDataSetFromXML(jeuElem, dataSet,
                                                           idialog.isSupprElement(), this);
                }
                /***** (END) Plugin update DataSet *****/
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
        return listJeuModif;
    }

    /**
     * Methode qui ajoute ou met a jour les executions d'une campagne
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param campaign
     *            campagne du modele
     * @param listJeuModif
     *            liste des jeux qui ont ete modifies
     */
    public void updateExecutions(Element campElem, Campaign campaign,
                                 ArrayList<String> listJeuModif) throws Exception {
        List<Node> listExecXML = campElem
            .selectNodes("ExecCampTests/ExecCampTest");
        Iterator<Node> itExec = listExecXML.iterator();
        while (itExec.hasNext()) {
            Element execElem = (Element) itExec.next();
            String nomExec = execElem.elementText("Nom");
            String description = (execElem.elementText("Description") != null) ? execElem
                .elementText("Description")
                : "";
            description = description.replaceAll("\\\\n", "\n");
            // Verification de l'appartenance ou non de l'execution a la
            // campagne
            ArrayList<Execution> execList = campaign
                .getExecutionListFromModel();
            boolean appartient = false;
            Execution exec = null;
            Iterator<Execution> itEx = execList.iterator();
            while (itEx.hasNext() && !appartient) {
                exec = itEx.next();
                if (exec.getNameFromModel().equals(nomExec)) {
                    appartient = true;
                }
            }
            boolean different = false;
            if (appartient) {
                // verification que c'est vraiment la meme execution avec meme
                // jeu de donnees, meme environnement, meme scripts meme jeu de
                // donnees
                Element jeuElem = execElem.element("JeuDonneesEx");
                if (jeuElem != null) {
                    String nomJeu = jeuElem.elementText("Nom");
                    if (exec.getDataSetFromModel() == null) {
                        different = true;
                    } else if (!exec.getDataSetFromModel().getNameFromModel()
                               .equals(nomJeu)) {
                        different = true;
                    } else if (listJeuModif.contains(nomJeu)) {
                        different = true;
                    }
                } else if (exec.getDataSetFromModel() != null
                           && exec.getDataSetFromModel().getIdBdd() != -1) {
                    different = true;
                }
                // meme environnement
                Element envElem = execElem.element("EnvironnementEx");
                String nomEnvXML = envElem.elementText("Nom");
                if (!nomEnvXML.equals(exec.getEnvironmentFromModel()
                                      .getNameFromModel())) {
                    different = true;
                }
                // meme scripts
                Element scriptPre = (Element) execElem
                    .selectSingleNode("Script[1]");
                if (scriptPre != null) {
                    String type = scriptPre.attributeValue("type");
                    String nomScript = scriptPre.attributeValue("nom");
                    if (type.equals("PRE_SCRIPT")) {
                        if (exec.getPreScriptFromModel() == null) {
                            different = true;
                        } else {
                            if (!exec.getPreScriptFromModel()
                                .getNameFromModel().equals(nomScript)) {
                                different = true;
                            }
                        }
                    } else {
                        if (exec.getPostScriptFromModel() == null) {
                            different = true;
                        } else {
                            if (!exec.getPostScriptFromModel()
                                .getNameFromModel().equals(nomScript)) {
                                different = true;
                            }
                        }
                    }
                }
                Element scriptPost = (Element) execElem
                    .selectSingleNode("Script[2]");
                if (scriptPost != null) {
                    String nomScript = scriptPost.attributeValue("nom");
                    if (!exec.getPostScriptFromModel().getNameFromModel()
                        .equals(nomScript)) {
                        different = true;
                    }
                }
            }

            // Determination de l'identifiant du jeu de donnees associe a
            // l'execution
            DataSet pDataSet;
            Element jeuElem = execElem.element("JeuDonneesEx");
            if (jeuElem != null) {
                String nomJeu = jeuElem.elementText("Nom");
                pDataSet = campaign.getDataSetFromModel(nomJeu);
            } else {
                pDataSet = new DataSet(ApiConstants.EMPTY_NAME, "");
            }
            Element envElem = execElem.element("EnvironnementEx");
            String nomEnvXML = envElem.elementText("Nom");
            Environment pEnv = project.getEnvironmentFromModel(nomEnvXML);

            if (!appartient || different) {
                Execution pExec;
                try {
                    if (!appartient) {
                        pExec = new Execution(nomExec, description);
                    } else {
                        pExec = new Execution(nomExec + "_Bis", description);
                    }
                    pExec.updateEnvInModel(pEnv);
                    pExec.updateDatasetInModel(pDataSet);
                    campaign.addExecutionInDBAndModel(pExec, DataModel
                                                      .getCurrentUser());

                    // Ajout des scripts
                    Element scriptPre = (Element) execElem
                        .selectSingleNode("Script[1]");
                    if (scriptPre != null) {
                        ajouterScriptToExec(scriptPre, pExec);
                    }
                    Element scriptPost = (Element) execElem
                        .selectSingleNode("Script[2]");
                    if (scriptPost != null) {
                        ajouterScriptToExec(scriptPost, pExec);
                    }
                    // ajouter les attachements
                    ajouterAttachements(execElem, pExec);

                    /***** Plugin update Execution Data *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        pXMLPrinterPlugin.updateExecutionFromXML(execElem,
                                                                 pExec, idialog.isSupprElement(), this);
                    }
                    /***** (END) Plugin update Execution Data *****/
                    // ajouter les resultats d'executions
                    ajouterResulExec(execElem, pExec);// ,
                    // campElem.elementText("Nom")
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            } else {
                // mise a jour ou ajout des scripts
                Element scriptPre = (Element) execElem
                    .selectSingleNode("Script[1]");
                if (scriptPre != null) {
                    updateScriptToExec(scriptPre, exec);
                }
                Element scriptPost = (Element) execElem
                    .selectSingleNode("Script[2]");
                if (scriptPost != null) {
                    updateScriptToExec(scriptPost, exec);
                }
                // ajout ou mise a jour des attachements de l'execution
                updateElementAttachement(execElem, exec, false);

                /***** Plugin update Execution Data *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    pXMLPrinterPlugin.updateExecutionFromXML(execElem, exec,
                                                             idialog.isSupprElement(), this);
                }
                /***** (END) Plugin update Execution Data *****/

                // ajout ou mise a jour des resultats d'executions
                updateResulExec(execElem, exec, campaign); // campElem.elementText("Nom")
            }
        }
    }

    /**
     * Methode qui ajoute ou met a jour les resultats d'execution d'une campagne
     *
     * @param execElem
     *            element representant l'execution d'une campagne dans le
     *            document XML
     * @param exec
     *            excecution d'une campagne dans le modele
     * @param campName
     *            nom de la campagne qui contient l'execution
     */
    public void updateResulExec(Element execElem, Execution exec, Campaign camp)
        throws Exception {
        List<Node> resulExecListXML = execElem
            .selectNodes("ResulExecCampTests/ResulExecCampTest");
        Iterator<Node> itRExec = resulExecListXML.iterator();
        while (itRExec.hasNext()) {
            try {
                Element rexecElem = (Element) itRExec.next();
                String nom = rexecElem.elementText("Nom");
                String description = (rexecElem.elementText("Description") != null) ? rexecElem
                    .elementText("Description")
                    : "";
                String resultat = rexecElem.attributeValue("statut");
                description = description.replaceAll("\\\\n", "\n");
                Element resExecElem = rexecElem.element("ResulExecs");
                /*
                 * String statut = "A_FAIRE"; if (resExecElem != null){ statut =
                 * "FAIT"; }
                 */
                // verification de la presence ou non du resultat d'execution
                // dans le modele
                ArrayList<ExecutionResult> execResList = exec
                    .getExecutionResultListFromModel();
                boolean appartient = false;
                ExecutionResult execRes = null;
                Iterator<ExecutionResult> itResExec = execResList.iterator();
                while (itResExec.hasNext() && !appartient) {
                    execRes = itResExec.next();
                    if (execRes.getNameFromModel().equals(nom)) {
                        appartient = true;
                    }
                }
                boolean different = false;
                if (appartient) {
                    // difference au niveau du statue de l'execution
                    if (!resultat.equals(execRes.getExecutionStatusFromModel())) {
                        different = true;
                    }
                    // detection des differences dans les resultats d'execution
                    // de chaque test
                    if (resExecElem != null && !different) {
                        List<Node> execCasList = resExecElem
                            .selectNodes("ResulExec");
                        Iterator<Node> itExecCas = execCasList.iterator();
                        while (itExecCas.hasNext() && !different) {
                            Element execCasElem = (Element) itExecCas.next();
                            String res = execCasElem.attributeValue("res");
                            Element refTest = execCasElem.element("RefTest");
                            String nomTest = refTest.elementText("NomTest");
                            String nomSuite = refTest.elementText("NomSuite");
                            String nomFamille = refTest
                                .elementText("NomFamille");
                            Test test = project.getTestFromModel(nomFamille,
                                                                 nomSuite, nomTest);
                            if (execRes.getTestResultStatusFromModel(test) == null) {
                                different = true;
                            } else if (!execRes.getTestResultStatusFromModel(
                                                                             test).equals(res)) {
                                different = true;
                            }
                        }
                    }
                    // detection des differences au niveau des resultats des
                    // actions des tests
                    List<Node> resActionList = rexecElem
                        .selectNodes("ResulActionTests/ResulActionTest");
                    Iterator<Node> itResAction = resActionList.iterator();
                    while (itResAction.hasNext() && !different) {
                        Element resAction = (Element) itResAction.next();
                        Element refAction = resAction.element("RefAction");
                        String nomAction = refAction.elementText("NomAction");
                        String nomTest = refAction.elementText("NomTest");
                        String nomSuite = refAction.elementText("NomSuite");
                        String nomFamille = refAction.elementText("NomFamille");
                        Test test = project.getTestFromModel(nomFamille,
                                                             nomSuite, nomTest);
                        Action action = ((ManualTest) test)
                            .getActionFromModel(nomAction);
                        String resultatAction = resAction.attributeValue("res");
                        ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) execRes
                            .getExecutionTestResultFromModel(test);
                        if (resultatAction.equals("NonRenseigne")) {
                            resultatAction = "";
                        }
                        if (pManualExecutionResult
                            .getActionStatusInModel(action) == null) {
                            different = true;
                        } else if (!pManualExecutionResult
                                   .getActionStatusInModel(action).equals(
                                                                          resultatAction)) {
                            different = true;
                        }
                    }
                }
                if (!appartient || different) {
                    // int id_exec_res = 0;
                    ExecutionResult pExecutionResult;
                    if (!appartient) {
                        pExecutionResult = new ExecutionResult(nom,
                                                               description, exec);
                    } else {
                        pExecutionResult = new ExecutionResult(nom + "_Bis",
                                                               description, exec);
                    }

                    pExecutionResult.setExecutionStatusInModel(resultat);
                    exec.addExecutionResultInDBAndModel(pExecutionResult,
                                                        DataModel.getCurrentUser());
                    ajouterAttachements(rexecElem, pExecutionResult);

                    if (resExecElem != null) {
                        List<Node> execCasList = resExecElem
                            .selectNodes("ResulExec");
                        Iterator<Node> itExecCas = execCasList.iterator();
                        while (itExecCas.hasNext()) {
                            Element execCasElem = (Element) itExecCas.next();
                            String res = execCasElem.attributeValue("res");
                            Element refTest = execCasElem.element("RefTest");
                            String nomTest = refTest.elementText("NomTest");
                            String nomSuite = refTest.elementText("NomSuite");
                            String nomFamille = refTest
                                .elementText("NomFamille");
                            Test test = project.getTestFromModel(nomFamille,
                                                                 nomSuite, nomTest);

                            ExecutionTestResult pExecutionTestResult = pExecutionResult
                                .addTestResultStatusInModel(test, res);
                            pExecutionResult.addExecTestResultInDB(test);
                            ajouterAttachements(execCasElem,
                                                pExecutionTestResult);
                        }
                    }
                    List<Node> resActionList = rexecElem
                        .selectNodes("ResulActionTests/ResulActionTest");
                    Iterator<Node> itResAction = resActionList.iterator();
                    while (itResAction.hasNext()) {
                        Element resAction = (Element) itResAction.next();
                        Element refAction = resAction.element("RefAction");
                        String nomAction = refAction.elementText("NomAction");
                        String nomTest = refAction.elementText("NomTest");
                        String nomSuite = refAction.elementText("NomSuite");
                        String nomFamille = refAction.elementText("NomFamille");
                        Test test = project.getTestFromModel(nomFamille,
                                                             nomSuite, nomTest);
                        Action action = ((ManualTest) test)
                            .getActionFromModel(nomAction);
                        ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) pExecutionResult
                            .getExecutionTestResultFromModel(test);
                        if (!pManualExecutionResult.isInBase()) {
                            pExecutionResult
                                .addExecTestResultInDB((ManualTest) test);
                        }
                        String resultatAction = resAction.attributeValue("res");
                        if (resultatAction.equals("NonRenseigne")) {
                            resultatAction = "";
                        }
                        String descAction = (resAction
                                             .elementText("Description") == null) ? ""
                            : resAction.elementText("Description");
                        String resulAttendu = (resAction
                                               .elementText("ResultAttendu") == null) ? ""
                            : resAction.elementText("ResultAttendu");
                        String effectiveAttendu = (resAction
                                                   .elementText("ResulEffectif") == null) ? ""
                            : resAction.elementText("ResulEffectif");
                        descAction = descAction.replaceAll("\\\\n", "\n");
                        resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");
                        effectiveAttendu = effectiveAttendu.replaceAll("\\\\n",
                                                                       "\n");

                        pManualExecutionResult.addStatusForActionInModel(
                                                                         action, resultatAction);
                        pManualExecutionResult.addEffectivResultInModel(action,
                                                                        effectiveAttendu);
                        pManualExecutionResult.addDescriptionResultInModel(
                                                                           action, descAction);
                        pManualExecutionResult.addAwaitedResultInModel(action,
                                                                       resulAttendu);
                        // pManualExecutionResult.addActionResultInDB(action);
                        pExecutionResult.addExecActionResultInDB(test, action);
                    }

                    /***** Plugin update ExecutionResult Data *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                                    pExecutionResult, idialog.isSupprElement(),
                                                                    this);
                    }
                    /***** (END) Plugin update ExecutionResult Data *****/

                } else {
                    // int id_exec_res = execRes.getIdBDD();
                    updateElementAttachement(rexecElem, execRes, false);
                    if (resExecElem != null) {
                        List<Node> execCasList = resExecElem
                            .selectNodes("ResulExec");
                        Iterator<Node> itExecCas = execCasList.iterator();
                        while (itExecCas.hasNext()) {
                            Element execCasElem = (Element) itExecCas.next();
                            Element refTest = execCasElem.element("RefTest");
                            String nomTest = refTest.elementText("NomTest");
                            String nomSuite = refTest.elementText("NomSuite");
                            String nomFamille = refTest
                                .elementText("NomFamille");
                            Test test = project.getTestFromModel(nomFamille,
                                                                 nomSuite, nomTest);
                            ExecutionTestResult pExecutionTestResult = execRes
                                .getExecutionTestResultFromModel(test);
                            updateElementAttachement(execCasElem,
                                                     pExecutionTestResult, false);
                        }
                    }
                    List<Node> resActionList = rexecElem
                        .selectNodes("ResulActionTests/ResulActionTest");
                    Iterator<Node> itResAction = resActionList.iterator();
                    while (itResAction.hasNext()) {
                        Element resAction = (Element) itResAction.next();
                        Element refAction = resAction.element("RefAction");
                        String nomAction = refAction.elementText("NomAction");
                        String nomTest = refAction.elementText("NomTest");
                        String nomSuite = refAction.elementText("NomSuite");
                        String nomFamille = refAction.elementText("NomFamille");
                        Test test = project.getTestFromModel(nomFamille,
                                                             nomSuite, nomTest);
                        Action action = ((ManualTest) test)
                            .getActionFromModel(nomAction);
                        ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) execRes
                            .getExecutionTestResultFromModel(test);

                        String resultatAction = resAction.attributeValue("res");
                        if (resultatAction.equals("NonRenseigne")) {
                            resultatAction = "";
                        }
                        String effectiveResult = (resAction
                                                  .elementText("ResulEffectif") == null) ? ""
                            : resAction.elementText("ResulEffectif");

                        pManualExecutionResult.addEffectivResultInModel(action,
                                                                        effectiveResult);
                        pManualExecutionResult.addStatusForActionInModel(
                                                                         action, resultatAction);
                        pManualExecutionResult
                            .updateActionEffectiveResInDB(action);
                    }

                    /***** Plugin update ExecutionResult Data *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                                    execRes, idialog.isSupprElement(), this);
                    }
                    /***** (END) Plugin update ExecutionResult Data *****/

                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    /**
     * Methode qui ajoute ou met a jour un script d'une execution
     *
     * @param script
     *            element script du document XML
     * @param id_exec
     *            identifiant de l'execution
     */
    public void updateScriptToExec(Element script, Execution exec)
        throws Exception {
        String classpath = script.elementText("Classpath");
        String argScript = script.elementText("ArgScript");
        String type = script.attributeValue("type");
        String dirScript = script.attributeValue("dir");
        dirScript = restorePath(dirScript);
        File fScript = new File(dirXml + fs + dirScript);

        Script pScritp = new Script(fScript.getName(), "");
        pScritp.setTypeInModel(type);
        pScritp.setScriptExtensionInModel(classpath);
        pScritp.updatePlugArgInModel(argScript);

        if ((type.equals("PRE_SCRIPT") && exec.getPreScriptFromModel() == null)
            || (type.equals("POST_SCRIPT") && exec.getPostScriptFromModel() == null)) {
            try {
                exec.addScriptInDBAndModel(pScritp, fScript);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        } else {
            if (type.equals("PRE_SCRIPT")) {
                try {
                    Script scriptInit = exec.getPreScriptFromModel();
                    if (scriptInit != null) {
                        scriptInit.updateInDB(fScript);
                        scriptInit.updatePlugArgInDBAndModel(argScript);
                    }
                } catch (Exception fe) {
                    fe.printStackTrace();
                    if (!attachmentExecption) {
                        idialog
                            .setErrorMessage(idialog.getErrorMessage()
                                             + Language
                                             .getInstance()
                                             .getText(
                                                      "Probleme_lors_de_la_mise_a_jour_des_attachements"));
                        idialog.showErrorMessage();
                        attachmentExecption = true;
                    }
                }
            } else {
                Script scriptPost = exec.getPostScriptFromModel();
                try {
                    if (scriptPost != null) {
                        scriptPost.updateInDB(fScript);
                        scriptPost.updatePlugArgInDBAndModel(argScript);
                    }
                } catch (Exception fe) {
                    fe.printStackTrace();
                    if (!attachmentExecption) {
                        idialog
                            .setErrorMessage(idialog.getErrorMessage()
                                             + Language
                                             .getInstance()
                                             .getText(
                                                      "Probleme_lors_de_la_mise_a_jour_des_attachements"));
                        idialog.showErrorMessage();
                        attachmentExecption = true;
                    }
                }
            }
        }
    }

    /***********************************************************************************************************************/

    /**
     * Gestion de l'insertion ou la mise a jour des campagnes du projet Salome
     * courant en fonction du document XML
     */
    public void gestionDesCampagnes() throws Exception {
        ArrayList<Campaign> campList = project.getCampaignListFromModel();
        List<Node> campListXML = doc.selectNodes("//CampagneTest");
        Iterator<Node> itCampXML = campListXML.iterator();
        while (itCampXML.hasNext() && !annule) {
            try {
                Element campElem = (Element) itCampXML.next();
                boolean appartient = false;
                Campaign campaign = null;
                Iterator<Campaign> itCamp = campList.iterator();
                while (itCamp.hasNext() && !appartient) {
                    campaign = (Campaign) itCamp.next();
                    if (campElem.elementText("Nom").equals(
                                                           campaign.getNameFromModel())) {
                        appartient = true;
                    }
                    Util.log("[gestionDesCampagnes] compare "
                             + campaign.getNameFromModel() + " avec "
                             + campElem.elementText("Nom") + " result = "
                             + appartient);

                }

                String campName = campElem.elementText("Nom");
                String campDescription = (campElem.elementText("Description") == null) ? ""
                    : campElem.elementText("Description");
                if (campElem.element("Description") != null
                    && campElem.element("Description").attribute("isHTML") != null
                    && campElem.element("Description").attributeValue(
                                                                      "isHTML").equals("true")) {
                    campDescription = giveHTMLDescription(campElem);
                } else {
                    campDescription = campDescription.replaceAll("\\\\n",
                                                                 "<br>");
                    campDescription = "<html><head></head><body>"
                        + campDescription + "</body></html>";
                }
                if (!appartient) {

                    Campaign camp = new Campaign(campName, campDescription);
                    project.addCampaignInDBandModel(camp);

                    // ajout des attachements
                    ajouterAttachements(campElem, camp);
                    /***** Plugin update Campaign Data *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        if (idialog != null) {
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    camp, idialog.isSupprElement(), this);
                        } else {
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    camp, false, this);
                        }
                    }
                    /***** (END) Plugin update Campaing Data *****/
                    // ajout des jeux de donnees
                    ajouterJeuxDonnees(campElem, camp);
                    // ajout des tests associees a la campagne
                    ajoutTestsToCamp(campElem, camp);
                    // ajout des executions
                    ajouterExecutions(campElem, camp);
                } else {
                    // detection conflit
                    // i.e. campagne avec des executions qui contient de
                    // nouveaux tests
                    boolean conflit = detecterConflitCamp(campaign, campElem);
                    if (conflit && idialog.isCreer_copy()) {
                        int numCopy = 0;
                        String newName = "copy_" + campName;
                        Campaign campExiste = project
                            .getCampaignFromModel("copy_" + campName);
                        while (campExiste != null && numCopy < 100) {
                            newName = "copy" + numCopy + "_" + campName;
                            campExiste = project.getCampaignFromModel("copy_"
                                                                      + newName);
                            numCopy++;
                        }

                        if (campExiste != null) {
                            project.deleteCampaignInDBAndModel(campExiste);
                        }

                        Campaign newCamp = new Campaign(newName,
                                                        campDescription);
                        project.addCampaignInDBandModel(newCamp);

                        // ajout des attachements
                        ajouterAttachements(campElem, newCamp);
                        /***** Plugin update Campaign Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    newCamp, idialog.isSupprElement(), this);
                        }
                        /***** (END) Plugin update Campaing Data *****/
                        // ajout des jeux de donnees
                        ajouterJeuxDonnees(campElem, newCamp);
                        // ajout des tests associees a la campagne
                        ajoutTestsToCamp(campElem, newCamp);
                        // ajout des executions
                        ajouterExecutions(campElem, newCamp);
                    }
                    if (!conflit) {
                        // on effectue toutes les mises a jour necessaires de la
                        // BD en fonction du fichier XML
                        campaign.updateInDBAndModel(
                                                    campElem.elementText("Nom"), campDescription);
                        // mise a jour des attachements
                        updateElementAttachement(campElem, campaign, false);
                        /***** Plugin update Campaign Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    campaign, idialog.isSupprElement(), this);
                        }
                        /***** (END) Plugin update Campaing Data *****/
                        // ajout et mise a jour des jeux de donnees
                        ArrayList<String> listJeuModif = updateJeuxDonnees(
                                                                           campElem, campaign);
                        // ajout ou mise a jour des tests associees a la
                        // campagne
                        updateTestsToCamp(campElem, campaign);
                        // ajout ou mise a jour des executions
                        updateExecutions(campElem, campaign, listJeuModif);
                    }
                    if (conflit && idialog.isMajPossible()) {
                        // dans ce cas, il y a conflit et on doit faire la mise
                        // a jour des elements qui sont possibles
                        updateElementAttachement(campElem, campaign, false);
                        /***** Plugin update Campaign Data *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            pXMLPrinterPlugin.updateCampaignFromXML(campElem,
                                                                    campaign, idialog.isSupprElement(), this);
                        }
                        /***** (END) Plugin update Campaing Data *****/
                    }

                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    void updatePlugin() throws Exception {
        // gestion des drivers des scripts des tests automatiques
        Iterator<Test> itTestAuto = automatedTest2Update.iterator();
        while (itTestAuto.hasNext()) {
            AutomaticTest test = (AutomaticTest) itTestAuto.next();
            try {
                if (newProject) {

                } else {

                }
                TestDriver driver = ((AutomaticTest) test).ActivateExtention(
                                                                             (Extension) pIPlugObject.getAssociatedExtension(test
                                                                                                                             .getExtensionFromModel()), pIPlugObject
                                                                             .getUrlBase(), pIPlugObject.getPluginManager());

                // TestDriver driver =
                // ((AutomaticTest)test).ActivateExtention((Extension)SalomeTMF.associatedExtension.get(test.getExtensionFromModel()),
                // SalomeTMF.urlSalome, SalomeTMF.jpf);
                if (test.getScriptFromModel() != null && driver != null) {
                    File file = null;

                    file = ((AutomaticTest) test).getTestScriptFromDB();

                    if (file != null) {
                        driver.updateTestScriptFromImport(file
                                                          .getAbsolutePath(), (AutomaticTest) test);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                idialog
                    .setErrorMessage(idialog.getErrorMessage()
                                     + Language
                                     .getInstance()
                                     .getText(
                                              "Probleme_lors_de_la_mise_a_jour_des_drivers_des_scripts"));
                idialog.showErrorMessage();
            }
        }
    }

    /********************************* Ajout ************************************************/

    /**
     * Dans le cas ou le test n'etait pas deja dans la BD, ajout des actions
     *
     * @param testElem
     *            element qui represente le test dans le document XML
     * @param id_test
     *            identifiant du test qui a ete ajoute a la BD
     */
    public void ajouterActions(Element testElem, ManualTest test)
        throws Exception {
        List<Node> actionList = testElem.selectNodes(".//ActionTest");
        Iterator<Node> itAction = actionList.iterator();
        // int actionOrder = 0;
        while (itAction.hasNext()) {
            Element actionElem = (Element) itAction.next();
            String nomAction = actionElem.elementText("Nom");
            String description = (actionElem.elementText("Description") == null) ? ""
                : actionElem.elementText("Description");
            String resulAttendu = (actionElem.elementText("ResultAttendu") == null) ? ""
                : actionElem.elementText("ResultAttendu");
            description = description.replaceAll("\\\\n", "\n");
            resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");

            Action newAction = new Action(test, nomAction, description);
            newAction.setAwaitedResultInModel(resulAttendu);
            test.addActionInDBAndModel(newAction);

            ajouterAttachements(actionElem, newAction);
            ajoutParametresActions(actionElem, newAction);
            /***** Plugin update Action Data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateActionFromXML(actionElem,
                                                          newAction, idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateActionFromXML(actionElem,
                                                          newAction, false, this);
                }
            }
            /***** (END) Plugin update Action Data *****/
        }
    }

    /**
     * Dans le cas ou attachableElement n'etait pas deja dans la BD, ajout des
     * attachements
     *
     * @param elem
     *            : element qui represente le WithAttachment dans le document
     *            XML
     * @param attachableElement
     */
    public void ajouterAttachements(Element elem,
                                    WithAttachment attachableElement) throws Exception {
        List<Node> urlAttachementList = elem
            .selectNodes("Attachements/UrlAttachement");
        Iterator<Node> itUrlAttach = urlAttachementList.iterator();
        while (itUrlAttach.hasNext()) {
            Element urlElem = (Element) itUrlAttach.next();
            String url = urlElem.attributeValue("url");
            String description = (urlElem.elementText("Description") == null) ? ""
                : urlElem.elementText("Description");
            description = description.replaceAll("\\\\n", "\n");
            UrlAttachment pUrlAttachment = new UrlAttachment(url, description);
            try {
                // To check if the url is a real url
                new URL(url);
                attachableElement.addAttachementInDBAndModel(pUrlAttachment);
            } catch (Exception e1) {
                Tools.ihmExceptionView(e1);
            }
        }
        List<Node> testFileAttachementList = elem
            .selectNodes("Attachements/FileAttachement");
        Iterator<Node> itFileAttach = testFileAttachementList.iterator();
        while (itFileAttach.hasNext()) {
            Element fileElem = (Element) itFileAttach.next();
            String dirAtt = fileElem.attributeValue("dir");
            dirAtt = restorePath(dirAtt);
            File f = new File(dirXml + fs + dirAtt);
            String description = (fileElem.elementText("Description") == null) ? ""
                : fileElem.elementText("Description");
            description = description.replaceAll("\\\\n", "\n");
            FileAttachment pFileAttachment = new FileAttachment(f, description);
            try {
                attachableElement.addAttachementInDBAndModel(pFileAttachment);
            } catch (Exception e2) {
                Tools.ihmExceptionView(e2);
            }
        }
    }

    /**
     * Si l'action existait dans la BD, ajout ou mise a jour des parametres
     *
     * @param pAction
     *            identifiant de l'action
     * @param actionElem
     *            element action du document comportant les donnees
     */
    public void ajoutParametresActions(Element actionElem, Action pAction)
        throws Exception {
        List<Node> paramActionList = actionElem
            .selectNodes("ParamsT/ParamT/Nom");
        Iterator<Node> itParamAction = paramActionList.iterator();
        while (itParamAction.hasNext()) {
            String nomParamImport = ((Element) itParamAction.next()).getText();
            boolean appartient = false;
            if (pAction.getParameterFromModel(nomParamImport) != null) {
                appartient = true;
            }
            if (!appartient) {
                Parameter param;
                Test pTest = pAction.getTest();
                param = pTest.getUsedParameterFromModel(nomParamImport);
                if (param != null) {
                    pAction.setUseParamInDBAndModel(param);
                } else {
                    param = project.getParameterFromModel(nomParamImport);
                    if (param != null) {
                        pTest.setUseParamInDBAndModel(param);
                        pAction.setUseParamInDBAndModel(param);
                    } else {
                        // AIE
                    }
                }
            } else {
                // Le parametre est deja utilise (rien a faire)
            }
        }
    }

    /**
     * Si la campagne n'existait pas deja dans la BD, ajout des jeux de donnees
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param id_camp
     *            identifiant de la campagne ajoutee dans la base de donnees
     */
    public void ajouterJeuxDonnees(Element campElem, Campaign pCamp)
        throws Exception {
        List<Node> jeuListXML = campElem.selectNodes("JeuxDonnees/JeuDonnees");
        Iterator<Node> itJeu = jeuListXML.iterator();
        while (itJeu.hasNext()) {
            Element jeuElem = (Element) itJeu.next();
            String nomJeu = jeuElem.elementText("Nom");
            String description = jeuElem.elementText("Description");
            if (description == null) {
                description = "";
            } else {
                description = description.replaceAll("\\\\n", "\n");
            }
            DataSet pDataSet = new DataSet(nomJeu, description);
            pCamp.addDataSetInDBAndModel(pDataSet);

            // ajout du jeu de donnees au modele
            // ajout des parametres values du jeu de donnees
            List<Node> paramList = jeuElem
                .selectNodes("ValeurParams/ValeurParam");
            Iterator<Node> itParamJeu = paramList.iterator();
            while (itParamJeu.hasNext()) {
                Element paramElem = (Element) itParamJeu.next();
                String valParam = (paramElem.attributeValue("valeur") != null) ? paramElem
                    .attributeValue("valeur")
                    : "";
                String nomParam = paramElem.elementText("Nom");

                Parameter param = project.getParameterFromModel(nomParam);
                pDataSet.addParamValueToDBAndModel(valParam, param);
            }
            /***** Plugin update DataSet *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateDataSetFromXML(jeuElem, pDataSet,
                                                           idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateDataSetFromXML(jeuElem, pDataSet,
                                                           false, this);
                }
            }
            /***** (END) Plugin update DataSet *****/
        }
    }

    public void ajoutTestsToCamp(Element campElem, Campaign pCamp)
        throws Exception {
        List<Node> familyListXML = campElem
            .selectNodes("FamillesCamp/FamilleRef");
        Iterator<Node> itFam = familyListXML.iterator();
        while (itFam.hasNext()) {
            Element famElem = (Element) itFam.next();
            String nomFamille = famElem.elementText("Nom");
            Family family = project.getFamilyFromModel(nomFamille);
            List<Node> suiteListXML = famElem
                .selectNodes("SuiteTestsCamp/SuiteTestRef");
            Iterator<Node> itSuite = suiteListXML.iterator();
            while (itSuite.hasNext()) {
                Element suiteElem = (Element) itSuite.next();
                String nomSuite = suiteElem.elementText("Nom");
                TestList testList = family.getTestListInModel(nomSuite);
                List<Node> testListXML = suiteElem
                    .selectNodes("TestsCamp/TestRef");
                Iterator<Node> itTest = testListXML.iterator();
                while (itTest.hasNext()) {
                    Element testElem = (Element) itTest.next();
                    String nomTest = testElem.elementText("Nom");
                    int userID = -1;
                    String loginAssigned = testElem
                        .attributeValue("loginAssigned");
                    if (loginAssigned != null && !loginAssigned.equals("")) {
                        userID = project.containUser(loginAssigned);
                    }
                    if (userID == -1) {
                        if (idialog != null) {
                            // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                            // id_exec, DataModel.getCurrentUser().getLogin(),
                            // description, statut, resultat);
                            userID = DataModel.getCurrentUser().getIdBdd();
                        } else {
                            // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                            // id_exec,
                            // cdialog.getNewProject().getAdministrator().getLogin(),
                            // description, statut, resultat);
                            userID = project.getAdministratorFromModel()
                                .getIdBdd();
                        }
                    }
                    Test pTest = testList.getTestFromModel(nomTest);
                    pCamp.importTestInDBAndModel(pTest, userID);
                }
            }
        }
    }

    /**
     * Methode qui ajoute les executions aux campagnes qui n'etait pas au
     * prealable dans la BD
     *
     * @param campElem
     *            element representant la campagne dans le document XML
     * @param id_camp
     *            identifiant de la campagne dans la BD
     */
    public void ajouterExecutions(Element campElem, Campaign pCamp)
        throws Exception {
        List<Node> listExecXML = campElem
            .selectNodes("ExecCampTests/ExecCampTest");
        Iterator<Node> itExec = listExecXML.iterator();
        while (itExec.hasNext()) {
            Element execElem = (Element) itExec.next();
            String nomExec = execElem.elementText("Nom");
            String description = (execElem.elementText("Description") != null) ? execElem
                .elementText("Description")
                : "";
            description = description.replaceAll("\\\\n", "\n");
            Execution pExecution = new Execution(nomExec, description);

            Element jeuElem = execElem.element("JeuDonneesEx");
            DataSet pDataSet = null;
            if (jeuElem != null) {
                String nomJeu = jeuElem.elementText("Nom");
                // id_jeu =
                // ConnectionData.getCampTestSelect().getDataSetId(nomJeu,
                // campName);
                pDataSet = pCamp.getDataSetFromModel(nomJeu);
            } else {
                pDataSet = new DataSet(ApiConstants.EMPTY_NAME, "");
            }
            pExecution.updateDatasetInModel(pDataSet);

            Element envElem = execElem.element("EnvironnementEx");
            String nomEnvXML = envElem.elementText("Nom");

            Environment pEnvironement = project
                .getEnvironmentFromModel(nomEnvXML);
            pExecution.updateEnvInModel(pEnvironement);

            User pUser = null;
            if (idialog != null) {
                pUser = DataModel.getCurrentUser();
            } else {
                pUser = project.getAdministratorFromModel();
            }
            try {
                pCamp.addExecutionInDBAndModel(pExecution, pUser);

                // Ajout des scripts
                Element scriptPre = (Element) execElem
                    .selectSingleNode("Script[1]");
                if (scriptPre != null) {
                    ajouterScriptToExec(scriptPre, pExecution);
                }
                Element scriptPost = (Element) execElem
                    .selectSingleNode("Script[2]");
                if (scriptPost != null) {
                    ajouterScriptToExec(scriptPost, pExecution);
                }

                // ajouter les attachements
                ajouterAttachements(execElem, pExecution);

                /***** Plugin update Execution Data *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    if (idialog != null) {
                        pXMLPrinterPlugin.updateExecutionFromXML(execElem,
                                                                 pExecution, idialog.isSupprElement(), this);
                    } else {
                        pXMLPrinterPlugin.updateExecutionFromXML(execElem,
                                                                 pExecution, false, this);
                    }
                }
                /***** (END) Plugin update Execution Data *****/

                // ajouter les resultats d'executions
                ajouterResulExec(execElem, pExecution);
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }

        }
    }

    /**
     * Methode qui ajoute un script a une execution
     *
     * @param script
     *            element script du document XML
     * @param id_exec
     *            identifiant de l'execution
     */
    public void ajouterScriptToExec(Element script, Execution pExecution)
        throws Exception {
        String classpath = script.elementText("Classpath");
        String argScript = script.elementText("ArgScript");
        String type = script.attributeValue("type");
        String dirScript = script.attributeValue("dir");
        dirScript = restorePath(dirScript);
        File fScript = new File(dirXml + fs + dirScript);

        Script pScritp = new Script(fScript.getName(), "");
        pScritp.setTypeInModel(type);
        pScritp.setScriptExtensionInModel(classpath);
        pScritp.updatePlugArgInModel(argScript);
        try {
            pExecution.addScriptInDBAndModel(pScritp, fScript);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    /**
     * Methode qui rajoute les resultats d'execution a une campagne
     *
     * @param execElem
     *            element representant l'execution d'une campagne dans le
     *            document XML
     * @param id_exec
     *            identifiant de l'execution dans la BD
     * @param campName
     *            nom de la campagne qui contient l'execution
     */
    public void ajouterResulExec(Element execElem, Execution pExecution)
        throws Exception {
        List<Node> resulExecListXML = execElem
            .selectNodes("ResulExecCampTests/ResulExecCampTest");
        Iterator<Node> itRExec = resulExecListXML.iterator();
        while (itRExec.hasNext()) {
            Element rexecElem = (Element) itRExec.next();
            String nom = rexecElem.elementText("Nom");
            String description = (rexecElem.elementText("Description") != null) ? rexecElem
                .elementText("Description")
                : "";
            description = description.replaceAll("\\\\n", "\n");
            String resultat = rexecElem.attributeValue("statut");
            Element resExecElem = rexecElem.element("ResulExecs");
            /*
             * String statut = "A_FAIRE"; if (resExecElem != null){ statut =
             * "FAIT"; }
             */
            User pUser = null;
            if (idialog != null) {
                // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                // id_exec, DataModel.getCurrentUser().getLogin(), description,
                // statut, resultat);
                pUser = DataModel.getCurrentUser();
            } else {
                // ConnectionData.getCampTestInsert().addResExecCampUsingID(nom,
                // id_exec,
                // cdialog.getNewProject().getAdministrator().getLogin(),
                // description, statut, resultat);
                pUser = project.getAdministratorFromModel();
            }
            ExecutionResult pExecutionResult = new ExecutionResult(nom,
                                                                   description, pExecution);
            pExecutionResult.setExecutionStatusInModel(resultat);
            // pExecutionResult.addInDB(pExecution, pUser);
            pExecution.addExecutionResultInDBAndModel(pExecutionResult, pUser);

            ajouterAttachements(rexecElem, pExecutionResult);
            if (resExecElem != null) {
                List<Node> execCasList = resExecElem.selectNodes("ResulExec");
                Iterator<Node> itExecCas = execCasList.iterator();
                while (itExecCas.hasNext()) {
                    Element execCasElem = (Element) itExecCas.next();
                    String res = execCasElem.attributeValue("res");
                    Element refTest = execCasElem.element("RefTest");
                    String nomTest = refTest.elementText("NomTest");
                    String nomSuite = refTest.elementText("NomSuite");
                    String nomFamille = refTest.elementText("NomFamille");

                    Test pTest = project.getTestFromModel(nomFamille, nomSuite,
                                                          nomTest);
                    ExecutionTestResult pExecutionTestResult = pExecutionResult
                        .initTestResultStatusInModel(pTest, res, 0,
                                                     pExecution.getCampagneFromModel());
                    if (!pExecutionTestResult.isInBase()) {
                        pExecutionResult.addExecTestResultInDB(pTest);
                    }
                    ajouterAttachements(execCasElem, pExecutionTestResult);

                }
            }

            List<Node> resActionList = rexecElem
                .selectNodes("ResulActionTests/ResulActionTest");
            Iterator<Node> itResAction = resActionList.iterator();
            while (itResAction.hasNext()) {
                Element resAction = (Element) itResAction.next();
                Element refAction = resAction.element("RefAction");
                String nomAction = refAction.elementText("NomAction");
                String nomTest = refAction.elementText("NomTest");
                String nomSuite = refAction.elementText("NomSuite");
                String nomFamille = refAction.elementText("NomFamille");

                Test pTest = project.getTestFromModel(nomFamille, nomSuite,
                                                      nomTest);
                Action pAction = ((ManualTest) pTest)
                    .getActionFromModel(nomAction);
                ManualExecutionResult pManualExecutionResult = (ManualExecutionResult) pExecutionResult
                    .getExecutionTestResultFromModel(pTest);
                if (!pManualExecutionResult.isInBase()) {
                    pExecutionResult.addExecTestResultInDB((ManualTest) pTest);
                }
                String resultatAction = resAction.attributeValue("res");
                if (resultatAction.equals("NonRenseigne")) {
                    resultatAction = "";
                }
                String descAction = (resAction.elementText("Description") == null) ? ""
                    : resAction.elementText("Description");
                String resulAttendu = (resAction.elementText("ResultAttendu") == null) ? ""
                    : resAction.elementText("ResultAttendu");
                String effectiveAttendu = (resAction
                                           .elementText("ResulEffectif") == null) ? "" : resAction
                    .elementText("ResulEffectif");

                descAction = descAction.replaceAll("\\\\n", "\n");
                resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");
                effectiveAttendu = effectiveAttendu.replaceAll("\\\\n", "\n");

                // ConnectionData.getCampTestInsert().addResExecActionUsingID(id_exec_res,
                // id_test, id_action, resultatAction, descAction,
                // resulAttendu);

                pManualExecutionResult.addStatusForActionInModel(pAction,
                                                                 resultatAction);
                pManualExecutionResult.addEffectivResultInModel(pAction,
                                                                effectiveAttendu);
                pManualExecutionResult.addDescriptionResultInModel(pAction,
                                                                   descAction);
                pManualExecutionResult.addAwaitedResultInModel(pAction,
                                                               resulAttendu);
                pExecutionResult.addExecActionResultInDB(pTest, pAction);
                // pManualExecutionResult.addActionResultInDB(pAction);

            }
            /***** Plugin update ExecutionResult Data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                                pExecutionResult, idialog.isSupprElement(), this);
                } else {
                    pXMLPrinterPlugin.updateResExecutionFromXML(rexecElem,
                                                                pExecutionResult, false, this);
                }
            }
            /***** (END) Plugin update ExecutionResult Data *****/

        }
    }

    /****************************************************************************************/

    /**
     * Si le test appartient a une campagne dont une execution a deja ete lancee
     * on cherche a savoir si le test du projet possede des differences avec le
     * test present dans le fichier XML, ces differences generant un conflit
     *
     * @param familyName
     *            nom de la famille auquelle appartient le test
     * @param suiteName
     *            nom de la suite auquelle appartient le test
     * @param testElem
     *            element test du document XML
     * @return true si reel conflit, false sinon
     */
    public boolean detecterModif(Family pFamily, TestList suite,
                                 Element testElem) throws Exception {
        // on verifie que tous les parametres qui sont dans le document d'import
        // sont dans le projet
        boolean existeModif = false;
        String testName = testElem.elementText("Nom");
        // Test test = TestPlanData.getTest(familyName, suiteName, testName);
        Test test = project.getTestFromModel(pFamily.getNameFromModel(), suite
                                             .getNameFromModel(), testName);
        List<Node> paramsImport = testElem.selectNodes("ParamsT/ParamT/Nom");
        ArrayList<Parameter> paramsProjet = test.getParameterListFromModel();
        Iterator<Node> itImport = paramsImport.iterator();
        while (itImport.hasNext() && !existeModif) {
            String nomParamImport = ((Element) itImport.next()).getText();
            boolean appartient = false;
            Iterator<Parameter> itParamProjet = paramsProjet.iterator();
            while (itParamProjet.hasNext()) {
                Parameter param = itParamProjet.next();
                if (param.getNameFromModel().equals(nomParamImport)) {
                    appartient = true;
                }
            }
            if (!appartient) {
                existeModif = true;
            }
        }
        if (!existeModif) {
            /*
             * on verifie que tous les parametres qui sont dans le projet sont
             * bien dans le document d'import
             */
            Iterator<Parameter> itProj = paramsProjet.iterator();
            while (itProj.hasNext() && !existeModif) {
                Parameter param = itProj.next();
                boolean appartient = false;
                Iterator<Node> itDonnees = paramsImport.iterator();
                while (itDonnees.hasNext()) {
                    String nomParam = ((Element) itDonnees.next()).getText();
                    if (nomParam.equals(param.getNameFromModel())) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    existeModif = true;
                }
            }
        }
        /* Verification des actions si parametres ok */
        if (!existeModif && (test instanceof ManualTest)) {
            // detection de modifications dans les actions
            ManualTest testManuel = (ManualTest) test;
            List<Node> actionList = testElem.selectNodes(".//ActionTest");
            Iterator<Node> itAction = actionList.iterator();
            while (itAction.hasNext() && !existeModif) {
                Element actionElem = (Element) itAction.next();
                String nomAction = actionElem.elementText("Nom");
                String descriptionAction = (actionElem
                                            .elementText("Description") == null) ? "" : actionElem
                    .elementText("Description");
                String resulAttendu = (actionElem.elementText("ResultAttendu") == null) ? ""
                    : actionElem.elementText("ResultAttendu");
                descriptionAction = descriptionAction.replaceAll("\\\\n", "\n");
                resulAttendu = resulAttendu.replaceAll("\\\\n", "\n");

                Action pAction = testManuel.getActionFromModel(nomAction);
                if (pAction == null) {
                    existeModif = true;
                } else {
                    if (!pAction.getDescriptionFromModel().trim().equals(
                                                                         descriptionAction)
                        || !pAction.getAwaitedResultFromModel().trim()
                        .equals(resulAttendu)) {
                        existeModif = true;
                    }
                    if (!existeModif) {
                        existeModif = verifParamsAction(actionElem, pAction);
                    }
                }
            }
            if (!existeModif) {

                /*
                 * verifier que toutes les actions qui sont dans le projet sont
                 * dans le fichier XML
                 */
                ArrayList<Action> actionsProjet = testManuel
                    .getActionListFromModel(false);
                Iterator<Action> itProj = actionsProjet.iterator();
                while (itProj.hasNext() && !existeModif) {
                    Action action = itProj.next();
                    boolean appartient = false;
                    Iterator<Node> itDonnees = actionList.iterator();
                    while (itDonnees.hasNext()) {
                        Element actionElem = (Element) itDonnees.next();
                        String nomAction = actionElem.elementText("Nom");
                        if (action.getNameFromModel().equals(nomAction)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        existeModif = true;
                    }
                }
            }
        }
        return existeModif;
    }

    /**
     * Methode qui detecte si il existe un conflit pour la mise a jour des
     * campagnes
     *
     * @param campaign
     *            la campagne concernee
     * @param campElem
     *            l'element campagne du document XML
     * @return
     */
    public boolean detecterConflitCamp(Campaign campaign, Element campElem)
        throws Exception {
        boolean conflit = false;
        boolean contientResulExec = false;
        ArrayList<Execution> listExec = campaign.getExecutionListFromModel();
        Iterator<Execution> itListExec = listExec.iterator();
        while (itListExec.hasNext()) {
            Execution exec = itListExec.next();
            if (!exec.getExecutionResultListFromModel().isEmpty()) {
                contientResulExec = true;
            }
        }
        if (contientResulExec) {
            List<Node> testListXML = campElem
                .selectNodes("FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef");
            Iterator<Node> itTestXML = testListXML.iterator();
            ArrayList<Test> testList = campaign.getTestListFromModel();
            if (testListXML.size() != testList.size()) {
                conflit = true;
            } else {
                while (itTestXML.hasNext() && !conflit) {
                    Element testRef = (Element) itTestXML.next();
                    String nomTest = testRef.elementText("Nom");
                    String nomSuite = ((Element) testRef
                                       .selectSingleNode("ancestor::SuiteTestRef[1]"))
                        .elementText("Nom");
                    String nomFamille = ((Element) testRef
                                         .selectSingleNode("ancestor::FamilleRef[1]"))
                        .elementText("Nom");
                    boolean appartient = false;
                    // ArrayList testList = campaign.getTestListFromModel();
                    Iterator<Test> itTestList = testList.iterator();
                    while (itTestList.hasNext() && !appartient) {
                        Test test = itTestList.next();
                        String suiteName = test.getTestListFromModel()
                            .getNameFromModel();
                        String familyName = test.getTestListFromModel()
                            .getFamilyFromModel().getNameFromModel();
                        // if (test.getNameFromModel().equals(nomTest) &&
                        // suiteName.equals(suiteRootElem.elementText("Nom")) &&
                        // familyName.equals(famRootElem.elementText("Nom"))){
                        if (test.getNameFromModel().equals(nomTest)
                            && suiteName.equals(nomSuite)
                            && familyName.equals(nomFamille)) {
                            appartient = true;
                        }
                    }
                    if (!appartient) {
                        conflit = true;
                    }
                }
            }

        }
        return conflit;
    }

    /**
     * Methode qui evalue si il y a une difference entre les parametres d'une
     * action dans le document XML et ceux de cette meme action dans le projet
     * Salome
     *
     * @param actionElem
     *            element du document DOM4J contenant l'action
     * @param action
     *            action dans le modele de SalomeTMF
     * @return
     */
    public boolean verifParamsAction(Element actionElem, Action action)
        throws Exception {
        boolean existeModif = false;
        List<Node> paramsImport = actionElem.selectNodes("ParamsT/ParamT/Nom");
        Hashtable<String, Parameter> paramsProjet = action
            .getParameterHashSetFromModel();
        Iterator<Node> itImport = paramsImport.iterator();
        // on verifie que tous les parametres qui sont dans le document d'import
        // sont bien dans le projet
        while (itImport.hasNext() && !existeModif) {
            String nomParamImport = ((Element) itImport.next()).getText();
            if (action.getParameterFromModel(nomParamImport) == null) {
                existeModif = true;
            }
        }
        if (!existeModif) {
            // on verifie que tous les parametres qui sont dans le projet sont
            // bien dans le document d'import
            Enumeration<Parameter> itProj = paramsProjet.elements();
            while (itProj.hasMoreElements() && !existeModif) {
                Parameter param = itProj.nextElement();
                boolean appartient = false;
                Iterator<Node> itDonnees = paramsImport.iterator();
                while (itDonnees.hasNext()) {
                    String nomParam = ((Element) itDonnees.next()).getText();
                    if (nomParam.equals(param.getNameFromModel())) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    existeModif = true;
                }
            }
        }
        return existeModif;
    }

    /****************************************************************************************/

    void coreImport() {
        try {
            automatedTest2Update = new Vector<Test>();

            /***** Plugin update globale data *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (idialog != null) {
                    pXMLPrinterPlugin.updateProjectFromXML(doc, idialog
                                                           .isSupprElement(), project, this);
                } else {
                    pXMLPrinterPlugin.updateProjectFromXML(doc, false, project,
                                                           this);
                }
            }
            /***** (END) Plugin update globale data *****/

            updateProjectAttachement();

            // Parametres du projet
            updateProjectParameter();

            // Environnement du projet
            updateProjectEnvironment();

            // Abre de test
            updateFamily();

            if (!isImportOnlyTests() && !annule) {
                gestionDesCampagnes();
                // DataModel.reloadFromBase();
            }
            updatePlugin();

        } catch (Exception e) {
            Tools.ihmExceptionView(e);
            annule = true;
            return;
        }
    }

    public void importInProject() throws Exception {
        project = DataModel.getCurrentProject(); // Add for V2
        DataModel.reloadFromBase(false);

        try {
            if (idialog.isSupprElement() && !annule) {
                gestionDesSuppressionsProject();
                gestionDesSuppressionsTests();
                gestionDesSuppressionsCampagnes();

                /***** Plugin manage XML delete for Project *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    pXMLPrinterPlugin.manageDelete(doc, this);
                }
                /***** (END) Plugin manage XML delete for Project *****/

                if (isImportOnlyTests()) {
                    // suppressionsCampagnes();
                }
            }

        } catch (Exception e) {
            Tools.ihmExceptionView(e);
            annule = true;
            return;
        }
        coreImport();

        DataModel.reloadFromBase(true);

        /***** Plugin manage XML refresh *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            pXMLPrinterPlugin.refreshNewData();
        }
        /***** (END) Plugin manage XML refresh *****/
    }

    public void importInNewProject() throws Exception {
        // creer le nouveau projet avec son administrateur
        // System.out.println("debut import in new Project");
        newProject = true;
        ArrayList data = new ArrayList();
        project = cdialog.getNewProject();
        if (project != null && !annule) {
            if (Api.isConnected()) {
                // int transNumber = -1;
                try {
                    project.addInDB();
                    project.setUserInModel(project.getAdministratorFromModel());

                    data.add(Tools.createAppletImageIcon(PATH_TO_PROJECT_ICON,
                                                         ""));
                    data.add(project.getNameFromModel().trim());
                    data.add(project.getAdministratorFromModel()
                             .getLoginFromModel());
                    data.add(project.getCreationDateFromModel().toString());
                    data.add(project.getDescriptionFromModel());
                    JTable projectTable = (JTable) Administration
                        .getUIComponent(UICompCst.ADMIN_PROJECT_MANAGEMENT_TABLE);
                    ((MyTableModel) projectTable.getModel()).addRow(data);
                    project.setUserInModel(project.getAdministratorFromModel());
                    pAdminVTData.addProjectInModel(project);
                    pAdminVTData.addAdminToProjectInModel(project
                                                          .getAdministratorFromModel(), project);
                    projectTable.getColumnModel().getColumn(0).setMaxWidth(18);
                } catch (Exception exception) {
                    cdialog
                        .setErrorMessage(cdialog.getErrorMessage()
                                         + Language
                                         .getInstance()
                                         .getText(
                                                  "Probleme_lors_de_la_creation_du_nouveau_projet"));
                    cdialog.showErrorMessage();
                    setAnnule(true);
                }
            }

        }

        if (!annule) {
            coreImport();
        }
    }

    String getLogin(Element pElem) throws Exception {
        String login = pElem.element("Concepteur").elementText("Login");
        if (project.containUser(login) != -1) {
            return login;
        } else {
            return null;
        }
    }

    /****************************************************************************************/
    /**
     * @return Returns the doc.
     */
    public Document getDoc() {
        return doc;
    }

    /**
     * @param doc
     *            The doc to set.
     */
    public void setDoc(Document doc) {
        this.doc = doc;
    }

    /**
     * @return Returns the dirXml.
     */
    public String getDirXml() {
        return dirXml;
    }

    /**
     * @param dirXml
     *            The dirXml to set.
     */
    public void setDirXml(String dirXml) {
        this.dirXml = dirXml;
    }

    /**
     * @return Returns the selectionDesTests.
     */
    public boolean isSelectionDesTests() {
        return selectionDesTests;
    }

    /**
     * @param selectionDesTests
     *            The selectionDesTests to set.
     */
    public void setSelectionDesTests(boolean selectionDesTests) {
        this.selectionDesTests = selectionDesTests;
    }

    /**
     * @return Returns the importOnlyTests.
     */
    public boolean isImportOnlyTests() {
        return importOnlyTests;
    }

    /**
     * @param importOnlyTests
     *            The importOnlyTests to set.
     */
    public void setImportOnlyTests(boolean importOnlyTests) {
        this.importOnlyTests = importOnlyTests;
    }

    /**
     * @return Returns the annule.
     */
    public boolean isAnnule() {
        return annule;
    }

    /**
     * @param annule
     *            The annule to set.
     */
    public void setAnnule(boolean annule) {
        this.annule = annule;
    }

    String restorePath(String path) throws Exception {
        if (fs.equals("\\")) {
            // Unix2Windows
            return path.replace('/', '\\');
        } else {
            return path.replace('\\', '/');
        }
    }

    public Project getProject() {
        return project;
    }
}
