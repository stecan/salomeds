/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package  salomeTMF_plug.docXML;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.apache.log4j.Logger;

import org.java.plugin.ExtensionPoint;
import org.java.plugin.Plugin;
import org.java.plugin.PluginDescriptor;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.Admin;
import org.objectweb.salome_tmf.plugins.core.Common;

import salomeTMF_plug.docXML.common.CreateProjectDialog;
import salomeTMF_plug.docXML.common.ExportDialog;
import salomeTMF_plug.docXML.common.GenDocDialog;
import salomeTMF_plug.docXML.importxml.ImportDialog;
import salomeTMF_plug.docXML.importxml.ImportDialog2;
import salomeTMF_plug.docXML.languages.Language;

/**
 *
 * @version $Id$
 */
public class DocXMLPlugin extends Plugin implements Common, Admin, DataConstants, IHMConstants {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(DocXMLPlugin.class);
    
   // SalomeTMF pIhm;
    boolean isFreezed = false;
    JMenu testGendocSubMenu = null;
    JMenu campGendocSubMenu = null;
    JMenu dataGendocSubMenu = null;
    JMenu testEchangeDonneesSubMenu = null;
    JMenu campEchangeDonneesSubMenu = null;
    JMenu dataEchangeDonneesSubMenu = null;

    IPlugObject pIPlugObject;

    public static boolean importInV2 = true;

    /**
     * @param manager
     * @param descr
     */
    public DocXMLPlugin(PluginManager manager, PluginDescriptor descr) {
        super(manager, descr);
    }
    /**
     * @see org.java.plugin.Plugin()
     */
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * @see org.java.plugin.Plugin()
     */
    protected void doStop() throws Exception {
        // no-op
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void init(Object pIPlugObject) {
        //pIhm = (SalomeTMF)pIPlugObject;
    	this.pIPlugObject = (IPlugObject) pIPlugObject;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public boolean isActivableInTestToolsMenu() {
        return true;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public boolean isActivableInCampToolsMenu() {
        return true;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public boolean isActivableInDataToolsMenu() {
        return true;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public boolean usesOtherUIComponents() {
        return false;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInTestToolsMenu(JMenu testToolsMenu) {
        testGendocSubMenu = new JMenu(Language.getInstance().getText("Plugin_generation_de_documents"));
        testEchangeDonneesSubMenu = new JMenu(Language.getInstance().getText("Echange_de_donnees_au_format_XML"));
        activatePluginInToolsMenu(testToolsMenu, testGendocSubMenu, testEchangeDonneesSubMenu);
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInCampToolsMenu(JMenu campToolsMenu) {
        campGendocSubMenu = new JMenu(Language.getInstance().getText("Plugin_generation_de_documents"));
        campEchangeDonneesSubMenu = new JMenu(Language.getInstance().getText("Echange_de_donnees_au_format_XML"));
        activatePluginInToolsMenu(campToolsMenu, campGendocSubMenu, campEchangeDonneesSubMenu);
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInDataToolsMenu(JMenu dataToolsMenu) {
        dataGendocSubMenu = new JMenu(Language.getInstance().getText("Plugin_generation_de_documents"));
        dataEchangeDonneesSubMenu = new JMenu(Language.getInstance().getText("Echange_de_donnees_au_format_XML"));
        activatePluginInToolsMenu(dataToolsMenu, dataGendocSubMenu, dataEchangeDonneesSubMenu);
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInToolsMenu(JMenu toolsMenu, JMenu subMenu, JMenu subMenu2) {

        JMenuItem importer = new JMenuItem(Language.getInstance().getText("Importer_des_donnees_a_partir_d_un_fichier_XML"));
        importer.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	try {
            		if (importInV2) {
            			new ImportDialog2(pIPlugObject);
            		} else {
            			new ImportDialog(pIPlugObject);
            		}
            	} catch (Exception ex) {
            		ex.printStackTrace();
            		Tools.ihmExceptionView(ex);
            	}
            }
        });

        JMenuItem export = new JMenuItem(Language.getInstance().getText("Exporter_au_format_XML"));
        export.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	try {
            		new ExportDialog(pIPlugObject);
            	} catch (Exception ex) {
            		ex.printStackTrace();
            		Tools.ihmExceptionView(ex);
            	}
            }
        });

        JMenuItem createProject = new JMenuItem("Test: create project");
        createProject.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                //new CreateProjectDialog(pIhm);
            }
        });

        JMenuItem testIhm = new JMenuItem(Language.getInstance().getText("Dossier_de_tests"));
        testIhm.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	try {
                    logger.info("Aufruf von GenDocDialog!");
                    new GenDocDialog(TEST, pIPlugObject);
            	} catch (Exception ex) {
            		ex.printStackTrace();
            		Tools.ihmExceptionView(ex);
            	}
            }
        });

        JMenuItem testIhmCampaign = new JMenuItem(Language.getInstance().getText("Resultat_des_tests"));
        testIhmCampaign.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
            	try {
            		new GenDocDialog(CAMPAIGN, pIPlugObject);
            	} catch (Exception ex) {
            		ex.printStackTrace();
            		Tools.ihmExceptionView(ex);
            	}
            }
        });

        subMenu2.add(importer);
        subMenu2.add(export);
        //subMenu2.add(createProject);
        subMenu.add(testIhm);
        subMenu.add(testIhmCampaign);

        toolsMenu.addSeparator();
        toolsMenu.add(subMenu);
        toolsMenu.add(subMenu2);
    }

    /**
     * @see salome.plugins.core.Common
     */
    public Vector getUsedUIComponents() {
    	return null;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInStaticComponent(Integer uiCompCst) {
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void activatePluginInDynamicComponent(Integer uiCompCst) {}

    /**
     * @see salome.plugins.core.Common
     */
    public boolean isFreezable() {
        return true;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void freeze() {
        testGendocSubMenu.setEnabled(false);
        campGendocSubMenu.setEnabled(false);
        dataGendocSubMenu.setEnabled(false);
        isFreezed = true;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public void unFreeze() {
        testGendocSubMenu.setEnabled(true);
        campGendocSubMenu.setEnabled(true);
        dataGendocSubMenu.setEnabled(true);
        isFreezed = false;
    }

    /**
     * @see salome.plugins.core.Common
     */
    public boolean isFreezed() {
        return isFreezed;
    }

    public void allPluginActived(ExtensionPoint commonExtensions, ExtensionPoint testDriverExtensions, 
				 ExtensionPoint scriptEngineExtensions, ExtensionPoint bugTrackerExtensions) {

    }
    public void allPluginActived(ExtensionPoint commonExtensions, ExtensionPoint testDriverExtensions, 
				 ExtensionPoint scriptEngineExtensions, ExtensionPoint bugTrackerExtensions, 
				 ExtensionPoint statistiquesExtensions) {

    }
    //////////////////////////////// Admin interface ////////////////////////////////////

    /* (non-Javadoc)
	 * @see org.objectweb.salome_tmf.plugins.core.Admin#activateInSalomeAdmin(java.util.Map)
	 */
	public void activateInSalomeAdmin(final Map adminUIComps, IPlugObject iPlugObject) {
		try {
			this.pIPlugObject = iPlugObject;
			Object projectViewButtonsObj = adminUIComps.get(UICompCst.ADMIN_PROJECT_MANAGEMENT_BUTTONS_PANEL);
			if (projectViewButtonsObj != null) {
				JPanel projectViewButtonsPan = (JPanel)projectViewButtonsObj;
				JButton bouton = new JButton(Language.getInstance().getText("Creer_a_partir_d_un_fichier_XML"));
				bouton.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e) {
						try {
							new CreateProjectDialog(pIPlugObject);
						} catch (Exception ex2) {
							ex2.printStackTrace();
							Tools.ihmExceptionView(ex2);
						}
					}
				});
				projectViewButtonsPan.add(bouton);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Tools.ihmExceptionView(ex);
		}
		//!\\ Pour la frame parent de la fenetre secondaire -> Administarion.ptrFrame
	}

	public void onDeleteProject(Project p){

	}
}
