/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.ihm.models.TreeRenderer;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.docXML.languages.Language;

/**
 * Classe qui construit la fenetre permettant de choisir les chapitres a inserer
 * dans la documentation
 *
 * @author vapu8214
 */
public class ChapterChooser extends JDialog {

    private DefaultMutableTreeNode chosenRootNode;
    private DefaultMutableTreeNode chapterRootNode;
    private JTree chosenTree;
    private JTree chapterTree;
    private DefaultTreeModel chosenTreeModel;

    private ArrayList<String> chosenNodes = new ArrayList<String>();
    private ArrayList<String> allChaptersList;

    private String chapitres = Language.getInstance().getText("Chapitres");

    private GenDocDialog parentDialog;

    /**
     * Permet de selectionner les chapitres qui apparaitront dans le rapport
     *
     * @param dialog
     * @param initChapterSelection
     *            true si la liste des chapitres a deja ete initialisee
     * @param allTitlesList
     *            liste des extensions dont on peut choisir de les ajouter ou de
     *            les retirer du rapport
     */
    public ChapterChooser(GenDocDialog dialog, boolean initChapterSelection,
			  ArrayList<String> allTitlesList) {
	super(dialog, true);

	parentDialog = dialog;
	allChaptersList = allTitlesList;

	TreeRenderer chosenRenderer = new TreeRenderer();
	TreeRenderer chapterRenderer = new TreeRenderer();

	chosenRootNode = new DefaultMutableTreeNode(Language.getInstance()
						    .getText("Chapitres"));
	if (initChapterSelection) {
	    chosenNodes = parentDialog.getChosenChaptersList();
	    ArrayList<String> newChosenNodes = new ArrayList<String>();
	    for (String chapterTitle : newChosenNodes) {
		if (allChaptersList.contains(chapterTitle)) {
		    DefaultMutableTreeNode node = new DefaultMutableTreeNode(
									     chapterTitle);
		    chosenRootNode.add(node);
		    newChosenNodes.add(chapterTitle);
		}
	    }
	    chosenNodes = newChosenNodes;
	} else {
	    for (String chapterTitle : allChaptersList) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(
									 chapterTitle);
		chosenRootNode.add(node);
		chosenNodes.add(chapterTitle);
	    }
	}
	chapterRootNode = new DefaultMutableTreeNode(chapitres);
	for (String chapterTitle : allChaptersList) {
	    DefaultMutableTreeNode node = new DefaultMutableTreeNode(
								     chapterTitle);
	    chapterRootNode.add(node);
	}

	chosenTree = new JTree();
	chosenTree.setCellRenderer(chosenRenderer);
	chosenTreeModel = new DefaultTreeModel(chosenRootNode);
	chosenTree.setModel(chosenTreeModel);

	chapterTree = new JTree();
	chapterTree.setCellRenderer(chapterRenderer);
	chapterTree.setModel(new DefaultTreeModel(chapterRootNode));

	JButton addButton = new JButton(">");
	addButton.setToolTipText(Language.getInstance().getText(
								"Ajouter_a_la_selection"));
	addButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    TreePath[] pathTab = chapterTree.getSelectionPaths();
		    if (pathTab != null) {
			for (int i = 0; i < pathTab.length; i++) {
			    String nodeValue = (String) ((DefaultMutableTreeNode) pathTab[i]
							 .getLastPathComponent()).getUserObject();
			    if (!chosenNodes.contains(nodeValue)) {
				int index = getIndexFor(nodeValue);
				chosenNodes.add(index, nodeValue);
				DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(
											    nodeValue);
				chosenTreeModel.insertNodeInto(newNode,
							       chosenRootNode, index);
				chosenTree.scrollPathToVisible(new TreePath(newNode
									    .getPath()));
			    }
			}
		    }
		}
	    });

	JButton removeButton = new JButton("<");
	removeButton.setToolTipText(Language.getInstance().getText(
								   "Retirer_de_la_selection"));
	removeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    TreePath[] pathTab = chosenTree.getSelectionPaths();
		    if (pathTab != null) {
			for (int i = 0; i < pathTab.length; i++) {
			    DefaultMutableTreeNode nodeToRemove = (DefaultMutableTreeNode) pathTab[i]
				.getLastPathComponent();
			    String nodeValue = (String) nodeToRemove
				.getUserObject();
			    chosenTreeModel.removeNodeFromParent(nodeToRemove);
			    chosenNodes.remove(nodeValue);
			}
		    }

		}
	    });

	JPanel buttonSet = new JPanel();
	buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
	buttonSet.add(addButton);
	buttonSet.add(Box.createRigidArea(new Dimension(1, 25)));
	buttonSet.add(removeButton);

	JScrollPane chosenScrollPane = new JScrollPane(chosenTree);
	chosenScrollPane.setBorder(BorderFactory.createTitledBorder(Language
								    .getInstance().getText("Selection")));
	chosenScrollPane.setPreferredSize(new Dimension(300, 450));

	JScrollPane chapterScrollPane = new JScrollPane(chapterTree);
	chapterScrollPane.setBorder(BorderFactory.createTitledBorder(Language
								     .getInstance().getText("Document")));
	chapterScrollPane.setPreferredSize(new Dimension(300, 450));

	JPanel windowPanel = new JPanel();
	windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
	windowPanel.add(chapterScrollPane);
	windowPanel.add(Box.createRigidArea(new Dimension(20, 50)));
	windowPanel.add(buttonSet);
	windowPanel.add(Box.createRigidArea(new Dimension(20, 50)));
	windowPanel.add(chosenScrollPane);

	JButton validate = new JButton(Language.getInstance()
				       .getText("Valider"));
	validate.setToolTipText(Language.getInstance().getText("Valider"));
	validate.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			parentDialog.setChosenChaptersList(chosenNodes);
			parentDialog.setInitChapterSelection(true);
			ChapterChooser.this.dispose();
		    } catch (Exception ex) {
			ex.printStackTrace();
			Tools.ihmExceptionView(ex);
		    }
		}
	    });

	JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
	cancel.setToolTipText(Language.getInstance().getText("Annuler"));
	cancel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			ChapterChooser.this.dispose();
		    } catch (Exception ex) {
			ex.printStackTrace();
			Tools.ihmExceptionView(ex);
		    }
		}
	    });

	JPanel secondButtonSet = new JPanel();
	secondButtonSet.add(validate);
	secondButtonSet.add(cancel);

	JPanel center = new JPanel();
	center.add(windowPanel);

	JPanel page = new JPanel();
	page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
	page.add(center);
	page.add(secondButtonSet);

	getContentPane().add(page, BorderLayout.CENTER);
	setTitle(Language.getInstance().getText(
						"Ajouter_des_chapitres_a_la_selection"));
	// setLocation(300,200);
	centerScreen();
    }

    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2,
		    (dim.height - abounds.height) / 2);
	this.setVisible(true);
	requestFocus();
    }

    protected int getIndexFor(String nodeValue) {
	if (chosenNodes.size() == 0) {
	    return 0;
	} else {
	    int allChapterIndex = allChaptersList.indexOf(nodeValue);
	    int i = 0;
	    while (i < chosenNodes.size()) {
		String currentNode = (String) chosenNodes.get(i);
		int currentNodeChapterIndex = allChaptersList
		    .indexOf(currentNode);
		if (allChapterIndex < currentNodeChapterIndex) {
		    return i;
		}
		if (i == chosenNodes.size() - 1)
		    return i + 1;
		i++;
	    }
	}
	return 0;
    }

}
