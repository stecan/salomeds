/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;
import org.xml.sax.InputSource;

import salomeTMF_plug.docXML.data.GardePage;
import salomeTMF_plug.docXML.export.Contexte;
import salomeTMF_plug.docXML.export.XSLGenerator;
import salomeTMF_plug.docXML.export.XmlGenerator;
import salomeTMF_plug.docXML.export.Contexte.OutFormat;
import salomeTMF_plug.docXML.languages.Language;

import com.xmlmind.fo.converter.Converter;
import com.xmlmind.fo.converter.OutputDestination;
import org.apache.log4j.Logger;

/**
 * Classe creant l'IHM permettant a l'utilisateur de selectionner les
 * differentes options pour la creation de la documentation
 *
 * @author vapu8214
 */
public class GenDocDialog extends JDialog implements DataConstants,
						     ActionListener {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(GenDocDialog.class);
    
    final String DOC_XML_CFG_FILE = "/plugins/docXML/cfg/CfgDocXML.properties";
    Properties cfg_prop = null;

    private String tmpDir = System.getProperties()
	.getProperty("java.io.tmpdir");

    // Salome GUI
    static IPlugObject pIhm = null;

    JTabbedPane onglets;

    String reportFile;

    int sourceType;

    boolean annule = false;

    JRadioButton htmlRadio;

    JRadioButton docbookRadio;

    // 20100108 - D�but modification Forge ORTF
    JRadioButton wordRadio;
    JRadioButton pdfRadio;
    // 20100108 - Fin modifcation Forge ORTF

    JRadioButton withoutFrameRadio;

    JRadioButton multiFrameRadio;

    JLabel htmlLabel;

    JTextField reportTextField;

    JButton htmlButton;

    JButton validationButton;

    JButton cancelButton;

    final String url_txt;

    String urlTranslate;

    JRadioButton formBox;

    JRadioButton gardeBox;

    JLabel gardeLabel;

    JTextField gardeTextField;

    JButton gardeButton;

    JButton formulaireButton;

    String gardeFile;

    JLabel enTeteLabel;

    JTextField enTeteTextField;

    JButton enTeteButton;

    String enTeteFile;

    JLabel piedLabel;

    JTextField piedTextField;

    JButton piedButton;

    String piedFile;

    ButtonGroup miseFormeGroup;

    /*
     * Barre de progression
     */
    JProgressBar progress;

    /*
     * Attributs pour l'inclusion des fichiers
     */
    JLabel inclusPhrase;

    JTextField inclusFormat;

    JCheckBox jpgBox;

    JCheckBox pngBox;

    JCheckBox gifBox;

    Element pageGardeElement;

    JButton testSelectionButton;

    private ArrayList<TestList> suiteList;

    private ArrayList<Family> familyList;

    private ArrayList<Test> testList;

    JButton campSelectionButton;

    JButton chapterSelectionButton;

    ArrayList<Campaign> campList;

    ArrayList<Execution> execList;

    DefaultMutableTreeNode chosenRoot;

    boolean initSelection = false;
    boolean initChapterSelection = false;

    boolean generation = false;

    URL genFic;

    URL urlBase;

    String logoFilePath;

    GardePage save;

    private String errorMessage = "";
    private Vector<JPanel> panelsPluginsForExport = new Vector<JPanel>();
    private JComboBox comboEncoding;
    private Vector<String> codingList;

    private ArrayList<String> chosenChaptersList;

    /**
     * Creates a new instance of GraphicDialog
     */
    public GenDocDialog(int type, IPlugObject vt) throws Exception {
	super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
	pIhm = vt;
	urlBase = SalomeTMFContext.getInstance().getUrlBase();
	logger.info("urlBase = " + urlBase);

	codingList = new Vector<String>();
	codingList.add("ISO-8859-15");
	codingList.add("ISO-8859-1");
	codingList.add("UTF-8");
	codingList.add("UTF-16");
	comboEncoding = new JComboBox(codingList);
	comboEncoding.setEditable(false);

	Vector<Extension> listExtXMLPlugin = vt.getXMLPrintersExtension();
	int size = listExtXMLPlugin.size();
	for (int i = 0; i < size; i++) {
	    Extension pXMLExt = (Extension) listExtXMLPlugin.elementAt(i);
	    JPFManager pJPFManager = vt.getPluginManager();
	    try {
		XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager
		    .activateExtension(pXMLExt);
		if (pXMLPrinterPlugin != null) {
		    JPanel pPanel = pXMLPrinterPlugin.getExportOptionPanel();
		    if (pPanel != null
			&& !panelsPluginsForExport.contains(pPanel)) {
			panelsPluginsForExport.add(pPanel);
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	String _urlBase = urlBase.toString();
	url_txt = _urlBase.substring(0, _urlBase.lastIndexOf("/"));
	sourceType = type;

	htmlLabel = new JLabel();
	htmlLabel.setText(Language.getInstance().getText(
							 "Fichier_de_sauvegarde_pour_la_documentation_"));
	reportTextField = new JTextField(30);
	htmlButton = new JButton(Language.getInstance().getText("Choisir"));
	htmlButton.addActionListener(this);

	htmlRadio = new JRadioButton(Language.getInstance().getText("HTML"),
				     true);
	docbookRadio = new JRadioButton(Language.getInstance().getText(
								       "DocBook"));
	// 20100108 - D�but modification Forge ORTF
	wordRadio = new JRadioButton(Language.getInstance().getText("Word"));
	pdfRadio = new JRadioButton(Language.getInstance().getText("Pdf"));
	// 20100108 - Fin modification Forge ORTF
	withoutFrameRadio = new JRadioButton(Language.getInstance().getText(
									    "Mode_simple"), true);
	multiFrameRadio = new JRadioButton(Language.getInstance().getText(
									  "Mode_multi_frames"));
	htmlRadio.addActionListener(this);
	docbookRadio.addActionListener(this);
	// 20100108 - D�but modification Forge ORTF
	wordRadio.addActionListener(this);
	pdfRadio.addActionListener(this);
	// 20100108 - Fin modification Forge ORTF
	ButtonGroup frameGroup = new ButtonGroup();
	frameGroup.add(withoutFrameRadio);
	frameGroup.add(multiFrameRadio);
	ButtonGroup langageGroup = new ButtonGroup();
	langageGroup.add(htmlRadio);
	langageGroup.add(docbookRadio);
	// 20100108 - D�but modification Forge ORTF
	langageGroup.add(wordRadio);
	langageGroup.add(pdfRadio);
	// 20100108 - Fin modification Forge ORTF

	inclusPhrase = new JLabel(Language.getInstance().getText(
								 "Extensions_des_fichiers_a_inclure_txt_java_"));
	inclusFormat = new JTextField(30);
	jpgBox = new JCheckBox(".jpeg");
	pngBox = new JCheckBox(".png");
	gifBox = new JCheckBox(".gif");

	validationButton = new JButton(Language.getInstance()
				       .getText("Generer"));
	validationButton.addActionListener(this);

	cancelButton = new JButton(Language.getInstance().getText("Annuler"));
	cancelButton.addActionListener(this);

	formulaireButton = new JButton(Language.getInstance().getText(
								      "Formulaire"));
	formulaireButton.addActionListener(this);

	formBox = new JRadioButton(Language.getInstance().getText(
								  "A_partir_d_un_"));
	formBox.addActionListener(this);
	formBox.setSelected(true);

	gardeBox = new JRadioButton(Language.getInstance().getText(
								   "A_partir_de_fichiers_html__"));
	gardeBox.addActionListener(this);

	miseFormeGroup = new ButtonGroup();
	miseFormeGroup.add(formBox);
	miseFormeGroup.add(gardeBox);

	gardeLabel = new JLabel();
	gardeLabel.setText(Language.getInstance().getText("Page_de_garde_"));
	gardeTextField = new JTextField(30);
	gardeButton = new JButton(Language.getInstance().getText("Choisir"));
	gardeButton.addActionListener(this);

	enTeteLabel = new JLabel();
	enTeteLabel.setText(Language.getInstance().getText("En_tete_"));
	enTeteTextField = new JTextField(30);
	enTeteButton = new JButton(Language.getInstance().getText("Choisir"));
	enTeteButton.addActionListener(this);

	piedLabel = new JLabel();
	piedLabel.setText(Language.getInstance().getText("Pied_de_page_"));
	piedTextField = new JTextField(30);
	piedButton = new JButton(Language.getInstance().getText("Choisir"));
	piedButton.addActionListener(this);

	JLabel encodingLabel = new JLabel(Language.getInstance().getText(
									 "Encodage_"));

	JPanel encodingPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	encodingPanel.add(encodingLabel);
	encodingPanel.add(comboEncoding);
	encodingPanel.add(Box.createHorizontalGlue());

	JPanel htmlModePanel = new JPanel();
	htmlModePanel.setLayout(new BoxLayout(htmlModePanel, BoxLayout.Y_AXIS));
	htmlModePanel.add(withoutFrameRadio);
	htmlModePanel.add(multiFrameRadio);
	JPanel outputFormatFramePanel = new JPanel(new FlowLayout(
								  FlowLayout.LEFT));
	outputFormatFramePanel.add(Box.createHorizontalStrut(15));
	outputFormatFramePanel.add(htmlModePanel);

	JPanel htmlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	htmlPanel.add(htmlRadio);

	JPanel docbookPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	docbookPanel.add(docbookRadio);

	// 20100108 - D�but modification Forge ORTF
	JPanel wordPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	wordPanel.add(wordRadio);

	JPanel pdfPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	pdfPanel.add(pdfRadio);
	// 20100108 - Fin modification Forge ORTF

	JPanel outputFormatPanel = new JPanel();
	outputFormatPanel.setLayout(new BoxLayout(outputFormatPanel,
						  BoxLayout.Y_AXIS));
	outputFormatPanel.add(htmlPanel);
	outputFormatPanel.add(outputFormatFramePanel);
	outputFormatPanel.add(docbookPanel);
	// 20100108 - D�but modification Forge ORTF
	outputFormatPanel.add(wordPanel);
	outputFormatPanel.add(pdfPanel);
	// 20100108 - Fin modification Forge ORTF

	JPanel outputPanel = new JPanel();
	outputPanel.setLayout(new BoxLayout(outputPanel, BoxLayout.Y_AXIS));
	TitledBorder outputTitle = BorderFactory.createTitledBorder(Language
								    .getInstance().getText("Sortie"));
	outputPanel.setBorder(outputTitle);
	outputPanel.add(outputFormatPanel);
	outputPanel.add(Box.createVerticalStrut(10));
	outputPanel.add(encodingPanel);

	JPanel panelChoisir = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelChoisir.add(htmlButton);
	JPanel panelHtmlTF = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelHtmlTF.add(reportTextField);
	JPanel panelHtmlLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelHtmlLabel.add(htmlLabel);

	JPanel panelCenter = new JPanel();
	TitledBorder sauvTitle;
	sauvTitle = BorderFactory.createTitledBorder(Language.getInstance()
						     .getText("Sauvegarde"));
	panelCenter.setBorder(sauvTitle);
	panelCenter.setLayout(new BoxLayout(panelCenter, BoxLayout.Y_AXIS));
	panelCenter.add(panelHtmlLabel);
	panelCenter.add(panelHtmlTF);
	panelCenter.add(panelChoisir);

	JPanel buttons = new JPanel();
	buttons.add(validationButton);
	buttons.add(cancelButton);

	progress = new JProgressBar();
	JPanel progression = new JPanel(new FlowLayout(FlowLayout.CENTER));
	progression.add(progress);

	JPanel onglet1 = new JPanel();
	onglet1.setLayout(new BoxLayout(onglet1, BoxLayout.Y_AXIS));
	onglet1.add(outputPanel);
	onglet1.add(panelCenter);
	onglet1.add(Box.createRigidArea(new Dimension(1, 125)));

	JPanel commonPanel = new JPanel();
	commonPanel.setLayout(new BoxLayout(commonPanel, BoxLayout.Y_AXIS));
	commonPanel.add(buttons);
	commonPanel.add(Box.createRigidArea(new Dimension(1, 5)));
	commonPanel.add(progression);

	JPanel panelForm = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelForm.add(formBox);
	panelForm.add(formulaireButton);

	JPanel ligneFic = new JPanel(new FlowLayout(FlowLayout.LEFT));
	ligneFic.add(gardeBox);

	JPanel panelGardeLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelGardeLabel.add(gardeLabel);
	JPanel panelGardeTF = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelGardeTF.add(gardeTextField);
	panelGardeLabel.add(gardeLabel);
	JPanel panelTeteLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelTeteLabel.add(enTeteLabel);
	JPanel panelTeteTF = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelTeteTF.add(enTeteTextField);
	JPanel panelTeteButton = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelTeteButton.add(enTeteButton);
	JPanel panelPiedLabel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelPiedLabel.add(piedLabel);
	JPanel panelPiedTF = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelPiedTF.add(piedTextField);
	JPanel panelPiedButton = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelPiedButton.add(piedButton);
	JPanel panelGardeChoisir = new JPanel(new FlowLayout(FlowLayout.LEFT));
	panelGardeChoisir.add(gardeButton);
	JPanel fichier = new JPanel();
	fichier.setLayout(new BoxLayout(fichier, BoxLayout.Y_AXIS));
	fichier.add(panelGardeLabel);
	fichier.add(panelGardeTF);
	fichier.add(panelGardeChoisir);
	fichier.add(Box.createRigidArea(new Dimension(1, 10)));
	fichier.add(panelTeteLabel);
	fichier.add(panelTeteTF);
	fichier.add(panelTeteButton);
	fichier.add(Box.createRigidArea(new Dimension(1, 10)));
	fichier.add(panelPiedLabel);
	fichier.add(panelPiedTF);
	fichier.add(panelPiedButton);
	gardeLabel.setEnabled(false);
	enTeteLabel.setEnabled(false);
	piedLabel.setEnabled(false);
	gardeTextField.setEnabled(false);
	enTeteTextField.setEnabled(false);
	enTeteButton.setEnabled(false);
	piedTextField.setEnabled(false);
	piedButton.setEnabled(false);
	gardeButton.setEnabled(false);

	JPanel onglet2 = new JPanel();
	onglet2.setLayout(new BoxLayout(onglet2, BoxLayout.Y_AXIS));
	onglet2.add(panelForm);
	onglet2.add(Box.createRigidArea(new Dimension(1, 10)));
	onglet2.add(ligneFic);
	onglet2.add(fichier);

	testSelectionButton = new JButton(Language.getInstance().getText("Selection_des_tests"));
	testSelectionButton.addActionListener(this);

	JPanel selectTestCampPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

	campSelectionButton = new JButton(Language.getInstance().getText("Selection_des_campagnes"));
	campSelectionButton.addActionListener(this);

	if (sourceType != CAMPAIGN) {
	    selectTestCampPanel.add(testSelectionButton);
	} else {
	    selectTestCampPanel.add(campSelectionButton);
	}

	chapterSelectionButton = new JButton(Language.getInstance().getText("Selection_des_chapitres"));
	chapterSelectionButton.addActionListener(this);
	initChosenChaptersList();

	JPanel selectChapterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	selectChapterPanel.add(chapterSelectionButton);

	JPanel selectPanel = new JPanel();
	selectPanel.setLayout(new BoxLayout(selectPanel, BoxLayout.Y_AXIS));
	selectPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Filtrage")));
	selectPanel.add(selectTestCampPanel);
	selectPanel.add(selectChapterPanel);

	JPanel inclusLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	inclusLabelPanel.add(inclusPhrase);
	JPanel inclusTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	inclusTFPanel.add(inclusFormat);
	JPanel inclusionPanel = new JPanel();
	inclusionPanel
	    .setLayout(new BoxLayout(inclusionPanel, BoxLayout.Y_AXIS));
	inclusionPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Fichiers_textes")));
	inclusionPanel.add(inclusLabelPanel);
	inclusionPanel.add(inclusTFPanel);

	JPanel imgJpgPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	imgJpgPanel.add(jpgBox);
	JPanel imgGifPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	imgGifPanel.add(gifBox);
	JPanel imgPngPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	imgPngPanel.add(pngBox);

	JPanel imgPanel = new JPanel();
	imgPanel.setLayout(new BoxLayout(imgPanel, BoxLayout.Y_AXIS));
	imgPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Fichiers_images")));
	imgPanel.add(imgJpgPanel);
	imgPanel.add(imgGifPanel);
	imgPanel.add(imgPngPanel);

	JPanel inclusTotalPanel = new JPanel();
	inclusTotalPanel.setLayout(new BoxLayout(inclusTotalPanel,
						 BoxLayout.Y_AXIS));
	inclusTotalPanel.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Inclusion_des_fichiers")));
	inclusTotalPanel.add(inclusionPanel);
	inclusTotalPanel.add(imgPanel);

	JPanel onglet3 = new JPanel();
	onglet3.setLayout(new BoxLayout(onglet3, BoxLayout.Y_AXIS));
	onglet3.add(selectPanel);
	onglet3.add(Box.createRigidArea(new Dimension(1, 10)));
	onglet3.add(inclusTotalPanel);

	onglets = new JTabbedPane();
	onglets.addTab(Language.getInstance().getText("Principal"), onglet1);
	onglets
	    .addTab(Language.getInstance().getText("Mise_en_Forme"),
		    onglet2);
	onglets.addTab(Language.getInstance().getText("Options"), onglet3);

	/**** Ajout des tabs des plugins ****/
	int size2 = panelsPluginsForExport.size();
	for (int i = 0; i < size2; i++) {
	    JPanel pPanel = (JPanel) panelsPluginsForExport.elementAt(i);
	    String name = pPanel.getName();
	    onglets.addTab(name, pPanel);
	}

	Container contentPaneFrame = this.getContentPane();
	contentPaneFrame.add(onglets, BorderLayout.CENTER);
	contentPaneFrame.add(commonPanel, BorderLayout.SOUTH);
	setPreferredSize(new Dimension(450, 550));
	getRootPane().setDefaultButton(this.validationButton);
	this.setTitle(Language.getInstance().getText("Generation_de_document"));
	// this.setLocation(400, 100);
	centerScreen();
    }

    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2,
		    (dim.height - abounds.height) / 2);
	this.setVisible(true);
	requestFocus();
    }

    /***************************************************************************
     * Listener
     **************************************************************************/

    public void actionPerformed(ActionEvent evt) {
	if (evt.getSource().equals(htmlRadio)) {
	    htmlRadioPerformed();
	} else if (evt.getSource().equals(docbookRadio)) {
	    docbookRadioPerformed();
	}
	// 20100108 - D�but modification Forge ORTF
	else if (evt.getSource().equals(wordRadio)) {
	    wordRadioPerformed();
	} else if (evt.getSource().equals(pdfRadio)) {
	    pdfRadioPerformed();
	}
	// 20100108 - Fin modification Forge ORTF
	else if (evt.getSource().equals(htmlButton)) {
	    try {
		htmlButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(validationButton)) {
	    validationButtonPerformed();
	} else if (evt.getSource().equals(cancelButton)) {
	    try {
		cancelButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(formulaireButton)) {
	    try {
		formulaireButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(formBox)) {
	    try {
		formBoxPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(gardeBox)) {
	    try {
		gardeBoxPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(gardeButton)) {
	    try {
		gardeButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(enTeteButton)) {
	    try {
		enTeteButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(piedButton)) {
	    try {
		piedButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(testSelectionButton)) {
	    try {
		testSelectionButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(campSelectionButton)) {
	    try {
		campSelectionButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(chapterSelectionButton)) {
	    chapterSelectionButtonPerformed();
	}

    }

    void htmlRadioPerformed() {
	if (!withoutFrameRadio.isEnabled()) {
	    withoutFrameRadio.setEnabled(true);
	    multiFrameRadio.setEnabled(true);
	    onglets.setEnabledAt(1, true);
	    comboEncoding.setEnabled(true);
	}
    }

    void docbookRadioPerformed() {
	withoutFrameRadio.setEnabled(false);
	multiFrameRadio.setEnabled(false);
	onglets.setEnabledAt(1, false);
	comboEncoding.setEnabled(true);
    }

    // 20100108 - D�but modification Forge ORTF
    void wordRadioPerformed() {
	withoutFrameRadio.setEnabled(false);
	multiFrameRadio.setEnabled(false);
	onglets.setEnabledAt(1, false);
	comboEncoding.setEnabled(false);
    }

    void pdfRadioPerformed() {
	withoutFrameRadio.setEnabled(false);
	multiFrameRadio.setEnabled(false);
	onglets.setEnabledAt(1, false);
	comboEncoding.setEnabled(false);
    }

    // 20100108 - Fin modification Forge ORTF

    void htmlButtonPerformed() throws Exception {
	JFileChooser fileChooser = new JFileChooser();
	if (htmlRadio.isSelected()) {
	    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language
								    .getInstance().getText("Fichier_HTML_html"), ".html"));
	    fileChooser.setSelectedFile(new File("index.html"));
	    int returnVal = fileChooser.showDialog(GenDocDialog.this, Language
						   .getInstance().getText("Selectionner"));
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		reportFile = fileChooser.getSelectedFile().getAbsolutePath();
		if (reportFile.indexOf(".") != -1) {
		    if (!reportFile.substring(reportFile.lastIndexOf("."))
			.equals(".html")) {
			reportFile += ".html";
		    }
		} else {
		    reportFile += ".html";
		}
	    }
	} else if (docbookRadio.isSelected()) {
	    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language
								    .getInstance().getText("Fichier_XML____xml_"), ".xml"));
	    fileChooser.setSelectedFile(new File("docbook.xml"));
	    int returnVal = fileChooser.showDialog(GenDocDialog.this, Language
						   .getInstance().getText("Selectionner"));
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		reportFile = fileChooser.getSelectedFile().getAbsolutePath();
		if (reportFile.indexOf(".") != -1) {
		    if (!reportFile.substring(reportFile.lastIndexOf("."))
			.equals(".xml")
			&& !reportFile.substring(
						 reportFile.lastIndexOf(".")).equals(
										     ".docbook")) {
			reportFile += ".xml";
		    }
		} else {
		    reportFile += ".xml";
		}
	    }
	}
	// 20100108 - D�but modification Forge ORTF
	else if (wordRadio.isSelected()) {
	    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language
								    .getInstance().getText("Fichier_Word_docx"), ".docx"));
	    fileChooser.setSelectedFile(new File("word.docx"));
	    int returnVal = fileChooser.showDialog(GenDocDialog.this, Language
						   .getInstance().getText("Selectionner"));
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		reportFile = fileChooser.getSelectedFile().getAbsolutePath();
		if (reportFile.indexOf(".") != -1) {
		    if (!reportFile.substring(reportFile.lastIndexOf("."))
			.equals(".docx")) {
			reportFile += ".docx";
		    }
		} else {
		    reportFile += ".docx";
		}
	    }
	} else if (pdfRadio.isSelected()) {
	    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language
								    .getInstance().getText("Fichier_Pdf_pdf"), ".pdf"));
	    fileChooser.setSelectedFile(new File("pdf.pdf"));
	    int returnVal = fileChooser.showDialog(GenDocDialog.this, Language
						   .getInstance().getText("Selectionner"));
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
		reportFile = fileChooser.getSelectedFile().getAbsolutePath();
		if (reportFile.indexOf(".") != -1) {
		    if (!reportFile.substring(reportFile.lastIndexOf("."))
			.equals(".pdf")) {
			reportFile += ".pdf";
		    }
		} else {
		    reportFile += ".pdf";
		}
	    }
	}
	// 20100108 - Fin modification Forge ORTF
	reportTextField.setText(reportFile);
    }

    void validationButtonPerformed() {
	Thread t = new Thread() {
		public void run() {
		    // Instanciation et lancement du traitement
		    try {
			genereDoc();
		    } catch (Exception ex) {
			ex.printStackTrace();
			Tools.ihmExceptionView(ex);
		    }
		}
	    };
	t.start();
    }

    void cancelButtonPerformed() throws Exception {
	annule = true;
	GenDocDialog.this.dispose();
    }

    void formulaireButtonPerformed() throws Exception {
	new GardeDialog(GenDocDialog.this);
    }

    void formBoxPerformed() throws Exception {
	gardeLabel.setEnabled(false);
	enTeteLabel.setEnabled(false);
	piedLabel.setEnabled(false);
	gardeTextField.setEnabled(false);
	enTeteTextField.setEnabled(false);
	enTeteButton.setEnabled(false);
	piedTextField.setEnabled(false);
	piedButton.setEnabled(false);
	gardeButton.setEnabled(false);
	formulaireButton.setEnabled(true);
    }

    void gardeBoxPerformed() throws Exception {
	gardeLabel.setEnabled(true);
	enTeteLabel.setEnabled(true);
	piedLabel.setEnabled(true);
	gardeTextField.setEnabled(true);
	enTeteTextField.setEnabled(true);
	enTeteButton.setEnabled(true);
	piedTextField.setEnabled(true);
	piedButton.setEnabled(true);
	gardeButton.setEnabled(true);
	formulaireButton.setEnabled(false);
    }

    String selectHTMLPerformed() throws Exception {
	String fileHTML = null;
	JFileChooser fileChooser = new JFileChooser();
	fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language
								.getInstance().getText("Fichier_HTML_html"), ".html"));
	int returnVal = fileChooser.showDialog(GenDocDialog.this, Language
					       .getInstance().getText("Selectionner"));
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    fileHTML = fileChooser.getSelectedFile().getAbsolutePath();
	    if (fileHTML.indexOf(".") != -1) {
		if (!fileHTML.substring(fileHTML.lastIndexOf(".")).equals(
									  ".html")) {
		    fileHTML += ".html";
		}
	    } else {
		fileHTML += ".html";
	    }
	}
	return fileHTML;
    }

    void gardeButtonPerformed() throws Exception {
	String fileHTML = selectHTMLPerformed();
	if (fileHTML != null) {
	    gardeFile = fileHTML;
	    gardeTextField.setText(gardeFile);
	}
    }

    void enTeteButtonPerformed() throws Exception {
	String fileHTML = selectHTMLPerformed();
	if (fileHTML != null) {
	    enTeteFile = fileHTML;
	    enTeteTextField.setText(enTeteFile);
	}
    }

    void piedButtonPerformed() throws Exception {
	String fileHTML = selectHTMLPerformed();
	if (fileHTML != null) {
	    piedFile = fileHTML;
	    piedTextField.setText(piedFile);
	}
    }

    void testSelectionButtonPerformed() throws Exception {
	TestChooser testChooser = new TestChooser(SalomeTMFPanels
						  .getTestDynamicTree().getRoot(), GenDocDialog.this,
						  initSelection, getChosenRoot(), TEST);
	if (!testChooser.isCancelled()) {
	    setInitSelection(true);
	    setFamilyList(testChooser.getTemporaryFamilyList());
	    setSuiteList(testChooser.getTemporaryTestListList());
	    setTestList(testChooser.getTemporaryTestList());
	    setChosenRoot((DefaultMutableTreeNode) testChooser
			  .getChosenTreeModel().getRoot());
	}
    }

    void campSelectionButtonPerformed() throws Exception {
	TestChooser testChooser = new TestChooser(SalomeTMFPanels
						  .getCampaignDynamicTree().getRoot(), GenDocDialog.this,
						  initSelection, getChosenRoot(), CAMPAIGN);
	if (!testChooser.isCancelled()) {
	    setInitSelection(true);
	    setCampList(testChooser.getTemporaryCampList());
	    setExecList(testChooser.getTemporaryExecList());
	    setChosenRoot((DefaultMutableTreeNode) testChooser
			  .getChosenTreeModel().getRoot());
	}
    }

    void chapterSelectionButtonPerformed() {
	initChosenChaptersList();
	new ChapterChooser(GenDocDialog.this, initChapterSelection,
			   chosenChaptersList);
    }

    public void initChosenChaptersList() {
	ArrayList<String> extList = new ArrayList<String>();
	Vector<Extension> listExtXMLPlugin = pIhm.getXMLPrintersExtension();
	int size = listExtXMLPlugin.size();
	for (int i = 0; i < size; i++) {
	    Extension pXMLExt = (Extension) listExtXMLPlugin.elementAt(i);
	    JPFManager pJPFManager = pIhm.getPluginManager();
	    try {
		XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager
		    .activateExtension(pXMLExt);
		if (pXMLPrinterPlugin != null) {
		    String title = pXMLPrinterPlugin.getChapterTitleInReport();
		    if (title != null && !title.equals("")) {
			extList.add(title);
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	String projet = Language.getInstance().getText("Projet");
	String environnements = Language.getInstance()
	    .getText("Environnements");
	String dossierTests = Language.getInstance()
	    .getText("Dossier_de_tests");
	String campagnes = Language.getInstance()
	    .getText("Campagnes_du_projet");
	String executions = Language.getInstance().getText(
							   "Rapport_d_execution");
	chosenChaptersList = new ArrayList<String>();
	chosenChaptersList.add(projet);
	chosenChaptersList.add(environnements);
	chosenChaptersList.addAll(extList);
	chosenChaptersList.add(dossierTests);
	if (sourceType == CAMPAIGN) {
	    chosenChaptersList.add(campagnes);
	    chosenChaptersList.add(executions);
	}
    }

    /***************************************************************************
     * Methodes
     **************************************************************************/

    /**
     * Generation de la documentation Appel des methodes de XmlGenerator
     */
    public void genereDoc() throws Exception {
	boolean ok = true;
	if (reportTextField.getText().equals("")) {
	    JOptionPane
		.showMessageDialog(
				   GenDocDialog.this,
				   Language
				   .getInstance()
				   .getText("Vous_devez_entrez_un_nom_de_fichier_pour_le_document_genere"),
				   Language.getInstance().getText("Erreur_"),
				   JOptionPane.ERROR_MESSAGE);
	    ok = false;
	    return;
	}

	String[] ficType = null;
	if (!inclusFormat.getText().equals("")) {
	    ficType = inclusFormat.getText().split(",");
	}

	/** ************ TRANSFORMATION XML ******************* */

	progress.setIndeterminate(true);

	String saveFileName = reportTextField.getText().trim();
	String dirHtml = saveFileName.substring(0, saveFileName
						.lastIndexOf(File.separator));
	String xmlDirName = saveFileName.substring(0, saveFileName
						   .lastIndexOf(File.separator));
	URL dtdStat = null;
	URL dtdDyna = null;
	URL css = null;
	URL cssPrint = null;
	URL translate = null;
	OutFormat outputFormat = OutFormat.HTML;
	if (docbookRadio.isSelected()) {
	    outputFormat = OutFormat.DOCBOOK;
	}
	// 20100108 - D�but modification Forge ORTF
	if (wordRadio.isSelected()) {
	    outputFormat = OutFormat.WORD;
	}
	if (pdfRadio.isSelected()) {
	    outputFormat = OutFormat.PDF;
	}
	// 20100108 - Fin modification Forge ORTF
	Contexte pContexte = new Contexte(dirHtml, ficType,
					  (String) comboEncoding.getSelectedItem(), outputFormat);
	XmlGenerator pXmlGenerator;
	pXmlGenerator = new XmlGenerator(pIhm, pContexte);

	try {
	    css = new URL(url_txt + "/plugins/docXML/xsl/salome.css");
	    cssPrint = new URL(url_txt + "/plugins/docXML/xsl/salome_print.css");
	    dtdStat = new URL(url_txt
			      + "/plugins/docXML/xsl/SalomeStatique.dtd");
	    dtdDyna = new URL(url_txt
			      + "/plugins/docXML/xsl/SalomeDynamique.dtd");
	    translate = new URL(url_txt + "/plugins/docXML/xsl/translate.xml");
	    urlTranslate = translate.toString();
	    try {
		if (sourceType != CAMPAIGN) {
		    Tools.writeFile(dtdStat.openStream(), tmpDir
				    + File.separator + "SalomeStatique.dtd");
		    Tools.writeFile(dtdStat.openStream(), xmlDirName
				    + File.separator + "SalomeStatique.dtd");
		} else {
		    Tools.writeFile(dtdDyna.openStream(), tmpDir
				    + File.separator + "SalomeDynamique.dtd");
		    Tools.writeFile(dtdDyna.openStream(), xmlDirName
				    + File.separator + "SalomeDynamique.dtd");
		}
		Tools.writeFile(css.openStream(), xmlDirName + File.separator
				+ "salome.css");
		Tools.writeFile(cssPrint.openStream(), xmlDirName
				+ File.separator + "salome_print.css");

		Tools.writeFile(translate.openStream(), tmpDir + File.separator
				+ "translate.xml");
		Tools.writeFile(translate.openStream(), xmlDirName
				+ File.separator + "translate.xml");
		// image mail
		URL emailURL = GenDocDialog.class
		    .getResource("/salomeTMF_plug/docXML/resources/img/pied_mail.gif");
		Tools.writeFile(emailURL.openStream(), xmlDirName
				+ File.separator + "pied_mail.gif");

		if (htmlRadio.isSelected() && multiFrameRadio.isSelected()) {
		    URL icon = GenDocDialog.class
			.getResource("/salomeTMF_plug/docXML/resources/img/ftv2mnode.png");
		    Tools.writeFile(icon.openStream(), xmlDirName
				    + File.separator + "ftv2mnode.png");
		    icon = GenDocDialog.class
			.getResource("/salomeTMF_plug/docXML/resources/img/ftv2pnode.png");
		    Tools.writeFile(icon.openStream(), xmlDirName
				    + File.separator + "ftv2pnode.png");
		    icon = GenDocDialog.class
			.getResource("/salomeTMF_plug/docXML/resources/img/ftv2node.png");
		    Tools.writeFile(icon.openStream(), xmlDirName
				    + File.separator + "ftv2node.png");
		}

		// Logo
		if (logoFilePath != null) {
		    String shortName = logoFilePath.toString()
			.substring(
				   logoFilePath.toString().lastIndexOf(
								       File.separator));
		    Tools.writeFile(
				    new FileInputStream(new File(logoFilePath)),
				    xmlDirName + File.separator + shortName);
		}
	    } catch (Exception exception) {
		exception.printStackTrace();
		// Cas DEBUG
		if (sourceType != CAMPAIGN) {
		    Tools
			.writeFile(
				   GenDocDialog.class
				   .getResourceAsStream("/salome/plugins/docXML/xsl/SalomeStatique.dtd"),
				   tmpDir + File.separator
				   + "SalomeStatique.dtd");
		    Tools
			.writeFile(
				   GenDocDialog.class
				   .getResourceAsStream("/salome/plugins/docXML/xsl/SalomeStatique.dtd"),
				   xmlDirName + File.separator
				   + "SalomeStatique.dtd");
		} else {
		    Tools
			.writeFile(
				   GenDocDialog.class
				   .getResourceAsStream("/salome/plugins/docXML/xsl/SalomeDynamique.dtd"),
				   tmpDir + File.separator
				   + "SalomeDynamique.dtd");
		    Tools
			.writeFile(
				   GenDocDialog.class
				   .getResourceAsStream("/salome/plugins/docXML/xsl/SalomeDynamique.dtd"),
				   xmlDirName + File.separator
				   + "SalomeDynamique.dtd");
		}
		Tools
		    .writeFile(
			       GenDocDialog.class
			       .getResourceAsStream("/salome/plugins/docXML/xsl/salome.css"),
			       xmlDirName + File.separator + "salome.css");
		Tools
		    .writeFile(
			       GenDocDialog.class
			       .getResourceAsStream("/salome/plugins/docXML/xsl/salome.css"),
			       xmlDirName + File.separator
			       + "salome_print.css");
		Tools
		    .writeFile(
			       GenDocDialog.class
			       .getResourceAsStream("/salome/plugins/docXML/xsl/translate.xml"),
			       tmpDir + File.separator + "translate.xml");
		Tools
		    .writeFile(
			       GenDocDialog.class
			       .getResourceAsStream("/salome/plugins/docXML/xsl/translate.xml"),
			       xmlDirName + File.separator + "translate.xml");
	    }

	    // Generation du fichier xml pour le dossier de tests
	    if (!annule) {
		if (sourceType != CAMPAIGN) {
		    Document doc = pXmlGenerator.toDocument(familyList,
							    suiteList, testList);
		    pXmlGenerator.documentToXML(doc, tmpDir + File.separator
						+ "salome.xml");
		    pXmlGenerator.documentToXML(doc, xmlDirName
						+ File.separator + "salome.xml");
		} else {
		    Document doc2 = pXmlGenerator.toDocumentDyna(campList,
								 execList);
		    pXmlGenerator.documentToXML(doc2, tmpDir + File.separator
						+ "salomeDyna.xml");
		    pXmlGenerator.documentToXML(doc2, xmlDirName
						+ File.separator + "salomeDyna.xml");
		}
	    }

	    /*            ************* TRANSFORMATION XSLT ******************* */

	    // tableau pour la selection des options
	    boolean multiFrame = multiFrameRadio.isSelected();
	    boolean htmlFormat = htmlRadio.isSelected();
	    Vector<XMLPrinterPlugin> listXMLPlugins = pXmlGenerator
		.getListXMLPlugin();
	    if (sourceType == CAMPAIGN && !annule) {
		genereDocCamp(listXMLPlugins, multiFrame, htmlFormat, dirHtml,
			      xmlDirName, saveFileName);
	    } else {
		genereDocTest(listXMLPlugins, multiFrame, htmlFormat,
			      xmlDirName, saveFileName);
	    }

	    if (ok && !annule) {
		progress.setVisible(false);
		JOptionPane
		    .showMessageDialog(
				       GenDocDialog.this,
				       Language
				       .getInstance()
				       .getText(
						"La_generation_de_document_s_est_terminee_avec_succes"),
				       Language.getInstance().getText("Information"),
				       JOptionPane.INFORMATION_MESSAGE);
		GenDocDialog.this.dispose();
	    }

	} catch (Exception e) {
	    e.printStackTrace();
	    Tools.ihmExceptionView(e);
	    progress.setVisible(false);
	    ok = false;
	    GenDocDialog.this.dispose();
	    return;
	}
    }

    /**
     * Genere le document des campagnes i.e. contenant les resultats d'execution
     *
     * @param pContexte
     * @param option
     *            0 : mode multi-frames, 1 : inclure les executions dans le
     *            dossier des campagnes, 2 : inclure les executions dans le
     *            dossier des campagnes
     * @param dirHtml
     * @param xmlDirName
     * @param saveFileName
     */
    void genereDocCamp(Vector<XMLPrinterPlugin> listXMLPlugins,
		       boolean multiFrame, boolean htmlFormat, String dirHtml,
		       String xmlDirName, String saveFileName) throws Exception {
	XSLGenerator pXSLGenerator = new XSLGenerator(listXMLPlugins, true,
						      tmpDir + File.separator + "translate.xml");

	try {
	    URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
	    String url_txt = _urlBase.toString();
	    url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
	    URL urlBase = new URL(url_txt);

	    java.net.URL url_cfg = new java.net.URL(urlBase + DOC_XML_CFG_FILE);
	    try {
		cfg_prop = Util.getPropertiesFile(url_cfg);
	    } catch (Exception e) {
		e.printStackTrace();
		cfg_prop = Util.getPropertiesFile(DOC_XML_CFG_FILE);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}

	// Creation du fichier xml pour les campagnes
	if (!annule) {
	    /** ************ TRANSFORMATION XSLT ******************* */
	    if (htmlFormat) {
		if (multiFrame) {
		    // Mode = multi-frames => creation de 3 fichiers html
		    String tests = saveFileName.substring(0, saveFileName
							  .lastIndexOf("."))
			+ "_princ.html";
		    pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					    + File.separator + "salomeDyna.xml", url_txt,
					    "/plugins/docXML/xsl/frameDynaXmlToHtml.xsl",
					    saveFileName, jpgBox.isSelected(), gifBox
					    .isSelected(), pngBox.isSelected(),
					    xmlDirName, chosenChaptersList);
		    miseEnForme(tests, pXSLGenerator, false);
		} else {
		    // creation du fichier html pour les campages en mode simple
		    // frame
		    pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					    + File.separator + "salomeDyna.xml", url_txt,
					    "/plugins/docXML/xsl/dynaXmlToHtml.xsl",
					    saveFileName, jpgBox.isSelected(), gifBox
					    .isSelected(), pngBox.isSelected(),
					    xmlDirName, chosenChaptersList);
		    miseEnForme(saveFileName, pXSLGenerator, false);
		}
	    } else {
		// 20100108 - D�but modification Forge ORTF
		if (docbookRadio.isSelected()) {
		    // creation d'un fichier au format docbook
		    pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					    + File.separator + "salomeDyna.xml", url_txt,
					    "/plugins/docXML/xsl/dynaXmlToDocBook.xsl",
					    saveFileName, jpgBox.isSelected(), gifBox
					    .isSelected(), pngBox.isSelected(),
					    xmlDirName, chosenChaptersList);
		}
		if (wordRadio.isSelected()) {
		    String tempFileName = System.getProperty("user.dir")
			+ "/tmp.xml";
		    pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					    + File.separator + "salomeDyna.xml", url_txt,
					    "/plugins/docXML/xsl/dynaXmlToDocBook.xsl",
					    tempFileName, jpgBox.isSelected(), gifBox
					    .isSelected(), pngBox.isSelected(),
					    xmlDirName, chosenChaptersList);
		    File xmlFile = new File(tempFileName);
		    File wordFile = new File(saveFileName);
		    loadXSL(cfg_prop, url_txt);
		    String xsl = "/plugins/docXML/fo/docbook.xsl";
		    String temporaryFilePath = System.getProperties()
			.getProperty("java.io.tmpdir")
			+ File.separator + "fo/docbook.xsl";
		    try {
			URL xsltURL = new URL(url_txt + xsl);
			Tools
			    .writeFile(xsltURL.openStream(),
				       temporaryFilePath);
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    File xsltFile = new File(temporaryFilePath);
		    File foFile = null;

		    try {
			TransformerFactory factory = TransformerFactory
			    .newInstance();
			// Efficient, reusable, thread-safe representation of
			// the XSLT
			// style sheet.
			Templates compiledStylesheet = factory
			    .newTemplates(new StreamSource(xsltFile));

			// [1] Convert XML to XSL-FO ---

			Transformer transformer = compiledStylesheet
			    .newTransformer();
			foFile = File.createTempFile("foFile", ".fo");
			transformer.transform(new StreamSource(xmlFile),
					      new StreamResult(foFile));

			// [2] Convert XSL-FO to RTF ---
			// throw new Exception("Pas de driver XmlMind");
			Converter converter = new Converter();
			converter.setProperty("outputFormat", "docx");
			converter.setProperty("baseURL", xmlFile.toURI()
					      .toASCIIString());

			InputSource src = new InputSource(foFile.toURI()
							  .toASCIIString());
			OutputDestination dst = new OutputDestination(wordFile
								      .getPath());
			converter.convert(src, dst);
		    } catch (Exception e) {
			e.printStackTrace();
			System.err.println("cannot convert '" + xmlFile
					   + "' to '" + wordFile
					   + "' using XSLT stylesheet '" + xsltFile
					   + "': " + e);
		    }
		}
		if (pdfRadio.isSelected()) {
		    String tempFileName = System.getProperty("user.dir")
			+ "/tmp.xml";
		    pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					    + File.separator + "salomeDyna.xml", url_txt,
					    "/plugins/docXML/xsl/dynaXmlToDocBook.xsl",
					    tempFileName, jpgBox.isSelected(), gifBox
					    .isSelected(), pngBox.isSelected(),
					    xmlDirName, chosenChaptersList);
		    File xmlFile = new File(tempFileName);
		    File pdfFile = new File(saveFileName);
		    loadXSL(cfg_prop, url_txt);
		    String xsl = "/plugins/docXML/fo/docbook.xsl";
		    String temporaryFilePath = System.getProperties()
			.getProperty("java.io.tmpdir")
			+ File.separator + "fo/docbook.xsl";
		    try {
			URL xsltURL = new URL(url_txt + xsl);
			Tools
			    .writeFile(xsltURL.openStream(),
				       temporaryFilePath);
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    File xsltFile = new File(temporaryFilePath);

		    FopFactory fopFactory = FopFactory.newInstance();
		    FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

		    OutputStream out = new java.io.FileOutputStream(pdfFile);
		    out = new java.io.BufferedOutputStream(out);

		    try {
			// Construct fop with desired output format
			Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF,
						    foUserAgent, out);

			// Setup XSLT
			TransformerFactory factory = TransformerFactory
			    .newInstance();
			Transformer transformer = factory
			    .newTransformer(new StreamSource(xsltFile));
			// transformer.

			// Set the value of a <param> in the stylesheet
			transformer.setParameter("versionParam", "2.0");

			// Setup input for XSLT transformation
			Source src = new StreamSource(xmlFile);
			// Resulting SAX events (the generated FO) must be piped
			// through to FOP
			Result res = new SAXResult(fop.getDefaultHandler());

			// Start XSLT transformation and FOP processing
			transformer.transform(src, res);
		    } catch (Exception e) {
			e.printStackTrace();
		    } finally {
			out.close();
		    }
		}
		// 20100108 - Fin modification Forge ORTF
		// miseEnForme(saveFileName, pXSLGenerator, false);
	    }
	}
    }

    void genereDocTest(Vector<XMLPrinterPlugin> listXMLPlugins,
		       boolean multiFrame, boolean htmlFormat, String xmlDirName,
		       String saveFileName) throws Exception {
	XSLGenerator pXSLGenerator = new XSLGenerator(listXMLPlugins, false,
						      tmpDir + File.separator + "translate.xml");
	try {
	    URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
	    String url_txt = _urlBase.toString();
	    url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
	    URL urlBase = new URL(url_txt);
	    java.net.URL url_cfg = new java.net.URL(urlBase + DOC_XML_CFG_FILE);
	    try {
		cfg_prop = Util.getPropertiesFile(url_cfg);
	    } catch (Exception e) {
		e.printStackTrace();
		cfg_prop = Util.getPropertiesFile(DOC_XML_CFG_FILE);
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	if (htmlFormat) {
	    if (multiFrame) {
		String tests = saveFileName.substring(0, saveFileName
						      .lastIndexOf("."))
		    + "_princ.html";
		pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					+ File.separator + "salome.xml", url_txt,
					"/plugins/docXML/xsl/frameStatiqueXmlToHtml.xsl",
					saveFileName, jpgBox.isSelected(), gifBox.isSelected(),
					pngBox.isSelected(), xmlDirName, chosenChaptersList);

		miseEnForme(tests, pXSLGenerator, true);
	    } else { // mode simple frame
		pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					+ File.separator + "salome.xml", url_txt,
					"/plugins/docXML/xsl/statiqueXmlToHtml.xsl",
					saveFileName, jpgBox.isSelected(), gifBox.isSelected(),
					pngBox.isSelected(), xmlDirName, chosenChaptersList);

		miseEnForme(saveFileName, pXSLGenerator, true);
	    }
	} else {
	    // 20100108 - D�but modification Forge ORTF
	    if (docbookRadio.isSelected()) {
		// creation d'un fichier au format docbook
		pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					+ File.separator + "salome.xml", url_txt,
					"/plugins/docXML/xsl/statiqueXmlToDocBook.xsl",
					saveFileName, jpgBox.isSelected(), gifBox.isSelected(),
					pngBox.isSelected(), xmlDirName, chosenChaptersList);
	    }
	    if (wordRadio.isSelected()) {
		String tempFileName = System.getProperty("user.dir")
		    + "/tmp.xml";
		pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					+ File.separator + "salome.xml", url_txt,
					"/plugins/docXML/xsl/statiqueXmlToDocBook.xsl",
					tempFileName, jpgBox.isSelected(), gifBox.isSelected(),
					pngBox.isSelected(), xmlDirName, chosenChaptersList);
		File xmlFile = new File(tempFileName);
		File wordFile = new File(saveFileName);
		loadXSL(cfg_prop, url_txt);
		String xsl = "/plugins/docXML/fo/docbook.xsl";
		String temporaryFilePath = System.getProperties().getProperty(
									      "java.io.tmpdir")
		    + File.separator + "fo/docbook.xsl";
		try {
		    URL xsltURL = new URL(url_txt + xsl);
		    Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
		} catch (Exception e) {
		    e.printStackTrace();
		}
		File xsltFile = new File(temporaryFilePath);
		File foFile = null;

		try {
		    TransformerFactory factory = TransformerFactory
			.newInstance();
		    // Efficient, reusable, thread-safe representation of the
		    // XSLT
		    // style sheet.
		    Templates compiledStylesheet = factory
			.newTemplates(new StreamSource(xsltFile));

		    // [1] Convert XML to XSL-FO ---

		    Transformer transformer = compiledStylesheet
			.newTransformer();
		    foFile = File.createTempFile("foFile", ".fo");
		    transformer.transform(new StreamSource(xmlFile),
					  new StreamResult(foFile));

		    // [2] Convert XSL-FO to WORD ---

		    Converter converter = new Converter();
		    converter.setProperty("outputFormat", "docx");
		    converter.setProperty("baseURL", xmlFile.toURI()
					  .toASCIIString());

		    InputSource src = new InputSource(foFile.toURI()
						      .toASCIIString());
		    OutputDestination dst = new OutputDestination(wordFile
								  .getPath());
		    converter.convert(src, dst);
		} catch (Exception e) {
		    e.printStackTrace();
		    // e.getCause().printStackTrace();
		    System.err.println("cannot convert '" + xmlFile + "' to '"
				       + wordFile + "' using XSLT stylesheet '" + xsltFile
				       + "': " + e);
		}
	    }
	    if (pdfRadio.isSelected()) {
		String tempFileName = System.getProperty("user.dir")
		    + "/tmp.xml";
		pXSLGenerator.transform(multiFrame, htmlFormat, tmpDir
					+ File.separator + "salome.xml", url_txt,
					"/plugins/docXML/xsl/statiqueXmlToDocBook.xsl",
					tempFileName, jpgBox.isSelected(), gifBox.isSelected(),
					pngBox.isSelected(), xmlDirName, chosenChaptersList);
		File xmlFile = new File(tempFileName);
		File pdfFile = new File(saveFileName);
		loadXSL(cfg_prop, url_txt);
		String xsl = "/plugins/docXML/fo/docbook.xsl";
		String temporaryFilePath = System.getProperties().getProperty(
									      "java.io.tmpdir")
		    + File.separator + "fo/docbook.xsl";
		try {
		    URL xsltURL = new URL(url_txt + xsl);
		    Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
		} catch (Exception e) {
		}
		File xsltFile = new File(temporaryFilePath);

		FopFactory fopFactory = FopFactory.newInstance();
		FOUserAgent foUserAgent = fopFactory.newFOUserAgent();

		OutputStream out = new java.io.FileOutputStream(pdfFile);
		out = new java.io.BufferedOutputStream(out);

		try {
		    // Construct fop with desired output format
		    Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF,
						foUserAgent, out);

		    // Setup XSLT
		    TransformerFactory factory = TransformerFactory
			.newInstance();
		    Transformer transformer = factory
			.newTransformer(new StreamSource(xsltFile));

		    // Set the value of a <param> in the stylesheet
		    transformer.setParameter("versionParam", "2.0");

		    // Setup input for XSLT transformation
		    Source src = new StreamSource(xmlFile);
		    // Resulting SAX events (the generated FO) must be piped
		    // through to FOP
		    Result res = new SAXResult(fop.getDefaultHandler());

		    // Start XSLT transformation and FOP processing
		    transformer.transform(src, res);
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
		    out.close();
		}
	    }
	    // 20100108 - Fin modification Forge ORTF
	    // miseEnForme(saveFileName, pXSLGenerator, true);
	}
    }

    /**
     * Methode qui appelle les methodes de XmlGenerator qui permettent d'ajouter
     * les elements de la mise en forme en fonction de ce qui a ete rempli dans
     * l'onglet "mise en forme" de l'IHM
     *
     * @param path
     *            page html auquel on ajoute une mise en page
     * @param testPlan
     *            true if the generated document id the project test plan
     *
     */
    public void miseEnForme(String path, XSLGenerator pXSLGenerator,
			    boolean testPlan) throws Exception {
	if (gardeBox.isSelected()) {
	    // Insertion en-tete
	    if (!enTeteTextField.getText().equals("")) {
		pXSLGenerator.addTitlePage(path, enTeteTextField.getText()
					   .trim(), true);
	    }
	    // Insertion page de garde
	    if (!gardeTextField.getText().equals("")) {
		pXSLGenerator.addTitlePage(path, gardeTextField.getText()
					   .trim(), true);
	    }
	    // Insertion pied de page
	    if (!piedTextField.getText().equals("")) {
		pXSLGenerator.addTitlePage(path,
					   piedTextField.getText().trim(), false);
	    }
	} else {
	    // Insertion page de garde et/ou Pied de page avec formulaire de
	    // creation
	    if (pageGardeElement == null)
		createTitlePage(testPlan);
	    pXSLGenerator.addTitlePage(path, pageGardeElement, true);
	}
    }

    /**
     * Create the title page by default
     *
     * @param testPlan
     *            true if the generated document is the project test plan
     */
    public void createTitlePage(boolean testPlan) throws Exception {
	String title = "";
	if (testPlan) {
	    if (Api.getUsedLocale().equals("fr"))
		title = "Dossier de tests<br>"
		    + DataModel.getCurrentProject().getNameFromModel();
	    else
		title = "Tests plan<br>"
		    + DataModel.getCurrentProject().getNameFromModel();
	} else {
	    if (Api.getUsedLocale().equals("fr"))
		title = "Resultat des tests<br>"
		    + DataModel.getCurrentProject().getNameFromModel();
	    else
		title = "Tests result<br>"
		    + DataModel.getCurrentProject().getNameFromModel();
	}
	createTitlePage(null, null, title, null, null, new Date(), DataModel
			.getCurrentUser().getFirstNameFromModel()
			+ " " + DataModel.getCurrentUser().getLastNameFromModel(),
			DataModel.getCurrentUser().getEmailFromModel());
    }

    /**
     * To create title page
     *
     * @param urlLogo
     *            company logo url (can be null)
     * @param company
     *            company name (can be null)
     * @param title
     *            document's title (mandatory)
     * @param version
     *            document's version (can be null)
     * @param infos
     *            additional information (can be null)
     * @param date
     * @param author
     *            document's author (can be null)
     * @param email
     *            author's email (can be null)
     */
    public void createTitlePage(String logoFilePath, String company,
				String title, String version, String infos, Date date,
				String author, String email) throws Exception {
	DocumentFactory factory = new DocumentFactory();
	this.logoFilePath = logoFilePath;
	Element aTransmettre = factory.createElement("div");
	if (logoFilePath != null && !logoFilePath.equals("") || company != null
	    && !company.equals("")) {
	    Element tableLogoCompany = aTransmettre.addElement("table");
	    Element tr1 = tableLogoCompany.addElement("tr");
	    if (logoFilePath != null && !logoFilePath.equals("")) {
		Element td11 = tr1.addElement("td");
		Element img1 = td11.addElement("img");
		String shortName = logoFilePath.substring(logoFilePath
							  .lastIndexOf(File.separator) + 1);
		img1.addAttribute("src", shortName);
		img1.addAttribute("border", "0");
	    } else {
		logoFilePath = null;
	    }
	    if (company != null && !company.equals("")) {
		Element td12 = tr1.addElement("td");
		Element div = td12.addElement("div");
		String[] list = company.split("\n");
		Element h3Elem = div.addElement("h3");
		h3Elem.addText(list[0]);
		for (int i = 1; i < list.length; i++) {
		    h3Elem.addElement("br");
		    h3Elem.addText(list[i]);
		}
	    }
	}
	Element divTitle = aTransmettre.addElement("div");
	divTitle.addAttribute("id", "title_titlepage");
	Element table3 = divTitle.addElement("table");
	table3.addAttribute("bgcolor", "#ff9933");
	table3.addAttribute("width", "90%");
	table3.addAttribute("border", "0");
	table3.addAttribute("cellpading", "0");
	table3.addAttribute("cellspacing", "3");
	table3.addAttribute("align", "center");
	Element t3tr1 = table3.addElement("tr");
	Element t3td11 = t3tr1.addElement("td");
	Element center = t3td11.addElement("center");
	Element font = center.addElement("font");
	font.addAttribute("color", "#ffffff");
	font.addAttribute("size", "8");
	if (title.indexOf("<br>") != -1) {
	    String[] titles = title.split("<br>");
	    font.addText(titles[0]);
	    font.addElement("br");
	    font.addText(titles[1]);
	} else
	    font.setText(title);
	Element t3tr2 = table3.addElement("tr");
	Element t3td21 = t3tr2.addElement("td");
	if (version != null && !version.equals("")) {
	    Element center2 = t3td21.addElement("center");
	    Element font2 = center2.addElement("font");
	    font2.addAttribute("color", "#ffffff");
	    font2.addAttribute("size", "4");
	    font2.setText("Version " + version);
	}
	if (infos != null && !infos.equals("")) {
	    Element pInfo = aTransmettre.addElement("p");
	    pInfo.addAttribute("align", "center");
	    String[] list2 = infos.split("\n");
	    for (int i = 0; i < list2.length; i++) {
		pInfo.addText(list2[i]);
		pInfo.addElement("br");
	    }
	}
	Element divDateAuthor = aTransmettre.addElement("div");
	divDateAuthor.addAttribute("id", "date_author");
	Element tableDate = divDateAuthor.addElement("table");
	tableDate.addAttribute("width", "100%");
	Element tr5 = tableDate.addElement("tr");
	Element td51 = tr5.addElement("td");
	td51.addAttribute("height", "15%");
	td51.addAttribute("valign", "center");
	DateFormat df = DateFormat.getDateInstance(DateFormat.FULL,
						   Locale.FRENCH);
	td51.setText(df.format(date));
	if (author != null && !author.equals("")) {
	    Element td52 = tr5.addElement("td");
	    td52.addAttribute("colspan", "2");
	    td52.addAttribute("align", "right");
	    td52.addAttribute("valign", "center");
	    td52.addText(author);
	    td52.addElement("br");
	    if (email != null && !email.equals("")) {
		Element divMonitor = td52.addElement("div");
		divMonitor.addAttribute("id", "email_monitor");
		Element a = divMonitor.addElement("a");
		a.addAttribute("title", Language.getInstance().getText(
								       "Contacter_l_auteur"));
		a.addAttribute("href", "mailto:" + email);
		Element img = a.addElement("img");
		img.addAttribute("alt", Language.getInstance().getText(
								       "Contacter_l_auteur"));
		// URL mailURL =
		// getClass().getResource("/salomeTMF_plug/docXML/resources/img/pied_mail.gif");
		// img.addAttribute("src", mailURL.toString());
		img.addAttribute("src", "pied_mail.gif");
		img.addAttribute("border", "0");
		img.addAttribute("width", "25");
		img.addAttribute("height", "42");
		Element divPrint = td52.addElement("div");
		divPrint.addAttribute("id", "email_print");
		divPrint.addElement("b").setText("Email : \u00A0");
		divPrint.addText(email);
	    }
	}
	aTransmettre.addElement("hr");
	setElement(aTransmettre);
    }

    public void setElement(Element e) {
	pageGardeElement = e;
    }

    public Element getElement() {
	return pageGardeElement;
    }

    public void setSuiteList(ArrayList<TestList> li) {
	if (li != null)
	    suiteList = li;
    }

    public void setFamilyList(ArrayList<Family> li) {
	if (li != null)
	    familyList = li;
    }

    public void setTestList(ArrayList<Test> li) {
	if (li != null)
	    testList = li;
    }

    public void setCampList(ArrayList<Campaign> camp) {
	if (camp != null)
	    campList = camp;
    }

    public void setExecList(ArrayList<Execution> exec) {
	if (exec != null)
	    execList = exec;
    }

    public void annule() throws Exception {
	annule = true;
	GenDocDialog.this.dispose();
    }

    /**
     * Getter for property chosenRoot.
     *
     * @return Value of property chosenRoot.
     */
    public javax.swing.tree.DefaultMutableTreeNode getChosenRoot() {
	return chosenRoot;
    }

    /**
     * Setter for property chosenRoot.
     *
     * @param chosenRoot
     *            New value of property chosenRoot.
     */
    public void setChosenRoot(javax.swing.tree.DefaultMutableTreeNode chosenRoot) {
	this.chosenRoot = chosenRoot;
    }

    /**
     * Getter for property initSelection.
     *
     * @return Value of property initSelection.
     */
    public boolean isInitSelection() {
	return initSelection;
    }

    /**
     * Setter for property initSelection.
     *
     * @param initSelection
     *            New value of property initSelection.
     */
    public void setInitSelection(boolean initSelection) {
	this.initSelection = initSelection;
    }

    /**
     * Getter for property save.
     *
     * @return Value of property save.
     */
    public salomeTMF_plug.docXML.data.GardePage getSave() {
	return save;
    }

    /**
     * Setter for property save.
     *
     * @param save
     *            New value of property save.
     */
    public void setSave(salomeTMF_plug.docXML.data.GardePage save) {
	this.save = save;
    }

    /**
     * Getter for property url_txt.
     *
     * @return Value of property url_txt.
     */
    public java.lang.String getUrl_txt() {
	return url_txt;
    }

    /**
     * @return Returns the sourceType.
     */
    public int getSourceType() {
	return sourceType;
    }

    /**
     * @param sourceType
     *            The sourceType to set.
     */
    public void setSourceType(int sourceType) {
	this.sourceType = sourceType;
    }

    /**
     * Methode qui affiche les messages d'erreur
     *
     * @return
     */
    public void showErrorMessage() {
	JOptionPane.showMessageDialog(GenDocDialog.this, errorMessage, Language
				      .getInstance().getText("Erreur_"), JOptionPane.ERROR_MESSAGE);
	GenDocDialog.this.dispose();
    }

    public boolean isInitChapterSelection() {
	return initChapterSelection;
    }

    public void setInitChapterSelection(boolean initChapterSelection) {
	this.initChapterSelection = initChapterSelection;
    }

    public ArrayList<String> getChosenChaptersList() {
	return chosenChaptersList;
    }

    public void setChosenChaptersList(ArrayList<String> chosenChaptersList) {
	this.chosenChaptersList = chosenChaptersList;
    }

    public void loadXSL(Properties cfg_prop, String url_txt) {
	String tempPath = System.getProperties().getProperty("java.io.tmpdir")
	    + File.separator;
	// FO load
	String foPath = "/plugins/docXML/fo/";
	String listeXSL = cfg_prop.getProperty("XSLT_PDF_TRANSFORM_FO");
	String[] xsls = listeXSL.split(",");
	for (int i = 0; i < xsls.length; i++) {
	    String xsl = xsls[i];
	    String xslPath = foPath + xsl;
	    String tmpFilePath = tempPath + "fo/" + xsl;
	    try {
		URL xsltURL = new URL(url_txt + xslPath);
		Tools.writeFile(xsltURL.openStream(), tmpFilePath);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	// Common load
	foPath = "/plugins/docXML/common/";
	listeXSL = cfg_prop.getProperty("XSLT_PDF_TRANSFORM_COMMON");
	xsls = listeXSL.split(",");
	for (int i = 0; i < xsls.length; i++) {
	    String xsl = xsls[i];
	    String xslPath = foPath + xsl;
	    String tmpFilePath = tempPath + "common/" + xsl;
	    try {
		URL xsltURL = new URL(url_txt + xslPath);
		Tools.writeFile(xsltURL.openStream(), tmpFilePath);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	// HTML load
	foPath = "/plugins/docXML/html/";
	listeXSL = cfg_prop.getProperty("XSLT_PDF_TRANSFORM_HTML");
	xsls = listeXSL.split(",");
	for (int i = 0; i < xsls.length; i++) {
	    String xsl = xsls[i];
	    String xslPath = foPath + xsl;
	    String tmpFilePath = tempPath + "html/" + xsl;
	    try {
		URL xsltURL = new URL(url_txt + xslPath);
		Tools.writeFile(xsltURL.openStream(), tmpFilePath);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	// LIB load
	foPath = "/plugins/docXML/lib/";
	listeXSL = cfg_prop.getProperty("XSLT_PDF_TRANSFORM_LIB");
	xsls = listeXSL.split(",");
	for (int i = 0; i < xsls.length; i++) {
	    String xsl = xsls[i];
	    String xslPath = foPath + xsl;
	    String tmpFilePath = tempPath + "lib/" + xsl;
	    try {
		URL xsltURL = new URL(url_txt + xslPath);
		Tools.writeFile(xsltURL.openStream(), tmpFilePath);
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}

	String xsl = "/plugins/docXML/VERSION";
	String temporaryFilePath = System.getProperties().getProperty(
								      "java.io.tmpdir")
	    + File.separator + "VERSION";
	try {
	    URL xsltURL = new URL(url_txt + xsl);
	    Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
