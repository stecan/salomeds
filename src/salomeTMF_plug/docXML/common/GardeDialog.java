/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package  salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.docXML.data.GardePage;
import salomeTMF_plug.docXML.languages.Language;

import com.toedter.calendar.JDateChooser;

/**
 * Classe creant l'IMM permettant a l'utilisateur de remplir un formulaire
 * afin de pouvoir generer a partir de ces informations une page de garde
 * @author  vapu8214
 */
public class GardeDialog extends JDialog implements ActionListener {
    
    JLabel logoLabel;
    JTextField logoTextField;
    JButton logoButton;
    String logoFile = "";
    URL urlLogo;
    
    JLabel entrepriseLabel;
    JTextArea entrepriseArea;
    
    JLabel auteurLabel;
    JTextField auteurTextField;
    
    JLabel emailLabel;
    JTextField emailTextField;
    
    JLabel titreLabel;
    JTextField titreTextField;
    
    JLabel versionLabel;
    JTextField versionTextField;
    
    JDateChooser date;
        
    JLabel infosLabel;
    JTextArea infosArea;
    
    JButton annuler;
    JButton valider;
    
    JLabel champ;
    
    GenDocDialog g;
    
    private String errorMessage = "";
    
    
    /** Creates a new instance of GardeDialog */
    public GardeDialog(GenDocDialog g) throws Exception {
        super(g,true);
        this.g=g;
        logoLabel = new JLabel(Language.getInstance().getText("Logo__"));
        logoTextField = new JTextField(30);
        logoButton = new JButton(Language.getInstance().getText("Choisir"));
        logoButton.addActionListener(this);
        
        entrepriseLabel = new JLabel(Language.getInstance().getText("Entreprise__"));
        entrepriseArea = new JTextArea(3, 30);
        JScrollPane entreprisePane = new JScrollPane(entrepriseArea);
        
        auteurLabel = new JLabel(Language.getInstance().getText("Auteur__"));
        auteurTextField = new JTextField(30);
        auteurTextField.setText(DataModel.getCurrentUser().getFirstNameFromModel()+" "
				+DataModel.getCurrentUser().getLastNameFromModel());
        
        emailLabel = new JLabel(Language.getInstance().getText("Email_"));
        emailTextField = new JTextField(30);
        emailTextField.setText(DataModel.getCurrentUser().getEmailFromModel());
        
        titreLabel = new JLabel(Language.getInstance().getText("Titre_du_document__"));
        titreTextField = new JTextField(30);
        titreTextField.setText(DataModel.getCurrentProject().getNameFromModel());
        
        versionLabel = new JLabel(Language.getInstance().getText("Version_du_document__"));
        versionTextField = new JTextField(30);
                
        infosLabel = new JLabel(Language.getInstance().getText("Informations_complementaires__"));
        infosArea = new JTextArea(2,30);
        JScrollPane infosPane = new JScrollPane(infosArea);
        
        annuler = new JButton(Language.getInstance().getText("Annuler"));
        annuler.addActionListener(this);
        
        valider = new JButton(Language.getInstance().getText("Valider"));
        valider.addActionListener(this);
        
        champ = new JLabel(Language.getInstance().getText("_champ_obligatoire"));
        champ.setFont(new Font(null, Font.ITALIC, 12));
        
        JPanel logoChois = new JPanel(new FlowLayout(FlowLayout.LEFT));
        logoChois.add(logoButton);
        
        JPanel logo1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        logo1.add(logoTextField);
        
        JPanel logo2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        logo2.add(logoLabel);
        
        JPanel pan1 = new JPanel(new BorderLayout());
        pan1.setBorder(BorderFactory.createTitledBorder(""));
        pan1.add(logo2, BorderLayout.NORTH);
        pan1.add(logo1, BorderLayout.CENTER);
        pan1.add(logoChois, BorderLayout.SOUTH);
        
        JPanel entreprise = new JPanel(new FlowLayout(FlowLayout.LEFT));
        entreprise.add(entrepriseLabel);
        
        JPanel entreprise1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        entreprise1.add(entreprisePane);
        
        JPanel ent1 = new JPanel(new BorderLayout());
        ent1.setBorder(BorderFactory.createTitledBorder(""));
        ent1.add(entreprise, BorderLayout.NORTH);
        ent1.add(entreprise1, BorderLayout.CENTER);
        
        JPanel infosSte = new JPanel();
        infosSte.setLayout(new GridLayout(1, 2));
        infosSte.add(pan1);
        infosSte.add(ent1);
        
        JPanel auteur = new JPanel(new FlowLayout(FlowLayout.LEFT));
        auteur.add(auteurLabel);
        
        JPanel auteur1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        auteur1.add(auteurTextField);
        
        JPanel email = new JPanel(new FlowLayout(FlowLayout.LEFT));
        email.add(emailLabel);
        
        JPanel email1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        email1.add(emailTextField);
              
        JPanel aut1 = new JPanel(new BorderLayout());
        aut1.setBorder(BorderFactory.createTitledBorder(""));
        aut1.add(auteur, BorderLayout.NORTH);
        aut1.add(auteur1, BorderLayout.CENTER);
        
        JPanel mail1 = new JPanel(new BorderLayout());
        mail1.setBorder(BorderFactory.createTitledBorder(""));
        mail1.add(email, BorderLayout.NORTH);
        mail1.add(email1, BorderLayout.CENTER);
        
        JPanel infosAuteur = new JPanel();
        infosAuteur.setLayout(new GridLayout(1, 2));
        infosAuteur.add(aut1);
        infosAuteur.add(mail1);
        
        JPanel titre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        titre.add(titreLabel);
        
        JPanel titre1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        titre1.add(titreTextField);
        
        JPanel tit1 = new JPanel(new BorderLayout());
        tit1.setBorder(BorderFactory.createTitledBorder(""));
        tit1.add(titre, BorderLayout.NORTH);
        tit1.add(titre1, BorderLayout.CENTER);
        
        JPanel version = new JPanel(new FlowLayout(FlowLayout.LEFT));
        version.add(versionLabel);
        
        JPanel version1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        version1.add(versionTextField);
        
        JPanel vers1 = new JPanel (new BorderLayout());
        vers1.setBorder(BorderFactory.createTitledBorder(""));
        vers1.add(version, BorderLayout.NORTH);
        vers1.add(version1, BorderLayout.CENTER);
        
        JPanel infosTitre = new JPanel();
        infosTitre.setLayout(new GridLayout(1, 2));
        infosTitre.add(tit1);
        infosTitre.add(vers1);
        
        JLabel dateLabel = new JLabel(Language.getInstance().getText("Date_"));
        JPanel dateLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        dateLabelPanel.add(dateLabel);
        date = new JDateChooser("d MMMMM yyyy", false);
        JPanel datePanel = new JPanel();
        datePanel.setLayout(new BoxLayout(datePanel, BoxLayout.Y_AXIS));
        datePanel.setBorder(BorderFactory.createTitledBorder(""));
        datePanel.add(dateLabelPanel);
        datePanel.add(Box.createRigidArea(new Dimension(1,10)));
        datePanel.add(date);
        
        JPanel infos = new JPanel(new FlowLayout(FlowLayout.LEFT));
        infos.add(infosLabel);
        
        JPanel infos1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        infos1.add(infosPane);
        
        JPanel inf1 = new JPanel(new BorderLayout());
        inf1.setBorder(BorderFactory.createTitledBorder(""));
        inf1.add(infos, BorderLayout.NORTH);
        inf1.add(infos1, BorderLayout.CENTER);
        
        JPanel dateInfos = new JPanel();
        dateInfos.setLayout(new GridLayout(1, 2));
        dateInfos.add(datePanel);
        dateInfos.add(inf1);
        
	/* JPanel endPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
	   endPanel.add(valider);
	   endPanel.add(annuler);*/
        JPanel endPanel = new JPanel();
        endPanel.setLayout(new BoxLayout(endPanel, BoxLayout.X_AXIS));
        endPanel.add(Box.createHorizontalStrut(230));
        endPanel.add(valider);
        endPanel.add(annuler);
        endPanel.add(Box.createHorizontalStrut(100));
        endPanel.add(champ);
                 
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(infosSte);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(infosAuteur);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(infosTitre);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(dateInfos);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(endPanel);
        
        JPanel vide, vide1, vide2, vide3;
        vide = new JPanel();
        vide1 = new JPanel();
        vide2 = new JPanel();
        vide3 = new JPanel();
        
        if (g.getSave()!=null){
            restore(g.getSave());
        }
                 
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        contentPaneFrame.add(vide, BorderLayout.NORTH);
        contentPaneFrame.add(vide1, BorderLayout.EAST);
        contentPaneFrame.add(vide2, BorderLayout.SOUTH);
        contentPaneFrame.add(vide3, BorderLayout.WEST);

        this.setTitle(Language.getInstance().getText("Page_de_garde"));
        //this.setLocation(300,100);
        centerScreen();  
    }
        
    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);         
	this.setVisible(true); 
	requestFocus();
    }    
    
    
    /***************************************************************************************************** 
     * Action Listener
     ****************************************************************************************************/
    
    public void actionPerformed(ActionEvent evt) {
	if (evt.getSource().equals(logoButton)){
	    try {
		logoButtonPerformed();
	    } catch (Exception ex) {
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(annuler)){
	    try {
		annulerPerformed();
	    }catch (Exception ex) {
		Tools.ihmExceptionView(ex);
	    }
	} else if (evt.getSource().equals(valider)){
	    try {
		validerPerformed();
	    } catch (Exception ex) {
		Tools.ihmExceptionView(ex);
	    }
	}
    } 
    
    void logoButtonPerformed() throws Exception {
        JFileChooser fileChooser = new JFileChooser();
	String [] tab={".jpeg", ".jpg", ".png", ".gif"};
	fileChooser.addChoosableFileFilter(new ScriptFileFilter
					   (Language.getInstance().getText("Fichiers____jpeg____jpg___png___gif"),tab));
	int returnVal =  fileChooser.showDialog(GardeDialog.this, Language.getInstance().getText("Selectionner"));
	if (returnVal == JFileChooser.APPROVE_OPTION) {
	    try {
		logoFile = fileChooser.getSelectedFile().getAbsolutePath();
		urlLogo = fileChooser.getSelectedFile().toURL();
		logoTextField.setText(logoFile);
	    } catch (Exception ex) {
		Tools.ihmExceptionView(ex);
	    }
	}
    }
    
    void annulerPerformed() throws Exception {
	GardeDialog.this.dispose();
    }
    
    void validerPerformed() throws Exception {
        GardePage save = new GardePage(logoTextField.getText(), entrepriseArea.getText(), auteurTextField.getText(), 
				       emailTextField.getText(), titreTextField.getText(), versionTextField.getText(), 
				       date.getDate(), infosArea.getText());
        GardeDialog.this.g.setSave(save);     
        if(titreTextField.getText().equals("")){
            JOptionPane.showMessageDialog(
					  GardeDialog.this,
					  Language.getInstance().getText("Vous_devez_entrez_un_titre_pour_le_document"),
					  Language.getInstance().getText("Erreur_"),
					  JOptionPane.ERROR_MESSAGE);
        }else{
            //verif que le fichier donne pour le logo existe
            if (!logoTextField.getText().equals("")){
                File fic = new File(logoTextField.getText());
                boolean estFichier = fic.exists();
                if (!estFichier){
                    JOptionPane.showMessageDialog(
						  GardeDialog.this,
						  Language.getInstance().getText("Vous_devez_entrez_un_nom_de_fichier_existant"),
						  Language.getInstance().getText("Erreur_"),
						  JOptionPane.ERROR_MESSAGE);
                }else{
                    createPage(GardeDialog.this.g);                         
                }
            }else{
                createPage(GardeDialog.this.g);                         
            }
        }
    }
    
    /**
     * Methode quirestaure les elements sauvegardes
     * @param save objet qui sauvegarde les differentes valeurs des champs du formulaire
     */
    public void restore(GardePage save) throws Exception {
        logoTextField.setText(save.getLogo());
        entrepriseArea.setText(save.getEntreprise());
        auteurTextField.setText(save.getAuteur());
        emailTextField.setText(save.getEmail());
        titreTextField.setText(save.getTitre());
        versionTextField.setText(save.getVersion());
        date.setDate(save.getDate());
        infosArea.setText(save.getInfos());
    }
    
    /**
     * Methode qui cree l'element a inserer dans le document Html
     * afin d'obtenir une page de garde
     */
    public void createPage(GenDocDialog g) throws Exception {   
        String urlLogoForTitlePage = null;
        String company = null;
        String version = null;
        String author = null;
        String email = null;
        if (!logoTextField.getText().equals("")) urlLogoForTitlePage = logoTextField.getText();
        if (!entrepriseArea.getText().equals("")) company = entrepriseArea.getText();
        if (!versionTextField.getText().equals("")) version = versionTextField.getText();
        if (!auteurTextField.getText().equals("")) author = auteurTextField.getText();
        if (!emailTextField.getText().equals("")) email = emailTextField.getText();
        g.createTitlePage(urlLogoForTitlePage, company, titreTextField.getText(), version, infosArea.getText(), 
			  date.getDate(), author, email);       
        GardeDialog.this.dispose();        
    }  
    
    /**
     * Methode qui affiche les messages d'erreur
     * @return
     */
    public void showErrorMessage(){
        JOptionPane.showMessageDialog(
				      GardeDialog.this,
				      errorMessage,
				      Language.getInstance().getText("Erreur_"),
				      JOptionPane.ERROR_MESSAGE);
        GardeDialog.this.dispose();
    }
}//Fin de la classe GardeDialog
