/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */


package  salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.export.Contexte;
import salomeTMF_plug.docXML.export.XmlGenerator;
import salomeTMF_plug.docXML.export.Contexte.OutFormat;
import salomeTMF_plug.docXML.languages.Language;



/**
 *
 * @author  vapu8214
 */
public class ExportDialog extends JDialog implements ActionListener,DataConstants {

    // Salome GUI
    protected IPlugObject pIhm = null;

    private  Logger log;
    URL urlBase;
    String url_txt;
    private String fs = System.getProperties().getProperty("file.separator");

    String xmlFile;
    JLabel sauvLabel;
    JTextField sauvTF;
    JButton sauvButton;

    JCheckBox importTestBox;
    JButton testSelection;
    JLabel tousTests;
    boolean selectionDesTests = false;

    JButton valider;
    JButton annuler;
    boolean annule=false;
    JProgressBar progress;

    private boolean initSelection = false;
    private ArrayList<TestList> suiteSelectionList;
    private ArrayList<Family> familySelectionList;
    private ArrayList<Test> testSelectionList;
    private DefaultMutableTreeNode chosenRoot;

    private Vector<JPanel> panelsPluginsForExport = new Vector<JPanel>();
    private Thread exportProcess;

    private JComboBox comboEncoding;
    private Vector<String> codingList ;
    /** Creates a new instance of ExportDialog */
    public ExportDialog(IPlugObject vt) throws Exception {
        super(SalomeTMFContext.getInstance().getSalomeFrame(),true);
        log = Logger.getLogger(getClass());
        pIhm = vt;
        log.debug("try to get the local");
        urlBase =SalomeTMFContext.getInstance().getUrlBase();
        log.debug("succeed");
        String _urlBase = urlBase.toString();
        url_txt = _urlBase.substring(0,_urlBase.lastIndexOf("/"));

        createComponents();
    }

    public void createComponents() {
        codingList= new Vector<String>();
        codingList.add("ISO-8859-15");
        codingList.add("ISO-8859-1");
        codingList.add("UTF-8");
        codingList.add("UTF-16");

        comboEncoding = new JComboBox(codingList);
        comboEncoding.setEditable(false);

        Vector<Extension> listExtXMLPlugin = pIhm.getXMLPrintersExtension();
        int size = listExtXMLPlugin.size();
	for (int i = 0; i < size ; i++){
	    Extension pXMLExt = (Extension) listExtXMLPlugin.elementAt(i);
	    JPFManager pJPFManager =  pIhm.getPluginManager();
	    try {
		XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager.activateExtension(pXMLExt);
		JPanel pPanel = pXMLPrinterPlugin.getExportOptionPanel();
		if (pPanel != null && !panelsPluginsForExport.contains(pPanel)){
		    panelsPluginsForExport.add(pPanel);
		}
	    } catch (Exception e){
		e.printStackTrace();
	    }
	}

        //DataModel.reloadFromBase(false);

        sauvLabel = new JLabel(Language.getInstance().getText("Fichier_de_sauvegarde_"));
        sauvTF = new JTextField(30);
        sauvButton = new JButton(Language.getInstance().getText("Choisir"));
        sauvButton.addActionListener(this);

        importTestBox = new JCheckBox(Language.getInstance().getText("Exporter_uniquement_les_tests"));
        importTestBox.addActionListener(this);
        importTestBox.setSelected(true);
        testSelection = new JButton(Language.getInstance().getText("Selection_des_tests"));
        testSelection.addActionListener(this);

        tousTests = new JLabel(Language.getInstance().getText("_Par_defaut_tous_les_tests_sont_exportes_"));
        tousTests.setFont(new Font(null, Font.ITALIC, 12));

        valider = new JButton(Language.getInstance().getText("OK"));
        valider.addActionListener(this);

        annuler = new JButton(Language.getInstance().getText("Annuler"));
        annuler.addActionListener(this);

        JPanel importTestPanel = new JPanel();
        importTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        importTestPanel.add(importTestBox);

        JPanel selectTestPanel = new JPanel();
        selectTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        selectTestPanel.add(testSelection);
        selectTestPanel.add(tousTests);

        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new BoxLayout(selectPanel, BoxLayout.Y_AXIS));
        selectPanel.setBorder(BorderFactory.createTitledBorder(""));
        selectPanel.add(importTestPanel);
        selectPanel.add(selectTestPanel);

        JPanel sauvLabelPanel = new JPanel(new FlowLayout (FlowLayout.LEFT));
        sauvLabelPanel.add(sauvLabel);
        JPanel sauvTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvTFPanel.add(sauvTF);
        JPanel sauvButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvButtonPanel.add(sauvButton);

        JLabel encodingLabel = new JLabel(Language.getInstance().getText("Encodage_"));

        JPanel encodingPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        encodingPanel.add(encodingLabel);
        encodingPanel.add(comboEncoding);

        JPanel sauvPanel = new JPanel();
        sauvPanel.setLayout(new BoxLayout(sauvPanel, BoxLayout.Y_AXIS));
        sauvPanel.setBorder(BorderFactory.createTitledBorder(""));
        sauvPanel.add(sauvLabelPanel);
        sauvPanel.add(sauvTFPanel);
        sauvPanel.add(sauvButtonPanel);
        sauvPanel.add(Box.createVerticalStrut(5));
        sauvPanel.add(encodingPanel);

        JPanel buttons = new JPanel();
        buttons.add(valider);
        buttons.add(annuler);

        progress = new JProgressBar();
        JPanel progression = new JPanel(new FlowLayout(FlowLayout.CENTER));
        progression.add(progress);

        JPanel commonPanel = new JPanel();
	commonPanel.setLayout(new BoxLayout(commonPanel, BoxLayout.Y_AXIS));
	commonPanel.add(buttons);
	commonPanel.add(Box.createRigidArea(new Dimension(1, 5)));
	commonPanel.add(progression);

        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(Box.createVerticalStrut(10));
        page.add(sauvPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(selectPanel);

        JTabbedPane onglets = new JTabbedPane();
        onglets.addTab(Language.getInstance().getText("Principal"), page);
        /**** Ajout des tabs des plugins ****/
        for (JPanel pPanel : panelsPluginsForExport) {
	    String name = pPanel.getName();
	    onglets.addTab(name, pPanel);
        }

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(onglets, BorderLayout.CENTER);
        contentPaneFrame.add(commonPanel, BorderLayout.SOUTH);

        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent we) {
		    if (exportProcess != null && exportProcess.isAlive()){
                        return;
		    }else {
                        dispose();
		    }
		}
	    });

        setTitle(Language.getInstance().getText("Export_au_format_XML"));
        //setLocation(400,100);
        centerScreen();
    }

    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);
	this.setVisible(true);
	requestFocus();
    }


    /*****************************************************************************************************
     * Action Listener
     ****************************************************************************************************/

    public void actionPerformed(ActionEvent evt) {
        if (exportProcess != null && exportProcess.isAlive()){
	    return;
        }
        if (evt.getSource().equals(valider)){
	    try {
		validerPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
        } else if (evt.getSource().equals(annuler)){
	    try {
		annulerPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
        } else if (evt.getSource().equals(testSelection)){
	    try {
		testSelectionPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
        } else if (evt.getSource().equals(importTestBox)){
	    try {
		importTestBoxPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
        } else if (evt.getSource().equals(sauvButton)){
	    try {
		sauvButtonPerformed();
	    } catch (Exception ex) {
		ex.printStackTrace();
		Tools.ihmExceptionView(ex);
	    }
        }
    }

    void validerPerformed() throws Exception{
        //exporter();
        if (exportProcess == null || !exportProcess.isAlive()){
	    exportProcess = new Thread(new ExportProcess());
	    exportProcess.start();
        }
    }

    void annulerPerformed() throws Exception{
        dispose();
    }

    void testSelectionPerformed() throws Exception {
        TestChooser testChooser = new TestChooser(SalomeTMFPanels.getTestDynamicTree().getRoot(), 
						  ExportDialog.this, initSelection, getChosenRoot(), TEST);
        if (!testChooser.isCancelled()) {
	    setInitSelection(true);
	    setFamilySelectionList(testChooser.getTemporaryFamilyList());
	    setSuiteSelectionList(testChooser.getTemporaryTestListList());
	    setTestSelectionList(testChooser.getTemporaryTestList());
	    setChosenRoot((DefaultMutableTreeNode)testChooser.getChosenTreeModel().getRoot());
        }
    }

    void importTestBoxPerformed() throws Exception {
        if (!importTestBox.isSelected()){
            testSelection.setEnabled(false);
        }else{
            testSelection.setEnabled(true);
        }
    }

    void sauvButtonPerformed() throws Exception {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance().getText("Fichier_XML____xml_"),".xml"));
        int returnVal =  fileChooser.showDialog(ExportDialog.this, Language.getInstance().getText("Selectionner"));
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            xmlFile = fileChooser.getSelectedFile().getAbsolutePath();
            if (xmlFile.indexOf(".")!=-1){
                if (!xmlFile.substring(xmlFile.lastIndexOf(".")).equals(".xml")){
                    xmlFile+=".xml";
                }
            }else{
                xmlFile+=".xml";
            }
            sauvTF.setText(xmlFile);
        }
    }
    /*****************************************************************************************************
     * Methodes
     ****************************************************************************************************/

    class ExportProcess  implements  Runnable {
	public void run() {
	    boolean ok = true;
	    if(sauvTF.getText().equals("")){
                JOptionPane.showMessageDialog(
					      ExportDialog.this,
					      Language.getInstance().getText("Vous_devez_entrez_un_nom_de_fichier"),
					      Language.getInstance().getText("Erreur_"),
					      JOptionPane.ERROR_MESSAGE);
                ok=false;
	    }else{
                try {
		    String saveFileName = sauvTF.getText().trim();
		    String xmlDirName = "";
		    if (saveFileName.lastIndexOf(fs) != -1) {
			xmlDirName = saveFileName.substring(0, saveFileName.lastIndexOf(fs));
		    } else {
			File saveFile = new File(saveFileName);
			xmlDirName = saveFile.getParent();
		    }

		    Document doc;
		    Contexte pContexte = new Contexte(xmlDirName, null, (String)comboEncoding.getSelectedItem(), 
						      OutFormat.XML);
		    XmlGenerator generator = new XmlGenerator(pIhm, pContexte);
		    generator.addDocType = false;
		    setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
		    progress.setIndeterminate(true);
		    if (!isInitSelection()){
			doc = generator.toDocumentDyna( null, null);
		    }else{
			doc = generator.toDocument( getFamilySelectionList(), getSuiteSelectionList(), getTestSelectionList());
		    }
		    generator.documentToXML(doc, saveFileName);

		    progress.setVisible(false);
		    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		    if (ok && !annule){
			JOptionPane.showMessageDialog( ExportDialog.this,
						       Language.getInstance().
						       getText("L_export_au_format_xml_s_est_termine_avec_succes"),
						       Language.getInstance().getText("Information"),
						       JOptionPane.INFORMATION_MESSAGE);
		    }
		    ExportDialog.this.dispose();
                }catch (Exception e){
		    progress.setVisible(false);
		    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		    Tools.ihmExceptionView(e);
		    ok=false;
		    ExportDialog.this.dispose();
                }
	    }

	}
    }
    /*****************************************************************************************************
     * Selecteur
     ****************************************************************************************************/

    /**
     * @return Returns the chosenRoot.
     */
    public DefaultMutableTreeNode getChosenRoot() {
        return chosenRoot;
    }
    /**
     * @param chosenRoot The chosenRoot to set.
     */
    public void setChosenRoot(DefaultMutableTreeNode chosenRoot) {
        this.chosenRoot = chosenRoot;
    }

    /**
     * @return Returns the familySelectionList.
     */
    public ArrayList<Family> getFamilySelectionList() {
        return familySelectionList;
    }

    /**
     * @param familySelectionList The familySelectionList to set.
     */
    public void setFamilySelectionList(ArrayList<Family> familySelectionList) {
        if (familySelectionList != null) this.familySelectionList = familySelectionList;
    }

    /**
     * @return Returns the suiteSelectionList.
     */
    public ArrayList<TestList> getSuiteSelectionList() {
        return suiteSelectionList;
    }

    /**
     * @param suiteSelectionList The suiteSelectionList to set.
     */
    public void setSuiteSelectionList(ArrayList<TestList> suiteSelectionList) {
        if (suiteSelectionList != null) this.suiteSelectionList = suiteSelectionList;
    }

    /**
     * @return Returns the testSelectionList.
     */
    public ArrayList<Test> getTestSelectionList() {
        return testSelectionList;
    }

    /**
     * @param testSelectionList The testSelectionList to set.
     */
    public void setTestSelectionList(ArrayList<Test> testSelectionList) {
        if (testSelectionList != null) this.testSelectionList = testSelectionList;
    }

    /**
     * @return Returns the initSelection.
     */
    public boolean isInitSelection() {
        return initSelection;
    }

    /**
     * @param initSelection The initSelection to set.
     */
    public void setInitSelection(boolean initSelection) {
        this.initSelection = initSelection;
    }
}
