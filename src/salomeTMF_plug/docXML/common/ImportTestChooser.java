/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.dom4j.Document;
import org.dom4j.Element;
import org.objectweb.salome_tmf.api.data.AutomaticTestWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.ManualTestWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;
import org.objectweb.salome_tmf.ihm.models.TreeRenderer;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.docXML.importxml.ImportDialog;
import salomeTMF_plug.docXML.languages.Language;


public class ImportTestChooser extends JDialog implements DataConstants{

    /**
     * Modele de l'arbre des tests
     */
    protected TestTreeModel testTreeModel;

    /**
     * Modele de l'arbre des tests selectionnes
     */
    protected TestTreeModel chosenTreeModel;

    /**
     * l'arbre des tests choisis
     */
    JTree chosenTree;
        
    /**
     * l'arbre des tests
     */
    JTree testTree;

    /**
     * Noeud selectionne dans l'arbre des tests choisis
     */
    DefaultMutableTreeNode chosenSelectedNode;
        
    /**
     * Noeud selectionne dans l'arbre des tests
     */
    DefaultMutableTreeNode testSelectedNode;
        
    /**
     * Racine de l'arbre temporaire des tests choisis
     */
    DefaultMutableTreeNode temporaryChosenRootNode;
        
    /**
     * Racine de l'arbre final des tests choisis
     */
    DefaultMutableTreeNode realChosenRoot;

    /**
     * Liste des tests selectionnes
     */
    ArrayList temporaryTestList;
        
    /**
     * Liste des suites selectionnees
     */
    ArrayList temporaryTestListList;
        
    /**
     * Liste des familles selectionnees
     */
    ArrayList temporaryFamilyList;
        
    ArrayList testSelectedNodes;
        
    ArrayList chosenSelectedNodes;
        
    ImportDialog idialog;
        
    CreateProjectDialog cdialog;
        
    private String errorMessage = "";
    
    public ImportTestChooser(JDialog i, Document doc, boolean initSelection) throws Exception {
        super(i, true);
        
        if (i instanceof ImportDialog){
            idialog = (ImportDialog)i;
        }else{
            cdialog = (CreateProjectDialog)i;
        }
        temporaryTestList = new ArrayList();
	temporaryTestListList = new ArrayList();
	temporaryFamilyList = new ArrayList();
                
	chosenSelectedNodes = new ArrayList();
	testSelectedNodes = new ArrayList();
                
	DynamicTree testDynamicTree = createTestDynamicTree(doc);
                
	realChosenRoot = testDynamicTree.getRoot();
                
	TreeRenderer chosenRenderer = new TreeRenderer();
	TreeRenderer testRenderer = new TreeRenderer();         
                
	if (initSelection){
	    if (idialog!=null){
		temporaryChosenRootNode = new DefaultMutableTreeNode(idialog.getChosenRoot().getUserObject());
	    }else{
		temporaryChosenRootNode = new DefaultMutableTreeNode(cdialog.getChosenRoot().getUserObject());
	    }
	}else{
	    temporaryChosenRootNode = new DefaultMutableTreeNode(testDynamicTree.getRoot().getUserObject());
	}
                
	chosenSelectedNode = temporaryChosenRootNode;
                
	chosenTree = new JTree();
	chosenTreeModel = new TestTreeModel(temporaryChosenRootNode, chosenTree, null);
	chosenTree.setModel(chosenTreeModel);
                
	if (initSelection){
	    if (idialog!=null){
		initTemporaryTree(idialog.getChosenRoot());
	    }else{
		initTemporaryTree(cdialog.getChosenRoot());
	    }
	}else{
	    initTemporaryTree(testDynamicTree.getRoot());
	}
                
	chosenTree.setCellRenderer(chosenRenderer);
	chosenTree.addTreeSelectionListener(new TreeSelectionListener() {
		public void valueChanged(TreeSelectionEvent e) {
		    try {
			chosenSelectedNodes.clear();
			TreePath[] pathTab = chosenTree.getSelectionPaths();
			if (pathTab != null) {
			    for (int i = 0; i < pathTab.length; i++) {
				chosenSelectedNodes.add(pathTab[i].getLastPathComponent());
			    }
			}
		    } catch (Exception ex) {
			Tools.ihmExceptionView(ex);
		    }
		}
	    });
	testTree = new JTree(testTreeModel);
	testTreeModel = new TestTreeModel(testDynamicTree.getRoot(), testTree, null); 
	testTree.setModel(testTreeModel);
                
	testTree.setCellRenderer(testRenderer);
	testTree.addTreeSelectionListener(new TreeSelectionListener() {
		public void valueChanged(TreeSelectionEvent e) {
		    try {
			testSelectedNodes.clear();
			TreePath[] pathTab = testTree.getSelectionPaths();
			if (pathTab != null) {
			    for (int i = 0; i < pathTab.length; i++) {
				testSelectedNodes.add(pathTab[i].getLastPathComponent());
			    }
			}
		    } catch (Exception ex) {
			Tools.ihmExceptionView(ex);
		    }
		}
	    });
                
	JButton addButton = new JButton(">");
	addButton.setToolTipText(Language.getInstance().getText("Ajouter_a_la_selection"));
	addButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			for (int i = 0; i < testSelectedNodes.size(); i ++) {
			    Object nodeValue = ((DefaultMutableTreeNode)testSelectedNodes.get(i)).getUserObject(); 
			    testSelectedNode = ((DefaultMutableTreeNode)testSelectedNodes.get(i));
			    if (nodeValue instanceof Family) {
				addNodeToNode(testSelectedNode, temporaryChosenRootNode);                                                
			    } else if (nodeValue instanceof TestList) {
				DefaultMutableTreeNode familyNode = 
				    findFamilyNodeInChosenTree(((TestList)nodeValue).getFamilyFromModel().getNameFromModel());
				if (familyNode != null) {
				    addNodeToNode(testSelectedNode, familyNode);
				} else {
				    DefaultMutableTreeNode newFamilyNode = 
					addObject(temporaryChosenRootNode, ((TestList)nodeValue).getFamilyFromModel(), true);
				    addNodeToNode(testSelectedNode, newFamilyNode);
				}
			    } else if (nodeValue instanceof Test){
				DefaultMutableTreeNode testListNode = 
				    findTestListNodeInChosenTree(((Test)nodeValue).getTestListFromModel().getNameFromModel(), 
								 ((Test)nodeValue).getTestListFromModel().getFamilyFromModel().
								 getNameFromModel());
				if (testListNode != null) {
				    addNodeToNode(testSelectedNode, testListNode);
				} else {
				    DefaultMutableTreeNode familyNode = 
					findFamilyNodeInChosenTree(((Test)nodeValue).getTestListFromModel().
								   getFamilyFromModel().getNameFromModel());
				    if (familyNode != null) {
					DefaultMutableTreeNode newListNode = 
					    addObject(familyNode, ((Test)nodeValue).getTestListFromModel(), true);
					addNodeToNode(testSelectedNode, newListNode);
				    }else {
					DefaultMutableTreeNode newFamilyNode = 
                                                    addObject(temporaryChosenRootNode, 
                                                    ((Test)nodeValue).getTestListFromModel().getFamilyFromModel(), true);
					DefaultMutableTreeNode newList = 
					    addObject(newFamilyNode, ((Test)nodeValue).getTestListFromModel() , true);
					addNodeToNode(testSelectedNode, newList);
				    }
				}
			    }
			}
		    } catch (Exception ex) {
			ex.printStackTrace();
			errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
			showErrorMessage();
		    }
		}
	    });             

	JButton removeButton = new JButton("<");
	removeButton.setToolTipText(Language.getInstance().getText("Retirer_de_la_selection"));
	removeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			int i = chosenSelectedNodes.size() - 1;
			boolean continu = true;
			while(i >=0 && continu) {
			    Object nodeValue = ((DefaultMutableTreeNode)chosenSelectedNodes.get(i)).getUserObject();
			    DefaultMutableTreeNode node = (DefaultMutableTreeNode)chosenSelectedNodes.get(i);
			    if (nodeValue instanceof Campaign || nodeValue instanceof Execution || 
				nodeValue instanceof Family || nodeValue instanceof TestList || nodeValue instanceof Test){
				((DefaultTreeModel)chosenTree.getModel()).removeNodeFromParent(node);
				removeFromModel(node);
				i = chosenSelectedNodes.size() - 1;
			    }else{
				continu = false;
			    }
			}
		    } catch (Exception ex) {
			ex.printStackTrace();
			errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
			showErrorMessage();
		    }
		}
	    });             


	JPanel buttonSet = new JPanel();
	buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
	buttonSet.add(addButton);
	buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
	buttonSet.add(removeButton);
                
	JScrollPane chosenScrollPane = new JScrollPane(chosenTree);
	chosenScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Selection")));
	chosenScrollPane.setPreferredSize(new Dimension(300,450));
                
	JScrollPane testScrollPane = new JScrollPane(testTree);
	testScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Tests")));
	testScrollPane.setPreferredSize(new Dimension(300,450));
                
	JPanel windowPanel = new JPanel();
	windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
	windowPanel.add(testScrollPane);
	windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
	windowPanel.add(buttonSet);
	windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
	windowPanel.add(chosenScrollPane);

	JButton validate = new JButton(Language.getInstance().getText("Valider"));
	validate.setToolTipText(Language.getInstance().getText("Valider"));
	validate.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			updateList();
			if (temporaryTestList.size()==0){
			    JOptionPane.
				showMessageDialog(
						  ImportTestChooser.this,
						  Language.getInstance().
						  getText("Vous_devez_selectionner_au_moins_un_test_a_importer"),
						  Language.getInstance().getText("Erreur_"),
						  JOptionPane.ERROR_MESSAGE);
			} else {
			    if (idialog!=null){
				ImportTestChooser.this.idialog.setInitSelection(true);
				if (temporaryFamilyList != null) {
				    idialog.setFamilySelectionList(temporaryFamilyList);
				}       
				if (temporaryTestListList != null) {
				    idialog.setSuiteSelectionList(temporaryTestListList);
				}
				if (temporaryTestList != null) {
				    idialog.setTestSelectionList(temporaryTestList);
				}
				idialog.setChosenRoot((DefaultMutableTreeNode) chosenTreeModel.getRoot());
				idialog.setSelectionDesTests(true);
			    }else{
				ImportTestChooser.this.cdialog.setInitSelection(true);
				if (temporaryFamilyList != null) {
				    cdialog.setFamilySelectionList(temporaryFamilyList);
				}
				if (temporaryTestListList != null) {
				    cdialog.setSuiteSelectionList(temporaryTestListList);
				}
				if (temporaryTestList != null) {
				    cdialog.setTestSelectionList(temporaryTestList);
				}
				cdialog.setChosenRoot((DefaultMutableTreeNode) chosenTreeModel.getRoot());
				cdialog.setSelectionDesTests(true);
			    }
			    ImportTestChooser.this.dispose();
			}
		    } catch (Exception ex) {
			ex.printStackTrace();
			errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
			showErrorMessage();
		    }
		}
	    });
                
	JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
	cancel.setToolTipText(Language.getInstance().getText("Annuler"));
	cancel.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
			ImportTestChooser.this.dispose();
		    } catch (Exception ex) {
			Tools.ihmExceptionView(ex);
		    }
		}
	    });             
                
	JPanel secondButtonSet = new JPanel();
	secondButtonSet.add(validate);
	secondButtonSet.add(cancel);
                
	JPanel center = new JPanel();
	center.add(windowPanel);
                
	JPanel page = new JPanel();
	page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
	page.add(center);
	page.add(secondButtonSet);
                
	Container contentPaneFrame = this.getContentPane();
	contentPaneFrame.add(page, BorderLayout.CENTER);
	this.setTitle(Language.getInstance().getText("Ajouter_des_tests_a_la_selection"));
	//this.setLocation(300,200);
	centerScreen();
    }
        
    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);         
	this.setVisible(true); 
	requestFocus();
    }

    /**
     * @param doc
     * @return
     */
    private DynamicTree createTestDynamicTree(Document doc) throws Exception {
        DynamicTree testDynamicTree = new DynamicTree(Language.getInstance().getText("Plan_de_tests"), FAMILY);
        DefaultMutableTreeNode rootNode = testDynamicTree.getRoot();
        List familyList = doc.selectNodes("//Famille");
        Iterator itFamille = familyList.iterator();
        while (itFamille.hasNext()){
            Element familyElem = (Element)itFamille.next();
            String familyName = familyElem.elementText("Nom");
            FamilyWrapper f = new FamilyWrapper();
            f.setName(familyName);
            Family family = new Family(f);
            DefaultMutableTreeNode familyNode = testDynamicTree.addObject(rootNode, family, false);
            temporaryFamilyList.add(family);
            List suiteList = familyElem.selectNodes(".//SuiteTest");
            Iterator itSuite = suiteList.iterator();
            while (itSuite.hasNext()){
                Element suiteElem = (Element)itSuite.next();
                String suiteName = suiteElem.elementText("Nom");
                SuiteWrapper s = new SuiteWrapper();
                s.setName(suiteName);
                TestList sList = new TestList(s);
                DefaultMutableTreeNode suiteNode = testDynamicTree.addObject(familyNode, sList, false);
                temporaryTestListList.add(sList);
                family.addTestListInModel(sList);
                //sList.setFamily(family);
                List testList = suiteElem.selectNodes(".//Test");
                Iterator itTest = testList.iterator();
                while (itTest.hasNext()){
                    Element testElem = (Element)itTest.next();            
                    String testName = testElem.elementText("Nom");
                    if (testElem.element("TestAuto") == null){
                        ManualTestWrapper t = new ManualTestWrapper();
                        t.setName(testName);
                        ManualTest mTest = new ManualTest(t);
                        testDynamicTree.addObject(suiteNode, mTest, false);
                        temporaryTestList.add(mTest);
                        sList.addTestInModel(mTest);
                        //mTest.setTestList(sList);
                    }else{
                        AutomaticTestWrapper a = new AutomaticTestWrapper();
                        a.setName(testName);
                        AutomaticTest aTest = new AutomaticTest(a);
                        testDynamicTree.addObject(suiteNode, aTest);
                        temporaryTestList.add(aTest);
                        sList.addTestInModel(aTest);
                        //aTest.setTestList(sList);
                    }
                }
            }
        }
        return testDynamicTree;
    }
    
    /**
     * Methode qui initialise l'arbre temporaire a partir de la racine passe
     * en parametre. 
     * @param root la racine du nouvel arbre temporaire
     */
    public void initTemporaryTree(DefaultMutableTreeNode root) throws Exception {
	for (int i = 0 ; i < root.getChildCount(); i ++) {
	    addNodeToNode((DefaultMutableTreeNode)root.getChildAt(i), temporaryChosenRootNode);     
	}
    } // Fin de la methode initTemporaryTree/1
        
    /**
     * Methode Recursive qui ajoute un noeud a un autre noeud. Tous les fils du 
     * noeud a ajouter sont aussi ajouter recursivement. 
     * @param nodeToBeAdded le noeud a ajouter
     * @param nodeToReceive le noeud qui reçoit
     */
    private void addNodeToNode(DefaultMutableTreeNode nodeToBeAdded, DefaultMutableTreeNode nodeToReceive) throws Exception {
	DefaultMutableTreeNode newNode = null;
	for (int j=0; j < nodeToReceive.getChildCount(); j++) {
	    if (((DefaultMutableTreeNode)nodeToReceive.getChildAt(j)).getUserObject().equals(nodeToBeAdded.getUserObject())) {
		newNode = (DefaultMutableTreeNode)nodeToReceive.getChildAt(j); 
	    } 
	}
	if (newNode == null) {
	    newNode = new DefaultMutableTreeNode(nodeToBeAdded.getUserObject());
	    chosenTreeModel.insertNodeInto(newNode, nodeToReceive, nodeToReceive.getChildCount());
	    chosenTree.scrollPathToVisible(new TreePath(newNode.getPath()));
	    if (nodeToBeAdded.getUserObject() instanceof Family) {
		temporaryFamilyList.add(nodeToBeAdded.getUserObject());
	    } else if (nodeToBeAdded.getUserObject() instanceof TestList) {
		temporaryTestListList.add(nodeToBeAdded.getUserObject());
	    } else if (nodeToBeAdded.getUserObject() instanceof Test) {
		temporaryTestList.add(nodeToBeAdded.getUserObject());
	    }
	}
	for (int i = 0; i < nodeToBeAdded.getChildCount(); i++) {
	    addNodeToNode((DefaultMutableTreeNode)nodeToBeAdded.getChildAt(i), newNode);
	}
    } // Fin de la methode addNodeToNode/2
        
    /**
     * Methode qui retourne le noeud correspondant a une famille si le nom passe
     * en parametre est le nom d'une famille presente dans l'arbre des selections 
     * @param familyName un nom
     * @return le noeud correspondant a une famille si le nom passe
     * en parametre est le nom d'une famille presente dans l'arbre des selections,
     * <code>null</code> sinon.
     */     
    public DefaultMutableTreeNode findFamilyNodeInChosenTree(String familyName) throws Exception {
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)chosenTreeModel.getRoot();
	for (int j = 0; j < root.getChildCount(); j++) {
	    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(j);
	    if (familyNode.getUserObject() instanceof Family && 
		((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
		return familyNode;
	    }
	}
	return null;
    } // Fin de la methode findFamilyNode/1

    /**
     * Methode qui retourne le noeud correspondant a une suite si le nom passe
     * en parametre est le nom d'une suite presente dans l'arbre des selections 
     * @param testListName un nom
     * @return le noeud correspondant a une famille si le nom passe
     * en parametre est le nom d'une famille presente dans l'arbre des selections,
     * <code>null</code> sinon. 
     */     
    public DefaultMutableTreeNode findTestListNodeInChosenTree(String testListName, String familyName) throws Exception {
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)chosenTreeModel.getRoot();
	for (int j = 0; j < root.getChildCount(); j++) {
	    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(j);
	    if (familyNode.getUserObject() instanceof Family && 
		((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
		for (int k = 0; k < familyNode.getChildCount(); k ++) {
		    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode)familyNode.getChildAt(k);
		    if (testListNode.getUserObject() instanceof TestList && 
			((TestList)testListNode.getUserObject()).getNameFromModel().equals(testListName)) {
			return testListNode;
		    }
		}
	    }
	}
	return null;
    } // Fin de la methode findFamilyNode/1
        
    /**
     * Methode d'ajout d'un noeud dans l'arbre sous le parent.
     * @param parent le parent 
     * @param child le noeud a ajouter
     * @param shouldBeVisible visible ou non
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, 
					    Object child, 
					    boolean shouldBeVisible) throws Exception {
                
	DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
	if (parent == null) {
	    parent = temporaryChosenRootNode;
	}
	// Insertion du noeud
	chosenTreeModel.insertNodeInto(childNode, parent, parent.getChildCount());
                        
	// on s'assure que le noeud est visible
	if (shouldBeVisible) {
	    chosenTree.scrollPathToVisible(new TreePath(childNode.getPath()));
	}
	return childNode;
    } // Fin de la classe addObject/3
        
    /**
     * Supprime du modele les elements retires de la selection.
     * @param node le noeud a partir duquel on retire les elements
     */
    private void removeFromModel(DefaultMutableTreeNode node) throws Exception {
	for (int i = 0; i < node.getChildCount(); i++) {
	    removeFromModel((DefaultMutableTreeNode)node.getChildAt(i));
	}
	if (node.getUserObject() instanceof Family) {
	    temporaryFamilyList.remove(node.getUserObject());
	} else if (node.getUserObject() instanceof TestList) {
	    temporaryTestListList.remove(node.getUserObject());
	} else if (node.getUserObject() instanceof Test) {
	    temporaryTestList.remove(node.getUserObject());
	}
    } // Fin de la methode removeFromModel/1
    
    public void updateList() throws Exception {
	temporaryTestList = new ArrayList();
	temporaryTestListList = new ArrayList();
	temporaryFamilyList = new ArrayList();      
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)chosenTreeModel.getRoot();
	for (int j = 0; j < root.getChildCount(); j++) {
	    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(j);
	    if (familyNode.getUserObject() instanceof Family){
		temporaryFamilyList.add(familyNode.getUserObject());
		for (int k = 0; k < familyNode.getChildCount(); k ++) {
		    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode)familyNode.getChildAt(k);
		    if (testListNode.getUserObject() instanceof TestList) {
			temporaryTestListList.add(testListNode.getUserObject());
			for(int l=0;l<testListNode.getChildCount();l++){
			    DefaultMutableTreeNode testNode = (DefaultMutableTreeNode)testListNode.getChildAt(l);
			    if (testNode.getUserObject() instanceof Test){
				temporaryTestList.add(testNode.getUserObject());
			    }
			}
		    }
		}   
	    }   
	}
    } 
        
    /**
     * Methode qui affiche les messages d'erreur
     * @return
     */
    public void showErrorMessage(){
        JOptionPane.showMessageDialog(
				      ImportTestChooser.this,
				      errorMessage,
				      Language.getInstance().getText("Erreur_"),
				      JOptionPane.ERROR_MESSAGE);
        ImportTestChooser.this.dispose();
    }
}
