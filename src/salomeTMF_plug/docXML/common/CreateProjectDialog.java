/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.docXML.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.tree.DefaultMutableTreeNode;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.admin.Administration;
import org.objectweb.salome_tmf.ihm.admin.models.UserListRenderer;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.DocXMLPlugin;
import salomeTMF_plug.docXML.importxml.ImportXML;
import salomeTMF_plug.docXML.importxml.ImportXML2;
import salomeTMF_plug.docXML.languages.Language;


public class CreateProjectDialog extends JDialog{

    /**
     * Champ pour recuperer le nom du projet
     */
    private JTextField projetNameTextField;

    /**
     * Modele de donnees pour la liste des utilisateurs pouvant etre admin
     */
    private DefaultComboBoxModel comboModel;

    /**
     * La description du projet
     */
    private JTextPane descriptionArea;

    /**
     * La liste des utilisateurs pouvant etre admin
     */
    JComboBox adminNameComboBox;

    JLabel newProjectNameLabel;

    JScrollPane descriptionScrollPane;

    String errorMessage = "";

    private JCheckBox importCampBox;
    JButton testSelection;
    JLabel tousTests;
    boolean selectionDesTests = false;

    boolean initSelection = false;
    ArrayList<TestList> suiteSelectionList;
    ArrayList<Family> familySelectionList;
    ArrayList<Test> testSelectionList;
    DefaultMutableTreeNode chosenRoot;

    String xmlFile;
    JLabel sauvLabel;
    JTextField sauvTF;
    JButton sauvButton;

    boolean recupAttachPb = false;
    boolean majAttachPb = false;

    Project newProject;

    ImportXML traitement;
    ImportXML2 traitement2;

    AdminVTData pAdminVTData;
    IPlugObject pIPlugObject;

    Vector<JPanel> panelsPluginsForImport = new Vector<JPanel>();

    public CreateProjectDialog(IPlugObject iPlugObject) throws Exception {
        super(Administration.ptrFrame, true);
        pAdminVTData = Administration.pAdminVTData;
        this.pIPlugObject = iPlugObject;

        Vector<Extension> listExtXMLPlugin = pIPlugObject.getXMLPrintersExtension();
        for (Extension pXMLExt : listExtXMLPlugin) {
	    JPFManager pJPFManager =  pIPlugObject.getPluginManager();
	    try {
		XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager.activateExtension(pXMLExt);
		JPanel pPanel = pXMLPrinterPlugin.getImportOptionPanel();
		if (pPanel != null && !panelsPluginsForImport.contains(pPanel)){
		    panelsPluginsForImport.add(pPanel);
		}
	    } catch (Exception e){
		e.printStackTrace();
	    }
	}

        suiteSelectionList = new ArrayList<TestList>();
        familySelectionList = new ArrayList<Family>();
        testSelectionList = new ArrayList<Test>();

        projetNameTextField = new JTextField(20);
        comboModel = new DefaultComboBoxModel();
        descriptionArea = new JTextPane();
        adminNameComboBox = new JComboBox(comboModel);
        // Partie superieure
        JLabel adminNameLabel = new JLabel(Language.getInstance().getText("Administrateur_du_projet"));

        adminNameComboBox.setRenderer(new UserListRenderer());
        for (int i = 0; i < pAdminVTData.getAllUsersCountFromModel(); i ++) {
            comboModel.addElement(pAdminVTData.getAllUsersFromModel().get(i));
        }

        JPanel adminPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        adminPanel.add(adminNameLabel);
        adminPanel.add(adminNameComboBox);
        adminPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        JLabel newProjectLabel = new JLabel(Language.getInstance().getText("Nouveau_Projet"));
        newProjectLabel.setFont(new Font(null,Font.BOLD,18));
        newProjectNameLabel = new JLabel(Language.getInstance().getText("Nom_du_nouveau_projet_"));


        JPanel newProjectNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        newProjectNamePanel.add(newProjectNameLabel);
        newProjectNamePanel.add(projetNameTextField);

        descriptionArea.setPreferredSize(new Dimension(100,150));
        descriptionScrollPane = new JScrollPane(descriptionArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
						JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),
									 Language.getInstance().getText("Description")));

        JPanel newProjectPanel = new JPanel();
        newProjectPanel.setLayout(new BoxLayout(newProjectPanel, BoxLayout.Y_AXIS));
        newProjectPanel.add(newProjectLabel);
        newProjectPanel.add(newProjectNamePanel);
        newProjectPanel.add(descriptionScrollPane);
        newProjectPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        importCampBox = new JCheckBox(Language.getInstance().getText("Importer_les_campagnes"));
        importCampBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    try {
                        if (importCampBox.isSelected()){
			    testSelection.setEnabled(false);
                        }else{
			    testSelection.setEnabled(true);
                        }
		    } catch (Exception ex) {
                        ex.printStackTrace();
                        Tools.ihmExceptionView(ex);
		    }
		}
	    });
        importCampBox.setSelected(false);

        testSelection = new JButton(Language.getInstance().getText("Selection_des_tests"));
        testSelection.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if (xmlFile==null || xmlFile.equals("")){
			JOptionPane.
			    showMessageDialog(CreateProjectDialog.this,
					      Language.getInstance().
					      getText("Avant_de_selectionner_les_tests_a_importer_veuillez_indiquer_le_fichier_XML_a_utiliser"),
					      Language.getInstance().getText("Erreur_"),
					      JOptionPane.ERROR_MESSAGE);
		    }else{
			Document doc=null;
			try{
			    doc = xmlParser(xmlFile);
			}catch (Exception ex){
			    ex.printStackTrace();
			    errorMessage+=Language.getInstance().getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
			    showErrorMessage();
			}
			try {
			    if (initSelection){
                                new ImportTestChooser(CreateProjectDialog.this, doc, true);
			    }else{
                                new ImportTestChooser(CreateProjectDialog.this, doc, false);
			    }
			} catch (Exception ex) {
			    ex.printStackTrace();
			    errorMessage+=Language.getInstance().getText("Probleme_lors_de_la_selection_des_tests");
			    showErrorMessage();
			}
		    }
		}
	    });
        tousTests = new JLabel(Language.getInstance().getText("_Par_defaut_tous_les_tests_sont_importes_"));
        tousTests.setFont(new Font(null, Font.ITALIC, 12));

        JPanel importTestPanel = new JPanel();
        importTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        importTestPanel.add(importCampBox);

        JPanel selectTestPanel = new JPanel();
        selectTestPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        selectTestPanel.add(testSelection);
        selectTestPanel.add(tousTests);

        JPanel selectPanel = new JPanel();
        selectPanel.setLayout(new BoxLayout(selectPanel, BoxLayout.Y_AXIS));
        selectPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        selectPanel.add(importTestPanel);
        selectPanel.add(selectTestPanel);

        sauvLabel = new JLabel(Language.getInstance().getText("Fichier_XML_"));
        sauvTF = new JTextField(40);
        sauvButton = new JButton(Language.getInstance().getText("Choisir"));
        sauvButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    try {
                        JFileChooser fileChooser = new JFileChooser();
                        fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance().
										getText("Fichier_XML____xml_"),".xml"));
                        int returnVal =  fileChooser.showDialog(CreateProjectDialog.this, 
								Language.getInstance().getText("Selectionner"));
                        if (returnVal == JFileChooser.APPROVE_OPTION) {
			    xmlFile = fileChooser.getSelectedFile().getAbsolutePath();
			    if (xmlFile.indexOf(".")!=-1){
				if (!xmlFile.substring(xmlFile.lastIndexOf(".")).equals(".xml")){
				    xmlFile+=".xml";
				}
			    }else{
				xmlFile+=".xml";
			    }
			    sauvTF.setText(xmlFile);
                        }
		    } catch (Exception ex) {
                        ex.printStackTrace();
                        Tools.ihmExceptionView(ex);
		    }
		}
	    });

        JPanel sauvLabelPanel = new JPanel(new FlowLayout (FlowLayout.LEFT));
        sauvLabelPanel.add(sauvLabel);
        JPanel sauvTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvTFPanel.add(sauvTF);
        JPanel sauvButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sauvButtonPanel.add(sauvButton);

        JPanel sauvPanel = new JPanel();
        sauvPanel.setLayout(new BoxLayout(sauvPanel, BoxLayout.Y_AXIS));
        sauvPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        sauvPanel.add(sauvLabelPanel);
        sauvPanel.add(sauvTFPanel);
        sauvPanel.add(sauvButtonPanel);

        JButton validation = new JButton(Language.getInstance().getText("Valider"));
        validation.setToolTipText(Language.getInstance().getText("Valider"));
        validation.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    try {
                        if (projetNameTextField.getText().trim() != null && !projetNameTextField.getText().trim().equals("")) {
			    if (!pAdminVTData.containsProjectInModel(projetNameTextField.getText().trim())) {
				if (adminNameComboBox.getSelectedItem() != null) {
				    if (!sauvTF.getText().equals("")){
					Document doc=null;
					try{
					    doc = xmlParser(xmlFile);
					}catch (Exception ex){
					    ex.printStackTrace();
					    errorMessage+=Language.getInstance().
						getText("Probleme_lors_de_l_import_des_donnees_du_document_XML");
					    showErrorMessage();
					}
					newProject = new Project(projetNameTextField.getText().trim(), 
								 descriptionArea.getText());
					//newProject.setName(projetNameTextField.getText().trim());
					newProject.setAdministratorInModel((User)adminNameComboBox.getSelectedItem());
					//newProject.setDescription(descriptionArea.getText());
					if (!DocXMLPlugin.importInV2) {
					    traitement = new ImportXML(CreateProjectDialog.this, doc, 
								       familySelectionList, suiteSelectionList, 
								       testSelectionList, pAdminVTData, pIPlugObject);
					    traitement.setDirXml(xmlFile.substring(0, xmlFile.
										   lastIndexOf(System.getProperties().
											       getProperty("file.separator"))));
					    traitement.setSelectionDesTests(selectionDesTests);
					    traitement.setImportOnlyTests(!importCampBox.isSelected());
					    traitement.importInNewProject();
					} else {
					    traitement2 = new ImportXML2(CreateProjectDialog.this, doc, 
									 familySelectionList, suiteSelectionList, 
									 testSelectionList, pAdminVTData, pIPlugObject);
					    traitement2.setDirXml(xmlFile.substring(0, xmlFile.
										    lastIndexOf(System.getProperties().
												getProperty("file.separator"))));
					    traitement2.setInitSelection(isInitSelection());
					    traitement2.setImportCampaign(importCampBox.isSelected());
					    traitement2.importInNewProject();
					}
					if (!DocXMLPlugin.importInV2 && !traitement.isAnnule() || 
					    DocXMLPlugin.importInV2 && !traitement2.isAnnule()){
					    if (errorMessage.equals("")){
						JOptionPane.showMessageDialog( CreateProjectDialog.this,
									       Language.
									       getInstance().
									       getText("L_import_s_est_terminee_avec_succes"),
									       Language.getInstance().getText("Information"),
									       JOptionPane.INFORMATION_MESSAGE);
						CreateProjectDialog.this.dispose();
					    }else{
						JOptionPane.showMessageDialog( CreateProjectDialog.this,
									       errorMessage,
									       Language.getInstance().getText("Erreur"),
									       JOptionPane.ERROR_MESSAGE);
						CreateProjectDialog.this.dispose();
					    }
					}else if (!errorMessage.equals("")){
					    JOptionPane.showMessageDialog( CreateProjectDialog.this,
									   errorMessage,
									   Language.getInstance().getText("Erreur"),
									   JOptionPane.ERROR_MESSAGE);
					    CreateProjectDialog.this.dispose();
					}
				    }else{
					JOptionPane.showMessageDialog(
								      CreateProjectDialog.this,
								      Language.getInstance().
								      getText("Vous_devez_entrez_un_nom_de_fichier_pour_le_document_genere"),
								      Language.getInstance().getText("Erreur_"),
								      JOptionPane.ERROR_MESSAGE);
				    }
				} else {
				    JOptionPane.showMessageDialog(CreateProjectDialog.this,
								  Language.getInstance().
								  getText("Il_faut_obligatoirement_nommer_un_administrateur_au_projet_"),
								  Language.getInstance().getText("Attention_"),
								  JOptionPane.WARNING_MESSAGE);
				}
			    } else {
				JOptionPane.showMessageDialog(CreateProjectDialog.this,
							      Language.getInstance().getText("Ce_nom_de_projet_existe_deja_"),
							      Language.getInstance().getText("Attention_"),
							      JOptionPane.WARNING_MESSAGE);
			    }
                        } else {
			    JOptionPane.showMessageDialog(CreateProjectDialog.this,
							  Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_au_projet_"),
							  Language.getInstance().getText("Attention_"),
							  JOptionPane.WARNING_MESSAGE);
                        }
		    } catch (Exception ex) {
                        ex.printStackTrace();
                        Tools.ihmExceptionView(ex);
		    }
		}
	    });

        JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
        cancel.setToolTipText(Language.getInstance().getText("Annuler"));
        cancel.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    try {
                        if (traitement != null) {
			    traitement.setAnnule(true);
                        }
                        if (traitement2 != null) {
			    traitement2.setAnnule(true);
                        }
                        CreateProjectDialog.this.dispose();
		    } catch (Exception ex) {
                        ex.printStackTrace();
                        Tools.ihmExceptionView(ex);
		    }
		}
	    });

        JPanel buttonsSet = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttonsSet.add(validation);
        buttonsSet.add(cancel);

        JTabbedPane onglets = new JTabbedPane();
        onglets.addTab(Language.getInstance().getText("Principal"), selectPanel);
        int size2 = panelsPluginsForImport.size();
        for (int i = 0; i < size2; i++){
	    JPanel pPanel = (JPanel) panelsPluginsForImport.elementAt(i);
	    String name = pPanel.getName();
	    onglets.addTab(name, pPanel);
        }

        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(Box.createVerticalStrut(10));
        page.add(adminPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(newProjectPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(sauvPanel);
        page.add(Box.createVerticalStrut(10));
        page.add(onglets);
        page.add(Box.createVerticalStrut(10));
        page.add(buttonsSet);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);

        this.setTitle(Language.getInstance().getText("Creer_un_nouveau_projet"));
        //this.setLocation(300,100);
        centerScreen();
    }

    void centerScreen() {
	Dimension dim = getToolkit().getScreenSize();
	this.pack();
	Rectangle abounds = getBounds();
	setLocation((dim.width - abounds.width) / 2, (dim.height - abounds.height) / 2);
	this.setVisible(true);
	requestFocus();
    }

    /**
     * Methode qui parse le document XML dont le chemin est indique dans "path"
     * @param path
     * @return un document DOM4J
     * @throws Exception
     */
    public Document xmlParser(String path) throws Exception{
        SAXReader reader = new SAXReader(false);
        Document doc = reader.read(new FileInputStream(new File(path)));
        return doc;
    }

    /**
     * Methode qui affiche les messages d'erreur
     * @return
     */
    public void showErrorMessage(){
        JOptionPane.showMessageDialog(
				      CreateProjectDialog.this,
				      errorMessage,
				      Language.getInstance().getText("Erreur_"),
				      JOptionPane.ERROR_MESSAGE);
        CreateProjectDialog.this.dispose();
    }

    /**
     * @return Returns the chosenRoot.
     */
    public DefaultMutableTreeNode getChosenRoot() {
        return chosenRoot;
    }
    /**
     * @param chosenRoot The chosenRoot to set.
     */
    public void setChosenRoot(DefaultMutableTreeNode chosenRoot) {
        this.chosenRoot = chosenRoot;
    }
    /**
     * @return Returns the initSelection.
     */
    public boolean isInitSelection() {
        return initSelection;
    }
    /**
     * @param initSelection The initSelection to set.
     */
    public void setInitSelection(boolean initSelection) {
        this.initSelection = initSelection;
    }
    /**
     * @return Returns the familySelectionList.
     */
    public ArrayList<Family> getFamilySelectionList() {
        return familySelectionList;
    }
    /**
     * @param familySelectionList The familySelectionList to set.
     */
    public void setFamilySelectionList(ArrayList<Family> familySelectionList) {
        this.familySelectionList = familySelectionList;
    }
    /**
     * @return Returns the suiteSelectionList.
     */
    public ArrayList<TestList> getSuiteSelectionList() {
        return suiteSelectionList;
    }
    /**
     * @param suiteSelectionList The suiteSelectionList to set.
     */
    public void setSuiteSelectionList(ArrayList<TestList> suiteSelectionList) {
        this.suiteSelectionList = suiteSelectionList;
    }
    /**
     * @return Returns the testSelectionList.
     */
    public ArrayList<Test> getTestSelectionList() {
        return testSelectionList;
    }
    /**
     * @param testSelectionList The testSelectionList to set.
     */
    public void setTestSelectionList(ArrayList<Test> testSelectionList) {
        this.testSelectionList = testSelectionList;
    }
    /**
     * @return Returns the selectionDesTests.
     */
    public boolean isSelectionDesTests() {
        return selectionDesTests;
    }
    /**
     * @param selectionDesTests The selectionDesTests to set.
     */
    public void setSelectionDesTests(boolean selectionDesTests) {
        this.selectionDesTests = selectionDesTests;
    }
    /**
	* @return Returns the newProject.
	*/
    public Project getNewProject() {
        return newProject;
    }
    /**
     * @param newProject The newProject to set.
     */
    public void setNewProject(Project newProject) {
        this.newProject = newProject;
    }
    /**
     * @return Returns the errorMessage.
     */
    public String getErrorMessage() {
        return errorMessage;
    }
    /**
     * @param errorMessage The errorMessage to set.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    /**
     * @return Returns the recupAttachPb.
     */
    public boolean isRecupAttachPb() {
        return recupAttachPb;
    }
    /**
     * @param recupAttachPb The recupAttachPb to set.
     */
    public void setRecupAttachPb(boolean recupAttachPb) {
        this.recupAttachPb = recupAttachPb;
    }
    /**
     * @return Returns the majAttachPb.
     */
    public boolean isMajAttachPb() {
        return majAttachPb;
    }
    /**
     * @param majAttachPb The majAttachPb to set.
     */
    public void setMajAttachPb(boolean majAttachPb) {
        this.majAttachPb = majAttachPb;
    }
}
