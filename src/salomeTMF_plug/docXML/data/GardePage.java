/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package  salomeTMF_plug.docXML.data;

import java.util.Date;

/**
 * Classe qui permet la sauvegarde des elements
 * du formulaire pour la creation de la page de garde
 * @author  vapu8214
 */
public class GardePage {
    /**
     * Emplacement du fichier contenant le logo
     */
    private String logo;
    
    /**
     * Nom et adresse eventuelle de l'entreprise
     */
    private String entreprise;
    
    /**
     * Auteur du document
     */
    private String auteur;
    
    /**
     * Email de l'auteur du document
     */
    private String email;
    
    /**
     * Titre du document
     */
    private String titre;
    
    /**
     * Version du document
     */
    private String version;
    
    /**
     * Date
     */
    private Date date;
    
    /**
     * Informations complementaires
     */
    private String infos;
    
    
    /** Creates a new instance of FormSave */
    public GardePage(String logo, String entreprise, String auteur, String email, String titre, String version, Date date,
                     String infos) {
        this.logo = logo;
        this.entreprise = entreprise;
        this.auteur = auteur;
        this.email = email;
        this.titre = titre;
        this.version = version;
        this.date = date;
        this.infos = infos;
    }
    
    /**
     * Getter for property logo.
     * @return Value of property logo.
     */
    public java.lang.String getLogo() {
        return logo;
    }
    
    /**
     * Setter for property logo.
     * @param logo New value of property logo.
     */
    public void setLogo(java.lang.String logo) {
        this.logo = logo;
    }
    
    /**
     * Getter for property entreprise.
     * @return Value of property entreprise.
     */
    public java.lang.String getEntreprise() {
        return entreprise;
    }
    
    /**
     * Setter for property entreprise.
     * @param entreprise New value of property entreprise.
     */
    public void setEntreprise(java.lang.String entreprise) {
        this.entreprise = entreprise;
    }
    
    /**
     * Getter for property auteur.
     * @return Value of property auteur.
     */
    public java.lang.String getAuteur() {
        return auteur;
    }
    
    /**
     * Setter for property auteur.
     * @param auteur New value of property auteur.
     */
    public void setAuteur(java.lang.String auteur) {
        this.auteur = auteur;
    }
    
    /**
     * Getter for property titre.
     * @return Value of property titre.
     */
    public java.lang.String getTitre() {
        return titre;
    }
    
    /**
     * Setter for property titre.
     * @param titre New value of property titre.
     */
    public void setTitre(java.lang.String titre) {
        this.titre = titre;
    }
    
    /**
     * Getter for property version.
     * @return Value of property version.
     */
    public java.lang.String getVersion() {
        return version;
    }
    
    /**
     * Setter for property version.
     * @param version New value of property version.
     */
    public void setVersion(java.lang.String version) {
        this.version = version;
    }
          
    /**
     * Getter for property infos.
     * @return Value of property infos.
     */
    public java.lang.String getInfos() {
        return infos;
    }
    
    /**
     * Setter for property infos.
     * @param infos New value of property infos.
     */
    public void setInfos(java.lang.String infos) {
        this.infos = infos;
    }
    
    /**
     * Getter for property date.
     * @return Value of property date.
     */
    public java.util.Date getDate() {
        return date;
    }
    
    /**
     * Setter for property date.
     * @param date New value of property date.
     */
    public void setDate(java.util.Date date) {
        this.date = date;
    }
    
    /**
     * Getter for property email.
     * @return Value of property email.
     */
    public java.lang.String getEmail() {
        return email;
    }
    
    /**
     * Setter for property email.
     * @param email New value of property email.
     */
    public void setEmail(java.lang.String email) {
        this.email = email;
    }
    
}
