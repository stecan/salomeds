/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT, Marche Mikael
 *
 * Contact: mikael.marche@orange-ft.com
 */

package salomeTMF_plug.docXML.export;

public class Contexte {
        
    public enum OutFormat {XML, HTML, DOCBOOK, EXCEL, WORD, PDF};
        
    String pathAttach;
    String[] ficType;
    String coding;
    private OutFormat outputFormat; 
        
    /* Tobedeleted */
    /*boolean hasCampRequirementsGraph = false;
      boolean hasResExecRequirementsGraph = false;
      boolean hasRequirementsGraph = false;
    */
    public Contexte(String _pathAttach, String[] _ficType, String code, OutFormat outputFormat) {
        pathAttach = _pathAttach;
        ficType = _ficType;
        coding = code;
        this.outputFormat = outputFormat;
        /*
          hasCampRequirementsGraph = false;
          hasResExecRequirementsGraph = false;
          hasRequirementsGraph = false;
        */
    }

    public String[] getFicType() {
        return ficType;
    }

    public String getPathAttach() {
        return pathAttach;
    }

    public String getCoding(){
        return coding;
    }

    public OutFormat getOutputFormat() {
        return outputFormat;
    }

    public void setOutputFormat(OutFormat outputFormat) {
        this.outputFormat = outputFormat;
    }
        
    /*
      public boolean isHasCampRequirementsGraph() {
      return hasCampRequirementsGraph;
      }

      public void setHasCampRequirementsGraph(boolean hasCampRequirementsGraph) {
      this.hasCampRequirementsGraph = hasCampRequirementsGraph;
      }

      public boolean isHasRequirementsGraph() {
      return hasRequirementsGraph;
      }

      public void setHasRequirementsGraph(boolean hasRequirementsGraph) {
      this.hasRequirementsGraph = hasRequirementsGraph;
      }

      public boolean isHasResExecRequirementsGraph() {
      return hasResExecRequirementsGraph;
      }

      public void setHasResExecRequirementsGraph(
      boolean hasResExecRequirementsGraph) {
      this.hasResExecRequirementsGraph = hasResExecRequirementsGraph;
      }
    */
        
                
}
