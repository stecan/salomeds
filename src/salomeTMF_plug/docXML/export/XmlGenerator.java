/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT, Marche Mikael
 *
 * Contact: mikael.marche@orange-ft.com
 */

package salomeTMF_plug.docXML.export;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLWriterPlugin;

import salomeTMF_plug.docXML.export.Contexte.OutFormat;

//import salomeTMF_plug.requirements.data.ReqLeaf;
//import salomeTMF_plug.requirements.data.Requirement;

/**
 * Classe qui permet la construction des documents xml, leur transformation en
 * Html et leur sauvegarde dans un fichier
 *
 * @author vapu8214
 */
public class XmlGenerator implements XMLWriterPlugin {

    public boolean addDocType = true;

    // IPlugObject pIPlugObject;
    private Vector<XMLPrinterPlugin> listXMLPlugin = new Vector<XMLPrinterPlugin>();
    Hashtable<Integer, String> userLoginByID = new Hashtable<Integer, String>();
    Contexte pContexte;

    public XmlGenerator(IPlugObject _pIPlugObject, Contexte pContexte) {
        // pIPlugObject = _pIPlugObject;
        Vector<Extension> listExtXMLPlugin = _pIPlugObject
            .getXMLPrintersExtension();
        int size = listExtXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            Extension pXMLExt = (Extension) listExtXMLPlugin.elementAt(i);
            JPFManager pJPFManager = _pIPlugObject.getPluginManager();
            try {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) pJPFManager
                    .activateExtension(pXMLExt);
                if (!listXMLPlugin.contains(pXMLPrinterPlugin)) {
                    listXMLPlugin.add(pXMLPrinterPlugin);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.pContexte = pContexte;
    }

    void writeBaseProjet(Element projetVT) throws Exception {
        Project proj = DataModel.getCurrentProject();
        if (!proj.getNameFromModel().equals("")) {
            projetVT.addElement("Nom").setText(proj.getNameFromModel());
        }
        if (proj.getDescriptionFromModel() != null) {
            if (!proj.getDescriptionFromModel().equals("")) {
                projetVT.addElement("Description").setText(
                        proj.getDescriptionFromModel().replaceAll("\n", "\\\\n"));
            }
        }
        String date_proj = DateFormat.getDateInstance(DateFormat.MEDIUM,
                                                      Locale.FRANCE).format(proj.getCreationDateFromModel());
        projetVT.addElement("Date_crea").setText(date_proj);

        if (!proj.getParameterSetFromModel().isEmpty()) {
            Element params = projetVT.addElement("Params");
            Enumeration<Parameter> it = proj.getParameterSetFromModel()
                .elements();
            // iterator();
            while (it.hasMoreElements()) {
                Element param = params.addElement("Param");
                Parameter par = (Parameter) it.nextElement();
                param.addElement("Nom").setText(par.getNameFromModel());
                if (!par.getDescriptionFromModel().equals("")) {
                    param.addElement("Description").setText(
                                                            par.getDescriptionFromModel().replaceAll("\n",
                                                                                                     "\\\\n"));
                }
                param.addAttribute("id_param", "Param_"
                                   + new Integer(par.getIdBdd()).toString());
            }
        }

        HashMap<String, Attachment> attachsProj = proj
            .getAttachmentMapFromModel();
        addAttach(projetVT, pContexte.getPathAttach() + File.separator
                  + "Attachements", attachsProj, "Attachements");

        try {
            ISQLProject pISQLProject = Api.getISQLObjectFactory()
                .getISQLProject();
            GroupWrapper[] tmpArray = pISQLProject.getProjectGroups(DataModel
                                                                    .getCurrentProject().getIdBdd());
            Vector<GroupWrapper> project_Group = new Vector<GroupWrapper>();
            for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
                project_Group.add(tmpArray[tmpI]);
            }
            userLoginByID.clear();
            if (project_Group.size() != 0) {
                Element groups = projetVT.addElement("GroupesDePersonnes");
                for (int i = 0; i < project_Group.size(); i++) {
                    GroupWrapper pGroupWrapper = project_Group.get(i);
                    Element grElem = groups.addElement("GroupeDePersonnes");
                    grElem.addElement("Nom").setText(pGroupWrapper.getName());
                    if (!pGroupWrapper.getDescription().equals("")) {
                        grElem.addElement("Description").setText(
                                                                 pGroupWrapper.getDescription().replaceAll("\n",
                                                                                                           "\\\\n"));
                    }
                    ISQLGroup pISQLGroup = Api.getISQLObjectFactory()
                        .getISQLGroup();
                    UserWrapper[] tmpUserArray = pISQLGroup
                        .getUserWrappersInGroup(pGroupWrapper.getIdBDD());
                    Vector<UserWrapper> group_users = new Vector<UserWrapper>();
                    for (int tmpI = 0; tmpI < tmpUserArray.length; tmpI++) {
                        group_users.add(tmpUserArray[tmpI]);
                    }

                    if (group_users.size() != 0) {
                        Element listPersElem = grElem.addElement("Personnes");
                        for (int j = 0; j < group_users.size(); j++) {
                            UserWrapper pUserWrapper = (UserWrapper) group_users
                                .elementAt(j);
                            Element persElem = listPersElem
                                .addElement("Personne");
                            if (!pUserWrapper.getLogin().equals("")) {
                                persElem.addElement("Login").setText(
                                                                     pUserWrapper.getLogin());
                                userLoginByID.put(new Integer(pUserWrapper
                                                              .getIdBDD()), pUserWrapper.getLogin());
                            }
                            if (pUserWrapper.getName() != null
                                && !pUserWrapper.getName().equals("")) {
                                persElem.addElement("Nom").setText(
                                                                   pUserWrapper.getName());
                            }
                            if (pUserWrapper.getPrenom() != null
                                && !pUserWrapper.getPrenom().equals("")) {
                                persElem.addElement("Prenom").setText(
                                                                      pUserWrapper.getPrenom());
                            }
                            if (pUserWrapper.getDescription() != null
                                && !pUserWrapper.getDescription()
                                .equals("")) {
                                persElem.addElement("Description").setText(
                                                                           pUserWrapper.getDescription()
                                                                           .replaceAll("\n", "\\\\n"));
                            }
                            if (pUserWrapper.getEmail() != null
                                && !pUserWrapper.getEmail().equals("")) {
                                persElem.addElement("Email").setText(
                                                                     pUserWrapper.getEmail());
                            }
                            if (pUserWrapper.getTel() != null
                                && !pUserWrapper.getTel().equals("")) {
                                persElem.addElement("Tel").setText(
                                                                   pUserWrapper.getTel());
                            }
                            if (pUserWrapper.getCreateDate().toString() != null
                                && !pUserWrapper.getCreateDate().toString()
                                .equals("")) {
                                persElem.addElement("Date_crea")
                                    .setText(
                                             pUserWrapper.getCreateDate()
                                             .toString());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }

        /***** Plugin add XML for Project *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2Project(projetVT, proj, this,
                                                        pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for Project *****/

    }

    // void writeRequirement(Element reqsElem, DefaultMutableTreeNode pTempNode,
    // boolean first) throws Exception {
    // Requirement pTempReq = (Requirement)pTempNode.getUserObject();
    // HashMap<String,Attachment> attachs = null;
    // try {
    // if (!first){
    // attachs = pTempReq.getAttachmentMapFromModel();
    // }
    // }catch (Exception e ){
    //
    // }
    // if (pTempReq instanceof ReqLeaf){
    // //Write leaf
    // Element reqLeafElem = reqsElem.addElement("Requirement");
    // reqLeafElem.addElement("Nom").setText(pTempReq.getNameFromModel());
    // reqLeafElem.addAttribute("priority",
    // ""+((ReqLeaf)pTempReq).getPriorityFromModel());
    // reqLeafElem.addAttribute("version",
    // ((ReqLeaf)pTempReq).getVersionFromModel());
    //
    // reqLeafElem.addAttribute("id_req", "Req_"+new
    // Integer(pTempReq.getIdBdd()).toString());
    // reqLeafElem.addAttribute("id_req_parent", "Req_"+new
    // Integer(pTempReq.getParent().getIdBdd()).toString());
    // if (pTempReq.getDescriptionFromModel()!=null &&
    // !pTempReq.getDescriptionFromModel().equals("")){
    // addHTMLDescription(reqLeafElem, pTempReq.getDescriptionFromModel());
    // }
    // if (attachs!=null){
    // addAttach(reqLeafElem,
    // pContexte.getPathAttach()+File.separator+"Attachements"+ File.separator
    // +"Requirements"+ File.separator +pTempReq.getIdBdd(), attachs,
    // "Attachements"+ File.separator +"Requirements"+ File.separator
    // +pTempReq.getIdBdd());
    // }
    // } else {
    // //Write family
    // Element reqFamilyElem = reqsElem;
    // if (!first){
    // reqFamilyElem = reqsElem.addElement("RequirementFamily");
    //
    // reqFamilyElem.addElement("Nom").setText(pTempReq.getNameFromModel());
    // reqFamilyElem.addAttribute("id_req", "Req_"+new
    // Integer(pTempReq.getIdBdd()).toString());
    // reqFamilyElem.addAttribute("id_req_parent", "Req_"+new
    // Integer(pTempReq.getParent().getIdBdd()).toString());
    // if (pTempReq.getDescriptionFromModel()!=null &&
    // !pTempReq.getDescriptionFromModel().equals("")){
    // addHTMLDescription(reqFamilyElem, pTempReq.getDescriptionFromModel());
    // }
    // if (attachs!=null){
    // addAttach(reqFamilyElem,
    // pContexte.getPathAttach()+File.separator+"Attachements"+ File.separator
    // +"Requirements"+ File.separator +pTempReq.getIdBdd(), attachs,
    // "Attachements"+ File.separator +"Requirements"+ File.separator +
    // pTempReq.getIdBdd());
    // }
    // } else {
    // first = false;
    // }
    // int nbChild = pTempNode.getChildCount();
    //
    // for (int i = 0; i < nbChild ; i++){
    // writeRequirement(reqFamilyElem,
    // (DefaultMutableTreeNode)pTempNode.getChildAt(i), first);
    // }
    //
    // }
    // }

    void writeEnvironnement(Element projetVT) throws Exception {
        ArrayList<Environment> envirs = DataModel.getCurrentProject()
            .getEnvironmentListFromModel();
        if (!envirs.isEmpty()) {
            Element envsElem = projetVT.addElement("Environnements");
            Iterator<Environment> it = envirs.iterator();
            while (it.hasNext()) {
                Environment env_courant = it.next();
                Element envElem = envsElem.addElement("Environnement");
                envElem.addElement("Nom").setText(
                                                  env_courant.getNameFromModel());
                envElem.addAttribute("idEnv", "Env_"
                                     + new Integer(env_courant.getIdBdd()).toString());
                if (env_courant.getDescriptionFromModel() != null
                    && !env_courant.getDescriptionFromModel().equals("")) {
                    envElem.addElement("Description").setText(
                                                              env_courant.getDescriptionFromModel().replaceAll(
                                                                                                               "\n", "\\\\n"));
                }
                Hashtable<Parameter, String> params = env_courant
                    .getParametersHashTableFromModel();
                if (!params.isEmpty()) {
                    Element elemValParams = envElem.addElement("ValeurParams");
                    Enumeration<Parameter> e = params.keys();
                    while (e.hasMoreElements()) {
                        Element elemValParam = elemValParams
                            .addElement("ValeurParam");
                        Parameter p = e.nextElement();
                        elemValParam.addAttribute("ref", "Param_"
                                                  + new Integer(p.getIdBdd()).toString());
                        elemValParam.addElement("Nom").setText(
                                                               p.getNameFromModel());
                        if (!(params.get(p)).equals("")) {
                            elemValParam.addAttribute("valeur", params.get(p));
                        }
                    }
                }
                Script scInit = env_courant.getInitScriptFromModel();
                if (scInit != null) {
                    Element scriptElem = envElem.addElement("Script");
                    if (scInit.getScriptExtensionFromModel() != null) {
                        scriptElem.addElement("Classpath").setText(
                                                                   scInit.getScriptExtensionFromModel());
                    }
                    if (scInit.getPlugArgFromModel() != null
                        && !scInit.getPlugArgFromModel().equals("")) {
                        scriptElem.addElement("ArgScript").setText(
                                                                   scInit.getPlugArgFromModel());
                    }
                    if (!scInit.getTypeFromModel().equals("")) {
                        scriptElem.addAttribute("type", scInit
                                                .getTypeFromModel());
                    }
                    scriptElem.addAttribute("nom", scInit.getNameFromModel());

                    String dest = new String(pContexte.getPathAttach()
                                             + File.separator + "Attachements" + File.separator
                                             + "Environnements" + File.separator
                                             + formater(env_courant.getNameFromModel())
                                             + File.separator + "Script");
                    scriptElem.addAttribute("dir",
                                            "Attachements/Environnements/"
                                            + formater(env_courant.getNameFromModel())
                                            + "/Script/"
                                            + formater(scInit.getNameFromModel()));

                    try {
                        scInit.getFileFromDB(dest);
                        String extension = scInit.getNameFromModel().substring(
                                                                               scInit.getNameFromModel().lastIndexOf("."));
                        if (pContexte.getFicType() != null) {
                            for (int i = 0; i < pContexte.getFicType().length; i++) {
                                if (pContexte.getFicType()[i].indexOf(".") != -1
                                    && extension.equals(pContexte
                                                        .getFicType()[i]
                                                        .substring(pContexte
                                                                   .getFicType()[i]
                                                                   .lastIndexOf(".")))) {
                                    Element text = scriptElem
                                        .addElement("Text");
                                    if (pContexte.getOutputFormat() == OutFormat.DOCBOOK) {
                                        ajouterScript(text, pContexte
                                                      .getPathAttach()
                                                      + File.separator
                                                      + "Attachements"
                                                      + File.separator
                                                      + "Environnements"
                                                      + File.separator
                                                      + formater(env_courant
                                                                 .getNameFromModel())
                                                      + File.separator
                                                      + "Script"
                                                      + File.separator
                                                      + formater(scInit
                                                                 .getNameFromModel()));
                                    } else {
                                        ajouterTexte(text, pContexte
                                                     .getPathAttach()
                                                     + File.separator
                                                     + "Attachements"
                                                     + File.separator
                                                     + "Environnements"
                                                     + File.separator
                                                     + formater(env_courant
                                                                .getNameFromModel())
                                                     + File.separator
                                                     + "Script"
                                                     + File.separator
                                                     + formater(scInit
                                                                .getNameFromModel()));
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
                HashMap<String, Attachment> attachs = env_courant
                    .getAttachmentMapFromModel();
                addAttach(envElem, pContexte.getPathAttach() + File.separator
                          + "Attachements" + File.separator + "Environnements"
                          + File.separator + env_courant.getIdBdd(), attachs,
                          "Attachements" + File.separator + "Environnements"
                          + File.separator + env_courant.getIdBdd());

                /***** Plugin add XML for Environnement *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    if (pXMLPrinterPlugin != null)
                        pXMLPrinterPlugin.addXMLElement2Environment(envElem,
                                                                    env_courant, this, pContexte.getPathAttach());
                }
                /***** (END) Plugin add XML for Environnement *****/
            }
        }
    }

    /**
     * Methode qui ajoute dans le document resultant le texte contenu dans le
     * fichier dont le chemin est indique en parametre
     *
     * @param text
     *            element auquel on rajoute lengthcontenu du fichier
     * @param path
     *            chemin du fichier dont le contenu doit etre ajoute au document
     */
    public void ajouterScript(Element text, String path) throws Exception {
        File fichier = new File(path);
        FileReader fluxFichier = null;
        try {
            fluxFichier = new FileReader(fichier);
            /*
             * BufferedReader tamponLecture = new BufferedReader(fluxFichier);
             * String ligne; while((ligne=tamponLecture.readLine())!=null){
             * text.addElement("ligne").setText(ligne); } tamponLecture.close();
             */
            StringBuffer contenu = new StringBuffer();
            int car;
            while ((car = fluxFichier.read()) != -1) {
                contenu.append((char) car);
            }
            fluxFichier.close();
            text.addCDATA(contenu.toString());
            System.out.println("contenu" + contenu.toString());
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    void writeFamily(Element projetVT, boolean filtre,
                     ArrayList<Family> selectFamilyList,
                     ArrayList<TestList> selectSuiteList, ArrayList<Test> selectTestList)
        throws Exception {

        ArrayList<Family> familleList = DataModel.getCurrentProject()
            .getFamilyListFromModel();
        if ((familleList != null) && (!familleList.isEmpty())
            || (filtre && !selectFamilyList.isEmpty())) {
            Element famsElem = projetVT.addElement("Familles");
            Iterator<Family> it = familleList.iterator();
            while (it.hasNext()) {
                Family family = it.next();
                if (!filtre || selectFamilyList.contains(family)) {
                    Element famElem = famsElem.addElement("Famille");
                    famElem.addElement("Nom")
                        .setText(family.getNameFromModel());
                    if (family.getDescriptionFromModel() != null
                        && !family.getDescriptionFromModel().equals("")) {
                        String description = family.getDescriptionFromModel();
                        addHTMLDescription(famElem, description);
                    }
                    famElem.addAttribute("id_famille", "Fam_"
                                         + new Integer(family.getIdBdd()).toString());
                    HashMap<String, Attachment> attachsFam = family
                        .getAttachmentMapFromModel();
                    addAttach(famElem, pContexte.getPathAttach()
                              + File.separator + "Attachements" + File.separator
                              + family.getIdBdd(), attachsFam, "Attachements"
                              + File.separator + family.getIdBdd());

                    /***** Plugin add XML for Family *****/
                    int size = listXMLPlugin.size();
                    for (int i = 0; i < size; i++) {
                        XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                            .elementAt(i);
                        if (pXMLPrinterPlugin != null)
                            pXMLPrinterPlugin.addXMLElement2Family(famElem,
                                                                   family, this, pContexte.getPathAttach());
                    }
                    /***** (END) Plugin add XML for Family *****/

                    ArrayList<TestList> suiteTestList = family
                        .getSuiteListFromModel();
                    if ((suiteTestList != null) && (!suiteTestList.isEmpty())
                        || (filtre && !selectSuiteList.isEmpty())) {
                        Element suitesElem = famElem.addElement("SuiteTests");
                        Iterator<TestList> iter = suiteTestList.iterator();
                        while (iter.hasNext()) {
                            TestList suiteTest = iter.next();
                            if (!filtre || selectSuiteList.contains(suiteTest)) {
                                writeTestList(suitesElem, family, suiteTest,
                                              filtre, selectSuiteList, selectTestList);
                            }

                        }
                    }
                }
            }
        }
    }

    private void addHTMLDescription(Element parentElem, String description)
        throws Exception {
        String desc = description.replaceAll("<br>", "<br />");
        try {
            Document document = DocumentHelper.parseText(desc);
            Element bodyElem = (Element) document.selectSingleNode("//body");
            Element descElem = bodyElem.createCopy("Description");
            if (descElem.isTextOnly()) {
                String text = descElem.getText();
                if (text.trim().length() != 0) {
                    descElem.addAttribute("isHTML", "true");
                    parentElem.add(descElem);
                }
            } else {
                descElem.addAttribute("isHTML", "true");
                parentElem.add(descElem);
            }
        } catch (Exception e) {
            parentElem.addElement("Description").setText(
                                                         desc.replaceAll("\n", "\\\\n"));
        }
    }

    void writeTestList(Element suitesElem, Family pFamily, TestList pSuite,
                       boolean filtre, ArrayList<TestList> selectSuiteList,
                       ArrayList<Test> selectTestList) throws Exception {
        Element suiteElem = suitesElem.addElement("SuiteTest");
        suiteElem.addElement("Nom").setText(pSuite.getNameFromModel());
        if (pSuite.getDescriptionFromModel() != null
            && !pSuite.getDescriptionFromModel().equals("")) {
            addHTMLDescription(suiteElem, pSuite.getDescriptionFromModel());
        }
        suiteElem.addAttribute("id_suite", "SuiteTest_"
                               + new Integer(pSuite.getIdBdd()).toString());

        /***** Plugin add XML for Suite *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2Suite(suiteElem, pSuite, this,
                                                      pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for Suite *****/

        ArrayList<Test> testList = DataModel.getCurrentProject()
            .getAllTestFromModel(pSuite);
        if ((testList != null) && (!testList.isEmpty())
            || (filtre && !selectTestList.isEmpty())) {
            Element testsElem = suiteElem.addElement("Tests");
            Iterator<Test> it2 = testList.iterator();
            while (it2.hasNext()) {
                Test test_cour = (Test) it2.next();
                if (!filtre || selectTestList.contains(test_cour)) {
                    writeTest(testsElem, pFamily, pSuite, test_cour);
                }
            }
        }
        HashMap<String, Attachment> attachs = pSuite
            .getAttachmentMapFromModel();
        addAttach(suiteElem, pContexte.getPathAttach() + File.separator
                  + "Attachements" + File.separator + pFamily.getIdBdd()
                  + File.separator + pSuite.getIdBdd(), attachs, "Attachements"
                  + File.separator + pFamily.getIdBdd() + File.separator
                  + pSuite.getIdBdd());

    }

    void writeTest(Element testsElem, Family pFamily, TestList pSuite,
                   Test pTest) throws Exception {
        Element testElem = testsElem.addElement("Test");
        Element pers = testElem.addElement("Concepteur");
        if (pTest.getConceptorFromModel() != null
            && !pTest.getConceptorFromModel().equals("")) {
            pers.addElement("Nom").setText(pTest.getConceptorFromModel());
        }
        // String login =
        // ConnectionData.getSuiteTestSelect().getTestDesignerLogin(test_cour.getNameFromModel(),
        // suiteTest.getNameFromModel(), family.getNameFromModel());
        String login = pTest.getConceptorLoginFromModel();
        if (login != null && !login.equals("")) {
            pers.addElement("Login").setText(login);
        }
        testElem.addElement("Nom").setText(pTest.getNameFromModel());
        String laDate = DateFormat.getDateInstance(DateFormat.MEDIUM,
                                                   Locale.FRANCE).format(pTest.getCreationDateFromModel());
        testElem.addElement("Date_crea").setText(laDate);
        if (pTest.getDescriptionFromModel() != null
            && !pTest.getDescriptionFromModel().equals("")) {
            addHTMLDescription(testElem, pTest.getDescriptionFromModel());
        }
        testElem.addAttribute("id_test", "Test_"
                              + new Integer(pTest.getIdBdd()).toString());

        ajouterInfoExecToTest(pTest, testElem);

        ArrayList<Parameter> paramList = pTest.getParameterListFromModel();
        if (!paramList.isEmpty()) {
            Element params = testElem.addElement("ParamsT");
            Iterator<Parameter> itp = paramList.iterator();
            while (itp.hasNext()) {
                Parameter param = (Parameter) itp.next();
                Element paramElem = params.addElement("ParamT");
                paramElem.addAttribute("ref", "Param_"
                                       + new Integer(param.getIdBdd()).toString());
                paramElem.addElement("Nom").setText(param.getNameFromModel());
            }
        }

        /***** Plugin add XML for Test *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2Test(testElem, pTest, this,
                                                     pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for Test *****/

        HashMap<String, Attachment> attachs = pTest.getAttachmentMapFromModel();
        addAttach(testElem, pContexte.getPathAttach() + File.separator
                  + "Attachements" + File.separator + pFamily.getIdBdd()
                  + File.separator + pSuite.getIdBdd() + File.separator
                  + pTest.getIdBdd(), attachs, "Attachements" + File.separator
                  + pFamily.getIdBdd() + File.separator + pSuite.getIdBdd()
                  + File.separator + pTest.getIdBdd());

        if (pTest instanceof ManualTest) {
            writeManualTest(testElem, pFamily, pSuite, (ManualTest) pTest);
        } else {
            writeAutomaticTest(testElem, pFamily, pSuite, (AutomaticTest) pTest);
        }
    }

    void writeManualTest(Element testElem, Family pFamily, TestList pSuite,
                         ManualTest pTest) throws Exception {

        Element testManElem = testElem.addElement("TestManuel");
        ArrayList<Action> actionList = pTest.getActionListFromModel(false);
        Iterator<Action> it3 = actionList.iterator();
        while (it3.hasNext()) {
            Element actionElem = testManElem.addElement("ActionTest");
            Action ac = it3.next();
            actionElem.addElement("Nom").setText(ac.getNameFromModel());
            if (ac.getDescriptionFromModel() != null
                && !ac.getDescriptionFromModel().equals("")) {
                actionElem.addElement("Description").setText(
                                                             ac.getDescriptionFromModel().replaceAll("\n", "\\\\n"));
            }
            if (ac.getAwaitedResultFromModel() != null
                && !ac.getAwaitedResultFromModel().equals("")) {
                actionElem.addElement("ResultAttendu").setText(
                                                               ac.getAwaitedResultFromModel()
                                                               .replaceAll("\n", "\\\\n"));
            }

            actionElem.addAttribute("id_action", "Action_"
                                    + new Integer(ac.getIdBdd()).toString());
            Hashtable<String, Parameter> paramEns = ac
                .getParameterHashSetFromModel();
            if (!paramEns.isEmpty()) {
                Element params = actionElem.addElement("ParamsT");
                Enumeration<Parameter> itp = paramEns.elements();
                while (itp.hasMoreElements()) {
                    Parameter param = (Parameter) itp.nextElement();
                    Element paramElem = params.addElement("ParamT");
                    paramElem.addAttribute("ref", "Param_"
                                           + new Integer(param.getIdBdd()).toString());
                    paramElem.addElement("Nom").setText(
                                                        param.getNameFromModel());
                }
            }
            HashMap<String, Attachment> attachs = ac
                .getAttachmentMapFromModel();
            addAttach(actionElem, pContexte.getPathAttach() + File.separator
                      + "Attachements" + File.separator + pFamily.getIdBdd()
                      + File.separator + pSuite.getIdBdd() + File.separator
                      + pTest.getIdBdd() + File.separator + ac.getIdBdd(),
                      attachs, "Attachements" + File.separator
                      + pFamily.getIdBdd() + File.separator
                      + pSuite.getIdBdd() + File.separator
                      + pTest.getIdBdd() + File.separator + ac.getIdBdd());

            /***** Plugin add XML for Action *****/
            int size = listXMLPlugin.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                    .elementAt(i);
                if (pXMLPrinterPlugin != null)
                    pXMLPrinterPlugin.addXMLElement2Action(actionElem, ac,
                                                           this, pContexte.getPathAttach());
            }
            /***** (END) Plugin add XML for Action *****/

        }
    }

    void writeAutomaticTest(Element testElem, Family pFamily, TestList pSuite,
                            AutomaticTest pTest) throws Exception {

        Element testAutoElem = testElem.addElement("TestAuto");
        testAutoElem.addAttribute("plug_ext", ((AutomaticTest) pTest)
                                  .getExtensionFromModel());
        Script sc = ((AutomaticTest) pTest).getScriptFromModel();
        if (sc != null) {
            Element scriptElem = testAutoElem.addElement("Script");
            if (sc.getScriptExtensionFromModel() != null
                && !sc.getScriptExtensionFromModel().equals("")) {
                scriptElem.addElement("Classpath").setText(
                                                           sc.getScriptExtensionFromModel());
            }
            if (sc.getPlugArgFromModel() != null
                && !sc.getPlugArgFromModel().equals("")) {
                scriptElem.addElement("ArgScript").setText(
                                                           sc.getPlugArgFromModel());
            }
            if (!sc.getTypeFromModel().equals("")) {
                scriptElem.addAttribute("type", sc.getTypeFromModel());
            }
            scriptElem.addAttribute("nom", sc.getNameFromModel());
            String dest = new String(pContexte.getPathAttach() + File.separator
                                     + "Attachements" + File.separator
                                     + formater(pFamily.getNameFromModel()) + File.separator
                                     + formater(pSuite.getNameFromModel()) + File.separator
                                     + formater(pTest.getNameFromModel()) + File.separator
                                     + "Script");
            scriptElem.addAttribute("dir", "Attachements/"
                                    + formater(pFamily.getNameFromModel()) + "/"
                                    + formater(pSuite.getNameFromModel()) + "/"
                                    + formater(pTest.getNameFromModel()) + "/Script/"
                                    + formater(sc.getNameFromModel()));
            try {
                sc.getFileFromDB(dest);
                String extension = sc.getNameFromModel().substring(
                                                                   sc.getNameFromModel().lastIndexOf("."));
                if (pContexte.getFicType() != null) {
                    for (int i = 0; i < pContexte.getFicType().length; i++) {
                        if (pContexte.getFicType()[i].indexOf(".") != -1
                            && extension.equals(pContexte.getFicType()[i]
                                                .substring(pContexte.getFicType()[i]
                                                           .lastIndexOf(".")))) {
                            Element text = scriptElem.addElement("Text");
                            if (pContexte.getOutputFormat() == OutFormat.DOCBOOK) {
                                ajouterScript(text, pContexte.getPathAttach()
                                              + File.separator + "Attachements"
                                              + File.separator
                                              + formater(pFamily.getNameFromModel())
                                              + File.separator
                                              + formater(pSuite.getNameFromModel())
                                              + File.separator
                                              + formater(pTest.getNameFromModel())
                                              + File.separator + "Script"
                                              + File.separator
                                              + formater(sc.getNameFromModel()));
                            } else {
                                ajouterTexte(text, pContexte.getPathAttach()
                                             + File.separator + "Attachements"
                                             + File.separator
                                             + formater(pFamily.getNameFromModel())
                                             + File.separator
                                             + formater(pSuite.getNameFromModel())
                                             + File.separator
                                             + formater(pTest.getNameFromModel())
                                             + File.separator + "Script"
                                             + File.separator
                                             + formater(sc.getNameFromModel()));
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);
            }
        }
    }

    void writeDataSet(Element campElem, Campaign pCamp) throws Exception {
        // jeu de donnees
        ArrayList<DataSet> datasetList = pCamp.getDataSetListFromModel();
        if (!datasetList.isEmpty()) {
            Element datasetsElem = campElem.addElement("JeuxDonnees");
            Iterator<DataSet> iter = datasetList.iterator();
            while (iter.hasNext()) {
                Element datasetElem = datasetsElem.addElement("JeuDonnees");
                DataSet dataset = iter.next();
                datasetElem.addElement("Nom").setText(
                                                      dataset.getNameFromModel());
                if (dataset.getDescriptionFromModel() != null
                    && !dataset.getDescriptionFromModel().equals("")) {
                    datasetElem.addElement("Description").setText(
                                                                  dataset.getDescriptionFromModel().replaceAll("\n",
                                                                                                               "\\\\n"));
                }
                HashMap<String, String> parameters = dataset
                    .getParametersHashMapFromModel();// HashMap nomParam ->
                // ValeurParam
                // String -> String
                if (!parameters.isEmpty()) {
                    Element elemValParams = datasetElem
                        .addElement("ValeurParams");
                    Iterator<String> it2 = parameters.keySet().iterator();
                    while (it2.hasNext()) {
                        Element elemValParam = elemValParams
                            .addElement("ValeurParam");
                        String par = it2.next();
                        Parameter p = DataModel.getCurrentProject()
                            .getParameterFromModel(par);
                        elemValParam.addAttribute("ref", "Param_"
                                                  + new Integer(p.getIdBdd()).toString());
                        elemValParam.addElement("Nom").setText(
                                                               p.getNameFromModel());
                        if (parameters.get(par) != null
                            && !((String) parameters.get(par)).equals("")) {
                            elemValParam.addAttribute("valeur",
                                                      (String) parameters.get(par));
                        }
                    }
                }
                datasetElem.addAttribute("id_jeu", "Jeu_"
                                         + new Integer(dataset.getIdBdd()).toString());

                /***** Plugin add XML for DataSet *****/
                int size = listXMLPlugin.size();
                for (int i = 0; i < size; i++) {
                    XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                        .elementAt(i);
                    if (pXMLPrinterPlugin != null)
                        pXMLPrinterPlugin.addXMLElement2DataSet(datasetElem,
                                                                dataset, this, pContexte.getPathAttach());
                }
                /***** (END) Plugin add XML for DataSet *****/

            }
        }
    }

    ArrayList<Test> writeTestInCamp(Element campElem, Campaign pCamp)
        throws Exception {
        ArrayList<Test> testOrdonne = new ArrayList<Test>();
        ArrayList<Family> familyList = pCamp.getFamilyListFromModel();
        if (!familyList.isEmpty()) {
            Element famsElem = campElem.addElement("FamillesCamp");
            Iterator<Family> iter = familyList.iterator();
            while (iter.hasNext()) {
                Family fam = iter.next();
                Element famElem = famsElem.addElement("FamilleRef");
                famElem.addAttribute("ref", "Fam_"
                                     + new Integer(fam.getIdBdd()).toString());
                famElem.addElement("Nom").setText(fam.getNameFromModel());
                ArrayList<TestList> suiteTestList = fam.getSuiteListFromModel();
                if (!suiteTestList.isEmpty()) {
                    Iterator<TestList> iter2 = suiteTestList.iterator();
                    Element suiteTestsElem = famElem
                        .addElement("SuiteTestsCamp");
                    while (iter2.hasNext()) {
                        TestList suitetest = iter2.next();
                        if (pCamp.containsTestListInModel(suitetest)) {
                            Element suiteTestElem = suiteTestsElem
                                .addElement("SuiteTestRef");
                            suiteTestElem.addAttribute("ref", "SuiteTest_"
                                                       + new Integer(suitetest.getIdBdd())
                                                       .toString());
                            suiteTestElem.addElement("Nom").setText(
                                                                    suitetest.getNameFromModel());
                            ArrayList<Test> testList = suitetest
                                .getTestListFromModel();
                            if (!testList.isEmpty()) {
                                Element testsElem = suiteTestElem
                                    .addElement("TestsCamp");
                                Iterator<Test> iter3 = testList.iterator();
                                while (iter3.hasNext()) {
                                    Test test = iter3.next();
                                    if (pCamp.containsTestInModel(test)) {
                                        testOrdonne.add(test);
                                        Element testElem = testsElem
                                            .addElement("TestRef");
                                        testElem.addAttribute("ref", "Test_"
                                                              + new Integer(test.getIdBdd())
                                                              .toString());
                                        testElem.addElement("Nom").setText(
                                                                           test.getNameFromModel());
                                        int userID = pCamp
                                            .getAssignedUserID(test);
                                        if (userID == -1) {
                                            userID = DataModel.getCurrentUser()
                                                .getIdBdd();
                                        }
                                        String login = (String) userLoginByID
                                            .get(new Integer(userID));
                                        if (login != null && !login.equals("")) {
                                            testElem.addAttribute(
                                                                  "loginAssigned", login);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return testOrdonne;
    }

    void writeManualActionResult(Element actionsElem,
                                 ManualExecutionResult pManualExecutionResult, ManualTest pTest)
        throws Exception {
        if (pManualExecutionResult == null) {
            return;
        }
        HashMap<Action, String> actionMap = pManualExecutionResult
            .getActionsMapInModel();
        ArrayList<Action> actionList = pTest.getActionListFromModel(false);
        if (!actionList.isEmpty()) {
            Iterator<Action> it4 = actionList.iterator();
            while (it4.hasNext()) {
                Action action = it4.next();
                if (actionMap.containsKey(action)) {
                    Element actionElem = actionsElem
                        .addElement("ResulActionTest");
                    actionElem.addAttribute("refAction", "Action_"
                                            + new Integer(action.getIdBdd()).toString());
                    Element refAction = actionElem.addElement("RefAction");
                    refAction.addElement("NomAction").setText(
                                                              action.getNameFromModel());
                    refAction.addElement("NomTest").setText(
                                                            pTest.getNameFromModel());
                    refAction.addElement("NomSuite").setText(
                                                             pTest.getTestListFromModel().getNameFromModel());
                    refAction.addElement("NomFamille").setText(
                                                               pTest.getTestListFromModel().getFamilyFromModel()
                                                               .getNameFromModel());
                    String resultat = (String) actionMap.get(action);
                    if (resultat == null || resultat.equals("")) {
                        resultat = "NonRenseigne";
                    }
                    actionElem.addAttribute("res", resultat);
                    if (pManualExecutionResult
                        .getDescriptionResultFromModel(action) != null) {
                        Element test2 = actionElem.addElement("Description");
                        test2.setText(pManualExecutionResult
                                      .getDescriptionResultFromModel(action)
                                      .replaceAll("\n", "\\\\n"));
                    }
                    if (pManualExecutionResult
                        .getAwaitedResultFromModel(action) != null) {
                        actionElem.addElement("ResultAttendu").setText(
                                                                       pManualExecutionResult
                                                                       .getAwaitedResultFromModel(action)
                                                                       .replaceAll("\n", "\\\\n"));
                    }
                    if (pManualExecutionResult
                        .getEffectivResultFromModel(action) != null) {
                        actionElem.addElement("ResulEffectif").setText(
                                                                       pManualExecutionResult
                                                                       .getEffectivResultFromModel(action)
                                                                       .replaceAll("\n", "\\\\n"));
                    }
                }
            }
        }
    }

    void writeExecResultStatus(Element execResElem, Campaign pCamp,
                               Execution pExec, ExecutionResult execRes,
                               ArrayList<Test> testOrdonne) throws Exception {
        execResElem.addAttribute("statut", execRes
                                 .getExecutionStatusFromModel());
        HashMap<Test, ExecutionTestResult> resultTest = execRes
            .getTestsResultMapFromModel();
        if (!resultTest.isEmpty()) {
            Element resElems = execResElem.addElement("ResulExecs");
            if (!testOrdonne.isEmpty()) {
                Iterator<Test> it3 = testOrdonne.iterator();
                while (it3.hasNext()) {
                    Test test = it3.next();
                    if (resultTest.containsKey(test)) {
                        ExecutionTestResult etr = (ExecutionTestResult) resultTest
                            .get(test);
                        Element resElem = resElems.addElement("ResulExec");
                        resElem.addAttribute("refTest",
                                             "Test_"
                                             + new Integer(etr.getTestFromModel()
                                                           .getIdBdd()).toString());
                        Element refTest = resElem.addElement("RefTest");
                        refTest.addElement("NomTest").setText(
                                                              test.getNameFromModel());
                        refTest.addElement("NomSuite").setText(
                                                               test.getTestListFromModel().getNameFromModel());
                        refTest.addElement("NomFamille").setText(
                                                                 test.getTestListFromModel()
                                                                 .getFamilyFromModel()
                                                                 .getNameFromModel());
                        if (etr.getStatusFromModel().equals("PASSED")
                            || etr.getStatusFromModel().equals("FAILED")
                            || etr.getStatusFromModel().equals(
                                                               "INCONCLUSIF")) {
                            resElem.addAttribute("res", etr
                                                 .getStatusFromModel());
                        } else {
                            resElem.addAttribute("res", "NonRenseigne");
                        }
                        HashMap<String, Attachment> attachsExecRes = etr
                            .getAttachmentMapFromModel();
                        addAttach(resElem, pContexte.getPathAttach()
                                  + File.separator + "Attachements"
                                  + File.separator + "Campagnes" + File.separator
                                  + pCamp.getIdBdd() + File.separator
                                  + pExec.getIdBdd() + File.separator
                                  + execRes.getIdBdd() + File.separator
                                  + etr.getTestFromModel().getIdBdd(),
                                  attachsExecRes, "Attachements" + File.separator
                                  + "Campagnes" + File.separator
                                  + pCamp.getIdBdd() + File.separator
                                  + pExec.getIdBdd() + File.separator
                                  + execRes.getIdBdd() + File.separator
                                  + etr.getTestFromModel().getIdBdd());
                        /***** Plugin add XML for ExecResul *****/
                        int size = listXMLPlugin.size();
                        for (int i = 0; i < size; i++) {
                            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                                .elementAt(i);
                            if (pXMLPrinterPlugin != null)
                                pXMLPrinterPlugin
                                    .addXMLElement2ResTestExecution(
                                                                    resElem, etr, test, this,
                                                                    pContexte.getPathAttach());
                        }
                        /***** (END) Plugin add XML for ExecResul *****/
                    }
                }
            }
        }
    }

    void writeExecResults(Element execElem, Campaign pCamp, Execution pExec,
                          ArrayList<Test> testOrdonne) throws Exception {
        ArrayList<ExecutionResult> resulExecList = pExec
            .getExecutionResultListFromModel();
        if (!resulExecList.isEmpty()) {
            Element resulExecsElem = execElem.addElement("ResulExecCampTests");
            Iterator<ExecutionResult> it2 = resulExecList.iterator();
            while (it2.hasNext()) {
                ExecutionResult execRes = it2.next();
                writeExecResult(resulExecsElem, pCamp, pExec, execRes,
                                testOrdonne);
            }
        }
    }

    void writeExecResult(Element resulExecsElem, Campaign pCamp,
                         Execution pExec, ExecutionResult execRes,
                         ArrayList<Test> testOrdonne) throws Exception {
        Element execResElem = resulExecsElem.addElement("ResulExecCampTest");
        execResElem.addAttribute("id_exec_res", "ResExecCamp_"
                                 + new Integer(execRes.getIdBdd()).toString());
        if (execRes.getTesterFromModel() != null
            && !execRes.getTesterFromModel().equals("")) {
            execResElem.addElement("Testeur").setText(
                                                      execRes.getTesterFromModel());
        }
        if (!execRes.getNameFromModel().equals("")) {
            execResElem.addElement("Nom").setText(execRes.getNameFromModel());
        }
        if (execRes.getDescriptionFromModel() != null
            && !execRes.getDescriptionFromModel().equals("")) {
            execResElem.addElement("Description")
                .setText(
                         execRes.getDescriptionFromModel().replaceAll("\n",
                                                                      "\\\\n"));
        }
        if (execRes.getExecutionDateFromModel() != null) {
            String date_exec = DateFormat.getDateInstance(DateFormat.MEDIUM,
                                                          Locale.FRANCE).format(execRes.getExecutionDateFromModel());
            execResElem.addElement("Date_crea").setText(date_exec);
        }
        if (execRes.getTimeFromModel() != null) {
            execResElem.addElement("Heure_crea").setText(
                                                         execRes.getTimeFromModel().toString());
        }
        HashMap<String, Attachment> attachRes = execRes
            .getAttachmentMapFromModel();
        addAttach(execResElem, pContexte.getPathAttach() + File.separator
                  + "Attachements" + File.separator + "Campagnes"
                  + File.separator + pCamp.getIdBdd() + File.separator
                  + pExec.getIdBdd() + File.separator + execRes.getIdBdd(),
                  attachRes, "Attachements" + File.separator + "Campagnes"
                  + File.separator + pCamp.getIdBdd() + File.separator
                  + pExec.getIdBdd() + File.separator
                  + execRes.getIdBdd());
        writeExecResultStatus(execResElem, pCamp, pExec, execRes, testOrdonne);

        /***** Plugin add XML for ExecResul *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2ResExecution(execResElem,
                                                             execRes, this, pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for ExecResul *****/

        Element actionsElem = null;
        boolean elementActionWrited = false;
        if (!testOrdonne.isEmpty()) {
            Iterator<Test> it3 = testOrdonne.iterator();
            while (it3.hasNext()) {
                Test test = it3.next();
                ExecutionTestResult pExecutionTestResult = execRes
                    .getExecutionTestResultFromModel(test);
                if (test instanceof ManualTest) {
                    if (!elementActionWrited) {
                        actionsElem = execResElem
                            .addElement("ResulActionTests");
                        elementActionWrited = true;
                    }
                    // System.out.println("Write result for "
                    // +execRes.getNameFromModel() + ", exec : " +
                    // execRes.getExecution().getNameFromModel() + ", camp :" +
                    // execRes.getExecution().getCampagneFromModel().getNameFromModel());
                    writeManualActionResult(actionsElem,
                                            (ManualExecutionResult) pExecutionTestResult,
                                            ((ManualTest) test));
                }
            }
        }
    }

    void writePreScript(Element execElem, Campaign pCamp, Execution exec)
        throws Exception {
        writeScriptExec(execElem, "ScriptInitialisation", pCamp, exec, exec
                        .getPreScriptFromModel());
    }

    void writePostScript(Element execElem, Campaign pCamp, Execution exec)
        throws Exception {
        writeScriptExec(execElem, "ScriptRestitution", pCamp, exec, exec
                        .getPostScriptFromModel());
    }

    /**
     * @param execElem
     * @param type
     *            ScriptInitialisation ou ScriptRestitution
     * @param pCamp
     * @param exec
     * @param pathAttach
     * @param ficType
     */
    void writeScriptExec(Element execElem, String type, Campaign pCamp,
                         Execution exec, Script pScript) throws Exception {
        Element scriptInitElem = execElem.addElement("Script");
        if (pScript.getScriptExtensionFromModel() != null) {
            scriptInitElem.addElement("Classpath").setText(
                                                           pScript.getScriptExtensionFromModel());
        }
        if (pScript.getPlugArgFromModel() != null
            && !pScript.getPlugArgFromModel().equals("")) {
            scriptInitElem.addElement("ArgScript").setText(
                                                           pScript.getPlugArgFromModel());
        }
        if (!pScript.getTypeFromModel().equals("")) {
            scriptInitElem.addAttribute("type", pScript.getTypeFromModel());
        }
        scriptInitElem.addAttribute("nom", pScript.getNameFromModel());
        String dest = new String(pContexte.getPathAttach() + File.separator
                                 + "Attachements" + File.separator + "Campagnes"
                                 + File.separator + formater(pCamp.getNameFromModel())
                                 + File.separator + formater(exec.getNameFromModel())
                                 + File.separator + type);
        scriptInitElem.addAttribute("dir", "Attachements/Campagnes/"
                                    + formater(pCamp.getNameFromModel()) + "/"
                                    + formater(exec.getNameFromModel()) + "/" + type + "/"
                                    + formater(pScript.getNameFromModel()));
        try {
            pScript.getFileFromDB(dest);
            String extension = pScript.getNameFromModel().substring(
                                                                    pScript.getNameFromModel().lastIndexOf("."));
            if (pContexte.getFicType() != null) {
                for (int i = 0; i < pContexte.getFicType().length; i++) {
                    if (pContexte.getFicType()[i].indexOf(".") != -1
                        && extension.equals(pContexte.getFicType()[i]
                                            .substring(pContexte.getFicType()[i]
                                                       .lastIndexOf(".")))) {
                        Element text = scriptInitElem.addElement("Text");
                        if (pContexte.getOutputFormat() == OutFormat.DOCBOOK) {
                            ajouterScript(text, pContexte.getPathAttach()
                                          + File.separator + "Attachements"
                                          + File.separator + "Campagnes"
                                          + File.separator
                                          + formater(pCamp.getNameFromModel())
                                          + File.separator
                                          + formater(exec.getNameFromModel())
                                          + File.separator + type + File.separator
                                          + formater(pScript.getNameFromModel()));
                        } else {
                            ajouterTexte(text, pContexte.getPathAttach()
                                         + File.separator + "Attachements"
                                         + File.separator + "Campagnes"
                                         + File.separator
                                         + formater(pCamp.getNameFromModel())
                                         + File.separator
                                         + formater(exec.getNameFromModel())
                                         + File.separator + type + File.separator
                                         + formater(pScript.getNameFromModel()));
                        }
                    }
                }
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    void writeExecutions(Element campElem, Campaign pCamp,
                         ArrayList<Test> testOrdonne, int nbTest2, boolean filtre,
                         ArrayList<Execution> selectExecList) throws Exception {
        ArrayList<Execution> execList = pCamp.getExecutionListFromModel();
        if (!execList.isEmpty() || (filtre && !selectExecList.isEmpty())) {
            Element execsElem = campElem.addElement("ExecCampTests");
            Iterator<Execution> iter = execList.iterator();
            while (iter.hasNext()) {
                Execution exec = (Execution) iter.next();
                if (!filtre || selectExecList.contains(exec)) {
                    writeExecution(execsElem, pCamp, exec, testOrdonne,
                                   nbTest2, filtre);
                }
            }
        }
    }

    void writeExecution(Element execsElem, Campaign pCamp, Execution exec,
                        ArrayList<Test> testOrdonne, int nbTest2, boolean filtre)
        throws Exception {
        Element execElem = execsElem.addElement("ExecCampTest");
        execElem.addElement("Nom").setText(exec.getNameFromModel());
        Element nbGraph = execElem.addElement("NbGraph");
        nbGraph.setText(new Integer((nbTest2 - 1) / 13).toString());
        execElem.addAttribute("id_exec_camp", "ExecCamp_"
                              + new Integer(exec.getIdBdd()).toString());
        if (exec.getDescriptionFromModel() != null
            && !exec.getDescriptionFromModel().equals("")) {
            execElem.addElement("Description").setText(
                                                       exec.getDescriptionFromModel().replaceAll("\n", "\\\\n"));
        }
        Element envRefElem = execElem.addElement("EnvironnementEx");
        envRefElem.addAttribute("ref", "Env_"
                                + new Integer(exec.getEnvironmentFromModel().getIdBdd())
                                .toString());
        envRefElem.addElement("Nom").setText(
                                             exec.getEnvironmentFromModel().getNameFromModel());
        if (exec.getDataSetFromModel() != null
            && (exec.getDataSetFromModel().getIdBdd() != -1)) {
            Element refJeu = execElem.addElement("JeuDonneesEx");
            refJeu.addAttribute("ref", "Jeu_"
                                + new Integer(exec.getDataSetFromModel().getIdBdd())
                                .toString());
            refJeu.addElement("Nom").setText(
                                             exec.getDataSetFromModel().getNameFromModel());
        }
        if (exec.getPreScriptFromModel() != null) {
            writePreScript(execElem, pCamp, exec);
        }
        if (exec.getPostScriptFromModel() != null) {
            writePostScript(execElem, pCamp, exec);
        }

        HashMap<String, Attachment> attachExec = exec
            .getAttachmentMapFromModel();
        addAttach(execElem, pContexte.getPathAttach() + File.separator
                  + "Attachements" + File.separator + "Campagnes"
                  + File.separator + pCamp.getIdBdd() + File.separator
                  + exec.getIdBdd(), attachExec, "Attachements" + File.separator
                  + "Campagnes" + File.separator + pCamp.getIdBdd()
                  + File.separator + exec.getIdBdd());

        /***** Plugin add XML for Execution *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2Execution(execElem, exec, this,
                                                          pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for Execution *****/

        writeExecResults(execElem, pCamp, exec, testOrdonne);
    }

    void writeCampTest(Element campsElem, Campaign pCamp, int nbTest2,
                       boolean filtre, ArrayList<Execution> selectExecList)
        throws Exception {
        Element campElem = campsElem.addElement("CampagneTest");
        campElem.addAttribute("id_camp", "Camp_"
                              + new Integer(pCamp.getIdBdd()).toString());
        Element conceptor = campElem.addElement("Concepteur");
        if (pCamp.getConceptorFroModel() != null
            && !pCamp.getConceptorFroModel().equals("")) {
            conceptor.addElement("Nom").setText(pCamp.getConceptorFroModel());
        }
        campElem.addElement("Nom").setText(pCamp.getNameFromModel());
        String date_camp = DateFormat.getDateInstance(DateFormat.MEDIUM,
                                                      Locale.FRANCE).format(pCamp.getDateFromModel());
        campElem.addElement("Date_crea").setText(date_camp);
        if (!pCamp.getDescriptionFromModel().equals("")) {
            addHTMLDescription(campElem, pCamp.getDescriptionFromModel());
        }
        HashMap<String, Attachment> attachs = pCamp.getAttachmentMapFromModel();
        addAttach(campElem, pContexte.getPathAttach() + File.separator
                  + "Attachements" + File.separator + "Campagnes"
                  + File.separator + pCamp.getIdBdd(), attachs, "Attachements"
                  + File.separator + "Campagnes" + File.separator
                  + pCamp.getIdBdd());

        /***** Plugin add XML for Campaing *****/
        int size = listXMLPlugin.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugin
                .elementAt(i);
            if (pXMLPrinterPlugin != null)
                pXMLPrinterPlugin.addXMLElement2Campaign(campElem, pCamp, this,
                                                         pContexte.getPathAttach());
        }
        /***** (END) Plugin add XML for Execution *****/

        /*** DataSet ***/
        writeDataSet(campElem, pCamp);

        /*** Test in Campaign ***/
        ArrayList<Test> testOrdonne = writeTestInCamp(campElem, pCamp);

        /*** Execution in Campaign ***/
        writeExecutions(campElem, pCamp, testOrdonne, nbTest2, filtre,
                        selectExecList);

    }

    /**
     * Methode de creation du document xml contenant la description de la partie
     * statique du projet Salome courant
     *
     * @param pathAttach
     *            repertoire pour la sauvegarde des attachements
     * @param selectFamilyList
     *            liste des familles de tests selectionnees pour figurer dans le
     *            rapport
     * @param selectSuiteList
     *            liste des suites de tests selectionnees pour figurer dans le
     *            rapport
     * @param selectTestList
     *            liste des tests selectionnes pour figurer dans le rapport
     * @param ficType
     *            liste des types de fichier a inserer dans le document
     */
    public Document toDocument(ArrayList<Family> selectFamilyList,
                               ArrayList<TestList> selectSuiteList, ArrayList<Test> selectTestList)
        throws Exception {
        boolean filtre = (selectFamilyList != null || selectSuiteList != null || selectTestList != null);

        if (!filtre) {
            selectFamilyList = new ArrayList<Family>();
            selectSuiteList = new ArrayList<TestList>();
            selectTestList = new ArrayList<Test>();
        }
        Document document = DocumentHelper.createDocument();
        if (addDocType) {
            File fileSalomeStatique = new File(pContexte.getPathAttach()
                                               + File.separator + "SalomeStatique.dtd");
            try {
                document.addDocType("SalomeStatique", null, fileSalomeStatique
                                    .toURL().toString());
            } catch (Exception e) {
                e.printStackTrace();
                document.addDocType("SalomeStatique", null, pContexte
                                    .getPathAttach()
                                    + File.separator + "SalomeStatique.dtd");
            }
        }
        Element salomeStatique = document.addElement("SalomeStatique");
        Element projetVT = salomeStatique.addElement("ProjetVT");

        /***** USER/GROUP ******/
        writeBaseProjet(projetVT);

        /***** ENVIRONNEMENT ******/
        writeEnvironnement(projetVT);

        /***** FAMILY/SUITE/TEST ******/
        writeFamily(projetVT, filtre, selectFamilyList, selectSuiteList,
                    selectTestList);

        return document;
    }

    /**
     * Methode qui ajoute dans le document resultant le texte contenu dans le
     * fichier dont le chemin est indique en parametre
     *
     * @param text
     *            element auquel on rajoute lengthcontenu du fichier
     * @param path
     *            chemin du fichier dont le contenu doit etre ajoute au document
     */
    public void ajouterTexte(Element text, String path) throws Exception {
        File fichier = new File(path);
        FileReader fluxFichier = null;
        try {
            fluxFichier = new FileReader(fichier);
            BufferedReader tamponLecture = new BufferedReader(fluxFichier);
            String ligne;
            while ((ligne = tamponLecture.readLine()) != null) {
                text.addElement("ligne").setText(ligne);
            }
            tamponLecture.close();
            fluxFichier.close();
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    void ajouterInfoExecToTest(Test pTest, Element testElem) throws Exception {
        boolean executed = false;
        ArrayList<Campaign> campaignList = DataModel.getCurrentProject()
            .getCampaignOfTest(pTest);
        if (campaignList != null && campaignList.size() > 0) {
            int i = 0;
            int size = campaignList.size();
            SalomeTMFPanels.getAutomaticButtonCampaignDetails()
                .setEnabled(true);
            while (i < size && !executed) {
                Campaign pCampaign = campaignList.get(i);
                if (pCampaign.containsExecutionResultInModel()) {
                    executed = true;
                }
                i++;
            }
        }
        testElem.addElement("Executed").setText("" + executed);
    }

    /**
     * Methode de creation du document xml contenant la description de la partie
     * dynamique du projet Salome courant
     *
     * @param pathAttach
     *            repertoire pour la sauvegarde des attachements
     * @param selectCampList
     *            liste des campagnes de tests selectionnees pour figurer dans
     *            le rapport
     * @param selectExecList
     *            liste des executions selectionnees pour figurer dans le
     *            rapport
     * @param ficType
     *            liste des types de fichier a inserer dans le document
     * @param tab
     *            tab[3] -> inclusion des executions ; tab[4] -> inclusion des
     *            resultats d'executions
     */
    public Document toDocumentDyna(ArrayList<Campaign> selectCampList,
                                   ArrayList<Execution> selectExecList) throws Exception {
        Document doc = toDocument(null, null, null);
        boolean filtre = (selectCampList != null || selectExecList != null);
        if (!filtre) {
            selectCampList = new ArrayList<Campaign>();
            selectExecList = new ArrayList<Execution>();
        }
        int nbTest2 = 0;
        if (addDocType) {
            File fileSalomeStatique = new File(pContexte.getPathAttach()
                                               + File.separator + "SalomeDynamique.dtd");
            try {
                doc.addDocType("SalomeStatique", null, fileSalomeStatique
                               .toURL().toString());
            } catch (Exception e) {
                doc.addDocType("SalomeStatique", null, pContexte
                               .getPathAttach()
                               + File.separator + "SalomeStatique.dtd");
            }
        }
        Element root = doc.getRootElement();
        root.setName("SalomeDynamique");

        ArrayList<Campaign> campList = DataModel.getCurrentProject()
            .getCampaignListFromModel();
        if (!campList.isEmpty() || (filtre && !selectCampList.isEmpty())) {
            Element nodeProj = (Element) doc.selectSingleNode("//ProjetVT");

            Element campsElem = nodeProj.addElement("CampagneTests");
            Iterator<Campaign> it = campList.iterator();
            while (it.hasNext()) {
                nbTest2 = 0;
                Campaign camp = it.next();
                if (!filtre || selectCampList.contains(camp)) {
                    ArrayList<Test> tList2 = camp.getTestListFromModel();
                    if (!tList2.isEmpty()) {
                        Iterator<Test> itList2 = tList2.iterator();
                        while (itList2.hasNext()) {
                            itList2.next();
                            nbTest2 = nbTest2 + 1;
                        }
                    }
                    writeCampTest(campsElem, camp, nbTest2, filtre,
                                  selectExecList);
                }
            }
        }
        return doc;
    }

    /**
     * Methode qui ajoute les attachements a l'element elem passe en parametre,
     * copie le fichier d'attachement a l'emplacement specifie par le parametre
     * path et suivant le tableau ficType inclut le contenu des fichiers dans le
     * document xml
     *
     * @param elem
     *            element auquel sont ajoutes les attachements
     * @param path
     *            chemin ou sont sauvegarde les attachements
     * @param attachs
     *            HashMap qui contient les attachements
     * @param relativePath
     *            chemin relatif
     * @param ficType
     *            indiquant les extensions des fichiers a inclure dans le
     *            document
     */
    public void addAttach(Element elem, String path,
                          HashMap<String, Attachment> attachs, String relativePath)
        throws Exception {
        if (!attachs.isEmpty()) {
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Element attachsElem = elem.addElement("Attachements");
            Iterator<Attachment> ita = attachs.values().iterator();
            while (ita.hasNext()) {
                Attachment at = (Attachment) ita.next();
                if (at instanceof UrlAttachment) {
                    Element urlElem = attachsElem.addElement("UrlAttachement");
                    if (at.getDescriptionFromModel() != null
                        && !at.getDescriptionFromModel().equals("")) {
                        urlElem.addElement("Description").setText(
                                                                  at.getDescriptionFromModel().replaceAll("\n",
                                                                                                          "\\\\n"));
                    }
                    urlElem.addAttribute("url", at.getNameFromModel());
                } else {
                    FileAttachment fa = (FileAttachment) at;
                    Element fileElem = attachsElem
                        .addElement("FileAttachement");
                    if (fa.getDate() != null) {
                        // a modifier pour formater la date
                        fileElem.addElement("Date").setText(
                                                            fa.getDate().toString());
                    }
                    if (fa.getDescriptionFromModel() != null
                        && !fa.getDescriptionFromModel().equals("")) {
                        fileElem.addElement("Description").setText(
                                                                   fa.getDescriptionFromModel().replaceAll("\n",
                                                                                                           "\\\\n"));
                    }
                    fileElem.addAttribute("nom", at.getNameFromModel());
                    String relativePathURI = relativePath;
                    if (File.separator.equals("\\")) {
                        relativePathURI = relativePath.replaceAll("\\\\", "/");
                    }
                    String destElem = new String(relativePathURI + "/"
                                                 + formater(at.getNameFromModel()));
                    fileElem.addAttribute("dir", destElem);
                    String dest = new String(path + File.separator
                                             + formater(at.getNameFromModel()));
                    dest = dest.substring(0, dest.lastIndexOf(File.separator));
                    try {
                        ((FileAttachment) at).getFileFromDB(dest);

                        String extension = null;
                        try {
                            extension = at.getNameFromModel().substring(
                                                                        at.getNameFromModel().lastIndexOf("."));
                        } catch (Exception ex) {

                        }
                        if (pContexte.getFicType() != null && extension != null) {
                            for (int i = 0; i < pContexte.getFicType().length; i++) {
                                if (pContexte.getFicType()[i].indexOf(".") != -1
                                    && extension.equals(pContexte
                                                        .getFicType()[i]
                                                        .substring(pContexte
                                                                   .getFicType()[i]
                                                                   .lastIndexOf(".")))) {
                                    Element text = fileElem.addElement("Text");
                                    ajouterTexte(text, path + File.separator
                                                 + formater(at.getNameFromModel()));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    /**
     * Methode pour ecrire le document dom4j dans un fichier
     *
     * @param doc
     *            le document dom4j
     * @param path
     *            chemin et nom du fichier a creer
     */
    public void documentToXML(Document doc, String path) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding(pContexte.getCoding());
        XMLWriter writer = new XMLWriter(fileOutputStream, format);
        writer.write(doc);
        writer.close();
    }

    /**
     * Methode pour le formatage des noms de fichiers
     *
     */
    public String formater(String s) throws Exception {
        s = s.replace('/', '_');
        s = s.replace('\\', '_');
        s = s.replace(':', '_');
        s = s.replace('*', '_');
        s = s.replace('?', '_');
        s = s.replace('\"', '_');
        s = s.replace('<', '_');
        s = s.replace('>', '_');
        s = s.replace('|', '_');
        return s;
    }

    public Vector<XMLPrinterPlugin> getListXMLPlugin() {
        return listXMLPlugin;
    }

}
