/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Aurore PENAULT, Marche Mikael
 *
 * Contact: mikael.marche@orange-ft.com
 */

package salomeTMF_plug.docXML.export;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.OutputURIResolver;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;

import salomeTMF_plug.docXML.languages.Language;

public class XSLGenerator {

    private Vector listXMLPlugins;

    private boolean dynamicMode = false;

    private String translatePath;

    public XSLGenerator(Vector listXMLPlugins, boolean dynamicMode,
                        String translatePath) {
        this.listXMLPlugins = listXMLPlugins;
        this.dynamicMode = dynamicMode;
        this.translatePath = translatePath;
    }

    /**
     * Methode pour effectuer une transformation xslt
     *
     * @param multiFrame
     *            true if the generated document is in HTML multi frame mode
     * @param srcXml
     *            document xml source pour la transformation
     * @param srcXslt
     *            document xslt pour la transformation
     * @param outHtml
     *            fichier dans lequel doit etre mis lengthresultat de la
     *            transformation
     * @param jpegImg
     *            si true, insertion des fichiers jpeg dans le document html
     * @param gifImg
     *            si true, insertion des fichiers gif dans le document html
     * @param pngImg
     *            si true, insertion des fichiers png dans le document html
     * @param chosenChaptersList
     * @param urlTranslate
     */
    public void transform(boolean multiFrame, boolean htmlFormat,
                          String srcXml, String url_txt, String srcXslt, String outHtml,
                          boolean jpegImg, boolean gifImg, boolean pngImg, String destDir,
                          ArrayList chosenChaptersList) throws Exception {

        File newMainXslt = resolveImport(url_txt, srcXslt, multiFrame,
                                         htmlFormat);

        importTranslate(translatePath);

        // to use the Saxon XSLT processor
        System.setProperty("javax.xml.transform.TransformerFactory",
                           "net.sf.saxon.TransformerFactoryImpl");
        StreamSource styleSource = new StreamSource(newMainXslt.toURI().toURL()
                                                    .openStream());
        TransformerFactory factory = TransformerFactory.newInstance();
        String outputDir = outHtml.substring(0, outHtml
                                             .lastIndexOf(File.separator) + 1);
        factory.setAttribute(net.sf.saxon.FeatureKeys.OUTPUT_URI_RESOLVER,
                             new UserOutputResolver(outputDir));
        Transformer x = null;
        Util.log("[XmlGenerator:transform] create TransformerFactory");
        x = factory.newTransformer(styleSource);

        Util.log("[XmlGenerator:transform] set parameter");

        if (jpegImg) {
            Util.log("\t jpeg = true");
            x.setParameter("jpeg", "1");
        }
        if (gifImg) {
            Util.log("\t gif = true");
            x.setParameter("gif", "1");
        }
        if (pngImg) {
            Util.log("\t png = true");
            x.setParameter("png", "1");
        }
        if (!htmlFormat) {
            String urlBase = new File(outputDir).toURI().toString();
            Util.log("\t urlBase = " + urlBase);
            x.setParameter("urlBase", urlBase);
        }
        Iterator it = chosenChaptersList.iterator();
        while (it.hasNext()) {
            String chapterName = (String) it.next();
            if (chapterName.equals(Language.getInstance().getText("Projet"))) {
                Util.log("\t project = true");
                x.setParameter("project", "1");
            } else if (chapterName.equals(Language.getInstance().getText(
                                                                         "Environnements"))) {
                Util.log("\t environments = true");
                x.setParameter("environments", "1");
            } else if (chapterName.equals(Language.getInstance().getText(
                                                                         "Dossier_de_tests"))) {
                Util.log("\t testplan = true");
                x.setParameter("testplan", "1");
            } else if (chapterName.equals(Language.getInstance().getText(
                                                                         "Campagnes_du_projet"))) {
                Util.log("\t campaigns = true");
                x.setParameter("campaigns", "1");
            } else if (chapterName.equals(Language.getInstance().getText(
                                                                         "Rapport_d_execution"))) {
                Util.log("\t executionreport = true");
                x.setParameter("executionreport", "1");
            }
            int size = listXMLPlugins.size();
            for (int i = 0; i < size; i++) {
                XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugins
                    .elementAt(i);
                if (pXMLPrinterPlugin != null) {
                    String paramName = pXMLPrinterPlugin
                        .getParameterName(chapterName);
                    if (paramName != null) {
                        Util.log("\t " + paramName + " = true");
                        x.setParameter(paramName, "1");
                    }
                }
            }
        }
        Util.log("\t local = " + Api.getUsedLocale());
        x.setParameter("local", Api.getUsedLocale());
        String urlTranslate = new File(translatePath).toURL().toString();
        x.setParameter("translate", urlTranslate);

        /* Utilise pour que les xsl trouve translate.xml */
        System.setProperty("user.dir", System.getProperties().getProperty(
                                                                          "java.io.tmpdir"));

        if (multiFrame) {
            String frame1Param = outHtml.substring(outHtml
                                                   .lastIndexOf(File.separator) + 1, outHtml.lastIndexOf("."))
                + "_sommaire.html";
            String frame2Param = outHtml.substring(outHtml
                                                   .lastIndexOf(File.separator) + 1, outHtml.lastIndexOf("."))
                + "_princ.html";
            Util.log("\t frame1 = " + frame1Param);
            Util.log("\t frame2 = " + frame2Param);
            x.setParameter("frame1", frame1Param);
            x.setParameter("frame2", frame2Param);
        }
        FileOutputStream fileOut = null;
        try {
            x.setOutputProperty(OutputKeys.INDENT, "yes");
            Util.log("[XmlGenerator:transform] get source file : " + srcXml);
            Source source = new StreamSource(new File(srcXml));
            Result target = null;
            Util.log("[XmlGenerator:transform] set target : " + outHtml);
            fileOut = new FileOutputStream(outHtml);

            target = new javax.xml.transform.stream.StreamResult(fileOut);
            Util.log("[XmlGenerator:transform] do transformation ");
            x.transform(source, target);
            fileOut.close();
        } catch (Exception e) {
            if (fileOut != null) {
                fileOut.close();
            }
            throw e;
        }
    }

    /**
     * Import translations from plugins
     *
     * @param urlTranslate
     * @throws Exception
     */
    private void importTranslate(String urlTranslate) throws Exception {
        File mainTranslateFile = new File(urlTranslate);
        int size = listXMLPlugins.size();
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugins
                .elementAt(i);
            if (pXMLPrinterPlugin != null) {
                File translateFileToImport = pXMLPrinterPlugin
                    .getTranslationFile();
                if (translateFileToImport != null) {
                    importTranslation(mainTranslateFile, translateFileToImport);
                }
            }
        }
    }

    /**
     * Import translation file from plugins in the main translation file
     *
     * @param mainTranslationFile
     *            the main translation file
     * @param translationFileToImport
     *            translation file from plugin to import
     * @throws Exception
     */
    private void importTranslation(File mainTranslationFile,
                                   File translationFileToImport) throws Exception {
        SAXReader reader = new SAXReader();
        Document mainDoc = reader.read(mainTranslationFile);
        Document importDoc = reader.read(translationFileToImport);
        List importTemplateList = importDoc.selectNodes("//headings");
        Iterator itImport = importTemplateList.iterator();
        while (itImport.hasNext()) {
            Element headingsImportElem = (Element) itImport.next();
            String language = headingsImportElem.attributeValue("lang");
            Element mainHeadingsElem = (Element) mainDoc
                .selectSingleNode("//headings[@xml:lang='" + language
                                  + "']");
            List children = headingsImportElem.elements();
            Iterator elemIt = children.iterator();
            while (elemIt.hasNext()) {
                Element elemToAdd = ((Element) elemIt.next()).createCopy();
                String category = elemToAdd.attributeValue("category");
                if (mainDoc.getRootElement().selectSingleNode(
                                                              "//headings[@xml:lang='" + language
                                                              + "']/heading[@category='" + category + "']") == null) {
                    mainHeadingsElem.add(elemToAdd);
                }
            }
        }
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(new FileOutputStream(
                                                              mainTranslationFile), format);
        writer.write(mainDoc);
    }

    /**
     * Modify the XSLT file to include XSL files from plugins
     *
     * @param url_txt
     * @param srcXslt
     * @param multiFrame
     */
    private File resolveImport(String url_txt, String srcXslt,
                               boolean multiFrame, boolean htmlFormat) throws Exception {
        int size = listXMLPlugins.size();
        File temporaryFile = File.createTempFile("oldMain", ".xsl");
        String temporaryFilePath = temporaryFile.getAbsolutePath();
        try {
            URL xsltURL = new URL(url_txt + srcXslt);
            Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
        } catch (Exception e) {
            Tools.writeFile(XSLGenerator.class.getResourceAsStream("/salome/"
                                                                   + srcXslt), temporaryFilePath);
        }

        File newMainXslt = null;
        for (int i = 0; i < size; i++) {
            XMLPrinterPlugin pXMLPrinterPlugin = (XMLPrinterPlugin) listXMLPlugins
                .elementAt(i);
            if (pXMLPrinterPlugin != null) {
                File xsltFileToImport = pXMLPrinterPlugin.getXSLToImport(
                                                                         dynamicMode, multiFrame, htmlFormat);
                if (xsltFileToImport != null) {
                    newMainXslt = importXSLT(temporaryFile, xsltFileToImport);
                }
                copy(newMainXslt, temporaryFile);
            }

        }
        if (newMainXslt == null) {
            return temporaryFile;
        }
        return newMainXslt;
    }

    public void copy(File source, File destination) throws Exception {
        FileInputStream sourceFile = null;
        FileOutputStream destinationFile = null;
        destination.createNewFile();
        sourceFile = new java.io.FileInputStream(source);
        destinationFile = new java.io.FileOutputStream(destination);
        byte buffer[] = new byte[512 * 1024];
        int nbLecture;
        while ((nbLecture = sourceFile.read(buffer)) != -1) {
            destinationFile.write(buffer, 0, nbLecture);
        }
        sourceFile.close();
        destinationFile.close();
    }

    /**
     * Import an XSLT file from a plugin in the main XSLT file
     *
     * @param mainXsltFile
     *            the main XSLT file
     * @param xsltFileToImport
     *            the XSLT file to import
     * @throws DocumentException
     */
    private File importXSLT(File mainXsltFile, File xsltFileToImport)
        throws Exception {
        SAXReader reader = new SAXReader();
        Document mainDoc = reader.read(mainXsltFile);
        Document importDoc = reader.read(xsltFileToImport);
        Element mainRootElem = mainDoc.getRootElement();
        List importParamList = importDoc
            .selectNodes("/xsl:stylesheet/xsl:param");
        Iterator it = importParamList.iterator();
        while (it.hasNext()) {
            Element elemToAdd = ((Element) it.next()).createCopy();
            mainRootElem.add(elemToAdd);
        }
        List importVariableList = importDoc
            .selectNodes("/xsl:stylesheet/xsl:variable");
        it = importVariableList.iterator();
        while (it.hasNext()) {
            Element elemToAdd = ((Element) it.next()).createCopy();
            mainRootElem.add(elemToAdd);
        }
        List importTemplateList = importDoc.selectNodes("//xsl:template");
        it = importTemplateList.iterator();
        while (it.hasNext()) {
            Element importTemplateElem = (Element) it.next();
            String importTemplateName = importTemplateElem
                .attributeValue("name");
            if (importTemplateName != null) {
                Element mainElem = (Element) mainDoc
                    .selectSingleNode("//xsl:template[@name='"
                                      + importTemplateName + "']");
                if (mainElem != null) {
                    // add children of importTemplateElem to mainElem
                    List children = importTemplateElem.elements();
                    Iterator elemIt = children.iterator();
                    while (elemIt.hasNext()) {
                        Element elemToAdd = ((Element) elemIt.next())
                            .createCopy();
                        if (!elemToAdd.getName().equals("param"))
                            mainElem.add(elemToAdd);
                    }
                } else {
                    Element elemToAdd = importTemplateElem.createCopy();
                    mainRootElem.add(elemToAdd);
                }
            } else {
                // add at the end of the main stylesheet
                Element elemToAdd = importTemplateElem.createCopy();
                mainRootElem.add(elemToAdd);
            }
        }
        List importFunctionList = importDoc
            .selectNodes("/xsl:stylesheet/xsl:function");
        it = importFunctionList.iterator();

        while (it.hasNext()) {
            Element elemToAdd = ((Element) it.next()).createCopy();
            mainRootElem.add(elemToAdd);
        }
        // write the main document in a temporary file
        File temp = File.createTempFile("main", ".xsl");

        // Delete temp file when program exits.
        temp.deleteOnExit();

        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(new FileOutputStream(temp), format);
        writer.write(mainDoc);

        return temp;
    }

    /**
     * Methode qui permet d'ajouter soit une page de garde soit une en-tete soit
     * un pied de page
     *
     * @param pathHtml
     *            document Html auquel il faut ajouter la mise en page
     * @param pathAdd
     *            document Html contenant la mise en page
     * @param tete
     *            true->en-tete ou page de garde, false->pied de page
     */
    public void addTitlePage(String pathHtml, String pathAdd, boolean tete)
        throws Exception {
        SAXReader reader = new SAXReader(false);
        reader.setIncludeExternalDTDDeclarations(false);
        reader.setIncludeInternalDTDDeclarations(false);
        Document doc = reader.read(new FileInputStream(new File(pathHtml)));
        SAXReader reader2 = new SAXReader(false);
        reader.setIncludeExternalDTDDeclarations(false);
        reader.setIncludeInternalDTDDeclarations(false);
        Document add = reader2.read(new FileInputStream(new File(pathAdd)));

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("html");
        Element head = ((Element) doc.selectSingleNode("//head")).createCopy();
        root.add(head);
        Element body = root.addElement("body");
        if (tete) {
            ArrayList listElement = (ArrayList) add
                .selectNodes("//body/child::*");
            if (!listElement.isEmpty()) {
                Iterator it = listElement.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
            ArrayList bodyElems = (ArrayList) doc
                .selectNodes("//body/child::*");
            if (!bodyElems.isEmpty()) {
                Iterator it = bodyElems.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
        } else {
            ArrayList bodyElems = (ArrayList) doc
                .selectNodes("//body/child::*");
            if (!bodyElems.isEmpty()) {
                Iterator it = bodyElems.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
            body.addElement("hr");
            ArrayList listElement = (ArrayList) add
                .selectNodes("//body/child::*");
            if (!listElement.isEmpty()) {
                Iterator it = listElement.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
        }
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("iso-8859-1");
        XMLWriter writer;
        Writer outputWriter = new BufferedWriter(new FileWriter(new File(
                                                                         pathHtml)));
        writer = new XMLWriter(outputWriter, format);
        writer.write(document);
        writer.close();
    }

    /**
     * Methode qui ajoute la mise en forme si boolean tete = true : ajoute un
     * en-tete sinon ajoute un pied de page
     *
     * @param pathHtml
     *            page html auquel on ajoute une mise en page
     * @param addElement
     *            element a ajouter
     * @param tete
     *            determine en-tete ou pied de page
     */
    public void addTitlePage(String pathHtml, Element addElement, boolean tete)
        throws Exception {
        SAXReader reader = new SAXReader(false);
        reader.setIncludeExternalDTDDeclarations(false);
        reader.setIncludeInternalDTDDeclarations(false);
        Document doc = reader.read(new FileInputStream(new File(pathHtml)));

        Document document = DocumentHelper.createDocument();
        Element elemCopy = addElement.createCopy();
        Element root = document.addElement("html");
        Element head = ((Element) doc.selectSingleNode("//head")).createCopy();
        root.add(head);
        Element body = root.addElement("body");
        if (tete) {
            body.add(elemCopy);
            ArrayList bodyElems = (ArrayList) doc
                .selectNodes("//body/child::*");
            if (!bodyElems.isEmpty()) {
                Iterator it = bodyElems.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
        } else {
            ArrayList bodyElems = (ArrayList) doc
                .selectNodes("//body/child::*");
            if (!bodyElems.isEmpty()) {
                Iterator it = bodyElems.iterator();
                while (it.hasNext()) {
                    Element elem = ((Element) it.next()).createCopy();
                    body.add(elem);
                }
            }
            body.addElement("hr");
            body.add(elemCopy);
        }
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("iso-8859-1");
        XMLWriter writer;
        Writer outputWriter = new BufferedWriter(new FileWriter(new File(
                                                                         pathHtml)));
        writer = new XMLWriter(outputWriter, format);
        writer.write(document);
        writer.close();
    }

    public static class UserOutputResolver implements OutputURIResolver {

        FileOutputStream fileOut;
        String outputDir;

        public UserOutputResolver(String outputDir) {
            this.outputDir = outputDir;
        }

        public void close(Result arg0) throws TransformerException {
            try {
                if (fileOut != null)
                    fileOut.close();
            } catch (IOException e) {
                // TODO
            }
        }

        public Result resolve(String href, String base)
            throws TransformerException {
            try {
                File out = new File(outputDir + href);
                fileOut = new FileOutputStream(out);
            } catch (FileNotFoundException e) {
                // TODO
            }
            StreamResult res = new StreamResult(fileOut);
            return res;
        }

    }
}
