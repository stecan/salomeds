/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements;

import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.java.plugin.Extension;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.Plugin;
import org.java.plugin.PluginDescriptor;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.SalomeEvent;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.Admin;
import org.objectweb.salome_tmf.plugins.core.Common;
import org.objectweb.salome_tmf.plugins.core.ReqManager;
import org.objectweb.salome_tmf.plugins.core.XMLLoaderPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLPrinterPlugin;
import org.objectweb.salome_tmf.plugins.core.XMLWriterPlugin;

import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.ihm.ExecResultStatDialog;
import salomeTMF_plug.requirements.ihm.RequirementActionPanel;
import salomeTMF_plug.requirements.ihm.RequirementCampPanel;
import salomeTMF_plug.requirements.ihm.RequirementDescriptionPanel;
import salomeTMF_plug.requirements.ihm.RequirementPanel;
import salomeTMF_plug.requirements.ihm.RequirementTestPanel;
import salomeTMF_plug.requirements.ihm.SelectRequirement;
import salomeTMF_plug.requirements.ihm.StatRequirement;
import salomeTMF_plug.requirements.sqlWrapper.ISQLRequirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_EXP_000010 - \ufffd2.7.10
 * @covers SFD_ForgeORTF_EXG_EXP_000020 - \ufffd2.7.10
 * @covers SFD_ForgeORTF_EXG_EXP_000030 - \ufffd2.7.10 Jira : FORTF - 43
 */
public class ReqPlugin extends Plugin implements ReqManager, Common, Admin,
                                                 XMLPrinterPlugin, XMLLoaderPlugin, Observer, ActionListener,
                                                 ListSelectionListener {
    // JApplet ptrSalome;
    String id_plugin;

    RequirementPanel pRequirementPanel;
    RequirementTestPanel pRequirementMTestPanel;
    RequirementTestPanel pRequirementATestPanel;
    boolean ACTION_REQ = false;
    RequirementActionPanel pRequirementActionPanel;
    RequirementCampPanel pRequirementCampPanel;
    DynamicTree campaignDynamicTree;
    // ResultPanel pResultPanel;

    // Requirements statistics are not used in Sirona for 12 years. Thus,
    // they are disabled (stecan, 9. April 2018)
    // JButton statResExecButton;
    
    static JTabbedPane salomeMainTabs;
    static int tabIndex = -1;
    static Project project_ref = null;
    static boolean isGlobalProject;
    ISQLRequirement pSQLRequirement = null;

    StatRequirement pStatRequirement;

    ImportXMLReq pImportXMLReq;

    final static String REQ_NAME = "SALOME_REQ_PLUG";

    private boolean mandatoryImport = false;
    private boolean mandatoryExport = false;
    private boolean alwaysCopyReqLink = false;

    // 20100119 - D\ufffdbut modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;

    // 20100119 - Fin modification Forge ORTF

    public ReqPlugin(PluginManager manager, PluginDescriptor descr) {
        super(manager, descr);
        pStatRequirement = new StatRequirement();
        id_plugin = descr.getId() + ".Common";

        // 20100119 - D\ufffdbut modification Forge ORTF
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
        // 20100119 - Fin modification Forge ORTF

        project_ref = DataModel.getCurrentProject();
        isGlobalProject = false;
        Collection colExt = descr.getExtensions();
        Iterator it = colExt.iterator();
        while (it.hasNext()) {
            Extension ext = (Extension) it.next();
            if (ext.getUniqueId()
                .equals("requirements@requirements.ReqManager")) {
                Collection colAtt = ext.getParameters();
                Iterator it2 = colAtt.iterator();
                while (it2.hasNext()) {

                    org.java.plugin.Extension.Parameter param = (org.java.plugin.Extension.Parameter) it2
                        .next();
                    if (param.getId().equals("projectref")) {
                        try {
                            String project_name = param.rawValue();
                            ISQLProject pISQLProject = Api
                                .getISQLObjectFactory().getISQLProject();
                            ProjectWrapper pProjectWrapper = pISQLProject
                                .getProject(project_name);
                            if (pProjectWrapper != null) {
                                project_ref = new Project(pProjectWrapper);
                                if (project_ref.getIdBdd() == DataModel
                                    .getCurrentProject().getIdBdd()) {
                                    isGlobalProject = true;
                                }
                                Util.log("[ReqPlugin-> ReqPlugin] Project is "
                                         + project_ref.getNameFromModel());
                            } else {
                                Util.log("[ReqPlugin-> ReqPlugin] Project "
                                         + project_name + " not found");
                            }
                        } catch (Exception e) {

                        }
                    }
                }
            }
        }
    }

    /********************* extends Plugin **********************************/

    /**
     * @see org.java.plugin.Plugin()
     */
    protected void doStart() throws Exception {

    }

    /**
     * @see org.java.plugin.Plugin()
     */
    protected void doStop() throws Exception {
        // no-op
    }

    // ********************* interface common ********************//

    public void activatePluginInCampToolsMenu(javax.swing.JMenu jMenu) {
        //              jMenu.addSeparator();
        //              jMenu.add(createJMenu());
    }

    public void activatePluginInDataToolsMenu(javax.swing.JMenu jMenu) {
    }

    public void activatePluginInDynamicComponent(Integer uiCompCst) {
        if (ACTION_REQ) {
            if (uiCompCst.equals(UICompCst.ACTION_NEW_TAB)) {

                // System.out.println("ReqPlugin");

                JTabbedPane actionTabs = (JTabbedPane) SalomeTMFContext
                    .getInstance().getUIComponent(uiCompCst);
                // System.out.println("ReqPlugin 2");

                actionTabs.addTab("Plug-in "
                                  + Language.getInstance().getText("Exigences"),
                                  pRequirementActionPanel);
                // System.out.println("ReqPlugin 3");
                // tabIndexAct =
                // actionTabs.indexOfComponent(pRequirementActionPanel);

                pRequirementActionPanel.setParent(actionTabs);
                // System.out.println("ReqPlugin passed");

            }
        }
    }

    public void activatePluginInStaticComponent(Integer uiCompCst) {
        if (uiCompCst.equals(UICompCst.MAIN_TABBED_PANE)) {
            salomeMainTabs = (JTabbedPane) SalomeTMFContext.getInstance()
                .getUIComponent(uiCompCst);
            salomeMainTabs.addTab("Plug-in "
                                  + Language.getInstance().getText("Exigences"),
                                  pRequirementPanel);
            tabIndex = salomeMainTabs.indexOfComponent(pRequirementPanel);
            /*
             * salomeMainTabs.addChangeListener(new ChangeListener() { public
             * void stateChanged(ChangeEvent e) { if
             * (salomeMainTabs.getSelectedComponent
             * ().equals(pRequirementPanel)){ pRequirementPanel.focusChange(); }
             * } });
             */
        } else if (uiCompCst
                   .equals(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS)) {
            JTabbedPane manTestTabs = (JTabbedPane) SalomeTMFContext
                .getInstance().getUIComponent(uiCompCst);
            manTestTabs.addTab("Plug-in "
                               + Language.getInstance().getText("Exigences"),
                               pRequirementMTestPanel);
            pRequirementMTestPanel.setParent(manTestTabs);
            /*
             * manTestTabs.addChangeListener(new ChangeListener() { public void
             * stateChanged(ChangeEvent e) { if
             * (((JTabbedPane)e.getSource()).getSelectedComponent
             * ().equals(pRequirementMTestPanel)){
             * System.out.println("Update Requiment Manual test");
             * pRequirementMTestPanel.InitData(DataModel.getCurrentTest()); } }
             * });
             */
        } else if (uiCompCst
                   .equals(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS)) {
            JTabbedPane autTestTabs = (JTabbedPane) SalomeTMFContext
                .getInstance().getUIComponent(uiCompCst);
            autTestTabs.addTab("Plug-in "
                               + Language.getInstance().getText("Exigences"),
                               pRequirementATestPanel);
            pRequirementATestPanel.setParent(autTestTabs);
            /*
             * autTestTabs.addChangeListener(new ChangeListener() { public void
             * stateChanged(ChangeEvent e) { if
             * (((JTabbedPane)e.getSource()).getSelectedComponent
             * ().equals(pRequirementATestPanel)){
             * System.out.println("Update Requiment Automatic test");
             * pRequirementATestPanel.InitData(DataModel.getCurrentTest()); } }
             * });
             */
        } else if (uiCompCst
                   .equals(UICompCst.CAMPAIGN_WORKSPACE_PANEL_FOR_TABS)) {
            JTabbedPane campTabs = (JTabbedPane) SalomeTMFContext.getInstance()
                .getUIComponent(uiCompCst);
            campTabs.addTab("Plug-in "
                            + Language.getInstance().getText("Exigences"),
                            pRequirementCampPanel);
            pRequirementCampPanel.setParent(campTabs);
        } else if (uiCompCst.equals(UICompCst.TEST_DYNAMIC_TREE)) {
            DynamicTree pDynamicTree = (DynamicTree) SalomeTMFContext
                .getInstance().getUIComponent(uiCompCst);
            pRequirementATestPanel.setTestTree(pDynamicTree);
            pRequirementMTestPanel.setTestTree(pDynamicTree);
            pRequirementPanel.setTestTree(pDynamicTree);
        } else if (uiCompCst.equals(UICompCst.CAMPAIGN_DYNAMIC_TREE)) {
            campaignDynamicTree = (DynamicTree) SalomeTMFContext.getInstance()
                .getUIComponent(uiCompCst);
            pRequirementCampPanel.setCampTree(campaignDynamicTree);

        } else if (uiCompCst
                   .equals(UICompCst.CAMP_EXECUTION_RESULTS_BUTTONS_PANEL)) {
            JPanel panButton = (JPanel) SalomeTMFContext.getInstance()
                .getUIComponent(uiCompCst);
            // Requirements statistics are not used in Sirona for 12 years. Thus,
            // they are disabled (stecan, 9. April 2018)
            // panButton.add(statResExecButton);
        } else if (uiCompCst.equals(UICompCst.CAMP_EXECUTION_RESULTS_TABLE)) {
            JTable pTableResExec = (JTable) SalomeTMFContext.getInstance()
                .getUIComponent(uiCompCst);
            ListSelectionModel rowSM = pTableResExec.getSelectionModel();
            rowSM.addListSelectionListener(this);
        }
    }

    public void activatePluginInTestToolsMenu(javax.swing.JMenu jMenu) {

    }

    public void freeze() {
    }

    public java.util.Vector getUsedUIComponents() {
        Vector uiComponentsUsed = new Vector();
        uiComponentsUsed.add(UICompCst.MAIN_TABBED_PANE);
        uiComponentsUsed.add(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS);
        uiComponentsUsed.add(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS);
        uiComponentsUsed.add(UICompCst.CAMPAIGN_WORKSPACE_PANEL_FOR_TABS);
        uiComponentsUsed.add(UICompCst.TEST_DYNAMIC_TREE);
        uiComponentsUsed.add(UICompCst.CAMPAIGN_DYNAMIC_TREE);
        uiComponentsUsed.add(UICompCst.CAMP_EXECUTION_RESULTS_BUTTONS_PANEL);
        uiComponentsUsed.add(UICompCst.CAMP_EXECUTION_RESULTS_TABLE);
        if (ACTION_REQ) {
            uiComponentsUsed.add(UICompCst.ACTION_NEW_TAB);
        }
        return uiComponentsUsed;
    }

    public void init(Object pIhm) {
        // ptrSalome = ((JApplet) pIhm);

        try {
            SQLWrapper.init(Api.getISQLObjectFactory());
            pRequirementPanel = new RequirementPanel(pStatRequirement, this);

            pRequirementCampPanel = new RequirementCampPanel(pRequirementPanel
                                                             .getTree());
            pRequirementATestPanel = new RequirementTestPanel(1,
                                                              pRequirementPanel.getTree(), pRequirementCampPanel);
            pRequirementMTestPanel = new RequirementTestPanel(0,
                                                              pRequirementPanel.getTree(), pRequirementCampPanel);

            if (ACTION_REQ) {
                // Faute sur le parent mais on s'en fout!
                pRequirementActionPanel = new RequirementActionPanel(0,
                                                                     pRequirementPanel.getTree(), pRequirementCampPanel);
            }
// Requirements statistics are not used in Sirona for 12 years. Thus,
// they are disabled (stecan, 9. April 2018)
//            statResExecButton = new JButton(Language.getInstance().getText(
//                                                                           "Statistique")
//                                            + " " + Language.getInstance().getText("Exigences"));
//            statResExecButton.addActionListener(this);
//            statResExecButton.setEnabled(false);
            // pResultPanel = new ResultPanel();
            DataModel.getCurrentProject().registerObserver(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isActivableInCampToolsMenu() {
        return true;
    }

    public boolean isActivableInDataToolsMenu() {
        return false;
    }

    public boolean isActivableInTestToolsMenu() {
        return false;
    }

    public boolean isFreezable() {
        return false;
    }

    public boolean isFreezed() {
        return false;
    }

    public void unFreeze() {
    }

    public boolean usesOtherUIComponents() {
        return true;
    }

    public void allPluginActived(ExtensionPoint commonExtensions,
                                 ExtensionPoint testDriverExtensions,
                                 ExtensionPoint scriptEngineExtensions,
                                 ExtensionPoint bugTrackerExtensions) {
    }

    public void allPluginActived(ExtensionPoint commonExtensions,
                                 ExtensionPoint testDriverExtensions,
                                 ExtensionPoint scriptEngineExtensions,
                                 ExtensionPoint bugTrackerExtensions,
                                 ExtensionPoint statistiquesExtensions) {
    }

    /************************************************************************************************/

    static public Project getProjectRef() {
        return project_ref;
    }

    /**
     *
     * @return true if projectref is defined in pulgin.xml and the currentProjet
     *         is the projectRef
     */
    static public boolean isGlobalProject() {
        return isGlobalProject;
    }

    public void update(Observable o, Object arg) {
        // System.out.println("[ReqPlugin->update] : " +arg);
        try {
            if (arg instanceof SalomeEvent) {
                SalomeEvent pSalomeEvent = (SalomeEvent) arg;
                int code = pSalomeEvent.getCode();
                if (code == ApiConstants.DELETE_TEST) {
                    Test pTest = (Test) pSalomeEvent.getArg();
                    Requirement.deleteAllCoverForTest(pTest.getIdBdd());
                } else if (code == ApiConstants.LOADING) {
                    pRequirementPanel.reloadReqPerformed();
                } else if (code == ApiConstants.DELETE_ACTION) {
                    Action pAction = (Action) pSalomeEvent.getArg();
                    /*
                     * TODO : delete requierment link in database concerning the
                     * action
                     */
                } else if (code == ApiConstants.COPIE_TEST) {
                    Vector<Test> listOfTest = (Vector<Test>) pSalomeEvent
                        .getArg();
                    Test pTestSource = listOfTest.elementAt(0);
                    Test pTestCopied = listOfTest.elementAt(1);
                    if (alwaysCopyReqLink) {
                        Requirement.copyReqCover(pTestSource.getIdBdd(),
                                                 pTestCopied.getIdBdd());
                    } else {
                        if (SalomeTMFContext.getBaseIHM().isGraphique()) {
                            Object[] possibleValues = {
                                Language.getInstance().getText("Toujours"), // =
                                // YES
                                Language.getInstance().getText("Oui"),// =
                                // NO
                                Language.getInstance().getText("Non") }; // =CANCEL

                            int n = JOptionPane.showOptionDialog(
                                                                 SalomeTMFContext.getInstance()
                                                                 .getSalomeFrame(), Language
                                                                 .getInstance().getText(
                                                                                        "Copier_les_exigences")
                                                                 + "?", Language.getInstance()
                                                                 .getText("Test")
                                                                 + " :"
                                                                 + pTestSource.getNameFromModel(),
                                                                 JOptionPane.YES_NO_CANCEL_OPTION,
                                                                 JOptionPane.QUESTION_MESSAGE, null,
                                                                 possibleValues, possibleValues[0]);
                            if (n == JOptionPane.YES_OPTION) {
                                alwaysCopyReqLink = true;
                                Requirement.copyReqCover(
                                                         pTestSource.getIdBdd(), pTestCopied
                                                         .getIdBdd());
                            } else if (n == JOptionPane.NO_OPTION) {
                                Requirement.copyReqCover(
                                                         pTestSource.getIdBdd(), pTestCopied
                                                         .getIdBdd());
                            }
                        } else {
                            Requirement.copyReqCover(pTestSource.getIdBdd(),
                                                     pTestCopied.getIdBdd());
                        }

                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*********************************************************************************************/

    public void actionPerformed(ActionEvent e) {
// Requirements statistics are not used in Sirona for 12 years. Thus,
// they are disabled (stecan, 9. April 2018)
//        if (e.getSource().equals(statResExecButton)) {
//            statResExecPerformed(e);
//        }
    }

    void statResExecPerformed(ActionEvent e) {
        if (DataModel.getObservedExecutionResult() != null) {
            // ExecResultStatDialog pResultPanel = new
            // ExecResultStatDialog(DataModel.getObservedExecutionResult(),
            // pRequirementPanel.getTree());
            new ExecResultStatDialog(DataModel.getObservedExecutionResult(),
                                     pRequirementPanel.getTree());
            // pResultPanel.show(DataModel.getObservedExecutionResult());
        }
    }

    /*********************************************************************************************/

    public void valueChanged(ListSelectionEvent e) {
        // Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
// Requirements statistics are not used in Sirona for 12 years. Thus,
// they are disabled (stecan, 9. April 2018)
//        if (lsm.isSelectionEmpty()) {
//            // no rows are selected
//            statResExecButton.setEnabled(false);
//        } else {
//            statResExecButton.setEnabled(true);
//        }
    }

    /*********************************************************************************************/

    static public void selectTestTab() {
        salomeMainTabs.setSelectedIndex(0);
    }

    static public void selectCampTab() {
        salomeMainTabs.setSelectedIndex(1);
    }

    static public void selectReqTab() {
        salomeMainTabs.setSelectedIndex(tabIndex);
    }

    public JTree getTreeRequirement() {
        if (pRequirementPanel != null) {
            pRequirementPanel.getTree().reload();
            return pRequirementPanel.getTree().getCopy(false);
        }
        return null;
    }

    public void reloadReq() {
        pRequirementPanel.getTree().reload();
    }

    private JMenu createJMenu() {
        JMenu reqSubMenu;
        reqSubMenu = new JMenu("Plug-in "
                               + Language.getInstance().getText("Exigences"));

        JMenuItem importItem = new JMenuItem(Language.getInstance().getText(
                                                                            "Importer")
                                             + " " + Language.getInstance().getText("Exigences"));
        importItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    SalomeTMFPanels.setCursor(DataConstants.CAMPAIGN,
                                              Cursor.WAIT_CURSOR);
                    importReqInCampPerformed();
                    SalomeTMFPanels.setCursor(DataConstants.CAMPAIGN,
                                              Cursor.DEFAULT_CURSOR);
                }
            });

        reqSubMenu.add(importItem);
        return reqSubMenu;
    }

    void importReqInCampPerformed() {
        Campaign pCamp = DataModel.getCurrentCampaign();

        if (pCamp == null) {
            return;
        }

        if (pCamp.containsExecutionResultInModel()) {
            JOptionPane
                .showMessageDialog(
                                   SalomeTMFContext.getInstance().getSalomeFrame(),
                                   org.objectweb.salome_tmf.ihm.languages.Language
                                   .getInstance()
                                   .getText(
                                            "Cette_campagne_contient_deja_des_resultat_d_execution_Il_n_est_plus_possible_de_la_modifier"),
                                   org.objectweb.salome_tmf.ihm.languages.Language
                                   .getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                Vector reqCovered = InitData(pCamp);
                JTree pTree = pRequirementPanel.getTree().getCopy(true);
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) pTree
                                                                        .getModel()).getRoot();
                Hashtable<Integer, Requirement> removedReq = new Hashtable<Integer, Requirement>();
                removeReqWithNoTestLink((DefaultTreeModel) pTree.getModel(),
                                        node, removedReq);
                if (((DefaultMutableTreeNode) ((DefaultTreeModel) pTree
                                               .getModel()).getRoot()).getChildCount() == 0) {
                    JOptionPane.showMessageDialog(SalomeTMFContext
                                                  .getInstance().getSalomeFrame(),
                                                  org.objectweb.salome_tmf.ihm.languages.Language
                                                  .getInstance().getText(
                                                                         "Aucune_exigence_couverte"),
                                                  org.objectweb.salome_tmf.ihm.languages.Language
                                                  .getInstance().getText("Attention_"),
                                                  JOptionPane.WARNING_MESSAGE);
                    return;
                }
                SelectRequirement pSelectRequirement = new SelectRequirement(
                                                                             pTree, Language.getInstance().getText("Selection"),
                                                                             reqCovered, false, removedReq);
                Hashtable selected = pSelectRequirement.getSelection();
                if (selected != null) {
                    reqCovered.clear();
                    Enumeration enumSelected = selected.elements();
                    while (enumSelected.hasMoreElements()) {
                        reqCovered.add(enumSelected.nextElement());
                    }
                    fillCamp(pCamp, reqCovered);
                }
            } catch (Exception e) {
                Tools.ihmExceptionView(e);

            }
        }
    }

    boolean removeReqWithNoTestLink(DefaultTreeModel treeModel,
                                    DefaultMutableTreeNode pTempNode,
                                    Hashtable<Integer, Requirement> removedReq) {
        Requirement pTempReq = (Requirement) pTempNode.getUserObject();
        if (pTempReq instanceof ReqLeaf) {
            try {
                Vector testLinked = pTempReq.getTestWrapperCoveredFromDB();
                if (testLinked.size() == 0) {
                    treeModel.removeNodeFromParent(pTempNode);
                    removedReq.put(pTempReq.getIdBdd(), pTempReq);
                    return true;
                }
            } catch (Exception e) {

            }
            return false;
        } else {
            int nbChild = pTempNode.getChildCount();

            for (int i = 0; i < nbChild; i++) {
                if (removeReqWithNoTestLink(treeModel,
                                            (DefaultMutableTreeNode) pTempNode.getChildAt(i),
                                            removedReq)) {
                    i--;
                    nbChild--;
                }
            }
            if (pTempNode.getChildCount() == 0
                && !pTempNode.equals(treeModel.getRoot())) {
                treeModel.removeNodeFromParent(pTempNode);
                removedReq.put(pTempReq.getIdBdd(), pTempReq);
                return true;
            }
        }
        return false;
    }

    void fillCamp(Campaign pCamp, Vector reqCovered) {
        try {
            if (!pCamp.isValideModel()) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return;
        }

        Hashtable selectedTests = new Hashtable();

        int reqCoveredSize = reqCovered.size();
        for (int i = 0; i < reqCoveredSize; i++) {
            Requirement pRequirement = (Requirement) reqCovered.elementAt(i);

            try {
                Vector testCover = pRequirement.getTestWrapperCoveredFromDB();
                int testCoverSize = testCover.size();

                for (int j = 0; j < testCoverSize; j++) {
                    TestWrapper pTestWrapper = (TestWrapper) testCover
                        .elementAt(j);
                    Test pTest = DataModel.getCurrentProject()
                        .getTestFromModel(pTestWrapper.getIdBDD());
                    if (pTest == null && !isGlobalProject()) {
                        if (getProjectRef().getIdBdd() == DataModel
                            .getCurrentProject().getIdBdd()) {
                            DataModel.reloadFromBase(true);
                            pTest = DataModel.getCurrentProject()
                                .getTestFromModel(pTestWrapper.getIdBDD());
                        }
                        if (pTest != null) {
                            selectedTests.put(new Integer(pTest.getIdBdd()),
                                              pTest);
                        }
                    } else {
                        selectedTests.put(new Integer(pTest.getIdBdd()), pTest);
                    }
                }

            } catch (Exception e) {
                // Hum ??
            }
        }
        reInitCamp(pCamp, selectedTests);
    }

    void reInitCamp(Campaign pCamp, Hashtable selectedTests) {
        ArrayList oldTestList = new ArrayList();
        ArrayList datasetsCreated;
        Hashtable oldAssignedUser = new Hashtable();
        boolean newDataSetCreated = false;

        ArrayList dataSets = pCamp.getDataSetListFromModel();

        for (int k = 0; k < pCamp.getTestListFromModel().size(); k++) {
            Test pTest = (Test) pCamp.getTestListFromModel().get(k);
            int idUserAssigned = pCamp.getAssignedUserID(pTest);
            if (idUserAssigned == -1) {
                idUserAssigned = pCamp.getIdBdd();
            }
            oldAssignedUser.put(new Integer(pTest.getIdBdd()), new Integer(
                                                                           idUserAssigned));
            oldTestList.add(pTest);

        }
        Hashtable testBySuite = new Hashtable();
        Enumeration enumSelected = selectedTests.elements();

        /* On classe les tests par suite */
        while (enumSelected.hasMoreElements()) {
            Test pTest = (Test) enumSelected.nextElement();
            TestList pTestList = pTest.getTestListFromModel();
            Vector listTest = (Vector) testBySuite.get(pTestList);
            if (listTest != null) {
                listTest.add(0, pTest);
            } else {
                listTest = new Vector();
                listTest.add(0, pTest);
                testBySuite.put(pTestList, listTest);
            }
        }

        int transNumber = -1;
        try {
            // BDD & MODEL

            transNumber = Api.beginTransaction(110,
                                               ApiConstants.INSERT_TEST_INTO_CAMPAIGN);
            /* 1 On vide la campagne */
            for (int i = 0; i < oldTestList.size(); i++) {
                // pCamp.deleteTestFromCampInDB(((Test)oldTestList.get(i)).getIdBdd(),
                // false);
                pCamp.deleteTestFromCampInDBAndModel((Test) oldTestList.get(i),
                                                     false);
            }
            pCamp.clearAssignedUserForTest();

            /* 2 On Import les Tests */
            Enumeration enumSuiteSelected = testBySuite.elements();
            while (enumSuiteSelected.hasMoreElements()) {
                Vector testlist = (Vector) enumSuiteSelected.nextElement();
                int testlistSize = testlist.size();
                for (int i = 0; i < testlistSize; i++) {
                    Test pTest = (Test) testlist.elementAt(i);
                    int id = DataModel.getCurrentUser().getIdBdd();
                    Integer userID = (Integer) oldAssignedUser.get(new Integer(
                                                                               pTest.getIdBdd()));
                    if (userID != null) {
                        id = userID.intValue();
                    }
                    datasetsCreated = DataModel.getCurrentProject()
                        .addTestInCampaignInDBAndModel(pTest, pCamp, id);
                    if (datasetsCreated.size() > 0) {
                        newDataSetCreated = true;
                        for (int k = 0; k < pCamp.getExecutionListFromModel()
                                 .size(); k++) {
                            DataModel.getExecutionTableModel().setValueAt(
                                                                          ((DataSet) datasetsCreated.get(k))
                                                                          .getNameFromModel(), k, 3);
                        }
                    }
                }
            }

            if (newDataSetCreated) {
                for (int i = 0; i < dataSets.size(); i++) {
                    DataSet newDataSet = (DataSet) dataSets.get(i);
                    ArrayList dataView = new ArrayList();
                    dataView.add(newDataSet.getNameFromModel());
                    dataView.add(newDataSet.getDescriptionFromModel());
                    DataModel.getDataSetTableModel().addRow(dataView);
                }
            }
            // DataModel.reloadFromBase(true);
            DataModel.reloadCampaign(pCamp);
            Api.commitTrans(transNumber);
        } catch (Exception exception) {
            Api.forceRollBackTrans(transNumber);
            pCamp.setTestListInModel(oldTestList);
            Tools.ihmExceptionView(exception);
        }
    }

    Vector InitData(Campaign pCamp) throws Exception {
        Vector reqCovered = new Vector();
        Vector reqCoveredWrapper;
        reqCovered.clear();
        try {
            ArrayList pTestList = pCamp.getTestListFromModel();
            int nbTest = pTestList.size();
            for (int i = 0; i < nbTest; i++) {
                Test pTest = (Test) pTestList.get(i);
                reqCoveredWrapper = Requirement
                    .getReqWrapperCoveredByTest(pTest.getIdBdd());
                int size = reqCoveredWrapper.size();
                for (int j = 0; j < size; j++) {
                    ReqWrapper pReqWrapper = (ReqWrapper) reqCoveredWrapper
                        .elementAt(j);
                    DefaultMutableTreeNode node = pRequirementPanel.getTree()
                        .findRequirementFromParent(pReqWrapper);
                    Requirement pReq = null;
                    if (node != null) {
                        pReq = (Requirement) node.getUserObject();
                        if (!reqCovered.contains(pReq)) {
                            reqCovered.add(pReq);
                        }
                    } else {
                        pRequirementPanel.getTree().reload();
                        node = pRequirementPanel.getTree()
                            .findRequirementFromParent(pReqWrapper);
                        if (node != null) {
                            pReq = (Requirement) node.getUserObject();
                            if (!reqCovered.contains(pReq)) {
                                reqCovered.add(pReq);
                            }
                        } else {
                            // Hum Data corruption !!!
                            Tools
                                .ihmExceptionView(new Exception(
                                                                "Hum ??, it seem that data integrity are corrupted"));

                        }
                    }
                }
            }

        } catch (Exception e) {
            throw e;
        }
        return reqCovered;
    }

    // ////////////////////////////// Admin interface
    // ////////////////////////////////////

    /*
     * (non-Javadoc)
     *
     * @see
     * org.objectweb.salome_tmf.plugins.core.Admin#activateInSalomeAdmin(java
     * .util.Map)
     */
    public void activateInSalomeAdmin(Map adminUIComps, IPlugObject iPlugObject) {

    }

    public void onDeleteProject(Project p) {
        try {
            if (pSQLRequirement == null) {
                SQLWrapper.init(Api.getISQLObjectFactory());
                pSQLRequirement = SQLWrapper.getSQLRequirement();
            }
            pSQLRequirement.deleteProjectReq(p.getIdBdd());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean deleteConfirme(String quoi) {
        Object[] options = {
            org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
            .getText("Oui"),
            org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
            .getText("Non") };
        int choice = -1;

        choice = SalomeTMFContext.getInstance().askQuestion(
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
                                                            .getText("confimation_suppression2")
                                                            + " " + quoi + " ?",
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance()
                                                            .getText("Attention_"), JOptionPane.WARNING_MESSAGE,
                                                            options);

        if (choice == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    /********************* DoxXML Implementation **********************/

    void writeRequirement(Element reqsElem, DefaultMutableTreeNode pTempNode,
                          boolean first, String attachPath, XMLWriterPlugin pXMLWriter)
        throws Exception {
        if (!doExport()) {
            return;
        }
        Requirement pTempReq = (Requirement) pTempNode.getUserObject();
        HashMap attachs = null;
        try {
            if (!first) {
                attachs = pTempReq.getAttachmentMapFromModel();
            }
        } catch (Exception e) {

        }
        if (pTempReq instanceof ReqLeaf) {
            // Write leaf
            // 20100119 - D\ufffdbut modification Forge ORTF
            Element reqLeafElem = reqsElem.addElement("Requirement");
            reqLeafElem.addElement("Nom").setText(pTempReq.getNameFromModel());
            // Category
            int category = ((ReqLeaf) pTempReq).getCatFromModel();
            String sql = "SELECT name FROM REQ_CATEGORY where id=" + category + ";";
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            String cat = "";
            if (result.next()) {
                cat = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("category", "" + cat);
            // Priority
            int priority = ((ReqLeaf) pTempReq).getPriorityFromModel();
            sql = "SELECT name FROM REQ_PRIORITY where id=" + priority + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String priorite = "";
            if (result.next()) {
                priorite = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("priority", "" + priorite);
            // Complexity
            int complexity = ((ReqLeaf) pTempReq).getComplexeFromModel();
            sql = "SELECT name FROM REQ_COMPLEXE where id=" + complexity + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String complexe = "";
            if (result.next()) {
                complexe = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("complexity", "" + complexe);
            // Critical
            int criticity = ((ReqLeaf) pTempReq).getCriticalFromModel();
            sql = "SELECT name FROM REQ_CRITICAL where id=" + criticity + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String critical = "";
            if (result.next()) {
                critical = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("criticity", "" + critical);
            // Understanding
            int understanding = ((ReqLeaf) pTempReq)
                .getUnderstandingFromModel();
            sql = "SELECT name FROM REQ_UNDERSTANDING where id=" + understanding
                + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String comprehension = "";
            if (result.next()) {
                comprehension = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("understanding", "" + comprehension);
            // Owner
            int owner = ((ReqLeaf) pTempReq).getOwnerFromModel();
            sql = "SELECT name FROM REQ_OWNER where id=" + owner + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String proprietaire = "";
            if (result.next()) {
                proprietaire = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("owner", "" + proprietaire);
            // State
            int state = ((ReqLeaf) pTempReq).getStateFromModel();
            sql = "SELECT name FROM REQ_STATE where id=" + state + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String etat = "";
            if (result.next()) {
                etat = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("state", "" + etat);
            // CheckedBy
            int validBy = ((ReqLeaf) pTempReq).getValidByFromModel();
            sql = "SELECT name FROM REQ_VALID_BY where id=" + validBy + ";";
            prep = db.prepareStatement(sql);
            result = prep.executeQuery(sql);
            String checkedBy = "";
            if (result.next()) {
                checkedBy = result.getString(1);
            }
            result.close();
            reqLeafElem.addAttribute("checkedBy", "" + checkedBy);
            // 20100119 - Fin modification Forge ORTF
            reqLeafElem.addAttribute("origine", ((ReqLeaf) pTempReq)
                                     .getOrigineFromModel());
            reqLeafElem.addAttribute("verifway", ((ReqLeaf) pTempReq)
                                     .getVerifFromModel());

            reqLeafElem.addAttribute("version", ((ReqLeaf) pTempReq)
                                     .getVersionFromModel());
            reqLeafElem.addAttribute("reference", ((ReqLeaf) pTempReq)
                                     .getReferenceFromModel());

            reqLeafElem.addAttribute("id_req", "Req_"
                                     + new Integer(pTempReq.getIdBdd()).toString());
            reqLeafElem.addAttribute("id_req_parent", "Req_"
                                     + new Integer(pTempReq.getParent().getIdBdd()).toString());
            if (pTempReq.getDescriptionFromModel() != null
                && !pTempReq.getDescriptionFromModel().equals("")) {
                addHTMLDescription(reqLeafElem, pTempReq
                                   .getDescriptionFromModel());
            }
            if (attachs != null) {
                pXMLWriter.addAttach(reqLeafElem, attachPath + File.separator
                                     + "Attachements" + File.separator + "Requirements"
                                     + File.separator + pTempReq.getIdBdd(), attachs,
                                     "Attachements" + File.separator + "Requirements"
                                     + File.separator + pTempReq.getIdBdd());
            }
        } else {
            // Write family
            Element reqFamilyElem = reqsElem;
            if (!first) {
                reqFamilyElem = reqsElem.addElement("RequirementFamily");

                reqFamilyElem.addElement("Nom").setText(
                                                        pTempReq.getNameFromModel());
                reqFamilyElem.addAttribute("id_req", "Req_"
                                           + new Integer(pTempReq.getIdBdd()).toString());
                reqFamilyElem.addAttribute("id_req_parent", "Req_"
                                           + new Integer(pTempReq.getParent().getIdBdd())
                                           .toString());
                if (pTempReq.getDescriptionFromModel() != null
                    && !pTempReq.getDescriptionFromModel().equals("")) {
                    addHTMLDescription(reqFamilyElem, pTempReq
                                       .getDescriptionFromModel());
                }
                if (attachs != null) {
                    pXMLWriter.addAttach(reqFamilyElem, attachPath
                                         + File.separator + "Attachements" + File.separator
                                         + "Requirements" + File.separator
                                         + pTempReq.getIdBdd(), attachs, "Attachements"
                                         + File.separator + "Requirements" + File.separator
                                         + pTempReq.getIdBdd());
                }
            } else {
                first = false;
            }
            int nbChild = pTempNode.getChildCount();

            for (int i = 0; i < nbChild; i++) {
                writeRequirement(reqFamilyElem,
                                 (DefaultMutableTreeNode) pTempNode.getChildAt(i),
                                 first, attachPath, pXMLWriter);
            }

        }
    }

    public void addXMLElement2Root(Element rootElement,
                                   XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    private void addHTMLDescription(Element parentElem, String description)
        throws Exception {
        String desc = description.replaceAll("<br>", "<br />");
        try {
            Document document = DocumentHelper.parseText(desc);
            Element bodyElem = (Element) document.selectSingleNode("//body");
            Element descElem = bodyElem.createCopy("Description");
            descElem.addAttribute("isHTML", "true");
            parentElem.add(descElem);
        } catch (Exception e) {
            parentElem.addElement("Description").setText(
                                                         desc.replaceAll("\n", "\\\\n"));
        }
    }

    /* Test Plan */
    public void addXMLElement2Project(Element projectElement, Project pProject,
                                      XMLWriterPlugin pXMLWriter, String pathAttach) {
        if (!doExport()) {
            return;
        }
        try {
            // pReqPlugin = (ReqPlugin)
            // pIPlugObject.getPluginManager().activateExtension(ext);
            JTree pTree = getTreeRequirement();
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) pTree
                                                                    .getModel()).getRoot();
            if (node.getChildCount() > 0) {
                Element reqsElem = projectElement.addElement("Requirements");
                Requirement pTempReq = (Requirement) node.getUserObject();
                reqsElem.addAttribute("id_req", "Req_"
                                      + new Integer(pTempReq.getIdBdd()).toString());
                writeRequirement(reqsElem, node, true, pathAttach, pXMLWriter);
            }
            /*
             * pContexte.setHasCampRequirementsGraph(true);
             * pContexte.setHasResExecRequirementsGraph(true);
             * pContexte.setHasRequirementsGraph(true);
             */
            try {
                File dir = new File(pathAttach + File.separator
                                    + "Attachements" + File.separator + "Requirements");
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Requirement.writeReqCoverChart(pathAttach + File.separator
                                               + "Attachements" + File.separator + "Requirements"
                                               + File.separator + "requirements.jpeg");
            } catch (Exception e) {
                // pContexte.setHasRequirementsGraph(false);
            }
        } catch (Exception e) {

        }
    }

    public void addXMLElement2Family(Element familyElement, Family pFamily,
                                     XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    public void addXMLElement2Suite(Element suiteElement, TestList ptestList,
                                    XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    public void addXMLElement2Test(Element testElement, Test pTest,
                                   XMLWriterPlugin pXMLWriter, String pathAttach) {
        if (!doExport()) {
            return;
        }
        Vector reqCoveredWrapper;
        try {
            reqCoveredWrapper = Requirement.getReqWrapperCoveredByTest(pTest
                                                                       .getIdBdd());
            int size = reqCoveredWrapper.size();
            if (size > 0) {
                Element reqLinkElem = testElement.addElement("LinkRequirement");
                for (int i = 0; i < size; i++) {
                    ReqWrapper pReqWrapper = (ReqWrapper) reqCoveredWrapper
                        .elementAt(i);
                    Element reqRef = reqLinkElem.addElement("RequirementRef");
                    reqRef.addAttribute("ref", "Req_"
                                        + new Integer(pReqWrapper.getIdBDD()).toString());
                    reqRef.addElement("Nom").setText(pReqWrapper.getName());
                    String description = pReqWrapper.getDescription();
                    if (description != null && !description.equals("")) {
                        addHTMLDescription(reqRef, pReqWrapper.getDescription());
                        // reqRef.addElement("Description").setText(pReqWrapper.getDescription());
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    // void addXMLElement2ValuedParameter(Element paramElement, Test pTest,
    // Parameter pParam);
    public void addXMLElement2Action(Element testElement, Action pAction,
                                     XMLWriterPlugin pXMLWriter, String pathAttach) {
        if (!doExport()) {
            return;
        }
    }

    /* Campaing plan */
    public void addXMLElement2Campaign(Element campaignElement,
                                       Campaign pCampaign, XMLWriterPlugin pXMLWriter, String pathAttach) {
        if (!doExport()) {
            return;
        }
        Vector reqCoveredWrapper = new Vector();
        try {
            ArrayList pTestList = pCampaign.getTestListFromModel();
            int nbTest = pTestList.size();
            for (int i = 0; i < nbTest; i++) {
                Test pTest = (Test) pTestList.get(i);
                Vector tempCoveredWrapper = Requirement
                    .getReqWrapperCoveredByTest(pTest.getIdBdd());
                int size = tempCoveredWrapper.size();
                for (int j = 0; j < size; j++) {
                    ReqWrapper pReqWrapper = (ReqWrapper) tempCoveredWrapper
                        .elementAt(j);
                    if (!reqCoveredWrapper.contains(pReqWrapper)) {
                        reqCoveredWrapper.add(pReqWrapper);
                    }
                }
            }

            try {
                File dir = new File(pathAttach + File.separator
                                    + "Attachements" + File.separator + "Campagnes"
                                    + File.separator
                                    + pXMLWriter.formater(pCampaign.getNameFromModel()));
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                Requirement.writeCampaingChart(reqCoveredWrapper, pathAttach
                                               + File.separator + "Attachements" + File.separator
                                               + "Campagnes" + File.separator
                                               + pXMLWriter.formater(pCampaign.getNameFromModel())
                                               + File.separator + "requirements.jpeg");
            } catch (Exception e) {
                // pContexte.setHasRequirementsGraph(false);
            }

            int size = reqCoveredWrapper.size();
            if (size > 0) {
                Element reqLinkElem = campaignElement
                    .addElement("LinkRequirement");
                for (int i = 0; i < size; i++) {
                    ReqWrapper pReqWrapper = (ReqWrapper) reqCoveredWrapper
                        .elementAt(i);
                    Element reqRef = reqLinkElem.addElement("RequirementRef");
                    reqRef.addAttribute("ref", "Req_"
                                        + new Integer(pReqWrapper.getIdBDD()).toString());
                    reqRef.addElement("Nom").setText(pReqWrapper.getName());
                    String description = pReqWrapper.getDescription();
                    if (description != null && !description.equals("")) {
                        addHTMLDescription(reqRef, pReqWrapper.getDescription());
                        // reqRef.addElement("Description").setText(pReqWrapper.getDescription());
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    public void addXMLElement2Execution(Element execElement,
                                        Execution pExecution, XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    public void addXMLElement2ResExecution(Element resExecElement,
                                           ExecutionResult pExecRes, XMLWriterPlugin pXMLWriter,
                                           String pathAttach) {
        if (!doExport()) {
            return;
        }
        try {
            Campaign pCamp = pExecRes.getExecution().getCampagneFromModel();
            Execution pExec = pExecRes.getExecution();
            File dir = new File(pathAttach + File.separator + "Attachements"
                                + File.separator + "Campagnes" + File.separator
                                + pXMLWriter.formater(pCamp.getNameFromModel())
                                + File.separator
                                + pXMLWriter.formater(pExec.getNameFromModel())
                                + File.separator
                                + pXMLWriter.formater(pExecRes.getNameFromModel()));
            if (!dir.exists()) {
                dir.mkdirs();
            }
            Requirement.writeResExecChart(pExecRes, pathAttach + File.separator
                                          + "Attachements" + File.separator + "Campagnes"
                                          + File.separator
                                          + pXMLWriter.formater(pCamp.getNameFromModel())
                                          + File.separator
                                          + pXMLWriter.formater(pExec.getNameFromModel())
                                          + File.separator
                                          + pXMLWriter.formater(pExecRes.getNameFromModel())
                                          + File.separator + "requirements.jpeg");
        } catch (Exception e) {
            // Contexte.setHasRequirementsGraph(false);
        }
    }

    public void addXMLElement2ResTestExecution(Element resTestElem,
                                               ExecutionTestResult pExecTestRes, Test pTest,
                                               XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    public void addXMLElement2DataSet(Element dataSetElement, DataSet pDataSet,
                                      XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    /* Data */
    public void addXMLElement2Parameter(Element paramElement, Parameter pParam,
                                        XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    public void addXMLElement2Environment(Element envElement, Environment pEnv,
                                          XMLWriterPlugin pXMLWriter, String pathAttach) {

    }

    /***************** IMPORT ******************/
    public void manageDelete(Document doc, XMLLoaderPlugin pXMLLoader)
        throws Exception {
        if (!doImport()) {
            return;
        }
        pImportXMLReq = new ImportXMLReq(this, pXMLLoader, pRequirementPanel
                                         .getpRequirementDescriptionPanel());
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) getTreeRequirement()
                                                                .getModel()).getRoot();
        pImportXMLReq.loadRequirementCacheFromModel(node, true);
        pImportXMLReq.loadRequirementCacheFromXML(doc);
        pImportXMLReq.gestionDesSuppressionsRequirements(doc);
    }

    public void updateProjectFromXML(Document doc, boolean isSupOption,
                                     Project project, XMLLoaderPlugin pXMLLoader) throws Exception {
        if (!doImport()) {
            return;
        }
        project_ref = project;
        // if (pImportXMLReq == null){
        pImportXMLReq = new ImportXMLReq(this, pXMLLoader, pRequirementPanel
                                         .getpRequirementDescriptionPanel());
        if (pImportXMLReq != null && getTreeRequirement() != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) getTreeRequirement()
                                                                    .getModel()).getRoot();
            pImportXMLReq.loadRequirementCacheFromModel(node, true);
        }
        pImportXMLReq.loadRequirementCacheFromXML(doc);
        // }
        pImportXMLReq.updateProjectRequirement(doc);
    }

    public void updateProjectFromXML(String xmlFile) throws Exception {
        dirXml = xmlFile.substring(0, xmlFile.lastIndexOf(fs));
        SAXReader reader = new SAXReader(false);
        try {
            Document doc = reader.read(new FileInputStream(new File(xmlFile)));

            refreshNewData();
            // if (pImportXMLReq == null){
            pImportXMLReq = new ImportXMLReq(this, this, pRequirementPanel
                                             .getpRequirementDescriptionPanel());
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) getTreeRequirement()
                                                                    .getModel()).getRoot();
            pImportXMLReq.loadRequirementCacheFromModel(node, true);
            pImportXMLReq.loadRequirementCacheFromXML(doc);
            // }
            pImportXMLReq.updateProjectRequirement(doc);
            refreshNewData();
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText(
                                                                                      "L_import_s_est_terminee_avec_succes"),
                                                       Language.getInstance().getText("Information_"),
                                                       JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            SalomeTMFContext.getInstance().showMessage(e.toString(),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            refreshNewData();
        }

    }

    public void updateFamilyFromXML(Element familyElement, Family pFamily,
                                    boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void updateSuiteFromXML(Element suiteElement, TestList pSuite,
                                   boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void updateTestFromXML(Element testElement, Test pTest,
                                  boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {
        if (!doImport()) {
            return;
        }
        /*
         * if (pImportXMLReq == null){ pImportXMLReq = new ImportXMLReq(this,
         * pXMLLoader); }
         */
        pImportXMLReq.updateReqLink(testElement, pTest, isSupOption);
    }

    public void updateActionFromXML(Element actionElement, Action pAction,
                                    boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    /* Campaing plan */
    public void updateCampaignFromXML(Element campaignElement,
                                      Campaign pcampaign, boolean isSupOption, XMLLoaderPlugin pXMLLoader)
        throws Exception {

    }

    public void updateExecutionFromXML(Element execElement,
                                       Execution pExecution, boolean isSupOption,
                                       XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void updateResExecutionFromXML(Element resExecElement,
                                          ExecutionResult pExecRes, boolean isSupOption,
                                          XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void updateDataSetFromXML(Element dataSetElement, DataSet pDataSet,
                                     boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    /* Data */
    public void updateParameterFromXML(Element paramElement, Parameter pParam,
                                       boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void updateEnvironmentFromXML(Element envElement, Environment pEnv,
                                         boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception {

    }

    public void refreshNewData() {
        try {
            reloadReq();
        } catch (Exception e) {
        }
    }

    boolean doImport() {
        if (mandatoryImport)
            return true;
        if (importReqBox == null || !importReqBox.isSelected()) {
            return false;
        }
        return true;
    }

    boolean doExport() {
        if (mandatoryExport)
            return true;
        if (exportReqBox == null || !exportReqBox.isSelected()) {
            return false;
        }
        return true;
    }

    /******************* IHM *******************/
    JCheckBox importReqBox;
    JPanel importReqPanel;

    JCheckBox exportReqBox;
    JPanel exportReqPanel;

    public JPanel getExportOptionPanel() {
        exportReqBox = new JCheckBox(Language.getInstance().getText(
                                                                    "Exporter_les_exigences"));
        exportReqBox.setSelected(true);
        exportReqPanel = new JPanel();
        exportReqPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        exportReqPanel.add(exportReqBox);
        exportReqPanel.setName(Language.getInstance().getText("Exigences"));
        return exportReqPanel;
    }

    public JPanel getImportOptionPanel() {
        importReqBox = new JCheckBox(/*Language.getInstance().getText(
                                       "Importer_les_exigences")*/);
        importReqBox.setSelected(true);
        importReqPanel = new JPanel();
        importReqPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        importReqPanel.add(importReqBox);
        importReqPanel.setName(Language.getInstance().getText("Exigences"));
        return importReqPanel;
    }

    /**************** XSLT *******************/
    public File getXSLToImport(boolean dynamicMode, boolean multiFrame,
                               boolean htmlFormat) {
        String _urlBase = SalomeTMFContext.getInstance().getUrlBase()
            .toString();
        String url_txt = _urlBase.substring(0, _urlBase.lastIndexOf("/"));
        String xsl = "";
        if (dynamicMode) {
            if (htmlFormat) {
                if (multiFrame) {
                    xsl = "/plugins/requirements/xsl/frameDynaRequirements.xsl";
                } else {
                    xsl = "/plugins/requirements/xsl/dynaRequirements.xsl";
                }
            } else
                xsl = "/plugins/requirements/xsl/dynaRequirementsDocBook.xsl";

        } else {// staticMode
            if (htmlFormat) {
                if (multiFrame) {
                    xsl = "/plugins/requirements/xsl/frameRequirements.xsl";
                } else {
                    xsl = "/plugins/requirements/xsl/requirements.xsl";
                }
            } else
                xsl = "/plugins/requirements/xsl/requirementsDocBook.xsl";
        }
        String temporaryFilePath = System.getProperties().getProperty(
                                                                      "java.io.tmpdir")
            + File.separator + "requirementsXSLT.xsl";
        try {
            URL xsltURL = new URL(url_txt + xsl);
            Tools.writeFile(xsltURL.openStream(), temporaryFilePath);
        } catch (Exception e) {
            Tools.writeFile(ReqPlugin.class.getResourceAsStream("/salome/"
                                                                + xsl), temporaryFilePath);
        }
        File xsltFile = new File(temporaryFilePath);
        return xsltFile;
    }

    public File getTranslationFile() {
        String _urlBase = SalomeTMFContext.getInstance().getUrlBase()
            .toString();
        String url_txt = _urlBase.substring(0, _urlBase.lastIndexOf("/"));
        String translate = "/plugins/requirements/xsl/translate.xml";
        String temporaryFilePath = System.getProperties().getProperty(
                                                                      "java.io.tmpdir")
            + File.separator + "requirementsTranslate.xml";
        try {
            URL translateURL = new URL(url_txt + translate);
            Tools.writeFile(translateURL.openStream(), temporaryFilePath);
        } catch (Exception e) {
            Tools.writeFile(ReqPlugin.class.getResourceAsStream("/salome/"
                                                                + translate), temporaryFilePath);
        }
        File translateFile = new File(temporaryFilePath);
        return translateFile;
    }

    /**************************************** XMLLoaderPlugin ***************************************/
    String fs = System.getProperties().getProperty("file.separator");
    String dirXml;

    public void updateElementAttachement(Element envElem,
                                         WithAttachment simpleElement, boolean newElement) {
        try {
            // URL
            List urlAttachementList = envElem
                .selectNodes("Attachements/UrlAttachement");
            Iterator itEnvUrlAttach = urlAttachementList.iterator();
            while (itEnvUrlAttach.hasNext()) {
                Element urlElem = (Element) itEnvUrlAttach.next();
                String url = urlElem.attributeValue("url");
                String description = (urlElem.elementText("Description") == null) ? ""
                    : urlElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (newElement == false) {
                    HashMap envMap = simpleElement.getAttachmentMapFromModel();
                    Iterator itEnvAttachs = envMap.values().iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = (Attachment) itEnvAttachs.next();
                        if ((attach instanceof UrlAttachment)
                            && ((UrlAttachment) attach).getNameFromModel()
                            .equals(url)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    simpleElement.addAttachementInDBAndModel(new UrlAttachment(
                                                                               url, description));
                } else {
                    ((UrlAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }
            }
            // fichiers
            List envFileAttachementList = envElem
                .selectNodes("Attachements/FileAttachement");
            Iterator itEnvFileAttach = envFileAttachementList.iterator();
            while (itEnvFileAttach.hasNext()) {
                Element fileElem = (Element) itEnvFileAttach.next();
                String dirAtt = fileElem.attributeValue("dir");
                String nom = fileElem.attributeValue("nom");
                dirAtt = restorePath(dirAtt);
                File f = new File(dirXml + fs + dirAtt);
                String description = (fileElem.elementText("Description") == null) ? ""
                    : fileElem.elementText("Description");
                description = description.replaceAll("\\\\n", "\n");
                boolean appartient = false;
                Attachment attach = null;
                if (newElement == false) {
                    HashMap envMap = simpleElement.getAttachmentMapFromModel();
                    Iterator itEnvAttachs = envMap.values().iterator();
                    while (itEnvAttachs.hasNext() && !appartient) {
                        attach = (Attachment) itEnvAttachs.next();
                        if ((attach instanceof FileAttachment)
                            && ((FileAttachment) attach).getNameFromModel()
                            .equals(nom)) {
                            appartient = true;
                        }
                    }
                }
                if (!appartient) {
                    attach = new FileAttachment(f, description);
                    simpleElement.addAttachementInDBAndModel(attach);
                } else {
                    ((FileAttachment) attach).updateInDBAndModel(f);
                    ((FileAttachment) attach)
                        .updateDescriptionInDBdAndModel(description);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String restorePath(String path) throws Exception {
        if (fs.equals("\\")) {
            // Unix2Windows
            return path.replace('/', '\\');
        } else {
            return path.replace('\\', '/');
        }
    }

    private Vector getAllReqLeavesByCatAndPri(int cat, int pri) {
        Vector res = new Vector();

        JTree reqTree = getTreeRequirement();
        if (reqTree != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) reqTree
                                                                    .getModel()).getRoot();
            salomeTMF_plug.requirements.data.Requirement pTempReq = (salomeTMF_plug.requirements.data.Requirement) node
                .getUserObject();
            salomeTMF_plug.requirements.data.ReqFamily reqRoot = (salomeTMF_plug.requirements.data.ReqFamily) pTempReq;

            if (reqRoot != null) {
                Vector allLeaves = reqRoot.getAllLeaf();
                for (int i = 0; i < allLeaves.size(); i++) {
                    ReqLeaf req = (ReqLeaf) allLeaves.elementAt(i);
                    if ((req.getCatFromModel() == cat)
                        && (req.getPriorityFromModel() == pri)) {
                        res.add(req);
                    }
                }
            }
        }

        return res;
    }

    private Vector getAllReqLeavesByCat(int cat) {
        Vector res = new Vector();

        JTree reqTree = getTreeRequirement();
        if (reqTree != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) ((DefaultTreeModel) reqTree
                                                                    .getModel()).getRoot();
            salomeTMF_plug.requirements.data.Requirement pTempReq = (salomeTMF_plug.requirements.data.Requirement) node
                .getUserObject();
            salomeTMF_plug.requirements.data.ReqFamily reqRoot = (salomeTMF_plug.requirements.data.ReqFamily) pTempReq;

            if (reqRoot != null) {
                Vector allLeaves = reqRoot.getAllLeaf();
                for (int i = 0; i < allLeaves.size(); i++) {
                    ReqLeaf req = (ReqLeaf) allLeaves.elementAt(i);
                    if (req.getCatFromModel() == cat) {
                        res.add(req);
                    }
                }
            }
        }

        return res;
    }

    // ***********************************************************************************************//
    // Interface RegManager //
    // ***********************************************************************************************//

    public int getNbReqForCat(int cat) {
        int res = 0;
        Vector reqs = getAllReqLeavesByCat(cat);
        if (reqs != null) {
            res = reqs.size();
        }
        return res;
    }

    public boolean isAllReqCovered(int cat, int pri) {
        boolean res = true;

        Vector reqs = getAllReqLeavesByCatAndPri(cat, pri);
        if ((reqs != null) && (reqs.size() != 0)) {
            int i = 0;
            while ((i < reqs.size()) && res) {
                ReqLeaf req = (ReqLeaf) reqs.elementAt(i);
                Vector associatedTests = new Vector();
                try {
                    associatedTests = req.getTestWrapperCoveredFromDB();
                } catch (Exception e) {
                    // TODO
                    e.printStackTrace();
                }
                res = (associatedTests != null)
                    && (associatedTests.size() != 0);
                i++;
            }
        }

        return res;
    }

    public boolean isAllReqCoveredExecuted(int cat, int pri) {
        boolean res = true;

        Vector reqs = getAllReqLeavesByCatAndPri(cat, pri);
        if (!isAllReqCovered(cat, pri)) {
            res = false;
        } else {
            int i = 0;
            while ((i < reqs.size()) && res) {
                ReqLeaf req = (ReqLeaf) reqs.elementAt(i);
                Vector associatedTests = new Vector();
                try {
                    associatedTests = req.getTestWrapperCoveredFromDB();
                } catch (Exception e) {
                    // TODO
                    e.printStackTrace();
                }
                int j = 0;
                while ((j < associatedTests.size()) && res) {
                    TestWrapper testWrap = (TestWrapper) associatedTests
                        .elementAt(j);
                    Test test = DataModel.getCurrentProject().getTestFromModel(
                                                                               testWrap.getIdBDD());
                    if (test == null && !isGlobalProject()) {
                        if (getProjectRef().getIdBdd() == DataModel
                            .getCurrentProject().getIdBdd()) {
                            DataModel.reloadFromBase(true);
                            test = DataModel.getCurrentProject()
                                .getTestFromModel(testWrap.getIdBDD());
                        }
                        if (test == null) {
                            res = false;
                        }
                    }
                    ArrayList campaignList = DataModel.getCurrentProject()
                        .getCampaignOfTest(test);
                    if (campaignList == null || campaignList.size() == 0) {
                        res = false;
                    } else {
                        int k = 0;
                        int size = campaignList.size();
                        while (k < size && res) {
                            Campaign pCampaign = (Campaign) campaignList.get(k);
                            if (!pCampaign.containsExecutionResultInModel()) {
                                res = false;
                            }
                            k++;
                        }
                    }
                    j++;
                }
                i++;
            }
        }

        return res;
    }

    public Vector getAssociatedTestsForReqCat(int cat) {
        Vector tests = new Vector();
        Vector reqs = getAllReqLeavesByCat(cat);

        if ((reqs != null) && (reqs.size() != 0)) {
            for (int i = 0; i < reqs.size(); i++) {
                ReqLeaf req = (ReqLeaf) reqs.elementAt(i);
                Vector testWrappers = new Vector();
                try {
                    testWrappers = req.getTestWrapperCoveredFromDB();
                } catch (Exception e) {
                    // TODO
                    e.printStackTrace();
                }
                if (testWrappers != null) {
                    for (int j = 0; j < testWrappers.size(); j++) {
                        TestWrapper testWrap = (TestWrapper) testWrappers
                            .elementAt(j);
                        if (testWrap != null) {
                            Test test = DataModel.getCurrentProject()
                                .getTestFromModel(testWrap.getIdBDD());
                            if (test == null && !isGlobalProject()) {
                                if (getProjectRef().getIdBdd() == DataModel
                                    .getCurrentProject().getIdBdd()) {
                                    DataModel.reloadFromBase(true);
                                    test = DataModel.getCurrentProject()
                                        .getTestFromModel(
                                                          testWrap.getIdBDD());
                                }
                            }
                            if (test != null) {
                                tests.add(test);
                            }
                        }
                    }
                }
            }
        }
        return tests;
    }

    public String getReqManagerName() {
        return REQ_NAME;
    }

    public void deleteReqLinkWithTest(int idTest) {
        try {
            Requirement.deleteAllCoverForTest(idTest);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    public void setExport(boolean b) {
        mandatoryExport = b;
    }

    public void setImport(boolean b) {
        mandatoryImport = b;
    }

    public String getChapterTitleInReport() {
        if (doExport()) {
            return Language.getInstance().getText("Exigences");
        } else {
            return null;
        }
    }

    public String getParameterName(String chapterName) {
        if (chapterName.equals(Language.getInstance().getText("Exigences"))) {
            return "requirements";
        }
        return null;
    }

    public void addReqLinkWithTest(Test test, int idReq) {
        try {
            if (pSQLRequirement == null) {
                SQLWrapper.init(Api.getISQLObjectFactory());
                pSQLRequirement = SQLWrapper.getSQLRequirement();
            }
            pSQLRequirement.addReqConvert(idReq, test.getIdBdd());
        } catch (Exception e) {
            Util
                .log("[ReqPlugin->addReqLinkWithTest] Unable to link test and requirement");
        }
    }

    public void updateReqHMI(int type, String value) {
        RequirementDescriptionPanel panel = pRequirementPanel
            .getpRequirementDescriptionPanel();
        switch (type) {
        case PRIORITY:
            if (!panel.itemPriority.contains(value)) {
                panel.priorityBox.addItem(value);
            }
            break;
        case CATEGORY:
            if (!panel.itemCat.contains(value)) {
                panel.catBox.addItem(value);
                panel.pInfoFiltrePanel.catBox.addItem(value);
            }
            break;
        case COMPLEXE:
            if (!panel.itemComplex.contains(value)) {
                panel.complexeBox.addItem(value);
                panel.pInfoFiltrePanel.complexeBox.addItem(value);
            }
            break;
        case CRITICAL:
            if (!panel.itemCritical.contains(value)) {
                panel.criticalBox.addItem(value);
            }
            break;
        case OWNER:
            if (!panel.itemOwner.contains(value)) {
                panel.ownerBox.addItem(value);
            }
            break;
        case STATE:
            if (!panel.itemState.contains(value)) {
                panel.stateBox.addItem(value);
                panel.pInfoFiltrePanel.stateBox.addItem(value);
            }
            break;
        case UNDERSTANDING:
            if (!panel.itemUnderstanding.contains(value)) {
                panel.understandingBox.addItem(value);
            }
            break;
        case VALID_BY:
            if (!panel.itemValidBy.contains(value)) {
                panel.validByBox.addItem(value);
            }
            break;
        }
    }
}
