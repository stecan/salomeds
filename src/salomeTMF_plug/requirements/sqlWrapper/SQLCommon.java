/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.sqlWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110 Jira : FORTF - 38
 */
public class SQLCommon {

    public void installPlugin() throws Exception {
        PreparedStatement prep = SQLWrapper.getSQLCommonQuery("showTable"); // ok
        ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
        boolean trouve = false;
        while (stmtRes.next() && !trouve) {
            String tableName = stmtRes.getString(1);
            String nomTable = "REQUIREMENTS";
            if (tableName.equals(nomTable)
                || tableName.equals(nomTable.toLowerCase())) {
                trouve = true;
            }
        }
        if (trouve == false) {
            // INSTALL TABLE OF PLUGINS
            int transNumber = -1;
            try {
                transNumber = SQLWrapper.beginTransaction();

                prep = SQLWrapper.getSQLCommonQuery("createTableReq"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableReqAttach"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableReqLink"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableHistory"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableReqReference"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableReqLinkAction"); // ok
                SQLWrapper.runAddQuery(prep);

                // 20100115 - D�but modification Forge ORTF
                prep = SQLWrapper.getSQLCommonQuery("createTablePriority"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableCategory"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableComplexe"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableState"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableValidBy"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableCritical"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableUnderstanding"); // ok
                SQLWrapper.runAddQuery(prep);

                prep = SQLWrapper.getSQLCommonQuery("createTableOwner"); // ok
                SQLWrapper.runAddQuery(prep);
                // 20100115 - Fin modification Forge ORTF

                SQLWrapper.commitTrans(transNumber);
            } catch (Exception e) {
                SQLWrapper.rollBackTrans(transNumber);
                throw e;
            }
            try {
                ISQLConfig pISQLConfig = Api.getISQLObjectFactory()
                    .getISQLConfig();
                pISQLConfig.insertSalomeConf("req_plug_version", "1.5");
            } catch (Exception e) {

            }
        } else {
            // UPDATE PLUGINS
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            String version = pISQLConfig.getSalomeConf("req_plug_version");
            System.out.println("Version : " + version);
            int transNumber = -1;
            if (version == null) {
                try {
                    transNumber = SQLWrapper.beginTransaction();
                    // UPDATE TABLE 1.2
                    prep = SQLWrapper.getSQLCommonQuery("addPriorityFiled"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addVersionField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // 1.3
                    prep = SQLWrapper.getSQLCommonQuery("addCatField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addComplexField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOrigineField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addStateField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addVerifeField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addReferenceField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // 1.4
                    prep = SQLWrapper.getSQLCommonQuery("createTableHistory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqReference"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqLinkAction"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // 20100115 - D�but modification Forge ORTF
                    prep = SQLWrapper.getSQLCommonQuery("createTablePriority"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCategory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableComplexe"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableState"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableValidBy"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCritical"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableUnderstanding"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableOwner"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addValidByField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addCriticalField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("addUnderstandingField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOwnerField"); // ok
                    SQLWrapper.runAddQuery(prep);
                    // 20100115 - Fin modification Forge ORTF

                    // UPDATE VALUE
                    SQLWrapper.commitTrans(transNumber);
                } catch (Exception e) {
                    SQLWrapper.rollBackTrans(transNumber);
                    throw e;
                }
                try {
                    pISQLConfig.insertSalomeConf("req_plug_version", "1.5");
                } catch (Exception e) {

                }
            } else if (version.equals("1.2")) {
                try {
                    transNumber = SQLWrapper.beginTransaction();

                    // UPDATE TABLE FROM 1.3
                    prep = SQLWrapper.getSQLCommonQuery("addCatField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addComplexField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOrigineField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addStateField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addVerifeField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addReferenceField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // FROM 1.4
                    prep = SQLWrapper.getSQLCommonQuery("createTableHistory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqReference"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqLinkAction"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // 20100115 - Debut modification Forge ORTF
                    prep = SQLWrapper.getSQLCommonQuery("createTablePriority"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCategory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableComplexe"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableState"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableValidBy"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCritical"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableUnderstanding"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableOwner"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addValidByField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addCriticalField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("addUnderstandingField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOwnerField"); // ok
                    SQLWrapper.runAddQuery(prep);
                    // 20100115 - Fin modification Forge ORTF

                    // UPDATE VALUE
                    SQLWrapper.commitTrans(transNumber);
                } catch (Exception e) {
                    SQLWrapper.rollBackTrans(transNumber);
                    throw e;
                }
                try {
                    pISQLConfig.updateSalomeConf("req_plug_version", "1.5");
                } catch (Exception e) {

                }
            } else if (version.equals("1.3")) {
                try {
                    transNumber = SQLWrapper.beginTransaction();

                    // FROM 1.4
                    prep = SQLWrapper.getSQLCommonQuery("createTableHistory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqReference"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableReqLinkAction"); // ok
                    SQLWrapper.runAddQuery(prep);

                    // 20100115 - D�but modification Forge ORTF
                    prep = SQLWrapper.getSQLCommonQuery("createTablePriority"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCategory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableComplexe"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableState"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableValidBy"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCritical"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableUnderstanding"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableOwner"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addValidByField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addCriticalField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("addUnderstandingField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOwnerField"); // ok
                    SQLWrapper.runAddQuery(prep);
                    // 20100115 - Fin modification Forge ORTF

                    // UPDATE VALUE
                    SQLWrapper.commitTrans(transNumber);
                } catch (Exception e) {
                    SQLWrapper.rollBackTrans(transNumber);
                    throw e;
                }
                try {
                    pISQLConfig.updateSalomeConf("req_plug_version", "1.5");
                } catch (Exception e) {

                }
            } else if (version.equals("1.4")) {
                try {
                    transNumber = SQLWrapper.beginTransaction();

                    // 20100115 - D�but modification Forge ORTF
                    prep = SQLWrapper.getSQLCommonQuery("createTablePriority"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCategory"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableComplexe"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableState"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableValidBy"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableCritical"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("createTableUnderstanding"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("createTableOwner"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addValidByField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addCriticalField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper
                        .getSQLCommonQuery("addUnderstandingField"); // ok
                    SQLWrapper.runAddQuery(prep);

                    prep = SQLWrapper.getSQLCommonQuery("addOwnerField"); // ok
                    SQLWrapper.runAddQuery(prep);
                    // 20100115 - Fin modification Forge ORTF

                    // UPDATE VALUE
                    SQLWrapper.commitTrans(transNumber);
                } catch (Exception e) {
                    SQLWrapper.rollBackTrans(transNumber);
                    throw e;
                }
                try {
                    pISQLConfig.updateSalomeConf("req_plug_version", "1.5");
                } catch (Exception e) {

                }
            }

        }
    }
}
