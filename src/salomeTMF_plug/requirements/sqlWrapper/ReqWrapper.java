/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.sqlWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.data.DataWrapper;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110 Jira : FORTF - 38
 */
public class ReqWrapper extends DataWrapper {
    int parent_id;
    int type;
    int project_id;

    // From 1.2
    String version = "1";
    int priority = 100;

    // FROM 1.3
    int cat = 0;
    int complexe = 100;
    String origine = "Marketing";
    int state = 0;
    String verif = "";
    String reference = "";

    // 20100115 - D�but modification Forge ORTF
    int validBy = 1;
    int critical = 1;
    int understanding = 1;
    int owner = 1;

    static IDataBase db;
    static ISQLEngine pISQLEngine;

    static Hashtable catMap;
    static Hashtable stateMap;
    static Hashtable complexMap;
    static Hashtable validByMap;
    static Hashtable criticalMap;
    static Hashtable understandingMap;
    static Hashtable ownerMap;
    static {
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();

        catMap = new Hashtable();
        String sql = "SELECT * FROM REQ_CATEGORY;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String category = result.getString(2);
                int id = result.getInt(1);
                catMap.put(new Integer(id), category);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        stateMap = new Hashtable();
        sql = "SELECT * FROM REQ_STATE;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String state = result.getString(2);
                int id = result.getInt(1);
                stateMap.put(new Integer(id), state);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        complexMap = new Hashtable();
        sql = "SELECT * FROM REQ_COMPLEXE;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String complexe = result.getString(2);
                int id = result.getInt(1);
                complexMap.put(new Integer(id), complexe);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        validByMap = new Hashtable();
        sql = "SELECT * FROM REQ_VALID_BY;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String validBy = result.getString(2);
                int id = result.getInt(1);
                validByMap.put(new Integer(id), validBy);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        criticalMap = new Hashtable();
        sql = "SELECT * FROM REQ_CRITICAL;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String critical = result.getString(2);
                int id = result.getInt(1);
                criticalMap.put(new Integer(id), critical);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        understandingMap = new Hashtable();
        sql = "SELECT * FROM REQ_UNDERSTANDING;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String understanding = result.getString(2);
                int id = result.getInt(1);
                understandingMap.put(new Integer(id), understanding);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        ownerMap = new Hashtable();
        sql = "SELECT * FROM REQ_OWNER;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String owner = result.getString(2);
                int id = result.getInt(1);
                ownerMap.put(new Integer(id), owner);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    static public String getComplexString(int i) {
        String sql = "SELECT name FROM REQ_COMPLEXE where id=" + i + ";";
        String complex = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                complex = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return complex;
    }

    static public String getCatString(int i) {
        String sql = "SELECT name FROM REQ_CATEGORY where id=" + i + ";";
        String category = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                category = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return category;
    }

    static public String getStateString(int i) {
        String sql = "SELECT name FROM REQ_STATE where id=" + i + ";";
        String state = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                state = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return state;
    }

    static public String getValidByString(int i) {
        String sql = "SELECT name FROM REQ_VALID_BY where id=" + i + ";";
        String validBy = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                validBy = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return validBy;
    }

    static public String getCriticalString(int i) {
        String sql = "SELECT name FROM REQ_CRITICAL where id=" + i + ";";
        String critical = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                critical = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return critical;
    }

    static public String getUnderstandingString(int i) {
        String sql = "SELECT name FROM REQ_UNDERSTANDING where id=" + i + ";";
        String understanding = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                understanding = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return understanding;
    }

    static public String getOwnerString(int i) {
        String sql = "SELECT name FROM REQ_OWNER where id=" + i + ";";
        String owner = "";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            if (result.next()) {
                owner = result.getString(1);
            }
            result.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return owner;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getProject_id() {
        return project_id;
    }

    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    // FROM 1.2
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    // From 1.3
    public int getCat() {
        return cat;
    }

    public void setCat(int cat) {
        this.cat = cat;
    }

    public int getComplexe() {
        return complexe;
    }

    public void setComplexe(int complexe) {
        this.complexe = complexe;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getVerif() {
        return verif;
    }

    public void setVerif(String verif) {
        this.verif = verif;
    }

    public int getValidBy() {
        return validBy;
    }

    public void setValidBy(int validBy) {
        this.validBy = validBy;
    }

    public int getCritical() {
        return critical;
    }

    public void setCritical(int critical) {
        this.critical = critical;
    }

    public int getUnderstanding() {
        return understanding;
    }

    public void setUnderstanding(int understanding) {
        this.understanding = understanding;
    }

    public int getOwner() {
        return owner;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    // 20100115 - Fin modification Forge ORTF

    public boolean equals(Object o) {
        if (o instanceof ReqWrapper) {
            ReqWrapper other = (ReqWrapper) o;
            return (getIdBDD() == other.getIdBDD());
        } else {
            return false;
        }
    }

    public int hashCode() {
        if (getIdBDD() > 0) {
            return getIdBDD();
        }
        return super.hashCode();
    }

}
