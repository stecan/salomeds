package salomeTMF_plug.requirements.sqlWrapper;

import java.sql.Date;

public class HistoryWrapper {
    int idReq;
    int code;
    int idUser;
    Date date;
    String valeur;
    public HistoryWrapper() {
                
    }
    public HistoryWrapper(int idReq,int code,int idUser, Date date,String valeur){
        this.idReq = idReq ;
        this.code = code ;
        this.idUser =idUser ;
        this.date =date ;
        this.valeur = valeur ; 
    }
        
    public int getCode() {
        return code;
    }
    public Date getDate() {
        return date;
    }
    public int getIdReq() {
        return idReq;
    }
    public int getIdUser() {
        return idUser;
    }
    public String getValeur() {
        return valeur;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public void setIdReq(int idReq) {
        this.idReq = idReq;
    }
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    public void setValeur(String valeur) {
        this.valeur = valeur;
    }
        
}
