package salomeTMF_plug.requirements.sqlWrapper;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020 
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public interface ISQLRequirement extends Remote {
        
    /******************* CODE HYSTORY ****************/
    public final int HIST_CREATE_REQ = 11; 
    public final int HIST_ADD_REQLEAF = 12; 
    public final int HIST_ADD_REQFAM = 13; 
    public final int HIST_ADD_ATTACH = 14;
    public final int HIST_ADD_COVER = 15;
    public final int HIST_ADD_LINK = 16;
        
    public final int HIST_DEL_REQ = 101; 
    public final int HIST_DEL_ATTACH = 102;
    public final int HIST_DEL_COVER = 103;
    public final int HIST_DEL_LINK = 104;
        
    public final int HIST_UPDATE_NAME = 1001; 
    public final int HIST_UPDATE_DESCRIPTION = 1002; 
    public final int HIST_UPDATE_VERSION = 1003; 
    public final int HIST_UPDATE_REFERENCE = 1004; 
    public final int HIST_UPDATE_INFO = 1005; 
    public final int HIST_UPDATE_PRIORITY = 1006; 
    public final int HIST_UPDATE_CATEGORY = 1007; 
    public final int HIST_UPDATE_COMPLEXITY = 1008; 
    public final int HIST_UPDATE_ORIGINE = 1009; 
    public final int HIST_UPDATE_SATE = 1010; 
    public final int HIST_UPDATE_VERIFWAY = 1011; 
    public final int HIST_UPDATE_PARENT = 1012; 
    //20100115 - Debut modification Forge ORTF
    public final int HIST_UPDATE_VALID_BY = 1013; 
    public final int HIST_UPDATE_CRITICAL = 1014; 
    public final int HIST_UPDATE_UNDERSTANDING = 1015; 
    public final int HIST_UPDATE_OWNER = 1016;
    //20100115 - Fin modification Forge ORTF
        
        
    /**
     * Insert a requirments in database
     * @param name
     * @param description
     * @param req_type :  1  for requirement or 0 for requirement set
     * @param id_parent
     * @param id_project
     * @return the id of the requirment
     * @throws Exception
     */
    public ReqWrapper add(String name, String description, int req_type, int id_parent, int id_project) throws Exception;
        
    /**
     * Attach a file attach to a test in table ATTACHEMENT and reference in table REQ_ATTACHEMENT
     * @param idReq : id of the requirement
     * @param f : the file
     * @param description of the file
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @See ISQLFileAttachment.insert(File, String);
     * no permission needed
     */
    public int addAttachFile(int idReq, SalomeFileWrapper f, String description) throws Exception;
        
    /**
     * Attach a url to a test in table ATTACHEMENT and reference in table REQ_ATTACHEMENT
     * @param idReq : id of the requirement
     * @param url
     * @param description of the url
     * @return the id of the attachment in the table ATTACHEMENT
     * @see ISQLUrlAttachment.insert(String, String);
     * @throws Exception
     */
    public int addAttachUrl(int idReq, String url, String description) throws Exception;
        
    public void addReqConvert(int idReq, int idTest)  throws Exception;
        
    public void updateName(int idReq, String newName, int id_parent, int idProjet) throws Exception;
        
    public void updateDescription(int idReq, String newdescription) throws Exception;
        
    public void updateVersion(int idReq, String version) throws Exception;
        
    public void updateReference(int idReq, String reference) throws Exception;
        
    public void updateInfo(int idReq, String _version, String _origine, String _verif, String _reference) throws Exception;
        
    public void updatePriority(int idReq, int priority) throws Exception;
        
    /**
     * 
     * @param idReq
     * @param cat  0 to 7
     * @throws Exception
     */
    public void updateCat(int idReq, int cat) throws Exception;
        
    public void updateComplexe(int idReq, int comp) throws Exception;
        
    public void updateOrigine(int idReq, String org) throws Exception;
        
    public void updateState(int idReq, int state) throws Exception;
        
    public void updateVerif(int idReq, String verif) throws Exception;
        
    public void updateParent(int idReq, int idParent) throws Exception;
        
    //20100115 - Debut modification Forge ORTF
    public void updateValidBy(int idReq, int validBy) throws Exception;
        
    public void updateCritical(int idReq, int critical) throws Exception;
        
    public void updateUnderstanding(int idReq, int understanding) throws Exception;
        
    public void updateOwner(int idReq, int owner) throws Exception;
    //20100115 - Fin modification Forge ORTF
        
    public void deleteReq(int idReq) throws Exception;
        
    public void deleteAllTestCover(int idTest) throws Exception;
        
    public void deleteAllReqCover(int idReq) throws Exception;
        
    public void deleteCover(int idReq, int idTest) throws Exception;
        
    /**
     * Delete all attachment in table (REQ_ATTACHEMENT and ATTACHEMENT) for the test
     * @param idReq : id of the requirement
     * @throws Exception
     * @see deleteAttach(int, int)
     * no permission needed
     */
    public void deleteAllAttach(int idReq ) throws Exception;
        
    /**
     * Delete attachement for the test and the attachement (tables REQ_ATTACHEMENT, ATTACHEMENT)
     * @param idReq : id of the requirement
     * @param attachId : id of the attachment
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idReq , int attachId) throws Exception;
        
        
    public ReqWrapper getReq(String name, int id_parent, int id_project ) throws Exception;
        
    public ReqWrapper getReqById(int idReq) throws Exception;
        
        
    public ReqWrapper[] getProjectRequirements(int idProject) throws Exception;
        
    public ReqWrapper[] getProjectRequirementByType(int idProject, int type) throws Exception;
        
    public ReqWrapper[] getFistChildInReqFamily(int idReq) throws Exception;
        
    public ReqWrapper[] getReqWrapperCoveredByCamp(int idCamp) throws Exception;
        
    public boolean isReqReqCoveredByTest(int idReq, int idTest) throws Exception;
        
    public ReqWrapper[] getReqCoveredByTest(int idTest) throws Exception;
        
    public ReqWrapper[] getCoveredReq(int idProject) throws Exception;
        
    public ReqWrapper[] getReqCoveredByResExecAndStatus(int idExec, String satus) throws Exception;
        
        
    public TestWrapper[] getTestCoveredForReq(int idReq) throws Exception;
        
    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the requirement identified by idReq
     * @param idReq : id of the requirement
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int idReq) throws Exception;
        
    /**
     * Get a Vector of FileAttachementWrapper for the requirement identified by idreq
     * @param idReq : id of the requirement
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int idReq) throws Exception;
        
    /**
     * Get a Vector of UrlAttachementWrapper for the requirement identified by idReq
     * @param idReq : id of the requirement
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idReq) throws Exception;
        
    public void deleteProjectReq(int idProject )throws Exception;
        
    public HistoryWrapper[] getHistory(int idReq) throws Exception ;

}
