/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.sqlWrapper;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;

public class SQLWrapper {
    static Properties addSqlQuery;
    static Properties deleteSqlQuery;
    static Properties updateSqlQuery;
    static Properties selectSqlQuery;
    static Properties comonQuery;

    static IDataBase db;

    static SQLCommon pSQLCommon = new SQLCommon();
    static ISQLRequirement pSQLRequirement = new SQLRequirement();

    static ISQLEngine pISQLEngine = null;

    public static void init(ISQLObjectFactory pISQLObjectFactory)
        throws Exception {
        addSqlQuery = Util
            .getPropertiesFile(SQLWrapper.class
                               .getResource("/salomeTMF_plug/requirements/resources/sql/addSqlQuery.properties"));

        comonQuery = Util
            .getPropertiesFile(SQLWrapper.class
                               .getResource("/salomeTMF_plug/requirements/resources/sql/commonSqlQuery.properties"));

        updateSqlQuery = Util
            .getPropertiesFile(SQLWrapper.class
                               .getResource("/salomeTMF_plug/requirements/resources/sql/updateSqlQuery.properties"));

        selectSqlQuery = Util
            .getPropertiesFile(SQLWrapper.class
                               .getResource("/salomeTMF_plug/requirements/resources/sql/selectSqlQuery.properties"));

        deleteSqlQuery = Util
            .getPropertiesFile(SQLWrapper.class
                               .getResource("/salomeTMF_plug/requirements/resources/sql/delSqlQuery.properties"));

        db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        Util.log("[SQLWrapper->init] database is " + db);
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();

        if (pISQLEngine != null && !Api.isSOAP()) {
            try {
                String SqlLockWrite = comonQuery.getProperty("lockWrite");
                Util.log("[SQLWrapper->init] Add lock Write " + SqlLockWrite
                         + " to " + pISQLEngine);
                String SqlLockRead = comonQuery.getProperty("lockWrite");
                Util.log("[SQLWrapper->init] Add lock Read " + SqlLockRead
                         + " to " + pISQLEngine);
                installPlugin();
                pISQLEngine.addSQLLoackWrite(SqlLockWrite);
                pISQLEngine.addSQLLoackRead(SqlLockRead);
            } catch (Exception e) {
                Util.err(e);
            }
        }
    }

    static void installPlugin() throws Exception {
        pSQLCommon.installPlugin();
    }

    public static ISQLRequirement getSQLRequirement() {
        // Selection de la factory en fonction de la version (SQL ou SOAP)

        // Version SQL
        ISQLRequirement pSQLRequirement = SQLWrapper.pSQLRequirement;

        if (Api.isSOAP()) {
            // Version SOAP
            try {
                ISQLSoapFactory pISQLSoapFactory = (ISQLSoapFactory) Class
                    .forName(
                             "salomeTMF_plug.requirements.soap.SQLSoapFactory")
                    .newInstance();
                pSQLRequirement = pISQLSoapFactory.getISQLRequirement();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return pSQLRequirement;
    }

    public static PreparedStatement getSQLAddQuery(String queryName)
        throws Exception {
        Util.log("[SQLWrapper->getSQLAddQuery]" + queryName);
        String sql = addSqlQuery.getProperty(queryName);
        Util.log("[SQLWrapper->getSQLAddQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLCommonQuery(String queryName)
        throws Exception {
        Util.log("[SQLWrapper->getSQLCommonQuery]" + queryName);
        String sql = comonQuery.getProperty(queryName);
        Util.log("[SQLWrapper->getSQLCommonQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLSelectQuery(String queryName)
        throws Exception {
        Util.log("[SQLWrapper->getSQLSelectQuery]" + queryName + " : "
                 + selectSqlQuery);
        String sql = selectSqlQuery.getProperty(queryName);
        Util.log("[SQLWrapper->getSQLSelectQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLUpdateQuery(String queryName)
        throws Exception {
        Util.log("[SQLWrapper->getSQLUpdateQuery]" + queryName);
        String sql = updateSqlQuery.getProperty(queryName);
        Util.log("[SQLWrapper->getSQLUpdateQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLDeleteQuery(String queryName)
        throws Exception {
        Util.log("[SQLWrapper->getSQLDeleteQuery]" + queryName);
        String sql = deleteSqlQuery.getProperty(queryName);
        Util.log("[SQLWrapper->getSQLDeleteQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static synchronized int runAddQuery(PreparedStatement prep)
        throws Exception {
        return prep.executeUpdate();
    }

    static synchronized int runDeleteQuery(PreparedStatement prep)
        throws Exception {
        return prep.executeUpdate();
    }

    static synchronized int runUpdateQuery(PreparedStatement prep)
        throws Exception {
        return prep.executeUpdate();
    }

    static synchronized ResultSet runSelectQuery(PreparedStatement prep)
        throws Exception {
        return prep.executeQuery();
    }

    static public synchronized int beginTransaction() throws Exception {
        if (pISQLEngine != null) {
            return pISQLEngine.beginTransDB(0, 1000);
        }
        return -1;
    }

    static synchronized public void commitTrans(int id) throws Exception {
        if (pISQLEngine != null) {
            pISQLEngine.commitTransDB(id);
        }
    }

    static synchronized public void rollBackTrans(int id) throws Exception {
        if (pISQLEngine != null) {
            pISQLEngine.rollForceBackTransDB(id);
        }
    }

}
