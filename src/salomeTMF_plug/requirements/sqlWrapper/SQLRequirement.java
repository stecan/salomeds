/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.sqlWrapper;

import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110 Jira : FORTF - 38
 */
public class SQLRequirement implements ISQLRequirement {

    int idPersonne = 0;

    // 20100301 - D�but modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;

    final String REQUIREMENTS_CFG_FILE = "/cfg/Requirements.properties";

    // 201010301 - Fin modification Forge ORTF

    /**
     * Insert a requirments in database
     *
     * @param name
     * @param description
     * @param req_type
     *            : 1 for requirement or 0 for requirement set
     * @param id_parent
     * @param id_project
     * @return the id of the requirment
     * @throws Exception
     */
    public ReqWrapper add(String name, String description, int req_type,
                          int id_parent, int id_project) throws Exception {
        int id = -1;
        if (req_type < 0) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        if (id_parent < 0) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        if (id_project < 0) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        if (name == null || name.equals("")) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        if (description == null) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        ReqWrapper pReqWrapper = getReq(name, id_parent, id_project);
        if (pReqWrapper != null) {
            throw new Exception(
                                "[SQLRequirement] requirment name already exist");
        }

        int transNumber = -1;

        try {
            // 201010301 - D�but modification Forge ORTF
            ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
            try {
                db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            Properties prop = new Properties();
            try {
                URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
                String url_txt = _urlBase.toString();
                url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
                URL urlBase = new URL(url_txt);

                java.net.URL url_cfg = new java.net.URL(urlBase
                                                        + REQUIREMENTS_CFG_FILE);
                try {
                    prop = Util.getPropertiesFile(url_cfg);
                } catch (Exception e) {
                    prop = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultPriority = prop.getProperty("Priorite_default");
            int priorityDefault = 1;
            String sql = "SELECT id FROM REQ_PRIORITY where name='"
                + defaultPriority + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    priorityDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultCategory = prop.getProperty("Categorie_default");
            int categoryDefault = 1;
            sql = "SELECT id FROM REQ_CATEGORY where name='" + defaultCategory
                + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    categoryDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultComplexe = prop.getProperty("Complexite_default");
            int complexeDefault = 1;
            sql = "SELECT id FROM REQ_COMPLEXE where name='" + defaultComplexe
                + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    complexeDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultState = prop.getProperty("Statut_default");
            int stateDefault = 1;
            sql = "SELECT id FROM REQ_STATE where name='" + defaultState + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    stateDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultValidBy = prop.getProperty("Valide_default");
            int validByDefault = 1;
            sql = "SELECT id FROM REQ_VALID_BY where name='" + defaultValidBy
                + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    validByDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultCritical = prop.getProperty("Criticite_default");
            int criticalDefault = 1;
            sql = "SELECT id FROM REQ_CRITICAL where name='" + defaultCritical
                + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    criticalDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String defaultUnderstanding = prop
                .getProperty("Comprehension_default");
            int understandingDefault = 1;
            sql = "SELECT id FROM REQ_UNDERSTANDING where name='"
                + defaultUnderstanding + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    understandingDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String defaultOwner = prop.getProperty("Proprietaire_default");
            int ownerDefault = 1;
            sql = "SELECT id FROM REQ_OWNER where name='" + defaultOwner + "';";
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    ownerDefault = result.getInt(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 201010301 - Fin modification Forge ORTF
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLAddQuery("addRequirement");
            prep.setInt(1, id_project);
            prep.setString(2, name);
            prep.setString(3, description);
            prep.setInt(4, id_parent);
            prep.setInt(5, req_type);
            prep.setInt(6, priorityDefault);
            prep.setInt(7, categoryDefault);
            prep.setInt(8, complexeDefault);
            prep.setInt(9, stateDefault);
            prep.setInt(10, validByDefault);
            prep.setInt(11, criticalDefault);
            prep.setInt(12, understandingDefault);
            prep.setInt(13, ownerDefault);
            SQLWrapper.runAddQuery(prep);

            pReqWrapper = getReq(name, id_parent, id_project);
            id = pReqWrapper.getIdBDD();

            /* HISTORY */
            try {
                addHistory(id, idPersonne, HIST_CREATE_REQ, name);
                if (id_parent != 0) {
                    int code = HIST_ADD_REQLEAF;
                    if (req_type == 0) {
                        code = HIST_ADD_REQFAM;
                    }
                    addHistory(id_parent, idPersonne, code, name);
                }
            } catch (Exception eH) {

            }
            if (id == id_parent) {
                throw new Exception(
                                    "[SQLRequirement->add] A requeriement canot have same id of his parent");
            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
        return pReqWrapper;
    }

    /**
     * Attach a file attach to a test in table ATTACHEMENT and reference in
     * table REQ_ATTACHEMENT
     *
     * @param idReq
     *            : id of the requirement
     * @param f
     *            : the file
     * @param description
     *            of the file
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @See ISQLFileAttachment.insert(File, String); no permission needed
     */
    public int addAttachFile(int idReq, SalomeFileWrapper f, String description)
        throws Exception {
        if (idReq < 1 || f == null) {
            throw new Exception(
                                "[SQLRequirement->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            idAttach = Api.getISQLObjectFactory().getISQLFileAttachment()
                .insert(f, description);

            PreparedStatement prep = SQLWrapper
                .getSQLAddQuery("addAttachToReq");
            prep.setInt(1, idReq);
            prep.setInt(2, idAttach);
            SQLWrapper.runAddQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_ADD_ATTACH, f.getName()
                           + " [id = " + idAttach + "]");
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->addAttachFile]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
        return idAttach;
    }

    /**
     * Attach a url to a test in table ATTACHEMENT and reference in table
     * REQ_ATTACHEMENT
     *
     * @param idReq
     *            : id of the requirement
     * @param url
     * @param description
     *            of the url
     * @return the id of the attachment in the table ATTACHEMENT
     * @see ISQLUrlAttachment.insert(String, String);
     * @throws Exception
     */
    public int addAttachUrl(int idReq, String url, String description)
        throws Exception {
        if (idReq < 1 || url == null) {
            throw new Exception(
                                "[SQLRequirement->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            idAttach = Api.getISQLObjectFactory().getISQLUrlAttachment()
                .insert(url, description);

            PreparedStatement prep = SQLWrapper
                .getSQLAddQuery("addAttachToReq");
            prep.setInt(1, idReq);
            prep.setInt(2, idAttach);
            SQLWrapper.runAddQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_ADD_ATTACH, url + " [id = "
                           + idAttach + "]");
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->addAttachUrl]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        return idAttach;
    }

    public void addReqConvert(int idReq, int idTest) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->addReqConvert] data entry are not valid");
        }
        if (idTest < 0) {
            throw new Exception(
                                "[SQLRequirement->addReqConvert] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper.getSQLAddQuery("addReqCover");
            prep.setInt(1, idReq);
            prep.setInt(2, idTest);

            SQLWrapper.runAddQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_ADD_COVER, "" + idTest);
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->addReqConvert]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    public void updateName(int idReq, String newName, int id_parent,
                           int idProjet) throws Exception {

        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateName] data entry are not valid");
        }
        if (idProjet < 0) {
            throw new Exception(
                                "[SQLRequirement->updateName] data entry are not valid");
        }
        if (newName == null || newName.equals("")) {
            throw new Exception(
                                "[SQLRequirement->updateName] data entry are not valid");
        }

        ReqWrapper pReqWrapper = getReq(newName, id_parent, idProjet);
        if (pReqWrapper != null) {
            throw new Exception(
                                "[SQLRequirement->updateName] requiment name already exist");
        }

        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateReqName");
            prep.setString(1, newName);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_NAME, newName);
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateDescription(int idReq, String newdescription)
        throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateDescription] data entry are not valid");
        }
        if (newdescription == null) {
            throw new Exception(
                                "[SQLRequirement->updateDescription] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateReqDescription");
            prep.setString(1, newdescription);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);

            /* HISTORY */
            try {
                String strDesc = newdescription;
                if (strDesc.length() > 254) {
                    strDesc = strDesc.substring(0, 254);
                }
                addHistory(idReq, idPersonne, HIST_UPDATE_DESCRIPTION, strDesc);
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateVersion(int idReq, String version) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateVersion] data entry are not valid");
        }
        if (version == null) {
            throw new Exception(
                                "[SQLRequirement->updateVersion] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateVersion");
            prep.setString(1, version);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_VERSION, version);
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateReference(int idReq, String reference) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateReference] data entry are not valid");
        }
        if (reference == null) {
            throw new Exception(
                                "[SQLRequirement->updateReference] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateReference");
            prep.setString(1, reference);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_REFERENCE, reference);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateInfo(int idReq, String _version, String _origine,
                           String _verif, String _reference) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateInfo] data entry are not valid");
        }
        if (_version == null) {
            throw new Exception(
                                "[SQLRequirement->updateInfo] data entry are not valid");
        }
        if (_origine == null) {
            throw new Exception(
                                "[SQLRequirement->updateInfo] data entry are not valid");
        }
        if (_verif == null) {
            throw new Exception(
                                "[SQLRequirement->updateInfo] data entry are not valid");
        }
        if (_reference == null) {
            throw new Exception(
                                "[SQLRequirement->updateInfo] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper.getSQLUpdateQuery("updateInfo");
            prep.setString(1, _version);
            prep.setString(2, _origine);
            prep.setString(3, _verif);
            prep.setString(4, _reference);
            prep.setInt(5, idReq);
            SQLWrapper.runUpdateQuery(prep);

            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_INFO, "[" + _origine
                           + "]; [" + _reference + "]; [" + _version + "]; ["
                           + _verif + "]");
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updatePriority(int idReq, int priority) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        if (priority < 1) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updatePriority");
            prep.setInt(1, priority);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_PRIORITY, ""
                           + priority);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     *
     * @param idReq
     * @param cat
     *            0 to 7
     * @throws Exception
     */
    public void updateCat(int idReq, int cat) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateCat] data entry are not valid");
        }
        if (cat < 0) {
            throw new Exception(
                                "[SQLRequirement->updateCat] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper.getSQLUpdateQuery("updateCat");
            prep.setInt(1, cat);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_CATEGORY, "" + cat);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateComplexe(int idReq, int comp) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateComplexe] data entry are not valid");
        }
        if (comp < 1) {
            throw new Exception(
                                "[SQLRequirement->updateComplexe] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateComplexe");
            prep.setInt(1, comp);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_COMPLEXITY, "" + comp);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateOrigine(int idReq, String org) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateOrigine] data entry are not valid");
        }
        if (org == null) {
            org = "";
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateOrigine");
            prep.setString(1, org);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_ORIGINE, org);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateState(int idReq, int state) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateState] data entry are not valid");
        }
        if (state < 0) {
            throw new Exception(
                                "[SQLRequirement->updateState] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateState");
            prep.setInt(1, state);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_SATE, "" + state);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateVerif(int idReq, String verif) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateVerif] data entry are not valid");
        }
        if (verif == null) {
            verif = "";
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateVerif");
            prep.setString(1, verif);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_VERIFWAY, verif);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateParent(int idReq, int idParent) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updateParent] data entry are not valid");
        }
        if (idParent < 0) {
            throw new Exception(
                                "[SQLRequirement->updateParent] data entry are not valid");
        }
        if (idParent == idReq) {
            throw new Exception(
                                "[SQLRequirement->updateParent] data entry are not valid parent equals req");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateReqParent");
            prep.setInt(1, idParent);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_PARENT, "" + idParent);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    // 20100115 - D�but modification Forge ORTF
    public void updateValidBy(int idReq, int validBy) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        if (validBy < 1) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateValidBy");
            prep.setInt(1, validBy);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_VALID_BY, ""
                           + validBy);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateCritical(int idReq, int critical) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        if (critical < 1) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateCritical");
            prep.setInt(1, critical);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_CRITICAL, ""
                           + critical);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateUnderstanding(int idReq, int understanding)
        throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        if (understanding < 1) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateUnderstanding");
            prep.setInt(1, understanding);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_UNDERSTANDING, ""
                           + understanding);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    public void updateOwner(int idReq, int owner) throws Exception {
        int transNumber = -1;
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        if (owner < 1) {
            throw new Exception(
                                "[SQLRequirement->updatePriority] data entry are not valid");
        }
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLUpdateQuery("updateOwner");
            prep.setInt(1, owner);
            prep.setInt(2, idReq);
            SQLWrapper.runUpdateQuery(prep);
            /* HISTORY */
            try {
                addHistory(idReq, idPersonne, HIST_UPDATE_OWNER, "" + owner);
            } catch (Exception eH) {

            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    // 20100115 - Fin modification Forge ORTF

    public void deleteReq(int idReq) throws Exception {
        if (idReq < 0) {
            throw new Exception("[SQLRequirement] data entry are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            ReqWrapper[] reqChild = getFistChildInReqFamily(idReq);
            int sizeChild = reqChild.length;
            for (int i = 0; i < sizeChild; i++) {
                ReqWrapper pReqWrapper = (ReqWrapper) reqChild[i];
                deleteReq(pReqWrapper.getIdBDD());
            }

            simpleDeleteReq(idReq);
            /* NO History because delete req */

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
    }

    protected void simpleDeleteReq(int idReq) throws Exception {
        // Delete LINK
        deleteAllReqCover(idReq);

        // Delete Action LINK
        deleteAllReqActionCover(idReq);

        // Delete Attachement
        deleteAllAttach(idReq);

        // Delete History
        deleteAllHistory(idReq);

        // Delete Reference
        deleteAllReference(idReq);

        // Delete Requirement
        PreparedStatement prep = SQLWrapper
            .getSQLDeleteQuery("deleteRequirement");
        prep.setInt(1, idReq);
        SQLWrapper.runDeleteQuery(prep);

    }

    public void deleteAllTestCover(int idTest) throws Exception {
        if (idTest < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAllTestCover] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            /* DO History */
            ReqWrapper[] reqConcerned = getReqCoveredByTest(idTest);
            int sizeConcerned = reqConcerned.length;
            for (int i = 0; i < sizeConcerned; i++) {
                ReqWrapper pReqWrapper = (ReqWrapper) reqConcerned[i];
                /* HISTORY */
                try {
                    addHistory(pReqWrapper.getIdBDD(), idPersonne,
                               HIST_DEL_COVER, "" + idTest);
                } catch (Exception eH) {

                }
            }

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllTestCover");
            prep.setInt(1, idTest);
            SQLWrapper.runDeleteQuery(prep);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteAllTestCover]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    public void deleteAllReqCover(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->deteleAllReqCover] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllReqCover");
            prep.setInt(1, idReq);
            SQLWrapper.runDeleteQuery(prep);
            /* NO History because delete req */

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deteleAllReqCover]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    protected void deleteAllReqActionCover(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAllReqActionCover] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllReqCoverAction");
            prep.setInt(1, idReq);
            SQLWrapper.runDeleteQuery(prep);
            /* NO History because delete req */

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteAllReqActionCover]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    public void deleteCover(int idReq, int idTest) throws Exception {
        if (idReq < 1 || idTest < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteCover] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteOneCover");
            prep.setInt(1, idTest);
            prep.setInt(2, idReq);
            SQLWrapper.runDeleteQuery(prep);
            /* DO History */
            try {
                addHistory(idReq, idPersonne, HIST_DEL_COVER, "" + idTest);
            } catch (Exception eH) {

            }

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteCover]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    /**
     * Delete all attachment in table (REQ_ATTACHEMENT and ATTACHEMENT) for the
     * test
     *
     * @param idReq
     *            : id of the requirement
     * @throws Exception
     * @see deleteAttach(int, int) no permission needed
     */
    public void deleteAllAttach(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            AttachementWrapper[] attachList = getAllAttachemnt(idReq);

            for (int i = 0; i < attachList.length; i++) {
                AttachementWrapper pAttachementWrapper = (AttachementWrapper) attachList[i];
                deleteAttach(idReq, pAttachementWrapper.getIdBDD(), false);
            }
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteAllAttach]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    /**
     * Delete attachement for the test and the attachement (tables
     * REQ_ATTACHEMENT, ATTACHEMENT)
     *
     * @param idReq
     *            : id of the requirement
     * @param attachId
     *            : id of the attachment
     * @throws Exception
     * @see ISQLAttachment.delete(int) no permission needed
     */
    public void deleteAttach(int idReq, int attachId) throws Exception {
        deleteAttach(idReq, attachId, true);
    }

    protected void deleteAttach(int idReq, int attachId, boolean history)
        throws Exception {
        if (idReq < 1 || attachId < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAttachFromReq");
            prep.setInt(1, idReq);
            prep.setInt(2, attachId);
            SQLWrapper.runDeleteQuery(prep);
            if (history) {
                /* DO History */
                try {
                    addHistory(idReq, idPersonne, HIST_DEL_ATTACH, ""
                               + attachId);
                } catch (Exception eH) {
                }
            }
            Api.getISQLObjectFactory().getISQLAttachment().delete(attachId);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->deleteAttach]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    public ReqWrapper getReq(String name, int id_parent, int id_project)
        throws Exception {
        if (name == null || name.equals("")) {
            throw new Exception(
                                "[SQLRequirement->getReq] data entry are not valid");
        }
        if (id_project < 0) {
            throw new Exception(
                                "[SQLRequirement->getReq] data entry are not valid");
        }

        ReqWrapper pReqWrapper = null;
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectRequirement");
            prep.setString(1, name);
            prep.setInt(2, id_parent);
            prep.setInt(3, id_project);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            if (stmtRes.next()) {
                pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
            }

        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getReq]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
        return pReqWrapper;
    }

    public ReqWrapper getReqById(int idReq) throws Exception {
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->getReqById] data entry are not valid");
        }
        ReqWrapper pReqWrapper = null;
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectRequirementByID");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            if (stmtRes.next()) {
                pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getReqById]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
        return pReqWrapper;
    }

    public ReqWrapper[] getProjectRequirements(int idProject) throws Exception {
        if (idProject < 0) {
            throw new Exception(
                                "[SQLRequirement->getProjectRequirements] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectAllRequirement");
            prep.setInt(1, idProject);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getProjectRequirements]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public ReqWrapper[] getProjectRequirementByType(int idProject, int type)
        throws Exception {
        if (idProject < 0) {
            throw new Exception(
                                "[SQLRequirement->getProjectRequirements] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectAllRequirementByType");
            prep.setInt(1, idProject);
            prep.setInt(2, type);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getProjectRequirementByType]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public ReqWrapper[] getFistChildInReqFamily(int idReq) throws Exception {
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->getFistChildInReqFamily] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectRequirementsInFamily");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getFistChildInReqFamily]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public ReqWrapper[] getReqWrapperCoveredByCamp(int idCamp) throws Exception {
        if (idCamp < 0) {
            throw new Exception(
                                "[SQLRequirement->getReqCoveredByTest] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectRequirementsForCamp");
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getReqCoveredByTest]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public boolean isReqReqCoveredByTest(int idReq, int idTest)
        throws Exception {
        if (idTest < 0) {
            throw new Exception(
                                "[SQLRequirement->isReqReqCoveredByTest] data entry are not valid");
        }
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->isReqReqCoveredByTest] data entry are not valid");
        }
        boolean res = false;
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectTestRequirement");
            prep.setInt(1, idReq);
            prep.setInt(2, idTest);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);

            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            if (stmtRes.next()) {
                res = true;
            }

        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->isReqReqCoveredByTest]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
        return res;
    }

    public ReqWrapper[] getReqCoveredByTest(int idTest) throws Exception {
        if (idTest < 0) {
            throw new Exception(
                                "[SQLRequirement->getReqCoveredByTest] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectRequirementsForTest");
            prep.setInt(1, idTest);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getReqCoveredByTest]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public ReqWrapper[] getCoveredReq(int idProject) throws Exception {
        if (idProject < 0) {
            throw new Exception(
                                "[SQLRequirement->getCoveredReq] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectReqCovered");
            prep.setInt(1, idProject);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getCoveredReq]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public ReqWrapper[] getReqCoveredByResExecAndStatus(int idExec, String satus)
        throws Exception {
        if (idExec < 0) {
            throw new Exception(
                                "[SQLRequirement->getReqCoveredByTest] data entry are not valid");
        }
        if (satus == null || satus.equals("")) {
            throw new Exception(
                                "[SQLRequirement->getReqCoveredByTest] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectStatusRequirementsForResExec");
            prep.setInt(1, idExec);
            prep.setString(2, satus);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                ReqWrapper pReqWrapper = new ReqWrapper();
                pReqWrapper.setIdBDD(stmtRes.getInt("id_req"));
                pReqWrapper.setProject_id(stmtRes.getInt("id_project"));
                pReqWrapper.setName(stmtRes.getString("req_name"));
                pReqWrapper
                    .setDescription(stmtRes.getString("req_description"));
                pReqWrapper.setParent_id(stmtRes.getInt("id_req_parent"));
                pReqWrapper.setType(stmtRes.getInt("req_type"));
                // FROM 1.2
                pReqWrapper.setPriority(stmtRes.getInt("priority"));
                pReqWrapper.setVersion(stmtRes.getString("version"));
                // FROM 1.3
                pReqWrapper.setCat(stmtRes.getInt("cat"));
                pReqWrapper.setComplexe(stmtRes.getInt("complexe"));
                pReqWrapper.setOrigine(stmtRes.getString("origine"));
                pReqWrapper.setState(stmtRes.getInt("state"));
                pReqWrapper.setVerif(stmtRes.getString("verif"));
                pReqWrapper.setReference(stmtRes.getString("reference"));
                // 20100115 - D�but modification Forge ORTF
                pReqWrapper.setValidBy(stmtRes.getInt("valid_by"));
                pReqWrapper.setCritical(stmtRes.getInt("critical"));
                pReqWrapper.setUnderstanding(stmtRes.getInt("understanding"));
                pReqWrapper.setOwner(stmtRes.getInt("owner"));
                // 20100115 - Fin modification Forge ORTF
                res.add(pReqWrapper);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getReqCoveredByResExecAndStatus]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        ReqWrapper[] tmpArray = new ReqWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (ReqWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    public TestWrapper[] getTestCoveredForReq(int idReq) throws Exception {
        if (idReq < 0) {
            throw new Exception(
                                "[SQLRequirement->getTestCoveredForReq] data entry are not valid");
        }
        Vector res = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectTestCoveredReq");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                TestWrapper pTest = new TestWrapper();
                pTest.setName(stmtRes.getString("nom_cas"));
                pTest.setDescription(stmtRes.getString("description_cas"));
                pTest.setIdBDD(stmtRes.getInt("id_cas"));
                pTest.setOrder(stmtRes.getInt("ordre_cas"));
                pTest.setIdSuite(stmtRes.getInt("SUITE_TEST_id_suite"));
                pTest.setType(stmtRes.getString("type_cas"));
                res.add(pTest);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getTestCoveredForReq]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        TestWrapper[] tmpArray = new TestWrapper[res.size()];
        for (int tmpI = 0; tmpI < res.size(); tmpI++) {
            tmpArray[tmpI] = (TestWrapper) res.get(tmpI);
        }

        return tmpArray;
    }

    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper,
     * UrlAttachementWrapper) for the requirement identified by idReq
     *
     * @param idReq
     *            : id of the requirement
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->getAllAttachemnt] entry data are not valid");
        }
        // Vector result = new Vector();
        //
        // Vector fileList = getAllAttachFiles(idReq);
        // Vector urlList = getAllAttachUrls(idReq);
        // result.addAll(fileList);
        // result.addAll(urlList);

        FileAttachementWrapper[] fileList = getAllAttachFiles(idReq);
        UrlAttachementWrapper[] urlList = getAllAttachUrls(idReq);

        AttachementWrapper[] result = new AttachementWrapper[fileList.length
                                                             + urlList.length];

        for (int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for (int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }

        return result;
    }

    /**
     * Get a Vector of FileAttachementWrapper for the requirement identified by
     * idreq
     *
     * @param idReq
     *            : id of the requirement
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int idReq)
        throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->getAllAttachFiles] entry data are not valid");
        }
        ArrayList<FileAttachementWrapper> result = new ArrayList<FileAttachementWrapper>();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectReqAttachFiles");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                FileAttachementWrapper fileAttach = new FileAttachementWrapper();
                fileAttach.setName(stmtRes.getString("nom_attach"));
                fileAttach.setLocalisation("");
                fileAttach.setDate(stmtRes.getDate("date_attachement"));
                fileAttach.setSize(new Long(stmtRes
                                            .getLong("taille_attachement")));
                fileAttach.setDescription(stmtRes
                                          .getString("description_attach"));
                fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
                result.add(fileAttach);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getAllAttachFiles]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        return result.toArray(new FileAttachementWrapper[result.size()]);
        /*
         * FileAttachementWrapper[] tmpArray = new
         * FileAttachementWrapper[result.size()]; for(int tmpI = 0; tmpI <
         * result.size(); tmpI++) { tmpArray[tmpI] = (FileAttachementWrapper)
         * result.get(tmpI); }
         *
         * return tmpArray;
         */
    }

    /**
     * Get a Vector of UrlAttachementWrapper for the requirement identified by
     * idReq
     *
     * @param idReq
     *            : id of the requirement
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectReqAttachUrls");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
                String url = stmtRes.getString("url_attach");
                // pUrlAttachment.setUrl(url);
                pUrlAttachment.setName(url);
                pUrlAttachment.setDescription(stmtRes
                                              .getString("description_attach"));
                ;
                pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
                result.addElement(pUrlAttachment);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getAllAttachUrls]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }

        UrlAttachementWrapper[] tmpArray = new UrlAttachementWrapper[result
                                                                     .size()];
        for (int tmpI = 0; tmpI < result.size(); tmpI++) {
            tmpArray[tmpI] = (UrlAttachementWrapper) result.get(tmpI);
        }

        return tmpArray;
    }

    public void deleteProjectReq(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteProjectReq] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            /* ALL Reference test link */
            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllProjectReq1");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Reference attachement */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq2");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Reference action linlk */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq4");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Reference history */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq5");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Reference source */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq6");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Reference target */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq7");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            /* ALL Requirement */
            prep = SQLWrapper.getSQLDeleteQuery("deleteAllProjectReq3");
            prep.setInt(1, idProject);
            SQLWrapper.runDeleteQuery(prep);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->deleteProjectReq]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    /********************************** History *************************************************/

    protected void deleteAllHistory(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAllHistory] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllHistory");
            prep.setInt(1, idReq);
            SQLWrapper.runDeleteQuery(prep);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteAllHistory]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    protected void deleteAllReference(int idReq) throws Exception {
        if (idReq < 1) {
            throw new Exception(
                                "[SQLRequirement->deleteAllReference] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper
                .getSQLDeleteQuery("deleteAllReferenceSource");
            prep.setInt(1, idReq);
            SQLWrapper.runDeleteQuery(prep);

            prep = SQLWrapper.getSQLDeleteQuery("deleteAllReferenceTarget");
            prep.setInt(1, idReq);
            SQLWrapper.runDeleteQuery(prep);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLTest->deleteAllHistory]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    /**
     * Add History when a requiremnt change (create, update, deleted)
     *
     * @param idReq
     * @param idUser
     * @param code
     * @param valeur
     */
    protected void addHistory(int idReq, int idUser, int code, String valeur)
        throws Exception {
        if (idReq < 1 || code < 0) {
            throw new Exception(
                                "[SQLRequirement->addHistory] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();

            PreparedStatement prep = SQLWrapper.getSQLAddQuery("addReqHistory");
            prep.setInt(1, idReq);
            prep.setInt(2, idUser);
            prep.setDate(3, getCurrentDate());
            prep.setInt(4, code);
            prep.setString(5, valeur);

            SQLWrapper.runAddQuery(prep);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->addHistory]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
    }

    public HistoryWrapper[] getHistory(int idReq) throws Exception {
        ArrayList<HistoryWrapper> result = new ArrayList<HistoryWrapper>();
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            PreparedStatement prep = SQLWrapper
                .getSQLSelectQuery("selectReqHistory");
            prep.setInt(1, idReq);
            ResultSet stmtRes = SQLWrapper.runSelectQuery(prep);
            SQLWrapper.commitTrans(transNumber);
            transNumber = -1;
            while (stmtRes.next()) {
                int code = stmtRes.getInt("code");
                int idUser = stmtRes.getInt("PERSONNE_id_personne");
                Date date = stmtRes.getDate("date");
                String valeur = stmtRes.getString("valeur");
                HistoryWrapper history = new HistoryWrapper(idReq, code,
                                                            idUser, date, valeur);
                result.add(history);
            }
        } catch (Exception e) {
            SQLWrapper.rollBackTrans(transNumber);
            Util.log("[SQLRequirement->getAllAttachFiles]" + e);
            Util.err(e);
            /*
             * if (Api.isDEBUG()){ e.printStackTrace(); }
             */
            throw e;
        }
        // HistoryWrapper[] ret;
        return result.toArray(new HistoryWrapper[result.size()]);
    }

    protected Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }
}
