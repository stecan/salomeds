package salomeTMF_plug.requirements;

import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import org.dom4j.Document;
import org.dom4j.Element;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.XMLLoaderPlugin;

import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.ihm.RequirementDescriptionPanel;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
/**
 * @covers SFD_ForgeORTF_EXG_IMP_000400
 * @covers SFD_ForgeORTF_EXG_IMP_000410
 * @covers SFD_ForgeORTF_EXG_IMP_000420 Jira : FORTF - 43
 */
public class ImportXMLReq {

    // ReqPlugin pReqPlugin = null;
    ReqPlugin pReqPlugin = null;

    Hashtable projectReqByName;
    Hashtable projectReqById;
    Hashtable docReqById;
    Hashtable docElementById;
    Hashtable docReqByName;
    // ReqFamily reqRoot;
    ReqFamily reqRoot;
    // boolean importReq;
    XMLLoaderPlugin pImport;

    // 20100119 - D\ufffdbut modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;
    RequirementDescriptionPanel pRequirementDescriptionPanel;
    final String REQUIREMENTS_CFG_FILE = "/cfg/Requirements.properties";
    Properties reqProp;

    // 20100119 - Fin modification Forge ORTF

    ImportXMLReq(ReqPlugin _pReqPlugin, XMLLoaderPlugin pXMLLoader,
                 RequirementDescriptionPanel _pReqDescriptionPanel) {
        // Requirements
        pReqPlugin = _pReqPlugin;
        // importReq = _importReq;
        pImport = pXMLLoader;
        projectReqByName = new Hashtable();
        projectReqById = new Hashtable();
        docReqByName = new Hashtable();
        docReqById = new Hashtable();
        docElementById = new Hashtable();
        // 20100119 - D\ufffdbut modification Forge ORTF
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
        pRequirementDescriptionPanel = _pReqDescriptionPanel;
        try {
            URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            URL urlBase = new URL(url_txt);

            java.net.URL url_cfg = new java.net.URL(urlBase
                                                    + REQUIREMENTS_CFG_FILE);
            try {
                reqProp = Util.getPropertiesFile(url_cfg);
            } catch (Exception e) {
                reqProp = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 20100119 - Fin modification Forge ORTF
    }

    void loadRequirementCacheFromModel(DefaultMutableTreeNode pTempNode,
                                       boolean first) throws Exception {
        Requirement pTempReq = (Requirement) pTempNode.getUserObject();
        if (!first) {
            projectReqByName.put(pTempReq.getLongName(), pTempReq);
            projectReqById.put(new Integer(pTempReq.getIdBdd()), pTempReq);
        } else {
            first = false;
        }

        if (pTempReq instanceof ReqLeaf) {
            return;
        } else {
            int nbChild = pTempNode.getChildCount();

            for (int i = 0; i < nbChild; i++) {
                loadRequirementCacheFromModel(
                                              (DefaultMutableTreeNode) pTempNode.getChildAt(i), first);
            }
        }
    }

    void loadRequirementCacheFromXML(Document doc) throws Exception {
        docElementById.clear();
        docReqById.clear();
        docReqByName.clear();
        reqRoot = new ReqFamily("Requirements_"
                                + ReqPlugin.getProjectRef().getNameFromModel(), "");
        docReqById.put(new Integer(0), reqRoot);
        Iterator itReqFamilyfXML = doc.selectNodes("//RequirementFamily")
            .iterator();
        while (itReqFamilyfXML.hasNext()) {
            Element reqElem = (Element) itReqFamilyfXML.next();
            String nomReq = reqElem.elementText("Nom");
            String desc = (reqElem.elementText("Description") == null) ? ""
                : reqElem.elementText("Description");
            if (reqElem.element("Description") != null
                && reqElem.element("Description").attribute("isHTML") != null
                && reqElem.element("Description").attributeValue("isHTML")
                .equals("true")) {
                desc = giveHTMLDescription(reqElem);
            } else {
                desc = desc.replaceAll("\\\\n", "<br>");
                desc = "<html><head></head><body>" + desc + "</body></html>";
            }
            String id_reqDoc = reqElem.attributeValue("id_req");
            id_reqDoc = id_reqDoc.substring(4, id_reqDoc.length()); // remove
            // Req_
            String id_reqParentDoc = reqElem.attributeValue("id_req_parent");
            id_reqParentDoc = id_reqParentDoc.substring(4, id_reqParentDoc
                                                        .length()); // remove Req_

            // docReqByName.put(nomReq, reqElem);
            try {
                docElementById.put(new Integer(id_reqDoc), reqElem);
            } catch (java.lang.NumberFormatException e) {
                e.printStackTrace();
                continue;
            }
            ReqWrapper pReqWrapper = new ReqWrapper();
            pReqWrapper.setName(nomReq);
            pReqWrapper.setDescription(desc);
            pReqWrapper.setIdBDD(new Integer(id_reqDoc).intValue());
            // pReqWrapper.setType(0);
            ReqFamily pParent;
            try {
                pParent = (ReqFamily) docReqById.get(new Integer(
                                                                 id_reqParentDoc));
            } catch (java.lang.NumberFormatException e) {
                e.printStackTrace();
                continue;
            }
            ReqFamily pReqFamily = new ReqFamily(pReqWrapper, pParent);
            pParent.addRequirement(pReqFamily);
            docReqById.put(new Integer(id_reqDoc), pReqFamily);
        }
        Iterator itReqLeqfXML = doc.selectNodes("//Requirement").iterator();
        while (itReqLeqfXML.hasNext()) {
            Element reqElem = (Element) itReqLeqfXML.next();
            String nomReq = reqElem.elementText("Nom");

            int priority = -1;
            String version = null;
            int cat = -1;
            int comp = -1;
            int state = -1;
            int validBy = -1;
            int critical = -1;
            int understanding = -1;
            int owner = -1;
            String origine = null;
            String verifway = null;
            String reference = null;
            try {
                version = reqElem.attributeValue("version");
                // 20100119 - D\ufffdbut modification Forge ORTF
                // Priority
                String priorite = reqElem.attributeValue("priority");
                if (priorite != null) {
                    String sql = "SELECT id FROM PRIORITY WHERE name='"
                        + priorite + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        priority = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemPriority
                            .contains(priorite)) {
                            pRequirementDescriptionPanel.priorityBox
                                .addItem(priorite);
                        }
                    } else {
                        sql = "INSERT INTO PRIORITY (name,user_entry) values ('"
                            + priorite + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM PRIORITY WHERE name='" + priorite
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            priority = result.getInt(1);
                            pRequirementDescriptionPanel.priorityBox
                                .addItem(priorite);
                        }
                    }
                    result.close();
                } else {
                    String priorityDefault = reqProp
                        .getProperty("Priorite_default");
                    String sql = "SELECT id FROM PRIORITY WHERE name='"
                        + priorityDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        priority = result.getInt(1);
                    }
                }
                // Category
                String category = reqElem.attributeValue("category");
                if (category != null) {
                    String sql = "SELECT id FROM CATEGORY WHERE name='"
                        + category + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        cat = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemCat
                            .contains(category)) {
                            pRequirementDescriptionPanel.catBox
                                .addItem(category);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.catBox
                                .addItem(category);
                        }
                    } else {
                        sql = "INSERT INTO CATEGORY (name,user_entry) values ('"
                            + category + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM CATEGORY WHERE name='" + category
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            cat = result.getInt(1);
                            pRequirementDescriptionPanel.catBox
                                .addItem(category);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.catBox
                                .addItem(category);
                        }
                    }
                    result.close();
                } else {
                    String categoryDefault = reqProp
                        .getProperty("Categorie_default");
                    String sql = "SELECT id FROM CATEGORY WHERE name='"
                        + categoryDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        cat = result.getInt(1);
                    }
                }
                // Complexity
                String complexity = reqElem.attributeValue("complexity");
                if (complexity != null) {
                    String sql = "SELECT id FROM COMPLEXE WHERE name='"
                        + complexity + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        comp = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemComplex
                            .contains(complexity)) {
                            pRequirementDescriptionPanel.complexeBox
                                .addItem(complexity);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.complexeBox
                                .addItem(complexity);
                        }
                    } else {
                        sql = "INSERT INTO COMPLEXE (name,user_entry) values ('"
                            + complexity + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM COMPLEXE WHERE name='"
                            + complexity + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            comp = result.getInt(1);
                            pRequirementDescriptionPanel.complexeBox
                                .addItem(complexity);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.complexeBox
                                .addItem(complexity);
                        }
                    }
                    result.close();
                } else {
                    String complexeDefault = reqProp
                        .getProperty("Complexite_default");
                    String sql = "SELECT id FROM COMPLEXE WHERE name='"
                        + complexeDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        comp = result.getInt(1);
                    }
                }
                // State
                String etat = reqElem.attributeValue("state");
                if (etat != null) {
                    String sql = "SELECT id FROM STATE WHERE name='" + etat
                        + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        state = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemState
                            .contains(etat)) {
                            pRequirementDescriptionPanel.stateBox.addItem(etat);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.stateBox
                                .addItem(etat);
                        }
                    } else {
                        sql = "INSERT INTO STATE (name,user_entry) values ('"
                            + etat + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM STATE WHERE name='" + etat + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            state = result.getInt(1);
                            pRequirementDescriptionPanel.stateBox.addItem(etat);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.stateBox
                                .addItem(etat);
                        }
                    }
                    result.close();
                } else {
                    String stateDefault = reqProp.getProperty("Statut_default");
                    String sql = "SELECT id FROM STATE WHERE name='"
                        + stateDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        state = result.getInt(1);
                    }
                }
                // CheckedBy
                String checkedBy = reqElem.attributeValue("checkedBy");
                if (checkedBy != null) {
                    String sql = "SELECT id FROM VALID_BY WHERE name='"
                        + checkedBy + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBy = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemValidBy
                            .contains(checkedBy)) {
                            pRequirementDescriptionPanel.validByBox
                                .addItem(checkedBy);
                        }
                    } else {
                        sql = "INSERT INTO VALID_BY (name,user_entry) values ('"
                            + checkedBy + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM VALID_BY WHERE name='"
                            + checkedBy + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            validBy = result.getInt(1);
                            pRequirementDescriptionPanel.validByBox
                                .addItem(checkedBy);
                        }
                    }
                    result.close();
                } else {
                    String validByDefault = reqProp
                        .getProperty("Valide_default");
                    String sql = "SELECT id FROM VALID_BY WHERE name='"
                        + validByDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBy = result.getInt(1);
                    }
                }
                // Criticity
                String criticity = reqElem.attributeValue("criticity");
                if (criticity != null) {
                    String sql = "SELECT id FROM CRITICAL WHERE name='"
                        + criticity + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        critical = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemCritical
                            .contains(criticity)) {
                            pRequirementDescriptionPanel.criticalBox
                                .addItem(criticity);
                        }
                    } else {
                        sql = "INSERT INTO CRITICAL (name,user_entry) values ('"
                            + criticity + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM CRITICAL WHERE name='"
                            + criticity + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            critical = result.getInt(1);
                            pRequirementDescriptionPanel.criticalBox
                                .addItem(criticity);
                        }
                    }
                    result.close();
                } else {
                    String criticalDefault = reqProp
                        .getProperty("Criticite_default");
                    String sql = "SELECT id FROM CRITICAL WHERE name='"
                        + criticalDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        critical = result.getInt(1);
                    }
                }
                // Understanding
                String comprehension = reqElem.attributeValue("understanding");
                if (comprehension != null) {
                    String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                        + comprehension + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        understanding = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemUnderstanding
                            .contains(comprehension)) {
                            pRequirementDescriptionPanel.understandingBox
                                .addItem(comprehension);
                        }
                    } else {
                        sql = "INSERT INTO UNDERSTANDING (name,user_entry) values ('"
                            + comprehension + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                            + comprehension + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            understanding = result.getInt(1);
                            pRequirementDescriptionPanel.understandingBox
                                .addItem(comprehension);
                        }
                    }
                    result.close();
                } else {
                    String understandingDefault = reqProp
                        .getProperty("Comprehension_default");
                    String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                        + understandingDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        understanding = result.getInt(1);
                    }
                }
                // Owner
                String proprio = reqElem.attributeValue("owner");
                if (proprio != null) {
                    String sql = "SELECT id FROM OWNER WHERE name='" + proprio
                        + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        owner = result.getInt(1);
                        if (!pRequirementDescriptionPanel.itemOwner
                            .contains(proprio)) {
                            pRequirementDescriptionPanel.ownerBox
                                .addItem(proprio);
                        }
                    } else {
                        sql = "INSERT INTO OWNER (name,user_entry) values ('"
                            + proprio + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM OWNER WHERE name='" + proprio
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            owner = result.getInt(1);
                            pRequirementDescriptionPanel.ownerBox
                                .addItem(proprio);
                        }
                    }
                    result.close();
                } else {
                    String ownerDefault = reqProp
                        .getProperty("Proprietaire_default");
                    String sql = "SELECT id FROM OWNER WHERE name='"
                        + ownerDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        owner = result.getInt(1);
                    }
                }
                // 20100119 - Fin modification Forge ORTF
                origine = reqElem.attributeValue("origine");
                verifway = reqElem.attributeValue("verifway");
                reference = reqElem.attributeValue("reference");
            } catch (Exception e) {
                // XML from old plugin
            }
            String desc = (reqElem.elementText("Description") == null) ? ""
                : reqElem.elementText("Description");
            if (reqElem.element("Description") != null
                && reqElem.element("Description").attribute("isHTML") != null
                && reqElem.element("Description").attributeValue("isHTML")
                .equals("true")) {
                desc = giveHTMLDescription(reqElem);
            } else {
                desc = desc.replaceAll("\\\\n", "<br>");
                desc = "<html><head></head><body>" + desc + "</body></html>";
            }
            String id_reqDoc = reqElem.attributeValue("id_req");
            id_reqDoc = id_reqDoc.substring(4, id_reqDoc.length()); // remove
            // Req_
            String id_reqParentDoc = reqElem.attributeValue("id_req_parent");
            id_reqParentDoc = id_reqParentDoc.substring(4, id_reqParentDoc
                                                        .length()); // remove Req_

            ReqWrapper pReqWrapper = new ReqWrapper();
            pReqWrapper.setName(nomReq);
            pReqWrapper.setDescription(desc);
            pReqWrapper.setIdBDD(new Integer(id_reqDoc).intValue());
            if (priority != -1) {
                pReqWrapper.setPriority(priority);
            }
            if (version != null) {
                pReqWrapper.setVersion(version);
            }
            if (cat != -1) {
                pReqWrapper.setCat(cat);
            }
            if (comp != -1) {
                pReqWrapper.setComplexe(comp);
            }
            if (state != -1) {
                pReqWrapper.setState(state);
            }
            // 20100118 - D\ufffdbut modification Forge ORTF
            if (validBy != -1) {
                pReqWrapper.setValidBy(validBy);
            }
            if (critical != -1) {
                pReqWrapper.setCritical(critical);
            }
            if (understanding != -1) {
                pReqWrapper.setUnderstanding(understanding);
            }
            if (owner != -1) {
                pReqWrapper.setOwner(owner);
            }
            // 20100118 - Fin modification Forge ORTF
            if (origine != null) {
                pReqWrapper.setOrigine(origine);
            }
            if (verifway != null) {
                pReqWrapper.setVerif(verifway);
            }
            if (reference != null) {
                pReqWrapper.setReference(reference);
            }
            ReqFamily pParent = null;
            try {
                docElementById.put(new Integer(id_reqDoc), reqElem);
                pParent = (ReqFamily) docReqById.get(new Integer(
                                                                 id_reqParentDoc));
            } catch (java.lang.NumberFormatException e) {
                e.printStackTrace();
                continue;
            }
            ReqLeaf pReq = new ReqLeaf(pReqWrapper, pParent);
            pParent.addRequirement(pReq);
            docReqById.put(new Integer(id_reqDoc), pReq);
            docElementById.put(new Integer(id_reqDoc), reqElem);
        }
        Enumeration enumReqs = docReqById.elements();
        while (enumReqs.hasMoreElements()) {
            Requirement pReq = (Requirement) enumReqs.nextElement();
            docReqByName.put(pReq.getLongName(), pReq);
        }
    }

    /**
     * Supprime les liens qui ne sont pas dans le XML
     *
     * @param testElem
     * @param test
     */
    public void removeReqLink(Element testElem, Test test) {
        try {
            Vector listReqWrapper = Requirement.getReqWrapperCoveredByTest(test
                                                                           .getIdBdd());
            int size = listReqWrapper.size();

            for (int i = 0; i < size; i++) {
                ReqWrapper pReqWrapper = (ReqWrapper) listReqWrapper
                    .elementAt(i);
                Requirement pReq = (Requirement) projectReqById
                    .get(new Integer(pReqWrapper.getIdBDD()));

                Iterator itReqLinkXML = testElem.selectNodes(
                                                             "LinkRequirement/RequirementRef").iterator();
                boolean appartient = false;
                while (itReqLinkXML.hasNext() && !appartient) {
                    Element linkElem = (Element) itReqLinkXML.next();
                    String id = linkElem.attributeValue("ref");
                    id = id.substring(4, id.length());
                    Requirement pTmpReq = (Requirement) docReqById
                        .get(new Integer(id));
                    // String nomReq = linkElem.elementText("Nom");
                    if (pReq != null && pTmpReq != null
                        && pTmpReq.getLongName().equals(pReq.getLongName())) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    if (pReq != null) {
                        try {
                            pReq.deleteCoverForTest(test.getIdBdd());
                        } catch (Exception e1) {
                            Tools.ihmExceptionView(e1);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    /**
     * Suprrime les exigences qui ne sont pas dans le XML
     *
     * @param doc
     * @throws Exception
     */
    public void gestionDesSuppressionsRequirements(Document doc)
        throws Exception {

        Enumeration itReqProjet = projectReqByName.elements();

        /* Analyse des requiements feuilles */
        while (itReqProjet.hasMoreElements()) {
            Requirement pReq = (Requirement) itReqProjet.nextElement();
            if (pReq instanceof ReqLeaf) {

                Iterator itReqLeqfXML = doc.selectNodes("//Requirement")
                    .iterator();
                boolean appartient = false;
                Element reqElem = null;
                while (itReqLeqfXML.hasNext() && !appartient) {
                    reqElem = (Element) itReqLeqfXML.next();
                    String id = reqElem.attributeValue("id_req");
                    id = id.substring(4, id.length());
                    Requirement pTmpReq = (Requirement) docReqById
                        .get(new Integer(id));
                    if (pTmpReq.getLongName().equals(pReq.getLongName())) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    try {
                        pReq.deleteInDBAndModel();
                        projectReqByName.remove(pReq.getLongName());
                        projectReqById.remove(new Integer(pReq.getIdBdd()));
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            } else if (pReq.getIdBdd() > 0) {
                Iterator itReqFamXML = doc.selectNodes("//RequirementFamily")
                    .iterator();
                boolean appartient = false;
                Element reqElem = null;
                while (itReqFamXML.hasNext() && !appartient) {
                    reqElem = (Element) itReqFamXML.next();
                    String id = reqElem.attributeValue("id_req");
                    id = id.substring(4, id.length());
                    Requirement pTmpReq = (Requirement) docReqById
                        .get(new Integer(id));
                    if (pTmpReq.getLongName().equals(pReq.getLongName())) {
                        appartient = true;
                    }
                }
                if (!appartient) {
                    try {
                        Vector reqLeafs = ((ReqFamily) pReq).getAllLeaf();
                        if (reqLeafs.size() == 0) {
                            pReq.deleteInDBAndModel();
                            projectReqByName.remove(pReq.getLongName());
                            projectReqById.remove(new Integer(pReq.getIdBdd()));
                        } else {
                            for (int i = 0; i < reqLeafs.size(); i++) {
                                Requirement pTmpReq = (Requirement) reqLeafs
                                    .elementAt(i);
                                projectReqByName.remove(pTmpReq.getLongName());
                            }
                        }
                    } catch (Exception e) {
                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }
    }

    /**
     * Mise a jour des couverture d'exigence avec le doc XML Ajout des nouvelle
     * couverture et suppression des couvertures qui ne sont pas dans le XML si
     * supprElement = true
     *
     * @param testElem
     * @param test
     * @param supprElement
     * @throws Exception
     */
    public void updateReqLink(Element testElem, Test test, boolean supprElement)
        throws Exception {
        Iterator itReqLinkXML = testElem.selectNodes(
                                                     "LinkRequirement/RequirementRef").iterator();
        try {
            Vector listReqWrapper = Requirement.getReqWrapperCoveredByTest(test
                                                                           .getIdBdd());
            int size = listReqWrapper.size();
            while (itReqLinkXML.hasNext()) {
                Element linkElem = (Element) itReqLinkXML.next();
                String nomReq = linkElem.elementText("Nom");
                String id = linkElem.attributeValue("ref");
                id = id.substring(4, id.length());
                Requirement pTmpReq = (Requirement) docReqById.get(new Integer(
                                                                               id));
                /*
                 * Util.log ("[updateReqLink] id = " + id); Util.log
                 * ("[updateReqLink] pTmpReq = " + pTmpReq); Util.log
                 * ("[updateReqLink] nomReq = " + nomReq); Util.log
                 * ("[updateReqLink] test = " + test.getNameFromModel() +
                 * ", suite : " + test.getTestListFromModel().getNameFromModel()
                 * + ", famille : " +
                 * test.getTestListFromModel().getFamilyFromModel
                 * ().getNameFromModel());
                 */
                boolean doLink = true;
                if (pTmpReq == null) {
                    doLink = false;
                    Util
                        .log("[ImportXMLReq->updateReqLink] requirement linked not found in XML");
                }
                Requirement pReq = null;
                if (doLink) {
                    pReq = (Requirement) projectReqByName.get(pTmpReq
                                                              .getLongName());
                }
                if (doLink && pReq == null) {
                    Tools.ihmExceptionView(new Exception(
                                                         "[updateReqLink] Requirment " + nomReq
                                                         + " not found"));
                    doLink = false;
                }
                if (doLink && pReq instanceof ReqFamily) {
                    Tools
                        .ihmExceptionView(new Exception(
                                                        "[updateReqLink] Requirment "
                                                        + nomReq
                                                        + " is a family and can't be linked to the test "
                                                        + test.getNameFromModel()));
                    doLink = false;
                }
                if (doLink) {
                    boolean couvre = false;
                    int i = 0;
                    while (i < size && !couvre) {
                        ReqWrapper pReqWrapper = (ReqWrapper) listReqWrapper
                            .elementAt(i);
                        Requirement pTmpReq2 = (Requirement) projectReqById
                            .get(new Integer(pReqWrapper.getIdBDD()));
                        if (pTmpReq2.getLongName()
                            .equals(pTmpReq.getLongName())) {
                            couvre = true;
                        }
                        i++;
                    }

                    if (!couvre) {
                        try {
                            pReq.addTestCoverInDB(test.getIdBdd(), true);
                        } catch (Exception e1) {
                            Tools.ihmExceptionView(e1);
                        }
                    }
                }
            }
            if (supprElement) {
                removeReqLink(testElem, test);
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    /**
     * Ajout les Requirements du XML dans le projet (Ne fait pas de test pour
     * savoir si requirement est deja present)
     *
     * @param reqElem
     * @param ReqFamily
     *            la famille d'exigence ou ajouter le requirement Si null,
     *            recherche de la famille
     * @throws Exception
     */
    private void ajoutRequirement(Element reqElem, ReqFamily pReqParent)
        throws Exception {
        String nomReq = reqElem.elementText("Nom");
        int priority = -1;
        String version = null;
        int cat = -1;
        int comp = -1;
        int state = -1;
        // 20100118 - D\ufffdbut Modification Forge ORTF
        int validBy = -1;
        int critical = -1;
        int understanding = -1;
        int owner = -1;
        // 20100118 - Fin modification Forge ORTF
        String origine = null;
        String verifway = null;
        String reference = null;
        try {
            version = reqElem.attributeValue("version");
            // 20100119 - D\ufffdbut modification Forge ORTF
            // Priority
            String priorite = reqElem.attributeValue("priority");
            if (priorite != null) {
                String sql = "SELECT id FROM PRIORITY WHERE name='" + priorite
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    priority = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertPriorityInDB");
                    prep.setString(1, priorite);
                    prep.executeUpdate();
                    sql = "SELECT id FROM PRIORITY WHERE name='" + priorite
                        + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        priority = result.getInt(1);
                        pRequirementDescriptionPanel.priorityBox
                            .addItem(priorite);
                    }
                }
                result.close();
            } else {
                String priorityDefault = reqProp
                    .getProperty("Priorite_default");
                String sql = "SELECT id FROM PRIORITY WHERE name='"
                    + priorityDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    priority = result.getInt(1);
                }
            }
            // Category
            String category = reqElem.attributeValue("category");
            if (category != null) {
                String sql = "SELECT id FROM CATEGORY WHERE name='" + category
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    cat = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertCategoryInDB");
                    prep.setString(1, category);
                    prep.executeUpdate();
                    sql = "SELECT id FROM CATEGORY WHERE name='" + category
                        + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        cat = result.getInt(1);
                        pRequirementDescriptionPanel.catBox.addItem(category);
                        pRequirementDescriptionPanel.pInfoFiltrePanel.catBox
                            .addItem(category);
                    }
                }
                result.close();
            } else {
                String categoryDefault = reqProp
                    .getProperty("Categorie_default");
                String sql = "SELECT id FROM CATEGORY WHERE name='"
                    + categoryDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    cat = result.getInt(1);
                }
            }
            // Complexity
            String complexity = reqElem.attributeValue("complexity");
            if (complexity != null) {
                String sql = "SELECT id FROM COMPLEXE WHERE name='"
                    + complexity + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    comp = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertComplexeInDB");
                    prep.setString(1, complexity);
                    prep.executeUpdate();
                    sql = "SELECT id FROM COMPLEXE WHERE name='" + complexity
                        + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        comp = result.getInt(1);
                        pRequirementDescriptionPanel.complexeBox
                            .addItem(complexity);
                        pRequirementDescriptionPanel.pInfoFiltrePanel.complexeBox
                            .addItem(complexity);
                    }
                }
                result.close();
            } else {
                String complexeDefault = reqProp
                    .getProperty("Complexite_default");
                String sql = "SELECT id FROM COMPLEXE WHERE name='"
                    + complexeDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    comp = result.getInt(1);
                }
            }
            // State
            String etat = reqElem.attributeValue("state");
            if (etat != null) {
                String sql = "SELECT id FROM STATE WHERE name='" + etat + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    state = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertStateInDB");
                    prep.setString(1, etat);
                    prep.executeUpdate();
                    sql = "SELECT id FROM STATE WHERE name='" + etat + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        state = result.getInt(1);
                        pRequirementDescriptionPanel.stateBox.addItem(etat);
                        pRequirementDescriptionPanel.pInfoFiltrePanel.stateBox
                            .addItem(etat);
                    }
                }
                result.close();
            } else {
                String stateDefault = reqProp.getProperty("Statut_default");
                String sql = "SELECT id FROM STATE WHERE name='" + stateDefault
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    state = result.getInt(1);
                }
            }
            // CheckedBy
            String checkedBy = reqElem.attributeValue("checkedBy");
            if (checkedBy != null) {
                String sql = "SELECT id FROM VALID_BY WHERE name='" + checkedBy
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    validBy = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertValidByInDB");
                    prep.setString(1, checkedBy);
                    prep.executeUpdate();
                    sql = "SELECT id FROM VALID_BY WHERE name='" + checkedBy
                        + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBy = result.getInt(1);
                        pRequirementDescriptionPanel.validByBox
                            .addItem(checkedBy);
                    }
                }
                result.close();
            } else {
                String validByDefault = reqProp.getProperty("Valide_default");
                String sql = "SELECT id FROM VALID_BY WHERE name='"
                    + validByDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    validBy = result.getInt(1);
                }
            }
            // Criticity
            String criticity = reqElem.attributeValue("criticity");
            if (criticity != null) {
                String sql = "SELECT id FROM CRITICAL WHERE name='" + criticity
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    critical = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertCriticalInDB");
                    prep.setString(1, criticity);
                    prep.executeUpdate();
                    sql = "SELECT id FROM CRITICAL WHERE name='" + criticity
                        + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        critical = result.getInt(1);
                        pRequirementDescriptionPanel.criticalBox
                            .addItem(criticity);
                    }
                }
                result.close();
            } else {
                String criticalDefault = reqProp
                    .getProperty("Criticite_default");
                String sql = "SELECT id FROM CRITICAL WHERE name='"
                    + criticalDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    critical = result.getInt(1);
                }
            }
            // Understanding
            String comprehension = reqElem.attributeValue("understanding");
            if (comprehension != null) {
                String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                    + comprehension + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    understanding = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertUnderstandingInDB");
                    prep.setString(1, comprehension);
                    prep.executeUpdate();
                    sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                        + comprehension + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        understanding = result.getInt(1);
                        pRequirementDescriptionPanel.understandingBox
                            .addItem(comprehension);
                    }
                }
                result.close();
            } else {
                String understandingDefault = reqProp
                    .getProperty("Comprehension_default");
                String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                    + understandingDefault + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    understanding = result.getInt(1);
                }
            }
            // Owner
            String proprio = reqElem.attributeValue("owner");
            if (proprio != null) {
                String sql = "SELECT id FROM OWNER WHERE name='" + proprio
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    owner = result.getInt(1);
                } else {
                    prep = SQLWrapper.getSQLAddQuery("insertOwnerInDB");
                    prep.setString(1, proprio);
                    prep.executeUpdate();
                    sql = "SELECT id FROM OWNER WHERE name='" + proprio + "'";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        owner = result.getInt(1);
                        pRequirementDescriptionPanel.ownerBox.addItem(proprio);
                    }
                }
                result.close();
            } else {
                String ownerDefault = reqProp
                    .getProperty("Proprietaire_default");
                String sql = "SELECT id FROM OWNER WHERE name='" + ownerDefault
                    + "'";
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    owner = result.getInt(1);
                }
            }
            // 20100119 - Fin modification Forge ORTF
            origine = reqElem.attributeValue("origine");
            verifway = reqElem.attributeValue("verifway");
            reference = reqElem.attributeValue("reference");
        } catch (Exception e) {
            // XML from old plugin
        }

        String desc = (reqElem.elementText("Description") == null) ? ""
            : reqElem.elementText("Description");
        if (reqElem.element("Description") != null
            && reqElem.element("Description").attribute("isHTML") != null
            && reqElem.element("Description").attributeValue("isHTML")
            .equals("true")) {
            desc = giveHTMLDescription(reqElem);
        } else {
            desc = desc.replaceAll("\\\\n", "<br>");
            desc = "<html><head></head><body>" + desc + "</body></html>";
        }
        String strId_reqParent = reqElem.attributeValue("id_req_parent");
        strId_reqParent = strId_reqParent
            .substring(4, strId_reqParent.length()); // remove Req_
        Integer id_reqParent = null;
        try {
            id_reqParent = new Integer(strId_reqParent);
        } catch (java.lang.NumberFormatException e) {
            e.printStackTrace();
            return;
        }

        // ReqFamily pReqParent;
        if (pReqParent == null) {
            if (id_reqParent.equals(new Integer(0))) {
                pReqParent = (ReqFamily) reqRoot;
            } else {
                pReqParent = (ReqFamily) docReqById.get(id_reqParent);
                Requirement pTempReq = (Requirement) projectReqByName
                    .get(pReqParent.getLongName());

                if (pTempReq != null && pTempReq instanceof ReqFamily) {
                    pReqParent = (ReqFamily) pTempReq;
                } else {
                    pReqParent = (ReqFamily) reqRoot;
                }
            }
        }

        ReqLeaf pNewReq = new ReqLeaf(nomReq, desc, pReqParent, 0);
        try {
            pNewReq.addInDB();
            if (priority != -1) {
                pNewReq.updatePriorityInDBAndModel(priority);
            }
            if (cat != -1) {
                pNewReq.updateCatInDBAndModel(cat);
            }
            if (comp != -1) {
                pNewReq.updateComplexeInDBAndModel(comp);
            }
            if (state != -1) {
                pNewReq.updateStateInDBAndModel(state);
            }
            // 20100118 - D\ufffdbut modification Forge ORTF
            if (validBy != -1) {
                pNewReq.updateValidByInDBAndModel(validBy);
            }
            if (critical != -1) {
                pNewReq.updateCriticalInDBAndModel(critical);
            }
            if (understanding != -1) {
                pNewReq.updateUnderstandingInDBAndModel(understanding);
            }
            if (owner != -1) {
                pNewReq.updateOwnerInDBAndModel(owner);
            }
            // 20100118 - Fin modification ORTF
            if (version != null && origine != null && verifway != null
                && reference != null) {
                pNewReq.updateInfoInDB(version, origine, verifway, reference);
            } else {
                if (version != null) {
                    pNewReq.updateVersionInDBAndModel(version);
                }
                if (origine != null) {
                    pNewReq.updateOrigineInDBAndModel(origine);
                }
                if (verifway != null) {
                    pNewReq.updateVerifInDBAndModel(verifway);
                }
                if (reference != null) {
                    pNewReq.updateReferenceInDBAndModel(reference);
                }
            }
            pReqParent.addRequirement(pNewReq);
            projectReqByName.put(pNewReq.getLongName(), pNewReq);
            projectReqById.put(new Integer(pNewReq.getIdBdd()), pNewReq);
            pImport.updateElementAttachement(reqElem, pNewReq, true);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    }

    /**
     * Ajoute l'exigence famille au repository (pas de test si deja presente)
     *
     * @param reqElem
     * @return
     * @throws Exception
     */
    private ReqFamily ajoutReqFamily(Element reqElem) throws Exception {
        if (reqElem == null) {
            return (ReqFamily) reqRoot;
        }
        String nomReq = reqElem.elementText("Nom");
        String id = reqElem.attributeValue("id_req");
        id = id.substring(4, id.length());
        Requirement pTmpReq = (Requirement) docReqById.get(new Integer(id));
        pTmpReq = (Requirement) projectReqByName.get(pTmpReq.getLongName());
        if (pTmpReq != null && pTmpReq instanceof ReqFamily) {
            return (ReqFamily) pTmpReq;
        }

        String strId_reqParent = reqElem.attributeValue("id_req_parent");
        strId_reqParent = strId_reqParent
            .substring(4, strId_reqParent.length()); // remove Req_
        Integer id_reqParent = new Integer(strId_reqParent);
        Element reqParentElement = (Element) docElementById.get(id_reqParent);
        ReqFamily pParent = ajoutReqFamily(reqParentElement);

        String desc = (reqElem.elementText("Description") == null) ? ""
            : reqElem.elementText("Description");
        if (reqElem.element("Description") != null
            && reqElem.element("Description").attribute("isHTML") != null
            && reqElem.element("Description").attributeValue("isHTML")
            .equals("true")) {
            desc = giveHTMLDescription(reqElem);
        } else {
            desc = desc.replaceAll("\\\\n", "<br>");
            desc = "<html><head></head><body>" + desc + "</body></html>";
        }
        ReqFamily pNewReqFamily = new ReqFamily(nomReq, desc, pParent);

        try {
            pNewReqFamily.addInDB();
            pParent.addRequirement(pNewReqFamily);
            projectReqByName.put(pNewReqFamily.getLongName(), pNewReqFamily);
            projectReqById.put(new Integer(pNewReqFamily.getIdBdd()),
                               pNewReqFamily);
            pImport.updateElementAttachement(reqElem, pNewReqFamily, true);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
        return pNewReqFamily;
    }

    private String giveHTMLDescription(Element elem) {
        Element descElem = elem.element("Description");
        String description = "";
        if (descElem.hasContent()) {
            description = elem.element("Description").asXML();
            description = description.substring(27, description
                                                .indexOf("</Description>"));
            description = description.replaceAll("<br/>", "<br>");
            description = "<html><head></head><body>" + description
                + "</body></html>";
        }
        return description;
    }

    /**
     * Mise a jour des Requirements existants et ajout des nouveaux (Si gestion
     * des suppression deja effectue, ne fait rien)
     *
     * @param doc
     * @throws Exception
     */
    public void updateProjectRequirement(Document doc) throws Exception {
        Vector reqElementToAdd = new Vector();
        Vector reqFamilyToAdd = new Vector();
        Iterator itReqLeqfXML = doc.selectNodes("//Requirement").iterator();
        while (itReqLeqfXML.hasNext()) {
            Element reqElem = (Element) itReqLeqfXML.next();
            // String nomReq = reqElem.elementText("Nom");

            int priority = -1;
            String version = null;
            int cat = -1;
            int comp = -1;
            int state = -1;
            // 20100118 - D\ufffdbut Modification Forge ORTF
            int validBy = -1;
            int critical = -1;
            int understanding = -1;
            int owner = -1;
            // 20100118 - Fin modification Forge ORTF
            String origine = null;
            String verifway = null;
            String reference = null;
            try {
                version = reqElem.attributeValue("version");
                // 20100119 - D\ufffdbut modification Forge ORTF
                // Priority
                String priorite = reqElem.attributeValue("priority");
                if (priorite != null) {
                    String sql = "SELECT id FROM PRIORITY WHERE name='"
                        + priorite + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        priority = result.getInt(1);
                    } else {
                        sql = "INSERT INTO PRIORITY (name,user_entry) values ('"
                            + priorite + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM PRIORITY WHERE name='" + priorite
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            priority = result.getInt(1);
                            pRequirementDescriptionPanel.priorityBox
                                .addItem(priorite);
                        }
                    }
                    result.close();
                } else {
                    String priorityDefault = reqProp
                        .getProperty("Priorite_default");
                    String sql = "SELECT id FROM PRIORITY WHERE name='"
                        + priorityDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        priority = result.getInt(1);
                    }
                }
                // Category
                String category = reqElem.attributeValue("category");
                if (category != null) {
                    String sql = "SELECT id FROM CATEGORY WHERE name='"
                        + category + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        cat = result.getInt(1);
                    } else {
                        sql = "INSERT INTO CATEGORY (name,user_entry) values ('"
                            + category + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM CATEGORY WHERE name='" + category
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            cat = result.getInt(1);
                            pRequirementDescriptionPanel.catBox
                                .addItem(category);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.catBox
                                .addItem(category);
                        }
                    }
                    result.close();
                } else {
                    String categoryDefault = reqProp
                        .getProperty("Categorie_default");
                    String sql = "SELECT id FROM CATEGORY WHERE name='"
                        + categoryDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        cat = result.getInt(1);
                    }
                }
                // Complexity
                String complexity = reqElem.attributeValue("complexity");
                if (complexity != null) {
                    String sql = "SELECT id FROM COMPLEXE WHERE name='"
                        + complexity + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        comp = result.getInt(1);
                    } else {
                        sql = "INSERT INTO COMPLEXE (name,user_entry) values ('"
                            + complexity + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM COMPLEXE WHERE name='"
                            + complexity + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            comp = result.getInt(1);
                            pRequirementDescriptionPanel.complexeBox
                                .addItem(complexity);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.complexeBox
                                .addItem(complexity);
                        }
                    }
                    result.close();
                } else {
                    String complexeDefault = reqProp
                        .getProperty("Complexite_default");
                    String sql = "SELECT id FROM COMPLEXE WHERE name='"
                        + complexeDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        comp = result.getInt(1);
                    }
                }
                // State
                String etat = reqElem.attributeValue("state");
                if (etat != null) {
                    String sql = "SELECT id FROM STATE WHERE name='" + etat
                        + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        state = result.getInt(1);
                    } else {
                        sql = "INSERT INTO STATE (name,user_entry) values ('"
                            + etat + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM STATE WHERE name='" + etat + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            state = result.getInt(1);
                            pRequirementDescriptionPanel.stateBox.addItem(etat);
                            pRequirementDescriptionPanel.pInfoFiltrePanel.stateBox
                                .addItem(etat);
                        }
                    }
                    result.close();
                } else {
                    String stateDefault = reqProp.getProperty("Statut_default");
                    String sql = "SELECT id FROM STATE WHERE name='"
                        + stateDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        state = result.getInt(1);
                    }
                }
                // CheckedBy
                String checkedBy = reqElem.attributeValue("checkedBy");
                if (checkedBy != null) {
                    String sql = "SELECT id FROM VALID_BY WHERE name='"
                        + checkedBy + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBy = result.getInt(1);
                    } else {
                        sql = "INSERT INTO VALID_BY (name,user_entry) values ('"
                            + checkedBy + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM VALID_BY WHERE name='"
                            + checkedBy + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            validBy = result.getInt(1);
                            pRequirementDescriptionPanel.validByBox
                                .addItem(checkedBy);
                        }
                    }
                    result.close();
                } else {
                    String validByDefault = reqProp
                        .getProperty("Valide_default");
                    String sql = "SELECT id FROM VALID_BY WHERE name='"
                        + validByDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBy = result.getInt(1);
                    }
                }
                // Criticity
                String criticity = reqElem.attributeValue("criticity");
                if (criticity != null) {
                    String sql = "SELECT id FROM CRITICAL WHERE name='"
                        + criticity + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        critical = result.getInt(1);
                    } else {
                        sql = "INSERT INTO CRITICAL (name,user_entry) values ('"
                            + criticity + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM CRITICAL WHERE name='"
                            + criticity + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            critical = result.getInt(1);
                            pRequirementDescriptionPanel.criticalBox
                                .addItem(criticity);
                        }
                    }
                    result.close();
                } else {
                    String criticalDefault = reqProp
                        .getProperty("Criticite_default");
                    String sql = "SELECT id FROM CRITICAL WHERE name='"
                        + criticalDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        critical = result.getInt(1);
                    }
                }
                // Understanding
                String comprehension = reqElem.attributeValue("understanding");
                if (comprehension != null) {
                    String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                        + comprehension + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        understanding = result.getInt(1);
                    } else {
                        sql = "INSERT INTO UNDERSTANDING (name,user_entry) values ('"
                            + comprehension + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                            + comprehension + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            understanding = result.getInt(1);
                            pRequirementDescriptionPanel.understandingBox
                                .addItem(comprehension);
                        }
                    }
                    result.close();
                } else {
                    String understandingDefault = reqProp
                        .getProperty("Comprehension_default");
                    String sql = "SELECT id FROM UNDERSTANDING WHERE name='"
                        + understandingDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        understanding = result.getInt(1);
                    }
                }
                // Owner
                String proprio = reqElem.attributeValue("owner");
                if (proprio != null) {
                    String sql = "SELECT id FROM OWNER WHERE name='" + proprio
                        + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        owner = result.getInt(1);
                    } else {
                        sql = "INSERT INTO OWNER (name,user_entry) values ('"
                            + proprio + "',1);";
                        prep = db.prepareStatement(sql);
                        prep.executeUpdate();
                        sql = "SELECT id FROM OWNER WHERE name='" + proprio
                            + "'";
                        prep = db.prepareStatement(sql);
                        result = prep.executeQuery(sql);
                        if (result.next()) {
                            owner = result.getInt(1);
                            pRequirementDescriptionPanel.ownerBox
                                .addItem(proprio);
                        }
                    }
                    result.close();
                } else {
                    String ownerDefault = reqProp
                        .getProperty("Proprietaire_default");
                    String sql = "SELECT id FROM OWNER WHERE name='"
                        + ownerDefault + "'";
                    PreparedStatement prep = db.prepareStatement(sql);
                    ResultSet result = prep.executeQuery(sql);
                    if (result.next()) {
                        owner = result.getInt(1);
                    }
                }
                // 20100119 - Fin modification Forge ORTF
                origine = reqElem.attributeValue("origine");
                verifway = reqElem.attributeValue("verifway");
                reference = reqElem.attributeValue("reference");
            } catch (Exception e) {
                // XML from old plugin
            }
            String desc = (reqElem.elementText("Description") == null) ? ""
                : reqElem.elementText("Description");
            if (reqElem.element("Description") != null
                && reqElem.element("Description").attribute("isHTML") != null
                && reqElem.element("Description").attributeValue("isHTML")
                .equals("true")) {
                desc = giveHTMLDescription(reqElem);
            } else {
                desc = desc.replaceAll("\\\\n", "<br>");
                desc = "<html><head></head><body>" + desc + "</body></html>";
            }
            String id_reqDoc = reqElem.attributeValue("id_req");
            id_reqDoc = id_reqDoc.substring(4, id_reqDoc.length()); // remove
            // Req_

            Requirement docXMLReq = (Requirement) docReqById.get(new Integer(
                                                                             id_reqDoc));
            Requirement pReq = null;
            if (docXMLReq != null) {
                pReq = (Requirement) projectReqByName.get(docXMLReq
                                                          .getLongName());
            }

            if (pReq != null) {
                // Existe --> Update
                if (reqElem != null) {
                    pImport.updateElementAttachement(reqElem, pReq, false);
                    if (desc != null) {
                        pReq.updateDescriptionInDBAndModel(desc);
                    }
                    if (pReq instanceof ReqLeaf) {
                        if (version != null && origine != null
                            && verifway != null && reference != null) {
                            ((ReqLeaf) pReq).updateInfoInDB(version, origine,
                                                            verifway, reference);
                        } else {
                            if (version != null) {
                                ((ReqLeaf) pReq)
                                    .updateVersionInDBAndModel(version);
                            }
                            if (origine != null) {
                                ((ReqLeaf) pReq)
                                    .updateOrigineInDBAndModel(origine);
                            }
                            if (verifway != null) {
                                ((ReqLeaf) pReq)
                                    .updateVerifInDBAndModel(verifway);
                            }
                            if (reference != null) {
                                ((ReqLeaf) pReq)
                                    .updateReferenceInDBAndModel(reference);
                            }
                        }
                        if (priority != -1) {
                            ((ReqLeaf) pReq)
                                .updatePriorityInDBAndModel(priority);
                        }
                        if (cat != -1) {
                            ((ReqLeaf) pReq).updateCatInDBAndModel(cat);
                        }
                        if (comp != -1) {
                            ((ReqLeaf) pReq).updateComplexeInDBAndModel(comp);
                        }
                        if (state != -1) {
                            ((ReqLeaf) pReq).updateStateInDBAndModel(state);
                        }
                        // 20100118 - D\ufffdbut modification Forge ORTF
                        if (validBy != -1) {
                            ((ReqLeaf) pReq).updateValidByInDBAndModel(validBy);
                        }
                        if (critical != -1) {
                            ((ReqLeaf) pReq)
                                .updateCriticalInDBAndModel(critical);
                        }
                        if (understanding != -1) {
                            ((ReqLeaf) pReq)
                                .updateUnderstandingInDBAndModel(understanding);
                        }
                        if (owner != -1) {
                            ((ReqLeaf) pReq).updateOwnerInDBAndModel(owner);
                        }
                        // 20100118 - Fin modification Forge ORTF
                    }
                }
            } else {
                // Not Existe --> Ajout
                reqElementToAdd.add(reqElem);

            }
        }

        Iterator itReqFamilyfXML = doc.selectNodes("//RequirementFamily")
            .iterator();
        while (itReqFamilyfXML.hasNext()) {
            Element reqElem = (Element) itReqFamilyfXML.next();
            // String nomReq = reqElem.elementText("Nom");
            String desc = (reqElem.elementText("Description") == null) ? ""
                : reqElem.elementText("Description");
            if (reqElem.element("Description") != null
                && reqElem.element("Description").attribute("isHTML") != null
                && reqElem.element("Description").attributeValue("isHTML")
                .equals("true")) {
                desc = giveHTMLDescription(reqElem);
            } else {
                desc = desc.replaceAll("\\\\n", "<br>");
                desc = "<html><head></head><body>" + desc + "</body></html>";
            }
            String id_reqDoc = reqElem.attributeValue("id_req");
            id_reqDoc = id_reqDoc.substring(4, id_reqDoc.length()); // remove
            // Req_
            Requirement docXMLReq = null;
            try {
                docElementById.put(new Integer(id_reqDoc), reqElem);
                docXMLReq = (Requirement) docReqById
                    .get(new Integer(id_reqDoc));
            } catch (java.lang.NumberFormatException e) {
                e.printStackTrace();
                continue;
            }

            Requirement pReq = null;
            if (docXMLReq != null) {
                pReq = (Requirement) projectReqByName.get(docXMLReq
                                                          .getLongName());
            }

            if (pReq != null) {
                // Existe --> Update
                if (reqElem != null) {
                    pImport.updateElementAttachement(reqElem, pReq, false);
                    if (desc != null) {
                        pReq.updateDescriptionInDBAndModel(desc);
                    }
                }
            } else {
                // Not Existe --> Ajout
                reqFamilyToAdd.add(reqElem);
                // updateElementAttachement(reqElem, pReq, true);
            }
        }
        // Ajput des nouveau requirements
        for (int i = 0; i < reqFamilyToAdd.size(); i++) {
            Element reqElem = (Element) reqFamilyToAdd.elementAt(i);
            ajoutReqFamily(reqElem);
        }
        for (int i = 0; i < reqElementToAdd.size(); i++) {
            Element reqElem = (Element) reqElementToAdd.elementAt(i);
            ajoutRequirement(reqElem, null);
        }
    }

}
