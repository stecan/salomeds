package salomeTMF_plug.requirements.ihm;

import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.requirements.data.FiltreUserData;
import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

public class CoverFiltrePanel extends JPanel implements ActionListener {
    JRadioButton bCover;
    JRadioButton bNotCover;
    JRadioButton bUserData;
    JButton buttonUserFilter;

    IReqFilter m_ReqFilter;
    FilterReqTreeModel m_FilterReqTreeModel;
    Hashtable reqsWrapCoveredHash;
    FiltreUserData pFiltreUserData;

    int filtre = 11;
    Observer pObserver = null;

    public CoverFiltrePanel(FilterReqTreeModel pFilterReqTreeModel,
                            Observer _pObserver) {
        super();
        m_FilterReqTreeModel = pFilterReqTreeModel;
        if (m_FilterReqTreeModel != null) {
            m_ReqFilter = pFilterReqTreeModel.getFilter();
        }
        reqsWrapCoveredHash = new Hashtable();
        pFiltreUserData = new FiltreUserData();
        pObserver = _pObserver;
        initComponent();
    }

    void initComponent() {
        setLayout(new GridLayout(1, 4));

        bCover = new JRadioButton(Language.getInstance().getText(
                                                                 "Filtre_Exigence_Couverte"), true);
        bNotCover = new JRadioButton(Language.getInstance().getText(
                                                                    "Filtre_Exigence_Non_Couverte"), true);
        bUserData = new JRadioButton("Informations", false);
        buttonUserFilter = new JButton(Language.getInstance().getText("Filtre"));

        bCover.addActionListener(this);
        bNotCover.addActionListener(this);
        bUserData.addActionListener(this);
        buttonUserFilter.addActionListener(this);

        add(bCover);
        add(bNotCover);
        add(bUserData);
    }

    void reInit(boolean filter) {
        bCover.setSelected(true);
        bNotCover.setSelected(true);
        bUserData.setSelected(false);
        if (filter) {
            m_ReqFilter.reInit();
        }
    }

    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource().equals(bCover)) {
            bCoverPerformed();
        } else if (evt.getSource().equals(bNotCover)) {
            bNotCoverPerformed();
        } else if (evt.getSource().equals(buttonUserFilter)) {
            buttonUserFilterPerformed();
        } else if (evt.getSource().equals(bUserData)) {
            bUserDataPerformed();
        }
    }

    void updateReqCover() {
        try {
            if (Requirement.coverChange) {
                reqsWrapCoveredHash.clear();
                Vector reqsWrapCovered = Requirement.getReqWrapperCovered();
                int size = reqsWrapCovered.size();
                for (int i = 0; i < size; i++) {
                    ReqWrapper pReq = (ReqWrapper) reqsWrapCovered.elementAt(i);
                    reqsWrapCoveredHash.put(new Integer(pReq.getIdBDD()), pReq);
                }
            }
            Requirement.coverChange = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    int getFiltre() {
        int f = 0;
        if (bCover.isSelected()) {
            f += 10;
        }
        if (bNotCover.isSelected()) {
            f += 1;
        }
        return f;
    }

    void bCoverPerformed() {
        filtre = getFiltre();
        if (filtre != 11) {
            updateReqCover();
        }
        applyFilter();
    }

    void bNotCoverPerformed() {
        filtre = getFiltre();
        if (filtre != 11) {
            updateReqCover();
        }
        applyFilter();
    }

    void bUserDataPerformed() {
        if (pFiltreUserData.isActive()) {
            System.out.println("Update Data Filter");
            applyFilter();
        }
    }

    void buttonUserFilterPerformed() {
        UserDataFiltrePanel pUserDataFiltrePanel = new UserDataFiltrePanel(
                                                                           pFiltreUserData);
        pUserDataFiltrePanel.setVisible(true);
        if (pUserDataFiltrePanel.isDoValidate()) {
            if (bUserData.isSelected()) {
                applyFilter();
            }
        }
    }

    void applyFilter() {
        if (m_ReqFilter != null) {
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(
                                                                      new Cursor(Cursor.WAIT_CURSOR));
            m_ReqFilter.setCoverFiltre(filtre, reqsWrapCoveredHash);
            m_ReqFilter.setUserDataFiltre(bUserData.isSelected(),
                                          pFiltreUserData);
            m_FilterReqTreeModel.setFiltered(true);
            if (pObserver != null) {
                pObserver.update(null, new ReqEvent(ReqEvent.FILTRE_REQ_CHANGE,
                                                    null));
            }
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(
                                                                      new Cursor(Cursor.DEFAULT_CURSOR));
        }

    }

}
