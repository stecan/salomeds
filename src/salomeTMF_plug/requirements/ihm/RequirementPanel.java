/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.plugin.Synchronize;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000120 - \ufffd2.7.8
 * Jira : FORTF - 39
 *
 */
public class RequirementPanel extends JPanel implements ActionListener , Observer{
    JButton addReqButton;
    JButton delReqButton;
    JButton addReqFamilyButton;
    JButton findReqButton;
    JButton renameReqButton;
    JButton reloadReqButton;
    JButton creatTestReqButton;
    JButton importerReqButton;

    JPanel buttonPanel;
    JPanel treePanel;

    RequirementTree reqTree;
    RequirementDescriptionPanel pRequirementDescriptionPanel;

    public RequirementDescriptionPanel getpRequirementDescriptionPanel() {
        return pRequirementDescriptionPanel;
    }


    //JApplet ptrSalome;
    FiltrePanel pFiltrePanel;
    CoverFiltrePanel pCoverFiltrePanel;
    InfoFiltrePanel pInfoFiltrePanel;

    boolean loadData = false;
    Requirement pReq = null;
    ReqPlugin pReqPlugin;
    StatRequirement pStatRequirement;
    public RequirementPanel(StatRequirement _pStatRequirement, ReqPlugin pReqPlugin){
        super(new BorderLayout());
        this.pReqPlugin = pReqPlugin;
        //this.ptrSalome = ptrSalome;
        initComponent(_pStatRequirement);
    }


    void initComponent(StatRequirement _pStatRequirement){
        int t_x = 1024;
        int     t_y = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width;
            t_y = r.height;

        } catch(Exception E){

        }
        pStatRequirement = _pStatRequirement;
        addReqButton =  new JButton(Language.getInstance().getText("Ajouter") + " " +Language.getInstance().getText("Exigence"));
        addReqButton.addActionListener(this);
        //              addReqButton.setEnabled(Permission.canCreateReq());

        delReqButton =  new JButton(Language.getInstance().getText("Supprimer") + " " +Language.getInstance().getText("Exigence"));
        delReqButton.addActionListener(this);
        delReqButton.setEnabled(false);

        addReqFamilyButton =  new JButton(Language.getInstance().getText("Ajouter") + " " +Language.getInstance().getText("Branche"));
        addReqFamilyButton.addActionListener(this);
        //              addReqFamilyButton.setEnabled(Permission.canCreateReq());

        findReqButton =  new JButton(Language.getInstance().getText("Chercher"));
        findReqButton.addActionListener(this);

        renameReqButton=  new JButton(Language.getInstance().getText("Renommer"));
        renameReqButton.addActionListener(this);
        renameReqButton.setEnabled(false);

        reloadReqButton=  new JButton(Language.getInstance().getText("Rafraichir"));
        reloadReqButton.addActionListener(this);
        reloadReqButton.setEnabled(true);

        creatTestReqButton = new JButton(Language.getInstance().getText("Generer_les_tests"));
        creatTestReqButton.addActionListener(this);
        //              creatTestReqButton.setEnabled(Permission.canCreateTest());

        importerReqButton= new JButton(Language.getInstance().getText("Importer") + " " +Language.getInstance().getText("Exigence"));
        importerReqButton.addActionListener(this);

        //              JButton synchronize = new JButton("Synchronize");
        //              Synchronize synchro = new Synchronize();
        //              synchronize.addActionListener(l);
        //              importerReqButton.setEnabled(Permission.canCreateReq());


        buttonPanel = new JPanel(new GridLayout(3,3));
        buttonPanel.add(addReqFamilyButton);
        buttonPanel.add(addReqButton);
        buttonPanel.add(delReqButton);
        buttonPanel.add(findReqButton);
        buttonPanel.add(renameReqButton);
        buttonPanel.add(creatTestReqButton);
        buttonPanel.add(reloadReqButton);
        buttonPanel.add(importerReqButton);
        buttonPanel.setBorder(BorderFactory.createRaisedBevelBorder());


        reqTree = new RequirementTree();
        JScrollPane scrollPane = new JScrollPane(reqTree,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        treePanel = new JPanel(new GridLayout(1,0));
        treePanel.add(scrollPane);

        JPanel lesFiltresPanel = new JPanel(new GridLayout(2, 1));
        lesFiltresPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Filtre")));

        JPanel filtresPanel1 = new JPanel(new GridLayout(2, 1));

        pFiltrePanel = new FiltrePanel((FilterReqTreeModel)reqTree.getModel(), this);
        pCoverFiltrePanel = new CoverFiltrePanel((FilterReqTreeModel)reqTree.getModel(), this);
        //              filtresPanel1.add(pFiltrePanel);
        filtresPanel1.add(pCoverFiltrePanel);
        lesFiltresPanel.add(filtresPanel1);
        pInfoFiltrePanel =  new InfoFiltrePanel((FilterReqTreeModel)reqTree.getModel(), this);
        lesFiltresPanel.add(pInfoFiltrePanel);
        reqTree.setStatTools(pStatRequirement, pFiltrePanel);


        JPanel buttonsAndTree = new JPanel(new BorderLayout());
        buttonsAndTree.add(buttonPanel, BorderLayout.NORTH);
        buttonsAndTree.add(treePanel, BorderLayout.CENTER);
        buttonsAndTree.add(lesFiltresPanel, BorderLayout.SOUTH);

        //buttonsAndTree.setPreferredSize(new Dimension(380,380));
        buttonsAndTree.setMinimumSize(new Dimension(t_x/4,t_y));
        buttonsAndTree.setMaximumSize(new Dimension(t_x/2,t_y));
        //20100118 - D\ufffdbut modification Forge ORTF
        pRequirementDescriptionPanel = new RequirementDescriptionPanel(pStatRequirement, pFiltrePanel, reqTree,pInfoFiltrePanel);
        //20100118 - Fin modification Forge ORTF
        pRequirementDescriptionPanel.setPreferredSize(new Dimension(t_x/4*2,t_y));
        reqTree.addObserver(this);


        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                                              buttonsAndTree, pRequirementDescriptionPanel);
        splitPane.setDividerLocation(t_x/3);
        add(splitPane);

        //add(pRequirementDescriptionPanel, BorderLayout.CENTER);
        //add(buttonsAndTree,BorderLayout.WEST);
        pReq = (Requirement)reqTree.parent.getUserObject();
        pRequirementDescriptionPanel.setRequirement(pReq);
        //pRequirementDescriptionPanel.setVisible(false);
        pRequirementDescriptionPanel.initCalcStatAllCover(pFiltrePanel.getFiltre());
        //pRequirementDescriptionPanel.setPie3D(pFiltrePanel.getFiltre());
        reloadReqPerformed();
    }

    public void update(Observable pObservable, Object arg) {
        //System.out.println("[RequirementDescriptionPanel->update] : " + arg + ", observable " + pObservable);
        if (loadData ){
            return;
        }

        reqTree.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        if (arg instanceof ReqEvent){
            ReqEvent event = (ReqEvent)arg;
            if (event.code == ReqEvent.REQSELECTED){
                pReq= (Requirement)event.arg;
                if (pReq.isFamilyReq()){
                    addReqFamilyButton.setEnabled(true/*Permission.canCreateReq()*/);
                    addReqButton.setEnabled(true/*Permission.canCreateReq()*/);
                } else {
                    addReqFamilyButton.setEnabled(false);
                    addReqButton.setEnabled(false);
                }
                if (pReq.getParent() == null){
                    delReqButton.setEnabled(false);
                    renameReqButton.setEnabled(false);
                    //pRequirementDescriptionPanel.setVisible(false);
                    //pRequirementDescriptionPanel.setPie3D(pFiltrePanel.getFiltre());
                } else {
                    delReqButton.setEnabled(/*Permission.canDeleteReq()*/true);
                    renameReqButton.setEnabled(true/*Permission.canCreateReq()*/);
                    //pRequirementDescriptionPanel.unsetPie3D(pFiltrePanel.getFiltre(), pReq);
                }
            }  else if (event.code == ReqEvent.FILTRE_REQ_CHANGE){
                Requirement pReq =  reqTree.getCurrentRequirement();
                try {
                    if (pReq != null) {
                        //if (pReq != null && pReq.getParent() != null) {
                        pStatRequirement.update(pReq, pFiltrePanel.getFilter(), true);
                        pRequirementDescriptionPanel.setRequirement(pReq);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }

            }
            if (pObservable != null) {
                pRequirementDescriptionPanel.update(pObservable, arg);
            }
        }
        reqTree.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void setTestTree(DynamicTree pDynamicTree){
        pRequirementDescriptionPanel.setTestTree(pDynamicTree);
    }




    public void actionPerformed(ActionEvent e ) {
        if (e.getSource().equals(addReqButton)){
            addReqPerformed(e);
        } else if (e.getSource().equals(addReqFamilyButton)){
            addReqFamilyPerformed(e);
        } else if (e.getSource().equals(delReqButton)){
            delReqPerformed(e);
        } else if (e.getSource().equals(findReqButton)){
            findReqPerformed(e);
        }else if (e.getSource().equals(renameReqButton)){
            renameReqPerformed(e);
        } else if (e.getSource().equals(reloadReqButton)){
            reloadReqPerformed();
        } else if (e.getSource().equals(creatTestReqButton)){
            //reqTree.print();reqTree.
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            new SelectReqTree(reqTree, Language.getInstance().getText("Gestion_des_tests"));
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        } else if (e.getSource().equals(importerReqButton)){
            importerReqPerformed();
        }
    }

    void importerReqPerformed(){
        try {
            String xmlFile = null;
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.addChoosableFileFilter(new ScriptFileFilter(("XML [*.xml]"),".xml"));
            int returnVal =  fileChooser.showDialog(this, Language.getInstance().getText("Selection"));
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                xmlFile = fileChooser.getSelectedFile().getAbsolutePath();
                if (xmlFile.indexOf(".")!=-1){
                    if (!xmlFile.substring(xmlFile.lastIndexOf(".")).equals(".xml")){
                        xmlFile+=".xml";
                    }
                }else{
                    xmlFile+=".xml";
                }

            }
            if (xmlFile!=null && !xmlFile.equals("")){
                pReqPlugin.updateProjectFromXML(xmlFile);
            }
        } catch (Exception ex) {
            Tools.ihmExceptionView(ex);
        }
    }

    public void  reloadReqPerformed(){
        try {
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
            loadData = true;
            reqTree.reload();
            //DataLoader.loadData(reqTree);
            loadData = false;
            reqTree.setPathToRoot();
            pRequirementDescriptionPanel.pStatRequirement.clearStat(true);
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        } catch (Exception ex){
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            ex.printStackTrace();
        }
    }

    void renameReqPerformed(ActionEvent e){
        Requirement pReq = reqTree.getCurrentRequirement();

        if (pReq == null) {
            //System.out.println("Current req is null");
            return;
        }
        CustomDialog dialog = new CustomDialog(SalomeTMFContext.getInstance().getSalomeFrame(), Language.getInstance().getText("Renommer") + " " + Language.getInstance().getText("Exigence"), Language.getInstance().getText("Entrer_nom_exigence"), pReq.getNameFromModel());
        dialog.setVisible(true);
        if(dialog.getValue() != null){
            try {
                pReq.updateNameInDBAndModel(dialog.getValue());
                DefaultMutableTreeNode node = reqTree.getCurrentNode();
                reqTree.refreshCurrent();
            } catch(Exception ex){
                //msg
                SalomeTMFContext.getInstance().showMessage(Language.getInstance().getText("Nom_existe"));
                ex.printStackTrace();
            }
        }
    }

    void addReqPerformed(ActionEvent e){
        CustomDialog dialog = new CustomDialog(SalomeTMFContext.getInstance().getSalomeFrame(), Language.getInstance().getText("Nouvelle_exigence"), Language.getInstance().getText("Entrer_nom_exigence"), null);
        dialog.setVisible(true);
        if(dialog.getValue() != null){
            Requirement req = new ReqLeaf(dialog.getValue(), "", null, 1);
            // Desactiver le filtre
            addReq(req);
            // Reactiver le filtre
        }
    }

    void addReqFamilyPerformed(ActionEvent e){
        CustomDialog dialog = new CustomDialog(SalomeTMFContext.getInstance().getSalomeFrame(), Language.getInstance().getText("Nouvel_arbre"), Language.getInstance().getText("Entrer_nom_branche"), null);
        dialog.setVisible(true);
        if(dialog.getValue() != null){
            reInitFiltre();
            Requirement req = new ReqFamily(dialog.getValue(), "", null);
            addReq(req);

        }
    }

    void addReq(Requirement req){
        try {
            ReqFamily pReqParent = (ReqFamily) reqTree.getCurrentRequirement();
            req.setParent(pReqParent);
            req.addInDB();
            reqTree.addRequirementToCurrent(req);
            if (pReqParent!=null){
                pReqParent.addRequirement(req);
            }

        } catch (Exception ex){
            Tools.ihmExceptionView(ex);
            ex.printStackTrace();
        }
    }

    void delReqPerformed(ActionEvent e){
        if (deleteConfirme(org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Exigence"))){
            reqTree.deleteCurrentRequirement();
        }
    }

    boolean deleteConfirme(String quoi){
        Object[] options = {org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Oui"), org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Non")};
        int choice = -1;

        choice = SalomeTMFContext.getInstance().askQuestion(
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("confimation_suppression2") + " " + quoi + " ?",
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Attention_"),
                                                            JOptionPane.WARNING_MESSAGE,
                                                            options);

        if (choice == JOptionPane.YES_OPTION){
            return true;
        } else {
            return false;
        }
    }

    void findReqPerformed(ActionEvent e){
        CustomDialog dialog = new CustomDialog(SalomeTMFContext.getInstance().getSalomeFrame(), Language.getInstance().getText("Rechercher_exigence"), Language.getInstance().getText("Entrer_nom_exigence"), null);
        dialog.setVisible(true);
        if(dialog.getValue() != null){
            boolean isID = false;
            try {
                Integer.parseInt(dialog.getValue());
                isID = true;
            } catch ( Exception ex) {
                isID = false;
            }
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
            reqTree.findRequirementFromCurrent(dialog.getValue(), isID);
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public RequirementTree getTree(){
        return reqTree;
    }


    /*public void focusChange(){
      if (pRequirementDescriptionPanel != null){
      pRequirementDescriptionPanel.doUpdateDesc();
      }
      }*/


    /*public static void main(String arg[]){
      JFrame pFrame = new  JFrame("Requirement Plugins");
      RequirementPanel pRequirementPanel = new RequirementPanel();
      pFrame.getContentPane().add(pRequirementPanel);
      pFrame.pack();
      pFrame.setVisible(true);
      }*/


    void reInitFiltre(){
        pFiltrePanel.reInit(true);
        pCoverFiltrePanel.reInit(true);
        pInfoFiltrePanel.reInit(true);
    }


}
