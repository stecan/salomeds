package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

public class HistoryDescViewDialog extends JDialog implements ActionListener {
    JButton fermerButton;
         
    public HistoryDescViewDialog(String htmlText){
        super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
        setModal(true);
        JPanel contentPan = new JPanel(new BorderLayout());
        HistoryDescViewPanel pHistoryDescViewPanel = new HistoryDescViewPanel(htmlText);
       
        JPanel buttonPan = new JPanel(new FlowLayout());
            
        fermerButton = new JButton(Language.getInstance().getText("Fermer"));
        fermerButton.setEnabled(true); //false
        fermerButton.addActionListener(this);
        buttonPan.add(fermerButton);
        
        contentPan.add(pHistoryDescViewPanel, BorderLayout.CENTER);
        contentPan.add(buttonPan, BorderLayout.SOUTH);
        setContentPane(contentPan);
        /*pack();
          setLocationRelativeTo(SalomeTMFContext.getInstance().getSalomeFrame());
          setVisible(true);*/
        centerScreen();
    }
                
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
         
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(fermerButton)){
            fermerPerformed(e);
        }
            
    }
            
    void fermerPerformed(ActionEvent e){
        dispose();
    }
}
