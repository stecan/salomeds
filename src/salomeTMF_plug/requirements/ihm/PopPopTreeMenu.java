package salomeTMF_plug.requirements.ihm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;

public class PopPopTreeMenu  extends  JPopupMenu{
            
    JMenuItem copieMenuItem = null;
    JMenuItem couperMenuItem = null;
    JMenuItem collerMenuItem = null;
            
    Requirement toCopie = null;
    boolean couper = false;
            
    RequirementTree reqTree;
            
    public PopPopTreeMenu (RequirementTree _reqTree){
        super();
        reqTree = _reqTree;
                
        copieMenuItem = new JMenuItem(Language.getInstance().getText("Copier"));
        copieMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //DataModel.deleteInTestTree();
                    toCopie = (Requirement) reqTree.getCurrentNode().getUserObject();
                    collerMenuItem.setEnabled(true);
                                        
                                
                }
            });
        add(copieMenuItem);
                        
        couperMenuItem = new JMenuItem(Language.getInstance().getText("Couper"));
        couperMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    //DataModel.deleteInTestTree();
                    toCopie = (Requirement) reqTree.getCurrentNode().getUserObject();
                    collerMenuItem.setEnabled(true);
                    couper = true;
                }
            });
        add(couperMenuItem);
                        
                        
        collerMenuItem = new JMenuItem(Language.getInstance().getText("Coller"));
        collerMenuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Requirement pDest =  (Requirement) reqTree.getCurrentNode().getUserObject();
                    if (pDest instanceof ReqFamily && toCopie != null){
                                                        
                        ReqFamily reqDest = (ReqFamily) reqTree.getCurrentNode().getUserObject();
                        Requirement newReq = toCopie.getCopie(reqDest);
                        DefaultMutableTreeNode nodeSource = reqTree.findRequirementFromParent(toCopie);
                        DefaultMutableTreeNode nodeDest = reqTree.findRequirementFromParent(reqDest);
                        /* Garde fou */
                        if (toCopie.getIdBdd() == reqDest.getIdBdd()){
                            return;
                        }
                                                        
                        if (couper){
                            try {
                                //BDD
                                toCopie.updateParentInDBAndModel(reqDest);
                                                                
                                //IHM
                                reqTree.treeModel.removeNodeFromParent(nodeSource);
                                reqTree.insertNode(nodeDest, nodeSource);
                                                                        
                                reqTree.setSelectionPath(new TreePath(nodeSource.getPath()));
                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        } else {
                                                                
                            boolean copieLink = false;
                            if (newReq instanceof ReqLeaf){
                                Object[] options = {org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Oui"), org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Non")};
                                int choice = -1;

                                choice = SalomeTMFContext.getInstance().askQuestion(
                                                                                    org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Copie_link") ,
                                                                                    "",
                                                                                    JOptionPane.QUESTION_MESSAGE,
                                                                                    options);

                                if (choice == JOptionPane.YES_OPTION){
                                    copieLink = true;
                                } else {
                                    copieLink =  false;
                                }
                            }
                            try {
                                reqDest.pastInDBandModel(newReq, reqDest, true);
                                                                        
                                if (copieLink){
                                    Vector listTestWrapper = ((ReqLeaf)toCopie).getTestWrapperCoveredFromDB();
                                    int size = listTestWrapper.size();
                                    for (int i=0; i<size ; i++){
                                        TestWrapper pTestWrapper = (TestWrapper)listTestWrapper.elementAt(i);
                                        newReq.addTestCoverInDB(pTestWrapper.getIdBDD(), false);
                                    }
                                }
                                                                
                                                                        
                                //IHM
                                reqTree.addRequirementToReqNodeInTree(newReq , nodeDest, true);
                                //reqTree.deleteRequirementInModelAndDB(toCopie);
                            } catch (Exception ex){
                                ex.printStackTrace();
                            }
                        }
                    } else {
                        return;
                    }
                                                
                                                
                    collerMenuItem.setEnabled(false);
                                                
                    toCopie = null;
                    couper = false;

                }
            });
        collerMenuItem.setEnabled(false);
        add(collerMenuItem);
                        
    } // Fin de la m?thode createTreePopUpForList
                    
}
           

