/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@orange-ftgroup.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.requirements.data.FiltreUserData;

public class UserDataFiltrePanel extends JDialog implements ActionListener {

    JButton applyButton;
    JButton cancelButton;
        
    JCheckBox checkNameIsFiltred;
    JCheckBox checkOrigineIsFiltred;
    JCheckBox checkReferenceIsFiltred;
    JCheckBox checkVersionIsFiltred;
    JCheckBox checkVerificationIsFiltred;
        
    JTextField filedNamePatern;
    JComboBox filedOriginePatern;
    JComboBox filedReferencePatern;
    JTextField filedVersionPatern;
    JComboBox filedVerificationPatern;
        
    JLabel labelName;
    JLabel labelOrigine;
    JLabel labelReference;
    JLabel labelVersion;
    JLabel labelVerification;
        
    JPanel mainPanel;
    JPanel dataPanel;
    JPanel buttonPanel;
        
    boolean doValidate = false;
        
    FiltreUserData pFiltreUserData;
    public UserDataFiltrePanel(FiltreUserData _pFiltreUserData) {
        super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
        setModal(true);
        pFiltreUserData = _pFiltreUserData;
        initComponent();
                 
    }
        
    void initComponent(){
        mainPanel = new JPanel(new BorderLayout());
        dataPanel = new JPanel(new GridLayout(5,3));
        buttonPanel = new JPanel(new FlowLayout());
                
        /* Label */
        labelName = new JLabel(Language.getInstance().getText("Nom") +" : ");
        labelOrigine = new JLabel("Origine : ");
        labelReference = new JLabel(Language.getInstance().getText("Reference") +" : ");
        labelVersion = new JLabel("Version : ");
        labelVerification = new JLabel(Language.getInstance().getText("Mode_de_verification") + " : ");
                
        /* Input Field */
        filedNamePatern = new JTextField(pFiltreUserData.getNamePatern());
        filedOriginePatern = new JComboBox(FiltreUserData.listOfOrigine);
        filedOriginePatern.setEditable(false);
        filedOriginePatern.setSelectedItem(pFiltreUserData.getOriginePatern());
        filedReferencePatern= new JComboBox(FiltreUserData.listOfReference);
        filedReferencePatern.setEditable(false);
        filedReferencePatern.setSelectedItem(pFiltreUserData.getReferencePatern());
        filedVersionPatern  = new JTextField(pFiltreUserData.getVersionPatern());
        filedVerificationPatern = new JComboBox(FiltreUserData.listOfVerification);
        filedVerificationPatern.setEditable(false); 
        filedVerificationPatern.setSelectedItem(pFiltreUserData.getVerificationPatern());
                
        /* JCheckBox */
        checkNameIsFiltred = new JCheckBox();
        checkNameIsFiltred.setSelected(pFiltreUserData.isNameIsFiltred());
        checkOrigineIsFiltred  = new JCheckBox();
        checkOrigineIsFiltred.setSelected(pFiltreUserData.isOrigineIsFiltred());
        checkReferenceIsFiltred = new JCheckBox();
        checkReferenceIsFiltred.setSelected(pFiltreUserData.isReferenceIsFiltred());
        checkVersionIsFiltred = new JCheckBox();
        checkVersionIsFiltred.setSelected(pFiltreUserData.isVersionIsFiltred());
        checkVerificationIsFiltred = new JCheckBox();
        checkVerificationIsFiltred.setSelected(pFiltreUserData.isVerificationIsFiltred());
                
        dataPanel.add(labelName);
        dataPanel.add(filedNamePatern);
        dataPanel.add(checkNameIsFiltred);
                
        dataPanel.add(labelOrigine);
        dataPanel.add(filedOriginePatern);
        dataPanel.add(checkOrigineIsFiltred);
                
        dataPanel.add(labelReference);
        dataPanel.add(filedReferencePatern);
        dataPanel.add(checkReferenceIsFiltred);
                
        dataPanel.add(labelVersion);
        dataPanel.add(filedVersionPatern);
        dataPanel.add(checkVersionIsFiltred);
                
        dataPanel.add(labelVerification);
        dataPanel.add(filedVerificationPatern);
        dataPanel.add(checkVerificationIsFiltred);
                
                
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        applyButton = new JButton(Language.getInstance().getText("Valider"));
        applyButton.addActionListener(this);
        buttonPanel.add(cancelButton);
        buttonPanel.add(applyButton);
            
        mainPanel.add(dataPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
            
        setContentPane(mainPanel);
        centerScreen();
    }
        
    public void actionPerformed(ActionEvent evt){
        if (evt.getSource().equals(applyButton)){
            applyButtonPerformed();
        } else if (evt.getSource().equals(cancelButton)){
            dispose();
        }
    }
        
    void applyButtonPerformed(){
        pFiltreUserData.setFiltre(
                                  filedNamePatern.getText(), checkNameIsFiltred.isSelected(), 
                                  (String) filedOriginePatern.getSelectedItem(), checkOrigineIsFiltred.isSelected(), 
                                  (String) filedReferencePatern.getSelectedItem(), checkReferenceIsFiltred.isSelected(), 
                                  filedVersionPatern.getText(), checkVersionIsFiltred.isSelected(), 
                                  (String) filedVerificationPatern.getSelectedItem(), checkVerificationIsFiltred.isSelected());
        doValidate = true;
        dispose();
    }
        
    /**********************************************************/
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();  
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        requestFocus();
    }

    public boolean isDoValidate() {
        return doValidate;
    }
        
}
