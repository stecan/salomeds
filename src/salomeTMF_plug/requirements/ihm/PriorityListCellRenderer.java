/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JList;

import salomeTMF_plug.requirements.data.ReqFilter;
import salomeTMF_plug.requirements.data.ReqLeaf;

public class PriorityListCellRenderer extends DefaultListCellRenderer {
    static ImageIcon ReqLeafIncon ;
    static {
        java.net.URL imgURL = RequirementTree.class.getResource("/salomeTMF_plug/requirements/resources/images/req.png");
        if (imgURL != null) {
            ReqLeafIncon =  new ImageIcon(imgURL);
        } else {
            ReqLeafIncon =  null;
        }
    }

    /**
     * @covers SFD_ForgeORTF_EXG_SEL_000020 - \ufffd2.4.3
     * Jira : FORTF - 40
     */
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index, boolean isSelected,
                                                  boolean cellHasFocus) {
        super.getListCellRendererComponent(list,
                                           value,
                                           index,
                                           isSelected,
                                           cellHasFocus);

        if (value instanceof ReqLeaf){
            ReqLeaf pReqLeaf = (ReqLeaf)value;
            if (ReqLeafIncon != null){
                setIcon(ReqLeafIncon);
            }
            setText(pReqLeaf.getLongName());

            //20100113 - D\ufffdbut modification Forge ORTF
            if (pReqLeaf.getPriorityFromModel() == ReqFilter.P_HIGHT){
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.red);
                }
            } else if (pReqLeaf.getPriorityFromModel() == ReqFilter.P_MEDIUM){
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.blue);
                }
            }else {
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.gray);
                }
            }
            //20100113 - Fin modification Forge ORTF
        }
        return this;
    }
}
