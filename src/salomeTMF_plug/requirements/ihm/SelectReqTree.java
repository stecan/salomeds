/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Jean-Marie Hallouet
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.plugins.UICompCst;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.GenTestTools;
import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;


/**
 * 
 * @author Jean-Marie HALLOUET
 *
 */
public class SelectReqTree extends JDialog implements ActionListener, DataConstants, WindowListener {
        
    /* JDialog buttons */
    JButton removeButton;
    JButton addButton;
    JButton validateButton;
    JButton cancelButton;
        
    /* Test tree */
    SimpleDynamicTestTree dynTestTree;
    DynamicTree pDynamicTree;
    Hashtable simpleDataCache = new Hashtable();
        
    /* Original requirement trees */
    RequirementTree globalReqTree;
        
    /* Jtrees used by the selection window  */
    JTree usedReqTree;
    JTree usedDynTestTree;      
        
    /* List of simpleData to remove from model */
    Vector simpleDataRemoved;
        
    /**
     * Constructor
     * @param _reqTree the original requirement tree
     * @param title Window tiltle
     */
    public SelectReqTree(RequirementTree _reqTree, String title){
        super(SalomeTMFContext.getInstance().getSalomeFrame());
        setResizable(false);
        setModal(true);
        Util.log("[requirements] -> SelectReqTree : start constructor");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(this);
        /* Initialize list */
        simpleDataRemoved  = new Vector();
                
        /* Refresh DataModel */
        DataModel.reloadFromBase(true);
                
        /* Load Requirement tree without links */
        usedReqTree=_reqTree.getCopy(true);
                
        /* Keep requirement tree to check existence in base */
        globalReqTree = _reqTree;
        // TODO externaliser
        dynTestTree =new SimpleDynamicTestTree(Language.getInstance().getText("Plan_de_tests"),DataConstants.FAMILY);           
        pDynamicTree = (DynamicTree)SalomeTMFContext.getInstance().getUIComponent(UICompCst.TEST_DYNAMIC_TREE);
        GenTestTools.initTools(_reqTree, pDynamicTree);
                
        /* initialize trees */
        initTrees(false); // ADD By MM
        
        usedDynTestTree = dynTestTree.getTree();
        initComponent(title);
        
        Util.log("[requirements] -> SelectReqTree : end constructor");
    }
        
    /**
     *  GUI initialization  
     * @param title Window title
     */
    void initComponent(String title){
        Util.log("[requirements] -> SelectReqTree : start initComponent");
        Util.log("[requirements] -> SelectReqTree : title : "+title);
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        removeButton = new JButton("<");
        removeButton.addActionListener(this);
        
        addButton = new JButton(">");
        addButton.addActionListener(this);
        
        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(addButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
        buttonSet.add(removeButton);
        
        JScrollPane reqScrollPane = new JScrollPane(usedReqTree);
        reqScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Exigence_disponible")));
        reqScrollPane.setPreferredSize(new Dimension(300,450));

        JScrollPane selectedScrollPane = new JScrollPane(usedDynTestTree);
        selectedScrollPane.setBorder(BorderFactory.createTitledBorder("Test Plan"));
        selectedScrollPane.setPreferredSize(new Dimension(300,450));
        selectedScrollPane.getViewport().setView(usedDynTestTree);
        
        JPanel windowPanel = new JPanel();
        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
        windowPanel.add(reqScrollPane);
        windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
        windowPanel.add(buttonSet);
        windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
        windowPanel.add(selectedScrollPane);
        
        validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(this);
        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        
        JPanel secondButtonSet = new JPanel();
        secondButtonSet.add(validateButton);
        secondButtonSet.add(cancelButton);
        JPanel center = new JPanel();
        center.add(windowPanel);
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(center);
        page.add(secondButtonSet);
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        this.setTitle(title);
        //this.setLocation(300,200);
        /*this.setLocationRelativeTo(this.getParent()); 
          this.pack();
          this.setVisible(true);
        */ 
        centerScreen();
        Util.log("[requirements] -> SelectReqTree : end initComponent");
    }
        
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
                 
        this.setVisible(true); 
        requestFocus();
    }
        
    /**
     * Build trees
     * @param reinit reinitialize tree if true
     */
    void initTrees(boolean reinit){
        Util.log("[requirements] -> SelectReqTree : start initTrees("+reinit+") start");
        if (reinit){
            reInitReqTree();
        }
        DefaultMutableTreeNode pTempNode= globalReqTree.getParentNode();
        dynTestTree.clear();
        GenTestTools.getBijectiveSimpleDataTree(pTempNode, dynTestTree);
        dynTestTree.getModel();
        ((DefaultTreeModel)dynTestTree.getModel()).reload();
        
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)((DefaultTreeModel)usedReqTree.getModel()).getRoot();
        GenTestTools.removeReqBijectiveLinked((DefaultTreeModel)usedReqTree.getModel(), node);
                
        expandAll(dynTestTree.getTree()); //ADD by MM
        expandAll(usedReqTree); //ADD by MM
        Util.log("[requirements] -> SelectReqTree : start initTrees("+reinit+") end");
    }
        
    /**
     * Rebuild trees after nodes modifications
     *
     */
    void reInitReqTree(){
        Util.log("[requirements] -> SelectReqTree : start reInitReqTree ");
        DefaultMutableTreeNode copyRootNode = (DefaultMutableTreeNode)((DefaultTreeModel)usedReqTree.getModel()).getRoot();
        copyRootNode.removeAllChildren();
                
        DefaultMutableTreeNode refereceRootNode = (DefaultMutableTreeNode) globalReqTree.getCopy(false).getModel().getRoot();
        int nbChild =  refereceRootNode.getChildCount();
        for (int i = 0; i < nbChild; i++){
            DefaultMutableTreeNode nodeToCopy = (DefaultMutableTreeNode) refereceRootNode.getChildAt(i);
            copyRootNode.add(getCopy(nodeToCopy));
        }
        ((DefaultTreeModel)usedReqTree.getModel()).reload();
        Util.log("[requirements] -> SelectReqTree : end reInitReqTree ");
    }
        
    /**
     * get a copy of a tree
     * @param node
     * @return the parent node
     */
    private DefaultMutableTreeNode getCopy(DefaultMutableTreeNode node){
        Util.log("[requirements] -> SelectReqTree : start getCopy("+node.toString()+") start");
        DefaultMutableTreeNode copy_node = new DefaultMutableTreeNode(node.getUserObject());
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            return copy_node;
        } else {
            int i = 0;
            while (i < nbChild){
                DefaultMutableTreeNode child_copy = getCopy((DefaultMutableTreeNode)node.getChildAt(i));
                copy_node.insert(child_copy, i);
                i++;
            }
        }
        Util.log("[requirements] -> SelectReqTree :  getCopy("+node.toString()+") return "+copy_node.toString());
        Util.log("[requirements] -> SelectReqTree :  getCopy("+node.toString()+") end");
        return copy_node;
    }
        
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(removeButton)){
            removePerformed(e);
        } 
        else if (e.getSource().equals(addButton)) {
            addPerformed(e);
        } 
        else if (e.getSource().equals(validateButton)) {
            validatePerformed(e);
        } 
        else if (e.getSource().equals(cancelButton)){
            cancelPerformed(e);
        } 
    }
        
        
    /**
     * Remove links from the Test Plan
     * @param e  corresponding to the '<' button click
     */
    void removePerformed(ActionEvent e){
        Util.log("[requirements] -> SelectReqTree -> removePerformed : start ");
        /* if test tree is not empty */
        if(dynTestTree.getTree()!=null){
            TreePath[] pathTab =dynTestTree.getTree().getSelectionPaths(); 
            if(pathTab!=null) {
                /* get All selected simpledata =(family | testList | test ) */
                for (int i = 0; i < pathTab.length; i++)  {
                    Util.log("[requirements] -> SelectReqTree -> removePerformed : pathTab["+i+"] ="+ pathTab[i].toString());
                    Util.log("[requirements] -> SelectReqTree -> removePerformed : lastComponent de pathTab["+i+"] = "+
                             pathTab[i].getLastPathComponent().toString());
                                         
                    /*  for each selected simpledata get its type (family, testList, test)*/
                    DefaultMutableTreeNode simpleDataNode=(DefaultMutableTreeNode)pathTab[i].getLastPathComponent();
                    int level = simpleDataNode.getLevel();
                                        
                    if(level != 0 && simpleDataNode != null) {
                        SimpleData simpleData = (SimpleData) simpleDataNode.getUserObject();
                        //GenTestTools2.getRequirment(simpleData);
                        GenTestTools.removeSimpleDataToDynamicTestTree(simpleData);
                        SimpleData pSimpleDataOrg =  getSalomeModelSimpleData(simpleData);
                        if (pSimpleDataOrg!= null){
                            simpleData = pSimpleDataOrg;
                        }
                        addSimpleDataToRemoveLink(simpleData);
                    }
                }
            }
        } 
        initTrees(true);
        Util.log("[requirements] -> SelectReqTree -> removePerformed : end ");
    }
                

    /**
     * remove requirement links from one simpleData
     * @param simpleData
     */
    void addSimpleDataToRemoveLink(SimpleData simpleData){
        Util.log("[requirements] -> SelectReqTree : start addSimpleDataToRemoveLink("+simpleData.toString()+") start");
        if (simpleData instanceof Test){
            if (!simpleDataRemoved.contains(simpleData)){
                simpleDataRemoved.add(simpleData);
            }
        } else if (simpleData instanceof TestList){
            TestList pTestList = (TestList) simpleData;
            ArrayList lestOfTest = pTestList.getTestListFromModel();
            int size = lestOfTest.size();
            for (int i = 0 ; i < size ; i++){
                Test pTest = (Test) lestOfTest.get(i);
                addSimpleDataToRemoveLink(pTest);
            }
        } else if (simpleData instanceof Family){
            Family pFamily = (Family) simpleData;
            ArrayList lestOfSuite  = pFamily.getSuiteListFromModel();
            int size = lestOfSuite.size();
            for (int i = 0 ; i < size ; i++){
                TestList pTestList = (TestList) lestOfSuite.get(i);
                addSimpleDataToRemoveLink(pTestList);
            }
        }
        Util.log("[requirements] -> SelectReqTree : end addSimpleDataToRemoveLink("+simpleData.toString()+") end");
    }
        

    /**
     *  Generate requirement structures to test structures
     *  @param e corresponding to the '>' button click
     */
    void addPerformed(ActionEvent e)
    {  
        Util.log("[requirements] -> SelectReqTree -> addPerformed start");
        TreePath[] pathTab =usedReqTree.getSelectionPaths();
        if(pathTab!=null) {
            /* get All selected requirements */
            for (int i = 0; i < pathTab.length; i++) {
                Util.log("[requirements] -> SelectReqTree : pathTab["+i+"] ="+ pathTab[i].toString());
                /*  for each selected requirement */
                DefaultMutableTreeNode reqNode = (DefaultMutableTreeNode)pathTab[i].getLastPathComponent();
                addTestTreeFromReqNode(reqNode);
            }
        }
        initTrees(true);
        Util.log("[requirements] -> SelectReqTree -> addPerformed end");
    }// end addPerformed
    
        
  
        
    /**
     * validate in base the test generation and/or remove test<->requirement links
     * @param e corresponding to the 'Validate' button click
     */
    void validatePerformed(ActionEvent e){
        Util.log("[requirements] -> SelectReqTree -> validatedPerformed start");
        valideTemplateModel();
        /* Refresh DataModel */
        DataModel.reloadFromBase(true);
        dispose();
        Util.log("[requirements] -> SelectReqTree -> validatedPerformed end");
    }
        
        
        
    /**
     * Cancel the transformation and close the dialog window
     * @param e corresponding to 'Cancel' button
     */
    void cancelPerformed(ActionEvent e){
        Util.log("[requirements] -> SelectReqTree -> cancelPerformed start");
        clearTemplateModel();
        dispose();
        Util.log("[requirements] -> SelectReqTree -> cancelPerformed end");
    }
        
    /**
     * 
     * clear all simpleData that have been temporarly used during the selection
     */
    void clearTemplateModel(){
        Util.log("[requirements] -> SelectReqTree -> clearTemplateModel() start");
        DefaultMutableTreeNode rootNode = dynTestTree.getRoot(); 
        int nbChild =  rootNode.getChildCount();
        for (int i = 0; i < nbChild; i++){
            clearTemplateModel((DefaultMutableTreeNode) rootNode.getChildAt(i));
        }
        Util.log("[requirements] -> SelectReqTree -> clearTemplateModel() end");
    }
        
    /**
     *  Validate all dynTestTree simpledata (test,testList,family) in bdd
     *
     */
    void valideTemplateModel(){
        Util.log("[requirements] -> SelectReqTree -> valideTemplateModel start");
        DefaultMutableTreeNode rootNode = dynTestTree.getRoot();
        int nbChild =  rootNode.getChildCount();
        for (int i = 0; i < nbChild; i++){
            valideTemplateModel((DefaultMutableTreeNode) rootNode.getChildAt(i));
        }
        int nbLinkToRemove = simpleDataRemoved.size();
        for (int i = 0 ; i < nbLinkToRemove; i++ ) {
            SimpleData pSimpleData = (SimpleData) simpleDataRemoved.elementAt(i);
            if (pSimpleData instanceof Test){
                Test pTest = (Test) pSimpleData;
                Requirement bijectiveReq  = GenTestTools.getRequirement(pTest);
                if (GenTestTools.isReqDefaultCovered(bijectiveReq, pTest.getNameFromModel())){
                    try {
                        ((ReqLeaf)bijectiveReq).deleteCoverForTest(pTest.getIdBdd());
                    } catch(Exception  e){
                        e.printStackTrace();
                    }
                }
            }
        }
        Util.log("[requirements] -> SelectReqTree -> valideTemplateModel end");
    }
        
        
    /**
     * Clear a simpleData from the model
     * @param node
     */
    void clearTemplateModel(DefaultMutableTreeNode node){
        Util.log("[requirements] -> SelectReqTree -> clearTemplateModel("+node.toString()+") start");
        Util.log("[requirements] -> SelectReqTree -> clearTemplateModel node to clearTemplateModel " + node.getUserObject());
        SimpleData pSimplData = (SimpleData) node.getUserObject();
        if (!pSimplData.isInBase()){
            if (pSimplData instanceof Family){
                                
            } else if (pSimplData instanceof TestList){
                TestList pTestList = (TestList) pSimplData;
                Family pFamily = pTestList.getFamilyFromModel();
                if (pFamily != null){
                    pFamily.deleteTestListInModel(pTestList);
                }
            } else if (pSimplData instanceof Test){
                Test pTest = (Test) pSimplData;
                TestList pTestList = pTest.getTestListFromModel();
                if (pTestList != null){
                    pTestList.deleteTestInModel(pTest);
                }
            } 
        }
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            return;
        } else {
            int i = 0;
            while (i < nbChild){
                clearTemplateModel((DefaultMutableTreeNode)node.getChildAt(i));
                i++;
            }
        }
        Util.log("[requirements] -> SelectReqTree -> clearTemplateModel("+node.toString()+") end");
    }
        
    /**
     * get the simpleData from the Salome model
     * @param pSimplData
     * @return
     */
    SimpleData getSalomeModelSimpleData(SimpleData pSimplData){
        Util.log("[requirements] -> SelectReqTree -> getSalomeModelSimpleData("+pSimplData.toString()+") start");
        SimpleData dataInSalome = null;
        
        if(pSimplData!=null){   
            dataInSalome = (SimpleData) simpleDataCache.get(pSimplData);
            if (dataInSalome != null){
                return dataInSalome;
            } 
            if (pSimplData instanceof Family){
                Family pFamily = (Family) pSimplData;
                DefaultMutableTreeNode pSimpleDataNode = pDynamicTree.findRemoveFamilyNode(pFamily.getNameFromModel(), false);
                if (pSimpleDataNode != null){
                    dataInSalome = (SimpleData) pSimpleDataNode.getUserObject();
                    simpleDataCache.put(pSimplData, dataInSalome );
                }
            } else if (pSimplData instanceof TestList){
                TestList pTestList = (TestList) pSimplData;
                Family pFamily = pTestList.getFamilyFromModel();
                DefaultMutableTreeNode pSimpleDataNode = pDynamicTree.findRemoveTestListNode(pTestList.getNameFromModel(), pFamily.getNameFromModel(),  false);
                if (pSimpleDataNode != null) {
                    dataInSalome = (SimpleData) pSimpleDataNode.getUserObject();
                    simpleDataCache.put(pSimplData, dataInSalome );
                }
            } else if (pSimplData instanceof Test){
                Test pTest = (Test) pSimplData;
                TestList pTestList = pTest.getTestListFromModel();
                Family pFamily = pTestList.getFamilyFromModel();
                DefaultMutableTreeNode pSimpleDataNode = pDynamicTree.findRemoveTestNode(pTest.getNameFromModel(), pTestList.getNameFromModel(), pFamily.getNameFromModel(),  false);
                if (pSimpleDataNode != null) {
                    dataInSalome = (SimpleData) pSimpleDataNode.getUserObject();
                    simpleDataCache.put(pSimplData, dataInSalome );
                }
            }
        }
                
        Util.log("[requirements] -> SelectReqTree -> getSalomeModelSimpleData("+pSimplData.toString()+") end");
        return dataInSalome;
    }
        
    /**
     * Add simpledata template in bdd
     * @param node
     */
    void valideTemplateModel(DefaultMutableTreeNode node){
        Util.log("[requirements] -> SelectReqTree -> valideTemplateModel("+node.toString()+") start");
        SimpleData pSimplData = (SimpleData) node.getUserObject();
        SimpleData pSimplData2 = getSalomeModelSimpleData(pSimplData);
        if (pSimplData2 != null){
            pSimplData = pSimplData2;
        }
                
        if (!pSimplData.isInBase()){
            // Ajout en base et model
            if (pSimplData instanceof Family){
                Family pFamily = (Family) pSimplData;
                try {
                    DataModel.getCurrentProject().addFamilyInDBAndModel(pFamily);
                } catch(Exception  e){
                    e.printStackTrace();
                }
            } else if (pSimplData instanceof TestList){
                TestList pTestList = (TestList) pSimplData;
                Family pFamily = pTestList.getFamilyFromModel();
                try {
                    pFamily.addTestListInDB(pTestList);
                } catch(Exception  e){
                    e.printStackTrace();
                }
            } else if (pSimplData instanceof Test){
                Test pTest = (Test) pSimplData;
                User currentUser = DataModel.getCurrentUser();
                Util.log("[requirements] -> SelectReqTree -> valideTemplateModel : Conceptor login = "  + currentUser.getLoginFromModel());
                pTest.setConceptorLoginInModel(currentUser.getLoginFromModel());
                pTest.setConceptorInModel(currentUser.getFirstNameFromModel() + " " + currentUser.getLastNameFromModel());
                TestList pTestList = pTest.getTestListFromModel();
                try {
                    pTestList.addTestInDB(pTest);
                    Requirement bijectiveReq  = GenTestTools.getRequirement(pTest);
                    if (bijectiveReq != null && bijectiveReq instanceof ReqLeaf) {
                        ((ReqLeaf)bijectiveReq).addTestCoverInDB(pTest.getIdBdd(),true);
                    }
                                        
                } catch(Exception  e){
                    e.printStackTrace();
                }
                                
            } 
        } else {
            if (pSimplData instanceof Test){
                //Ajout du lien si non existant
                Test pTest = (Test) pSimplData;
                try {
                    Requirement bijectiveReq  = GenTestTools.getRequirement(pTest);
                    if (bijectiveReq != null && bijectiveReq instanceof ReqLeaf) {
                        if (!GenTestTools.isReqDefaultCovered(bijectiveReq, pTest.getNameFromModel())){
                            ((ReqLeaf)bijectiveReq).addTestCoverInDB(pTest.getIdBdd(), true);
                        }
                    }
                } catch(Exception  e){
                    e.printStackTrace();
                }
                                
            } 
        }
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            return;
        } else {
            int i = 0;
            while (i < nbChild){
                valideTemplateModel((DefaultMutableTreeNode)node.getChildAt(i));
                i++;
            }
        }
        Util.log("[requirements] -> SelectReqTree -> valideTemplateModel("+node.toString()+") end");
    }
        
    /**
     * Add test and its family and testList to the dynTestTree
     * @param reqNode
     */
    public void addTestTreeFromReqNode(DefaultMutableTreeNode reqNode){
        Util.log("[requirements] -> SelectReqTree -> addTestTreeFromReqNode("+reqNode.toString()+") start");
        if(reqNode!=null && !reqNode.isRoot()) {
            Requirement pReq = (Requirement) reqNode.getUserObject();
            if (pReq instanceof ReqLeaf){
                //Cette methode cree le simple data (si besoin) et l'ajoute dans l'arbre de tests de reference de GenTestTools2
                SimpleData pSimpleData = GenTestTools.getSimpleData(pReq, new Vector(), true);
                if (pSimpleData instanceof Test){
                    checkAndAddTest((Test)pSimpleData);
                }
            } else {
                //Ajouter tous les reqLeaf depuis la reqfamily
                ReqFamily pReqFamily = (ReqFamily)reqNode.getUserObject();
                Vector allLeafReq = pReqFamily.getAllLeaf();
                for(int j=0;j<allLeafReq.size();j++){
                    ReqLeaf reqLeaf = (ReqLeaf)allLeafReq.elementAt(j); 
                    SimpleData pSimpleData = GenTestTools.getSimpleData(reqLeaf, new Vector(), true);
                    checkAndAddTest((Test)pSimpleData);
                }
                                
                        
            }
        }
        Util.log("[requirements] -> SelectReqTree -> addTestTreeFromReqNode("+reqNode.toString()+") end");
    }
                
    /**
     *  Check if test name < 255 char and if is not already in the model
     * @param pTest
     */
    private void checkAndAddTest(Test pTest){
        Util.log("[requirements] -> SelectReqTree -> checkAndAddTest("+pTest.toString()+") start");
        if(pTest!=null){
            if(pTest.getNameFromModel().length()>255)
                {//TODO a externaliser
                    JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame(),
                                                  Language.getInstance().getText("erreur_longueur_param")
                                                  +pTest.getNameFromModel().length()+". \n",
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                } else {
                Test pSimpleDataOrg =  (Test)getSalomeModelSimpleData(pTest);
                if (pSimpleDataOrg!= null){
                    pTest = pSimpleDataOrg;
                }
                if (simpleDataRemoved.contains(pTest)){
                    simpleDataRemoved.remove(pTest);
                }
            }
        }
        Util.log("[requirements] -> SelectReqTree -> checkAndAddTest("+pTest.toString()+") end");
    }
        
        
        
    /*************************** TreeTools ********************/
                
    /**
     * Expand all tree nodes 
     * @param tree JTree to expand
     */
    static void expandAll(JTree tree)  { 
        Util.log("[requirements] -> SelectReqTree -> expandAll start");
        int row = 0; 
        while (row < tree.getRowCount())  {
            tree.expandRow(row);
            row++;
        }
        Util.log("[requirements] -> SelectReqTree -> expandAll end");
    } 
        
    /**
     * Collapse all tree nodes
     * @param tree JTree to collapse
     */
    public void collapseAll(JTree tree) 
    {
        Util.log("[requirements] -> SelectReqTree -> collapseAll start");
        int row = tree.getRowCount() - 1;
        while (row >= 0) 
            {
                tree.collapseRow(row);
                row--;
            }
        Util.log("[requirements] -> SelectReqTree -> collapseAll end");
    }
        

    public void windowClosing(WindowEvent e) {
        cancelPerformed(null);
    }
    public void windowDeiconified(WindowEvent e) {
    }
    public void windowOpened(WindowEvent e) {
    }
    public void windowActivated(WindowEvent e) {
    }
    public void windowDeactivated(WindowEvent e) {
    }
    public void windowClosed(WindowEvent e) {
    }
    public void windowIconified(WindowEvent e) {
    } 
}


