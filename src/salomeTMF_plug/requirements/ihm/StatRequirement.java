package salomeTMF_plug.requirements.ihm;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.Requirement;

public class StatRequirement {
         
    Vector leafInCurrentFamily = new Vector(); //Vector of ReqFamily
    Hashtable allTestCovredByCurrentReq = new Hashtable(); //Hashtable Integer (id of test) -> Test
    Hashtable allTestCovredByReq = new Hashtable(); //Hashtable Integer (id of req) -> Vector of Test
    Hashtable allReqCovredByTest =  new Hashtable(); // Hastable Integer (id of Test) -> Vector of Req
    Hashtable executedStatCovredByCurrentReq = new Hashtable(); //Hashtable Test->Vector[campain, Execution, {ExecResult}, nbP, nbF, nbU, lastExecRusult, lastResult, test]
        
    Hashtable executedStatByCamp = new Hashtable();
    Hashtable executedStatByExec = new Hashtable();
    Hashtable executedStatByEnv = new Hashtable();
        
    Requirement pCurrentReq = null ;
        
        
    /*UTIL*/
    //Project currentProject;
        
    /* CACHE*/
    private Hashtable testwrapper2model = new Hashtable();
        
        
    public StatRequirement(){
        //currentProject = ReqPlugin.getProjectRef();
    }
        
    //public void update(Requirement pRequirement, int filtre, boolean forceReload) throws Exception{
    public void update(Requirement pRequirement, IReqFilter filtre, boolean forceReload) throws Exception{
        //System.out.println("Update State : "  + filtre);
        if (pRequirement == null){
            pCurrentReq = pRequirement;
            clearStat(forceReload);
            return;
        }
                
        if (forceReload == false && pRequirement.equals(pCurrentReq)){
            return;
        }
                
        pCurrentReq = pRequirement;
        clearStat(forceReload);
                
        if (pRequirement.isFamilyReq()){
                        
            leafInCurrentFamily = ((ReqFamily)pCurrentReq).getAllLeaf(filtre);
                        
            for (int j = 0; j < leafInCurrentFamily.size(); j ++ ){
                Requirement pTmpRequirement = (Requirement)leafInCurrentFamily.elementAt(j); 
                Vector testWraCoverTmp = pTmpRequirement.getTestWrapperCoveredFromDB();
                Vector coverdTest = new Vector();
                for (int k = 0; k < testWraCoverTmp.size(); k++){
                    try {
                        TestWrapper pTestWrapper = (TestWrapper) testWraCoverTmp.elementAt(k);
                        Test pTest = getTestFromWrapper(pTestWrapper);
                        Test oldTest = (Test) allTestCovredByCurrentReq.put(new Integer(pTestWrapper.getIdBDD()), pTest);
                        if (oldTest == null){
                            getExecutedStat(pTest);
                        }
                        coverdTest.add(pTest);
                        Vector coveredReq = (Vector) allReqCovredByTest.get(new Integer(pTestWrapper.getIdBDD()));
                        if (coveredReq == null){
                            coveredReq = new Vector();
                            coveredReq.add(pTmpRequirement);
                            allReqCovredByTest.put(new Integer(pTestWrapper.getIdBDD()), coveredReq);
                        } else {
                            coveredReq.add(pTmpRequirement);
                        }
                    }catch (Exception e){
                                                
                    }
                }
                if (coverdTest.size()>0){
                    allTestCovredByReq.put(new Integer(pTmpRequirement.getIdBdd()), coverdTest);
                }
            }
        } else {
            if (filtre.isFiltred(pRequirement)){
                //if ((((ReqLeaf)pRequirement).getPriorityFromModel()  | filtre) == filtre) {
                                
                Vector testWraCoverTmp = pRequirement.getTestWrapperCoveredFromDB();
                Vector coverdTest = new Vector();
                for (int k = 0; k < testWraCoverTmp.size(); k++){
                    TestWrapper pTestWrapper = (TestWrapper) testWraCoverTmp.elementAt(k);
                    try {
                        Test pTest = getTestFromWrapper(pTestWrapper);
                        Test oldTest = (Test) allTestCovredByCurrentReq.put(new Integer(pTestWrapper.getIdBDD()), pTest);
                        if (oldTest == null){
                            getExecutedStat(pTest);
                        }
                        coverdTest.add(pTest);
                        Vector coveredReq = (Vector) allReqCovredByTest.get(new Integer(pTestWrapper.getIdBDD()));
                        if (coveredReq == null){
                            coveredReq = new Vector();
                            coveredReq.add(pRequirement);
                            allReqCovredByTest.put(new Integer(pTestWrapper.getIdBDD()), coveredReq);
                        } else {
                            coveredReq.add(pRequirement);
                        }
                    }catch (Exception e){
                                                
                    }
                }
                if (coverdTest.size()>0){
                    allTestCovredByReq.put(new Integer(pRequirement.getIdBdd()), coverdTest);
                }
            }
        }
    }
        
    public Hashtable getAllTestCovredByCurrentReq(){
        return allTestCovredByCurrentReq;
    }
        
    public Hashtable getAllTestCovredByReq(){
        return allTestCovredByReq;
    }
        
    public Hashtable getEexecutedStatCovredByCurrentReq(){
        return executedStatCovredByCurrentReq;
    }
        
    public Hashtable getAllReqCovredByTest(){
        return allReqCovredByTest;
    }
        
    public Vector getLeafInCurrentFamily(){
        return leafInCurrentFamily;
    }
        
    public boolean isCovredTestIsExecuted(Test pTest){
        if (executedStatCovredByCurrentReq.get(pTest) != null){
            return true;
        } else {
            return false;
        }
    }
        
        
    private Hashtable getExecutedStat(Test pTest){
        //boolean executed = false;
        ArrayList campaignList = DataModel.getCurrentProject().getCampaignOfTest(pTest);
        Vector detailExec = new Vector();
        if (campaignList != null && campaignList.size() > 0){
            int i = 0;
            int size = campaignList.size();
            while (i < size /* && !executed */ ){
                Campaign pCampaign = (Campaign)campaignList.get(i);
                        
                if (pCampaign.containsExecutionResultInModel()){
                    //executed = true;
                                
                    ArrayList executionList = pCampaign.getExecutionListFromModel();
                    if (executionList != null && executionList.size() > 0){
                        //Echec=Echec Inconclusif=Inconclusif Succes=Succes
                        int sizeExecutionList = executionList.size();
                                                                        
                        for (int j = 0 ; j < sizeExecutionList ; j++){
                            Execution pExec = (Execution) executionList.get(j);
                            if (pExec!= null && pExec.getExecutionResultListFromModel()!=null &&  pExec.getExecutionResultListFromModel().size()>0){
                                ArrayList executionResultList =  pExec.getExecutionResultListFromModel();
                                int sizeExecutionResList =  executionResultList.size();
                                                        
                                Vector listOfExecResult = new Vector();
                                ExecutionResult lastExecutionResult = null;
                                String lastResult = ApiConstants.UNKNOWN;
                                int nbFail = 0;
                                int nbPass = 0;
                                int nbUnknow = 0;
                                int nbNotApplicable = 0;
                                int nbBlocked = 0;
                                int nbNone = 0;
                                                        
                                for (int k = 0; k < sizeExecutionResList; k++){
                                                                
                                    ExecutionResult pExecutionResult = (ExecutionResult) executionResultList.get(k);
                                    listOfExecResult.add(pExecutionResult);
                                    if (lastExecutionResult != null) {
                                        Date pDate = pExecutionResult.getExecutionDateFromModel();
                                        Date currentDate = lastExecutionResult.getExecutionDateFromModel();
                                        if (pDate.after(currentDate)){
                                            lastExecutionResult = pExecutionResult;
                                        }
                                    } else {
                                        lastExecutionResult = pExecutionResult;
                                    }
                                    String status;
                                    if (lastExecutionResult != pExecutionResult){
                                        lastResult = lastExecutionResult.getTestResultStatusFromModel(pTest);
                                        status = pExecutionResult.getTestResultStatusFromModel(pTest);
                                                                        
                                    } else {
                                        status = pExecutionResult.getTestResultStatusFromModel(pTest);
                                        lastResult = status;
                                    }
                                                                
                                    if (status.equals(ApiConstants.UNKNOWN)) {
                                        nbUnknow++;
                                    } else if (status.equals(ApiConstants.FAIL)) {
                                        nbFail++;
                                    } else if (status.equals(ApiConstants.NOTAPPLICABLE)) {
                                        nbNotApplicable++;
                                    } else if (status.equals(ApiConstants.BLOCKED)) {
                                        nbBlocked++;
                                    } else if (status.equals(ApiConstants.NONE)) {
                                        nbNone++;
                                    } else if (status.equals(ApiConstants.SUCCESS)) {
                                        nbPass++;
                                    }   
                                }// End For ExecutionResult
                                if (listOfExecResult.size()>0) {
                                    Vector data = new Vector();
                                    data.add(pCampaign);
                                    data.add(pExec);
                                    data.add(listOfExecResult);
                                    data.add(new Integer(nbPass));
                                    data.add(new Integer(nbFail));
                                    data.add(new Integer(nbUnknow));
                                    data.add(new Integer(nbNotApplicable));
                                    data.add(new Integer(nbBlocked));
                                    data.add(new Integer(nbNone));
                                    data.add(lastExecutionResult);
                                    data.add(lastResult);
                                    data.add(pTest);
                                    Vector statByCamp = (Vector) executedStatByCamp.get(pCampaign);
                                    if (statByCamp == null){
                                        statByCamp = new Vector();
                                    }
                                    statByCamp.add(data);
                                    executedStatByCamp.put(pCampaign, statByCamp);
                                                                        
                                    Vector statByExec = (Vector) executedStatByExec.get(pExec);
                                    if (statByExec == null){
                                        statByExec = new Vector();
                                    }
                                    statByExec.add(data);
                                    executedStatByExec.put(pExec, statByExec);
                                                                        
                                    Vector statByEnv = (Vector) executedStatByEnv.get(pExec.getEnvironmentFromModel());
                                    if (statByEnv == null){
                                        statByEnv = new Vector();
                                    }
                                    statByEnv.add(data);
                                    executedStatByEnv.put(pExec.getEnvironmentFromModel(), statByEnv);
                                                                        
                                    detailExec.add(data);
                                }
                            } // End If have Execresult
                        } //End For Execution
                    }
                } else {
                    // data for empty campaigns!
                    Vector data = new Vector();
                    data.add(pCampaign);
                    data.add(null);
                    data.add(null);
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(new Integer(0));
                    data.add(null);
                    data.add(null);
                    data.add(pTest);

                    Vector statByCamp = (Vector) executedStatByCamp.get(pCampaign);
                    if (statByCamp == null){
                        statByCamp = new Vector();
                    }
                    statByCamp.add(data);
                    executedStatByCamp.put(pCampaign, statByCamp);

                    detailExec.add(data);
                }
                // End if Campaign contain exec with result
                i++;
            }// End while Campaign
            if (detailExec.size() > 0) {
                executedStatCovredByCurrentReq.put(pTest, detailExec);
            }
        }// End if have campaign
        return executedStatCovredByCurrentReq;
    }
        
    private Test getTestFromWrapper(TestWrapper pTestWrapper) throws Exception {
        Test pTest;
        pTest = (Test) testwrapper2model.get(new Integer(pTestWrapper.getIdBDD()));
        if (pTest != null){
            return pTest;
        }
        pTest = DataModel.getCurrentProject().getTestFromModel(pTestWrapper.getIdBDD());
        if (pTest == null && !ReqPlugin.isGlobalProject()){
            if (ReqPlugin.getProjectRef().getIdBdd() == DataModel.getCurrentProject().getIdBdd()) {
                DataModel.reloadFromBase(true);
                pTest = DataModel.getCurrentProject().getTestFromModel(pTestWrapper.getIdBDD());
            }
            if (pTest == null) {
                throw  new Exception ("No test found in model representing " + pTestWrapper.getName()  + ", id=" + pTestWrapper.getIdBDD() );
            }
        }
        testwrapper2model.put(new Integer(pTestWrapper.getIdBDD()), pTest);
        return pTest;
    }
        
    public void clearStat(boolean clearCache){
        leafInCurrentFamily.clear();
        allTestCovredByCurrentReq.clear();
        allTestCovredByReq.clear();
        executedStatCovredByCurrentReq.clear();
        executedStatByCamp.clear();
        executedStatByEnv.clear();
        executedStatByExec.clear();
        allReqCovredByTest.clear();
                
        if (clearCache){
            testwrapper2model.clear();
        }
    }
}
