/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;
import java.awt.Color;
import java.awt.Component;
import java.util.Observer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.PopupListener;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.DataLoader;
import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqFilter;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;



public class RequirementTree extends  JTree {
        
    protected DefaultMutableTreeNode parent;
    protected DefaultTreeCellRenderer renderer  = null;
    DefaultTreeModel treeModel;
    RequirementTreeObservable pRequirementTreeObservable;
    IReqFilter pPriorityFilter ;
        
        
    public RequirementTree(){
        super();
        pRequirementTreeObservable = new RequirementTreeObservable();
        addTreeSelectionListener(pRequirementTreeObservable);
        parent = new  DefaultMutableTreeNode(new ReqFamily("Requirements_" + ReqPlugin.getProjectRef().getNameFromModel(), ""));
        //treeModel = new DefaultTreeModel(parent);

        pPriorityFilter = new ReqFilter(1007);
        treeModel = new FilterReqTreeModel(pPriorityFilter, parent, this);
        pRequirementTreeObservable.currentNode = parent;
                
        setModel(treeModel);
        setCellRenderer(getRender());
        treeModel.setRoot(parent);
        setSelectionPath(new TreePath(parent.getPath()));
        addMouseListener(new PopupListener(new PopPopTreeMenu(this), this));
    }
        
    IReqFilter getFilter(){
        return pPriorityFilter;
    }
        
    void setStatTools(StatRequirement _pStatRequirement, FiltrePanel _pFiltrePanel){
        pRequirementTreeObservable.setStatTools(_pStatRequirement, _pFiltrePanel);
    }
        
    void setPathToRoot(){
        //setSelectionPath(new TreePath(parent.getPath()));
        expandPath(new TreePath(parent.getPath()));
    }
        
    public void reload(){
        try {
            parent.removeAllChildren();
            DataLoader.loadData(this);
            treeModel.reload();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
        
    public void print (){
        //pJTree = new JTree(new DefaultTreeModel(parent));
        print(parent, 0);
    }
        
    static public void print(DefaultMutableTreeNode node, int tab){
        Requirement preq = (Requirement) node.getUserObject();
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            for(int t = 0; t< tab; t++){
                System.out.print("\t");
            }
            System.out.println("Reaqleaf : " + preq );
            return;
        } else {
            int i = 0;
            for(int t = 0; t< tab; t++){
                System.out.print("\t");
            }
            System.out.println("Reqfamily : " + preq +"["+ ((ReqFamily)preq).getFistLeaf()+"]");
            while (i < nbChild){
                print((DefaultMutableTreeNode)node.getChildAt(i), tab+1);
                i++;
            }
        }
        return ;
    }
    /*public void refresh(){
      treeModel.reload();
      }*/
        
    public void refreshCurrent(){
        DefaultMutableTreeNode node = pRequirementTreeObservable.currentNode;
        if (node != null){
            treeModel.reload(node);
            pRequirementTreeObservable.refresh();
        }
    }
    public void refreshNode(DefaultMutableTreeNode node){
        if (node != null){
            treeModel.reload(node);
        }
    }
        
    public JTree getCopy(boolean reload){
        JTree pJTree = null;
        if (reload){
            reload();
        }
        //pJTree = new JTree(new DefaultTreeModel(parent));
        pJTree = new JTree(new DefaultTreeModel(getCopy(parent)));
        pJTree.setCellRenderer(getRender());
        return pJTree;
    }
        
    public void copyIn(JTree pJTree, boolean reload){
        if (reload){
            reload();
        }
        TreeModel pTreeModel = pJTree.getModel();
        DefaultMutableTreeNode pParent = (DefaultMutableTreeNode) pTreeModel.getRoot();
        pParent.removeAllChildren();
        int nbChild = parent.getChildCount();
        int i = 0;
        while (i < nbChild){
            DefaultMutableTreeNode child_copy = getCopy((DefaultMutableTreeNode)parent.getChildAt(i));
            pParent.insert(child_copy, i);
            i++;
        }
    }
        
    private DefaultMutableTreeNode getCopy(DefaultMutableTreeNode node){
        DefaultMutableTreeNode copy_node = new DefaultMutableTreeNode(node.getUserObject());
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            return copy_node;
        } else {
            int i = 0;
            while (i < nbChild){
                DefaultMutableTreeNode child_copy = getCopy((DefaultMutableTreeNode)node.getChildAt(i));
                copy_node.insert(child_copy, i);
                i++;
            }
        }
                
        return copy_node;
    }
        
    public void addRequirementToCurrent(Requirement req) throws Exception {
        Requirement pReq = (Requirement)pRequirementTreeObservable.currentNode.getUserObject();
        if (pReq instanceof ReqFamily){
            req.setParent(pReq);
            DefaultMutableTreeNode pNode = new  DefaultMutableTreeNode(req);
            insertNode(pRequirementTreeObservable.currentNode, pNode);
            //currentNode.add(pNode);
            //scrollPathToVisible(new TreePath(pNode.getPath()));
            setSelectionPath(new TreePath(pNode.getPath()));
        } else {
            throw new Exception("[RequirementTree->addRequirementToCurrent] no requiment family found");
        }
    }
    public void addRequirementToReqNode(Requirement req, Requirement reqFamily, boolean updatePath) throws Exception {
        if (reqFamily instanceof ReqFamily){
            req.setParent(reqFamily);
            DefaultMutableTreeNode _parent = search(parent, reqFamily);
            DefaultMutableTreeNode pNode =  new  DefaultMutableTreeNode(req);
            insertNode(_parent, pNode);
            if (updatePath){
                setSelectionPath(new TreePath(pNode.getPath()));
            }
        }else {
            throw new Exception("[RequirementTree->addRequirementToReqNode] no requiment family found");
        }
    }
        
    public void addRequirementToReqNodeInTree(Requirement pReq ,DefaultMutableTreeNode _parent, boolean updatePath){    
        DefaultMutableTreeNode pNode =  new  DefaultMutableTreeNode(pReq);
        insertNode(_parent, pNode);
        if (pReq instanceof ReqFamily){
            ReqFamily pReq2 = (ReqFamily) pReq;
            Vector reqFams = pReq2.getFirstFamily();
            int size = reqFams.size();
            for (int i = 0 ; i < size ; i++){
                Requirement pReq2Add = (Requirement) reqFams.elementAt(i);
                addRequirementToReqNodeInTree(pReq2Add , pNode, false); 
            }
            Vector reqLeafs = pReq2.getFistLeaf();
            size = reqLeafs.size();
            for (int i = 0 ; i < size ; i++){
                Requirement pReq2Add = (Requirement) reqLeafs.elementAt(i);
                addRequirementToReqNodeInTree(pReq2Add , pNode, false); 
            }
        }
        if (updatePath){
            setSelectionPath(new TreePath(pNode.getPath()));
        }
        
    }
        
        
    void insertNode(DefaultMutableTreeNode parent, DefaultMutableTreeNode childNode){
        //Desactiver le filtre
        boolean reactive = false;
        if (pPriorityFilter.isActived()){
            pPriorityFilter.setActived(false);
            reactive = true;
            ((FilterReqTreeModel)treeModel).setFiltered(false);
        }
        treeModel.insertNodeInto(childNode, parent, parent.getChildCount());
        if (reactive){
            pPriorityFilter.setActived(true);
            ((FilterReqTreeModel)treeModel).setFiltered(true);
        }
    }
        
        
        
    public void deleteCurrentRequirement(){
        if (pRequirementTreeObservable.currentNode.equals(parent)){
            return;
                        
        }
        if (getCurrentNode() == null || getCurrentNode().equals(parent)){
            return;
        }
        DefaultMutableTreeNode p_parent;
                
        Requirement pReq = (Requirement)pRequirementTreeObservable.currentNode.getUserObject();
        p_parent = (DefaultMutableTreeNode) pRequirementTreeObservable.currentNode.getParent();
        try {
            //if (pReq instanceof ReqFamily){
            pReq.deleteInDBAndModel();
            //}
            treeModel.removeNodeFromParent(pRequirementTreeObservable.currentNode);
            setSelectionPath(new TreePath(p_parent.getPath()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
        
    public void deleteRequirementInModelAndDB(Requirement pReq){
        DefaultMutableTreeNode node2del = findRequirementFromParent(pReq);
                
        if (node2del == null || node2del.equals(parent)){
            return;
        }
        DefaultMutableTreeNode p_parent;
                
        p_parent = (DefaultMutableTreeNode) node2del.getParent();
        try {
                        
            pReq.deleteInDBAndModel();
            treeModel.removeNodeFromParent(pRequirementTreeObservable.currentNode);
            setSelectionPath(new TreePath(p_parent.getPath()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }
        
        
    DefaultMutableTreeNode getCurrentNode(){
        return pRequirementTreeObservable.currentNode;
    }
        
    DefaultMutableTreeNode getParentNode(){
        return parent;
    }
        
    public Requirement getCurrentRequirement(){
        if (pRequirementTreeObservable.currentNode != null){
            return (Requirement) pRequirementTreeObservable.currentNode.getUserObject();
        }
        return null;
    }
        
    public Requirement getRootRequirement(){
        if (parent != null) {
            return (Requirement)parent.getUserObject();
        }
        return null;
    }
        
        
    DefaultMutableTreeNode search(DefaultMutableTreeNode pTempNode, Requirement pReq) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        DefaultMutableTreeNode retNode = null;
        if (pTempReq.equals(pReq)) {
            return pTempNode;
        } else {
            int nbChild =  pTempNode.getChildCount();
            boolean trouve = false;
            int i = 0;
            while (i< nbChild && !trouve){
                retNode = search((DefaultMutableTreeNode)pTempNode.getChildAt(i), pReq);
                if (retNode != null){
                    trouve = true;
                }
                i++;
            }
            return retNode;
        }
    }
        
    DefaultMutableTreeNode searchWrapper(DefaultMutableTreeNode pTempNode, ReqWrapper pReq) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        DefaultMutableTreeNode retNode = null;
        if (pTempReq.getIdBdd() == pReq.getIdBDD()) {
            return pTempNode;
        } else {
            int nbChild =  pTempNode.getChildCount();
            boolean trouve = false;
            int i = 0;
            while (i< nbChild && !trouve){
                retNode = searchWrapper((DefaultMutableTreeNode)pTempNode.getChildAt(i), pReq);
                if (retNode != null){
                    trouve = true;
                }
                i++;
            }
            return retNode;
        }
    }
        
    DefaultMutableTreeNode searchRegx(DefaultMutableTreeNode pTempNode, String reqex, boolean id) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        DefaultMutableTreeNode retNode = null;
                
        if (id){
            String idReq = (pTempReq.getIdBdd()+"").trim();
            if (idReq.equals(reqex)) {
                return pTempNode;
            }
        } else {
            if (pTempReq.getNameFromModel().matches(reqex)) {
                return pTempNode;
            }
        }
                
        int nbChild =  pTempNode.getChildCount();
        boolean trouve = false;
        int i = 0;
        while (i< nbChild && !trouve){
            retNode = searchRegx((DefaultMutableTreeNode)pTempNode.getChildAt(i), reqex, id);
            if (retNode != null){
                trouve = true;
            }
            i++;
        }
        return retNode;

        /*String idReq = (pTempReq.getIdBdd()+"").trim();
          if (idReq.equals(reqex) || pTempReq.getNameFromModel().matches(reqex)){
          return pTempNode;
          }
          else {
          int nbChild =  pTempNode.getChildCount();
          boolean trouve = false;
          int i = 0;
          while (i< nbChild && !trouve){
          retNode = searchRegx((DefaultMutableTreeNode)pTempNode.getChildAt(i), reqex, id);
          if (retNode != null){
          trouve = true;
          }
          i++;
          }
          return retNode;
          }*/
    }
        
    DefaultMutableTreeNode searchString(DefaultMutableTreeNode pTempNode, String name) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        DefaultMutableTreeNode retNode = null;
        if (pTempReq.getNameFromModel().equals(name)) {
            return pTempNode;
        } else {
            int nbChild =  pTempNode.getChildCount();
            boolean trouve = false;
            int i = 0;
            while (i< nbChild && !trouve){
                retNode = searchString((DefaultMutableTreeNode)pTempNode.getChildAt(i), name);
                if (retNode != null){
                    trouve = true;
                }
                i++;
            }
            return retNode;
        }
    }
    public DefaultMutableTreeNode findRequirementFromCurrent(Requirement pReq) {
        return search(pRequirementTreeObservable.currentNode, pReq);
    }
        
    public DefaultMutableTreeNode findRequirementFromParent(Requirement pReq) {
        return search(parent, pReq);
    }
        
    public DefaultMutableTreeNode findRequirementFromParent(ReqWrapper pReq) {
        return searchWrapper(parent, pReq);
    }
        
    public DefaultMutableTreeNode findRequirementFromCurrent(String regex, boolean id) {
        DefaultMutableTreeNode pReqNode = searchRegx(pRequirementTreeObservable.currentNode, regex, id);
        if (pReqNode != null){
            setSelectionPath(new TreePath(pReqNode.getPath()));
        } else {
            //Pas trouve 
            //TODO
        }
        return pReqNode;
    }
        
    /*public DefaultMutableTreeNode findRequirementFromParent(String regex) {
      DefaultMutableTreeNode pReqNode = searchRegx(parent, regex);
      if (pReqNode != null){
      setSelectionPath(new TreePath(pReqNode.getPath()));
      } else {
      //Pas trouve 
      //TODO
      }
      return pReqNode;
      }*/
        
    public DefaultMutableTreeNode findRequirementByNameFromParent(String name) {
        DefaultMutableTreeNode pReqNode = searchString(parent, name);
        if (pReqNode != null){
            setSelectionPath(new TreePath(pReqNode.getPath()));
        } else {
            //Pas trouve 
            //TODO
        }
        return pReqNode;
    }
        
    DefaultTreeCellRenderer getRender() {
        if (renderer == null){
            renderer = new      myReqRenderer();
        }
        return renderer;
    }
        
    void addObserver(Observer o){
        try {
            pRequirementTreeObservable.addObserver(o);          
        }catch (Exception e){
                        
        }
    }
        
    class myReqRenderer extends DefaultTreeCellRenderer {
        //ImageIcon ReqFamIcon = new javax.swing.ImageIcon(getClass().getResource("/salomeTMF_plug/requirements/resources/images/reqFam.png"));
        //ImageIcon ReqLeafIncon = new javax.swing.ImageIcon(getClass().getResource("/salomeTMF_plug/requirements/resources/images/req.png"));
        ImageIcon ReqFamIcon = createImageIcon("/salomeTMF_plug/requirements/resources/images/reqFam.png");
        ImageIcon ReqLeafIncon =createImageIcon("/salomeTMF_plug/requirements/resources/images/req.png");
                
        myReqRenderer(){
            super();
        }
                
        /**
         * @covers SFD_ForgeORTF_EXG_SEL_000020 - 2.4.3
         * Jira : FORTF - 40
         */
        public Component getTreeCellRendererComponent(
                                                      JTree tree,
                                                      Object value,
                                                      boolean sel,
                                                      boolean expanded,
                                                      boolean leaf,
                                                      int row,
                                                      boolean hasFocus) {
                        
            //System.out.println("Get renderer for " + value);
            super.getTreeCellRendererComponent(
                                               tree, value, sel,
                                               expanded, leaf, row,
                                               hasFocus);
                        
            if (value instanceof DefaultMutableTreeNode){
                Requirement req = (Requirement) ((DefaultMutableTreeNode)value).getUserObject();
                if (req.isFamilyReq()){
                    setIcon(ReqFamIcon);
                }else {
                    setIcon(ReqLeafIncon);
                    ReqLeaf pReqLeaf = (ReqLeaf)req;
                                        
                    //20100113 - Debut modification Forge ORTF
                    if (pReqLeaf.getPriorityFromModel() == ReqFilter.P_HIGHT){
                        if (sel){
                            setForeground(Color.white);
                        }
                        else{
                            setForeground(Color.red);
                        }
                    } else if (pReqLeaf.getPriorityFromModel() == ReqFilter.P_MEDIUM){
                        if (sel){
                            setForeground(Color.white);
                        }
                        else{
                            setForeground(Color.blue);
                        }
                    } else if (pReqLeaf.getPriorityFromModel() == ReqFilter.P_LOW){
                        if (sel){
                            setForeground(Color.white);
                        }
                        else{
                            setForeground(Color.black);
                        }
                    } else {
                        if (sel){
                            setForeground(Color.white);
                        }
                        else{
                            setForeground(Color.gray);
                        }
                    }
                    //20100113 - Fin modification Forge ORTF

                }
                //setName(req.getNameFromModel());
            }
            setEnabled(true);
            return this;
        }
                
                
                
    }
    protected static ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = RequirementTree.class.getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
        
}
