/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;

import salomeTMF_plug.requirements.data.ReqFilter;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.HistoryWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110 Jira : FORTF - 38
 */
public class HistoryPanel extends JPanel implements ListSelectionListener,
                                                    ActionListener {
    JTable historyTable;
    TableSorter sorter;
    SimpleTableModel model;
    Requirement currentReq;

    JButton viewButton;
    HistoryWrapper[] tabHisto;

    Hashtable<Integer, String> itemComplex;
    Hashtable<Integer, String> itemPriority;
    Hashtable<Integer, String> itemCat;
    Hashtable<Integer, String> itemState;
    // 20100118 - D�but modification Forge ORTF
    Hashtable<Integer, String> itemValidBy;
    Hashtable<Integer, String> itemCritical;
    Hashtable<Integer, String> itemUnderstanding;
    Hashtable<Integer, String> itemOwner;

    IDataBase db;
    ISQLEngine pISQLEngine;

    final String REQUIREMENTS_CFG_FILE = "/plugins/requirements/cfg/Requirements.properties";

    public HistoryPanel() {
        super(new BorderLayout());
        initData();
        initComponent();
    }

    void initData() {
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
        Properties prop = new Properties();
        try {
            URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            URL urlBase = new URL(url_txt);

            java.net.URL url_cfg = new java.net.URL(urlBase
                                                    + REQUIREMENTS_CFG_FILE);
            try {
                prop = Util.getPropertiesFile(url_cfg);
            } catch (Exception e) {
                prop = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Iterator it = prop.keySet().iterator();
        itemPriority = new Hashtable<Integer, String>();
        itemCat = new Hashtable<Integer, String>();
        itemComplex = new Hashtable<Integer, String>();
        itemState = new Hashtable<Integer, String>();
        itemValidBy = new Hashtable<Integer, String>();
        itemCritical = new Hashtable<Integer, String>();
        itemUnderstanding = new Hashtable<Integer, String>();
        itemOwner = new Hashtable<Integer, String>();
        String sql = "";
        while (it.hasNext()) {
            String property = (String) it.next();
            if (property.equals("Categorie")) {
                String[] categories = prop.getProperty(property).split(",");
                for (int i = 0; i < categories.length; i++) {
                    sql = "SELECT * FROM REQ_CATEGORY where name='" + categories[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_CATEGORY (name,user_entry) values ('"
                                + categories[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemCat.put(id, categories[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Priorite")) {
                String[] priorities = prop.getProperty(property).split(",");
                for (int i = 0; i < priorities.length; i++) {
                    sql = "SELECT * FROM REQ_PRIORITY where name='" + priorities[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_PRIORITY (name,user_entry) values ('"
                                + priorities[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemPriority.put(id, priorities[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Complexite")) {
                String[] complexes = prop.getProperty(property).split(",");
                for (int i = 0; i < complexes.length; i++) {
                    sql = "SELECT * FROM REQ_COMPLEXE where name='" + complexes[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_COMPLEXE (name,user_entry) values ('"
                                + complexes[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemComplex.put(id, complexes[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Statut")) {
                String[] states = prop.getProperty(property).split(",");
                for (int i = 0; i < states.length; i++) {
                    sql = "SELECT * FROM REQ_STATE where name='" + states[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_STATE (name,user_entry) values ('"
                                + states[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemState.put(id, states[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Valide")) {
                String[] validBys = prop.getProperty(property).split(",");
                for (int i = 0; i < validBys.length; i++) {
                    sql = "SELECT * FROM REQ_VALID_BY where name='" + validBys[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_VALID_BY (name,user_entry) values ('"
                                + validBys[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemValidBy.put(id, validBys[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Criticite")) {
                String[] criticals = prop.getProperty(property).split(",");
                for (int i = 0; i < criticals.length; i++) {
                    sql = "SELECT * FROM REQ_CRITICAL where name='" + criticals[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_CRITICAL (name,user_entry) values ('"
                                + criticals[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemCritical.put(id, criticals[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Comprehension")) {
                String[] understandings = prop.getProperty(property).split(",");
                for (int i = 0; i < understandings.length; i++) {
                    sql = "SELECT * FROM REQ_UNDERSTANDING where name='"
                        + understandings[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_UNDERSTANDING (name,user_entry) values ('"
                                + understandings[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemUnderstanding.put(id, understandings[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Proprietaire")) {
                String[] owners = prop.getProperty(property).split(",");
                for (int i = 0; i < owners.length; i++) {
                    sql = "SELECT * FROM REQ_OWNER where name='" + owners[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_OWNER (name,user_entry) values ('"
                                + owners[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        } else {
                            Integer id = result.getInt(1);
                            itemOwner.put(id, owners[i]);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        sql = "SELECT id,name FROM REQ_CATEGORY where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String category = result.getString(2);
                itemCat.put(id, category);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_PRIORITY where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String priority = result.getString(2);
                itemPriority.put(id, priority);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_COMPLEXE where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String complexe = result.getString(2);
                itemComplex.put(id, complexe);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_STATE where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String state = result.getString(2);
                itemState.put(id, state);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_VALID_BY where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String validBy = result.getString(2);
                itemValidBy.put(id, validBy);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_CRITICAL where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String critical = result.getString(2);
                itemCritical.put(id, critical);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_UNDERSTANDING where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String understanding = result.getString(2);
                itemUnderstanding.put(id, understanding);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        sql = "SELECT id,name FROM REQ_OWNER where user_entry=1;";
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                Integer id = result.getInt(1);
                String owner = result.getString(2);
                itemOwner.put(id, owner);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        // while (it.hasNext()){
        // String property = (String) it.next();
        // if (property.startsWith("Priorite")){
        // String priorite = prop.getProperty(property);
        // sql = "SELECT * FROM priority where name='"+priorite+"';";
        // try {
        // PreparedStatement prep = db.prepareStatement(sql);
        // ResultSet result = prep.executeQuery(sql);
        // if (!result.next()){
        // sql =
        // "INSERT INTO priority (name,user_entry) values ('"+priorite+"',0);";
        // prep = db.prepareStatement(sql);
        // prep.executeUpdate();
        // }
        // else{
        // Integer id_priority = new Integer(result.getInt(3));
        // itemPriority.put(id_priority, priorite);
        // }
        // } catch (Exception e1) {
        // e1.printStackTrace();
        // }
        // }
        // else if (property.startsWith("Priorite")){
        // String priorite = prop.getProperty(property);
        // sql = "SELECT * FROM priority where name='"+priorite+"';";
        // try {
        // PreparedStatement prep = db.prepareStatement(sql);
        // ResultSet result = prep.executeQuery(sql);
        // if (!result.next()){
        // sql =
        // "INSERT INTO priority (name,user_entry) values ('"+priorite+"',0);";
        // prep = db.prepareStatement(sql);
        // prep.executeUpdate();
        // }
        // else{
        // Integer id_priority = new Integer(result.getInt(3));
        // itemPriority.put(id_priority, priorite);
        // }
        // } catch (Exception e1) {
        // e1.printStackTrace();
        // }
        // }
        //
        // }
        // sql = "SELECT name,id FROM priority where user_entry=1;";
        // try {
        // PreparedStatement prep = db.prepareStatement(sql);
        // ResultSet result = prep.executeQuery(sql);
        // while (result.next()){
        // String priorite = result.getString(1);
        // Integer id_priority = result.getInt(2);
        // itemPriority.put(id_priority, priorite);
        // }
        // } catch (Exception e1) {
        // e1.printStackTrace();
        // }

        // itemPriority.put(ReqFilter.P_HIGHT,
        // Language.getInstance().getText("Filtre_Exigence_Haute"));
        // itemPriority.put(ReqFilter.P_MEDIUM,
        // Language.getInstance().getText("Filtre_Exigence_Moyenne"));
        // itemPriority.put(ReqFilter.P_LOW,
        // Language.getInstance().getText("Filtre_Exigence_Faible"));
        // itemPriority.put(ReqFilter.P_NONE,
        // Language.getInstance().getText("Filtre_Exigence_Option"));
        //
        //
        // itemCat = new Hashtable<Integer,String>();
        // itemCat.put(0, Language.getInstance().getText("Cat_Fonctionnel"));
        // itemCat.put(1,
        // Language.getInstance().getText("Cat_Interoperabilite"));
        // itemCat.put(2, Language.getInstance().getText("Cat_Charge"));
        // itemCat.put(3, Language.getInstance().getText("Cat_Performance"));
        // itemCat.put(4, Language.getInstance().getText("Cat_Disponibilite"));
        // itemCat.put(5, Language.getInstance().getText("Cat_Securite"));
        // itemCat.put(6, Language.getInstance().getText("Cat_Exploitabilite"));
        // itemCat.put(7, Language.getInstance().getText("Cat_Autre"));
        //
        // itemState = new Hashtable<Integer,String>();
        // itemState.put(0, Language.getInstance().getText("State_A_Analyser"));
        // itemState.put(1, Language.getInstance().getText("State_Analysee"));
        // itemState.put(2, Language.getInstance().getText("State_Approuvee"));
        // itemState.put(3, Language.getInstance().getText("State_Verifiee"));
        // itemState.put(4, Language.getInstance().getText("State_Finalisee"));
        // itemState.put(5, Language.getInstance().getText("State_Reportee"));
        // itemState.put(6, Language.getInstance().getText("State_Abandonnee"));
        //
        // itemComplex = new Hashtable<Integer,String>();
        // itemComplex.put(ReqFilter.C0,
        // Language.getInstance().getText("Com_C0"));
        // itemComplex.put(ReqFilter.C1,
        // Language.getInstance().getText("Com_C1"));
        // itemComplex.put(ReqFilter.C2,
        // Language.getInstance().getText("Com_C2"));
        // itemComplex.put(ReqFilter.C3,
        // Language.getInstance().getText("Com_C3"));
    }

    // 20100118 - Fin modification Forge ORTF

    void initComponent() {
        int t_y = 768;
        int t_x = 1024;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_y = r.height;
            ;
        } catch (Exception E) {

        }
        model = new SimpleTableModel();
        model.addColumn(Language.getInstance().getText("Date"));
        model.addColumn(Language.getInstance().getText("Action"));
        model.addColumn(Language.getInstance().getText("Nouvelle_valeur"));
        sorter = new TableSorter(model);

        historyTable = new JTable(sorter);

        historyTable.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent me) {
                    if (me.getButton() == MouseEvent.BUTTON1) {
                        if (me.getClickCount() == 2) {
                            viewDescriptionPerformed();
                        }
                    }
                }

                public void mouseEntered(MouseEvent me) {
                }

                public void mouseExited(MouseEvent me) {
                }

                public void mousePressed(MouseEvent me) {
                }

                public void mouseReleased(MouseEvent me) {
                }
            });

        ListSelectionModel rowSM = historyTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        setColumnSize(historyTable);
        sorter.setTableHeader(historyTable.getTableHeader());

        historyTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane tablePane = new JScrollPane(historyTable,
                                                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                                                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
        setBorder(BorderFactory.createTitledBorder(BorderFactory
                                                   .createLineBorder(Color.BLACK), Language.getInstance().getText(
                                                                                                                  "Historique")));
        add(tablePane, BorderLayout.CENTER);

        JPanel panelButton = new JPanel(new FlowLayout());
        viewButton = new JButton(Language.getInstance().getText("Visualiser")
                                 + " " + Language.getInstance().getText("Test"));
        viewButton.setEnabled(false);
        viewButton.addActionListener(this);
        panelButton.add(viewButton);
        add(panelButton, BorderLayout.SOUTH);

        setMaximumSize(new Dimension(t_x / 3 * 2, t_y / 5)); // MM

    }

    public void updateData(Requirement pReq, boolean force) {
        if (pReq == null) {
            return;
        }
        if (currentReq != null && !force) {
            if (currentReq.getIdBdd() == pReq.getIdBdd()) {
                return;
            }
        }
        boolean isSorting = sorter.isSorting();
        int statusSort_col0 = -1;
        int statusSort_col1 = -1;
        int statusSort_col2 = -1;
        if (isSorting) {
            statusSort_col0 = sorter.getSortingStatus(0);
            statusSort_col1 = sorter.getSortingStatus(1);
            statusSort_col2 = sorter.getSortingStatus(2);
        }
        model.clearTable();
        // System.out.println("UpDateHistory for  " + pReq.getLongName());

        try {
            tabHisto = pReq.getHistory();
            for (int i = 0; i < tabHisto.length; i++) {
                HistoryWrapper pHistoryWrapper = tabHisto[i];
                Date pDate = pHistoryWrapper.getDate();
                int code = pHistoryWrapper.getCode();
                String strcode = Requirement.getStringCodeValue(code);
                String valeur = pHistoryWrapper.getValeur();
                if (Requirement.isTestIdValeur(code)) {
                    Test pTest = (Test) DataModel.getCurrentProject()
                        .getTestFromModel(new Integer(valeur).intValue());
                    if (pTest != null) {
                        valeur = pTest.getNameFromModel();
                    }
                }
                // 20100118 - D�but modification Forge ORTF
                else if (Requirement.isStateValeur(code)) {
                    int state = new Integer(valeur).intValue();
                    valeur = itemState.get(state);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_STATE where id=" + state
                            + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemState.put(state, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isPriorityValeur(code)) {
                    int priority = new Integer(valeur).intValue();
                    valeur = itemPriority.get(priority);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_PRIORITY where id="
                            + priority + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemPriority.put(priority, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isComplexityValeur(code)) {
                    int comp = new Integer(valeur).intValue();
                    valeur = itemComplex.get(comp);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_COMPLEXE where id="
                            + comp + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemComplex.put(comp, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isCategoryValeur(code)) {
                    int cat = new Integer(valeur).intValue();
                    valeur = itemCat.get(cat);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_CATEGORY where id="
                            + cat + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemCat.put(cat, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isValidByValeur(code)) {
                    int validBy = new Integer(valeur).intValue();
                    valeur = itemValidBy.get(validBy);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_VALID_BY where id="
                            + validBy + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemValidBy.put(validBy, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isCriticalValeur(code)) {
                    int critical = new Integer(valeur).intValue();
                    valeur = itemCritical.get(critical);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_CRITICAL where id="
                            + critical + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemCritical.put(critical, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isUnderstandingValeur(code)) {
                    int understanding = new Integer(valeur).intValue();
                    valeur = itemUnderstanding.get(understanding);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_UNDERSTANDING where id="
                            + understanding + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemUnderstanding.put(understanding, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                } else if (Requirement.isOwnerValeur(code)) {
                    int owner = new Integer(valeur).intValue();
                    valeur = itemOwner.get(owner);
                    if (valeur == null) {
                        String sql = "SELECT name FROM REQ_OWNER where id=" + owner
                            + ";";
                        try {
                            PreparedStatement prep = db.prepareStatement(sql);
                            ResultSet result = prep.executeQuery(sql);
                            if (result.next()) {
                                valeur = result.getString(1);
                                itemOwner.put(owner, valeur);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
                // 20100118 - Fin modification Forge ORTF

                Vector<String> data = new Vector<String>();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
                data.add(sdf.format(pDate));
                data.add(strcode);
                data.add(valeur);
                model.insertRow(i, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        currentReq = pReq;
        setColumnSize(historyTable);
        if (isSorting) {
            sorter.setSortingStatus(0, statusSort_col0);
            sorter.setSortingStatus(1, statusSort_col1);
            sorter.setSortingStatus(2, statusSort_col2);
        }
    }

    /**
     * redefine size of table columns
     */
    public void setColumnSize(JTable table) {
        FontMetrics fm = table.getFontMetrics(table.getFont());
        for (int i = 0; i < table.getColumnCount(); i++) {
            int max = 0;
            for (int j = 0; j < table.getRowCount(); j++) {
                if (table.getValueAt(j, i) != null) {
                    int taille = fm
                        .stringWidth((String) table.getValueAt(j, i));
                    if (taille > max)
                        max = taille;
                }
            }
            String nom = (String) table.getColumnModel().getColumn(i)
                .getIdentifier();
            int taille = fm.stringWidth(nom);
            if (taille > max) {
                max = taille;
            }
            table.getColumnModel().getColumn(i).setPreferredWidth(max + 10);
        }
    }

    /********************** ListSelectionListener ***************************/
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(historyTable.getSelectionModel())) {
            assigendTableValueChanged(e);
        }
    }

    void assigendTableValueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting())
            return;

        int selectedRow = historyTable.getSelectedRow();
        if (selectedRow != -1) {
            /* Mono Selection */
            int modeIndex = sorter.modelIndex(selectedRow);
            if (tabHisto != null) {
                HistoryWrapper pHistoryWrapper = tabHisto[modeIndex];
                int code = pHistoryWrapper.getCode();
                String valeur = pHistoryWrapper.getValeur();
                if (Requirement.isTestIdValeur(code)) {
                    Test pTest = (Test) DataModel.getCurrentProject()
                        .getTestFromModel(new Integer(valeur).intValue());
                    if (pTest != null) {
                        viewButton.setEnabled(true);
                    }
                } else {
                    viewButton.setEnabled(false);
                }
            } else {
                viewButton.setEnabled(false);
            }

            // viewButton.setEnabled(true);
        } else {
            /* Pas de Selection */
            viewButton.setEnabled(false);
        }

    }

    /********************************** Implements ActionListener *********************************/

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(viewButton)) {
            viewTestPerformed();
        }
    }

    void doubleClicPerformed() {
        viewDescriptionPerformed();
    }

    void viewTestPerformed() {
        int selectedRow = historyTable.getSelectedRow();
        if (selectedRow != -1) {
            int modeIndex = sorter.modelIndex(selectedRow);
            if (tabHisto != null) {
                HistoryWrapper pHistoryWrapper = tabHisto[modeIndex];
                int code = pHistoryWrapper.getCode();
                String valeur = pHistoryWrapper.getValeur();
                if (Requirement.isTestIdValeur(code)) {
                    Test pTest = (Test) DataModel.getCurrentProject()
                        .getTestFromModel(new Integer(valeur).intValue());
                    if (pTest != null) {
                        DataModel.view(pTest);
                    }
                }
            }
        }
    }

    void viewDescriptionPerformed() {
        int selectedRow = historyTable.getSelectedRow();
        if (selectedRow != -1) {
            int modeIndex = sorter.modelIndex(selectedRow);
            if (tabHisto != null) {
                HistoryWrapper pHistoryWrapper = tabHisto[modeIndex];
                int code = pHistoryWrapper.getCode();
                if (Requirement.isDescriptionValeur(code)) {
                    String valeur = pHistoryWrapper.getValeur();
                    System.out.println("Valeur = " + valeur);
                    HistoryDescViewDialog pHistoryDescViewDialog = new HistoryDescViewDialog(
                                                                                             valeur);
                }
            }
        }
    }
}
