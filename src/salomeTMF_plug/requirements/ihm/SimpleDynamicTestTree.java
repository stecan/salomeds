/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;


import java.awt.GridLayout;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;
import org.objectweb.salome_tmf.ihm.models.TreeRenderer;


public class SimpleDynamicTestTree extends JPanel implements ApiConstants, DataConstants {
    
    /**
     * La racine de l'arbre
     */
    protected DefaultMutableTreeNode rootNode;
    
    /**
     * Mod?le de l'arbre
     */
    protected TestTreeModel treeModel;
    
    /**
     * L'arbre
     */
    protected JTree tree;
    
    /**
     *
     */
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    
    /**
     * Le noeud selectionn?
     */
    private DefaultMutableTreeNode selectedNode;
    
    /**
     * Type de l'arbre "campagne" ou "suite"
     */
    private int treeType;
   
    
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    /**
     * Le constructeur de l'arbre
     */
    public SimpleDynamicTestTree(String rootName, int type) {
        super(new GridLayout(1,0));
        
        // Le support graphique sur lequel est fond? l'arbre
        TreeRenderer renderer = new TreeRenderer();
        
        treeType = type;
        
        // la racine de l'arbre de test
        rootNode = new DefaultMutableTreeNode(rootName);
        
        tree = new JTree();
        treeModel = new TestTreeModel(rootNode, tree, null);
        tree.setModel(treeModel);
        
        //tree.setEditable(true);
        tree.setCellRenderer(renderer);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.setShowsRootHandles(true);
           
        
        JScrollPane scrollPane = new JScrollPane(tree,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);
    } // Fin du constructeur DynamicTree/0
    
    public SimpleDynamicTestTree(DefaultMutableTreeNode _rootNode, int type) {
        super(new GridLayout(1,0));
        
        // Le support graphique sur lequel est fond? l'arbre
        TreeRenderer renderer = new TreeRenderer();
        
        treeType = type;
        
        // la racine de l'arbre de test
        rootNode = _rootNode;
        
        tree = new JTree();
        treeModel = new TestTreeModel(rootNode, tree, null);
        tree.setModel(treeModel);
 
        //tree.setEditable(true);
        tree.setCellRenderer(renderer);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.setShowsRootHandles(true);
           
        
        JScrollPane scrollPane = new JScrollPane(tree,ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);
    } // Fin du constructeur DynamicTree/0
    
    /**
     * Supprime tout de l'arbre, sauf la racine
     */
    public void clear() {
        rootNode.removeAllChildren();
        treeModel.reload();
        //treeModel.reload();
    } // Fin de la m?thode clear/0
    
   
    
    public void removeNode(DefaultMutableTreeNode node) {
        treeModel.removeNodeFromParent(node);
    }
    /**
     * Cherche de mani?re r?cursive et supprime le noeud correspond ? celui
     * pass? en param?tre. A l'appel le premier param?tre est le noeud de
     * d?but de la recherche.
     * @param currentNode le noeud courant
     * @param node le noeud ? retirer.
     */
    public void searchAndRemove(DefaultMutableTreeNode currentNode, DefaultMutableTreeNode node) {
        if (currentNode.equals(node)) {
            MutableTreeNode parent = (MutableTreeNode)(currentNode.getParent());
            if (parent != null) {
                treeModel.removeNodeFromParent(currentNode);
            }
        } else {
            for (int i=0; i < currentNode.getChildCount(); i ++) {
                searchAndRemove((DefaultMutableTreeNode)currentNode.getChildAt(i), node);
            }
        }
    } // Fin de la m?thode searchAndRemove/2
    
    /**
     * M?thode d'ajout d'un noeud dans l'arbre
     * @param child l'objet ajout? dans l'arbre
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(Object child) {
        DefaultMutableTreeNode parentNode = null;
        TreePath parentPath = tree.getSelectionPath();
        
        if (parentPath == null) {
            parentNode = rootNode;
        } else {
            parentNode = (DefaultMutableTreeNode)(parentPath.getLastPathComponent());
        }
        return addObject(parentNode, child, true);
    } // Fin de la classe addObject/1
    
    /**
     * M?thode d'ajout d'un noeud invisible sous un parent
     * @param parent le parent
     * @param child le noeud ajout?
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
                                            Object child) {
        return addObject(parent, child, false);
    } // Fin de la classe addObject/2
    
    /**
     * M?thode d'ajout d'un noeud dans l'arbre sous le parent.
     * @param parent le parent
     * @param child le noeud ? ajouter
     * @param shouldBeVisible visible ou non
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child, boolean shouldBeVisible) {
        //              System.out.println("Ajout de : " + child + " ? " + parent);
        DefaultMutableTreeNode childNode =      new DefaultMutableTreeNode(child);
        if (parent == null) {
            parent = rootNode;
        }
        // Insertion du noeud
        treeModel.insertNodeInto(childNode, parent, parent.getChildCount());
        
        // on s'assure que le noeud est visible
        if (shouldBeVisible) {
            tree.scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        
        return childNode;
    } // Fin de la classe addObject/3
    
    
    /**
     * M?thode qui retourne le noeud de l'arbre qui est s?lectionn?
     * @return un noeud de l'arbre
     */
    public DefaultMutableTreeNode getSelectedNode() {
        return selectedNode;
    } // Fin de la m?thode getSelectedNode/0
    
    /**
     * Retourne la racine de l'arbre
     * @return la racine de l'arbre
     */
    public DefaultMutableTreeNode getRoot() {
        return rootNode;
    } // Fin de la m?thode getRoot/0
    
    /**
     * Retourne le type de l'arbre : "campagne" ou "suite"
     * @return le type de l'arbre
     */
    public int getTreeType() {
        return treeType;
    } // Fin de la m?thode getTreeType/0
    
    /**
     * Retourne le noeud correspondant ? la famille dont le nom est pass? en
     * param?tre. Retourne <code>null</code>, si le nom ne correspond pas ? une
     * famille.
     * @param familyName un nom
     * @return le noeud correspondant ? la famille dont le nom est pass? en
     * param?tre. Retourne <code>null</code>, si le nom ne correspond pas ? une
     * famille.
     */
    public DefaultMutableTreeNode findRemoveFamilyNode(String familyName, boolean toRemove) {
        // DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot(); 
        DefaultMutableTreeNode root = rootNode;
        for (int i=0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(i);
            if (familyNode.getUserObject() instanceof Family && ((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
                if (toRemove) {
                    familyNode.removeFromParent();
                }
                return familyNode;
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1
    
    
    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est
     * pass? en param?tre. Retourne <code>null</code>, si le nom ne
     * correspond pas ? une suite de tests
     * @param testListName un nom
     * @param familyName un nom de famille
     * @param toRemove pour indiquer que le noeud doit ?tre supprim?
     * @return le noeud correspondant ? la suite de tests dont le nom est
     * pass? en param?tre. Retourne <code>null</code>, si le nom ne
     * correspond pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveTestListNode(String testListName, String familyName, boolean toRemove) {
        //       DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot(); 
        DefaultMutableTreeNode root = rootNode;
        for (int i=0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(i);
            if (familyNode.getUserObject() instanceof Family && ((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
                for (int j = 0; j < familyNode.getChildCount(); j++) {
                    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode)familyNode.getChildAt(j);
                    if (testListNode.getUserObject() instanceof TestList && ((TestList)testListNode.getUserObject()).getNameFromModel().equals(testListName)) {
                        if (toRemove) {
                            testListNode.removeFromParent();
                        }
                        return testListNode;
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1
    
    
    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est
     * pass? en param?tre. Retourne <code>null</code>, si le nom ne
     * correspond pas ? une suite de tests
     * @param testListName un nom
     * @param toRemove pour supprimer le noeud une fois trouv?
     * @return le noeud correspondant ? la suite de tests dont le nom est
     * pass? en param?tre. Retourne <code>null</code>, si le nom ne
     * correspond pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveTestNode(String testName, String testListName, String familyName, boolean toRemove) {
        //       DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot(); 
        DefaultMutableTreeNode root = rootNode;
        for (int i=0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(i);
            if (familyNode.getUserObject() instanceof Family && ((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
                for (int j = 0; j < familyNode.getChildCount(); j++) {
                    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode)familyNode.getChildAt(j);
                    if (testListNode.getUserObject() instanceof TestList && ((TestList)testListNode.getUserObject()).getNameFromModel().equals(testListName)) {
                        for (int k = 0; k < testListNode.getChildCount(); k++) {
                            DefaultMutableTreeNode testNode = (DefaultMutableTreeNode)testListNode.getChildAt(k);
                            if (testNode.getUserObject() instanceof Test && ((Test)testNode.getUserObject()).getNameFromModel().equals(testName)) {
                                if (toRemove) {
                                    DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) testNode.getParent();
                                    parentNode.remove(testNode);
                                }
                                return testNode;
                            }
                        }
                        
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1
    
    
    
    
    
    /**
     * Retourne le mod?le de donn?es de l'arbre
     * @return le mod?le de donn?es de l'arbre
     */
    public TestTreeModel getModel() {
        return treeModel;
    } // Fin de la m?thode getModel/0
    
   
  
  
    /**
     * @return
     */
    public JTree getTree() {
        return tree;
    }
    
    
} // Fin de la classe DynamicTree
