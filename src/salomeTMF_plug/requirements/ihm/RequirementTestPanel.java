/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class RequirementTestPanel extends JPanel implements ActionListener , ListSelectionListener, Observer{

    JButton viewReqButton;

    JButton delReqButton;

    JButton addReqButton;

    JTable reqTable;

    JPanel buttonsPanel;
    JScrollPane tablePane;
    JTabbedPane salomeParent;
    JTree salomeDynamicTree;


    RequirementTree pReqTree;
    RequirementCampPanel pRequirementCampPanel;
    //JTree reqTree;

    Test pCurrentTest;
    Vector reqCovered;
    Vector filtredreqCovered;

    int type ; /* 0 for Manual Test 1 for AutomatedTest */
    FiltrePanel pFiltrePanel;

    public RequirementTestPanel(int _type,  RequirementTree _pReqTree, RequirementCampPanel _RequirementCampPanel) {
        type = _type;
        pReqTree = _pReqTree;
        pRequirementCampPanel = _RequirementCampPanel;
        initComponent();
    }

    void initComponent(){
        reqCovered = new Vector();
        filtredreqCovered = new Vector();
        viewReqButton = new JButton(Language.getInstance().getText("Visualiser"));
        addReqButton = new JButton(Language.getInstance().getText("Utiliser"));
        delReqButton = new JButton(Language.getInstance().getText("Supprimer"));


        viewReqButton.addActionListener(this);
        addReqButton.addActionListener(this);
        delReqButton.addActionListener(this);

        viewReqButton.setEnabled(false);
        //      addReqButton.setEnabled(Permission.canUpdateReq());
        delReqButton.setEnabled(false);

        buttonsPanel = new JPanel(new GridLayout(1,3));
        buttonsPanel.add(addReqButton);
        buttonsPanel.add(delReqButton);
        buttonsPanel.add(viewReqButton);

        SimpleTableModel model = new SimpleTableModel();

        reqTable = new JTable(model);
        try {
            //reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(reqCovered));
            reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(filtredreqCovered));
        } catch (Exception  e){
            e.printStackTrace();
        }
        model.addColumn("id");
        model.addColumn(Language.getInstance().getText("Nom"));
        model.addColumn(Language.getInstance().getText("Categorie"));
        model.addColumn(Language.getInstance().getText("Complexite"));
        model.addColumn(Language.getInstance().getText("Etat"));
        //20100115 - D\ufffdbut modification Forge ORTF
        model.addColumn(Language.getInstance().getText("Valide_par"));
        model.addColumn(Language.getInstance().getText("Criticite"));
        model.addColumn(Language.getInstance().getText("Comprehension"));
        model.addColumn(Language.getInstance().getText("Proprietaire"));
        //20100115 - Fin modification Forge ORTF

        //model.addColumn(Language.getInstance().getText("Description"));

        reqTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        reqTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        /*for (int i = 0; i < 5; i++) {
          column = reqTable.getColumnModel().getColumn(i);
          column.sizeWidthToFit();
          }*/

        ListSelectionModel rowSM = reqTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        tablePane = new JScrollPane(reqTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        pFiltrePanel = new FiltrePanel(null, this);

        setLayout(new BorderLayout());
        add(buttonsPanel, BorderLayout.NORTH);
        add(tablePane, BorderLayout.CENTER);
        add(pFiltrePanel, BorderLayout.SOUTH);
        //addFocusListener(new requirementTestFocusListener());
    }

    public void setParent(JTabbedPane _salomeParent){
        if (salomeParent != null){
            return;
        } else {
            salomeParent = _salomeParent;
            salomeParent.addChangeListener(new ChangeListener() {
                    public void stateChanged(ChangeEvent e) {
                        if (((JTabbedPane)e.getSource()).getSelectedComponent().equals(RequirementTestPanel.this)){
                            System.out.println("Update Requiment for test");
                            InitData(DataModel.getCurrentTest());
                        }
                    }
                });
        }
    }

    public void setTestTree(DynamicTree pDynamicTree){
        if (salomeDynamicTree == null){
            salomeDynamicTree = pDynamicTree.getTree();
            salomeDynamicTree.addTreeSelectionListener(new TreeSelectionListener() {
                    public void valueChanged(TreeSelectionEvent e) {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                            salomeDynamicTree.getLastSelectedPathComponent();

                        if (node == null) return;

                        Object nodeInfo = node.getUserObject();
                        /* React to the node selection. */
                        if ((nodeInfo instanceof ManualTest) && type == 0 ) {
                            if (salomeParent.getSelectedComponent().equals(RequirementTestPanel.this)){
                                InitData((Test)nodeInfo);
                            }
                        } else if ((nodeInfo instanceof AutomaticTest) && type == 1) {
                            if (salomeParent.getSelectedComponent().equals(RequirementTestPanel.this)){
                                InitData((Test)nodeInfo);
                            }
                        }
                    }
                });
        }
    }

    public void InitData(Test pTest) {

        if (pTest == pCurrentTest || pTest == null){
            return;
        }
        pCurrentTest = pTest;
        Vector reqCoveredWrapper;
        reqCovered.clear();

        try {
            reqCoveredWrapper = Requirement.getReqWrapperCoveredByTest(pTest.getIdBdd());
            int size = reqCoveredWrapper.size();
            for (int i = 0 ; i < size ; i++) {
                ReqWrapper pReqWrapper = (ReqWrapper)reqCoveredWrapper.elementAt(i);
                DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReqWrapper);
                Requirement pReq = null;
                if (node != null){
                    pReq = (Requirement) node.getUserObject();
                    reqCovered.add(pReq);
                } else {
                    pReqTree.reload();
                    node = pReqTree.findRequirementFromParent(pReqWrapper);
                    if (node != null){
                        pReq = (Requirement) node.getUserObject();
                        reqCovered.add(pReq);
                    } else {
                        //Hum Data corruption !!!
                        Tools.ihmExceptionView(new Exception("Hum ??, it seem that data integrity are corrupted"));

                    }
                }
            }
            // Init table data
            //updateReqUse(reqCovered, false);
            updateTable(reqCovered);
        } catch (Exception e){
            Tools.ihmExceptionView(e);
        }
    }

    void updateReqUse(Vector reqCovered, boolean onBDD){
        int transNumber = -1;
        try {
            int reqSize = reqCovered.size();
            if (onBDD){
                transNumber = SQLWrapper.beginTransaction();
                Requirement.deleteAllCoverForTest(pCurrentTest.getIdBdd());
            }
            //IHM
            //((SimpleTableModel)reqTable.getModel()).clearTable();

            for (int i = 0 ; i < reqSize; i ++){
                Requirement pReq  = (Requirement) reqCovered.elementAt(i);

                if (onBDD){
                    // Bdd
                    pReq.addTestCoverInDB(pCurrentTest.getIdBdd(), false);
                }

                // IHM
                /*Vector data = new Vector();
                  data.add(pReq.getNameFromModel());
                  if (pReq.getDescriptionFromModel().length()> 60){
                  data.add(pReq.getDescriptionFromModel().substring(0, 59)+"...");
                  } else {
                  data.add(pReq.getDescriptionFromModel());
                  }
                  ((SimpleTableModel)reqTable.getModel()).insertRow(i,data);
                */

            }
            if (onBDD){
                SQLWrapper.commitTrans(transNumber);
            }
            // ihm
            updateTable(reqCovered);
        } catch(Exception e){
            try {
                if (onBDD){
                    SQLWrapper.rollBackTrans(transNumber);
                }
            } catch (Exception e2){}
        }


    }

    void updateTable(Vector reqCovered){
        filtredreqCovered.clear();
        int filtre = pFiltrePanel.getFiltre();
        int reqSize = reqCovered.size();
        ((SimpleTableModel)reqTable.getModel()).clearTable();
        int compteur = 0;
        for (int i = 0 ; i < reqSize; i ++){
            Requirement pReq  = (Requirement) reqCovered.elementAt(i);


            if (pReq instanceof ReqLeaf){
                if (!filtredreqCovered.contains(pReq)){
                    int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                    if ((reqP | filtre) == filtre){
                        filtredreqCovered.add(pReq);
                        Vector data = new Vector();
                        data.add("" + pReq.getIdBdd());
                        data.add(pReq.getLongName());
                        /*if (pReq.getDescriptionFromModel().length()> 60){
                          data.add(pReq.getDescriptionFromModel().substring(0, 59)+"...");
                          } else {
                          data.add(pReq.getDescriptionFromModel());
                          }*/
                        data.add(ReqWrapper.getCatString(((ReqLeaf)pReq).getCatFromModel()));
                        data.add(ReqWrapper.getComplexString(((ReqLeaf)pReq).getComplexeFromModel()));
                        data.add(ReqWrapper.getStateString(((ReqLeaf)pReq).getStateFromModel()));
                        //20100115 - D\ufffdbut modification Forge ORTF
                        data.add(ReqWrapper.getValidByString(((ReqLeaf)pReq).getValidByFromModel()));
                        data.add(ReqWrapper.getCriticalString(((ReqLeaf)pReq).getCriticalFromModel()));
                        data.add(ReqWrapper.getUnderstandingString(((ReqLeaf)pReq).getUnderstandingFromModel()));
                        data.add(ReqWrapper.getOwnerString(((ReqLeaf)pReq).getOwnerFromModel()));
                        //20100115 - Fin modification Forge ORTF
                        ((SimpleTableModel)reqTable.getModel()).insertRow(compteur,data);
                        compteur++;
                    }
                }
            }
        }
        setColumnSize(reqTable);
    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(addReqButton)){
            if (addReqPerformed(e)){
                pReqTree.refreshCurrent();
                if (pRequirementCampPanel != null) {
                    pRequirementCampPanel.refreshCurrentCover(false);
                }
            }
        } else if (e.getSource().equals(delReqButton)){
            if (deleteConfirme( Language.getInstance().getText("Couverture"))){
                delReqPerformed(e);
                pReqTree.refreshCurrent();
                if (pRequirementCampPanel != null) {
                    pRequirementCampPanel.refreshCurrentCover(false);
                }
            }
        } else if (e.getSource().equals(viewReqButton)){
            viewReqPerformed(e);
        }
    }

    boolean deleteConfirme(String quoi){
        Object[] options = {org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Oui"), org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Non")};
        int choice = -1;

        choice = SalomeTMFContext.getInstance().askQuestion(
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("confimation_suppression2") + " " + quoi + " ?",
                                                            org.objectweb.salome_tmf.ihm.languages.Language.getInstance().getText("Attention_"),
                                                            JOptionPane.WARNING_MESSAGE,
                                                            options);

        if (choice == JOptionPane.YES_OPTION){
            return true;
        } else {
            return false;
        }
    }

    boolean addReqPerformed(ActionEvent e) {
        SelectRequirement pSelectRequirement = new SelectRequirement(pReqTree.getCopy(true), Language.getInstance().getText("Selection"), reqCovered, true, null);
        Hashtable selected = pSelectRequirement.getSelection();
        if (selected != null){
            reqCovered.clear();
            Enumeration enumE = selected.elements();
            while (enumE.hasMoreElements()){
                reqCovered.add(enumE.nextElement());
            }
            updateReqUse(reqCovered, true);
            return true;
        }
        return false;
    }

    void delReqPerformed(ActionEvent e) {
        int selectedRow = reqTable.getSelectedRow();
        if (selectedRow >-1){
            //if (reqCovered != null) {
            if (filtredreqCovered != null) {
                //Requirement pReq = (Requirement)reqCovered.elementAt(selectedRow);
                Requirement pReq = (Requirement)filtredreqCovered.elementAt(selectedRow);
                try {
                    // Bdd
                    pReq.deleteCoverForTest(pCurrentTest.getIdBdd());

                    //IHM
                    reqCovered.remove(selectedRow);
                    //updateReqUse(reqCovered, false);
                    updateTable(reqCovered);
                } catch (Exception ex){

                }
            }
        }
    }

    void viewReqPerformed(ActionEvent e) {
        int selectedRow = reqTable.getSelectedRow();

        if (selectedRow >-1){
            //if (reqCovered != null) {
            if (filtredreqCovered != null) {
                //Requirement pReq = (Requirement)reqCovered.elementAt(selectedRow);
                Requirement pReq = (Requirement)filtredreqCovered.elementAt(selectedRow);
                DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReq);
                if (node != null){
                    //pReqTree.refreshNode(node);
                    ReqPlugin.selectReqTab();
                    pReqTree.setSelectionPath(new TreePath(node.getPath()));

                }
            }
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
            //no rows are selected
            delReqButton.setEnabled(false);
            viewReqButton.setEnabled(false);
        } else {
            //                    int selectedRow = lsm.getMinSelectionIndex();
            //            delReqButton.setEnabled(Permission.canUpdateReq());
            viewReqButton.setEnabled(true);
            //selectedRow is selected
        }
    }



    /**
     * redefine size of table columns
     */
    public void setColumnSize(JTable table){
        FontMetrics fm = table.getFontMetrics(table.getFont());
        for (int i = 0 ; i < table.getColumnCount() ; i++)
            {
                int max = 0;
                for (int j = 0 ; j < table.getRowCount() ; j++)
                    {
                        int taille = fm.stringWidth((String)table.getValueAt(j,i));
                        if (taille > max)
                            max = taille;
                    }
                String nom = (String)table.getColumnModel().getColumn(i).getIdentifier();
                int taille = fm.stringWidth(nom);
                if (taille > max)
                    max = taille;
                table.getColumnModel().getColumn(i).setPreferredWidth(max+10);
            }
    }

    /********************************* Observer  *************************/

    public void update(Observable o, Object arg){
        if (arg instanceof ReqEvent){
            ReqEvent event = (ReqEvent)arg;
            if (event.code == ReqEvent.FILTRE_REQ_CHANGE){
                //refreshCurrent(false);
                updateTable(reqCovered);
            }
        }
    }
}
