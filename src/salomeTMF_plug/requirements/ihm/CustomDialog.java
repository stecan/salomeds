/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

/* 1.4 example used by DialogDemo.java. */
class CustomDialog extends JDialog implements PropertyChangeListener {
    private String typedText = null;
    private JTextField textField;
        
    private String btnOk = "Enter";
    private String btnCancel = "Cancel";
        
    
    String result;
    
    private JOptionPane optionPane;
        
    public String getValidatedText() {
        return typedText;
    }
        
        
        
    /** Creates the reusable dialog. */
    public CustomDialog(Frame aFrame, String title, String question, String defaultValue) {
        //super(true);
        super(SalomeTMFContext.getInstance().getSalomeFrame());
        setModal(true);
        setResizable(false);
                
        setTitle(title);
                
        textField = new JTextField(10);
        if(defaultValue != null && !defaultValue.equals("")){
            textField.setText(defaultValue);
        }
        setResizable(false);
        Object[] array = {question, textField};
                
        //Create an array specifying the number of dialog buttons
        //and their text.
        Object[] options = {btnOk, btnCancel};
                
        //Create the JOptionPane.
        optionPane = new JOptionPane(array,
                                     JOptionPane.QUESTION_MESSAGE,
                                     JOptionPane.YES_NO_OPTION,
                                     null,
                                     options,
                                     options[0]);
                
        //Make this dialog display it.
        setContentPane(optionPane);
                
        //Handle window closing correctly.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent we) {
                    optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
                }
            });
                
        //Ensure the text field always gets the first focus.
        addComponentListener(new ComponentAdapter() {
                public void componentShown(ComponentEvent ce) {
                    textField.requestFocusInWindow();
                }
            });
                
        optionPane.addPropertyChangeListener(this);
        //this.setLocation(300,200);
        //this.setLocationRelativeTo(this.getParent()); 
        //pack();
        centerScreen();
    }
        
    void centerScreen() {
                  
        Dimension dim = getToolkit().getScreenSize();
        this.pack();  
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        requestFocus();
    }
        
    /** This method handles events for the text field. */
    /*public void actionPerformed(ActionEvent e) {
                
      optionPane.setValue(btnString1);
      System.out.println("[CustomDialog->actionPerformed] : " +e  );
      * Object value = optionPane.getValue();
      *         if (value == JOptionPane.UNINITIALIZED_VALUE) {
      //ignore reset
      return;
      }
                
      }*/
        
    public void propertyChange(PropertyChangeEvent e) {
        //System.out.println("[CustomDialog->PropertyChangeEvent] : " +e  );
        String prop = e.getPropertyName();
        if (isVisible() && (e.getSource() == optionPane)
            && (JOptionPane.VALUE_PROPERTY.equals(prop) 
                || JOptionPane.INPUT_VALUE_PROPERTY.equals(prop))) {
                            
            Object value = optionPane.getValue();
            //System.out.println("[CustomDialog->PropertyChangeEvent] value  " + value );
            if (value.equals(btnOk)){
                result = textField.getText();
                //System.out.println("[CustomDialog->PropertyChangeEvent] OK : " + value);
                clearAndHide();
            }else if (value.equals(btnCancel)){
                //System.out.println("[CustomDialog->PropertyChangeEvent] CANCEL");
                result = null;
                clearAndHide();
            } else {
                //System.out.println("[CustomDialog->PropertyChangeEvent] default");
                result = null;
            }
        }
    }
        
        
    /** This method clears the dialog and hides it. */
    public void clearAndHide() {
        //textField.setText(null);
        setVisible(false);
    }
        
    public String getValue(){
        return result;
    }
}
