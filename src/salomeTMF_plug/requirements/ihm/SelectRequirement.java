/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.ReqFamily;
import salomeTMF_plug.requirements.data.ReqFilter;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;

public class SelectRequirement extends JDialog implements ActionListener,
                                                          WindowListener {

    JTree reqTree;
    // RequirementTree pRequirementTree; // Used for reload
    JList reqList;
    JButton removeButton;
    JButton addButton;
    JButton validateButton;
    JButton cancelButton;
    // JButton refreshReq;
    ArrayList reqSelectedNodes;

    Hashtable reqSelected;
    FiltrePanel pFiltrePanel;
    InfoFiltrePanel pInfoFiltrePanel;
    CoverFiltrePanel pCoverFiltrePanel;
    IReqFilter pPriorityFilter;
    FilterReqTreeModel treeModel;
    Hashtable<Integer, Requirement> removedReq;

    /**
     *
     * @param _reqTree
     * @param title
     * @param usedReq
     * @param coverFiltre
     * @param _removedReq
     *            hashtable of requirement that can't be selected when add
     *            action is performed
     */
    public SelectRequirement(JTree _reqTree, String title, Vector usedReq,
                             boolean coverFiltre, Hashtable<Integer, Requirement> _removedReq) {
        // RequirementTree _pRequirementTree){
        super(SalomeTMFContext.getInstance().getSalomeFrame());

        int t_x = 1024 - 100;
        int t_y = 768 - 50;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height - 50;
        } catch (Exception E) {

        }

        setResizable(false);
        setModal(true);
        reqTree = _reqTree;
        // pRequirementTree = _pRequirementTree;
        reqSelectedNodes = new ArrayList();

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        addWindowListener(this);
        removeButton = new JButton("<");
        removeButton.addActionListener(this);

        addButton = new JButton(">");
        addButton.addActionListener(this);

        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(addButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1, 25)));
        buttonSet.add(removeButton);

        JPanel reqPanel = new JPanel(new BorderLayout());

        pPriorityFilter = new ReqFilter(1007);
        reqTree.getModel().getRoot();
        treeModel = new FilterReqTreeModel(pPriorityFilter, (TreeNode) reqTree
                                           .getModel().getRoot(), reqTree);
        reqTree.setModel(treeModel);

        pFiltrePanel = new FiltrePanel(treeModel);

        JPanel lesFiltresPanel = new JPanel(new GridBagLayout());
        lesFiltresPanel.setBorder(BorderFactory.createTitledBorder(
                                                                   BorderFactory.createLineBorder(Color.BLACK), Language
                                                                   .getInstance().getText("Filtre")));

        JPanel filtresPanel1 = new JPanel(new GridBagLayout());

        if (coverFiltre) {
            pCoverFiltrePanel = new CoverFiltrePanel(treeModel, null);
            filtresPanel1.add(pCoverFiltrePanel, new GridBagConstraints(0, 0,
                                                                        1, 1, 1, 1, GridBagConstraints.CENTER,
                                                                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }

        lesFiltresPanel.add(filtresPanel1, new GridBagConstraints(0, 0, 1, 1,
                                                                  1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                                                  new Insets(0, 0, 0, 0), 0, 0));
        pInfoFiltrePanel = new InfoFiltrePanel((FilterReqTreeModel) reqTree
                                               .getModel(), null);
        lesFiltresPanel.add(pInfoFiltrePanel, new GridBagConstraints(0, 1, 1,
                                                                     1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                                                                     new Insets(0, 0, 0, 0), 0, 0));
        /*
         * JPanel buttonOptionPanel = new JPanel(new FlowLayout());
         * buttonOptionPanel.add(refreshReq);
         * lesFiltresPanel.add(buttonOptionPanel);
         */

        JScrollPane reqScrollPane = new JScrollPane(reqTree);
        reqScrollPane.setBorder(BorderFactory.createTitledBorder(Language
                                                                 .getInstance().getText("Exigence_disponible")));
        reqScrollPane.setPreferredSize(new Dimension(t_x / 2 - 50,
                                                     t_y / 3 * 2 - 50));
        reqPanel.add(reqScrollPane, BorderLayout.CENTER);
        // reqPanel.add(pFiltrePanel, BorderLayout.SOUTH);
        reqPanel.add(lesFiltresPanel, BorderLayout.SOUTH);

        if (_removedReq != null) {
            removedReq = _removedReq;
            // System.out.println("RemovedReq is not null, size = " +
            // removedReq.size());
        } else {
            removedReq = new Hashtable<Integer, Requirement>();
        }

        reqList = new JList(usedReq);
        reqList.setCellRenderer(new PriorityListCellRenderer());
        reqSelected = new Hashtable();
        int size = usedReq.size();
        for (int i = 0; i < size; i++) {
            Requirement pReq = (Requirement) usedReq.elementAt(i);
            reqSelected.put(new Integer(pReq.getIdBdd()), pReq);
        }
        JScrollPane selectedScrollPane = new JScrollPane(reqList);
        selectedScrollPane.setBorder(BorderFactory.createTitledBorder(Language
                                                                      .getInstance().getText("Exigence_selectionne")));
        selectedScrollPane.setPreferredSize(new Dimension(t_x / 2 - 50,
                                                          t_y / 3 * 2 - 50));
        selectedScrollPane.getViewport().setView(reqList);

        JPanel windowPanel = new JPanel();
        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
        // windowPanel.add(reqScrollPane);
        windowPanel.add(reqPanel);
        windowPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        windowPanel.add(buttonSet);
        windowPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        windowPanel.add(selectedScrollPane);

        validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(this);

        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);

        JPanel secondButtonSet = new JPanel();
        secondButtonSet.add(validateButton);
        secondButtonSet.add(cancelButton);

        JPanel center = new JPanel();
        center.add(windowPanel);

        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(center);
        page.add(secondButtonSet);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);

        /*
         * addWindowFocusListener(new java.awt.event.WindowFocusListener() {
         * public void windowGainedFocus(java.awt.event.WindowEvent evt) { }
         * public void windowLostFocus(java.awt.event.WindowEvent evt) {
         * toFront(); } });
         */
        this.setTitle(title);
        // this.setLocation(300,200);
        this.setSize(t_x, t_y);
        // this.setLocation(50,50);
        /*
         * this.setLocationRelativeTo(this.getParent()); this.pack();
         * this.setVisible(true);
         */
        centerScreen();

    }

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);

        this.setVisible(true);
        requestFocus();
    }

    /*
     * void reloadData(){ pRequirementTree.copyIn(reqTree, true);
     * treeModel.reload(); }
     */

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(removeButton)) {
            removePerformed(e);
        } else if (e.getSource().equals(addButton)) {
            addPerformed(e);
        } else if (e.getSource().equals(validateButton)) {
            validatePerformed(e);
        } else if (e.getSource().equals(cancelButton)) {
            cancelPerformed(e);
        }
        /*
         * } else if (e.getSource().equals(refreshReq)){ reloadData(); }
         */
    }

    /**
     * Return true if pReq is fitred (appears in ReqTree)
     *
     * @param pReq
     * @return
     */

    void removePerformed(ActionEvent e) {
        Object[] selectedValue = reqList.getSelectedValues();
        for (int i = 0; i < selectedValue.length; i++) {
            Requirement pReq = (Requirement) selectedValue[i];
            reqSelected.remove(new Integer(pReq.getIdBdd()));

        }
        Vector data = new Vector();
        reqList.clearSelection();
        Enumeration enumReq = reqSelected.elements();
        while (enumReq.hasMoreElements()) {
            data.add(enumReq.nextElement());
        }
        reqList.setListData(data);
    }

    void addPerformed(ActionEvent e) {
        TreePath[] pathTab = reqTree.getSelectionPaths();
        // DefaultTreeModel treeModel =(DefaultTreeModel) reqTree.getModel();
        if (pathTab != null) {
            reqSelectedNodes.clear();
            for (int i = 0; i < pathTab.length; i++) {
                reqSelectedNodes.add(pathTab[i].getLastPathComponent());
            }
            int filtre = pFiltrePanel.getFiltre();
            Vector data = new Vector();
            for (int i = 0; i < reqSelectedNodes.size(); i++) {
                Requirement pReq = (Requirement) ((DefaultMutableTreeNode) reqSelectedNodes
                                                  .get(i)).getUserObject();
                if (pReq instanceof ReqLeaf) {
                    if (pPriorityFilter.isFiltred((ReqLeaf) pReq)) {
                        if (removedReq.get(pReq.getIdBdd()) == null) {
                            reqSelected.put(new Integer(pReq.getIdBdd()), pReq);
                        }
                    }
                } else {
                    ReqFamily pReqFamily = (ReqFamily) pReq;
                    Vector listOfReq = pReqFamily.getAllLeaf();
                    int size = listOfReq.size();
                    for (int j = 0; j < size; j++) {
                        Requirement pReq2 = (Requirement) listOfReq
                            .elementAt(j);
                        if (pPriorityFilter.isFiltred((ReqLeaf) pReq2)) {
                            if (removedReq.get(pReq2.getIdBdd()) == null) {
                                reqSelected.put(new Integer(pReq2.getIdBdd()),
                                                pReq2);
                            }
                        }

                    }
                }
            }
            reqList.clearSelection();
            Enumeration enumReq = reqSelected.elements();
            while (enumReq.hasMoreElements()) {
                data.add(enumReq.nextElement());
            }
            reqList.setListData(data);
        }
    }

    void validatePerformed(ActionEvent e) {
        dispose();
    }

    void cancelPerformed(ActionEvent e) {
        reqSelected.clear();
        reqSelected = null;
        dispose();
    }

    /*
     * public void setVisible(boolean arg0) { // TODO Auto-generated method stub
     * super.setVisible(arg0); if (arg0 == true){ reqSelected = new Hashtable();
     * } }
     */

    public Hashtable getSelection() {
        return reqSelected;
    }

    public void windowClosing(WindowEvent e) {
        reqSelected.clear();
        reqSelected = null;
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    /*
     * class MyCellReqRenderer extends JLabel implements ListCellRenderer {
     * ImageIcon ReqFamIcon =
     * createImageIcon("/salomeTMF_plug/requirements/resources/images/reqFam.png"
     * ); ImageIcon ReqLeafIncon
     * =createImageIcon("/salomeTMF_plug/requirements/resources/images/req.png"
     * );
     *
     * // This is the only method defined by ListCellRenderer. // We just
     * reconfigure the JLabel each time we're called.
     *
     * public Component getListCellRendererComponent( JList list, Object value,
     * // value to display int index, // cell index boolean isSelected, // is
     * the cell selected boolean cellHasFocus) // the list and the cell have the
     * focus { String s = value.toString(); setText(s); setIcon((s.length() >
     * 10) ? longIcon : shortIcon); if (isSelected) {
     * setBackground(list.getSelectionBackground());
     * setForeground(list.getSelectionForeground()); } else {
     * setBackground(list.getBackground()); setForeground(list.getForeground());
     * } setEnabled(list.isEnabled()); setFont(list.getFont()); setOpaque(true);
     * return this; } }
     */

}
