package salomeTMF_plug.requirements.ihm;

import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMF;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;

import salomeTMF_plug.requirements.data.IReqFilter;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000120 - \ufffd2.7.8 Jira : FORTF - 39
 *
 */
public class InfoFiltrePanel extends JPanel implements ActionListener {

    public JComboBox catBox;
    Vector itemCat;

    public JComboBox complexeBox;
    Vector itemComplex;

    public JComboBox stateBox;
    Vector itemState;

    int catSelected = -1;
    int complexSelected = -1;
    int stateSelected = -1;

    // 20100118 - D\ufffdbut modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;

    final String REQUIREMENTS_CFG_FILE = "/plugins/requirements/cfg/Requirements.properties";
    // 20100118 - Fin modification Forge ORTF

    IReqFilter m_ReqFilter;
    FilterReqTreeModel m_FilterReqTreeModel;
    Observer pObserver = null;

    InfoFiltrePanel(FilterReqTreeModel _FilterReqTreeModel, Observer _pObserver) {
        super();
        m_FilterReqTreeModel = _FilterReqTreeModel;
        if (m_FilterReqTreeModel != null) {
            m_ReqFilter = m_FilterReqTreeModel.getFilter();
        }
        pObserver = _pObserver;
        initCompnent();
    }

    void initCompnent() {
        setLayout(new GridLayout(2, 3));
        // 20100118 - D\ufffdbut modification Forge ORTF
        itemCat = new Vector();
        itemState = new Vector();
        itemComplex = new Vector();
        itemCat.add(Language.getInstance().getText("Tout"));
        itemState.add(Language.getInstance().getText("Tout"));
        itemComplex.add(Language.getInstance().getText("Tout"));
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
        Properties prop = new Properties();
        try {
            URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            URL urlBase = new URL(url_txt);

            java.net.URL url_cfg = new java.net.URL(urlBase
                                                    + REQUIREMENTS_CFG_FILE);
            try {
                prop = Util.getPropertiesFile(url_cfg);
            } catch (Exception e) {
                prop = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Iterator it = prop.keySet().iterator();
        String sql = "";
        while (it.hasNext()) {
            String property = (String) it.next();
            if (property.equals("Categorie")) {
                String[] categories = prop.getProperty(property).split(",");
                for (int i = 0; i < categories.length; i++) {
                    sql = "SELECT * FROM REQ_CATEGORY where name='" + categories[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_CATEGORY (name,user_entry) values ('"
                                + categories[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemCat.add(categories[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Complexite")) {
                String[] complexes = prop.getProperty(property).split(",");
                for (int i = 0; i < complexes.length; i++) {
                    sql = "SELECT * FROM REQ_COMPLEXE where name='" + complexes[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_COMPLEXE (name,user_entry) values ('"
                                + complexes[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemComplex.add(complexes[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Statut")) {
                String[] states = prop.getProperty(property).split(",");
                for (int i = 0; i < states.length; i++) {
                    sql = "SELECT * FROM REQ_STATE where name='" + states[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_STATE (name,user_entry) values ('"
                                + states[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemState.add(states[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }
        sql = "SELECT id,name FROM REQ_CATEGORY where user_entry=1;";
        List<String> userCategories = new ArrayList<String>();
        List<Integer> userCategoriesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String category = result.getString(2);
                int id = result.getInt(1);
                userCategories.add(category);
                userCategoriesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userCategoriesId.size(); i++) {
            int id = userCategoriesId.get(i);
            try {
                sql = "select count(cat) from REQUIREMENTS where cat=" + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbCat = result.getInt(1);
                    if (nbCat != 0) {
                        itemCat.add(userCategories.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_COMPLEXE where user_entry=1;";
        List<String> userComplexes = new ArrayList<String>();
        List<Integer> userComplexesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String complexe = result.getString(2);
                int id = result.getInt(1);
                userComplexes.add(complexe);
                userComplexesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userComplexesId.size(); i++) {
            int id = userComplexesId.get(i);
            try {
                sql = "select count(complexe) from REQUIREMENTS where complexe="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbComp = result.getInt(1);
                    if (nbComp != 0) {
                        itemComplex.add(userComplexes.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_STATE where user_entry=1;";
        List<String> userStates = new ArrayList<String>();
        List<Integer> userStatesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String state = result.getString(2);
                int id = result.getInt(1);
                userStates.add(state);
                userStatesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userStatesId.size(); i++) {
            int id = userStatesId.get(i);
            try {
                sql = "select count(state) from REQUIREMENTS where state=" + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbState = result.getInt(1);
                    if (nbState != 0) {
                        itemState.add(userStates.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        JLabel labelCat = new JLabel(Language.getInstance()
                                     .getText("Categorie")
                                     + " :");
        // Collections.sort(itemCat);
        catBox = new JComboBox(itemCat);
        catBox.setEditable(false);
        catBox.addActionListener(this);

        JLabel labelComplexe = new JLabel(Language.getInstance().getText(
                                                                         "Complexite")
                                          + " :");
        // Collections.sort(itemComplex);
        complexeBox = new JComboBox(itemComplex);
        complexeBox.setEditable(false);
        complexeBox.addActionListener(this);

        JLabel labelState = new JLabel(Language.getInstance().getText("Etat")
                                       + " :");
        // Collections.sort(itemState);
        stateBox = new JComboBox(itemState);
        stateBox.setEditable(false);
        stateBox.addActionListener(this);
        // 20100118 - Fin modification Forge ORTF

        add(labelCat);
        add(labelComplexe);
        add(labelState);
        add(catBox);
        add(complexeBox);
        add(stateBox);
        // setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),"Filtre"));

    }

    // 20100118 - D\ufffdbut modification Forge ORTF
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(stateBox)) {
            String state = (String) stateBox.getSelectedItem();
            String sql = "SELECT id FROM REQ_STATE where name='" + state + "';";
            stateSelected = -1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    stateSelected = result.getInt(1);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (e.getSource().equals(complexeBox)) {
            String complexe = (String) complexeBox.getSelectedItem();

            String sql = "SELECT id FROM REQ_COMPLEXE where name='" + complexe
                + "';";
            complexSelected = -1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    complexSelected = result.getInt(1);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else if (e.getSource().equals(catBox)) {
            String cat = (String) catBox.getSelectedItem();
            String sql = "SELECT id FROM REQ_CATEGORY where name='" + cat + "';";
            catSelected = -1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    catSelected = result.getInt(1);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        applyFilter();
    }

    // 20100118 - Fin modification Forge ORTF

    void applyFilter() {
        if (m_ReqFilter != null) {
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(
                                                                      new Cursor(Cursor.WAIT_CURSOR));
            m_ReqFilter.setInfoFiltre(catSelected, complexSelected,
                                      stateSelected);
            m_FilterReqTreeModel.setFiltered(true);
            if (pObserver != null) {
                pObserver.update(null, new ReqEvent(ReqEvent.FILTRE_REQ_CHANGE,
                                                    null));
            }
            SalomeTMFContext.getInstance().getSalomeFrame().setCursor(
                                                                      new Cursor(Cursor.DEFAULT_CURSOR));
        }

    }

    void reInit(boolean filter) {
        catSelected = -1;
        catBox.setSelectedIndex(0);
        complexSelected = -1;
        complexeBox.setSelectedIndex(0);
        stateSelected = -1;
        stateBox.setSelectedIndex(0);
        if (filter) {
            m_ReqFilter.reInit();
        }
    }

    public int getCatSelected() {
        return catSelected;
    }

    public int getComplexSelected() {
        return complexSelected;
    }

    public int getStateSelected() {
        return stateSelected;
    }

}
