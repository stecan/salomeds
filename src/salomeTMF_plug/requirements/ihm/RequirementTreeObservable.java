/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;
import java.util.Observable;
import java.util.Observer;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import salomeTMF_plug.requirements.data.Requirement;



public class RequirementTreeObservable extends  Observable  implements TreeSelectionListener{
        
    protected DefaultMutableTreeNode currentNode = null;
    protected DefaultMutableTreeNode oldNode = null;
        
    protected StatRequirement pStatRequirement;
    protected FiltrePanel pFiltrePanel;
        
    void setStatTools(StatRequirement _pStatRequirement, FiltrePanel _pFiltrePanel){
        pStatRequirement = _pStatRequirement;
        pFiltrePanel = _pFiltrePanel;
    }
        
        
    public void addObserver(Observer o) throws NullPointerException{
        super.addObserver(o);
        //notifyObservers(new ReqEvent(ReqEvent.REQSELECTED, currentNode.getUserObject()));
        setChanged();
    }
        
    public void valueChanged(TreeSelectionEvent e) {
        TreePath path = e.getPath();
        oldNode = currentNode;
        currentNode = (DefaultMutableTreeNode) path.getLastPathComponent(); 
        if (!currentNode.equals(oldNode)){
            //System.out.println("valueChanged : " + e);
            calStat(false);
            notifyObservers(new ReqEvent(ReqEvent.REQSELECTED, currentNode.getUserObject()));
            setChanged();
        }
    }
        
    public void refresh() {
        if (currentNode != null){
            calStat(true);
            notifyObservers(new ReqEvent(ReqEvent.REQSELECTED, currentNode.getUserObject()));
            setChanged();
        }
    }
        
    void calStat(boolean forceReload){
        if (pStatRequirement != null){
            //System.out.println("ReqTreeObseve-> : " + forceReload);
            try {
                pStatRequirement.update((Requirement)currentNode.getUserObject(), pFiltrePanel.getFilter(), forceReload);
                //pStatRequirement.update((Requirement)currentNode.getUserObject(), pFiltrePanel.getFiltre(), forceReload);
            } catch(Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
