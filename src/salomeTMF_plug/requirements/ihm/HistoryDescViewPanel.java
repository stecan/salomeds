package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.io.StringReader;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.rtf.RTFEditorKit;



public class HistoryDescViewPanel  extends JPanel{
    HTMLDocument m_oDocument;
    HTMLEditorKit m_oHTML;
    JTextPane m_txtTexte;
        
    HistoryDescViewPanel(String htmlText){
        setLayout(new BorderLayout());
        m_oHTML  = new HTMLEditorKit(); 
        m_oDocument  = new HTMLDocument();      
        m_txtTexte = new JTextPane();
        m_txtTexte.setEditorKit(m_oHTML);
        m_txtTexte.setDocument(m_oDocument);
        m_txtTexte.setEditable(false);
        m_txtTexte.setMinimumSize(new Dimension(400, 400));
        JScrollPane sclScroll = new JScrollPane(m_txtTexte);
        sclScroll.setPreferredSize(new Dimension(400, 400));
        if (htmlText != null ) {
            loadText(htmlText);
        }
        add(sclScroll, BorderLayout.CENTER);
    }
        
    void loadText(String htmlText){
        try {
            StringReader input = new StringReader(htmlText);
            m_txtTexte.setText("");
            m_oHTML.read(input, m_oDocument, 0);
            input.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Impossible de charger le document : " + ex.toString());}
    }
}
