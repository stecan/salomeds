package salomeTMF_plug.requirements.ihm;

import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.Requirement;

public class FilterReqTreeModel extends DefaultTreeModel {
    protected TreeNode m_root;
    protected IReqFilter m_filter;
    protected  JTree reqTree;
    public FilterReqTreeModel(IReqFilter filter, TreeNode root, JTree reqTree){
        super(root);
        m_filter = filter;
        m_root = root;
        this.reqTree = reqTree;
    }
        
    public IReqFilter getFilter(){
        return m_filter;
    }
        
        
    public void setFiltered(boolean pass) {
        //Enumeration<TreePath> enumExpandPath = reqTree.getExpandedDescendants();
        TreePath pTreePath = reqTree.getSelectionPath();
                
        /*int nbRow = reqTree.getRowCount();
          String[] states = new String[nbRow];
          for (int i = 0; i < nbRow ; i++){
          states[i] = getExpansionState(reqTree, i);
          }*/
        Hashtable<TreePath, Boolean> state = getExpansionState(reqTree);
        m_filter.setActived(pass);
        Object[] path = {root};
        int[] childIndices  = new int[root.getChildCount()];      
        Object[] children  = new Object[root.getChildCount()];
        for (int i = 0; i < root.getChildCount(); i++) {
            childIndices[i] = i;
            children[i] = root.getChildAt(i);
        }
           
        fireTreeStructureChanged(this,path,childIndices, children);
        if (pTreePath != null){
            DefaultMutableTreeNode pNode = (DefaultMutableTreeNode) pTreePath.getLastPathComponent();
            Requirement pReq = (Requirement) pNode.getUserObject();
            reqTree.setSelectionPath(pTreePath);
            if (!m_filter.isFiltred(pReq)){
                boolean trouve = false;
                while (!trouve && pTreePath!=null){
                    pTreePath = pTreePath.getParentPath();
                    if (pTreePath != null){
                        pNode = (DefaultMutableTreeNode) pTreePath.getLastPathComponent();
                        pReq = (Requirement) pNode.getUserObject();
                        if (m_filter.isFiltred(pReq)){
                            trouve = true;
                        }
                    }
                }
                reqTree.setSelectionPath(pTreePath);
            }
        }
        /*for (int i = 0; i < nbRow ; i++){
          restoreExpanstionState(reqTree, i, states[i]);
          }*/
        restoreExpanstionState(reqTree,state);
    }
        
    public int getChildCount(Object parent) {
        int realCount = super.getChildCount(parent), filterCount=0;;
        for (int i=0; i<realCount; i++) {
            DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode)super.getChild(parent,i);
            if (m_filter.pass(dmtn)) filterCount++;
        }
        return filterCount;
    }

    public Object getChild(Object parent, int index) {
        int cnt=-1;
        for (int i=0; i<super.getChildCount(parent); i++) {
            Object child = super.getChild(parent,i);
            if (m_filter.pass(child)) cnt++;
            if (cnt==index) return child;
        }
        return null;
    }
          
          
    /********************* UTIL **************************/
          
    //   is path1 descendant of path2
    public boolean isDescendant(TreePath path1, TreePath path2){
        int count1 = path1.getPathCount();
        int count2 = path2.getPathCount();
        if(count1<=count2)
            return false;
        while(count1!=count2){
            path1 = path1.getParentPath();
            count1--;
        }
        return path1.equals(path2);
    }
         
    public Hashtable<TreePath, Boolean> getExpansionState(JTree tree){
        Hashtable<TreePath, Boolean> statePath = new Hashtable<TreePath, Boolean>();
        int nbRow = reqTree.getRowCount();
        for (int row = 0; row < nbRow ;row++){
            TreePath rowPath = tree.getPathForRow(row);
            int rowCount = tree.getRowCount();
            for(int i=row; i<rowCount; i++){
                TreePath path = tree.getPathForRow(i);
                if(i==row || isDescendant(path, rowPath)){
                    if(tree.isExpanded(path)) {
                        statePath.put(path, true);
                    }
                }else
                    break;
            }
        }
        return statePath;
    }
    public void restoreExpanstionState(JTree tree, Hashtable<TreePath, Boolean> statePath){
        int nbRow = reqTree.getRowCount();
        for (int row = 0; row < nbRow ;row++){
            //TreePath rowPath = tree.getPathForRow(row);
            int rowCount = tree.getRowCount();
            for(int i=row; i<rowCount; i++){
                TreePath path = tree.getPathForRow(i);
                Boolean expanded = statePath.get(path);
                if (expanded != null){
                    tree.expandRow(tree.getRowForPath(path));
                }
            }
        }
    }
}
