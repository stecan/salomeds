package salomeTMF_plug.requirements.ihm;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import salomeTMF_plug.requirements.data.ReqFilter;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class PriorityTableCellRenderer  extends DefaultTableCellRenderer {
    Vector reqCovered;
    public PriorityTableCellRenderer(Vector _reqCovered) {
        reqCovered = _reqCovered;
    }

    /**
     * @covers SFD_ForgeORTF_EXG_SEL_000020 - \ufffd2.4.3
     * Jira : FORTF - 40
     */
    public Component getTableCellRendererComponent
        (JTable table, Object value, boolean isSelected,
         boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        try {

            Object objReq = reqCovered.elementAt(row);
            int priority = 0;
            if (objReq instanceof ReqLeaf){
                priority = ((ReqLeaf)objReq).getPriorityFromModel();
            } else if (objReq instanceof ReqWrapper){
                priority = ((ReqWrapper)objReq).getPriority();
            }

            //20100113 - D\ufffdbut modification Forge ORTF
            if (priority == ReqFilter.P_HIGHT){
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.red);
                }
            } else if (priority == ReqFilter.P_MEDIUM){
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.blue);
                }
            } else if (priority == ReqFilter.P_LOW){
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.black);
                }
            } else{
                if (isSelected){
                    setForeground(Color.white);
                }
                else{
                    setForeground(Color.gray);
                }
            }
            //20100113 - Fin modification Forge ORTF

            /*
              TableColumn pcolumn = null;
              pcolumn = table.getColumnModel().getColumn(0);
              pcolumn.setPreferredWidth(2);
              pcolumn = table.getColumnModel().getColumn(1);
              pcolumn.setPreferredWidth(2);
              pcolumn = table.getColumnModel().getColumn(2);
              pcolumn.setPreferredWidth(5);
              pcolumn = table.getColumnModel().getColumn(3);
              pcolumn.setPreferredWidth(5);
              pcolumn = table.getColumnModel().getColumn(4);
              pcolumn.setPreferredWidth(5);
            */


        } catch (Exception e){

        }
        return this;
    }
}
