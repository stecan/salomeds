/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class RequirementCampPanel extends JPanel implements ActionListener , ListSelectionListener, Observer{
    //ChangeListener
    JButton viewReqButton;
    // JButton infoReqButton;

    JTable reqTable;

    JPanel buttonsPanel;
    JScrollPane tablePane;
    JTabbedPane salomeParent;
    JTree salomeDynamicTree;

    ChartPanel chartPanel;

    RequirementTree pReqTree;
    //JTree reqTree;

    Campaign pCurrentCampaign;
    Vector reqCovered;
    Vector filtredreqCovered;

    PiePlot3D plot;
    DefaultPieDataset dataset;
    FiltrePanel pFiltrePanel;
    int nbReqTotal = 0;

    public RequirementCampPanel(RequirementTree _pReqTree) {

        pReqTree = _pReqTree;
        initComponent();
    }

    void initComponent(){
        reqCovered = new Vector();
        filtredreqCovered = new Vector();

        viewReqButton = new JButton(Language.getInstance().getText("Visualiser"));
        //infoReqButton = new JButton(Language.getInstance().getText("Statistique"));



        viewReqButton.addActionListener(this);
        //infoReqButton.addActionListener(this);


        viewReqButton.setEnabled(false);
        //infoReqButton.setEnabled(true);


        buttonsPanel = new JPanel(new GridLayout(1,2));
        //buttonsPanel.add(infoReqButton);
        buttonsPanel.add(viewReqButton);

        SimpleTableModel model = new SimpleTableModel();

        reqTable = new JTable(model);
        try {
            //reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(reqCovered));
            reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(filtredreqCovered));
        } catch (Exception  e){
            e.printStackTrace();
        }
        //model.addColumn("Id");
        model.addColumn("id");
        model.addColumn(Language.getInstance().getText("Nom"));
        model.addColumn(Language.getInstance().getText("Categorie"));
        model.addColumn(Language.getInstance().getText("Complexite"));
        model.addColumn(Language.getInstance().getText("Etat"));
        //20100118 - D\ufffdbut modification Forge ORTF
        model.addColumn(Language.getInstance().getText("Valide_par"));
        model.addColumn(Language.getInstance().getText("Criticite"));
        model.addColumn(Language.getInstance().getText("Comprehension"));
        model.addColumn(Language.getInstance().getText("Proprietaire"));
        //20100118 - Fin modification Forge ORTF

        reqTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        reqTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = reqTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        tablePane = new JScrollPane(reqTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());


        dataset = new  DefaultPieDataset();
        dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), new Double(100.0));
        dataset.setValue(Language.getInstance().getText("Exigence_couverte"), new Double(0.0));

        JFreeChart chart = ChartFactory.createPieChart3D(
                                                         Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                         dataset,                // data
                                                         true,                   // include legend
                                                         true,
                                                         false
                                                         );

        plot =  (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        plot.setNoDataMessage("No data to display");
        chartPanel = new ChartPanel(chart);

        JPanel graphPanel = new JPanel(new BorderLayout());
        pFiltrePanel = new FiltrePanel(null, this);
        graphPanel.add(chartPanel, BorderLayout.CENTER);
        graphPanel.add(pFiltrePanel, BorderLayout.SOUTH);


        setLayout(new BorderLayout());
        add(buttonsPanel, BorderLayout.NORTH);
        add(tablePane, BorderLayout.CENTER);
        //add(chartPanel, BorderLayout.SOUTH);
        add(graphPanel, BorderLayout.SOUTH);

    }

    public void setParent(JTabbedPane _salomeParent){
        if (salomeParent != null){
            return;
        } else {
            salomeParent = _salomeParent;
            salomeParent.addChangeListener(new ChangeListener() {
                    public void stateChanged(ChangeEvent e) {
                        if (((JTabbedPane)e.getSource()).getSelectedComponent().equals(RequirementCampPanel.this)){
                            System.out.println("Update Requiment for test");
                            Campaign pCamp = DataModel.getCurrentCampaign();
                            if (pCamp == pCurrentCampaign || pCamp == null){
                                return;
                            }
                            InitData(pCamp, true);
                        }
                    }
                });
        }
    }

    public void setCampTree(DynamicTree pDynamicTree){
        if (salomeDynamicTree == null){
            salomeDynamicTree = pDynamicTree.getTree();
            salomeDynamicTree.addTreeSelectionListener(new TreeSelectionListener() {
                    public void valueChanged(TreeSelectionEvent e) {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode)salomeDynamicTree.getLastSelectedPathComponent();

                        if (node == null) return;

                        Object nodeInfo = node.getUserObject();
                        /* React to the node selection. */
                        if ((nodeInfo instanceof Campaign) ) {
                            if (salomeParent.getSelectedComponent().equals(RequirementCampPanel.this)){
                                Campaign pCamp = (Campaign)nodeInfo;
                                if (pCamp == pCurrentCampaign || pCamp == null){
                                    return;
                                }
                                InitData(pCamp, true);
                            }
                        }
                    }
                });
        }
    }


    /**
     * Call When an Test caver change
     * @param updateSQL
     */
    void refreshCurrentCover(boolean updateSQL){
        if (pCurrentCampaign!=null){
            InitData(pCurrentCampaign, updateSQL);
        }
    }


    /**
     * Campaign nopt change but refresh because :
     * - New req added
     * - Filtre change
     */
    void refreshCurrentDataModel(){
        if (pCurrentCampaign!=null){
            int reqCoveredWrapperSize = reqCovered.size();
            filtredreqCovered.clear();
            int filtre = pFiltrePanel.getFiltre();
            for (int i = 0 ; i < reqCoveredWrapperSize ; i++) {
                Requirement pReq = (Requirement) reqCovered.elementAt(i);
                if (pReq instanceof ReqLeaf){
                    if (!filtredreqCovered.contains(pReq)){
                        int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                        if ((reqP | filtre) == filtre){
                            filtredreqCovered.add(pReq);
                        }
                    }
                }
            }
            updateReqUse(filtredreqCovered);
            updateChart(filtredreqCovered, false);
        }
    }

    void InitData(Campaign pCamp, boolean updateSQL) {


        pCurrentCampaign = pCamp;
        Vector reqCoveredWrapper;
        reqCovered.clear();
        filtredreqCovered.clear();
        nbReqTotal = 0;

        int filtre = pFiltrePanel.getFiltre();

        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            reqCoveredWrapper = Requirement.getReqWrapperCoveredByCamp(pCamp.getIdBdd());
            int size = reqCoveredWrapper.size();
            for (int j = 0 ; j < size ; j++) {
                ReqWrapper pReqWrapper = (ReqWrapper)reqCoveredWrapper.elementAt(j);
                DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReqWrapper);
                Requirement pReq = null;
                if (node != null){
                    pReq = (Requirement) node.getUserObject();
                    if (!reqCovered.contains(pReq)){
                        reqCovered.add(pReq);
                    }
                    if (pReq instanceof ReqLeaf){
                        if (!filtredreqCovered.contains(pReq)){
                            int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                            if ((reqP | filtre) == filtre){
                                filtredreqCovered.add(pReq);
                            }
                        }
                    }
                } else {
                    pReqTree.reload();
                    node = pReqTree.findRequirementFromParent(pReqWrapper);
                    if (node != null){
                        pReq = (Requirement) node.getUserObject();
                        if (!reqCovered.contains(pReq)){
                            reqCovered.add(pReq);
                        }
                        if (pReq instanceof ReqLeaf){
                            if (!filtredreqCovered.contains(pReq)){
                                int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                                if ((reqP | filtre) == filtre){
                                    filtredreqCovered.add(pReq);
                                }
                            }
                        }
                    } else {
                        //Hum Data corruption !!!
                        Tools.ihmExceptionView(new Exception("Hum ??, it seem that data integrity are corrupted"));

                    }
                }
            }
            // Init table data
            /*updateReqUse(reqCovered);
              updateChart(reqCovered);
            */
            updateReqUse(filtredreqCovered);
            updateChart(filtredreqCovered, updateSQL);

            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e){
            try {
                SQLWrapper.rollBackTrans(transNumber);
            } catch (Exception e1){

            }
            Tools.ihmExceptionView(e);
        }
    }


    void updateReqUse(Vector reqCovered){

        int reqSize = reqCovered.size();
        ((SimpleTableModel)reqTable.getModel()).clearTable();

        for (int i = 0 ; i < reqSize; i ++){
            Requirement pReq  = (Requirement) reqCovered.elementAt(i);
            Vector data = new Vector();
            //data.add(pReq.getNameFromModel());
            data.add("" + pReq.getIdBdd());
            data.add(pReq.getLongName());
            /*if (pReq.getDescriptionFromModel().length()> 60){
              data.add(pReq.getDescriptionFromModel().substring(0, 59)+"...");
              } else {
              data.add(pReq.getDescriptionFromModel());
              }*/
            data.add(ReqWrapper.getCatString(((ReqLeaf)pReq).getCatFromModel()));
            data.add(ReqWrapper.getComplexString(((ReqLeaf)pReq).getComplexeFromModel()));
            data.add(ReqWrapper.getStateString(((ReqLeaf)pReq).getStateFromModel()));
            //20100118 - D\ufffdbut modification Forge ORTF
            data.add(ReqWrapper.getValidByString(((ReqLeaf)pReq).getValidByFromModel()));
            data.add(ReqWrapper.getCriticalString(((ReqLeaf)pReq).getCriticalFromModel()));
            data.add(ReqWrapper.getUnderstandingString(((ReqLeaf)pReq).getUnderstandingFromModel()));
            data.add(ReqWrapper.getOwnerString(((ReqLeaf)pReq).getOwnerFromModel()));
            //20100118 - Fin modification Forge ORTF
            ((SimpleTableModel)reqTable.getModel()).insertRow(i,data);
        }
        setColumnSize(reqTable);
    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(viewReqButton)){
            viewReqPerformed(e);
        }
    }



    void viewReqPerformed(ActionEvent e) {
        int selectedRow = reqTable.getSelectedRow();

        if (selectedRow >-1){
            //if (reqCovered != null) {
            if (filtredreqCovered != null) {
                //Requirement pReq = (Requirement)reqCovered.elementAt(selectedRow);
                Requirement pReq = (Requirement)filtredreqCovered.elementAt(selectedRow);

                DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReq);
                if (node != null){
                    //pReqTree.refreshNode(node);
                    ReqPlugin.selectReqTab();
                    pReqTree.setSelectionPath(new TreePath(node.getPath()));

                }
            }
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
            //no rows are selected
            viewReqButton.setEnabled(false);
        } else {
            //int selectedRow = lsm.getMinSelectionIndex();
            viewReqButton.setEnabled(true);
            //selectedRow is selected
        }
    }
    public void setColumnSize(JTable table){
        FontMetrics fm = table.getFontMetrics(table.getFont());
        for (int i = 0 ; i < table.getColumnCount() ; i++)
            {
                int max = 0;
                for (int j = 0 ; j < table.getRowCount() ; j++)
                    {
                        int taille = fm.stringWidth((String)table.getValueAt(j,i));
                        if (taille > max)
                            max = taille;
                    }
                String nom = (String)table.getColumnModel().getColumn(i).getIdentifier();
                int taille = fm.stringWidth(nom);
                if (taille > max)
                    max = taille;
                table.getColumnModel().getColumn(i).setPreferredWidth(max+10);
            }
    }

    void updateChart(Vector reqCovered, boolean doSQL){
        int nbReqCovered = reqCovered.size();
        //int nbReqTotal = 0;

        try {
            if (doSQL){
                nbReqTotal = Requirement.getReqWrapperLeafInCurrentProject().size();
            } else if (nbReqTotal == 0){
                nbReqTotal = Requirement.getReqWrapperLeafInCurrentProject().size();
            }
        } catch (Exception e){
            nbReqTotal = 0;
        }

        dataset = new  DefaultPieDataset();
        if (nbReqTotal > 0) {
            Double nbCovered = new Double( (nbReqCovered*100)/nbReqTotal);
            Double nbNotCovered = new Double(100 -  (nbReqCovered*100)/nbReqTotal);
            dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), nbNotCovered);
            dataset.setValue(Language.getInstance().getText("Exigence_couverte"),nbCovered);
        }
        plot.setDataset(dataset);
    }

    /******************************** Observer  *************************/

    public void update(Observable o, Object arg){
        if (arg instanceof ReqEvent){
            ReqEvent event = (ReqEvent)arg;
            if (event.code == ReqEvent.FILTRE_REQ_CHANGE){
                refreshCurrentDataModel();
            }
        }
    }
}
