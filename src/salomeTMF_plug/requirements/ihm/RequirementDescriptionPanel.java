/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;
import org.objectweb.salome_tmf.api.Api;
//import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.ActionDetailsView;
import org.objectweb.salome_tmf.ihm.main.AttachmentView;
import org.objectweb.salome_tmf.ihm.main.AttachmentViewWindow;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.ViewCampaignOfTest;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.htmleditor.EkitCore;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
/**
 * @covers SFD_ForgeORTF_EXG_CRE_000120 - \ufffd2.7.8
 * Jira : FORTF - 39
 *
 */
/**
 * @covers SFD_ForgeORTF_EXG_IMP_000400
 * @covers SFD_ForgeORTF_EXG_IMP_000410
 * @covers SFD_ForgeORTF_EXG_IMP_000420 Jira : FORTF - 43
 */
public class RequirementDescriptionPanel extends JPanel implements Observer,
                                                                   ListSelectionListener, ActionListener {

    // JTextPane requirementDescription;
    EkitCore pRequirementDescription;
    HistoryPanel pHistoryPanel;
    JPanel requirementIDPanel;
    // JPanel requirementIDFamPanel;
    JPanel requirementInfoPanel;

    public JComboBox priorityBox;
    public Vector itemPriority;

    public JComboBox catBox;
    public Vector itemCat;

    public JComboBox complexeBox;
    public Vector itemComplex;

    public JComboBox stateBox;
    public Vector itemState;

    JTextField versionField;
    // Vector versionList;
    JComboBox origineField;
    Vector origineList;
    JComboBox verifmodeField;
    Vector verifList;
    JComboBox referenceField;
    Vector referenceList;

    JComboBox comboFiltreChoice;
    JComboBox comboNameChoice;
    JButton changeVersion;

    JTabbedPane requirementWorkSpace;
    AttachmentView attachment;
    String lastDes;

    Requirement pRequirement;
    Requirement poldRequirement;

    // boolean tabChanged = false;
    boolean isPie3D = false;

    JTable testReqFamilyTable;
    JTable testReqLeafTable;
    JTable satisfactionTable;

    Vector testCover;
    DynamicTree pDynamicTree;

    JButton viewTestFamilyReqButton;
    JButton viewCampFamilyReqButton;
    JPanel coverFamilyPanel;

    JButton viewTestReqLeafButton;
    JButton viewCampReqLeafButton;
    JPanel coverLeafPanel;

    JPanel details;

    JButton viewResExecReqButton;
    JButton viewCampButton;
    JPanel statisfactionPanel;
    JLabel coverTestRepresend;
    JLabel satisfaction;

    ChartPanel chartPanAllCover;
    PiePlot3D plotAllCover;
    DefaultPieDataset datasetAllCover;

    ChartPanel chartPanFamilyCover;
    PiePlot3D plotFamilyCover;
    DefaultPieDataset datasetFamilyCover;

    // ChartPanel chartPanSatisCover;
    JTabbedPane defectTabPanel;

    PiePlot3D plotSatisCover;
    DefaultPieDataset datasetSatisCover;

    boolean canDoUpdatePrirotity = false;
    boolean canDoUpdateNameChoise = true;
    boolean canDoUpdateState = false;
    boolean canDoUpdateComplexe = false;
    boolean canDoUpdateCat = false;
    // 20100115 - D\ufffdbut modification Forge ORTF
    boolean canDoUpdateValidBy = false;
    boolean canDoUpdateCritical = false;
    boolean canDoUpdateUnderstanding = false;
    boolean canDoUpdateOwner = false;
    // 20100115 - Fin modification Forge ORTF
    StatRequirement pStatRequirement;

    /* Vartiable used for computation (refhresh by updateData */
    Vector satisfactionTableData = new Vector();
    Vector satisfactionTableFiltredData = new Vector();

    // Vector leafInCurrentFamily;
    // Hashtable coverReqsInCurrentFamily = new Hashtable();
    FiltrePanel pFiltrePanel;
    RequirementTree pReqTree;
    public InfoFiltrePanel pInfoFiltrePanel;

    // 20100115 - D\ufffdbut modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;

    final String REQUIREMENTS_CFG_FILE = "/plugins/requirements/cfg/Requirements.properties";

    public JComboBox validByBox;
    public Vector itemValidBy;

    public JComboBox criticalBox;
    public Vector itemCritical;

    public JComboBox understandingBox;
    public Vector itemUnderstanding;

    public JComboBox ownerBox;
    public Vector itemOwner;

    public RequirementDescriptionPanel(StatRequirement _pStatRequirement,
                                       FiltrePanel _pFiltrePanel, RequirementTree reqTree,
                                       InfoFiltrePanel _pInfoFiltrePanel) {
        super(new BorderLayout());
        pFiltrePanel = _pFiltrePanel;
        pReqTree = reqTree;
        pStatRequirement = _pStatRequirement;
        pInfoFiltrePanel = _pInfoFiltrePanel;
        initDBAndSQLEngine();
        initComponent();
        add(requirementWorkSpace, BorderLayout.CENTER);
    }

    void initDBAndSQLEngine() {
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
    }

    void initComponent() {
        testCover = new Vector();
        requirementWorkSpace = new JTabbedPane();

        // A hierarchy listener is added, because the relevant data is 
        // handled in another hierarchy (under the tab "manage campaigns"!
        // 
        // As of the same reason, a change listener is not used (because the
        // change is made in another tab!)
        requirementWorkSpace.addHierarchyListener(new HierarchyListener() {

            @Override
            public void hierarchyChanged(HierarchyEvent e) {
                try {
                    pStatRequirement.update(pRequirement, pFiltrePanel.getFilter(), true);
                    if ((pRequirement != null) && (pRequirement.isFamilyReq())) {
                        updateData(pFiltrePanel.getFiltre(),
                                   (SimpleTableModel) testReqFamilyTable
                                   .getModel());
                    } else {
                        updateData(pFiltrePanel.getFiltre(),
                                   (SimpleTableModel) testReqLeafTable.getModel());
                    }
                    upDateHistory(pRequirement);
                } catch (Exception ex) {
                    Logger.getLogger(RequirementDescriptionPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
                
        attachment = new AttachmentView(null, -1, -1, null,
                                        DataConstants.NORMAL_SIZE_FOR_ATTACH, null);

        details = initDetailPanel();

        coverLeafPanel = initCoverLeafPanel();
        coverFamilyPanel = initCoverFamilyPanel();
        chartPanAllCover = initChartPanAllCover();
        statisfactionPanel = initSatisfactionReqPanel();
        // requirementWorkSpace.addTab(Language.getInstance().getText("Details"),details)
        // ;
        // requirementWorkSpace.addTab(Language.getInstance().getText("Attachements"),
        // attachment);
        // requirementWorkSpace.addTab(Language.getInstance().getText("Couverture"),
        // coverFamilyPanel);
        requirementWorkSpace.addTab(Language.getInstance()
                                    .getText("Couverture"), chartPanAllCover);
    }

    JPanel initDetailPanel() {
        Properties prop = new Properties();
        try {
            URL _urlBase = SalomeTMFContext.getInstance().getUrlBase();
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            URL urlBase = new URL(url_txt);

            java.net.URL url_cfg = new java.net.URL(urlBase
                                                    + REQUIREMENTS_CFG_FILE);
            try {
                prop = Util.getPropertiesFile(url_cfg);
            } catch (Exception e) {
                prop = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Iterator it = prop.keySet().iterator();
        JPanel jp_details = new JPanel(new BorderLayout());
        itemPriority = new Vector();
        itemCat = new Vector();
        itemState = new Vector();
        itemComplex = new Vector();
        itemValidBy = new Vector();
        itemCritical = new Vector();
        itemUnderstanding = new Vector();
        itemOwner = new Vector();

        String sql = "";
        while (it.hasNext()) {
            String property = (String) it.next();
            if (property.equals("Categorie")) {
                String[] categories = prop.getProperty(property).split(",");
                for (int i = 0; i < categories.length; i++) {
                    sql = "SELECT * FROM REQ_CATEGORY where name='" + categories[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_CATEGORY (name,user_entry) values ('"
                                + categories[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemCat.add(categories[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Priorite")) {
                String[] priorities = prop.getProperty(property).split(",");
                for (int i = 0; i < priorities.length; i++) {
                    sql = "SELECT * FROM REQ_PRIORITY where name='" + priorities[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            if (priorities[i].startsWith("P0")) {
                                sql = "INSERT INTO REQ_PRIORITY (id,name,user_entry) values (1000,'"
                                    + priorities[i] + "',0);";
                                prep = db.prepareStatement(sql);
                                prep.executeUpdate();
                            } else if (priorities[i].startsWith("P1")) {
                                sql = "INSERT INTO REQ_PRIORITY (id,name,user_entry) values (100,'"
                                    + priorities[i] + "',0);";
                                prep = db.prepareStatement(sql);
                                prep.executeUpdate();
                            } else if (priorities[i].startsWith("P2")) {
                                sql = "INSERT INTO REQ_PRIORITY (id,name,user_entry) values (10,'"
                                    + priorities[i] + "',0);";
                                prep = db.prepareStatement(sql);
                                prep.executeUpdate();
                            } else if (priorities[i].startsWith("P3")) {
                                sql = "INSERT INTO REQ_PRIORITY (id,name,user_entry) values (1,'"
                                    + priorities[i] + "',0);";
                                prep = db.prepareStatement(sql);
                                prep.executeUpdate();
                            } else {
                                sql = "INSERT INTO REQ_PRIORITY (name,user_entry) values ('"
                                    + priorities[i] + "',0);";
                                prep = db.prepareStatement(sql);
                                prep.executeUpdate();
                            }
                        }
                        itemPriority.add(priorities[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }

                }
            } else if (property.equals("Complexite")) {
                String[] complexes = prop.getProperty(property).split(",");
                for (int i = 0; i < complexes.length; i++) {
                    sql = "SELECT * FROM REQ_COMPLEXE where name='" + complexes[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_COMPLEXE (name,user_entry) values ('"
                                + complexes[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemComplex.add(complexes[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Statut")) {
                String[] states = prop.getProperty(property).split(",");
                for (int i = 0; i < states.length; i++) {
                    sql = "SELECT * FROM REQ_STATE where name='" + states[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_STATE (name,user_entry) values ('"
                                + states[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemState.add(states[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Valide")) {
                String[] validBys = prop.getProperty(property).split(",");
                for (int i = 0; i < validBys.length; i++) {
                    sql = "SELECT * FROM REQ_VALID_BY where name='" + validBys[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_VALID_BY (name,user_entry) values ('"
                                + validBys[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemValidBy.add(validBys[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Criticite")) {
                String[] criticals = prop.getProperty(property).split(",");
                for (int i = 0; i < criticals.length; i++) {
                    sql = "SELECT * FROM REQ_CRITICAL where name='" + criticals[i]
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_CRITICAL (name,user_entry) values ('"
                                + criticals[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemCritical.add(criticals[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Comprehension")) {
                String[] understandings = prop.getProperty(property).split(",");
                for (int i = 0; i < understandings.length; i++) {
                    sql = "SELECT * FROM REQ_UNDERSTANDING where name='"
                        + understandings[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_UNDERSTANDING (name,user_entry) values ('"
                                + understandings[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();

                        }
                        itemUnderstanding.add(understandings[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } else if (property.equals("Proprietaire")) {
                String[] owners = prop.getProperty(property).split(",");
                for (int i = 0; i < owners.length; i++) {
                    sql = "SELECT * FROM REQ_OWNER where name='" + owners[i] + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()) {
                            sql = "INSERT INTO REQ_OWNER (name,user_entry) values ('"
                                + owners[i] + "',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                        itemOwner.add(owners[i]);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

        sql = "SELECT id,name FROM REQ_CATEGORY where user_entry=1;";
        List<String> userCategories = new ArrayList<String>();
        List<Integer> userCategoriesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String category = result.getString(2);
                int id = result.getInt(1);
                userCategories.add(category);
                userCategoriesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userCategoriesId.size(); i++) {
            int id = userCategoriesId.get(i);
            try {
                sql = "select count(cat) from REQUIREMENTS where cat=" + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbCat = result.getInt(1);
                    if (nbCat != 0) {
                        itemCat.add(userCategories.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_PRIORITY where user_entry=1;";
        List<String> userPriorities = new ArrayList<String>();
        List<Integer> userPrioritiesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String priority = result.getString(2);
                int id = result.getInt(1);
                userPriorities.add(priority);
                userPrioritiesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userPrioritiesId.size(); i++) {
            int id = userPrioritiesId.get(i);
            try {
                sql = "select count(priority) from REQUIREMENTS where priority="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbPrio = result.getInt(1);
                    if (nbPrio != 0) {
                        itemPriority.add(userPriorities.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_COMPLEXE where user_entry=1;";
        List<String> userComplexes = new ArrayList<String>();
        List<Integer> userComplexesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String complexe = result.getString(2);
                int id = result.getInt(1);
                userComplexes.add(complexe);
                userComplexesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userComplexesId.size(); i++) {
            int id = userComplexesId.get(i);
            try {
                sql = "select count(complexe) from REQUIREMENTS where complexe="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbComp = result.getInt(1);
                    if (nbComp != 0) {
                        itemComplex.add(userComplexes.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_STATE where user_entry=1;";
        List<String> userStates = new ArrayList<String>();
        List<Integer> userStatesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String state = result.getString(2);
                int id = result.getInt(1);
                userStates.add(state);
                userStatesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userStatesId.size(); i++) {
            int id = userStatesId.get(i);
            try {
                sql = "select count(state) from REQUIREMENTS where state=" + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbState = result.getInt(1);
                    if (nbState != 0) {
                        itemState.add(userStates.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_VALID_BY where user_entry=1;";
        List<String> userValidBy = new ArrayList<String>();
        List<Integer> userValidById = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String validBy = result.getString(2);
                int id = result.getInt(1);
                userValidBy.add(validBy);
                userValidById.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userValidById.size(); i++) {
            int id = userValidById.get(i);
            try {
                sql = "select count(valid_by) from REQUIREMENTS where valid_by="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbValid = result.getInt(1);
                    if (nbValid != 0) {
                        itemValidBy.add(userValidBy.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_CRITICAL where user_entry=1;";
        List<String> userCritical = new ArrayList<String>();
        List<Integer> userCriticalId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String critical = result.getString(2);
                int id = result.getInt(1);
                userCritical.add(critical);
                userCriticalId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userCriticalId.size(); i++) {
            int id = userCriticalId.get(i);
            try {
                sql = "select count(critical) from REQUIREMENTS where critical="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbCrit = result.getInt(1);
                    if (nbCrit != 0) {
                        itemCritical.add(userCritical.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_UNDERSTANDING where user_entry=1;";
        List<String> userUnderstanding = new ArrayList<String>();
        List<Integer> userUnderstandingId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String understanding = result.getString(2);
                int id = result.getInt(1);
                userUnderstanding.add(understanding);
                userUnderstandingId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userUnderstandingId.size(); i++) {
            int id = userUnderstandingId.get(i);
            try {
                sql = "select count(understanding) from REQUIREMENTS where understanding="
                    + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbUnder = result.getInt(1);
                    if (nbUnder != 0) {
                        itemUnderstanding.add(userUnderstanding.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        sql = "SELECT id,name FROM REQ_OWNER where user_entry=1;";
        List<String> userOwner = new ArrayList<String>();
        List<Integer> userOwnerId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()) {
                String owner = result.getString(2);
                int id = result.getInt(1);
                userOwner.add(owner);
                userOwnerId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0; i < userOwnerId.size(); i++) {
            int id = userOwnerId.get(i);
            try {
                sql = "select count(owner) from REQUIREMENTS where owner=" + id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    int nbOwner = result.getInt(1);
                    if (nbOwner != 0) {
                        itemOwner.add(userOwner.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        requirementIDPanel = new JPanel();
        requirementIDPanel.setLayout(new GridLayout(4, 4));

        JLabel labelCat = new JLabel(Language.getInstance()
                                     .getText("Categorie")
                                     + " :");
        // Collections.sort(itemCat);
        catBox = new JComboBox(itemCat);
        catBox.setEditable(true);
        catBox.addActionListener(this);
        //              catBox.setEnabled(Permission.canUpdateReq());

        JLabel labelPriority = new JLabel(Language.getInstance().getText(
                                                                         "Priority")
                                          + " :");
        // Collections.sort(itemPriority);
        priorityBox = new JComboBox(itemPriority);
        priorityBox.setEditable(true);
        priorityBox.addActionListener(this);
        //              priorityBox.setEnabled(Permission.canUpdateReq());

        JLabel labelComplexe = new JLabel(Language.getInstance().getText(
                                                                         "Complexite")
                                          + " :");
        // Collections.sort(itemComplex);
        complexeBox = new JComboBox(itemComplex);
        complexeBox.setEditable(true);
        complexeBox.addActionListener(this);
        //              complexeBox.setEnabled(Permission.canUpdateReq());Cs

        JLabel labelState = new JLabel(Language.getInstance().getText("Etat")
                                       + " :");
        // Collections.sort(itemState);
        stateBox = new JComboBox(itemState);
        stateBox.setEditable(true);
        stateBox.addActionListener(this);
        //              stateBox.setEnabled(Permission.canUpdateReq());

        JLabel labelValidBy = new JLabel(Language.getInstance().getText(
                                                                        "Valide_par")
                                         + " :");
        // Collections.sort(itemValidBy);
        validByBox = new JComboBox(itemValidBy);
        validByBox.setEditable(true);
        validByBox.addActionListener(this);
        //              validByBox.setEnabled(Permission.canUpdateReq());

        JLabel labelCritical = new JLabel(Language.getInstance().getText(
                                                                         "Criticite")
                                          + " :");
        // Collections.sort(itemCritical);
        criticalBox = new JComboBox(itemCritical);
        criticalBox.setEditable(true);
        criticalBox.addActionListener(this);
        //              criticalBox.setEnabled(Permission.canUpdateReq());

        JLabel labelUnderstanding = new JLabel(Language.getInstance().getText(
                                                                              "Comprehension")
                                               + " :");
        // Collections.sort(itemUnderstanding);
        understandingBox = new JComboBox(itemUnderstanding);
        understandingBox.setEditable(true);
        understandingBox.addActionListener(this);
        //              understandingBox.setEnabled(Permission.canUpdateReq());

        JLabel labelOwner = new JLabel(Language.getInstance().getText(
                                                                      "Proprietaire")
                                       + " :");
        // Collections.sort(itemOwner);
        ownerBox = new JComboBox(itemOwner);
        ownerBox.setEditable(true);
        ownerBox.addActionListener(this);
        //              ownerBox.setEnabled(Permission.canUpdateReq());

        requirementIDPanel.add(labelCat);
        requirementIDPanel.add(labelPriority);
        requirementIDPanel.add(labelComplexe);
        requirementIDPanel.add(labelCritical);
        requirementIDPanel.add(catBox);
        requirementIDPanel.add(priorityBox);
        requirementIDPanel.add(complexeBox);
        requirementIDPanel.add(criticalBox);
        requirementIDPanel.add(labelUnderstanding);
        requirementIDPanel.add(labelOwner);
        requirementIDPanel.add(labelState);
        requirementIDPanel.add(labelValidBy);
        requirementIDPanel.add(understandingBox);
        requirementIDPanel.add(ownerBox);
        requirementIDPanel.add(stateBox);
        requirementIDPanel.add(validByBox);
        requirementIDPanel.setBorder(BorderFactory.createTitledBorder(
                                                                      BorderFactory.createLineBorder(Color.BLACK), "id : "));
        // 20100115 - Fin modification Forge ORTF

        // JPanel desc_histoPanel = new JPanel(new BorderLayout());
        JPanel desc_histoPanel = new JPanel(new GridLayout(2, 1));
        /*
         * requirementDescription = new JTextPane();
         * requirementDescription.setBorder
         * (BorderFactory.createTitledBorder(BorderFactory
         * .createLineBorder(Color.BLACK),"Description"));
         */

        pRequirementDescription = new EkitCore(pReqTree, true /*Permission.canUpdateReq()*/);
        pHistoryPanel = new HistoryPanel();
        // desc_histoPanel.add(requirementDescription, BorderLayout.CENTER);

        /*
         * JSplitPane jspltDisplay = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
         * jspltDisplay.setTopComponent(pRequirementDescription);
         * jspltDisplay.setBottomComponent(pHistoryPanel);
         */
        desc_histoPanel.add(pRequirementDescription, BorderLayout.CENTER);
        desc_histoPanel.add(pHistoryPanel);
        // desc_histoPanel.add(jspltDisplay, BorderLayout.CENTER);

        requirementInfoPanel = new JPanel();
        requirementInfoPanel.setLayout(new GridLayout(2, 6));
        requirementInfoPanel.setBorder(BorderFactory.createTitledBorder(
                                                                        BorderFactory.createLineBorder(Color.BLACK), "Information"));

        origineList = new Vector();
        origineList.add(Language.getInstance().getText("Origine_value_0"));
        origineList.add(Language.getInstance().getText("Origine_value_1"));
        origineList.add(Language.getInstance().getText("Origine_value_2"));
        origineList.add(Language.getInstance().getText("Origine_value_3"));

        verifList = new Vector();
        verifList.add(Language.getInstance().getText("Verif_value_0"));
        verifList.add(Language.getInstance().getText("Verif_value_1"));
        verifList.add(Language.getInstance().getText("Verif_value_2"));
        verifList.add(Language.getInstance().getText("Verif_value_3"));

        referenceList = new Vector();
        referenceList.add(Language.getInstance().getText("Reference_value_0"));
        referenceList.add(Language.getInstance().getText("Reference_value_1"));
        referenceList.add(Language.getInstance().getText("Reference_value_2"));
        referenceList.add(Language.getInstance().getText("Reference_value_3"));

        JLabel labelOrigine = new JLabel("Origine : ");
        origineField = new JComboBox();
        origineField.setEditable(true);
        //              origineField.setEnabled(Permission.canUpdateReq());

        JLabel labelVersion = new JLabel("Version : ");
        versionField = new JTextField();
        //              versionField.setEnabled(Permission.canUpdateReq());

        JLabel labelReference = new JLabel(Language.getInstance().getText(
                                                                          "Reference")
                                           + " : ");
        referenceField = new JComboBox();
        referenceField.setEditable(true);
        //              referenceField.setEnabled(Permission.canUpdateReq());

        JLabel labelVerif = new JLabel(Language.getInstance().getText(
                                                                      "Mode_de_verification"));
        verifmodeField = new JComboBox();
        verifmodeField.setEditable(true);
        //              verifmodeField.setEnabled(Permission.canUpdateReq());

        changeVersion = new JButton(Language.getInstance().getText("Valider"));
        changeVersion.addActionListener(this);
        //              changeVersion.setEnabled(Permission.canUpdateReq());

        requirementInfoPanel.add(labelOrigine);
        requirementInfoPanel.add(origineField);
        // requirementInfoPanel.add(Box.createRigidArea(new Dimension(5,5)));
        requirementInfoPanel.add(labelReference);
        requirementInfoPanel.add(referenceField);
        // requirementInfoPanel.add(Box.createRigidArea(new Dimension(5,5)));
        requirementInfoPanel.add(labelVersion);
        requirementInfoPanel.add(versionField);

        requirementInfoPanel.add(labelVerif);
        requirementInfoPanel.add(verifmodeField);
        requirementInfoPanel.add(Box.createRigidArea(new Dimension(5, 5)));
        requirementInfoPanel.add(Box.createRigidArea(new Dimension(5, 5)));
        requirementInfoPanel.add(Box.createRigidArea(new Dimension(5, 5)));
        requirementInfoPanel.add(changeVersion);

        jp_details.add(requirementIDPanel, BorderLayout.NORTH);
        // jp_details.add(requirementDescription,BorderLayout.CENTER);
        jp_details.add(desc_histoPanel, BorderLayout.CENTER);
        jp_details.add(requirementInfoPanel, BorderLayout.SOUTH);

        /*
         * requirementInfoPanel = new JPanel();
         * requirementInfoPanel.setLayout(new GridLayout(1, 6)); JLabel
         * labelPriority = new JLabel("Priority : "); priorityBox = new
         * JComboBox(itemPriority); priorityBox.setEditable(false);
         * priorityBox.addActionListener(this); JLabel labelVersion = new
         * JLabel("Version : "); versionField = new JTextField(); changeVersion
         * = new JButton(Language.getInstance().getText("Valider"));
         * changeVersion.addActionListener(this);
         *
         * requirementInfoPanel.add(labelPriority);
         * requirementInfoPanel.add(priorityBox);
         * requirementInfoPanel.add(Box.createRigidArea(new Dimension(15,15)));
         * requirementInfoPanel.add(labelVersion);
         * requirementInfoPanel.add(versionField);
         * requirementInfoPanel.add(changeVersion);
         * requirementInfoPanel.setBorder
         * (BorderFactory.createTitledBorder(BorderFactory
         * .createLineBorder(Color.BLACK),"Information"));
         *
         * requirementDescription = new JTextPane();
         * requirementDescription.setBorder
         * (BorderFactory.createTitledBorder(BorderFactory
         * .createLineBorder(Color.BLACK),"Description"));
         * //requirementDescription.addFocusListener(new
         * requirementDescriptionFocusListener());
         *
         * jp_details.add(requirementInfoPanel,BorderLayout.NORTH);
         * jp_details.add(requirementDescription,BorderLayout.CENTER);
         */
        return jp_details;
    }

    JPanel initSatisfactionReqPanel() {
        JPanel jp_sat_Req = new JPanel(new GridLayout(2, 1));

        viewResExecReqButton = new JButton(Language.getInstance().getText(
                                                                          "Visualiser")
                                           + " " + Language.getInstance().getText("Derniere_Execution"));
        viewResExecReqButton.addActionListener(this);
        viewCampButton = new JButton(Language.getInstance().getText(
                                                                    "Visualiser")
                                     + " " + Language.getInstance().getText("Campagne"));
        viewCampButton.addActionListener(this);
        /*
         * viewCampFamilyReqButton = new
         * JButton(Language.getInstance().getText("Campagne"));
         * viewCampFamilyReqButton.addActionListener(this);
         */
        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2));
        buttonsPanel.add(viewResExecReqButton);
        buttonsPanel.add(viewCampButton);
        // buttonsPanel.add(viewCampFamilyReqButton);

        SimpleTableModel model = new SimpleTableModel();

        satisfactionTable = new JTable(model);

        model.addColumn(Language.getInstance().getText("Exigence"));
        model.addColumn(Language.getInstance().getText("Test"));
        model.addColumn(Language.getInstance().getText("Campagnes") + "/"
                        + Language.getInstance().getText("Executions"));
        model
            .addColumn(Language.getInstance().getText(
                                                      "Resultats_d_execution"));
        model.addColumn(Language.getInstance().getText("Derniere_Execution"));

        satisfactionTable.setPreferredScrollableViewportSize(new Dimension(600,
                                                                           200));
        satisfactionTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = satisfactionTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        JScrollPane tablePane = new JScrollPane(satisfactionTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        JPanel infoSatisCoverPanel = new JPanel(new BorderLayout());
        JPanel fitrePanel = initFiltreSatisCoverPanel();
        infoSatisCoverPanel.add(fitrePanel, BorderLayout.NORTH);
        infoSatisCoverPanel.add(buttonsPanel, BorderLayout.SOUTH);
        infoSatisCoverPanel.add(tablePane, BorderLayout.CENTER);
        // chartPanSatisCover = initChartPanSatisCover();

        JPanel defectPanel = new JPanel(new BorderLayout());
        JPanel labelPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JLabel defectLabel = new JLabel(Language.getInstance().getText(
                                                                       "Anomalies"));
        labelPanel.add(defectLabel);
        defectTabPanel = new JTabbedPane();
        defectPanel.add(labelPanel, BorderLayout.NORTH);
        defectPanel.add(defectTabPanel, BorderLayout.CENTER);

        jp_sat_Req.add(infoSatisCoverPanel);
        // jp_sat_Req.add(chartPanSatisCover);
        jp_sat_Req.add(defectPanel);

        return jp_sat_Req;
    }

    JPanel initFiltreSatisCoverPanel() {

        JPanel filtreSatisCoverPanel = new JPanel(new GridLayout(2, 3));

        JLabel filtreByLabel = new JLabel(Language.getInstance().getText(
                                                                         "Filtrer_par")
                                          + " : ");
        Vector choice = new Vector();
        choice.add(Language.getInstance().getText("Tout"));
        choice.add(Language.getInstance().getText("Campagnes"));
        choice.add(Language.getInstance().getText("Campagnes") + "/"
                   + Language.getInstance().getText("Executions"));
        choice.add(Language.getInstance().getText("Environnements"));
        choice.add(Language.getInstance().getText("Campagnes") + "/"
                   + Language.getInstance().getText("Environnements"));
        comboFiltreChoice = new JComboBox(choice);
        comboFiltreChoice.setEditable(false);
        comboFiltreChoice.addActionListener(this);

        JLabel nameLabel = new JLabel(Language.getInstance().getText("Nom")
                                      + " : ");

        Vector choice2 = new Vector();
        choice2.add(Language.getInstance().getText("Tout"));
        comboNameChoice = new JComboBox(choice2);
        comboNameChoice.setEditable(false);
        comboNameChoice.addActionListener(this);

        coverTestRepresend = new JLabel(Language.getInstance().getText(
                                                                       "Test_couvert_represente")
                                        + ": 100%");

        satisfaction = new JLabel(Language.getInstance()
                                  .getText("Satisfaction"));

        filtreSatisCoverPanel.add(filtreByLabel);
        filtreSatisCoverPanel.add(nameLabel);
        filtreSatisCoverPanel.add(coverTestRepresend);

        filtreSatisCoverPanel.add(comboFiltreChoice);
        filtreSatisCoverPanel.add(comboNameChoice);
        filtreSatisCoverPanel.add(satisfaction);

        return filtreSatisCoverPanel;
    }

    JPanel initCoverFamilyPanel() {
        JPanel jp_cover_fam = new JPanel(new GridLayout(2, 1));

        viewTestFamilyReqButton = new JButton(Language.getInstance().getText(
                                                                             "Visualiser"));
        viewTestFamilyReqButton.addActionListener(this);
        viewCampFamilyReqButton = new JButton(Language.getInstance().getText(
                                                                             "Campagne"));
        viewCampFamilyReqButton.addActionListener(this);

        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2));
        buttonsPanel.add(viewTestFamilyReqButton);
        buttonsPanel.add(viewCampFamilyReqButton);

        SimpleTableModel model = new SimpleTableModel();

        testReqFamilyTable = new JTable(model);

        model.addColumn(Language.getInstance().getText("Famille"));
        model.addColumn(Language.getInstance().getText("Suite"));
        model.addColumn(Language.getInstance().getText("Test"));
        model.addColumn(Language.getInstance().getText("Execute"));

        testReqFamilyTable.setPreferredScrollableViewportSize(new Dimension(
                                                                            600, 200));
        testReqFamilyTable
            .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // ListSelectionModel rowSM = testTable.getSelectionModel();
        // rowSM.addListSelectionListener(this);

        JScrollPane tablePane = new JScrollPane(testReqFamilyTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        JPanel infoTestCoverPanel = new JPanel(new BorderLayout());
        infoTestCoverPanel.add(buttonsPanel, BorderLayout.NORTH);
        infoTestCoverPanel.add(tablePane, BorderLayout.CENTER);
        chartPanFamilyCover = initChartPanFamilyCover();
        jp_cover_fam.add(infoTestCoverPanel);
        jp_cover_fam.add(chartPanFamilyCover);

        return jp_cover_fam;
    }

    JPanel initCoverLeafPanel() {

        viewTestReqLeafButton = new JButton(Language.getInstance().getText(
                                                                           "Visualiser"));
        viewTestReqLeafButton.addActionListener(this);
        viewCampReqLeafButton = new JButton(Language.getInstance().getText(
                                                                           "Campagne"));
        viewCampReqLeafButton.addActionListener(this);

        JPanel buttonsPanel = new JPanel(new GridLayout(1, 2));
        buttonsPanel.add(viewTestReqLeafButton);
        buttonsPanel.add(viewCampReqLeafButton);

        SimpleTableModel model = new SimpleTableModel();

        testReqLeafTable = new JTable(model);

        model.addColumn(Language.getInstance().getText("Famille"));
        model.addColumn(Language.getInstance().getText("Suite"));
        model.addColumn(Language.getInstance().getText("Test"));
        model.addColumn(Language.getInstance().getText("Execute"));

        testReqLeafTable.setPreferredScrollableViewportSize(new Dimension(600,
                                                                          200));
        testReqLeafTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // ListSelectionModel rowSM = testTable.getSelectionModel();
        // rowSM.addListSelectionListener(this);

        JScrollPane tablePane = new JScrollPane(testReqLeafTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        JPanel infoTestCoverPanel = new JPanel(new BorderLayout());
        infoTestCoverPanel.add(buttonsPanel, BorderLayout.NORTH);
        infoTestCoverPanel.add(tablePane, BorderLayout.CENTER);
        return infoTestCoverPanel;
    }

    ChartPanel initChartPanSatisCover() {
        datasetSatisCover = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(Language.getInstance()
                                                         .getText("Satisfaction"), // chart title
                                                         datasetSatisCover, // data
                                                         true, // include legend
                                                         true, false);

        plotSatisCover = (PiePlot3D) chart.getPlot();
        plotSatisCover.setStartAngle(290);
        plotSatisCover.setDirection(Rotation.CLOCKWISE);
        plotSatisCover.setForegroundAlpha(0.5f);
        plotSatisCover.setNoDataMessage("No data to display");
        return new ChartPanel(chart);

    }

    ChartPanel initChartPanFamilyCover() {
        datasetFamilyCover = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(Language.getInstance()
                                                         .getText("Exigence_couverte"), // chart title
                                                         datasetFamilyCover, // data
                                                         true, // include legend
                                                         true, false);

        plotFamilyCover = (PiePlot3D) chart.getPlot();
        plotFamilyCover.setStartAngle(290);
        plotFamilyCover.setDirection(Rotation.CLOCKWISE);
        plotFamilyCover.setForegroundAlpha(0.5f);
        plotFamilyCover.setNoDataMessage("No data to display");
        return new ChartPanel(chart);
    }

    ChartPanel initChartPanAllCover() {
        datasetAllCover = new DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(Language.getInstance()
                                                         .getText("Exigence_couverte"), // chart title
                                                         datasetAllCover, // data
                                                         true, // include legend
                                                         true, false);

        plotAllCover = (PiePlot3D) chart.getPlot();
        plotAllCover.setStartAngle(290);
        plotAllCover.setDirection(Rotation.CLOCKWISE);
        plotAllCover.setForegroundAlpha(0.5f);
        plotAllCover.setNoDataMessage("No data to display");
        // chartPanAllCover = new ChartPanel(chart);
        return new ChartPanel(chart);
    }

    void setPie3D(int filtre) {
        if (isPie3D)
            return;
        isPie3D = true;
        /*
         * requirementWorkSpace.remove(details) ;
         * requirementWorkSpace.remove(attachment);
         * requirementWorkSpace.remove(coverFamilyPanel);
         */
        requirementWorkSpace.removeAll();
        calcStatAllCover(filtre);
        requirementWorkSpace.addTab(Language.getInstance()
                                    .getText("Couverture"), chartPanAllCover);

    }

    void unsetPie3D(int filtre, Requirement pReq) {
        // if (!isPie3D)
        // return;
        isPie3D = false;
        // requirementWorkSpace.remove(chartPanAllCover);
        int selectedTab = requirementWorkSpace.getSelectedIndex();
        requirementWorkSpace.removeAll();
        requirementWorkSpace.addTab(Language.getInstance().getText("Details"),
                                    details);
        requirementWorkSpace.addTab(Language.getInstance().getText(
                                                                   "Attachements"), attachment);
        if (pReq.isFamilyReq()) {
            calcStatFamilyCover(filtre);
            requirementWorkSpace.addTab(Language.getInstance().getText(
                                                                       "Couverture"), coverFamilyPanel);
        } else {
            requirementWorkSpace.addTab(Language.getInstance().getText(
                                                                       "Couverture"), coverLeafPanel);
        }
        requirementWorkSpace.addTab(Language.getInstance().getText(
                                                                   "Satisfaction"), statisfactionPanel);
        if (selectedTab < requirementWorkSpace.getTabCount()) {
            requirementWorkSpace.setSelectedIndex(selectedTab);
        }
    }

    public void update(Observable o, Object arg) {
        // System.out.println("requirementdescription-update : ReqEvent.REQSELECTED");
        if (arg instanceof ReqEvent) {
            // System.out.println("requirementdescription-update : ReqEvent.REQSELECTED");
            ReqEvent event = (ReqEvent) arg;
            if (event.code == ReqEvent.REQSELECTED) {
                poldRequirement = pRequirement;
                pRequirement = (Requirement) event.arg;
                if (!pRequirement.getNameFromModel().equals(
                                                            "Requirements_"
                                                            + ReqPlugin.getProjectRef().getNameFromModel())) {
                    attachment.setAttachmentObject(pRequirement);
                    if (pRequirement.isFamilyReq()) {
                        updateData(pFiltrePanel.getFiltre(),
                                   (SimpleTableModel) testReqFamilyTable
                                   .getModel());
                    } else {
                        updateData(pFiltrePanel.getFiltre(),
                                   (SimpleTableModel) testReqLeafTable.getModel());
                    }
                    upDateHistory(pRequirement);
                }

                if (!pRequirement.getNameFromModel().equals(
                                                            "Requirements_"
                                                            + ReqPlugin.getProjectRef().getNameFromModel())) {
                    // requirementDescription.setText(pRequirement.getDescriptionFromModel());
                    pRequirementDescription.setText(pRequirement
                                                    .getDescriptionFromModel());
                }

                if (pRequirement instanceof ReqLeaf) {
                    requirementInfoPanel.setVisible(true);
                    requirementIDPanel.setBorder(BorderFactory
                                                 .createTitledBorder(BorderFactory
                                                                     .createLineBorder(Color.BLACK), "id : "
                                                                     + pRequirement.getIdBdd()));
                    requirementIDPanel.setVisible(true);
                    pRequirementDescription.setBorder(BorderFactory
                                                      .createRaisedBevelBorder());

                    versionField.setText(((ReqLeaf) pRequirement)
                                         .getVersionFromModel());
                    setInfo(referenceField, referenceList,
                            ((ReqLeaf) pRequirement).getReferenceFromModel());
                    setInfo(origineField, origineList, ((ReqLeaf) pRequirement)
                            .getOrigineFromModel());
                    setInfo(verifmodeField, verifList, ((ReqLeaf) pRequirement)
                            .getVerifFromModel());

                    // 20100115 - D\ufffdbut modification Forge ORTF
                    int idRequirement = ((ReqLeaf) pRequirement).getIdBdd();
                    System.out.println("idRequirement : "+idRequirement);
                    String sql = "select priority,complexe,state,cat,valid_by,critical,understanding,owner from REQUIREMENTS where id_req="
                        + idRequirement + ";";
                    List<Integer> listPropriete = new ArrayList<Integer>();
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()){
                            listPropriete.add(result.getInt(1));
                            listPropriete.add(result.getInt(2));
                            listPropriete.add(result.getInt(3));
                            listPropriete.add(result.getInt(4));
                            listPropriete.add(result.getInt(5));
                            listPropriete.add(result.getInt(6));
                            listPropriete.add(result.getInt(7));
                            listPropriete.add(result.getInt(8));
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                    canDoUpdatePrirotity = false;
                    //                                  int priority = ((ReqLeaf) pRequirement)
                    //                                                  .getPriorityFromModel();
                    int priority = listPropriete.get(0);
                    sql = "SELECT name FROM REQ_PRIORITY where id='" + priority
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String priorite = result.getString(1);
                            priorityBox.setSelectedItem(priorite);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdatePrirotity = true;

                    canDoUpdateComplexe = false;
                    //                                  int complexe = ((ReqLeaf) pRequirement)
                    //                                                  .getComplexeFromModel();
                    int complexe = listPropriete.get(1);
                    sql = "SELECT name FROM REQ_COMPLEXE where id='" + complexe
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String complex = result.getString(1);
                            complexeBox.setSelectedItem(complex);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateComplexe = true;

                    canDoUpdateState = false;
                    int state = listPropriete.get(2);
                    sql = "SELECT name FROM REQ_STATE where id='" + state + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String etat = result.getString(1);
                            stateBox.setSelectedItem(etat);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateState = true;

                    canDoUpdateCat = false;
                    int category = listPropriete.get(3);
                    sql = "SELECT name FROM REQ_CATEGORY where id='" + category
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String cat = result.getString(1);
                            catBox.setSelectedItem(cat);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateCat = true;

                    canDoUpdateValidBy = false;
                    int validBy = listPropriete.get(4);
                    sql = "SELECT name FROM REQ_VALID_BY where id='" + validBy
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String valid = result.getString(1);
                            validByBox.setSelectedItem(valid);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateValidBy = true;

                    canDoUpdateCritical = false;
                    int critical = listPropriete.get(5);
                    sql = "SELECT name FROM REQ_CRITICAL where id='" + critical
                        + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String critique = result.getString(1);
                            criticalBox.setSelectedItem(critique);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateCritical = true;

                    canDoUpdateUnderstanding = false;
                    int understanding = listPropriete.get(6);
                    sql = "SELECT name FROM REQ_UNDERSTANDING where id='"
                        + understanding + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String comprehension = result.getString(1);
                            understandingBox.setSelectedItem(comprehension);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateUnderstanding = true;

                    canDoUpdateOwner = false;
                    int owner = listPropriete.get(7);
                    sql = "SELECT name FROM REQ_OWNER where id='" + owner + "';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (result.next()) {
                            String proprietaire = result.getString(1);
                            ownerBox.setSelectedItem(proprietaire);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    canDoUpdateOwner = true;
                    // 20100115 - D\ufffdbut modification Forge ORTF

                } else {
                    requirementInfoPanel.setVisible(false);
                    requirementIDPanel.setVisible(false);
                    pRequirementDescription.setBorder(BorderFactory
                                                      .createTitledBorder(BorderFactory
                                                                          .createLineBorder(Color.BLACK), "id : "
                                                                          + pRequirement.getIdBdd()));
                }

                if (pRequirement.getParent() == null) {
                    setPie3D(pFiltrePanel.getFiltre());
                } else {
                    unsetPie3D(pFiltrePanel.getFiltre(), pRequirement);
                }
            } else if (event.code == ReqEvent.FILTRE_REQ_CHANGE) {
                if (pRequirement != null && pRequirement.getParent() != null) {
                    calcStatAllCover(pFiltrePanel.getFiltre());
                }

            }
        }
    }

    void updateData(int filtre, SimpleTableModel pTestTable) {
        int typeSelected = comboFiltreChoice.getSelectedIndex();
        String nameStelected = (String) comboNameChoice.getSelectedItem();
        canDoUpdateNameChoise = false;
        if (typeSelected > 0) {
            comboNameChoice.removeAllItems();
            comboNameChoice.addItem(Language.getInstance().getText("Tout"));
        }

        pTestTable.clearTable();
        SimpleTableModel satisfactionTableModel = (SimpleTableModel) satisfactionTable
            .getModel();
        satisfactionTableModel.clearTable();
        satisfactionTableData.clear();
        satisfactionTableFiltredData.clear();

        Hashtable executedStatCovredByCurrentReq = pStatRequirement
            .getEexecutedStatCovredByCurrentReq();
        Enumeration enumCovredTest = pStatRequirement
            .getAllTestCovredByCurrentReq().elements();
        testCover.clear();
        int i = 0;
        int j = 0;
        int nbLastPass = 0;
        Vector listTest = new Vector();
        while (enumCovredTest.hasMoreElements()) {
            Test pTest = (Test) enumCovredTest.nextElement();
            Vector data = new Vector();
            testCover.add(pTest);
            String famName = pTest.getTestListFromModel().getFamilyFromModel()
                .getNameFromModel();
            String suiteName = pTest.getTestListFromModel().getNameFromModel();
            String testName = pTest.getNameFromModel();
            data.add(famName);
            data.add(pTest.getTestListFromModel().getNameFromModel());
            data.add(testName);
            boolean executed = false;
            Vector statExec = (Vector) executedStatCovredByCurrentReq
                .get(pTest);
            if (statExec != null) {
                // statExec = Vector[campain, Execution, {ExecResult}, nbP, nbF,
                // nbU, lastExecRusult, lastResult]
                executed = true;
                int size = statExec.size();
                for (int k = 0; k < size; k++) {
                    Vector statExecLine = (Vector) statExec.elementAt(k);
                    Vector data2 = new Vector();
                    String str_listOfReq = "";
                    Hashtable hashTestReq = pStatRequirement
                        .getAllReqCovredByTest();
                    Vector listOfReq = (Vector) hashTestReq.get(new Integer(
                                                                            pTest.getIdBdd()));
                    if (listOfReq != null && listOfReq.size() > 0) {
                        str_listOfReq = ((Requirement) listOfReq.elementAt(0))
                            .getNameFromModel();
                        for (int l = 1; l < listOfReq.size(); l++) {
                            str_listOfReq += ", "
                                + ((Requirement) listOfReq.elementAt(l))
                                .getNameFromModel();
                        }
                    }
                    data2.add(str_listOfReq);
                    data2
                        .add(testName + " (" + famName + "/" + suiteName
                             + ")");
                    if (!listTest.contains(pTest)) {
                        listTest.add(pTest);
                    }
                    Campaign pCamp = (Campaign) statExecLine.elementAt(0);
                    Execution pExec = (Execution) statExecLine.elementAt(1);
                    int nbPass = ((Integer) statExecLine.elementAt(3))
                        .intValue();
                    int nbFail = ((Integer) statExecLine.elementAt(4))
                        .intValue();
                    int nbUnknow = ((Integer) statExecLine.elementAt(5))
                        .intValue();
                    int nbNotApplicable = ((Integer) statExecLine.elementAt(6))
                        .intValue();
                    int nbBlocked = ((Integer) statExecLine.elementAt(7))
                        .intValue();
                    int nbNone = ((Integer) statExecLine.elementAt(8))
                        .intValue();
                    String lastResult = (String) statExecLine.elementAt(10);
                    if (pExec == null) {
                        data2.add(pCamp.getNameFromModel() + "/"
                                  + "Null");
                    } else {
                        data2.add(pCamp.getNameFromModel() + "/"
                                  + pExec.getNameFromModel());
                    }
                    data2.add("" + nbPass + "P, " + nbFail + "F, " + nbUnknow
                              + "I" + nbNotApplicable + "A" + 
                            nbBlocked + "B" + nbNone + "N");
                    data2.add(lastResult);
                    if ((lastResult != null) && (lastResult.equals(ApiConstants.SUCCESS))) {
                        nbLastPass++;
                    }
                    satisfactionTableModel.insertRow(j, data2);
                    satisfactionTableData.add(j, statExecLine);
                    satisfactionTableFiltredData.add(j, statExecLine);
                    j++;
                    // Set campagne, exec, env Filtres //remplir des hashtables
                }
            }
            // boolean executed =
            // pStatRequirement.isCovredTestIsExecuted(pTest);
            data.add("" + executed);
            pTestTable.insertRow(i, data);
            i++;
        }
        float pourcentSat = 0;
        if (j != 0) {
            pourcentSat = nbLastPass * 100 / j;
        }
        // satisfaction.setText(Language.getInstance().getText("Satisfaction") +
        // " : " + pourcentSat +"%");

        int nbTest = pStatRequirement.getAllTestCovredByCurrentReq().size();
        int nbTest2 = pStatRequirement.getEexecutedStatCovredByCurrentReq()
            .size();
        float pourcentTest = 0;
        float pourcentTest2 = 0;
        if (nbTest != 0) {
            pourcentTest = listTest.size() * 100 / nbTest;
        }
        if (nbTest2 != 0) {
            pourcentTest2 = listTest.size() * 100 / nbTest2;
        }
        coverTestRepresend.setText(Language.getInstance().getText(
                                                                  "Test_couvert_represente")
                                   + ": " + pourcentTest + "%");
        satisfaction.setText(Language.getInstance().getText("Satisfaction")
                             + " : " + pourcentSat + "% / " + pourcentTest2 + "% "
                             + Language.getInstance().getText("Execute"));
        canDoUpdateNameChoise = true;
        if (typeSelected > 0) {
            changeFiltreChoicePerformed(null);
            int nbName = comboNameChoice.getItemCount();
            if (nbName > 0) {
                int index = 0;
                boolean trouve = false;
                while (index < nbName && !trouve) {
                    String tempName = (String) comboNameChoice.getItemAt(index);
                    if (tempName.equals(nameStelected)) {
                        trouve = true;
                        comboNameChoice.setSelectedIndex(index);
                    }
                    index++;
                }
            }
        }

        Vector allTrackers = SalomeTMFContext.getInstance().getBugTracker();
        defectTabPanel.removeAll();
        int size = allTrackers.size();
        for (int k = 0; k < size; k++) {
            BugTracker pBugTracker = (BugTracker) allTrackers.elementAt(k);
            JPanel pJPanel = pBugTracker.getDefectPanelForTests(listTest);
            if (pJPanel != null) {
                defectTabPanel.addTab(pBugTracker.getBugTrackerName(), pJPanel);
            }
        }
        setColumnSize(testReqFamilyTable);
        setColumnSize(testReqLeafTable);
        setColumnSize(satisfactionTable);
    }

    void upDateHistory(Requirement pReq) {
        pHistoryPanel.updateData(pReq, false);
    }

    void calcStatFamilyCover(int filtre) {
        try {

            int nbTotalReq = 0;
            int nbCoveredReq = 0;
            if (filtre > 0) {
                // nbTotalReq = leafInCurrentFamily.size();
                nbTotalReq = pStatRequirement.getLeafInCurrentFamily().size();
                // int nbTotalReq2 = 0;
                for (int i = 0; i < nbTotalReq; i++) {
                    // ReqLeaf pReqLeaf = (ReqLeaf)
                    // leafInCurrentFamily.elementAt(i);
                    ReqLeaf pReqLeaf = (ReqLeaf) pStatRequirement
                        .getLeafInCurrentFamily().elementAt(i);

                    // int reqP = pReqLeaf.getPriorityFromModel();
                    // if ((reqP & filtre) > 0){
                    // nbTotalReq2++;
                    /*
                     * if (coverReqsInCurrentFamily.get(new
                     * Integer(pReqLeaf.getIdBdd())) != null){ nbCoveredReq++; }
                     */
                    if (pStatRequirement.getAllTestCovredByReq().get(
                                                                     new Integer(pReqLeaf.getIdBdd())) != null) {
                        nbCoveredReq++;
                    }
                    // }

                }
                // nbTotalReq = nbTotalReq2;

            }

            // int nbTotalReq =
            // Requirement.getReqWrapperLeafInCurrentProject().size();
            // int nbCoveredReq = Requirement.getReqWrapperCovered().size();
            if (nbTotalReq > 0) {
                Double nbCovered = new Double((nbCoveredReq * 100) / nbTotalReq);
                Double nbNotCovered = new Double(100 - (nbCoveredReq * 100)
                                                 / nbTotalReq);
                datasetFamilyCover.setValue(Language.getInstance().getText(
                                                                           "Exigence_non_couverte"), nbNotCovered);
                datasetFamilyCover.setValue(Language.getInstance().getText(
                                                                           "Exigence_couverte"), nbCovered);
                plotFamilyCover.setDataset(datasetFamilyCover);
            }

        } catch (Exception e) {

        }
    }

    void initCalcStatAllCover(int filtre) {
        try {
            int nbTotalReq = 1;
            int nbCoveredReq = 0;
            if (filtre > 0) {
                Vector leafInCurrentProject = Requirement
                    .getReqWrapperLeafInCurrentProject();
                nbTotalReq = leafInCurrentProject.size();

                Vector reqWrapperCovered = Requirement.getReqWrapperCovered();
                nbCoveredReq = reqWrapperCovered.size();
                if (filtre != 111) {
                    int nbTotalReq2 = 0;
                    for (int i = 0; i < nbTotalReq; i++) {
                        int reqP = ((ReqWrapper) leafInCurrentProject
                                    .elementAt(i)).getPriority();
                        if ((reqP & filtre) > 0) {
                            nbTotalReq2++;
                        }
                    }
                    int nbCoveredReq2 = 0;
                    for (int i = 0; i < nbCoveredReq; i++) {
                        int reqP = ((ReqWrapper) reqWrapperCovered.elementAt(i))
                            .getPriority();
                        if ((reqP & filtre) > 0) {
                            nbCoveredReq2++;
                        }
                    }
                    nbTotalReq = nbTotalReq2;
                    nbCoveredReq = nbCoveredReq2;
                }
            }

            if (nbTotalReq > 0) {
                Double nbCovered = new Double((nbCoveredReq * 100) / nbTotalReq);
                Double nbNotCovered = new Double(100 - (nbCoveredReq * 100)
                                                 / nbTotalReq);
                datasetAllCover.setValue(Language.getInstance().getText(
                                                                        "Exigence_non_couverte"), nbNotCovered);
                datasetAllCover.setValue(Language.getInstance().getText(
                                                                        "Exigence_couverte"), nbCovered);
                plotAllCover.setDataset(datasetAllCover);
            }

        } catch (Exception e) {

        }
    }

    void calcStatAllCover(int filtre) {
        try {
            /*
             * int nbTotalReq = 1; int nbCoveredReq = 0; if (filtre> 0){ Vector
             * leafInCurrentProject =
             * Requirement.getReqWrapperLeafInCurrentProject(); nbTotalReq =
             * leafInCurrentProject.size();
             *
             * Vector reqWrapperCovered = Requirement.getReqWrapperCovered();
             * nbCoveredReq = reqWrapperCovered.size(); if (filtre != 111){ int
             * nbTotalReq2 = 0; for (int i = 0 ; i < nbTotalReq; i++){ int reqP
             * = ((ReqWrapper) leafInCurrentProject.elementAt(i)).getPriority();
             * if ((reqP & filtre) > 0){ nbTotalReq2++; } } int nbCoveredReq2 =
             * 0; for (int i = 0 ; i < nbCoveredReq; i++){ int reqP =
             * ((ReqWrapper) reqWrapperCovered.elementAt(i)).getPriority(); if
             * ((reqP & filtre) > 0){ nbCoveredReq2++; } } nbTotalReq =
             * nbTotalReq2; nbCoveredReq = nbCoveredReq2; } }
             */

            int nbTotalReq = 1;
            int nbCoveredReq = 0;
            if (filtre > 0) {
                // nbTotalReq = leafInCurrentFamily.size();
                nbTotalReq = pStatRequirement.getLeafInCurrentFamily().size();
                // int nbTotalReq2 = 0;
                for (int i = 0; i < nbTotalReq; i++) {
                    // ReqLeaf pReqLeaf = (ReqLeaf)
                    // leafInCurrentFamily.elementAt(i);
                    ReqLeaf pReqLeaf = (ReqLeaf) pStatRequirement
                        .getLeafInCurrentFamily().elementAt(i);

                    // int reqP = pReqLeaf.getPriorityFromModel();
                    // if ((reqP & filtre) > 0){
                    // nbTotalReq2++;
                    /*
                     * if (coverReqsInCurrentFamily.get(new
                     * Integer(pReqLeaf.getIdBdd())) != null){ nbCoveredReq++; }
                     */
                    if (pStatRequirement.getAllTestCovredByReq().get(
                                                                     new Integer(pReqLeaf.getIdBdd())) != null) {
                        nbCoveredReq++;
                    }
                    // }

                }
                // nbTotalReq = nbTotalReq2;

            }

            if (nbTotalReq > 0) {
                Double nbCovered = new Double((nbCoveredReq * 100) / nbTotalReq);
                Double nbNotCovered = new Double(100 - (nbCoveredReq * 100)
                                                 / nbTotalReq);
                datasetAllCover.setValue(Language.getInstance().getText(
                                                                        "Exigence_non_couverte"), nbNotCovered);
                datasetAllCover.setValue(Language.getInstance().getText(
                                                                        "Exigence_couverte"), nbCovered);
                plotAllCover.setDataset(datasetAllCover);
            }

        } catch (Exception e) {

        }
    }

    void setRequirement(Requirement pRequirement) {
        poldRequirement = this.pRequirement;
        this.pRequirement = pRequirement;
        if (!pRequirement.getNameFromModel().equals(
                                                    "Requirements_" + ReqPlugin.getProjectRef().getNameFromModel())) {
            attachment.setAttachmentObject(pRequirement);
            if (pRequirement.isFamilyReq()) {
                updateData(pFiltrePanel.getFiltre(),
                           (SimpleTableModel) testReqFamilyTable.getModel());
            } else {
                updateData(pFiltrePanel.getFiltre(),
                           (SimpleTableModel) testReqLeafTable.getModel());
            }
        }
        if (pRequirement.getParent() == null) {
            setPie3D(pFiltrePanel.getFiltre());
        } else {
            unsetPie3D(pFiltrePanel.getFiltre(), pRequirement);
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(viewTestFamilyReqButton)) {
            viewTestPerformed(e, testReqFamilyTable);
        } else if (e.getSource().equals(viewCampFamilyReqButton)) {
            viewCampPerformed(e, testReqFamilyTable);
        } else if (e.getSource().equals(viewTestReqLeafButton)) {
            viewTestPerformed(e, testReqLeafTable);
        } else if (e.getSource().equals(viewCampReqLeafButton)) {
            viewCampPerformed(e, testReqLeafTable);
        } else if (e.getSource().equals(priorityBox)) {
            priorityBoxPerformed(e);
        } else if (e.getSource().equals(stateBox)) {
            stateBoxPerformed(e);
        } else if (e.getSource().equals(complexeBox)) {
            complexeBoxPerformed(e);
        } else if (e.getSource().equals(catBox)) {
            catBoxPerformed(e);
        } else if (e.getSource().equals(validByBox)) {
            validByBoxPerformed(e);
        } else if (e.getSource().equals(criticalBox)) {
            criticalBoxPerformed(e);
        } else if (e.getSource().equals(understandingBox)) {
            understandingBoxPerformed(e);
        } else if (e.getSource().equals(ownerBox)) {
            ownerBoxPerformed(e);
        } else if (e.getSource().equals(changeVersion)) {
            changeVersionPerformed(e);
        } else if (e.getSource().equals(comboNameChoice)) {
            changeNamePerformed(e);
        } else if (e.getSource().equals(comboFiltreChoice)) {
            changeFiltreChoicePerformed(e);
        } else if (e.getSource().equals(viewResExecReqButton)) {
            viewResExecReqButtonPerformed(e);
        } else if (e.getSource().equals(viewCampButton)) {
            viewCampButtonButtonPerformed(e);
        }
    }

    void setTestTree(DynamicTree _pDynamicTree) {
        pDynamicTree = _pDynamicTree;
    }

    void viewCampButtonButtonPerformed(ActionEvent e) {
        int selectedRow = satisfactionTable.getSelectedRow();
        if (selectedRow > -1) {
            Vector statExecLine = (Vector) satisfactionTableFiltredData
                .elementAt(selectedRow);
            Campaign pCamp = (Campaign) statExecLine.elementAt(0);
            DataModel.view(pCamp);
        }
    }

    void viewResExecReqButtonPerformed(ActionEvent e) {
        // Get selected
        int selectedIndex = satisfactionTable.getSelectedRow();
        if (selectedIndex > -1) {
            Vector statExecLine = (Vector) satisfactionTableFiltredData
                .elementAt(selectedIndex);
            ExecutionResult lastExecutionResult = (ExecutionResult) statExecLine
                .elementAt(6);
            Test pTest = (Test) statExecLine.elementAt(8);
            DataModel.setCurrentExecutionTestResult(lastExecutionResult
                                                    .getExecutionTestResultFromModel(pTest));
            DataModel.setObervedExecutionResult(lastExecutionResult);
            if (pTest instanceof ManualTest) {
                new ActionDetailsView((ManualTest) pTest, lastExecutionResult
                                      .getExecution(), lastExecutionResult,
                                      lastExecutionResult
                                      .getExecutionTestResultFromModel(pTest));
            } else {
                new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(),
                                         DataConstants.EXECUTION_RESULT_TEST, DataModel
                                         .getCurrentExecutionTestResult());
            }
        }
    }

    void changeNamePerformed(ActionEvent e) {
        if (!canDoUpdateNameChoise) {
            return;
        }
        int choice = comboFiltreChoice.getSelectedIndex();
        Vector data = null;
        Enumeration keys;
        boolean trouve = false;
        String name = (String) comboNameChoice.getSelectedItem();
        if (choice == 0) {
            // ALL
            // USE satisfactionTableData
            reloadDefaultSatisfactionTable();
        } else if (choice == 1) {
            // Camp
            keys = pStatRequirement.executedStatByCamp.keys();
            while (keys.hasMoreElements() && !trouve) {
                Campaign pCamp = (Campaign) keys.nextElement();
                if (pCamp.getNameFromModel().equals(name)) {
                    trouve = true;
                    data = (Vector) pStatRequirement.executedStatByCamp
                        .get(pCamp);
                }
            }
        } else if (choice == 2) {
            // Exex
            keys = pStatRequirement.executedStatByExec.keys();
            while (keys.hasMoreElements() && !trouve) {
                Execution pExec = (Execution) keys.nextElement();
                String longName = pExec.getCampagneFromModel()
                    .getNameFromModel()
                    + "/" + pExec.getNameFromModel();
                if (longName.equals(name)) {
                    trouve = true;
                    data = (Vector) pStatRequirement.executedStatByExec
                        .get(pExec);
                }
            }
        } else if (choice == 3) {
            // Env
            keys = pStatRequirement.executedStatByEnv.keys();
            while (keys.hasMoreElements() && !trouve) {
                Environment pEnv = (Environment) keys.nextElement();
                if (pEnv.getNameFromModel().equals(name)) {
                    trouve = true;
                    data = (Vector) pStatRequirement.executedStatByEnv
                        .get(pEnv);
                }
            }
        } else if (choice == 4) {
            // Exex
            keys = pStatRequirement.executedStatByExec.keys();
            while (keys.hasMoreElements()) {
                Execution pExec = (Execution) keys.nextElement();
                String longName = pExec.getCampagneFromModel()
                    .getNameFromModel()
                    + "/"
                    + pExec.getEnvironmentFromModel().getNameFromModel();
                if (longName.equals(name)) {
                    if (data == null) {
                        data = new Vector();
                    }
                    Vector data2 = (Vector) pStatRequirement.executedStatByExec
                        .get(pExec);
                    int size = data2.size();
                    for (int i = 0; i < size; i++) {
                        data.add(data2.elementAt(i));
                    }
                }
            }
        }

        if (data != null) {
            // loadFromFiltre(data);
            loadSatisfactionTable(data);
        } else {
            reloadDefaultSatisfactionTable();
        }
    }

    void changeFiltreChoicePerformed(ActionEvent e) {
        int choice = comboFiltreChoice.getSelectedIndex();

        // if e != null
        canDoUpdateNameChoise = false;
        comboNameChoice.removeAllItems();
        comboNameChoice.addItem(Language.getInstance().getText("Tout"));
        Enumeration data = null;

        if (choice == 0) {

        } else if (choice == 1) {
            // Camp
            data = pStatRequirement.executedStatByCamp.keys();
        } else if (choice == 2) {
            // Exex
            data = pStatRequirement.executedStatByExec.keys();
        } else if (choice == 3) {
            // Env
            data = pStatRequirement.executedStatByEnv.keys();
        } else if (choice == 4) {
            // Env
            data = pStatRequirement.executedStatByExec.keys();
        }
        if (data != null) {
            Vector unique = new Vector();
            while (data.hasMoreElements()) {
                if (choice == 1) {
                    // Camp
                    Campaign pCamp = (Campaign) data.nextElement();
                    comboNameChoice.addItem(pCamp.getNameFromModel());
                } else if (choice == 2) {
                    // Exex
                    Execution pExec = (Execution) data.nextElement();
                    comboNameChoice.addItem(pExec.getCampagneFromModel()
                                            .getNameFromModel()
                                            + "/" + pExec.getNameFromModel());
                } else if (choice == 3) {
                    // Env
                    Environment pEnv = (Environment) data.nextElement();
                    comboNameChoice.addItem(pEnv.getNameFromModel());
                } else if (choice == 4) {
                    Execution pExec = (Execution) data.nextElement();
                    String name = pExec.getCampagneFromModel()
                        .getNameFromModel()
                        + "/"
                        + pExec.getEnvironmentFromModel()
                        .getNameFromModel();
                    if (!unique.contains(name)) {
                        comboNameChoice.addItem(pExec.getCampagneFromModel()
                                                .getNameFromModel()
                                                + "/"
                                                + pExec.getEnvironmentFromModel()
                                                .getNameFromModel());
                        unique.add(name);
                    }
                }
            }
        }
        canDoUpdateNameChoise = true;
        // end if e!=null

        // SimpleTableModel satisfactionTableModel = (SimpleTableModel)
        // satisfactionTable.getModel();
        // if (satisfactionTableData.size() !=
        // satisfactionTableModel.getRowCount()){
        reloadDefaultSatisfactionTable();
        // }

    }

    /*
     * void loadFromFiltre(Vector filtredData){
     * satisfactionTableFiltredData.clear(); SimpleTableModel
     * satisfactionTableModel = (SimpleTableModel) satisfactionTable.getModel();
     * satisfactionTableModel.clearTable(); int nbLastPass = 0; Vector listTest
     * = new Vector(); int size = filtredData.size(); for (int k=0; k < size ;
     * k++){ Vector statExecLine = (Vector) filtredData.elementAt(k); Vector
     * data2 = new Vector(); Test pTest = (Test) statExecLine.elementAt(8); if
     * (!listTest.contains(pTest)){ listTest.add(pTest); } String famName =
     * pTest.getTestListFromModel().getFamilyFromModel().getNameFromModel();
     * String suiteName = pTest.getTestListFromModel().getNameFromModel();
     * String testName = pTest.getNameFromModel();
     *
     * data2.add(testName + " (" + famName + "/" + suiteName + ")"); Campaign
     * pCamp = (Campaign) statExecLine.elementAt(0); Execution pExec =
     * (Execution) statExecLine.elementAt(1); int nbPass = ((Integer)
     * statExecLine.elementAt(3)).intValue(); int nbFail = ((Integer)
     * statExecLine.elementAt(4)).intValue(); int nbUnknow = ((Integer)
     * statExecLine.elementAt(5)).intValue(); String lastResult = (String)
     * statExecLine.elementAt(7); data2.add(pCamp.getNameFromModel() + "/" +
     * pExec.getNameFromModel()); data2.add("" + nbPass + "P, " +nbFail + "F, "
     * +nbUnknow + "I"); data2.add(lastResult); if
     * (lastResult.equals(ApiConstants.SUCCESS)){ nbLastPass++; }
     * satisfactionTableModel.insertRow(k,data2);
     * satisfactionTableFiltredData.add(k, statExecLine); //j++; //Set campagne,
     * exec, env Filtres //remplir des hashtables } float pourcentSat = 0; if
     * (size!=0){ pourcentSat= nbLastPass*100/size; }
     * //satisfaction.setText(Language.getInstance().getText("Satisfaction") +
     * " : " + pourcentSat+"%"); int nbTest =
     * pStatRequirement.getAllTestCovredByCurrentReq().size(); int nbTest2 =
     * pStatRequirement.getEexecutedStatCovredByCurrentReq().size(); float
     * pourcentTest = 0; float pourcentTest2 = 0; if (nbTest!=0){ pourcentTest =
     * listTest.size()*100/nbTest; } if (nbTest2 != 0){ pourcentTest2 =
     * listTest.size()*100/nbTest2; }
     * coverTestRepresend.setText(Language.getInstance
     * ().getText("Test_couvert_represente") + ": " + pourcentTest+"%" );
     * satisfaction.setText(Language.getInstance().getText("Satisfaction") +
     * " : " + pourcentSat+"% / " + pourcentTest2 +"% " +
     * Language.getInstance().getText("Execute"));
     *
     * }
     */

    void reloadDefaultSatisfactionTable() {
        loadSatisfactionTable(satisfactionTableData);
        /*
         * int size = satisfactionTableData.size(); Vector listTest = new
         * Vector(); satisfactionTableFiltredData.clear(); SimpleTableModel
         * satisfactionTableModel = (SimpleTableModel)
         * satisfactionTable.getModel(); satisfactionTableModel.clearTable();
         * int nbLastPass = 0; for (int k=0; k < size ; k++){ Vector
         * statExecLine = (Vector) satisfactionTableData.elementAt(k); Vector
         * data2 = new Vector(); Test pTest = (Test) statExecLine.elementAt(8);
         * if (!listTest.contains(pTest)){ listTest.add(pTest); } String famName
         * =
         * pTest.getTestListFromModel().getFamilyFromModel().getNameFromModel();
         * String suiteName = pTest.getTestListFromModel().getNameFromModel();
         * String testName = pTest.getNameFromModel();
         *
         * data2.add(testName + " (" + famName + "/" + suiteName + ")");
         * Campaign pCamp = (Campaign) statExecLine.elementAt(0); Execution
         * pExec = (Execution) statExecLine.elementAt(1); int nbPass =
         * ((Integer) statExecLine.elementAt(3)).intValue(); int nbFail =
         * ((Integer) statExecLine.elementAt(4)).intValue(); int nbUnknow =
         * ((Integer) statExecLine.elementAt(5)).intValue(); String lastResult =
         * (String) statExecLine.elementAt(7);
         * data2.add(pCamp.getNameFromModel() + "/" + pExec.getNameFromModel());
         * data2.add("" + nbPass + "P, " +nbFail + "F, " +nbUnknow + "I");
         * data2.add(lastResult); if (lastResult.equals(ApiConstants.SUCCESS)){
         * nbLastPass++; } satisfactionTableModel.insertRow(k,data2);
         * satisfactionTableFiltredData.add(k, statExecLine); //j++; //Set
         * campagne, exec, env Filtres //remplir des hashtables } float
         * pourcentSat = 0; if (size!=0){ pourcentSat= nbLastPass*100/size; }
         *
         * coverTestRepresend.setText(Language.getInstance().getText(
         * "Test_couvert_represente") + ": 100%" ); int nbTest =
         * pStatRequirement.getAllTestCovredByCurrentReq().size(); int nbTest2 =
         * pStatRequirement.getEexecutedStatCovredByCurrentReq().size(); float
         * pourcentTest = 0; float pourcentTest2 = 0; if (nbTest!=0){
         * pourcentTest = listTest.size()*100/nbTest; } if (nbTest2 != 0){
         * pourcentTest2 = listTest.size()*100/nbTest2; }
         * coverTestRepresend.setText
         * (Language.getInstance().getText("Test_couvert_represente") + ": " +
         * pourcentTest+"%");
         * satisfaction.setText(Language.getInstance().getText("Satisfaction") +
         * " : " + pourcentSat+"% / " + pourcentTest2 +"% " +
         * Language.getInstance().getText("Execute"));
         */
    }

    void loadSatisfactionTable(Vector satisfactionData) {
        int size = satisfactionData.size();
        Vector listTest = new Vector();

        satisfactionTableFiltredData.clear();
        SimpleTableModel satisfactionTableModel = (SimpleTableModel) satisfactionTable
            .getModel();
        satisfactionTableModel.clearTable();

        int nbLastPass = 0;
        for (int k = 0; k < size; k++) {
            Vector statExecLine = (Vector) satisfactionData.elementAt(k);
            Vector data2 = new Vector();

            Test pTest = (Test) statExecLine.elementAt(8);
            if (!listTest.contains(pTest)) {
                listTest.add(pTest);
            }
            String str_listOfReq = "";
            Hashtable hashTestReq = pStatRequirement.getAllReqCovredByTest();
            Vector listOfReq = (Vector) hashTestReq.get(new Integer(pTest
                                                                    .getIdBdd()));
            if (listOfReq != null && listOfReq.size() > 0) {
                str_listOfReq = ((Requirement) listOfReq.elementAt(0))
                    .getNameFromModel();
                for (int l = 1; l < listOfReq.size(); l++) {
                    str_listOfReq += ", "
                        + ((Requirement) listOfReq.elementAt(l))
                        .getNameFromModel();
                }
            }
            data2.add(str_listOfReq);
            String famName = pTest.getTestListFromModel().getFamilyFromModel()
                .getNameFromModel();
            String suiteName = pTest.getTestListFromModel().getNameFromModel();
            String testName = pTest.getNameFromModel();

            data2.add(testName + " (" + famName + "/" + suiteName + ")");
            Campaign pCamp = (Campaign) statExecLine.elementAt(0);
            Execution pExec = (Execution) statExecLine.elementAt(1);
            int nbPass = ((Integer) statExecLine.elementAt(3)).intValue();
            int nbFail = ((Integer) statExecLine.elementAt(4)).intValue();
            int nbUnknow = ((Integer) statExecLine.elementAt(5)).intValue();
            int nbNotApplicable = ((Integer) statExecLine.elementAt(6)).intValue();
            int nbBlocked = ((Integer) statExecLine.elementAt(7)).intValue();
            int nbNone = ((Integer) statExecLine.elementAt(8)).intValue();
            String lastResult = (String) statExecLine.elementAt(10);
            data2
                .add(pCamp.getNameFromModel() + "/"
                     + pExec.getNameFromModel());
            data2.add("" + nbPass + "P, " + nbFail + "F, " + nbUnknow + "I" +
                    nbNotApplicable + "A" + nbBlocked + "B" + nbNone + "N");
            data2.add(lastResult);
            if (lastResult.equals(ApiConstants.SUCCESS)) {
                nbLastPass++;
            }
            satisfactionTableModel.insertRow(k, data2);
            satisfactionTableFiltredData.add(k, statExecLine);
            // j++;
            // Set campagne, exec, env Filtres //remplir des hashtables
        }
        float pourcentSat = 0;
        if (size != 0) {
            pourcentSat = nbLastPass * 100 / size;
        }

        coverTestRepresend.setText(Language.getInstance().getText(
                                                                  "Test_couvert_represente")
                                   + ": 100%");
        int nbTest = pStatRequirement.getAllTestCovredByCurrentReq().size();
        int nbTest2 = pStatRequirement.getEexecutedStatCovredByCurrentReq()
            .size();
        float pourcentTest = 0;
        float pourcentTest2 = 0;
        if (nbTest != 0) {
            pourcentTest = listTest.size() * 100 / nbTest;
        }
        if (nbTest2 != 0) {
            pourcentTest2 = listTest.size() * 100 / nbTest2;
        }
        coverTestRepresend.setText(Language.getInstance().getText(
                                                                  "Test_couvert_represente")
                                   + ": " + pourcentTest + "%");
        satisfaction.setText(Language.getInstance().getText("Satisfaction")
                             + " : " + pourcentSat + "% / " + pourcentTest2 + "% "
                             + Language.getInstance().getText("Execute"));

        Vector allTrackers = SalomeTMFContext.getInstance().getBugTracker();
        defectTabPanel.removeAll();
        int size2 = allTrackers.size();
        for (int k = 0; k < size2; k++) {
            BugTracker pBugTracker = (BugTracker) allTrackers.elementAt(k);
            JPanel pJPanel = pBugTracker.getDefectPanelForTests(listTest);
            if (pJPanel != null) {
                defectTabPanel.addTab(pBugTracker.getBugTrackerName(), pJPanel);
            }
        }
    }

    void changeVersionPerformed(ActionEvent e) {
        // if (canDoUpdatePrirotity){
        ReqLeaf pReqLeaf = null;
        if (pRequirement instanceof ReqLeaf) {
            try {
                pReqLeaf = (ReqLeaf) pRequirement;
                pReqLeaf.updateInfoInDBAndModel(versionField.getText().trim(),
                                                ((String) origineField.getSelectedItem()).trim(),
                                                ((String) verifmodeField.getSelectedItem()).trim(),
                                                ((String) referenceField.getSelectedItem()).trim());
                pHistoryPanel.updateData(pReqLeaf, true);
            } catch (Exception ex) {
                ex.printStackTrace();
                if (pReqLeaf != null) {
                    versionField.setText(((ReqLeaf) pRequirement)
                                         .getVersionFromModel());
                    setInfo(referenceField, referenceList,
                            ((ReqLeaf) pRequirement).getReferenceFromModel());
                    setInfo(origineField, origineList, ((ReqLeaf) pRequirement)
                            .getOrigineFromModel());
                    setInfo(verifmodeField, verifList, ((ReqLeaf) pRequirement)
                            .getVerifFromModel());

                }
            }
        }
        // }
    }

    void stateBoxPerformed(ActionEvent e) {
        if (canDoUpdateState) {
            String state = (String) stateBox.getSelectedItem();
            if (!state.contains("\\'")){
                state = state.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_STATE where name='" + state
                + "';";
            int stateSelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    stateSelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemState.contains(state)) {
                        stateBox.addItem(state);
                    }
                } else {
                    sql = "INSERT INTO REQ_STATE (name,user_entry) values ('"
                        + state + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    stateBox.addItem(state);
                    stateBox.setSelectedItem(state);
                    sql = "SELECT id FROM REQ_STATE where name='" + state + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        stateSelected = result.getInt(1);
                    }
                    pInfoFiltrePanel.stateBox.addItem(state);
                }
            } catch (Exception e1) {
                System.out.println("SQL : "+sql);
                e1.printStackTrace();
            }
            int oldstate = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldstate = pReqLeaf.getStateFromModel();
                    if (oldstate == stateSelected) {
                        return;
                    }
                    pReqLeaf.updateStateInDBAndModel(stateSelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldstate != -1) {
                        pReqLeaf.updateStateInModel(oldstate);
                    }
                }
            }
        }
    }

    void catBoxPerformed(ActionEvent e) {
        if (canDoUpdateCat) {
            String category = (String) catBox.getSelectedItem();
            if (!category.contains("\\'")){
                category = category.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_CATEGORY where name='"
                + category + "';";
            int categorySelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    categorySelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemCat.contains(category)) {
                        catBox.addItem(category);
                    }
                } else {
                    sql = "INSERT INTO REQ_CATEGORY (name,user_entry) values ('"
                        + category + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    catBox.addItem(category);
                    catBox.setSelectedItem(category);
                    pInfoFiltrePanel.catBox.addItem(category);
                    sql = "SELECT id FROM REQ_CATEGORY where name='" + category
                        + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        categorySelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                System.out.println("SQL : "+sql);
                e1.printStackTrace();
            }
            int oldcat = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldcat = pReqLeaf.getCatFromModel();
                    if (oldcat == categorySelected) {
                        return;
                    }
                    pReqLeaf.updateCatInDBAndModel(categorySelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldcat != -1) {
                        pReqLeaf.updateCatInModel(oldcat);
                    }
                }
            }
        }
    }

    void complexeBoxPerformed(ActionEvent e) {
        if (canDoUpdateComplexe) {
            String complex = (String) complexeBox.getSelectedItem();
            if (!complex.contains("\\'")){
                complex = complex.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_COMPLEXE where name='"
                + complex + "';";
            int complexeSelected = 100;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    complexeSelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemComplex.contains(complex)) {
                        complexeBox.addItem(complex);
                    }
                } else {
                    sql = "INSERT INTO REQ_COMPLEXE (name,user_entry) values ('"
                        + complex + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    complexeBox.addItem(complex);
                    complexeBox.setSelectedItem(complex);
                    pInfoFiltrePanel.complexeBox.addItem(complex);
                    sql = "SELECT id FROM REQ_COMPLEXE where name='" + complex
                        + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        complexeSelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldComplexe = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldComplexe = pReqLeaf.getComplexeFromModel();
                    if (oldComplexe == complexeSelected) {
                        return;
                    }
                    pReqLeaf.updateComplexeInDBAndModel(complexeSelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldComplexe != -1) {
                        pReqLeaf.updateComplexeInModel(oldComplexe);
                    }
                }
            }
        }
    }

    void priorityBoxPerformed(ActionEvent e) {
        if (canDoUpdatePrirotity) {
            String priority = (String) priorityBox.getSelectedItem();
            if (!priority.contains("\\'")){
                priority = priority.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_PRIORITY where name='"
                + priority + "';";
            int prioritySelected = 100;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    prioritySelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemPriority.contains(priority)) {
                        priorityBox.addItem(priority);
                    }
                } else {
                    sql = "INSERT INTO REQ_PRIORITY (name,user_entry) values ('"
                        + priority + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    priorityBox.addItem(priority);
                    priorityBox.setSelectedItem(priority);
                    sql = "SELECT id FROM REQ_PRIORITY where name='" + priority
                        + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        prioritySelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldPriority = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldPriority = pReqLeaf.getPriorityFromModel();
                    if (oldPriority == prioritySelected) {
                        return;
                    }
                    pReqLeaf.updatePriorityInDBAndModel(prioritySelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldPriority != -1) {
                        pReqLeaf.updatePriorityInModel(oldPriority);
                    }
                }
            }
        }
    }

    void validByBoxPerformed(ActionEvent e) {
        if (canDoUpdateValidBy) {
            String validBy = (String) validByBox.getSelectedItem();
            if (!validBy.contains("\\'")){
                validBy = validBy.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_VALID_BY where name='"
                + validBy + "';";
            int validBySelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    validBySelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemValidBy.contains(validBy)) {
                        validByBox.addItem(validBy);
                    }
                } else {
                    sql = "INSERT INTO REQ_VALID_BY (name,user_entry) values ('"
                        + validBy + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    validByBox.addItem(validBy);
                    validByBox.setSelectedItem(validBy);
                    sql = "SELECT id FROM REQ_VALID_BY where name='" + validBy
                        + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        validBySelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldValidBy = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldValidBy = pReqLeaf.getValidByFromModel();
                    if (oldValidBy == validBySelected) {
                        return;
                    }
                    pReqLeaf.updateValidByInDBAndModel(validBySelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldValidBy != -1) {
                        pReqLeaf.updateValidByInModel(oldValidBy);
                    }
                }
            }
        }
    }

    void criticalBoxPerformed(ActionEvent e) {
        if (canDoUpdateCritical) {
            String critical = (String) criticalBox.getSelectedItem();
            if (!critical.contains("\\'")){
                critical = critical.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_CRITICAL where name='"
                + critical + "';";
            int criticalSelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    criticalSelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemCritical.contains(critical)) {
                        criticalBox.addItem(critical);
                    }
                } else {
                    sql = "INSERT INTO REQ_CRITICAL (name,user_entry) values ('"
                        + critical + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    criticalBox.addItem(critical);
                    criticalBox.setSelectedItem(critical);
                    sql = "SELECT id FROM REQ_CRITICAL where name='" + critical
                        + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        criticalSelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldCritical = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldCritical = pReqLeaf.getCriticalFromModel();
                    if (oldCritical == criticalSelected) {
                        return;
                    }
                    pReqLeaf.updateCriticalInDBAndModel(criticalSelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldCritical != -1) {
                        pReqLeaf.updateCriticalInModel(oldCritical);
                    }
                }
            }
        }
    }

    void understandingBoxPerformed(ActionEvent e) {
        if (canDoUpdateUnderstanding) {
            String understanding = (String) understandingBox.getSelectedItem();
            if (!understanding.contains("\\'")){
                understanding = understanding.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_UNDERSTANDING where name='"
                + understanding + "';";
            int understandingSelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    understandingSelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemUnderstanding.contains(understanding)) {
                        understandingBox.addItem(understanding);
                    }
                } else {
                    sql = "INSERT INTO REQ_UNDERSTANDING (name,user_entry) values ('"
                        + understanding + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    understandingBox.addItem(understanding);
                    understandingBox.setSelectedItem(understanding);
                    sql = "SELECT id FROM REQ_UNDERSTANDING where name='"
                        + understanding + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        understandingSelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldUnderstanding = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldUnderstanding = pReqLeaf.getUnderstandingFromModel();
                    if (oldUnderstanding == understandingSelected) {
                        return;
                    }
                    pReqLeaf
                        .updateUnderstandingInDBAndModel(understandingSelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldUnderstanding != -1) {
                        pReqLeaf.updateUnderstandingInModel(oldUnderstanding);
                    }
                }
            }
        }
    }

    void ownerBoxPerformed(ActionEvent e) {
        if (canDoUpdateOwner) {
            String owner = (String) ownerBox.getSelectedItem();
            if (!owner.contains("\\'")){
                owner = owner.replace("'", "\\'");
            }
            String sql = "SELECT id,user_entry FROM REQ_OWNER where name='" + owner
                + "';";
            int ownerSelected = 1;
            try {
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()) {
                    ownerSelected = result.getInt(1);
                    int user_entry = result.getInt(2);
                    if (user_entry == 1 && !itemOwner.contains(owner)) {
                        ownerBox.addItem(owner);
                    }
                } else {
                    sql = "INSERT INTO REQ_OWNER (name,user_entry) values ('"
                        + owner + "',1);";
                    prep = db.prepareStatement(sql);
                    prep.executeUpdate();
                    ownerBox.addItem(owner);
                    ownerBox.setSelectedItem(owner);
                    sql = "SELECT id FROM REQ_OWNER where name='" + owner + "';";
                    prep = db.prepareStatement(sql);
                    result = prep.executeQuery(sql);
                    if (result.next()) {
                        ownerSelected = result.getInt(1);
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            int oldOwner = -1;
            ReqLeaf pReqLeaf = null;
            if (pRequirement instanceof ReqLeaf) {
                try {
                    pReqLeaf = (ReqLeaf) pRequirement;
                    oldOwner = pReqLeaf.getOwnerFromModel();
                    if (oldOwner == ownerSelected) {
                        return;
                    }
                    pReqLeaf.updateOwnerInDBAndModel(ownerSelected);
                    pHistoryPanel.updateData(pReqLeaf, true);
                } catch (Exception ex) {
                    if (pReqLeaf != null && oldOwner != -1) {
                        pReqLeaf.updateOwnerInModel(oldOwner);
                    }
                }
            }
        }
    }

    void viewTestPerformed(ActionEvent e, JTable pTable) {
        int selectedRow = pTable.getSelectedRow();
        if (selectedRow > -1) {
            if (testCover != null) {
                Test pTest = (Test) testCover.elementAt(selectedRow);
                if (pDynamicTree != null) {
                    DefaultMutableTreeNode node = pDynamicTree
                        .findRemoveTestNode(pTest.getNameFromModel(), pTest
                                            .getTestListFromModel().getNameFromModel(),
                                            pTest.getTestListFromModel()
                                            .getFamilyFromModel()
                                            .getNameFromModel(), false);
                    if (node != null) {
                        // pReqTree.refreshNode(node);
                        ReqPlugin.selectTestTab();
                        pDynamicTree.getTree().setSelectionPath(
                                                                new TreePath(node.getPath()));

                    }
                }
            }
        }
    }

    void viewCampPerformed(ActionEvent e, JTable pTable) {
        int selectedRow = pTable.getSelectedRow();
        if (selectedRow > -1) {
            if (testCover != null) {
                Test pTest = (Test) testCover.elementAt(selectedRow);
                if (DataModel.isTestExecuted(pTest)) {
                    new ViewCampaignOfTest(pTest);
                }
            }
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        // Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        if (lsm.isSelectionEmpty()) {
            // no rows are selected

        } else {
            // int selectedRow = lsm.getMinSelectionIndex();
            // selectedRow is selected
        }
    }

    /********************* ComboField from info ************************/
    public void setColumnSize(JTable table) {
        FontMetrics fm = table.getFontMetrics(table.getFont());
        int cols = table.getColumnCount();
        int rows = table.getRowCount();
        try {
            for (int i = 0; i < cols; i++) {
                int max = 0;
                for (int j = 0; j < rows; j++) {
                    int taille = fm.stringWidth((String) table.getValueAt(j, i));
                    if (taille > max)
                        max = taille;
                }
                String nom = (String) table.getColumnModel().getColumn(i)
                    .getIdentifier();
                int taille = fm.stringWidth(nom);
                if (taille > max)
                    max = taille;
                table.getColumnModel().getColumn(i).setPreferredWidth(max + 10);
            }
        } catch (Exception ex) {
            Logger.getLogger("Exception Occurred in setColumnSize: access to null element!");
        }
    }

    void setInfo(JComboBox pJcomboBox, Vector pVector, String value) {
        if (value == null) {
            value = "";
        }
        pJcomboBox.removeAllItems();
        int size = pVector.size();
        int selectedItem = -1;
        for (int i = 0; i < size; i++) {
            String item = (String) pVector.elementAt(i);
            pJcomboBox.addItem(item);
            if (value.equals(item)) {
                selectedItem = i;
            }
        }
        if (selectedItem == -1) {
            pJcomboBox.addItem(value);
            pJcomboBox.setSelectedIndex(size);
        } else {
            pJcomboBox.setSelectedIndex(selectedItem);
        }
    }
}
