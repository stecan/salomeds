package salomeTMF_plug.requirements.ihm;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;

import salomeTMF_plug.requirements.data.IReqFilter;
import salomeTMF_plug.requirements.data.ReqFilter;

public class FiltrePanel extends JPanel implements ActionListener {

    JRadioButton bHigh ;
    JRadioButton bMedium ;
    JRadioButton bLow ;
    JRadioButton bNone;
    int filtre = ReqFilter.P_FILTRE;
    IReqFilter m_ReqFilter;
    FilterReqTreeModel m_FilterReqTreeModel;
    Observer pObserver = null;

    public FiltrePanel(FilterReqTreeModel pFilterReqTreeModel){
        super();
        m_FilterReqTreeModel = pFilterReqTreeModel;
        if (m_FilterReqTreeModel != null) {
            m_ReqFilter = pFilterReqTreeModel.getFilter();
        }
        initComponent();
    }
    public FiltrePanel(FilterReqTreeModel pFilterReqTreeModel , Observer _pObserver){
        super();
        m_FilterReqTreeModel = pFilterReqTreeModel;
        if (m_FilterReqTreeModel != null) {
            m_ReqFilter = pFilterReqTreeModel.getFilter();
        }
        initComponent();
        pObserver = _pObserver;
    }

    void initComponent(){
        setLayout(new GridLayout(1, 4));
        bHigh = new JRadioButton(Language.getInstance().getText("Filtre_Exigence_Haute"), true);
        bHigh.setForeground(Color.red);

        bMedium = new JRadioButton(Language.getInstance().getText("Filtre_Exigence_Moyenne"), true);
        bMedium.setForeground(Color.blue);

        bLow = new JRadioButton(Language.getInstance().getText("Filtre_Exigence_Faible"), true);
        bLow.setForeground(Color.BLACK);

        bNone = new JRadioButton(Language.getInstance().getText("Filtre_Exigence_Option"), true);
        bNone.setForeground(Color.gray);

        //              bHigh.addActionListener(this);
        //              bMedium.addActionListener(this);
        //              bLow.addActionListener(this);
        //              bNone.addActionListener(this);
        //              add(bHigh);
        //              add(bMedium);
        //              add(bLow);
        //              add(bNone);
    }

    public void actionPerformed(ActionEvent evt){
        if (evt.getSource().equals(bHigh)){
            bHighPerformed();
        } else if (evt.getSource().equals(bMedium)){
            bMediumPerformed();
        } else  if (evt.getSource().equals(bLow)){
            bLowPerformed();
        } else  if (evt.getSource().equals(bNone)){
            bNonePerformed();
        }
    }

    int getMaxSelected(){
        if (bHigh.isSelected()){
            return ReqFilter.P_HIGHT;
        }
        if (bMedium.isSelected()){
            return ReqFilter.P_MEDIUM;
        }
        if (bLow.isSelected()){
            return ReqFilter.P_LOW;
        }
        if (bNone.isSelected()){
            return ReqFilter.P_NONE;
        }
        return ReqFilter.P_HIGHT;
    }
    /****************** Actions *************/
    void bHighPerformed(){
        filtre = getFiltre();
        applyFilter();
    }

    void bMediumPerformed(){
        filtre = getFiltre();
        applyFilter();
    }

    void bLowPerformed() {
        filtre = getFiltre();
        applyFilter();
    }

    void bNonePerformed() {
        filtre = getFiltre();
        applyFilter();
    }

    int getFiltre(){
        int f = 0;
        if (bHigh.isSelected()){
            f = f | ReqFilter.P_HIGHT;
        }
        if (bMedium.isSelected()){
            f = f | ReqFilter.P_MEDIUM;
        }
        if (bLow.isSelected()){
            f = f | ReqFilter.P_LOW;
        }
        if (bNone.isSelected()){
            f = f | ReqFilter.P_NONE;
        }
        return f;
    }

    IReqFilter getFilter(){
        return  m_ReqFilter;
    }

    void reInit(boolean filter){
        bHigh.setSelected(true);
        bMedium.setSelected(true);
        bLow.setSelected(true);
        bNone.setSelected(true);
        if (filter){
            m_ReqFilter.reInit();
        }
    }

    void applyFilter(){
        SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.WAIT_CURSOR));
        if (m_ReqFilter != null){
            m_ReqFilter.setFilter(filtre);
            m_FilterReqTreeModel.setFiltered(true);
        }
        if (pObserver != null){
            pObserver.update(null, new ReqEvent(ReqEvent.FILTRE_REQ_CHANGE,null));
        }
        SalomeTMFContext.getInstance().getSalomeFrame().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}
