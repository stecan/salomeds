/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.ihm;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.ExecutionResultView;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.data.ReqLeaf;
import salomeTMF_plug.requirements.data.Requirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class ExecResultStatDialog extends JDialog implements ListSelectionListener, ActionListener, Observer{
    PiePlot3D plot;
    DefaultPieDataset dataset;
    ChartPanel chartPanel;

    MyTableModel testResultTableModel;
    JTable testResultTable;
    TableSorter  sorter;
    ListSelectionModel rowSM;

    JPanel contentPan;
    JPanel buttonPan;
    JButton detailsButton;
    JButton fermerButton;

    ReqWrapper currentpReqWrapper;
    ExecutionResult currentExecutionResult;
    Hashtable reqWrapperList;
    Hashtable allReq;
    Vector filtredreqCovered;

    FiltrePanel pFiltrePanel;
    RequirementTree pReqTree;

    Vector itemState;
    //20100118 - D\ufffdbut modification Forge ORTF
    IDataBase db;
    ISQLEngine pISQLEngine;

    final String REQUIREMENTS_CFG_FILE = "/plugins/requirements/cfg/Requirements.properties";
    //20100118 - Fin modification Forge ORTF

    public ExecResultStatDialog(ExecutionResult executionResult, RequirementTree _pReqTree){
        super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
        pReqTree = _pReqTree;
        setTitle("ResExec : " + executionResult.getNameFromModel());
        setModal(true);
        contentPan = new JPanel(new BorderLayout());
        dataset = new  DefaultPieDataset();


        filtredreqCovered = new Vector();
        reqWrapperList = new Hashtable();
        allReq = new Hashtable();

        JFreeChart chart = ChartFactory.createPieChart3D(
                                                         //Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                         Language.getInstance().getText("Exigence_Pass"),  // chart title
                                                         dataset,                // data
                                                         true,                   // include legend
                                                         true,
                                                         false
                                                         );

        plot =  (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
        plot.setNoDataMessage("No data to display");
        chartPanel = new ChartPanel(chart);


        testResultTableModel = new MyTableModel();
        testResultTable = new JTable();


        sorter = new TableSorter(testResultTableModel);
        testResultTable.setModel(sorter);
        sorter.setTableHeader(testResultTable.getTableHeader());

        try {
            //reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(reqCovered));
            testResultTable.setDefaultRenderer(Class.forName( "java.lang.Object" ), new PriorityTableCellRenderer(filtredreqCovered));
        } catch (Exception  e){
            e.printStackTrace();
        }

        /*testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
          testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
          testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resultats"));
        */
        testResultTableModel.addColumnNameAndColumn("id");
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Categorie"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Complexite"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Etat"));
        //20100118 - D\ufffdbut modification Forge ORTF
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Valide_par"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Criticite"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Comprehension"));
        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Proprietaire"));

        TableColumn stateColumn = testResultTable.getColumnModel().getColumn(4);

        itemState = new Vector();
        //              itemState.add(Language.getInstance().getText("State_A_Analyser"));
        //              itemState.add(Language.getInstance().getText("State_Analysee"));
        //              itemState.add(Language.getInstance().getText("State_Approuvee"));
        //              itemState.add(Language.getInstance().getText("State_Verifiee"));
        //              itemState.add(Language.getInstance().getText("State_Finalisee"));
        //              itemState.add(Language.getInstance().getText("State_Reportee"));
        //              itemState.add(Language.getInstance().getText("State_Abandonnee"));
        ISQLObjectFactory pISQLObjectFactory = Api.getISQLObjectFactory();
        try {
            db = pISQLObjectFactory.getInstanceOfSalomeDataBase();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        pISQLEngine = pISQLObjectFactory.getCurrentSQLEngine();
        Properties prop = new Properties();
        try {
            URL _urlBase =  SalomeTMFContext.getInstance().getUrlBase();
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            URL urlBase = new URL(url_txt);

            java.net.URL url_cfg = new java.net.URL(urlBase
                                                    + REQUIREMENTS_CFG_FILE);
            try {
                prop = Util.getPropertiesFile(url_cfg);
            } catch (Exception e) {
                prop = Util.getPropertiesFile(REQUIREMENTS_CFG_FILE);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Iterator it = prop.keySet().iterator();
        while (it.hasNext()){
            String property = (String) it.next();
            if (property.equals("Statut")){
                String[] states = prop.getProperty(property).split(",");
                for (int i=0;i<states.length;i++){
                    String sql = "SELECT * FROM STATE where name='"+states[i]+"';";
                    try {
                        PreparedStatement prep = db.prepareStatement(sql);
                        ResultSet result = prep.executeQuery(sql);
                        if (!result.next()){
                            sql = "INSERT INTO STATE (name,user_entry) values ('"+states[i]+"',0);";
                            prep = db.prepareStatement(sql);
                            prep.executeUpdate();
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    itemState.add(states[0]);
                }
            }
        }
        String sql = "SELECT id,name FROM STATE where user_entry=1;";
        List<String> userStates = new ArrayList<String>();
        List<Integer> userStatesId = new ArrayList<Integer>();
        try {
            PreparedStatement prep = db.prepareStatement(sql);
            ResultSet result = prep.executeQuery(sql);
            while (result.next()){
                String state = result.getString(1);
                int id = result.getInt(1);
                userStates.add(state);
                userStatesId.add(id);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        for (int i = 0 ;i<userStatesId.size();i++){
            int id = userStatesId.get(i);
            try{
                sql = "select count(state) from REQUIREMENTS where state="+id;
                PreparedStatement prep = db.prepareStatement(sql);
                ResultSet result = prep.executeQuery(sql);
                if (result.next()){
                    int nbState = result.getInt(1);
                    if (nbState!=0){
                        itemState.add(userStates.get(i));
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        //20100118 - Fin modification Forge ORTF

        //JComboBox stateBox = new JComboBox(itemState);
        //stateColumn.setCellEditor(new DefaultCellEditor(stateBox));

        stateColumn.setCellEditor(new MyComboBoxEditor(itemState));

        // If the cell should appear like a combobox in its
        // non-editing state, also set the combobox renderer
        stateColumn.setCellRenderer(new MyComboBoxRenderer(itemState));


        testResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resultats"));


        rowSM = testResultTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        testResultTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        testResultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane tablePane = new JScrollPane(testResultTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());




        detailsButton = new JButton(Language.getInstance().getText("Details"));
        detailsButton.setEnabled(true); //false
        detailsButton.addActionListener(this);

        fermerButton = new JButton(Language.getInstance().getText("Fermer"));
        fermerButton.setEnabled(true); //false
        fermerButton.addActionListener(this);

        buttonPan = new JPanel(new GridLayout(1,3));
        pFiltrePanel = new FiltrePanel(null, this);

        buttonPan.add(detailsButton);
        buttonPan.add(pFiltrePanel);
        buttonPan.add(fermerButton);

        contentPan.add(tablePane, BorderLayout.NORTH);
        contentPan.add(buttonPan, BorderLayout.CENTER);
        contentPan.add(chartPanel, BorderLayout.SOUTH);
        setContentPane(contentPan);

        initData(executionResult);
        /*pack();
          setVisible(true);
        */
        centerScreen();
    }

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }

    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(detailsButton)){
            detailsPerformed(e);
        }if (e.getSource().equals(fermerButton)){
            fermerPerformed(e);
        }

    }

    void detailsPerformed(ActionEvent e){
        if (currentpReqWrapper != null){
            showDetail(currentExecutionResult, currentpReqWrapper);
        }
    }

    void fermerPerformed(ActionEvent e){
        dispose();
    }

    public void initData(ExecutionResult executionResult){
        //Campaign pCamp = DataModel.getCurrentCampaign();
        currentExecutionResult = executionResult;
        testResultTableModel.clearTable();
        reqWrapperList.clear();
        //filtredreqCovered.clear();
        try {
            Vector reqPass = new Vector();
            Vector reqFail = new Vector();
            Vector reqInco = new Vector();
            Vector reqNotApplicable = new Vector();
            Vector reqBlocked = new Vector();
            Vector reqNone = new Vector();

            reqPass = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.SUCCESS);
            reqFail = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.FAIL);
            reqInco = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.UNKNOWN);
            reqNotApplicable = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NOTAPPLICABLE);
            reqBlocked = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.BLOCKED);
            reqNone = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NONE);
            int sizePass = reqPass.size();
            int sizeFail = reqFail.size();
            int sizeInco = reqInco.size();
            int sizeNotApplicable = reqNotApplicable.size();
            int sizeBlocked = reqBlocked.size();
            int sizeNone = reqNone.size();

            allReq.clear();

            for (int i = 0; i < sizePass; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqPass.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.SUCCESS);

            }
            for (int i = 0; i < sizeInco; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqInco.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.UNKNOWN);

            }
            for (int i = 0; i < sizeFail; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqFail.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.FAIL);
            }
            for (int i = 0; i < sizeNotApplicable; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNotApplicable.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NOTAPPLICABLE);
            }
            for (int i = 0; i < sizeBlocked; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqBlocked.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.BLOCKED);
            }
            for (int i = 0; i < sizeNone; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNone.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NONE);
            }
            Enumeration enumReq = allReq.keys();
            int nbFail = 0;
            int nbPass = 0;
            int nbInco = 0;
            int nbNotApplicable = 0;
            int nbBlocked = 0;
            int nbNone = 0;
            int filtre = pFiltrePanel.getFiltre();
            filtredreqCovered.clear();
            int ligne = 0;
            while (enumReq.hasMoreElements()){
                ReqWrapper pReqWrapper =  (ReqWrapper)enumReq.nextElement();
                //reqWrapperList.put(pReqWrapper.getName(),pReqWrapper);
                DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReqWrapper);
                Requirement pReq = null;
                if (node != null){
                    pReq = (Requirement) node.getUserObject();
                }
                int reqP = pReqWrapper.getPriority();
                if ((reqP & filtre) > 0){
                    ArrayList data = new ArrayList();
                    String nom;
                    if (pReq != null){
                        nom = pReq.getLongName();
                    } else {
                        nom = pReqWrapper.getName();
                    }
                    reqWrapperList.put(nom, pReqWrapper);
                    String description = pReqWrapper.getDescription();
                    String status = (String) allReq.get(pReqWrapper);
                    if (status.equals(ApiConstants.SUCCESS)){
                        nbPass++;
                    } else if (status.equals(ApiConstants.FAIL)){
                        nbFail++;
                    } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                        nbNotApplicable++;
                    } else if (status.equals(ApiConstants.BLOCKED)){
                        nbBlocked++;
                    } else if (status.equals(ApiConstants.NONE)){
                        nbNone++;
                    } else {
                        nbInco++;
                    }
                    data.add("" + pReq.getIdBdd());
                    data.add(nom);
                    data.add(ReqWrapper.getCatString(((ReqLeaf)pReq).getCatFromModel()));
                    data.add(ReqWrapper.getComplexString(((ReqLeaf)pReq).getComplexeFromModel()));
                    data.add(ReqWrapper.getStateString(((ReqLeaf)pReq).getStateFromModel()));
                    //20100118 - D\ufffdbut modification Forge ORTF
                    data.add(ReqWrapper.getValidByString(((ReqLeaf)pReq).getValidByFromModel()));
                    data.add(ReqWrapper.getCriticalString(((ReqLeaf)pReq).getCriticalFromModel()));
                    data.add(ReqWrapper.getUnderstandingString(((ReqLeaf)pReq).getUnderstandingFromModel()));
                    data.add(ReqWrapper.getOwnerString(((ReqLeaf)pReq).getOwnerFromModel()));
                    //20100118 - Fin modification Forge ORTF
                    data.add(Tools.getActionStatusIcon(status));

                    //data.add(nom);
                    //data.add(description);
                    //data.add(Tools.getActionStatusIcon(status));

                    /*
                      testResultTableModel.addValueAt(data.get(0), ligne, 0 );
                      testResultTableModel.addValueAt(data.get(1), ligne, 1 );
                      testResultTableModel.addValueAt(data.get(2), ligne, 2 );
                      testResultTableModel.addValueAt(data.get(3), ligne, 3 );
                      //testResultTableModel.addValueAt(data.get(4), ligne, 4 );
                      testResultTableModel.addValueAt(data.get(5), ligne, 5 );
                      ligne++;
                    */
                    testResultTableModel.addRow(data);
                    filtredreqCovered.add(pReqWrapper);
                }
            }
            updateChart(nbFail, nbPass, nbInco, nbNotApplicable, nbBlocked, nbNone);

        } catch (Exception e ){
            e.printStackTrace();
        }
    }


    void updateTableAndChart(int filtre){
        int nbFail = 0;
        int nbPass = 0;
        int nbInco = 0;
        int nbNotApplicable = 0;
        int nbBlocked = 0;
        int nbNone = 0;
        testResultTableModel.clearTable();
        filtredreqCovered.clear();
        currentpReqWrapper = null;
        Enumeration enumReq = reqWrapperList.elements();
        int ligne = 0;
        while (enumReq.hasMoreElements()){
            ReqWrapper pReqWrapper =  (ReqWrapper)enumReq.nextElement();
            DefaultMutableTreeNode node = pReqTree.findRequirementFromParent(pReqWrapper);
            Requirement pReq = null;
            if (node != null){
                pReq = (Requirement) node.getUserObject();
            }
            int reqP = pReqWrapper.getPriority();
            if ((reqP & filtre) > 0){
                ArrayList data = new ArrayList();
                String nom;
                if (pReq != null){
                    nom = pReq.getLongName();
                } else {
                    nom = pReqWrapper.getName();
                }
                String description = pReqWrapper.getDescription();
                String status = (String) allReq.get(pReqWrapper);
                if (status.equals(ApiConstants.SUCCESS)){
                    nbPass++;
                } else if (status.equals(ApiConstants.FAIL)){
                    nbFail++;
                } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                    nbNotApplicable++;
                } else if (status.equals(ApiConstants.BLOCKED)){
                    nbBlocked++;
                } else if (status.equals(ApiConstants.NONE)){
                    nbNone++;
                } else {
                    nbInco++;
                }
                //data.add(nom);
                //data.add(description);
                //data.add(Tools.getActionStatusIcon(status));

                data.add("" + pReq.getIdBdd());
                data.add(nom);
                data.add(ReqWrapper.getCatString(((ReqLeaf)pReq).getCatFromModel()));
                data.add(ReqWrapper.getComplexString(((ReqLeaf)pReq).getComplexeFromModel()));
                data.add(ReqWrapper.getStateString(((ReqLeaf)pReq).getStateFromModel()));
                //20100118 - D\ufffdbut modification Forge ORTF
                data.add(ReqWrapper.getValidByString(((ReqLeaf)pReq).getValidByFromModel()));
                data.add(ReqWrapper.getCriticalString(((ReqLeaf)pReq).getCriticalFromModel()));
                data.add(ReqWrapper.getUnderstandingString(((ReqLeaf)pReq).getUnderstandingFromModel()));
                data.add(ReqWrapper.getOwnerString(((ReqLeaf)pReq).getOwnerFromModel()));
                //20100118 - Fin modification Forge ORTF
                data.add(Tools.getActionStatusIcon(status));

                /*testResultTableModel.addValueAt(data.get(0), ligne, 0 );
                  testResultTableModel.addValueAt(data.get(1), ligne, 1 );
                  testResultTableModel.addValueAt(data.get(2), ligne, 2 );
                  testResultTableModel.addValueAt(data.get(3), ligne, 3 );
                  testResultTableModel.addValueAt(data.get(4), ligne, 4 );
                  testResultTableModel.addValueAt(data.get(5), ligne, 5 );
                  ligne++;*/

                testResultTableModel.addRow(data);

                filtredreqCovered.add(pReqWrapper);
            }
        }
        setColumnSize(testResultTable);
        updateChart(nbFail, nbPass, nbInco, nbNotApplicable, nbBlocked, nbNone);
    }

    void updateChart(int nbFail, int nbPass, int nbInco, 
            int nbNotApplicable, int nbBlocked, int nbNone){

        int nbReqTotal = nbFail + nbPass + nbInco + nbNotApplicable + 
                nbBlocked + nbNone;

        dataset = new  DefaultPieDataset();
        if (nbReqTotal > 0) {
            Double percentPass = new Double((nbPass*100)/nbReqTotal);
            Double percentFail = new Double((nbFail*100)/nbReqTotal);
            Double percentInco = new Double((nbInco*100)/nbReqTotal);
            Double percentNotApplicable = new Double((nbNotApplicable*100)/nbReqTotal);
            Double percentBlocked = new Double((nbBlocked*100)/nbReqTotal);
            Double percentNone = new Double((nbNone*100)/nbReqTotal);
            dataset.setValue(Language.getInstance().getText("Exigence_Fail"),percentFail);
            dataset.setValue(Language.getInstance().getText("Exigence_Inco"), percentInco);
            dataset.setValue(Language.getInstance().getText("Exigence_Pass"),percentPass);
            dataset.setValue(Language.getInstance().getText("Exigence_NotApplicable"),percentNotApplicable);
            dataset.setValue(Language.getInstance().getText("Exigence_Blocked"),percentBlocked);
            dataset.setValue(Language.getInstance().getText("Exigence_None"),percentNone);
        }
        plot.setDataset(dataset);
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
            return;

        int selectedRowIndex = testResultTable.getSelectedRow();

        if (selectedRowIndex != -1 && testResultTableModel.getRowCount() > 0) {
            String reqNam = (String)sorter.getValueAt(selectedRowIndex, 1);
            currentpReqWrapper = (ReqWrapper) reqWrapperList.get(reqNam);
            //System.out.println("[ResultPanel] req selected is " + currentpReqWrapper);
        }
    }

    void showDetail(ExecutionResult pCurrentEcutionResult, ReqWrapper pReqWrapper){
        ExecutionResult pExecutionResult = new ExecutionResult(pCurrentEcutionResult.getNameFromModel(), pCurrentEcutionResult.getDescriptionFromModel(), DataModel.getObservedExecution() );

        //pExecutionResult.addTestResultStatusInModel();
        try {
            Vector testWraCover = Requirement.getTestWrapperCoveredFromDB(pReqWrapper.getIdBDD());
            Vector testCover = new Vector();
            int size = testWraCover.size();

            for (int i = 0; i < size; i++){
                TestWrapper pTestWrapper = (TestWrapper) testWraCover.elementAt(i);
                Test pTest = DataModel.getCurrentProject().getTestFromModel(pTestWrapper.getIdBDD());
                if (pTest == null && !ReqPlugin.isGlobalProject()){
                    if (ReqPlugin.getProjectRef().getIdBdd() == DataModel.getCurrentProject().getIdBdd()) {
                        DataModel.reloadFromBase(true);
                        pTest = DataModel.getCurrentProject().getTestFromModel(pTestWrapper.getIdBDD());
                        if (pTest == null) {
                            //Hum Data corruption !!!
                            Tools.ihmExceptionView(new Exception("Hum ??, it seem that data integrity are corrupted"));
                        }
                    }
                }
                if (pTest != null){
                    testCover.add(pTest);
                }
            }
            int nbTestCover = testCover.size();
            int order = 0;
            for (int i = 0; i < nbTestCover ; i++){
                Test pTest = (Test) testCover.elementAt(i);
                ExecutionTestResult pExecutionTestResult = pCurrentEcutionResult.getExecutionTestResultFromModel(pTest);
                if (pExecutionTestResult != null){
                    pExecutionResult.setTestExecutionTestResultInModel(pTest, pExecutionTestResult, order);
                    order++;
                } else {
                    /* le test ne fait pas partie de la campagne */
                }
            }
            new ExecutionResultView(pCurrentEcutionResult.getNameFromModel(), pExecutionResult);
            //this.setVisible(true);
        } catch(Exception e){
            e.printStackTrace();
        }
    }


    Vector filtredVectorOfReqWrapper(int filtre, Vector vectorOfReqWrapper){
        Vector filtredVector = new Vector();
        int size = vectorOfReqWrapper.size();
        for (int i = 0; i < size ; i++){
            ReqWrapper pReq = (ReqWrapper) vectorOfReqWrapper.elementAt(i);
            int reqP = pReq.getPriority();
            if ((reqP & filtre) > 0){
                filtredVector.add(pReq);
            }

        }
        return filtredVector;
    }

    int filtredSizeVectorOfReqWrapper(int filtre, Vector vectorOfReqWrapper){
        int filtredVectorSize  = 0;
        int size = vectorOfReqWrapper.size();
        for (int i = 0; i < size ; i++){
            ReqWrapper pReq = (ReqWrapper) vectorOfReqWrapper.elementAt(i);
            int reqP = pReq.getPriority();
            if ((reqP & filtre) > 0){
                filtredVectorSize ++;
            }

        }
        return filtredVectorSize;
    }

    public void update(Observable o, Object arg){
        if (arg instanceof ReqEvent){
            ReqEvent event = (ReqEvent)arg;
            if (event.code == ReqEvent.FILTRE_REQ_CHANGE){
                int filtre = pFiltrePanel.getFiltre();
                updateTableAndChart(filtre);
            }
        }
    }

    /**
     * redefine size of table columns
     */
    public void setColumnSize(JTable table){
        FontMetrics fm = table.getFontMetrics(table.getFont());
        for (int i = 0 ; i < table.getColumnCount() ; i++)
            {
                int max = 0;
                for (int j = 0 ; j < table.getRowCount() ; j++)
                    {
                        int taille = fm.stringWidth((String)table.getValueAt(j,i));
                        if (taille > max)
                            max = taille;
                    }
                String nom = (String)table.getColumnModel().getColumn(i).getIdentifier();
                int taille = fm.stringWidth(nom);
                if (taille > max)
                    max = taille;
                table.getColumnModel().getColumn(i).setPreferredWidth(max+10);
            }
    }

    public class MyComboBoxRenderer extends JComboBox implements TableCellRenderer {
        public MyComboBoxRenderer(Vector items) {
            super(items);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                                                       boolean isSelected, boolean hasFocus, int row, int column) {
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                super.setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }

            // Select the current value
            //setSelectedItem(value);
            setSelectedIndex(0);
            return this;
        }
    }

    public class MyComboBoxEditor extends DefaultCellEditor {
        public MyComboBoxEditor(Vector items) {
            super(new JComboBox(items));
        }
    }

    //////////////////////////////////////////////////

}
