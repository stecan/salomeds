package salomeTMF_plug.requirements.plugin;

import java.awt.event.ActionListener;

public interface Synchronize extends ActionListener {

    public boolean getImplemented();

}
