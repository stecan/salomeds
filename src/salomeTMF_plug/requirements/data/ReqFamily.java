/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.data;
import java.util.Vector;

import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class ReqFamily extends Requirement{

    Vector reqLeafList;

    public ReqFamily(String name, String description, ReqFamily parent){
        super(name, description, parent);
        reqLeafList = new Vector();
    }

    public ReqFamily(String name, String description){
        super(name, description, null);
        reqLeafList = new Vector();
        idBdd = 0;
    }

    public ReqFamily(ReqWrapper pReqWrapper, ReqFamily parent){
        super(pReqWrapper.getName(), pReqWrapper.getDescription(), parent);
        idBdd = pReqWrapper.getIdBDD();
        reqLeafList = new Vector();
        //LoadAttachement
    }

    public Requirement getCopie(ReqFamily pReqFamily){
        ReqFamily pReqFam = new ReqFamily(name, description, pReqFamily);

        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);
            pReqFam.addRequirement(pReq.getCopie(pReqFam));
        }
        return pReqFam;
    }

    public void pastInDBandModel(Requirement pReq, ReqFamily pReqOringine, boolean testName) throws Exception {
        if (pReqOringine == null){
            pReq.m_parent = this;
        }
        //GetUniqueName
        String tmpName = pReq.getNameFromModel();
        boolean trouve = false;
        int j = 0;
        while (!trouve && testName){
            ReqWrapper pReqWrapper = pSQLRequirement.getReq(pReq.getNameFromModel(), idBdd, ReqPlugin.getProjectRef().getIdBdd());
            if (pReqWrapper == null){
                trouve = true;
            } else {
                pReq.updateNameInModel("copy_" + j + "_" +    tmpName);
                j++;
            }
        }
        pReq.addInDB();
        if (pReq instanceof ReqFamily){
            ReqFamily pReqFam = (ReqFamily) pReq;
            Vector reqLeafList2 = pReqFam.reqLeafList;
            int size = reqLeafList2.size();
            for (int i = 0 ; i < size ; i++){
                Requirement pReqTmp = (Requirement) reqLeafList2.elementAt(i);
                pastInDBandModel(pReqTmp, this, false);
            }
        } else {
            ReqLeaf pReqLef = (ReqLeaf) pReq;
            pReqLef.updateInfoInDB(pReqLef.version,pReqLef.origine, pReqLef.verif, pReqLef.reference);
            pReqLef.updateCatInDB(pReqLef.cat);
            pReqLef.updateComplexeInDB(pReqLef.complexe);
            pReqLef.updateStateInDB(pReqLef.state);
            //20100118 - D\ufffdbut modification Forge ORTF
            pReqLef.updateValidByInDB(pReqLef.validBy);
            pReqLef.updateCriticalInDB(pReqLef.critical);
            pReqLef.updateUnderstandingInDB(pReqLef.understanding);
            pReqLef.updateOwnerInDB(pReqLef.owner);
            //20100118 - Fin modification Forge ORTF
        }
    }

    public void addRequirement(Requirement req){
        if (contain(req)== null){
            reqLeafList.add(req);
        }
    }

    Requirement contain(Requirement reqTested){
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);
            if (reqTested.getNameFromModel().equals(pReq.getNameFromModel())){
                return pReq;
            }
        }
        return null;
    }

    public Vector getFistLeaf(){
        Vector res = new Vector();
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);
            if (pReq instanceof ReqLeaf){
                res.add(pReq);
            }
        }
        return res;
    }
    public Vector getFirstFamily(){
        Vector res = new Vector();
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);
            if (pReq instanceof ReqFamily){
                res.add(pReq);
            }
        }
        return res;
    }

    public Vector getAllLeaf(){
        Vector res = new Vector();
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);
            if (pReq instanceof ReqLeaf){
                res.add(pReq);
            } else {
                Vector reqLeafList2 = ((ReqFamily)pReq).getAllLeaf();
                for (int j = 0 ; j < reqLeafList2.size(); j++){
                    res.add(reqLeafList2.elementAt(j));
                }
            }
        }
        return res;
    }

    public Vector getAllLeaf(IReqFilter filtre){
        Vector res = new Vector();
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);

            if (pReq instanceof ReqLeaf){
                int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                //if (( reqP | filtre) == filtre){
                if (filtre.isFiltred(pReq)){
                    res.add(pReq);
                }
            } else {
                Vector reqLeafList2 = ((ReqFamily)pReq).getAllLeaf(filtre);
                for (int j = 0 ; j < reqLeafList2.size(); j++){
                    res.add(reqLeafList2.elementAt(j));
                }
            }
        }
        return res;
    }

    public Vector getAllLeafByPriority(int filtre){
        Vector res = new Vector();
        int size = reqLeafList.size();
        for (int i = 0 ; i < size ; i++){
            Requirement pReq = (Requirement) reqLeafList.elementAt(i);

            if (pReq instanceof ReqLeaf){
                int reqP = ((ReqLeaf)pReq).getPriorityFromModel();
                if ((   reqP | filtre) == filtre){
                    //if (filtre.isFiltred(pReq)){
                    res.add(pReq);
                }
            } else {
                Vector reqLeafList2 = ((ReqFamily)pReq).getAllLeafByPriority(filtre);
                for (int j = 0 ; j < reqLeafList2.size(); j++){
                    res.add(reqLeafList2.elementAt(j));
                }
            }
        }
        return res;
    }

    public boolean  isFamilyReq(){
        return true;
    }

    void removeRequirment(Requirement req){
        //reqLeafList.remove(req);
        Requirement reqToDel = contain(req);
        if (reqToDel != null){
            reqLeafList.remove(reqToDel);
        }
    }

    void deleteInModel()  {
        reqLeafList.clear();
    }

    public String toString(){
        return "ReqSet : " + name;
    }

    public String getLongName(){
        if (m_parent !=null){
            String parentName  = m_parent.getLongName();
            if (parentName != ""){
                return parentName + "." + name;
            } else {
                return name;
            }
        } else {
            return "";
        }
    }
    public void addInDB() throws Exception {
        if (isInBase()){
            throw new Exception ("[ReqFamily->addInDB] requirment already in base");
        }
        int idParent = m_parent.getIdBdd();
        idBdd = pSQLRequirement.add(name, description, 0, idParent, ReqPlugin.getProjectRef().getIdBdd()).getIdBDD();
    }

    public void addTestCoverInDB(int idTest, boolean verifie)throws Exception{
        throw new Exception ("[ReqFamily->addTestCover] unable to link test with a set of requiments");
    }
}
