package salomeTMF_plug.requirements.data;

import java.util.Hashtable;

public interface IReqFilter {
    public boolean pass(Object obj);
    public void setActived(boolean actived);
    public boolean isActived();
    public void setFilter(int filtre);
    public int getFilter();
    public void setCoverFiltre(int filtre, Hashtable reqsWrapCoveredHash);
    public void setInfoFiltre(int catSelected, int complexSelected , int stateSelected);
    public void setUserDataFiltre(boolean activate, FiltreUserData pFiltreUserData);
    //public boolean isFiltredByInfo (ReqLeaf pReq) ;
    /**
     * Return true if the requirement req resect the contraints defines by the filtre
     */
    public boolean isFiltred (Requirement pReq) ;
    public void reInit();
}
