/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@orange-ftgroup.com
 */

package salomeTMF_plug.requirements.data;

import java.util.Vector;

public class FiltreUserData {

    /* Static Interface */
    public static Vector<String> listOfOrigine = new Vector();
    public static Vector<String> listOfReference = new Vector();
    public static Vector<String> listOfVerification = new Vector();
        
    public static void addOrigine(String data){
        if (data == null){
            return;
        }
        data = data.trim();
        if (!listOfOrigine.contains(data)){
            listOfOrigine.add(data);
        }
    }
        
    public static void addReference(String data){
        if (data == null){
            return;
        }
        data = data.trim();
        if (!listOfReference.contains(data)){
            listOfReference.add(data);
        }
    }
        
    public static void addVerification(String data){
        if (data == null){
            return;
        }
        data = data.trim();
        if (!listOfVerification.contains(data)){
            listOfVerification.add(data);
        }
    }
        
    /* Object Implementation */
        
    String filtreName; // For Saving info */
    boolean nameIsFiltred;
    boolean origineIsFiltred;
    boolean referenceIsFiltred;
    boolean versionIsFiltred;
    boolean verificationIsFiltred;
        
    String namePatern;
    String originePatern;
    String referencePatern;
    String versionPatern;
    String verificationPatern;

    public FiltreUserData(){
        nameIsFiltred = false;
        origineIsFiltred = false;
        referenceIsFiltred = false;
        versionIsFiltred = false ;
        verificationIsFiltred = false;
    }
        
    /**
     * 
     * @param _namePatern :  regex of name
     * @param _nameIsFiltred
     * @param _originePatern
     * @param _origineIsFiltred
     * @param _referencePatern
     * @param _referenceIsFiltred
     * @param _versionPatern
     * @param _versionIsFiltred
     * @param _verificationPatern
     * @param _verificationIsFiltred
     */
    public void setFiltre(
                          String _namePatern, boolean _nameIsFiltred,
                          String _originePatern, boolean _origineIsFiltred,
                          String _referencePatern, boolean _referenceIsFiltred,
                          String _versionPatern, boolean _versionIsFiltred,
                          String _verificationPatern, boolean _verificationIsFiltred) {
                
        if (_namePatern == null || _namePatern.trim().equals("")){
            nameIsFiltred = false;
        } else {
            if (_nameIsFiltred){
                namePatern = _namePatern.trim();
            }
            nameIsFiltred = _nameIsFiltred;
        }
                
        if (_originePatern == null ){
            origineIsFiltred = false;
        } else {
            if (_origineIsFiltred){
                originePatern = _originePatern.trim();
            }
            origineIsFiltred = _origineIsFiltred;
        }
                
        if (_referencePatern == null ){
            referenceIsFiltred = false;
        } else {
            if (_referenceIsFiltred){
                referencePatern = _referencePatern.trim();
            }
            referenceIsFiltred = _referenceIsFiltred;
        }
        
        if (_versionPatern == null || _versionPatern.trim().equals("")){
            versionIsFiltred = false;
        } else {
            if (_versionIsFiltred){
                versionPatern = _versionPatern.trim();
            }
            versionIsFiltred = _versionIsFiltred;
        }
                
        if (_verificationPatern == null ){
            verificationIsFiltred = false;
            //System.out.println("VerificationPatern is Null ");
                        
        } else {
            if (_verificationIsFiltred){
                verificationPatern = _verificationPatern.trim();
            }
            verificationIsFiltred = _verificationIsFiltred;
        }
        /*
          System.out.println("Name filtred :" + nameIsFiltred + ", patern = " + namePatern);
          System.out.println("Origine filtred :" + origineIsFiltred + ", patern = " + originePatern);
          System.out.println("ReferencePatern filtred :" + referenceIsFiltred + ", patern = " + referencePatern);
          System.out.println("Version filtred :" + versionIsFiltred + ", patern = " + versionPatern);
          System.out.println("Verification filtred :" + verificationIsFiltred + ", patern = " + verificationPatern);
        */
    }
        
    /**
     * 
     * @param req
     * @return true if req is conform with the date filter
     */
    public boolean isFiltred(Requirement req){
        boolean isFiltred = true;
        if (req instanceof ReqFamily) {
            /* TODO */  
        } else {
            ReqLeaf pReqLeaf = (ReqLeaf) req;
            //boolean continuTest = true; 
            String tmpStr;
            if (nameIsFiltred ){
                tmpStr = pReqLeaf.getNameFromModel();
                if (tmpStr != null) {
                    tmpStr= tmpStr.trim();
                    if (!tmpStr.matches(namePatern+".*")){
                        return false;
                    }
                }  else {
                    if (!(namePatern == tmpStr)){
                        return false;
                    }
                }
            }
                        
            if (origineIsFiltred ){
                tmpStr = pReqLeaf.getOrigineFromModel();
                if (tmpStr != null) {
                    tmpStr= tmpStr.trim();
                    if (!tmpStr.equals(originePatern)){
                        return false;
                    }
                }
            }
                        
            if (referenceIsFiltred ){
                tmpStr = pReqLeaf.getReferenceFromModel();
                if (tmpStr != null) {
                    tmpStr= tmpStr.trim();
                    if (!tmpStr.equals(referencePatern)){
                        return false;
                    }
                } else {
                    if (!(referencePatern == tmpStr)){
                        return false;
                    }
                }
            }
                        
            if (versionIsFiltred ){
                tmpStr = pReqLeaf.getVersionFromModel();
                if (tmpStr != null) {
                    tmpStr= tmpStr.trim();
                    if (!tmpStr.matches(versionPatern+".*")){
                        return false;
                    }
                } else {
                    if (!(versionPatern == tmpStr)){
                        return false;
                    }
                }
            }
                        
            if (verificationIsFiltred ){
                tmpStr = pReqLeaf.getVerifFromModel();
                if (tmpStr != null) {
                    tmpStr= tmpStr.trim();
                    if (!tmpStr.equals(verificationPatern)){
                        return false;
                    } else {
                        //System.out.println("MATCH : "+ tmpStr + " = " + verificationPatern);
                    }
                } else {
                    if (!(verificationPatern == tmpStr)){
                        return false;
                    }
                }
            }
        }
                
        return isFiltred;
    }

    /**
     * Reset the filter information
     *
     */
    public void reset(){
        nameIsFiltred = false;
        origineIsFiltred = false;
        referenceIsFiltred = false;
        versionIsFiltred = false ;
        verificationIsFiltred = false;
    }
        
    /**
     * 
     * @return true if the filtre can have effect;
     */
    public boolean isActive(){
        return  nameIsFiltred || origineIsFiltred || referenceIsFiltred || versionIsFiltred || verificationIsFiltred;
    }
        
    public String getFiltreName() {
        return filtreName;
    }

    public boolean isNameIsFiltred() {
        return nameIsFiltred;
    }

    public String getNamePatern() {
        return namePatern;
    }

    public boolean isOrigineIsFiltred() {
        return origineIsFiltred;
    }

    public String getOriginePatern() {
        return originePatern;
    }

    public boolean isReferenceIsFiltred() {
        return referenceIsFiltred;
    }

    public String getReferencePatern() {
        return referencePatern;
    }

    public boolean isVerificationIsFiltred() {
        return verificationIsFiltred;
    }

    public String getVerificationPatern() {
        return verificationPatern;
    }

    public boolean isVersionIsFiltred() {
        return versionIsFiltred;
    }

    public String getVersionPatern() {
        return versionPatern;
    }
}
