/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package salomeTMF_plug.requirements.data;

import java.util.Hashtable;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000120 - \ufffd2.7.8
 * Jira : FORTF - 39
 *
 */
public class ReqFilter implements IReqFilter{
    //boolean pass=true;
    boolean actived = true;



    public final static int P_HIGHT = 1000;
    public final static int P_MEDIUM = 100;
    public final static int P_LOW = 10;
    public final static int P_NONE = 1;
    public final static int P_FILTRE = 1007;

    public final static int C0 = 1000;
    public final static int C1 = 100;
    public final static int C2 = 10;
    public final static int C3 = 1;

    int catSelected = -1;
    int complexSelected = -1;
    int stateSelected = -1;
    int m_filtre = P_FILTRE; //1000 H, 100 M, 10 L, 1 N (combinaison)
    int coverFiltre = 11;
    Hashtable reqsWrapCoveredHash;
    FiltreUserData pFiltreUserData;
    boolean activateUserFilter = false;

    public ReqFilter(int filtre){
        m_filtre = filtre;
    }

    public boolean pass(Object obj){
        if (!isActived()) return true;
        if (!isFiltred()) return true;
        if (obj instanceof DefaultMutableTreeNode) {
            Object req = ((DefaultMutableTreeNode)obj).getUserObject();
            if (req instanceof Requirement){
                return isFiltred((Requirement)req);
            }
        }
        return false;
    }



    /**
     *
     * @param pReq
     * @return true if pReq is filtred by info (catSelected, complexSelected, stateSelected)
     */
    boolean isFiltredByInfo(ReqLeaf pReq){
        int _catSelected = pReq.getCatFromModel();
        int _complexSelected = pReq.getComplexeFromModel();
        int _stateSelected = pReq.getStateFromModel();

        // Matching complex box <-> complexity values
        Vector com_values = new Vector();
        com_values.add(0,new Integer(C0));
        com_values.add(1,new Integer(C1));
        com_values.add(2,new Integer(C2));
        com_values.add(3,new Integer(C3));

        //20100118 - D\ufffdbut modification Forge ORTF
        if (
            (catSelected == -1 || catSelected == _catSelected) &&
            (complexSelected == -1 || complexSelected == _complexSelected) &&
            (stateSelected == -1 || stateSelected == _stateSelected)
            )
            //20100118 - Fin modification Forge ORTF
            {
                //return true;
                if (activateUserFilter && pFiltreUserData !=  null && pFiltreUserData.isActive()){
                    return pFiltreUserData.isFiltred(pReq);
                } else {
                    return true;
                }
            } else {
            return false;
        }
        //return false;
    }

    /**
     * Return true if the requirement req resect the contraints defines by the filtre
     */
    public boolean isFiltred (Requirement req) {
        if (req instanceof ReqLeaf){
            int reqP = ((ReqLeaf)req).getPriorityFromModel();
            if ((reqP | m_filtre) == m_filtre){
                if (coverFiltre == 11 || reqsWrapCoveredHash == null) { // Toutes les exigences
                    return isFiltredByInfo((ReqLeaf)req);
                } else if (coverFiltre == 10){ // Seulement les exigences couverte
                    Integer id =  new Integer(((ReqLeaf)req).getIdBdd());
                    if (reqsWrapCoveredHash.get(id) != null){
                        return isFiltredByInfo((ReqLeaf)req);
                    }
                    return false;
                } else if (coverFiltre == 1){ // Seulement les exigences non couverte
                    Integer id =  new Integer(((ReqLeaf)req).getIdBdd());
                    if (reqsWrapCoveredHash.get(id) == null){
                        return isFiltredByInfo((ReqLeaf)req);
                    }
                    return false;
                } else {
                    return false;
                }
            }
        } else {
            if (!isFiltred()){
                return true;
            }else {
                ReqFamily pReq = ((ReqFamily)req);
                //Vector list_of_leaf =  pReq.getAllLeaf(m_filtre);
                Vector list_of_leaf =  pReq.getAllLeafByPriority(m_filtre);
                int size = list_of_leaf.size();
                boolean trouve = false;
                int cpt = 0;
                while (cpt < size && !trouve){
                    Requirement pTempReq =  (Requirement) list_of_leaf.get(cpt);
                    if (isFiltred(pTempReq)){
                        trouve = true; //Au moins une exigence est filtree
                    }
                    cpt ++;
                }
                return trouve;
            }
            //return true;
        }
        return false;
    }

    public void setCoverFiltre(int filtre, Hashtable reqsWrapCoveredHash) {
        coverFiltre = filtre;
        this.reqsWrapCoveredHash = reqsWrapCoveredHash;
    }

    public void setInfoFiltre(int _catSelected, int _complexSelected , int _stateSelected) {
        catSelected = _catSelected;
        complexSelected = _complexSelected;
        stateSelected = _stateSelected;
    }

    public void setUserDataFiltre(boolean _activateUserFilter , FiltreUserData _pFiltreUserData){
        pFiltreUserData = _pFiltreUserData;
        if (pFiltreUserData == null){
            activateUserFilter = false;
        } else {
            activateUserFilter= _activateUserFilter;
        }
    }

    public void setActived(boolean actived) {
        this.actived = actived;
    }

    public boolean isActived() {
        return actived;
    }

    /**
     *
     * @return true if a filtre is active
     */
    public boolean isFiltred() {
        /*System.out.println("m_filtre = " + m_filtre);
          System.out.println("coverFiltre = " + coverFiltre);
          System.out.println("catSelected = " + catSelected);
          System.out.println("complexSelected = " + complexSelected);
          System.out.println("stateSelected = " + stateSelected);
        */
        if (coverFiltre != 11){
            return true;
        }
        if (m_filtre != P_FILTRE){
            return true;
        }
        if (catSelected != -1 || complexSelected != -1 || stateSelected != -1){
            return true;
        }
        if (activateUserFilter && pFiltreUserData !=  null && pFiltreUserData.isActive()){
            return true;
        }
        return false;
    }

    public void reInit(){
        m_filtre = P_FILTRE;
        coverFiltre = 11;
        catSelected = -1;
        complexSelected = -1;
        stateSelected = -1;
        if (pFiltreUserData != null){
            pFiltreUserData.reset();
        }
    }

    public void setFilter(int filtre){
        m_filtre = filtre;
    }

    public int getFilter() {
        return m_filtre;
    }
}
