package salomeTMF_plug.requirements.data;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.ihm.languages.Language;

import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

public class ChartProxy {
        
    static public void saveChartAsJPEG(JFreeChart chart, File file){
        try {
            ChartUtilities.saveChartAsJPEG(file, chart , 500, 300);
        } catch (IOException e){
                        
        }
    }
        
    /**
     * Provide a chart representing a pie of requirements covered by Tests
     * @param filtre 
     * @return
     */
    public static JFreeChart getCoveredReqChart(int filtre){
        PiePlot3D plot;
        DefaultPieDataset dataset;
        dataset = new  DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(
                                                         Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                         dataset,                // data
                                                         true,                   // include legend
                                                         true,
                                                         false
                                                         );
        
        plot =  (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
                
        try {
            int nbTotalReq = 1;
            int nbCoveredReq = 0;
            if (filtre> 0){
                Vector leafInCurrentProject = Requirement.getReqWrapperLeafInCurrentProject();
                nbTotalReq = leafInCurrentProject.size();
                                
                Vector reqWrapperCovered =  Requirement.getReqWrapperCovered(); 
                nbCoveredReq = reqWrapperCovered.size();
                if (filtre != 111){
                    int nbTotalReq2 = 0;
                    for (int i = 0 ; i < nbTotalReq; i++){
                        int reqP = ((ReqWrapper) leafInCurrentProject.elementAt(i)).getPriority();
                        if ((reqP & filtre) > 0){
                            nbTotalReq2++;
                        }
                    }
                    int nbCoveredReq2 = 0;
                    for (int i = 0 ; i < nbCoveredReq; i++){
                        int reqP = ((ReqWrapper) reqWrapperCovered.elementAt(i)).getPriority();
                        if ((reqP & filtre) > 0){
                            nbCoveredReq2++;
                        }
                    }
                    nbTotalReq = nbTotalReq2;
                    nbCoveredReq = nbCoveredReq2;
                }
            }
            if (nbTotalReq > 0) {
                Double nbCovered = new Double( (nbCoveredReq*100)/nbTotalReq);
                Double nbNotCovered = new Double(100 -  (nbCoveredReq*100)/nbTotalReq);
                dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), nbNotCovered);
                dataset.setValue(Language.getInstance().getText("Exigence_couverte"),nbCovered);
                plot.setDataset(dataset);
            } 
                        
        } catch (Exception e){
                        
        }
                
        return chart;
    }
        
    /**
     * Provide a chart representing a pie of requirements covered by a campaing
     * @param pCamp
     * @param filtre
     * @return
     */
    public static JFreeChart getCoveredCampChart(Campaign pCamp, int filtre){
        PiePlot3D plot;
        DefaultPieDataset dataset =  new  DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(
                                                         Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                         dataset,                // data
                                                         true,                   // include legend
                                                         true,
                                                         false
                                                         );
        plot =  (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
                
        Vector reqCoveredWrapper;
        Vector filtredreqCovered = new Vector();
        int nbReqTotal = 0;
        int transNumber = -1;
        try {
            transNumber = SQLWrapper.beginTransaction();
            reqCoveredWrapper = Requirement.getReqWrapperCoveredByCamp(pCamp.getIdBdd());
            int size = reqCoveredWrapper.size();
            for (int j = 0 ; j < size ; j++) {
                ReqWrapper pReqWrapper = (ReqWrapper)reqCoveredWrapper.elementAt(j);
                if (pReqWrapper.getType() == 1 ){
                    if (!filtredreqCovered.contains(pReqWrapper)){
                        int reqP = pReqWrapper.getPriority();
                        if ((reqP & filtre) > 0){
                            filtredreqCovered.add(pReqWrapper);
                        }
                    }
                }
            }
            int nbReqCovered = filtredreqCovered.size();
            try {
                nbReqTotal = Requirement.getReqWrapperLeafInCurrentProject().size();
                                
            } catch (Exception e){
                nbReqTotal = 0;
            }
                         
            dataset = new  DefaultPieDataset();
            if (nbReqTotal > 0) {
                Double nbCovered = new Double( (nbReqCovered*100)/nbReqTotal);
                Double nbNotCovered = new Double(100 -  (nbReqCovered*100)/nbReqTotal);
                dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), nbNotCovered);
                dataset.setValue(Language.getInstance().getText("Exigence_couverte"),nbCovered);
            } 
            plot.setDataset(dataset);
                        
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e){
            try {
                SQLWrapper.rollBackTrans(transNumber);
            } catch (Exception e1){
            }
            return null;
        }
        return chart;
    }
        
    /**
     * Provide a chart representing a pie of requirements satisfaction in a execution result
     * @param executionResult
     * @param filtre
     * @return
     */
    public static JFreeChart getExecutionResultChart(ExecutionResult executionResult, int filtre){
        Hashtable allReq = new Hashtable();
        PiePlot3D plot;
        DefaultPieDataset dataset =  new  DefaultPieDataset();
        JFreeChart chart = ChartFactory.createPieChart3D(
                                                         Language.getInstance().getText("Exigence_Pass"),  // chart title
                                                         dataset,                // data
                                                         true,                   // include legend
                                                         true,
                                                         false
                                                         );
        
        plot =  (PiePlot3D) chart.getPlot();
        plot.setStartAngle(290);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setForegroundAlpha(0.5f);
                
        try {
            Vector reqPass = new Vector();
            Vector reqFail = new Vector();
            Vector reqInco = new Vector();
            Vector reqNotApplicable = new Vector();
            Vector reqBlocked = new Vector();
            Vector reqNone = new Vector();
                        
            reqPass = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.SUCCESS);
            reqFail = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.FAIL);
            reqInco = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.UNKNOWN);
            reqNotApplicable = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NOTAPPLICABLE);
            reqBlocked = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.BLOCKED);
            reqNone = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NONE);
            int sizePass = reqPass.size();
            int sizeFail = reqFail.size();
            int sizeInco = reqInco.size();
            int sizeNotApplicable = reqNotApplicable.size();
            int sizeBlocked = reqBlocked.size();
            int sizeNone = reqNone.size();
                        
            allReq.clear();
                        
            for (int i = 0; i < sizePass; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqPass.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.SUCCESS);  
            }
            for (int i = 0; i < sizeInco; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqInco.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.UNKNOWN);
            }
            for (int i = 0; i < sizeFail; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqFail.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.FAIL);
            }
            for (int i = 0; i < sizeNotApplicable; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNotApplicable.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NOTAPPLICABLE);
            }
            for (int i = 0; i < sizeBlocked; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqBlocked.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.BLOCKED);
            }
            for (int i = 0; i < sizeNone; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNone.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NONE);
            }
            Enumeration enumReq = allReq.keys();
            int nbFail = 0;
            int nbPass = 0;
            int nbInco = 0;
            int nbNotApplicable = 0;
            int nbBlocked = 0;
            int nbNone = 0;
                        
            while (enumReq.hasMoreElements()){
                ReqWrapper pReqWrapper =  (ReqWrapper)enumReq.nextElement();
                int reqP = pReqWrapper.getPriority();
                if ((reqP & filtre) > 0){
                    String status = (String) allReq.get(pReqWrapper);
                    if (status.equals(ApiConstants.SUCCESS)){
                        nbPass++;
                    } else if (status.equals(ApiConstants.FAIL)){
                        nbFail++;
                    } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                        nbNotApplicable++;
                    } else if (status.equals(ApiConstants.BLOCKED)){
                        nbBlocked++;
                    } else if (status.equals(ApiConstants.NONE)){
                        nbNone++;
                    } else {
                        nbInco++; 
                    }
                }
            }
                        
            int nbReqTotal = nbFail + nbPass + nbInco + nbNotApplicable +
                    nbBlocked + nbNone;
                
            dataset = new  DefaultPieDataset();
            if (nbReqTotal > 0) {
                Double percentPass = new Double((nbPass*100)/nbReqTotal);
                Double percentFail = new Double((nbFail*100)/nbReqTotal);
                Double percentInco = new Double((nbInco*100)/nbReqTotal);
                Double percentNotApplicable = new Double((nbNotApplicable*100)/nbReqTotal);
                Double percentBlocked = new Double((nbBlocked*100)/nbReqTotal);
                Double percentNone = new Double((nbNone*100)/nbReqTotal);
                dataset.setValue(Language.getInstance().getText("Exigence_Fail"),percentFail);
                dataset.setValue(Language.getInstance().getText("Exigence_Inco"), percentInco);
                dataset.setValue(Language.getInstance().getText("Exigence_Pass"),percentPass);
                dataset.setValue(Language.getInstance().getText("Exigence_NotApplicable"),percentNotApplicable);
                dataset.setValue(Language.getInstance().getText("Exigence_Blocked"),percentBlocked);
                dataset.setValue(Language.getInstance().getText("Exigence_None"),percentNone);
            } 
            plot.setDataset(dataset);   
                        
        } catch (Exception e ){
            e.printStackTrace();
        }
        return chart;
    }
}
