package salomeTMF_plug.requirements.data;

import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;

import salomeTMF_plug.requirements.ihm.RequirementTree;
import salomeTMF_plug.requirements.ihm.SimpleDynamicTestTree;

public class GenTestTools {

    /* simpleData prefix */
    static String PREFIX_FAMILLE = "RF_";
    static String PREFIX_SUITE = "RS_";
    static String PREFIX_TEST = "RT_";
    static String PREFIX = ".";

    /* Requirement type */
    static int REQ_FAMILY = 0;
    static int REQ_LEAF = 1;
        
    /* Cache used in order to avoid recomputing element */
    static Hashtable reqMapp  = new Hashtable();
    static Hashtable simpeDataMapp  = new Hashtable();
        
    /* Key : Requirement , Values : Vector of String representing testName */ 
    static Hashtable reqLink = new Hashtable();
        
    /* Key : Requirement , Values : Vector of String representing testName */ 
    static Hashtable defaultreqLink = new Hashtable();
    static Hashtable testLink = new Hashtable();
        
        
    static SimpleDynamicTestTree pCopyDynamicTestTree;
    static RequirementTree pOrgReqTree;
        
    //static DefaultMutableTreeNode rootReq;
        
    static public void initTools(RequirementTree _reqTree, DynamicTree _pDynamicTree){
        reqMapp = new Hashtable();
        simpeDataMapp = new Hashtable();
        pCopyDynamicTestTree = createCopyOfTestTree(_pDynamicTree);
        reqLink = new Hashtable();
        defaultreqLink = new Hashtable(); 
        testLink = new Hashtable();
        pOrgReqTree = _reqTree;
        //rootReq = (DefaultMutableTreeNode)((JTree)_reqTree).getModel().getRoot();
        initReqLink();
    }
        
    /**************************** Bijection functions ****************************/
        
    /**
     * 
     * @param pSimpleData (family,testList,test)
     * @return  the associated requirement to the simpleData
     */
    static public Requirement getRequirement(SimpleData pSimpleData){
        Util.log("[requirements] -> GenTestTools : getRequirment("+pSimpleData.getDescriptionFromModel()+") start");
        Requirement pReqReturn = null;
                
        pReqReturn = (Requirement) simpeDataMapp.get(pSimpleData);
        if (pReqReturn != null){
            return pReqReturn;
        }
        if (pSimpleData instanceof Family){
            // look at the first child (only ReqFamily type)
            pReqReturn = findReqForFamily(pSimpleData);
                        
                        
        } else if (pSimpleData instanceof TestList){
            // Look at the grandson (only ReqFamily type) 
            pReqReturn = findReqForTestList(pSimpleData);
                        
                        
        } else if (pSimpleData instanceof Test){
            // Look at the great-grandson (arriere petit fils) 
            // if search failed
            // for each ReqFamily build prefix until reqLeaf and check

                        
            TestList pTestList = ((Test)pSimpleData).getTestListFromModel();
            Family pFamily = pTestList.getFamilyFromModel();
            if (pFamily.getNameFromModel().equals(DataModel.DEFAULT_FAMILY_NAME)){
                                
                if (pTestList.getNameFromModel().equals(DataModel.DEFAULT_TESTLIST_NAME)){
                    // requirement level is 1
                                        
                    if (!(pSimpleData.getNameFromModel().startsWith(PREFIX_TEST))){
                        return null;
                    }
                    String nameOfrequirement = pSimpleData.getNameFromModel().substring(PREFIX_TEST.length(), pSimpleData.getNameFromModel().length() ); //A VERIFIER
                    DefaultMutableTreeNode reqNode = pOrgReqTree.findRequirementByNameFromParent(nameOfrequirement);
                    //DefaultMutableTreeNode reqNode = searchString(rootReq, nameOfrequirement);
                    if (reqNode == null){
                        return null;
                    }
                    Requirement pReq = (Requirement)reqNode.getUserObject();
                    if (pReq.isFamilyReq() || reqNode.getLevel() > 1){
                        return null;
                    }
                    pReqReturn = pReq;
                                        
                }else {
                    Requirement reqParent = findReqForTestList(pTestList);
                    if (reqParent != null && reqParent.isFamilyReq()){
                        pReqReturn =  findInLeaf(reqParent, pSimpleData, PREFIX_TEST, REQ_LEAF);
                    }
                                        
                }
            } else {
                if (pTestList.getNameFromModel().equals(DataModel.DEFAULT_TESTLIST_NAME)){
                    if (!(pSimpleData.getNameFromModel().startsWith(PREFIX_TEST))){
                        return null;
                    }
                    Requirement reqParent = findReqForFamily(pFamily);
                    if (reqParent != null && reqParent.isFamilyReq()){
                        pReqReturn =  findInLeaf(reqParent, pSimpleData, PREFIX_TEST, REQ_LEAF);
                    }
                }else {
                    //prefix or level 3
                    Requirement reqParent = findReqForTestList(pTestList);
                    if (reqParent != null && reqParent.isFamilyReq()){
                        Vector resultPrefix = getPrefixListForTest(pSimpleData, PREFIX_TEST);
                                                
                        int size = resultPrefix.size();
                        if (size == 0){
                            pReqReturn =  findInLeaf(reqParent, pSimpleData, PREFIX_TEST, REQ_LEAF);
                        } else {
                            int i = 0;
                            boolean trouve = false;
                            String totalPrefix = "";
                            while (!trouve &&  i < size) {
                                totalPrefix+=(String)resultPrefix.elementAt(i) + PREFIX; //A verifier
                                                                
                                reqParent = findInLeafFamilyByName(reqParent, (String)resultPrefix.elementAt(i), "");
                                if (reqParent != null && reqParent.isFamilyReq()){
                                    if (i + 1 == size) {
                                        trouve  = true;
                                    } else {
                                        i++;
                                    }
                                } else {
                                    i = size + 1;
                                }
                            }
                            if (trouve == true){
                                pReqReturn =  findInLeaf(reqParent, pSimpleData, totalPrefix + PREFIX_TEST, REQ_LEAF); //A verifier : totalPrefix + PREFIX_TEST
                            }
                        }
                    }
                }
            }           
        }
        if (pReqReturn != null){
            //simpeDataMapp.put(pSimpleData, pReqReturn);
        }
        Util.log("[requirements] -> GenTestTools : getRequirment("+pSimpleData.getDescriptionFromModel()+") end");
        return pReqReturn;
                
    }
        
    /**
     * Find a prefix List from the Test
     * @param pSimpleData (Test)
     * @param defaultPrefix default prefix used for the simpleData
     * @return the prefix List in a String Vector
     */
    static Vector getPrefixListForTest(SimpleData pSimpleData, String defaultPrefix){
        Util.log("[requirements] -> GenTestTools : getPrefixListForTest("+pSimpleData.getDescriptionFromModel()+", "+defaultPrefix+") start");
        Vector result = new Vector();
        String dataName = pSimpleData.getNameFromModel();
        int beginIndex =  dataName.indexOf(defaultPrefix);
        if (beginIndex != -1 ){ 
            String pathPrefix = dataName.substring(0, beginIndex + defaultPrefix.length());
            StringTokenizer st = new StringTokenizer( pathPrefix, PREFIX);
            while (st.hasMoreTokens()){
                result.add(st.nextToken());
            }
        }
        if (result.size()>0){
            result.remove(result.size()-1);
        }
        Util.log("[requirements] -> GenTestTools : getPrefixListForTest("+pSimpleData.getDescriptionFromModel()+", "+defaultPrefix+") return ="+result);
        Util.log("[requirements] -> GenTestTools : getPrefixListForTest("+pSimpleData.getDescriptionFromModel()+", "+defaultPrefix+") end");
        return result;
    }
        
    /**
     * Find a requirement from the Family
     * @param pSimpleData (Family)
     * @return the requirement corresponding to the Family name
     */
    static Requirement findReqForFamily(SimpleData pSimpleData){
        Util.log("[requirements] -> GenTestTools : findReqForFamily("+pSimpleData.getDescriptionFromModel()+") start");
        // Look at the first child in the pRequirementTree (only ReqFamily type)
        Requirement pReqReturn = null;
                
        pReqReturn = (Requirement) simpeDataMapp.get(pSimpleData);
        if (pReqReturn != null){
            return pReqReturn;
        }
        if (!pSimpleData.getNameFromModel().startsWith(PREFIX_FAMILLE)){
            return null;
        }
        
        String nameOfrequirement = pSimpleData.getNameFromModel().substring(PREFIX_FAMILLE.length(), pSimpleData.getNameFromModel().length() );
        DefaultMutableTreeNode reqNode = pOrgReqTree.findRequirementByNameFromParent(nameOfrequirement);
        //DefaultMutableTreeNode reqNode = searchString(rootReq, nameOfrequirement);
        if (reqNode == null){
            return null;
        }
                
        pReqReturn = (Requirement)reqNode.getUserObject();
        if (!pReqReturn.isFamilyReq() || reqNode.getLevel() > 1){
            return null;
        }
        Util.log("[requirements] -> GenTestTools : findReqForFamily("+pSimpleData.getDescriptionFromModel()+") end");
        return  pReqReturn;
    }
        
        
    /**
     * Find a requirement from the TestList
     * @param pSimpleData (TestList)
     * @return the requirement corresponding to the TestList Name
     */
    static Requirement findReqForTestList(SimpleData pSimpleData){
        Util.log("[requirements] -> GenTestTools : findReqForTestList("+pSimpleData.getDescriptionFromModel()+") start");
        Requirement pReqReturn = null;
                
        pReqReturn = (Requirement) simpeDataMapp.get(pSimpleData);
        if (pReqReturn != null){
            return pReqReturn;
        }
        // Look at the great-grandson (arriere petit fils) 
        Requirement pRootReq = pOrgReqTree.getRootRequirement();
        //Requirement pRootReq = (Requirement) rootReq.getUserObject();
        if (pRootReq == null || !pRootReq.isFamilyReq() ){
            return null;
        }
        Vector listFirstLeaf = ((ReqFamily)pRootReq).getFirstFamily();
        int size = listFirstLeaf.size();
        int i = 0;
        boolean trouve = false;
        while (!trouve && i < size){
            Requirement pTempReq = (Requirement) listFirstLeaf.elementAt(i);
            Requirement reqFind = findInLeaf(pTempReq, pSimpleData, PREFIX_SUITE, REQ_FAMILY);
            if (reqFind != null){
                trouve = true;
                pReqReturn = reqFind;
            }
            i++;
        }
                
        Util.log("[requirements] -> GenTestTools : findReqForTestList("+pSimpleData.getDescriptionFromModel()+") end");
        return  pReqReturn;
    }
        
        
    /**
     * Find a requirement from a Family requirement
     * @param pReq the selected requirement
     * @param name of the requirement to find
     * @param prefix prefix corresponding to the test
     * @return the found requirement or null 
     */
    static Requirement findInLeafFamilyByName(Requirement pReq, String name, String prefix ){
        Util.log("[requirements] -> GenTestTools : findInLeafFamilyByName("+pReq.getDescriptionFromModel()+", "+name+", "+prefix+") start");
        Requirement pReqReturn = null;
        if (pReq.isFamilyReq()){
            Vector listFirstLeaf = ((ReqFamily)pReq).getFirstFamily();
            int size = listFirstLeaf.size();
            int i = 0;
            boolean trouve = false;
            while (!trouve && i < size){
                Requirement pTempReq = (Requirement) listFirstLeaf.elementAt(i);
                if (pTempReq.isFamilyReq()){
                    if (name.equals(prefix+pTempReq.getNameFromModel())){
                        pReqReturn = pTempReq;
                        trouve = true;
                    }
                }
                i++;
            }
        }
        Util.log("[requirements] -> GenTestTools : findInLeafFamilyByName("+pReq.getDescriptionFromModel()+", "+name+", "+prefix+") end");
        return pReqReturn;      
    }
        
        
    /**
     * Find a requirement from a test 
     * @param pReq  the requirement root to start the research
     * @param pSimpleData the test to find
     * @param prefix prefix corresponding to the test
     * @param type REQ_FAMILY=0 or REQ_LEAF = 1
     * @return the found requirement or null 
     */
    static Requirement findInLeaf(Requirement pReq, SimpleData pSimpleData, String prefix, int type){
        Util.log("[requirements] -> GenTestTools : findInLeaf("+pReq.getDescriptionFromModel()+", "+pSimpleData.getDescriptionFromModel()+", "+prefix+", "+type+")  start");
        Requirement pReqReturn = null;
        if (pReq.isFamilyReq()){
            Vector listFirstLeaf = new Vector();
            if (type == REQ_FAMILY){
                listFirstLeaf = ((ReqFamily)pReq).getFirstFamily();
            } else {
                listFirstLeaf = ((ReqFamily)pReq).getFistLeaf();
            }
            int size = listFirstLeaf.size();
            int i = 0;
            boolean trouve = false;
            while (!trouve && i < size){
                Requirement pTempReq = (Requirement) listFirstLeaf.elementAt(i);
                if (pSimpleData.getNameFromModel().equals(prefix+pTempReq.getNameFromModel())){
                    pReqReturn = pTempReq;
                    trouve = true;
                }
                i++;
            }
        }
        Util.log("[requirements] -> GenTestTools : findInLeaf("+pReq.getDescriptionFromModel()+", "+pSimpleData.getDescriptionFromModel()+", "+prefix+", "+type+")  end");
        return pReqReturn;      
    }
        
    //static Vector getFistLeaf(Requirement pReq){
    //Vector listFirstLeaf = new Vector();
    //DefaultMutableTreeNode reqNode = pOrgReqTree.findRequirementFromParent(pReq);
    //}
    /**
     * get the simpledata corresponding to the requirement
     * @param pReq the selected requirement
     * @param path path from root to the simpleData
     * @param create force to create simpleData if not found
     * @return the simpledata found  or if create true it returns a new simpledata corresponding to the requirement
     */
    static public SimpleData getSimpleData(Requirement pReq, Vector path, boolean create){
        Util.log("[requirements] -> GenTestTools : getSimpleData("+pReq.getDescriptionFromModel()+", "+path+", "+create+")  start");
        SimpleData pSimpleData = null;
                
        // Does  SimpleData exist ?
        pSimpleData = (SimpleData) reqMapp.get(pReq);
        if (pSimpleData != null){
            // Already created
            return pSimpleData;
        }
                
        int level = getLevel(pReq);
        if (level == -1){
            return null;
        }
                
        int type = -1;
        if (pReq.isFamilyReq()){
            type = REQ_FAMILY;
        } else {
            type = REQ_LEAF;
        }
                
        if (type == REQ_FAMILY && level == 1){
            // Se transforme en Famille
            pSimpleData = getOrCreateFamily(pReq, true, create);
            if(pSimpleData!=null)path.add(pSimpleData);
                
        } else if (type == REQ_FAMILY && level == 2){
            // Se transforme en Suite           
            Family pFamily =  (Family) getOrCreateFamily(pReq.getParent(), true, create);
            if(pFamily!=null){
                path.add(pFamily);
                pSimpleData = getOrCreateSuite(pReq, pFamily, true, create);
                if(pSimpleData!=null)path.add(pSimpleData);
            }   
        } else if (type == REQ_FAMILY && level > 2){
            // Pas de transformation mais prefix
                        
        } else if (type == REQ_LEAF && level == 1){
            Family pFamily =  getDefaultFamily();
            if(pFamily!=null)
                {
                    path.add(pFamily);
                    TestList pTestList = getDefaultTestList(pFamily);
                    if(pTestList!=null)path.add(pTestList);
                        
                    // transformed in test (FamilY def, TestList def)
                    pSimpleData = getOrCreateTest(pReq,pFamily,pTestList, true , "", create);
                    if(pSimpleData!=null)path.add(pSimpleData);
                }
        } else if (type == REQ_LEAF && level == 2){
            Family pFamily =  (Family) getOrCreateFamily(pReq.getParent(), true, create);
                        
            if(pFamily!=null)
                {
                    path.add(pFamily);
                    TestList pTestList = getDefaultTestList(pFamily);
                    if(pTestList!=null)path.add(pTestList);
                        
        
                    // Se transforme en test (Famille def, Suite nom du per)
                    pSimpleData = getOrCreateTest(pReq,pFamily,pTestList,true, "", create);
                    if(pSimpleData!=null)path.add(pSimpleData);
                }
        } else if (type == REQ_LEAF && level  == 3){
            // Se transforme en test (Famille nom  du grand pere , Suite nom du pere)
            Family pFamily =  (Family) getOrCreateFamily(pReq.getParent().getParent(),true, create);
            if(pFamily!=null) {
                path.add(pFamily);
                        
                TestList pTestList = (TestList)getOrCreateSuite(pReq.getParent(),pFamily,true, create);
                if(pTestList!=null) {
                    path.add(pTestList);
                    // Se transforme en test (Famille def, Suite nom du per)
                    pSimpleData = getOrCreateTest(pReq,pFamily,pTestList,true,"", create);
                    if(pSimpleData!=null){
                        path.add(pSimpleData);
                    }
                }
            }
                        
        } else if (type == REQ_LEAF && level  > 3){
            Vector parent = new Vector();
            String prefix = getPrefix(pReq, level, parent);
                        
            Requirement pReqSuite = (Requirement) parent.elementAt(0);
            Family pFamily =  (Family) getOrCreateFamily(pReqSuite.getParent(),true, create);
            if(pFamily!=null)
                {
                    path.add(pFamily);
                    TestList pTestList = (TestList)getOrCreateSuite(pReqSuite,pFamily,true, create);
                    if(pTestList!=null) {
                        path.add(pTestList);
                                
                        pSimpleData = getOrCreateTest(pReq, pFamily, pTestList, true, prefix, create );
                        if(pSimpleData!=null) {
                            path.add(pSimpleData);
                        }
                    }
                }
        } 
        Util.log("[requirements] -> GenTestTools : getSimpleData("+pReq.getDescriptionFromModel()+", "+path+", "+create+")  end");
        return pSimpleData;
    }
        
    /**
     * Get prefix corresponding to the requirement
     * @param pReq the selected requirement 
     * @param level the level 
     * @param parent List of requirement parents
     * @return
     */
    static String getPrefix(Requirement pReq, int level, Vector parent){
        Util.log("[requirements] -> GenTestTools : getPrefix("+pReq.getDescriptionFromModel()+", "+level+", "+parent+")  start");
        int remonte = level - 3;
        String prefix = "";
        Requirement pCurrentReq = pReq;
        for (int i = 0 ; i< remonte ; i++){
            Requirement pReqParent = pCurrentReq.getParent();
            if (prefix.equals("")){
                prefix = pReqParent.getNameFromModel() ;
            }else {
                prefix = pReqParent.getNameFromModel() + PREFIX + prefix;
            }
            pCurrentReq = pReqParent;
        }
        prefix+= ".";
        parent.add(pCurrentReq.getParent());
        Util.log("[requirements] -> GenTestTools : getPrefix("+pReq.getDescriptionFromModel()+", "+level+", "+parent+")  return ="+prefix);
        Util.log("[requirements] -> GenTestTools : getPrefix("+pReq.getDescriptionFromModel()+", "+level+", "+parent+")  end");
        return prefix;
    }
        
        
    /**
     * get Requirement level
     * @param pReq the Requirement selected
     * @param pRequirementTree the current Requiremenet tree
     * @return the Requirment level
     */
    static int getLevel(Requirement pReq){
        Util.log("[requirements] -> GenTestTools : getLevel("+pReq.getDescriptionFromModel()+")  start");
        int level = -1;
        DefaultMutableTreeNode reqNode = pOrgReqTree.findRequirementFromParent(pReq);
        //DefaultMutableTreeNode reqNode = searchString(rootReq, pReq.getNameFromModel());
        if (reqNode != null) {
            level = reqNode.getLevel();
        }
        Util.log("[requirements] -> GenTestTools : getLevel("+pReq.getDescriptionFromModel()+")  return ="+level);
        Util.log("[requirements] -> GenTestTools : getLevel("+pReq.getDescriptionFromModel()+")  end");
        return level;
    }
        
        
        
    /**
     * 
     * @return the default family 
     */
    static Family getDefaultFamily(){
        Util.log("[requirements] -> GenTestTools : getDefaultFamily()  start");
        Family pFamily = null;
        DefaultMutableTreeNode pSimpleDataNode = pCopyDynamicTestTree.findRemoveFamilyNode(DataModel.DEFAULT_FAMILY_NAME, false);
        if (pSimpleDataNode != null) {
            // Already created
            pFamily = (Family) pSimpleDataNode.getUserObject();
        } else {
            // Create one
            pFamily =  new Family(DataModel.DEFAULT_FAMILY_NAME, DataModel.DEFAULT_FAMILY_DESC);
        }
        Util.log("[requirements] -> GenTestTools : getDefaultFamily()  return ="+pFamily.getNameFromModel());
        Util.log("[requirements] -> GenTestTools : getDefaultFamily()  end");
        return pFamily;
    }
        
    /**
     * 
     * @param pFamily
     * @return the default TestList and add testList to the family
     */
    static TestList getDefaultTestList(Family pFamily){
        Util.log("[requirements] -> GenTestTools : getDefaultTestList()  start");
        TestList pTestList = null;
        DefaultMutableTreeNode pSimpleDataNode = pCopyDynamicTestTree.findRemoveTestListNode(DataModel.DEFAULT_TESTLIST_NAME, pFamily.getNameFromModel(), false);
        if (pSimpleDataNode != null) {
            // Already created
            pTestList = (TestList) pSimpleDataNode.getUserObject();
        } else {
            // Create one
            pTestList =  new TestList(DataModel.DEFAULT_TESTLIST_NAME, DataModel.DEFAULT_TESTLIST_DESC);
            pFamily.addTestListInModel(pTestList);
        }
        Util.log("[requirements] -> GenTestTools : getDefaultTestList()  return ="+pTestList.getNameFromModel());
        Util.log("[requirements] -> GenTestTools : getDefaultTestList()  end");
        return pTestList;
    }
        

        
    /**
     * get the family of the corresponding selected requirement
     * @param pReq the selected requirement
     * @param insertInMap keep family 
     * @param create force to create a new family
     * @return get the family if found or if create it returns a new family corresponding to the requirement
     */
    static SimpleData getOrCreateFamily(Requirement pReq, boolean insertInMap, boolean create){
        Util.log("[requirements] -> GenTestTools : getOrCreateFamily("+pReq.getNameFromModel()+", "+insertInMap+","+create+")  start");
        SimpleData pSimpleData = null;
                
        pSimpleData = (SimpleData) reqMapp.get(pReq);
        if (pSimpleData != null){
            return pSimpleData;
        }
        
        DefaultMutableTreeNode pSimpleDataNode = pCopyDynamicTestTree.findRemoveFamilyNode(PREFIX_FAMILLE + pReq.getNameFromModel(), false);
        if (pSimpleDataNode != null) {
            // Already created
            pSimpleData = (SimpleData) pSimpleDataNode.getUserObject();
            if (insertInMap){
            }
        } else {
            // Create one
            if (create){
                pSimpleData =  new Family(PREFIX_FAMILLE + pReq.getNameFromModel(), pReq.getDescriptionFromModel());
                if (insertInMap){
                }
            }
        }
        Util.log("[requirements] -> GenTestTools : getOrCreateFamily("+pReq.getNameFromModel()+", "+insertInMap+","+create+")  end");
        return pSimpleData;
    }
        
        
    /**
     * get the testList of the corresponding selected requirement
     * @param pReq the selected requirement
     * @param pFamily the family 
     * @param insertInMap
     * @param create force to create a new testList
     * @return get the family if found or if create it returns a new family corresponding to the requirement
     */
    static SimpleData getOrCreateSuite(Requirement pReq, Family pFamily, boolean insertInMap, boolean create){
        Util.log("[requirements] -> GenTestTools : getOrCreateSuite("+pReq.getNameFromModel()+", "+pFamily.getNameFromModel()+", "+insertInMap+","+create+")  start");
        SimpleData pSimpleData = null;
                
        pSimpleData = (SimpleData) reqMapp.get(pReq);
        if (pSimpleData != null){
            return pSimpleData;
        }
                
        DefaultMutableTreeNode pSimpleDataNode = pCopyDynamicTestTree.findRemoveTestListNode(PREFIX_SUITE + pReq.getNameFromModel(), pFamily.getNameFromModel(),  false);
        if (pSimpleDataNode != null) {
            // Already created
            pSimpleData = (SimpleData) pSimpleDataNode.getUserObject();
        } else {
            // Create one
            if (create){
                pSimpleData =  new TestList(PREFIX_SUITE + pReq.getNameFromModel(), pReq.getDescriptionFromModel());
                pFamily.addTestListInModel((TestList)pSimpleData);
            }
        }
                
        Util.log("[requirements] -> GenTestTools : getOrCreateSuite("+pReq.getNameFromModel()+", "+pFamily.getNameFromModel()+", "+insertInMap+","+create+")  end");
        return pSimpleData;
    }
        
    /**
     *  Get the test if exists or creates it if not.
     * @param pReq the requirement corresponding to the test 
     * @param pFamily the family
     * @param pTestList the testList
     * @param insertInMap using a cache or not
     * @param prefix prefix used
     * @param create force create if not exists
     * @return the corresponding test
     */
    static SimpleData getOrCreateTest(Requirement pReq, Family pFamily, TestList pTestList,  boolean insertInMap, String prefix , boolean create){
        Util.log("[requirements] -> GenTestTools : getOrCreateSuite("+pReq.getNameFromModel()+", "+pFamily.getNameFromModel()+", "+pTestList.getDescriptionFromModel()+", "+insertInMap+","+create+")  start");
        SimpleData pSimpleData = null;
                
        pSimpleData = (SimpleData) reqMapp.get(pReq);
        if (pSimpleData != null){
            return pSimpleData;
        }
                
        DefaultMutableTreeNode pSimpleDataNode = pCopyDynamicTestTree.findRemoveTestNode(prefix + PREFIX_TEST + pReq.getNameFromModel(), pTestList.getNameFromModel(), pFamily.getNameFromModel(),  false);
                
        //Add by MIK
        boolean exist_covered = false; 
        if (pSimpleDataNode != null){
            pSimpleData = (SimpleData) pSimpleDataNode.getUserObject();
            exist_covered = isReqBijectiveCovered(pReq, pSimpleData.getNameFromModel());
        }
                
        if (exist_covered){
            if (insertInMap) {
            }
                        
        } else {
            pSimpleData = null; //Added by MM
            // Create one
            if (create){
                pSimpleData =  new ManualTest(prefix + PREFIX_TEST + pReq.getNameFromModel(), pReq.getDescriptionFromModel());
                pTestList.addTestInModel((Test)pSimpleData);
                addReqBijectiveCovered(pReq, pSimpleData.getNameFromModel()); //Added by MM
                addSimleDataToDynamicTestTree(pSimpleData); //Added by MM

            }
        }
                
        Util.log("[requirements] -> GenTestTools : getOrCreateSuite("+pReq.getNameFromModel()+", "+pFamily.getNameFromModel()+", "+pTestList.getDescriptionFromModel()+", "+insertInMap+", "+create+")  end");
        return pSimpleData;
    }
        
        
        
        
        
    /**************************** TOOLS ***********************************/
        

    /**
     * Recursive procedure that builds the test tree by parsing the requirement tree
     * @param pTempNode the currentNode
     * @param pRequirementTree the requirement tree
     */
    public static void getBijectiveSimpleDataTree(DefaultMutableTreeNode pTempNode, SimpleDynamicTestTree dynTestTree) 
    {
        Util.log("[requirements] -> GenTestTools : addSimpleDataTree : start ");
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        SimpleData data  = getSimpleData(pTempReq, new Vector(), false);
        Util.log("[requirements] -> GenTestTools : addSimpleDataTree : Data for " + pTempReq + ", is " + data);
                
        if (data != null) {     
            /* if data element is a test */
            if(data instanceof  Test) {
                if(data.getNameFromModel().length()<255)
                    {
                        TestList testList=(TestList)((Test)data).getTestListFromModel();
                        Family family=testList.getFamilyFromModel();
                                        
                        String familyName = family.getNameFromModel();
                        String testListName = testList.getNameFromModel();
                        String testName = data.getNameFromModel();
                                        
                        /* Add family if not in model*/
                        DefaultMutableTreeNode familyNode = dynTestTree.findRemoveFamilyNode(familyName,false);
                        if(familyNode==null) {
                            familyNode =dynTestTree.addObject(dynTestTree.getRoot(),family);
                        }
                        
                        /* Add testList if not in model  */
                        DefaultMutableTreeNode testListNode = dynTestTree.findRemoveTestListNode(testListName,familyName,false);
                        if(testListNode==null) {
                            testListNode = dynTestTree.addObject(familyNode,testList);
                        }
        
                        /* Add test in model */
                        DefaultMutableTreeNode testNode = dynTestTree.findRemoveTestNode(testName,testListName,familyName,false);
                        if(testNode==null)  {
                            dynTestTree.addObject(testListNode,data);   
                        }
                    }
            }
                        
        }

        if (pTempNode.getChildCount() == 0)  {
            // On Stop
            return;
        } 
        else  {
            int nbChild =  pTempNode.getChildCount();
            int i = 0;
            while (i < nbChild) {       
                getBijectiveSimpleDataTree((DefaultMutableTreeNode)pTempNode.getChildAt(i), dynTestTree);
                i++;
            }
        }
        Util.log("[requirements] -> GenTestTools : addSimpleDataTree : end ");
    }
        
        
    //  ADD by MM
    /**
     * Add the simpleData to the pCopyDynamicTestTree
     * @param data simpleData to add
     */
    static void addSimleDataToDynamicTestTree(SimpleData data){
        Util.log("[requirements] -> GenTestTools : addSimleDataToDynamicTestTree("+data.getDescriptionFromModel()+") : start ");
        if(data != null && data instanceof  Test) {
            TestList testList=(TestList)((Test)data).getTestListFromModel();
            Family family=testList.getFamilyFromModel();
                        
            String familyName = family.getNameFromModel();
            String testListName = testList.getNameFromModel();
            String testName = data.getNameFromModel();
                        
            /* Add family if not in model*/
            DefaultMutableTreeNode familyNode = pCopyDynamicTestTree.findRemoveFamilyNode(familyName,false);
            if(familyNode==null) {
                familyNode =pCopyDynamicTestTree.addObject(null, family, false);
            }
            
            /* Add testList if not in model  */
            DefaultMutableTreeNode testListNode = pCopyDynamicTestTree.findRemoveTestListNode(testListName,familyName,false);
            if(testListNode==null) {
                testListNode = pCopyDynamicTestTree.addObject(familyNode, testList, false);
            }

            /* Add test in model */
            DefaultMutableTreeNode testNode = pCopyDynamicTestTree.findRemoveTestNode(testName,testListName,familyName,false);
            if(testNode==null)  {
                pCopyDynamicTestTree.addObject(testListNode, data, false);      
            }
        }
        Util.log("[requirements] -> GenTestTools : addSimleDataToDynamicTestTree("+data.getDescriptionFromModel()+") : end ");
    }
        

    /**
     * Remove the simpleData to the pCopyDynamicTestTree
     * @param data simpleData to remove
     */
    public static void removeSimpleDataToDynamicTestTree(SimpleData data){
        Util.log("[requirements] -> GenTestTools : removeSimpleDataToDynamicTestTree("+data.getDescriptionFromModel()+") : start ");
        if (data == null){
            return;
        } else if (data instanceof Test){
            TestList testList=(TestList)((Test)data).getTestListFromModel();
            Family family=testList.getFamilyFromModel();
            String familyName = family.getNameFromModel();
            String testListName = testList.getNameFromModel();
            String testName = data.getNameFromModel();
                
            DefaultMutableTreeNode testNode = pCopyDynamicTestTree.findRemoveTestNode(testName,testListName,familyName, false);
            if (testNode != null){
                DefaultMutableTreeNode suiteNode = (DefaultMutableTreeNode) testNode.getParent();
                suiteNode.remove(testNode);
                if (suiteNode.getChildCount() == 0){
                    removeSimpleDataToDynamicTestTree((SimpleData)suiteNode.getUserObject());
                }
            }
            //remove testLinkToReq if link not in BDD
            removeNotDefaultReqBijectiveCovered(data.getNameFromModel());
        } else if (data instanceof TestList){
            TestList testList = (TestList) data;
            Family family=testList.getFamilyFromModel();
            String familyName = family.getNameFromModel();
            String testListName = testList.getNameFromModel();
            DefaultMutableTreeNode testListNode = pCopyDynamicTestTree.findRemoveTestListNode(testListName,familyName, false);
            if(testListNode != null) {
                DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) testListNode.getParent();
                familyNode.remove(testListNode);
                if (familyNode.getChildCount() ==  0){
                    removeSimpleDataToDynamicTestTree((SimpleData)familyNode.getUserObject());
                }
            }
        } else if (data instanceof Family){
            Family family= (Family) data;
            String familyName = family.getNameFromModel();
            DefaultMutableTreeNode familNode =  pCopyDynamicTestTree.findRemoveFamilyNode(familyName, false);
            if(familNode != null) {
                DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) familNode.getParent();
                rootNode.remove(familNode);
            }
        }
        Util.log("[requirements] -> GenTestTools : removeSimpleDataToDynamicTestTree("+data.getDescriptionFromModel()+") : start ");
    }
        
        
        
    // ADD by MM
    /**
     * create a copy of a DynamicTree
     * @param _pDynamicTree
     * @return the  new test dynamic tree
     */
    static SimpleDynamicTestTree createCopyOfTestTree( DynamicTree _pDynamicTree)  {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)((DefaultTreeModel)_pDynamicTree.getModel()).getRoot();
        DefaultMutableTreeNode nodeCopy = getCopyNode(node);
        SimpleDynamicTestTree _pCopyDynamicTestTree = new SimpleDynamicTestTree(nodeCopy , DataConstants.FAMILY);
        _pCopyDynamicTestTree.getModel().setRoot(nodeCopy);
        return _pCopyDynamicTestTree;
    }
        
        
    /**
     * create a copy of a node
     * @param node
     * @return the new copied node
     */
    static DefaultMutableTreeNode getCopyNode(DefaultMutableTreeNode node){
        DefaultMutableTreeNode copy_node = new DefaultMutableTreeNode(node.getUserObject());
        int nbChild = node.getChildCount();
        if (nbChild == 0){
            return copy_node;
        } else {
            int i = 0;
            while (i < nbChild){
                DefaultMutableTreeNode child_copy = getCopyNode((DefaultMutableTreeNode)node.getChildAt(i));
                copy_node.insert(child_copy, i);
                i++;
            }
        }
        return copy_node;
    }
        

    /**
     * remove link from requirement if at least one  test linked to it contains the same name 
     * of the requirement. It removes also requirement leaf from the available requirement tree.
     * @param treeModel requirement tree model
     * @param pTempNode requirement node to start recursive function 
     * @return true if one test have the same 
     */
    public static boolean removeReqBijectiveLinked(DefaultTreeModel treeModel, DefaultMutableTreeNode pTempNode ) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        if (pTempReq instanceof ReqLeaf){
            SimpleData pTest = getSimpleData(pTempReq,  new Vector(), false);
            boolean res = false;
            if (pTest instanceof Test){
                res = isReqBijectiveCovered( pTempReq, pTest.getNameFromModel());
                if (res){
                    treeModel.removeNodeFromParent(pTempNode); 
                }
            }
            return res;
        } 
        else  {
            int nbChild =  pTempNode.getChildCount(); 
            for (int i = 0; i < nbChild ; i++) { 
                if (removeReqBijectiveLinked(treeModel, (DefaultMutableTreeNode)pTempNode.getChildAt(i))) {
                    i--;
                    nbChild--;
                } 
            }
            if (pTempNode.getChildCount() == 0 && !pTempNode.equals(treeModel.getRoot())) {
                treeModel.removeNodeFromParent(pTempNode);
                return true;
            }
        }
        return false;
    }
                   
    /**
     * Build the requirement tree without requirements linked to test which checks the bijection
     *
     */
    static void initReqLink(){
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)((DefaultTreeModel)pOrgReqTree.getModel()).getRoot();
        initReqWithTestLink((DefaultTreeModel)pOrgReqTree.getModel(), node);       
    }
           
    /**
     * Init requirements by adding those which are linked to test
     * @param treeModel the requirement tree model
     * @param pTempNode the root node
     */
    static void initReqWithTestLink(DefaultTreeModel treeModel, DefaultMutableTreeNode pTempNode ) {
        Requirement pTempReq = (Requirement)pTempNode.getUserObject();
        if (pTempReq instanceof ReqLeaf){
            try  {
                Vector testLinked = pTempReq.getTestWrapperCoveredFromDB();
                int testLinkedSize = testLinked.size();
                Vector linkedTestName = new Vector();
                Vector linkedTestName2 = new Vector();
                                   
                for (int i = 0; i < testLinkedSize ; i ++){
                    TestWrapper testWrap=(TestWrapper)testLinked.elementAt(i);
                    linkedTestName.add(testWrap.getName()); 
                    linkedTestName2.add(testWrap.getName());
                }
                                 
                //reqLink.put(pTempReq, linkedTestName);
                //defaultreqLink.put(pTempReq, linkedTestName2);
                reqLink.put(pTempReq.getNameFromModel(), linkedTestName);
                defaultreqLink.put(pTempReq.getNameFromModel(), linkedTestName2);
            } catch(Exception e) {
                                  
                //reqLink.put(pTempReq, new Vector());
                //defaultreqLink.put(pTempReq, new Vector());
                reqLink.put(pTempReq.getNameFromModel(), new Vector());
                defaultreqLink.put(pTempReq.getNameFromModel(), new Vector());
            }
        }  else  {
            int nbChild =  pTempNode.getChildCount(); 
            for (int i = 0; i < nbChild ; i++) { 
                initReqWithTestLink(treeModel, (DefaultMutableTreeNode)pTempNode.getChildAt(i));
            }
        }
    }
           
           
    /**
     * Remove test link to the requirement
     * @param testName name of the test to unlink
     */
    static void removeNotDefaultReqBijectiveCovered(String testName){
        Vector linkedReq = (Vector) testLink.get(testName);
        if (linkedReq == null){
            return;
        }
        for (int i = 0 ; i < linkedReq.size(); i++){
            Requirement pTempReq  = (Requirement) linkedReq.elementAt(i); 
            removeReqBijectiveCovered(pTempReq, testName);

        }
    }
           
    /**
     * Add test link to the requirement
     * @param pTempReq a requirement leaf
     * @param testName name of the test to add
     */
    static void addTestBijectiveCovered( Requirement pTempReq, String testName){
        Vector linkedReq = (Vector) testLink.get(testName);
        if (linkedReq == null){
            linkedReq = new Vector(); 
            linkedReq.add(pTempReq); 
            testLink.put(testName, linkedReq);
            return;
        }
        if (!linkedReq.contains(pTempReq)){
            linkedReq.add(pTempReq);
        }  
    }
           
    /**
     * Remove link from test
     * @param pTempReq a requirement leaf
     * @param testName name of the test to unlink
     */
    static void removeTestBijectiveCovered( Requirement pTempReq, String testName){
        Vector linkedReq = (Vector) testLink.get(testName);
        if (linkedReq == null){
            linkedReq = new Vector();
            testLink.put(testName, linkedReq);
            return;
        }
        if (linkedReq.contains(pTempReq)){
            linkedReq.remove(pTempReq);
        }
    }
           
           
    /**
     *  
     * @param pTempReq a requirement leaf
     * @param testName name of the test
     * @return  true if Requirement pTempReq is  linked in DB  with testName 
     */
    public static boolean isReqDefaultCovered( Requirement pTempReq, String testName){
        boolean covered = false;
        if (pTempReq instanceof ReqLeaf){
            // Vector linkedTestName = (Vector) defaultreqLink.get(pTempReq);
            Vector linkedTestName = (Vector) defaultreqLink.get(pTempReq.getNameFromModel());
            return linkedTestName.contains(testName);  
        }
        return covered;
    }
           
    /**
     * 
     * @param pTempReq a requirement leaf
     * @param testName name of the test
     * @return Return true if Requirement is linked in model with testName 
     */
    public static boolean isReqBijectiveCovered( Requirement pTempReq, String testName){
        boolean covered = false;
        if (pTempReq instanceof ReqLeaf){
            //Vector linkedTestName = (Vector) reqLink.get(pTempReq);
            Vector linkedTestName = (Vector) reqLink.get(pTempReq.getNameFromModel());
            return linkedTestName.contains(testName);  
        }
        return covered;
    }
           
         
    /**
     * 
     * @param pTempReq a requirement leaf
     * @param testName name of the test to add
     */
    static void addReqBijectiveCovered( Requirement pTempReq, String testName){
        if (pTempReq instanceof ReqLeaf){
            //Vector linkedTestName = (Vector) reqLink.get(pTempReq);
            Vector linkedTestName = (Vector) reqLink.get(pTempReq.getNameFromModel());
            if (!linkedTestName.contains(testName)){
                linkedTestName.add(testName);
            }
            addTestBijectiveCovered(pTempReq, testName);
        }
    }
            
    /**
     * Remove link from requirement
     * @param pTempReq the selected requirement
     * @param testName the name of the test
     */
    static void removeReqBijectiveCovered(Requirement pTempReq, String testName){
        if (pTempReq instanceof ReqLeaf){
            //Vector linkedTestName = (Vector) reqLink.get(pTempReq);
            Vector linkedTestName = (Vector) reqLink.get(pTempReq.getNameFromModel());
            if (linkedTestName.contains(testName)){
                linkedTestName.remove(testName);
            }
        }
    }
           
           
    /**
     *  print all requirements links
     *
     */
    /* static public void printData(){
       Enumeration reqEnum = defaultreqLink.keys();
       System.out.println("Database Req Cover");
       while (reqEnum.hasMoreElements()){
       Requirement pTempReq = (Requirement) reqEnum.nextElement();
                          
       System.out.println("\tReq "+ pTempReq + " is cover by");
       Vector linkedTestName = (Vector) defaultreqLink.get(pTempReq);
       for (int i = 0; i < linkedTestName.size(); i++){
       System.out.println("\t\t test "+ linkedTestName.elementAt(i));
       }
       } 
       System.out.println("");
                   
       reqEnum = reqLink.keys();
       System.out.println("Model Req Cover");
       while (reqEnum.hasMoreElements()){
       Requirement pTempReq = (Requirement) reqEnum.nextElement(); 
       System.out.println("\tReq "+ pTempReq + "( "+ ((Object)pTempReq).hashCode()  +") is cover by");
       Vector linkedTestName = (Vector) reqLink.get(pTempReq);
       for (int i = 0; i < linkedTestName.size(); i++){
       System.out.println("\t\t test "+ linkedTestName.elementAt(i));
       }
       }
       }*/
           
    /**
     * Find a node by name in tree
     * @param pTempNode
     * @param name
     * @return
     */
    /*static DefaultMutableTreeNode searchString(DefaultMutableTreeNode pTempNode, String name) {
      Requirement pTempReq = (Requirement)pTempNode.getUserObject();
      DefaultMutableTreeNode retNode = null;
      if (pTempReq.getNameFromModel().equals(name)) {
      return pTempNode;
      } else {
      int nbChild =  pTempNode.getChildCount();
      boolean trouve = false;
      int i = 0;
      while (i< nbChild && !trouve){
      retNode = searchString((DefaultMutableTreeNode)pTempNode.getChildAt(i), name);
      if (retNode != null){
      trouve = true;
      }
      i++;
      }
      return retNode;
      }
      }*/
}

