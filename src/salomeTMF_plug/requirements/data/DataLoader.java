/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.data;

import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Util;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.ihm.RequirementTree;
import salomeTMF_plug.requirements.sqlWrapper.ISQLRequirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

public class DataLoader {
        
    static boolean recurMode = true;
    static public  Requirement loadData(RequirementTree reqTree) throws Exception{
        int transNumber = -1;
        Requirement rootReq;
        Hashtable<Integer, ReqWrapper> reqProjet = new  Hashtable<Integer,ReqWrapper>();
        try {
            transNumber = SQLWrapper.beginTransaction();
            Hashtable<Integer,Requirement> reqInserted = new Hashtable<Integer,Requirement>();
            rootReq = reqTree.getRootRequirement();
            rootReq.deleteInModel();
            if (rootReq == null){
                throw new Exception ("[DataLoader->loadData] no parent in requirement tree");
            }
            reqInserted.put(0, rootReq);
            ISQLRequirement pSQLRequirement = SQLWrapper.getSQLRequirement();
            //Vector allReq = new Vector();
            ReqWrapper[] tmpArray = pSQLRequirement.getProjectRequirements(ReqPlugin.getProjectRef().getIdBdd());
            for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
                ReqWrapper pReqWrapper = tmpArray[tmpI];
                reqProjet.put(pReqWrapper.getIdBDD(), pReqWrapper);     
            }
                        
                        
            if (recurMode){
                for (int i = 0; i < tmpArray.length ; i++){
                    insertRequimentFunc(reqTree, tmpArray[i] ,reqInserted, reqProjet );
                }
            } else {
                int previousSize = tmpArray.length;
                boolean continu = true;
                Vector<ReqWrapper> resteAInserer = new Vector<ReqWrapper>();
                while (continu){
                    resteAInserer.clear();
                    for (int i = 0; i < tmpArray.length ; i++){
                        boolean inserted = insertRequimentIT(reqTree, tmpArray[i] ,reqInserted, reqProjet );
                        if (!inserted){
                            resteAInserer.add(tmpArray[i]);
                        }
                    }
                    int size = resteAInserer.size();
                    Util.log("[DataLoader->insertRequiment] Requirments not insterd number " + size);
                    Util.log("[DataLoader->insertRequiment] Nb Req to insert" + tmpArray.length);
                    Util.log("[DataLoader->insertRequiment] Nb Req inserted" + reqInserted.size());
                    Util.log("[DataLoader->insertRequiment] Nb Req in Projet" + reqProjet.size());
                    if (size == previousSize){
                        Util.log("[DataLoader->insertRequiment] Data are corrupted for requirments ");
                        continu = false;
                    }  else {
                        if (size > 0){
                            tmpArray = new ReqWrapper[size];
                            for (int j = 0 ; j < size; j++){
                                tmpArray[j] = resteAInserer.elementAt(j);
                            }
                        } else {
                            continu = false;
                        }
                    }
                }
            }
                        
                        
                        
            SQLWrapper.commitTrans(transNumber);
        } catch (Exception e){
            SQLWrapper.rollBackTrans(transNumber);
            throw e;
        }
        return rootReq;
        // Construction de l'arbre
    }
        
    static void insertRequimentFunc(RequirementTree reqTree, ReqWrapper pReqWrapper,
				    Hashtable<Integer,Requirement> reqInserted, Hashtable<Integer, 
				    ReqWrapper> reqProjet ) throws Exception{
        int idP = pReqWrapper.getParent_id();
        int idR = pReqWrapper.getIdBDD();
        if (idP == idR){
            Util.log("[DataLoader->insertRequiment] Data are corrupted for requirments " + pReqWrapper);
            Util.log("[DataLoader->insertRequiment] restore the requiement " + pReqWrapper + " at root");
            String newName = "Restore1_" + pReqWrapper.getName();
            pReqWrapper.setName(newName);
            pReqWrapper.setParent_id(0);
            try {
                ISQLRequirement pSQLRequirement = SQLWrapper.getSQLRequirement();
                pSQLRequirement.updateParent(idR, 0);
                pSQLRequirement.updateName(idR, newName, 0, ReqPlugin.getProjectRef().getIdBdd());
            } catch (Exception e){
                e.printStackTrace();
            }
            idP = 0;
        }
        int type = pReqWrapper.getType();
        if (reqInserted.get(idR) != null){
            /* Exigences deja inseree */
            return;
        }
        Requirement pReq;
        ReqFamily pParent = (ReqFamily) reqInserted.get(idP);
        if (pParent != null){ // Le parent est deja inseree
            if (type == 1) {
                //feuille
                pReq = new ReqLeaf(pReqWrapper, pParent);
                FiltreUserData.addOrigine(((ReqLeaf)pReq).getOrigineFromModel());
                FiltreUserData.addReference(((ReqLeaf)pReq).getReferenceFromModel());
                FiltreUserData.addVerification(((ReqLeaf)pReq).getVerifFromModel());
            } else {
                //famille
                pReq = new ReqFamily(pReqWrapper, pParent);
            }
            pParent.addRequirement(pReq);
            reqTree.addRequirementToReqNode(pReq, pParent, false);
            reqInserted.put(idR, pReq);
                        
                        
        } else { // Sinon, il faut l'inserer avant
            ReqWrapper pReqParentWrapper = reqProjet.get(idP);
            if (pReqParentWrapper == null){ //Il n'y a pas de parent dans la base de donnees !!!!!!
                /* Les donnees sont corompues 
                 * mais on insert quand meme a la racine
                 * throw new Exception ("[DataLoader->loadData] no parent in requirement tree");
                 */
                Util.log("[DataLoader->insertRequiment] Data are corrupted for requirments " + pReqWrapper);
                pReqWrapper.setParent_id(0);
                pReqWrapper.setName("Restore2_" + pReqWrapper.getName());
                                
            } else {
                insertRequimentFunc(reqTree, pReqParentWrapper , reqInserted, reqProjet );
                                
            }
            insertRequimentFunc(reqTree, pReqWrapper , reqInserted, reqProjet );
        }
    }
        
        
    static boolean insertRequimentIT(RequirementTree reqTree, ReqWrapper pReqWrapper,
				     Hashtable<Integer,Requirement> reqInserted, Hashtable<Integer, 
				     ReqWrapper> reqProjet ) throws Exception{
        int idP = pReqWrapper.getParent_id();
        int idR = pReqWrapper.getIdBDD();
        if (idP == idR){
            Util.log("[DataLoader->insertRequiment] Data are corrupted for requirments " + pReqWrapper);
            Util.log("[DataLoader->insertRequiment] restore the requiement " + pReqWrapper + " at root");
            String newName = "Restore1_" + pReqWrapper.getName();
            pReqWrapper.setName(newName);
            pReqWrapper.setParent_id(0);
            try {
                ISQLRequirement pSQLRequirement = SQLWrapper.getSQLRequirement();
                pSQLRequirement.updateParent(idR, 0);
                pSQLRequirement.updateName(idR, newName, 0, ReqPlugin.getProjectRef().getIdBdd());
            } catch (Exception e){
                e.printStackTrace();
            }
            idP = 0;
        }
        int type = pReqWrapper.getType();
        if (reqInserted.get(idR) != null){
            /* Exigences deja inseree */
            return true;
        }
        Requirement pReq;
        ReqFamily pParent = (ReqFamily) reqInserted.get(idP);
        if (pParent != null){ // Le parent est deja inseree
            if (type == 1) {
                //feuille
                pReq = new ReqLeaf(pReqWrapper, pParent);
            } else {
                //famille
                pReq = new ReqFamily(pReqWrapper, pParent);
            }
            pParent.addRequirement(pReq);
            reqTree.addRequirementToReqNode(pReq, pParent, false);
            reqInserted.put(idR, pReq);
            return true;
        } else { // Sinon, il faut l'inserer avant
            ReqWrapper pReqParentWrapper = reqProjet.get(idP);
            if (pReqParentWrapper == null){ //Il n'y a pas de parent dans la base de donnees !!!!!!
                /* Les donnees sont corompues 
                 * mais on insert quand meme a la racine
                 * throw new Exception ("[DataLoader->loadData] no parent in requirement tree");
                 */
                Util.log("[DataLoader->insertRequiment] Data are corrupted for requirments " + pReqWrapper);
                pReqWrapper.setParent_id(0);
                pReqWrapper.setName("Restore2_" + pReqWrapper.getName());
                return insertRequimentIT(reqTree, pReqWrapper , reqInserted, reqProjet );
            } else {
                return false;
                //insertRequiment(reqTree, pReqParentWrapper , reqInserted, reqProjet );
            }
        }
    }
        
}
