/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.data;

import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public class ReqLeaf extends Requirement {

    //FROM 1.2
    int priority;
    String version;

    //FROM 1.3
    int cat;
    int complexe;
    String origine;
    String reference;
    int state;
    String verif;

    //20100115 - D\ufffdbut modification Forge ORTF
    int validBy;
    int critical;
    int understanding;
    int owner;
    //20100115 - Fin modification Forge ORTF

    public ReqLeaf(String name, String description, ReqFamily parent, int p){
        super(name, description, parent);
        //FROM 1.2
        priority = p;
        version ="1"; //Default
        //FROM 1.3
        cat = 0;
        complexe = 100;
        origine = "Marketing";
        state = 0;
        verif = "";
        reference ="";
        //20100115 - D\ufffdbut modification Forge ORTF
        validBy=1;
        critical=1;
        understanding=1;
        owner=1;
        //20100115 - Fin modification Forge ORTF
    }

    public ReqLeaf(ReqWrapper pReqWrapper, ReqFamily parent){
        super(pReqWrapper.getName(), pReqWrapper.getDescription(), parent);
        idBdd = pReqWrapper.getIdBDD();
        //FROM 1.2
        priority = pReqWrapper.getPriority();
        version = pReqWrapper.getVersion();
        //FROM 1.3
        cat = pReqWrapper.getCat();
        complexe = pReqWrapper.getComplexe();
        origine = pReqWrapper.getOrigine();
        state = pReqWrapper.getState();
        verif = pReqWrapper.getVerif();
        reference = pReqWrapper.getReference();
        //LoadAttachement
        //20100115 - D\ufffdbut modification Forge ORTF
        validBy = pReqWrapper.getValidBy();
        critical = pReqWrapper.getCritical();
        understanding = pReqWrapper.getUnderstanding();
        owner = pReqWrapper.getOwner();
        //20100115 - Fin modification Forge ORTF
    }

    public Requirement getCopie(ReqFamily pReqFamily){
        ReqLeaf pReq = new ReqLeaf(name, description, pReqFamily, priority);
        //FROM 1.2
        if (version != null){
            pReq.version = version;
        }
        //FROM 1.3
        pReq.cat = cat;
        pReq.complexe = complexe;
        if (origine != null){
            pReq.origine = origine;
        }
        pReq.state = state;
        if (verif != null){
            pReq.verif = verif;
        }
        if (reference != null){
            pReq.reference =reference;
        }
        //LoadAttachement
        //20100115 - D\ufffdbut modification Forge ORTF
        pReq.validBy = validBy;
        pReq.critical = critical;
        pReq.understanding = understanding;
        pReq.owner = owner;
        //20100115 - Fin modification Forge ORTF
        return pReq;
    }

    public boolean  isFamilyReq(){
        return false;
    }

    public int getPriorityFromModel(){
        return priority;
    }


    public String getVersionFromModel() {
        return version;
    }

    public String getReferenceFromModel(){
        return reference;
    }

    public int getCatFromModel() {
        return cat;
    }

    public int getComplexeFromModel() {
        return complexe;
    }

    public String getOrigineFromModel() {
        return origine;
    }

    public int getStateFromModel() {
        return state;
    }

    public String getVerifFromModel() {
        return verif;
    }

    //20100115 - D\ufffdbut modification Forge ORTF
    public int getValidByFromModel() {
        return validBy;
    }

    public int getCriticalFromModel() {
        return critical;
    }

    public int getOwnerFromModel() {
        return owner;
    }

    public int getUnderstandingFromModel() {
        return understanding;
    }
    //20100115 - Fin modification Forge ORTF

    public String toString(){
        return "Req : " + name;
    }

    public String getLongName(){
        if (m_parent !=null){
            //return m_parent.getLongName() + "->" + name;
            String parentName  = m_parent.getLongName();
            if (parentName != ""){
                return parentName + "->" + name;
            } else {
                return name;
            }
        } else {
            return name;
        }
        //return "Req : " + name;
    }

    void deleteInModel(){
        ((ReqFamily)m_parent).removeRequirment(this);
    }

    public void addInDB() throws Exception {
        if (isInBase()){
            throw new Exception ("[ReqLeaf->addInDB] requirement already in base");
        }
        if (!(m_parent instanceof ReqFamily)){
            throw new Exception ("[ReqLeaf->addInDB] requirement must have family in parent");
        }
        int idParent = m_parent.getIdBdd();
        ReqWrapper wrap = pSQLRequirement.add(name, description, 1, idParent, ReqPlugin.getProjectRef().getIdBdd());
        idBdd = wrap.getIdBDD();
        priority = wrap.getPriority();
        cat = wrap.getCat();
        complexe = wrap.getComplexe();
        state = wrap.getState();
        validBy = wrap.getValidBy();
        critical = wrap.getCritical();
        understanding = wrap.getUnderstanding();
        owner = wrap.getOwner();
        coverChange = true;
    }

    public void addTestCoverInDB(int idTest, boolean verifie)throws Exception{
        if (!isInBase()){
            throw new Exception ("[ReqLeaf->addTestCover] requirment is not DB");
        }
        if (verifie) {
            if (!pSQLRequirement.isReqReqCoveredByTest(idBdd, idTest)) {
                pSQLRequirement.addReqConvert(idBdd, idTest);
            }
        } else {
            pSQLRequirement.addReqConvert(idBdd, idTest);
        }
        coverChange = true;
    }


    public void updateVersionInModel(String _version){
        version = _version;
    }

    public void updateVersionInDB(String _version) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateVersionInDB] requirement is not in DB");
        }
        pSQLRequirement.updateVersion(idBdd, _version);
    }

    public void updateVersionInDBAndModel(String _version) throws Exception {
        if (_version.trim().equals(version.trim())){
            return;
        }
        updateVersionInDB(_version);
        updateVersionInModel(_version);
    }

    public void updateReferenceInModel(String _reference){
        reference = _reference;
        FiltreUserData.addReference(_reference);
    }

    public void updateReferenceInDB(String _reference) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateReferenceInDB] requirement is not in DB");
        }
        pSQLRequirement.updateReference(idBdd, _reference);
    }

    public void updateReferenceInDBAndModel(String _reference) throws Exception {
        if (_reference.trim().equals(reference.trim())){
            return;
        }
        updateReferenceInDB(_reference);
        updateReferenceInModel(_reference);
    }


    public void updateInfoInDB(String _version, String _origine, String _verif, String _reference) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateVersionInDB] requirement is not in DB");
        }
        pSQLRequirement.updateInfo(idBdd, _version, _origine, _verif, _reference);
    }

    public void updateInfoInDBAndModel(String _version, String _origine, String _verif, String _reference) throws Exception {
        updateInfoInDB(_version, _origine, _verif, _reference);
        updateVersionInModel(_version);
        updateOrigineInModel(_origine);
        updateVerifInModel(_verif);
        updateReferenceInModel(_reference);
    }

    public void updatePriorityInModel(int _priority){
        priority = _priority;
    }



    public void updatePriorityInDB(int _priority) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updatePriorityInDB] requirement is not in DB");
        }
        pSQLRequirement.updatePriority(idBdd, _priority);
    }

    public void updatePriorityInDBAndModel(int _priority) throws Exception {
        if (_priority == priority){
            return;
        }
        updatePriorityInDB(_priority);
        updatePriorityInModel(_priority);
    }

    public void updateCatInModel(int _cat) {
        this.cat = _cat;
    }

    public void updateCatInDB(int _cat)  throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateCatInDB] requirement is not in DB");
        }
        pSQLRequirement.updateCat(idBdd, _cat);
    }

    public void updateCatInDBAndModel(int _cat) throws Exception {
        if (_cat == cat){
            return;
        }
        updateCatInDB(_cat);
        updateCatInModel(_cat);
    }


    public void updateComplexeInModel(int _complexe) {
        this.complexe = _complexe;
    }

    public void updateComplexeInDB(int _complexe) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateComplexeInDB] requirement is not in DB");
        }
        pSQLRequirement.updateComplexe(idBdd, _complexe);
    }

    public void updateComplexeInDBAndModel(int _complexe) throws Exception{
        if (_complexe == complexe){
            return;
        }
        updateComplexeInDB(_complexe);
        updateComplexeInModel(_complexe);
    }


    public void updateOrigineInModel(String _origine) {
        this.origine = _origine;
        FiltreUserData.addOrigine(_origine);
    }

    public void updateOrigineInDB(String _origine) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateOrigineInDB] requirement is not in DB");
        }
        pSQLRequirement.updateOrigine(idBdd, _origine);
    }

    public void updateOrigineInDBAndModel(String _origine) throws Exception{
        if (_origine.trim().equals(origine.trim())){
            return;
        }
        updateOrigineInDB(_origine);
        updateOrigineInModel(_origine);
    }

    public void updateStateInModel(int _state) {
        this.state = _state;
    }

    public void updateStateInDB(int _state) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateStateInDB] requirement is not in DB");
        }
        pSQLRequirement.updateState(idBdd, _state);
    }

    public void updateStateInDBAndModel(int _state) throws Exception{
        if (_state == state){
            return;
        }
        updateStateInDB(_state);
        updateStateInModel(_state);
    }

    public void updateVerifInModel(String _verif) {
        this.verif = _verif;
        FiltreUserData.addVerification(_verif);
    }

    public void updateVerifInDB(String _verif) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateVerifInDB] requirement is not in DB");
        }
        pSQLRequirement.updateVerif(idBdd, _verif);
    }

    public void updateVerifInDBAndModel(String _verif) throws Exception{
        if (_verif.trim().equals(verif.trim())){
            return;
        }
        updateVerifInDB(_verif);
        updateVerifInModel(_verif);
    }

    //20100115 - D\ufffdbut modification Forge ORTF
    public void updateValidByInModel(int _validBy) {
        this.validBy = _validBy;
    }

    public void updateValidByInDB(int _validBy) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateValidByInDB] requirement is not in DB");
        }
        pSQLRequirement.updateValidBy(idBdd, _validBy);
    }

    public void updateValidByInDBAndModel(int _validBy) throws Exception{
        if (_validBy == validBy){
            return;
        }
        updateValidByInDB(_validBy);
        updateValidByInModel(_validBy);
    }

    public void updateCriticalInModel(int _critical) {
        this.critical = _critical;
    }

    public void updateCriticalInDB(int _critical) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateCriticalInDB] requirement is not in DB");
        }
        pSQLRequirement.updateCritical(idBdd, _critical);
    }

    public void updateCriticalInDBAndModel(int _critical) throws Exception{
        if (_critical == critical){
            return;
        }
        updateCriticalInDB(_critical);
        updateCriticalInModel(_critical);
    }

    public void updateUnderstandingInModel(int _understanding) {
        this.understanding = _understanding;
    }

    public void updateUnderstandingInDB(int _understanding) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateUnderstandingInDB] requirement is not in DB");
        }
        pSQLRequirement.updateUnderstanding(idBdd, _understanding);
    }

    public void updateUnderstandingInDBAndModel(int _understanding) throws Exception{
        if (_understanding == understanding){
            return;
        }
        updateUnderstandingInDB(_understanding);
        updateUnderstandingInModel(_understanding);
    }

    public void updateOwnerInModel(int _owner) {
        this.owner = _owner;
    }

    public void updateOwnerInDB(int _owner) throws Exception{
        if (!isInBase()){
            throw new Exception ("[Requirement->updateOwnerInDB] requirement is not in DB");
        }
        pSQLRequirement.updateOwner(idBdd, _owner);
    }

    public void updateOwnerInDBAndModel(int _owner) throws Exception{
        if (_owner == owner){
            return;
        }
        updateOwnerInDB(_owner);
        updateOwnerInModel(_owner);
    }
    //20100115 - Fin modification Forge ORTF
}
