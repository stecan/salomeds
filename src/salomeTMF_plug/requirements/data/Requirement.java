/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package salomeTMF_plug.requirements.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

import salomeTMF_plug.requirements.ReqPlugin;
import salomeTMF_plug.requirements.sqlWrapper.HistoryWrapper;
import salomeTMF_plug.requirements.sqlWrapper.ISQLRequirement;
import salomeTMF_plug.requirements.sqlWrapper.ReqWrapper;
import salomeTMF_plug.requirements.sqlWrapper.SQLWrapper;

/**
 * @covers SFD_ForgeORTF_EXG_CRE_000010
 * @covers SFD_ForgeORTF_EXG_CRE_000020 
 * @covers SFD_ForgeORTF_EXG_CRE_000030
 * @covers SFD_ForgeORTF_EXG_CRE_000040
 * @covers SFD_ForgeORTF_EXG_CRE_000050
 * @covers SFD_ForgeORTF_EXG_CRE_000060
 * @covers SFD_ForgeORTF_EXG_CRE_000070
 * @covers SFD_ForgeORTF_EXG_CRE_000080
 * @covers SFD_ForgeORTF_EXG_CRE_000090
 * @covers SFD_ForgeORTF_EXG_CRE_000110
 * Jira : FORTF - 38
 */
public abstract  class Requirement extends WithAttachment{
        
    protected Requirement m_parent;
    static ISQLRequirement pSQLRequirement;
        
    public static boolean coverChange = true;
        
    public Requirement(String name, String description, Requirement parent){
        super(name, description);
        m_parent = parent;
        if (pSQLRequirement == null){
            pSQLRequirement = SQLWrapper.getSQLRequirement();
        }
    }
        
    /*public Requirement(String name, String description){
      super(name, description, null);
      }*/
        
    public Requirement getParent() {
        return m_parent;
    }
        
    public void setParent(Requirement parent) {
        m_parent = parent;
    }
        
    public abstract boolean  isFamilyReq();
        
    public abstract String getLongName() ;
        
    /******************** Basic Operation *************************/
    abstract public Requirement getCopie(ReqFamily pReqFamily); 
        
    abstract public void addInDB() throws Exception;
        
    abstract public void addTestCoverInDB(int idTest, boolean verifie)throws Exception;
        
        
    public void updateDescriptionInModel(String _description){
        description = _description;
    }   
        
    public void updateDescriptionInDB(String _description) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateDescriptionInDB] requirement is not in DB");
        }
        pSQLRequirement.updateDescription(idBdd, _description);
    }   
        
    public void updateDescriptionInDBAndModel(String _description) throws Exception {
        if (_description.trim().equals(description.trim())){
            return;
        }
        updateDescriptionInDB(_description);
        updateDescriptionInModel(_description);
    }
        
    public void updateNameInModel(String _name){
        name = _name;
    }   
        
    public void updateNameInDB(String _name) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateDescriptionInDB] requirement is not in DB");
        }
        pSQLRequirement.updateName(idBdd, _name, m_parent.getIdBdd(), ReqPlugin.getProjectRef().getIdBdd());
    }   
        
    public void updateNameInDBAndModel(String _name) throws Exception {
        if (_name.trim().equals(name.trim())){
            return;
        }
        updateNameInDB(_name);
        updateNameInModel(_name);
    }   
        
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateNameInDBAndModel(newName);
        updateDescriptionInDBAndModel(newDesc);
    }
        
    public void updateParentInDB(ReqFamily _parent) throws Exception {
        if (!isInBase()){
            throw new Exception ("[Requirement->updateParentInDB] requirement is not in DB");
        }
        int idParent = _parent.getIdBdd();
                
        String tmpName = name;
        boolean trouve = false;
        int j = 0;
        while (!trouve){
            ReqWrapper pReqWrapper = pSQLRequirement.getReq(getNameFromModel(), _parent.getIdBdd(), ReqPlugin.getProjectRef().getIdBdd());
            if (pReqWrapper == null){
                trouve = true;
            } else {
                updateNameInModel("copy_" + j + "_" +    tmpName);
                j++;
            }
        }
        if ( j>0 ){
            updateNameInDB(name);
        }
        pSQLRequirement.updateParent(idBdd, idParent);
    }
        
    public void updateParentInModel(ReqFamily _parent) {
        ((ReqFamily)m_parent).removeRequirment(this);
        _parent.addRequirement(this);
        m_parent = _parent;
    }
        
    public void updateParentInDBAndModel(ReqFamily _parent) throws Exception {
                
        updateParentInDB(_parent);
        updateParentInModel(_parent);
    }   
        
    public void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
    void deleteInDB() throws Exception {
        pSQLRequirement.deleteReq(idBdd);
        coverChange = true;
    }
        
    abstract void deleteInModel();
        
    public void deleteCoverForTest(int idTest) throws Exception{
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        }
        pSQLRequirement.deleteCover(idBdd, idTest);
        coverChange = true;
    }
        
    static public void deleteAllCoverForTest(int idTest) throws Exception{
        pSQLRequirement.deleteAllTestCover(idTest);
        coverChange = true;
    }
        
    static public void copyReqCover(int idTestSource, int idTestDestination) throws Exception{
        Vector<ReqWrapper> tmpVector =  getReqWrapperCoveredByTest(idTestSource);
        int size = tmpVector.size();
        for(int i = 0; i < size; i++) {
            ReqWrapper pReqWrapper = tmpVector.elementAt(i);
            pSQLRequirement.addReqConvert(pReqWrapper.getIdBDD(), idTestDestination);
        }
        coverChange = true;
    }
        
    public Vector<TestWrapper> getTestWrapperCoveredFromDB() throws Exception{
        Vector<TestWrapper> tmpVector = new Vector<TestWrapper>();
        TestWrapper[] tmpArray = pSQLRequirement.getTestCoveredForReq(idBdd);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<TestWrapper> getTestWrapperCoveredFromDB(int idBdd) throws Exception{
        Vector<TestWrapper> tmpVector = new Vector<TestWrapper>();
        TestWrapper[] tmpArray = pSQLRequirement.getTestCoveredForReq(idBdd);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperCoveredByTest(int idTest) throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getReqCoveredByTest(idTest);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperCoveredByCamp(int idCamp) throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getReqWrapperCoveredByCamp(idCamp);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperCovered() throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        //ReqWrapper[] tmpArray = pSQLRequirement.getCoveredReq(ReqPlugin.getProjectRef().getIdBdd());
        ReqWrapper[] tmpArray = pSQLRequirement.getCoveredReq( DataModel.getCurrentProject().getIdBdd());
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperInCurrentProject() throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getProjectRequirements(ReqPlugin.getProjectRef().getIdBdd());
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperLeafInCurrentProject() throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getProjectRequirementByType(ReqPlugin.getProjectRef().getIdBdd(), 1);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperFamilyInCurrentProject() throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getProjectRequirementByType(ReqPlugin.getProjectRef().getIdBdd(), 0);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static Vector<ReqWrapper> getReqWrapperInExecByStatus(int ideExec, String status) throws Exception{
        Vector<ReqWrapper> tmpVector = new Vector<ReqWrapper>();
        ReqWrapper[] tmpArray = pSQLRequirement.getReqCoveredByResExecAndStatus(ideExec, status );
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
    /******************** Attachements ****************************/
        
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        } 
        //SalomeFileWrapper f = file.getLocalFile();
        File f = file.getLocalFile();
        SalomeFileWrapper pFileWra = new SalomeFileWrapper(f);
        int id = pSQLRequirement.addAttachFile(idBdd, pFileWra, file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        }
        int id = pSQLRequirement.addAttachUrl(idBdd, url.getNameFromModel(),url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
        
    public void addAttachInDBAndModel(Attachment attach)        throws Exception {
        if (attach instanceof FileAttachment){
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
        addAttachementInModel(attach);
    }
        
        
    public void deleteAttachementInDBAndModel(Attachment attach)throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
                
        deleteAttachmentInModel(attach);
    }
        
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        }
        pSQLRequirement.deleteAttach(idBdd, attachId);
    }
        
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        }
        Vector tmpVector = new Vector();
        FileAttachementWrapper[] tmpArray = pSQLRequirement.getAllAttachFiles(idBdd);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Requirement " + name + " is not in BDD");
        }
        Vector tmpVector = new Vector();
        UrlAttachementWrapper[] tmpArray = pSQLRequirement.getAllAttachUrls(idBdd);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    /**************************************************************************************/
        
    static public void writeCampaingChart(Campaign pCamp, String filePath){
        try {
            Vector reqCoveredWrapper;
            Vector reqCovered = new Vector();
            ArrayList pTestList = pCamp.getTestListFromModel();
            int nbTest = pTestList.size();
            for (int i = 0; i < nbTest; i++){
                Test pTest = (Test) pTestList.get(i);
                reqCoveredWrapper = Requirement.getReqWrapperCoveredByTest(pTest.getIdBdd());
                int size = reqCoveredWrapper.size();
                for (int j = 0 ; j < size ; j++) {
                    ReqWrapper pReqWrapper = (ReqWrapper)reqCoveredWrapper.elementAt(j);
                    if (!reqCovered.contains(pReqWrapper)){
                        reqCovered.add(pReqWrapper);
                    }
                }
            }
            writeCampaingChart(reqCovered, filePath);
                        
        } catch (Exception e){
                                
        }
    }
        
    static public void writeCampaingChart(Vector reqCovered,  String filePath){         
        try {
            int nbReqCovered = reqCovered.size();
            int nbReqTotal = 0;
                        
            try {
                nbReqTotal = Requirement.getReqWrapperLeafInCurrentProject().size();
            } catch (Exception e){
                nbReqTotal = 0;
            }
                        
            DefaultPieDataset dataset = new  DefaultPieDataset();
            if (nbReqTotal > 0) {
                Double nbCovered = new Double( (nbReqCovered*100)/nbReqTotal);
                Double nbNotCovered = new Double(100 -  (nbReqCovered*100)/nbReqTotal);
                dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), nbNotCovered);
                dataset.setValue(Language.getInstance().getText("Exigence_couverte"),nbCovered);
            } 
                        
            JFreeChart chart = ChartFactory.createPieChart3D(
                                                             Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                             dataset,                // data
                                                             true,                   // include legend
                                                             true,
                                                             false
                                                             );
                        
            ChartUtilities.saveChartAsJPEG(new java.io.File(filePath),chart,500,300);
                        
        } catch (Exception e){
                        
        }
                
                
    }
        
    static public void writeReqCoverChart(String filePath){             
        try {
            int nbTotalReq = Requirement.getReqWrapperLeafInCurrentProject().size();
            int nbCoveredReq =  Requirement.getReqWrapperCovered().size(); 
            DefaultPieDataset dataset = new  DefaultPieDataset();
            if (nbTotalReq > 0) {
                Double nbCovered = new Double( (nbCoveredReq*100)/nbTotalReq);
                Double nbNotCovered = new Double(100 -  (nbCoveredReq*100)/nbTotalReq);
                dataset.setValue(Language.getInstance().getText("Exigence_non_couverte"), nbNotCovered);
                dataset.setValue(Language.getInstance().getText("Exigence_couverte"),nbCovered);
                                
            } 
            JFreeChart chart = ChartFactory.createPieChart3D(
                                                             Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                             dataset,                // data
                                                             true,                   // include legend
                                                             true,
                                                             false
                                                             );
                        
            ChartUtilities.saveChartAsJPEG(new java.io.File(filePath),chart,500,300);
        } catch (Exception e){
                        
        }
    }
        
    static public void writeResExecChart(ExecutionResult executionResult,  String filePath){            
        int nbFail = 0;
        int nbPass = 0;
        int nbInco = 0;
        int nbNotApplicable = 0;
        int nbBlocked = 0;
        int nbNone = 0;
        try {
            Vector reqPass = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.SUCCESS);
            Vector reqFail = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.FAIL);
            Vector reqInco = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.UNKNOWN);
            Vector reqNotApplicable = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NOTAPPLICABLE);
            Vector reqBlocked = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.BLOCKED);
            Vector reqNone = Requirement.getReqWrapperInExecByStatus(executionResult.getIdBdd(), ApiConstants.NONE);
            int sizePass = reqPass.size();
            int sizeFail = reqFail.size();
            int sizeInco = reqInco.size();
            int sizeNotApplicable = reqNotApplicable.size();
            int sizeBlocked = reqBlocked.size();
            int sizeNone = reqNone.size();
                
            Hashtable allReq = new Hashtable();
            for (int i = 0; i < sizePass; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqPass.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.SUCCESS);  
            }
            for (int i = 0; i < sizeInco; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqInco.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.UNKNOWN);
            }
            for (int i = 0; i < sizeFail; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqFail.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.FAIL);
            }
            for (int i = 0; i < sizeNotApplicable; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNotApplicable.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NOTAPPLICABLE);
            }
            for (int i = 0; i < sizeBlocked; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqBlocked.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.BLOCKED);
            }
            for (int i = 0; i < sizeNone; i++){
                ReqWrapper pReqWrapper = (ReqWrapper) reqNone.elementAt(i);
                allReq.put(pReqWrapper, ApiConstants.NONE);
            }
                
            Enumeration enumReq = allReq.keys();
            while (enumReq.hasMoreElements()){
                ReqWrapper pReqWrapper =  (ReqWrapper)enumReq.nextElement();
                String status = (String) allReq.get(pReqWrapper);
                if (status.equals(ApiConstants.SUCCESS)){
                    nbPass++;
                } else if (status.equals(ApiConstants.FAIL)){
                    nbFail++;
                } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                    nbNotApplicable++;
                } else if (status.equals(ApiConstants.BLOCKED)){
                    nbBlocked++;
                } else if (status.equals(ApiConstants.NONE)){
                    nbNone++;
                } else {
                    nbInco++; 
                }
            }
            int nbReqTotal = nbFail + nbPass + nbInco;
                
            DefaultPieDataset dataset = new  DefaultPieDataset();
            if (nbReqTotal > 0) {
                Double percentPass = new Double((nbPass*100)/nbReqTotal);
                Double percentFail = new Double((nbFail*100)/nbReqTotal);
                Double percentInco = new Double((nbInco*100)/nbReqTotal);
                Double percentNotApplicable = new Double((nbNotApplicable*100)/nbReqTotal);
                Double percentBlocked = new Double((nbBlocked*100)/nbReqTotal);
                Double percentNone = new Double((nbNone*100)/nbReqTotal);
                dataset.setValue(Language.getInstance().getText("Exigence_Fail"),percentFail);
                dataset.setValue(Language.getInstance().getText("Exigence_Inco"), percentInco);
                dataset.setValue(Language.getInstance().getText("Exigence_Pass"),percentPass);
                dataset.setValue(Language.getInstance().getText("Exigence_NotApplicable"),percentNotApplicable);
                dataset.setValue(Language.getInstance().getText("Exigence_Blocked"),percentBlocked);
                dataset.setValue(Language.getInstance().getText("Exigence_None"),percentNone);
            } 
            JFreeChart chart = ChartFactory.createPieChart3D(
                                                             Language.getInstance().getText("Exigence_couverte"),  // chart title
                                                             dataset,                // data
                                                             true,                   // include legend
                                                             true,
                                                             false
                                                             );
                        
            ChartUtilities.saveChartAsJPEG(new java.io.File(filePath),chart,500,300);
        } catch (Exception e){
                        
        }
    }
        
        
    public int hashCode() {
        if (isInBase()){
            return idBdd;
        } else {
            return super.hashCode();
        }
    }
        
    public boolean equals(Object o) {
        if (o instanceof Requirement){
            Requirement toTest = (Requirement) o;
            if (isInBase() &&  toTest.isInBase() ){
                return idBdd == toTest.getIdBdd();
            }else {
                return getNameFromModel() == toTest.getNameFromModel();
            }
        } else {
            return false;
        }
    }
        
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pSQLRequirement.getReqById(idBdd) != null;
    }

    /*********************************** History ****************************************/
    public HistoryWrapper[] getHistory() throws Exception{
        if (!isInBase()) {
            return null;
        }
        return pSQLRequirement.getHistory(idBdd);
    }
        
    static public String getStringCodeValue(int code){
        String res= "";
        if (code == ISQLRequirement.HIST_CREATE_REQ){
            res = "Creation";
        } else if (code == ISQLRequirement.HIST_ADD_REQLEAF){
            res = "Add Requiement";
        } else if (code == ISQLRequirement.HIST_ADD_REQFAM){
            res = "Add Requiement family";
        } else if (code == ISQLRequirement.HIST_ADD_ATTACH){
            res = "Add Requiement Attachement";
        } else if (code == ISQLRequirement.HIST_ADD_COVER){
            res = "Add Requiement Test Cover";
        } else if (code == ISQLRequirement.HIST_ADD_LINK){
            res = "Add Requiement Link";
        } else if (code == ISQLRequirement.HIST_DEL_REQ){
            res = "Delete Requiement";
        } else if (code == ISQLRequirement.HIST_DEL_ATTACH){
            res = "Delete Attachement";
        } else if (code == ISQLRequirement.HIST_DEL_COVER){
            res = "Delete Test Cover";
        } else if (code == ISQLRequirement.HIST_DEL_LINK){
            res = "Delete Requiement Link";
        }  else if (code == ISQLRequirement.HIST_UPDATE_NAME){
            res = "Update Name";
        }  else if (code == ISQLRequirement.HIST_UPDATE_DESCRIPTION){
            res = "Update Description";
        }  else if (code == ISQLRequirement.HIST_UPDATE_VERSION){
            res = "Update Version";
        }  else if (code == ISQLRequirement.HIST_UPDATE_REFERENCE){
            res = "Update Reference";
        }  else if (code == ISQLRequirement.HIST_UPDATE_INFO){
            res = "Update Information";
        }  else if (code == ISQLRequirement.HIST_UPDATE_PRIORITY){
            res = "Update Priority";
        }  else if (code == ISQLRequirement.HIST_UPDATE_CATEGORY){
            res = "Update Category";
        }  else if (code == ISQLRequirement.HIST_UPDATE_COMPLEXITY){
            res = "Update Complexity";
        }  else if (code == ISQLRequirement.HIST_UPDATE_ORIGINE){
            res = "Update Origine";
        }  else if (code == ISQLRequirement.HIST_UPDATE_SATE){
            res = "Update State";
        }  else if (code == ISQLRequirement.HIST_UPDATE_VERIFWAY){
            res = "Update Verification Way";
        }  else if (code == ISQLRequirement.HIST_UPDATE_PARENT){
            res = "Update Requirement Parent";
        }  else if (code == ISQLRequirement.HIST_UPDATE_VALID_BY){
            res = "Update Valid by";
        }  else if (code == ISQLRequirement.HIST_UPDATE_CRITICAL){
            res = "Update Critical";
        }  else if (code == ISQLRequirement.HIST_UPDATE_UNDERSTANDING){
            res = "Update Understanding";
        }  else if (code == ISQLRequirement.HIST_UPDATE_OWNER){
            res = "Update Owner";
        }
        return res;     
    }
        
    static public boolean isTestIdValeur(int code){
        if (code == ISQLRequirement.HIST_DEL_COVER){
            return true;
        } else if (code == ISQLRequirement.HIST_ADD_COVER){
            return true;
        }
                        
        return false;
    }
        
    static public boolean isStateValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_SATE){
            return true;
        } 
        return false;
    }
        
    static public boolean isPriorityValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_PRIORITY){
            return true;
        } 
        return false;
    }
        
    static public boolean isComplexityValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_COMPLEXITY){
            return true;
        } 
        return false;
    }
        
    static public boolean isCategoryValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_CATEGORY){
            return true;
        } 
        return false;
    }
        
    static public boolean isValidByValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_VALID_BY){
            return true;
        } 
        return false;
    }
        
    static public boolean isCriticalValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_CRITICAL){
            return true;
        } 
        return false;
    }
        
    static public boolean isUnderstandingValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_UNDERSTANDING){
            return true;
        } 
        return false;
    }
        
    static public boolean isOwnerValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_OWNER){
            return true;
        } 
        return false;
    }
        
    static public boolean isDescriptionValeur(int code){
        if (code == ISQLRequirement.HIST_UPDATE_DESCRIPTION){
            return true;
        } 
        return false;
    }
}
