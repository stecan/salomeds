import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class SalomeTMF_Update {
    URL urlSalome;
    //http://download.forge.objectweb.org/salome-tmf/salome_tmf_api.jar
        
    String salomeDir = System.getProperties().getProperty("user.dir");
    String fs = System.getProperties().getProperty("file.separator");
    Properties pluginProperties = new Properties();
    String propertiesFile = "CfgPlugins.properties";
    String avaiblePlugin;
    //String commons_jars = "commons-logging.jar, jfreechart-1.0.1.jar, log4j-1.2.6.jar, driver.jar, jcommon-1.0.0.jar, jpf-0.3.jar";
    String jar_salome = "salome_tmf_data.jar,salome_tmf_api.jar,salome_tmf_ihm.jar,salome_tmf_sqlengine.jar,salome_tmf_coreplug.jar,salome_tmf_login.jar,salome_tmf_tools.jar,salome_tmf_codex.jar";

    
    SalomeTMF_Update(){
        
    }
    void doUpdate() throws Exception {
        urlSalome = new URL("http://download.forge.objectweb.org/salome-tmf/");
        System.out.println("Do update with : ");
        System.out.println("\tRemote dir : " + urlSalome.toString());
        System.out.println("\tLocal dir : " + salomeDir);
        
        System.out.println("\n--------------------\n");
        System.out.println("Update Core Jar");
        updateCoreJar();
        
        System.out.println("\n--------------------\n");
        System.out.println("Update Plug-in Jar");
        loadProperties();
        updatePluginJar();
    }
    
    void usage(){
        System.out.println("java -jar SalomeTMF_Update.jar [proxy] [proxy_port] -help");
        System.out.println("\t proxy_port default = 8080");
    }
    
    void loadProperties() throws Exception {
        pluginProperties = new Properties(); 
        FileInputStream ins = new FileInputStream(salomeDir + fs + "plugins" + fs +propertiesFile);
        pluginProperties.load(ins);
        ins.close();
        avaiblePlugin = pluginProperties.getProperty("pluginsList", "core");
    }
    
    void updateCoreJar() throws Exception {
        updateJar(jar_salome, "");
    }
    
    void updatePluginJar() throws Exception {
        StringTokenizer st = new StringTokenizer(avaiblePlugin, ",");
        while (st.hasMoreTokens()){
            String pluginName = st.nextToken().trim();
            if (!pluginName.equals("core")){
                System.out.println("Update Plug-in : " + pluginName);
                String remoteDir = "plugins" + fs + pluginName + fs;
                updateJar(pluginName + ".jar", remoteDir);
                System.out.println();
            }
            /*if (pluginName.equals("abbotScriptRunner")){
              updateJar(pluginName + ".jar", remoteDir);
              } else if (pluginName.equals("cronExec")){
                        
              } else if (pluginName.equals("beanshell")){
                        
              } else if (pluginName.equals("bugzilla")){
                        
              } else if (pluginName.equals("mantis")){
                        
              } else if (pluginName.equals("helpgui")){
                        
              } else if (pluginName.equals("gen_doc_xml")){
                        
              } else if (pluginName.equals("simpleJunit")){
                        
              } else if (pluginName.equals("lookAndfeel")){
                        
              } else if (pluginName.equals("requirements")){
                        
              } */
        }
    }
                                         
                 
                           
                              

    
    /**
     * 
     * @param jarLists 
     * @param remoteDir ex : plugin/simpleJunit
     * @throws Exception
     */
    void updateJar(String jarLists, String remoteDir) throws Exception {
        String[] jar_list = jarLists.split(",");
           
        for (int i=0; i< jar_list.length; i++){
            String url =  urlSalome.toString()  + jar_list[i].trim();
                 
            String urlRemote =  "jar:" +   urlSalome.toString() ;
            urlRemote += jar_list[i].trim() + "!/";
            try {
                File fileLocal = new File(salomeDir + fs + remoteDir + jar_list[i].trim());
                 
                if(needUpdate(urlRemote, fileLocal)){
                    installFile(new URL(url), fileLocal);
                }
            } catch (Exception e){
                System.out.println("Error when update " + jar_list[i].trim() );
            }
        }
    }
     
    
    boolean needUpdate(String remoteUrl, File localFile){
        boolean ret = false;
        try {
            URL jar = new URL(remoteUrl);
            JarURLConnection jarConnection = (JarURLConnection)jar.openConnection();
            Manifest remoteManifest = jarConnection.getManifest();
                   
            JarFile localeJar = new JarFile(localFile);
            Manifest localManifest = localeJar.getManifest();
                   
            if (!remoteManifest.equals(localManifest)){
                System.out.println("Need to Update : " + localFile);
                ret = true;
            } else {
                System.out.println("No Update for : " + localFile);
            }
        }catch (Exception e){
            e.printStackTrace();
            ret = true;
        }
        return ret;
    }
    
    
    void installFile(URL pUrl, File localFile) throws Exception{
        try {
            URLConnection urlconn = pUrl.openConnection();
            InputStream is = urlconn.getInputStream();
            System.out.println("\tInstall Update " + localFile);
            writeStream(is, localFile);
        }catch (Exception  e){
            e.printStackTrace();
        }
    }
    
    void writeStream( InputStream is , File f) throws Exception{
        byte[] buf = new byte [102400];
        f.createNewFile();
        FileOutputStream fos = new FileOutputStream (f); 
        int len = 0;
        while ((len = is.read (buf)) != -1){
            fos.write (buf, 0, len);
        }
        fos.close();
        is.close();
    }
    
    public static void main(String[] args){
        SalomeTMF_Update pSalomeTMF_Update = new SalomeTMF_Update();
        String proxy="";
        String proxy_port = "8080";
        if (args.length > 0){
            for (int i = 0 ; i < args.length; i++){
                if (args[i].equals("-help")){
                    pSalomeTMF_Update.usage();
                    System.exit(0);
                } else {
                    if (i<1){
                        proxy = args[i];
                    } else {
                        proxy_port = args[i];
                    }
                }
            }
            //Apply proxy
            System.out.println("SalomeTMF Update Using proxy : " + proxy + " on port " + proxy_port);
            System.setProperty("http.proxyHost", proxy);
            System.setProperty("http.proxyPort", proxy_port);
        }
        try {
            pSalomeTMF_Update.doUpdate();
        } catch(Exception e){
            e.printStackTrace();
            pSalomeTMF_Update.usage();
        }
    }
}
