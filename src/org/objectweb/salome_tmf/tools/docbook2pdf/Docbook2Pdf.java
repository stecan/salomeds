package org.objectweb.salome_tmf.tools.docbook2pdf;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Locale;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FormattingResults;
import org.apache.fop.apps.MimeConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.docbook2pdf.languages.Language;
import org.objectweb.salome_tmf.tools.docbook2pdf.utils.ScriptFileFilter;

public class Docbook2Pdf extends JFrame {
        
    // configure fopFactory as desired
    private FopFactory fopFactory = FopFactory.newInstance();
    private final static String xsltFilePath = "resources/docbook-xsl-1.73.2/fo/docbook2Pdf.xsl";
    private final static String languageCfgPath = "cfg/language.properties";
        
    public JTextField docbookTF;
    public JTextField pdfTF;
    public JProgressBar progress;
        
    public Thread transformProcess;
    public boolean cancelled = false;
        
    private Locale locale;
                        
    public Docbook2Pdf() {
        super("Docbook2Pdf Tool");
                
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(languageCfgPath));
        } catch (Exception e1) {
            locale = new Locale(properties.getProperty("en"));
        }
                
        locale = new Locale(properties.getProperty("language"));
                
        JLabel docbookLabel = new JLabel(Language.getInstance(locale).getText("Fichier_Docbook_"));
        docbookTF = new JTextField(30);
        JButton docbookButton = new JButton(Language.getInstance(locale).getText("Choisir"));
        docbookButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser fileChooser = new JFileChooser();
                    String docbookFilePath = "";
                    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance(locale).getText("Fichier_Docbook____xml_"), ".xml"));
                    if (docbookTF.getText().length() != 0) {
                        fileChooser.setSelectedFile(new File(docbookTF.getText()));
                    } else if (pdfTF.getText().length()!=0) {
                        String pdfPath = pdfTF.getText();
                        Util.debug(pdfPath.substring(0, pdfPath.lastIndexOf(File.separator)+1));
                        fileChooser.setSelectedFile(new File(pdfPath.substring(0, pdfPath.lastIndexOf(File.separator)+1)+"docbook.xml"));                                       
                    }
                    int returnVal = fileChooser.showDialog(Docbook2Pdf.this, Language.getInstance(locale).getText("Selectionner"));
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        docbookFilePath = fileChooser.getSelectedFile().getAbsolutePath();
                        if (docbookFilePath.indexOf(".") != -1) {
                            if (!docbookFilePath.substring(docbookFilePath.lastIndexOf(".")).equals(".xml")) {
                                docbookFilePath += ".xml";
                            }
                        } else {
                            docbookFilePath += ".xml";
                        }
                        docbookTF.setText(docbookFilePath);
                    }
                }
            });
                
        JPanel docbookLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        docbookLabelPanel.add(docbookLabel);
        JPanel docbookTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        docbookTFPanel.add(docbookTF);
        JPanel docbookButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        docbookButtonPanel.add(docbookButton);
                
        JPanel docbookPanel = new JPanel();
        docbookPanel.setLayout(new BoxLayout(docbookPanel, BoxLayout.Y_AXIS));
        docbookPanel.setBorder(new TitledBorder(""));
        docbookPanel.add(docbookLabelPanel);
        docbookPanel.add(docbookTFPanel);
        docbookPanel.add(docbookButtonPanel);
                                
        JLabel pdfLabel = new JLabel(Language.getInstance(locale).getText("Fichier_PDF_"));
        pdfTF = new JTextField(30);
        JButton pdfButton = new JButton(Language.getInstance(locale).getText("Choisir"));
        pdfButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JFileChooser fileChooser = new JFileChooser();
                    String pdfFilePath = "";
                    fileChooser.addChoosableFileFilter(new ScriptFileFilter(Language.getInstance(locale).getText("Fichier_PDF____pdf_"), ".pdf"));
                    if (pdfTF.getText().length() != 0) {
                        fileChooser.setSelectedFile(new File(pdfTF.getText()));
                    } else if (docbookTF.getText().length()!=0) {
                        String docbookPath = docbookTF.getText();
                        fileChooser.setSelectedFile(new File(docbookPath.substring(0, docbookPath.lastIndexOf(File.separator))+File.separator+"docbook.pdf"));                                  
                    } else {
                        fileChooser.setSelectedFile(new File("docbook.pdf"));
                    }
                    int returnVal = fileChooser.showDialog(Docbook2Pdf.this, Language.getInstance(locale).getText("Selectionner"));
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        pdfFilePath = fileChooser.getSelectedFile().getAbsolutePath();
                        if (pdfFilePath.indexOf(".") != -1) {
                            if (!pdfFilePath.substring(pdfFilePath.lastIndexOf(".")).equals(".pdf")) {
                                pdfFilePath += ".pdf";
                            }
                        } else {
                            pdfFilePath += ".pdf";
                        }
                        pdfTF.setText(pdfFilePath);
                    }
                }
            });
                
        JPanel pdfLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pdfLabelPanel.add(pdfLabel);
        JPanel pdfTFPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pdfTFPanel.add(pdfTF);
        JPanel pdfButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pdfButtonPanel.add(pdfButton);
                
        JPanel pdfPanel = new JPanel();
        pdfPanel.setLayout(new BoxLayout(pdfPanel, BoxLayout.Y_AXIS));
        pdfPanel.setBorder(new TitledBorder(""));
        pdfPanel.add(pdfLabelPanel);
        pdfPanel.add(pdfTFPanel);
        pdfPanel.add(pdfButtonPanel);
                
        JPanel filesPanel = new JPanel();
        filesPanel.setLayout(new BoxLayout(filesPanel, BoxLayout.Y_AXIS));
        filesPanel.add(docbookPanel);
        filesPanel.add(pdfPanel);
                
        JButton transformButton = new JButton(Language.getInstance(locale).getText("Transformer"));
        transformButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (transformProcess != null && transformProcess.isAlive()){
                        return;
                    } else {
                        transformProcess = new Thread(new TransformProcess());
                        transformProcess.start();
                    }                           
                }
            });
        JButton cancelButton = new JButton(Language.getInstance(locale).getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cancelled = true;
                    Docbook2Pdf.this.dispose();
                }
            });
                
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(transformButton);
        buttonsPanel.add(cancelButton);

        progress = new JProgressBar();
        JPanel progression = new JPanel(new FlowLayout(FlowLayout.CENTER));
        progression.add(progress);
            
        JPanel commonPanel = new JPanel();
        commonPanel.setLayout(new BoxLayout(commonPanel, BoxLayout.Y_AXIS));
        commonPanel.add(buttonsPanel);
        commonPanel.add(Box.createRigidArea(new Dimension(1, 5)));
        commonPanel.add(progression);
             
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(filesPanel, BorderLayout.CENTER);
        contentPaneFrame.add(commonPanel, BorderLayout.SOUTH);
            
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    if (transformProcess != null && transformProcess.isAlive()){
                        return;
                    } else {
                        dispose();
                    }
                }
            });
            
        getRootPane().setDefaultButton(transformButton);
        this.setLocation(400, 100);
        this.pack();
        this.setVisible(true);
    }
        
    public class TransformProcess implements Runnable {

        @Override
        public void run() {
            String docbookFilePath = docbookTF.getText();
            String pdfFilePath = pdfTF.getText();
            if (docbookFilePath.length() == 0) {
                JOptionPane.showMessageDialog(
                                              Docbook2Pdf.this,
                                              Language.getInstance(locale).getText("Vous_devez_entrer_un_nom_de_fichier_Docbook"),
                                              Language.getInstance(locale).getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            } else if (pdfFilePath.length() == 0) {
                JOptionPane.showMessageDialog(
                                              Docbook2Pdf.this,
                                              Language.getInstance(locale).getText("Vous_devez_entrer_un_nom_de_fichier_PDF"),
                                              Language.getInstance(locale).getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            } else {
                File docbookFile = new File(docbookFilePath);
                if (!docbookFile.exists()) {
                    JOptionPane.showMessageDialog(
                                                  Docbook2Pdf.this,
                                                  Language.getInstance(locale).getText("Vous_devez_entrer_le_nom_d_un_fichier_Docbook_existant"),
                                                  Language.getInstance(locale).getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                } else {
                    File pdfFile = new File(pdfFilePath);
                    try {
                        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        progress.setIndeterminate(true);
                        transform(docbookFile, pdfFile);
                        progress.setVisible(false);
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        if (!cancelled) {
                            JOptionPane.showMessageDialog( Docbook2Pdf.this,
                                                           Language.getInstance(locale).getText("Le_fichier_PDF_a_ete_genere_avec_succes"),
                                                           Language.getInstance(locale).getText("Information"),
                                                           JOptionPane.INFORMATION_MESSAGE);
                        }
                        Docbook2Pdf.this.dispose();
                    } catch (Exception e1) {
                        progress.setVisible(false);
                        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                        JOptionPane.showMessageDialog(
                                                      Docbook2Pdf.this,
                                                      Language.getInstance(locale).getText("Erreur_lors_de_la_creation_du_fichier_pdf_pour_plus_de_details_voir_la_console_java"),
                                                      Language.getInstance(locale).getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                        Util.err(e1);
                    }
                }
            }
        }
                
    }
                

    public void transform(File docbookFile,  File pdfFile) throws Exception {
        File temporaryFoFile = File.createTempFile("docbook", ".fo");
        temporaryFoFile.deleteOnExit();
        transformDocbook2Fo(docbookFile, temporaryFoFile);
        transformFo2Pdf(temporaryFoFile, pdfFile);
    }

        
    private void transformDocbook2Fo(File docbookFile, File temporaryFoFile) throws Exception {
        //to use Saxon XSLT processor
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");
                
        File xsltFile = new File(xsltFilePath);
        StreamSource styleSource = new StreamSource(xsltFile);
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(net.sf.saxon.FeatureKeys.DTD_VALIDATION, new Boolean(false));
        Transformer x = factory.newTransformer(styleSource);
        x.setOutputProperty(OutputKeys.INDENT, "yes");
        x.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        
        String docbookString = readFileAsString(docbookFile, "UTF-8");
        docbookString = docbookString.substring(0, docbookString.indexOf("<!DOCTYPE"))+docbookString.substring(docbookString.indexOf("<article"));
        Source source = new StreamSource(new StringReader(docbookString));
        FileOutputStream tempFoOutputStream = new FileOutputStream(temporaryFoFile);
        Result target = new StreamResult(tempFoOutputStream);
        x.transform(source, target);
        tempFoOutputStream.close();
    }
        
    private String readFileAsString(File file, String encoding) throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        return fileData.toString();
    }


    /**
     * Converts an FO file to a PDF file using FOP
     * @param fo the FO file
     * @param pdf the target PDF file
     * @throws IOException In case of an I/O problem
     * @throws FOPException In case of a FOP problem
     */
    private void transformFo2Pdf(File temporaryFoFile, File pdfFile) throws Exception {
        OutputStream out = null;

        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        // configure foUserAgent as desired

        // Setup output stream.  Note: Using BufferedOutputStream
        // for performance reasons (helpful with FileOutputStreams).
        out = new FileOutputStream(pdfFile);
        out = new BufferedOutputStream(out);

        // Construct fop with desired output format
        Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

        // Setup JAXP using identity transformer
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer(); // identity transformer

        // Setup input stream
        Source src = new StreamSource(temporaryFoFile);

        // Resulting SAX events (the generated FO) must be piped through to FOP
        Result res = new SAXResult(fop.getDefaultHandler());

        // Start XSLT transformation and FOP processing
        transformer.transform(src, res);

        // Result processing
        FormattingResults foResults = fop.getResults();
        Util.debug("Generated " + foResults.getPageCount() + " pages in total.");
        out.close();
    }
        
    public static void main(String [] argv) {
        new Docbook2Pdf();
    }
}
