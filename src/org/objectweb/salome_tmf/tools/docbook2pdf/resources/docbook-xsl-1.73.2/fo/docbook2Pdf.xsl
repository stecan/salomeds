<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="1.0">
  <xsl:import href="docbook.xsl"/>
  <xsl:param name="section.autolabel">1</xsl:param>
  <xsl:param name="fop.extensions">0</xsl:param>
  <xsl:param name="fop1.extensions">1</xsl:param>
  <xsl:param name="paper.type">A4</xsl:param>
  <xsl:param name="body.font.family">Helvetica</xsl:param>
  <xsl:param name="insert.xref.page.number">1</xsl:param>
  <xsl:param name="draft.mode">no</xsl:param>
  <xsl:param name="ulink.show">0</xsl:param>
  <xsl:param name="toc.section.depth">4</xsl:param>
  <xsl:param name="toc.indent.width">4</xsl:param>
  
  <xsl:attribute-set name="monospace.verbatim.properties" 
                   use-attribute-sets="verbatim.properties monospace.properties">
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="font-size">75%</xsl:attribute>
    <xsl:attribute name="hyphenation-character">&#x25BA;</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="section.title.properties">
    <xsl:attribute name="color">#00688B</xsl:attribute>
  </xsl:attribute-set>
                                                                                
  <xsl:attribute-set name="section.level1.properties">
    <xsl:attribute name="break-before">page</xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:attribute-set name="component.title.properties">
  	<xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="parent::article">#ffffff</xsl:when>
        <xsl:otherwise>black</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
   	<xsl:attribute name="background-color">
      <xsl:choose>
        <xsl:when test="parent::article">#ff9933</xsl:when>
        <xsl:otherwise>white</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:attribute-set>
  
  <xsl:template match="processing-instruction('lb')">
    <fo:block />
  </xsl:template>
    
</xsl:stylesheet>