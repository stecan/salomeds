/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.tools.encrypt_prop;


import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;

import javax.crypto.Cipher;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.objectweb.salome_tmf.api.Util;

/**
 *
 * @author  marchemi
 */
public class MD5paswd {
    static Key key;
    private static String encoder = "DES";
    private static String cyphencoder = "DES/ECB/PKCS5Padding";
    /*private static byte[] iv = { (byte) 0xc9, (byte) 0x36, (byte) 0x78, (byte) 0x99,
      (byte) 0x52, (byte) 0x3e, (byte) 0xea, (byte) 0xf2 };*/
    //private static IvParameterSpec salt = new IvParameterSpec(iv);
    private static Provider provider;
    static Cipher cipher;
    static{   
        try { 
            provider = new com.sun.crypto.provider.SunJCE();
            cipher = Cipher.getInstance(cyphencoder, provider.getName());
        }catch(Exception e){
            Util.err(e);
        }
    }
    
    static void readkey(java.net.URL base) throws Exception{
        //byte[] rawKey = null;
        java.net.URL url_key = new java.net.URL(base +"/key.txt");
        java.io.DataInputStream in = new java.io.DataInputStream(url_key.openStream());
        java.io.ByteArrayOutputStream bytekey = new java.io.ByteArrayOutputStream() ;  
        byte[] buf = new byte[1024];
        int off = 0;
        int len = 0;
        while (len != -1){
            len = in.read(buf, 0, 1024);
            if (len != -1){
                off += len;
                bytekey.write(buf, 0, len); 
            }
        }
        in.close();
        key = new SecretKeySpec(bytekey.toByteArray() , encoder);
        bytekey.close();
    }
    
    
        
    public static void writekey(String paht_base) throws Exception{
        java.io.File keyFile = new java.io.File(paht_base + java.io.File.separator + "key.txt");
        java.io.DataOutputStream out = new java.io.DataOutputStream(new java.io.FileOutputStream(keyFile));
        
        javax.crypto.KeyGenerator kgen = javax.crypto.KeyGenerator.getInstance(encoder,  provider);
        javax.crypto.SecretKey skey = kgen.generateKey();
        byte[] raw = skey.getEncoded();
        DESKeySpec desKey = new DESKeySpec(raw); 
        raw = desKey.getKey();
        key = new SecretKeySpec(raw , encoder);
        
        out.write(raw, 0, raw.length);
        out.close();
  
    }
    
    public static String getEncodedPassword(String clearTextPassword)
        throws NoSuchAlgorithmException {
        //return encrypString(clearTextPassword);
        
        clearTextPassword = clearTextPassword.trim();
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(clearTextPassword.getBytes()); 
        return toHexString(md.digest());
        
    }
    
    private static char[] hexChar = {
        '0' , '1' , '2' , '3' ,
        '4' , '5' , '6' , '7' ,
        '8' , '9' , 'a' , 'b' ,
        'c' , 'd' , 'e' , 'f'};
    
    private static int charToNibble ( char c ) {
        if ( '0' <= c && c <= '9' ) {
            return c - '0';
        }
        else if ( 'a' <= c && c <= 'f' ) {
            return c - 'a' + 0xa;
        }
        else if ( 'A' <= c && c <= 'F' ){
            return c - 'A' + 0xa;
        }
        else {
            throw new IllegalArgumentException ( "Invalid hex character: " + c );
        }
    }
    
    
    private static String toHexString ( byte[] b ) {
        StringBuffer sb = new StringBuffer( b.length * 2 );
        for ( int i=0; i<b.length; i++ ) {
            // look up high nibble char
            sb.append( hexChar [( b[i] & 0xf0 ) >>> 4] );
            
            // look up low nibble char
            sb.append( hexChar [b[i] & 0x0f] );
        }
        return sb.toString();
    }

    private static byte[] toBytesArray ( String s ) {
        int stringLength = s.length();
        if ( (stringLength & 0x1) != 0 ) {
            throw new IllegalArgumentException ( "fromHexString requires an even number of hex characters" );
        }
        byte[] b = new byte[stringLength / 2];
        for ( int i=0,j=0; i<stringLength; i+=2,j++ ) {
            int high = charToNibble( s.charAt ( i ) );
            int low = charToNibble( s.charAt ( i+1 ) );
            b[j] = (byte)( ( high << 4 ) | low );
        }
        return b;
    }
    
    /*
      private static String toHexString(byte bytes[]) {
      StringBuffer retString = new StringBuffer();
      for (int i = 0; i < bytes.length; ++i){
      retString.append(
      Integer.toHexString(0x0100 + (bytes[i] & 0x00FF)).substring(1));
      }
      return retString.toString();
      }
    
      private static byte[] toBytesArray(String hexString){
      //Convert hex string back to byte
      byte[] hexByte = new BigInteger(hexString,16).toByteArray(); 
      return hexByte;
      }
    */
    public static boolean testPassword(String clearTextTestPassword,
                                       String encodedActualPassword)
        throws NoSuchAlgorithmException{
        if (clearTextTestPassword.equals(encodedActualPassword))
            return true;
        /*String clearTextTestPassword2 = decryptString(encodedActualPassword);
          Util.debug("MD5 compare : "+ clearTextTestPassword2 + " and " + clearTextTestPassword);
          return (clearTextTestPassword2.equals(clearTextTestPassword));
        */
        
        String encodedTestPassword = MD5paswd.getEncodedPassword(clearTextTestPassword);
        //Util.debug("MD5 compare : "+ encodedTestPassword + " and2 " + encodedActualPassword);
        return (encodedTestPassword.equals(encodedActualPassword));
        
    }
    
    public static String decryptString(String hexString){
        byte[] data = toBytesArray(hexString);
        return decryptData(data);
    }
    
    public static String encrypString(String data){
        byte[] sdata  = encryptData(data);
        return toHexString(sdata);
    }
    
    /*static private boolean testKeyWith(String cleardata){
      try  {
      String codedata = encrypString(cleardata);
      if (cleardata.equals(decryptString(codedata)))
      return true;
      }catch(Exception e){
      return false;
      }
      return true;
      }*/
    
    static private String decryptData(byte[] data) {
        try {
            //cipher.init(Cipher.DECRYPT_MODE, key,  salt);
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] original = cipher.doFinal(data);
            //Util.debug("Decrypted data: " + new String(original));
            return new String(original);
        } catch (InvalidKeyException e) {
            Util.err(e);
        } catch (IllegalStateException e) {
            Util.err(e);
        } catch (Exception e) {
            Util.err(e);
        }
        return null;
    }
    
    static private byte[] encryptData(String sData) {
        try {
            byte[] data = sData.getBytes();
            //cipher.init(Cipher.ENCRYPT_MODE, key,  salt);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] result = cipher.doFinal(data);
            return result;              
        } catch (InvalidKeyException e){
            Util.err(e);
        } catch (IllegalStateException e){
            Util.err(e);
        } catch (Exception e){
            Util.err(e);
        } 
        return null;
    }
}
