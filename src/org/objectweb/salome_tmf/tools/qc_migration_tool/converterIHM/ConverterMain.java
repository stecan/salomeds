/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml.Project;

public class ConverterMain extends JFrame {

    private static final long serialVersionUID = 1L;
    private JTabbedPane tabs;
    private JTextField sourceDirQC_Salome;
    private JTextField targetFileQC_Salome;
    private JTextField sourceFileSalome_QC;
    private JTextField targetFileSalome_QC;
    private JRadioButton version8;
    private JRadioButton version9;
    private JTextField textNameSeparator;
    private JTextField textDescripSeparator;
    private JRadioButton btnTestSuiteRule;
    private JRadioButton btnFamilyRule;
    private JRadioButton btnTestRule;
    private JCheckBox boxDelUnusedDirectory;
    private JCheckBox boxDelTestAuto;
    private JProgressBar progressBarQC_Salome;
    private JProgressBar progressBarSalome_QC;  
    private String lastSearch = "";
        
    public ConverterMain(){
        super("Passerelle d'import-export entre SalomeTMF et Quality Center*");
        addWindowListener(new WindowAdapter()
            {@Override
            public void windowClosing(WindowEvent e){
                if (CmdExit.getCanExit()){
                    int response = JOptionPane.showConfirmDialog(getContentPane(), "Voulez-vous vraiment quitter", "Confirmation", JOptionPane.YES_NO_OPTION);
                    if (response == JOptionPane.YES_OPTION)             
                        System.exit(0);
                }
                else
                    JOptionPane.showMessageDialog(getContentPane(),"Vous ne pouvez pas quitter l'application a ce stage d'execution\n" );
            }} ) ; 
            
        addWindowListener(new WindowAdapter() {
                @Override
                public void windowActivated(WindowEvent e) {
                    sourceDirQC_Salome.requestFocusInWindow();
                }
            });
    }
        
    /**
     * initialise la fenetre 
     */
    public void init(){
        Container cp = this.getContentPane();
        cp.setLayout(new BorderLayout());               
        tabs = new JTabbedPane();
        tabs.setSize(620, 580);
        tabs.setPreferredSize(tabs.getSize());
        cp.add(tabs);                   
        initQC_Salome();
        initSalome_QC();
        JPanel pQC = new JPanel();
        pQC.add(new JLabel("*Mercury Quality Center is a trademark of Mercury Interactive Corporation"));
        cp.add(pQC, BorderLayout.SOUTH);
        setSize(tabs.getWidth() + 10, tabs.getHeight() + 40);
        setPreferredSize(getSize());
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screen.width - getSize().width)/2,(screen.height - getSize().height)/2); 
    }
        
    /**
     * initialise l'onglet de la migration de Quality Center vers SalomeTMF
     */
    private void initQC_Salome(){
        //panel des chemins de fichiers
        JPanel tab = new JPanel();
        tabs.addTab("Quality Center* --> Salome", tab);
        tabs.setMnemonicAt(0, KeyEvent.VK_Q);
        JPanel directoryPanel = new JPanel();
        directoryPanel.setLayout(new GridLayout(2,1));
        directoryPanel.setPreferredSize(new Dimension(580,90));
        directoryPanel.setBorder(new TitledBorder("Fichiers"));
        tab.add(directoryPanel);
                
        JPanel sourcePanel = new JPanel();
        directoryPanel.add(sourcePanel);
        sourcePanel.add(new JLabel("Chemin du repertoire source : "));  
        sourceDirQC_Salome = new JTextField(26);
        sourcePanel.add(sourceDirQC_Salome);
        Invoker searchSce = new Invoker("Parcourir", this);
        searchSce.setCommande(new CmdSearch(searchSce, sourceDirQC_Salome, JFileChooser.DIRECTORIES_ONLY));
        sourcePanel.add(searchSce);
                
        JPanel targetPanel = new JPanel();
        targetPanel.add(new JLabel("Chemin du fichier resultat :"));
        targetPanel.add(Box.createHorizontalStrut(15));
        targetFileQC_Salome = new JTextField(26);                               
        targetPanel.add(targetFileQC_Salome);
        Invoker searchTarget = new Invoker("Parcourir", this);
        searchTarget.setCommande(new CmdSearch(searchTarget,targetFileQC_Salome, JFileChooser.FILES_ONLY));
        targetPanel.add(searchTarget);
        directoryPanel.add(targetPanel);                                
                
        //panel des versions
        JPanel versionPanel = new JPanel();
        versionPanel.setLayout(new GridLayout(2,1));
        versionPanel.setPreferredSize(new Dimension(580, 80));
        versionPanel.setBorder(new TitledBorder("Version de Quality Center*"));
        tab.add(versionPanel);
        ButtonGroup version = new ButtonGroup();
        version8 = new JRadioButton("Version 8", true);
        version.add(version8);
        versionPanel.add(version8);
        version9 = new JRadioButton("Version 9");
        version.add(version9);
        versionPanel.add(version9);
                
        //panel pour la transformation de l'arborescence
        JPanel rulesPanel = new JPanel();
        rulesPanel.setLayout(new GridLayout(9,1));
        rulesPanel.setPreferredSize(new Dimension(580, 250));
        rulesPanel.setBorder(new TitledBorder("Transformation de l'arborescence des tests"));
        tab.add(rulesPanel);
                
        JPanel nodeSeparatorPanel = new JPanel();
        rulesPanel.add(nodeSeparatorPanel);
        nodeSeparatorPanel.add(new JLabel("Separateur des noms de niveaux d'arborescence :"));
        nodeSeparatorPanel.add(Box.createHorizontalStrut(36));
        textNameSeparator = new JTextField(Project.getNameSeparator(),2);
        nodeSeparatorPanel.add(textNameSeparator);
        nodeSeparatorPanel.add(Box.createHorizontalStrut(192));
        JPanel descripSeparatorPanel = new JPanel();
        rulesPanel.add(descripSeparatorPanel);
        descripSeparatorPanel.add(new JLabel("Separateur des descriptions de niveaux d'arborescence : "));
        textDescripSeparator = new JTextField(Project.getDescripSepartor(),20);
        descripSeparatorPanel.add(textDescripSeparator);
                
        rulesPanel.add(new JLabel());
        ButtonGroup rules = new ButtonGroup();
        btnTestSuiteRule = new JRadioButton("Priorite au premier niveau de l'arborescence",true);
        rules.add(btnTestSuiteRule);
        rulesPanel.add(btnTestSuiteRule);
        btnFamilyRule = new JRadioButton("Priorite au dernier niveau de l'arborescence");
        rules.add(btnFamilyRule);
        rulesPanel.add(btnFamilyRule);
        btnTestRule = new JRadioButton("Priorite aux deux premiers niveaux de l'arborescence");
        rules.add(btnTestRule);
        rulesPanel.add(btnTestRule);
        rulesPanel.add(new JLabel());
        boxDelTestAuto = new JCheckBox("Supprimer les tests automatiques");
        rulesPanel.add(boxDelTestAuto); 
        boxDelUnusedDirectory = new JCheckBox("Supprimer les repertoires vides ou contenant un seul sous-repertoire");
        rulesPanel.add(boxDelUnusedDirectory);
                        
                
        //panel d'execution
        JPanel panelExec = new JPanel(); 
        tab.add(panelExec);
        panelExec.setPreferredSize(new Dimension(580, 50));
        JPanel pExec = new JPanel();
        panelExec.add(pExec);
        Invoker cmdExecute = new Invoker("Executer", this);
        cmdExecute.setCommande(new CmdExecute(cmdExecute, CmdExecute.QC_SALOME));
        pExec.add(cmdExecute);
        Invoker cmdExitInvoker = new Invoker("Quitter", this);
        cmdExitInvoker.setCommande(new CmdExit(cmdExitInvoker));
        pExec.add(cmdExitInvoker);      
        progressBarQC_Salome = new JProgressBar();
        tab.add(progressBarQC_Salome);
    }
        
    /**
     * initialise l'onglet de la migration de SalomeTMF vers Quality Center  
     */
    public void initSalome_QC(){
        //panel des chemins de fichiers
        JPanel tab = new JPanel();
        tabs.addTab("Salome --> Quality Center*", tab);
        tabs.setMnemonicAt(1, KeyEvent.VK_S);
        JPanel directoryPanel = new JPanel();
        directoryPanel.setLayout(new GridLayout(2,1));
        directoryPanel.setPreferredSize(new Dimension(580,90));
        directoryPanel.setBorder(new TitledBorder("Fichiers"));
        tab.add(directoryPanel);
        JPanel sourcePanel = new JPanel();
        directoryPanel.add(sourcePanel);                
        sourcePanel.add(new JLabel("Chemin du fichier source :  "));    
        sourceFileSalome_QC = new JTextField(27);                       
        sourcePanel.add(sourceFileSalome_QC);
        Invoker searchSce = new Invoker("Parcourir", this);
        searchSce.setCommande(new CmdSearch(searchSce,sourceFileSalome_QC, JFileChooser.FILES_ONLY));
        sourcePanel.add(searchSce);
                
        JPanel targetPanel = new JPanel();
                
        directoryPanel.add(targetPanel);
        targetPanel.add(new JLabel("Chemin du fichier resultat : "));   
        targetFileSalome_QC = new JTextField(27);
        targetPanel.add(targetFileSalome_QC);
        Invoker searchTarget = new Invoker("Parcourir", this);
        searchTarget.setCommande(new CmdSearch(searchTarget, targetFileSalome_QC, JFileChooser.FILES_ONLY));
        targetPanel.add(searchTarget);
                
        //panel separateur
        JPanel panelSpace = new JPanel(); 
        panelSpace.setPreferredSize(new Dimension(580,335));
        tab.add(panelSpace);
                
        //panel d'execution
        JPanel panelExec = new JPanel(); 
        tab.add(panelExec);
        panelExec.setPreferredSize(new Dimension(580, 50));
        JPanel pExec = new JPanel();
        panelExec.add(pExec);
        Invoker cmdExecInvoker = new Invoker("Executer", this);
        cmdExecInvoker.setCommande(new CmdExecute(cmdExecInvoker, CmdExecute.SALOME_QC));
        pExec.add(cmdExecInvoker);
        Invoker cmdExitInvoker = new Invoker("Quitter", this);
        cmdExitInvoker.setCommande(new CmdExit(cmdExitInvoker));
        pExec.add(cmdExitInvoker);      
        progressBarSalome_QC = new JProgressBar();
        tab.add(progressBarSalome_QC);          
    }
        
    /**
     * retourne le chemin du repertoire source de la transformation Quality Center --> SalomeTMF
     * @return
     */
    public String getSourceDirQC_Salome() {
        return sourceDirQC_Salome.getText();
    }

    /**
     * retourne le chemin du fichier source de la transformation SalomeTMF --> Quality Center
     * @return
     */
    public String getSourceFileSalome_QC() {
        return sourceFileSalome_QC.getText();
    }
        
    /**
     * retourne le chemin du fichier a creer par la transformation SalomeTMF --> Quality Center
     * @return
     */

    public String getTargetFileSalome_QC() {
        return targetFileSalome_QC.getText();
    }

    /**
     * retourne le chemin du fichier a creer par la transformation Quality Center --> SalomeTMF
     * @return
     */
    public String getTargetFileQC_Salome() {
        return targetFileQC_Salome.getText();
    }

    /**
     * retourne true s'il faut supprimer les tests automatiques
     * @return
     */
    public boolean getDelTestAuto() {
        return boxDelTestAuto.isSelected();
    }
        
    /**
     * retourne true s'il faut supprimer les repertoires possedant un seul sous-noeud et aucun test  
     */
    public boolean getDelUnusedDirectory() {
        return boxDelUnusedDirectory.isSelected();
    }

    /**
     * retourne la chaine separatrice des descriptions des noeuds concatenes apres l'execution d'une regle de transformation
     * @return
     */
    public String getDescripSeparator() {
        return textDescripSeparator.getText();
    }

    /**
     * retourne la chaine separatrice des noms des noeuds concatenes apres l'execution d'une regle de transformation
     * @return
     */
    public String getNameSeparator() {
        return textNameSeparator.getText();
    }
        
    /**
     * retourne le chemin du dernier repertoire ou fichier indique a l'interface utilisateur
     * @return
     */
    public String getLastSearch() {
        return lastSearch;
    }

    /**
     * affecte le chemin du dernier repertoire ou fichier indique a l'interface utilisateur
     * @return
     */
    public void setLastSearch(String lastSearch) {
        this.lastSearch = lastSearch;
    }

    /**
     * retourne le numero de la regle de transformation selectionnee
     * @return 
     */
    public int getRuleSelected(){
        int rule;
        if (btnTestSuiteRule.isSelected())
            rule = ThreadExecuteQC_Salome.TESTSUITE_RULE;
        else if (btnFamilyRule.isSelected())
            rule = ThreadExecuteQC_Salome.FAMILY_RULE;
        else
            rule = ThreadExecuteQC_Salome.TEST_RULE;
        return rule;
    }
        
    /**
     * retourne la version de Quality Center selectionnee
     * @return
     */
    public short getVersionSelected(){
        short v;
        if (version8.isSelected())
            v = Project.VERSION8;
        else
            v = Project.VERSION9;
        return v;
    }

    /**
     * active ou desactive la bar de progression de l'onglet Quality Center --> SalomeTMF
     * @param actived si true active la bar, sinon desactive
     */
    public void setActivedProgressBarQC_Salome(boolean actived){
        progressBarQC_Salome.setIndeterminate(actived);
    }
        
    /**
     * active ou desactive la bar de progression de l'onglet SalomeTMF --> Quality Center
     * @param actived si true active la bar, sinon desactive
     */
    public void setActivedProgressBarSalome_QC(boolean actived){
        progressBarSalome_QC.setIndeterminate(actived);
    }
                
    /**
     * fonction principale de l'application
     * @param args
     */
    public static void main(String[] args) {
        ConverterMain c = new ConverterMain();
        c.init();
        c.setVisible(true);
        c.pack();       
    }
        
}
