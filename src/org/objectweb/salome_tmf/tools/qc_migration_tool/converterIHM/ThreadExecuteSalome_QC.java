/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

import java.io.File;

import javax.swing.JOptionPane;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toExcel.ConverterToExcel;

public class ThreadExecuteSalome_QC extends Thread{
        
    private ConverterMain param;
        
    public ThreadExecuteSalome_QC(ConverterMain param){
        super();
        this.param = param;
    }

    @Override
    public void run() {
        param.setActivedProgressBarSalome_QC(true);
                
        //verifier le contenu des champs
        String errors = "";
        String sourceFile = param.getSourceFileSalome_QC();
        String targetFile = param.getTargetFileSalome_QC();
        if (sourceFile.equals("") || targetFile.equals(""))
            errors += "Tous les champs de texte sont obligatoires \n";
        if((sourceFile.indexOf(File.separator) != 2 || sourceFile.length() <= 3) && !sourceFile.equals(""))
            errors += "Le chemin " + sourceFile + " est incorrecte\n";
        else if (!(new File(sourceFile)).isFile())
            errors += "Le fichier " + sourceFile + " n'existe pas";             
        if((targetFile.indexOf(File.separator) != 2 || targetFile.length() <= 3) && !targetFile.equals("")) 
            errors += "Le chemin " + targetFile+ " est incorrecte\n";
        else if (new File(targetFile).isDirectory())
            errors += targetFile + " n'est pas un fichier";     
        else if (!targetFile.endsWith(".xls"))
            errors += targetFile + " n'est pas un fichier excel";
        if (!errors.equals("")){
            param.setActivedProgressBarSalome_QC(false);        
            JOptionPane.showMessageDialog(param, errors, "Erreurs", JOptionPane.ERROR_MESSAGE); 
        }
        //tous les champs sont bien renseignes
        else{
            try {
                ConverterToExcel converter = new ConverterToExcel(sourceFile, targetFile);
                converter.convertToExcel();
                param.setActivedProgressBarSalome_QC(false);
                JOptionPane.showMessageDialog(param, 
                                              "La transformation a ete effectuee avec succes",
                                              "Fin de transformation",
                                              JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                param.setActivedProgressBarSalome_QC(false);    
                JOptionPane.showMessageDialog(param, "L'erreur suivante s'est produite " + e.getMessage());
                Util.err(e);
            }
        }
    }   
}
