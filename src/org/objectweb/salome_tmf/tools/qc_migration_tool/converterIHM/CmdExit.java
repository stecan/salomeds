/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

import javax.swing.JOptionPane;

public class CmdExit implements ICommand{

    private static boolean canExit = true;
    private Invoker invoker;
        
    /**
     * @param invoker declencheur de la commande
     */
    public CmdExit(Invoker invoker){
        this.invoker = invoker;
    }

    @Override
    public void execute() {
        if (canExit){
            int response = JOptionPane.showConfirmDialog(invoker.getParam(), "Voulez-vous vraiment quitter", "Confirmation", JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION)             
                System.exit(0);
        }
        else
            JOptionPane.showMessageDialog(invoker.getParam(), "Vous ne pouvez pas quitter l'application a ce stage d'execution\n" );
    }

    @Override
    public Invoker getInvoker() {
        return invoker;
    }

    @Override
    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }
        
    /**
     * indique si l'arret de l'application est possible sans alteration de donnees.
     * @param canExit true si l'arret est possible, false sinon
     */
    public static void setCanExit(boolean canExit) {
        CmdExit.canExit = canExit;
    }

    public static boolean getCanExit() {
        return canExit;
    }                   
}
