/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

import javax.swing.JFileChooser;
import javax.swing.JTextField;

public class CmdSearch implements ICommand {
        
    private Invoker invoker;
    private int selectMode;
    private JTextField target;
        
    /**
     * 
     * @param invoker declencheur de la commande
     * @param target cible de la commande
     * @param selectMode indique le type de donnees a selectionner (repertoire ou fichier)
     */
    public CmdSearch(Invoker invoker, JTextField target, int selectMode){
        this.invoker =invoker;
        this.selectMode = selectMode;
        this.target = target;
    }
        
    @Override
    public void execute() {
        ConverterMain parent = invoker.getParam();
        JFileChooser chooser = new JFileChooser(parent.getLastSearch());
        chooser.setFileSelectionMode(selectMode);
        int returnVal = chooser.showOpenDialog(parent);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            target.setText(chooser.getSelectedFile().getAbsolutePath());
            parent.setLastSearch(target.getText());
        }
    }

    @Override
    public Invoker getInvoker() {
        return invoker;
    }

    @Override
    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }   

}
