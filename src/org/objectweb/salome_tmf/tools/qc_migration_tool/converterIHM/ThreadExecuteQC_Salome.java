/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml.ConverterToXml;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml.LabelNotFoundException;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml.Project;

public class ThreadExecuteQC_Salome extends Thread{
        
    //regles de tansformation d'arborescence
    public static final short TESTSUITE_RULE = 1;
    public static final short FAMILY_RULE = 2;
    public static final short TEST_RULE = 3;
    private ConverterMain param;

    public ThreadExecuteQC_Salome(ConverterMain param) {
        super();
        this.param = param;
    }
        
    @Override
    public void run(){  
        param.setActivedProgressBarQC_Salome(true);
                
        //verifier le contenu des champs
        String errors = "";
        String sourceDir = param.getSourceDirQC_Salome();
        String targetFile = param.getTargetFileQC_Salome();
        if (sourceDir.equals("") || targetFile.equals("") || param.getNameSeparator().equals("") || param.getDescripSeparator().equals(""))
            errors += "Tous les champs de texte sont obligatoires \n";
        if(!sourceDir.equals("")){
            if (sourceDir.indexOf(File.separator) != 2)
                errors += "Le chemin de repertoire " + sourceDir + " est incorrecte\n";
            else if (new File(sourceDir).isFile())
                errors += "Il existe deja un fichier de nom " + sourceDir;
        }
        if (!targetFile.equals(""))
            if(targetFile.indexOf(File.separator) != 2 || targetFile.length() <= 3) 
                errors += "Le chemin de fichier " + targetFile+ " est incorrecte\n";
            else if (new File(targetFile).isDirectory())
                errors += targetFile + " est deja un nom de repertoire";                

        if (!errors.equals("")){
            param.setActivedProgressBarQC_Salome(false);        
            JOptionPane.showMessageDialog(param, errors, "Erreurs", JOptionPane.ERROR_MESSAGE); 
        }
                
        //tous les champs sont bien renseignes
        else{
            Project.setNameSeparator(param.getNameSeparator());
            Project.setDescripSepartor(param.getDescripSeparator());
            ConverterToXml converter = null;
            try{        
                converter = new ConverterToXml(param.getSourceDirQC_Salome(), param.getTargetFileQC_Salome(), param.getVersionSelected());
                converter.initConversion();
                CmdExit.setCanExit(false);
                if (param.getRuleSelected() == TESTSUITE_RULE)
                    converter.convertWithTestListRule(param.getDelTestAuto(), param.getDelUnusedDirectory());
                else if (param.getRuleSelected() == FAMILY_RULE)
                    converter.convertWithFamilyRule(param.getDelTestAuto(), param.getDelUnusedDirectory());
                else
                    converter.convertWithTestRule(param.getDelTestAuto(), param.getDelUnusedDirectory());
                param.setActivedProgressBarQC_Salome(false);    
                if (new File(converter.getProject().getReportFilePath()).length() > 0)
                    JOptionPane.showMessageDialog(param, 
                                                  "Un rapport <" + converter.getProject().getReportName() + "> a ete genere dans " + converter.getProject().getTargetDirectory(),
                                                  "Fin de transformation",
                                                  JOptionPane.ERROR_MESSAGE);   
                else
                    JOptionPane.showMessageDialog(param, 
                                                  "La transformation a ete effectuee avec succes",
                                                  "Fin de transformation",
                                                  JOptionPane.INFORMATION_MESSAGE);
                CmdExit.setCanExit(true);
            }catch(LabelNotFoundException e){
                param.setActivedProgressBarQC_Salome(false);    
                Object[] options = {"Corriger le fichier", "Remplacer par les valeurs par defaut", "Aide"};
                int response ;
                response = JOptionPane.showOptionDialog(param, 
                                                        e.getMessage(),                                          
                                                        "Erreur dans le fichier de configuration",
                                                        JOptionPane.YES_NO_OPTION,
                                                        JOptionPane.QUESTION_MESSAGE,
                                                        null,     
                                                        options, 
                                                        options[0]); 
                if (response == JOptionPane.YES_OPTION){
                    String executable = "cmd /c call \""+ Project.CONFIG_FILE + "\"";
                    try {
                        Runtime.getRuntime().exec(executable);
                    } catch (IOException e1) {
                        Util.err(e1);
                    }
                }
                else if (response == JOptionPane.NO_OPTION){
                    param.setActivedProgressBarQC_Salome(true); 
                    converter.getProject().useDefaultLabel();
                    try{
                        converter.initConversion();
                        CmdExit.setCanExit(false);
                        if (param.getRuleSelected() == TESTSUITE_RULE)
                            converter.convertWithTestListRule(param.getDelUnusedDirectory(),  param.getDelTestAuto());
                        else if (param.getRuleSelected() == FAMILY_RULE)
                            converter.convertWithFamilyRule(param.getDelUnusedDirectory(),  param.getDelTestAuto());
                        else
                            converter.convertWithTestRule(param.getDelUnusedDirectory(),  param.getDelTestAuto());
                        param.setActivedProgressBarQC_Salome(false);    
                        if (new File(converter.getProject().getReportFilePath()).length() > 0)
                            JOptionPane.showMessageDialog(param, 
                                                          "Un rapport <" + converter.getProject().getReportName() + "> a ete genere dans " + converter.getProject().getTargetDirectory(),
                                                          "Fin de transformation",
                                                          JOptionPane.ERROR_MESSAGE);
                        else
                            JOptionPane.showMessageDialog(param, 
                                                          "La transformation a ete effectuee avec succes",
                                                          "Fin de transformation",
                                                          JOptionPane.INFORMATION_MESSAGE);
                        CmdExit.setCanExit(true);
                    }catch(Exception ex){
                        param.setActivedProgressBarQC_Salome(false);    
                        CmdExit.setCanExit(true);
                        Util.err(e);
                    }
                }
            }                   
            catch(Exception e){
                param.setActivedProgressBarQC_Salome(false);    
                JOptionPane.showMessageDialog(param, "L'erreur suivante s'est produite " + e.getMessage());
                CmdExit.setCanExit(true);
                Util.err(e);
            }
        }
                                
    }
        
}
