/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converterIHM;

public class CmdExecute extends Thread implements ICommand{
        
    private Invoker invoker;
    private short conversionDirection;
    public static final short QC_SALOME = 1;
    public static final short SALOME_QC = 2;
        
    /**
     * @param invoker le declencheur de la commande
     * @param conversionDirection sens de la transformation de fichier QC_SALOME ou SALOME_QC
     */
    public CmdExecute(Invoker invoker, short conversionDirection){
        super();
        this.invoker = invoker;
        this.conversionDirection = conversionDirection;
    }

    @Override
    public void execute() {
        if (conversionDirection == QC_SALOME)
            new ThreadExecuteQC_Salome(invoker.getParam()).start();
        else if (conversionDirection == SALOME_QC)
            new ThreadExecuteSalome_QC(invoker.getParam()).start();
    }
                
    @Override
    public Invoker getInvoker() {
        return invoker;
    }

    @Override
    public void setInvoker(Invoker invoker) {
        this.invoker = invoker;
    }   
        
}
