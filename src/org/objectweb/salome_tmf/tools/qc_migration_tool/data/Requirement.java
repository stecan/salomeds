/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.data;

import java.io.File;

public class Requirement extends CommunData implements IRequirement{
        
    private int priority = RequirementFamily.DEFAULT_PRIORITY;
    private int category = RequirementFamily.DEFAULT_CATEGORY;
    private int complexity = RequirementFamily.DEFAULT_COMPLEXITY;
    private int state = RequirementFamily.DEFAULT_STATE;
    private String verifway = RequirementFamily.DEFAULT_VERIFWAY;
    private String origin = RequirementFamily.DEFAULT_ORIGIN;
    private String version = RequirementFamily.DEFAULT_VERSION;
    private RequirementFamily parent;
    private String path;
        
    public Requirement() {
    }
        
    public Requirement(RequirementFamily parent){
        if (parent != null){
            this.parent = parent;
            parent.addReq(this);
        }
    }
        
    @Override
    public RequirementFamily getParent() {
        return parent;
    }
        
    @Override
    public void setParent(RequirementFamily parent) {
        this.parent = parent;
        path = null;
    }
        
    public int getPriority() {
        return priority;
    }
        
    public void setPriority(int priority) {
        this.priority = priority;
    }
                
    public String getVersion() {
        return version;
    }
        
    public void setVersion(String version) {
        if (!version.equals(""))
            this.version = version;
    }   
        
                
    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public int getComplexity() {
        return complexity;
    }

    public void setComplexity(int complexity) {
        this.complexity = complexity;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        if (!origin.equals(""))
            this.origin = origin;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getVerifway() {
        return verifway;
    }

    public void setVerifway(String verifway) {
        if (!verifway.equals(""))
            this.verifway = verifway;
    }

    @Override
    public String toString(){
        return "**"+ name + " " + version + " " + priority;
    }

    @Override
    public String getAbsolutePath() {
        String absolutePath;
        if (path == null){
            RequirementFamily current = parent;
            absolutePath = name;
            while(current != null){
                absolutePath = current.getName() + "\\" + path;
                current = current.getParent();
            }                   
        }
        else if (parent != null)
            absolutePath = path + "\\" + name;
        else
            absolutePath = name;
        return absolutePath;
    }
        

    @Override
    public String getPath() {
        if (path == null){
            if (parent != null){
                RequirementFamily current = parent.getParent();
                path = parent.getName();
                while(current != null){
                    path = current.getName() + "\\" + path;
                    current = current.getParent();
                }
            }
            else
                path = "";
        }
        return path;
    }

    public String getIdAbsolutePath() {
        RequirementFamily current = parent;
        path = "" + id;
        while(current != null){
            path = current.getName() + File.separator + path;
            current = current.getParent();
        }
        return path;
    }

}
