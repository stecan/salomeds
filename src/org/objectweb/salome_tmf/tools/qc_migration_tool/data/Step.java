/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.data;

public class Step extends CommunData{

    private String result;
    private Test test;
        
    public Step(){}
        
    public Step(Test test){
        this.test = test;
    }
                
    public String getResult() {
        return result;
    }
        
    public void setResult(String result) {
        this.result = result;
    }   
        
    /**
     * ajoute une nouvelle ligne a un resultat attendu comportant plusieurs lignes 
     * @param result valeur de la nouvelle ligne
     */
    public void completeResult(String result) {
        this.result += "\\n" + result;
    }
        
    @Override
    public String getAbsolutePath(){
        if (test != null)
            return test.getAbsolutePath() + "\\" + name;
        else
            return "";
    }
        
    @Override
    public String toString(){
        String string = "\n//////" + name;
        for (int i = 0; i < attachments.size(); i++){
            string += "\n-----------" + ((IAttachment)attachments.get(i)).getLocation();
        }
        return string;
    }
}
