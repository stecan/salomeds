/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.data;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml.Project;

public class CommunData {
                
    protected int id;
    protected String name = "inconnu";
    protected String description;
    protected List attachments = new ArrayList();
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
        
    public void setDescription(String description) {
        this.description = description;
    }
        
    /**
     * ajoute une nouvelle ligne a une description comportant plusieurs lignes 
     * @param description valeur de la nouvelle ligne
     */
    public void completeDescription(String description){
        this.description += "\\n" + description;
    }
        
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getAttachments() {
        return attachments;
    }
        
    public void setAttachments(List attachments) {
        this.attachments = attachments;
    }   
        
    public void addAttachment(IAttachment att) {
        this.attachments.add(att);
    }
        
    public void addAttachment(List att) {
        for (int i = 0; i < att.size(); i++)
            if (!attachments.contains(att.get(i)))
                this.attachments.add(att.get(i));
    }
        
    /**
     * transfere les attachements dans une nouvelle liste et les supprime de la donnee courant 
     * @return
     */
    public List moveAttachments(){
        List atts = new ArrayList();
        atts.addAll(attachments);
        attachments.clear();
        return atts;
    }
        
    /**
     * retourne le chemin absolu des parents, s'ils existent, jusqu'a l'objet
     * @return
     */
    public String getAbsolutePath(){
        return name;
    }
        
    /**
     *Supprime les attachements dupliques. 
     *Le critere de verification est l'emplacement reel des attachements
     *S'il existe des fichiers portants le meme nom est ayant des tailles differentes, le second est renomme
     */
    public String rmDuplicateAttachments(){
        String report = "";
        for (int i = 0; i < attachments.size(); i++){
            IAttachment attach_i = (IAttachment)attachments.get(i);
            for (int j = i + 1; j < attachments.size(); j++){
                IAttachment attach_j = (IAttachment)attachments.get(j);
                if (attach_i instanceof UrlAttachment && attach_i.getLocation().equals(attach_j.getLocation())){
                    attachments.remove(attach_j);
                    j--;
                }
                else if (attach_i instanceof FileAttachment && attach_j instanceof FileAttachment && ((FileAttachment)attach_i).getName().equals(((FileAttachment)attach_j).getName())){
                    File file_i = new File(attach_i.getLocation() + File.separator + ((FileAttachment)attach_i).getName());
                    File file_j = new File(attach_j.getLocation() + File.separator + ((FileAttachment)attach_j).getName());
                    if (file_i.length() == file_j.length()){                                            
                        attachments.remove(attach_j);
                        j--;
                        boolean b = file_j.delete();
                        if (!b)
                            report += "Erreur fatale : Le fichier " + file_j.getAbsolutePath() + " n'a pas ete supprime car il est introuvable.\n";
                    }
                    else{
                        String oldName = ((FileAttachment)attach_j).getName();
                        boolean b = file_j.renameTo( new File(attach_j.getLocation() + File.separator + i + "_" + oldName));                                            
                        if (b){
                            ((FileAttachment)attach_j).setName(i + "_" + oldName);                                              
                            report += "Le fichier " + attach_j.getLocation() + File.separator + oldName + " a ete renomme en " +
                                attach_j.getLocation() + File.separator + ((FileAttachment)attach_j).getName() + "\n";
                        }
                    }                                           
                }
            }
        }
        return report;
    }
        
    /**
     * concatene les attributs complete avec ses attributs 
     * @param complete
     */
    public void completeWith(CommunData complete){
        addAttachment(complete.moveAttachments());
        setName(complete.getName() + Project.getNameSeparator()  + getName());
        if (complete.getDescription() != null)
            setDescription(complete.name + " :\\n" + complete.description + "\\n" + Project.getDescripSepartor()
                           + "\\n" + name + " \\n" + description);      
    }
                
    @Override
    public String toString(){
        return name;
    }
        
    /**
     * se compare un objet 
     * @param elmt 
     * @return retourne true s'ils sont du meme type et si les attributs sont egaux (pour les attachements, on compare uniquement la taille)
     */
    public boolean equalsToElement (CommunData elmt){
        if (this instanceof CommunData && elmt instanceof CommunData)
            if (id == elmt.getId() && name.equals(elmt.name) && attachments.size() == elmt.getAttachments().size()
                && (description == null && elmt.getDescription() == null || description.equals(elmt.description)))
                return true;
        return false;
    }
        
}
