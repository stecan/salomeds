/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Test extends CommunData{
        
    private static List allParams = new ArrayList();
    private TestSuite suite;
    private static int num = 1; 
    private int incrementalId;
    private Date creationDate;
    private Designer designer = new Designer("inconnu");
    private String status;
    private String testType;
    private List steps = new ArrayList();
    private List requirements = new ArrayList();
    private List params = new ArrayList();
        
    public Test(){
        incrementalId= num++;
    }
        
    public Test(TestSuite suite){
        this.suite = suite; 
    }
                
    public int getIncrementalId() {
        return incrementalId;
    }

    public Designer getDesigner() {
        return designer;
    }
        
    public void setDesigner(Designer designer) {
        this.designer = designer;
    }
        
    public Date getCreationDate() {
        return creationDate;
    }
        
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
        
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType){
        this.testType = testType;
    }

    public List getSteps() {
        return steps;
    }

    public void setSteps(List steps){
        this.steps = steps;
    }
        
    public void addStep(Step step){
        if (!steps.contains(step))
            this.steps.add(step);
    }

    public List getRequirements() {
        return requirements;
    }
        
    public void setRequirements(List requirements) {
        this.requirements = requirements;
    }

    public void addRequirement(IRequirement req) {
        if (!requirements.contains(req))
            this.requirements.add(req);
    }

    public List getParams() {
        return params;
    }

    public void setParams(List params) {
        this.params = params;
        for (int i = 0; i < params.size(); i++){
            if (!allParams.contains(params.get(i)))
                Test.allParams.add(params.get(i));
        }
    }
        
    public void addParams(List params) {                
        for (int i = 0; i < params.size(); i++){;
            if (!this.params.contains(params.get(i))){
                this.params.add(params.get(i));
                if (!allParams.contains(params.get(i)))
                    Test.allParams.add(params.get(i));
            }
        }
    }
        
    /**
     * retrouve l'index d'un parametre dans la liste des parametres appartenant aux tests
     * @param param parametre concerne
     * @return
     */
    public static int getParamId(String param){
        return allParams.indexOf(param) + 1;
    }

    public static List getAllParams() {
        return allParams;
    }   
        
    /**
     * retourne true des tests possedent des parametres
     * @return
     */
    public static boolean hasParameters(){
        return allParams.size() > 0;
    }
        
    @Override
    public String getAbsolutePath(){
        if (suite != null && suite.getFamily() != null)
            return suite.getAbsolutePath() + "\\" + name;
        else
            return "Default\\" + name;
    }
        
    public String getPath(){
        if (suite != null && suite.getFamily() != null)
            return suite.getAbsolutePath();
        else
            return "Default";
    }
        
    @Override
    public String toString(){
        String string = name;
        //              for (int i = 0; i < attachments.size(); i++){
        //                      string += "\n++" + ((FileAttachment)attachments.get(i)).getName();
        //              }
        //              for (int i = 0; i < steps.size(); i++)
        //                      string += steps.get(i).toString();
        return string;
    }
}
