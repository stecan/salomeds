/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.data;

import java.util.ArrayList;
import java.util.List;

public class RequirementFamily extends CommunData implements IRequirement{
        
    public static final int DEFAULT_PRIORITY = 100;
    public static final int DEFAULT_CATEGORY = 0;
    public static final int DEFAULT_COMPLEXITY = 100;
    public static final int DEFAULT_STATE = 0;
    public static final String DEFAULT_ORIGIN = "Marketing";
    public static final String DEFAULT_VERIFWAY = "";
    public static final String DEFAULT_VERSION = "-";
        
    private List reqsFams = new ArrayList();
    private List reqs = new ArrayList();
    private RequirementFamily parent;
    private String path;
        
    public RequirementFamily(){
        super();
    }
        
    public RequirementFamily(RequirementFamily parent){
        super();
        if (parent != null && !reqsFams.contains(parent)){
            this.parent = parent;
            parent.addReqFamily(this);
        }               
    }
        
    @Override
    public RequirementFamily getParent() {
        return parent;
    }

    @Override
    public void setParent(RequirementFamily parent) {
        this.parent = parent;
        path = null;
    }

    public List getReqs() {
        return reqs;
    }
        
    public void addReq(IRequirement req) {
        if (!reqs.contains(req))
            this.reqs.add(req);
    }
        
    public List getReqsFamilies() {
        return reqsFams;
    }
                        
    public void addReqFamily(RequirementFamily reqFam) {
        if (!reqsFams.contains(reqFam))
            this.reqsFams.add(reqFam);
    }   
        
    public int order(){
        int intDegree = 0;
        RequirementFamily fam = parent;
        while(fam != null){
            intDegree++;
            fam = fam.parent;
        }
        return intDegree;
    }
        
    @Override
    public String toString(){
        String string = id + "  ---------" + name;
        for (int i = 0; i < reqs.size(); i++)
            string += "\n" + reqs.get(i).toString();
        for (int i = 0; i < reqsFams.size(); i++)
            string += "\n" + reqsFams.get(i).toString();
        return name;
    }
        
    @Override
    public String getAbsolutePath() {
        String absolutePath;
        if (path == null){
            RequirementFamily current = parent;
            absolutePath = name;
            while(current != null){
                absolutePath = current.getName() + "\\" + path;
                current = current.getParent();
            }                   
        }
        else if (parent != null)
            absolutePath = path + "\\" + name;
        else
            absolutePath = name;
        return absolutePath;
    }
        

    @Override
    public String getPath() {
        if (path == null){
            if (parent != null){
                RequirementFamily current = parent.getParent();
                path = parent.getName();
                while(current != null){
                    path = current.getName() + "\\" + path;
                    current = current.getParent();
                }
            }
            else
                path = "";
        }
        return path;
    }
}
