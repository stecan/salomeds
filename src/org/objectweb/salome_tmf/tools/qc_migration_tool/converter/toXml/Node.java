/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.CommunData;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Step;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Test;

public class Node{
                
    private static int num = 1;
    private int id = 0;
    private List underFolders = new ArrayList();;
    private List leaves = new ArrayList();;
    private CommunData elmt;
    private Node parent;
    private int deep = 0;
        
    /**
     * 
     * @param elmt donnee caracterisant le noeud
     * @param parent parent du noeud
     */
    public Node(CommunData elmt, Node parent){
        this.elmt = elmt;
        this.parent = parent;
        this.id = num++;
        if (parent != null)
            parent.addUnderFolders(this);               
    }
                
    public int getId() {
        return id;
    }
        
    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {                
        if (this.parent != null)
            this.parent.getUnderFolders().remove(this);
        if (parent != null)
            parent.addUnderFolders(this);               
        this.parent = parent;
    }

    public List getUnderFolders(){
        return underFolders;
    }
        
    public void setUnderFolders(List underFolders) {
        this.underFolders = underFolders;
        deep = 0;
    }

    public void addUnderFolders(Node child) {
        underFolders.add(child);
        deep = 0;
    }
        
    public void rmUnderFolders(Node child) {
        underFolders.remove(child);
        deep = 0;
    }
        
    public List getLeaves() {
        return leaves;
    }

    public void setLeaves(List leaves) {
        this.leaves = leaves;
    }

    public void addLeave(Test leaf) {
        this.leaves.add(leaf);
    }
        
    /**
     * retourne la profondeur du noeud
     * @return -1 si la profondeur est superieur a deux 2 
     *                  le nombre de noeuds sinon 
     */
    public int getDeep(){
        if (deep == 0){
            deep = 1;
            int i = 0;
            while (i < underFolders.size() && deep != -1){
                int s = ((Node)underFolders.get(i)).getDeep();                  
                if (s == -1 )
                    deep = s;   
                else if (deep <= s)
                    deep += s;
                i++;
            }
            if(deep == 1 && leaves.size() != 0)
                deep += 1;
            if (deep > 2)
                deep = -1;      
        }
        return deep;
    }
        
    /**
     * retourne le nombre de noeuds fils de la racine
     */
    public int getWidth(){
        return underFolders.size();
    }
        
    public CommunData getElement() {
        return elmt;
    }
        
    public void setElement(CommunData elmt) {
        this.elmt = elmt;
    }   
        
    /**
     * retourne la concatenation du nom de l'element ainsi que le celui des sous-noeuds et des tests que ce noeud contient
     */
    @Override
    public String toString(){
        String string = "" + elmt.toString();
        for (int i = 0; i < leaves.size(); i++){
            string += " \n "+leaves.get(i).toString();
        }
        for (int i = 0; i < underFolders.size(); i++){
            string += "\n*";
            Node avt = parent;
            while (avt != null){
                string += "*";
                avt = avt.parent;
            }
            //string += ((Node)underFolders.get(i)).getElement().getName();
            string += underFolders.get(i).toString();
        }
        return string;
    }
        
        
    //  public String toString(){
    //  String string = getPath();
    //  for (int i = 0; i < underFolders.size(); i++){
    //          string += "\n*";
    //          Node avt = parent;
    //          while (avt != null){
    //                  string += "*";
    //                  avt = avt.parent;
    //          }
    //          string += underFolders.get(i).toString();
    //  }
    //  return string;
    //}
        
    //  public String toString(){
    //          String string = elmt.getName();
    //          List attachs = elmt.getAttachments();
    //          for (int i = 0; i < attachs.size(); i++){
    //                  string += "\n+++" + ((IAttachment)attachs.get(i)).getLocation();
    //          }
    //          for (int i = 0; i < underFolders.size(); i++){
    //                  if (((Node)underFolders.get(i)).getElement().getAttachments().size() > 0){
    //                          string += "\n*";
    //                          Node avt = parent;
    //                          while (avt != null){
    //                                  string += "*";
    //                                  avt = avt.parent;
    //                          }
    //                          string += underFolders.get(i).toString();
    //                  }
    //          }
    //          return string;
    //  }
        
        
    /**
     * recherche le parent d'un autre noeud
     * @param nodePath le chemin (non absolu ie ne contient le nom du noeud) du noeud dont le parent est recherche 
     * @return le parent recherche, null si le parent est introuvable
     */
    public Node findAnotherNodeParent(String nodePath){ 
        Node nodeParent = null;
        if (getPath().equals(nodePath))
            nodeParent = parent;
        else if (parent != null){
            if (nodePath.equals(getPath() + File.separator + elmt.getName()))
                nodeParent = this;
            else
                nodeParent = parent.findAnotherNodeParent(nodePath);
        }
        else if (nodePath.equals(elmt.getName()))
            nodeParent = this;
        return nodeParent;
    }

    /**
     * @return le chemin (non absolu ie ne contient le nom du noeud) du noeud
     *                  <br> retourne une chaine vide si le noeud n'a pas de parent
     */
    public String getPath(){
        String chaine = "";
        Node current = parent;
        while (current != null){
            if (chaine.equals(""))
                chaine = current.getElement().getName();
            else
                chaine = current.getElement().getName() + File.separator + chaine;
            current = current.parent;
        }
        return chaine;
    }

    /**
     * @return le chemin absolu du noeud
     */
    public String getIdAbsolutePath(){
        String chaine = "" + id;
        Node current = parent;
        while (current != null){
            chaine = current.getId() + File.separator + chaine;
            current = current.parent;
        }
        return chaine;
    }
        
    /**  
     * applique <b><la regle des familles></b> au noeud
     * @param prj le projet auquel le noeud appartient
     * @return retourne une liste d'arbre resultant de transformation de l'arborescence. 
     */
    public List modifyFamily(Project prj){
        List altered = new ArrayList();      
        //on recupere les arbres deja construits
        for (int i = 0; i < underFolders.size(); i++){
            Node child = (Node)underFolders.get(i);
            altered.addAll(child.modifyFamily(prj));
        }
        //reconstitution d'un nouvel arbre allant des feuilles jusqu'au root 
        //chaque noeud, hormis celui contenant les feuilles, n'a qu'un fils
        if (leaves.size() > 0 || getWidth() == 0)
            if (parent != null){
                Node current = this.parent;
                CommunData e_family = new CommunData();
                e_family.setName(parent.elmt.getName());
                if (parent.elmt.getDescription() != null)
                    e_family.setDescription(parent.elmt.getName() + " : \\n" + parent.elmt.getDescription());
                else
                    e_family.setDescription("");
                e_family.addAttachment(parent.elmt.moveAttachments());
                while(current.parent != null){
                    current = current.getParent();
                    e_family.completeWith(current.elmt);                        
                }
                if (e_family.getDescription().equals("")) 
                    e_family.setDescription(null);
                Node root = new Node (e_family, null);
                Node underRoot = new Node(elmt, root);
                underRoot.setLeaves(leaves);
                try{
                    altered.add(new Tree(root, prj));                           
                }catch(Exception e){}                   
            }
            else{
                CommunData nvElmt = new CommunData();
                nvElmt.setName(Project.DEFAULT_FAMILY_NAME);
                Node root = new Node (nvElmt, null);
                Node underNode = new Node (elmt, root);
                underNode.setLeaves(leaves);
                try{
                    altered.add(new Tree(root, prj));                           
                }catch(Exception e){
                    Util.err(e);
                }
            }
        return altered;         
    }   
        
    /**  
     * applique <b><la regle des suites de test></b> au noeud
     * @return retourne une liste d'arbre resultant de transformation de l'arborescence. 
     */
    public List modifyTestList(){
        List altered = new ArrayList();       
        //on recupere les arbres deja construits
        for (int i = 0; i < underFolders.size(); i++){
            Node child = (Node)underFolders.get(i);
            altered.addAll(child.modifyTestList());
        }
                
        //constitution des informations associees
        if (leaves.size() > 0 || getWidth() == 0){
            Node current = this;
            CommunData e_family = new CommunData();
            e_family.setName(elmt.getName());
            if (elmt.getDescription() != null)
                e_family.setDescription(elmt.getName() + " : \\n" + elmt.getDescription());
            else
                e_family.setDescription("");
            e_family.addAttachment(elmt.moveAttachments());
            while(current.parent.parent != null){
                current = current.getParent();
                e_family.completeWith(current.elmt);    
            }
            if (e_family.getDescription().equals(""))
                e_family.setDescription(null);
            Node root = new Node (e_family, null);
            root.setLeaves(leaves);
            altered.add(root);  
        }
        return altered;         
    }
        
    /**  
     * applique <b><la regle des tests></b> au noeud
     */
    public void modifyTest(){     
        //on recupere les arbres deja construits
        for (int i = 0; i < underFolders.size(); i++){
            Node child = (Node)underFolders.get(i);
            child.modifyTest();
            if (!underFolders.contains(child)) //l'element a ete soustraite de l'ensemble
                i--;
        }
        //constitution des informations associees
        if (leaves.size() > 0 && parent.parent != null ){                       
            Node current = this;
            CommunData e_Test = new CommunData();
            e_Test.setName(elmt.getName());
            if (elmt.getDescription() != null)
                e_Test.setDescription(elmt.getName() + " : \\n" + elmt.getDescription());
            else
                e_Test.setDescription("");
            e_Test.addAttachment(elmt.moveAttachments());
            while(current.parent.parent.parent != null){
                current = current.parent;
                e_Test.completeWith(current.elmt);
            }
            if (e_Test.getDescription().equals(""))
                e_Test.setDescription(null);
            parent.getUnderFolders().remove(this);
            for (int j = 0; j < leaves.size(); j++){
                Test t = (Test)leaves.get(j);
                t.setName(e_Test.getName() + Project.getNameSeparator()  + t.getName());
                current.parent.addLeave(t);
                current.parent.elmt.addAttachment(t.moveAttachments());
            }
            current = parent;
            while (current.getWidth() == 0 && current.getLeaves().size() == 0){
                current.parent.getUnderFolders().remove(current);
                current = current.parent;
            }
        }
    }
        
    /**
     * supprime le noeud s'il ne possede qu'un seul sous-noeud et aucun test et s'il n'est pas la racine d'un arbre
     */
    public void delUnusedNode(){
        if (getWidth() == 1 && leaves.size() == 0 && parent != null){
            Node noeud = (Node)underFolders.get(0);
            noeud.setParent(parent);                    
            parent.elmt.addAttachment(elmt.moveAttachments());
            parent.rmUnderFolders(this);                                
        }
        for (int i = 0; i < underFolders.size(); i++){
            Node n = (Node)underFolders.get(i);
            n.delUnusedNode();
            if (!underFolders.contains(n))
                i--;
        }
    }
        
    /**
     * supprime les tests automatiques du noeud
     *
     */
    public void removeTestAuto() {
        for (int i = 0; i < leaves.size(); i++){
            Test theTest = (Test)leaves.get(i);
            if (theTest.getTestType() != null && theTest.getTestType().equals("auto")){
                leaves.remove(theTest);
                i--;
            }
        }
        for (int i = 0; i < underFolders.size(); i++)
            ((Node)underFolders.get(i)).removeTestAuto();
                                
    }
        
    /**
     * transforme le noeud en une famille par defaut et lui ajoute un sous-noeud qui contiendra ses tests et ses attachements
     */
    public void addDefaultFamily() {
        CommunData nvElmt = new CommunData();
        nvElmt.setId(elmt.getId());
        nvElmt.setName(elmt.getName());
        nvElmt.setDescription(elmt.getDescription());
        nvElmt.setAttachments(elmt.getAttachments());
        Node node = new Node(nvElmt, this);
        node.getLeaves().addAll(leaves);
        elmt.setName(Project.DEFAULT_FAMILY_NAME);
        elmt.setDescription(null);
        elmt.getAttachments().clear();
        leaves.clear();                         
    }
        
    /**
     * ajoute un sous-noeud qui est une suite par defaut, et affecte a celui-ci tous les tests du noeud
     */
    public void addDefaultTestSuite() {
        CommunData nvElmt = new CommunData();
        nvElmt.setName(Project.DEFAULT_TESTSUITE_NAME);
        Node node = new Node(nvElmt, this);
        node.getLeaves().addAll(leaves);
        leaves.clear();
    }

    /**
     * renomme et deplace tous les attachements du noeud dans le repertoire destination des attachements du projet 
     * auquel le noeud appartient   
     * @param attachDirSrc repertoire source des attachements du project
     * @param attachDirTarget repertoire destination des attachements du project
     */
    public void moveAttachments(String attachDirSrc, String attachDirTarget){
        try{
            List attachs = elmt.getAttachments();
            boolean renamed = false;
                        
            //deplacer les attachements du noeud                        
            for (int i = 0; i < attachs.size(); i++){
                if (attachs.get(i) instanceof FileAttachment){
                    FileAttachment theAttach = (FileAttachment)attachs.get(i);
                    File directory = new File(attachDirTarget + File.separator+ getIdAbsolutePath());
                    directory.mkdirs(); //TODO
                    File attachSrc = new File(theAttach.getLocation() + File.separator + theAttach.getName());                                            
                    File attachTarget = new File(attachDirTarget + File.separator + getIdAbsolutePath() + File.separator + theAttach.getName());
                    attachSrc.renameTo(attachTarget);   
                    theAttach.setLocation(Project.ATTACH_DIR + File.separator + getIdAbsolutePath() + File.separator + theAttach.getName());
                }
            }
                        
            //copier les attachements de ses sous-noeuds
            for (int i = 0; i < underFolders.size(); i++)
                ((Node)underFolders.get(i)).moveAttachments(attachDirSrc, attachDirTarget);
                        
            //renommer et deplacer les attachements des tests qu'il contient
            for (int k = 0; k < leaves.size(); k++){
                Test theTest = (Test)leaves.get(k);
                List testAttachs = theTest.getAttachments();
                renamed = false;
                for (int i = 0; i < testAttachs.size(); i++){
                    if (testAttachs.get(i) instanceof FileAttachment){
                        FileAttachment theTestAttach = (FileAttachment)testAttachs.get(i);
                        if (!renamed){
                            File dirSrc = new File(theTestAttach.getLocation());
                            File[] children = dirSrc.listFiles();
                            if (children != null){
                                for (int j = 0; j < children.length; j++)
                                    if (children[j].isFile()){
                                        String fileName = children[j].getName().substring(children[j].getName().indexOf("_", Project.TEST_ATTACH_PREFIX.length())+1);                                                           
                                        File fileRenamed = new File(theTestAttach.getLocation() + File.separator + fileName);
                                        children[j].renameTo(fileRenamed);
                                    }
                                renamed = true;
                            }
                        }
                        theTestAttach.setLocation(Project.ATTACH_DIR + File.separator + getIdAbsolutePath() + File.separator + theTest.getIncrementalId() + File.separator + theTestAttach.getName());
                    }
                }
                                
                //renommer et deplacer les attachements des steps contenus dans le test
                List steps = theTest.getSteps();
                for (int step_k = 0; step_k < steps.size(); step_k++){
                    Step theStep = (Step)steps.get(step_k);
                    List stepAttachs = theStep.getAttachments();
                    renamed = false;
                    for (int i = 0; i < stepAttachs.size(); i++){
                        if (stepAttachs.get(i) instanceof FileAttachment){
                            FileAttachment theAttach = (FileAttachment)stepAttachs.get(i);
                            if (!renamed){                                                              
                                File dirSrc = new File(theAttach.getLocation());
                                File[] children = dirSrc.listFiles();                                                           
                                if (children != null){                                                                  
                                    for (int j = 0; j < children.length; j++)
                                        if (children[j].isFile()){
                                            String fileName = children[j].getName().substring(children[j].getName().indexOf("_", Project.STEP_ATTACH_PREFIX.length())+1);                                                               
                                            File fileRenamed = new File(theAttach.getLocation() + File.separator + fileName);
                                            children[j].renameTo(fileRenamed);                                                                                  ;
                                        }
                                    renamed = true;
                                }
                            }   
                            theAttach.setLocation(Project.ATTACH_DIR + File.separator + getIdAbsolutePath() + File.separator + theTest.getIncrementalId() + File.separator + theStep.getName() + File.separator + theAttach.getName());                                                 
                        }
                    }
                }
                                
                //on deplace le repertoire du test
                File directory = new File(attachDirSrc + File.separator + Project.TEST_DIR + File.separator + theTest.getId()); 
                if (directory.exists()){
                    File targetDirectory = new File(attachDirTarget + File.separator + getIdAbsolutePath());
                    targetDirectory.mkdirs();
                    directory.renameTo(new File(attachDirTarget + File.separator + getIdAbsolutePath() + File.separator + theTest.getIncrementalId()));
                }
            }
        }
        catch(Exception e){
            Util.err(e);
        }               
    }
}


