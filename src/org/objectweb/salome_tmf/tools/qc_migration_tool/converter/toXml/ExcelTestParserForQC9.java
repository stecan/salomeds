/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.ParserUtil;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.CommunData;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Designer;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IRequirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Step;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Test;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.UrlAttachment;


public class ExcelTestParserForQC9 implements IExcelTestParser{
        
    private static final String DEFAULT_TEST_NAME = "Test Name";
    private static final String DEFAULT_TEST_CREATION_DATE = "Creation Date";
    private static final String DEFAULT_TEST_DESIGNER = "Designer";
    private static final String DEFAULT_TEST_ID = "Test";
    private static final String DEFAULT_TEST_TYPE = "Type";
    private static final String DEFAULT_TEST_DESCRIPTION = "Description :";
    private static final String DEFAULT_STEP_TITLE = "Design Steps :";
    private static final String DEFAULT_STEP_DESCRIPTION = "Description :";
    private static final String DEFAULT_STEP_RESULT = "Expected Result :";      
    private static final String DEFAULT_ATTACH_TITLE = "Attachments :";
    private static final String DEFAULT_ATTACH_TITLE_NODE_V9 = "Attachments:";
    private static final String DEFAULT_ATTACH_DESCRIP = "Description";
    private static final String DEFAULT_COVERAGE_TITLE = "Covered Requirements :";
    private static final String DEFAULT_COVERAGE_REQ_ID = "ReqID";      
          
    //noms des labels contenus dans le document excel et utilisees dans le programme
    private static String TEST_NAME;
    private static String TEST_CREATION_DATE;
    private static String TEST_DESIGNER;
    private static String TEST_ID;
    private static String TEST_TYPE;
    private static String TEST_DESCRIPTION;
    private static String STEP_TITLE;
    private static String STEP_DESCRIPTION;
    private static String STEP_RESULT;
    private static String ATTACH_TITLE_NODE_V9;
    private static String ATTACH_TITLE;
    private static String ATTACH_DESCRIP;
    private static String COVERAGE_TITLE;
    private static String COVERAGE_REQ_ID;
        
    private static final short SUBJECT = 1;     
    private static final short TEST_DETAILS = 2;
    private static final short TEST_DECRIP = 3;
    private static final short COVERAGE = 4;
    private static final short STEP = 5;
    private static final short STEP_NAM = 6;
    private static final short STEP_DESCRIP = 7;
    private static final short STEP_RESULT_EXPECTED = 8;        
    private static final short ATTACH_FILE = 9;
    private static final short ATTACH_URL = 10;
    private static final short SUBJECT_ATTACH = 11;
    private static final short TEST_ATTACH = 12;
    private static final short STEP_ATTACH = 13;
        
    Project project;
        
    //elements de l'arbre contruit
    private List trees = new ArrayList();
    private Tree currentTree;
    private Node currentNode;
    private Test test;
    private Step step;
    private IAttachment att;
                
    //repertoire source contenant les attachements des familles
    private static final String FIRST_DIRECTORY = "Subject";
        
    //liens avec les exigences
    private Hashtable reqLinkHashtable;
        
    //relatif aux cellules du document excel traite
    private static final int MAXROW = 1000000; 
    private SSTRecord sstRecord;
    private LabelSSTRecord labelSSTRecord;
    private String cellContent;
    private String testPerhaps;
    private String cellContentBefore;
    private Hashtable labelHashtable = new Hashtable();
    private int stepFirtLine = MAXROW;
    private int reqLinkColumn;
    
    //flags
    private short position = 0; //indicateur de parties
    private int ss_position = 0; // sous-partie d'un type d'informations traite

    /**
     * @param prj projet utilisant le parseur
     * @throws LabelNotFoundException leve l'exception quand un label utilise dans le parsing reste introuvable dans le fichier de configuration
     */
    public ExcelTestParserForQC9(Project prj) throws LabelNotFoundException{
        this.project = prj;
        initQCLabels();
    }
                
    /**
     * initialise les labels a partir du fichier de configuration
     * @throws LabelNotFoundException leve l'exception quand un label utilise dans le parsing reste introuvable dans le fichier de configuration
     */
    private void initQCLabels() throws LabelNotFoundException{
        try {
            String labelsUnfound = "";
            String separator = ", ";                    
            FileInputStream  in = new FileInputStream(Project.CONFIG_FILE);             
            Properties prop = new Properties(); 
            prop.load(in);
            in.close(); 
            TEST_NAME = prop.getProperty("TEST_NAME");
            TEST_CREATION_DATE = prop.getProperty("TEST_CREATION_DATE");
            TEST_DESIGNER = prop.getProperty("TEST_DESIGNER");
            TEST_ID = prop.getProperty("TEST_ID");
            TEST_TYPE = prop.getProperty("TEST_TYPE");
            TEST_DESCRIPTION = prop.getProperty("TEST_DESCRIPTION");
                        
            STEP_TITLE = prop.getProperty("STEP_TITLE");
            STEP_DESCRIPTION = prop.getProperty("STEP_DESCRIPTION");
            STEP_RESULT = prop.getProperty("STEP_RESULT");      
                        
            ATTACH_TITLE = prop.getProperty("ATTACH_TITLE");
            ATTACH_TITLE_NODE_V9 = prop.getProperty("ATTACH_TITLE_NODE_V9");
            ATTACH_DESCRIP = prop.getProperty("ATTACH_DESCRIP");
                        
            COVERAGE_TITLE = prop.getProperty("COVERAGE_TITLE");
            COVERAGE_REQ_ID = prop.getProperty("COVERAGE_REQ_ID");                      

            if (TEST_NAME == null)
                labelsUnfound += "TEST_NAME" + separator;
            if (TEST_CREATION_DATE == null)
                labelsUnfound += "TEST_CREATION_DATE" + separator;
            if (TEST_DESIGNER == null)
                labelsUnfound += "TEST_DESIGNER" + separator;
            if (TEST_ID == null)
                labelsUnfound += "TEST_ID" + separator;
            if (TEST_TYPE == null)
                labelsUnfound += "TEST_TYPE" + separator;
            if (TEST_DESCRIPTION == null)
                labelsUnfound += "TEST_DESCRIPTION" + separator;
            if (STEP_TITLE == null)
                labelsUnfound += "STEP_TITLE" + separator;
            if (STEP_DESCRIPTION == null)
                labelsUnfound += "STEP_DESCRIPTION" + separator;
            if (STEP_RESULT == null)
                labelsUnfound += "STEP_RESULT" + separator;
            if (ATTACH_TITLE == null)
                labelsUnfound += "ATTACH_TITLE" + separator;
            if (ATTACH_TITLE_NODE_V9 == null)
                labelsUnfound += "ATTACH_TITLE_NODE_V9" + separator;
            if (ATTACH_DESCRIP == null)
                labelsUnfound += "ATTACH_DESCRIP" + separator;
            if (COVERAGE_TITLE == null)
                labelsUnfound += "COVERAGE_TITLE" + separator;
            if (COVERAGE_REQ_ID == null)
                labelsUnfound += "COVERAGE_REQ_ID" + separator;         
            if (!labelsUnfound.equals("")){
                labelsUnfound = labelsUnfound.substring(0, labelsUnfound.length() - separator.length());
                if (labelsUnfound.indexOf(separator) == -1)
                    labelsUnfound = "Le label " + labelsUnfound + " est introuvable dans le fichier de configuration " + Project.CONFIG_FILE;
                else
                    labelsUnfound = "Les labels " + labelsUnfound + " sont introuvables dans le fichier de configuration " + Project.CONFIG_FILE;
                throw new LabelNotFoundException(labelsUnfound);
            }
        } catch (IOException e) {
            project.writeInReport("Le fichier de configuration " + Project.CONFIG_FILE + " est introuvable\n"); 
            Util.err(e);
        }
    }
        
    /**
     * corrige le fichier de configuration config.properties en ajoutant les labels manquants
     */
    public static void useDefaultLabel(){               
        try {           
            FileInputStream in = new FileInputStream(Project.CONFIG_FILE);      
            Properties prop = new Properties(); 
            prop.load(in);
            in.close(); 
            if (prop.getProperty("TEST_NAME") == null)
                prop.setProperty("TEST_NAME", DEFAULT_TEST_NAME);       
            if (prop.getProperty("TEST_CREATION_DATE") == null)
                prop.setProperty("TEST_CREATION_DATE", DEFAULT_TEST_CREATION_DATE);
            if (prop.getProperty("TEST_DESIGNER") == null)
                prop.setProperty("TEST_DESIGNER", DEFAULT_TEST_DESIGNER);
            if (prop.getProperty("TEST_ID") == null)
                prop.setProperty("TEST_ID", DEFAULT_TEST_ID);
            if (prop.getProperty("TEST_TYPE") == null)
                prop.setProperty("TEST_TYPE", DEFAULT_TEST_TYPE);
            if (prop.getProperty("TEST_DESCRIPTION") == null)
                prop.setProperty("TEST_DESCRIPTION", DEFAULT_TEST_DESCRIPTION);
                        
            if (prop.getProperty("STEP_TITLE") == null)
                prop.setProperty("STEP_TITLE", DEFAULT_STEP_TITLE);
            if (prop.getProperty("STEP_NAME") == null)
                if (prop.getProperty("STEP_DESCRIPTION") == null)
                    prop.setProperty("STEP_DESCRIPTION", DEFAULT_STEP_DESCRIPTION);
            if (prop.getProperty("STEP_RESULT") == null)
                prop.setProperty("STEP_RESULT", DEFAULT_STEP_RESULT);
                        
            if (prop.getProperty("ATTACH_TITLE") == null)
                prop.setProperty("ATTACH_TITLE", DEFAULT_ATTACH_TITLE);
            if (prop.getProperty("ATTACH_TITLE_NODE_V9") == null)
                prop.setProperty("ATTACH_TITLE_NODE_V9", DEFAULT_ATTACH_TITLE_NODE_V9);
            if (prop.getProperty("ATTACH_DESCRIP") == null)
                prop.setProperty("ATTACH_DESCRIP", DEFAULT_ATTACH_DESCRIP);
                        
            if (prop.getProperty("COVERAGE_TITLE") == null)
                prop.setProperty("COVERAGE_TITLE", DEFAULT_COVERAGE_TITLE);
            if (prop.getProperty("COVERAGE_REQ_ID") == null)
                prop.setProperty("COVERAGE_REQ_ID", DEFAULT_COVERAGE_REQ_ID);
            FileOutputStream out = new FileOutputStream(Project.CONFIG_FILE); 
            prop.store(out, null);
            out.close();
        } catch (IOException e) {
            Util.err(e);
        }               
    }

    /**
     * @see converter.IExcelTestParser.#getTrees()
     */
    @Override
    public List getTrees(){
        return trees;
    }
        
    /**
     * @see converter.IExcelTestParser#setReqLinkHashtable(Hashtable reqLinkHashtable)()
     */
    @Override
    public void setReqLinkHashtable(Hashtable reqLinkHashtable){
        this.reqLinkHashtable = reqLinkHashtable;
    }
        
    /**
     * parse le fichier excel
     */
    @Override
    public void processRecord(Record record){           
        switch (record.getSid()){
        case NumberRecord.sid:
            NumberRecord numRecord = (NumberRecord) record;
            //reperage de la date de creation d'un test
            if ((position == TEST_DETAILS || position == COVERAGE || position == STEP ||
                 ss_position == STEP_DESCRIP || ss_position == STEP_RESULT_EXPECTED || ss_position == ATTACH_FILE || ss_position == ATTACH_URL)
                && labelHashtable.get(numRecord.getColumn()) != null && labelHashtable.get(numRecord.getColumn()).equals(TEST_CREATION_DATE)
                && (Double.toString(numRecord.getValue()).length() <= 10)) { 
                if ((test.getCreationDate() != null)){ 
                    test = new Test();                                          
                    currentNode.addLeave(test);
                    test.setName(testPerhaps);
                    position = TEST_DETAILS;  
                }//ces 2 conditions peuvent etre verifiees uniquement si la 1ere colonne des tests est un champ personnalise 
                else if(ss_position == STEP_DESCRIP && !cellContentBefore.equals("")){
                    step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));    
                }
                else if(ss_position ==STEP_RESULT_EXPECTED && !cellContentBefore.equals("")){
                    step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                }
                test.setCreationDate(HSSFDateUtil.getJavaDate(numRecord.getValue()));
                cellContentBefore = "";
            }
            else if (position == TEST_DETAILS && labelHashtable.get(numRecord.getColumn()) != null      && labelHashtable.get(numRecord.getColumn()).equals(TEST_ID)){
                test.setId((int)numRecord.getValue());
            }
            else if (position == COVERAGE && numRecord.getColumn() == reqLinkColumn && reqLinkHashtable != null && reqLinkHashtable.get(numRecord.getValue()) != null)
                test.addRequirement((IRequirement)reqLinkHashtable.get(numRecord.getValue()));
            else if ((position == STEP || position == STEP_ATTACH) && numRecord.getColumn()==0){
                if(ss_position == STEP_DESCRIP && !cellContentBefore.equals("")){
                    step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));    
                }
                else if(ss_position ==STEP_RESULT_EXPECTED && !cellContentBefore.equals("")){
                    step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                }
                step = new Step();
                test.addStep(step);
                step.setId((int)numRecord.getValue()); 
                ss_position = STEP_NAM;
                position = STEP;
            }                
            else if (ss_position == ATTACH_FILE){       
                att = new FileAttachment();
                ((FileAttachment)att).setName(testPerhaps);  
                ((FileAttachment)att).setDate(HSSFDateUtil.getJavaDate(numRecord.getValue()));          
                String nodePath = currentNode.getPath().equals("") == true ? currentNode.getElement().getName() : currentNode.getPath() + File.separator + currentNode.getElement().getName();
                if (position == SUBJECT_ATTACH){
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR + File.separator 
                                    + FIRST_DIRECTORY + File.separator + nodePath);
                    currentNode.getElement().addAttachment(att);
                }
                else if (position == TEST_ATTACH) {
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR  + File.separator
                                    + Project.TEST_DIR + File.separator + test.getId());
                    test.addAttachment(att); 
                }
                else if (position == STEP_ATTACH) {
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR  + File.separator 
                                    + Project.TEST_DIR + File.separator + test.getId() + File.separator + step.getName());
                    step.addAttachment(att);
                }
            }
            break;
        case SSTRecord.sid:
            sstRecord = (SSTRecord) record;            
            break;
        case LabelSSTRecord.sid:
            labelSSTRecord = (LabelSSTRecord) record;
            // quick an dirty to avoid compiler error (stecan, 05.02.2015)
            cellContent = TEST_NAME; // sstRecord.getString(labelSSTRecord.getSSTIndex());                
            if (labelSSTRecord.getColumn() == 0){
                if (cellContent.startsWith("Subject\\")){
                    CommunData elmt = new CommunData();
                    elmt.setName(ParserUtil.replaceDirSpecialChars(cellContent.substring(cellContent.lastIndexOf(File.separator)+1, cellContent.length())));
                    if (cellContent.lastIndexOf(File.separator) == 7){  
                        currentNode = new Node(elmt,null);
                        try{currentTree = new Tree(currentNode, project);
                            trees.add(currentTree);
                        }catch(Exception e){}
                    }
                    else{ 
                        Node currentNodeParent = currentNode.findAnotherNodeParent(ParserUtil.findPath(ParserUtil.replaceDirSpecialChars(cellContent)));
                        if (currentNodeParent != null)
                            currentNode = new Node(elmt, currentNodeParent);    
                        else 
                            project.writeInReport("Erreur fatale : Le repertoire precedent " + cellContent + " est introuvable\n");
                    }
                    if(ss_position == STEP_DESCRIP && !cellContentBefore.equals("")){
                        step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                        test.addParams(ParserUtil.getParamsInString(cellContentBefore));        
                    }
                    else if(ss_position == STEP_RESULT_EXPECTED && !cellContentBefore.equals("")){
                        step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                        test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                    }
                    else if (position== TEST_DETAILS && !cellContentBefore.equals("")){
                        test.completeDescription(cellContentBefore);
                        cellContentBefore = "";
                    }
                    position = SUBJECT;
                    ss_position = 0;
                }
                else if (cellContent.equals(TEST_NAME)){
                    if (labelHashtable.size() == 5){ 
                        test = new Test();
                        currentNode.addLeave(test);
                        position = TEST_DETAILS;
                    }
                    else //le test sera cree a la fin du remplissage du hashtable
                        labelHashtable.put(labelSSTRecord.getColumn(), TEST_NAME);                                                                                      
                }
                else if (cellContent.equals(ATTACH_TITLE)){                        
                    if (position == TEST_DETAILS){
                        position = TEST_ATTACH;
                        if (!cellContentBefore.equals("")){
                            test.completeDescription(cellContentBefore);
                            cellContentBefore = "";
                        }
                    }
                    else if (position == STEP){
                        if(ss_position == STEP_DESCRIP && !cellContentBefore.equals("")){
                            step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                            test.addParams(ParserUtil.getParamsInString(cellContentBefore));    
                        }
                        else if(ss_position == STEP_RESULT_EXPECTED && !cellContentBefore.equals("")){
                            step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                            test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                        }
                        position = STEP_ATTACH;
                        cellContentBefore = "";
                        ss_position = 0;
                    }                      
                }
                else if (cellContent.equals(ATTACH_TITLE_NODE_V9)){
                    position = SUBJECT_ATTACH;
                    ss_position = 0;
                }
                else if (cellContent.equals(COVERAGE_TITLE)){
                    position = COVERAGE;
                    ss_position = 0;
                    if (!cellContentBefore.equals("")){
                        test.completeDescription(cellContentBefore);
                        cellContentBefore = "";
                    }
                }
                else if (cellContent.equals(STEP_TITLE)){        
                    position = STEP; 
                    ss_position = 0;
                    if (!cellContentBefore.equals("")){
                        test.completeDescription(cellContentBefore);
                        cellContentBefore = "";
                    }
                }                      
            } //remplissage du hashtable contenant le nom et la colonne des entetes relatifs au details des tests
            //on recherche la premiere ligne contenant les entetes a cette fin
            else if (labelHashtable.size() < 5){
                if (cellContent.equals(TEST_NAME))
                    labelHashtable.put(labelSSTRecord.getColumn(), TEST_NAME);
                else if (cellContent.equals(TEST_CREATION_DATE))
                    labelHashtable.put(labelSSTRecord.getColumn(),TEST_CREATION_DATE);
                else if (cellContent.equals(TEST_DESIGNER))
                    labelHashtable.put(labelSSTRecord.getColumn(),TEST_DESIGNER);
                else if (cellContent.equals(TEST_ID))
                    labelHashtable.put(labelSSTRecord.getColumn(),TEST_ID);
                else if (cellContent.equals(TEST_TYPE)){
                    labelHashtable.put(labelSSTRecord.getColumn(),TEST_TYPE);
                    position = TEST_DETAILS;
                    ss_position = 0;
                    test = new Test();
                    currentNode.addLeave(test);
                }
            }//on est dans l'entete des details de test mais le premier champ n'est pas Test Name
            //confusion s'il existe un champ de nom TESTTITLE_TYPE ou ayant pour valeur TESTTITLE_TYPE dans une partie, situee dans la meme colonne
            //que le nom du test : chose tres peu probable
            else if (labelHashtable.get(labelSSTRecord.getColumn()) != null && labelHashtable.get((short)0) == null
                     && cellContent.equals(TEST_TYPE) && labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_TYPE)){
                position = TEST_DETAILS;  
                ss_position = 0;
                test = new Test();
                currentNode.addLeave(test);     
            }
            //entetes de sous-parties et leur valeur
            switch (position){
            case TEST_DETAILS : 
                parseTestDetails(); 
                break;
            case STEP : 
                parseStep();
                break;
            case SUBJECT_ATTACH:  case TEST_ATTACH : case STEP_ATTACH : 
                parseAttachment();
                break;                  
            case COVERAGE : parseCoverage();  
            }           
        }
    }

        
    /** 
     * parse les details du test
     * */
    private void parseTestDetails(){
        if (cellContent.equals(TEST_DESCRIPTION) && labelSSTRecord.getColumn() == 0){
            ss_position =TEST_DECRIP;
            stepFirtLine = MAXROW;
        }
        else if (ss_position == TEST_DECRIP && labelSSTRecord.getColumn() == 0){
            if (test.getDescription() == null)
                test.setDescription(cellContent);
            //description sur plusieurs lignes
            else {
                if (!cellContentBefore.equals(""))
                    test.completeDescription(cellContentBefore);
                cellContentBefore = cellContent;
                testPerhaps = cellContent;
            }
        }
        else if (labelHashtable.get(labelSSTRecord.getColumn()) != null){
            if (test.getTestType() != null){//c'est pas un nouveau test
                test = new Test();
                currentNode.addLeave(test);
            }                           
            if (labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_NAME) && !cellContent.equals(TEST_NAME)) {                   
                test.setName(cellContent) ;
                test.setStatus("False");
            }   
            else if (labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_DESIGNER) && !cellContent.equals(TEST_DESIGNER)){               
                test.setDesigner(new Designer(cellContent));
            }
            else if (labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_TYPE) && !cellContent.equals(TEST_TYPE))
                test.setTestType(cellContent.equals("MANUAL") == true ? "manuel" : "auto");                             
            else
                testPerhaps = cellContent;
        }
    }
        
        
    /**
     * parse les informations sur les actions
     */
    private void parseStep(){
        if (cellContent.equals(STEP_DESCRIPTION) && labelSSTRecord.getColumn() == 0)
            ss_position =STEP_DESCRIP;
        else if (cellContent.equals(STEP_RESULT) && labelSSTRecord.getColumn() == 0)
            ss_position =STEP_RESULT_EXPECTED;
        else if (ss_position == STEP_NAM) { //l'affectation de ss_position = STEP_NAM a ete faite apres l'affectation de l'ID du step
            step.setName(ParserUtil.replaceStepSpecialChars(cellContent));      
            stepFirtLine = labelSSTRecord.getRow();     
            ss_position = 0;
        }
        else if(labelSSTRecord.getRow() > stepFirtLine) {
            if (ss_position == STEP_DESCRIP && labelSSTRecord.getColumn() == 0){
                if (step.getDescription() == null){
                    step.setDescription(ParserUtil.formatParamsFromQC(cellContent));
                    test.addParams(ParserUtil.getParamsInString(cellContent));  
                }
                else{
                    if (!cellContentBefore.equals("")){
                        step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                        test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                    }
                    cellContentBefore = cellContent;
                    testPerhaps = cellContent;
                }
            }
            else if (ss_position == STEP_RESULT_EXPECTED && labelSSTRecord.getColumn() == 0){
                if (step.getResult() == null){
                    step.setResult(ParserUtil.formatParamsFromQC(cellContent));
                    test.addParams(ParserUtil.getParamsInString(cellContent));
                    if(!cellContentBefore.equals("")){
                        step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                        test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                        cellContentBefore = "";
                    }                                           
                }
                else{ 
                    if (!cellContentBefore.equals("")){
                        step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                        test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                    }
                    testPerhaps = cellContent;
                    cellContentBefore = cellContent;
                }                                       
            }
            //partie gerant les champs personnalises du test
            else{
                if(ss_position == STEP_DESCRIP && !cellContentBefore.equals("")){
                    step.completeDescription(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));    
                    cellContentBefore = "";
                }
                else if(ss_position == STEP_RESULT_EXPECTED && !cellContentBefore.equals("")){
                    step.completeResult(ParserUtil.formatParamsFromQC(cellContentBefore));
                    test.addParams(ParserUtil.getParamsInString(cellContentBefore));
                    cellContentBefore = "";
                }
                position = TEST_DETAILS;
                test = new Test();
                currentNode.addLeave(test);
                //si le nom du step est le premier champ de la ligne, son affectation aura lieu en meme temps que celle de la date de creation
                //sinon si elle est la seconde de la ligne, l'affection a lieu ci-apres
                //sinon elle a lieu dans parseTestDetails
                if (labelHashtable.get(labelSSTRecord.getColumn()) != null && labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_NAME))                                 
                    test.setName(cellContent);
                test.setStatus("False");
            }                           
        }                       
    }
 
        
    /**
     * parse les attachements de repertoire, de test et d'action 
     */
    private void parseAttachment() {
        if (labelSSTRecord.getColumn() == 0){
            if (ParserUtil.isUrl(cellContent)){
                att = new UrlAttachment();
                att.setLocation(cellContent);
                ss_position = ATTACH_URL; 
                if (position == SUBJECT_ATTACH)
                    currentNode.getElement().addAttachment(att);
                else if (position == TEST_ATTACH)
                    test.addAttachment(att);
                else if (position == STEP_ATTACH)
                    step.addAttachment(att);
            }
            else{
                att =null;
                testPerhaps = cellContent;
                ss_position = ATTACH_FILE;
            }   
        }       
        else if (ss_position == ATTACH_URL && labelSSTRecord.getColumn() == 2){
            if (att.getDescription() == null){  
                att.setDescription(cellContent);  
            }
            else
                ((UrlAttachment)att).completeDescription(cellContent);
        }   
        else if (ss_position == ATTACH_FILE && labelSSTRecord.getColumn() == 2 && !cellContent.equals(ATTACH_DESCRIP)){ 
            if (att == null ){
                att = new FileAttachment();
                ((FileAttachment)att).setName(testPerhaps);
                String nodePath = currentNode.getPath().equals("") == true ? currentNode.getElement().getName() : currentNode.getPath() + File.separator + currentNode.getElement().getName();                          
                if (position == SUBJECT_ATTACH){
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR + File.separator + FIRST_DIRECTORY +  File.separator + nodePath );
                    currentNode.getElement().addAttachment(att);
                }
                else if (position == TEST_ATTACH) {
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR + File.separator + Project.TEST_DIR + File.separator + test.getId());
                    test.addAttachment(att); 
                }
                else if (position == STEP_ATTACH) {
                    att.setLocation(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR + File.separator + Project.TEST_DIR + File.separator + test.getId() + File.separator + step.getName());
                    step.addAttachment(att);
                }
            }   
            if (att.getDescription() == null){  
                att.setDescription(cellContent);  
            }
            else
                ((FileAttachment)att).completeDescription(cellContent);
        }                               
        else if (labelHashtable.get(labelSSTRecord.getColumn()) != null && labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_NAME)){
            testPerhaps = cellContent;          
        }
    }
    
    /**
     * parse les liens avec les exigences convertes
     */
    private void parseCoverage() {      
        if (cellContent.equals(COVERAGE_REQ_ID))
            reqLinkColumn = labelSSTRecord.getColumn();
        else if (labelHashtable.get(labelSSTRecord.getColumn()) != null && labelHashtable.get(labelSSTRecord.getColumn()).equals(TEST_NAME))
            testPerhaps = cellContent;  
    }  
        
}
