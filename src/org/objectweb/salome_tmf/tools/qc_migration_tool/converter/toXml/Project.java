/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.UrlAttachment;

public class Project {

    public static final short VERSION8 = 8;
    public static final short VERSION9 = 9;     
    public static final String SUBJECT_DIRECTORY = "Subject";
    public static final String ATTACH_DIR = "Attachements";
    public static final String REQ_DIR = "Exigences";
    public static final String TEST_DIR = "Tests";
    public static final String DIR_ATTACH_PREFIX = "ALL_LISTS_";
    public static final String REQ_ATTACH_PREFIX = "REQ_";
    public static final String DEFAULT_FAMILY_NAME = "Famille par defaut";
    public static final String DEFAULT_TESTSUITE_NAME = "Suite par defaut";
    public static final String TEST_ATTACH_PREFIX = "TEST_";
    public static final String STEP_ATTACH_PREFIX = "DESSTEPS_";
    public static final String CONFIG_FILE = "." + File.separator + "cfg" + File.separator + "config.properties";
  
    private static String nameSeparator = "\\";
    private static String descripSepartor = "-----------------------------------------------------";
    private boolean fileRenamed = false;
    private String srcDirectory;
    private String targetDirectory;
    private List attachments = new ArrayList();
    private List families = new ArrayList();
    private Hashtable requirements = new Hashtable();
    private FileOutputStream reportFile;
    private short version;
    private ExcelReqParser reqParser;
    private IExcelTestParser testParser;
    private String reportName = "";
        
    /**
     * @param srcDirectory nom du repertoire source contenant tous les fichiers de tests, le repertoire contenant les exigences 
     * et le repertoire contenant les attachements
     * @param targetDirectory repertoire cible qui contiendra le fichier excel, le repertoire des attachements ainsi que le rapport de la conversion
     * @param version la version de quality center ayant generee les fichiers excel
     * @throws Exception leve une exception si la version est differente de 8 ou 9 ou transmet une IOException
     */
    public Project(String srcDirectory, String targetDirectory, short version) throws Exception{
        if (version != VERSION8 && version != VERSION9)
            throw new Exception("Version invalide");
        this.srcDirectory = srcDirectory;
        this.targetDirectory = targetDirectory;
        this.version = version;                 
    }
                
    public static String getDescripSepartor() {
        return descripSepartor;
    }

    public static void setDescripSepartor(String descripSepartor) {
        Project.descripSepartor = descripSepartor;
    }

    public static String getNameSeparator() {
        return nameSeparator;
    }

    public static void setNameSeparator(String nameSeparator) {
        Project.nameSeparator = nameSeparator;
    }

    public List getAttachments() {
        return attachments;
    }
        
    /**
     * ajoute une liste d'attachements au projet
     * @param att
     */
    public void addAttachments(List att){
        for (int i = 0; i < att.size(); i++)
            if (!attachments.contains(att.get(i)))
                this.attachments.add(att.get(i));                       
    }

    public List getFamilies() {
        return families;
    }

    public Hashtable getRequirements() {
        return requirements;
    }
        
    public String getReportName(){
        return reportName;
    }
        
    public String getReportFilePath(){
        return targetDirectory + File.separator + reportName;
    }
        
    /**
     * ecrit une erreur dans le rapport d'erreurs
     * @param value
     */
    public void writeInReport(String value){    
        try {
            if (reportName.equals("")){
                File target = new File(targetDirectory);
                if (!target.exists())
                    target.mkdirs();             
                this.reportName = "rapport_SalomeTMF_QualityCenter_" +  new SimpleDateFormat("yy-MM-dd_HHmmss").format(new Date()) + ".txt";
                this.reportFile = new FileOutputStream(targetDirectory + File.separator + reportName);
            }
            reportFile.write(value.getBytes());
        } catch (IOException e) {
            Util.err(e);
        }
    }
        
    /**
     * ferme le fichier du rapport d'erreurs
     *
     */
    public void closeReportFile(){
        if (!reportName.equals(""))
            try {
                reportFile.close();
            } catch (IOException e) {
                Util.err(e);
            }
    }
        
    public String getSrcDirectory() {
        return srcDirectory;
    }

    public String getTargetDirectory() {
        return targetDirectory;
    }

    public short getVersion() {
        return version;
    }

    /**
     * remplace les informations manquantes dans le fichier de configuration config.properties par celles par defaut  
     */
    public void useDefaultLabel(){
        if (version == VERSION8)
            ExcelTestParserForQC8.useDefaultLabel();
        else
            ExcelTestParserForQC9.useDefaultLabel();
        ExcelReqParser.useDefaultLabel();
    }
        
    /**
     * parse tous les fichiers excel du projet
     * @throws LabelNotFoundException leve l'exception quand un label utilise dans le parsing reste introuvable dans le fichier de configuration
     */
    public void parseFiles() throws LabelNotFoundException{
        reqParser = new ExcelReqParser(this);
        if (version == VERSION8)
            testParser = new ExcelTestParserForQC8(this);
        else
            testParser = new ExcelTestParserForQC9(this);
                
        //parsing des exigences
        File reqDir = new File(srcDirectory + File.separator + REQ_DIR);                
        if (reqDir.exists()){
            File [] reqFiles = reqDir.listFiles();
            Arrays.sort(reqFiles);
            for(int i = 0; i < reqFiles.length; i++){   
                if (reqFiles[i].isFile() && reqFiles[i].getName().endsWith(".xls")){
                    try{
                        FileInputStream fin = new FileInputStream(reqFiles[i]);
                        POIFSFileSystem poifs = new POIFSFileSystem(fin);
                        InputStream din = poifs.createDocumentInputStream("Workbook");
                        HSSFRequest req = new HSSFRequest();            
                        HSSFEventFactory factory = new HSSFEventFactory();                             
                        req.addListenerForAllRecords(reqParser);
                        factory.processEvents(req, din);
                        fin.close();
                        din.close();
                    }catch(Exception e){
                        writeInReport("Erreur fatale : Une erreur s'est produite durant le parsing du ficher " + reqFiles[i] + "\n");
                        Util.err(e);
                    }
                }       
            }   
            requirements = reqParser.getReqsHastable();
            testParser.setReqLinkHashtable(reqParser.getReqsHastable());
        }

        //parsing des tests
        File directory = new File(srcDirectory);
        if (directory.exists()){
            File [] testFiles = directory.listFiles();
            Arrays.sort(testFiles);
            for(int i = 0; i < testFiles.length; i++){                  
                if (testFiles[i].isFile() && testFiles[i].getName().endsWith(".xls")){
                    try{
                        FileInputStream fin = new FileInputStream(testFiles[i]);                                        
                        POIFSFileSystem poifs = new POIFSFileSystem(fin);
                        InputStream din = poifs.createDocumentInputStream("Workbook");
                        HSSFRequest req = new HSSFRequest();            
                        HSSFEventFactory factory = new HSSFEventFactory();                              
                        req.addListenerForAllRecords(testParser);
                        factory.processEvents(req, din);
                        fin.close();
                        din.close();                                    
                    }catch(Exception e){
                        writeInReport("Une erreur s'est produite durant le parsing du ficher " + testFiles[i] + "\n");
                        Util.err(e);
                    }
                }
            }
            families = testParser.getTrees();
        }
    }
        
        
    /**
     *Supprime les doublons des attachements du projet 
     *Le critere de verification est l'emplacement reel des attachements dans le repertoire source
     */
    public void rmDuplicateAttachs(){
        for (int i = 0; i < attachments.size(); i++){
            IAttachment attach_i = (IAttachment)attachments.get(i);
            for (int j = i + 1; j < attachments.size(); j++){
                IAttachment attach_j = (IAttachment)attachments.get(j);
                if (attach_i instanceof UrlAttachment && attach_i.getLocation().equals(attach_j.getLocation())){
                    attachments.remove(attach_j);
                    j--;                                        
                }
                else if (attach_i instanceof FileAttachment && attach_j instanceof FileAttachment && ((FileAttachment)attach_i).getName().equals(((FileAttachment)attach_j).getName())){
                    File file_i = new File(attach_i.getLocation() + File.separator + ((FileAttachment)attach_i).getName());
                    File file_j = new File(attach_j.getLocation() + File.separator + ((FileAttachment)attach_j).getName());
                    if (file_i.length() == file_j.length()){                                            
                        attachments.remove(attach_j);
                        j--;
                        boolean b = file_j.delete();
                        if (!b)
                            writeInReport("Erreur fatale : Le fichier " + file_j.getAbsolutePath() + " n'a pas ete supprime car il est introuvable.\n");
                    }
                    else{
                        String oldName = ((FileAttachment)attach_j).getName();
                        boolean b = file_j.renameTo( new File(attach_j.getLocation() + File.separator + i + "_" + oldName));
                        if (b){
                            ((FileAttachment)attach_j).setName(i + "_" + oldName);                                              
                            writeInReport("Le fichier " + attach_j.getLocation() + File.separator + oldName + " a ete renomme en " +
                                          attach_j.getLocation() + File.separator + ((FileAttachment)attach_j).getName() + "\n");
                        }
                    }                                           
                }
            }
        }
    }
        
        
    /**
     * deplace les attachements du projet dans le repertoire cible des attachements
     *
     */
    public void moveProjetAttachs(){            
        for (int i = 0; i < attachments.size(); i++){
            if (attachments.get(i) instanceof FileAttachment){
                FileAttachment theAttach = (FileAttachment)attachments.get(i);
                File attachSrc = new File(theAttach.getLocation() + File.separator + theAttach.getName());                                                
                File attachTarget = new File(targetDirectory  + File.separator + ATTACH_DIR + File.separator + theAttach.getName());
                attachSrc.renameTo(attachTarget);       
                theAttach.setLocation(Project.ATTACH_DIR + File.separator + theAttach.getName());
            }
        }
    }
        
        
    /**
     * renomme chaque attachement de l'arborescence des exigences
     * deplace l'arborescence dans le repertoire cible des attachements  
     */
    public void renameAndMoveReqsAttachs(){
        //renommer toutes les exigences
        File reqAttachSrc = new File(srcDirectory + File.separator + ATTACH_DIR + File.separator + REQ_DIR);
        File[] reqDirs = reqAttachSrc.listFiles();
        if (reqDirs != null){
            for (int num_req =0; num_req < reqDirs.length; num_req++)
                renameReqSetAttachs(reqDirs[num_req]);
                        
            //deplacer les fichiers
            (new File(targetDirectory + File.separator + ATTACH_DIR )).mkdirs();
            File reqAttachTarget = new File(targetDirectory  + File.separator + ATTACH_DIR + File.separator + REQ_DIR);
            reqAttachSrc.renameTo(reqAttachTarget);
        }
    }
        
    /**
     * renomme les fichiers du repertoire d'attachements d'exigence ainsi que les ceux de ses sous-repertoires  
     * @param directory le repertoire d'exigence
     */
    private void renameReqSetAttachs(File directory) {
        File[] ss_req = directory.listFiles();
        for (int i =0; i < ss_req.length; i++){
            if (ss_req[i].isFile()){
                String fileName = ss_req[i].getName().substring(ss_req[i].getName().indexOf("_", REQ_ATTACH_PREFIX.length())+1);        
                String filePath = ss_req[i].getPath().substring(0, ss_req[i].getPath().lastIndexOf(File.separator));
                File fileRenamed = new File(filePath+ File.separator + fileName);
                ss_req[i].renameTo(fileRenamed);
            }
            else
                renameReqSetAttachs(ss_req[i]);                 
        }
    }
        
    /**
     * renomme tous les fichiers attaches au plan de test
     */
    public void renameSubjectAttachs(){
        if (!fileRenamed){
            renameDirAttachs(srcDirectory + File.separator + ATTACH_DIR + File.separator + SUBJECT_DIRECTORY);
            fileRenamed = true;
        }
    }

    /**
     * renomme les fichiers du repertoire d'attachements appartenant au plan de test ainsi que les fichiers de ses sous-repertoires  
     * @param attachDirSrc le repertoire
     */
    private void renameDirAttachs(String attachDirSrc){
        File dirSrc = new File(attachDirSrc);
        File[] children = dirSrc.listFiles();
        if (children != null){
            for (int j = 0; j < children.length; j++){ 
                if (children[j].isFile()){
                    String fileName = children[j].getName().substring(children[j].getName().indexOf("_", DIR_ATTACH_PREFIX.length())+1);                                                                
                    File fileRenamed = new File(attachDirSrc + File.separator + fileName);
                    children[j].renameTo(fileRenamed);                                                  
                }
                else
                    renameDirAttachs(attachDirSrc + File.separator + children[j].getName());                            
            }
        }
    }

}
