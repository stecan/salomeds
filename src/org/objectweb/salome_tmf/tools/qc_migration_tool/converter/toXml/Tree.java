/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.objectweb.salome_tmf.tools.qc_migration_tool.data.CommunData;

public class Tree {
        
    private Project project;
    private Node root;
        
    /**
     * @param root le noeud racine 
     * @param project le project qui cree l'arborescence
     * @throws Exception leve l'exception si root = null
     */
    public Tree(Node root, Project project) throws Exception{
        if (root.getParent()!= null)
            throw new Exception("Element racine incorrect");
        this.root = root;
        this.project = project;
    }

    /**
     * retourne la profondeur du noeud
     * @return -1 si la profondeur est superieur a deux 2 
     *                  le nombre de noeuds sinon 
     */
    public int getDeep() {
        return root != null ? root.getDeep() : 0;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }   
        
    public Project getProject() {
        return project;
    }

    @Override
    public String toString(){
        return root.toString();
    }   

    /**
     * applique <b><la regle des familles></b> a l'arbre
     * @return retourne une liste d'arbre resultant de transformation de l'arborescence. 
     *         <br>Chaque arbre possede au plus 3 niveaux. 
     *         <br>Une famille par defaut est ajoute a la racine de chaque arbre possedant 2 niveaux et contenant des tests.
     */
    public List modifyFamily(){
        List newtreeList = new ArrayList();
        if(getDeep() == -1){
            newtreeList = root.modifyFamily(project);
            //on relie les familles ayant le meme root 
            for (int i = 0; i < newtreeList.size()-1; i++){
                Tree tree = (Tree)newtreeList.get(i);
                CommunData tree_elmt = tree.getRoot().getElement(); 
                for (int j = i+1; j < newtreeList.size(); j++ ){
                    Tree underTree = (Tree)newtreeList.get(j); 
                    CommunData underTree_elmt = underTree.getRoot().getElement(); 
                    if (tree_elmt.equalsToElement(underTree_elmt)){
                        int k = 0;
                        while(k < underTree.root.getUnderFolders().size() && underTree.root.getUnderFolders().size() != 0){                                                     
                            ((Node)underTree.root.getUnderFolders().get(k)).setParent(tree.root);
                        }
                        newtreeList.remove(underTree);
                        j--;
                    }
                }
            }
        }
        else if (getDeep() != 0){
            root.addDefaultFamily();
            newtreeList.add(this);
        }
        //deplacer les attachements des familles dans le projet
        for (int i = 0; i < newtreeList.size(); i ++){
            project.addAttachments(((Tree)newtreeList.get(i)).root.getElement().moveAttachments());
        }
                
        //gestion des attachements
        //renommer tous les fichiers de l'arborescence des repertoires s'ils ne l'ont pas encore ete
        //supprimmer les attachements dupliques
        //construire la nouvelle arborescence
        project.renameSubjectAttachs();
        project.rmDuplicateAttachs();
        for(int i = 0; i < newtreeList.size(); i++)
            ((Tree)newtreeList.get(i)).getRoot().moveAttachments(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR, project.getTargetDirectory() + File.separator + Project.ATTACH_DIR);                  
        return newtreeList;
    }
        
    /**
     * applique <b><la regle des suites de tests></b> a l'arbre
     * <br>Une suite de test par defaut est ajoute a chaque arbre possedant 2 niveaux et contenant des tests.
     */
    public void modifyTestList(){
        if (getDeep() != 0) {
            Node alterRoot = new Node (root.getElement(), null);
            for (int i = 0; i < root.getUnderFolders().size(); i++){
                Node child = (Node) root.getUnderFolders().get(i);                              
                if (child.getDeep() != -1){ //le noeud n'est pas a modifier
                    child.setParent(alterRoot);
                    i--;
                    alterRoot.getElement().addAttachment(child.getElement().moveAttachments());
                }
                else{//modifier le noeud en lui ajoutant tous ces sous-noeuds                                   
                    List alteredChildren = child.modifyTestList();
                    for (int j = 0; j < alteredChildren.size(); j ++){
                        Node n = (Node)alteredChildren.get(j);
                        n.setParent(alterRoot);
                        alterRoot.getElement().addAttachment(n.getElement().moveAttachments());
                    }
                }
            }
            if (root.getLeaves().size() > 0){
                alterRoot.setLeaves(root.getLeaves());
                alterRoot.addDefaultTestSuite();
            }
            root = alterRoot;
                        
            //gestion des attachements
            //renommer les fichiers s'ils ne l'ont pas encore ete
            //supprimmer les attachements dupliques
            //construire la nouvelle arborescence
            project.renameSubjectAttachs();
            String report =     root.getElement().rmDuplicateAttachments();
            if (!report.equals(""))
                project.writeInReport(report);
            root.moveAttachments(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR, project.getTargetDirectory() + File.separator + Project.ATTACH_DIR);                  
        }
    }
        
    /**
     * applique <b><la regle des tests></b> a l'arbre
     * <br>Une suite de test par defaut est ajoute a chaque arbre possedant 2 niveaux et contenant des tests.
     */
    public void modifyTest(){
        if (getDeep() == -1){
            for (int i = 0; i < root.getUnderFolders().size(); i++){
                Node child = (Node) root.getUnderFolders().get(i);
                if (child.getDeep() == -1) {
                    child.modifyTest();
                }
            }
            if (root.getLeaves().size() > 0)
                root.addDefaultTestSuite();
        }
        else if (getDeep() != 0)
            root.addDefaultFamily();
                
        //gestion des attachements
        //renommer les fichiers s'ils ne l'ont pas encore ete
        //supprimmer les attachements dupliques
        //construire la nouvelle arborescence
        project.renameSubjectAttachs();
        String report ="";
        for (int i = 0; i < root.getUnderFolders().size(); i++){
            report += ((Node)root.getUnderFolders().get(i)).getElement().rmDuplicateAttachments();                      
        }
        if (!report.equals(""))
            project.writeInReport(report);
        root.moveAttachments(project.getSrcDirectory() + File.separator + Project.ATTACH_DIR, project.getTargetDirectory() + File.separator + Project.ATTACH_DIR);
    }
        
    /**
     * supprime les noeuds de l'arborescence n'ayant qu'un seul sous-noeud et aucun test
     */
    public void delUnusedNode(){
        if (root.getLeaves().size() == 0)
            if (root.getWidth() == 0)
                root = null;    
            else 
                while (root.getLeaves().size() == 0 && root.getWidth() == 1 && ((Node)root.getUnderFolders().get(0)).getDeep() == -1){
                    root = (Node)root.getUnderFolders().get(0);
                    root.setParent(null);
                }                               
        if (getDeep() == -1)
            root.delUnusedNode();
    }   
        
    /**
     * supprime les tests automatiques de l'arborescence
     */
    public void removeTestAuto(){
        root.removeTestAuto();
    }   
}
