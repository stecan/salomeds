/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Requirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.RequirementFamily;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Step;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Test;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.UrlAttachment;

public class ConverterToXml {   
                
    //nom des balises selon la dtd de salome dynamique 
    private static final String DTD_NAME = "SalomeDynamique"; 
    private static final String PROJET = "ProjetVT"; 
    private static final String NOM = "Nom"; 
    private static final String DESCRIPTION = "Description"; 
    private static final String CREATON_DATE = "Date_crea"; 
    private static final String PARAMS = "Params";
    private static final String PARAM = "Param";
    private static final String PARAM_ID = "id_param";
    private static final String PARAM_ID_PREFIXE = "Param_";
        
    private static final String ATTACHMENTS = "Attachements";
    private static final String URL_ATTACH = "UrlAttachement";
    private static final String URL = "url";
    private static final String FILE_ATTACH = "FileAttachement";
    private static final String FILE_ATTACH_DATE = "Date";
    private static final String FILE_ATTACH_DIR = "dir";
    private static final String FILE_ATTACH_NOM = "nom";
        
    private static final String REQUIREMENTS = "Requirements";
    private static final String REQ_FAMILY = "RequirementFamily";
    private static final String REQ = "Requirement";
    private static final String REQ_ID = "id_req";
    private static final String REQ_PARENT_ID = "id_req_parent";
    private static final String REQ_ID_PREFIXE = "Req_";
    private static final String REQ_VERSION = "version";
    private static final String REQ_PRIORITY = "priority";
    private static final String REQ_CATEGORY = "category";
    private static final String REQ_COMPLEXITY = "complexity";
    private static final String REQ_STATE = "state";
    private static final String REQ_ORIGIN = "origine";
    private static final String REQ_VERIFWAY = "verifway";
        
    private static final String FAMILLES = "Familles"; 
    private static final String FAMILLE = "Famille"; 
    private static final String FAM_ID = "id_famille";   
    private static final String FAM_ID_PREFIXE = "Fam_";
    private static final String SUITE_TESTS = "SuiteTests"; 
    private static final String SUITE_TEST = "SuiteTest";
    private static final String SUITE_ID = "id_suite";
    private static final String SUITE_ID_PREFIXE = "Suite_";
    private static final String TESTS = "Tests";
    private static final String TEST = "Test";
    private static final String TEST_ID = "id_test";
    private static final String TEST_ID_PREFIXE = "Test_";
    private static final String CONCEPTEUR = "Concepteur";
    private static final String EXECUTED = "Executed";
    private static final String PARAMST = "ParamsT";
    private static final String PARAMT = "ParamT";
        
    private static final String TEST_MANUEL = "TestManuel";
    private static final String ACTION_TEST = "ActionTest";
    private static final String ACTION_ID = "id_action";
    private static final String ACTION_ID_PREFIXE = "Action_";  
    private static final String RESULT_EXPECTED = "ResultAttendu";
        
    private static final String LINK_REQ = "LinkRequirement";
    private static final String REQ_REF = "RequirementRef";
    private static final String REQ_REF_ID = "ref"; 
        
    private Project project;
    private String xmlFileName; 
    private Document document;
    private int step_num;       
        
        
    /**
     * @param xmlFileName fichier xml resultant de l'operation
     * @param testFile fichier excel contenant le plan de tests
     * @param reqFile fichier excel contenant les exigences
     * @throws Exception 
     */
    public ConverterToXml(String projectSrcDir, String xmlFileName, short projectVersion) throws Exception{
        this.xmlFileName = xmlFileName;         
        String projectTargetDir = xmlFileName.substring(0, xmlFileName.lastIndexOf(File.separator));
        project = new Project(projectSrcDir, projectTargetDir, projectVersion);
    }
        
    /**
     * retourne le projet a convertir
     * @return
     */ 
    public Project getProject() {
        return project;
    }


    /**
     * parse les fichiers excel
     * ajoute au document xml toutes les informations necessaires non liees aux familles hormis les attachements du projet
     * @throws LabelNotFoundException leve une exception si des labels manquent dans le fichier de configuration <br>config.properties</br> 
     */
    public void initConversion() throws LabelNotFoundException{
        project.parseFiles();
        step_num = 1;   
        document = DocumentHelper.createDocument();
        Element e_projet = document.addElement(DTD_NAME ).addElement(PROJET);
        e_projet.addElement(CREATON_DATE).addText(new Date().toString());
            
        //ajout des parametres du projet 
        if (Test.hasParameters()){
            Element e_params = e_projet.addElement(PARAMS);                     
            List tests_params = Test.getAllParams();
            for (int num_param = 0; num_param < tests_params.size(); num_param++){
                Element e_param = e_params.addElement(PARAM).addAttribute(PARAM_ID, PARAM_ID_PREFIXE + (num_param + 1));
                e_param.addElement(NOM).addText((String)tests_params.get(num_param));                                   
            }
        }  
                   
        //ajout des exigences
        Hashtable reqs = project.getRequirements(); 
        if (reqs.size() > 0){           
            Element e_reqAndReqSets = e_projet.addElement(REQUIREMENTS).addAttribute(REQ_ID, REQ_ID_PREFIXE + 0);
            Object[] reqCollection = reqs.values().toArray();
            for (int i = 0; i < reqCollection.length ; i++){
                //ecriture d'un requirement dans le fichier
                if (reqCollection[i] instanceof Requirement){
                    Requirement req = (Requirement) reqCollection[i] ;
                    if (req.getParent() == null)
                        addRequirement(e_reqAndReqSets, req);                           
                }
                else{
                    RequirementFamily reqFam = (RequirementFamily)reqCollection[i] ;
                    if (reqFam.getParent() == null)
                        addReqFamily(e_reqAndReqSets, reqFam);
                }                               
            }
        }
    }
                
    /**
     * convertit le plan de tests avec <b><la regle des familles></b>
     * Chaque sous-repertoire de <b>Subject</b> est la racine d'un arbre 
     * Pour chaque arbre, on considere le dernier noeud de chaque branche comme une suite de tests ; les noeuds precedents sont concatenes en famille
     * Les attachements de la famille sont alors rattaches au projet en supprimant les doublons
     * @param removeOnceUsedNode si true, supprime les noeuds de l'arborescence n'ayant qu'un seul noeud et aucun test
     * @param removeTestAuto si true, supprime les tests automatiques
     */
    public void convertWithFamilyRule(boolean removeTestAuto, boolean removeOnceUsedNode){      
        List trees = project.getFamilies();
        Element e_project = (Element) document.selectNodes("//" + PROJET).get(0);
        Element e_familles = e_project.addElement(FAMILLES);
        for (int parsing_id = 0; parsing_id < trees.size(); parsing_id++){        
            Tree parsing_tree = (Tree) trees.get(parsing_id);
            if (removeTestAuto)
                parsing_tree.removeTestAuto();
            if (removeOnceUsedNode)
                parsing_tree.delUnusedNode();          
            if (parsing_tree.getRoot() != null){
                List family_list = parsing_tree.modifyFamily();
                for (int i = 0; i < family_list.size(); i++){
                    Tree family = (Tree) family_list.get(i);
                    addFamily(family, e_familles);                      
                }       
            }
        }  
        project.moveProjetAttachs();
        project.renameAndMoveReqsAttachs();
        if (project.getAttachments().size()> 0)
            addAttachments(e_project, project.getAttachments());
        try {
            writeFile(document);
        }catch(Exception e){
            project.writeInReport("Erreur fatale : Une erreur s'est produite durant l'ecriture du fichier xml\n");
            Util.err(e);
        }
        project.closeReportFile();
    }
        
    /**
     * convertit le plan de tests avec <b><la regle des suite de tests></b>     
     * Chaque sous-repertoire de <b>Subject</b> est la racine d'un arbre 
     * La racine de chaque arbre est famille ; chaque concatenation de noeuds allant des fils de la racine jusqu'aux feuilles constituent une suite de famille 
     * Les attachements des noeuds concatenes sont alors rattaches a la famille en supprimant les doublons
     * @param removeOnceUsedNode si True, supprime les noeuds de l'arborescence n'ayant qu'un seul noeud et aucun test
     * @param removeTestAuto si true, supprime les tests automatiques
     */
    public void convertWithTestListRule(boolean removeTestAuto , boolean removeOnceUsedNode){
        List trees = project.getFamilies();
        Element e_project = (Element) document.selectNodes("//" + PROJET).get(0);
        if (trees.size() > 0){
            Element e_familles = e_project.addElement(FAMILLES);            
            for (int parsing_id = 0; parsing_id < trees.size(); parsing_id++){    
                Tree parsing_tree = (Tree) trees.get(parsing_id);
                if (removeTestAuto)
                    parsing_tree.removeTestAuto();
                if (removeOnceUsedNode)
                    parsing_tree.delUnusedNode();
                if (parsing_tree.getRoot() != null){
                    parsing_tree.modifyTestList(); 
                    addFamily(parsing_tree, e_familles);
                }
            }           
        }
        project.renameAndMoveReqsAttachs();
        if (project.getAttachments().size()> 0)
            addAttachments(e_project, project.getAttachments());
        try{
            writeFile(document);
        }catch(Exception e){
            project.writeInReport("Erreur fatale : Une erreur s'est produite durant l'ecriture du fichier xml\n");
            Util.err(e);
        }         
        project.closeReportFile();
    }
        
    /**
     * convertit le plan de tests avec <b><la regle des suite de tests></b>     
     * Chaque sous-repertoire de <b>Subject</b> est la racine d'un arbre 
     * La racine de chaque arbre est famille ; les fils de la racine sont des suites de tests 
     * Les noeuds suivants sont concatenes aux tests 
     * Les attachements des noeuds concatenes et des tests sont alors rattaches a la suite de tests en supprimant les doublons
     * @param removeOnceUsedNode si True, supprime les noeuds de l'arborescence n'ayant qu'un seul noeud et aucun test
     * @param removeTestAuto si true, supprime les tests automatiques
     */
    public void convertWithTestRule(boolean removeTestAuto, boolean removeOnceUsedNode){        
        List trees = project.getFamilies();
        Element e_project = (Element) document.selectNodes("//" + PROJET).get(0);
        Element e_familles = e_project.addElement(FAMILLES);
        for (int parsing_id = 0; parsing_id < trees.size(); parsing_id++){        
            Tree parsing_tree = (Tree) trees.get(parsing_id);           
            if (removeTestAuto)
                parsing_tree.removeTestAuto();
            if (removeOnceUsedNode)
                parsing_tree.delUnusedNode();           
            if (parsing_tree.getRoot() != null){
                parsing_tree.modifyTest();
                addFamily(parsing_tree, e_familles);     
            }
        }               
        project.renameAndMoveReqsAttachs();
        if (project.getAttachments().size()> 0)
            addAttachments(e_project, project.getAttachments());
        try {
            writeFile(document);
        }catch(Exception e){
            project.writeInReport("Erreur fatale : Une erreur s'est produite durant l'ecriture du fichier xml\n");
            Util.err(e);
        }
        project.closeReportFile();
    }
                        
    /**
     * ecrit la couverture d'une exigence par un test dans le document xml
     * @param parent le noeud parent, en l'occurence celui du test
     * @param reqLinkChild l'exigence couverte
     */
    private void addLinkRequirement(Element parent, Requirement reqLinkChild){
        Element e_reqRef = parent.addElement(REQ_REF).addAttribute(REQ_REF_ID, REQ_ID_PREFIXE + reqLinkChild.getId());
        e_reqRef.addElement(NOM).addText(reqLinkChild.getName());
        if (reqLinkChild.getDescription() != null)
            e_reqRef.addElement(DESCRIPTION).addText(reqLinkChild.getDescription());                  
    }
        
    /**
     * ecrit la couverture d'une famille d'exigence par un test dans le document xml
     * Chaque exigence contenue dans l'arborescence dont la famille d'exigence est la racine
     * @param parent le noeud parent, en l'occurence celui du test
     * @param reqLinkChild la famille d'exigence couverte
     */
    private void addLinkReqFamily(Element parent , RequirementFamily reqLinkFam){
        List reqs = reqLinkFam.getReqs();
        for (int i = 0; i < reqs.size(); i++)
            addLinkRequirement(parent, (Requirement)reqs.get(i));
        List reqFams = reqLinkFam.getReqsFamilies();
        for (int i = 0; i < reqFams.size(); i++)
            addLinkReqFamily(parent, (RequirementFamily)reqFams.get(i));                
    }
        

    /**
     * ecrit une exigence dans le document xml
     * @param parent le noeud parent
     * @param req l'exigence a ecrire
     */
    private void addRequirement(Element parent, Requirement req){
        Element e_requirement = parent.addElement(REQ).addAttribute(REQ_PRIORITY, "" + req.getPriority())
            .addAttribute(REQ_CATEGORY,"" + req.getCategory())
            .addAttribute(REQ_COMPLEXITY, "" + req.getComplexity())
            .addAttribute(REQ_STATE, "" + req.getState())
            .addAttribute(REQ_ORIGIN, req.getOrigin())
            .addAttribute(REQ_VERIFWAY, req.getVerifway())
            .addAttribute(REQ_VERSION, req.getVersion())
            .addAttribute(REQ_ID, REQ_ID_PREFIXE + req.getId())
            .addAttribute(REQ_PARENT_ID, parent.attributeValue(REQ_ID));  
        e_requirement.addElement(NOM).addText(req.getName());
        if (req.getDescription() != null)
            e_requirement.addElement(DESCRIPTION).addText(req.getDescription());
        if (req.getAttachments().size() > 0)
            addAttachments(e_requirement, req.getAttachments());       
    }
        
    /**
     * ecrit une famille d'exigences, avec ses sous-familles et ses exigences, dans le document xml
     * @param parent le noeud parent
     * @param reqFam la famille d'exigences a ecrire
     */
    private void addReqFamily(Element parent , RequirementFamily reqFam){
        Element e_reqFam = parent.addElement(REQ_FAMILY).addAttribute(REQ_ID, REQ_ID_PREFIXE + reqFam.getId())
            .addAttribute(REQ_PARENT_ID, parent.attributeValue(REQ_ID))  ;                                                                                                                                                      
        e_reqFam.addElement(NOM).addText(reqFam.getName());
        if (reqFam.getDescription() != null)
            e_reqFam.addElement(DESCRIPTION).addText(reqFam.getDescription());
        if (reqFam.getAttachments().size() > 0)
            addAttachments(e_reqFam, reqFam.getAttachments());
        List reqs = reqFam.getReqs();
        for (int i = 0; i < reqs.size(); i++)
            addRequirement(e_reqFam, (Requirement)reqs.get(i));
        List reqFams = reqFam.getReqsFamilies();
        for (int i = 0; i < reqFams.size(); i++)
            addReqFamily(e_reqFam, (RequirementFamily)reqFams.get(i));          
    }
        

    /**
     * ajoute les attachements d'un noeud au document xml
     * @param elmt l'element concerne
     * @param attach liste des attachements
     */ 
    private void addAttachments(Element elmt, List attach) {
        Element attach_parent = elmt.addElement(ATTACHMENTS);
        for (int i = 0; i < attach.size(); i++)
            if (attach.get(i) instanceof UrlAttachment){
                UrlAttachment attUrl = (UrlAttachment) attach.get(i);
                Element e_attUrl = attach_parent.addElement(URL_ATTACH).addAttribute(URL, attUrl.getLocation());
                if (attUrl.getDescription() != null)
                    e_attUrl.addElement(DESCRIPTION).addText(attUrl.getDescription());
            }   
            else{
                FileAttachment attFile = (FileAttachment)attach.get(i);
                Element e_attFile = attach_parent.addElement(FILE_ATTACH).addAttribute(FILE_ATTACH_DIR, attFile.getLocation())
                    .addAttribute(FILE_ATTACH_NOM, attFile.getName());
                if (attFile.getDate() != null)
                    e_attFile.addElement(FILE_ATTACH_DATE).addText(attFile.getDate().toString());
                if (attFile.getDescription() != null)
                    e_attFile.addElement(DESCRIPTION).addText(attFile.getDescription());
            }           
    }
        
    /**
     * ajoute tous les elements d'une famille au document xml
     * @param family la famille concernee
     * @param e_familleParent le noeud projet du document xml
     */
    private void addFamily(Tree family, Element e_familleParent){
        Node famille = family.getRoot();
        Element e_famille = e_familleParent.addElement(FAMILLE).addAttribute(FAM_ID,FAM_ID_PREFIXE + famille.getId());
        e_famille.addElement(NOM).addText(famille.getElement().getName());
        if (famille.getElement().getDescription() != null)
            e_famille.addElement(DESCRIPTION).addText(famille.getElement().getDescription());
        if (famille.getWidth() > 0){
            Element e_testListParent = e_famille.addElement(SUITE_TESTS);
            for (int j = 0; j < famille.getWidth(); j++){
                Node testListNode = (Node)famille.getUnderFolders().get(j);
                Element e_TestList = e_testListParent.addElement(SUITE_TEST).addAttribute(SUITE_ID, SUITE_ID_PREFIXE + testListNode.getId());
                e_TestList.addElement(NOM).addText(testListNode.getElement().getName());
                if(testListNode.getElement().getDescription() != null)
                    e_TestList.addElement(DESCRIPTION).addText(testListNode.getElement().getDescription());
                if (testListNode.getLeaves().size() > 0){
                    Element e_testParent = e_TestList.addElement(TESTS);
                    for (int i = 0; i < testListNode.getLeaves().size(); i++){
                        Test test = (Test)testListNode.getLeaves().get(i);                                              
                        Element e_test = e_testParent.addElement(TEST).addAttribute(TEST_ID, TEST_ID_PREFIXE + test.getIncrementalId());
                        e_test.addElement(CONCEPTEUR).addElement(NOM).addText(test.getDesigner().getName());
                        e_test.addElement(NOM).addText(test.getName());
                        if (test.getCreationDate() != null)
                            e_test.addElement(CREATON_DATE).addText(test.getCreationDate().toString());
                        if (test.getDescription() != null)
                            e_test.addElement(DESCRIPTION).addText(test.getDescription());
                        if (test.getStatus() != null)
                            e_test.addElement(EXECUTED).addText(test.getStatus());
                                                
                        //ajout des parametres
                        List params = test.getParams();
                        if (params.size() > 0){
                            Element e_params_T = e_test.addElement(PARAMST);                    
                            for (int num_param = 0; num_param < params.size(); num_param++){                                    
                                Element e_paramT = e_params_T.addElement(PARAMT).addAttribute(REQ_REF_ID, PARAM_ID_PREFIXE + Test.getParamId((String)params.get(num_param)));
                                e_paramT.addElement(NOM).addText((String)params.get(num_param));
                            }
                        }
                                                
                        //ajout des exigences liees au test
                        List reqLinks = test.getRequirements();
                        if (reqLinks.size() > 0){
                            Element e_reqLinks = e_test.addElement(LINK_REQ);
                            for (int num_req = 0; num_req < reqLinks.size() ; num_req++){
                                if (reqLinks.get(num_req) instanceof Requirement){
                                    addLinkRequirement(e_reqLinks,(Requirement)reqLinks.get(num_req)); 
                                }
                                else{
                                    addLinkReqFamily(e_reqLinks,(RequirementFamily)reqLinks.get(num_req));
                                }
                            }
                        }
                                                
                        //ajout des steps
                        Element e_testManuel = e_test.addElement(TEST_MANUEL); 
                        if (test.getSteps().size() > 0){                                                        
                            for (int num_step = 0; num_step < test.getSteps().size(); num_step++){
                                Step step = (Step)test.getSteps().get(num_step);
                                Element e_step = e_testManuel.addElement(ACTION_TEST).addAttribute(ACTION_ID, ACTION_ID_PREFIXE +  (step.getId() == 0 ? step_num++ : step.getId()));
                                e_step.addElement(NOM).addText(step.getName());
                                if (step.getDescription() != null)
                                    e_step.addElement(DESCRIPTION).addText(step.getDescription());
                                if (step.getResult() != null)
                                    e_step.addElement(RESULT_EXPECTED).addText(step.getResult());
                                if (step.getAttachments().size() > 0)
                                    addAttachments(e_step, step.getAttachments());
                            }
                        }
                        if (test.getAttachments().size() > 0)
                            addAttachments(e_test, test.getAttachments());
                    }
                }
                if (testListNode.getElement().getAttachments().size() > 0)
                    addAttachments(e_TestList, testListNode.getElement().getAttachments());
            }
        }
        if (famille.getElement().getAttachments().size() > 0)
            addAttachments(e_famille, famille.getElement().getAttachments());
    }
                
    /**
     * ecrit le document xml dans un fichier
     * @param doc
     * @throws Exception
     */
    public void writeFile(Document doc) throws Exception{
        OutputFormat format = OutputFormat.createPrettyPrint();
        format.setEncoding("ISO-8859-1");
        XMLWriter writer = new XMLWriter( new FileWriter(xmlFileName), format );
        writer.write( doc );
        writer.close();  
    }

}
