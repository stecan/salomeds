/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toXml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.record.LabelSSTRecord;
import org.apache.poi.hssf.record.NumberRecord;
import org.apache.poi.hssf.record.Record;
import org.apache.poi.hssf.record.SSTRecord;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.ParserUtil;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Requirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.RequirementFamily;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.UrlAttachment;

public class ExcelReqParser implements HSSFListener{
        
    private static final String DEFAULT_REQ_NAME = "Name";
    private static final String DEFAULT_REQ_PRIORITY = "Priority";
    private static final String DEFAULT_REQ_ID = "ReqID";
    private static final String DEFAULT_REQ_VERSION = "Reviewed";
    private static final String DEFAULT_REQ_CATEGORY = "Category";
    private static final String DEFAULT_REQ_COMPLEXITY = "Complexity";
    private static final String DEFAULT_REQ_STATE = "State in progress";
    private static final String DEFAULT_REQ_VERIFWAY = "Method of checking";
    private static final String DEFAULT_REQ_ORIGIN = "Origin";
    private static final String DEFAULT_REQ_DESCRIPTION = "Description :";
    private static final String DEFAULT_REQ_ATTACH_TITLE = "Attachments :";
    private static final String DEFAULT_REQ_ATTACH_NAME = "Name";
    private static final String DEFAULT_REQ_ATTACH_MODIFIED = "Modified";
    private static final String DEFAULT_REQ_ATTACH_DESCRIPTION = "Description";
        
    //noms des labels contenus dans le document excel et utilisees dans le programme
    private static String REQ_NAME;
    private static String REQ_PRIORITY;
    private static String REQ_ID;
    private static String REQ_VERSION;
    private static String REQ_CATEGORY;
    private static String REQ_COMPLEXITY;
    private static String REQ_STATE;
    private static String REQ_VERIFWAY;
    private static String REQ_ORIGIN;
    private static String REQ_DESCRIPTION;
    private static String REQ_ATTACH_TITLE;
    private static String REQ_ATTACH_NAME;
    private static String REQ_ATTACH_MODIFIED;
    private static String REQ_ATTACH_DESCRIPTION;
       
    //indicateurs de parties et sous-parties du document excel  
    private static final short REQUIREMENT = 1;
    private static final short REQFAMILY = 2;
    private static final short ATTACHMENT = 3;
    private static final short DESCRIPTION = 4;
        
    //donnees recuperees
    private Hashtable reqsHastable = new Hashtable();
    private RequirementFamily reqset;
    private Requirement req;
    private IAttachment attach;
        
    //relatifs aux cellules rencontrees
    private SSTRecord sstRecord;
    private Hashtable labelHashtable = new Hashtable();
    private short position = 0;
    private short ss_position = 0;
    private Project project;
   
    /**
     * @param prj le projet dont les exigences seront parses
     * @throws LabelNotFoundException leve l'exception quand un label utilise dans le parsing reste introuvable dans le fichier de configuration
     */
    public ExcelReqParser(Project prj)throws LabelNotFoundException{
        project = prj;
        initQCLabels();
    }
        
    /**
     * initialise les labels a partir du fichier de configuration
     * @throws LabelNotFoundException leve l'exception quand un label utilise dans le parsing reste introuvable dans le fichier de configuration
     */
    private void initQCLabels() throws LabelNotFoundException{
        try {
            String labelsUnfound = "";
            String separator = ", ";                    
            FileInputStream  in = new FileInputStream(Project.CONFIG_FILE);             
            Properties prop = new Properties(); 
            prop.load(in);
            in.close(); 
            REQ_NAME = prop.getProperty("REQ_NAME");
            REQ_PRIORITY = prop.getProperty("REQ_PRIORITY");
            REQ_ID = prop.getProperty("REQ_ID");
            REQ_VERSION = prop.getProperty("REQ_VERSION");
            REQ_CATEGORY = prop.getProperty("REQ_CATEGORY");
            REQ_COMPLEXITY = prop.getProperty("REQ_COMPLEXITY");
            REQ_STATE = prop.getProperty("REQ_STATE");
            REQ_VERIFWAY = prop.getProperty("REQ_VERIFWAY");
            REQ_ORIGIN = prop.getProperty("REQ_ORIGIN");
            REQ_DESCRIPTION = prop.getProperty("REQ_DESCRIPTION");
            REQ_ATTACH_TITLE = prop.getProperty("REQ_ATTACH_TITLE");
            REQ_ATTACH_NAME = prop.getProperty("REQ_ATTACH_NAME");
            REQ_ATTACH_MODIFIED = prop.getProperty("REQ_ATTACH_MODIFIED");
            REQ_ATTACH_DESCRIPTION = prop.getProperty("REQ_ATTACH_DESCRIPTION");
                    
            if (REQ_NAME == null)
                labelsUnfound += "REQ_NAME" + separator;
            if (REQ_PRIORITY == null)
                labelsUnfound += "REQ_PRIORITY" + separator;
            if (REQ_ID == null)
                labelsUnfound += "REQ_ID" + separator;
            if (REQ_VERSION == null) 
                labelsUnfound += "REQ_VERSION" + separator;
            if (REQ_CATEGORY == null)
                labelsUnfound += "REQ_CATEGORY" + separator;
            if (REQ_COMPLEXITY == null) 
                labelsUnfound += "REQ_COMPLEXITY" + separator;
            if (REQ_STATE == null) 
                labelsUnfound += "REQ_STATE" + separator;
            if (REQ_VERIFWAY == null) 
                labelsUnfound += "REQ_VERIFWAY" + separator;
            if (REQ_ORIGIN == null) 
                labelsUnfound += "REQ_ORIGIN" + separator;
            if (REQ_DESCRIPTION == null) 
                labelsUnfound += "REQ_DESCRIPTION" + separator;
            if (REQ_ATTACH_TITLE == null) 
                labelsUnfound += "REQ_ATTACH_TITLE" + separator;
            if (REQ_ATTACH_NAME == null) 
                labelsUnfound += "REQ_ATTACH_NAME" + separator;
            if (REQ_ATTACH_MODIFIED == null) 
                labelsUnfound += "REQ_ATTACH_MODIFIED" + separator;
            if (REQ_ATTACH_DESCRIPTION == null) 
                labelsUnfound += "REQ_ATTACH_DESCRIPTION" + separator;
            if (!labelsUnfound.equals("")){
                labelsUnfound = labelsUnfound.substring(0, labelsUnfound.length() - separator.length());
                if (labelsUnfound.indexOf(separator) == -1)
                    labelsUnfound = "Le label " + labelsUnfound + " est introuvable dans le fichier de configuration " + Project.CONFIG_FILE;
                else
                    labelsUnfound = "Les labels " + labelsUnfound + " sont introuvables dans le fichier de configuration " + Project.CONFIG_FILE;
                throw new LabelNotFoundException(labelsUnfound);
            }
        } catch (IOException e) {
            project.writeInReport("Erreur fatale : Le fichier de configuration " + Project.CONFIG_FILE + " est introuvable\n"); 
            Util.err(e);
        }
    }
        
    /**
     * corrige le fichier de configuration config.properties en ajoutant les labels manquants
     */
    public static void useDefaultLabel(){
        try {           
            FileInputStream in = new FileInputStream(Project.CONFIG_FILE);      
            Properties prop = new Properties(); 
            prop.load(in);
            in.close(); 
            if (prop.getProperty("REQ_NAME") == null)
                prop.setProperty("REQ_NAME", DEFAULT_REQ_NAME);
            if (prop.getProperty("REQ_PRIORITY")== null)
                prop.setProperty("REQ_PRIORITY", DEFAULT_REQ_PRIORITY);
            if (prop.getProperty("REQ_ID")== null)
                prop.setProperty("REQ_ID", DEFAULT_REQ_ID);
            if (prop.getProperty("REQ_VERSION")== null)
                prop.setProperty("REQ_VERSION", DEFAULT_REQ_VERSION);
            if (prop.getProperty("REQ_CATEGORY")== null)
                prop.setProperty("REQ_CATEGORY", DEFAULT_REQ_CATEGORY);
            if (prop.getProperty("REQ_COMPLEXITY")== null)
                prop.setProperty("REQ_COMPLEXITY" , DEFAULT_REQ_COMPLEXITY);
            if (prop.getProperty("REQ_STATE")== null)
                prop.setProperty("REQ_STATE", DEFAULT_REQ_STATE);
            if (prop.getProperty("REQ_VERIFWAY")== null)
                prop.setProperty("REQ_VERIFWAY", DEFAULT_REQ_VERIFWAY);
            if (prop.getProperty("REQ_ORIGIN")== null)
                prop.setProperty("REQ_ORIGIN", DEFAULT_REQ_ORIGIN);
            if (prop.getProperty("REQ_DESCRIPTION")== null)
                prop.setProperty("REQ_DESCRIPTION", DEFAULT_REQ_DESCRIPTION);
            if (prop.getProperty("REQ_ATTACH_TITLE")== null)
                prop.setProperty("REQ_ATTACH_TITLE", DEFAULT_REQ_ATTACH_TITLE);
            if (prop.getProperty("REQ_ATTACH_NAME")== null)
                prop.setProperty("REQ_ATTACH_NAME", DEFAULT_REQ_ATTACH_NAME);
            if (prop.getProperty("REQ_ATTACH_MODIFIED")== null)
                prop.setProperty("REQ_ATTACH_MODIFIED", DEFAULT_REQ_ATTACH_MODIFIED);
            if (prop.getProperty("REQ_ATTACH_DESCRIPTION")== null)
                prop.setProperty("REQ_ATTACH_DESCRIPTION", DEFAULT_REQ_ATTACH_DESCRIPTION);
            FileOutputStream out = new FileOutputStream(Project.CONFIG_FILE); 
            prop.store(out, null);
            out.close();
        } catch (IOException e) {
            Util.err(e);
        }               
    }
        
    /**
     * retourne le Hastable contenant toutes les exigences
     * @return
     */
    public Hashtable getReqsHastable() {
        return reqsHastable;
    }

    /**
     * parse un fichier donne
     */
    @Override
    public void processRecord(Record record) {
        switch (record.getSid()) {                       
        case NumberRecord.sid:
            NumberRecord numRecord = (NumberRecord) record;
            if (numRecord.getColumn() == 0){                    
                if (sstRecord.toString().contains( "= " + (int)numRecord.getValue() + ".1")){
                    reqset = new RequirementFamily();
                    position = REQFAMILY;
                }
                else{
                    req = new Requirement();
                    position = REQUIREMENT;
                }
                ss_position = 0;
            }
            else if (labelHashtable.get(numRecord.getColumn()) != null && labelHashtable.get(numRecord.getColumn()).equals(REQ_ID)){
                if (position == REQFAMILY){
                    reqset.setId((int)numRecord.getValue());
                    reqsHastable.put(numRecord.getValue(), reqset);
                }
                else{
                    req.setId((int)numRecord.getValue());
                    reqsHastable.put(numRecord.getValue(), req);                                
                }
            }
            else if (ss_position == ATTACHMENT && attach instanceof FileAttachment){
                ((FileAttachment)attach).setDate(HSSFDateUtil.getJavaDate(numRecord.getValue()));
            }                   
            break;
        case SSTRecord.sid:
            sstRecord = (SSTRecord) record;
            break;
        case LabelSSTRecord.sid:
            LabelSSTRecord labelRecord = (LabelSSTRecord) record; 
            // quick an dirty to avoid compiler error (stecan, 05.02.2015)
            String cellContent = REQ_NAME;  // sstRecord.getString(labelRecord.getSSTIndex());
            if (labelHashtable.size() < 4 || (labelHashtable.size() < 9 && labelHashtable.contains(REQ_CATEGORY))){
                if (cellContent.equals(REQ_NAME))
                    labelHashtable.put(labelRecord.getColumn(), REQ_NAME);
                else if (cellContent.equals(REQ_PRIORITY))
                    labelHashtable.put(labelRecord.getColumn(), REQ_PRIORITY);
                else if (cellContent.equals(REQ_ID))
                    labelHashtable.put(labelRecord.getColumn(), REQ_ID);
                else if (cellContent.equals(REQ_VERSION))
                    labelHashtable.put(labelRecord.getColumn(), REQ_VERSION);
                else if (cellContent.equals(REQ_CATEGORY))
                    labelHashtable.put(labelRecord.getColumn(), REQ_CATEGORY);
                else if (cellContent.equals(REQ_COMPLEXITY))
                    labelHashtable.put(labelRecord.getColumn(), REQ_COMPLEXITY);
                else if (cellContent.equals(REQ_ORIGIN))
                    labelHashtable.put(labelRecord.getColumn(), REQ_ORIGIN);
                else if (cellContent.equals(REQ_STATE))
                    labelHashtable.put(labelRecord.getColumn(), REQ_STATE);
                else if (cellContent.equals(REQ_VERIFWAY))
                    labelHashtable.put(labelRecord.getColumn(), REQ_VERIFWAY);
            }
            else if (labelRecord.getColumn() == 0){
                if (ParserUtil.isParagraph(cellContent) ){
                    //on est dans un nouveau paragraphe
                    //recherche du parent du noeud courant
                    if (sstRecord.toString().contains("= " + cellContent + ".1")){
                        try{
                            Integer.parseInt(cellContent);//nouvel arbre d'exigence ayant un numero de type string
                            reqset = new RequirementFamily(null);                                               
                        }catch(Exception e){    
                            if(cellContent.endsWith("1") || reqset.order() < ParserUtil.paragraphOrder(cellContent)) 
                                reqset = new RequirementFamily(reqset);
                            else{
                                RequirementFamily parent = reqset.getParent();
                                for(int i = 0; i < reqset.order() - ParserUtil.paragraphOrder(cellContent); i++)
                                    parent = parent.getParent();                                                                                                                        
                                reqset = new RequirementFamily(parent);      
                            }
                        }                               
                        position = REQFAMILY;
                    }
                    else{
                        try{
                            Integer.parseInt(cellContent);
                            req = new Requirement(null);//exigence ayant un numero de type string                                               
                        }catch(Exception e){
                            RequirementFamily parent = reqset;
                            for(int i = 0; i < reqset.order() - ParserUtil.paragraphOrder(cellContent) + 1; i++)
                                parent = parent.getParent();                                     
                            req = new Requirement(parent);
                        }
                        position = REQUIREMENT;
                    }
                    ss_position = 0;
                }
                //sous-partie d'un paragraphe
                //traitements a effectuer en fonction du type du paragraphe (ReqFamily ou Requirement) 
                else if (cellContent.equals(REQ_ATTACH_TITLE))
                    ss_position = ATTACHMENT;
                else if (cellContent.equals(REQ_DESCRIPTION))
                    ss_position = DESCRIPTION;
                else if (ss_position == DESCRIPTION){
                    if (position == REQFAMILY)
                        if (reqset.getDescription() == null)
                            reqset.setDescription(cellContent);
                        else
                            reqset.completeDescription(cellContent);
                    else if (position == REQUIREMENT)
                        if (req.getDescription() == null)
                            req.setDescription(cellContent);
                        else
                            req.completeDescription(cellContent);
                }   
            }            
            else if (labelHashtable.get(labelRecord.getColumn()) != null && ss_position == 0){
                //id de type string
                if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_ID)){
                    try{
                        int val = Integer.parseInt(cellContent);
                        if (position == REQFAMILY){
                            reqset.setId(val);
                            reqsHastable.put(val, reqset);
                        }
                        else{
                            req.setId(val);
                            reqsHastable.put(val, req);                         
                        }
                    }catch(Exception e){
                        project.writeInReport("Erreur fatale : La colonne " + labelRecord.getColumn() + " est dediee pour les identifiants des exigences\n");
                    }                           
                }
                else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_NAME)){   
                    if (position == REQFAMILY)
                        reqset.setName(ParserUtil.replaceDirSpecialChars(cellContent));
                    else
                        req.setName(ParserUtil.replaceDirSpecialChars(cellContent));
                }
                else if (position == REQUIREMENT)
                    if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_PRIORITY))
                        setReqPriority(req, cellContent);                               
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_CATEGORY))
                        setReqCategory(req, cellContent);
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_COMPLEXITY))
                        setReqComplexity(req, cellContent);
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_STATE))
                        setReqState(req, cellContent);
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_ORIGIN))
                        req.setOrigin(cellContent);
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_VERIFWAY))
                        req.setVerifway(cellContent);
                    else if (labelHashtable.get(labelRecord.getColumn()).equals(REQ_VERSION))
                        req.setVersion(cellContent);                                    
            }
            if (ss_position == ATTACHMENT  && !cellContent.equals(REQ_ATTACH_TITLE) && !cellContent.equals(REQ_ATTACH_NAME) && !cellContent.equals(REQ_ATTACH_MODIFIED) && !cellContent.equals(REQ_ATTACH_DESCRIPTION)){
                if (labelRecord.getColumn() == 0) {
                    if (ParserUtil.isUrl(cellContent)){
                        attach = new UrlAttachment();
                        attach.setLocation(cellContent);
                        if (position == REQFAMILY)
                            reqset.addAttachment(attach);
                        else
                            req.addAttachment(attach);
                    }
                    else{
                        attach = new FileAttachment();
                        ((FileAttachment)attach).setName(cellContent);
                        if (position == REQFAMILY){
                            attach.setLocation(Project.ATTACH_DIR + File.separator + Project.REQ_DIR + File.separator + reqset.getId() + File.separator + cellContent);
                            reqset.addAttachment(attach);
                        }                                                                                               
                        else{
                            attach.setLocation(Project.ATTACH_DIR + File.separator + Project.REQ_DIR + File.separator + req.getId() + File.separator + cellContent);
                            req.addAttachment(attach);
                        }
                    }           
                }
                else if (attach instanceof FileAttachment && labelRecord.getColumn() == 1){                             
                    try{
                        Date date = new SimpleDateFormat("dd/mm/yyyy").parse(cellContent);
                        ((FileAttachment)attach).setDate(date);
                    }catch(Exception e){
                        project.writeInReport("La cellule " + labelRecord.getRow() + ","+ labelRecord.getColumn() + " devrait contenir une date\n");
                    }
                } 
                else if (labelRecord.getColumn() == 2)
                    if (attach.getDescription() == null)
                        attach.setDescription(cellContent);
                    else
                        attach.completeDescription(cellContent);        
            }   
            break;
        }
    }
        
    /**
     * affecte la priorite adequate a l'exigence sachant que :
     * <br><b>P3 Optional</b> correspond a 1 soit <b>low</b>
     * <br><b>P2 Normal</b> correspond a 10 soit <b>medium</b>
     * <br><b>P1 Important</b> correspondent a 100 soit <b>high</b>
     *<br><b>P0 Essential</b> correspondent a 1000 soit <b>essential</b>
     */
    private void setReqPriority(Requirement req, String priority){
        if (priority.startsWith("P3"))
            req.setPriority(1);
        else if (priority.startsWith("P2"))
            req.setPriority(10);
        else if (priority.startsWith("P0"))
            req.setPriority(1000);
    }
        
    /**
     *affecte la categorie adequate a l'exigence sachant que :
     * <br><b>Functional</b> corespond a 0 soit <b>Functionnel</b>
     * <br><b>Interoperability</b> corespond a 1 soit <b>Interoperabilite</b>
     * <br><b>Load</b> corespond a 2 soit <b>Charge</b>
     * <br><b>Performance</b> corespond a 3 soit <b>Performance</b>
     * <br><b>Availability</b>, <b>Facility of recover</b>, <b>Reliability</b>, <b>Tolerance with the faults</b> corespondent a 4 soit <b>Disponibilite</b>
     * <br><b>Safety</b> corespond a 5 soit <b>Securite</b>
     * <br><b>Exploitability</b>, <b>Exploitation</b>, <b>Installation</b>, <b>Safeguard/restoration</b>, <b>Supervision</b> corespondent a 6 soit <b>Exploitabilite</b>
     * les autres possibilites corespondent a 7 soit <b>Autre</b>
     */
    private void setReqCategory(Requirement req, String category){
        if (category.equals("Functional"))
            req.setCategory(0);
        else if (category.equals("Interoperability"))
            req.setCategory(1);
        else if (category.equals("Load"))
            req.setCategory(2);
        else if (category.equals("Performance"))
            req.setCategory(3);
        else if (category.equals("Availability") || category.equals("Facility of recover") || category.equals("Reliability") || category.equals("Tolerance with the faults"))
            req.setCategory(4);
        else if (category.equals("Safety"))
            req.setCategory(5);
        else if (category.equals("Exploitability") || category.equals("Exploitation") || category.equals("Installation") 
                 || category.equals("Safeguard/restoration") || category.equals("Supervision"))
            req.setCategory(6);
        else 
            req.setCategory(7);
    }
        
    /**
     * affecte la complexite adequate a l'exigence sachant que :
     * <br><b>C0 Very complex</b> equivaut a 1000 <b>C0</b>
     * <br><b>C1 Complex</b> equivaut a 100 <b>C1</b>
     * <br><b>C2 Medium</b> equivaut a 10<b>C2</b>
     * <br><b>C3 Simple</b> equivaut a 1<b>C3</b>
     */
    private void setReqComplexity(Requirement req, String complexity){
        if (complexity.startsWith("C0"))
            req.setComplexity(1000);
        else if (complexity.startsWith("C1"))
            req.setComplexity(100);
        else if (complexity.startsWith("C2"))
            req.setComplexity(10);
        else if (complexity.startsWith("C3"))
            req.setComplexity(1);       
    }

    /**
     * affecte l'etat adequat a l'exigence sachant que :
     * <br><b>Analyzed</b> correspond a 1 soit <b>Analysee</b>
     * <br><b>Approved</b> correspond a 2 soit <b>Approuvee</b>
     */
    private void setReqState(Requirement req, String state){
        if (state.equals("Analyzed"))
            req.setState(1);
        else if (state.equals("Approved"))
            req.setState(2);
    }

}
