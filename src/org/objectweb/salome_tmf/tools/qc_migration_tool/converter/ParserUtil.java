/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParserUtil {
        
    public static final String SALOME_PARAM_SEPARATOR = "$";
    public static final String QC_PARAM_BEGIN = "<<<";
    public static final String QC_PARAM_END = ">>>";
    public static final int SALOME_PARAM_SEP_LENGTH = SALOME_PARAM_SEPARATOR.length();
    public static final int QC_PARAM_BEGIN_LENGTH = QC_PARAM_BEGIN.length();
    public static final int QC_PARAM_END_LENGTH = QC_PARAM_END.length();
    public static final String DIRECTORY_SPECIAL_CHARS = "/:?\"<>|";
    public static final String STEP_SPECIAL_CHARS = "\\/:*?<>|\"";
    public static final String TEST_SPECIAL_CHARS = "\\/:%*?<>|\"";
    public static final String QC_FOLDER_SPECIAL_CHARS = "\\*^";
    public static final char REPLACEMENT_CHAR = '_';

    /**
     * recherche dans la chaine provenant d'un export de Quality Center des parametres et les formate de sorte 
     * qu'ils soient reconnaissables par SalomeTMF       
     */
    public static String formatParamsFromQC(String chaine){
        int beginIndex = chaine.indexOf(QC_PARAM_BEGIN, 0);
        StringBuffer nv_chaine = null;
        while (beginIndex > -1 && beginIndex < chaine.length() - QC_PARAM_END_LENGTH){
            int endIndex = chaine.indexOf(QC_PARAM_END,beginIndex + 1);
            if (endIndex > -1){
                String param = chaine.substring(beginIndex,endIndex + QC_PARAM_END_LENGTH);
                String nv_param = SALOME_PARAM_SEPARATOR + param.substring(QC_PARAM_BEGIN_LENGTH, param.length() - QC_PARAM_END_LENGTH) + SALOME_PARAM_SEPARATOR;
                nv_chaine = new StringBuffer(chaine).replace(beginIndex,endIndex + QC_PARAM_END_LENGTH,nv_param);
            }           
            chaine = nv_chaine.toString();
            beginIndex = chaine.indexOf(QC_PARAM_BEGIN,endIndex+1);             
        }               
        return chaine;
    }
        
    /**
     * retourne la liste des parametres contenus dans une chaine de caracteres provenant de Quality Center
     * @param chaine la chaine de caracteres en question
     * @return liste de parametres
     */
    public static List getParamsInString(String chaine){
        List paramsList = new ArrayList();
        int beginIndex = chaine.indexOf(QC_PARAM_BEGIN, 0);
        while (beginIndex > -1 && beginIndex < chaine.length() - QC_PARAM_END_LENGTH){
            int endIndex = chaine.indexOf(QC_PARAM_END,beginIndex + 1);
            if (endIndex > -1)
                paramsList.add(chaine.substring(beginIndex + QC_PARAM_END_LENGTH , endIndex));
            beginIndex = chaine.indexOf(QC_PARAM_BEGIN,endIndex+1);             
        }
        return paramsList;
    }
        
    /**
     * retourne le path (non absolu) d'un noeud
     * @param chaine chaine de caratere contenant le path
     * @return
     */
    public static String findPath(String chaine){
        return chaine.substring(chaine.indexOf(File.separator) + 1,chaine.lastIndexOf(File.separator));
    }
        
    /**
     * verifie si une chaine de caractere correspond a un paragraphe
     * @param paragraph chaine de caractere a analyser
     * @return vrai si la chiane est un paragraphe, faux sinon
     */
    public static boolean isParagraph(String paragraph){
        String[] num = paragraph.split("\\.");
        boolean isTrue = true;
        int i = 0;
        while (i < num.length && isTrue){
            try {
                int entier = Integer.parseInt(num[i]);
            }catch(NumberFormatException e){
                isTrue = false;
            }
            i++;
        }
        return isTrue;
    }
        
    /**
     * retourne vrai si la chaine de caractere passee en parametre est une url
     * @param url
     * @return retourne vrai si 3>=index("://")<=6
     */
    public static boolean isUrl(String url){
        return url.indexOf("://", 2) >=3 && url.indexOf("://", 2) <=6;
    }
        
    /**
     * retourne vrai si chaine est de la forme <b>Step entier</b>
     */
    public static boolean hasStepFormat(String chaine){
        boolean result = false;
        String[] mots = chaine.split(" ");
        try {
            if (mots.length == 2  && mots[0].equals("Step") && Integer.parseInt(mots[1]) >= 0) 
                result = true;
        }catch(NumberFormatException e){}
        return result;
    }
        
    /**
     * retourne l'ordre(niveau) d'un numero paragraphe
     * @param paragraph
     * @return retourne 0 si paragraph n'est pas en realite un numero de paragraphe
     */
    public static int paragraphOrder(String paragraph){
        int order = 0;
        int i = paragraph.indexOf(".");
        while (i != -1){
            order++;
            i = paragraph.indexOf(".", i+1);
        }
        return order;
    }
        
    /**
     * remplace dans le nom d'un repertoire les caracteres interdits dans un nom d'un repertoire
     * @param dirName le nom du repertoire
     */
    public static String replaceDirSpecialChars(String dirName){
        for(int i = 0; i < DIRECTORY_SPECIAL_CHARS.length(); i++)
            if (dirName.indexOf(DIRECTORY_SPECIAL_CHARS.charAt(i)) != -1)
                dirName = dirName.replace(DIRECTORY_SPECIAL_CHARS.charAt(i), REPLACEMENT_CHAR);         
        return dirName;
    }
        
    /**
     * remplace dans le nom d'une action les caracteres interdits dans un nom d'un repertoire
     * @param stepName le nom de l'action
     */
    public static String replaceStepSpecialChars(String stepName){
        for(int i = 0; i < STEP_SPECIAL_CHARS.length(); i++)
            if (stepName.indexOf(STEP_SPECIAL_CHARS.charAt(i)) != -1)
                stepName = stepName.replace(STEP_SPECIAL_CHARS.charAt(i), REPLACEMENT_CHAR);            
        return stepName;
    }
        
    /**
     * remplace dans le nom d'un test les caracteres interdits dans un nom d'un repertoire
     * @param testName nom du test
     * @return
     */
    public static String replaceTestSpecialChars(String testName){
        for(int i = 0; i < TEST_SPECIAL_CHARS.length(); i++)
            if (testName.indexOf(TEST_SPECIAL_CHARS.charAt(i)) != -1)
                testName = testName.replace(TEST_SPECIAL_CHARS.charAt(i), REPLACEMENT_CHAR);    
        if (testName.indexOf("'") != -1)
            testName = testName.replace("'", " ");
        return testName;
    }
        
    /**
     * remplace dans le nom d'une famille ou d'une suite de test les caracteres interdits ou mal geres dans les noms
     * de repertoire dans Quality Center
     * @param folderName le nom de la famille ou de la suite de test
     * @return
     */
    public static String replaceQC_FolderSpecialChars(String folderName){
        for(int i = 0; i < QC_FOLDER_SPECIAL_CHARS.length(); i++)
            if (folderName.indexOf(QC_FOLDER_SPECIAL_CHARS.charAt(i)) != -1)
                folderName = folderName.replace(QC_FOLDER_SPECIAL_CHARS.charAt(i), REPLACEMENT_CHAR);
        if (folderName.indexOf("'") != -1)
            folderName = folderName.replace("'", " ");
        return folderName;
    }
        
        
    /**
     * * recherche dans la chaine provenant d'un export de Quality Center des parametres et les formate de sorte 
     * qu'ile soient reconnaissables par SalomeTMF      
     */
    public static String formatParamsFromSalome(String chaine){
        int beginIndex = chaine.indexOf(SALOME_PARAM_SEPARATOR, 0);
        StringBuffer nv_chaine = null;
        while (beginIndex > -1 && beginIndex < chaine.length() - SALOME_PARAM_SEP_LENGTH){
            int endIndex =chaine.indexOf(SALOME_PARAM_SEPARATOR,beginIndex + 1);
            if (endIndex > -1){
                String param = chaine.substring(beginIndex,endIndex + SALOME_PARAM_SEP_LENGTH);
                String nv_param = QC_PARAM_BEGIN + param.substring(SALOME_PARAM_SEP_LENGTH, param.length() - SALOME_PARAM_SEP_LENGTH) + QC_PARAM_END;
                nv_chaine = new StringBuffer(chaine).replace(beginIndex,endIndex+1,nv_param);
            }           
            chaine = nv_chaine.toString();
            beginIndex = chaine.indexOf(SALOME_PARAM_SEPARATOR,beginIndex+1);           
        }
        return chaine; 
    }
        
    /**
     * retourne une date correspondant a la date donnee sous forme de chaine de caracteres
     * @param date
     * @return
     * @throws Exception
     */
    public static Date formatDateForQC(String date) throws Exception {
        String[]dateElmt = date.split(" ");
        Date dat;
        if (dateElmt.length == 3){
            date = dateElmt[0] + "/" + dateElmt[1]+ "/" + dateElmt[2];
            dat = new SimpleDateFormat("dd/MMMM/yyyy").parse(date);
        }
        else{
            dateElmt = date.split("-");
            date = dateElmt[2] + "/" + dateElmt[1]+ "/" + dateElmt[0];
            dat = new SimpleDateFormat("dd/MM/yyyy").parse(date); 
        }
                
        return dat;
    }
        
    /**
     * retourne un identifiant mis a la fin d'une chaine de caracteres
     * @param chaine
     * @return
     */
    public static int getIDInString(String chaine){
        return Integer.parseInt(chaine.substring(chaine.lastIndexOf("_") + 1));         
    }
}
