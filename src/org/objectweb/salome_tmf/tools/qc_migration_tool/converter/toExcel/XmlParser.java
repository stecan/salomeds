/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toExcel;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.ParserUtil;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Designer;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Family;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Requirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.RequirementFamily;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Step;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Test;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.TestSuite;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.UrlAttachment;

public class XmlParser {
        
    //  nom des balises selon la dtd de salome dynamique 
    private static final String PROJET = "ProjetVT"; 
    private static final String NOM = "Nom"; 
    private static final String DESCRIPTION = "Description"; 
    private static final String CREATON_DATE = "Date_crea"; 
        
    private static final String ATTACHMENTS = "Attachements";
    private static final String URL_ATTACH = "UrlAttachement";
    private static final String URL = "url";
    private static final String FILE_ATTACH_DATE = "Date";
    private static final String FILE_ATTACH_DIR = "dir";
        
    private static final String REQUIREMENTS = "Requirements";
    private static final String REQ_FAMILY = "RequirementFamily";
    private static final String REQ = "Requirement";
    private static final String REQ_ID = "id_req";
    private static final String REQ_PARENT_ID = "id_req_parent";
    private static final String REQ_ID_PREFIXE = "Req_";
    private static final String REQ_VERSION = "version";
    private static final String REQ_PRIORITY = "priority";
    private static final String REQ_CATEGORY = "category";
    private static final String REQ_COMPLEXITY = "complexity";
    private static final String REQ_STATE = "state";
    private static final String REQ_ORIGIN = "origine";
    private static final String REQ_VERIFWAY = "verifway";
        
    private static final String FAMILLES = "Familles"; 
    private static final String FAM_ID = "id_famille";   
    private static final String SUITE_TESTS = "SuiteTests"; 
    private static final String SUITE_ID = "id_suite";
    private static final String TESTS = "Tests";
    private static final String TEST_ID = "id_test";
    private static final String CONCEPTEUR = "Concepteur";
        
    private static final String TEST_MANUEL = "TestManuel";
    private static final String ACTION_ID = "id_action";
    private static final String RESULT_EXPECTED = "ResultAttendu";
    private static final String TEST_AUTO = "TestAuto";
        
    private static final String LINK_REQ = "LinkRequirement";
    private static final String REQ_REF_ID = "ref"; 

    private SAXReader reader; 
    private Document document; 
    private Hashtable reqHashtable;
    private List families;
         
    /**
     * @param filename nom de l'export XML de SalomeTMF a parser
     * @throws Exception
     */
    public XmlParser(String filename) throws Exception{
        reader = new SAXReader();
        document = reader.read(filename);
        reqHashtable = new Hashtable();
        families = new ArrayList(); 
    }
    
   
    /**
     * retourne l'arborescence des exigences provenant de l'export XML
     * @return
     */
    public Hashtable getReqHashtable(){
        if (reqHashtable.size() == 0){
            Element requierements = document.getRootElement().element(PROJET).element(REQUIREMENTS);
            if (requierements != null){
                List e_reqsSets= requierements.elements();
                                
                //parcours des parcours des ensembles d'exigences
                for(int i = 0; i < e_reqsSets.size(); i++){
                    makeRequirementsTree((Element)e_reqsSets.get(i));
                }
            }
        }       
        return reqHashtable;            
    }   
        
    /**
     * traite un noeud d'exigence en la mettant dans la base des exigences
     * @param e_elmt une famille d'exigence ou une exigence 
     * @param parent la famille contenant e-elmt
     */
    private void makeRequirementsTree(Element e_elmt){
        RequirementFamily parent = null;
        if (!e_elmt.attribute(REQ_PARENT_ID).getText().equals(REQ_ID_PREFIXE + "0"))
            parent = (RequirementFamily)reqHashtable.get(e_elmt.attribute(REQ_PARENT_ID).getText());
        //cas d'une famille d'exigences
        if (e_elmt.getName().equals(REQ_FAMILY)){
            RequirementFamily reqFamily;
                                                
            //construire l'arborescence des exigences 
            if(parent != null)
                reqFamily = new RequirementFamily(parent);
            else
                reqFamily = new RequirementFamily();
            reqFamily.setId(ParserUtil.getIDInString(e_elmt.attribute(REQ_ID).getText()));
            reqFamily.setName(ParserUtil.replaceQC_FolderSpecialChars(e_elmt.element(NOM).getText()));
            if (e_elmt.element(DESCRIPTION) != null)
                reqFamily.setDescription(e_elmt.element(DESCRIPTION).getText());
            getAttachments(e_elmt,reqFamily);
                
            //ajouter a la base des exigences
            reqHashtable.put(e_elmt.attribute(REQ_ID).getText(),reqFamily);
                
            //traiter les sous-exigences de la famille
            List e_childs = e_elmt.elements();
            if (e_childs != null)
                for(int i = 0; i < e_childs.size(); i++){
                    Element e_child = (Element)e_childs.get(i);
                    if(e_child.getName().equals(REQ) || e_child.getName().equals(REQ_FAMILY))
                        makeRequirementsTree(e_child);
                }               
        } 
        //cas d'une exigence  
        else if (e_elmt.getName().equals(REQ)){
            Requirement req;
                                        
            //construire l'arborescence des exigences 
            if(parent != null)
                req = new Requirement(parent);
            else
                req = new Requirement();
            req.setId(ParserUtil.getIDInString(e_elmt.attribute(REQ_ID).getText()));
            req.setName(ParserUtil.replaceQC_FolderSpecialChars(e_elmt.element(NOM).getText()));
            req.setPriority(Integer.parseInt(e_elmt.attribute(REQ_PRIORITY).getText()));
            req.setVersion(e_elmt.attribute(REQ_VERSION).getText());
            if (e_elmt.element(DESCRIPTION) != null)
                req.setDescription(e_elmt.element(DESCRIPTION).getText());
            if (e_elmt.attribute(REQ_CATEGORY ) != null)
                req.setCategory(Integer.parseInt(e_elmt.attribute(REQ_CATEGORY ).getText()));           
            if (e_elmt.attribute(REQ_COMPLEXITY ) != null)
                req.setComplexity(Integer.parseInt(e_elmt.attribute(REQ_COMPLEXITY ).getText()));
            if (e_elmt.attribute(REQ_ORIGIN ) != null)
                req.setOrigin(e_elmt.attribute(REQ_ORIGIN ).getText());
            if (e_elmt.attribute(REQ_STATE ) != null)
                req.setState(Integer.parseInt(e_elmt.attribute(REQ_STATE ).getText()));
            if (e_elmt.attribute(REQ_VERIFWAY ) != null)
                req.setVerifway(e_elmt.attribute(REQ_VERIFWAY).getText());
            getAttachments(e_elmt, req);
                
            //ajouter a la base des exigences
            reqHashtable.put(e_elmt.attribute(REQ_ID).getText(),req);
        }
    }
    
    /**
     * retourne l'arborescence des familles provenant de l'export XML
     * @return
     * @throws Exception
     */
    public List getFamilies(){
        if (families.size() == 0){                      
            //parcours de chaque famille
            Element familles = document.getRootElement().element(PROJET).element(FAMILLES);
            if (familles != null){
                List e_families = familles.elements();         
                                             
                for(int nb_f = 0; nb_f < e_families.size(); nb_f++){
                    Element e_family = (Element)e_families.get(nb_f);
                    Family family = new Family();
                    family.setId(ParserUtil.getIDInString(e_family.attribute(FAM_ID).getText()));
                    family.setName(ParserUtil.replaceQC_FolderSpecialChars(e_family.element(NOM).getText()));
                    if(e_family.element(DESCRIPTION) != null)
                        family.setDescription(e_family.element(DESCRIPTION).getText());
                    getAttachments(e_family, family);
                                
                    //parcours de chaque suite de tests de la famille
                    Element e_testsSuites = e_family.element(SUITE_TESTS);
                    if (e_testsSuites != null){
                        List testsSuites = e_testsSuites.elements();
                        for (int nb_list = 0; nb_list < testsSuites.size(); nb_list++)
                            getTestList((Element)testsSuites.get(nb_list),family);
                    }                           
                    families.add(family);
                }       
            }
        }
        return families;
    }
        
    /**
     * ajouter a un element ses attachements
     * @param e_elmt le noeud concerne dans l'arbre XML
     * @param elmt l'objet concerne
     */
    private void getAttachments(Element e_elmt, org.objectweb.salome_tmf.tools.qc_migration_tool.data.CommunData elmt){
        Element attachements = e_elmt.element(ATTACHMENTS);
        if (attachements != null){
            List atts = attachements.elements();
            for(int num_att = 0; num_att < atts.size(); num_att++){
                Element e_att = (Element)atts.get(num_att);
                IAttachment att;
                if (e_att.getName().equals(URL_ATTACH)){
                    att = new UrlAttachment();
                    att.setLocation(e_att.attribute(URL).getText());
                }
                else{
                    att = new FileAttachment();
                    att.setLocation(e_att.attribute(FILE_ATTACH_DIR).getText());
                }
                if(e_att.element(DESCRIPTION) != null)
                    att.setDescription(e_att.element(DESCRIPTION).getText());
                if(e_att.element(FILE_ATTACH_DATE) != null)
                    try {
                        ((FileAttachment)att).setDate(ParserUtil.formatDateForQC(e_att.element(FILE_ATTACH_DATE).getText()));
                    } catch (Exception e) {
                        Util.err(e);
                    }
                elmt.addAttachment(att);
            }
        }
    }
        
    /**
     * traite une suite de tests
     * @param e_suite
     * @throws Exception
     */
    private void getTestList(Element e_suite, Family fam){
        TestSuite testSuite = new TestSuite(fam);
        testSuite.setId(ParserUtil.getIDInString(e_suite.attribute(SUITE_ID).getText()));
        testSuite.setName(ParserUtil.replaceQC_FolderSpecialChars(e_suite.element(NOM).getText()));
        if(e_suite.element(DESCRIPTION) != null)
            testSuite.setDescription(e_suite.element(DESCRIPTION).getText());
        getAttachments(e_suite,testSuite);
        fam.addTestSuites(testSuite);
                
        //parcours de chaque test de la suite
        Element tests = e_suite.element(TESTS);
        if (tests != null) {
            List e_tests = tests.elements();
            for(int nb_test = 0; nb_test <e_tests.size(); nb_test++){
                Element e_test = (Element)e_tests.get(nb_test);
                if (e_test.element(TEST_AUTO) == null)
                    getTest(e_test,testSuite);  
            }
        }
    }
        
    /**
     * traite un test
     * @param e_test
     * @throws Exception
     */
    private void getTest(Element e_test, TestSuite suite){
        Test test = new Test(suite);
        test.setId(ParserUtil.getIDInString(e_test.attribute(TEST_ID).getText()));
        test.setName(ParserUtil.replaceTestSpecialChars(e_test.element(NOM).getText()));
        try {
            test.setCreationDate(ParserUtil.formatDateForQC(e_test.element(CREATON_DATE).getText()));
        } catch (Exception e) {
            Util.err(e);
        }
        test.setStatus("Imported");
        test.setDesigner(new Designer(e_test.element(CONCEPTEUR).element(NOM).getText()));
        if(e_test.element(DESCRIPTION) != null)
            test.setDescription(e_test.element(DESCRIPTION).getText());
        getAttachments(e_test, test);
                
        //parcours des exigences
        Element LinkReq = e_test.element(LINK_REQ);
        if (LinkReq != null){
            List e_reqs = LinkReq.elements();
            for (int nb_req = 0; nb_req < e_reqs.size(); nb_req++){
                Element e_req = (Element)e_reqs.get(nb_req);
                Requirement req = (Requirement)reqHashtable.get(e_req.attribute(REQ_REF_ID).getText());
                test.addRequirement(req);
            }
        }
                
        //type manuel
        Element e_manualT = e_test.element(TEST_MANUEL);
        if (e_manualT != null){
            test.setTestType("manuel");                 
            List e_steps =e_manualT.elements();
            for (int nb_step = 0; nb_step < e_steps.size(); nb_step++){
                Element e_step = (Element)e_steps.get(nb_step);
                Step step = new Step(test);
                step.setId(ParserUtil.getIDInString(e_step.attribute(ACTION_ID).getText()));
                step.setName(e_step.element(NOM).getText());
                if(e_step.element(DESCRIPTION) != null)
                    step.setDescription(e_step.element(DESCRIPTION).getText()); 
                if (e_step.element(RESULT_EXPECTED) != null)
                    step.setResult(e_step.element(RESULT_EXPECTED).getText());
                if (e_step.element(ATTACHMENTS) != null){
                    getAttachments(e_step, step);
                }
                test.addStep(step);
            }                   
        }
        suite.addTest(test);
    }
        
}
