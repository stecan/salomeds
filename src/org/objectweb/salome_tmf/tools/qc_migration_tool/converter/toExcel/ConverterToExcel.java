/*
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@orange-ftgroup.com
 *              faycal.sougrati@orange-ftgroup.com
 */
 
/*
 * Mercury Quality Center is a trademark of Mercury Interactive Corporation
 */

package org.objectweb.salome_tmf.tools.qc_migration_tool.converter.toExcel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;

import org.objectweb.salome_tmf.tools.qc_migration_tool.converter.ParserUtil;

import org.objectweb.salome_tmf.tools.qc_migration_tool.data.FileAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IAttachment;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.CommunData;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Family;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.IRequirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.RequirementFamily;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Requirement;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Step;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.Test;
import org.objectweb.salome_tmf.tools.qc_migration_tool.data.TestSuite;

public class ConverterToExcel {
        
    private static final String linkPath = "Tests a executer";
    private static final String ATTACH_SCRIPT_FILE = "." + File.separator + "resources" + File.separator + "qc_VAPI_XP_scripts" + File.separator + "liaisons_attachements.txt";
    private static final String COVERAGE_SCRIPT_FILE = "." + File.separator + "resources" + File.separator + "qc_VAPI_XP_scripts" + File.separator + "couverture.txt";
    private final HSSFWorkbook wb;
    private final XmlParser parser;
    private final HSSFCellStyle cellStyle1;
    private final HSSFCellStyle cellStyle2;
    private HSSFSheet attachSheet;
    private HSSFSheet linkSheet;
    private String sourceFile;
    private String targetFile;
        
    /**
     * 
     * @param sourceFile nom de l'export XML de SalomeTMF 
     * @param targetFile nom du fichier Excel a creer
     * @throws Exception cette exception est leve par le parseur XML 
     */
    public ConverterToExcel(String sourceFile, String targetFile) throws Exception{
        wb = new HSSFWorkbook();
        parser = new XmlParser(sourceFile);
        this.sourceFile = sourceFile;
        this.targetFile = targetFile;
                
        //creation du style1
        cellStyle1 = wb.createCellStyle();
        HSSFFont font1 = wb.createFont();
        font1.setColor(HSSFColor.ROYAL_BLUE.index );
        font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        cellStyle1.setFont(font1);
                
        //creation du style2
        cellStyle2 = wb.createCellStyle();
        HSSFFont font2 = wb.createFont();
        font2.setColor(HSSFColor.VIOLET.index);
        font2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        cellStyle2.setFont(font2);              
    }

    /**
     * cree un entete de colonne
     * @param row ligne entete
     * @param name nom de l'entete
     * @param style style de l'entete
     */
    private void createColumnName(HSSFRow row, String name, HSSFCellStyle style){
        HSSFCell cell = row.createCell((short)(row.getLastCellNum()+1));
        cell.setCellStyle(style);
        cell.setCellValue(name);
    }
                
    /**
     * cree une feuille de calcul contenant les exigences du projet exporte
     * @throws Exception
     */
    public void createReqSheet()throws Exception{
        HSSFSheet reqSheet = wb.createSheet("Exigences");
        int numRow = 0;         
                
        //creation des entetes de colonnes              
        HSSFRow row = reqSheet.createRow(numRow++);             
        createColumnName(row,"Path",cellStyle1);
        createColumnName(row,"Nom", cellStyle1);
        createColumnName(row,"Description", cellStyle1);                
        createColumnName(row,"Priorite",cellStyle1);
        createColumnName(row,"Version",cellStyle1);
        createColumnName(row,"Categorie",cellStyle1);
        createColumnName(row,"Complexite",cellStyle1);
        createColumnName(row,"Etat",cellStyle1);
        createColumnName(row,"Mode de verification",cellStyle1);
        createColumnName(row,"Origine",cellStyle1);     
        createColumnName(row,"Identifier",cellStyle1);  
                
        //creation du contenu des cellules
        Collection reqs = parser.getReqHashtable().values();
        Iterator it_reqs = reqs.iterator();
        while (it_reqs.hasNext()){
            CommunData elmt = (CommunData)it_reqs.next();
            row = reqSheet.createRow(numRow++);
            short numCell=0;
            HSSFCell cell = row.createCell(numCell++);
            cell.setCellValue(((IRequirement)elmt).getPath());
            cell = row.createCell(numCell++);
            cell.setCellValue(elmt.getName());
            cell = row.createCell(numCell++);
            cell.setCellValue(elmt.getDescription() != null ? elmt.getDescription() : elmt.getName());
            if (elmt.getAttachments().size() > 0)
                writeAttachement(elmt);
            if (elmt instanceof Requirement){
                Requirement req = (Requirement) elmt;                                                   
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqPriority(req.getPriority()));                           
                cell = row.createCell(numCell++);
                cell.setCellValue(RequirementFamily.DEFAULT_VERSION);
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqCategory(req.getCategory()));
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqComplexity(req.getComplexity()));
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqState(req.getState()));
                cell = row.createCell(numCell++);
                cell.setCellValue(req.getVerifway());
                cell = row.createCell(numCell++);
                cell.setCellValue(req.getOrigin());
            }
            else{
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqPriority(RequirementFamily.DEFAULT_PRIORITY));                          
                cell = row.createCell(numCell++);
                cell.setCellValue(RequirementFamily.DEFAULT_VERSION);
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqCategory(RequirementFamily.DEFAULT_CATEGORY));
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqComplexity(RequirementFamily.DEFAULT_COMPLEXITY));
                cell = row.createCell(numCell++);
                cell.setCellValue(getReqState(RequirementFamily.DEFAULT_STATE));
                cell = row.createCell(numCell++);
                cell.setCellValue(RequirementFamily.DEFAULT_VERIFWAY);
                cell = row.createCell(numCell++);
                cell.setCellValue(RequirementFamily.DEFAULT_ORIGIN);
            }
            cell = row.createCell(numCell++);
            cell.setCellValue(numRow);
        }
        
        //ecriture du fichier cree
        FileOutputStream fileOut = new FileOutputStream(targetFile);
        wb.write(fileOut);
        fileOut.close();
    }
        
    /**
     * retourne la priorite adequate sachant que :
     * <br><b>P3 optional</b> correspond a 1
     * <br><b>P2 normal</b> correspond a 10
     * <br><b>P1 important</b> correspond a 100 
     * <br><b>P0 essential</b> correspond a 1000 
     */
    private String getReqPriority(int priority){
        String pr ;
        if (priority == 1)
            pr = "P3 Optional";
        else if (priority == 10)
            pr = "P2 Normal";
        else  if (priority == 1000)
            pr = "P1 Important";
        else
            pr = "P0 Essential";
        return pr;
    }
        
    /**
     *retourne la categorie adequate sachant que :
     * <br><b>Functional</b> correspond a 0 
     * <br><b>Interoperability</b> correspond a 1 
     * <br><b>Load</b> correspond a 2
     * <br><b>Performance</b> correspond a 3
     * <br><b>Availability</b>, <b>Facility of recover</b>, <b>Reliability</b>, <b>Tolerance with the faults</b> correspondent a 4 soit <b>Disponibilite</b>
     * <br><b>Safety</b> correspond a 5
     * <br><b>Exploitability</b>, <b>Exploitation</b>, <b>Installation</b>, <b>Safeguard/restoration</b>, <b>Supervision</b> correspondent a 6 soit <b>Exploitabilite</b>
     * les autres possibilites correspondent a 7 
     */
    private String getReqCategory(int category){
        String cat ;
        if (category == 0)
            cat = "Functional";                 
        else if (category == 1)
            cat = "Interoperability";
        else if (category == 2)
            cat = "Load";
        else if (category == 3)
            cat = "Performance";
        else if (category == 4)
            cat = "Availability";
        else if (category == 5)
            cat ="Safety";
        else if (category == 6)
            cat = "Exploitability";
        else 
            cat = "Project specific";
        return cat;
    }
        
    /**
     * retourne la complexite adequatesachant que :
     * <br><b>C0 Very complex</b> equivaut a 1000 <b>C0</b>
     * <br><b>C1 Complex</b> equivaut a 100 <b>C1</b>
     * <br><b>C2 Medium</b> equivaut a 10<b>C2</b>
     * <br><b>C3 Simple</b> equivaut a 1<b>C3</b>
     */
    private String getReqComplexity(int complexity){
        String complex ;
        if (complexity == 1000)
            complex = "C0 Very complex";
        else if (complexity == 100)
            complex = "C1 Complex";
        else if (complexity == 10)
            complex = "C2 Medium";
        else 
            complex = "C3 Simple";
        return complex;
    }

    /**
     * retourne l'etat adequat sachant que :
     * <br><b>Analyzed</b> correspond a 1 soit <b>Analysee</b>
     * <br><b>Approved</b> correspond a 2 soit <b>Approuvee</b>
     */
    private String getReqState(int state){
        String stat ;
        if (state == 2)
            stat = "Approved";
        else 
            stat = "Analyzed";
        return stat;
    }

    /**
     * cree la feuille de calcul des attachements si elle ne l'est encore et y ecrit les informations sur les attachements 
     * d'un objet
     * @param elmt objet possedant des attachements
     * @throws IOException
     */
    private void writeAttachement(CommunData elmt) throws IOException{
        if (attachSheet == null){
            attachSheet = wb.createSheet("Attachements");
            HSSFRow titleRow = attachSheet.createRow(0);
            createColumnName(titleRow,"Path", cellStyle1);
            createColumnName(titleRow,"Test a executer", cellStyle1);
            createColumnName(titleRow,"Script", cellStyle1);
            createColumnName(titleRow,"Element", cellStyle2);
            createColumnName(titleRow,"Attachement", cellStyle2);
            createColumnName(titleRow,"Type d'element", cellStyle2);
        }
        List atts = elmt.getAttachments();
        HSSFRow row = attachSheet.createRow(attachSheet.getLastRowNum()+1);
        short numCell = 0;
        String script = "";
        if (attachSheet.getLastRowNum() == 1){
            BufferedReader in = new BufferedReader(new FileReader(ATTACH_SCRIPT_FILE));
            String s;
            while ((s = in.readLine()) != null)
                script += s + "\n";
        }               
        HSSFCell cell = row.createCell(numCell++);
        cell.setCellValue(linkPath);
        cell = row.createCell(numCell++);
        cell.setCellValue("Liens avec les attachements");
        cell = row.createCell(numCell++);
        cell.setCellValue(script);
        cell = row.createCell(numCell++);
        cell.setCellValue(elmt.getAbsolutePath());                              
        cell = row.createCell(numCell++);
        IAttachment att0 = (IAttachment)atts.get(0);
        String attachs = "";
        String path_beginning = sourceFile.substring(0, sourceFile.lastIndexOf(File.separator)) + File.separator ;
        if (att0 instanceof FileAttachment)
            attachs += path_beginning ;
        if (att0.getDescription() == null )
            attachs += att0.getLocation(); 
        else
            attachs += att0.getLocation() + " @ " + att0.getDescription();
        for (int num_att = 1; num_att < atts.size(); num_att++){        
            IAttachment att = (IAttachment)atts.get(num_att);
            attachs += " $ ";
            if (att instanceof FileAttachment)
                attachs += path_beginning;
            if (att.getDescription() == null )
                attachs += att.getLocation();
            else
                attachs += att.getLocation() + " @ " + att.getDescription();
        }
        cell.setCellValue(attachs);
        cell = row.createCell(numCell++);
        if (elmt instanceof RequirementFamily || elmt instanceof Requirement) 
            cell.setCellValue("Requirement");   
        else if (elmt instanceof Test)
            cell.setCellValue("Test");
        else if (elmt instanceof Step)
            cell.setCellValue("Step");
        else cell.setCellValue("Folder");               
    } 
        
    /**
     * cree la feuille de calcul des couvertures d'exigence si elle ne l'est encore et y ecrit les informations sur la couverture 
     * des exigences par ce test 
     * @param test test couvrant des exigences
     * @throws IOException
     */
    private void writeLinkRequirement(Test test) throws IOException {
        if (linkSheet == null){
            linkSheet = wb.createSheet("Couverture des exigences par les tests");
            HSSFRow titleRow = linkSheet.createRow(0);
            createColumnName(titleRow,"Path", cellStyle1);
            createColumnName(titleRow,"Test a executer", cellStyle1);
            createColumnName(titleRow,"Script", cellStyle1);
            createColumnName(titleRow,"Test", cellStyle2);
            createColumnName(titleRow,"Exigences couvertes", cellStyle2);
        }                       
        List reqs = test.getRequirements();
        if (reqs.size() > 0){                   
            short numCell=0;    
            HSSFRow linkRow = linkSheet.createRow(linkSheet.getLastRowNum()+1);
            String script = "";         
            if (linkSheet.getLastRowNum() == 1){
                BufferedReader in = new BufferedReader(new FileReader(COVERAGE_SCRIPT_FILE));
                String s;
                while ((s = in.readLine()) != null)
                    script += s + "\n";
            }                                   
            HSSFCell cell = linkRow.createCell(numCell++);
            cell.setCellValue(linkPath);
            cell = linkRow.createCell(numCell++);
            cell.setCellValue("Couverture des exigences");
            cell = linkRow.createCell(numCell++);
            cell.setCellValue(script);
            cell = linkRow.createCell(numCell++);
            cell.setCellValue(test.getAbsolutePath());
            String reqsList = ((Requirement) reqs.get(0)).getAbsolutePath();
            for (int num_req = 1; num_req < reqs.size(); num_req++)
                reqsList += " @ " + ((Requirement) reqs.get(num_req)).getAbsolutePath();                        
            cell = linkRow.createCell(numCell++);
            cell.setCellValue(reqsList);                                                
        }                               
    }
        
    /**
     * cree une feuille de calcul contenant le plan de test
     * @throws Exception
     */
    public void createTestSheet() throws Exception{
        HSSFSheet testSheet = wb.createSheet("Plan de test");
                                
        //creation des entetes de colonnes de test
        HSSFRow row = testSheet.createRow(0);   
        createColumnName(row,"Path", cellStyle1);
        createColumnName(row,"Nom du test", cellStyle1);
        createColumnName(row,"Description du test", cellStyle1);
        createColumnName(row,"Concepteur", cellStyle1);
        createColumnName(row,"Date de creation", cellStyle1);
        createColumnName(row,"Status", cellStyle1);
        createColumnName(row,"Nom de l'action",cellStyle2);
        createColumnName(row,"Description de l'action", cellStyle2);
        createColumnName(row,"Resultat attendu", cellStyle2);   
                                                
        //creation du contenu des cellules
        //parcours des familles
        HSSFCell cell;
        List families = parser.getFamilies();   
        for(int num_fam = 0; num_fam < families.size(); num_fam++){
            Family fam=(Family)families.get(num_fam);
            if (fam.getAttachments().size() > 0)
                writeAttachement(fam);
                        
            //parcours des suites de tests de la famille
            List testLists = fam.getTestSuites();
            if (testLists.size() > 0){
                for (int num_list = 0; num_list < testLists.size(); num_list++){
                    TestSuite list = (TestSuite)testLists.get(num_list);
                    if (list.getAttachments().size() > 0)
                        writeAttachement(list);
                                        
                    //parcours des tests de la suite de tests
                    List tests =list.getTests();        
                    if (tests.size() > 0){
                        for (int num_test = 0; num_test < tests.size(); num_test++){
                            Test test = (Test)tests.get(num_test);
                            if (test.getAttachments().size() > 0)
                                writeAttachement(test);                 
                            if (test.getRequirements().size() > 0)
                                writeLinkRequirement(test);
                                                        
                            //remplissage de la feuille des tests
                            List steps = test.getSteps(); 
                            if (steps.size() > 0)                                               
                                for (int num_step = 0; num_step < steps.size(); num_step++){
                                    //renseignement des cellules concernant les tests                                                   
                                    short numCell=0;
                                    row = testSheet.createRow(testSheet.getLastRowNum() + 1);
                                    cell = row.createCell(numCell++);
                                    cell.setCellValue(test.getPath());
                                    cell = row.createCell(numCell++);
                                    cell.setCellValue(test.getName());
                                    cell = row.createCell(numCell++);
                                    if (test.getDescription() != null && num_step == 0)
                                        cell.setCellValue(test.getDescription());                                                       
                                    cell = row.createCell(numCell++);
                                    //TODO cell.setCellValue(test.getDesigner().getName());
                                    cell.setCellValue("jeit8341");
                                    cell = row.createCell(numCell++);
                                    cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(test.getCreationDate()));
                                    cell = row.createCell(numCell++);
                                    cell.setCellValue(test.getStatus());                                                        
                                                                        
                                    //renseignement des cellules concernant les actions des tests
                                    Step step = (Step)steps.get(num_step);
                                    if (step.getAttachments().size() > 0)
                                        writeAttachement(step);
                                    cell = row.createCell(numCell++);
                                    cell.setCellValue(step.getName());
                                    cell = row.createCell(numCell++);
                                    if (step.getDescription() != null)
                                        cell.setCellValue(ParserUtil.formatParamsFromSalome(step.getDescription()));
                                    if (step.getResult() != null){
                                        cell = row.createCell(numCell++);
                                        cell.setCellValue(ParserUtil.formatParamsFromSalome(step.getResult())); 
                                    }
                                }       
                            else{
                                short numCell=0;
                                row = testSheet.createRow(testSheet.getLastRowNum() + 1);
                                cell = row.createCell(numCell++);
                                cell.setCellValue(test.getPath());
                                cell = row.createCell(numCell++);
                                cell.setCellValue(test.getName());
                                cell = row.createCell(numCell++);
                                if (test.getDescription() != null)
                                    cell.setCellValue(test.getDescription());                                           
                                cell = row.createCell(numCell++);
                                cell.setCellValue("jeit8341");
                                //TODO cell.setCellValue(test.getDesigner().getName());
                                cell = row.createCell(numCell++);
                                cell.setCellValue(new SimpleDateFormat("dd/MM/yyyy").format(test.getCreationDate()));
                                cell = row.createCell(numCell++);
                                cell.setCellValue(test.getStatus());    
                            }                                           
                        }
                    }
                    else{
                        short numCell=0;
                        row = testSheet.createRow(testSheet.getLastRowNum() + 1);
                        cell = row.createCell(numCell++);
                        cell.setCellValue(list.getAbsolutePath());
                        cell = row.createCell(numCell++);
                        cell.setCellValue("Test vide");
                    }
                }                               
            }
            else{
                short numCell=0;
                row = testSheet.createRow(testSheet.getLastRowNum() + 1);
                cell = row.createCell(numCell++);
                cell.setCellValue(fam.getAbsolutePath());
                cell = row.createCell(numCell++);
                cell.setCellValue("Test vide");
            }
        }
                
        //ecriture du fichier cree
        FileOutputStream fileOut = new FileOutputStream(targetFile);
        wb.write(fileOut);
        fileOut.close();
    }
        
    /**
     * recupere les informations provenant d'un export XML de SalomeTMF dans un fichier Excel a importer dans Quality Center
     * @throws Exception
     */
    public void convertToExcel() throws Exception{
        createReqSheet();
        createTestSheet();              
    }
                
}
