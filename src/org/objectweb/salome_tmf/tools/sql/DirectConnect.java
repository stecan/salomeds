/*
 * Copyright (C) 2017 Stefan Canali <Stefan.Canali@Kabelbw.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.objectweb.salome_tmf.tools.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.loggingData.LoggingData;

/**
 * This class is for direct connection to "attached" databases
 * 
 * @author Stefan Canali <Stefan.Canali@Kabelbw.de>
 */
public class DirectConnect {
    
    final static Logger LOGGER = Logger.getLogger(DirectConnect.class);
    
    static int numberOfConnections = 0;
    static HashMap <Connection, Integer> connectionMap = new HashMap<>();
    static HashMap <Integer, Connection> connectionMapRev = new HashMap<>();
    static HashMap <String, Integer> connectionString = new HashMap<>();
    static List<Connection> connectionArray = new ArrayList<>();
    static List<List<String>> allActionNames = new ArrayList<>();
    
    public static enum ResultState 
    {
        NO_TESTCASE,
        NO_DATA,
        EMPTY_EFFECTIVE_RESULT,
        OK
    };
    
    static ResultState resultState;
    
    
    public static Connection DirectConnect(String url) {
        Connection connection;
        int index;
        try {
            index = connectionString.get(url);
            connection = connectionMapRev.get(index);
        } catch (Exception ex) {
            connection = null;
            try {
                // load driver for database 
                Class.forName("com.mysql.jdbc.Driver");

                String dbUser = Api.getUser();              // user of database
                String dbPass = Api.getPassword();          // user password
                String db;
                if (url.contains("jdbc:mysql:")) {
                    db = url+"?"+"user="+
                            dbUser+"&"+"password="+dbPass;
                } else {
                    db = "jdbc:mysql:"+
                            url+"?"+"user="+
                            dbUser+"&"+"password="+dbPass;
                }
                // connect to database
                connection = DriverManager.getConnection(db);
                if (!connectionMap.containsKey(connection)) {
                    // new connection
                    connectionString.put(url, numberOfConnections);
                    connectionMap.put(connection, numberOfConnections);     // save association
                    connectionMapRev.put(numberOfConnections, connection);  // save reverse association
                    connectionArray.add(connection);                        // save connection
                    allActionNames.add(new ArrayList<>());                  // save reference
                    numberOfConnections++;
                }
            } catch (ClassNotFoundException | SQLException e) {
                // do nothing: This error is handled in the calling method!
            }
        }
        return connection;
       }
    
    /**
     * This method scans the "last effective results" from the 
     * attached databases. The last result in time is returned.
     * 
     * @param   connections to attached databases
     * @param   actionName
     * @return  last result in time
     */
    public static List<String> getLastEffectiveResults(
            List<Connection> connections, 
            String actionName) {
        List<String> result = new ArrayList<>();
        List<String> cas = new ArrayList<>();
        List<String> exec_cas = new ArrayList<>();
        List<String> res_exec_camp = new ArrayList<>();
        List<String> effective_res_action = new ArrayList<>();
        Date date_tmp = new Date(0);
        Time time_tmp = new Time(0);
        
        boolean cas_found = false;
        boolean exec_cas_found = false;
        boolean res_exec_camp_found = false;
        boolean effective_res_action_found = false;
        
        for (Connection connection : connections) {
            try {
                Statement stmt = connection.createStatement();
                String dbStr;
                ResultSet rs;

                cas.addAll(getCas(connection, actionName));
                for (String s : cas) {
                    cas_found = true;
                    dbStr = "select * from `EXEC_CAS` where CAS_TEST_id_cas = " + 
                            s + ";";
                    rs = stmt.executeQuery(dbStr);
                    exec_cas.clear();
                    res_exec_camp.clear();
                    while (rs.next()) {
                        exec_cas_found = true;
                        exec_cas.add(rs.getString("id_exec_cas"));
                        res_exec_camp_found = true;
                        res_exec_camp.add(
                                rs.getString("RES_EXEC_CAMP_id_res_exec_camp"));
                    }

                    int i = -1;
                    for (String s1 : exec_cas) {
                        i++;
                        dbStr = "select * from `EXEC_ACTION` where EXEC_CAS_id_exec_cas =" + 
                                s1 + ";";
                        rs = stmt.executeQuery(dbStr);
                        effective_res_action.clear();
                        while (rs.next()) {
                            String str = rs.getString("effectiv_res_action");
                            effective_res_action_found = true;
                            effective_res_action.add(str);
                        }


                        dbStr = "select * from `RES_EXEC_CAMP` where id_res_exec_camp =" + 
                                res_exec_camp.get(i) + ";";
                        rs = stmt.executeQuery(dbStr);
                        String date_str = "";
                        String time_str = "";
                        while (rs.next()) {
                            date_str = rs.getString("date_res_exec_camp");
                            time_str = rs.getString("heure_res_exec_camp");
                        }

                        // get time and date
                        Date date = convertDateString(date_str);
                        Time time = convertTimeString(time_str);

                        if (date.after(date_tmp)) {
                            if (effective_res_action.get(0) == null) {
                                // do nothing!
                            } else {
                                result.clear();
                                result = copyToResult(effective_res_action);
                                date_tmp = date;
                                time_tmp = time;
                            }
                        } else if (date.equals(date_tmp)) {
                            if (time.after(time_tmp)) {
                                if (effective_res_action.get(0) == null) {
                                    // do nothing!
                                } else {
                                    result.clear();
                                    result = copyToResult(effective_res_action);
                                    date_tmp = date;
                                    time_tmp = time;
                                }
                            } else {
                                // do nothing!
                            }
                        } else {
                            // do nothing!
                        }
                    }
                }
            } catch (SQLException e) {
                LOGGER.error(LoggingData.getData() + 
                        "\nMethod getLastEffectiveResults: " +
                        "\nSQLException: " + e.getMessage() + 
                        "\nSQLState: " + e.getSQLState() +
                        "\nVendorError: " + e.getErrorCode() );
            }
        }
        // set result status
        if (!cas_found) {
            resultState = ResultState.NO_TESTCASE;
        } else if (!exec_cas_found) {
            resultState = ResultState.NO_DATA;
        } else if (!res_exec_camp_found) {
            resultState = ResultState.NO_DATA;
        } else if (!effective_res_action_found) {
            resultState = ResultState.NO_DATA;
        } else {
            boolean empty = true;
            for (String r : result) {
                if (r == null) {
                    // do nothing
                } else if (r.isEmpty()) {
                    // do nothing
                } else {
                    empty = false;
                }
            }
            if (empty) {
                resultState = ResultState.EMPTY_EFFECTIVE_RESULT;
            } else {
                resultState = ResultState.OK;
            }
        }
        return result;
    }
    
    public static ResultState getResultState() {
        return resultState;
    }
    
    private static List<String> copyToResult (List<String> from) {
        List<String> result = new ArrayList<>();
        for (String s : from) {
            result.add(s);
        }
        return result;
    }

    private static Date convertDateString(String date_str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date parsed;
        try {
            parsed = format.parse(date_str);
        } catch (ParseException ex) {
            parsed = new Date(0);
        }
        
        return new java.sql.Date(parsed.getTime());
    }
            
    private static Time convertTimeString(String time_str) {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss");
        long t;
        try {
            t = format.parse(time_str).getTime();
        } catch (ParseException ex) {
            t = new Time(0).getTime();
        }
        return new Time(t);
    }

    /**
     * Local method to correct the Polarion key (sometimes, the polarion key is 
     * imported with an undersore '_').
     * 
     * @param   n1 = "XGPF-38375_Dataprocessing"
     * @param   n2 = "XGPF-38375"
     * @return  Corrected key format
     */
    // 
    // n1 = "XGPF-38375_Dataprocessing"
    // n2 = "XGPF-38375"
    private static String checkUnderscore(String n1, String n2) {
        String n;
        String regexp = "\\w{1,}-\\d{1,}_";
        Pattern pattern = Pattern.compile(regexp, 
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(n1);
        if (matcher.find()) {
            // n1 is in the wrong format!
            // n1 = "XGPF-38375_Dataprocessing"
            n = n2;
        } else {
            // n1 is in the correct format!
            // n1 = "XGPF-38375"
            n = n1;
        }
        return n;
    }
    
    private static List<String> getCas (Connection connection, 
            String actionName) {
        List<String> nom_cas = new ArrayList<>();
        Integer v = connectionMap.get(connection);
        List<String> actionNames = allActionNames.get(v);
        String[] split1 = actionName.split(" ");
        String[] split2 = actionName.split("_");
        String reducedName1 = split1[0];
        String reducedName2 = split2[0];
        String reducedName = checkUnderscore(reducedName1,reducedName2);
        for (String s : actionNames) {
            if (s.startsWith(reducedName)) {
                nom_cas.add(s);
            }
        }
        
        List<String> cas = new ArrayList<>();
        Connection c = connectionArray.get(v);
        for (String name : nom_cas) {
            Statement stmt;
            try {
                stmt = c.createStatement();
                String dbStr = "select * from `CAS_TEST` where nom_cas = '" + 
                        name + "\';";
                ResultSet rs = stmt.executeQuery(dbStr);
                while (rs.next()) {
                    cas.add(rs.getString("id_cas"));
                }
            } catch (SQLException ex) {
                // do nothing
            }
        }
        return cas;
    }
    
    public static void init() {
        Collection<Integer> values = connectionMap.values();
        for (int v : values) {
            if (allActionNames.get(v).isEmpty()) {
                Connection c = connectionArray.get(v);
                
                Statement stmt;
                try {
                    stmt = c.createStatement();
                    ResultSet rs = stmt.executeQuery("select * from `CAS_TEST` where 1;");
                    List<String> actionNames = new ArrayList<>();
                    while (rs.next()) {
                        actionNames.add(rs.getString("nom_cas"));
                    }
                    allActionNames.get(v).addAll(actionNames);
                } catch (SQLException ex) {
                    // do nothing
                }
            } else {
                // do nothing
            }
        }
    }

    /**
     * This method returns the Polarion revision number for the applied 
     * project
     * 
     * @param   idProject = applied project (number)
     */
    public static int getPolarionRevision(
            int idProject) {
        int revisionNumber = -1;    // on error or head revision
        Connection connection;
        connection = DirectConnect(Api.getUrl());
        
        try {
            Statement stmt = connection.createStatement();
            String dbStr;
            ResultSet rs;

            dbStr = "select * from `CONFIG` where id_projet = " + 
                    idProject + ";";
            rs = stmt.executeQuery(dbStr);
            String str;
            String value;
            while (rs.next()) {
                str = rs.getString("CLE");
                if (str.equals("PolarionRevision")) {
                    value = rs.getString("VALEUR");
                    if (value.equals("head")) {
                        // revisionNumber = -1;
                    } else {
                        revisionNumber = Integer.parseInt(value);
                    }
                } else if (str. equals("PolarionPrefix")) {
                    value = rs.getString("VALEUR");
                } else {
                    value = rs.getString("VALEUR");
                }
            }
            
        } catch (SQLException e) {
            LOGGER.error(LoggingData.getData() + 
                    "\nMethod: getPolarionRevision: " +
                    "\nSQLException: " + e.getMessage() + 
                    "\nSQLState: " + e.getSQLState() +
                    "\nVendorError: " + e.getErrorCode() );
        } catch (Exception e) {
            LOGGER.error(LoggingData.getData() + 
                    "\nMethod: getPolarionRevision: " +
                    "\nException: " + e.getMessage());           
        }
        return revisionNumber;
    }
    
    /**
     * This method returns the Polarion revision number for the applied 
     * project
     * 
     * @param   idProject = applied project (number)
     */
    public static String getPolarionPrefix(
            int idProject) {
        String prefix = "";
        Connection connection;
        connection = DirectConnect(Api.getUrl());
        
        try {
            Statement stmt = connection.createStatement();
            String dbStr;
            ResultSet rs;

            dbStr = "select * from `CONFIG` where id_projet = " + 
                    idProject + ";";
            rs = stmt.executeQuery(dbStr);
            String str;
            String value;
            while (rs.next()) {
                str = rs.getString("CLE");
                if (str.equals("PolarionRevision")) {
                    value = rs.getString("VALEUR");
                } else if (str. equals("PolarionPrefix")) {
                    value = rs.getString("VALEUR");
                    prefix = value;
                    break;
                } else {
                    value = rs.getString("VALEUR");
                    break;
                }
            }
            
        } catch (SQLException e) {
            LOGGER.error(LoggingData.getData() + 
                    "\nMethod: getPolarionRevision: " +
                    "\nSQLException: " + e.getMessage() + 
                    "\nSQLState: " + e.getSQLState() +
                    "\nVendorError: " + e.getErrorCode() );
        } catch (Exception e) {
            LOGGER.error(LoggingData.getData() + 
                    "\nMethod: getPolarionRevision: " +
                    "\nException: " + e.getMessage());           
        }
        return prefix;
    }

}
