/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.login;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.naming.ldap.LdapContext;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;
import org.apache.log4j.Category;

import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.JXHyperlink;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.api.sql.ISQLSession;
import org.objectweb.salome_tmf.ihm.admin.Administration;
import org.objectweb.salome_tmf.ihm.admin.AdministrationProject;
import org.objectweb.salome_tmf.ihm.main.BaseIHM;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.tools.sql.DirectConnect;

public class LoginSalomeTMF extends javax.swing.JApplet implements BaseIHM,
                                                                   WindowListener  {
    /**
     * Logger for this class
     */
    static final Category logger = Category.getInstance(LoginSalomeTMF.class);

    Vector porjectsList;
    JSObject myDocument;
    Vector usersProject;
    Vector adminsProject;
    UserWrapper superUser = null;

    /**************************************************************************/
    protected SalomeTMFContext pSalomeTMFContext;
    protected SalomeTMFPanels pSalomeTMFPanels;
    protected URL urlSalome;
    protected String strProject = null;
    protected String strLogin = null;
    /**************************************************************************/

    Vector languages = new Vector();

    ISQLSession pISQLSession;
    ISQLProject pISQLProject;
    ISQLPersonne pISQLPersonne;

    File salome_html = null;

    boolean init;

    String proj;

    String proj1;

    String expires;

    String admin_pwd = "";

    String usedLocale = "";
    boolean closeDB = true;
    
    boolean wrongPassword = false;
    int selectedTab = 0;                // selected tab of login window

    JavaScriptUtils pJSUtils;

    static String userAuthentification = "";

    public LoginSalomeTMF() {
        if (!inBrowser)
            onInit();
    }

    public void connectToAPI() {

        Util.log("[LoginSalomeTMF->connectToAPI] connection");

        if (Api.getNbConnect() > 0) {
            quit();
            closeDB = false;
            // return false;
        }
        Util.log("[LoginSalomeTMF->connectToAPI] nb connection active : "
                 + Api.getNbConnect());

        Api.openConnection(urlSalome);
        pISQLSession = Api.getISQLObjectFactory().getISQLSession();
        pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();

        usedLocale = Api.getUsedLocale();
        Util.log("[LoginSalomeTMF->connectToAPI] Used Local is " + usedLocale);
        Language.getInstance().setLocale(new Locale(usedLocale));
        languages = Api.getLocales();
        Util.log("[LoginSalomeTMF->connectToAPI] Available Languages is "
                 + languages);
        if (languages == null) {
            Util.log("[LoginSalomeTMF->connectToAPI] Set default languages list to fr");
            languages = new Vector();
            languages.add("fr");
        }

    }

    void quit() {

//        try {
//            getAppletContext().showDocument(new URL("http://wiki.objectweb.org/salome-tmf"));
//        } catch (Exception e) {
//            e.printStackTrace();
//            // logger.error("quit()", e);
//        }

    }

    void onStart() {
        //
    }

    void onInit() {
        File dir;
        String classPath = (new File(System.getProperty("java.class.path"))).getParent();
        if (classPath == null) {
            dir = new File("").getAbsoluteFile();
        } else 
        {
            if (classPath.contains(":") ||           // for Linux
                    classPath.contains(";")) {       // for Windows only
                // it runs under netbeans debugger!
                dir = new File("").getAbsoluteFile();
            } else {
                // normal call 
                dir = new File(classPath);
            }
        }
        
        try {
            // only for debugging purposes!
            // urlSalome = new URL("http://bhmf457a/salome_softwaretest_v3_integrationstest_eo2_r2-dev/index.html");
            String url_string = dir.toURI().toString();
            urlSalome = new URL(url_string);
        } catch (MalformedURLException ex) {
            logger.error("Cannot determine urlSalome!", ex);
        }

        LoggingData.setLocalUser(System.getProperty("user.name"));
        try {
            java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
            LoggingData.setLocalMachine(localMachine.getHostName().toString());
        } catch (UnknownHostException ex) {
            logger.error("Cannot determine Host address!", ex);
        }

        connectToAPI();

        if (Api.isConnected()) {
            initData();
            initComponents();
            init = true;
            initList();
            init = false;
        } else {
            JOptionPane.showMessageDialog(this, 
                    Language.getInstance().getText("Connexion_Erreur"), "Error", 
                    JOptionPane.ERROR_MESSAGE);
        }
        
        List<String> errorListCon = Api.getErrorListConnections();
        List<String> errorListProp = Api.getErrorListProperties();
        if (errorListCon.size() > 0) {
            String msg = Language.getInstance()
                    .getText("errorAttachedDatabases") + ":\n";
            int i = 0;
            for (String e : errorListCon) {
                msg = msg + errorListProp.get(i) + " = " + e + "\n";
                i++;
            }
            JOptionPane optionPane = new JOptionPane(msg, JOptionPane.WARNING_MESSAGE);
            JDialog dialog = optionPane.createDialog("Warning!");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true); // to visible the dialog
        }
        
        int count = 0;
        while (count < 5) {
            count++;
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                // do nothing
            }
            password_Field.requestFocus();
        }
    }

    void onStop() {

    }

    void initData() {
        try {
            ProjectWrapper[] tmpProjectArray = pISQLProject.getAllProjects();
            Vector tmpProjectVector = new Vector();
            for (int i = 0; i < tmpProjectArray.length; i++) {
                tmpProjectVector.add(tmpProjectArray[i]);
            }
            porjectsList = tmpProjectVector;
            Util.log("[LoginSalomeTMF->initData] Projects list is "
                     + porjectsList);
            if (porjectsList.size() > 0) {
                ProjectWrapper pProjectWrapper = (ProjectWrapper) porjectsList
                    .elementAt(0);
                UserWrapper[] tmpUserArray = pISQLProject
                    .getUsersOfProject(pProjectWrapper.getName());
                Vector tmpUserVector = new Vector();
                for (int i = 0; i < tmpUserArray.length; i++) {
                    tmpUserVector.add(tmpUserArray[i]);
                }
                usersProject = tmpUserVector;
                Util.log("[LoginSalomeTMF->initData] Users Project list is "
                         + usersProject + ", for project "
                         + pProjectWrapper.getName());
                UserWrapper[] tmpAdminArray = pISQLProject
                    .getAdminsOfProject(pProjectWrapper.getName());
                Vector tmpAdminVector = new Vector();
                for (int i = 0; i < tmpAdminArray.length; i++) {
                    tmpAdminVector.add(tmpAdminArray[i]);
                }
                adminsProject = tmpAdminVector;
                Util.log("[LoginSalomeTMF->initData] Admins project list is "
                         + adminsProject + ", for project "
                         + pProjectWrapper.getName());
            } else {
                usersProject = new Vector();
                adminsProject = new Vector();
            }
            superUser = pISQLPersonne.getUserByLogin("AdminSalome");
            if (superUser != null) {
                admin_pwd = superUser.getPassword();
                Util
                    .log("[LoginSalomeTMF->initData] superUser is "
                         + superUser);
            }
        } catch (Exception e) {
            Util.log("[LoginSalomeTMF->initData]" + e);
            porjectsList = null;
        }
    }

    /**
     * Initializes the application Salome
     *
     * @return
     */
    public static void main(String[] args) {
        /**
         * Je sais que je ne suis pas dans le browser
         **/
        inBrowser = false;
        new LoginSalomeTMF();
    }

    public void handlerCookies() throws IOException, URISyntaxException {

        /**
         * Si Je suis dans le browser je m'occupe des cookies
         * */
        pJSUtils = new JavaScriptUtils(this);
        CookieManager cm = new CookieManager(null /* in ram store */,
                                             CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(cm);
        CookieHandler handler = CookieHandler.getDefault();
        // handler.
        if (handler != null) {
            String cookieValue = null;
            Map<String, List<String>> headers = handler.get(urlSalome.toURI(),
                    new HashMap<String, List<String>>());
            List<String> values = headers.get("Cookie");
            if (values != null)
                for (Iterator<String> iter = values.iterator(); iter.hasNext();) {
                    String v = iter.next();

                    if (cookieValue == null)
                        cookieValue = v;
                    else
                        cookieValue = cookieValue + ";" + v;
                }

            System.out.println("cookieValue : " + cookieValue);

            headers = new HashMap<String, List<String>>();
            values = new Vector<String>();
            values.add("grege");
            headers.put("Cookie", values);
            handler.put(urlSalome.toURI(), headers);
        }

    }

    /** Initializes the applet Salome */
    @Override
    public void init() {
//        /**
//         * je met a jour l'url
//         * */
//        try {
//            /*
//             * I try to open the connexion with documentBase
//             */
//            urlSalome = getDocumentBase();
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            /*
//             * if i'm not in applet getDocumentbase() throw a nullPointer
//             * Exception
//             */
//            if (e instanceof NullPointerException) {
//                BasicService bs;
//                try {
//                    /* so i get the documentBase with a jnlp service */
//                    bs = (BasicService) ServiceManager.
//                            lookup("javax.jnlp.BasicService");
//                    urlSalome = bs.getCodeBase();
//
//                } catch (UnavailableServiceException e1) {
//                    e1.printStackTrace();
//                }
//            } else
//                e.printStackTrace();
//        }

        /**
         * Je fais une verification pour savoir si je suis dans le navigateur
         **/
//        try {
//            inBrowser = true;
//            JSObject myBrowser = JSObject.getWindow(this);
//            myDocument = (JSObject) myBrowser.getMember("document");
//
//            /*
//             * * write a cookie* computes the expiration date, good for 1 month
//             */
//            java.util.Calendar c = java.util.Calendar.getInstance();
//            c.add(java.util.Calendar.MONTH, 1);
//            expires = "; expires=" + c.getTime().toString();
//
//        } catch (JSException jsEx) {
//            inBrowser = false;
//        }

        inBrowser = false;
        try {
            onInit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        Util.log("[LoginSalomeTMF->destroy] closeConnection");
        if (closeDB) {
            Api.closeConnection();
        }

    }

    @Override
    public void stop() {

        Util.log("[LoginSalomeTMF] stop");
        if (salome_html != null) {
            salome_html.delete();
        }

    }

    @Override
    public void start() {

        Util.log("[LoginSalomeTMF] start");

    }

    public void error(String err) {

        javax.swing.JOptionPane.showMessageDialog(this, err, Language
                .getInstance().getText("Authentification"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
        wrongPassword = true;

    }

    private void projectChange(ProjectWrapper pProjectWrapper) {

        String project = pProjectWrapper.getName();
        proj = project;
        if (!init) {
            try {
                proj = project;
                // users = db_AdminVT.getAllUserOfProject(proj);

                UserWrapper[] tmpUserArray = pISQLProject
                    .getUsersOfProject(project);
                Vector tmpUserVector = new Vector();
                for (int i = 0; i < tmpUserArray.length; i++) {
                    tmpUserVector.add(tmpUserArray[i]);
                }
                usersProject = tmpUserVector;

                user_List.removeAllItems();

                // Enumeration e = users.keys();
                Enumeration e = usersProject.elements();
                while (e.hasMoreElements()) {
                    Object pUser = e.nextElement();
                    Util.log("[LoginSalomeTMF->projectChange] add user_List "
                             + pUser);
                    user_List.addItem(pUser);
                    if (((UserWrapper) pUser).getLogin().equals(System.getProperty("user.name"))) {
                        user_List.setSelectedItem(pUser);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }

    }

    private void projectChange1(ProjectWrapper pProjectWrapper) {
        String project = pProjectWrapper.getName();
        proj1 = project;
        if (!init) {
            try {
                proj1 = project;

                UserWrapper[] tmpAdminArray = pISQLProject
                    .getAdminsOfProject(project);
                Vector tmpAdminVector = new Vector();
                for (int i = 0; i < tmpAdminArray.length; i++) {
                    tmpAdminVector.add(tmpAdminArray[i]);
                }
                adminsProject = tmpAdminVector;
                user_List1.removeAllItems();

                Enumeration e = adminsProject.elements();
                while (e.hasMoreElements()) {
                    Object pUser = e.nextElement();
                    Util.log("[LoginSalomeTMF->projectChange1] add user_List1 "
                             + pUser);
                    user_List1.addItem(pUser);
                    if (((UserWrapper) pUser).getLogin().equals(System.getProperty("user.name"))) {
                        user_List1.setSelectedItem(pUser);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    private UserWrapper getUser() {

        UserWrapper returnUserWrapper = (UserWrapper) user_List
            .getSelectedItem();
        LoggingData.setLogin(returnUserWrapper.getLogin());
        return returnUserWrapper;
    }

    private UserWrapper getUser1() {

        UserWrapper returnUserWrapper = (UserWrapper) user_List1
            .getSelectedItem();
        LoggingData.setLogin(returnUserWrapper.getLogin());
        return returnUserWrapper;
    }

    private boolean validPassword() {

        // String paswd = (String) users.get(getUser());
        String paswd = getUser().getPassword();
        String typed_paswd = new String(password_Field.getPassword());
        if (paswd != null) {
            try {
                boolean returnboolean = org.objectweb.salome_tmf.api.MD5paswd
                    .testPassword(typed_paswd, paswd);

                return returnboolean;
            } catch (Exception e) {
                logger.error(LoggingData.getData() + "Database Authentification failed!", e);
            }
        }

        return false;
    }

    private boolean validPasswordLdap() {

        String typed_paswd = new String(password_Field.getPassword());
        String login = getUser().getLogin();
        boolean isConnected = false;
        
        if (typed_paswd != null) {
            try{
                LdapContext ctx = ActiveDirectory.getConnection(login, typed_paswd);
                ctx.close();
                isConnected = true;
            }
            catch(Exception e){
                logger.error(LoggingData.getData() + "Ldap: " + e.getMessage());
                //Failed to authenticate user!
            }
        }
        return isConnected;
    }

    private boolean validPassword1() {

        // String paswd = (String) users1.get(getUser1());
        String paswd = getUser1().getPassword();
        String typed_paswd = new String(password_Field1.getPassword());
        if (paswd != null) {
            try {
                boolean returnboolean = org.objectweb.salome_tmf.api.MD5paswd
                    .testPassword(typed_paswd, paswd);
                return returnboolean;
            } catch (Exception e) {
                logger.error(LoggingData.getData() + "Database Authentification failed!", e);
            }
        }
        return false;
    }

    private boolean validPasswordLdap1() {

        String typed_paswd = new String(password_Field1.getPassword());
        String login = getUser1().getLogin();
        boolean isConnected = false;

        if (typed_paswd != null) {
            try{
                LdapContext ctx = ActiveDirectory.getConnection(login, typed_paswd);
                ctx.close();
                isConnected = true;
            }
            catch(Exception e){
                logger.error(LoggingData.getData() + "Ldap: " + e.getMessage());
                }
        }
        return isConnected;
    }

    private boolean validPassword2() {

        String typed_paswd = new String(password_Field2.getPassword());
        Util.log("[LoginSalomeTMF->validPassword2] verify paswd " + typed_paswd
                 + " with " + admin_pwd);
        if (admin_pwd != null) {
            try {
                boolean returnboolean = org.objectweb.salome_tmf.api.MD5paswd
                    .testPassword(typed_paswd, admin_pwd);
                return returnboolean;
            } catch (Exception e) {
                logger.error(LoggingData.getData() + "Database Authentification failed!", e);
            }
        }
        return false;
    }

    private void initList() {

        project_List.removeAllItems();
        project_List1.removeAllItems();
        user_List.removeAllItems();
        user_List1.removeAllItems();
        languages_List.removeAllItems();
        user_List.removeAllItems();
        String projectCookie = "";
        String userCookie = "";

        try {
            if (inBrowser && myDocument != null) {

                String cookies = (String) myDocument.getMember("cookie");
                if (cookies != null) {
                    String[] tabCookie = cookies.split(";");
                    for (int i = 0; i < tabCookie.length; i++) {
                        if (tabCookie[i].trim().startsWith("Login"))
                            userCookie = tabCookie[i].split("=")[1];
                        if (tabCookie[i].trim().startsWith("Project"))
                            projectCookie = tabCookie[i].split("=")[1];
                    }
                }
            }
        } catch (JSException jsx) {
            // jsx.printStackTrace();
            System.out.println("no cookie found");
        }
        for (int i = 0; i < porjectsList.size(); i++) {
            project_List.addItem(porjectsList.elementAt(i));
            project_List1.addItem(porjectsList.elementAt(i));
            if (inBrowser
                && ((ProjectWrapper) porjectsList.elementAt(i)).getName()
                .equals(projectCookie)) {
                project_List.setSelectedItem(porjectsList.elementAt(i));

                // When i selected a project i update the user of the project
                try {
                    String project = ((ProjectWrapper) porjectsList
                                      .elementAt(i)).getName();
                    proj = project;
                    // users = db_AdminVT.getAllUserOfProject(proj);

                    UserWrapper[] tmpUserArray;

                    tmpUserArray = pISQLProject.getUsersOfProject(project);

                    Vector tmpUserVector = new Vector();
                    for (int k = 0; k < tmpUserArray.length; k++) {
                        tmpUserVector.add(tmpUserArray[k]);
                    }
                    usersProject = tmpUserVector;
                } catch (Exception e1) {
                    logger.error(LoggingData.getData() + 
                            "Error when loading project users!", e1);
                }

            }
        }

        Enumeration e = usersProject.elements();
        while (e.hasMoreElements()) {
            Object pUser = e.nextElement();
            Util.log("[LoginSalomeTMF->initList] add user_List " + pUser);
            user_List.addItem(pUser);
            if (((UserWrapper) pUser).getLogin().equals(System.getProperty("user.name"))) {
                user_List.setSelectedItem(pUser);
            }
        }
        e = adminsProject.elements();
        while (e.hasMoreElements()) {
            Object pUser = e.nextElement();
            Util.log("[LoginSalomeTMF->initList] add user_List1 " + pUser);
            user_List1.addItem(pUser);
            if (((UserWrapper) pUser).getLogin().equals(System.getProperty("user.name"))) {
                user_List1.setSelectedItem(pUser);
            }
        }

        for (int j = 0; j < languages.size(); j++) {
            languages_List.addItem(((String) languages.elementAt(j)).trim());
        }
        languages_List.setSelectedItem(usedLocale);
    }

    /**
     * Are we in a browser or outside
     */
    private static boolean inBrowser = true;

    Frame root;

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {
        // GEN-BEGIN:initComponents

        /** je récupère le parent **/

        mainPanel = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        project_List = new javax.swing.JComboBox();
        user_List = new javax.swing.JComboBox();
        password_Field = new javax.swing.JPasswordField();
        password_Field.addKeyListener(new passwordKeyListener(0));

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        b_start = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        project_List1 = new javax.swing.JComboBox();
        user_List1 = new javax.swing.JComboBox();
        password_Field1 = new javax.swing.JPasswordField();
        password_Field1.addKeyListener(new passwordKeyListener(1));

        jPanel8 = new javax.swing.JPanel();
        b_start1 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        password_Field2 = new javax.swing.JPasswordField();
        password_Field2.addKeyListener(new passwordKeyListener(2));

        jPanel12 = new javax.swing.JPanel();
        b_start2 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        languages_List = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jEditorPane2 = new javax.swing.JEditorPane();

        setForeground(new java.awt.Color(51, 51, 51));
        mainPanel.setBackground(new java.awt.Color(255, 255, 255));
        mainPanel.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(1, 1, 1, 1)));
        mainPanel.setForeground(new java.awt.Color(255, 102, 0));
        mainPanel.setOpaque(true);
        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.setLayout(new java.awt.GridLayout(2, 3, 10, 0));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setText(Language.getInstance().getText("Projet"));
        jLabel2.setOpaque(true);
        jPanel1.add(jLabel2);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setText(Language.getInstance().getText("Utilisateur"));
        jLabel3.setOpaque(true);
        jPanel1.add(jLabel3);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel4.setOpaque(true);
        jPanel1.add(jLabel4);

        project_List.setBackground(new java.awt.Color(255, 204, 0));
        project_List.setName("l_projet");

        jPanel1.add(project_List);

        user_List.setBackground(new java.awt.Color(255, 204, 0));

        jPanel1.add(user_List);

        jPanel1.add(password_Field);

        jPanel4.add(jPanel1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel1.setText(Language.getInstance().getText("Se_connecter_a_Salome_TMF"));
        jPanel2.add(jLabel1);

        jPanel4.add(jPanel2, java.awt.BorderLayout.NORTH);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setForeground(new java.awt.Color(51, 51, 51));
        b_start.setText(Language.getInstance().getText("Demarrer_Salome_TMF"));
        b_start.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {

                    SalomeLaunch(evt);

                }
            });

        jPanel3.add(b_start);

        jPanel4.add(jPanel3, java.awt.BorderLayout.SOUTH);

        mainPanel.addTab(Language.getInstance().getText("Demarrer_Salome_TMF"),
                         jPanel4);
        ChangeListener changeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
                int index = sourceTabbedPane.getSelectedIndex();
                switch (index) {
                    case 0:
                        password_Field.requestFocus();
                        break;
                    case 1:
                        password_Field1.requestFocus();
                        break;
                    case 2:
                        password_Field2.requestFocus();
                        break;
                    default:
                        break;
                }
            }
        };
        mainPanel.addChangeListener(changeListener);

        jPanel5.setLayout(new java.awt.BorderLayout());

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setForeground(new java.awt.Color(51, 51, 51));
        jPanel7.setLayout(new java.awt.GridLayout(2, 3, 10, 0));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setText(Language.getInstance().getText("Projet"));
        jLabel5.setOpaque(true);
        jPanel7.add(jLabel5);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setText(Language.getInstance().getText("Utilisateur"));
        jLabel6.setOpaque(true);
        jPanel7.add(jLabel6);

        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel7.setOpaque(true);
        jPanel7.add(jLabel7);

        project_List1.setBackground(new java.awt.Color(255, 204, 0));
        project_List1.setName("l_projet");

        jPanel7.add(project_List1);

        user_List1.setBackground(new java.awt.Color(255, 204, 0));

        
        jPanel7.add(user_List1);

        jPanel7.add(password_Field1);

        jPanel5.add(jPanel7, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        b_start1.setText(Language.getInstance()
                         .getText("Administrer_le_projet"));
        b_start1.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {

                    launchAdminProject(evt);

                }
            });

        jPanel8.add(b_start1);

        jPanel5.add(jPanel8, java.awt.BorderLayout.SOUTH);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel8.setText(Language.getInstance().getText("Se_connecter_en_Administrateur_de_projet"));
        jPanel9.add(jLabel8);
        jLabel8.getAccessibleContext().setAccessibleName(Language.getInstance().
                getText("Se_connecter_en_Administrateur_de_projet"));

        jPanel5.add(jPanel9, java.awt.BorderLayout.NORTH);

        mainPanel.addTab(Language.getInstance()
                         .getText("Administrer_un_projet"), jPanel5);

        jPanel6.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setForeground(new java.awt.Color(51, 51, 51));
        jPanel10.setLayout(new java.awt.BorderLayout());

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setLayout(new java.awt.GridLayout(2, 2, 10, 0));

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setText(Language.getInstance().getText("Utilisateur"));
        jLabel10.setOpaque(true);
        jPanel11.add(jLabel10);

        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel11.setOpaque(true);
        jPanel11.add(jLabel11);

        jTextField1.setBackground(new java.awt.Color(255, 204, 0));
        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Dialog", 1, 12));
        jTextField1.setText("AdminSalome");
        jPanel11.add(jTextField1);

        jPanel11.add(password_Field2);

        jPanel10.add(jPanel11, java.awt.BorderLayout.CENTER);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        b_start2.setText(Language.getInstance().getText("Administrer_Salome_TMF"));
        b_start2.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {

                    launchAdministration(evt);

                }
            });

        jPanel12.add(b_start2);

        jPanel10.add(jPanel12, java.awt.BorderLayout.SOUTH);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel12.setText(Language.getInstance().getText("Se_connecter_en_administrateur_de_Salome_TMF"));
        jPanel13.add(jLabel12);

        jPanel10.add(jPanel13, java.awt.BorderLayout.NORTH);

        jPanel6.add(jPanel10, java.awt.BorderLayout.CENTER);

        mainPanel.addTab(Language.getInstance().getText("Administrer_Salome_TMF"), jPanel6);

        jPanel14.setLayout(new java.awt.BorderLayout());

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setLayout(new java.awt.BorderLayout());

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel15.setText(Language.getInstance().getText("Choissisez_votre_langue_"));
        jPanel19.add(jLabel15);

        jPanel16.add(jPanel19, java.awt.BorderLayout.NORTH);

        jPanel17.setLayout(new java.awt.GridLayout(2, 3, 10, 0));

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setForeground(new java.awt.Color(51, 51, 51));
        jPanel17.add(jLabel13);

        jLabel14.setText(Language.getInstance().getText("Langue_"));
        jPanel17.add(jLabel14);

        jPanel17.add(jLabel16);

        jPanel17.add(jLabel17);

        languages_List.setBackground(new java.awt.Color(255, 204, 0));
        jPanel17.add(languages_List);

        jPanel17.add(jLabel19);

        jPanel16.add(jPanel17, java.awt.BorderLayout.CENTER);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        // jPanel18.setPreferredSize(new java.awt.Dimension(15, 33));
        jPanel18.add(jLabel9);

        jPanel18.add(jLabel18);

        jPanel16.add(jPanel18, java.awt.BorderLayout.SOUTH);

        jPanel14.add(jPanel16, java.awt.BorderLayout.CENTER);

        mainPanel.addTab(Language.getInstance().getText("Langues"), jPanel14);

        jPanel15.setLayout(new java.awt.BorderLayout());

        jEditorPane2.setEditable(false);
        HTMLDocument m_oDocument;
        HTMLEditorKit m_oHTML;
        m_oDocument = new HTMLDocument();
        m_oHTML = new HTMLEditorKit();
        jEditorPane2.setEditorKit(m_oHTML);
        jEditorPane2.setDocument(m_oDocument);
        try {
            jEditorPane2.setText(htmlAbout());
        } catch (NullPointerException e) {
            logger.error(LoggingData.getData() + 
                    "Can't parse about tab", e);
        }

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.add(jEditorPane2, java.awt.BorderLayout.CENTER);

        mainPanel.addTab(Language.getInstance().getText("A_propos"), jPanel15);
        mainPanel.setPreferredSize(new Dimension(900, 230));
        contain = new JLayeredPane();
        // JPanel contain = new JPanel();
        contain.setLayout(new GridBagLayout());
        // contain.add

        contain.add(mainPanel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(30, 10, 10, 10), 0, 0), 1);

        busyLabel = new JXBusyLabel(new Dimension(110, 110));
        busyLabel.getBusyPainter().setHighlightColor(Color.black);
        busyLabel.getBusyPainter().setBaseColor(new Color(170, 170, 170));
        busyLabel.getBusyPainter().setPoints(25);
        busyLabel.getBusyPainter().setTrailLength(5);
        busyLabel.getBusyPainter().setFrame(3);
        busyLabel.setBusy(true);
        busyLabel.setVisible(false);

        contain.add(busyLabel, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                GridBagConstraints.NORTH, GridBagConstraints.NONE, new Insets(50, 0, 10, 0), 0, 0), 0);

        project_List1.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {

                    project_List1ItemStateChanged(evt);
                    password_Field1.requestFocus();                    

                }
            });
        user_List1.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {

                    user_ListItemStateChanged(evt);
                    password_Field1.requestFocus();                    

                }
            });
        
        project_List.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {

                    project_List1ItemStateChanged(evt);
                    password_Field.requestFocus();                    

                }
            });
        user_List.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {

                    user_ListItemStateChanged(evt);
                    password_Field.requestFocus();                    

                }
            });

        
        JXHyperlink contactAdmin = new JXHyperlink();
        contactAdmin.setName("contactAdmin");
        try {
            contactAdmin.setURI(new URI("mailto:Stefan.Canali@Dentsplysirona.com"));
            ;
            JPanel problem = new JPanel();
            problem.setLayout(new GridBagLayout());
            problem.setBackground(Color.white);

            problem.add(new JLabel(Language.getInstance().getText("Pour_tout_question_ou_prob")
                    + " : "), 
                    new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,
                            GridBagConstraints.CENTER, GridBagConstraints.NONE,
                            new Insets(0, 0, 0, 0), 0, 0), 0);
            problem.add(contactAdmin, 
                    new GridBagConstraints(1, 0, 1, 1, 1.0,
                            1.0, GridBagConstraints.CENTER, 
                            GridBagConstraints.NONE,
                            new Insets(0, 0, 0, 0), 0, 0), 0);

            contain.add(problem, 
                    new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0,
                            GridBagConstraints.CENTER, GridBagConstraints.NONE,
                            new Insets(50, 0, 10, 0), 0, 0), 0);
        } catch (URISyntaxException e1) {
            e1.printStackTrace();
        }

        if (!inBrowser) {
            dialogSalome = new JFrame();
            dialogSalome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);       
            dialogSalome.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    Util.log("[LoginSalomeTMF->destroy] closeConnection");
                    if (closeDB) {
                        Api.closeConnection();  // Close all connections!
                    }
                    e.getWindow().dispose();
                }
            });            
            dialogSalome.add(contain, java.awt.BorderLayout.CENTER);
            dialogSalome.setTitle(Api.getTitle());
            dialogSalome.pack();
            dialogSalome.setVisible(true);
        } else {
            getContentPane().setBackground(Color.WHITE);
            getContentPane().removeAll();
            getContentPane().add(contain, java.awt.BorderLayout.CENTER);
            getContentPane().validate();
            getContentPane().repaint();
        }
    }

    JXBusyLabel busyLabel;
    JLayeredPane contain;

    String htmlAbout() {
        
        String version = "3.2.1.310";
        LoggingData.setVersion(version);

        String returnString = "<body>"
            + "<div style=\"text-align: center;\">"
            + "<div style=\"text-align: left;\">"
            + "<div style=\"text-align: center;\"><small><small><span style=\"font-weight: bold;\"><big><big>Salome-TMF</big></big></span>"
            + "<div style=\"text-align: center;\"><small><small><span style=\"font-weight: bold;\"><big><big>Version " + version + "</big></big></span>"
            + "</small></small></div>"
            + "<div style=\"text-align: center;\"><small><small><span style=\"font-weight: bold;\"><a href=\"https://wiki.objectweb.org/salome-tmf/\">https://wiki.objectweb.org/salome-tmf/</a></span>"
            + "<span style=\"font-weight: bold;\"></span></small></small></div>"
            + "<div style=\"text-align: left;\"><small><small><span style=\"font-weight: bold;\">SalomeTMF is a Test Management Framework, Copyright (C) 2010 France Telecom R&amp;D"
            + "<ul>"
            + "<li>Jean-Yves Haguet (Project manager, developer)</li>"
            + "<li>Véronique Theault (functional project manager, validation, training, documentation)</li>"
            + "<li>Guillaume Favro (Developer , plugin developer, documentation)</li>"
            + "<li>Pierre Paulus (Developer, plugin developer, project manager AQ Salome)</li>"
            + "<li>Frank Simonet (installation and technical support)</li>"
            + "<li>Jérôme Peignien (validation, training, documentation)</li>"
            + "<li>Stefan Canali (modifications due to Sirona test requirements)</li>"
            + "<li>David Lukartono (modifications due to Sirona test requirements)</li>"
            + "</ul>" + "</span>" + "</body>";

        return returnString;
        // "</html>";
    }

    private void project_List1ItemStateChanged(java.awt.event.ItemEvent evt) {

        projectChange1((ProjectWrapper) evt.getItem());

    }

    private void launchAdminProject(java.awt.event.ActionEvent evt) {
        if (wrongPassword) {
            wrongPassword = false;
        } else {
            // GEN-FIRST:event_b_start1ActionPerformed
            usedLocale = ((String) languages_List.getSelectedItem()).trim();
            Api.saveLocale(usedLocale);
            Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
            if (testAuthentification1()) {
                busyLabel.setVisible(true);
                mainPanel.setEnabled(false);

                strProject = ((ProjectWrapper) project_List1.getSelectedItem())
                    .getName();
                LoggingData.setProject(strProject);

                SwingWorker worker = new SwingWorker() {

                        @Override
                        protected Object doInBackground() throws Exception {

                            try {

                                AdministrationProject administrationProject = 
                                        new AdministrationProject(LoginSalomeTMF.this);
                                administrationProject.setAppletParent(LoginSalomeTMF.this);
                                administrationProject
                                    .setStrProject(((ProjectWrapper) project_List1.
                                            getSelectedItem()).getName());
                                administrationProject.setStrUser(getUser1().getLogin());
                                administrationProject.setDocumentBase(urlSalome);
                                administrationProject.init();

                                logger.info(LoggingData.getData() + "Project Administration: " + "Successfully logged in!");

                                if (!inBrowser) {
                                    selectedTab = 1;
                                    dialogSalome.setVisible(false);
                                    dialogSalome = new JFrame();
                                    dialogSalome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);            
                                    dialogSalome.add(administrationProject.getTabs(),
                                                     java.awt.BorderLayout.CENTER);
                                    dialogSalome.setTitle(Api.getTitle());
                                    dialogSalome.setVisible(true);
                                    dialogSalome.pack();
                                } else {

                                    getContentPane().removeAll();
                                    getContentPane().add(administrationProject.getTabs(),
                                                         java.awt.BorderLayout.CENTER);
                                    getContentPane().validate();
                                    getContentPane().repaint();

                                }
                                repaint();
                            } catch (Exception me) {
                                me.printStackTrace();

                                busyLabel.setVisible(false);
                                mainPanel.setEnabled(true);
                            }

                            return null;
                        }
                    };
                worker.execute();
            } else {
                error(Language.getInstance().getText("Mot_de_passe_invalide"));
            }
        }
    }

    public void setAppletCloseListener(ActionListener l) {

    }

    private void launchAdministration(java.awt.event.ActionEvent evt) {

        if (wrongPassword) {
            wrongPassword = false;
        } else {
            usedLocale = ((String) languages_List.getSelectedItem()).trim();
            Api.saveLocale(usedLocale);
            Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
            if (validPassword2()) {
                busyLabel.setVisible(true);
                mainPanel.setEnabled(false);
                SwingWorker worker = new SwingWorker() {

                        @Override
                        protected Object doInBackground() throws Exception {

                            try {
                                // int idConnection = Api.addConnection("ALL",
                                // "AdminSalome");

                                Administration administration = new Administration(
                                        LoginSalomeTMF.this);
                                administration.setDocumentBase(urlSalome);
                                administration.setAppletParent(LoginSalomeTMF.this);
                                administration.init();

                                // pJSUtils.setCookies("ALL", "AdminSalome", new String(
                                // password_Field2.getPassword()), Api
                                // .getUserAuthentification());

                                logger.info(LoggingData.getData() + 
                                        "Global Administration: " + 
                                        "Successfully logged in!");

                                if (!inBrowser) {
                                    selectedTab = 2;
                                    dialogSalome.setVisible(false);
                                    dialogSalome = new JFrame();
                                    dialogSalome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);            
                                    dialogSalome.add(administration.getTabs(),
                                                     java.awt.BorderLayout.CENTER);
                                    dialogSalome.setTitle(Api.getTitle());
                                    dialogSalome.setVisible(true);
                                    dialogSalome.pack();
                                } else {
                                    getContentPane().removeAll();
                                    getContentPane().add(administration.getTabs(),
                                            java.awt.BorderLayout.CENTER);
                                    getContentPane().validate();
                                    getContentPane().repaint();
                                }
                                repaint();
                            } catch (Exception me) {
                                me.printStackTrace();

                                busyLabel.setVisible(false);
                                mainPanel.setEnabled(true);
                                Util.log("[LoginSalomeTMF->b_start2ActionPerformed]"
                                         + me);
                            }
                            return null;
                        }

                    };
                worker.execute();
            } else {
                error(Language.getInstance().getText("Mot_de_passe_invalide"));
            }
        }
    }

    private void project_ListItemStateChanged(java.awt.event.ItemEvent evt) {
        // GEN-FIRST:event_project_ListItemStateChanged
        projectChange((ProjectWrapper) evt.getItem());
    }

    private void user_ListItemStateChanged(java.awt.event.ItemEvent evt) {
        // GEN-FIRST:event_user_ListItemStateChanged
    }

    private boolean testAuthentification() {

        boolean retourPassword = false;
        if (Api.getUserAuthentification().equalsIgnoreCase("Database")) {
            LoggingData.setAuthentification("Database");
            retourPassword = validPassword();
        } else if (Api.getUserAuthentification().equalsIgnoreCase("Ldap")) {
            LoggingData.setAuthentification("Ldap");
            retourPassword = validPasswordLdap();
        } else if (Api.getUserAuthentification().equalsIgnoreCase("both")) {
            retourPassword = validPassword();
            if (retourPassword) {
                LoggingData.setAuthentification("Database");
                // Api.setUserAuthentification("Database");
            } else {
                retourPassword = validPasswordLdap();
                LoggingData.setAuthentification("Ldap");
                // Api.setUserAuthentification("Ldap");
            }
        }
        return retourPassword;
    }

    private boolean testAuthentification1() {
        boolean retourPassword1 = false;
        if (Api.getUserAuthentification().equalsIgnoreCase("Database")) {
            LoggingData.setAuthentification("Database");
            retourPassword1 = validPassword1();
        } else if (Api.getUserAuthentification().equalsIgnoreCase("Ldap")) {
            LoggingData.setAuthentification("Ldap");
            retourPassword1 = validPasswordLdap1();
        } else if (Api.getUserAuthentification().equalsIgnoreCase("both")) {
            retourPassword1 = validPassword1();
            if (retourPassword1) {
                LoggingData.setAuthentification("Database");
                // Api.setUserAuthentification("Database");
            } else {
                retourPassword1 = validPasswordLdap1();
                LoggingData.setAuthentification("Ldap");
                // Api.setUserAuthentification("Ldap");
            }
        }
        return retourPassword1;
    }

    private JFrame dialogSalome;

    /**
     * the current pressed button
     */
    private JButton currentPresedButton;

    private void SalomeLaunch(java.awt.event.ActionEvent evt) {
        if (wrongPassword) {
            wrongPassword = false;
        } else {
            usedLocale = ((String) languages_List.getSelectedItem()).trim();
            Api.saveLocale(usedLocale);
            Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
            if (testAuthentification()) {

                busyLabel.setVisible(true);
                mainPanel.setEnabled(false);

                strLogin = ((UserWrapper) user_List.getSelectedItem()).getLogin();

                strProject = ((ProjectWrapper) project_List.getSelectedItem())
                    .getName();
                LoggingData.setProject(strProject);

                if (inBrowser && myDocument != null) {
                    myDocument.setMember("cookie", "Login=" + strLogin + expires);
                    myDocument.setMember("cookie", "Project=" + strProject
                                         + expires);
                }

                // this.getContentPane().setEnabled(false);
                SwingWorker worker = new SwingWorker() {

                        @Override
                        protected Object doInBackground() throws Exception {

                            try {
                                try {
                                    Class lnfClass = Class.forName(UIManager.
                                            getSystemLookAndFeelClassName());
                                    LookAndFeel newLAF = (LookAndFeel) (lnfClass.newInstance());
                                    UIManager.setLookAndFeel(newLAF);
                                    Util.adaptFont();

                                } catch (Exception exc) {
                                    exc.printStackTrace();

                                    Util.debug("Error loading L&F: " + exc);
                                }
                                Api.openConnection(urlSalome);
                                if (!Api.isConnected()) {
                                    return null;
                                }

                                Api.initConnectionUser(strProject, strLogin);
                                javax.swing.JOptionPane.getFrameForComponent(
                                        LoginSalomeTMF.this).addWindowListener(LoginSalomeTMF.this);
                                pSalomeTMFContext = new SalomeTMFContext(
                                        Api.getCodeBase(), 
                                        javax.swing.JOptionPane.getFrameForComponent(LoginSalomeTMF.this),
                                        LoginSalomeTMF.this);
                                pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, 
                                        LoginSalomeTMF.this);
                                pSalomeTMFPanels.initComponentPanel();

                                DataModel.loadFromBase(strProject, strLogin,
                                                       LoginSalomeTMF.this);
                                pSalomeTMFPanels.loadModel(strProject, strLogin);

                                // waitView.dispose();
                                pSalomeTMFContext.loadPlugin(pSalomeTMFPanels);

                                pSalomeTMFContext.startPlugin();

                                logger.info(LoggingData.getData() + "Successfully logged in!");

                                if (!inBrowser) {
                                    selectedTab = 0;
                                    dialogSalome.setVisible(false);
                                    dialogSalome = new JFrame();
                                    dialogSalome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);            
                                    dialogSalome.add(pSalomeTMFPanels.tabs,
                                                     java.awt.BorderLayout.CENTER);
                                    dialogSalome.setTitle(Api.getTitle());
                                    dialogSalome.setVisible(true);
                                    dialogSalome.pack();
                                } else {
                                    getContentPane().removeAll();
                                    getContentPane().add(pSalomeTMFPanels.tabs,
                                                         java.awt.BorderLayout.CENTER);
                                    getContentPane().validate();
                                    getContentPane().repaint();
                                }
                            } catch (Throwable me) {

                                busyLabel.setVisible(false);
                                mainPanel.setEnabled(true);
                                Util.log("[LoginSalomeTMF->b_startActionPerformed]"
                                         + me);
                                Util.err(me);
                            }
                            DirectConnect.init();
                            return null;
                        }
                        
                    };
                worker.execute();

            } else {
                error(Language.getInstance().getText("Mot_de_passe_invalide"));
            }
        }
    }

    void loadModel() {

        if (strProject == null || strLogin == null) {
            quit(true, true);
        }
        DataModel.loadFromBase(strProject, strLogin, this);
        pSalomeTMFPanels.loadModel(strProject, strLogin);
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton b_start;
    private javax.swing.JButton b_start1;
    private javax.swing.JButton b_start2;
    private javax.swing.JEditorPane jEditorPane2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane mainPanel;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox languages_List;
    private javax.swing.JPasswordField password_Field;
    private javax.swing.JPasswordField password_Field1;
    private javax.swing.JPasswordField password_Field2;
    private javax.swing.JComboBox project_List;
    private javax.swing.JComboBox project_List1;
    private javax.swing.JComboBox user_List;
    private javax.swing.JComboBox user_List1;

    class passwordKeyListener implements KeyListener {

        int paswdType = -1;

        passwordKeyListener(int t) {
            paswdType = t;
        }

        @Override
        public void keyTyped(KeyEvent ke) {

        }

        @Override
        public void keyPressed(KeyEvent ke) {

        }

        @Override
        public void keyReleased(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                if (paswdType == 0) {
                    SalomeLaunch(null);
                } else if (paswdType == 1) {
                    launchAdminProject(null);
                } else if (paswdType == 2) {
                    launchAdministration(null);
                }
            }
        }
    }

    @Override
    public SalomeTMFContext getSalomeTMFContext() {
        return pSalomeTMFContext;
    }

    @Override
    public SalomeTMFPanels getSalomeTMFPanels() {
        return pSalomeTMFPanels;
    }

    @Override
    public boolean isGraphique() {
        return true;
    }

    @Override
    public void showDocument(URL toShow, String where) {

        // getAppletContext().showDocument(toShow, where);
        try {
            // Lookup the javax.jnlp.BasicService object
            BasicService bs = (BasicService) ServiceManager
                .lookup("javax.jnlp.BasicService");
            // Invoke the showDocument method
            bs.showDocument(toShow);

        } catch (UnavailableServiceException ue) {
            ue.printStackTrace();

            // Service is not supported

        }

    }

    public JFrame getDialogSalome() {

        return dialogSalome;
    }

    public boolean isInBrowser() {

        return inBrowser;
    }

    public Frame getRoot() {

        return root;
    }

    @Override
    public void quit(boolean do_recup, boolean doclose) {

        if (pSalomeTMFContext != null && doclose == true) {
            pSalomeTMFContext.suspendPlugin();
        }

        // remove TestPanel
        dialogSalome.removeAll();
        dialogSalome.dispose();
        dialogSalome.repaint();

        // Do not show the progress indicator
        busyLabel.setBusy(true);
        busyLabel.setVisible(false);

        // Show login panel
        dialogSalome = new JFrame();

        // Note:
        // The components are disabled by calling "worker.execute();"
        //
        // Thus, the components have to be enabled!
        Component[] components = contain.getComponents();
        for (Component c : components) {
            c.setEnabled(true);
        }
        dialogSalome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);            
        dialogSalome.add(contain, java.awt.BorderLayout.CENTER);
        dialogSalome.setTitle(Api.getTitle());
        dialogSalome.pack();
        dialogSalome.setVisible(true);
        
        // Set focus to password shown field
        switch (selectedTab) {
            case 0:
                password_Field.setText("");
                password_Field.requestFocus();
                break;
            case 1:
                password_Field1.setText("");
                password_Field1.requestFocus();
                break;
            case 2:
                password_Field2.setText("");
                password_Field2.requestFocus();
                break;
                
        }
    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }
}
