/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;


import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;


public class PanelAvecFond extends JPanel {
    Image backGround = null;
    public PanelAvecFond( Image backGround)
    {
        this.backGround = backGround;
        setOpaque( false );
    }
    
    @Override
    public void paint( Graphics g )
    {
        if ( backGround != null )
            g.drawImage( backGround, 0, 0,getSize().width, getSize().height, this );
        super.paint( g );
    }
    
    public static void main(String[] args )
    {
        JTextArea groupDescriptionArea = new JTextArea();
        groupDescriptionArea.setPreferredSize(new Dimension(100,50));
                
        JScrollPane groupDescriptionScrollPane = new JScrollPane(groupDescriptionArea);
                
        groupDescriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),"Description")); 
        groupDescriptionScrollPane.setBounds(150,150,100,100);
        JFrame frm = new JFrame( "test panel back ground" );
        frm.setBounds( 100, 100, 500, 500 );
        frm.getContentPane().setLayout( new BorderLayout() );
        JPanel pnl = new PanelAvecFond( loadImages( frm, ".//salome_intro.jpg" ) );
        pnl.setLayout( null );//new GridLayout( 10, 10 ) );
        JButton btn = new PanelAvecFond.ButtonAvecFond( loadImages( frm, ".//icon31.jpg" ) );
        btn.setForeground( Color.green );
        btn.setText( "TOTO" );
        pnl.add( btn );
        pnl.add(groupDescriptionScrollPane);
        btn.setBounds( 0, 50, 100, 100 );
        frm.getContentPane().add( pnl, BorderLayout.CENTER );

        frm.setVisible( true );
        frm.addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e)
                {
                    super.windowClosing(e);
                    System.exit( 0 );
                }
            });
    }
    
    private static Image loadImages( JFrame frm, String imageFile )
    {
        try
            {
                MediaTracker mTrack = new MediaTracker( frm ); // load les image avan de les afficher 
                Image image = frm.getToolkit().getImage( getURL( imageFile  ) );
                mTrack.addImage( image, 0 );
                mTrack.waitForAll();
                return image;
            }
        catch (Exception e)
            {
                //Util.debug( " getimages : " + e );
            }
        return null;
    }

    public static URL getURL( String file )
        throws MalformedURLException
    {
        URL documentBase = new URL("file:///" + System.getProperty("user.dir") + "/");
        return new URL( documentBase, file );
    }
    
    public static class ButtonAvecFond extends JButton
    {
        Image backGround = null;
        public ButtonAvecFond( Image backGround )
        {
            this.backGround = backGround;
            setOpaque( false );
        }
        
        @Override
        public void paint( Graphics g )
        {
            if ( backGround != null )
                g.drawImage( backGround, 0, 0,getSize().width, getSize().height, this );
            //                  ButtonModel model = getModel();
            //                  if ( !model.isArmed() )
            super.paint( g );
        }
    
    }
}


