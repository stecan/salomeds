package org.objectweb.salome_tmf.ihm.main;

import org.objectweb.salome_tmf.data.Attachment;

public interface IBugJDialog {
    public void onViewPerformed();
        
    public void onModifyPerformed() ;
        
    public void onCancelPerformed();
        
    public void onCommitPerformed(Attachment bugURL);
}
