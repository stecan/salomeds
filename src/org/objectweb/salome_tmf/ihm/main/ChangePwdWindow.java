/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;


public class ChangePwdWindow extends JDialog {
    
    JPanel panel = new JPanel(new BorderLayout());
    
    /**
     * Ancien mot de passe
     */
    JPasswordField oldUserPassword = new JPasswordField(20);
    
    /**
     * Nouveau mot de passe : premier
     */
    JPasswordField newUserPasswordFirst = new JPasswordField(20);
    
    /**
     * Nouveau mot de passe : second
     */
    JPasswordField newUserPasswordSecond = new JPasswordField(20);
    
    /** Creates a new instance of ChangePwdWindow */
    public ChangePwdWindow(Frame owner) {
        
        super(owner,true);
        JButton validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton.setToolTipText(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (Api.isConnected()) {
                    
                        //String pass = ConnectionData.getAdminVTSelect().getUserPassword(ProjectData.getCurrentUser().getLogin());
                        String pass = "";
                        try {
                            pass = DataModel.getCurrentUser().getPasswordFromDB();
                        } catch (Exception passEx){
                            Util.err("[ChangePwdWindow->validateButton] Exception ", passEx);
                            JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                          passEx,
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                            return; 
                        }
                        //if (oldUserPassword.getText().trim().equals(pass)) {
                        boolean _valid = false;
                        try {
                            _valid = MD5paswd.testPassword(new String(oldUserPassword.getPassword()).trim(),pass);
                        }catch(Exception ex){
                            JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                          ex,
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                            return; 
                        }
                        if (_valid) {
                            if (new String(newUserPasswordFirst.getPassword()).trim().equals(new String(newUserPasswordSecond.getPassword()).trim())) {
                                //int transNumber = -1;
                                try {
                                    //transNumber = Api.beginTransaction(ApiConstants.UPDATE_PASSWORD);
                                    //ConnectionData.getAdminVTUpdate().updateAdminPassword(ProjectData.getCurrentUser().getLogin(),new String(newUserPasswordSecond.getPassword()).trim());
                                        
                                    DataModel.getCurrentUser().updatePasswordInDB(new String(newUserPasswordSecond.getPassword()).trim());
                                        
                                    oldUserPassword.setText("");
                                    newUserPasswordFirst.setText("");
                                    newUserPasswordSecond.setText("");
                                    JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                                  Language.getInstance().getText("Votre_mot_de_passe_a_ete_modifie"),
                                                                  Language.getInstance().getText("Succes_"),
                                                                  JOptionPane.INFORMATION_MESSAGE);
                                    //Api.commitTrans(transNumber);
                                    ChangePwdWindow.this.dispose();
                                } catch (Exception exception) {
                                    //Api.forceRollBackTrans(transNumber);
                                    Tools.ihmExceptionView(exception);
                                }
                            
                            } else {
                                newUserPasswordFirst.setText("");
                                newUserPasswordSecond.setText("");
                                JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                              Language.getInstance().getText("Impossible_de_changer_le_mot_de_passe_Vous_n_avez_pas_entrer_deux_fois_le_meme_mot_de_passe"),
                                                              Language.getInstance().getText("Erreur_"),
                                                              JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            oldUserPassword.setText("");
                            JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                          Language.getInstance().getText("Impossible_de_changer_le_mot_de_passe_L_ancien_mot_de_passe_est_incorrect"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(ChangePwdWindow.this,
                                                      Language.getInstance().getText("Impossible_de_changer_le_mot_de_passe_Vous_n_etes_pas_connecter_a_la_base"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    oldUserPassword.setText(null);
                    newUserPasswordFirst.setText(null);
                    newUserPasswordSecond.setText(null);
                    ChangePwdWindow.this.dispose();
                
                }
            });
        
        JLabel oldQuestion = new JLabel(Language.getInstance().getText("Entrez_votre_ancien_mot_de_passe_"));
        
        JLabel question = new JLabel(Language.getInstance().getText("Entrez_votre_nouveau_mot_de_passe_"));
        
        JLabel confirmation = new JLabel(Language.getInstance().getText("Confirmer_le_nouveau_mot_de_passe_"));
        
        
        JPanel textFieldPane = new JPanel();
        textFieldPane.setLayout(new BoxLayout(textFieldPane,BoxLayout.Y_AXIS));
        textFieldPane.add(oldUserPassword);
        textFieldPane.add(Box.createRigidArea(new Dimension(1,50)));
        textFieldPane.add(newUserPasswordFirst);
        textFieldPane.add(Box.createRigidArea(new Dimension(1,50)));
        textFieldPane.add(newUserPasswordSecond);
        
        JPanel textPane = new JPanel();
        textPane.setLayout(new BoxLayout(textPane,BoxLayout.Y_AXIS));
        textPane.add(oldQuestion);
        textPane.add(Box.createRigidArea(new Dimension(1,50)));
        textPane.add(question);
        textPane.add(Box.createRigidArea(new Dimension(1,50)));
        textPane.add(confirmation);
        
        JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
        textPaneAll.add(textPane);
        textPaneAll.add(textFieldPane);
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(validateButton);
        buttonPanel.add(cancelButton);
        
        JPanel labelSet = new JPanel();
        labelSet.add(textPaneAll);
        //labelSet.setBorder(BorderFactory.createEmptyBorder(100, 10, 10, 100));
        
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(labelSet);
        panel.add(buttonPanel);
        panel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        this.getContentPane().add(panel);
        
        this.setTitle(Language.getInstance().getText("Changement_du_mot_de_passe"));
        //this.setLocation(300,300);
        /**this.pack();
           this.setLocationRelativeTo(this.getParent()); 
        */
        centerScreen();
    }
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        // this.setVisible(true); 
        // requestFocus();
    }
}
