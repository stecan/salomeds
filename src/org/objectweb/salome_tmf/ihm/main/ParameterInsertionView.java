/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;


public class ParameterInsertionView extends JDialog implements IHMConstants {
    
    /**
     *
     */
    JTable parametersTable;
    
    /**
     * Mod?le de s?lection pour la table des param?tres
     */
    ListSelectionModel rowSM;
    
    Parameter parameter;
    
    JButton okButton;
    
    TableSorter  sorter ;
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    
    public ParameterInsertionView() {
        
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        
        parametersTable = new JTable();
        JButton newParameterButton = new JButton(Language.getInstance().getText("Nouveau"));
        newParameterButton.setToolTipText(Language.getInstance().getText("Ajouter_un_nouveau_parametre"));
        newParameterButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ParameterView.addNewParameter(true);
                }
            });
        
        JButton useParameterButton = new JButton(Language.getInstance().getText("Utiliser"));
        useParameterButton.setToolTipText(Language.getInstance().getText("Utiliser_un_parametre_existant"));
        useParameterButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ParameterView.usingParameter();
                }
            });
        
        sorter = new TableSorter(DataModel.getTestParameterTableModel());
        parametersTable.setModel(sorter);
        sorter.setTableHeader(parametersTable.getTableHeader());
        
        
        
        parametersTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        parametersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane tableScrollPane = new JScrollPane(parametersTable);
        tableScrollPane.setBorder(BorderFactory.createEmptyBorder(20,0,30,0));
        
        
        
        okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setEnabled(false);
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRow = parametersTable.getSelectedRow();
                    if (selectedRow != -1) {
                        //parameter = DataModel.getCurrentTest().getParameter((String)DataModel.getTestParameterTableModel().getValueAt(selectedRow, 0));
                        parameter = DataModel.getCurrentTest().getUsedParameterFromModel((String)sorter.getValueAt(selectedRow, 0));
                        ParameterInsertionView.this.dispose();
                    }
                
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ParameterInsertionView.this.dispose();
                }
            });
        
        
        JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonsPanel.add(okButton);
        buttonsPanel.add(cancelButton);
        
        JPanel upButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        upButtonPanel.add(newParameterButton);
        upButtonPanel.add(useParameterButton);
        
        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.add(upButtonPanel, BorderLayout.NORTH);
        tablePanel.add(tableScrollPane, BorderLayout.CENTER);
        
        rowSM = parametersTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;
                
                    int selectedRow = parametersTable.getSelectedRow();
                    if (selectedRow != -1) {
                        okButton.setEnabled(true);
                    } else {
                        okButton.setEnabled(false);
                    }
                }
            });
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(tablePanel, BorderLayout.CENTER);
        contentPaneFrame.add(buttonsPanel, BorderLayout.SOUTH);
        
        //              initData();
        
        // this.setLocation(350,200);
        this.setTitle(Language.getInstance().getText("Ajouter_un_parametre"));
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);*/
        centerScreen();
    } // Fin du constructeur ParameterInsertionView/0
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    /**
     *
     *
     */
    /*private void initData() {
      for (int i = 0; i < DataModel.getCurrentTest().getParameterList().size(); i++) {
      DataModel.getTestParameterTableModel().addValueAt(((Parameter)DataModel.getCurrentTest().getParameterList().get(i)).getName(), i ,0);
      DataModel.getTestParameterTableModel().addValueAt(((Parameter)DataModel.getCurrentTest().getParameterList().get(i)).getDescription(), i ,1);
      }
      } // Fin de la m?thode initData/0
    */
    public Parameter getParameter() {
        return parameter;
    }
} // Fin de la m?thode ParameterInsertionView
