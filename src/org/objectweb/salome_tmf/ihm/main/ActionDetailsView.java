/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.plugins.UICompCst;

/**
 * Classe representant la vue sur le details des actions de tests
 *
 * @author teaml039
 * @version : 0.1
 */
public class ActionDetailsView extends JDialog implements DataConstants,
                                                          ActionListener, ListSelectionListener {

    final static Logger logger = Logger.getLogger(ActionDetailsView.class);
    /**
     * Modele de donnees de la table des resultats d'action
     */
    MyTableModel actionsResultTableModel;

    /**
     * Table des resultats d'action
     */
    JTable actionsResultTable;

    /**
     * Description de l'action
     */
    JTextPane descriptionArea;

    /**
     * Description du resultat attendu
     */
    JTextPane awaitedResultArea;

    /**
     * Description du resultat effectif
     */
    JTextPane effectiveResultArea;

    /**
     * Le panel des attachements
     */
    AttachmentView attachmentPanel;

    /**
     * La table des anciens attachements
     */
    HashMap oldAttachMap;

    /**
     * Le resultat de test courant
     */
    ExecutionTestResult effectivExecutionTestResult;

    /**
     * Nom de l'execution courante
     */
    String effectivExecutionName;

    /**
     * Nom du resultat d'execution courant
     */
    String effectivExecutionResultName;

    ExecutionResult execRes;
    /******************************************************************************/
    /** CONSTRUCTEUR ***/
    /******************************************************************************/
    JButton quitButton;
    JButton saveButton;

    /**
     * Constructeur de la vue.
     */
    public ActionDetailsView(ManualTest observedTest, Execution execution,
                             ExecutionResult executionResult,
                             ExecutionTestResult executionTestResult, JDialog parent) {

        super(parent, true);
        initComponent(observedTest, execution, executionResult,
                      executionTestResult);
    } // Fin du constructeur ActionDeatilsView

    public ActionDetailsView(ManualTest observedTest, Execution execution,
                             ExecutionResult executionResult,
                             ExecutionTestResult executionTestResult) {

        super(SalomeTMFContext.getInstance().ptrFrame, true);
        initComponent(observedTest, execution, executionResult,
                      executionTestResult);
    } // Fin du constructeur ActionDeatilsView

    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/
    void initComponent(ManualTest observedTest, Execution execution,
                       ExecutionResult executionResult,
                       ExecutionTestResult executionTestResult) {

        int t_x = 1024 - 100;
        int t_y = 768 / 3 * 2 - 50;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height / 3 * 2 - 50;
        } catch (Exception E) {

        }

        execRes = executionResult;
        oldAttachMap = executionTestResult.getCopyOfAttachmentMapFromModel();
        // Initialisation des elements de la fenetre
        actionsResultTableModel = new MyTableModel();
        actionsResultTable = new JTable();
        effectiveResultArea = new JTextPane();
        awaitedResultArea = new JTextPane();
        descriptionArea = new JTextPane();
        JLabel testName = new JLabel(observedTest.getTestListFromModel()
                                     .getFamilyFromModel().getNameFromModel()
                                     + "/"
                                     + observedTest.getTestListFromModel().getNameFromModel()
                                     + "/" + observedTest.getNameFromModel());
        JLabelLink hyperLink = new JLabelLink ("Link");
        if (Config.getPolarionLinks()) {
            // Polarion links are shown
            hyperLink.generatePolarionLink(observedTest.getNameFromModel());
        }
        testName.setFont(new Font(null, Font.BOLD, 18));

        // Initialisation du modele de donnees de la table
        actionsResultTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Nom"));
        actionsResultTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Description"));
        actionsResultTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Resultat_attendu"));
        actionsResultTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Resultat_effectif"));
        actionsResultTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Statut"));

        actionsResultTable.setModel(actionsResultTableModel);

        // actionsResultTable.setPreferredScrollableViewportSize(new
        // Dimension(700,150));
        actionsResultTable.setPreferredScrollableViewportSize(new Dimension(
                                                                            t_x, t_y / 3));
        actionsResultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        actionsResultTable.setRowHeight(25);
        
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTIONS_TABLE,
                                                            actionsResultTable);
        // Activation des plugins associes
        PluginsTools
            .activateAssociatedPlgs(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTIONS_TABLE);

        JScrollPane actionsResultTableScrollPane = new JScrollPane(
                                                                   actionsResultTable);
        actionsResultTableScrollPane
            .setMaximumSize(new Dimension(t_x, t_y / 3));

        // Action lorsqu'un element est selectionne dans la table
        ListSelectionModel rowSM = actionsResultTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        if (Api.isLockExecutedTest()) {
            descriptionArea.setEditable(false);
        }
        // descriptionArea.setPreferredSize(new Dimension(100,100));

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_DESC,
                                                            descriptionArea);
        // Activation des plugins associes
        PluginsTools
            .activateAssociatedPlgs(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_DESC);

        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(
                                                                         BorderFactory.createLineBorder(Color.BLACK), Language
                                                                         .getInstance().getText("Description")));
        // descriptionScrollPane.setPreferredSize(new Dimension(100,100));
        descriptionScrollPane.setPreferredSize(new Dimension(t_x, t_y / 3 / 2));

        if (Api.isLockExecutedTest()) {
            awaitedResultArea.setEditable(false);
        }
        // awaitedResultArea.setPreferredSize(new Dimension(100,100));

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_WAITED_RES,
                                                            awaitedResultArea);
        // Activation des plugins associes
        PluginsTools
            .activateAssociatedPlgs(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_WAITED_RES);

        JScrollPane awaitedResultScrollPane = new JScrollPane(awaitedResultArea);
        awaitedResultScrollPane.setBorder(BorderFactory.createTitledBorder(
                                                                           BorderFactory.createLineBorder(Color.BLACK), Language
                                                                           .getInstance().getText("Resultat_attendu")));
        // awaitedResultScrollPane.setPreferredSize(new Dimension(100,100));
        // awaitedResultScrollPane.setPreferredSize(new Dimension(t_x/2,
        // t_y/3));

        if (Permission.canDeleteTest()) {
            // The administrator group is right to modify the details of a test
            // (regardless of the parameter "LockExecutedTest").
        } else {
            if (Api.isLockExecutedTest()) {
                effectiveResultArea.setEditable(false);
            }
        }
        // effectiveResultArea.setPreferredSize(new Dimension(100,100));

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_EFF_RES,
                                                            effectiveResultArea);
        // Activation des plugins associes
        PluginsTools
            .activateAssociatedPlgs(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_EFF_RES);

        JScrollPane effectiveResultScrollPane = new JScrollPane(
                                                                effectiveResultArea);
        effectiveResultScrollPane.setBorder(BorderFactory.createTitledBorder(
                                                                             BorderFactory.createLineBorder(Color.BLACK), Language
                                                                             .getInstance().getText("Resultat_effectif")));
        // effectiveResultScrollPane.setPreferredSize(new Dimension(100,100));
        // effectiveResultScrollPane.setPreferredSize(new Dimension(t_x/2,
        // t_y/3));

        quitButton = new JButton(Language.getInstance().getText("Terminer"));
        quitButton.addActionListener(this);

        saveButton = new JButton(Language.getInstance().getText("Modifier"));
        saveButton.addActionListener(this);
        if (Permission.canDeleteTest()) {
            // The administrator group is right to modify the details of a test
            // (regardless of the parameter "LockExecutedTest").
        } else {
            if (Api.isLockExecutedTest()) {
                saveButton.setEnabled(false);
            }
        }
        initActionResultsData(observedTest, executionTestResult, execution
                              .getNameFromModel(), executionResult.getNameFromModel());

        // attachmentPanel = new AttachmentView(DataModel.getApplet(),
        // EXECUTION_RESULT_TEST, EXECUTION_RESULT_TEST, executionTestResult,
        // SMALL_SIZE_FOR_ATTACH, execution, executionResult, observedTest);
        attachmentPanel = new AttachmentView(DataModel.getApplet(),
                                             EXECUTION_RESULT_TEST, EXECUTION_RESULT_TEST,
                                             executionTestResult, SMALL_SIZE_FOR_ATTACH, this);

        attachmentPanel.setBorder(BorderFactory.createTitledBorder(
                                                                   BorderFactory.createLineBorder(Color.BLACK), Language
                                                                   .getInstance().getText("Attachements")));

        JPanel downPart = new JPanel(new FlowLayout());
        downPart.setLayout(new BoxLayout(downPart, BoxLayout.X_AXIS));
        downPart.setPreferredSize(new Dimension(t_x, t_y / 3 / 2));
        downPart.add(awaitedResultScrollPane);
        downPart.add(effectiveResultScrollPane);

        /*
         * JPanel actionsDetailsPanel = new JPanel(new BorderLayout());
         * actionsDetailsPanel.add(descriptionScrollPane, BorderLayout.NORTH);
         * actionsDetailsPanel.add(downPart, BorderLayout.SOUTH);
         */

        JPanel actionsDetailsPanel = new JPanel();
        actionsDetailsPanel.setLayout(new BoxLayout(actionsDetailsPanel,
                                                    BoxLayout.Y_AXIS));
        actionsDetailsPanel.setPreferredSize(new Dimension(t_x, t_y / 3 * 2));
        actionsDetailsPanel.add(descriptionScrollPane);
        actionsDetailsPanel.add(downPart);

        JPanel upPart = new JPanel(new BorderLayout(20,20));
        upPart.setBorder(BorderFactory.createEmptyBorder(20,10,20,20));
        upPart.add(BorderLayout.NORTH, testName);
        if (Config.getPolarionLinks()) {
            // Polarion links are shown
            upPart.add(hyperLink);
        }

        JPanel allView = new JPanel(new BorderLayout());

        allView.add(upPart, BorderLayout.NORTH);
        allView.add(actionsResultTableScrollPane, BorderLayout.CENTER);
        actionsResultTableScrollPane.setPreferredSize(new Dimension(t_x,
                                                                    t_y / 3 / 2));
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(saveButton);
        buttonPanel.add(quitButton);

        /*
         * Container contentPaneFrame = this.getContentPane(); Dimension d =
         * buttonPanel.getSize(); contentPaneFrame.setLayout(new
         * BoxLayout(contentPaneFrame, BoxLayout.Y_AXIS));
         *
         * allView.setPreferredSize(new Dimension(t_x, t_y/3/2 - d.height));
         * contentPaneFrame.add(allView);
         *
         * actionsDetailsPanel.setPreferredSize(new Dimension(t_x, t_y/3*2 -
         * d.height)); contentPaneFrame.add(actionsDetailsPanel);
         *
         * attachmentPanel.setPreferredSize(new Dimension(t_x, t_y/3 -
         * d.height)); contentPaneFrame.add(attachmentPanel);
         *
         * contentPaneFrame.add(buttonPanel);
         */

        JTabbedPane pJTabbedPane = new JTabbedPane();
        Container contentPaneFrame = this.getContentPane();
        Dimension d = buttonPanel.getSize();
        contentPaneFrame.setLayout(new BoxLayout(contentPaneFrame,
                                                 BoxLayout.Y_AXIS));

        JPanel result = new JPanel();
        result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
        allView.setPreferredSize(new Dimension(t_x, t_y / 2 - d.height));
        actionsDetailsPanel.setPreferredSize(new Dimension(t_x, t_y / 2
                                                           - d.height));
        result.add(allView);
        result.add(actionsDetailsPanel);

        pJTabbedPane.add(Language.getInstance().getText("Test"), result);
        pJTabbedPane.add(Language.getInstance().getText("Attachements"),
                         attachmentPanel);
        contentPaneFrame.add(pJTabbedPane);
        contentPaneFrame.add(buttonPanel);

        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.MANUAL_TEST_EXECUTION_RESULT_DETAILS_TAB,
                                                            pJTabbedPane);
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_TEST_EXECUTION_RESULT_DETAILS_TAB);

        setSize(t_x, t_y);
        // this.setLocation(50,50);
        this.setTitle(Language.getInstance().getText("Details_d_executions"));
        /*
         * this.setLocationRelativeTo(this.getParent()); this.pack();
         * this.setVisible(true);
         */
        centerScreen();
    }

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);

        this.setVisible(true);
        requestFocus();
    }

    /**
     * Methode d'initialisation de la table des resultats d'actions
     */
    public void initActionResultsData(ManualTest test,
                                      ExecutionTestResult execTestResult, String executionName,
                                      String executionResultName) {
        // Conservation des anciennes valeurs
        // oldAttachMap = execTestResult.getCopyOfAttachmentMapFromModel();

        effectivExecutionTestResult = execTestResult;
        effectivExecutionName = executionName;
        effectivExecutionResultName = executionResultName;
        ArrayList actionsList = test.getActionListFromModel(false);
        for (int i = 0; i < actionsList.size(); i++) {
            ArrayList data = new ArrayList();
            Action action = (Action) actionsList.get(i);
            data.add(action.getNameFromModel());
            data.add(action.getDescriptionFromModel());
            data.add(action.getAwaitedResultFromModel());
            //                  data.add(((ManualExecutionResult) execTestResult)
            //                                  .getDescriptionResultFromModel(action));
            // data.add(((ManualExecutionResult)execTestResult).getAwaitedResultFromModel(action));
            data.add(((ManualExecutionResult)execTestResult).getEffectivResultFromModel(action));
            data.add(Tools.getActionStatusIcon(((ManualExecutionResult)execTestResult).getActionStatusInModel(action)));
            actionsResultTableModel.addRow(data);
        }
        if (actionsResultTableModel.getRowCount() > 0) {
            actionsResultTable.getSelectionModel().setSelectionInterval(0, 0);
        }
    } // Fin de la methode initActionResultsData/0

    /********************** Manage events *************************/

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(quitButton)) {
            quitPerformed();
        } else if (e.getSource().equals(saveButton)) {
            updatePerformed();
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(actionsResultTable.getSelectionModel())) {
            actionsResultTableValueChanged(e);
        }
    }

    void actionsResultTableValueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
            return;

        // Recuperation de la l'index de la ligne selectionnee
        int selectedRowIndex = actionsResultTable.getSelectedRow();
        // Mise a jour des differentes descriptions
        if (selectedRowIndex != -1) {
            String descString = (String) actionsResultTableModel.getValueAt(
                                                                            selectedRowIndex, 1);
            descriptionArea.setText(descString);
            descriptionArea.getCaret().setDot(0);
            String awaitedResultString = ((String) actionsResultTableModel
                                          .getValueAt(selectedRowIndex, 2));
            awaitedResultArea.setText(awaitedResultString);
            awaitedResultArea.getCaret().setDot(0);
            String effectiveResultString = (String) actionsResultTableModel
                .getValueAt(selectedRowIndex, 3);
            effectiveResultArea.setText((String) actionsResultTableModel
                                        .getValueAt(selectedRowIndex, 3));
            effectiveResultArea.getCaret().setDot(0);
        } else {
            descriptionArea.setText("");
            awaitedResultArea.setText("");
            effectiveResultArea.setText("");
        }
    }

    void quitPerformed() {
        if (Api.isConnected()) {
            int transNumber = -1;
            try {
                transNumber = Api.beginTransaction(100,
                                                   ApiConstants.INSERT_ATTACHMENT);

                effectivExecutionTestResult.updateAttachement(oldAttachMap);

                Api.commitTrans(transNumber);

            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
        }

        // effectivExecutionTestResult.setAttachmentMapInModel(oldAttachMap);
        ActionDetailsView.this.dispose();
    }

    /* Update of current action test result */
    void updatePerformed() {
        ManualExecutionResult pManualExecutionResult;
        Action pAction;
        ArrayList actionsList;
        int selectedRowIndex;

        try {
            pManualExecutionResult = (ManualExecutionResult) effectivExecutionTestResult;
            selectedRowIndex = actionsResultTable.getSelectedRow();
            actionsList = ((ManualTest) effectivExecutionTestResult
                           .getTestFromModel()).getActionListFromModel(false);
            pAction = (Action) actionsList.get(selectedRowIndex);
        } catch (Exception exception) {
            Tools.ihmExceptionView(exception);
            return;
        }

        if (Api.isConnected()) {
            int transNumber = -1;
            try {

                pManualExecutionResult.addEffectivResultInModel(pAction,
                                                                effectiveResultArea.getText());
                pManualExecutionResult.addDescriptionResultInModel(pAction,
                                                                   descriptionArea.getText());
                pManualExecutionResult.addAwaitedResultInModel(pAction,
                                                               awaitedResultArea.getText());

                pManualExecutionResult.updateActionResInDB(pAction);

                Api.commitTrans(transNumber);

                actionsResultTableModel.setValueAt(descriptionArea.getText(),
                                                   selectedRowIndex, 1);
                actionsResultTableModel.setValueAt(awaitedResultArea.getText(),
                                                   selectedRowIndex, 2);
                actionsResultTableModel.setValueAt(effectiveResultArea
                                                   .getText(), selectedRowIndex, 3);

                logger.info(LoggingData.getData() +  
                        "Kampanie: " +
                        DataModel.getCurrentCampaign().toString() + "\n" +
                        "Execution-Result: " + 
                        effectivExecutionResultName + "\n" +
                        "Action-Result: " + 
                        pAction.getNameFromModel().toString() + 
                        " Successfully modified!");

            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                // replace model in original forme
                pManualExecutionResult.addDescriptionResultInModel(pAction,
                                                                   (String) actionsResultTableModel.getValueAt(
                                                                                                               selectedRowIndex, 1));
                pManualExecutionResult.addAwaitedResultInModel(pAction,
                                                               (String) actionsResultTableModel.getValueAt(
                                                                                                           selectedRowIndex, 2));
                pManualExecutionResult.addEffectivResultInModel(pAction,
                                                                (String) actionsResultTableModel.getValueAt(
                                                                                                            selectedRowIndex, 3));

                Tools.ihmExceptionView(exception);
            }
        }
    }
} // Fin de la classe ActionDetailsView
