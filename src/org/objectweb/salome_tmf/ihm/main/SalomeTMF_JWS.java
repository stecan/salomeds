package org.objectweb.salome_tmf.ihm.main;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.util.Locale;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;
import javax.swing.JFrame;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

public class SalomeTMF_JWS implements  BaseIHM{
    protected   URL urlSalome;
    protected   JFrame ptrFrame;


    protected String strProject = null;
    protected String strLogin = null;

    /***************************************************************************************/
    protected SalomeTMFContext pSalomeTMFContext;
    protected SalomeTMFPanels pSalomeTMFPanels;
    /****************************************************************************************/

    protected SalomeTMF_JWS(){
        if (connectToAPI()){
            WaitView waitView = new WaitView();
            long l1 = System.currentTimeMillis();
            initComponent();
            loadModel();
            loadPlugin();
            long l2 = System.currentTimeMillis();
            Util.debug("Temps de chargement = " + (l2-l1));
            waitView.dispose();
            ptrFrame.pack();
            ptrFrame.setVisible(true);
            ptrFrame.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosing(WindowEvent e) {
                        quit(false, true);
                    }
                });

        }else  {
            System.exit(0);
        }

    }

    void initComponent(){
        ptrFrame = new JFrame("SalomeTMF");

        Api.setUrlBase(urlSalome);
        //Util.debug("URLBASE IS " + urlSalome);
        try {
            Class lnfClass = Class.forName(UIManager.getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel)(lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
            Util.adaptFont();
        } catch (Exception exc) {
            Util.err("Error loading L&F: " + exc);
        }
        Util.log("[SalomeTMF->initComponent] Used Local is "+ Api.getUsedLocale());
        Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));



        pSalomeTMFContext = new SalomeTMFContext(urlSalome, new Frame(), this);
        pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, this);
        pSalomeTMFPanels.initComponentPanel();

        Container cp = ptrFrame.getContentPane();
        cp.add(pSalomeTMFPanels.tabs);


    }

    void loadPlugin(){
        pSalomeTMFContext.loadPlugin(pSalomeTMFPanels);
        pSalomeTMFContext.startPlugin();
    }


    void loadModel(){
        if (strProject == null || strLogin == null) {
            quit(true, true);
        }
        DataModel.loadFromBase(strProject, strLogin, this);
        pSalomeTMFPanels.loadModel(strProject, strLogin);
    }

    /**************************************************************************************/
    protected boolean connectToAPI(){
        try {
            // Lookup the javax.jnlp.BasicService object
            BasicService bs = (BasicService)ServiceManager.lookup("javax.jnlp.BasicService");
            // Invoke the showDocument method
            urlSalome =  bs.getCodeBase();
            Api.openConnection(urlSalome);

            ProjectLogin dialog = new ProjectLogin(null);
            dialog.setVisible(true);
            if(dialog.getSelectedProject() != null && dialog.getSelectedUser() != null){
                strProject = dialog.getSelectedProject() ;
                strLogin = dialog.getSelectedUser();
                //               Recuperation du login et du mot de passe pour l'authentification
                try {
                    Api.setStrUsername(strLogin);
                    Api.setStrMD5Password(MD5paswd.getEncodedPassword(dialog.getSelectedPassword()));
                } catch (Exception e) {
                    Util.err(e);
                }
                Api.initConnectionUser(strProject, strLogin);
            } else {
                return false;
            }
        } catch(UnavailableServiceException ue) {
            // Service is not supported
            return false;
        }
        // strProject = "projet_test";
        //strLogin = "marchemi";

        return true;
    }
    /**********************************************************************************************************************/

    @Override
    public void quit(boolean do_recup, boolean doclose) {
        Util.log("[SalomeTMF] - Applet quit (recup = " +do_recup +")" );
        if (pSalomeTMFContext != null && doclose == true){
            pSalomeTMFContext.suspendPlugin();
        }
        if (Api.isConnected() && doclose) {
            Api.closeConnection();
        }
        System.exit(0);
    } // Fin de la m?thode quit();


    @Override
    public SalomeTMFContext getSalomeTMFContext(){
        return pSalomeTMFContext;
    }

    @Override
    public SalomeTMFPanels getSalomeTMFPanels(){
        return pSalomeTMFPanels;
    }


    @Override
    public void showDocument(URL toShow , String where){
        //getAppletContext().showDocument(toShow, where);
        try {
            // Lookup the javax.jnlp.BasicService object
            BasicService bs = (BasicService)ServiceManager.lookup("javax.jnlp.BasicService");
            // Invoke the showDocument method
            bs.showDocument(toShow);

        } catch(UnavailableServiceException ue) {
            // Service is not supported

        }
    }
    @Override
    public boolean isGraphique() {
        return true;
    }

    public static void main(String arg[]){
        new SalomeTMF_JWS();
    }
}
