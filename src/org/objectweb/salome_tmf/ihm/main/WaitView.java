/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui definit l'ecran d'attente lors du chargement de la base
 */
public class WaitView extends JFrame {
    
    /**
     * Un tableau d'image ? afficher
     */
    Image[] img;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la vue
     */
    public WaitView() {
        super();
        setSize(500,300);
        setUndecorated( true );
        setFocusable( false );
        setEnabled( false );
        
        // On charge l'image de fond
        img = new Image[1];
        img[0] = Tools.loadImages( this, ".//salome_intro.jpg" );
        JPanel panel = new PanelAvecFond(img[0]);
        panel.setPreferredSize(new Dimension(500,300));
        
        // Le texte ? afficher
        JLabel label = new JLabel(Language.getInstance().getText("Chargement_de_Salome_TMF"));
        label.setFont(new Font(null, Font.BOLD, 40));
        label.setForeground(Color.WHITE);
        panel.add(label);
        
        // le conteneur
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(panel);
        
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gs = ge.getScreenDevices();
        GraphicsDevice gd = gs[0];
        GraphicsConfiguration[] gc = gd.getConfigurations();
        
        // On place la fen?tre au milieu de l'?cran
        /*Rectangle r = gc[0].getBounds();
          Point pt = new Point( r.width/2, r.height/2 );
          Point loc = new Point( pt.x - 200, pt.y - 150 );
        
          // Affichage
          //this.setLocation(loc);*/
        this.setLocationRelativeTo(this.getParent()); 
        this.pack();
        this.setVisible(true);
    } // Fin du constructeur WaitView/0
} // Fin de la classe WaitView
