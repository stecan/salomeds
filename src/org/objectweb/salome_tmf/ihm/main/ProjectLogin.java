package org.objectweb.salome_tmf.ihm.main;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.common.CommonLogin;
import org.objectweb.salome_tmf.ihm.languages.Language;

public class ProjectLogin extends JDialog implements ActionListener{
        
    CommonLogin pCommonLogin;
    String usedLocale = "";
    String strProjet = null;
    String strUser = null;
    String strPassword = null;
    JavaScriptUtils pJWS;

    ProjectLogin(JavaScriptUtils pJWS){
        setModal(true);
        this.pJWS = pJWS;
        pCommonLogin = new CommonLogin(true, false, false, this);
        setTitle("SalomeTMF Login");
        initComponents();
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getActionCommand().equals(CommonLogin.ACTION_START_SALOME)){
            if (pCommonLogin.testAuthentification()) {
                try {
                    usedLocale = pCommonLogin.getUsedLocal();
                    Api.saveLocale(usedLocale);
                    strProjet = pCommonLogin.getSelectedSalomeProject();
                    strUser = pCommonLogin.getSelectedSalomeUser().getLogin();
                    strPassword = pCommonLogin.getPassword();
                                        
                    setVisible(false);
                } catch (Exception me) {
                    Util.log("[LoginSalomeTMF->b_startActionPerformed]" + me);
                }
            }else {
                strProjet = null;
                strUser = null;
                pCommonLogin.error(Language.getInstance().getText("Mot_de_passe_invalide"));
            }
        }
    }
        
    public String getSelectedProject(){
        return strProjet;
    }
        
    public String getSelectedUser(){
        return strUser;
    }
        
    public String getSelectedPassword(){
        return strPassword;
    }
        
    private void initComponents() {    
                
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            Point pt = new Point( r.width/2, r.height/2 );
            Point loc = new Point( pt.x - 200, pt.y - 150 );
                        
            // Affichage
            setLocation(loc);
        } catch (Exception e){
                        
        }
                
                
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    if (pJWS != null){
                        pJWS.closeWindow();
                        dispose();
                    } else {
                        System.exit(0);
                    }
                }
            });
                
        setContentPane(pCommonLogin);
        pack();
                
    }
        
        
    /*
      Vector porjectsList;
      Vector usersProject;
        
      Vector languages = new Vector(); 
        
      ISQLProject pISQLProject;
      ISQLPersonne pISQLPersonne;
        
      boolean init;
      String proj;
        
        
      JTabbedPane pJTabbedPane;
        
        
        
        
        
      ProjectLogin(){
      setModal(true);
      setTitle("SalomeTMF Login");
                
      connectToAPI();
      initData();
      initComponents();
      init = true;
      initList();
      init = false;
      }
        
      void connectToAPI(){;
        
      pISQLProject = Api.getISQLObjectFactory().getISQLProject();
      pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
      usedLocale = Api.getUsedLocale();
      Util.log("[LoginSalomeTMF->connectToAPI] Used Local is "+ usedLocale);
      Language.getInstance().setLocale(new Locale(usedLocale));
      languages = Api.getLocales();
      Util.log("[LoginSalomeTMF->connectToAPI] Available Languages is "+ languages);
      if (languages == null){
      Util.log("[LoginSalomeTMF->connectToAPI] Set default languages list to fr");
      languages = new Vector();
      languages.add("fr");
      }
      }
        
        
        
        
        
      void initData(){
      try {
      porjectsList = pISQLProject.getAllProjects();
      Util.log("[LoginSalomeTMF->initData] Projects list is "+ porjectsList);
      if (porjectsList.size()>0) {
      ProjectWrapper pProjectWrapper = (ProjectWrapper) porjectsList.elementAt(0);
      usersProject = pISQLProject.getUsersOfProject(pProjectWrapper.getName());
      Util.log("[LoginSalomeTMF->initData] Users Project list is "+  usersProject + ", for project "+ pProjectWrapper.getName());
      } else {
      usersProject = new Vector();
      }
                        
      } catch (Exception e ){
      Util.log("[LoginSalomeTMF->initData]" + e);
      porjectsList = null;
      }
      }
        
      private void error(String err){
      javax.swing.JOptionPane.showMessageDialog(this,
      err,
      Language.getInstance().getText("Authentification"),
      javax.swing.JOptionPane.ERROR_MESSAGE);
                
      }
        
      private void projectChange(ProjectWrapper pProjectWrapper){
      String project = pProjectWrapper.getName();
      proj = project;
      if (!init){
      try {
      proj = project;
      //users = db_AdminVT.getAllUserOfProject(proj);
                                
      usersProject = pISQLProject.getUsersOfProject(project);
                                
                                
      user_List.removeAllItems();
                                
      //Enumeration e = users.keys();
      Enumeration e = usersProject.elements();
      while (e.hasMoreElements() ) {
      Object pUser = e.nextElement();
      Util.log("[LoginSalomeTMF->projectChange] add user_List " + pUser);
      user_List.addItem(pUser);
      }
      } catch (Exception e ){
                                
      }
      }
      }
        
        
        
      private UserWrapper getUser(){
      return  (UserWrapper) user_List.getSelectedItem();
      }
        
        
      private boolean validPassword(){
      //String paswd = (String) users.get(getUser());
      String paswd =  getUser().getPassword();
      String typed_paswd = new String(password_Field.getPassword()); 
      if (paswd != null){
      try {
      return org.objectweb.salome_tmf.api.MD5paswd.testPassword(typed_paswd, paswd);
      }catch(Exception e){
      Util.log("[LoginSalomeTMF->validPassword]"+ e);
      }
      }
      return false;
      }
        
        
        
        
      private  void initList(){
      project_List.removeAllItems();
                
      user_List.removeAllItems();
                
      languages_List.removeAllItems();
      user_List.removeAllItems();
      for (int i = 0 ; i < porjectsList.size(); i++) {
      project_List.addItem(porjectsList.elementAt(i));
                        
      } 
                
      Enumeration e = usersProject.elements();
      while (e.hasMoreElements() ) {
      Object pUser = e.nextElement();
      Util.log("[LoginSalomeTMF->initList] add user_List " + pUser);
      user_List.addItem(pUser);
      }
                
      for(int j=0 ; j < languages.size() ; j++) {
        
      languages_List.addItem(((String)languages.elementAt(j)).trim());
      }
      //languages_List.setSelectedItem(new Locale(Api.LOCALE).getDisplayName());
      languages_List.setSelectedItem(usedLocale);
                
      }
        

      private void initComponents() {    
                
      try {
      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice[] gs = ge.getScreenDevices();
      GraphicsDevice gd = gs[0];
      GraphicsConfiguration[] gc = gd.getConfigurations();
      Rectangle r = gc[0].getBounds();
      Point pt = new Point( r.width/2, r.height/2 );
      Point loc = new Point( pt.x - 200, pt.y - 150 );
                        
      // Affichage
      setLocation(loc);
      } catch (Exception e){
                        
      }
      pJTabbedPane = new JTabbedPane();
      jPanel4 = new javax.swing.JPanel();
      jPanel1 = new javax.swing.JPanel();
      jLabel2 = new javax.swing.JLabel();
      jLabel3 = new javax.swing.JLabel();
      jLabel4 = new javax.swing.JLabel();
      project_List = new javax.swing.JComboBox();
      user_List = new javax.swing.JComboBox();
      password_Field = new javax.swing.JPasswordField();
      password_Field.addKeyListener(new passwordKeyListener(0));
                
      jPanel2 = new javax.swing.JPanel();
      jLabel1 = new javax.swing.JLabel();
      jPanel3 = new javax.swing.JPanel();
      b_start = new javax.swing.JButton();
                
      jPanel14 = new javax.swing.JPanel();
      jPanel16 = new javax.swing.JPanel();
      jPanel19 = new javax.swing.JPanel();
      jLabel15 = new javax.swing.JLabel();
      jPanel17 = new javax.swing.JPanel();
      jLabel13 = new javax.swing.JLabel();
      jLabel14 = new javax.swing.JLabel();
      jLabel16 = new javax.swing.JLabel();
      jLabel17 = new javax.swing.JLabel();
      languages_List = new javax.swing.JComboBox();
      jLabel19 = new javax.swing.JLabel();
      jPanel18 = new javax.swing.JPanel();
      jLabel9 = new javax.swing.JLabel();
      jLabel18 = new javax.swing.JLabel();
                
                
      pJTabbedPane.setBackground(new java.awt.Color(255, 255, 255));
      pJTabbedPane.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(1, 1, 1, 1)));
      pJTabbedPane.setForeground(new java.awt.Color(255, 102, 0));
      pJTabbedPane.setOpaque(true);
                
      jPanel4.setLayout(new java.awt.BorderLayout());
                
      jPanel4.setBackground(new java.awt.Color(255, 255, 255));
      jPanel4.setForeground(new java.awt.Color(51, 51, 51));
      jPanel1.setLayout(new java.awt.GridLayout(2, 3, 10, 0));
                
      jPanel1.setBackground(new java.awt.Color(255, 255, 255));
      jPanel1.setForeground(new java.awt.Color(51, 51, 51));
      jLabel2.setBackground(new java.awt.Color(255, 255, 255));
      jLabel2.setText(Language.getInstance().getText("Projet"));
      jLabel2.setOpaque(true);
      jPanel1.add(jLabel2);
                
      jLabel3.setBackground(new java.awt.Color(255, 255, 255));
      jLabel3.setText(Language.getInstance().getText("Utilisateur"));
      jLabel3.setOpaque(true);
      jPanel1.add(jLabel3);
                
      jLabel4.setBackground(new java.awt.Color(255, 255, 255));
      jLabel4.setText(Language.getInstance().getText("Mot_de_passe"));
      jLabel4.setOpaque(true);
      jPanel1.add(jLabel4);
                
      project_List.setBackground(new java.awt.Color(255, 204, 0));
      project_List.setName("l_projet");
      project_List.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
      project_ListActionPerformed(evt);
      }
      });
      project_List.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
      project_ListItemStateChanged(evt);
      }
      });
                
      jPanel1.add(project_List);
                
      user_List.setBackground(new java.awt.Color(255, 204, 0));
      user_List.addItemListener(new java.awt.event.ItemListener() {
      public void itemStateChanged(java.awt.event.ItemEvent evt) {
      user_ListItemStateChanged(evt);
      }
      });
                
      jPanel1.add(user_List);
                
      jPanel1.add(password_Field);
                
      jPanel4.add(jPanel1, java.awt.BorderLayout.CENTER);
                
      jPanel2.setBackground(new java.awt.Color(255, 255, 255));
      jPanel2.setForeground(new java.awt.Color(51, 51, 51));
      jLabel1.setBackground(new java.awt.Color(255, 255, 255));
      jLabel1.setFont(new java.awt.Font("Dialog", 1, 24));
      jLabel1.setText(Language.getInstance().getText("Se_connecter_a_Salome_TMF"));
      jPanel2.add(jLabel1);
                
      jPanel4.add(jPanel2, java.awt.BorderLayout.NORTH);
                
      jPanel3.setBackground(new java.awt.Color(255, 255, 255));
      jPanel3.setForeground(new java.awt.Color(51, 51, 51));
      b_start.setText(Language.getInstance().getText("Demarrer_Salome_TMF"));
      b_start.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
      b_startActionPerformed(evt);
      }
      });
                
      jPanel3.add(b_start);
                
      jPanel4.add(jPanel3, java.awt.BorderLayout.SOUTH);
                
      pJTabbedPane.addTab(Language.getInstance().getText("Demarrer_Salome_TMF"), jPanel4);
                
                
                
                
                
      jPanel14.setLayout(new java.awt.BorderLayout());
                
      jPanel14.setBackground(new java.awt.Color(255, 255, 255));
      jPanel16.setLayout(new java.awt.BorderLayout());
                
      jPanel16.setBackground(new java.awt.Color(255, 255, 255));
      jPanel19.setBackground(new java.awt.Color(255, 255, 255));
      jLabel15.setBackground(new java.awt.Color(255, 255, 255));
      jLabel15.setFont(new java.awt.Font("Dialog", 1, 24));
      jLabel15.setText(Language.getInstance().getText("Choissisez_votre_langue_"));
      jPanel19.add(jLabel15);
                
      jPanel16.add(jPanel19, java.awt.BorderLayout.NORTH);
                
      jPanel17.setLayout(new java.awt.GridLayout(2, 3, 10, 0));
                
      jPanel17.setBackground(new java.awt.Color(255, 255, 255));
      jPanel17.setForeground(new java.awt.Color(51, 51, 51));
      jPanel17.add(jLabel13);
                
      jLabel14.setText(Language.getInstance().getText("Langue_"));
      jPanel17.add(jLabel14);
                
      jPanel17.add(jLabel16);
                
      jPanel17.add(jLabel17);
                
      languages_List.setBackground(new java.awt.Color(255, 204, 0));
      jPanel17.add(languages_List);
                
      jPanel17.add(jLabel19);
                
      jPanel16.add(jPanel17, java.awt.BorderLayout.CENTER);
                
      jPanel18.setBackground(new java.awt.Color(255, 255, 255));
      jPanel18.setPreferredSize(new java.awt.Dimension(15, 33));
      jPanel18.add(jLabel9);
                
      jPanel18.add(jLabel18);
                
      jPanel16.add(jPanel18, java.awt.BorderLayout.SOUTH);
                
      jPanel14.add(jPanel16, java.awt.BorderLayout.CENTER);
                
      pJTabbedPane.addTab(Language.getInstance().getText("Langues"), jPanel14);
                
      setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
      addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
      System.exit(0);
      }
      });
                
      setContentPane(pJTabbedPane);
      pack();
                
      }//GEN-END:initComponents
        
        
        
        
      private void project_ListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_project_ListActionPerformed
      // TODO add your handling code here:
      }//GEN-LAST:event_project_ListActionPerformed
        
      private void project_ListItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_project_ListItemStateChanged
      // TODO add your handling code here:
      projectChange((ProjectWrapper) evt.getItem());
      }//GEN-LAST:event_project_ListItemStateChanged
        
      private void user_ListItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_user_ListItemStateChanged
      // TODO add your handling code here:
      }//GEN-LAST:event_user_ListItemStateChanged
        
      private void b_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_startActionPerformed
      // TODO add your handling code here:
      if (validPassword()) {
      try {
      usedLocale = ((String)languages_List.getSelectedItem()).trim();
      Api.saveLocale(usedLocale);
      strProjet = proj;
      strUser = getUser().getLogin();   
      setVisible(false);
      } catch (Exception me) {
      Util.log("[LoginSalomeTMF->b_startActionPerformed]" + me);
      }
      }else {
      strProjet = null;
      strUser = null;
      error(Language.getInstance().getText("Mot_de_passe_invalide"));
      }
      }//GEN-LAST:event_b_startActionPerformed
        
        
      // Variables declaration - do not modify//GEN-BEGIN:variables
      private javax.swing.JButton b_start;
        
        
      private javax.swing.JLabel jLabel1;
      private javax.swing.JLabel jLabel13;
      private javax.swing.JLabel jLabel14;
      private javax.swing.JLabel jLabel15;
      private javax.swing.JLabel jLabel16;
      private javax.swing.JLabel jLabel17;
      private javax.swing.JLabel jLabel18;
      private javax.swing.JLabel jLabel19;
      private javax.swing.JLabel jLabel2;
      private javax.swing.JLabel jLabel3;
      private javax.swing.JLabel jLabel4;
      private javax.swing.JLabel jLabel9;
      private javax.swing.JPanel jPanel1;
      private javax.swing.JPanel jPanel14;
      private javax.swing.JPanel jPanel16;
      private javax.swing.JPanel jPanel17;
      private javax.swing.JPanel jPanel18;
      private javax.swing.JPanel jPanel19;
      private javax.swing.JPanel jPanel2;
      private javax.swing.JPanel jPanel3;
      private javax.swing.JPanel jPanel4;
        
        
        
        
        
        
        
      private javax.swing.JComboBox languages_List;
      private javax.swing.JPasswordField password_Field;
        
      private javax.swing.JComboBox project_List;
        
      private javax.swing.JComboBox user_List;
        
        
        
      class passwordKeyListener implements KeyListener {
      int paswdType = -1;
                
      passwordKeyListener(int t){
      paswdType  = t;
      }
                
      public void keyTyped(KeyEvent ke){
                        
      }
                
      public void keyPressed(KeyEvent ke){
                        
      }
                
      public void keyReleased(KeyEvent ke){
      if (ke.getKeyCode() == KeyEvent.VK_ENTER){
      if (paswdType == 0){
      b_startActionPerformed(null);
      }
      }
      }
      }
    */
}
