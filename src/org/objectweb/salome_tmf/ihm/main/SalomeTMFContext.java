/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Frame;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.java.plugin.Extension;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.PluginClassLoader;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.Common;

public class SalomeTMFContext implements IPlugObject{



    public Hashtable associatedTestDriver;
    public Hashtable associatedScriptEngine;
    public Hashtable associatedExtension;
    public Vector bugTrackers;
    public Vector statistiquess;
    public Vector reqManagers;
    Vector xmlPrinters;
    public Hashtable bugTrackerExtensions;
    public Hashtable statistiquesExtensions;

    /**
     * JPF Parameters
     */
    PluginManager pluginManager;
    ExtensionPoint CommonExtPoint;
    ExtensionPoint testDriverExtPoint;
    ExtensionPoint scriptEngineExtPoint;
    ExtensionPoint bugTrackerExtPoint;
    ExtensionPoint statistiquesExtPoint;
    ExtensionPoint reqManagerExtPoint;
    JPFManager jpf;

    /**
     * Mapping between UI components and constants defined in org.objectweb.salome_tmf.ihm.UICompCst
     */
    Map UIComponentsMap;

    /**
     * Associated plugins to UI components
     * key = associated constante to UI Component , value = List of associated plugins
     */
    Map associatedPlgsToCompsMap;




    URL urlSalome;
    Frame ptrFrame;
    static BaseIHM pBaseIHM;

    static SalomeTMFContext pSalomeTMFContext = null;

    public SalomeTMFContext(URL documentBase, Frame pFrame, BaseIHM _BaseIHM){
        urlSalome = documentBase;
        ptrFrame = pFrame;
        pBaseIHM = _BaseIHM;

        associatedTestDriver = new Hashtable();
        associatedScriptEngine =  new Hashtable();
        associatedExtension =  new Hashtable();
        bugTrackers = new Vector();
        statistiquess = new Vector();
        reqManagers = new Vector();
        xmlPrinters = new Vector();
        //bugTrackerExtensions = new Vector();
        bugTrackerExtensions = new Hashtable();
        statistiquesExtensions = new Hashtable();
        UIComponentsMap = new HashMap();
        associatedPlgsToCompsMap = new HashMap();
        pSalomeTMFContext = this;
        /*try
          {
          EventQueue eq = Toolkit.getDefaultToolkit().getSystemEventQueue();
          eq.invokeAndWait(new Runnable() {
          public void run() {
          Thread.currentThread().setContextClassLoader(pSalomeClassLoader);
          }
          });
          }
          catch (Exception e)
          {
          Util.err(e);
          }*/
    }

    public PluginClassLoader getSalomeClassLoader(){
        return jpf.getClassLoader();
    }

    public void addJarToClassLoader(URL jar){
        jpf.getClassLoader().AddJar(jar);
    }

    public static BaseIHM getBaseIHM() {
        return pBaseIHM;
    }

    public static SalomeTMFContext getInstance() {
        return pSalomeTMFContext;
    }


    public Frame getSalomeFrame(){
        return ptrFrame;
    }
    /********************************* BDD/API***************************************/



    /************************ Plugin Method ***********************************************/

    public void loadPlugin(SalomeTMFPanels pSalomeTMFPanels){
        if (Api.isALLOW_PLUGINS()) {
            jpf = new JPFManager();
            jpf.startJPF(this, urlSalome, UIComponentsMap);
            pSalomeTMFPanels.loadPlugin();
        }
    }

    void reloadPlugin(SalomeTMFPanels pSalomeTMFPanels){
        if (Api.isALLOW_PLUGINS()){
            //jpf.reActivatePlugin(this, CommonExtPoint, bugTrackerExtPoint, UIComponentsMap);
            jpf.reActivatePlugin(this, CommonExtPoint, bugTrackerExtPoint, statistiquesExtPoint, UIComponentsMap);
            pSalomeTMFPanels.loadPlugin();
        }
    }

    public void startPlugin(){
        if (Api.isALLOW_PLUGINS()) {
            for (Iterator it = CommonExtPoint.getConnectedExtensions()
                     .iterator(); it.hasNext();) {
                Extension commonExt = (Extension) it.next();
                try {
                    Object ob = jpf.activateExtension(commonExt);
                    if (ob != null) {
                        //((Common) ob).allPluginActived(CommonExtPoint,
                        //              testDriverExtPoint, scriptEngineExtPoint, bugTrackerExtPoint);
                        ((Common) ob).allPluginActived(CommonExtPoint,
                                                       testDriverExtPoint, scriptEngineExtPoint, bugTrackerExtPoint, statistiquesExtPoint);
                    }
                } catch (Exception e) {
                    Util.err(e);
                }
            }
        }
    }

    public void suspendPlugin(){
        if (Api.isALLOW_PLUGINS()) {
            int size = bugTrackers.size();
            for (int i = 0 ; i < size ; i++){
                BugTracker tracker = (BugTracker) bugTrackers.elementAt(i);
                tracker.suspend();
            }
        }
    }

    /**
     * M?thode utilis?e par les autres classes de l'IHM pour remplir la table
     * de hachage pour le mapping entre composants graphiques et constantes
     * @param uiConst
     * @param uiComp
     */
    public void addToUIComponentsMap(Integer uiConst, Object uiComp) {
        if (UIComponentsMap != null)
            UIComponentsMap.put(uiConst,uiComp);
    }
    /************************ IPlugObject IMPLEMENTATION ***********************************/

    @Override
    public Object associatedExtension(Object key, Object value) {
        //implementation by Hastable
        return associatedExtension.put(key, value);
    }

    public Object associatedBugTrackerExtension(Object key, Object value) {
        //implementation by Hastable
        Util.debug("Associate "  + key + " with " + value);
        return bugTrackerExtensions.put(key, value);
    }

    public Vector getAllTracker(){
        Vector  listTracker = new Vector();
        Enumeration e = bugTrackerExtensions.elements();
        while (e.hasMoreElements()){
            listTracker.add(e.nextElement());
        }
        return listTracker;

    }

    @Override
    public Object associatedScriptEngine(Object key, Object value) {
        //implementation by Hastable
        //associatedExtension.put(key, value);
        return associatedScriptEngine.put(key, value);
    }

    @Override
    public Object associatedTestDriver(Object key, Object value) {
        //implementation by Hastable
        //associatedExtension.put(key, value);
        return associatedTestDriver.put(key,value);
    }
    @Override
    public Object getAssociatedExtension(Object key){
        return associatedExtension.get(key);
    }

    @Override
    public Object getAssociatedScriptEngine(Object key){
        return associatedScriptEngine.get(key);
    }

    @Override
    public Object get1ssociatedTestDriver(Object key) {
        return associatedTestDriver.get(key);
    }

    @Override
    public void initExtsionTestDriver(ExtensionPoint ext) {
    }

    @Override
    public void initExtsionScriptEngine(ExtensionPoint ext){
    }

    @Override
    public void showMessage(String msg) {
        JOptionPane.showMessageDialog(ptrFrame, msg,
                                      Language.getInstance().getText("Information_"),
                                      JOptionPane.INFORMATION_MESSAGE);
    }

    @Override
    public void  init_Component(PluginManager pluginM, ExtensionPoint commonE, ExtensionPoint testE,
                                ExtensionPoint scriptE, ExtensionPoint bugTrackerE, ExtensionPoint reqMgr) {
        pluginManager = pluginM;
        CommonExtPoint = commonE;
        testDriverExtPoint = testE;
        scriptEngineExtPoint = scriptE;
        bugTrackerExtPoint = bugTrackerE;
        reqManagerExtPoint = reqMgr;
    }

    @Override
    public URL getUrlBase() {
        return urlSalome;
    }

    @Override
    public JPFManager getPluginManager() {
        return jpf;
    }

    @Override
    public void addBugTrackers(Object bugTracker) {
        bugTrackers.add(bugTracker);
    }

    @Override
    public Vector getBugTracker() {
        return bugTrackers;
    }

    @Override
    public void addXMLPrinterExtension(Object xmlPrinter){
        if (!xmlPrinters.contains(xmlPrinter)){
            xmlPrinters.add(xmlPrinter);
        }
    }

    @Override
    public Vector getXMLPrintersExtension() {
        return xmlPrinters;
    }

    public BugTracker getBugTrackerFromAttachment(Attachment pAttachment){
        BugTracker pBugTracker = null;
        if (pAttachment == null){
            return null;
        }
        int size = bugTrackers.size();
        if (size == 0 ){
            return null;
        }
        String description =  pAttachment.getDescriptionFromModel();
        int i = 0;
        while (i < size && pBugTracker == null){
            pBugTracker = (BugTracker) bugTrackers.elementAt(i);
            if (!pBugTracker.getBugTrackerAttachDesc().equals(description)){
                pBugTracker = null;
            }
            i++;
        }
        return pBugTracker;
    }

    @Override
    public void addPlgToUICompList(Integer uiConst, Common commonPlg) {
        if (associatedPlgsToCompsMap != null) {
            Object listPlgs = associatedPlgsToCompsMap.get(uiConst);

            if (listPlgs == null) {
                java.util.LinkedList list = new java.util.LinkedList();
                list.add(commonPlg);
                associatedPlgsToCompsMap.put(uiConst, list);
            } else {
                ((java.util.LinkedList)listPlgs).add(commonPlg);
            }
        }
    }

    public Object getUIComponent(Integer uiConst) {
        Object comp = null;
        if (UIComponentsMap != null)
            comp = UIComponentsMap.get(uiConst);
        return comp;
    }

    /**
     * Methode qui retourne la liste des plugins associ? ? un composant graphique
     * @param uiConst
     * @return
     */
    public java.util.LinkedList getAssociatedPluginsToUIComp(Integer uiConst) {
        java.util.LinkedList associatedPlgs = null;
        if (associatedPlgsToCompsMap != null)
            associatedPlgs = (java.util.LinkedList)associatedPlgsToCompsMap.get(uiConst);
        return associatedPlgs;
    }

    public JPFManager getJpf() {
        return jpf;
    }

    public void setJpf(JPFManager jpf) {
        this.jpf = jpf;
    }

    public Map getUIComponentsMap() {
        return UIComponentsMap;
    }

    /************************* Context Messaging *************************/
    /**
     * @param type @see JOptionPane dialog type (WARNING_MESSAGE, QUESTION_MESSAGE, ...)
     */
    public void showMessage(String msg, String info, int type) {
        JOptionPane.showMessageDialog(ptrFrame, msg,
                                      info,
                                      type);
    }

    /**
     * @param type @see JOptionPane dialog type (WARNING_MESSAGE, QUESTION_MESSAGE, ...)
     */
    public int askQuestion(String msg, String info,int type, Object[] options){
        return JOptionPane.showOptionDialog(ptrFrame,
                                            msg,
                                            info,
                                            JOptionPane.YES_NO_OPTION,
                                            type,
                                            null,
                                            options,
                                            options[1]);
    }

    @Override
    public void addReqManager(Object reqManager) {
        reqManagers.add(reqManager);
    }

    @Override
    public Vector getReqManagers() {
        return reqManagers;
    }

    public Object associatedStatistiquesExtension(Object key, Object value) {
        //implementation by Hastable
        Util.debug("Associate "  + key + " with " + value);
        return statistiquesExtensions.put(key, value);
    }

    public Vector getAllStatistiques(){
        Vector  listStatistiques = new Vector();
        Enumeration e = statistiquesExtensions.elements();
        while (e.hasMoreElements()){
            listStatistiques.add(e.nextElement());
        }
        return listStatistiques;
    }

    @Override
    public void addStatistiquess(Object statistiques) {
        statistiquess.add(statistiques);
    }

    @Override
    public Vector getStatistiques() {
        return statistiquess;
    }

    @Override
    public void  init_Component(PluginManager pluginM, ExtensionPoint commonE, ExtensionPoint testE,                    ExtensionPoint scriptE, ExtensionPoint bugTrackerE, ExtensionPoint statistiquesE, ExtensionPoint reqMgr) {              pluginManager = pluginM;                CommonExtPoint = commonE;               testDriverExtPoint = testE;             scriptEngineExtPoint = scriptE;         bugTrackerExtPoint = bugTrackerE;               statistiquesExtPoint = statistiquesE;           reqManagerExtPoint = reqMgr;
    }
}
