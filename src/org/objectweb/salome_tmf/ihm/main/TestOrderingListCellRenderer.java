//20100108 - D\ufffdbut modification Forge ORTF V1.0.0
package org.objectweb.salome_tmf.ihm.main;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.data.Test;

/**
 * Renderer for Test Ordering panel.
 * For campagne ordering, add the test liste name to the test name.
 * @author hchauvin
 * @covers SFG_ForgeORTF_TST_CMP_000040 - \ufffd2.4.4
 * @covers EDF-2.4.4
 * @jira FORTF-6
 */
public class TestOrderingListCellRenderer extends DefaultListCellRenderer {

    private boolean campaignMode = false;

    public TestOrderingListCellRenderer(boolean campaignMode){
        super();
        this.campaignMode = campaignMode;
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        Component label = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if(value instanceof DefaultMutableTreeNode && this.campaignMode){
            Object userObject = ((DefaultMutableTreeNode)value).getUserObject();
            if(userObject instanceof Test){
                Test test = (Test) userObject;
                ((JLabel)label).setText(test.getTestListFromModel().getNameFromModel() + "/" + test.getNameFromModel());
            }
        }
        return label;
    }

}
//20100108 - Fin modification Forge ORTF V1.0.0
