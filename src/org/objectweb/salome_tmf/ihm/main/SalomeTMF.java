/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;




/**
 * Applet repr?sentant l'outil de gestion de tests Voice Testing.
 * @author teaml039
 * @version 0.1
 */
public class SalomeTMF extends JApplet implements  BaseIHM , WindowListener{

    boolean problemURL = false;

    public static URL urlSalome;
    Frame ptrFrame;

    boolean closeDB = true;

    String strProject = null;
    String strLogin = null;
    int idConn = -1;


    /***************************************************************************************/
    SalomeTMFContext pSalomeTMFContext;
    SalomeTMFPanels pSalomeTMFPanels;
    /****************************************************************************************/

    boolean standAlone = false;
    static boolean exit = false;
    JavaScriptUtils pJSUtils;

    @Override
    public void init() {
        standAlone = false;
        Util.log("[SalomeTMF] - Applet init");
        onInit();
        exit = false;

    }

    void initComponent(){
        //Api.setUrlBase(getDocumentBase());
        Api.setUrlBase(urlSalome);
        try {
            Class lnfClass = Class.forName(UIManager.getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel)(lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
            Util.adaptFont();
        } catch (Exception exc) {
            Util.err("Error loading L&F: " + exc);
        }
        Util.log("[SalomeTMF->initComponent] Used Local is "+ Api.getUsedLocale());
        Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));

        ptrFrame = javax.swing.JOptionPane.getFrameForComponent(SalomeTMF.this);
        ptrFrame.addWindowListener(this);

        pSalomeTMFContext = new SalomeTMFContext(urlSalome, ptrFrame, this);
        pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, this);
        pSalomeTMFPanels.initComponentPanel();

        Container cp = this.getContentPane();
        cp.add(pSalomeTMFPanels.tabs);

    }

    private void closeMessage(){
        JOptionPane.showMessageDialog(SalomeTMF.this,
                                      Language.getInstance().getText("Vous_avez_deja_une_session_Salome_ouverte_avec_ce_navigateur") +
                                      Language.getInstance().getText("Une_seule_session_est_autorisee_par_navigateur_afin_d_eviter_les_conflits_"),
                                      Language.getInstance().getText("Erreur_"),
                                      JOptionPane.ERROR_MESSAGE);
        quit(false, false);
        closeDB = false;
    }

    boolean connectToAPI(){
        Util.log("[SalomeTMF] - Try connection to API (NbConnect = " + Api.getNbConnect() +")");
        //if (Api.isConnected() && !reload) {
        pJSUtils = new JavaScriptUtils(this);
        if(Api.getNbConnect()>0){
            JOptionPane.showMessageDialog(SalomeTMF.this,
                                          Language.getInstance().getText("Vous_avez_deja_une_session_Salome_ouverte_avec_ce_navigateur") +
                                          Language.getInstance().getText("Une_seule_session_est_autorisee_par_navigateur_afin_d_eviter_les_conflits_"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            quit(false, false);
            closeDB = false;
            return false;
            // Api.setConnected(true);
        } else {
            strLogin= pJSUtils.getLoginCookies();
            strProject = pJSUtils.getProjectCookies();
            String unixName = pJSUtils.getUnixProjectCookies();
            if(unixName == null){
                //              closeMessage();
                return false;
            } else if(unixName.equalsIgnoreCase("")){
                //              closeMessage();
                return false;
            }
            Api.openConnection(getDocumentBase());

            //reload = true;
            if (!Api.isConnected()){
                JOptionPane.showMessageDialog(SalomeTMF.this,
                                              "Can't connect to the database",
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                quit(true, false);
                return false;
            }
            /*if (Api.isIDE_DEV()) {
              tab[0]="projet_test";
              tab[1] = "marchemi";
              } else {
              tab = Tools.chooseProjectAndUser(getDocumentBase());
              }*/


            //Tools.chooseProjectAndUser(getDocumentBase());
            Api.setUserAuthentification(pJSUtils.getUserAuthentficationCookies());

            Api.initConnectionUser(strProject, strLogin);
            //                  Api.getConnectionProject(idConnection);
            //                  Api.getConnectionUser(idConnection);


            if ((strProject == null)||(strProject.equals(""))) {
                ProjectLogin dialog = new ProjectLogin(pJSUtils);
                dialog.setVisible(true);
                if(dialog.getSelectedProject() != null && dialog.getSelectedUser() != null){
                    strProject = dialog.getSelectedProject() ;
                    strLogin= dialog.getSelectedUser();
                    Util.log("project name: "+strProject);
                    Util.log("user name: "+strLogin);

                    // Recuperation du login et du mot de passe pour l'authentification
                    try {
                        Api.setStrUsername(strLogin);
                        Api.setStrMD5Password(MD5paswd.getEncodedPassword(dialog.getSelectedPassword()));
                    } catch (Exception e) {
                        Util.err(e);
                    }

                    Api.initConnectionUser(strProject, strLogin);
                    standAlone = true;
                } else {
                    quit(true, true);
                    return false;
                }
            } else {
                Api.initConnectionUser(strProject, strLogin);
            }

            /*if ((tab[0] == null)||(tab[0].equals(""))) {
              JOptionPane.showMessageDialog(SalomeTMF.this,
              Language.getInstance().getText("Probleme_dans_l_URL_saisie_"),
              Language.getInstance().getText("Erreur_"),
              JOptionPane.ERROR_MESSAGE);
              problemURL = true;
              //Api.closeConnection();
              quit(true, true);
              return false;
              } else {
              try{
              String[] tab2 = getDocumentBase().toString().split("[?=]");
              idConn = Integer.parseInt(tab2[2]);
              } catch(Exception e1) {
              Util.log("[SalomeTMF->connectToAPI] WARNING idConn can't be found");
              }
              strProject = tab[0];
              strLogin = tab[1];

              }*/
        }
        //urlSalome = getDocumentBase();
        urlSalome = Api.getCodeBase();
        return true;
    }

    void loadPlugin(){
        pSalomeTMFContext.loadPlugin(pSalomeTMFPanels);
    }


    void loadModel(){
        if (strProject == null || strLogin == null) {
            quit(true, true);
        }
        DataModel.loadFromBase(strProject, strLogin, this);
        pSalomeTMFPanels.loadModel(strProject, strLogin);
    }

    /**
     * M?thode appel?e ? chaque chargement de la page
     */
    @Override
    public void start() {
        Util.log("[SalomeTMF] - Applet start");
        onStart();
    }


    /*********************************************/

    void onStart(){
        pSalomeTMFContext.startPlugin();
    }

    void onStop(){

    }

    void onInit(){
        Util.log("[SalomeTMF] - Applet init");
        System.runFinalization();
        System.gc();
        if (connectToAPI()){
            WaitView waitView = new WaitView();
            initComponent();
            loadModel();
            loadPlugin();
            waitView.dispose();
        }

    }

    /**
     * M?thode appel? lorsque l'utilisateur quitte l'application.
     */
    @Override
    public void quit(boolean do_recup, boolean doclose) {
        Util.log("[SalomeTMF] - Applet quit (recup = " +do_recup +")" );
        if (pSalomeTMFContext != null && doclose == true){
            pSalomeTMFContext.suspendPlugin();
        }
        if (Api.isConnected() && doclose) {
            if ((!problemURL)&&(!Api.isIDE_DEV()) && idConn != -1) {

                Api.deleteConnection(idConn);
            }
            closeDB = false;
            Api.closeConnection();
        }
        URL recup;

        ptrFrame.dispose();
        ptrFrame = null;
        System.gc();
        System.runFinalization();

        if (!standAlone) {
            try {
                if (do_recup) {
                    /* r?cup?re l'URL ? partir du document courant et "page.html" */
                    recup = new URL(getDocumentBase(), "index.html");
                    /* Affiche le document apr?s avoir recup?rer le contexte courant */
                    getAppletContext().showDocument(recup);
                    repaint();
                } else {
                    getAppletContext().showDocument(new URL("http://wiki.objectweb.org/salome-tmf"));
                }
            } catch (MalformedURLException me) {
                Util.err(me);
            }
        }else {
            if (pJSUtils != null) {
                pJSUtils.closeWindow();
            }
        }
        exit = true;
    } // Fin de la m?thode quit();


    @Override
    public void stop() {
        Util.log("[SalomeTMF] - Applet stop");
    }

    @Override
    public void destroy() {
        Util.log("[SalomeTMF] Applet destroy (closeDB = " + closeDB +")");
        if (closeDB == true)
            Api.closeConnection();
    }

    /* void closeWindow(){
       try {
       //Class classJSObject = Class.forName("sun.plugin.javascript.JSObject");
       Class classJSObject = Class.forName("netscape.javascript.JSObject");
       Method meth_getWindow = classJSObject.getMethod("getWindow", new Class[] {Applet.class});
       Object pJSObject = meth_getWindow.invoke(null ,new Object[] {this});
       Util.debug("pJSObject = " + pJSObject);
       Method meth_call  = classJSObject.getMethod("call", new Class[] {String.class, Object[].class});
       meth_call.invoke(pJSObject, new Object[] {"close", null});

       } catch (Exception e) {
       Util.err(e);
       }
       }*/

    /**********************************************************************************************************************/

    @Override
    public SalomeTMFContext getSalomeTMFContext(){
        return pSalomeTMFContext;
    }

    @Override
    public SalomeTMFPanels getSalomeTMFPanels(){
        return pSalomeTMFPanels;
    }


    @Override
    public void showDocument(URL toShow , String where){
        getAppletContext().showDocument(toShow, where);
    }


    @Override
    public boolean isGraphique() {
        return true;
    }
    /***************** Windows Listener ***************/
    @Override
    public void windowClosing(WindowEvent e) {
        if (!exit)
            quit(true, true);
    }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowClosed(WindowEvent e) { }


    @Override
    public void windowDeactivated(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowOpened(WindowEvent e) { }


} // Fin de la classe SalomeTMF
