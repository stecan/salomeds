package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

public class AnomalieView extends JPanel implements ActionListener,
                                                    ListSelectionListener {

    JButton viewDefectButton;
    JButton viewTestButton;
    JPanel buttonsPanel;

    TableSorter sorter;
    JTable defectTable;
    JScrollPane tablePane;

    JTree salomeDynamicTree;
    Vector data;

    MyTableModel myTableModel;
    boolean dynamique = false;
    JDialog pJDialog;

    AnomalieView(JDialog _pJDialog) {
        pJDialog = _pJDialog;
        if (pJDialog != null) {
            dynamique = true;
        }
        data = new Vector();
        initComponent();
    }

    void updateData(Campaign pCamp) {
        try {
            // Util.debug("-------> Update data for defect in Campaigne : " +
            // pCamp.getNameFromModel());
            boolean doRefresh = false;
            Vector tmpdata = pCamp.getAllResExecAttahFromDB();
            DataModel.getDefectTableModel().clearTable();
            data.clear();
            for (int i = 0; i < tmpdata.size(); i++) {

                Vector ligne = (Vector) tmpdata.elementAt(i);
                String execNam = ((ExecutionWrapper) ligne.elementAt(0))
                    .getName();
                int idTest = ((TestCampWrapper) ligne.elementAt(1)).getIdBDD();
                Attachment pAttachment = null;
                AttachementWrapper pAttachementWrapper = (AttachementWrapper) ligne
                    .elementAt(2);
                if (pAttachementWrapper != null
                    && pAttachementWrapper instanceof UrlAttachementWrapper) {
                    pAttachment = new UrlAttachment(
                                                    (UrlAttachementWrapper) pAttachementWrapper);
                }
                Test pTest = DataModel.getCurrentProject().getTestFromModel(
                                                                            idTest);
                if (pTest == null) {
                    doRefresh = true;
                } else {
                    BugTracker pBugTracker = SalomeTMFContext.getInstance()
                        .getBugTrackerFromAttachment(pAttachment);
                    if (pBugTracker != null) {
                        // TODO je dois vérifier que l'anomalie lié a cet
                        // attachement apparetient bien au projet Jira
                        // syncyhronisé avec le projet Salomé courant

                        ArrayList dataList = new ArrayList();
                        ArrayList dataList2 = new ArrayList();
                        dataList.add(execNam);
                        dataList.add(pTest.getNameFromModel());

                        // pBugTracker.isFromid


                        String bugID = pBugTracker.getBugKey(pAttachment);
                        if (bugID != null) {
                            dataList.add(bugID);
                            dataList2.add(pTest);
                            dataList2.add(pBugTracker);
                            dataList2.add(pAttachment);
                            data.add(dataList2);
                        } else {
                            // remove attachement
                        }
                        /**
                         * The name
                         */
                        String name = pBugTracker.getName(pAttachment);
                        if (name != null)
                            dataList.add(name);

                        /**
                         * The type
                         */
                        String type = pBugTracker.getType(pAttachment);
                        if (type != null)
                            dataList.add(type);

                        /**
                         * The priority
                         */
                        String priority = pBugTracker.getPriority(pAttachment);
                        if (priority != null)
                            dataList.add(priority);

                        /**
                         * The status
                         */
                        String status = pBugTracker.getStatus(pAttachment);
                        if (status != null)
                            dataList.add(status);

                        /**
                         * Send the row to the model
                         */
                        DataModel.getDefectTableModel().addRow(dataList);
                    }
                }
            }
            if (doRefresh == true) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText(
                                                                                          "Veillez_rafraichier_les_donnees"));
            }

        } catch (Exception e) {
            Util.err(e);
            data = new Vector();

        }
    }

    void updateData(ExecutionResult pExec) {
        try {
            boolean doRefresh = false;
            Vector tmpdata = pExec.getAllResExecTestAttachFromDB();
            myTableModel.clearTable();

            for (int i = 0; i < tmpdata.size(); i++) {

                Vector ligne = (Vector) tmpdata.elementAt(i);
                int idTest = ((Integer) ligne.elementAt(0)).intValue();
                Attachment pAttachment = (Attachment) ligne.elementAt(1);
                Test pTest = DataModel.getCurrentProject().getTestFromModel(
                                                                            idTest);
                if (pTest == null) {
                    doRefresh = true;
                } else {
                    BugTracker pBugTracker = SalomeTMFContext.getInstance()
                        .getBugTrackerFromAttachment(pAttachment);
                    if (pBugTracker != null) {
                        ArrayList dataList = new ArrayList();
                        ArrayList dataList2 = new ArrayList();
                        dataList.add(pTest.getTestListFromModel()
                                     .getFamilyFromModel().getNameFromModel());
                        dataList.add(pTest.getTestListFromModel()
                                     .getNameFromModel());
                        dataList.add(pTest.getNameFromModel());
                        // dataList.add(pBugTracker.getBugTrackerName());

                        String bugID = pBugTracker.getBugKey(pAttachment);
                        // pBugTracker.get
                        if (bugID != null) {
                            dataList.add(bugID);
                            dataList2.add(pTest);
                            dataList2.add(pBugTracker);
                            dataList2.add(pAttachment);
                            data.add(dataList2);
                        } else {
                            // remove attachement
                            try {
                                ExecutionTestResult pExecutionTestResult = pExec
                                    .getExecutionTestResultFromModel(pTest);
                                pExecutionTestResult
                                    .deleteAttachementInDBAndModel(pAttachment);
                                return;
                            } catch (Exception e) {
                                // TODO: handle exception
                                Util.err(e);
                            }
                        }
                        /**
                         * The name
                         */
                        String name = pBugTracker.getName(pAttachment);
                        if (name != null)
                            dataList.add(name);

                        /**
                         * The type
                         */
                        String type = pBugTracker.getType(pAttachment);
                        if (type != null)
                            dataList.add(type);

                        /**
                         * The priority
                         */
                        String priority = pBugTracker.getPriority(pAttachment);
                        if (priority != null)
                            dataList.add(priority);

                        /**
                         * The status
                         */
                        String status = pBugTracker.getStatus(pAttachment);
                        if (status != null)
                            dataList.add(status);

                        /**
                         * Send the row to the model
                         */
                        myTableModel.addRow(dataList);
                    }
                }
            }
            if (doRefresh == true) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText(
                                                                                          "Veillez_rafraichier_les_donnees"));
            }

        } catch (Exception e) {
            data = new Vector();

        }
    }

    Test getTest(int id) {
        Test pTest = null;
        pTest = DataModel.getCurrentProject().getTestFromModel(id);
        return pTest;
    }

    void initComponent() {
        viewDefectButton = new JButton(Language.getInstance().getText(
                                                                      "Visualiser")
                                       + " " + Language.getInstance().getText("Anomalie"));
        viewDefectButton.addActionListener(this);
        viewDefectButton.setEnabled(false);

        viewTestButton = new JButton(Language.getInstance().getText(
                                                                    "Visualiser")
                                     + " " + Language.getInstance().getText("Test"));
        viewTestButton.addActionListener(this);
        viewTestButton.setEnabled(false);

        buttonsPanel = new JPanel(new GridLayout(1, 2));
        buttonsPanel.add(viewTestButton);
        buttonsPanel.add(viewDefectButton);

        if (dynamique == false) {
            myTableModel = DataModel.getDefectTableModel();

        } else {
            myTableModel = new MyTableModel();
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Famille"));
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Suite"));
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Test"));
            myTableModel.addColumnNameAndColumn("Id");
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Resume_______"));
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Type"));
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Priorite__"));
            myTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                               "Status"));
        }
        sorter = new TableSorter(myTableModel);
        defectTable = new JTable(sorter);
        sorter.setTableHeader(defectTable.getTableHeader());

        try {
            // reqTable.setDefaultRenderer(Class.forName( "java.lang.Object" ),
            // new PriorityTableCellRenderer(reqCovered));
            defectTable.setDefaultRenderer(Class.forName("java.lang.Object"),
                                           new StateTableCellRenderer());
        } catch (Exception e) {
            Util.err(e);
        }

        defectTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        defectTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = defectTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        tablePane = new JScrollPane(defectTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        setLayout(new BorderLayout());
        add(buttonsPanel, BorderLayout.NORTH);
        add(tablePane, BorderLayout.CENTER);

        /*
         * Vector allBugTracker =
         * SalomeTMFContext.getInstance().getBugTracker(); int size =
         * allBugTracker.size(); if (size > 0){ boolean add = false; JPanel
         * statePanel = new JPanel(new GridLayout(size -1, 1 )); for (int i = 0;
         * i < size; i++) { BugTracker pBugTracker =
         * (BugTracker)allBugTracker.elementAt(i); JPanel pJPanel =
         * pBugTracker.getStateColorPanel(); if (pJPanel!=null){
         * statePanel.add(pJPanel); add = true; } } if (add){ add(statePanel,
         * BorderLayout.SOUTH); } }
         */
        if (dynamique == false) {
            DynamicTree pDynamicTree = (DynamicTree) SalomeTMFContext
                .getInstance().getUIComponent(
                                              UICompCst.CAMPAIGN_DYNAMIC_TREE);
            salomeDynamicTree = pDynamicTree.getTree();
            salomeDynamicTree
                .addTreeSelectionListener(new TreeSelectionListener() {
                        @Override
                        public void valueChanged(TreeSelectionEvent e) {
                            DefaultMutableTreeNode node = (DefaultMutableTreeNode) salomeDynamicTree
                                .getLastSelectedPathComponent();

                            if (node == null)
                                return;

                            Object nodeInfo = node.getUserObject();
                            /* React to the node selection. */

                            if ((nodeInfo instanceof Campaign)) {
                                // Util.log("[requirementTestChangeListener-> Camp Changed] : "
                                // + e);
                                updateData((Campaign) nodeInfo);
                            }
                        }
                    });
        } else {
            updateData(DataModel.getObservedExecutionResult());
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        // Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }

        ListSelectionModel lsm = (ListSelectionModel) e.getSource();
        if (lsm.isSelectionEmpty()) {
            // no rows are selected
            viewDefectButton.setEnabled(false);
            viewTestButton.setEnabled(false);
        } else {
            // int selectedRow = lsm.getMinSelectionIndex();
            viewDefectButton.setEnabled(true);
            viewTestButton.setEnabled(true);
            // selectedRow is selected
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(viewDefectButton)) {
            if (pJDialog != null) {
                pJDialog.dispose();
            }
            viewDefectPerformed(e);
        } else if (e.getSource().equals(viewTestButton)) {
            viewTestPerformed(e);
        }
    }

    void viewDefectPerformed(ActionEvent e) {
        int selectedRowIndex = defectTable.getSelectedRow();

        if (selectedRowIndex != -1) {
            int index = sorter.modelIndex(selectedRowIndex);
            if (data.size() >= index) {
                try {
                    ArrayList pArrayList = (ArrayList) data.elementAt(index);
                    BugTracker pBugTracker = (BugTracker) pArrayList.get(1);
                    Attachment pAttachment = (Attachment) pArrayList.get(2);
                    pBugTracker.showBug(pAttachment);
                } catch (Exception ex) {
                    ex.printStackTrace();
                    Util.err(ex);
                }

            }
        }
    }

    void viewTestPerformed(ActionEvent e) {
        int selectedRowIndex = defectTable.getSelectedRow();

        if (selectedRowIndex != -1) {
            int index = sorter.modelIndex(selectedRowIndex);
            if (data.size() >= index) {
                try {
                    ArrayList pArrayList = (ArrayList) data.elementAt(index);
                    Test pTest = (Test) pArrayList.get(0);
                    DataModel.view(pTest);
                } catch (Exception ex) {
                    Util.err(ex);
                }
            }
        }
    }

    public class StateTableCellRenderer extends DefaultTableCellRenderer {
        Color colorToUse = null;

        @Override
        public Component getTableCellRendererComponent(JTable table,
                                                       Object value, boolean isSelected, boolean hasFocus, int row,
                                                       int column) {

            // super.getTableCellRendererComponent(table, value, isSelected,
            // hasFocus, row, column);
            super.getTableCellRendererComponent(table, value, isSelected,
                                                hasFocus, row, column);

            try {
                int index = sorter.modelIndex(row);
                if (data.size() >= index) {
                    try {
                        ArrayList pArrayList = (ArrayList) data
                            .elementAt(index);
                        Attachment pAttachment = (Attachment) pArrayList.get(2);
                        BugTracker pBugTracker = SalomeTMFContext.getInstance()
                            .getBugTrackerFromAttachment(pAttachment);
                        colorToUse = pBugTracker.getStateDefect(pAttachment);
                    } catch (Exception ex) {
                        Util.err(ex);
                    }
                }
                if (colorToUse == null) {
                    colorToUse = Color.WHITE;
                }
                if (isSelected) {
                    setBorder(BorderFactory.createLineBorder(Color.BLACK));
                }
                setBackground(colorToUse);
                setForeground(Color.BLACK);
                // setOpaque(false);
                setText((String) value);

                // super.getTableCellRendererComponent(table, value, isSelected,
                // hasFocus, row, column);

            } catch (Exception e) {
                Util.err(e);
            }
            return this;
        }
    }
}
