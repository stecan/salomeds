/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main.plugins;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.Common;


/**
 * Methods and tools for plugins management
 * @author  capg2710
 */
public class PluginsTools {
    
    /** Creates a new instance of PluginsTools */
    public PluginsTools() {
    }
    
    /**
     * Activates associated plugins to dynamic UI components
     * @param uiConst
     */
    public static void activateAssociatedPlgs(Integer uiConst) {
        LinkedList list = SalomeTMFContext.getInstance().getAssociatedPluginsToUIComp(uiConst);
        if (list != null) {
            for (int i = 0; i<list.size();i++) {
                ((Common)list.get(i)).activatePluginInDynamicComponent(uiConst);
            }
        }
    }
    
    /**
     * Returns the current project in Salome
     * @return Current project in Salome
     */
    public static Project getCurrentProject() {
        return DataModel.getCurrentProject();
    }
    
    /**
     * Returns the current user in Salome
     * @return Current user in Salome
     */
    public static User getCurrentUser() {
        return DataModel.getCurrentUser();
    }
    
    /**
     * Returns current campaign execution
     * @return current campaign execution
     */
    public static Execution getCurrentCampExecution() {
        return DataModel.getObservedExecution();
    }
    
    /**
     * Returns current campaign execution's environement
     * @return current campaign execution's environement
     */
    public static Environment getCurrentCampExecutionEnvironment() {
        Environment result;
        if (DataModel.getObservedExecution() == null) {
            result = null;
        }
        else {
            result = DataModel.getObservedExecution().getEnvironmentFromModel();
        }
        return result;
    }
    
    /**
     * Returns current campaign execution result
     * @return current campaign execution result
     */
    public static ExecutionResult getCurrentCampExecResult() {
        return DataModel.getObservedExecutionResult();
    }
    
    /**
     * Returns current test execution result
     * @return current test execution result
     */
    public static ExecutionTestResult getCurrentTestExecutionResult() {
        return DataModel.getCurrentExecutionTestResult();
    }
    
    /**
     * Returns current campaign
     * @return current campaign
     */
    public static Campaign getCurrentCampaign() {
        return DataModel.getCurrentCampaign();
    }
    
    /**
     * Returns current test
     * @return current test
     */
    public static Test getCurrentTest() {
        return DataModel.getCurrentTest();
    }
    
    /**
     * Returns current test list
     * @return current test list
     */
    public static TestList getCurrentTestList() {
        return DataModel.getCurrentTestList();
    }
    
    /**
     * Returns current family
     * @return current family
     */
    public static Family getCurrentFamily() {
        return DataModel.getCurrentFamily();
    }
    
    /**
     * Returns the associated test to the current test execution result
     * @return the associated test to the current test execution result
     */
    public static Test getTestForCurrentTestExecResult() {
        Test result;
        if (DataModel.getCurrentExecutionTestResult() == null) {
            result = null;
        }
        else {
            result = DataModel.getCurrentExecutionTestResult().getTestFromModel();
        }
        return result;
    }
    
    public static void activateBugTrackingPlgsInToolsMenu(JMenu toolsMenu,
                                                          final BugTracker bTrack) {

        JMenu bTrackSubMenu = new JMenu(bTrack.getBugTrackerName());

        //Adding the current user in Salome TMF to bug tracking database
        JMenuItem addCurrentUserItem = new JMenuItem(Language.getInstance()
                                                     .getText("add_current_usr_to_bug_db"));
        addCurrentUserItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        bTrack.addUser(DataModel.getCurrentUser());
                    } catch (Exception E) {
                        Util.err(E);
                    }
                }
            });

        bTrackSubMenu.add(addCurrentUserItem);

        // Adding the current project in Salome TMF to bug tracking database
        JMenuItem addCurrentProjectItem = new JMenuItem(Language.getInstance()
                                                        .getText("add_current_prj_to_bug_db"));
        addCurrentProjectItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        bTrack.addProject(DataModel.getCurrentProject());
                    } catch (Exception E) {
                        Util.err(E);
                    }
                }
            });

        //if (!bTrack.isUserExistsInBugDB()) addCurrentProjectItem.setEnabled(false);
        bTrackSubMenu.add(addCurrentProjectItem);

        toolsMenu.addSeparator();
        toolsMenu.add(bTrackSubMenu);

    }
                
    
}
