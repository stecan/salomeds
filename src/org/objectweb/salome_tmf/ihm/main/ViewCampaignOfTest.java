package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

public class ViewCampaignOfTest extends JDialog implements ActionListener , ListSelectionListener{
        
    Test pTest;
    Hashtable campHash;
    JButton validateButton;
    JButton viewButton;
    JTable campTable;
    JScrollPane tablePane;
        
        
    public ViewCampaignOfTest(Test _pTest){
        super(SalomeTMFContext.getInstance().getSalomeFrame());
                
        int t_x = 1024 - 100;
        int t_y = 768 - 50;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height -50 ;
        } catch(Exception E){
                        
        }
                
        pTest = _pTest;
        campHash = new Hashtable();
                
        setResizable(false);
        setModal(true);
        setTitle(Language.getInstance().getText("Campagnes_de_test"));
                
        JPanel panelButton = new JPanel(new FlowLayout());
        validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(this);
        viewButton = new JButton(Language.getInstance().getText("Visualiser"));
        viewButton.addActionListener(this);
        panelButton.add(viewButton);
        panelButton.add(validateButton);
                
        CampTableModel model = new CampTableModel();
        campTable = new JTable(model);
        model.addColumn(Language.getInstance().getText("Campagne"));
        model.addColumn(Language.getInstance().getText("Details_d_executions"));
        campTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        campTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ListSelectionModel rowSM = campTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
                
        tablePane = new JScrollPane(campTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
                
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(tablePane, BorderLayout.CENTER);
        contentPaneFrame.add(panelButton, BorderLayout.SOUTH);
        initData();
                
        /*
          this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);
        */
        centerScreen();
    }
        
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
        
        
    void initData() {
        if (pTest == null){
            close();
            return;
        }
        campHash.clear();
        ArrayList campaignList = DataModel.getCurrentProject().getCampaignOfTest(pTest);
        if (campaignList != null && campaignList.size() > 0){
            int size = campaignList.size();
            for  (int i = 0 ; i  < size ; i++){
                Campaign pCampaign = (Campaign)campaignList.get(i);
                campHash.put(pCampaign.getNameFromModel(), pCampaign);
            }
            Enumeration enumCamp = campHash.elements();
            int num = 0;
            while (enumCamp.hasMoreElements()){
                Vector data = new Vector();
                Campaign pCampaign = (Campaign) enumCamp.nextElement();
                data.add(pCampaign.getNameFromModel());
                ArrayList executionList = pCampaign.getExecutionListFromModel();
                if (executionList != null && executionList.size() > 0){
                    //Echec=Echec Inconclusif=Inconclusif Succes=Succes
                    int sizeExecutionList = executionList.size();
                    Hashtable statusTable = new Hashtable();
                    String res = "";
                    boolean first = true;
                                        
                    for (int j = 0 ; j < sizeExecutionList ; j++){
                        Execution pExec = (Execution) executionList.get(j);
                        if (pExec!= null && pExec.getExecutionResultListFromModel()!=null &&  pExec.getExecutionResultListFromModel().size()>0){
                            ArrayList executionResultList =  pExec.getExecutionResultListFromModel();
                            int sizeExecutionResList =  executionResultList.size();
                            for (int k = 0; k < sizeExecutionResList; k++){
                                ExecutionResult pExecutionResult = (ExecutionResult) executionResultList.get(k);
                                String status = pExecutionResult.getTestResultStatusFromModel(pTest);
                                Integer occStatus = (Integer) statusTable.get(status);
                                int val = 1;
                                if (occStatus != null){
                                    val = occStatus.intValue() + 1;
                                }
                                statusTable.put(status, new Integer(val));
                            }
                        } 
                    }
                    if (statusTable.isEmpty()){
                        data.add(Language.getInstance().getText("Pas_d_resultat_execution"));
                    } else {
                        Enumeration enumStatus = statusTable.keys();
                        while (enumStatus.hasMoreElements()){
                            String key = (String) enumStatus.nextElement();
                            if (first){
                                res += key + " : " + statusTable.get(key);
                                first = false;
                            } else {
                                res += ", " + key + " : " + statusTable.get(key);
                            }
                        }
                        data.add(res);
                    }
                                        
                } else {
                    data.add(Language.getInstance().getText("Pas_d_execution"));
                }
                ((CampTableModel)campTable.getModel()).insertRow(num,data);
                num++;
            }
                        
                        
        } else {
            close();
            return;
        }
    }
        
        
    void viewCampPerformed(ActionEvent e) {
        int selectedRow = campTable.getSelectedRow();
                
        if (selectedRow >-1){
            Campaign pCamp = (Campaign) campHash.get(campTable.getValueAt(selectedRow,0));
            DataModel.view(pCamp);
            close();
        }
    }
        
    /*************************** Listener *************************/
        
    void close(){
        setVisible(false);
        dispose();
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(validateButton)){
            close();
        } else if (e.getSource().equals(viewButton)){
            viewCampPerformed(e);
        }
    }
        
    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Ignore extra messages.
        if (e.getValueIsAdjusting()) {
            return;
        }
                
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
        if (lsm.isSelectionEmpty()) {
            //no rows are selected
            viewButton.setEnabled(false);
        } else {
            //int selectedRow = lsm.getMinSelectionIndex();
            viewButton.setEnabled(true);
            //selectedRow is selected
        }
    }
        
    class CampTableModel extends DefaultTableModel {
        @Override
        public boolean isCellEditable(int row, int col) {
            return false;
        } // Fin de la m?thode isCellEditable/2 
                
        public void clearTable() {
            int nbRow = getRowCount();
            for(int i=0; i < nbRow; i++) {
                removeRow(0);
            }
            fireTableStructureChanged();
        } 
    }
}
