package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.LockInfoWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLSalomeLock;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;

public class SalomeLocksPanel extends JPanel implements ActionListener {

    JTable locksTable;
        
    MyTableModel locksTableModel;
        
    JButton deletelockButton;
    JButton deleteAlllocksButton;
    JButton refreshlocksButton;
        
    ISQLSalomeLock pSQLlock;
    ISQLPersonne pSQLPersone;
        
    Vector locksVector;
        
    int project_id = -1;
        
    public SalomeLocksPanel(int project_id){
        super(new BorderLayout());
        this.project_id = project_id;
        initComponent();
    }
    void initComponent(){
        locksTableModel = new MyTableModel();
        locksTable = new JTable();
                
                
        deletelockButton = new JButton(Language.getInstance().getText("Supprimer"));
        deletelockButton.addActionListener(this);
                
        deleteAlllocksButton = new JButton(Language.getInstance().getText("Supprimer_tous_les_locks"));
        deleteAlllocksButton.addActionListener(this);
                
        refreshlocksButton = new JButton(Language.getInstance().getText("Actualiser"));
        refreshlocksButton.addActionListener(this);
                
        locksTableModel.addColumnNameAndColumn("PID");
        locksTableModel.addColumnNameAndColumn(Language.getInstance().getText("Login"));
        locksTableModel.addColumnNameAndColumn(Language.getInstance().getText("Lock_Type"));
        locksTableModel.addColumnNameAndColumn(Language.getInstance().getText("Action_Code"));
        locksTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date"));
                
        locksTable.setModel(locksTableModel);
                
        locksTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        JScrollPane tablePane = new JScrollPane(locksTable);
                
                
        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(refreshlocksButton);
        buttonSet.add(deletelockButton);
        buttonSet.add(deleteAlllocksButton);
        buttonSet.setBorder(BorderFactory.createRaisedBevelBorder());
                
        JLabel locksTableLabel = new JLabel(Language.getInstance().getText("Liste_des_locks_existantes"));
        locksTableLabel.setFont(new Font(null,Font.BOLD,20));
        locksTableLabel.setSize(300,60);
                
        JLabel noteLabel = new JLabel(Language.getInstance().getText("locks_types_precisions"));
        Language.getInstance().getText("Liste_des_locks_existantes");
        JPanel notePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        notePanel.add(noteLabel);
                
        JPanel center = new JPanel(new BorderLayout());
        center.add(BorderLayout.NORTH, locksTableLabel);
        center.add(BorderLayout.CENTER, tablePane);
        center.add(notePanel, BorderLayout.SOUTH);
        center.setBorder(BorderFactory.createEmptyBorder(20,10,10,10));   
        this.add(BorderLayout.NORTH,buttonSet);
        this.add(BorderLayout.CENTER,center);
        pSQLlock = Api.getISQLObjectFactory().getISQLSalomeLock();
        pSQLPersone = Api.getISQLObjectFactory().getISQLPersonne();
    }
        
    public void loadDataFromDB(){
        //On cree une instance pour les methodes de selection
        //pISQLlock = Api.getISQLObjectFactory().getISQLSalomeLock();
        if (pSQLlock != null){
            refreshData();
        }
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(deletelockButton)) {
            try { 
                int selectedRow = locksTable.getSelectedRow();
                if (selectedRow != -1) {
                    Integer value = (Integer) locksTableModel.getValueAt(selectedRow,0);
                    deleteLock(value.intValue());
                }
            }catch (Exception ex){}
                        
        } else if (e.getSource().equals(deleteAlllocksButton)) {
            deleteAllSession();
        } else if (e.getSource().equals(refreshlocksButton)){
            refreshData();
        }
    }
        
    void deleteLock(int pid){
        try {
            if (pSQLlock != null){
                pSQLlock.delete(project_id,pid);
                refreshData();
            }
        } catch (Exception e){          
        }
                
    }
        
    void deleteAllSession(){     
        try {
            if (pSQLlock != null){
                pSQLlock.delete(project_id);
                refreshData();
            }
        } catch (Exception e){
        }
    }
        
    void refreshData(){
        try {
            LockInfoWrapper[] tmpArray = pSQLlock.getAllProjectLocks(project_id);
            Vector tmpVector = new Vector();
            for(int i = 0; i < tmpArray.length; i++) {
                tmpVector.add(tmpArray[i]);
            }
            locksVector =  tmpVector;
            locksTableModel.clearTable();
            int nbData = locksVector.size();
            for (int i = 0 ; i < nbData ; i++ ) {
                LockInfoWrapper pLockInfo = (LockInfoWrapper)locksVector.elementAt(i);
                                
                int lock_type = pLockInfo.getLock_code();
                int test = lock_type / 100;
                if (test == 1) lock_type-=100;
                int camp = lock_type / 10;
                if (camp == 1) lock_type-=10;
                int data = lock_type;
                String lockTypeStr = new String("[T="+test+", C="+camp+", D="+data+"]");
                                                                
                String login = "unknow";
                if (pSQLPersone != null){
                    login = pSQLPersone.getLogin(pLockInfo.getPersonne_id());
                }
                locksTableModel.addValueAt(new Integer(pLockInfo.getPid()), i, 0);
                locksTableModel.addValueAt(login, i, 1);
                locksTableModel.addValueAt(lockTypeStr, i, 2);
                locksTableModel.addValueAt(new Integer(pLockInfo.getAction_code()), i, 3);
                locksTableModel.addValueAt(pLockInfo.getLockdate(), i, 4);
            }    
        }catch (Exception ex){
            Util.err(ex);
        }
    }

}
