/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.log4j.Logger;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.datawrapper.TestMethods;
import org.objectweb.salome_tmf.ihm.models.ExecutionResultMouseListener;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.ScriptEngine;

public class ExecutionView extends JPanel implements ApiConstants, DataConstants, ActionListener {
        
    final static Logger logger = Logger.getLogger(ExecutionView.class);
    
    /**
     * Table des executions
     */
    static JTable executionTable;
        
    /**
     * Table des details des executions
     */
    JTable detailsExecutionTable;
        
    /**
     * Bouton pour lancer une ex?cution
     */
    static JButton launchExecution;
        
    /**
     * Bouton pour avoir le details des resultats sur les tests
     */
    JButton resultExecution;
        
    /**
     * Bouton pour gerer les anomalies d'un resultat d'execution
     */
    JButton defectResultExecution;
    /**
     * Bouton pour supprimer une ex?cution
     */
    static JButton deleteExecution;
        
    /**
     * Bouton pour modifier une ex?cution
     */
    static JButton modifyExecutionButton;
        
    /**
     * Bouton pour supprimer une ex?cution
     */
    static JButton deleteExecutionResult;
        
    /**
     * Bouton pour reprendre une ex?cution
     */
    static JButton continueExecution;
        
    static JButton relancerExecution;
    /**
     * Bouton pour g?rer les attachements d'une ex?cution
     */
    JButton attachExecutionResultButton;
        
    /**
     * Bouton pour g?rer les attachements d'un r?sultat d'ex?cution
     */
    JButton attachExecutionButton;
    /**
     * Bouton pour ajouter une nouvelle ex?cution
     */
    static JButton addExecution;
        
    /**
     *
     */
    //private boolean automatic_finished;
        
    static JMenu modifyScriptMenu;
    static JMenuItem initScriptItem;
    static JMenuItem postScriptItem;
    static JMenuItem setUpPreEngineItem;
    static JMenuItem setUpPostEngineItem;
    static JMenuBar modifyScriptMenuBar;
        
        
    //static JMenu commitMenu;
    static JMenuItem commitInitScriptItem;
    static JMenuItem commitPostScriptItem;
    //static JMenuBar commitMenuBar;
        
        
    HashMap modifyPreScriptMap;
    HashMap modifyPostScriptMap;
        
    File importFile;
        
    TableSorter  sorter;
    TableSorter detailsSorter;
    boolean forModif = false;
    /******************************************************************************/
    /**                                                                 CONSTRUCTEUR                                                     **/
    /******************************************************************************/
    public ExecutionView() {
                
        executionTable = new JTable();
        detailsExecutionTable = new JTable();
                
        addExecution = new JButton(Language.getInstance().getText("Ajouter"));
        addExecution.setToolTipText(Language.getInstance().getText("Ajouter_une_execution"));
        addExecution.addActionListener(this);
                
        launchExecution = new JButton(Language.getInstance().getText("Lancer"));
        launchExecution.setToolTipText(Language.getInstance().getText("Lancer_l_execution"));
        launchExecution.addActionListener(this);
                
        resultExecution = new JButton(Language.getInstance().getText("Details"));
        resultExecution.setToolTipText(Language.getInstance().getText("Details_sur_les_resultats_de_l_execution"));
        resultExecution.setEnabled(false);
        resultExecution.addActionListener(this);
                
        defectResultExecution =  new JButton(Language.getInstance().getText("Anomalies"));
        defectResultExecution.setEnabled(false);
        defectResultExecution.addActionListener(this);
                
                
        modifyExecutionButton = new JButton(Language.getInstance().getText("Modifier"));
        modifyExecutionButton.setToolTipText(Language.getInstance().getText("Modifier_une_execution"));
        modifyExecutionButton.setEnabled(false);
        modifyExecutionButton.addActionListener(this);
                
        deleteExecution = new JButton(Language.getInstance().getText("Supprimer"));
        deleteExecution.setToolTipText(Language.getInstance().getText("Supprimer_une_execution"));
        deleteExecution.setEnabled(false);
        deleteExecution.addActionListener(this);
                
        deleteExecutionResult = new JButton(Language.getInstance().getText("Supprimer"));
        deleteExecutionResult.setToolTipText(Language.getInstance().getText("Supprimer_un_resultat_d_execution"));
        deleteExecutionResult.setEnabled(false);
        deleteExecutionResult.addActionListener(this);
                
        continueExecution = new JButton(Language.getInstance().getText("Reprendre"));
        continueExecution.setToolTipText(Language.getInstance().getText("Reprendre_une_execution"));
        continueExecution.setEnabled(false);
        continueExecution.addActionListener(this);
                
        relancerExecution  = new JButton(Language.getInstance().getText("Reexecuter"));
        relancerExecution.setToolTipText(Language.getInstance().getText("Reexecuter_une_execution"));
        relancerExecution.setEnabled(false);
        relancerExecution.addActionListener(this);
                
        attachExecutionButton = new JButton(Language.getInstance().getText("Attachements"));
        attachExecutionButton.setEnabled(false);
        attachExecutionButton.addActionListener(this);
                
        attachExecutionResultButton = new JButton(Language.getInstance().getText("Attachements"));
        attachExecutionResultButton.setEnabled(false);
        attachExecutionResultButton.addActionListener(this);
                
        //commitMenu = new JMenu("Actualiser Script");
        commitInitScriptItem = new JMenuItem(Language.getInstance().getText("Actualiser_pre_Script"));
        commitInitScriptItem.addActionListener(this);
                
        commitPostScriptItem = new JMenuItem(Language.getInstance().getText("Actualiser_post_Script"));
        commitPostScriptItem.addActionListener(this);
                
        //commitMenu.add(commitInitScriptItem);
        //commitMenu.add(commitPostScriptItem);
        //commitMenuBar = new JMenuBar();
        //commitMenuBar.add(commitMenu);
                
                
        //commitMenu.setEnabled(false);
                
                
                
        modifyScriptMenu = new JMenu(Language.getInstance().getText("Script"));
        initScriptItem = new JMenuItem(Language.getInstance().getText("Modifier_le_pre_Script"));
        initScriptItem.addActionListener(this);
                
                
        postScriptItem = new JMenuItem(Language.getInstance().getText("Modifier_le_post_Script"));
        postScriptItem.addActionListener(this);
                
        setUpPostEngineItem = new JMenuItem(Language.getInstance().getText("SetUp_Post_script"));
        setUpPostEngineItem.addActionListener(this);
        setUpPreEngineItem = new JMenuItem(Language.getInstance().getText("SetUp_Pre_script"));
        setUpPreEngineItem.addActionListener(this);
                
                
        modifyScriptMenu.add(initScriptItem);
        modifyScriptMenu.add(commitInitScriptItem);
        modifyScriptMenu.add(setUpPreEngineItem);
                
        modifyScriptMenu.add(postScriptItem);
        modifyScriptMenu.add(commitPostScriptItem);
        modifyScriptMenu.add(setUpPostEngineItem);
                
        modifyScriptMenuBar = new JMenuBar();
        modifyScriptMenuBar.add(modifyScriptMenu);
        modifyScriptMenu.setEnabled(false);
                
                
                
        launchExecution.setEnabled(false);
        deleteExecution.setEnabled(false);
        resultExecution.setEnabled(false);
                
                
        //JPanel executionButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel executionButtonsPanel = new JPanel(new GridLayout(1,6));
        executionButtonsPanel.add(addExecution);
        executionButtonsPanel.add(modifyExecutionButton);
        executionButtonsPanel.add(deleteExecution);
        executionButtonsPanel.add(launchExecution);
        executionButtonsPanel.add(attachExecutionButton);
        executionButtonsPanel.add(modifyScriptMenuBar);
        //executionButtonsPanel.add(commitMenuBar);
                
        executionButtonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());
                
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_EXECUTIONS_BUTTONS_PANEL,executionButtonsPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_EXECUTIONS_BUTTONS_PANEL);
                
        //JPanel executionResultButtonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel executionResultButtonsPanel = new JPanel(new GridLayout(1,6));
        executionResultButtonsPanel.add(resultExecution);
        executionResultButtonsPanel.add(continueExecution);
        executionResultButtonsPanel.add(relancerExecution);
        executionResultButtonsPanel.add(defectResultExecution);
        executionResultButtonsPanel.add(attachExecutionResultButton);
        executionResultButtonsPanel.add(deleteExecutionResult);
        executionResultButtonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());
                
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_EXECUTION_RESULTS_BUTTONS_PANEL,executionResultButtonsPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_EXECUTION_RESULTS_BUTTONS_PANEL);
                
        sorter = new TableSorter(DataModel.getExecutionTableModel());
        executionTable.setModel(sorter);
        sorter.setTableHeader(executionTable.getTableHeader());
        //              executionTable.setModel(DataModel.getExecutionTableModel());
        executionTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        executionTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        executionTable.getColumnModel().getColumn(0).setMaxWidth(18);
        executionTable.getColumnModel().getColumn(0).setWidth(18);
        executionTable.getColumnModel().getColumn(0).setMinWidth(18);
        executionTable.getColumnModel().getColumn(0).setPreferredWidth(18);
        executionTable.getColumnModel().getColumn(0).setResizable(false);
                
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_EXECUTIONS_TABLE,executionTable);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_EXECUTIONS_TABLE);
                
        JScrollPane tablePane = new JScrollPane(executionTable);
        tablePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Executions")));
                
        ListSelectionModel rowSM = executionTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;
                    Util.log("[ExcecutionView] Event is " + e);
                    int selectedRowIndex = executionTable.getSelectedRow();
                                
                    if (selectedRowIndex != -1) {
                        //Execution exec = DataModel.getCurrentCampaign().getExecution(selectedRowIndex);
                        //Util.debug("Get Execution "+ (String)sorter.getValueAt(selectedRowIndex,1) +" for current campaign "+ DataModel.getCurrentCampaign().getNameFromModel());
                        Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)sorter.getValueAt(selectedRowIndex,1));
                        //Util.debug("Execution is " + exec);
                        if (exec == null){
                            return;
                        }
                        DataModel.setObservedExecution(exec);
                        DataModel.getExecutionResultTableModel().clearTable();
                        if (Permission.canExecutCamp()) launchExecution.setEnabled(true);
                        if (Permission.canExecutCamp()) deleteExecution.setEnabled(true);
                        attachExecutionButton.setEnabled(true);
                        if (Permission.canUpdateCamp() || Permission.canExecutCamp()) modifyExecutionButton.setEnabled(true);
                        if ((Permission.canUpdateCamp() || Permission.canExecutCamp()) && (exec.getPreScriptFromModel() != null || exec.getPostScriptFromModel() != null)) {
                            modifyScriptMenuBar.setEnabled(true);
                            modifyScriptMenu.setEnabled(true);
                            if (exec.getPreScriptFromModel() != null) {
                                initScriptItem.setEnabled(true);
                                setUpPreEngineItem.setEnabled(true);
                            } else {
                                initScriptItem.setEnabled(false);
                                setUpPreEngineItem.setEnabled(false);
                            }
                            if (exec.getPostScriptFromModel() != null) {
                                postScriptItem.setEnabled(true);
                                setUpPostEngineItem.setEnabled(true);
                            } else {
                                postScriptItem.setEnabled(false);
                                setUpPostEngineItem.setEnabled(false);
                            }
                        } else {
                            modifyScriptMenuBar.setEnabled(false);
                            modifyScriptMenu.setEnabled(false);
                        }
                        if (Api.isConnected() && Permission.canUpdateCamp() && isCommitablePreScript((String)executionTable.getModel().getValueAt(selectedRowIndex,1))) {
                            commitInitScriptItem.setEnabled(true);
                        } else {
                            commitInitScriptItem.setEnabled(false);
                        }
                        if (Api.isConnected() && Permission.canUpdateCamp() && isCommitablePostScript((String)executionTable.getModel().getValueAt(selectedRowIndex,1))) {
                            commitPostScriptItem.setEnabled(true);
                        } else {
                            commitPostScriptItem.setEnabled(false);
                        }
                                        
                                        
                        for(int i = 0; i < exec.getExecutionResultListFromModel().size(); i ++) {
                            ArrayList resultList = new ArrayList();
                            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getNameFromModel());
                            Util.log("[ExecutionView] try to format : " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionDateFromModel().toString() + "  " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTimeFromModel().toString());
                            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                            resultList.add(formater.format(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionDateFromModel()).toString() + "  " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTimeFromModel().toString());
                            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTesterFromModel());
                            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionStatusFromModel());
                            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getFormatedStatFromModel(
                                    Language.getInstance().getText("Initial_SUCCES"),
                                    Language.getInstance().getText("Initial_ECHEC"),
                                    Language.getInstance().getText("Initial_INCON"),
                                    Language.getInstance().getText("Initial_NOTAPPLI"),
                                    Language.getInstance().getText("Initial_BLOCKED")                       
                            ));
                                                
                            DataModel.getExecutionResultTableModel().addRow(resultList);
                        }
                    } else {
                        //launchExecution.setEnabled(false);
                        deleteExecution.setEnabled(false);
                        attachExecutionButton.setEnabled(false);
                        modifyExecutionButton.setEnabled(false);
                        modifyScriptMenuBar.setEnabled(false);
                        initScriptItem.setEnabled(false);
                        setUpPreEngineItem.setEnabled(false);
                        postScriptItem.setEnabled(false);
                        setUpPostEngineItem.setEnabled(false);
                        commitInitScriptItem.setEnabled(false);
                        commitPostScriptItem.setEnabled(false);
                        modifyScriptMenu.setEnabled(false);
                    }
                }
            });
                
                
        detailsSorter = new TableSorter(DataModel.getExecutionResultTableModel());
        detailsExecutionTable.setModel(detailsSorter);
        detailsSorter.setTableHeader(detailsExecutionTable.getTableHeader());
        //              detailsExecutionTable.setModel(DataModel.getExecutionResultTableModel());
        detailsExecutionTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        detailsExecutionTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        detailsExecutionTable.addMouseListener(new ExecutionResultMouseListener());
                
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_EXECUTION_RESULTS_TABLE,detailsExecutionTable);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_EXECUTION_RESULTS_TABLE);
                
        JScrollPane detailsTablePane = new JScrollPane(detailsExecutionTable);
        detailsTablePane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Resultats")));
                
        ListSelectionModel detailsRowSM = detailsExecutionTable.getSelectionModel();
        detailsRowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;
                    //Util.log(Language.getInstance().getText("Execution_result_tab_listener"));
                                
                    int selectedRowIndex = detailsExecutionTable.getSelectedRow();
                    if (selectedRowIndex != -1) {
                        resultExecution.setEnabled(true);
                        defectResultExecution.setEnabled(true);
                        if (Permission.canExecutCamp()) { 
                            deleteExecutionResult.setEnabled(true);
                        }
                        attachExecutionResultButton.setEnabled(true);
                                        
                        DataModel.setObervedExecutionResult(DataModel.getObservedExecution().getExecutionResultFromModel((String)detailsSorter.getValueAt(selectedRowIndex, 0)));
                        if (DataModel.getObservedExecution().getExecutionResultFromModel((String)detailsSorter.getValueAt(selectedRowIndex, 0)).getExecutionStatusFromModel().equals(INCOMPLETED) ||
                            DataModel.getObservedExecution().getExecutionResultFromModel((String)detailsSorter.getValueAt(selectedRowIndex, 0)).getExecutionStatusFromModel().equals(INTERRUPT)) {
                            if (Permission.canExecutCamp()){ 
                                continueExecution.setText(Language.getInstance().getText("Reprendre"));
                                continueExecution.setEnabled(true);
                                relancerExecution.setEnabled(false);
                                forModif = false;
                            }
                        } else {
                            if (Api.isLockExecutedTest()){
                                continueExecution.setText(Language.getInstance().getText("Reprendre"));
                                continueExecution.setEnabled(false);
                                relancerExecution.setEnabled(false);
                                forModif = false;
                            } else {
                                //change label bouton
                                continueExecution.setText(Language.getInstance().getText("Modifier"));
                                continueExecution.setEnabled(true);
                                forModif = true;
                                relancerExecution.setEnabled(true);
                            }
                        }
                    } else {
                        attachExecutionResultButton.setEnabled(false);
                        resultExecution.setEnabled(false);
                        defectResultExecution.setEnabled(false);
                        continueExecution.setEnabled(false);
                        relancerExecution.setEnabled(false);
                        deleteExecutionResult.setEnabled(false);
                    }
                                
                }
            });
                
        JPanel executionPanel = new JPanel(new BorderLayout());
        executionPanel.add(executionButtonsPanel, BorderLayout.NORTH);
        executionPanel.add(tablePane, BorderLayout.CENTER);
                
        JPanel resultPanel = new JPanel(new BorderLayout());
        resultPanel.add(executionResultButtonsPanel, BorderLayout.NORTH);
        resultPanel.add(detailsTablePane, BorderLayout.CENTER);
                
        initData();
                
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(executionPanel);
        this.add(Box.createRigidArea(new Dimension(1,10)));
        this.add(resultPanel);
                
    } // Fin du constructeur ExecutionView/0
        
    public static JTable getTable() {
        return executionTable;
    }
        
    /**
     *
     *
     */
    public static void giveAccessToIhmExecutionView() {
        if (!Permission.canExecutCamp()) {
            deleteExecution.setEnabled(false);
            deleteExecutionResult.setEnabled(false);
        }
        if (!Permission.canCreateCamp() && !Permission.canExecutCamp()) {
            addExecution.setEnabled(false);
        }
        if (!Permission.canUpdateCamp() && !Permission.canExecutCamp()) {
            modifyExecutionButton.setEnabled(false);
            modifyScriptMenuBar.setEnabled(false);
            // commitMenuBar.setEnabled(false);
        }
        if (!Permission.canExecutCamp()) {
            launchExecution.setEnabled(false);
            continueExecution.setEnabled(false);
            relancerExecution.setEnabled(false);
        }
    }
        
    private boolean isCommitablePreScript(String execName) {
        Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel(execName);
        Boolean bool = (Boolean)modifyPreScriptMap.get(exec);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }
        
    private boolean isCommitablePostScript(String execName) {
        Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel(execName);
        Boolean bool = (Boolean)modifyPostScriptMap.get(exec);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }
        
    private void initData() {
        modifyPreScriptMap = new HashMap();
        modifyPostScriptMap = new HashMap();
        for (int i=0; i < executionTable.getModel().getRowCount(); i++) {
            modifyPreScriptMap.put(executionTable.getModel().getValueAt(i,0), new Boolean(false));
            modifyPostScriptMap.put(executionTable.getModel().getValueAt(i,0), new Boolean(false));
        }
    }
        
    private void modifyScript(String type) {
        int selectedRow = executionTable.getSelectedRow();
        if( selectedRow != -1) {
            Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)executionTable.getModel().getValueAt(selectedRow,1));
            try {
                Cursor c = new Cursor(Cursor.WAIT_CURSOR);
                ExecutionView.this.setCursor(c);
            } catch (Exception ex) {
                Util.err(ex);
                return;
            }
            try {
                Script pScript = null;
                String plugSeting ="";
                Hashtable param = new Hashtable();
                param.put("salome_projectName", DataModel.getCurrentProject().getNameFromModel());
                param.put("salome_ProjectObject", DataModel.getCurrentProject());
                param.put("salome_debug", new Boolean(true));
                param.put(Language.getInstance().getText("salome_CampagneName"), "");
                param.put("salome_ExecName", exec.getNameFromModel()); // Add MM
                param.put("salome_environmentName", "");
                param.put("salome_TestName", "");
                param.put("salome_SuiteTestName", "");
                param.put("salome_FamilyName", "");
                param.put("testLog","");
                param.put("Verdict", "");
                int plugScriptType;
                String fileScript = null;
                                
                if (type.equals(PRE_SCRIPT)) {
                    pScript = exec.getPreScriptFromModel();
                    plugScriptType = ScriptEngine.PRE_SCRIPT;
                } else {
                    pScript = exec.getPostScriptFromModel();
                    plugScriptType = ScriptEngine.POST_SCRIPT;
                                        
                }
                if (pScript == null){
                    JOptionPane.showMessageDialog(ExecutionView.this,
                                                  Language.getInstance().getText("Impossible_de_recuperer_le_fichier_depuis_la_base"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
                plugSeting = pScript.getPlugArgFromModel();
                                
                if (Api.isConnected()) {
                    //importFile = ConnectionData.getCampTestSelect().getScriptOfExecution(DataModel.getCurrentCampaign().getName(),exec.getName(), pScript.getName(), type);
                    //importFile = exec.getExecScriptFromDB(pScript.getNameFromModel(), type);
                    importFile = exec.getExecScriptFromDB(type);
                    fileScript = Tools.speedpurge(importFile.getAbsolutePath());
                } else {
                    fileScript = Tools.speedpurge(pScript.getNameFromModel());
                }
                                
                if (fileScript != null) {
                    ScriptEngine pEngine = pScript.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(pScript. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().urlSalome, SalomeTMFContext.getInstance().jpf);
                    if (pEngine != null){
                        pEngine.editScript(fileScript,plugScriptType, pScript, param, plugSeting);
                        //inter.eval("desktop(\""+ Tools.speedpurge(importFile.getAbsolutePath())+ "\");");
                    } else {
                        JOptionPane.showMessageDialog(ExecutionView.this,
                                                      Language.getInstance().getText("Impossible_d_initialiser_le_plugin_du_script"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(ExecutionView.this,
                                                  Language.getInstance().getText("Impossible_de_recuperer_le_fichier_depuis_la_base"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(ExecutionView.this,
                                              Language.getInstance().getText("Erreur_pour_lancer_l_editeur_de_script__") + ex.getMessage(),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
                        
            try {
                Cursor c = new Cursor(Cursor.DEFAULT_CURSOR);
                ExecutionView.this.setCursor(c);
            } catch (Exception ee) {
                Util.err(ee);
            }
            //commitMenu.setEnabled(true);
            if (type.equals(PRE_SCRIPT)) {
                commitInitScriptItem.setEnabled(true);
                commitPostScriptItem.setEnabled(false);
                modifyPreScriptMap.put(exec, new Boolean(true));
            } else {
                commitPostScriptItem.setEnabled(true);
                commitInitScriptItem.setEnabled(false);
                modifyPostScriptMap.put(exec, new Boolean(true));
            }
                        
        }
    }
        
    private void commitScript(String type) {
        if (Api.isConnected()) {
            int selectedRow = executionTable.getSelectedRow();
            if( selectedRow != -1) {
                Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)executionTable.getModel().getValueAt(selectedRow,1));
                int transNumber = -1;
                try {
                    // BdD
                    transNumber = Api.beginTransaction(10, ApiConstants.UPDATE_ATTACHMENT);
                    //Date date = new Date(importFile.lastModified());
                    /*ConnectionData.getCampTestUpdate().updateScriptForExecution(DataModel.getCurrentCampaign().getName(), exec.getName(), type, importFile.getAbsolutePath());
                      ConnectionData.getCampTestUpdate().updateScriptDateForExecution(DataModel.getCurrentCampaign().getName(), exec.getName(), type, importFile.getName(), date);
                      ConnectionData.getCampTestUpdate().updateScriptLengthForExecution(DataModel.getCurrentCampaign().getName(), exec.getName(), type, importFile.getName(), importFile.length());*/
                    if (type.equals(PRE_SCRIPT)) {
                        Script initScript = exec.getPreScriptFromModel();
                        initScript.updateInDB(importFile);
                        /*initScript.updateContentInDB(importFile.getAbsolutePath());
                          initScript.updateDateInDB(date);
                          initScript.updateLengthInDB(importFile.length());
                        */
                    } else {
                        Script postScript = exec.getPostScriptFromModel();
                        postScript.updateInDB(importFile);
                        /*postScript.updateContentInDB(importFile.getAbsolutePath());
                          postScript.updateDateInDB(date);
                          postScript.updateLengthInDB(importFile.length());
                        */
                    }
                    Api.commitTrans(transNumber);
                    transNumber = -1;
                    // IHM
                    if (type.equals(PRE_SCRIPT)) {
                        modifyPreScriptMap.put(exec, new Boolean(false));
                        commitInitScriptItem.setEnabled(false);
                                                
                    } else {
                        modifyPostScriptMap.put(exec, new Boolean(false));
                        commitPostScriptItem.setEnabled(false);
                                                
                    }
                                        
                    JOptionPane.showMessageDialog(ExecutionView.this,
                                                  Language.getInstance().getText("Le_fichier_a_ete_correctement_archive"),
                                                  Language.getInstance().getText("Info"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                    //}
                } catch (FileNotFoundException fe) {
                    Util.err(fe);
                    Api.forceRollBackTrans(transNumber);
                    JOptionPane.showMessageDialog(ExecutionView.this,
                                                  Language.getInstance().getText("Impossible_de_trouver_le_fichier_Vous_pouvez_l_importer_en_cliquant_sur_le_bouton_Modifier"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    Tools.ihmExceptionView(exception);
                }
                                
            }
        } else {
            JOptionPane.showMessageDialog(ExecutionView.this,
                                          Language.getInstance().getText("Impossible__Vous_n_etes_pas_connecte_a_la_base"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }
                
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        Object source = e.getSource();
        if (source.equals(addExecution)){
            addExecutionPerformed(e);
        } else if (source.equals(launchExecution)){
            launchExecutionPerformed(e);
        } else if (source.equals(resultExecution)){
            resultExecutionPerformed(e);
        } else if (source.equals(modifyExecutionButton)){
            modifyExecutionPerformed(e);
        } else if (source.equals(deleteExecution)){
            deleteExecutionPerformed(e);
        } else if (source.equals(deleteExecutionResult)){
            deleteExecutionResultPerformed(e);
        } else if (source.equals(continueExecution)){
            continueExecutionPerformed(e, forModif);
        } else if (source.equals(relancerExecution)){
            reloadExecutionPerformed(e);
        } else if (source.equals(attachExecutionButton)){
            attachExecutionPerformed(e);
        } else if (source.equals(attachExecutionResultButton)){
            attachExecutionResultPerformed(e);
        } else if (source.equals(initScriptItem)){
            modifyinitScriptPerformed(e);
        } else if (source.equals(postScriptItem)){
            modifypostScriptPerformed(e);
        } else if (source.equals(commitPostScriptItem)){
            commitPostScriptPerformed(e);
        } else if (source.equals(commitInitScriptItem)){
            commitInitScriptPerformed(e);
        } else if (source.equals(setUpPostEngineItem)){
            setUpEnginePerformed(e, POST_SCRIPT );
        } else if (source.equals(setUpPreEngineItem)){
            setUpEnginePerformed(e, PRE_SCRIPT );
        } else if (source.equals(defectResultExecution)){
            defectResultExecutionPerformed(e);
        }
    }
    void  defectResultExecutionPerformed(ActionEvent e) {
        new AnomalieDialog();
    }
        
    public void setUpEnginePerformed(ActionEvent e, String type) {
        Script pScript = null;
        int selectedRow = executionTable.getSelectedRow();
        Execution exec;
        if( selectedRow != -1) {
            exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)executionTable.getModel().getValueAt(selectedRow,1));
                        
        } else {
            return ;
        }
                
        if (type.equals(PRE_SCRIPT)) {
            pScript = exec.getPreScriptFromModel();
        } else {
            pScript = exec.getPostScriptFromModel();
        }
        ScriptEngine pEngine = pScript.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(pScript. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().urlSalome, SalomeTMFContext.getInstance().jpf);
        if (pEngine != null){
            String oldplugArg = pScript.getPlugArgFromModel();
            String plugArg = pEngine.modifyEngineAgument(pScript);
            if (oldplugArg.equals(plugArg)){
                return;
            }
            try {
                pScript.updatePlugArgInDBAndModel(plugArg);
                JOptionPane.showMessageDialog(ExecutionView.this,
                                              Language.getInstance().getText("Le_fichier_a_ete_correctement_archive"),
                                              Language.getInstance().getText("Info"),
                                              JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception exception) {
                Util.err(exception);
                pScript.updatePlugArgInModel(oldplugArg);
                Tools.ihmExceptionView(exception);
            }
        } else {
            JOptionPane.showMessageDialog(ExecutionView.this,
                                          Language.getInstance().getText("Impossible_d_initialiser_le_plugin_du_script"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }
        
    public void addExecutionPerformed(ActionEvent e) {
        if (DataModel.getCurrentCampaign().getTestListFromModel().size() == 0) {
            JOptionPane.showMessageDialog(ExecutionView.this,
                                          Language.getInstance().getText("Vous_ne_pouvez_pas_creer_d_execution_pour_cette_campagne_car_elle_ne_contient_aucun_test_"),
                                          Language.getInstance().getText("Erreur"),
                                          JOptionPane.ERROR_MESSAGE);
        } else {
                        
            AskNewExecution askNewExecution = new AskNewExecution(null);
            if (askNewExecution.getExecution() != null) {
                try {
                                        
                    // BdD 
                    Execution exec = askNewExecution.getExecution();
                    //exec.addInBDAndModel(DataModel.getCurrentUser(), DataModel.getCurrentCampaign(), askNewExecution.getInitScriptFile(), askNewExecution.getRestitutionScriptFile());
                    DataModel.getCurrentCampaign().addExecutionInDBAndModel(exec, DataModel.getCurrentUser(), askNewExecution.getInitScriptFile(), askNewExecution.getRestitutionScriptFile());
                                        
                    // IHM
                    ArrayList executionData = new ArrayList();
                    executionData.add(new Boolean(false));
                    executionData.add(askNewExecution.getExecution().getNameFromModel());
                    executionData.add(askNewExecution.getExecution().getEnvironmentFromModel());
                    executionData.add(askNewExecution.getExecution().getExecutionStatusFromModel());
                    executionData.add(askNewExecution.getExecution().getCreationDateFromModel());
                    executionData.add(askNewExecution.getExecution().getAttachmentMapFromModel().keySet());
                    executionData.add("");
                    DataModel.getExecutionTableModel().addRow(executionData);
                    executionTable.getColumnModel().getColumn(0).setMaxWidth(18);
                    
                    logger.info(LoggingData.getData() +  
                            "Kampanie: " +
                            DataModel.getCurrentCampaign().toString() + "\n" +
                            "Execution: " + 
                            exec.getNameFromModel().toString() + 
                            " Successfully added!");
                    
                                        
                } catch (Exception exception) {
                    Util.err(exception);
                    Tools.ihmExceptionView(exception);
                }
            }
        }
    }
        
    public void launchExecutionPerformed(ActionEvent e) {
        if (DataModel.getCurrentCampaign() != null) {
            try {
                if (!DataModel.getCurrentCampaign().isValideModel()){
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("Update_data"),
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch(Exception e1){
                Tools.ihmExceptionView(e1);
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return ;
            }
                
            if (DataModel.getCurrentCampaign().getTestListFromModel().size() > 0) {
                DataModel.getSelectedExecution().clear();
                
                boolean found = false;
                for (int k = 0; k < DataModel.getExecutionTableModel().getRowCount(); k++) {
                    if (((Boolean)sorter.getValueAt(k,0)).booleanValue()) {
                        found = true;
                        DataModel.addSelectedExecution(DataModel.getCurrentCampaign().getExecutionFromModel((String)sorter.getValueAt(k,1)));
                    }
                }
                if (!found) {
                    int selectedRow = executionTable.getSelectedRow();
                    Execution exec;
                    if( selectedRow != -1) {
                        exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)executionTable.getModel().getValueAt(selectedRow,1));
                        DataModel.addSelectedExecution(exec);
                    }
                }
                                
                ArrayList pListExec = DataModel.getSelectedExecution();
                try {
                    int nbExec = pListExec.size();
                    for (int i = 0;  i < nbExec; i ++) {
                        Execution pExec = (Execution) pListExec.get(i);
                        if (!pExec.isValideModel()){
                            SalomeTMFContext.getInstance().showMessage(
                                                                       Language.getInstance().getText("Update_data"),
                                                                       Language.getInstance().getText("Erreur_"),
                                                                       JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                } catch(Exception e1){
                    Util.err(e1);
                    Tools.ihmExceptionView(e1);
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("Update_data"),
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);
                    return ;
                }
// stecan, 17.11.2017 commented: In Sirona is is not used for 11 years!           
//                OptionExecDialog pOptionExecDialog = new OptionExecDialog(SalomeTMFContext.getInstance().getSalomeFrame(), false, false, true);
//                if (pOptionExecDialog.isOkSelected()){
//                    TestMethods.runExecution(DataModel.getSelectedExecution(), this, false, pOptionExecDialog.isAssignedTestSelected());
//                }
                TestMethods.runExecution(DataModel.getSelectedExecution(), this, false, false);
            } else {
                JOptionPane.showMessageDialog(ExecutionView.this,
                                              Language.getInstance().getText("Il_n_y_a_aucun_test_dans_la_campagne_"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.WARNING_MESSAGE);
            }
        } 
        /*
          if (DataModel.getCurrentCampaign() != null) {
          // La campagne doit contenir au moins un test
          if (DataModel.getCurrentCampaign().getTestList().size() > 0) {
          // L'ex?cution affich?e ? l'?cran
          //int selectedRowIndex = executionTable.getSelectedRow();
          // Mise ? jour de la liste des ex?cutions ? jouer
          DataModel.getSelectedExecution().clear();
                     
          for (int k = 0; k < DataModel.getExecutionTableModel().getRowCount(); k++) {
          if (((Boolean)sorter.getValueAt(k,0)).booleanValue()) {
          //DataModel.addSelectedExecution(DataModel.getCurrentCampaign().getExecution(k));
          DataModel.addSelectedExecution(DataModel.getCurrentCampaign().getExecution((String)sorter.getValueAt(k,1)));
          }
          }
          // il doit y avoir au moins une ex?cution s?lectionn?e
          if (DataModel.getSelectedExecution().size() > 0) {
          // table des attachements ? supprimer
          HashMap attachToBeSuppress = new HashMap();
          String message = "";
          for (int i = 0;  i < DataModel.getSelectedExecution().size(); i ++) { // FOR 1
          HashSet setOfParam = new HashSet();
          // R?cup?ration de tous les param?tres de la campagne
          for (int k = 0; k < DataModel.getCurrentCampaign().getTestList().size(); k++) {
          Test test = (Test)DataModel.getCurrentCampaign().getTestList().get(k);
          for (int j = 0; j < test.getParameterList().size(); j++) {
          setOfParam.add(test.getParameterList().get(j));
          }
          }
          // On v?rifie que tous les param?tres ont une valuation;
          ArrayList notValuedParamList = new ArrayList();
          message = TestMethods.notValuedParamListInDataSet(setOfParam, ((Execution)DataModel.getSelectedExecution().get(i)).getDataSet(), notValuedParamList);
          if (notValuedParamList.size() > 0) {
          message = "Les parametres :\n" + message + "de l'execution <" +  ((Execution)DataModel.getSelectedExecution().get(i)).getName() + "> ne sont pas values.";
          }
          }
          if (!message.equals("")) {
          JOptionPane.showMessageDialog(ExecutionView.this,
          "Attention !\n" + message,
          "Attention !",
          JOptionPane.INFORMATION_MESSAGE);
          }
                          
          CallExecThread precCallExecThread = null;
          // on parcourt la liste des ex?cutions s?lectionn?es
          //for (int i = 0;  i < DataModel.getSelectedExecution().size(); i ++) { // FOR 1
          for (int i = DataModel.getSelectedExecution().size()-1; i >= 0 ; i --) { // FOR 1
                            
          // Execution courante
          Execution exec = (Execution)DataModel.getSelectedExecution().get(i);
          Api.log(">>>>EXEC LANCEE = " + exec.getName());
          // R?sultat d'ex?cution final
          ExecutionResult finalExecResult = new ExecutionResult();
          finalExecResult.setNumberOfFail(0);
          finalExecResult.setNumberOfSuccess(0);
          finalExecResult.setNumberOfUnknow(0);
          finalExecResult.setName(exec.getName() + "_" + exec.getExecutionResultList().size());
          finalExecResult.setExecutionDate(new Date(GregorianCalendar.getInstance().getTimeInMillis()));
          finalExecResult.setTester(DataModel.getCurrentUser().getLastName() + " " + DataModel.getCurrentUser().getFirstName());
                              
          Tools.initExecutionResultMap(DataModel.getCurrentCampaign().getTestList(), finalExecResult, exec.getCampagne());
                              
                              
          //Liste des r?sultats d'ex?cutions de chaque sous-liste
          // Liste des tests group?s en sous-listes (manuels/automatiques)
          Vector pAllTestToExecute = new Vector();
          ArrayList splittedListOfTest = Tools.getListOfTestManualOrAutomatic(DataModel.getCurrentCampaign().getTestList(), null, pAllTestToExecute);
          // ADD
          //automatic_finished = false;
          Vector listExec = new Vector();
          //END ADD
          // On parcourt la liste des sous-listes
          for (int h =0; h < splittedListOfTest.size(); h++) {
          if (((ArrayList)splittedListOfTest.get(h)).get(0) instanceof ManualTest) {
          // Sous liste de tests manuels
          listExec.add(new ManualExecution((ArrayList)splittedListOfTest.get(h), exec, finalExecResult));
                                     
          } else {
          //Sous liste de tests automatiques
          ArrayList automaticTestList = (ArrayList)splittedListOfTest.get(h);
          listExec.add(new AutomaticExecution(automaticTestList, exec, finalExecResult));
          }
          }
          //ADD
          //precCallExecThread = new callExecThread(listExec, exec, splittedListOfTest, selectedRowIndex, attachToBeSuppress, true, finalExecResult, precCallExecThread, pAllTestToExecute.toArray());
          precCallExecThread = new CallExecThread(listExec, exec, splittedListOfTest, true, finalExecResult, precCallExecThread, pAllTestToExecute.toArray(), this, false);
                                        
          //precCallExecThread.start();
          // lancer le thread
          //
          } // FIN FOR 1
          precCallExecThread.start();
          } else {
          JOptionPane.showMessageDialog(ExecutionView.this,
          "Vous devez selectionner au moins une execution...",
          "Attention !",
          JOptionPane.WARNING_MESSAGE);
          }
          } else {
          JOptionPane.showMessageDialog(ExecutionView.this,
          "Il n'y a aucun test dans la campagne !",
          "Attention !",
          JOptionPane.WARNING_MESSAGE);
          }
          }*/
    }
        
        
    public void resultExecutionPerformed(ActionEvent e) {
        int detailsSelectedRowIndex = detailsExecutionTable.getSelectedRow();
        int execSelectedRowIndex = executionTable.getSelectedRow();
        if (execSelectedRowIndex != -1 && detailsSelectedRowIndex != -1) {
            new ExecutionResultView(DataModel.getObservedExecutionResult().getNameFromModel(), DataModel.getObservedExecutionResult());
        }
    }
        
        
public void modifyExecutionPerformed(ActionEvent e) {
    int selectedIndex = executionTable.getSelectedRow();
    if (selectedIndex != -1) {
        Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)executionTable.getModel().getValueAt(selectedIndex, 1));
        // Conservation des anciennes valeurs
                        
        /*HashMap oldAttachMap = new HashMap();
          Set keysSetOld = exec.getAttachmentMapFromModel().keySet();
          for (Iterator iter = keysSetOld.iterator(); iter.hasNext();) {
          Object elem = iter.next();
          oldAttachMap.put(elem, exec.getAttachmentMapFromModel().get(elem));
          }*/
                        
        HashMap oldAttachMap = exec.getCopyOfAttachmentMapFromModel();
                        
        Script oldInitScript = exec.getPreScriptFromModel();
        Script oldPostScript = exec.getPostScriptFromModel();
        String oldExecName = exec.getNameFromModel();
        if (exec.getExecutionResultListFromModel().size() != 0) {
            JOptionPane.showMessageDialog(ExecutionView.this,
                                          Language.getInstance().getText("Cette_execution_possede_des_resultats") +
                                          Language.getInstance().getText("Vous_ne_pourrez_modifier_que_ses_attachements"),
                                          Language.getInstance().getText("Avertissement_"),
                                          JOptionPane.WARNING_MESSAGE);
        }
        AskNewExecution askNewExecution = new AskNewExecution(exec);
        if (oldInitScript == null) {
            initScriptItem.setEnabled(false);
            setUpPreEngineItem.setEnabled(false);
            //commitInitScriptItem.setEnabled(false);
        } else {
            initScriptItem.setEnabled(true);
            setUpPreEngineItem.setEnabled(true);
        }
                        
        if (oldPostScript == null) {
            postScriptItem.setEnabled(false);
            setUpPostEngineItem.setEnabled(false);
            //commitPostScriptItem.setEnabled(false);
        }else {
            postScriptItem.setEnabled(true);
            setUpPostEngineItem.setEnabled(true);
        }
        if (askNewExecution.getExecution() != null) {
            Execution pExec = askNewExecution.getExecution();
            int transNumber = -1;
            try {
                                        
                // BdD
                transNumber = Api.beginTransaction(10, ApiConstants.UPDATE_EXECUTION);
                String newName = pExec.getNameFromModel();
                pExec.updateNameInModel(oldExecName);
                pExec.updateNameInDBAndModel(newName);
                // pExec.updateDatasetInDBAndModel(pExec.getDataSetFromModel());
                pExec.updateEnvInDBAndModel(pExec.getEnvironmentFromModel());
                if (pExec.getPreScriptFromModel() != null) {
                    if (!pExec.getPreScriptFromModel().equals(oldInitScript)) {
                        if (oldInitScript != null) {
                            //pExec.deleteScriptFromDB(oldInitScript.getNameFromModel(), PRE_SCRIPT);
                            pExec.deleteScriptInDB(PRE_SCRIPT);
                        }
                        //pExec.addPreScriptInBddAndModel(pExec.getPreScriptFromModel(),askNewExecution.getInitScriptFile());
                        pExec.addPreScriptInDBAndModel(pExec.getPreScriptFromModel(), askNewExecution.getInitScriptFile());
                        initScriptItem.setEnabled(true);
                        setUpPreEngineItem.setEnabled(true);
                    }
                } else if (oldInitScript != null) {
                    //pExec.deleteScriptFromDB(oldInitScript.getNameFromModel(), PRE_SCRIPT);
                    pExec.deleteScriptInDB(PRE_SCRIPT);
                    initScriptItem.setEnabled(false);
                    setUpPreEngineItem.setEnabled(false);
                }
                if (askNewExecution.getExecution().getPostScriptFromModel() != null) {
                    if (!pExec.getPostScriptFromModel().equals(oldPostScript)) {
                        if (oldPostScript != null) {
                            //pExec.deleteScriptFromDB(oldPostScript.getNameFromModel(), POST_SCRIPT);
                            pExec.deleteScriptInDB(POST_SCRIPT);
                        }
                        //pExec.addPostScriptInBddAndModel(pExec.getPostScriptFromModel(), askNewExecution.getRestitutionScriptFile());
                        pExec.addPreScriptInDBAndModel(pExec.getPostScriptFromModel(), askNewExecution.getRestitutionScriptFile());
                        postScriptItem.setEnabled(true);
                        setUpPostEngineItem.setEnabled(true);
                    }
                } else if (oldPostScript != null) {
                    //pExec.deleteScriptFromDB(oldPostScript.getNameFromModel(), POST_SCRIPT);
                    pExec.deleteScriptInDB(POST_SCRIPT);
                    postScriptItem.setEnabled(false);
                    setUpPostEngineItem.setEnabled(false);
                }
                                        
                /* Les attachement */
                pExec.updateAttachement(oldAttachMap);
                                        
                                        
                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                if (!initScriptItem.isEnabled() && !postScriptItem.isEnabled()) {
                    modifyScriptMenu.setEnabled(false);
                } else {
                    modifyScriptMenu.setEnabled(true);
                }
                executionTable.getModel().setValueAt(pExec.getNameFromModel(), selectedIndex, 1);
                executionTable.getModel().setValueAt(pExec.getEnvironmentFromModel(), selectedIndex, 2);
                executionTable.getModel().setValueAt(pExec.getExecutionStatusFromModel(), selectedIndex, 3);
                executionTable.getModel().setValueAt(pExec.getCreationDateFromModel(), selectedIndex, 4);
                executionTable.getModel().setValueAt(pExec.getAttachmentMapFromModel().keySet(), selectedIndex, 5);
                executionTable.getColumnModel().getColumn(0).setMaxWidth(18);
            } catch (Exception exception) {
                Util.err(exception);
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
                                
        } else {
            /* Annulation de la modif */
            exec.setAttachmentMapInModel(oldAttachMap);
        }
        commitPostScriptItem.setEnabled(false);
        commitInitScriptItem.setEnabled(false);
    } // if selecetd
}
        
        
public void deleteExecutionPerformed(ActionEvent e) {
    //int selectedRowIndex = executionTable.getSelectedRow();
    int[] selectedRows = executionTable.getSelectedRows();
    Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
    int choice = JOptionPane.showOptionDialog(ExecutionView.this,
                                              Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_toutes_les_executions_selectionnees_"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[1]);
    if (choice == JOptionPane.YES_OPTION) {
        //if (selectedRowIndex != -1) {
        for (int selectedRowIndex = selectedRows.length-1 ; selectedRowIndex>=0 ; selectedRowIndex--) {
            //Execution exec = DataModel.getObservedExecution();
            //Execution exec = DataModel.getCurrentCampaign().getExecution((String)sorter.getValueAt(selectedRowIndex, 1));
            Execution exec = DataModel.getCurrentCampaign().getExecutionFromModel((String)sorter.getValueAt(selectedRows[selectedRowIndex], 1));
            if (exec != null) {
                try {
                    // BdD
                    DataModel.getCurrentCampaign().deleteExecutionInDBAndModel(exec);
                    //exec.deleteInBddAndModel();
                                                
                    // IHM
                    DataModel.getExecutionTableModel().removeData(sorter.modelIndex(selectedRows[selectedRowIndex]));
                    executionTable.getColumnModel().getColumn(0).setMaxWidth(18);
                    DataModel.getExecutionResultTableModel().clearTable();

                    logger.info(LoggingData.getData() +  
                            "Kampanie: " +
                            DataModel.getCurrentCampaign().toString() + "\n" +
                            "Execution: " + 
                            exec.getNameFromModel().toString() + 
                            " Successfully deleted!");
                                                
                } catch (Exception exception) {
                    Util.err(exception);
                    Tools.ihmExceptionView(exception);
                }
                                        
            }
        }
    }
}
        
        
public void deleteExecutionResultPerformed(ActionEvent e) {
    //int selectedRowIndex = detailsExecutionTable.getSelectedRow();
    int[] selectedRows = detailsExecutionTable.getSelectedRows();
    Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
    int choice = JOptionPane.showOptionDialog(ExecutionView.this,
                                              Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_tous_les_resultats_d_execution_selectionnes_"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[1]);
    if (choice == JOptionPane.YES_OPTION) {
        //if (selectedRowIndex != -1) {
        for (int selectedRowIndex = selectedRows.length-1 ; selectedRowIndex>=0 ; selectedRowIndex--) {
            //ExecutionResult execResult = DataModel.getObservedExecutionResult();
            //ExecutionResult execResult = DataModel.getObservedExecution().getExecutionResult((String)detailsSorter.getValueAt(selectedRowIndex, 0));
            ExecutionResult execResult = DataModel.getObservedExecution().getExecutionResultFromModel((String)detailsSorter.getValueAt(selectedRows[selectedRowIndex], 0));
            if (execResult != null) {
                try {
                    // BdD
                    DataModel.getObservedExecution().deleteExecutionResultInDBAndModel(execResult);
                    //execResult.deleteFromDB();

                    logger.info(LoggingData.getData() +  
                            "Kampanie: " +
                            DataModel.getCurrentCampaign().toString() + "\n" +
                            "Execution-Result: " + 
                            execResult.getNameFromModel().toString() + 
                            " Successfully deleted!");
                    
                                                
                    // IHM
                    //DataModel.getExecutionResultTableModel().removeData(detailsSorter.modelIndex(selectedRows[selectedRowIndex]));
                    //DataModel.getObservedExecution().removeExecutionResult(execResult);
                                                
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
            }
        }
        // IHM
        DataModel.getExecutionResultTableModel().clearTable();
        Execution exec = DataModel.getObservedExecution();
        if (exec == null){
            return;
        }
        for(int i = 0; i < exec.getExecutionResultListFromModel().size(); i ++) {
            ArrayList resultList = new ArrayList();
            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getNameFromModel());
            Util.log("[ExecutionView] try to format : " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionDateFromModel().toString() + "  " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTimeFromModel().toString());
            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
            resultList.add(formater.format(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionDateFromModel()).toString() + "  " + ((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTimeFromModel().toString());
            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getTesterFromModel());
            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getExecutionStatusFromModel());
            resultList.add(((ExecutionResult)exec.getExecutionResultListFromModel().get(i)).getFormatedStatFromModel(
                    Language.getInstance().getText("Initial_SUCCES"),
                    Language.getInstance().getText("Initial_ECHEC"),
                    Language.getInstance().getText("Initial_INCON"),
                    Language.getInstance().getText("Initial_NOTAPPLI"),
                    Language.getInstance().getText("Initial_BLOCKED")
            ));
                                
            DataModel.getExecutionResultTableModel().addRow(resultList);
        }
    }
}
        
void reloadExecutionPerformed(ActionEvent e) {
    int detailsSelectedRowIndex = detailsExecutionTable.getSelectedRow();
    //boolean toBeContinued = true;
    if  (detailsSelectedRowIndex != -1) {
        // L'execution affichee a l'ecran
        // Execution courante
        Execution exec = DataModel.getObservedExecution();
        ExecutionResult finalExecResult = DataModel.getObservedExecutionResult();
                        
        try {
            if (exec!=null && finalExecResult!=null){
                Campaign pCamp = exec.getCampagneFromModel();
                if (finalExecResult.getNbOfTestWithStat() != pCamp.getTestListFromModel().size()){
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("resexec_non_valid"),
                                                               Language.getInstance().getText("Attention_"),
                                                               JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            if (!DataModel.getCurrentCampaign().isValideModel()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
                                
            if (!exec.isValideModel()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!finalExecResult.existeInBase()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
                                
        } catch(Exception e1){
            Tools.ihmExceptionView(e1);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return ;
        }
                        
        if (exec!=null && finalExecResult!=null){
            OptionExecDialog pOptionExecDialog = new OptionExecDialog(SalomeTMFContext.getInstance().getSalomeFrame(), true, false, true);
            if (pOptionExecDialog.isOkSelected()){
                /* Copie the final resul */
                try {
                    ExecutionResult copy_finalExecResult = finalExecResult.cloneInDB(pOptionExecDialog.ischeckWithOutSuccesSelected(), DataModel.getCurrentUser());
                    //Rajouter dans le tableau
                    updateIHMTable(true, exec, copy_finalExecResult);
                    detailsExecutionTable.getSelectedRow();
                    int ligne = detailsExecutionTable.getRowCount()-1;
                    //Util.debug("--------------------ligne - 1 = " + ligne);
                    if (ligne>-1){
                        detailsExecutionTable.setRowSelectionInterval(ligne, ligne);
                    }
                                                
                    TestMethods.continueExecution(exec, copy_finalExecResult, this, false, true, pOptionExecDialog.isAssignedTestSelected(), true);
                } catch (Exception ex){
                    Util.err(ex);
                }
            }
        } else {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }
                        
    }
}
public void continueExecutionPerformed(ActionEvent e, boolean forModif) {
    int detailsSelectedRowIndex = detailsExecutionTable.getSelectedRow();
    //boolean toBeContinued = true;
    if  (detailsSelectedRowIndex != -1) {
        // L'execution affichee a l'ecran
        // Execution courante
        Execution exec = DataModel.getObservedExecution();
        ExecutionResult finalExecResult = DataModel.getObservedExecutionResult();
                        
        try {
            if (exec!=null && finalExecResult!=null){
                //* Si l'execution n'est pas termine
                if (!finalExecResult.getExecutionStatusFromModel().equals(ApiConstants.INCOMPLETED)) {
                    Campaign pCamp = exec.getCampagneFromModel();
                    if (finalExecResult.getNbOfTestWithStat() != pCamp.getTestListFromModel().size()){
                        SalomeTMFContext.getInstance().showMessage(
                                                                   Language.getInstance().getText("resexec_non_valid"),
                                                                   Language.getInstance().getText("Attention_"),
                                                                   JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                }
            }
            if (!DataModel.getCurrentCampaign().isValideModel()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
                                
            if (!exec.isValideModel()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (!finalExecResult.existeInBase()){
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
                                
        } catch(Exception e1){
            Util.err(e1);
            Tools.ihmExceptionView(e1);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return ;
        }
                        
        if (exec!=null && finalExecResult!=null){
// stecan, 17.11.2017 commented: In Sirona is is not used for 11 years!           
//            OptionExecDialog pOptionExecDialog = new OptionExecDialog(SalomeTMFContext.getInstance().getSalomeFrame(), false,  !forModif, true);
//            if (pOptionExecDialog.isOkSelected()){
//                TestMethods.continueExecution(exec, finalExecResult, this, false, false, false, false);
//            }
            TestMethods.continueExecution(exec, finalExecResult, this, false, false, false, false);
        } else {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }
                        
    }
}
        
public void attachExecutionPerformed(ActionEvent e) {
    HashMap oldMap = DataModel.getObservedExecution().getCopyOfAttachmentMapFromModel();
                
    //AttachmentViewWindow attachViewWindow = new AttachmentViewWindow(SalomeTMF.ptrSalomeTMF, EXECUTION, DataModel.getObservedExecution(), null, null, null);
    AttachmentViewWindow attachViewWindow = new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(), EXECUTION, DataModel.getObservedExecution());
                
    if (!attachViewWindow.cancelPerformed){
        int transNumber = -1;
        try {
            // BdD
            transNumber = Api.beginTransaction(10, ApiConstants.UPDATE_ATTACHMENT);
            DataModel.getObservedExecution().updateAttachement(oldMap);
                                
            Api.commitTrans(transNumber);
            transNumber = -1;
            // IHM
            sorter.setValueAt(DataModel.getObservedExecution().getAttachmentMapFromModel().keySet(), executionTable.getSelectedRow(), 5);
                                
        } catch (Exception exception) {
            Util.err(exception);
            Api.forceRollBackTrans(transNumber);
            DataModel.getObservedExecution().setAttachmentMapInModel(oldMap);
            Tools.ihmExceptionView(exception);
        }
    }
}
        
public void attachExecutionResultPerformed(ActionEvent e) {
    HashMap oldMap = DataModel.getObservedExecutionResult().getCopyOfAttachmentMapFromModel();
                
    //AttachmentViewWindow attachViewWindow = new AttachmentViewWindow(SalomeTMF.ptrSalomeTMF, EXECUTION_RESULT, DataModel.getObservedExecutionResult(), null ,null, null);
    AttachmentViewWindow attachViewWindow = new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(), EXECUTION_RESULT, DataModel.getObservedExecutionResult());
                
    if (!attachViewWindow.cancelPerformed){
                        
        int transNumber = -1;
        try {
            // BdD
            Util.log("[ExecutionView:attachExecutionResultPerformed] : UPDATE_ATTACHMENT IN BDD");
            transNumber = Api.beginTransaction(10, ApiConstants.UPDATE_ATTACHMENT);
            DataModel.getObservedExecutionResult().updateAttachement(oldMap);
                                
                                
            Api.commitTrans(transNumber); 
        } catch (Exception exception) {
            Util.err(exception);
            Api.forceRollBackTrans(transNumber);
            DataModel.getObservedExecutionResult().setAttachmentMapInModel(oldMap);
            Tools.ihmExceptionView(exception);
        }
    } else{
                        
    }
}
        
        
        
        
public void modifyinitScriptPerformed(ActionEvent e) {
    modifyScript(PRE_SCRIPT);
}
        
public void modifypostScriptPerformed(ActionEvent e) {
    modifyScript(POST_SCRIPT);
}
        
        
        
public void commitInitScriptPerformed(ActionEvent e) {
    commitScript(PRE_SCRIPT);
}
        
        
        
public void commitPostScriptPerformed(ActionEvent e) {
    commitScript(POST_SCRIPT);
}
        
/*private int findIndexOfExcution(Execution pExec){
  Util.log("[ExecutionView:findIndexOfExcution] : try to find " + pExec.getNameFromModel() + " in executionTable");
  int ret = -1;
  boolean continu = true;
  int size = executionTable.getModel().getRowCount();
  int i = 0;
  while (continu && i < size ){
  String name = (String) executionTable.getModel().getValueAt(i,1);
  Util.log("[ExecutionView:findIndexOfExcution] : compare" + pExec.getNameFromModel() + " and " + name);
                        
  if (name.equals(pExec.getNameFromModel())){
  ret = i;
  continu = false;
  } else {
  i++;
  }
  }
  return ret;
                
  }*/
        
public void updateIHMTable(boolean newLaunch, Execution exec, ExecutionResult finalExecResult) {
    if (newLaunch) { 
        if (finalExecResult.getTimeFromModel() == null) {
            finalExecResult.setTimeInModel(new Time(Calendar.getInstance().getTimeInMillis()));
        }
        //int selectedRowIndex = findIndexOfExcution(exec);
        int[] selectedRows = executionTable.getSelectedRows();
        if (selectedRows.length == 1) {
            int selectedRowIndex = selectedRows[0];
            if ((selectedRowIndex != -1)&&(exec.getNameFromModel().equals(sorter.getValueAt(selectedRowIndex, 1)))) {
                ArrayList resultList = new ArrayList();
                // la table des r?sultats d'ex?cutions
                resultList.add(finalExecResult.getNameFromModel());
                SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
                resultList.add(formater.format(finalExecResult.getExecutionDateFromModel()).toString() + "  " + finalExecResult.getTimeFromModel().toString());
                resultList.add(finalExecResult.getTesterFromModel());
                resultList.add(finalExecResult.getExecutionStatusFromModel());
                resultList.add(finalExecResult.getFormatedStatFromModel(
                        Language.getInstance().getText("Initial_SUCCES"),
                        Language.getInstance().getText("Initial_ECHEC"),
                        Language.getInstance().getText("Initial_INCON"),
                        Language.getInstance().getText("Initial_NOTAPPLI"),
                        Language.getInstance().getText("Initial_BLOCKED")
                ));
                DataModel.getExecutionResultTableModel().addRow(resultList);
                // La table des ex?cutions
                sorter.setValueAt(finalExecResult.getExecutionDateFromModel().toString(),selectedRowIndex,6);
            }
        }
    } else {
        detailsSorter.setValueAt(finalExecResult.getTesterFromModel(), detailsExecutionTable.getSelectedRow(), 2);
        detailsSorter.setValueAt(finalExecResult.getExecutionStatusFromModel(), detailsExecutionTable.getSelectedRow(), 3);
        detailsSorter.setValueAt(finalExecResult.getFormatedStatFromModel(
                Language.getInstance().getText("Initial_SUCCES"),
                Language.getInstance().getText("Initial_ECHEC"),
                Language.getInstance().getText("Initial_INCON"),
                Language.getInstance().getText("Initial_NOTAPPLI"),
                Language.getInstance().getText("Initial_BLOCKED")),
                detailsExecutionTable.getSelectedRow(), 4);
    }
                
    int detailsTableIndex = detailsExecutionTable.getSelectedRow();
    //Util.debug("--------------------detailsTableIndex = " + detailsTableIndex);
    if (detailsTableIndex != -1 && finalExecResult.getNameFromModel().equals(detailsSorter.getValueAt(detailsTableIndex,0)) && finalExecResult.getExecutionStatusFromModel().equals(FINISHED)) 
    {
        if (Api.isLockExecutedTest()){
            continueExecution.setText(Language.getInstance().getText("Reprendre"));
            continueExecution.setEnabled(false);
            relancerExecution.setEnabled(false);
            forModif = false;
        } else {
            //change label
            continueExecution.setText(Language.getInstance().getText("Modifier"));
            continueExecution.setEnabled(true);
            relancerExecution.setEnabled(true);
            forModif = true;
        }
    }
}
        
        
} // Fin de la classe ExecutionView
