/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui definit la fenetre pour ordonner les suites de tests
 * @author teaml039
 * @version : 0.1
 */
public class ActionOrdering extends JDialog implements IHMConstants {
    
    /**
     * Ancien modele de donnees avant organisation
     */
    DefaultListModel listModel;
    
    JList list;
    /**
     * Modele de donnees pour le choix
     */
    private DefaultComboBoxModel comboModel;
    
    /**
     * Liste deroulante pour le choix des suites ou des tests.
     */
    JComboBox choiceComboBox;
    
    JButton upButton;
    
    JButton downButton;
    
    ArrayList workingList;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre
     * @param listModel le modele de liste
     */
    public ActionOrdering(ArrayList listToBeSort) {
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        int t_x = 1024 ;
        int t_y = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width;
            t_y = r.height;
        } catch(Exception E){
                        
        }
                
        comboModel = new DefaultComboBoxModel();
        choiceComboBox = new JComboBox(comboModel);
        upButton = new JButton();
        downButton = new JButton();
        upButton.setEnabled(false);
        Icon icon = Tools.createAppletImageIcon(PATH_TO_ARROW_UP_ICON,"");
        upButton.setIcon(icon);
        upButton.setToolTipText(Language.getInstance().getText("Monter_d_un_cran"));
        upButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedIndex = list.getSelectedIndex();
                    if (selectedIndex != -1) {
                        Object objToGoUp = listModel.getElementAt(selectedIndex);
                        Object objToGoDown = listModel.getElementAt(selectedIndex - 1);
                        try {
                            //BDD
                            ((Action)objToGoUp).updateOrderInDBAndModel(false);
                            
                            //IHM
                            listModel.setElementAt(objToGoUp, selectedIndex - 1);
                            listModel.setElementAt(objToGoDown, selectedIndex);
                            list.setSelectedIndex(selectedIndex - 1);
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                   
                    }
                }
            });
        
        
        icon = Tools.createAppletImageIcon(PATH_TO_ARROW_DOWN_ICON,"");
        if (listToBeSort.size() < 2) {
            downButton.setEnabled(false);
        }
        downButton.setIcon(icon);
        downButton.setToolTipText(Language.getInstance().getText("Descendre_d_un_cran"));
        downButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedIndex = list.getSelectedIndex();
                    if (selectedIndex != -1) {
                        Object objToGoUp = listModel.getElementAt(selectedIndex +1 );
                        Object objToGoDown = listModel.getElementAt(selectedIndex);
                        try {
                            //BDD
                            ((Action)objToGoDown).updateOrderInDBAndModel(true);
                            
                            //IHM
                            listModel.setElementAt(objToGoUp, selectedIndex);
                            listModel.setElementAt(objToGoDown, selectedIndex + 1);
                            list.setSelectedIndex(selectedIndex + 1);
                            
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    
                    }
                }
            });
        
        
        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(upButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
        buttonSet.add(downButton);
        
        initData(listToBeSort);
        
        int listSize = listToBeSort.size();
        list = new JList(listModel);
        list.setModel(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setVisibleRowCount(listSize);
        //list.setPreferredSize(new Dimension(250,150));
        list.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    int selectedIndex = list.getSelectedIndex();
                    if (selectedIndex != - 1) {
                        if (selectedIndex == 0) {
                            upButton.setEnabled(false);
                            downButton.setEnabled(true);
                        } else if (selectedIndex == listModel.getSize()-1) {
                            upButton.setEnabled(true);
                            downButton.setEnabled(false);
                        } else {
                            upButton.setEnabled(true);
                            downButton.setEnabled(true);
                        }
                    } else {
                        upButton.setEnabled(false);
                        downButton.setEnabled(false);
                    }
                }
            });
        
        JScrollPane usersListScrollPane = new JScrollPane(list);
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Actions")));
        usersListScrollPane.setMaximumSize(new Dimension(400,400));
        usersListScrollPane.setMinimumSize(new Dimension(250,400));
        
        int taille = 250;
        usersListScrollPane.setPreferredSize(new Dimension(400, taille));
       
        JButton validate = new JButton(Language.getInstance().getText("Fermer"));
        validate.setToolTipText(Language.getInstance().getText("Fermer"));
        validate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    workingList.clear();
                    DataModel.getActionTableModel().clearTable();
                    for (int i = 0; i < listModel.size(); i++) {
                        workingList.add(listModel.get(i));
                        DataModel.getActionTableModel().addValueAt(((Action)listModel.get(i)).getNameFromModel(),i , 0);
                        DataModel.getActionTableModel().addValueAt(((Action)listModel.get(i)).getDescriptionFromModel(),i , 1);
                        DataModel.getActionTableModel().addValueAt(((Action)listModel.get(i)).getAwaitedResultFromModel(),i , 2);
                        DataModel.getActionTableModel().addValueAt(((Action)listModel.get(i)).getAttachmentMapFromModel(),i , 3);
                    }
                    ActionOrdering.this.dispose();
                }
            });
        
        /*JButton cancel = new JButton("Annuler");
          cancel.setToolTipText("Annuler");
          cancel.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
          ActionOrdering.this.dispose();
          }
          });
        */
        JPanel secondButtonSet = new JPanel();
        secondButtonSet.add(validate);
        //secondButtonSet.add(cancel);
        
        JPanel center = new JPanel();
        center.add(usersListScrollPane);
        center.add(buttonSet);
        
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        
        page.add(center);
        page.add(secondButtonSet);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        
        /*int x = (t_x -400)/2;
          int y = (t_y - (taille))/2;
        
          if (x>0 && y > 0){
          this.setLocation(x , y);
          } else {
          this.setLocation(400,400);
          }*/
        
        this.setTitle(Language.getInstance().getText("Ordonner"));
        /*this.setLocationRelativeTo(this.getParent()); 
          this.pack();
          this.setVisible(true);*/
        centerScreen();
        
    } // Fin du constructeur Ordering/1
    
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
                 
        this.setVisible(true); 
        requestFocus();
    }
    
    public void initData(ArrayList list) {
        listModel = new DefaultListModel();
        for (int i = 0; i < list.size(); i++) {
            listModel.addElement(list.get(i));
        }
        workingList = list;
    }
} // Fin de la classe ActionOrdering
