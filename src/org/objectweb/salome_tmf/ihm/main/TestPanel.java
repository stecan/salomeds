package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.filtre.TestTreeFiltrePanel;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.htmleditor.EkitCore;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class TestPanel extends JPanel implements ActionListener {

    JPanel workSpaceTest;
    JPanel testTopPanel;

    JLabel projetName;
    JLabel userName;
    JLabel automaticTestNameLabel;
    JLabel automaticTestDateLabel;
    JLabel automaticTestConceptorLabel;
    JLabel automaticTestExecutedLabel;
    JLabel manualTestConceptorLabel;
    JLabel manualTestNameLabel;
    JLabel manualTestHyperlinkName;
    JLabelLink manualTestHyperlink;
    JLabel manualTestDateLabel;
    JLabel manualTestExecutedLabel;

    JTabbedPane listWorkSpace;
    JTabbedPane familyWorkSpace;
    JTabbedPane automaticTest;
    JTabbedPane manualTest;

    DynamicTree testDynamicTree;
    TestTreeFiltrePanel pTestTreeFiltrePanel;

    // changeListenerPanel testMultiUserChangeListenerPanel;
    ParameterView automaticTestParameterView;
    ParameterView testParameterView;

    JMenuItem addList;
    JMenuItem addTest;
    JMenuItem addSuite;
    JMenuItem addFamily;
    JMenuItem ordTest;
    JMenuItem delTestOrTestList;
    JMenuItem renameTestButton;
    JButton automaticButtonCampaignDetails;
    JButton manualButtonCampaignDetails;

    JMenu testToolsMenu;

    JMenu testActionMenu;

    // JTextPane familyDescription;
    EkitCore pfamilyDescription;
    EkitCore pTestListDescription;
    EkitCore pAutomaticDetailsDescription;
    EkitCore pManualDetailsDescription;
    // JTextPane testListDescription;
    // JTextPane automaticDetailsDescription;
    // JTextPane manualDetailsDescription;

    AttachmentView automaticTestAttachmentView;
    AttachmentView manualTestAttachmentView;
    AttachmentView testListAttachmentView;
    AttachmentView familyAttachmentView;

    BaseIHM pBaseIHM;

    SalomeTMFContext pSalomeTMFContext;
    int t_x = 1024;
    int t_y = 768;
    JTabbedPane tabs;
    
    public TestPanel(SalomeTMFContext m_SalomeTMFContext, BaseIHM m_BaseIHM,
                     JTabbedPane m_tabs, int x, int y) {
        super(new BorderLayout());
        pSalomeTMFContext = m_SalomeTMFContext;
        pBaseIHM = m_BaseIHM;
        tabs = m_tabs;
        t_x = x;
        t_y = y;
    }

    public void initCoponent() {
        familyWorkSpace = new JTabbedPane();
        listWorkSpace = new JTabbedPane();
        workSpaceTest = new JPanel();

        JLabel projectLabel = new JLabel(Language.getInstance().getText("Projet__"));
        projetName = new JLabel();
        userName = new JLabel();
        projetName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        userName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        JPanel namePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        projectLabel.setFont(new Font(null, Font.BOLD, 16));
        namePanel.setLayout(new GridBagLayout());
        JLabel userLabel = new JLabel(Language.getInstance().getText("Utilisateurs")
                                      + " : ");
        userLabel.setFont(new Font(null, Font.BOLD, 16));
        userLabel.setLayout(new GridBagLayout());
        namePanel.add(projectLabel, new GridBagConstraints(0, 0, 1, 1, 0.5,
                0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(10, 10, 10, 5), 0, 0));
        namePanel.add(projetName, new GridBagConstraints(1, 0, 1, 1, 0.5, 0.1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(10, 5, 10, 20), 0, 0));
        namePanel.add(userLabel, new GridBagConstraints(2, 0, 1, 1, 0.5, 0.1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(10, 20, 10, 5), 0, 0));
        namePanel.add(userName, new GridBagConstraints(3, 0, 1, 1, 0.5, 0.1,
                GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(10, 5, 10, 5), 0, 0));

        /*
         * testMultiUserChangeListenerPanel = new changeListenerPanel();
         * testMultiUserChangeListenerPanel.setPreferredSize(new
         * Dimension(70,25)); JPanel testMultiUserPanel = new JPanel(new
         * FlowLayout(FlowLayout.RIGHT));
         * testMultiUserPanel.add(testMultiUserChangeListenerPanel);
         */
        testTopPanel = new JPanel(new BorderLayout());
        testTopPanel.add(namePanel, BorderLayout.WEST);
        // testTopPanel.add(testMultiUserPanel, BorderLayout.EAST);

        testToolsMenu = new JMenu(Language.getInstance().getText("Outils"));
        testActionMenu = new JMenu(Language.getInstance().getText("Actions"));
        pSalomeTMFContext.UIComponentsMap.put(UICompCst.TEST_TOOLS_MENU,
                                              testToolsMenu);

        add(testTopPanel, BorderLayout.NORTH);
        createTestsPlane();
    }

    /**************************************************************************/
    public JMenuItem renameTest;
    public JMenuItem supprTest;
    JMenuItem copyTest;

    JMenuItem pasteTest;

    List<SimpleData> datas = new ArrayList<SimpleData>();

    /**
     * Methode qui cree la vue sur les tests
     */
    private void createTestsPlane() {

        // addList = new JButton(Language.getInstance().getText(
        // "Ajouter_une_suite"));
        // addList.setToolTipText(Language.getInstance().getText(
        // "Ajouter_une_suite_de_tests"));
        //
        // addList.addActionListener(this);

        // addFamily = new JButton(Language.getInstance().getText(
        // "Ajouter_une_famille"));
        // addFamily.setToolTipText(Language.getInstance().getText(
        // "Ajouter_une_famille"));
        //
        // addFamily.addActionListener(this);

        // addTest = new
        // JButton(Language.getInstance().getText("Ajouter_un_test"));
        // addTest.setToolTipText(Language.getInstance()
        // .getText("Ajouter_un_test"));
        //
        // addTest.addActionListener(this);

        // JPanel listPanel = new JPanel(new GridLayout(1, 3));
        // listPanel.add(addFamily);
        // listPanel.add(addList);
        // listPanel.add(addTest);
        // listPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        // orderTest = new JButton(Language.getInstance().getText("Ordonner"));
        // orderTest.setToolTipText(Language.getInstance().getText(
        // "Ordonner_l_arbre_des_suites_de_tests"));
        // orderTest.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent e) {
        // new TestOrdering(testDynamicTree, false);
        // }
        // });

        // delTestOrTestList = new JButton(Language.getInstance().getText(
        // "Supprimer"));
        // delTestOrTestList.setToolTipText(Language.getInstance().getText(
        // "Supprimer"));
        // delTestOrTestList.setEnabled(false);
        // delTestOrTestList.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent e) {
        // try {
        // DataModel.deleteInTestTree();
        // } catch (Exception ex) {
        // Util.err(ex);
        // Tools.ihmExceptionView(ex);
        // }
        // }
        // });

        JMenuBar refreshMenuBar = new JMenuBar();

        /**
         * Add Action Menu action
         */
        addFamily = new JMenuItem(Language.getInstance().getText("Ajouter_une_famille"));
        addFamily.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    addFamilyPerformed(e);
                }
            });
        testActionMenu.add(addFamily);

        addSuite = new JMenuItem(Language.getInstance().getText("Ajouter_une_suite"));
        addSuite.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    addListPerformed(e);
                }
            });
        testActionMenu.add(addSuite);

        addTest = new JMenuItem(Language.getInstance().getText("Ajouter_un_test"));
        addTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addTestPerformed(e);
                }
            });
        testActionMenu.add(addTest);

        ordTest = new JMenuItem(Language.getInstance().getText("Ordonner_l_arbre_des_suites_de_tests"));
        ordTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new TestOrdering(testDynamicTree, false);
                }
            });
        testActionMenu.add(ordTest);

        supprTest = new JMenuItem(Language.getInstance().getText("Supprimer"));
        supprTest.setEnabled(false);
        supprTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        DataModel.deleteInTestTree();
                    } catch (Exception ex) {
                        Util.err(ex);
                        Tools.ihmExceptionView(ex);
                    }
                }
            });
        testActionMenu.add(supprTest);

        renameTest = new JMenuItem(Language.getInstance().getText("Renommer"));
        renameTest.setEnabled(false);
        renameTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.renameTest();
                }
            });
        testActionMenu.add(renameTest);

        copyTest = new JMenuItem(Language.getInstance().getText("Copier"));
        // copyTest.setEnabled(false);
        copyTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    copyPerformed(e);
                }
            });
        testActionMenu.add(copyTest);

        pasteTest = new JMenuItem(Language.getInstance().getText("Coller"));
        pasteTest.setEnabled(false);
        pasteTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for (SimpleData data : datas) {
                        DynamicTree pDynamicTree = SalomeTMFPanels
                            .getTestDynamicTree();
                        SimpleData from = null;
                        Object fromTree = pDynamicTree.getSelectedNode()
                            .getUserObject();
                        if (fromTree instanceof SimpleData) {
                            from = (SimpleData) fromTree;
                        }
                        // pasteTest.setEnabled(false);
                        int transcode = -1;
                        try {
                            pDynamicTree.setCursorWait();
                            transcode = Api.beginTransaction(101,
                                    ApiConstants.INSERT_TEST);
                            DataModel.makeCopie(data, from);
                            Api.commitTrans(transcode);
                            pDynamicTree.setCursorNormal();
                        } catch (Exception ex) {
                            Api.forceRollBackTrans(transcode);
                            pDynamicTree.setCursorNormal();
                        }
                    }
                }
            });
        testActionMenu.add(pasteTest);

        refreshMenuBar.add(testActionMenu);
        refreshMenuBar.add(testToolsMenu);

        // JMenuItem refreshItem = new
        // JMenuItem(Language.getInstance().getText("Rafraichir"));
        JMenu refreshItem = new JMenu(Language.getInstance().getText("Rafraichir"));

        /*
         * refreshItem.addActionListener(new ActionListener() { public void
         * actionPerformed(ActionEvent e) { //TestMethods.refreshTestTree();
         * tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
         * workSpaceTest.removeAll(); DataModel.reloadFromBase(true);
         * tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); } });
         */

        JMenuItem refreshItemAll = new JMenuItem(Language.getInstance()
                                                 .getText("Tout"));
        refreshItemAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    workSpaceTest.removeAll();
                    DataModel.reloadFromBase(true);
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        JMenuItem refreshItemTest = new JMenuItem(Language.getInstance().getText("Plan_de_tests"));
        refreshItemTest.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    workSpaceTest.removeAll();
                    try {
                        DataModel.reloadTestPlan();
                    } catch (Exception ex) {
                        Util.err(ex);
                        DataModel.reloadFromBase(true);
                    }
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        refreshItem.add(refreshItemAll);
        refreshItem.add(refreshItemTest);

        testToolsMenu.add(refreshItem);
        if (Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            JMenuItem changePwdItem = new JMenuItem(Language.getInstance().getText("Changer_le_mot_de_passe"));
            changePwdItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new ChangePwdWindow(pSalomeTMFContext.ptrFrame).show();
                    }
                });

            testToolsMenu.add(changePwdItem);
        }

        // renameTestButton = new JButton(Language.getInstance().getText(
        // "Renommer"));
        // renameTestButton.setEnabled(false);
        // renameTestButton.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent e) {
        // DataModel.renameTest();
        // }
        // });

        JPanel testPanel = new JPanel(new GridLayout(1, 3));
        // testPanel.add(orderTest);
        // testPanel.add(renameTestButton);
        // testPanel.add(delTestOrTestList);
        testPanel.add(refreshMenuBar);
        testPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        testDynamicTree = new DynamicTree(Language.getInstance().getText("Plan_de_tests"), DataConstants.TESTLIST);

        createFamily();
        createTestList();
        createAutomaticTest();
        createManualTest();
        workSpaceTest.removeAll();

        // Construction finale
        JPanel allButtons = new JPanel();
        allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
        // allButtons.add(listPanel);
        allButtons.add(testPanel);

        // Mapping entre composants graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.TEST_DYNAMIC_TREE,
                                               testDynamicTree);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.TEST_DYNAMIC_TREE);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridLayout(2, 1));
        pTestTreeFiltrePanel = new TestTreeFiltrePanel(testDynamicTree
                                                       .getModel());
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton quitTestButton = new JButton(Language.getInstance().getText("Quitter"));
        quitTestButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pBaseIHM.quit(true, true);
                }
            });
        buttonPanel.add(quitTestButton);
        southPanel.add(pTestTreeFiltrePanel);
        southPanel.add(buttonPanel);

        JPanel buttonsAndTree = new JPanel(new BorderLayout());
        buttonsAndTree.add(allButtons, BorderLayout.NORTH);
        buttonsAndTree.add(testDynamicTree, BorderLayout.CENTER);
        // buttonsAndTree.add(quitTestButton, BorderLayout.SOUTH);
        buttonsAndTree.add(southPanel, BorderLayout.SOUTH);
        buttonsAndTree.setPreferredSize(new Dimension(t_x / 3, t_y));
        buttonsAndTree.setMinimumSize(new Dimension(t_x / 4, t_y));

        workSpaceTest.setPreferredSize(new Dimension(t_x / 3 * 2, t_y));
        workSpaceTest.setMinimumSize(new Dimension(t_x / 3, t_y));

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                                              buttonsAndTree, workSpaceTest);
        if (Api.isIDE_DEV()) {
            splitPane.setDividerLocation(3);
        } else {
            splitPane.setDividerLocation(380);
        }
        add(splitPane);

        // Mapping entre comosants graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.TEST_SECOND_BUTTONS_PANEL, testPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.TEST_SECOND_BUTTONS_PANEL);

    } // Fin de la m?thode createTestsPlane/0

    /**
     * Methode qui cree la vue sur les familles
     */
    private void createFamily() {

        workSpaceTest.removeAll();
        // pfamilyDescription = new PanelDescription();
        pfamilyDescription = new EkitCore(testDynamicTree.getTree(), true);
        /*
         * familyDescription = new JTextPane();
         * familyDescription.addCaretListener(new
         * TestTreeDescriptionListener());
         * familyDescription.addFocusListener(new
         * TestDescriptionFocusListener());
         *
         * JScrollPane familyDescrScrollPane = new
         * JScrollPane(familyDescription);
         */

        // Onglets
        familyAttachmentView = new AttachmentView(pBaseIHM,
                DataConstants.FAMILY, DataConstants.FAMILY, null,
                DataConstants.NORMAL_SIZE_FOR_ATTACH, null);
        // familyWorkSpace.addTab(Language.getInstance().getText("Description"),
        // familyDescrScrollPane);
        familyWorkSpace.addTab(Language.getInstance().getText("Description"),
                               pfamilyDescription);

        familyWorkSpace.addTab(Language.getInstance().getText("Attachements"),
                               familyAttachmentView);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.FAMILY_WORKSPACE_PANEL_FOR_TABS, familyWorkSpace);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.FAMILY_WORKSPACE_PANEL_FOR_TABS);

        workSpaceTest.setLayout(new BorderLayout());
        workSpaceTest.add(familyWorkSpace, BorderLayout.CENTER);
    } // Fin de la m?thode createAttachments/0

    /**
     * Methode qui cree la vue sur la description et les attachements (utilisee
     * pour le suites de test)
     *
     */
    private void createTestList() {

        workSpaceTest.removeAll();

        // Onglets
        /*
         * testListDescription = new JTextPane();
         * testListDescription.addCaretListener(new
         * TestTreeDescriptionListener());
         * testListDescription.addFocusListener(new
         * TestDescriptionFocusListener());
         */
        pTestListDescription = new EkitCore(testDynamicTree.getTree(), true);
        testListAttachmentView = new AttachmentView(pBaseIHM,
                DataConstants.TESTLIST, DataConstants.TESTLIST, null,
                DataConstants.NORMAL_SIZE_FOR_ATTACH, null);

        // JScrollPane testListDescrSrollPane = new
        // JScrollPane(testListDescription);
        JScrollPane testListDescrSrollPane = new JScrollPane(pTestListDescription);
        listWorkSpace.addTab(Language.getInstance().getText("Description"),
                             testListDescrSrollPane);
        listWorkSpace.addTab(Language.getInstance().getText("Attachements"),
                             testListAttachmentView);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.TESTLIST_WORKSPACE_PANEL_FOR_TABS, listWorkSpace);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.TESTLIST_WORKSPACE_PANEL_FOR_TABS);

        workSpaceTest.setLayout(new BorderLayout());
        workSpaceTest.add(listWorkSpace, BorderLayout.CENTER);

    } // Fin de la m?thode createTestList/0

    /**
     * Methode qui cree la vue pour les tests automatiques
     */
    private void createAutomaticTest() {
        workSpaceTest.removeAll();

        JPanel detailsPanel = new JPanel(new BorderLayout());
        JPanel scriptPanel = new AutomaticTestScriptView();
        JPanel parametersPanel = new JPanel();

        automaticTestAttachmentView = new AttachmentView(pBaseIHM,
                DataConstants.AUTOMATIC_TEST, DataConstants.TEST, null,
                DataConstants.NORMAL_SIZE_FOR_ATTACH, null);
        automaticTestParameterView = new ParameterView(true,
                                                       DataConstants.AUTOMATIC_TEST);
        automaticTestParameterView.setName(Language.getInstance().getText("Vue_automatique"));

        automaticTest = new JTabbedPane();
        automaticTest.addTab(Language.getInstance().getText("Details"),
                             detailsPanel);
        automaticTest.addTab(Language.getInstance().getText("Script"),
                             scriptPanel);
        // automaticTest.addTab(Language.getInstance().getText("Attachements"),
        // attachmentPanel);
        automaticTest.addTab(Language.getInstance().getText("Attachements"),
                             automaticTestAttachmentView);
        automaticTest.addTab(Language.getInstance().getText("Parametres"),
                             automaticTestParameterView);

        createAutomaticTestDetails(detailsPanel);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS,
                                               automaticTest);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS);

        workSpaceTest.setLayout(new BorderLayout());
        workSpaceTest.add(automaticTest, BorderLayout.CENTER);

        // Mapping entre composants graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.AUTOMATED_TEST_SCRIPT_PANEL, scriptPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_SCRIPT_PANEL);

    } // Fin de la methode createAutomaticTest/0

    /**
     * M?thode qui cr?e l'?cran des d?tails sur un test
     *
     * @param panel
     *            le panel qui contient la vue
     */
    private void createAutomaticTestDetails(JPanel panel) {

        automaticTestNameLabel = new JLabel(Language.getInstance().getText("Nom_du_test")
                + " : ");
        automaticTestDateLabel = new JLabel(Language.getInstance().getText("Date_de_creation")
                + " : ");
        automaticTestConceptorLabel = new JLabel(Language.getInstance().getText("Concepteur")
                + " : ");
        automaticTestExecutedLabel = new JLabel(Language.getInstance().getText("Execute")
                + " : ");
        automaticButtonCampaignDetails = new JButton(Language.getInstance().getText("Campagne"));
        automaticButtonCampaignDetails.addActionListener(this);
        JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLine.add(automaticTestNameLabel);
        firstLine.add(Box.createRigidArea(new Dimension(20, 20)));
        firstLine.add(automaticTestDateLabel);
        firstLine.add(Box.createRigidArea(new Dimension(20, 20)));
        firstLine.add(automaticTestExecutedLabel);
        firstLine.add(Box.createRigidArea(new Dimension(10, 20)));
        firstLine.add(automaticButtonCampaignDetails);

        JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLine.add(automaticTestConceptorLabel);

        JPanel allButtons = new JPanel();
        allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
        allButtons.add(firstLine);
        allButtons.add(secondLine);

        // allButtons.add()
        allButtons.setBorder(BorderFactory.createEmptyBorder(10, 0, 30, 0));
        /*
         * automaticDetailsDescription = new JTextPane();
         * automaticDetailsDescription
         * .setBorder(BorderFactory.createTitledBorder
         * (BorderFactory.createLineBorder
         * (Color.BLACK),Language.getInstance().getText("Description")));
         * automaticDetailsDescription.addCaretListener(new
         * TestTreeDescriptionListener());
         * automaticDetailsDescription.addFocusListener(new
         * TestDescriptionFocusListener());
         */
        pAutomaticDetailsDescription = new EkitCore(testDynamicTree.getTree(),
                                                    true);
        panel.add(allButtons, BorderLayout.NORTH);
        // JScrollPane automaticDetailsDescrSrollPane = new
        // JScrollPane(automaticDetailsDescription);
        JScrollPane automaticDetailsDescrSrollPane = new JScrollPane(pAutomaticDetailsDescription);

        panel.add(automaticDetailsDescrSrollPane, BorderLayout.CENTER);
    } // Fin de la m?thode createAutomaticTestDetails/1

    /**
     * Methode qui cree la vue pour les tests manuels
     */
    private void createManualTest() {
        workSpaceTest.removeAll();
        // Onglets

        JPanel detailsPanel = new JPanel(new BorderLayout());
        // JPanel actionsPanel = new JPanel();
        // JPanel attachmentPanel = new JPanel();
        // JPanel manualParameter = new JPanel();

        testParameterView = new ParameterView(true, DataConstants.PARAMETER);
        testParameterView.setName(Language.getInstance().getText(
                                                                 "Vue_des_donnees"));

        manualTestAttachmentView = new AttachmentView(pBaseIHM,
                DataConstants.MANUAL_TEST, DataConstants.TEST, null,
                DataConstants.NORMAL_SIZE_FOR_ATTACH, null);

        manualTest = new JTabbedPane();
        manualTest.addTab(Language.getInstance().getText("Details"),
                          detailsPanel);
        manualTest.addTab(Language.getInstance().getText("Actions"),
                          new ManualActionView());
        manualTest.addTab(Language.getInstance().getText("Attachements"),
                          manualTestAttachmentView);
        /*
         * manualTest.addChangeListener(new ChangeListener() { public void
         * stateChanged(ChangeEvent e) { if
         * (((JTabbedPane)e.getSource()).getSelectedIndex() == 1){
         * ManualActionView pManualActionView = (ManualActionView)
         * ((JTabbedPane)e.getSource()).getSelectedComponent();
         * pManualActionView.setColumnSize(); } } });
         */
        manualTest.addTab(Language.getInstance().getText("Parametres"),
                          testParameterView);

        createManualTestDetails(detailsPanel);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS, manualTest);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS);

        workSpaceTest.setLayout(new BorderLayout());
        workSpaceTest.add(manualTest, BorderLayout.CENTER);

    } // Fin de la m?thode createManualTest/0

    /**
     * M?thode qui cr?e l'?cran des d?tails sur un test
     *
     * @param panel
     *            le panel qui contient la vue
     */
    private void createManualTestDetails(JPanel panel) {

        JPanel allButtons = new JPanel();
        
        if (Config.getPolarionLinks()) {
            // Polarion links are shown
            manualTestConceptorLabel = new JLabel(Language.getInstance().getText("Concepteur")
                    + " : ");
            manualTestNameLabel = new JLabel(Language.getInstance().getText("Nom_du_test")
                    + " : ");
            manualTestHyperlinkName = new JLabel(Language.getInstance().getText("Link")
                    + " : ");
            manualTestHyperlink = new JLabelLink("Link");

            manualTestDateLabel = new JLabel(Language.getInstance().getText("Date_de_creation")
                    + " : ");
            manualTestExecutedLabel = new JLabel(Language.getInstance().getText("Execute")
                    + " : ");

            manualButtonCampaignDetails = new JButton(Language.getInstance()
                                                      .getText("Campagne"));
            manualButtonCampaignDetails.addActionListener(this);

            JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            firstLine.add(manualTestNameLabel);

            JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            secondLine.add(manualTestHyperlinkName);
            secondLine.add(manualTestHyperlink);

            JPanel thirdLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            thirdLine.add(manualTestDateLabel);
            thirdLine.add(manualTestExecutedLabel);
            thirdLine.add(manualButtonCampaignDetails);

            JPanel fourthLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            fourthLine.add(manualTestConceptorLabel);

            allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
            allButtons.add(firstLine);
            allButtons.add(secondLine);
            allButtons.add(thirdLine);
            allButtons.add(fourthLine);

            // allButtons.add()
            allButtons.setBorder(BorderFactory.createEmptyBorder(20, 0, 30, 0));
        } else {
            // Polarion links are not shown
            manualTestConceptorLabel = new JLabel(Language.getInstance().getText("Concepteur")
                    + " : ");
            manualTestNameLabel = new JLabel(Language.getInstance().getText("Nom_du_test")
                    + " : ");

            manualTestDateLabel = new JLabel(Language.getInstance().getText("Date_de_creation")
                    + " : ");
            manualTestExecutedLabel = new JLabel(Language.getInstance().getText("Execute")
                    + " : ");

            manualButtonCampaignDetails = new JButton(Language.getInstance()
                                                      .getText("Campagne"));
            manualButtonCampaignDetails.addActionListener(this);

            JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            firstLine.add(manualTestNameLabel);

            // JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            // secondLine.add(manualTestHyperlinkName);
            // secondLine.add(manualTestHyperlink);

            JPanel thirdLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            thirdLine.add(manualTestDateLabel);
            thirdLine.add(manualTestExecutedLabel);
            thirdLine.add(manualButtonCampaignDetails);

            JPanel fourthLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            fourthLine.add(manualTestConceptorLabel);

            allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
            allButtons.add(firstLine);
            // allButtons.add(secondLine);
            allButtons.add(thirdLine);
            allButtons.add(fourthLine);

            // allButtons.add()
            allButtons.setBorder(BorderFactory.createEmptyBorder(20, 0, 30, 0));
            
        }
        
        /*
         * manualDetailsDescription = new JTextPane();
         * manualDetailsDescription.setBorder
         * (BorderFactory.createTitledBorder(BorderFactory
         * .createLineBorder(Color
         * .BLACK),Language.getInstance().getText("Description")));
         * manualDetailsDescription.addCaretListener(new
         * TestTreeDescriptionListener());
         * manualDetailsDescription.addFocusListener(new
         * TestDescriptionFocusListener());
         */
        pManualDetailsDescription = new EkitCore(testDynamicTree.getTree(),
                                                 true);
        panel.add(allButtons, BorderLayout.NORTH);
        // JScrollPane manualDetailsDescrSrollPane = new
        // JScrollPane(manualDetailsDescription);
        JScrollPane manualDetailsDescrSrollPane = new JScrollPane(pManualDetailsDescription);

        panel.add(manualDetailsDescrSrollPane, BorderLayout.CENTER);
    } // Fin de la m?thode createManualTestDetails/1

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(automaticButtonCampaignDetails)) {
            Test pTest = DataModel.getCurrentTest();
            if (pTest != null) {
                new ViewCampaignOfTest(pTest);
            }
        } else if (e.getSource().equals(manualButtonCampaignDetails)) {
            Test pTest = DataModel.getCurrentTest();
            if (pTest != null) {
                new ViewCampaignOfTest(pTest);
            }
        }
        if (e.getSource().equals(addFamily)) {
            addFamilyPerformed(e);
        } else if (e.getSource().equals(addList)) {
            addListPerformed(e);
        } else if (e.getSource().equals(addTest)) {
            addTestPerformed(e);
        }
    }

    void addFamilyPerformed(ActionEvent e) {
        if (pTestTreeFiltrePanel.getFiltreType() != -1) {
            pTestTreeFiltrePanel.reInit(true);
        }
        DataModel.addNewFamily();
    }

    void addListPerformed(ActionEvent e) {
        if (pTestTreeFiltrePanel.getFiltreType() == DataConstants.TESTLIST
            || pTestTreeFiltrePanel.getFiltreType() == DataConstants.TEST) {
            pTestTreeFiltrePanel.reInit(true);
        }
        DataModel.addNewTestList();
    }

    void copyPerformed(ActionEvent e) {
        DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();

        /**
         * Je recupere toutes les elements selectionnes
         */
        List<SimpleData> simpleDatas = pDynamicTree.getSelectedNodes();

        /**
         * je verifie qu'ils sont du meme type
         */
        int cptTest = 0;
        int cptTestList = 0;
        int cptFamilly = 0;
        for (SimpleData data : simpleDatas) {
            if (data instanceof ManualTest || data instanceof AutomaticTest)
                cptTest++;

            if (data instanceof TestList)
                cptTestList++;

            if (data instanceof Family)
                cptFamilly++;
        }
        /*
         * Si au moins un test est selectionne alors il faut qu'aucun liste de
         * test ou famille ne soit selectionne
         */
        if (cptTest != 0 && (cptTestList != 0 || cptFamilly != 0)) {
            JOptionPane.showMessageDialog(pDynamicTree, Language.getInstance()
                                          .getText("Copier"), Language.getInstance().
                                                  getText("CantSelect"), JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (cptTestList != 0 && (cptTest != 0 || cptFamilly != 0)) {
            JOptionPane.showMessageDialog(pDynamicTree, Language.getInstance()
                                          .getText("Copier"), Language.getInstance().
                                                  getText("CantSelect"), JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (cptFamilly != 0 && (cptTest != 0 || cptTestList != 0)) {
            JOptionPane.showMessageDialog(pDynamicTree, Language.getInstance()
                                          .getText("Copier"), Language.getInstance().
                                                  getText("CantSelect"), JOptionPane.ERROR_MESSAGE);
            return;
        }

        /**
         * Si tout est ok je valide la copie
         */
        pasteTest.setEnabled(true);
        datas = simpleDatas;

    }

    void addTestPerformed(ActionEvent e) {
        if (pTestTreeFiltrePanel.getFiltreType() == DataConstants.TEST) {
            pTestTreeFiltrePanel.reInit(true);
        }
        DataModel.addNewTest();
    }

    void loadModel(String strProject, String strLogin) {
        // testMultiUserChangeListenerPanel.reset();
        testListAttachmentView.giveAccessToIhmScriptView();
        familyAttachmentView.giveAccessToIhmScriptView();
        testParameterView.giveAccessToIhmParameterView();
        manualTestAttachmentView.giveAccessToIhmScriptView();
        automaticTestParameterView.giveAccessToIhmParameterView();
        automaticTestAttachmentView.giveAccessToIhmScriptView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());
        testDynamicTree.givePopupMenuToTree();
    }

    void reloadModel() {
        // testMultiUserChangeListenerPanel.reset();
        testListAttachmentView.giveAccessToIhmScriptView();
        familyAttachmentView.giveAccessToIhmScriptView();
        testParameterView.giveAccessToIhmParameterView();
        manualTestAttachmentView.giveAccessToIhmScriptView();
        automaticTestParameterView.giveAccessToIhmParameterView();
        automaticTestAttachmentView.giveAccessToIhmScriptView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());
        testDynamicTree.givePopupMenuToTree();
    }

    void giveAccessToIhm() {
        if (Api.isConnected()) {
            if (!Permission.canDeleteTest()) {
                // if (delTestOrTestList == null)
                supprTest.setEnabled(false);
                // delTestOrTestList.setEnabled(false);
            }
            if (!Permission.canCreateTest()) {
                addTest.setEnabled(false);
                addSuite.setEnabled(false);
                addFamily.setEnabled(false);
                copyTest.setEnabled(false);
            }
            if (!Permission.canUpdateTest()) {
                renameTest.setEnabled(false);
                ordTest.setEnabled(false);
                // familyDescription.setEditable(false);
                pfamilyDescription.setEditable(false);
                pTestListDescription.setEditable(false);
                pManualDetailsDescription.setEditable(false);
                pAutomaticDetailsDescription.setEditable(false);
                testDynamicTree.setEnabled(false);
                pTestListDescription.setEditable(false);
                pManualDetailsDescription.setEditable(false);
                pAutomaticDetailsDescription.setEditable(false);
                // familyDescription.setEditable(false);
                pfamilyDescription.setEditable(false);
            }

        }
    }

    /**************************************************************************/
    void reValidate() {
        validate();
        repaint();
    }

    void removeTestWorkSpace() {
        workSpaceTest.removeAll();
    }

    void setWorkSpace(int type) {
        workSpaceTest.removeAll();
        if (type == DataConstants.FAMILY) {
            workSpaceTest.add(familyWorkSpace);
        } else if (type == DataConstants.TESTLIST) {
            workSpaceTest.add(listWorkSpace);
        } else if (type == DataConstants.MANUAL_TEST) {
            workSpaceTest.add(manualTest, BorderLayout.CENTER);
        } else if (type == DataConstants.AUTOMATIC_TEST) {
            workSpaceTest.add(automaticTest, BorderLayout.CENTER);
        }
    }

    void setDescription(int type, String text) {
        if (type == DataConstants.FAMILY) {
            // familyDescription.setText(text);
            pfamilyDescription.setText(text);
        } else if (type == DataConstants.TESTLIST) {
            pTestListDescription.setText(text);
        } else if (type == DataConstants.MANUAL_TEST) {
            pManualDetailsDescription.setText(text);
        } else if (type == DataConstants.AUTOMATIC_TEST) {
            pAutomaticDetailsDescription.setText(text);
        }
    }

    void setTestInfo(int type, String name, String conceptor, String date) {
        if (type == DataConstants.MANUAL_TEST) {
            if (name != null) {
                manualTestNameLabel.setText(Language.getInstance().getText("Nom_du_test__")
                        + name);
                if (Config.getPolarionLinks()) {
                    manualTestHyperlink.generatePolarionLink(name);
                }
            }
            if (conceptor != null) {
                manualTestConceptorLabel.setText(Language.getInstance().getText("Concepteur__")
                        + conceptor);
            }
            if (date != null) {
                manualTestDateLabel.setText(Language.getInstance().getText("Date_de_creation__")
                        + date);
            }
        } else if (type == DataConstants.AUTOMATIC_TEST) {
            if (name != null) {
                automaticTestNameLabel.setText(Language.getInstance().getText("Nom_du_test__")
                        + name);
            }
            if (conceptor != null) {
                automaticTestConceptorLabel.setText(Language.getInstance()
                                                    .getText("Concepteur__")
                                                    + conceptor);
            }
            if (date != null) {
                automaticTestDateLabel.setText(Language.getInstance().getText("Concepteur__")
                        + date);
            }
        }

    }

    void TestExecutedInfo(int type, String execcuted) {
        if (type == DataConstants.MANUAL_TEST) {
            if (execcuted != null) {
                manualTestExecutedLabel.setText(Language.getInstance().getText("Execute")
                        + " : " + execcuted);
            }

        } else if (type == DataConstants.AUTOMATIC_TEST) {
            if (execcuted != null) {
                automaticTestExecutedLabel.setText(Language.getInstance().getText("Execute")
                        + " : " + execcuted);
            }
        }
    }
}
