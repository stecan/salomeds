/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * @author teaml039
 */
public class BadDirectoryView extends JDialog implements IHMConstants {
    
    JCheckBox box;
    
    public BadDirectoryView() {
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        
        JLabel firstLine = new JLabel(Language.getInstance().getText("Une_copie_temporaire_du_fichier_a_ete_copiee_dans_le_repertoire_"));
        firstLine.setIcon(Tools.createAppletImageIcon(PATH_TO_CAUTION_ICON,""));
        Properties sys = System.getProperties();
        JLabel dirName = new JLabel(sys.getProperty("java.io.tmpdir") + sys.getProperty("file.separator") + ApiConstants.PATH_TO_ADD_TO_TEMP);
        JLabel lastLine = new JLabel(Language.getInstance().getText("Verifiez_bien_que_l_application_qui_permet_de_visualiser_ce_fichier_pointe_sur_le_bon_repertoire"));
        
        
        
        
        box = new JCheckBox(Language.getInstance().getText("Ne_plus_afficher_cette_fenetre"));
        box.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (box.isSelected()) {
                        DataModel.setBadDirectoryView(false);
                    } else {
                        DataModel.setBadDirectoryView(true);
                    }
                }
            });
        
        JButton endButton = new JButton(Language.getInstance().getText("Terminer"));
        endButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    BadDirectoryView.this.dispose();
                }
            });
        
        JPanel firstLinePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        firstLinePanel.add(firstLine);
        
        JPanel dirPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        dirPanel.add(dirName);
        
        JPanel lastLinePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        lastLinePanel.add(lastLine);
        
        JPanel boxPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        boxPanel.add(box);
        
        JPanel buttonPanel  = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(endButton);
        
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(firstLinePanel);
        page.add(dirPanel);
        page.add(Box.createRigidArea(new Dimension(1,20)));
        page.add(lastLinePanel);
        page.add(Box.createRigidArea(new Dimension(1,20)));
        page.add(buttonPanel);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(boxPanel);
        
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        this.setResizable(false);
        this.setTitle(Language.getInstance().getText("Attention_"));
        /** this.pack();
            this.setLocationRelativeTo(this.getParent()); 
            this.setVisible(true);
        */
        centerScreen();
    }
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
} // Fin de la classe BadDirectoryView
