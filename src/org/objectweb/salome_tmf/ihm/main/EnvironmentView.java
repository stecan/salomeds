/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.java.plugin.Extension;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.ScriptEngine;


/**
 * Classe qui d?finit la vue pour g?rer les environnements
 * @author teaml039
 * @version : 0.1
 */
public class EnvironmentView extends JPanel implements IHMConstants, ActionListener {
    
    static JButton refreshButton;
        
    /**
     * Bouton pour ajouter un environnement
     */
    static JButton addButton;
    
    /**
     * Bouton pour supprimer un environnement
     */
    static JButton deleteButton;
    
    /**
     * Bouton pour modifier un environnement
     */
    static JButton modifyButton;
    
    /**
     * Bouton pour visualiser des bugs
     */
    //JButton showBugButton;
    
    /**
     * Mod?le de la table des environnement
     */
    MyTableModel environmentTableModel;
    TableSorter  sorter ;
    /**
     * Table des environnements
     */
    JTable environmentTable;
    
    /**
     * Mod?le de s?lection pour la table des environnements
     */
    ListSelectionModel rowSM;
    
    //static JButton modifyScriptButton;
    //static JButton commitButton;
    
    static JMenuBar scriptMenuBar;
    static JMenu scriptMenu;
    static JMenuItem modifyScriptItem;
    static JMenuItem actualItem;
    static JMenuItem setUpEngineItem;
    
    JMenu bugTrackMenu;
    JPanel buttonsPanel;
    
    File importFile;
    
    HashMap modifyScriptList;
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    
    /**
     * Constructeur de la vue
     */
    public EnvironmentView() {
        refreshButton = new JButton(Language.getInstance().getText("Rafraichir"));
        addButton = new JButton(Language.getInstance().getText("Ajouter"));
        deleteButton = new JButton(Language.getInstance().getText("Supprimer"));
        modifyButton = new JButton(Language.getInstance().getText("Modifier"));
        
        environmentTable = new JTable();
        environmentTableModel = new MyTableModel();
        DataModel.setEnvironmentTableModel(environmentTableModel);
        
        refreshButton.addActionListener(this);
        
        addButton.setToolTipText(Language.getInstance().getText("Ajouter_un_nouvel_environnement"));
        addButton.addActionListener(this);
        
        modifyButton.setToolTipText(Language.getInstance().getText("Modifier_un_environnement"));
        modifyButton.setEnabled(false);
        modifyButton.addActionListener(this);
        
        deleteButton.setToolTipText(Language.getInstance().getText("Supprimer_un_environnement"));
        deleteButton.setEnabled(false);
        deleteButton.addActionListener(this);
        
        
        scriptMenu = new JMenu(Language.getInstance().getText("Script"));
        modifyScriptItem = new JMenuItem(Language.getInstance().getText("Modifier_le_Script"));
        modifyScriptItem.addActionListener(this);
        modifyScriptItem.setEnabled(false);
        
        bugTrackMenu = new JMenu(Language.getInstance().getText("Bug_tracking"));
        bugTrackMenu.setEnabled(false);
        
        actualItem = new JMenuItem(Language.getInstance().getText("Actualiser_le_Script"));
        actualItem.addActionListener(this);
        actualItem.setEnabled(false);
        
        setUpEngineItem = new JMenuItem("SetUp Script Engine");
        setUpEngineItem.addActionListener(this);
        setUpEngineItem.setEnabled(false);
        
        scriptMenu.add(modifyScriptItem);
        scriptMenu.add(actualItem);
        scriptMenu.add(setUpEngineItem);
        scriptMenuBar = new JMenuBar();
        scriptMenuBar.add(scriptMenu);
        
        
        // Construction de la table
        environmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        environmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Script"));
        environmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Parametres"));
        environmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        
        sorter = new TableSorter(environmentTableModel);
        environmentTable.setModel(sorter);
        sorter.setTableHeader(environmentTable.getTableHeader());
        
        
        
        environmentTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        environmentTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        // Gestion des s?lection dans la table
        rowSM = environmentTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;
                
                    int nbOfSelectedRows = environmentTable.getSelectedRowCount();
                    int selectedRow = environmentTable.getSelectedRow();
                    if (selectedRow != -1) {
                        if (nbOfSelectedRows != 1) {
                            if (Permission.canExecutCamp()) deleteButton.setEnabled(true);
                            modifyButton.setEnabled(false);
                            /*
                              modifyScriptButton.setEnabled(false);
                              commitButton.setEnabled(false);
                            */
                            actualItem.setEnabled(false);
                            modifyScriptItem.setEnabled(false);
                            setUpEngineItem.setEnabled(false);
                            bugTrackMenu.setEnabled(false);
                        } else {
                            //Environment env = DataModel.getCurrentProject().getEnvironment((String)environmentTableModel.getValueAt(selectedRow,0));
                            Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
                            if (Permission.canUpdateCamp() || Permission.canExecutCamp()) modifyButton.setEnabled(true);
                            if (Permission.canExecutCamp()) deleteButton.setEnabled(true);
                            if ((Permission.canUpdateCamp() || Permission.canExecutCamp()) && env.getInitScriptFromModel() != null)  {
                                //modifyScriptButton.setEnabled(true);
                                modifyScriptItem.setEnabled(true);
                                setUpEngineItem.setEnabled(true);
                            } else {
                                //modifyScriptButton.setEnabled(false);
                                modifyScriptItem.setEnabled(false);
                                setUpEngineItem.setEnabled(false);
                            }
                        
                            if (Api.isConnected() && Permission.canUpdateCamp() && isCommitable((String)sorter.getValueAt(selectedRow,0))) {
                                //commitButton.setEnabled(true);
                                actualItem.setEnabled(true);
                            
                            } else {
                                //commitButton.setEnabled(false);
                                actualItem.setEnabled(false);
                            
                            }
                            bugTrackMenu.setEnabled(true);
                        }
                    
                    } else {
                        //showBugButton.setEnabled(false);
                        deleteButton.setEnabled(false);
                        modifyButton.setEnabled(false);
                        //modifyScriptButton.setEnabled(false);
                        //commitButton.setEnabled(false);
                    
                        actualItem.setEnabled(false);
                        modifyScriptItem.setEnabled(false);
                        setUpEngineItem.setEnabled(false);
                        bugTrackMenu.setEnabled(false);
                    }
                    scriptMenu.setEnabled(actualItem.isEnabled() || setUpEngineItem.isEnabled() || modifyScriptItem.isEnabled());
                }
            });
        
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_MANAGEMENT_ENV_TABLE,environmentTable);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.DATA_MANAGEMENT_ENV_TABLE);
        
        JScrollPane environmentTableScrollPane = new JScrollPane(environmentTable);
        
        //JPanel buttonsPanel = new JPanel();
        buttonsPanel = new JPanel(new GridLayout(1,5));
        buttonsPanel.add(refreshButton);
        buttonsPanel.add(addButton);
        buttonsPanel.add(modifyButton);
        buttonsPanel.add(deleteButton);
        
        buttonsPanel.add(scriptMenuBar);
        //buttonsPanel.add(modifyScriptButton);
        //buttonsPanel.add(commitButton);
        //buttonsPanel.add(showBugButton);
        
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_MANAGEMENT_ENV_BUTTONS_PANEL,buttonsPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.DATA_MANAGEMENT_ENV_BUTTONS_PANEL);
        
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        environmentTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent e) {
                    if (e.getClickCount() > 1) {
                        if (Permission.canCreateCamp() || Permission.canExecutCamp()) modifyEnvironment();
                    }
                }
            });
        
        initData();
        
        this.setLayout(new BorderLayout());
        this.add(BorderLayout.NORTH, buttonsPanel);
        this.add(BorderLayout.CENTER, environmentTableScrollPane);
        
    } // Fin du constructeur EnvironmentView/0
    
    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/
    
    /**
     * M?thode permettant de modifier les environnements
     */
    private void modifyEnvironment() {
        int selectedRow = environmentTable.getSelectedRow();
        if( selectedRow != -1) {
            //Environment env = DataModel.getCurrentProject().getEnvironment((String)environmentTableModel.getValueAt(selectedRow,0));
            Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
            String oldEnvName = env.getNameFromModel();
            Script oldInitScript = env.getInitScriptFromModel();
            String oldDescription = env.getDescriptionFromModel();
            
            HashMap oldAttachMap = env.getCopyOfAttachmentMapFromModel();
            Hashtable oldParameter = env.getCopyOfParameterHashTableFromModel();
            
            AskNewEnvironment askNewEnvironment = new AskNewEnvironment(env);
            if (askNewEnvironment.getEnvironment() != null) {
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(11, ApiConstants.UPDATE_ENVIRONMENT);
                    Environment environment = askNewEnvironment.getEnvironment();
                    String newName = environment.getNameFromModel();
                    environment.updateInModel(oldEnvName,  environment.getDescriptionFromModel());
                    environment.updateInDBAndModel(newName, environment.getDescriptionFromModel());
                    if (environment.getInitScriptFromModel() != null) {
                        if (!environment.getInitScriptFromModel().equals(oldInitScript)) {
                            if (oldInitScript != null) {
                                //askNewEnvironment.getEnvironment().deleteScriptFromDB(oldInitScript.getNameFromModel());
                                environment.deleteScriptInDB();
                            }
                            Script script = environment.getInitScriptFromModel();
                            //environment.addScript2DB(script, askNewEnvironment.getScriptFile());
                            environment.addScriptInDBAndModel(script, askNewEnvironment.getScriptFile());
                        }
                    } else if (oldInitScript != null) {
                        //askNewEnvironment.getEnvironment().deleteScriptInDB(oldInitScript.getNameFromModel());
                        environment.deleteScriptInDB();
                    }
                    
                    /* Les Paramtres */
                    Hashtable newParameter = environment.getParametersHashTableFromModel();
                    Set keysSet = newParameter.keySet();
                    for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                        Parameter param =  (Parameter) iter.next();
                        String Value = (String) oldParameter.get(param);
                        String newValue = (String)newParameter.get(param);
                        if (Value == null){             
                            environment.addParameterValueInDB(param, newValue);
                        } else {
                            /* Update */
                            environment.updateParamValueInDB(param, newValue);
                        }
                    }
                                
                    keysSet = oldParameter.keySet();
                    for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                        Parameter param =  (Parameter) iter.next();
                        String Value = (String) newParameter.get(param);
                        if (Value == null) {
                            environment.deleteDefParamInDB(param.getIdBdd());
                        }
                    }
                    /* ATTACHEMENTS */
                    environment.updateAttachement(oldAttachMap);
                    
                    Api.commitTrans(transNumber);
                    
                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    env.updateInModel(oldEnvName, oldDescription);
                    env.addScriptInModel(oldInitScript);
                    // env.setParametersHashTableInModel(oldParamTable);
                    env.setParametersHashTableInModel(oldParameter);
                    env.setAttachmentMapInModel(oldAttachMap);
                    Tools.ihmExceptionView(exception);
                }
                
            } else {
                env.addScriptInModel(oldInitScript);
                //env.setParametersHashTableInModel(oldParamTable);
                env.setParametersHashTableInModel(oldParameter);
                env.updateInModel(oldEnvName, oldDescription);
                env.setAttachmentMapInModel(oldAttachMap);
            }
            //environmentTableModel.setValueAt(env.getName(), selectedRow, 0);
            sorter.setValueAt(env.getNameFromModel(), selectedRow, 0);
            if (env.getInitScriptFromModel() == null) {
                //environmentTableModel.setValueAt("", selectedRow, 1);
                sorter.setValueAt("", selectedRow, 1);
            } else {
                sorter.setValueAt(env.getInitScriptFromModel().getNameFromModel(), selectedRow, 1);
                modifyScriptItem.setEnabled(true);
                setUpEngineItem.setEnabled(true);
                modifyScriptList.put(env, new Boolean(true));
                
            }
            sorter.setValueAt(env.getParametersHashTableFromModel(), selectedRow, 2);
            sorter.setValueAt(env.getDescriptionFromModel(), selectedRow, 3);
            
        }
        scriptMenu.setEnabled(actualItem.isEnabled() || setUpEngineItem.isEnabled() || modifyScriptItem.isEnabled());
    } // Fin de la m?thode
    
    private void initData() {
        modifyScriptList = new HashMap();
        for (int i=0; i < environmentTableModel.getRowCount(); i++) {
            //modifyScriptList.put(environmentTableModel.getValueAt(i,0), new Boolean(false));
            modifyScriptList.put(sorter.getValueAt(i,0), new Boolean(false));
        }
    }
    
    private boolean isCommitable(String envName) {
        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(envName);
        Boolean bool = (Boolean)modifyScriptList.get(env);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }
    
    /**
     * M?thode qui g?re les permissions sur la vue
     */
    public static void giveAccessToIhmEnvironmentView() {
        if (!Permission.canExecutCamp()) {
            deleteButton.setEnabled(false);
        }
        if (!Permission.canCreateCamp() && !Permission.canExecutCamp()) {
            addButton.setEnabled(false);
        }
        if (!Permission.canUpdateCamp() && !Permission.canExecutCamp()) {
            modifyButton.setEnabled(false);
            //modifyScriptButton.setEnabled(false);
            //commitButton.setEnabled(false);
            
            actualItem.setEnabled(false);
            modifyScriptItem.setEnabled(false);
            setUpEngineItem.setEnabled(true);
        }
        scriptMenu.setEnabled(actualItem.isEnabled() || setUpEngineItem.isEnabled() || modifyScriptItem.isEnabled());
    } // Fin de la m?thode giveAccessToIhmEnvironmentView/0
    
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object source = e.getSource();
        if (source.equals(addButton)){
            addPerformed(e);
        } else if (source.equals(modifyButton)){
            modifyPerformed(e);
        } else if (source.equals(deleteButton)){
            deletePerformed(e);
        } else if (source.equals(actualItem)){
            commitScriptPerformed(e);
        } else if (source.equals(modifyScriptItem)){
            modifyScriptPerformed(e);
        } else if (source.equals(setUpEngineItem)){
            setUpEnginePerformed(e);
        } else if (source.equals(refreshButton)){
            refreshPerformed();
        }
    }
    
    void refreshPerformed(){
        try {
            DataModel.reloadParameters();
            DataModel.reloadEnvironnements();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    public void setUpEnginePerformed(ActionEvent e) {
        int selectedRow = environmentTable.getSelectedRow();
        if( selectedRow != -1) {
            //Environment env = DataModel.getCurrentProject().getEnvironment((String)environmentTableModel.getValueAt(selectedRow,0));
            Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
            if (env == null){
                JOptionPane.showMessageDialog(EnvironmentView.this,
                                              Language.getInstance().getText("Impossible_de_recuperer_l_environement_selectionne"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            try {
                Script pScript = null;
                //int plugScriptType;
                pScript = env.getInitScriptFromModel();
                if (pScript == null){
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Impossible_de_recuperer_le_fichier_depuis_la_base"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
                //plugScriptType = ScriptEngine.ENV_SCRIPT;
                ScriptEngine pEngine = pScript.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(pScript.getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().urlSalome, SalomeTMFContext.getInstance().jpf);
                if (pEngine != null){
                    String oldplugArg = pScript.getPlugArgFromModel();
                    String plugArg = pEngine.modifyEngineAgument(pScript);
                    
                    if (oldplugArg.equals(plugArg)){
                        return;
                    }
                    try {
                        pScript.updatePlugArgInDB(plugArg);
                        pScript.updatePlugArgInModel(plugArg);
                        JOptionPane.showMessageDialog(EnvironmentView.this,
                                                      Language.getInstance().getText("Le_fichier_a_ete_correctement_archive"),
                                                      Language.getInstance().getText("Info"),
                                                      JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception exception) {
                        pScript.updatePlugArgInModel(oldplugArg);
                        Tools.ihmExceptionView(exception);
                    }
                    
                }else {
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Impossible_d_initialiser_le_plugin_du_script"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } catch (Exception e1){
                Tools.ihmExceptionView(e1);
            }
        }
    }
    
    public void commitScriptPerformed(ActionEvent e) {
        if (Api.isConnected()) {
            int selectedRow = environmentTable.getSelectedRow();
            if( selectedRow != -1) {
                //Environment env = DataModel.getCurrentProject().getEnvironment((String)environmentTableModel.getValueAt(selectedRow,0));
                Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(1, ApiConstants.UPDATE_ATTACHMENT);
                    Date date = new Date(importFile.lastModified());
                    Script script = env.getInitScriptFromModel();
                    script.updateInDB(importFile);
                    /*script.updateContentInDB(importFile.getAbsolutePath());
                      script.updateDateInDB(date);
                      script.updateLengthInDB(importFile.length());
                    */
                    
                    Api.commitTrans(transNumber);
                    transNumber = -1;
                    //if (Api.getException() == null || Api.getException().size() == 0) {
                    modifyScriptList.put(env, new Boolean(false));
                    //commitButton.setEnabled(false);
                    actualItem.setEnabled(false);
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Le_fichier_a_ete_correctement_archive"),
                                                  Language.getInstance().getText("Info"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                    //}
                } catch (FileNotFoundException fe) {
                    Api.forceRollBackTrans(transNumber);
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Impossible_de_trouver_le_fichier_Vous_pouvez_l_importer_en_cliquant_sur_le_bouton_Modifier"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    Tools.ihmExceptionView(exception);
                }
                
            }
        } else {
            JOptionPane.showMessageDialog(EnvironmentView.this,
                                          Language.getInstance().getText("Impossible__Vous_n_etes_pas_connecte_a_la_base"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
    public void modifyScriptPerformed(ActionEvent e) {
        int selectedRow = environmentTable.getSelectedRow();
        if( selectedRow != -1) {
            Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
            if (env == null){
                JOptionPane.showMessageDialog(EnvironmentView.this,
                                              Language.getInstance().getText("Impossible_de_recuperer_l_environement_selectionne"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
                Cursor c = new Cursor(Cursor.WAIT_CURSOR);
                EnvironmentView.this.setCursor(c);
            } catch (Exception ex) {
                Util.err(ex);
            }
            try {
                Script pScript = null;
                String plugSeting ="";
                Hashtable param = new Hashtable();
                param.put("salome_projectName", DataModel.getCurrentProject().getNameFromModel());
                param.put("salome_ProjectObject", DataModel.getCurrentProject());
                param.put("salome_debug", new Boolean(true));
                param.put("salome_CampagneName", "");
                param.put("salome_ExecName", ""); // Add MM
                param.put("salome_environmentName", env.getNameFromModel());
                param.put("salome_TestName", "");
                param.put("salome_SuiteTestName", "");
                param.put(Language.getInstance().getText("salome_FamilyName"), "");
                param.put("testLog","");
                param.put("Verdict", "");
                int plugScriptType;
                String fileScript = null;
                pScript = new Script(env.getInitScriptFromDB());
                if (pScript == null){
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Impossible_de_recuperer_le_fichier_depuis_la_base"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
                plugScriptType = ScriptEngine.ENV_SCRIPT;
                plugSeting = pScript.getPlugArgFromModel();
                if (Api.isConnected()) {
                    //importFile = env.getEnvScriptFromDB(pScript.getNameFromModel());
                    importFile = env.getInitScriptFileFromDB();
                    fileScript = Tools.speedpurge(importFile.getAbsolutePath());
                } else {
                    fileScript = Tools.speedpurge(pScript.getNameFromModel());
                }
                
                if (fileScript != null) {
                    ScriptEngine pEngine = pScript.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(pScript. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().urlSalome, SalomeTMFContext.getInstance().jpf);
                    if (pEngine != null){
                        pEngine.editScript(fileScript,plugScriptType, pScript, param, plugSeting);
                    } else {
                        JOptionPane.showMessageDialog(EnvironmentView.this,
                                                      Language.getInstance().getText("Impossible_d_initialiser_le_plugin_du_script"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(EnvironmentView.this,
                                                  Language.getInstance().getText("Impossible_de_recuperer_le_fichier_depuis_la_base"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
                
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(EnvironmentView.this,
                                              Language.getInstance().getText("Erreur_pour_lancer_l_editeur_de_script__") + ex.getMessage(),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
            
            try {
                Cursor c = new Cursor(Cursor.DEFAULT_CURSOR);
                EnvironmentView.this.setCursor(c);
            } catch (Exception ee) {
                Util.err(ee);
            }
            //commitButton.setEnabled(true);
            actualItem.setEnabled(true);
            modifyScriptList.put(env, new Boolean(true));
        }
    }
    
    
    public void deletePerformed(ActionEvent e) {
        int selectedRow = environmentTable.getSelectedRow();
        String message = "";
        String text = "";
        ArrayList concernedExecutions = DataModel.getCurrentProject().getExecutionOfEnvironmentInModel((String)sorter.getValueAt(selectedRow,0));
        if (concernedExecutions.size() > 0) {
            for (int i = 0; i < concernedExecutions.size(); i ++) {
                message = message + "* " + ((Execution)concernedExecutions.get(i)).getCampagneFromModel().getNameFromModel() + "/" +((Execution)concernedExecutions.get(i)).getNameFromModel() + "\n";
            }
            
            text = Language.getInstance().getText("L_environnement_") + (String)sorter.getValueAt(selectedRow,0) + Language.getInstance().getText("_est_utilise_pour_les_executions_") + message;
            text = text + Language.getInstance().getText("Sa_suppression_entrainera_la_suppression_de_toutes_ces_executions");
        }
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        int choice = -1;
        //int actionCase = -1;
        choice = JOptionPane.showOptionDialog(EnvironmentView.this,
                                              //text + "Etes vous s?r de vouloir supprimer l'environnement <" + (String)environmentTableModel.getValueAt(selectedRow,0) + "> ?",
                                              text + Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_l_environnement_") + (String)sorter.getValueAt(selectedRow,0) + "> ?",
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[1]);
        if (choice == JOptionPane.YES_OPTION) {
            
            try {
                // BdD
                Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0));
                int idEnv = env.getIdBdd();
                //env.deleteFromDB();
                DataModel.getCurrentProject().deleteEnvironmentInDBandModel(env); 
                // IHM
                //ProjectData.getCurrentProject().removeEnvironment((String)sorter.getValueAt(selectedRow,0));
                environmentTableModel.removeData(sorter.modelIndex(selectedRow));
                for (int i = 0; i < concernedExecutions.size(); i++) {
                    Campaign camp = ((Execution)concernedExecutions.get(i)).getCampagneFromModel();
                    //camp.removeExecution((Execution)concernedExecutions.get(i));
                    if (DataModel.getCurrentCampaign() != null && DataModel.getCurrentCampaign().equals(camp)) {
                        
                        DataModel.getExecutionTableModel().removeRow(((Execution)concernedExecutions.get(i)).getNameFromModel());
                        
                    }
                }
                
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
            }
        }
    }
    
    
    
    public void modifyPerformed(ActionEvent e) {
        modifyEnvironment();
    }
    
    
    public void addPerformed(ActionEvent e) {
        // Ouverture de la fen?tre d'ajout
        AskNewEnvironment askNewEnvironment = new AskNewEnvironment();
        Environment env = askNewEnvironment.getEnvironment();
        // Si environnement est null = annuler
        if (env != null) {
            int transNumber = -1;
            try {
                // BdD
                transNumber = Api.beginTransaction(1, ApiConstants.INSERT_ENVIRONMENT);
                //env.add2DB();
                DataModel.getCurrentProject().addEnvironmentInDBAndModel(env);
                
                Set keysSet = env.getParametersHashTableFromModel().keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    Parameter param = (Parameter)iter.next();
                    //env.addParamValue2DB(param);
                    env.addParamValueInDBAndModel(param, env.getParameterValueFromModel(param));
                }
                if (askNewEnvironment.getScriptFile() != null) {
                    //env.addScript2BddAndModel(env.getInitScriptFromModel(),askNewEnvironment.getScriptFile());
                    env.addScriptInDBAndModel(env.getInitScriptFromModel(), askNewEnvironment.getScriptFile());
                }
                Set keySet = env.getAttachmentMapFromModel().keySet();
                for (Iterator iter = keySet.iterator(); iter.hasNext();) {
                    Attachment attach = (Attachment)env.getAttachmentMapFromModel().get(iter.next());
                    env.addAttachementInDB(attach);
                }
                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                ArrayList data = new ArrayList();
                data.add(askNewEnvironment.getEnvironment().getNameFromModel());
                String initScriptName = "";
                if (askNewEnvironment.getEnvironment().getInitScriptFromModel() != null) {
                    initScriptName = askNewEnvironment.getEnvironment().getInitScriptFromModel().getNameFromModel();
                }
                data.add(initScriptName);
                data.add(askNewEnvironment.getEnvironment().getParametersHashTableFromModel());
                data.add(askNewEnvironment.getEnvironment().getDescriptionFromModel());
                environmentTableModel.addRow(data);
                //ProjectData.getCurrentProject().addEnvironment(askNewEnvironment.getEnvironment());
                
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
        }
        
    }
    
    public void activateBugTrackersMenu() {
        //Menu for bugtrackers
        //Vector bugTrackers = SalomeTMFContext.getInstance().bugTrackerExtensions;
        Vector bugTrackers = SalomeTMFContext.getInstance().getBugTracker();

        if (bugTrackers.size() != 0) {
            for (int i=0 ; i<bugTrackers.size() ; i++) {
                //final BugTracker bugTracker = (BugTracker)SalomeTMFContext.getInstance().bugTrackers.elementAt(i);
                final BugTracker bugTracker = (BugTracker)bugTrackers.elementAt(i);
                //final BugTracker bugTracker = (BugTracker)bugTrackers.elementAt(i);
                JMenu bugTrackerMenu = new JMenu(bugTracker.getBugTrackerName());
                        
                // Menu item for environment bugs
                JMenuItem showBugsItem = new JMenuItem(Language.getInstance().getText("Show_env_bugs"));
                showBugsItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                int selectedRow = environmentTable.getSelectedRow();
                                Environment currentEnv = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0)); 
                                bugTracker.showEnvironmentBugs(currentEnv);
                            } catch (Exception E) {
                                Util.err(E);
                            }
                        }
                    });
                bugTrackerMenu.add(showBugsItem);
                        
                if (Api.isWith_ICAL()) {
                    // Menu item for ICAL calculation
                    JMenuItem calculateICALItem = new JMenuItem(Language.getInstance().getText("ICAL_calculation"));
                    calculateICALItem.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int selectedRow = environmentTable.getSelectedRow();
                                Environment currentEnv = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0)); 
                                calculateICALForEnv(bugTracker,currentEnv);
                            }
                        });
                    bugTrackerMenu.add(calculateICALItem);
                                
                    // Menu item for ICAL graph
                    JMenuItem graphICALItem = new JMenuItem(Language.getInstance().getText("ICAL_graph"));
                    graphICALItem.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                int selectedRow = environmentTable.getSelectedRow();
                                Environment currentEnv = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRow,0)); 
                                graphICALForEnv(bugTracker,currentEnv);
                            }
                        });
                    bugTrackerMenu.add(graphICALItem);
                }
                        
                if (!bugTracker.isUserExistsInBugDB()) {
                    bugTrackerMenu.setEnabled(false);
                }
                bugTrackMenu.add(bugTrackerMenu);
            }
            JMenuBar menuBar = new JMenuBar();
            menuBar.add(bugTrackMenu);
            buttonsPanel.add(menuBar);
        }
    }
    
    void calculateICALForEnv(BugTracker bugTracker,Environment env) {
        int choice = -1;
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        
        /*int n0 = bugTracker.getEnvironmentNbBugs(env,BugTracker.CRITICAL,false);
          Util.log("Nb Critical Bugs = " + n0);
                
          int n1 = bugTracker.getEnvironmentNbBugs(env,BugTracker.MAJOR,false);
          Util.log("Nb Major Bugs = " + n1);
                
          int nc0 = bugTracker.getEnvironmentNbBugs(env,BugTracker.CRITICAL,true);
          Util.log("Nb Corrected Critical Bugs = " + nc0);
                
          int nc1 = bugTracker.getEnvironmentNbBugs(env,BugTracker.MAJOR,true);
          Util.log("Nb Corrected Major Bugs = " + nc1);*/
                
        int nbMajorBugs = bugTracker.getEnvNbBugs(env,false);
        Util.log("Nb Major Bugs = " + nbMajorBugs);
                
        int nbCorMajorBugs = bugTracker.getEnvNbBugs(env,true);
        Util.log("Nb Corrected Major Bugs = " + nbCorMajorBugs);
                                        
        //if ((n0==0)&&(n1==0)) {
        if (nbMajorBugs==0) {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("ICAL_problem_env")+" '"+env.getNameFromModel()+"'."+Language.getInstance().getText("ICAL_problem_env2"),
                                          Language.getInstance().getText("Information_"),
                                          JOptionPane.INFORMATION_MESSAGE);;
                        
        } else {
            float ical_temp = Math.round(((float)nbCorMajorBugs/(float)nbMajorBugs)*10000); 
            float ical = ical_temp/100;
            String message = "- ICAL = "+ ical + " (env. '" + env.getNameFromModel()+ "')";
            choice = JOptionPane.showOptionDialog(new Frame(),
                                                  message + Language.getInstance().getText("ICAL_value_for_env"),
                                                  Language.getInstance().getText("Information_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
                        
            if (choice == JOptionPane.YES_OPTION) {
                                
                                
                try {
                    ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
                    if (pISQLConfig != null){
                        Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                        Locale locale = Locale.getDefault();
                        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                        java.util.Date date = new java.util.Date();
                        String myDate = dateFormat.format(date);
                        String key = bugTracker.getBugTrackerName()+"_ICAL_"+env.getIdBdd();
                                        
                        if (projectConfs.containsKey(key)) {
                            pISQLConfig.updateProjectConf(key,(String)projectConfs.get(key)+","+myDate+"_"+Float.toString(ical),
                                                          DataModel.getCurrentProject().getIdBdd());
                        } else {
                            pISQLConfig.insertProjectConf(bugTracker.getBugTrackerName()+"_ICAL_"+env.getIdBdd(), 
                                                          myDate+"_"+Float.toString(ical), DataModel.getCurrentProject().getIdBdd());
                        }
                                        
                    } else {
                        Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
                    }
                } catch (Exception E){
                    Util.err(E);
                    Tools.ihmExceptionView(E);
                }
                        
                JOptionPane.showMessageDialog(new Frame(),
                                              Language.getInstance().getText("ICAL_saved_confirmation_for_env"),
                                              Language.getInstance().getText("Information_"),
                                              JOptionPane.INFORMATION_MESSAGE);
                                
            }
        }
    }
    
    void graphICALForEnv(BugTracker bugTracker, Environment env) {
        JDialog dialog = new JDialog(SalomeTMFContext.getInstance().ptrFrame,true);
        dialog.setTitle(Language.getInstance().getText("ICAL"));
        JPanel pnl = new JPanel(new BorderLayout()); 
        dialog.setContentPane(pnl); 
        dialog.setSize(600,400); 

        try {
            String envICALKey = "";
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.equals(bugTracker.getBugTrackerName()+"_ICAL_"+env.getIdBdd())) {
                        envICALKey = key;
                    }
                }
                        
                if ((envICALKey != null) && (!envICALKey.equals(""))) {
                                
                    TimeSeriesCollection dataset = new TimeSeriesCollection();
                    TimeSeries ts = new TimeSeries(env.getNameFromModel(),Second.class);
                    String value = (String)projectConfs.get(envICALKey);
                    String[] res = value.split("[,]");
                    for (int j=0 ; j<res.length ; j++) {
                        String icalValues = res[j];
                        String[] icals = icalValues.split("[_]");
                        if (icals.length == 2) {
                            Locale locale = Locale.getDefault();
                            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                            java.util.Date date = dateFormat.parse(icals[0]);
                                                                                
                            float ical = Float.parseFloat(icals[1]); 
                            ts.add(new TimeSeriesDataItem(new Second(date),ical));
                        }
                    }
                    dataset.addSeries(ts);
                                
                    JFreeChart chart = ChartFactory.createTimeSeriesChart(Language.getInstance().getText("ICALgraphForEnv") + " '" + env.getNameFromModel() + "'", "Date", 
                                                                          "ICAL (%)", dataset, true, true, false);
                    chart.setBackgroundPaint(Color.white);
                    XYPlot plot = (XYPlot) chart.getPlot();
                    plot.setBackgroundPaint(Color.lightGray);
                    plot.setDomainGridlinePaint(Color.white);
                    plot.setRangeGridlinePaint(Color.white);
                    XYItemRenderer r = plot.getRenderer();
                    if (r instanceof XYLineAndShapeRenderer) {
                        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
                        renderer.setShapesVisible(true);
                        renderer.setShapesFilled(true);
                    }
                    DateAxis axis = (DateAxis) plot.getDomainAxis();
                    axis.setDateFormatOverride(new SimpleDateFormat("dd/MM/yy"));
                    ValueAxis vAxis = plot.getRangeAxis();
                    vAxis.setRange(0,100);
                    ChartPanel cPanel = new ChartPanel(chart); 
                    pnl.add(cPanel);
                            
                } else {
                    JLabel label = new JLabel(Language.getInstance().getText("No_ICAL_values_in_DB"));
                    pnl.add(label);
                }
                        
            } else {
                Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
                
        dialog.setVisible(true);
    }
    
} // Fin de la classe EnvironmentView
