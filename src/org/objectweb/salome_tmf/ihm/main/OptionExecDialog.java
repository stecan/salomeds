package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.languages.Language;

public class OptionExecDialog  extends JDialog implements ActionListener{

    JButton bOK;
    JButton bCancel;
    JCheckBox checkWithOutResTest;
    JCheckBox checkAssignedTest;
    JCheckBox checkWithOutSucces; 
    boolean cancelPerformed = false;
                
    OptionExecDialog(Frame parent,boolean withOutSucces, boolean withOutRes, boolean withAssigned){
        super(parent, true);
        initComponent(withOutSucces, withOutRes, withAssigned);
        centerParent();
    }
                
                
                
    void initComponent(boolean withOutSucces, boolean checkWithOutRes, boolean checkAssigned){
        bOK = new JButton(Language.getInstance().getText("Lancer"));
        bCancel = new JButton(Language.getInstance().getText("Annuler"));
        checkWithOutResTest = new JCheckBox(Language.getInstance().getText("Executer_tests_sans_resultat"));
        checkAssignedTest = new JCheckBox(Language.getInstance().getText("Executer_tests_assigne"));
        checkWithOutSucces = new JCheckBox(Language.getInstance().getText("Executer_tests_en_echec"));
                        
                        
        bOK.addActionListener(this);
        bCancel.addActionListener(this);
        checkWithOutResTest.addActionListener(this);
        checkAssignedTest.addActionListener(this);
                        
        JPanel checkBoxPanel = new JPanel();
        checkBoxPanel.setLayout(new BoxLayout(checkBoxPanel, BoxLayout.Y_AXIS));
        if (checkWithOutRes){
            checkBoxPanel.add(checkWithOutResTest);
        }
        if (withOutSucces){
            checkBoxPanel.add(checkWithOutSucces);
        }
        if (checkAssigned){
            checkBoxPanel.add(checkAssignedTest);
        }
                        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(bCancel);
        buttonPanel.add(bOK);
                        
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(checkBoxPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }
                
                
                
    void centerParent () {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }  
                
                
                 
    /*********************** Action Listener ***************************/
                
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(bOK)){
            bOKPerformed();
        } else if (e.getSource().equals(bCancel)){
            bCancelPerformed();
        } else if (e.getSource().equals(checkWithOutResTest)){
            checkWithOutResTestPerformed();
        } else if (e.getSource().equals(checkAssignedTest)){
            checkAssignedTestPerformed();
        }
    }
                
    boolean isOkSelected(){
        return !cancelPerformed;
    }
                
    boolean isAssignedTestSelected(){
        return checkAssignedTest.isSelected();
    }
                
    boolean isTestWihtoutResSelected(){
        return checkWithOutResTest.isSelected();
    }
                
    boolean ischeckWithOutSuccesSelected(){
        return checkWithOutSucces.isSelected();
    }
                
    void bOKPerformed(){
        dispose();
    }
                
    void bCancelPerformed(){
        cancelPerformed = true;
        dispose();
    }
                
    void checkWithOutResTestPerformed(){
                        
    }
                
    void checkAssignedTestPerformed(){
                        
    }
}
