/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import org.apache.log4j.Logger;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import static org.objectweb.salome_tmf.api.ApiConstants.FAIL;
import static org.objectweb.salome_tmf.api.ApiConstants.NOTAPPLICABLE;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import static org.objectweb.salome_tmf.ihm.IHMConstants.PATH_TO_FAIL_ICON;
import static org.objectweb.salome_tmf.ihm.IHMConstants.PATH_TO_NOTAPPLICABLE_ICON;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.tools.sql.DirectConnect;

/**
 * Classe qui cr?e la fen?tre tracant l'?x?cution d'un test
 *
 * @author teaml039
 * @version : 0.1
 */
public class ManualExecution extends JDialog implements ApiConstants,
                                                        DataConstants, IHMConstants, Runnable {

    final static Logger logger = Logger.getLogger(ManualExecution.class);
    
    /**
     * Bouton pour indiquer que l'action a r?ussi
     */
    JButton statusOkButton;

    /**
     * Bouton pour indiquer que l'action a ?chou?
     */
    JButton statusFailedButton;

    /**
     * Buttons for Dentsplysirona
     */
    JButton statusNotApplicableButton;
    JButton statusBlockedButton;
    JButton statusNoneButton;

    /**
     * Bouton pour indiquer que l'on ne conna?t pas le r?sultat de l'action
     */
    JButton statusUnknowButton;

    /**
     * Bouton pour revenir au test pr?c?dent
     */
    JButton prevTestButton;

    /**
     * Bouton pour passer au test suivant
     */
    JButton nextTestButton;

    /**
     * Bouton pour interrompre l'ex?cution
     */
    // JButton stopButton;

    /**
     * Bouton pour sortir de la fen?tre
     */
    JButton endButton;

    JButton lastExecutionResultButton;
    JLabel resultLabel;

    /**
     * Mod?le de donn?es pour la table des actions
     */
    MyTableModel actionsTableModel;

    /**
     * Table des actions
     */
    JTable actionsTable;

    /**
     * Description de l'action courante
     */
    JTextPane actionDescription;

    /**
     * R?sultat attendu de l'action courante
     */
    JTextPane awaitedResult;

    /**
     * R?sultat effectif de l'action courante
     */
    JTextPane effectiveResult;

    /**
     * Liste des tests ? ex?cuter
     */
    ArrayList testsToBeExecuted;

    /**
     * Nom du test courant
     */
    JLabel testName;
    JLabelLink TestNameHyperLink;

    /**
     * Description du test courant
     */
    JTextPane testDescription;
    HTMLDocument m_oDocument;
    HTMLEditorKit m_oHTML;
    /**
     * Conteneur pour l'affichage
     */
    Container contentPaneFrame;

    /**
     * Le test en cours d'ex?cution
     */
    ManualTest testInExecution;

    ManualExecutionResult execTestRes;
    /**
     * Indice du test en cours d'ex?cution
     */
    int indexOfTextInExecution;

    /**
     * R?sultat de l'?x?cution
     */
    ExecutionResult execResult;

    /**
     * Nom du r?sultat d'ex?cution courant
     */
    String currentExecutionResultName;

    /**
     * Indexe de bloquage dans la table des actions (pour emp?cher la s?lection)
     */
    int blockIndex;

    /**
     * Mod?le de s?lection pour la table des actions
     */
    ListSelectionModel rowSM;

    /**
     * Execution courante
     */
    Execution currentExecution;

    HashMap oldMap;

    Thread t;

    // ExecutionView execView;

    // JButton addBugButton;

    boolean finished = false;
    boolean stop_exec = false;
    List<Connection> attachedConnections = Config.getAttachedConnections();
    List<String> lastEffectiveResults = new ArrayList<>();

    String title;
    
    Config pConfig;
    
    /**************************************************************************/
    /** CONSTRUCTEUR ***/
    /**************************************************************************/
    // ExecutionResult finalExecResult;
    int t_x = 1024 - 200;
    int t_y = 768 - 200;
    int t_x2 = 1024;
    int t_y2 = 768;
    int t_label = 200;

    /****** Gestion des attachements par onglet *******/
    AttachmentView resExecTestAttachmentPanel;
    AttachmentView testAttachmentPanel;
    AttachmentView actionAttachmentPanel;
    JTabbedPane pJTabbedPane;

    /**
     * Constructeur de la fen?tre
     *
     * @param testList
     *            liste de tests ? ex?cuter
     */
    public ManualExecution(ArrayList testList, Execution execution,
                           ExecutionResult executionResult, boolean contineExec) {

        super(SalomeTMFContext.getInstance().ptrFrame, true);
        // setResizable(false);
        // this.finalExecResult = finalExecResult;

        DataModel.setObervedExecutionResult(executionResult);

        try {
            GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height - 200;
            t_x2 = r.width;
            t_y2 = r.height;
            
            if (t_x < 1200) {
                t_label = 200;
            } else if (t_x < 1500) {
                t_label = 300;
            } else if (t_x < 1800) {
                t_label = 400;
            } else if (t_x < 2100) {
                t_label = 500;
            } else {
                t_label = 600;
            }
        } catch (Exception E) {

        }

        finished = false;
        blockIndex = 0;

        oldMap = new HashMap();

        statusOkButton = new JButton(Language.getInstance().getText("Succes"));

        statusFailedButton = new JButton(Language.getInstance().getText("Echec"));

        statusUnknowButton = new JButton(Language.getInstance().getText("Inconclusif"));

        statusNotApplicableButton = new JButton(Language.getInstance().getText("NotApplicable"));
        statusBlockedButton = new JButton(Language.getInstance().getText("Blocked"));
        statusNoneButton = new JButton(Language.getInstance().getText("None"));

        prevTestButton = new JButton(Language.getInstance().getText("Test_precedent"));

        nextTestButton = new JButton(Language.getInstance().getText("Test_suivant"));

        endButton = new JButton(Language.getInstance().getText("Terminer"));

        resultLabel = new JLabel("Result Label");
        resultLabel.setVisible(false);

        if (Permission.canDeleteTest()) {
            // The administrator group is right to modify the details of a test
            lastEffectiveResults.clear();
            lastExecutionResultButton = new JButton(Language.getInstance()
                    .getText("buttonLastExecutionResult"));
        }
        
        actionsTableModel = new MyTableModel();

        actionsTable = new JTable();

        actionDescription = new JTextPane();

        awaitedResult = new JTextPane();

        effectiveResult = new JTextPane();
        
        testsToBeExecuted = new ArrayList();

        testName = new JLabel();
        if (pConfig.getPolarionLinks()) {
            // Polarion links are shown
            TestNameHyperLink = new JLabelLink("Link");
        }
        
        testDescription = new JTextPane();
        m_oDocument = new HTMLDocument();
        m_oHTML = new HTMLEditorKit();
        testDescription.setEditorKit(m_oHTML);
        testDescription.setDocument(m_oDocument);

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_TEST_DESCRIPTION_TEXTPANE,
            testDescription);
        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_ACTION_DESCRIPTION_TEXTPANE,
            actionDescription);
        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_ACTION_WAITED_RESULT_TEXTPANE,
            awaitedResult);
        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_ACTION_EFFECTIVE_RESULT_TEXTPANE,
            effectiveResult);
        // Activation des plugins associ?s
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_TEST_DESCRIPTION_TEXTPANE);
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_ACTION_DESCRIPTION_TEXTPANE);
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_ACTION_WAITED_RESULT_TEXTPANE);
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_ACTION_EFFECTIVE_RESULT_TEXTPANE);

        // execView = executionView;

        statusOkButton.setToolTipText(Language.getInstance().getText("L_action_a_reussie"));
        statusOkButton.setIcon(Tools.createAppletImageIcon(PATH_TO_SUCCESS_ICON, ""));
        statusOkButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    resultLabel.setVisible(false);
                    resultLabel.setText("");
                    lastEffectiveResults.clear();                    
                    
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));
                    if (selectedRowIndex != -1 && pAction != null) {
                        actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                            PATH_TO_SUCCESS_ICON, ""), selectedRowIndex,
                            actionsTableModel.getColumnCount() - 1);
                        execTestRes.addStatusForActionInModel(
                                testInExecution.getActionFromModel(
                                        (String) actionsTableModel.getValueAt(selectedRowIndex, 0)), 
                                SUCCESS);
                        if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {

                            blockIndexCalculation(selectedRowIndex);
                            ListSelectionModel lsm = actionsTable
                                .getSelectionModel();
                            lsm.setSelectionInterval(selectedRowIndex + 1,
                                                     selectedRowIndex + 1);
                        } else {

                            blockIndexCalculation(0);
                            changeTest();
                        }
                    }
                }
            });

        statusFailedButton.setToolTipText(Language.getInstance().getText("L_action_a_echouee"));
        statusFailedButton.setIcon(Tools.createAppletImageIcon(PATH_TO_FAIL_ICON, ""));
        statusFailedButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    resultLabel.setVisible(false);
                    resultLabel.setText("");
                    lastEffectiveResults.clear();                    

                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));

                    // Read keys that are used by bug trackers, e.g.
                    //   - BUG       - for teamtrack
                    //   - HDAEC-    - for Jira IO7 project
                    List<String> bugtrackingKeys = pConfig.getBugtrackingKeys();
                    String key = "";
                    int i = 0;
                    for (String str: bugtrackingKeys) {
                        if (i == 0) {
                            key = str;
                        } else {
                            key = key + "|" + str;
                        }
                        i++;
                    }
                    key = "("+key+")";
                    
                    if (selectedRowIndex != -1 && pAction != null) {
                        
                        if (pConfig.getBugVerification()) {
                            // correct BUG-Number of the form 
                            //                  "BUG04711"
                            //                  "HDAEC-23"
                            String regexp = key + "\\d{1,}";
                            Pattern pattern = Pattern.compile(regexp, 
                                    Pattern.CASE_INSENSITIVE);

                            // wrong BUG-Number of the form 
                            //                  "BUG 04711"
                            //                  "HDAEC- 04711"
                            String wrongRegexp1 = key + "\\s+\\d{1,}";
                            Pattern wrongPattern1 = Pattern.compile(wrongRegexp1, 
                                    Pattern.CASE_INSENSITIVE);

//                            // wrong BUG-Number of the form 
//                            //                  "BUG 4711"
//                            String wrongRegexp2 = "BUG\\s+\\d{1,}+";
//                            Pattern wrongPattern2 = Pattern.compile(wrongRegexp2, 
//                                    Pattern.CASE_INSENSITIVE);

//                            // wrong BUG-Number of the form 
//                            //                  "BUG4711"
//                            String wrongRegexp3 = "BUG\\d{4}+\\D";
//                            Pattern wrongPattern3 = Pattern.compile(wrongRegexp3, 
//                                    Pattern.CASE_INSENSITIVE);

                            // wrong BUG-Number of the form 
                            //                  "BUG 047112..."
                            String wrongRegexp4 = key + "\\s+\\d{6,}";
                            Pattern wrongPattern4 = Pattern.compile(wrongRegexp4, 
                                    Pattern.CASE_INSENSITIVE);

//                            // wrong BUG-Number of the form 
//                            //                  "BUG47112.."
//                            String wrongRegexp5 = key + "\\d{6,}";
//                            Pattern wrongPattern5 = Pattern.compile(wrongRegexp5, 
//                                    Pattern.CASE_INSENSITIVE);

                            String wrongRegexp6 = "\\w"+key+"\\d{1,}";
                            Pattern wrongPattern6 = Pattern.compile(wrongRegexp6, 
                                    Pattern.CASE_INSENSITIVE);

//                            String wrongRegexp7 = "BUG\\d{5}+\\w";
//                            Pattern wrongPattern7 = Pattern.compile(wrongRegexp7, 
//                                    Pattern.CASE_INSENSITIVE);

                            String str1 = execTestRes.getEffectivResultFromModel(pAction) + " ";
                            Matcher matcher = pattern.matcher(str1);
                            Matcher wrongMatcher1 = wrongPattern1.matcher(str1);
                            // Matcher wrongMatcher2 = wrongPattern2.matcher(str1);
                            // Matcher wrongMatcher3 = wrongPattern3.matcher(str1);
                            Matcher wrongMatcher4 = wrongPattern4.matcher(str1);
                            // Matcher wrongMatcher5 = wrongPattern5.matcher(str1);
                            Matcher wrongMatcher6 = wrongPattern6.matcher(str1);
                            // Matcher wrongMatcher7 = wrongPattern7.matcher(str1);

                            // Negative search: 
                            // 1. Search for wrong patterns
                            // 2. If no wrong pattern is available, then search
                            //    for correct bug number
                            // 3. Search for empty string
                            // 4. An error occured. The user should verify his
                            //    input string.
                            if (wrongMatcher4.find()) {
                                bug_047112();   // "BUG_047112"
//                            } else if (wrongMatcher5.find()) {
//                                bug047112();   // "BUG047112..."
                            } else if (wrongMatcher6.find()) {
                                abug04711();   // "aBUG04711"
//                            } else if (wrongMatcher7.find()) {
//                                bug04711a();   // "BUG04711a"
                            } else if (wrongMatcher1.find()) {
                                bug_04711();  // "BUG 04711"
//                            } else if (wrongMatcher2.find()) {
//                                bug_4711();   // "BUG 4711"
//                            } else if (wrongMatcher3.find()) {
//                                bug4711();   // "BUG4711"
                            } else if (matcher.find()) {
                                // A valid bug number is found. Though the 
                                // status of the test execution ist set to FAIL.
                                execTestRes.addStatusForActionInModel(
                                        testInExecution.getActionFromModel(
                                                (String) actionsTableModel.
                                                        getValueAt(selectedRowIndex, 0)), 
                                        FAIL);
                                actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                                        PATH_TO_FAIL_ICON, ""), selectedRowIndex,
                                        actionsTableModel.getColumnCount() - 1);
                                if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {
                                    // blockIndex = selectedRowIndex + 1;
                                    blockIndexCalculation(selectedRowIndex);
                                    ListSelectionModel lsm = actionsTable
                                        .getSelectionModel();
                                    lsm.setSelectionInterval(selectedRowIndex + 1,
                                                             selectedRowIndex + 1);
                                } else {
                                    // blockIndex = 0;
                                    blockIndexCalculation(0);
                                    changeTest();
                                }
//                            } else if (str1.equals(" ")) {
//                                noErrorNumber();    // No valid error number available!
                            } else {
                                noErrorNumber();    // No valid error number available!
//                                 generalUserError(); // general error
                            }
                        } else {
                            // A valid bug number is found. Though the 
                            // status of the test execution is set to FAIL.
                            execTestRes.addStatusForActionInModel(
                                    testInExecution.getActionFromModel(
                                            (String) actionsTableModel.
                                                    getValueAt(selectedRowIndex, 0)), 
                                    FAIL);
                            actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                                    PATH_TO_FAIL_ICON, ""), selectedRowIndex,
                                    actionsTableModel.getColumnCount() - 1);
                            if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {
                                // blockIndex = selectedRowIndex + 1;
                                blockIndexCalculation(selectedRowIndex);
                            } else {
                                // blockIndex = 0;
                                blockIndexCalculation(0);
                            }
                        }
                    }
                }
            });

        statusUnknowButton.setToolTipText(Language.getInstance().
                getText("Pas_de_resultat_pour_cette_action"));
        statusUnknowButton.setIcon(Tools.createAppletImageIcon(PATH_TO_UNKNOW_ICON, ""));
        statusUnknowButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    resultLabel.setVisible(false);
                    resultLabel.setText("");
                    lastEffectiveResults.clear();                    
                    
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));
                    if (selectedRowIndex != -1 && pAction != null) {
                        execTestRes.addStatusForActionInModel(
                                testInExecution.getActionFromModel(
                                        (String) actionsTableModel.
                                                getValueAt(selectedRowIndex, 0)), 
                                UNKNOWN);
                        actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                                PATH_TO_UNKNOW_ICON, ""), selectedRowIndex,
                                actionsTableModel.getColumnCount() - 1);
                        if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {
                            // blockIndex = selectedRowIndex + 1;
                            blockIndexCalculation(selectedRowIndex);
                            ListSelectionModel lsm = actionsTable
                                .getSelectionModel();
                            lsm.setSelectionInterval(selectedRowIndex + 1,
                                                     selectedRowIndex + 1);
                        } else {
                            // blockIndex = 0;
                            blockIndexCalculation(0);
                        }
                    }
                }
            });

        statusNotApplicableButton.setToolTipText(Language.getInstance().
                getText("L_action_not_applicable"));
        statusNotApplicableButton.setIcon(Tools.
                createAppletImageIcon(PATH_TO_NOTAPPLICABLE_ICON, ""));
        statusNotApplicableButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));
                    if (selectedRowIndex != -1 && pAction != null) {

                        // correct pattern (a comment is given)
                        String regexp = "\\S+";
                        Pattern pattern = Pattern.compile(regexp, 
                                Pattern.CASE_INSENSITIVE);

                        String str1 = execTestRes.getEffectivResultFromModel(pAction);
                        String str2 = str1 + " ";
                        Matcher matcher = pattern.matcher(str2);

                        if ((str1 != null) && (!str1.equals("null")) && (matcher.find())) {
                            actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                                PATH_TO_NOTAPPLICABLE_ICON, ""), selectedRowIndex,
                                actionsTableModel.getColumnCount() - 1);
                            execTestRes.addStatusForActionInModel(
                                    testInExecution.getActionFromModel(
                                            (String) actionsTableModel.
                                                    getValueAt(selectedRowIndex, 0)), 
                                    NOTAPPLICABLE);
                            if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {

                                blockIndexCalculation(selectedRowIndex);
                                ListSelectionModel lsm = actionsTable
                                    .getSelectionModel();
                                lsm.setSelectionInterval(selectedRowIndex + 1,
                                                         selectedRowIndex + 1);
                            } else {

                                blockIndexCalculation(0);
                                changeTest();
                            }
                        } else {
                            notCommented();   // The result has to be commented!
                        }
                    }
                }
            });
        
        statusBlockedButton.setToolTipText(Language.getInstance().
                getText("L_action_blocked"));
        statusBlockedButton.setIcon(Tools.
                createAppletImageIcon(PATH_TO_BLOCKED_ICON, ""));
        statusBlockedButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));
                    if (selectedRowIndex != -1 && pAction != null) {

                        // correct pattern (a comment is given)
                        String regexp = "\\S+";
                        Pattern pattern = Pattern.compile(regexp, 
                                Pattern.CASE_INSENSITIVE);

                        String str1 = execTestRes.getEffectivResultFromModel(pAction);
                        String str2 = str1 + " ";
                        Matcher matcher = pattern.matcher(str2);

                        if ((str1 != null) && (!str1.equals("null")) && (matcher.find())) {
                            actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                                PATH_TO_BLOCKED_ICON, ""), selectedRowIndex,
                                actionsTableModel.getColumnCount() - 1);
                            execTestRes.addStatusForActionInModel(
                                    testInExecution.getActionFromModel(
                                            (String) actionsTableModel.
                                                    getValueAt(selectedRowIndex, 0)), 
                                    BLOCKED);
                            if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {

                                blockIndexCalculation(selectedRowIndex);
                                ListSelectionModel lsm = actionsTable
                                    .getSelectionModel();
                                lsm.setSelectionInterval(selectedRowIndex + 1,
                                                         selectedRowIndex + 1);
                            } else {

                                blockIndexCalculation(0);
                                changeTest();
                            }
                        } else {
                            notCommented();   // The result has to be commented!
                        }
                    }
                }
            });
        
        statusNoneButton.setToolTipText(Language.getInstance().getText("L_action_none"));
        statusNoneButton.setIcon(Tools.createAppletImageIcon(PATH_TO_NONE_ICON, ""));
        statusNoneButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    Action pAction = testInExecution
                        .getActionFromModel((String) actionsTableModel
                                            .getValueAt(selectedRowIndex, 0));
                    if (selectedRowIndex != -1 && pAction != null) {
                        actionsTableModel.setValueAt(Tools.createAppletImageIcon(
                            PATH_TO_NONE_ICON, ""), selectedRowIndex,
                            actionsTableModel.getColumnCount() - 1);
                        execTestRes.addStatusForActionInModel(
                                testInExecution.getActionFromModel(
                                        (String) actionsTableModel.
                                                getValueAt(selectedRowIndex, 0)), 
                                NONE);
                        if (selectedRowIndex < (actionsTableModel.getRowCount() - 1)) {

                            blockIndexCalculation(selectedRowIndex);
                            ListSelectionModel lsm = actionsTable
                                .getSelectionModel();
                            lsm.setSelectionInterval(selectedRowIndex + 1,
                                                     selectedRowIndex + 1);
                        } else {

                            blockIndexCalculation(0);
                            changeTest();
                        }
                    }
                }
            });
        
        prevTestButton.setToolTipText(Language.getInstance().
                getText("Revenir_au_test_precedent"));
        prevTestButton.setEnabled(false);
        prevTestButton.setIcon(Tools.createAppletImageIcon(PATH_TO_ARROW_BACK_ICON, ""));
        prevTestButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    changeTestBack();
                    resultLabel.setVisible(false);
                    resultLabel.setText("");
                    lastEffectiveResults.clear();                    
                    blockIndexCalculation(0);
                }
            });

        nextTestButton.setToolTipText(Language.getInstance().
                getText("Passer_au_test_suivant"));
        nextTestButton.setIcon(Tools.createAppletImageIcon(PATH_TO_ARROW_ICON,
                                                           ""));
        nextTestButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    changeTest();
                    resultLabel.setVisible(false);
                    resultLabel.setText("");
                    lastEffectiveResults.clear();                    
                    blockIndexCalculation(0);
                }
            });
        if (testList.size() < 2) {
            nextTestButton.setEnabled(false);
        }

        endButton.setToolTipText(Language.getInstance().
                getText("Sortir_de_l_execution"));
        endButton.setIcon(Tools.createAppletImageIcon(PATH_TO_MOVE_ICON, ""));
        endButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    testStatusCalculation(true);

                    finished = true;
                    ManualExecution.this.dispose();
                    }
            });

        if (Permission.canDeleteTest()) {
            // The administrator group is right to modify the details of a test
            lastExecutionResultButton.setToolTipText(Language.getInstance()
                    .getText("TooltipLastExecutionResult"));
            lastExecutionResultButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        int selectedRowIndex = actionsTable.getSelectedRow();

                        String nameFromModel = testInExecution.getNameFromModel();
                        if (lastEffectiveResults.isEmpty()) {
                            lastEffectiveResults = DirectConnect.getLastEffectiveResults(
                                    attachedConnections, nameFromModel);
                            DirectConnect.ResultState resultState;
                            resultState = DirectConnect.getResultState();
                            switch (resultState) {
                                case NO_TESTCASE:
                                    resultLabel.setVisible(true);
                                    resultLabel.setText(Language.getInstance()
                                            .getText("MsgTestNotFound"));
                                    break;
                                case NO_DATA:
                                    resultLabel.setVisible(true);
                                    resultLabel.setText(Language.getInstance()
                                            .getText("MsgTestNoExecuted"));
                                    break;
                                case EMPTY_EFFECTIVE_RESULT:
                                    resultLabel.setVisible(true);
                                    resultLabel.setText(Language.getInstance()
                                            .getText("MsgNoEntries"));
                                    break;
                                case OK:

                                    HashMap map = execTestRes.getEffectivResultMapFromModel();
                                    ArrayList<Action> actionList;
                                    actionList = testInExecution.getActionListFromModel(false);
                                    if (actionList != null) {
                                        int i = 0;
                                        for (Action action : actionList) {
                                            if (i < lastEffectiveResults.size()) {
                                                map.put(action, lastEffectiveResults.get(i));
                                            }
                                            i++;
                                        }
                                    }
                                    execTestRes.setEffectivResultMapInModel(map);

                                    Action pAction = testInExecution
                                        .getActionFromModel((String) actionsTableModel
                                                            .getValueAt(selectedRowIndex, 0));

                                    if (selectedRowIndex != -1 && pAction != null) {
                                        if (selectedRowIndex < lastEffectiveResults.size()) {
                                            effectiveResult.setText(
                                                    lastEffectiveResults.get(selectedRowIndex));
                                        }
                                    }

                                    if (actionList.size() == lastEffectiveResults.size()) {
                                        resultLabel.setText(Language.getInstance()
                                            .getText("MsgResultOK"));
                                    } else if (actionList.size() < lastEffectiveResults.size()) {
                                        resultLabel.setText(Language.getInstance()
                                            .getText("MsgTeststepsAdded"));
                                    } else if (actionList.size() > lastEffectiveResults.size()) {
                                        resultLabel.setText(Language.getInstance()
                                            .getText("MsgTeststepsDeleted"));
                                    }
                                    resultLabel.setVisible(true);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                });
        }
        

        // Details sur le test
        testDescription.setEditable(false);
        JScrollPane actionDescriptionScrollPane  = new JScrollPane(
            actionDescription, 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        actionDescriptionScrollPane .setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Description_de_l_action")));

        // Table d'actions

        actionsTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Action"));
        actionsTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Attachement"));
        actionsTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Date"));
        actionsTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Statut"));

        /*
         * TableSorter sorter = new TableSorter(actionsTableModel);
         * actionsTable.setModel(sorter);
         * sorter.setTableHeader(actionsTable.getTableHeader());
         */
        actionsTable.setModel(actionsTableModel);
        actionsTable.setRowHeight(25);

        actionsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        rowSM = actionsTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;

                    int selectedRowIndex = actionsTable.getSelectedRow();
                    if (selectedRowIndex != -1
                        && actionsTableModel.getRowCount() > 0) {
                        if (blockIndex != -1 && selectedRowIndex > blockIndex) {
                            // selectedRowIndex = blockIndex;
                            blockIndex = selectedRowIndex;
                            rowSM.setSelectionInterval(selectedRowIndex,
                                                       selectedRowIndex);
                        }
                        Rectangle rect = actionsTable.getCellRect(selectedRowIndex,
                                actionsTable.getSelectedColumn(), false);
                        actionsTable.scrollRectToVisible(rect);
                        ArrayList dataList = actionsTableModel
                            .getData(selectedRowIndex);
                        Action action = testInExecution
                            .getActionFromModel((String) dataList.get(0));

                        actionDescription.setText(Tools.getInstantiedDescription(
                                action.getDescriptionFromModel(),
                                currentExecution));
                        actionDescription.getCaret().setDot(0);
                        awaitedResult.setText(Tools.getInstantiedDescription(
                                action.getAwaitedResultFromModel(), currentExecution));
                        awaitedResult.getCaret().setDot(0);
                        // effectiveResult.setText(execResult.getEffectivResult(action));
                        String str = execTestRes.getEffectivResultFromModel(action);
                        effectiveResult.setText(str);
                        effectiveResult.requestFocus();
                        effectiveResult.getCaret().setDot(0);
                    }
                }
            });

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                UICompCst.MANUAL_EXECUTION_ACTIONS_TABLE, actionsTable);
        
        // Activation des plugins associes
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_ACTIONS_TABLE);

        JScrollPane actionsTableScrollPane = new JScrollPane(actionsTable);
        actionsTableScrollPane.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Actions")));

        // Details sur les actions
        actionDescription.setEditable(false);
        JScrollPane testDescriptionScrollPane  = new JScrollPane(testDescription, 
            ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
            ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        testDescriptionScrollPane.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Description_du_test")));

        awaitedResult.setEditable(false);
        JScrollPane awaitedResultScrollPane = new JScrollPane(awaitedResult);
        awaitedResultScrollPane.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Resultat_attendu")));

        effectiveResult.addCaretListener(new EffectivResultListener());
        JScrollPane effectiveResultScrollPane = new JScrollPane(effectiveResult);
        effectiveResultScrollPane.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Resultat_effectif")));

        // Construction de la fen?tre
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setMaximumSize(new Dimension(t_x, 100));
        buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.X_AXIS));

        // JPanel basicbuttonPanel = new JPanel(new GridLayout(1, 8));
        int bouton_y = 30;
        statusOkButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        statusOkButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(statusOkButton);

        statusFailedButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        statusFailedButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(statusFailedButton);

//        statusUnknowButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
//        statusUnknowButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
//        buttonsPanel.add(statusUnknowButton);
//        buttonsPanel
//            .add(Box.createRigidArea(new Dimension(t_x / 20, bouton_y)));

        statusNotApplicableButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        statusNotApplicableButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(statusNotApplicableButton);
        
        statusBlockedButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        statusBlockedButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(statusBlockedButton);
        
        statusNoneButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        statusNoneButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(statusNoneButton);
        
        prevTestButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        prevTestButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(prevTestButton);

        nextTestButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        nextTestButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(nextTestButton);

        endButton.setMinimumSize(new Dimension(t_x / 10, bouton_y));
        endButton.setMaximumSize(new Dimension(t_x2 / 10, bouton_y));
        buttonsPanel.add(endButton);

        buttonsPanel
            .add(Box.createRigidArea(new Dimension(t_x / 20, bouton_y)));

        if (lastExecutionResultButton == null) {
            // do nothing (the button is not created because of not enough
            // rights of the calling user)
        } else {
            lastExecutionResultButton.setMinimumSize(new Dimension(t_x / 6, bouton_y));
            lastExecutionResultButton.setMaximumSize(new Dimension(t_x2 / 6, bouton_y));
            buttonsPanel.add(lastExecutionResultButton);
        }

        JPanel test = new JPanel();
        test.setMaximumSize(new Dimension(t_x, 100));
        test.setLayout(new BoxLayout(test, BoxLayout.X_AXIS));
        test.add(Box.createRigidArea(new Dimension(t_x-t_label, bouton_y)));
        test.add(resultLabel);
 
        // Menu for bugtrackers
        // Vector bugTrackers =
        // SalomeTMFContext.getInstance().bugTrackerExtensions;
        Vector bugTrackers = SalomeTMFContext.getInstance().getBugTracker();
        if (bugTrackers.size() != 0) {
            JMenu bugTrackMenu = new JMenu(Language.getInstance().getText("add_bug"));
            for (int i = 0; i < bugTrackers.size(); i++) {
                // final BugTracker bugTracker =
                // (BugTracker)SalomeTMFContext.getInstance().bugTrackers.elementAt(i);
                final BugTracker bugTracker = (BugTracker) bugTrackers
                    .elementAt(i);
                JMenuItem bugTrackerItem = new JMenuItem(bugTracker
                                                         .getBugTrackerName());
                bugTrackerItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int selectedRowIndex = actionsTable.getSelectedRow();
                            ArrayList dataList = actionsTableModel
                                .getData(selectedRowIndex);
                            Action action = testInExecution
                                .getActionFromModel((String) dataList.get(0));
                            new AskNewBug(ManualExecution.this, bugTracker, true,
                                action.getNameFromModel(), actionDescription
                                .getText(), awaitedResult.getText(),
                                effectiveResult.getText());

                        }
                    });

                if (!bugTracker.isUserExistsInBugDB()) {
                    bugTrackerItem.setEnabled(false);
                }
                bugTrackMenu.add(bugTrackerItem);
            }
            JMenuBar menuBar = new JMenuBar();
            menuBar.add(bugTrackMenu);
            buttonsPanel.add(menuBar);
        }

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_BUTTONS_PANEL, buttonsPanel);
        // Activation des plugins associ?es
        PluginsTools
            .activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_BUTTONS_PANEL);

        JPanel showTestPanel = new JPanel(new GridBagLayout());
        
        if (pConfig.getPolarionLinks()) {
            // Polarion links are shown
            showTestPanel.add(testName, 
                    new GridBagConstraints(0, 0, 2, 1, 0, 0,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(10, 10, 10, 10), 0, 0));
            // Polarion Link added
            showTestPanel.add(TestNameHyperLink, 
                    new GridBagConstraints(0, 1, 2, 1, 0, 0,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(10, 10, 10, 10), 0, 0));
            showTestPanel.add(testDescriptionScrollPane, 
                    new GridBagConstraints(0, 2, 2, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(actionDescriptionScrollPane, 
                    new GridBagConstraints(0, 3, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(actionsTableScrollPane, 
                    new GridBagConstraints(1, 3, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(awaitedResultScrollPane, 
                    new GridBagConstraints(0, 4, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(effectiveResultScrollPane, 
                    new GridBagConstraints(1, 4, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
        } else {
            // Polarion links are not shown!
            showTestPanel.add(testName, 
                    new GridBagConstraints(0, 0, 2, 1, 0, 0,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(10, 10, 10, 10), 0, 0));
            showTestPanel.add(testDescriptionScrollPane, 
                    new GridBagConstraints(0, 1, 2, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(actionDescriptionScrollPane, 
                    new GridBagConstraints(0, 2, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(actionsTableScrollPane, 
                    new GridBagConstraints(1, 2, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(awaitedResultScrollPane, 
                    new GridBagConstraints(0, 3, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
            showTestPanel.add(effectiveResultScrollPane, 
                    new GridBagConstraints(1, 3, 1, 1, 0.1, 0.1,
                        GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0), 0, 0));
        }

        initData(testList, execution, executionResult);
        if (contineExec) {
            setFirstTestWithNoResult();
        }
        setSize(t_x, t_y);

        pJTabbedPane = new JTabbedPane();

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        mainPanel.add(buttonsPanel);
        mainPanel.add(test);
        mainPanel.add(showTestPanel);
        
        pJTabbedPane.add(Language.getInstance().getText("Test"), mainPanel);

        /****** Attachement en panel ***********/
        resExecTestAttachmentPanel = new AttachmentView(DataModel.getApplet(),
            EXECUTION_RESULT_TEST, EXECUTION_RESULT_TEST, execTestRes,
            SMALL_SIZE_FOR_ATTACH, this);
        resExecTestAttachmentPanel.setBorder(BorderFactory.createTitledBorder(
            BorderFactory.createLineBorder(Color.BLACK), Language
            .getInstance().getText("Attachements")));
        pJTabbedPane.add(resExecTestAttachmentPanel, Language.getInstance()
                         .getText("Res_Exec_Attachs"));

        testAttachmentPanel = new AttachmentView(DataModel.getApplet(), TEST,
                                                 TEST, null, SMALL_SIZE_FOR_ATTACH, this);
        testAttachmentPanel.setReadOnly();
        pJTabbedPane.add(testAttachmentPanel, Language.getInstance().getText(
            "Current_Test_Attachs"));

        actionAttachmentPanel = new AttachmentView(DataModel.getApplet(),
            ACTION, ACTION, null, NORMAL_SIZE_FOR_ATTACH, this);
        actionAttachmentPanel.setReadOnly();
        pJTabbedPane.add(actionAttachmentPanel, Language.getInstance().getText(
            "Current_Action_Attachs"));

        pJTabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (pJTabbedPane.getSelectedComponent().equals(
                        resExecTestAttachmentPanel)) {
                    // DataModel.getCurrentExecutionTestResult()
                    resExecTestAttachmentPanel.setAttachmentObject(DataModel
                            .getCurrentExecutionTestResult());
                } else if (pJTabbedPane.getSelectedComponent().equals(
                        testAttachmentPanel)) {
                    DataModel.setCurrentTest(testInExecution);
                    testAttachmentPanel.setAttachmentObject(testInExecution);
                } else if (pJTabbedPane.getSelectedComponent().equals(
                        actionAttachmentPanel)) {
                    int selectedRowIndex = actionsTable.getSelectedRow();
                    ArrayList dataList = actionsTableModel
                        .getData(selectedRowIndex);
                    Action action = testInExecution
                        .getActionFromModel((String) dataList.get(0));
                    DataModel.setCurrentAction(action);
                    actionAttachmentPanel.setAttachmentObject(action);
                }
            }
        });
        /**************************************/
        ChangeListener changeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
                int index = sourceTabbedPane.getSelectedIndex();
                switch (index) {
                    case 0:
                        effectiveResult.requestFocus();
                        break;
                    default:
                        break;
                }
            }
        };
        pJTabbedPane.addChangeListener(changeListener);


        contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(pJTabbedPane);

        SalomeTMFContext.getInstance().addToUIComponentsMap(
            UICompCst.MANUAL_EXECUTION_TAB, pJTabbedPane);
        PluginsTools.activateAssociatedPlgs(UICompCst.MANUAL_EXECUTION_TAB);

        if (actionsTableModel.getRowCount() > 0) {
            rowSM.setSelectionInterval(0, 0);
        }

        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {

                    testStatusCalculation(true);
                    // execView.libere();
                    // execResult.setExecutionStatus(INTERRUPT);
                    stop_exec = true;
                    finished = true;
                    }

                @Override
                public void windowDeiconified(WindowEvent e) {
                }

                @Override
                public void windowOpened(WindowEvent e) {
                }

                @Override
                public void windowActivated(WindowEvent e) {
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                }

                @Override
                public void windowClosed(WindowEvent e) {
                }

                @Override
                public void windowIconified(WindowEvent e) {
                }
            });

        title = Language.getInstance().getText(
                "Execution_d_un_test_manuel_exec_en_cours__")
            + execution.getNameFromModel() + ")";
        this.setTitle(title + ", test : " + testInExecution.getNameFromModel());

        /*
         * this.setLocation(50,50); this.pack();
         */
        centerScreen();
        
    } // Fin du constructeur ManualExecution/0

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        // this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        // this.setVisible(true);
        requestFocus();
    }

    /**
     * M?thode qui initialise l'ex?cution avec la liste des tests pass?e en
     * param?tre
     *
     * @param list
     *            une liste de tests
     */
    public void initData(ArrayList list, Execution execution,
                         ExecutionResult executionResult) {

        execResult = executionResult;

        currentExecution = execution;
        int transNumber = -1;
        try {
            transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            ProgressBar pProgressBar = new ProgressBar("Data Loading", "", list
                                                       .size() + 1);
            pProgressBar.show();
            if (list != null && list.size() > 0) {
                testsToBeExecuted = list;
                indexOfTextInExecution = 0;
                blockIndex = 0;
                testInExecution = (ManualTest) testsToBeExecuted.get(0);
                this.setTitle(title + ", test : "
                              + testInExecution.getNameFromModel());
                execTestRes = (ManualExecutionResult) execResult
                    .getExecutionTestResultFromModel(testInExecution); // Add
                // FOR
                // V2
                DataModel.setCurrentExecutionTestResult(execTestRes);
                testName.setText(testInExecution.getTestListFromModel().
                        getFamilyFromModel().getNameFromModel()+" / "+
                        testInExecution.getTestListFromModel().getNameFromModel() + 
                        " / "+
                        testInExecution.getNameFromModel()
                                 );
                if (pConfig.getPolarionLinks()) {
                    // Polarion links are shown
                    TestNameHyperLink.generatePolarionLink(testInExecution.getNameFromModel());
                }
                testDescription.setText(testInExecution
                                        .getDescriptionFromModel());
                ArrayList actionList = testInExecution
                    .getActionListFromModel(false);
                pProgressBar.doTask("Load action");
                for (int i = 0; i < actionList.size(); i++) {
                    Action action = (Action) actionList.get(i);
                    ArrayList actionData = new ArrayList();
                    actionData.add(action.getNameFromModel());
                    actionData.add(action.getAttachmentMapFromModel().keySet());
                    actionData.add(Calendar.getInstance().getTime()
                                   .toString());
                    /*
                     * actionData.add(Tools.getActionStatusIcon(execResult.getActionStatus
                     * (action))); if (execResult.getActionStatus(action) !=
                     * null && !execResult.getActionStatus(action).equals("")) {
                     * blockIndex = blockIndex + 1; }
                     */
                    actionData.add(Tools.getActionStatusIcon(execTestRes
                                                             .getActionStatusInModel(action)));
                    if (execTestRes.getActionStatusInModel(action) != null
                        && !execTestRes.getActionStatusInModel(action)
                        .equals("")) {
                        blockIndex = blockIndex + 1;
                    }
                    actionsTableModel.addRow(actionData);
                }
                for (int i = 0; i < list.size(); i++) {
                    pProgressBar.doTask("Load action");
                    ManualTest test = (ManualTest) list.get(i);
                    // ADD FOR V2
                    ManualExecutionResult pExecTestRes = (ManualExecutionResult) execResult
                        .getExecutionTestResultFromModel(test);
                    ArrayList actionList2 = test.getActionListFromModel(false);
                    for (int j = 0; j < actionList2.size(); j++) {
                        Action action = (Action) actionList2.get(j);
                        // pExecTestRes.addDescriptionResultInModel(action,
                        // Tools.getInstantiedDescription(action.getDescriptionFromModel(),
                        // currentExecution.getDataSetFromModel()));
                        // pExecTestRes.addAwaitedResultInModel(action,
                        // Tools.getInstantiedDescription(action.getAwaitedResultFromModel(),
                        // currentExecution.getDataSetFromModel()));
                        pExecTestRes.addDescriptionResultInModel(action, Tools
                            .getInstantiedDescription(action
                                                      .getDescriptionFromModel(),
                                                      currentExecution));
                        pExecTestRes.addAwaitedResultInModel(action, Tools
                            .getInstantiedDescription(action
                                                      .getAwaitedResultFromModel(),
                                                      currentExecution));
                    }
                }
            }
            Api.commitTrans(transNumber);
            transNumber = -1;
            pProgressBar.finishAllTask();
        } catch (Exception e) {
            Api.forceRollBackTrans(transNumber);
        }
    } // Fin de la m?thode initData/1

    void setFirstTestWithNoResult() {
        if (testsToBeExecuted.size() > 1) {
            testInExecution = (ManualTest) testsToBeExecuted
                .get(indexOfTextInExecution);
            ExecutionTestResult pExecutionTestResult = execResult
                .getExecutionTestResultFromModel(testInExecution);
            String status = pExecutionTestResult.getStatusFromModel();
            while ((indexOfTextInExecution + 1) < testsToBeExecuted.size()
                   && status != null && !status.equals("")) {
                changeTest();
                testInExecution = (ManualTest) testsToBeExecuted
                    .get(indexOfTextInExecution);
                pExecutionTestResult = execResult
                    .getExecutionTestResultFromModel(testInExecution);
                status = pExecutionTestResult.getStatusFromModel();
            }
        }
    }

    /**
     * M?thode qui permet de passer au test suivant.
     */
    public void changeTest() {
        if (testsToBeExecuted.size() > 1) {
            prevTestButton.setEnabled(true);
        }
        if ((indexOfTextInExecution + 1) == (testsToBeExecuted.size() - 1)) {

            nextTestButton.setEnabled(false);
        }
        testStatusCalculation(false);
        if ((indexOfTextInExecution + 1) < testsToBeExecuted.size()) {

            indexOfTextInExecution++;
            testInExecution = (ManualTest) testsToBeExecuted
                .get(indexOfTextInExecution);
            this.setTitle(title + ", test : "
                          + testInExecution.getNameFromModel());

            DataModel.setCurrentExecutionTestResult(execResult
                .getExecutionTestResultFromModel(testInExecution));
            // NEW FOR V2
            execTestRes = (ManualExecutionResult) execResult
                .getExecutionTestResultFromModel(testInExecution);

            testName.setText(testInExecution.getTestListFromModel().
                    getFamilyFromModel().getNameFromModel()+" / "+
                    testInExecution.getTestListFromModel().getNameFromModel() + 
                    " / "+testInExecution.getNameFromModel());
                if (pConfig.getPolarionLinks()) {
                    // Polarion links are shown
                    TestNameHyperLink.generatePolarionLink(testInExecution.getNameFromModel());
                }
            testDescription.setText(testInExecution.getDescriptionFromModel());
            actionsTableModel.clearTable();
            ArrayList actionList = testInExecution
                .getActionListFromModel(false);
            if (actionList != null && actionList.size() > 0) {
                for (int i = 0; i < actionList.size(); i++) {
                    Action action = (Action) actionList.get(i);
                    ArrayList actionData = new ArrayList();
                    actionData.add(action.getNameFromModel());
                    actionData.add(action.getAttachmentMapFromModel().keySet());
                    actionData.add(Calendar.getInstance().getTime()
                                   .toString());
                    // putIconStatusInTable(actionData,
                    // execResult.getActionStatus(action), i);
                    putIconStatusInTable(actionData, execTestRes
                                         .getActionStatusInModel(action), i);
                    actionsTableModel.addRow(actionData);
                }
            } else {
                /* clear INFO */
                actionDescription.setText("");
                actionDescription.getCaret().setDot(0);
                awaitedResult.setText("");
                awaitedResult.getCaret().setDot(0);
                // effectiveResult.setText(execResult.getEffectivResult(action));
                effectiveResult.setText("");
                effectiveResult.getCaret().setDot(0);
            }
            ListSelectionModel lsm = actionsTable.getSelectionModel();
            lsm.setSelectionInterval(0, 0);

        } else if ((indexOfTextInExecution + 1) == testsToBeExecuted.size()) {

        }

    } // Fin de la m?thode changeTest/0

    /**
     * M?thode qui permet de passer au test pr?c?dant.
     */
    public void changeTestBack() {
        if (testsToBeExecuted.size() > 1) {
            nextTestButton.setEnabled(true);
        }
        if ((indexOfTextInExecution - 1) == 0) {
            prevTestButton.setEnabled(false);
        }
        testStatusCalculation(false);
        if ((indexOfTextInExecution - 1) > -1) {
            indexOfTextInExecution--;
            testInExecution = (ManualTest) testsToBeExecuted
                .get(indexOfTextInExecution);
            DataModel.setCurrentExecutionTestResult(execResult
                .getExecutionTestResultFromModel(testInExecution));
            this.setTitle(title + ", test : "
                          + testInExecution.getNameFromModel());
            // NEW FOR V2
            execTestRes = (ManualExecutionResult) execResult
                .getExecutionTestResultFromModel(testInExecution);
            testName.setText(testInExecution.getTestListFromModel().
                    getFamilyFromModel().getNameFromModel()+" / "+
                    testInExecution.getTestListFromModel().getNameFromModel() + 
                    " / "+testInExecution.getNameFromModel());
            if (pConfig.getPolarionLinks()) {
                // Polarion links are shown
                TestNameHyperLink.generatePolarionLink(testInExecution.getNameFromModel());
            }
            testDescription.setText(testInExecution.getDescriptionFromModel());
            actionsTableModel.clearTable();
            ArrayList actionList = testInExecution
                .getActionListFromModel(false);
            for (int i = 0; i < actionList.size(); i++) {
                Action action = (Action) actionList.get(i);
                ArrayList actionData = new ArrayList();
                actionData.add(action.getNameFromModel());
                actionData.add(action.getAttachmentMapFromModel().keySet());
                actionData.add(Calendar.getInstance().getTime()
                               .toString());
                // putIconStatusInTable(actionData,
                // execResult.getActionStatus(action), i);
                putIconStatusInTable(actionData, execTestRes
                                     .getActionStatusInModel(action), i);
                actionsTableModel.addRow(actionData);
            }
            ListSelectionModel lsm = actionsTable.getSelectionModel();
            lsm.setSelectionInterval(0, 0);

        }

    } // Fin de la m?thode changeTestBack/0

    /**
     * M?thode qui retourne le r?sultat de l'ex?cution
     *
     * @return le r?sultat de l'ex?cution
     */
    public ExecutionResult getExecutionResult() {
        return execResult;
    } // Fin de la m?thode getExecutionResult/0

    /**
     *
     * @param data
     * @param type
     * @param index
     */
    public void putIconStatusInTable(ArrayList data, String type, int index) {
        ImageIcon icon = getActionStatusIcon(type, index);
        if (icon != null) {
            data.add(icon);
        } else {
            data.add("");
        }
    } // Fin de la m?thode putIconStatusInTable/2

    /**
     * Retourne l'icone associ?e au type pass? au param?tre. On retourne
     * <code>null</code> si le type n'est pas SUCCESS, FAIL ou UNKNOW.
     *
     * @param type
     * @param index
     * @return
     */
    public ImageIcon getActionStatusIcon(String type, int index) {
        if (type.equals(SUCCESS)) {
        } else if (type.equals(FAIL)) {
            blockIndex = index;
        } else if (type.equals(UNKNOWN)) {
            blockIndex = index;
        } else if (type.equals(NOTAPPLICABLE)) {
            blockIndex = index;
        } else if (type.equals(BLOCKED)) {
            blockIndex = index;
        } else if (type.equals(NONE)) {
            blockIndex = index;
        }
        return Tools.getActionStatusIcon(type);
    } // Fin de la m?thode getActionStatusIcon/1

    /**
     *
     *
     */
    public void testStatusCalculation(Boolean printLog) {
        int successAction = 0;
        int failAction = 0;  // stecan (25. Jan. 2015)
        int unknownAction = 0;
        int notApplicableAction = 0;    // stecan (25. Mar. 2018)
        int blockedAction = 0;          // stecan (25. Mar. 2018)
        int noneAction = 0;             // stecan (25. Mar. 2018)
        ArrayList actionList = testInExecution.getActionListFromModel(false);
        for (int i = 0; i < actionList.size(); i++) {
            if (execTestRes
                .getActionStatusInModel(((Action) actionList.get(i)))
                .equals(FAIL)) 
            {
                failAction++;
                //return;  // stecan (25. Jan. 2015)
            } else if (execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(i))).equals(UNKNOWN)) 
            {
                unknownAction++;
                // return; // stecan (25. Jan. 2015)
            } else if (execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(i))).equals(NOTAPPLICABLE)) 
            {
                notApplicableAction++;
                // return; // stecan (25. Jan. 2015)
            } else if (execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(i))).equals(BLOCKED)) 
            {
                blockedAction++;
                // return; // stecan (25. Jan. 2015)
            } else if (execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(i))).equals(NONE)) 
            {
                noneAction++;
                // return; // stecan (25. Jan. 2015)
            } else if (execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(i))).equals(SUCCESS)) 
            {
                successAction++;
            }
        }

        int sum = successAction + failAction + unknownAction +
                notApplicableAction + blockedAction;
        boolean finished = sum == actionList.size();

        if (successAction == actionList.size()) {
            // Success!
            execResult.addTestResultStatusInModel(testInExecution, SUCCESS);
            if (printLog)
            {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: SUCCESS");
            }

        } else if ((failAction > 0) && finished) {
            // Failed!
            execResult.addTestResultStatusInModel(testInExecution, FAIL);
            if (printLog) {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: FAILED");
            }

        } else if ((blockedAction > 0) && finished) {
            // Blocked!
            execResult.addTestResultStatusInModel(testInExecution, BLOCKED);
            if (printLog) {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: BLOCKED");
            }

        } else if ((notApplicableAction > 0) && finished) {
            // Not Applicable!
            execResult.addTestResultStatusInModel(testInExecution, NOTAPPLICABLE);
            if (printLog) {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: NOTAPPLICABLE");
            }
        } else if ((unknownAction > 0) && finished) {
            // None Action!
            execResult.addTestResultStatusInModel(testInExecution, UNKNOWN);
            if (printLog) {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: UNKNOWN");
            }
        } else if (noneAction > 0) {
            // None Action!
            // Delete last test result status
            execResult.addTestResultStatusInModel(testInExecution, "");
            if (printLog) {
                logger.info(LoggingData.getData() + 
                        "Kampagne: " +
                        execResult.getExecution().getCampagneFromModel().toString() + "\n" +
                        "Execution-Result: " + 
                        execResult.toString() + "\n" +
                        "Status: BLOCKED");
            }
        } 
    } // Fin de la m?thode testStatusCalculation/0

    /**
     * Calcule l'index de bloquage de la s?lection.
     */
    private void blockIndexCalculation(int selectedRowIndex) {
        ArrayList actionList = testInExecution.getActionListFromModel(false);
        blockIndex = selectedRowIndex + 1;

        /*
         * while (blockIndex < actionList.size() &&
         * !execResult.getActionStatus((
         * (Action)actionList.get(blockIndex))).equals("")) { blockIndex++; }
         */
        while (blockIndex < actionList.size()
               && !execTestRes.getActionStatusInModel(
                    ((Action) actionList.get(blockIndex))).equals("")) {
            blockIndex++;
        }

    } // Fin de la m?thode blockIndexCalculation/0

    /**
     * Retourne la liste des attachements ? supprimer
     *
     * @return
     */
    public HashMap getOldAttachMap() {
        return oldMap;
    } // Fin de la m?thode getOldAttachMap/0

    /**
     *
     * @author teaml039
     */
    public class EffectivResultListener implements CaretListener {

        /*
         * (non-Javadoc)
         *
         * @see
         * javax.swing.event.CaretListener#caretUpdate(javax.swing.event.CaretEvent
         * )
         */
        @Override
        public void caretUpdate(CaretEvent e) {
            int selectedRowIndex = actionsTable.getSelectedRow();
            /*
             * if (selectedRowIndex != -1) {
             * execResult.addEffectivResult(testInExecution
             * .getActionFromModel((String
             * )actionsTableModel.getValueAt(selectedRowIndex, 0)),
             * ((JTextPane)e.getSource()).getText()); }
             */
            if (selectedRowIndex != -1) {
                execTestRes.addEffectivResultInModel(testInExecution
                    .getActionFromModel((String) actionsTableModel
                                        .getValueAt(selectedRowIndex, 0)),
                    ((JTextPane) e.getSource()).getText());
            }
        }
    } // Fin de la classe EffectivResultListener

    public void lauchManualExecution() {
        t = new Thread(this);
        stop_exec = false;
        t.start();
    }

    @Override
    public void run() {
        this.setVisible(true);
    }

    public boolean isStoped() {
        return stop_exec;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean b) {
        finished = b;
    }

    /************************** ScreenShot *********************/

    JDialog takeScreenShoot;
    boolean screenShootValided = false;

    public void screeshootFrame(java.awt.event.ActionEvent e) {
        screenShootValided = false;
        takeScreenShoot = new JDialog();
        takeScreenShoot.setResizable(false);
        takeScreenShoot.setTitle("Capture Screen");
        JButton screeshootButton2 = new JButton();
        ImageIcon icon = Tools.createAppletImageIcon(PATH_TO_SCREENSHOT_ICON, "");
        screeshootButton2.setIcon(icon);
        screeshootButton2.setMinimumSize(new Dimension(icon.getIconWidth(),
                                                       icon.getIconHeight()));
        // takeScreenShoot.setSize(new
        // Dimension(icon.getIconWidth(),icon.getIconHeight()));
        screeshootButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                    screenShootValided = true;
                    takeScreenShoot.dispose();
                    screeshootPerformed(DataModel.getCurrentExecutionTestResult());
                    // SalomeTMFContext.getInstance().getSalomeFrame().setVisible(true);
                    setVisible(true);
                }
            });

        takeScreenShoot.addWindowListener(new WindowListener() {
                @Override
                public void windowActivated(WindowEvent e) {
                }

                @Override
                public void windowClosed(WindowEvent e) {

                }

                @Override
                public void windowClosing(WindowEvent e) {
                    if (!screenShootValided) {
                        screenShootValided = true;
                        takeScreenShoot.dispose();
                        setVisible(true);
                    }
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                    takeScreenShoot.toFront();
                }

                @Override
                public void windowDeiconified(WindowEvent e) {
                }

                @Override
                public void windowIconified(WindowEvent e) {
                    takeScreenShoot.toFront();
                }

                @Override
                public void windowOpened(WindowEvent e) {
                }

            });
        // SalomeTMFContext.getInstance().getSalomeFrame().setVisible(false);
        setVisible(false);
        takeScreenShoot.getContentPane().add(screeshootButton2);
        takeScreenShoot.pack();
        takeScreenShoot.setVisible(true);

    }

    public void screeshootPerformed(WithAttachment withAttachment) {
        if (withAttachment == null) {
            return;
        }
        try {
            FileAttachment fileAttachment = withAttachment.takeScreenShot();
            withAttachment.addAttachementInModel(fileAttachment);

        } catch (Exception ex1) {
            JOptionPane.showMessageDialog(ManualExecution.this, Language
                .getInstance().getText("Le_fichier_")
                + "Screenshot "
                + Language.getInstance().getText("_est_deja_attache_a_")
                + withAttachment.getNameFromModel() + " !", Language
                .getInstance().getText("Erreur_"),
                JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed the form BUG 04711. 
    //
    //    Please remove the leading space in front of the number!
    void bug_04711() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG_04711"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed the form BUG 4711. 
    //
    //    Please remove the leading space in front of the number and correct 
    //    the number to a 5 digit number!
    void bug_4711() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG_4711"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed the form BUG4711. 
    //
    //    Please correct the number to a 5 digit number!
    void bug4711() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG4711"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed the form BUG 047112... 
    //
    //    Please remove the leading space in front of the number and correct 
    //    the number to a 5 digit number!    
    void bug_047112() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG_047112"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You type a number with more than 5 characters. 
    //
    //    Please correct the number to a 5 digit number.    
    void bug047112() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG047112"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed a bug in the form of aBug04711
    //
    //    Please insert a space in front of the bug.
    void abug04711() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("ABUG04711"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    A bug number should be of the form BUG04711. 
    //    You typed a bug in the form of Bug04711a
    //
    //    Please insert a space after the bug number.
    void bug04711a() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("BUG04711A"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //     Please type in a valid error number of the form BUG04711!
    void noErrorNumber() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("NoErrorNumber"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    No valid bug number found. Please verify your input!
    void generalUserError() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("GeneralUserError"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }

    // Prints the error message:
    //    Please type in a comment!
    //    
    void notCommented() {
        javax.swing.JOptionPane.showMessageDialog(this, 
                Language.getInstance().getText("NotCommented"),
                Language.getInstance().getText("InputError"),
                javax.swing.JOptionPane.ERROR_MESSAGE);
    }
    
    public void focusToEffectiveResult() {
        effectiveResult.requestFocus();
    }
} // Fin de la classe ManualExecution
