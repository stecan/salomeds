/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.admin.models.UserListRenderer;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui permet d'afficher une fen?tre pour ordonner les campagnes de tests
 * @author teaml039
 * @version : 0.1
 */
public class CampagneOrdering extends JDialog implements IHMConstants {
    
    /**
     * Ancien mod?le de donn?e (avant r?organisation)
     */
    DefaultListModel oldModel;
    
    /**
     * Mod?le de donn?es pour la liste de choix (famille, suites ou tests)
     */
    private DefaultComboBoxModel comboModel;
    
    /**
     * Liste d?roulante pour le choix des ?l?ments ? trier
     */
    JComboBox choiceComboBox;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fen?tre de r?ordonnancement
     * @param listModel le mod?le de donn?es de la liste
     */
    public CampagneOrdering(DefaultListModel listModel) {
        super(SalomeTMFContext.getInstance().ptrFrame);
        comboModel = new DefaultComboBoxModel();
        choiceComboBox = new JComboBox(comboModel);
        JRadioButton campagneButton = new JRadioButton(Language.getInstance().getText("Campagnes"));
        campagneButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    choiceComboBox.setEnabled(false);
                }
            });
        
        
        JRadioButton familyButton = new JRadioButton(Language.getInstance().getText("Familles"));
        familyButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    choiceComboBox.setEnabled(true);
                }
            });
        
        JRadioButton testListButton = new JRadioButton(Language.getInstance().getText("Suites"));
        testListButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    choiceComboBox.setEnabled(true);
                }
            });
        
        JRadioButton testButton = new JRadioButton(Language.getInstance().getText("Tests"));
        testButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    choiceComboBox.setEnabled(true);
                }
            });
        
        //Grouper les boutons radio
        ButtonGroup choiceGroup = new ButtonGroup();
        choiceGroup.add(campagneButton);
        choiceGroup.add(familyButton);
        choiceGroup.add(testListButton);
        choiceGroup.add(testButton);
        
        //Put the radio buttons in a column in a panel.
        JPanel radioPanel = new JPanel(new GridLayout(1, 0));
        radioPanel.add(campagneButton);
        radioPanel.add(familyButton);
        radioPanel.add(testListButton);
        radioPanel.add(testButton);
        
        // Combo Panel
        JPanel comboPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        comboPanel.add(choiceComboBox);
        choiceComboBox.setEnabled(false);
        
        JPanel choicePanel = new JPanel();
        choicePanel.setLayout(new BoxLayout(choicePanel, BoxLayout.Y_AXIS));
        choicePanel.add(radioPanel);
        choicePanel.add(comboPanel);
        choicePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        JButton upButton = new JButton();
        Icon icon = Tools.createAppletImageIcon(PATH_TO_ARROW_UP_ICON,"");
        upButton.setIcon(icon);
        upButton.setToolTipText(Language.getInstance().getText("Monter_d_un_cran"));
        upButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
        
        JButton downButton = new JButton();
        icon = Tools.createAppletImageIcon(PATH_TO_ARROW_DOWN_ICON,"");
        downButton.setIcon(icon);
        downButton.setToolTipText(Language.getInstance().getText("Descendre_d_un_cran"));
        downButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
        
        
        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(upButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
        buttonSet.add(downButton);
        
        JList list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.setCellRenderer(new UserListRenderer());
        list.setVisibleRowCount(15);
        JScrollPane usersListScrollPane = new JScrollPane(list);
        
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(""));
        
        JButton validate = new JButton(Language.getInstance().getText("Valider"));
        validate.setToolTipText(Language.getInstance().getText("Valider"));
        validate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CampagneOrdering.this.dispose();
                }
            });
        
        JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
        cancel.setToolTipText(Language.getInstance().getText("Annuler"));
        cancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CampagneOrdering.this.dispose();
                }
            });
        
        JPanel secondButtonSet = new JPanel();
        secondButtonSet.add(validate);
        secondButtonSet.add(cancel);
        
        JPanel center = new JPanel();
        center.add(usersListScrollPane);
        center.add(buttonSet);
        
        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        
        page.add(choicePanel);
        page.add(Box.createRigidArea(new Dimension(1,10)));
        page.add(center);
        page.add(secondButtonSet);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        this.setTitle(Language.getInstance().getText("Ordonner"));
        /**this.pack();
           this.setLocationRelativeTo(this.getParent()); 
           this.setVisible(true);*
        */
        centerScreen();
        
    } // Fin du constructeur CampagneOrdering/1
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
} // Fin de la classe CampagneOrdering
