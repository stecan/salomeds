/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Cursor;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.ReqManager;
import org.objectweb.salome_tmf.plugins.core.Statistiques;

public class SalomeTMFPanels {

    /**
     * Les onglets principaux
     */
    public JTabbedPane tabs;

    // LES 4 ECRANS

    /**
     * Le panel des tests
     */
    static TestPanel pTestPanel;

    /**
     * Le panel des campagnes
     */
    static CampainPanel pCampainPanel;

    /**
     * Le panel des donnees
     */
    static DataPanel pDataPanel;

    PluginsView pluginsView;

    /**
     * Le panel des indicateurs
     */
    static IndicatorsPanel pIndicatorsPanel;

    SalomeTMFContext pSalomeTMFContext;
    BaseIHM pBaseIHM;

    int t_x = 1024;
    int t_y = 768;

    BugTracker indicatorTracker;
    Statistiques stat;
    ReqManager reqPlug;

    public SalomeTMFPanels(SalomeTMFContext _pSalomeTMFContext, BaseIHM _BaseIHM) {
        pSalomeTMFContext = _pSalomeTMFContext;
        pBaseIHM = _BaseIHM;
    }

    public void initComponentPanel() {

        try {
            GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width-30;
            t_y = r.height-100;

        } catch (Exception E) {

        }

        try {
            /**
             * Les onglets principaux
             */
            tabs = new JTabbedPane();

            pTestPanel = new TestPanel(pSalomeTMFContext, pBaseIHM, tabs, t_x,
                                       t_y);
            pCampainPanel = new CampainPanel(pSalomeTMFContext, pBaseIHM, tabs,
                                             t_x, t_y);
            pDataPanel = new DataPanel(pSalomeTMFContext, pBaseIHM, tabs, t_x,
                                       t_y);

            tabs.addTab(Language.getInstance().getText("Plan_de_tests"),
                        pTestPanel);
            tabs.addTab(
                        Language.getInstance().getText("Gestion_des_campagnes"),
                        pCampainPanel);
            tabs.addTab(Language.getInstance().getText("Gestion_des_donnees"),
                        pDataPanel);

            tabs.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        if (tabs.getSelectedIndex() == 2) {
                            DataModel.initAttachmentTable(DataModel
                                                          .getCurrentProject()
                                                          .getAttachmentMapFromModel().values());
                        }
                    }
                });

            // Mapping entre objets graphiques et constantes
            pSalomeTMFContext.UIComponentsMap.put(UICompCst.MAIN_TABBED_PANE,
                                                  tabs);
            // Add this component as static component
            UICompCst.staticUIComps.add(UICompCst.MAIN_TABBED_PANE);

            tabs.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        if (tabs.getModel().getSelectedIndex() == 0) {
                            // Onglet des tests
                            if (getTestDynamicTree().getSelectedNode() != null) {
                                if (getTestDynamicTree().getSelectedNode()
                                    .getUserObject() instanceof Test) {
                                    DataModel
                                        .initAttachmentTable(((Test) getTestDynamicTree()
                                                              .getSelectedNode()
                                                              .getUserObject())
                                                             .getAttachmentMapFromModel()
                                                             .values());
                                    DataModel
                                        .setCurrentTest((Test) getTestDynamicTree()
                                                        .getSelectedNode()
                                                        .getUserObject());
                                } else if (getTestDynamicTree().getSelectedNode()
                                           .getUserObject() instanceof TestList) {
                                    DataModel
                                        .initAttachmentTable(((TestList) getTestDynamicTree()
                                                              .getSelectedNode()
                                                              .getUserObject())
                                                             .getAttachmentMapFromModel()
                                                             .values());
                                    DataModel
                                        .setCurrentTestList((TestList) getTestDynamicTree()
                                                            .getSelectedNode()
                                                            .getUserObject());
                                } else if (getTestDynamicTree().getSelectedNode()
                                           .getUserObject() instanceof Family) {
                                    DataModel
                                        .initAttachmentTable(((Family) getTestDynamicTree()
                                                              .getSelectedNode()
                                                              .getUserObject())
                                                             .getAttachmentMapFromModel()
                                                             .values());
                                    DataModel
                                        .setCurrentFamily((Family) getTestDynamicTree()
                                                          .getSelectedNode()
                                                          .getUserObject());
                                }
                            }
                        } else if (tabs.getModel().getSelectedIndex() == 1) {
                            // Onglet des campagnes
                            if (DataModel.getCurrentCampaign() != null) {
                                if (DataModel.getCurrentCampaign()
                                    .getAttachmentMapFromModel() != null) {
                                    DataModel.initAttachmentTable(DataModel
                                                                  .getCurrentCampaign()
                                                                  .getAttachmentMapFromModel().values());
                                }
                            }
                            if (getTestDynamicTree().getSelectedNode() != null
                                && getCampaignDynamicTree().getSelectedNode() != null
                                && getTestDynamicTree().getSelectedNode()
                                .getUserObject().equals(
                                                        getCampaignDynamicTree()
                                                        .getSelectedNode()
                                                        .getUserObject())) {
                                if (getTestDynamicTree().getSelectedNode()
                                    .getUserObject() instanceof Family) {
                                    setCampPanelDescription(DataConstants.FAMILY,
                                                            ((SimpleData) getTestDynamicTree()
                                                             .getSelectedNode()
                                                             .getUserObject())
                                                            .getDescriptionFromModel());
                                    DataModel
                                        .setCurrentFamily((Family) getTestDynamicTree()
                                                          .getSelectedNode()
                                                          .getUserObject());
                                } else if (getTestDynamicTree().getSelectedNode()
                                           .getUserObject() instanceof TestList) {
                                    setCampPanelDescription(DataConstants.TESTLIST,
                                                            ((SimpleData) getTestDynamicTree()
                                                             .getSelectedNode()
                                                             .getUserObject())
                                                            .getDescriptionFromModel());
                                    DataModel
                                        .setCurrentTestList((TestList) getTestDynamicTree()
                                                            .getSelectedNode()
                                                            .getUserObject());
                                } else if (getTestDynamicTree().getSelectedNode()
                                           .getUserObject() instanceof Test) {
                                    Test test = (Test) getTestDynamicTree()
                                        .getSelectedNode().getUserObject();
                                    setCampPanelDescription(DataConstants.TEST,
                                                            ((SimpleData) getTestDynamicTree()
                                                             .getSelectedNode()
                                                             .getUserObject())
                                                            .getDescriptionFromModel());
                                    SalomeTMFPanels.setCampPanelDetailsForTest(test
                                                                               .getNameFromModel(), test
                                                                               .getConceptorFromModel(), test
                                                                               .getCreationDateFromModel().toString(),
                                                                               DataModel.getCurrentCampaign()
                                                                               .getAssignedUserID(test));
                                    DataModel
                                        .setCurrentTest((Test) getTestDynamicTree()
                                                        .getSelectedNode()
                                                        .getUserObject());
                                }
                            }
                        } else if ((Api.isALLOW_PLUGINS())
                                   && (tabs.getModel().getSelectedIndex() == tabs
                                       .indexOfTab(Language.getInstance().getText(
                                                                                  "Plugins")))) {
                            pluginsView.refreshPluginsActivation();
                        }
                    }
                });

            DataModel.initData();
            pTestPanel.initCoponent();
            pCampainPanel.initCoponent();
            pDataPanel.initCoponent();
        } catch (Exception E) {
            E.printStackTrace();
        }
    }

    /******************************************************************************/
    /** METHODE ***/
    /******************************************************************************/

    public static void reValidateTestPanel() {
        pTestPanel.reValidate();
    }

    public static void reValidateCampainPanel() {
        pCampainPanel.reValidate();
    }

    public static void setCursor(int tabsPanel, int cursor) {
        if (tabsPanel == DataConstants.CAMPAIGN) {
            pCampainPanel.setCursor(new Cursor(cursor));
        } else if (tabsPanel == DataConstants.TEST) {
            pTestPanel.setCursor(new Cursor(cursor));
        } else if (tabsPanel == DataConstants.PROJECT) {
            pDataPanel.setCursor(new Cursor(cursor));
        }
    }

    public static void setTestPanelWorkSpace(int type) {
        pTestPanel.setWorkSpace(type);
    }

    public static void setTestPanelDescription(int type, String text) {
        pTestPanel.setDescription(type, text);
    }

    public static void setTestPanelTestInfo(int type, String name,
                                            String conceptor, String date) {
        pTestPanel.setTestInfo(type, name, conceptor, date);
    }

    public static void setTestPanelTestExecutedInfo(int type, String execcuted) {
        pTestPanel.TestExecutedInfo(type, execcuted);
    }

    public static void setCampPanelWorkSpace(int type) {
        pCampainPanel.setWorkSpace(type);
    }

    public static void setCampPanelDescription(int type, String text) {
        pCampainPanel.setDescription(type, text);
    }

    public static void setCampPanelDetailsForTest(String name,
                                                  String conceptor, String date, int assigned_to_ID) {
        pCampainPanel.setTestInfoForTestPanel(name, conceptor, date,
                                              assigned_to_ID);
    }

    public static void setCampPanelInfo(String name, String conceptor,
                                        String date) {
        pCampainPanel.setTestInfo(name, conceptor, date);
    }

    /******************************************************************************/
    /** ACCESSEURS ***/
    /******************************************************************************/

    public static DynamicTree getTestDynamicTree() {
        return pTestPanel.testDynamicTree;
    }

    public static JMenu getTestToolsMenu() {
        return pTestPanel.testToolsMenu;
    }

    public static JMenuItem getDelTestOrTestList() {
        return pTestPanel.supprTest;
    }

    public static JButton getAutomaticButtonCampaignDetails() {
        return pTestPanel.automaticButtonCampaignDetails;
    }

    public static JButton getManualTestButtonCampaignDetails() {
        return pTestPanel.manualButtonCampaignDetails;
    }

    public static JMenuItem getRenameTestButton() {
        return pTestPanel.renameTest;
    }

    public static DynamicTree getCampaignDynamicTree() {
        return pCampainPanel.campaignDynamicTree;
    }

    public static JButton getRenameCampaignButton() {
        return pCampainPanel.renameCampaignButton;
    }

    public static JButton getOrderCampagne() {
        return pCampainPanel.orderCampagne;
    }

    public static JButton getDelCampagne() {
        return pCampainPanel.delCampagne;
    }

    public static JButton getAddTestInCampagne() {
        return pCampainPanel.addTestInCampagne;
    }

    public static JMenu getCampToolsMenu() {
        return pCampainPanel.campToolsMenu;
    }

    public static void clearTrees() {
        getTestDynamicTree().clear();
        getCampaignDynamicTree().clear();
    }

    private void giveAccessToIhm() {
        pTestPanel.giveAccessToIhm();
        pCampainPanel.giveAccessToIhm();
    }

    public static JMenu getDataToolsMenu() {
        return pDataPanel.dataToolsMenu;
    }

    public static EnvironmentView getEnvironnementView() {
        return pDataPanel.pEnvView;
    }

    /*********************************************** MODEL ****************************************************/
    public void loadModel(String strProject, String strLogin) {
        // //Api.addWatchListener(this,
        // DataModel.getCurrentProject().getNameFromModel());
        giveAccessToIhm();
        ManualActionView.giveAccessToIhmManualActionView();
        EnvironmentView.giveAccessToIhmEnvironmentView();
        DataSetView.giveAccessToIhmDataSetView();
        ExecutionView.giveAccessToIhmExecutionView();
        AutomaticTestScriptView.giveAccessToIhmScriptView();
        pTestPanel.loadModel(strProject, strLogin);
        pCampainPanel.loadModel(strProject, strLogin);
        pDataPanel.loadModel(strProject, strLogin);
    }

    void reloadModel() {
        giveAccessToIhm();
        ManualActionView.giveAccessToIhmManualActionView();
        EnvironmentView.giveAccessToIhmEnvironmentView();
        DataSetView.giveAccessToIhmDataSetView();
        ExecutionView.giveAccessToIhmExecutionView();
        AutomaticTestScriptView.giveAccessToIhmScriptView();
        pTestPanel.reloadModel();
        pCampainPanel.reloadModel();
        pDataPanel.reloadModel();
    }

    public static void resetTreeSelection() {
        getTestDynamicTree().resetToRoot();
        getCampaignDynamicTree().resetToRoot();
    }

    /********************************** Observer Implementation **********************************************/

    /*
     * public void update(Observable observable, Object obj) {
     * Util.log("[SalomeTMF] " +
     * Language.getInstance().getText("On_entre_dans_update_observer"));
     *
     * if (obj instanceof Integer) { int code = ((Integer)obj).intValue();
     * Util.log("Valeur de Integer : " + code); if (code >= 100 && code < 105) {
     * testMultiUserChangeListenerPanel.addDelete(); } else if (code >= 105 &&
     * code < 110) { campagneMultiUserChangeListenerPanel.addDelete(); } else if
     * (code >= 110 && code < 112) {
     * dataMultiUserChangeListenerPanel.addDelete(); } else if (code >= 200 &&
     * code < 205) { testMultiUserChangeListenerPanel.addUInsert(); } else if
     * (code >= 205 && code < 211) {
     * campagneMultiUserChangeListenerPanel.addUInsert(); } else if (code >= 211
     * && code < 214) { dataMultiUserChangeListenerPanel.addUInsert(); } else if
     * ( code >= 300 && code < 304) {
     * testMultiUserChangeListenerPanel.addUpdate(); } else if ( code >= 304 &&
     * code < 307) { campagneMultiUserChangeListenerPanel.addUpdate(); } else if
     * (code >= 307 && code < 309) {
     * dataMultiUserChangeListenerPanel.addUpdate(); } //
     * DataModel.addModification(obj); } }
     */

    private void createPluginsPlane() {

        Vector commonExtensions = new Vector();
        Vector testDriverExtensions = new Vector();
        Vector scriptEngineExtensions = new Vector();
        Vector reqMgrExtensions = new Vector();
        Vector statistiquesExtensions = new Vector();

        // Extensions de type "TestDriver"
        for (Iterator it = pSalomeTMFContext.testDriverExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension testDriverExt = (Extension) it.next();
            testDriverExtensions.add(testDriverExt);
            // associatedTestDriver(testDriverExt.getId(), testDriverExt);
        }

        // Extensions de type "ScriptEngine"
        for (Iterator it = pSalomeTMFContext.scriptEngineExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension scriptEngineExt = (Extension) it.next();
            scriptEngineExtensions.add(scriptEngineExt);
            // associatedScriptEngine(scriptEngineExt.getId(), scriptEngineExt);
        }

        // Extensions "Common"
        for (Iterator it = pSalomeTMFContext.CommonExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension commonExt = (Extension) it.next();
            commonExtensions.add(commonExt);
            pSalomeTMFContext.associatedExtension(commonExt.getId(), commonExt);
        }

        // "BugTracker" extensions
        for (Iterator it = pSalomeTMFContext.bugTrackerExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension bTrackExt = (Extension) it.next();
            pSalomeTMFContext.associatedBugTrackerExtension(bTrackExt.getId(),
                                                            bTrackExt);
        }

        // "Statistiques" extensions
        for (Iterator it = pSalomeTMFContext.statistiquesExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension statExt = (Extension) it.next();
            // pSalomeTMFContext.associatedStatistiquesExtension(statExt.getId(),
            // statExt);
            statistiquesExtensions.add(statExt);
        }

        // "ReqManager" extensions
        for (Iterator it = pSalomeTMFContext.reqManagerExtPoint
                 .getConnectedExtensions().iterator(); it.hasNext();) {
            Extension reqMgrExt = (Extension) it.next();
            reqMgrExtensions.add(reqMgrExt);
        }

        // pluginsView = new PluginsView(commonExtensions,testDriverExtensions,
        // scriptEngineExtensions,pSalomeTMFContext.getAllTracker(),reqMgrExtensions,pSalomeTMFContext.pluginManager);
        pluginsView = new PluginsView(commonExtensions, testDriverExtensions,
                                      scriptEngineExtensions, pSalomeTMFContext.getAllTracker(),
                                      statistiquesExtensions, reqMgrExtensions,
                                      pSalomeTMFContext.pluginManager);

        // plugins.add(pluginsView);
    }

    void loadPlugin() {
        if (Api.isALLOW_PLUGINS()) {
            createPluginsPlane();
            tabs.addTab(Language.getInstance().getText("Plugins"), pluginsView);
            addPluginsInStaticUIComps();

            createIndicatorsPlane();
        }
    }

    public void addPluginsInStaticUIComps() {
        // Activate bugTrackers in environment view
        getEnvironnementView().activateBugTrackersMenu();

        // ICAL calculation
        /*
         * if (Api.isWith_ICAL()) { new ICAL(pSalomeTMFContext,
         * pTestPanel.testToolsMenu); }
         */
    }

    private void createIndicatorsPlane() {
        Vector bugTrackers = SalomeTMFContext.getInstance().getBugTracker();
        int i = 0;
        boolean found = false;
        if (bugTrackers != null && bugTrackers.size() != 0) {
            while ((i < bugTrackers.size()) && (!found)) {
                BugTracker bugTracker = (BugTracker) bugTrackers.elementAt(i);
                if (bugTracker.isSuportIndicators()) {
                    // if ((bugTracker.getBugTrackerName()).equals("Mantis")) {
                    indicatorTracker = bugTracker;
                    found = true;
                }
                i = i + 1;
            }
        }
        if ((indicatorTracker != null) && (Api.isWith_ICAL())) {
            Vector reqManagers = SalomeTMFContext.getInstance()
                .getReqManagers();
            int j = 0;
            boolean trouve = false;
            if (reqManagers != null && reqManagers.size() != 0) {
                while ((j < reqManagers.size()) && (!trouve)) {
                    ReqManager req = (ReqManager) reqManagers.elementAt(j);
                    if ((req.getReqManagerName()).equals("SALOME_REQ_PLUG")) {
                        reqPlug = req;
                        trouve = true;
                    }
                    j = j + 1;
                }
            }
            pIndicatorsPanel = new IndicatorsPanel(indicatorTracker, reqPlug);
            tabs.addTab(Language.getInstance().getText("Indicateurs"),
                        pIndicatorsPanel);
            pIndicatorsPanel.initComponents();
        }
    }

    public JTabbedPane getTabs() {
        return tabs;
    }

    public void setTabs(JTabbedPane tabs) {
        this.tabs = tabs;
    }

}
