/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.databaseSQL.AttachException;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.admin.AskName;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.BugTracker;



public class AttachmentView extends JPanel implements DataConstants, ActionListener, IHMConstants , ListSelectionListener, FocusListener {

    boolean readOnly = false;

    JButton openAttach;

    JButton delAttach;

    JButton modifyButton;

    JButton addFileAttach;

    JButton addUrlAttach;

    JButton saveAttachButton;

    JButton screeshootButton;

    JTable attachmentTable;

    /**
     *
     */
    int sourceType;

    /**
     * Table des attachements
     */
    //static HashMap attachmentMap;

    /**
     * Description de l'attachement
     */
    JTextArea descriptionArea;

    /**
     * Applet
     */
    BaseIHM applet;

    //WithAttachment actionOrExecutionForModification;

    WithAttachment withAttachment = null;

    String typeName ="";
    Attachment previousAtt ;

    //Execution exec;

    //ExecutionResult execResult;

    //Test observedTest;

    TableSorter  sorter;

    JPanel attachmentsPanel;

    static Hashtable extPrg = new Hashtable();

    JDialog parent;

    private boolean isFromWindow = false;

    public AttachmentView(BaseIHM app, int realtype, int type, WithAttachment pWithAttachment, int size, JDialog _parent, boolean isFromWindow){
        this(app, realtype, type, pWithAttachment, size, _parent);
        this.isFromWindow = isFromWindow;
    }
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    public AttachmentView(BaseIHM app, int realtype, int type, WithAttachment pWithAttachment, int size, JDialog _parent){
        //public AttachmentView(JApplet app, int realtype, int type, WithAttachment actionOrExecution, int size, Execution execution, ExecutionResult executionResult, Test testInObservation){
        previousAtt = null;
        parent = _parent;
        attachmentsPanel = new JPanel();
        attachmentTable = new JTable();

        descriptionArea = new JTextArea();
        modifyButton = new JButton(Language.getInstance().getText("Actualiser"));
        delAttach = new JButton(Language.getInstance().getText("Supprimer"));
        openAttach = new JButton(Language.getInstance().getText("Visualiser"));

        openAttach.setToolTipText(Language.getInstance().getText("Recuperer_et_visualiser_un_attachement"));
        //openAttach.setIcon(Tools.createAppletImageIcon(PATH_TO_VIEW_ATTACH_ICON,""));
        openAttach.setEnabled(false);
        openAttach.addActionListener(this);

        addFileAttach = new JButton(Language.getInstance().getText("Ajouter_fichier"));
        //addFileAttach.setIcon(Tools.createAppletImageIcon(PATH_TO_FILE_ATTACH_ICON,""));
        addFileAttach.setToolTipText(Language.getInstance().getText("Ajouter_un_fichier_en_attachement"));
        addFileAttach.addActionListener(this);

        addUrlAttach = new JButton(Language.getInstance().getText("Ajouter_url"));
        //addUrlAttach.setIcon(Tools.createAppletImageIcon(PATH_TO_URL_ATTACH_ICON,""));
        addUrlAttach.setToolTipText(Language.getInstance().getText("Ajouter_une_URL_en_attachement"));
        addUrlAttach.addActionListener(this);

        modifyButton.setToolTipText(Language.getInstance().getText("Enregistre_les_modifications_du_fichier"));
        modifyButton.setEnabled(false);
        //modifyButton.setIcon(Tools.createAppletImageIcon(PATH_TO_BDD_ATTACH_ICON,""));
        modifyButton.addActionListener(this);


        delAttach.setToolTipText(Language.getInstance().getText("Supprimer_un_attachement"));
        //delAttach.setIcon(Tools.createAppletImageIcon(PATH_TO_FAIL2_ICON,""));
        delAttach.setEnabled(false);
        delAttach.addActionListener(this);

        saveAttachButton = new JButton(Language.getInstance().getText("Enregistrer_sous"));
        //delAttach.setIcon(Tools.createAppletImageIcon(PATH_TO_FAIL2_ICON,""));
        saveAttachButton.setEnabled(false);
        saveAttachButton.addActionListener(this);

        screeshootButton = new JButton();
        screeshootButton.setIcon(Tools.createAppletImageIcon(PATH_TO_SCREENSHOT_ICON,""));
        screeshootButton.addActionListener(this);
        if (type == EXECUTION_RESULT_TEST){
            screeshootButton.setEnabled(false);
        }

        JPanel allButtons = new JPanel(new GridLayout(1,7));
        allButtons.add(addFileAttach);
        allButtons.add(addUrlAttach);
        allButtons.add(screeshootButton);
        allButtons.add(openAttach);
        allButtons.add(saveAttachButton);
        allButtons.add(modifyButton);
        allButtons.add(delAttach);

        if (type == EXECUTION_RESULT_TEST) {
            // Menu for bugtrackers
            //Vector bugTrackers = SalomeTMFContext.getInstance().bugTrackerExtensions;
            Vector bugTrackers = SalomeTMFContext.getInstance().getBugTracker();
            if (bugTrackers.size() != 0) {
                JMenu bugTrackMenu = new JMenu(Language.getInstance().getText("add_bug"));
                for (int i=0 ; i<bugTrackers.size() ; i++) {
                    //final BugTracker bugTracker = (BugTracker)SalomeTMFContext.getInstance().bugTrackers.elementAt(i);
                    final BugTracker bugTracker = (BugTracker)bugTrackers.elementAt(i);
                    JMenuItem bugTrackerItem = new JMenuItem(bugTracker.getBugTrackerName());
                    bugTrackerItem.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Test testInExec = PluginsTools.getTestForCurrentTestExecResult();
                                if (testInExec instanceof ManualTest) {
                                    JTextPane description = (JTextPane)SalomeTMFContext.getInstance().getUIComponent(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_DESC);
                                    JTextPane awaitedResult = (JTextPane)SalomeTMFContext.getInstance().getUIComponent(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_WAITED_RES);
                                    JTextPane effectiveResult = (JTextPane)SalomeTMFContext.getInstance().getUIComponent(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTION_EFF_RES);
                                    JTable actionsTable = (JTable)SalomeTMFContext.getInstance().getUIComponent(UICompCst.TEST_EXECUTION_RESULT_DETAILS_ACTIONS_TABLE);
                                    if ((description != null)&&(awaitedResult != null)&&(effectiveResult != null)&&(actionsTable != null)) {
                                        int selectedRowIndex = actionsTable.getSelectedRow();
                                        ArrayList dataList = ((MyTableModel)actionsTable.getModel()).getData(selectedRowIndex);
                                        ManualTest pTest = (ManualTest) testInExec;
                                        Action action = pTest.getActionFromModel((String)dataList.get(0));
                                        if (action != null){
                                            new AskNewBug(SalomeTMFContext.getInstance().getSalomeFrame(), bugTracker,true, action.getNameFromModel(),description.getText(),awaitedResult.getText(),effectiveResult.getText());
                                        } else {
                                            new AskNewBug(SalomeTMFContext.getInstance().getSalomeFrame(), bugTracker,true,null,null,null,null);
                                        }
                                    } else {
                                        new AskNewBug(SalomeTMFContext.getInstance().getSalomeFrame(), bugTracker,true,null,null,null,null);
                                    }
                                } else {
                                    new AskNewBug(SalomeTMFContext.getInstance().getSalomeFrame(), bugTracker,true,null,null,null,null);
                                }
                            }
                        });

                    if (!bugTracker.isUserExistsInBugDB()) {
                        bugTrackerItem.setEnabled(false);
                    }
                    bugTrackMenu.add(bugTrackerItem);
                }
                JMenuBar menuBar = new JMenuBar();
                menuBar.add(bugTrackMenu);
                allButtons.add(menuBar);
            }
        }

        allButtons.setBorder(BorderFactory.createRaisedBevelBorder());

        //  La liste des attachements
        sorter = new TableSorter(DataModel.getAttachmentTableModel());
        attachmentTable.setModel(sorter);
        sorter.setTableHeader(attachmentTable.getTableHeader());

        attachmentTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        JScrollPane tablePane = new JScrollPane(attachmentTable);
        tablePane.setPreferredSize(new Dimension(500,size));

        ListSelectionModel rowSM = attachmentTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        descriptionArea.setEditable(false);
        descriptionArea.addCaretListener(new AttachmentDescriptionListener());
        descriptionArea.addFocusListener(this);
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        descriptionScrollPane.setPreferredSize(new Dimension(500,75));

        //initData(type, app, actionOrExecution, execution, executionResult, testInObservation);
        initData(type, app, pWithAttachment);

        giveAccessToIhmScriptView();
        attachmentsPanel.setLayout(new BorderLayout());
        attachmentsPanel.add(allButtons, BorderLayout.NORTH);
        attachmentsPanel.add(tablePane, BorderLayout.CENTER);
        attachmentsPanel.add(descriptionScrollPane, BorderLayout.SOUTH);

        this.setLayout(new BorderLayout());
        this.add(attachmentsPanel, BorderLayout.CENTER);

        // Mapping GUI Object for plugins//
        mapUIComponents(attachmentsPanel, attachmentTable, allButtons, realtype);

    }

    void mapUIComponents(JPanel attachmentsPanel, JTable attachmentTable, JPanel allButtons, int type){
        switch (type) {
        case MANUAL_TEST : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.MANUAL_TEST_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.MANUAL_TEST_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_ATTACHMENTS_BUTTONS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.MANUAL_TEST_ATTACHMENTS_BUTTONS_PANEL);

            break;
        }
        case AUTOMATIC_TEST : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.AUTOMATED_TEST_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.AUTOMATED_TEST_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_ATTACHMENTS_BUTTONS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.AUTOMATED_TEST_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        case TESTLIST : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.TESTLIST_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.TESTLIST_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.TESTLIST_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.TESTLIST_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.TESTLIST_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.TESTLIST_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.TESTLIST_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.TESTLIST_ATTACHMENTS_BUTTONS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.TESTLIST_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        case CAMPAIGN : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.CAMP_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.CAMP_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.CAMP_ATTACHMENTS_BUTTONS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        case FAMILY : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.FAMILY_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.FAMILY_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.FAMILY_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.FAMILY_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.FAMILY_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.FAMILY_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        case ACTION : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ACTION_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.ACTION_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ACTION_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.ACTION_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ACTION_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.ACTION_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        case PROJECT : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PROJECT_ATTACHMENTS_PANEL,attachmentsPanel);
            UICompCst.staticUIComps.add(UICompCst.PROJECT_ATTACHMENTS_PANEL);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PROJECT_ATTACHMENTS_TABLE,attachmentTable);
            UICompCst.staticUIComps.add(UICompCst.PROJECT_ATTACHMENTS_TABLE);
            //PluginsTools.activateAssociatedPlgs(UICompCst.CAMP_ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PROJECT_ATTACHMENTS_BUTTONS_PANEL,allButtons);
            UICompCst.staticUIComps.add(UICompCst.PROJECT_ATTACHMENTS_BUTTONS_PANEL);
            break;
        }
        default: {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ATTACHMENTS_PANEL,attachmentsPanel);
            PluginsTools.activateAssociatedPlgs(UICompCst.ATTACHMENTS_PANEL);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ATTACHMENTS_TABLE,attachmentTable);
            PluginsTools.activateAssociatedPlgs(UICompCst.ATTACHMENTS_TABLE);

            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ATTACHMENTS_BUTTONS_PANEL,allButtons);
            PluginsTools.activateAssociatedPlgs(UICompCst.ATTACHMENTS_BUTTONS_PANEL);
        }
        }
    }
    /**
     *
     * @param type
     * @param app
     * @param actionOrExecution
     */
    //public void initData(int type, JApplet app, WithAttachment actionOrExecution, Execution execution, ExecutionResult executionResult, Test testInObservation) {
    public void initData(int type, BaseIHM app, WithAttachment pWithAttachment) {

        sourceType = type;

        if (app != null) {
            applet = app;
        } else {
            applet = DataModel.getApplet();
        }


        DataModel.getAttachmentTableModel().clearTable();
        initAttachmentObject(type);
        loadAttachment();

    }

    public void setAttachmentObject(WithAttachment _withAttachment){
        withAttachment = _withAttachment;
        loadAttachment();
    }

    void loadAttachment(){
        if (withAttachment != null){
            DataModel.getAttachmentTableModel().clearTable();
            Collection keysSet = withAttachment.getAttachmentMapFromModel().values();
            boolean isSorting  = sorter.isSorting();
            int statusSort_col0 = -1;
            int statusSort_col1 = -1;
            int statusSort_col2 = -1;
            if (isSorting){
                statusSort_col0 = sorter.getSortingStatus(0);
                statusSort_col1 = sorter.getSortingStatus(1);
                statusSort_col2 = sorter.getSortingStatus(2);
            }
            int i = 0;
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                Attachment attach = (Attachment)iter.next();
                if (attach instanceof FileAttachment) {
                    DataModel.getAttachmentTableModel().addValueAt(((FileAttachment)attach).getNameFromModel(), i, 0);
                    DataModel.getAttachmentTableModel().addValueAt(((FileAttachment)attach).getSize().toString(), i, 1);
                    DataModel.getAttachmentTableModel().addValueAt(((FileAttachment)attach).getDate().toString(), i, 2);
                } else {
                    DataModel.getAttachmentTableModel().addValueAt(((UrlAttachment)attach).getNameFromModel(), i, 0);
                    DataModel.getAttachmentTableModel().addValueAt("", i, 1);
                    DataModel.getAttachmentTableModel().addValueAt("", i, 2);
                }
                i ++;
            }
            if (isSorting){
                sorter.setSortingStatus(0, statusSort_col0);
                sorter.setSortingStatus(1, statusSort_col1);
                sorter.setSortingStatus(2, statusSort_col2);
            }

        }
    }

    void initAttachmentObject(int type){
        if (type == CAMPAIGN) {
            withAttachment = DataModel.getCurrentCampaign();
            typeName = Language.getInstance().getText("cette_campagne");
        } else if (type == TESTLIST) {
            withAttachment = DataModel.getCurrentTestList();
            typeName = Language.getInstance().getText("cette_suite_de_tests");
        } else if (type == TEST) {
            withAttachment = DataModel.getCurrentTest();
            typeName = Language.getInstance().getText("ce_test");
        } else if (type == ACTION) {
            withAttachment = DataModel.getCurrentAction();
            typeName = Language.getInstance().getText("cette_action");
        } else if (type == EXECUTION) {
            withAttachment = DataModel.getObservedExecution();
            typeName = Language.getInstance().getText("cette_execution");
        } else if (type == EXECUTION_RESULT) {
            withAttachment = DataModel.getObservedExecutionResult();
            typeName = Language.getInstance().getText("ce_resultat_d_execution");
        } else if (type == EXECUTION_RESULT_TEST) {
            withAttachment = DataModel.getCurrentExecutionTestResult();
            typeName = Language.getInstance().getText("ce_resultat_d_execution_de_test");
        } else if (type ==  ENVIRONMENT ){
            withAttachment = DataModel.getCurrentEnvironment();
            typeName = Language.getInstance().getText("cet_environnement_sous_test");
        } else if (type ==  FAMILY ){
            withAttachment = DataModel.getCurrentFamily();
            typeName = Language.getInstance().getText("cette_famille");
        } else if (type ==  PROJECT ){
            withAttachment = DataModel.getCurrentProject();
            typeName = Language.getInstance().getText("ce_projet");
        }
        Util.log("[AttachmentView->initAttachmentObject] for " + typeName + " = " +withAttachment );
    }

    /*public static HashMap getAttachmentMap() {
      return attachmentMap;
      }*/

    public class AttachmentDescriptionListener implements CaretListener, IHMConstants {

        /* (non-Javadoc)
         * @see javax.swing.event.CaretListener#caretUpdate(javax.swing.event.CaretEvent)
         */
        @Override
        public void caretUpdate(CaretEvent e) {
            int[] selectedRows = attachmentTable.getSelectedRows();
            Attachment at = null;
            if (selectedRows.length == 1 && ((JTextArea)e.getSource()).isEditable()) {

                int selectedRow = selectedRows[0];
                if (selectedRow != -1) {
                    initAttachmentObject(sourceType);
                    at = withAttachment.getAttachmentFromModel((String)sorter.getValueAt(selectedRow,0));
                    previousAtt = at;


                    try {
                        if (at != null) {
                            at.updateDescriptionInModel(((JTextArea)e.getSource()).getText());
                        } else {
                            getAttachByName((String)sorter.getValueAt(selectedRow, 0)).updateDescriptionInModel(((JTextArea)e.getSource()).getText());
                            //((Attachment)attachmentMap.get(sorter.getValueAt(selectedRow, 0))).updateDescriptionInModel(((JTextArea)e.getSource()).getText());
                        }

                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                }
            }
        }

    }

    /*private Attachment getAttachment(String attachName) {
      return (Attachment)attachmentMap.get(attachName);
      }*/

    public void giveAccessToIhmScriptView() {
        if (sourceType == PROJECT || sourceType == FAMILY || sourceType == TESTLIST || sourceType == TEST || sourceType == ACTION ){
            allowTest();
        } else if (sourceType == CAMPAIGN || sourceType == EXECUTION ){
            allowCampaign();
        } else if (sourceType == EXECUTION_RESULT || sourceType == EXECUTION_RESULT_TEST ){
            allowCampaignExec();
        }
    }

    private void allowTest() {
        if (!Permission.canDeleteTest()) {
            delAttach.setEnabled(false);
        }
        if (!Permission.canCreateTest()) {
            addFileAttach.setEnabled(false);
            screeshootButton.setEnabled(false);
            addUrlAttach.setEnabled(false);
        } else {
            addFileAttach.setEnabled(true);
            screeshootButton.setEnabled(true);
            addUrlAttach.setEnabled(true);
        }
        if (!Permission.canUpdateTest()) {
            modifyButton.setEnabled(false);
            descriptionArea.setEditable(false);
        }
    }

    private void allowCampaign() {
        if (!Permission.canDeleteCamp()) {
            delAttach.setEnabled(false);
        }
        if (!Permission.canCreateCamp()) {
            addFileAttach.setEnabled(false);
            screeshootButton.setEnabled(false);
            addUrlAttach.setEnabled(false);
        } else {
            addFileAttach.setEnabled(true);
            screeshootButton.setEnabled(true);
            addUrlAttach.setEnabled(true);
        }
        if (!Permission.canUpdateCamp()) {
            modifyButton.setEnabled(false);
            descriptionArea.setEditable(false);
        }

    }

    private void allowCampaignExec() {
        if (!Permission.canExecutCamp()) {
            delAttach.setEnabled(false);
            addFileAttach.setEnabled(false);
            screeshootButton.setEnabled(false);
            addUrlAttach.setEnabled(false);
            modifyButton.setEnabled(false);
            descriptionArea.setEditable(false);
        }

    }

    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Object source = e.getSource();
        if (source.equals(openAttach)){
            openPerformed(e);
        } else if (source.equals(addFileAttach)){
            addFileAttachPerformed(e);
        }  else if (source.equals(addUrlAttach)){
            addUrlAttachPerformed(e);
        }  else if (source.equals(modifyButton)){
            modifyButtonPerformed(e);
        } else if (source.equals(delAttach)){
            delAttachPerformed(e);
        } else if (source.equals(saveAttachButton)){
            saveAttachPerformed(e);
        } else if (source.equals(screeshootButton)){
            //screeshootPerformed(e);
            screeshootFrame(e);
        }

    }

    JDialog takeScreenShoot;
    //20100106 - D\ufffdbut modification Forge ORTF v1.0.0
    boolean screenShootValided = false;
    //20100106 - Fin modification Forge ORTF v1.0.0

    public void screeshootFrame(java.awt.event.ActionEvent e){
        screenShootValided = false;
        takeScreenShoot = new JDialog();
        takeScreenShoot.setResizable(false);
        takeScreenShoot.setTitle("Capture Screen");
        JButton screeshootButton2 = new JButton();
        ImageIcon icon = Tools.createAppletImageIcon(PATH_TO_SCREENSHOT_ICON,"");
        screeshootButton2.setIcon(icon);
        screeshootButton2.setMinimumSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
        //takeScreenShoot.setSize(new Dimension(icon.getIconWidth(),icon.getIconHeight()));
        screeshootButton2.addActionListener(new ActionListener () {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent e){
                    screenShootValided = true;
                    takeScreenShoot.dispose();
                    screeshootPerformed(e);
                    if (parent != null){
                        parent.setVisible(true);
                    }
                }
            });
        takeScreenShoot.addWindowListener(new WindowListener () {
                @Override
                public void windowActivated(WindowEvent e) {}
                @Override
                public void windowClosed(WindowEvent e) {

                }
                @Override
                public void windowClosing(WindowEvent e) {
                    if (!screenShootValided){
                        screenShootValided = true;
                        takeScreenShoot.dispose();
                        if (parent != null){
                            parent.setVisible(true);
                        }
                    }
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                    takeScreenShoot.toFront();
                }
                @Override
                public void windowDeiconified(WindowEvent e) {}
                @Override
                public void windowIconified(WindowEvent e) {
                    takeScreenShoot.toFront();
                }
                @Override
                public void windowOpened(WindowEvent e) {}

            });

        if (parent != null){
            parent.setVisible(false);
        }
        takeScreenShoot.getContentPane().add(screeshootButton2);
        takeScreenShoot.pack();
        takeScreenShoot.setVisible(true);


    }

    public void saveAttachPerformed(ActionEvent e){
        int selectedRow = attachmentTable.getSelectedRow();
        if (selectedRow != -1) {
            FileAttachment at = null;initAttachmentObject(sourceType);
            File file = null;
            String name;
            Attachment pAttch = withAttachment.getAttachmentFromModel((String)sorter.getValueAt(selectedRow,0));
            if (pAttch != null){
                if (pAttch instanceof FileAttachment){
                    name = pAttch.getNameFromModel();
                    JFileChooser chooser = new JFileChooser(Language.getInstance().getText("Enregistrer_sous"));
                    chooser.setSelectedFile(new File(name));
                    int returnVal = chooser.showSaveDialog(SalomeTMFContext.getInstance().ptrFrame);
                    if(returnVal == JFileChooser.APPROVE_OPTION) {
                        try {
                            File f = chooser.getSelectedFile();
                            if (pAttch.isInBase()){
                                ((FileAttachment) pAttch).getFileFromDBIn(f);
                            } else {
                                file = ((FileAttachment)pAttch).getLocalFile();
                                f.createNewFile();

                                // Flux d'ecriture sur le fichier
                                FileOutputStream fos = new FileOutputStream(f);
                                BufferedOutputStream bos = new BufferedOutputStream(fos);

                                byte[] content = new byte[(int)file.length()];
                                FileInputStream fis = new FileInputStream(file);
                                fis.read(content);
                                // Flux du fichier stocke dans la BdD
                                bos.write(content);

                                //      Fermeture des flux de donnees
                                bos.flush();
                            }
                        } catch(Exception ex){
                            Tools.ihmExceptionView(ex);
                        }
                    }
                }
            }
        }
    }

    public void openPerformed(ActionEvent e) {
        int selectedRow = attachmentTable.getSelectedRow();
        if (selectedRow != -1) {
            //URL recup = null;
            //File file = null;
            //FileAttachment at = null;
            //String fileName = null;
            initAttachmentObject(sourceType);
            Attachment pAttch = withAttachment.getAttachmentFromModel((String)sorter.getValueAt(selectedRow,0));
            if (pAttch != null){
                BugTracker pBugTracker = SalomeTMFContext.getInstance().getBugTrackerFromAttachment(pAttch);
                if (pBugTracker != null){
                    pBugTracker.showBug(pAttch);
                }else {
                    viewAttachement(pAttch);
                }
            } else {
                JOptionPane.showMessageDialog(AttachmentView.this,
                                              Language.getInstance().getText("Vous_devez_valider_l_ajout_des_attachements_avant_de_pouvoir_les_visualiser"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
            /*if (pAttch != null){
              BugTracker pBugTracker = SalomeTMFContext.getInstance().getBugTrackerFromAttachment(pAttch);
              if (pBugTracker != null){
              pBugTracker.showBug(pAttch);
              }
              try {
              if (pAttch instanceof FileAttachment){
              if (pAttch.isInBase()){
              file = ((FileAttachment)pAttch).getFileFromDB();
              } else {
              file = ((FileAttachment)pAttch).getLocalFile();
              }
              fileName = file.getAbsolutePath();
              } else {
              recup = new URL((String)sorter.getValueAt(selectedRow, 0));
              }
              }catch (Exception e1){
              Tools.ihmExceptionView(e1);
              }
              } else {
              JOptionPane.showMessageDialog(AttachmentView.this,
              Language.getInstance().getText("Vous_devez_valider_l_ajout_des_attachements_avant_de_pouvoir_les_visualiser"),
              Language.getInstance().getText("Erreur_"),
              JOptionPane.ERROR_MESSAGE);
              }


              Properties sys = System.getProperties();
              String fileSeparator = sys.getProperty("file.separator");
              Runtime r = Runtime.getRuntime();
              String executable = "cmd /c call \"";
              String ext = null;
              try       {
              if (!fileSeparator.equals("\\") && DataModel.isBadDirectoryView() && recup == null) {
              //new BadDirectoryView();
              ext = fileName.substring(fileName.lastIndexOf("."), fileName.length());
              //Util.debug("file extention : " + ext);
              executable = (String) extPrg.get(ext);
              if (executable == null){
              int n = JOptionPane.showConfirmDialog(
              SalomeTMFContext.getInstance().ptrFrame,
              Language.getInstance().getText("choice_exec_file") +"?",
              Language.getInstance().getText("Mime_type_not_detected"),
              JOptionPane.YES_NO_OPTION);
              if (n == JOptionPane.YES_OPTION){
              JFileChooser chooser = new JFileChooser(Language.getInstance().getText("Ouvrir_avec"));
              int returnVal = chooser.showOpenDialog(SalomeTMFContext.getInstance().ptrFrame);
              if(returnVal == JFileChooser.APPROVE_OPTION) {
              try {
              executable = chooser.getSelectedFile().getAbsolutePath();
              } catch(Exception ex){
              Tools.ihmExceptionView(ex);
              }
              } else {
              executable = null;
              }
              } else {
              new BadDirectoryView();
              }
              }
              }
              if (fileSeparator.equals("\\") && fileName != null) {

              //r.exec("cmd /c call " + "\"" + fileName + "\"");
              r.exec(executable + fileName + "\"");
              } else if (fileName != null){
              // Affiche le document apres avoir recuperer le contexte courant
              try {
              if (executable != null){
              r.exec(executable +  " " + fileName);
              if (ext != null)  {
              extPrg.put(ext, executable);
              }
              } else {
              //applet.getAppletContext().showDocument(new URL("file://" + fileName), "_blank");
              applet.showDocument(new URL("file:///" + fileName), "_blank");
              }
              } catch (Exception ex){
              //applet.getAppletContext().showDocument(new URL("file://" + fileName), "_blank");
              applet.showDocument(new URL("file:///" + fileName), "_blank");
              }
              } else if (recup != null) {

              //applet.getAppletContext().showDocument(recup, "_blank");
              applet.showDocument(recup, "_blank");
              }
              } catch (IOException ex) {
              Util.err(ex);
              }*/
        }
    }

    public static void viewAttachement(Attachment pAttch){
        URL recup = null;
        File file = null;
        String fileName = null;
        if (pAttch != null){
            try {
                if (pAttch instanceof FileAttachment){
                    if (pAttch.isInBase()){
                        file = ((FileAttachment)pAttch).getFileFromDB();
                    } else {
                        file = ((FileAttachment)pAttch).getLocalFile();
                    }
                    fileName = file.getAbsolutePath();
                } else {
                    recup = ((UrlAttachment)pAttch).getUrl();
                }
            }catch (Exception e1){
                Tools.ihmExceptionView(e1);
            }
        } else {
            return;
        }

        Properties sys = System.getProperties();
        String fileSeparator = sys.getProperty("file.separator");
        Runtime r = Runtime.getRuntime();
        String executable = "cmd /c call \"";
        String ext = null;
        try     {
            if (!fileSeparator.equals("\\") && DataModel.isBadDirectoryView() && recup == null && fileName != null) {
                //new BadDirectoryView();
                ext = fileName.substring(fileName.lastIndexOf("."), fileName.length());
                //Util.debug("file extention : " + ext);
                executable = (String) extPrg.get(ext);
                if (executable == null){
                    int n = JOptionPane.showConfirmDialog(
                                                          SalomeTMFContext.getInstance().ptrFrame,
                                                          Language.getInstance().getText("choice_exec_file") +"?",
                                                          Language.getInstance().getText("Mime_type_not_detected"),
                                                          JOptionPane.YES_NO_OPTION);
                    if (n == JOptionPane.YES_OPTION){
                        JFileChooser chooser = new JFileChooser(Language.getInstance().getText("Ouvrir_avec"));
                        int returnVal = chooser.showOpenDialog(SalomeTMFContext.getInstance().ptrFrame);
                        if(returnVal == JFileChooser.APPROVE_OPTION) {
                            try {
                                executable = chooser.getSelectedFile().getAbsolutePath();
                            } catch(Exception ex){
                                Tools.ihmExceptionView(ex);
                            }
                        } else {
                            executable = null;
                        }
                    } else {
                        new BadDirectoryView();
                    }
                }
            }
            if (fileSeparator.equals("\\") && fileName != null) {

                //r.exec("cmd /c call " + "\"" + fileName + "\"");
                r.exec(executable + fileName + "\"");
            } else if (fileName != null){
                /* Affiche le document apres avoir recuperer le contexte courant */
                try {
                    if (executable != null){
                        r.exec(executable +  " " + fileName);
                        if (ext != null)  {
                            extPrg.put(ext, executable);
                        }
                    } else {
                        //applet.getAppletContext().showDocument(new URL("file://" + fileName), "_blank");
                        //applet.showDocument(new URL("file:///" + fileName), "_blank");
                        SalomeTMFContext.getBaseIHM().showDocument(new URL("file:///" + fileName), "_blank");
                    }
                } catch (Exception ex){
                    //applet.getAppletContext().showDocument(new URL("file://" + fileName), "_blank");
                    //applet.showDocument(new URL("file:///" + fileName), "_blank");
                    SalomeTMFContext.getBaseIHM().showDocument(new URL("file:///" + fileName), "_blank");
                }
            } else if (recup != null) {

                //applet.getAppletContext().showDocument(recup, "_blank");
                //applet.showDocument(recup, "_blank");
                SalomeTMFContext.getBaseIHM().showDocument(recup, "_blank");
            }
        } catch (IOException ex) {
            Util.err(ex);
        }
    }

    public void screeshootPerformed(ActionEvent e){
        boolean add = false;
        initAttachmentObject(sourceType);
        try{
            FileAttachment fileAttachment = withAttachment.takeScreenShot();
            if (sourceType == PROJECT || sourceType == FAMILY || sourceType == CAMPAIGN || sourceType == TESTLIST || sourceType == TEST || isFromWindow || sourceType == -1) {
                System.out.println("sourceType : " +sourceType);
                withAttachment.addAttachementInDBAndModel(fileAttachment);
                add = true;

            } else {
                withAttachment.addAttachementInModel(fileAttachment);
                //attachmentMap.put(fileAttachment.getNameFromModel(), fileAttachment);
                add = true;
            }
            if (add) {
                ArrayList data = new ArrayList();
                data.add(fileAttachment.getNameFromModel());
                data.add(fileAttachment.getSize().toString());
                data.add(fileAttachment.getDate().toString());
                DataModel.getAttachmentTableModel().addRow(data);
            }
        } catch (AttachException ex){
            Util.err(ex);
            JOptionPane.showMessageDialog(AttachmentView.this,
                                          ex.getMessage(),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        } catch (Exception ex1) {
            Util.err(ex1);
            JOptionPane.showMessageDialog(AttachmentView.this,
                                          Language.getInstance().getText("Le_fichier_") + " Screenshot " +Language.getInstance().getText("_est_deja_attache_a_") + typeName +" !",
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    public void addFileAttachPerformed(ActionEvent e) {
        boolean exception = false;
        boolean add = false;
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setMultiSelectionEnabled(true);
        fileChooser.setApproveButtonText(Language.getInstance().getText("Valider"));
        int returnVal = fileChooser.showOpenDialog(AttachmentView.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                //Date dateOfFile = new Date(file.lastModified());
                FileAttachment fileAttachment = new FileAttachment(file, "");
                initAttachmentObject(sourceType);
                if (withAttachment.containsAttachmentInModel(fileAttachment.getNameFromModel())){
                    JOptionPane.showMessageDialog(AttachmentView.this,
                                                  Language.getInstance().getText("Le_fichier_") + " " + fileAttachment.getNameFromModel() + " " +Language.getInstance().getText("_est_deja_attache_a_") + typeName +" !",
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if (sourceType == PROJECT || sourceType == FAMILY || sourceType == CAMPAIGN || sourceType == TESTLIST || sourceType == TEST || isFromWindow || sourceType == -1) {
                    try{
                        withAttachment.addAttachementInDBAndModel(fileAttachment);
                        add = true;
                    } catch (Exception ex1) {
                        Tools.ihmExceptionView(ex1);
                        exception = true;
                    }
                } else {
                    withAttachment.addAttachementInModel(fileAttachment);
                    //attachmentMap.put(fileAttachment.getNameFromModel(), fileAttachment);
                    add = true;
                }


                if (add) {
                    ArrayList data = new ArrayList();
                    data.add(file.getName());
                    data.add(new Long(file.length()).toString());
                    data.add(new Date(file.lastModified()).toString());
                    DataModel.getAttachmentTableModel().addRow(data);

                } else if (!exception) {
                    JOptionPane.showMessageDialog(AttachmentView.this,
                                                  Language.getInstance().getText("Le_fichier_")+ file.getAbsolutePath() +Language.getInstance().getText("_est_deja_attache_a_") + typeName +" !",
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }




    public void addUrlAttachPerformed(ActionEvent e) {

        URL url = null;
        int essai = 0;
        String lastRes = null;
        while (essai < 3) {
            try {
                AskName askName = new AskName(Language.getInstance().getText("Entrez_une_URL_"), Language.getInstance().getText("Attacher_une_URL"), Language.getInstance().getText("url"), lastRes, SalomeTMFContext.getInstance().ptrFrame);
                lastRes = askName.getResult();
                if (lastRes!= null) {
                    url = new URL(lastRes);
                }
                essai = 4;
            } catch (MalformedURLException mue) {
                JOptionPane.showMessageDialog(AttachmentView.this,
                                              Language.getInstance().getText("L_url_")+ " " + lastRes + " " +Language.getInstance().getText("_est_mal_formee_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                essai ++;
            }
        }
        if (essai < 4 || lastRes == null) {
            return;
        }
        boolean add = false;
        UrlAttachment urlAttachment = new UrlAttachment(lastRes,"");
        initAttachmentObject(sourceType);
        if (withAttachment.containsAttachmentInModel(urlAttachment.getNameFromModel())){
            JOptionPane.showMessageDialog(AttachmentView.this,
                                          Language.getInstance().getText("L_url_")+  " " + urlAttachment.getNameFromModel()+  " " +  Language.getInstance().getText("_est_deja_attachee_a_") + typeName +" !",
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }

        urlAttachment.setUrl(url);
        if (sourceType == PROJECT || sourceType == FAMILY || sourceType == CAMPAIGN || sourceType == TESTLIST || sourceType == TEST || isFromWindow || sourceType == -1) {
            try{
                withAttachment.addAttachementInDBAndModel(urlAttachment);
                add = true;
            } catch (Exception ex1) {
                Tools.ihmExceptionView(ex1);
            }
        }else {
            withAttachment.addAttachementInModel(urlAttachment);
            //attachmentMap.put(urlAttachment.getNameFromModel(), urlAttachment);
            add = true;
        }

        if (add) {
            ArrayList data = new ArrayList();
            data.add(urlAttachment.getNameFromModel());
            data.add("");
            data.add("");
            DataModel.getAttachmentTableModel().addRow(data);
        } else {
            JOptionPane.showMessageDialog(AttachmentView.this,
                                          Language.getInstance().getText("L_url_")+ urlAttachment.getNameFromModel() +Language.getInstance().getText("_est_deja_attachee_a_") + typeName +" !",
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }

    }



    public void modifyButtonPerformed(ActionEvent e) {

        int selectedRow = attachmentTable.getSelectedRow();
        Attachment at = null;
        if (selectedRow != -1) {
            initAttachmentObject(sourceType);
            at = withAttachment.getAttachmentFromModel((String)sorter.getValueAt(selectedRow,0));

            if (at == null) {
                JOptionPane.showMessageDialog(AttachmentView.this,
                                              Language.getInstance().getText("Vous_devez_valider_l_ajout_des_attachements_avant_de_pouvoir_les_modifier"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                return;
            } else {
                FileAttachment fileAttach = (FileAttachment)at;
                File file = new File(fileAttach.getLocalisation());
                if (!file.exists()){
                    return;
                }
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
                    // On recupere le flux du fichier apres modification
                    file = new File(fileAttach.getLocalisation());
                    fileAttach.updateInDBAndModel(file);

                    Api.commitTrans(transNumber);
                    transNumber = -1;

                    // Mise a jour dans l'IHM
                    sorter.setValueAt(new Long(file.length()).toString(),selectedRow,1);
                    sorter.setValueAt(new Date(file.lastModified()).toString(), selectedRow, 2);

                    JOptionPane.showMessageDialog(AttachmentView.this,
                                                  Language.getInstance().getText("Le_fichier_a_ete_correctement_archive"),
                                                  Language.getInstance().getText("Info"),
                                                  JOptionPane.INFORMATION_MESSAGE);

                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    Tools.ihmExceptionView(exception);
                }
            }
        }
    }



    public void delAttachPerformed(ActionEvent e) {
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        int choice = -1;
        int selectedRow = attachmentTable.getSelectedRow();
        int[] selectedRows = attachmentTable.getSelectedRows();
        if (selectedRows.length > 0) {
            initAttachmentObject(sourceType);
            choice = JOptionPane.showOptionDialog(AttachmentView.this,
                                                  //"Etes vous sur de vouloir supprimer l'attachement <" + (String)DataModel.getAttachmentTableModel().getValueAt(selectedRow, 0) + "> ?",
                                                  Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_l_attachement_") + (String)sorter.getValueAt(selectedRow, 0) + "> ?",
                                                  Language.getInstance().getText("Attention_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
            if (choice == JOptionPane.YES_OPTION) {
                for (int i = 0; i <  selectedRows.length ; i ++){

                    Attachment attach = withAttachment.getAttachmentFromModel((String)sorter.getValueAt(selectedRows[i], 0));
                    if (attach != null){
                        try {
                            if (sourceType == PROJECT || sourceType == FAMILY || sourceType == CAMPAIGN || sourceType == TESTLIST || sourceType == TEST || sourceType == EXECUTION_RESULT_TEST || sourceType == -1) {

                                withAttachment.deleteAttachementInDBAndModel(attach);

                            } else {
                                withAttachment.deleteAttachmentInModel(attach);

                            }
                            //DataModel.getAttachmentTableModel().removeData(sorter.modelIndex(selectedRow));
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else {

                    }
                }
                DataModel.getAttachmentTableModel().clearTable();
                loadAttachment();
            }

        }
    }


    @Override
    public void valueChanged(ListSelectionEvent e){
        if (e.getSource().equals(attachmentTable.getSelectionModel())){
            attachmentTableValueChanged(e);
        }
    }

    Attachment getAttachByName(String name){
        Attachment pAttach = withAttachment.getAttachmentFromModel(name);
        return pAttach;
    }

    public void attachmentTableValueChanged(ListSelectionEvent e) {

        if (e.getValueIsAdjusting())
            return;

        int nbOfSelectedRows = attachmentTable.getSelectedRowCount();
        int selectedRow = attachmentTable.getSelectedRow();
        if (selectedRow != -1) {
            if (nbOfSelectedRows == 1) {
                openAttach.setEnabled(true);
                initAttachmentObject(sourceType);
                if (sourceType == EXECUTION_RESULT || sourceType == EXECUTION_RESULT_TEST ) {
                    if (Permission.canExecutCamp()) {
                        descriptionArea.setEditable(true);
                    }
                    if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                        if (Permission.canExecutCamp()) modifyButton.setEnabled(true);
                        saveAttachButton.setEnabled(true);
                    } else {
                        modifyButton.setEnabled(false);
                        saveAttachButton.setEnabled(false);
                    }
                    if (Permission.canExecutCamp()) {
                        delAttach.setEnabled(true);
                    }
                    if (descriptionArea.isEditable()) {
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                        //descriptionArea.setText(((Attachment)attachmentMap.get(sorter.getValueAt(selectedRow, 0))).getDescriptionFromModel());

                    }
                } else if (sourceType == ENVIRONMENT ) {
                    if (Permission.canUpdateCamp()) {
                        descriptionArea.setEditable(true);
                    }

                    if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                        if (Permission.canUpdateCamp()) modifyButton.setEnabled(true);
                        saveAttachButton.setEnabled(true);
                    } else {
                        modifyButton.setEnabled(false);
                        saveAttachButton.setEnabled(false);
                    }
                    if (Permission.canDeleteCamp()) {
                        delAttach.setEnabled(true);
                    }

                    if (descriptionArea.isEditable()) {
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                    }
                } else if (sourceType == EXECUTION ) {
                    if (Permission.canUpdateCamp() || Permission.canExecutCamp()) {
                        descriptionArea.setEditable(true);
                    }
                    // le bouton actualiser est valide suelement pour les fichiers

                    if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                        if (Permission.canUpdateCamp() || Permission.canExecutCamp()) modifyButton.setEnabled(true);
                        saveAttachButton.setEnabled(true);
                    } else {
                        modifyButton.setEnabled(false);
                        saveAttachButton.setEnabled(false);
                    }
                    if (Permission.canDeleteCamp() || Permission.canExecutCamp()) {
                        delAttach.setEnabled(true);
                    }

                    if (descriptionArea.isEditable()) {
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                        //descriptionArea.setText(((Attachment)attachmentMap.get(sorter.getValueAt(selectedRow, 0))).getDescriptionFromModel());
                    }
                } else if (sourceType == PROJECT || sourceType == FAMILY || sourceType == TESTLIST ||  sourceType == TEST || sourceType == ACTION) {
                    if (Permission.canUpdateTest()) {
                        descriptionArea.setEditable(true);
                    }
                    if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                        if (Permission.canUpdateTest()) modifyButton.setEnabled(true);
                        saveAttachButton.setEnabled(true);
                    } else {
                        modifyButton.setEnabled(false);
                        saveAttachButton.setEnabled(false);
                    }
                    if (Permission.canDeleteTest()) {
                        delAttach.setEnabled(true);
                    }
                    if (descriptionArea.isEditable()) {
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                        //descriptionArea.setText(DataModel.getCurrentTest().getAttachmentFromModel((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                    }
                } else if (sourceType == CAMPAIGN ) {
                    /*try{
                      previousAtt.updateDescriptionInDB(previousAtt.getDescriptionFromModel());
                      } catch (Exception e1){

                      }*/
                    if (Permission.canUpdateCamp()) descriptionArea.setEditable(true);

                    if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                        if (Permission.canUpdateCamp()) modifyButton.setEnabled(true);
                        saveAttachButton.setEnabled(true);
                    } else {
                        modifyButton.setEnabled(false);
                        saveAttachButton.setEnabled(false);
                    }
                    if (Permission.canDeleteCamp()) {
                        delAttach.setEnabled(true);
                    }

                    if (descriptionArea.isEditable()) {
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                        //descriptionArea.setText(DataModel.getCurrentCampaign().getAttachmentFromModel((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                    }

                } else  {
                    if (withAttachment != null){
                        openAttach.setEnabled(true);
                        descriptionArea.setEditable(true);
                        if (!((String)sorter.getValueAt(selectedRow, 1)).equals("")) {
                            modifyButton.setEnabled(true);
                            saveAttachButton.setEnabled(true);
                        } else {
                            modifyButton.setEnabled(false);
                            saveAttachButton.setEnabled(false);
                        }
                        delAttach.setEnabled(true);
                        descriptionArea.setText(getAttachByName((String)sorter.getValueAt(selectedRow, 0)).getDescriptionFromModel());
                    } else {
                        openAttach.setEnabled(false);
                        descriptionArea.setEditable(false);
                        modifyButton.setEnabled(false);
                        delAttach.setEnabled(false);
                        descriptionArea.setText("");
                    }
                }
            } else {
                openAttach.setEnabled(false);
                descriptionArea.setEditable(false);
                modifyButton.setEnabled(false);
                descriptionArea.setText("");
            }

        } else {
            descriptionArea.setEditable(false);
            descriptionArea.setText("");
            openAttach.setEnabled(false);
            delAttach.setEnabled(false);
            modifyButton.setEnabled(false);
        }

        if (readOnly)
            setReadOnly();

    }

    /* Focus on description */
    @Override
    public void focusGained(FocusEvent e) {
        //Util.debug("Focus gained" + e);
    }

    @Override
    public void focusLost(FocusEvent e) {
        if (sourceType == PROJECT || sourceType == FAMILY || sourceType == CAMPAIGN || sourceType == TESTLIST || sourceType == TEST || sourceType == -1) {
            try{
                previousAtt.updateDescriptionInDB(previousAtt.getDescriptionFromModel());
            } catch (Exception e1){
            }
        }
    }

    void setReadOnly() {
        readOnly = true;
        addFileAttach.setEnabled(false);
        addUrlAttach.setEnabled(false);
        screeshootButton.setEnabled(false);
        modifyButton.setEnabled(false);
        delAttach.setEnabled(false);
        descriptionArea.setEditable(false);
    }

} // Fin de la classe AttachmentView
