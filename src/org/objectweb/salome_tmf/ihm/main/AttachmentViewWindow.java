/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.WithAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;


public class AttachmentViewWindow extends JDialog implements DataConstants, ActionListener {
    
    AttachmentView attachView;
    
    int sourceType;
    
    WithAttachment concernedExecutionOrResult;
    
    JButton validationButton;
    JButton cancelButton;
    public boolean cancelPerformed;
    
    HashMap oldAttachMap;
    int t_x = 1024;
    int t_y = 768 ;
    
    static AttachmentViewWindow pAttachmentViewWindow = null;
    
    //public AttachmentViewWindow(JApplet app, int type, WithAttachment executionOrResult, Execution exec, ExecutionResult execResult, Test observedTest) {
    public AttachmentViewWindow(BaseIHM app, int type, WithAttachment executionOrResult) {
                
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width;
            t_y = r.height ;
        } catch(Exception E){
                
        }
        
        pAttachmentViewWindow = this;
        cancelPerformed = false;
        //attachView = new AttachmentView(app, type, type, executionOrResult, SMALL_SIZE_FOR_ATTACH, exec, execResult, observedTest);
        // attachView = new AttachmentView(app, type, type, executionOrResult, SMALL_SIZE_FOR_ATTACH, this);
        attachView = new AttachmentView(app, type, type, executionOrResult, NORMAL_SIZE_FOR_ATTACH, this, true);
        
        validationButton = new JButton(Language.getInstance().getText("Valider"));
        validationButton.addActionListener(this);
        
        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(validationButton);
        buttonsPanel.add(cancelButton);
        
        initData(type, executionOrResult);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(attachView, BorderLayout.CENTER);
        contentPaneFrame.add(buttonsPanel, BorderLayout.SOUTH);
        
        
        setSize(600, 500);
        this.setTitle(Language.getInstance().getText("Attachements"));
        
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);**/
        centerScreen();
        
    } // Fin du constructeur AttachmentViewWindow/3
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    private void initData(int type, WithAttachment executionOrResult) {
        sourceType = type;
        concernedExecutionOrResult = executionOrResult;
        oldAttachMap = executionOrResult.getCopyOfAttachmentMapFromModel();
    }
    
    @Override
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Object source = e.getSource();
        if (source.equals(cancelButton)){
            cancelPerformed(e);
        } else if (source.equals(validationButton)){
            validationPerformed(e);
        }
    }
    
    void validationPerformed(java.awt.event.ActionEvent e){
        cancelPerformed = false;
        //concernedExecutionOrResult.setAttachmentMapInModel(attachView.getAttachmentMap());
        AttachmentViewWindow.this.dispose();  
        pAttachmentViewWindow = null;
    }
    
    void cancelPerformed(java.awt.event.ActionEvent e){
        cancelPerformed = true;
        concernedExecutionOrResult.setAttachmentMapInModel(oldAttachMap);
        AttachmentViewWindow.this.dispose();
        pAttachmentViewWindow = null;
    }
    
    static void cache(boolean cache){
        if (pAttachmentViewWindow != null){
            pAttachmentViewWindow.setVisible(cache);
        }
        
    }
    
} // Fin de la classe AttachmentViewWindow
