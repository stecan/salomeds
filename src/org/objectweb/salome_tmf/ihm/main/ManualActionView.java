/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTextAreaRenderer;
import org.objectweb.salome_tmf.ihm.models.RowTransferHandler;
import org.objectweb.salome_tmf.ihm.models.RowTransferable;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class ManualActionView extends JPanel implements ActionListener,
                                                        ListSelectionListener, PropertyChangeListener {

    JPanel actionsPanel;

    /**
     * Bouton pour la suppression des actions pour les tests manuels
     */
    static JButton deleteManualTestAction;

    /**
     * Table des actions des tests manuels
     */
    JTable actionTable;
    RowTransferHandler transferHandler;

    static JButton modifyAction;
    static JButton addAction;
    static JButton paste;
    static JButton cut;
    static JButton copy;
    static JButton orderAction;
    static JButton plusAction;
    static JButton lessAction;

    static JPopupMenu contextualPopUpMenu;
    static JMenuItem modifyActionMenuItem;
    static JMenuItem deleteActionMenuItem;
    static JMenuItem pasteActionMenuItem;
    static JMenuItem cutActionMenuItem;
    static JMenuItem copyActionMenuItem;

    // JTabbedPane m_ParentTab;

    public ManualActionView() {
        actionsPanel = new JPanel();

        deleteManualTestAction = new JButton(Language.getInstance().getText(
                                                                            "Supprimer"));
        actionTable = new JTable();
        modifyAction = new JButton(Language.getInstance().getText("Modifier"));
        copy = new JButton(Language.getInstance().getText("Copier"));
        paste = new JButton(Language.getInstance().getText("Coller"));
        cut = new JButton(Language.getInstance().getText("Couper"));

        addAction = new JButton(Language.getInstance().getText("Ajouter"));
        addAction.setToolTipText(Language.getInstance().getText(
                                                                "Ajouter_une_action"));
        addAction.addActionListener(this);

        modifyAction.setToolTipText(Language.getInstance().getText(
                                                                   "Modifier_une_action"));
        modifyAction.setEnabled(false);
        modifyAction.addActionListener(this);

        orderAction = new JButton(Language.getInstance().getText("Ordonner"));
        orderAction.setToolTipText(Language.getInstance().getText(
                                                                  "Ordonner_les_actions"));
        orderAction.addActionListener(this);

        plusAction = new JButton("+");
        plusAction.setEnabled(false);
        plusAction.addActionListener(this);

        lessAction = new JButton("-");
        lessAction.setEnabled(false);
        lessAction.addActionListener(this);

        deleteManualTestAction.setEnabled(false);
        deleteManualTestAction.setToolTipText(Language.getInstance().getText(
                                                                             "Supprimer_une_action"));
        deleteManualTestAction.addActionListener(this);

        copy.setToolTipText(Language.getInstance().getText("Copier"));
        copy.setEnabled(false);
        copy.addActionListener(this);

        cut.setToolTipText(Language.getInstance().getText("Couper"));
        cut.setEnabled(false);
        cut.addActionListener(this);

        paste.setToolTipText(Language.getInstance().getText("Coller"));
        paste.setEnabled(false);
        paste.addActionListener(this);

        JPanel buttonsPanel = new JPanel(new GridLayout(1, 7));
        buttonsPanel.add(addAction);
        buttonsPanel.add(modifyAction);
        buttonsPanel.add(orderAction);
        buttonsPanel.add(deleteManualTestAction);
        buttonsPanel.add(copy);
        buttonsPanel.add(cut);
        buttonsPanel.add(paste);
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        JPanel resizePanel = new JPanel(new GridLayout(1, 8));
        resizePanel.add(new JLabel());
        resizePanel.add(new JLabel());
        resizePanel.add(new JLabel());
        resizePanel.add(new JLabel());
        resizePanel.add(new JLabel(Language.getInstance().getText(
                                                                  "Redimensionner")+ " ", SwingConstants.RIGHT));
        resizePanel.add(new JLabel(Language.getInstance().getText(
                                                                  "les_colonnes")
                                   + " : ", SwingConstants.LEFT));
        resizePanel.add(plusAction);
        resizePanel.add(lessAction);

        transferHandler = new RowTransferHandler();
        actionTable.setModel(DataModel.getActionTableModel());

        // TableSorter sorter = new TableSorter(actionTable.getModel() ,
        // actionTable.getTableHeader());
        // actionTable.setModel(sorter);

        actionTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        actionTable.setTransferHandler(transferHandler);
        actionTable.setDefaultRenderer(Object.class, new MyTextAreaRenderer());
        actionTable
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        actionTable.addPropertyChangeListener(this);

        ListSelectionModel rowSM = actionTable.getSelectionModel();
        rowSM.addListSelectionListener(this);

        actionTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (SwingUtilities.isLeftMouseButton(e)
                        && e.getClickCount() == 2) {
                        modifyActionPerformed();
                    }
                    if (SwingUtilities.isRightMouseButton(e)) {
                        Point p = e.getPoint();
                        int rowNumber = actionTable.rowAtPoint(p);
                        if (!actionTable.getSelectionModel().isSelectedIndex(
                                                                             rowNumber))
                            actionTable.getSelectionModel().setSelectionInterval(
                                                                                 rowNumber, rowNumber);
                        showMenu(e);
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        Point p = e.getPoint();
                        int rowNumber = actionTable.rowAtPoint(p);
                        if (!actionTable.getSelectionModel().isSelectedIndex(
                                                                             rowNumber))
                            actionTable.getSelectionModel().setSelectionInterval(
                                                                                 rowNumber, rowNumber);
                        showMenu(e);
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    if (SwingUtilities.isRightMouseButton(e)) {
                        Point p = e.getPoint();
                        int rowNumber = actionTable.rowAtPoint(p);
                        if (!actionTable.getSelectionModel().isSelectedIndex(
                                                                             rowNumber))
                            actionTable.getSelectionModel().setSelectionInterval(
                                                                                 rowNumber, rowNumber);
                        showMenu(e);
                    }
                }
            });

        JScrollPane tablePane = new JScrollPane(actionTable);

        actionsPanel.setLayout(new BorderLayout());
        actionsPanel.add(buttonsPanel, BorderLayout.NORTH);
        actionsPanel.add(tablePane, BorderLayout.CENTER);
        actionsPanel.add(resizePanel, BorderLayout.SOUTH);

        this.setLayout(new BorderLayout());
        this.add(actionsPanel);

        // Mapping GUI Object for plugins//
        // actionsPanel
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.MANUAL_TEST_ACTIONS_PANEL, actionsPanel);
        UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_ACTIONS_PANEL);

        // actionTable
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.MANUAL_TEST_ACTIONS_TABLE, actionTable);
        UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_ACTIONS_TABLE);

        // buttonsPanel
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.MANUAL_TEST_ACTIONS_BUTTONS_PANEL, buttonsPanel);
        UICompCst.staticUIComps
            .add(UICompCst.MANUAL_TEST_ACTIONS_BUTTONS_PANEL);

        modifyActionMenuItem = new JMenuItem(Language.getInstance().getText(
                                                                            "Modifier"));
        modifyActionMenuItem.setEnabled(false);
        modifyActionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    modifyActionPerformed();
                }
            });
        copyActionMenuItem = new JMenuItem(Language.getInstance().getText(
                                                                          "Copier"));
        copyActionMenuItem.setEnabled(false);
        copyActionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    copyPerformed();
                }
            });
        cutActionMenuItem = new JMenuItem(Language.getInstance().getText(
                                                                         "Couper"));
        cutActionMenuItem.setEnabled(false);
        cutActionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cutPerformed();
                }
            });
        pasteActionMenuItem = new JMenuItem(Language.getInstance().getText(
                                                                           "Coller"));
        pasteActionMenuItem.setEnabled(false);
        pasteActionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pastePerformed();
                }
            });
        deleteActionMenuItem = new JMenuItem(Language.getInstance().getText(
                                                                            "Supprimer_une_action"));
        deleteActionMenuItem.setEnabled(false);
        deleteActionMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    deleteManualTestActionPerformed();
                }
            });

        JSeparator sep = new JSeparator();

        contextualPopUpMenu = new JPopupMenu();
        contextualPopUpMenu.add(modifyActionMenuItem);
        contextualPopUpMenu.add(deleteActionMenuItem);
        contextualPopUpMenu.add(sep);
        contextualPopUpMenu.add(copyActionMenuItem);
        contextualPopUpMenu.add(cutActionMenuItem);
        contextualPopUpMenu.add(pasteActionMenuItem);

    } // Fin du constructeur ManualActionView/0

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if (e.getOldValue() == null) {
            // Util.debug("EVENT ------> = " + e.getPropertyName() + "[" +
            // e.getOldValue() +"->"+ e.getNewValue() +"]");
            setColumnSize();
            MyTextAreaRenderer.reinitRows();
        }
    }

    public void setColumnSize() {
        actionTable.getColumnModel().getColumn(0).setPreferredWidth(500);
        actionTable.getColumnModel().getColumn(1).setPreferredWidth(1000);
        actionTable.getColumnModel().getColumn(2).setPreferredWidth(1000);
        actionTable.getColumnModel().getColumn(3).setPreferredWidth(100);
        // table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }

    /*
     * public void setParent(JTabbedPane _ParentTab){ if (m_ParentTab != null){
     * return; } else { m_ParentTab = _ParentTab;
     * m_ParentTab.addChangeListener(new ChangeListener() { public void
     * stateChanged(ChangeEvent e) { if
     * (((JTabbedPane)e.getSource()).getSelectedComponent
     * ().equals(ManualActionView.this)){
     *
     * } } }); } }
     */
    /**
     *
     *
     */
    public static void giveAccessToIhmManualActionView() {
        if (!Permission.canDeleteTest()) {
            deleteManualTestAction.setEnabled(false);
            cut.setEnabled(false);
            cutActionMenuItem.setEnabled(false);
        }
        if (!Permission.canCreateTest()) {
            addAction.setEnabled(false);
            copy.setEnabled(false);
            copyActionMenuItem.setEnabled(false);
            paste.setEnabled(false);
            pasteActionMenuItem.setEnabled(false);
        }
        if (!Permission.canUpdateTest()) {
            modifyAction.setEnabled(false);
            modifyActionMenuItem.setEnabled(false);
            orderAction.setEnabled(false);
        }

    }

    /**
     * Mise a jour des numero d'ordre des tests d'une campagne
     *
     * @param campaign
     *            une campagne
     */
    private static void updateIhmActionOrder(ManualTest test) {
        ArrayList actionList = test.getActionListFromModel(false);
        for (int i = 0; i < actionList.size(); i++) {
            Action action = (Action) actionList.get(i);
            action.setOrderIndex(i);
        }

    } // Fin de la m?thode updateTestOrderInCampaignTree/1

    /********************** Manage events *************************/

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addAction)) {
            addActionPerformed();
            setColumnSize();
        } else if (e.getSource().equals(modifyAction)) {
            modifyActionPerformed();
            setColumnSize();
        } else if (e.getSource().equals(orderAction)) {
            orderActionPerformed();
            setColumnSize();
        } else if (e.getSource().equals(deleteManualTestAction)) {
            deleteManualTestActionPerformed();
            setColumnSize();
        } else if (e.getSource().equals(copy)) {
            copyPerformed();
        } else if (e.getSource().equals(cut)) {
            cutPerformed();
        } else if (e.getSource().equals(paste)) {
            pastePerformed();
            setColumnSize();
        } else if (e.getSource().equals(plusAction)) {
            int nbOfSelectedRows = actionTable.getSelectedRowCount();
            if (nbOfSelectedRows == 1) {
                actionTable
                    .setRowHeight(actionTable.getSelectedRow(), actionTable
                                  .getRowHeight(actionTable.getSelectedRow()) + 5);
            } else if (nbOfSelectedRows > 1) {
                int[] selectedRows = actionTable.getSelectedRows();
                for (int i = 0; i < selectedRows.length; i++) {
                    actionTable.setRowHeight(selectedRows[i], actionTable
                                             .getRowHeight(selectedRows[i]) + 5);
                }
            }
        } else if (e.getSource().equals(lessAction)) {
            int nbOfSelectedRows = actionTable.getSelectedRowCount();
            if (nbOfSelectedRows == 1) {
                actionTable
                    .setRowHeight(actionTable.getSelectedRow(), actionTable
                                  .getRowHeight(actionTable.getSelectedRow()) - 5);
            } else if (nbOfSelectedRows > 1) {
                int[] selectedRows = actionTable.getSelectedRows();
                for (int i = 0; i < selectedRows.length; i++) {
                    if (actionTable.getRowHeight(selectedRows[i]) > 20)
                        actionTable.setRowHeight(selectedRows[i], actionTable
                                                 .getRowHeight(selectedRows[i]) - 5);
                }
            }
        }
    }

    /**
     * @method addActionPerformed()
     * @covers SFG_ForgeORTF_TST_ACT_000030 - \ufffd2.4.8
     * @covers EDF-2.4.8
     * @jira FORTF-1
     */
    public void addActionPerformed() {
        String message = DataModel.getCurrentProject()
            .getCampaignWithExecResultWhereTestIsUse(
                                                     DataModel.getCurrentTest());
        if ((!message.equals("")) && (Api.isLockExecutedTest())) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language.getInstance().getText(
                                                                  "Ce_test_est_utilise_dans_les_campagnes__")
                                   + message
                                   + Language
                                   .getInstance()
                                   .getText(
                                            "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_ajouter_une_nouvelle_action"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText(
                                                                                                       "Ce_test_est_utilise_dans_les_campagnes__")
                                                                        + message
                                                                        + Language.getInstance().getText(
                                                                                                         "IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            // 20100106 - D\ufffdbut modification Forge ORTF v1.0.0
            new AskNewAction(ManualActionView.this, Language.getInstance()
                             .getText("Ajout_d_une_action"));
            // 20100106 - Fin modification Forge ORTF v1.0.0
        }
        // setColumnSize(actionTable);
    }

    /**
     * @method modifyActionPerformed()
     * @covers SFG_ForgeORTF_TST_ACT_000030 - \ufffd2.4.8
     * @covers EDF-2.4.8
     * @jira FORTF-1
     */
    public void modifyActionPerformed() {
        int selectedRowIndex = actionTable.getSelectedRow();
        if (selectedRowIndex != -1) {
            String message = DataModel.getCurrentProject()
                .getCampaignWithExecResultWhereTestIsUse(
                                                         DataModel.getCurrentTest());
            if ((!message.equals("")) && (Api.isLockExecutedTest())) {
                JOptionPane
                    .showMessageDialog(
                                       ManualActionView.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Ce_test_est_utilise_dans_les_campagnes__")
                                       + message
                                       + Language
                                       .getInstance()
                                       .getText(
                                                "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_modifier_que_les_attachements_lies_cette_action"),
                                       Language.getInstance()
                                       .getText("Avertissement_"),
                                       JOptionPane.WARNING_MESSAGE);
                Action oldAction = ((ManualTest) DataModel.getCurrentTest())
                    .getActionFromModel((String) DataModel
                                        .getActionTableModel().getValueAt(
                                                                          selectedRowIndex, 0));
                // 20100106 - D\ufffdbut modification Forge ORTF v1.0.0
                new AskNewAction(ManualActionView.this, Language.getInstance()
                                 .getText("Modifier_une_action"), oldAction,
                                 AskNewAction.MODIFY_ACTION_LOCK_TEST, selectedRowIndex);
                // 20100106 - Fin modification Forge ORTF v1.0.0
            } else {
                if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                    Object[] options = { Language.getInstance().getText("Oui"),
                                         Language.getInstance().getText("Non") };
                    int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                            Language.getInstance().getText(
                                                                                                           "Ce_test_est_utilise_dans_les_campagnes__")
                                                                            + message
                                                                            + Language.getInstance().getText(
                                                                                                             "IntegiteCheck"),
                                                                            Language.getInstance().getText("Attention_"),
                                                                            JOptionPane.WARNING_MESSAGE, options);
                    if (choice == JOptionPane.NO_OPTION) {
                        return;
                    }
                }
                if (DataModel.getCurrentTest() instanceof ManualTest) {

                    Action oldAction = ((ManualTest) DataModel.getCurrentTest())
                        .getActionFromModel((String) DataModel
                                            .getActionTableModel().getValueAt(
                                                                              selectedRowIndex, 0));
                    // 20100106 - D\ufffdbut modification Forge ORTF v1.0.0
                    AskNewAction addAction = new AskNewAction(
                                                              ManualActionView.this, Language.getInstance()
                                                              .getText("Modifier_une_action"), oldAction,
                                                              AskNewAction.MODIFY_ACTION, selectedRowIndex);
                    // 20100106 - Fin modification Forge ORTF v1.0.0

                }
            }
        }

    }

    public void orderActionPerformed() {
        String message = DataModel.getCurrentProject()
            .getCampaignWithExecResultWhereTestIsUse(
                                                     DataModel.getCurrentTest());
        if ((!message.equals("")) && (Api.isLockExecutedTest())) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language.getInstance().getText(
                                                                  "Ce_test_est_utilise_dans_les_campagnes__")
                                   + message
                                   + Language
                                   .getInstance()
                                   .getText(
                                            "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_reordonner_les_actions"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText(
                                                                                                       "Ce_test_est_utilise_dans_les_campagnes__")
                                                                        + message
                                                                        + Language.getInstance().getText(
                                                                                                         "IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            if (DataModel.getCurrentTest() instanceof ManualTest) {
                new ActionOrdering(((ManualTest) DataModel.getCurrentTest())
                                   .getActionListFromModel(false));
            }
        }
    }

    public void deleteManualTestActionPerformed() {
        String message = DataModel.getCurrentProject()
            .getCampaignWithExecResultWhereTestIsUse(
                                                     DataModel.getCurrentTest());
        if ((!message.equals("")) && (Api.isLockExecutedTest())) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language.getInstance().getText(
                                                                  "Ce_test_est_utilise_dans_les_campagnes__")
                                   + message
                                   + Language
                                   .getInstance()
                                   .getText(
                                            "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_supprimer_cette_action"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText(
                                                                                                       "Ce_test_est_utilise_dans_les_campagnes__")
                                                                        + message
                                                                        + Language.getInstance().getText(
                                                                                                         "IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            Object[] options = { Language.getInstance().getText("Oui"),
                                 Language.getInstance().getText("Non") };
            int choice = -1;
            int[] selectedRows = actionTable.getSelectedRows();
            String actionList = "";
            for (int i = selectedRows.length - 1; i >= 0; i--) {
                actionList = actionList
                    + "* "
                    + (String) DataModel.getActionTableModel().getValueAt(
                                                                          selectedRows[i], 0);
                if (i == 0) {
                    actionList = actionList + " ?";
                } else {
                    actionList = actionList + " \n";
                }
            }
            choice = JOptionPane.showOptionDialog(ManualActionView.this,
                                                  Language.getInstance().getText(
                                                                                 "Etes_vous_sur_de_vouloir_supprimer_les_actions__")
                                                  + actionList, Language.getInstance().getText(
                                                                                               "Attention_"), JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (choice == JOptionPane.YES_OPTION) {
                for (int i = selectedRows.length - 1; i >= 0; i--) {

                    try {
                        // BdD
                        Action action = ((ManualTest) DataModel
                                         .getCurrentTest())
                            .getActionFromModel((String) DataModel
                                                .getActionTableModel().getValueAt(
                                                                                  selectedRows[i], 0));
                        // action.deleteFromDB(true);
                        ((ManualTest) DataModel.getCurrentTest())
                            .deleteActionInDBModel(action);
                        // action.deleteFromBddAndModel(true);
                        // IHM
                        // ((ManualTest)DataModel.getCurrentTest()).deleteAction((String)DataModel.getActionTableModel().getValueAt(selectedRows[i],0));
                        if (DataModel.getActionTableModel().getRowCount() == 0) {
                            deleteManualTestAction.setEnabled(false);
                        }
                        DataModel.getActionTableModel().removeData(
                                                                   selectedRows[i]);
                        updateIhmActionOrder((ManualTest) DataModel
                                             .getCurrentTest());

                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                }
            }
        }
        // setColumnSize(actionTable);
    }

    public void copyPerformed() {
        int[] selectedRows = actionTable.getSelectedRows();
        ArrayList data = new ArrayList();
        for (int i = 0; i < selectedRows.length; i++) {
            data.add(((ManualTest) DataModel.getCurrentTest())
                     .getActionListFromModel(false).get(selectedRows[i]));
        }
        try {
            RowTransferable rowTransferable = new RowTransferable(data);
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                                                                         rowTransferable, null);
        } catch (IllegalStateException ise) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language
                                   .getInstance()
                                   .getText(
                                            "Le_presse_papier_est_deja_utilise_par_une_autre_application_"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        }
        paste.setEnabled(true);
        pasteActionMenuItem.setEnabled(true);
    }

    public void cutPerformed() {
        String message = DataModel.getCurrentProject()
            .getCampaignWithExecResultWhereTestIsUse(
                                                     DataModel.getCurrentTest());
        boolean error = false;
        if ((!message.equals("")) && (Api.isLockExecutedTest())) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language.getInstance().getText(
                                                                  "Ce_test_est_utilise_dans_les_campagnes__")
                                   + message
                                   + Language
                                   .getInstance()
                                   .getText(
                                            "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_supprimer_cette_action"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText(
                                                                                                       "Ce_test_est_utilise_dans_les_campagnes__")
                                                                        + message
                                                                        + Language.getInstance().getText(
                                                                                                         "IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            int[] selectedRows = actionTable.getSelectedRows();
            ArrayList data = new ArrayList();
            for (int i = 0; i < selectedRows.length; i++) {
                data.add(((ManualTest) DataModel.getCurrentTest())
                         .getActionListFromModel(false).get(selectedRows[i]));
            }
            try {
                RowTransferable rowTransferable = new RowTransferable(data);
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                                                                             rowTransferable, null);

            } catch (IllegalStateException ise) {
                JOptionPane
                    .showMessageDialog(
                                       ManualActionView.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Le_presse_papier_est_deja_utilise_par_une_autre_application_"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
                error = true;
            }
            if (!error) {
                for (int i = selectedRows.length - 1; i >= 0; i--) {

                    try {
                        // BdD
                        Action action = ((ManualTest) DataModel
                                         .getCurrentTest())
                            .getActionFromModel((String) DataModel
                                                .getActionTableModel().getValueAt(
                                                                                  selectedRows[i], 0));
                        // action.deleteFromDB(true);
                        // ((ManualTest)DataModel.getCurrentTest()).deleteActionInModel(action);
                        ((ManualTest) DataModel.getCurrentTest())
                            .deleteActionInDBModel(action);
                        // action.deleteFromBddAndModel(true);
                        // IHM
                        // ((ManualTest)DataModel.getCurrentTest()).deleteAction((String)DataModel.getActionTableModel().getValueAt(selectedRows[i],0));
                        DataModel.getActionTableModel().removeData(
                                                                   selectedRows[i]);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                }
                updateIhmActionOrder((ManualTest) DataModel.getCurrentTest());
                paste.setEnabled(true);
                pasteActionMenuItem.setEnabled(true);
            }
        }
    }

    public void pastePerformed() {
        String message = DataModel.getCurrentProject()
            .getCampaignWithExecResultWhereTestIsUse(
                                                     DataModel.getCurrentTest());
        if ((!message.equals("")) && (Api.isLockExecutedTest())) {
            JOptionPane
                .showMessageDialog(
                                   ManualActionView.this,
                                   Language.getInstance().getText(
                                                                  "Ce_test_est_utilise_dans_les_campagnes__")
                                   + message
                                   + Language
                                   .getInstance()
                                   .getText(
                                            "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_ajouter_une_nouvelle_action"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText(
                                                                                                       "Ce_test_est_utilise_dans_les_campagnes__")
                                                                        + message
                                                                        + Language.getInstance().getText(
                                                                                                         "IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard()
                .getContents(null);
            try {
                DataFlavor dataFlavor = new DataFlavor(ArrayList.class,
                                                       "ArrayList");
                /** V?rification que le contenu est de type texte. */
                if (t != null && t.isDataFlavorSupported(dataFlavor)) {
                    ArrayList actionsList = (ArrayList) t
                        .getTransferData(dataFlavor);
                    Test pTest = DataModel.getCurrentTest();
                    for (int i = 0; i < actionsList.size(); i++) {
                        int transNumber = -1;
                        try {
                            // Init
                            ArrayList data = new ArrayList();
                            Action pActionToCopy = (Action) actionsList.get(i);
                            Action pNewAction = new Action(pActionToCopy, pTest);

                            if (((ManualTest) pTest)
                                .hasActionNameInModel(pNewAction
                                                      .getNameFromModel())) {
                                String newName = Language.getInstance()
                                    .getText("Copie_de_")
                                    + pNewAction.getNameFromModel();
                                while (((ManualTest) pTest)
                                       .hasActionNameInModel(newName)) {
                                    newName = Language.getInstance().getText(
                                                                             "Copie_de_")
                                        + newName;
                                }
                                pNewAction.updateInModel(newName, pNewAction
                                                         .getDescriptionFromModel(), pNewAction
                                                         .getAwaitedResultFromModel());
                            }
                            // BDD
                            transNumber = Api.beginTransaction(101,
                                                               ApiConstants.INSERT_ACTION);

                            // 1 Add action
                            pNewAction.setOrderIndex(DataModel
                                                     .getActionTableModel().getRowCount());
                            ((ManualTest) pTest).addActionInDB(pNewAction);

                            // 2 Add Use Param
                            Vector paramToAdd2Test = new Vector();
                            HashSet paramsSet = new HashSet(pActionToCopy
                                                            .getParameterHashSetFromModel().values()); // FOR
                            // HashTable2HashSet
                            for (Iterator iter = paramsSet.iterator(); iter
                                     .hasNext();) {
                                Parameter param = (Parameter) iter.next();
                                if (!pTest.hasUsedParameterNameFromModel(param
                                                                         .getNameFromModel())) {
                                    pTest.setUseParamInDBAndModel(param);
                                    // pTest.setUseParamInDB(param.getIdBdd());
                                    paramToAdd2Test.add(param);
                                }
                                // pNewAction.setUseParamInDB(param.getIdBdd());
                                pNewAction.setUseParamInDBAndModel(param);
                            }
                            Api.commitTrans(transNumber);
                            transNumber = -1;
                            // IHM
                            data.add(pNewAction.getNameFromModel());
                            data.add(pNewAction.getDescriptionFromModel());
                            data.add(pNewAction.getAwaitedResultFromModel());
                            data.add(pNewAction.getAttachmentMapFromModel());
                            for (int j = 0; j < paramToAdd2Test.size(); j++) {
                                Parameter param = (Parameter) paramToAdd2Test
                                    .get(j);
                                ArrayList dataParam = new ArrayList();
                                dataParam.add(param.getNameFromModel());
                                dataParam.add(param.getDescriptionFromModel());
                                DataModel.getTestParameterTableModel().addRow(
                                                                              dataParam);
                            }
                            paramToAdd2Test.clear();
                            DataModel.getActionTableModel().addRow(data);
                            ((ManualTest) pTest).addActionInModel(pNewAction);
                        } catch (Exception e_copy) {
                            Api.forceRollBackTrans(transNumber);
                            Tools.ihmExceptionView(e_copy);

                        }
                    }
                }
            } catch (UnsupportedFlavorException e1) {
                JOptionPane
                    .showMessageDialog(
                                       ManualActionView.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Impossible_de_copier__il_ne_s_agit_pas_d_actions_"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
            } catch (IOException e2) {
                Util.err(e2);
                JOptionPane
                    .showMessageDialog(
                                       ManualActionView.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Probleme_lors_de_la_recuperation_des_donnees_"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // ************** Event table *******************//

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(actionTable.getSelectionModel())) {
            actionTablevalueChanged(e);
        }
        // setColumnSize2(actionTable);
    }

    public void actionTablevalueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting())
            return;

        int nbOfSelectedRows = actionTable.getSelectedRowCount();
        int selectedRow = actionTable.getSelectedRow();
        if (selectedRow != -1) {
            if (nbOfSelectedRows != 1) {
                if (Permission.canDeleteTest()) {
                    deleteManualTestAction.setEnabled(true);
                    deleteActionMenuItem.setEnabled(false);
                }
                modifyAction.setEnabled(false);
                modifyActionMenuItem.setEnabled(false);
                plusAction.setEnabled(true);
                lessAction.setEnabled(true);
            } else {
                plusAction.setEnabled(true);
                lessAction.setEnabled(true);
                if (Permission.canUpdateTest()) {
                    modifyAction.setEnabled(true);
                    modifyActionMenuItem.setEnabled(true);
                }
                if (Permission.canDeleteTest()) {
                    deleteManualTestAction.setEnabled(true);
                    deleteActionMenuItem.setEnabled(true);
                }
            }
            if (Permission.canDeleteTest()) {
                cut.setEnabled(true);
                cutActionMenuItem.setEnabled(true);
            }
            if (Permission.canCreateTest()) {
                copy.setEnabled(true);
                copyActionMenuItem.setEnabled(true);
            }

        } else {
            plusAction.setEnabled(false);
            lessAction.setEnabled(false);
            deleteManualTestAction.setEnabled(false);
            deleteActionMenuItem.setEnabled(false);
            modifyAction.setEnabled(false);
            modifyActionMenuItem.setEnabled(false);
            cut.setEnabled(false);
            cutActionMenuItem.setEnabled(false);
            copy.setEnabled(false);
            copyActionMenuItem.setEnabled(false);
        }

    }

    public void showMenu(MouseEvent e) {
        contextualPopUpMenu.show(actionTable, e.getX(), e.getY());
    }

} // Fin de la classe ManualActionView
