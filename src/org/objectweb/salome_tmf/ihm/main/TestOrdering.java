/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.TestListFamilyCampaignTreeNode;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui definit la fenetre pour ordonner les suites de tests
 * @covers SFG_ForgeORTF_TST_CMP_000040 - \ufffd2.4.4
 * @covers EDF-2.4.4
 * @jira FORTF-6
 */
public class TestOrdering extends JDialog implements IHMConstants , ActionListener, ListSelectionListener {

    DefaultListModel listModel; //Ancien modele de donnees avant organisation
    HashSet familyHashSet;
    HashSet campaignHashSet;

    DefaultComboBoxModel comboModel; //Modele de donnees pour le choix
    JLabel comboLabel;

    JComboBox choiceComboBox; //Liste deroulante pour le choix des suites ou des tests.
    JButton upButton;
    JButton downButton;
    JButton validate;
    DefaultComboBoxModel testListComboModel;
    JList list;


    HashMap testListMap; // Key : famille, Value ArrayList avec des suites de tests de cette famille
    HashMap testMap; // Key suite de tests, Value ArrayList avec les tests de cette suite.

    TestTreeModel workingModel;

    JRadioButton familyButton;
    JRadioButton testListButton;
    JRadioButton testButton;
    JRadioButton campagneButton;

    DefaultMutableTreeNode familySelected;

    DefaultMutableTreeNode testListSelected;
    JScrollPane usersListScrollPane;

    //boolean useCampaign;
    DynamicTree pDynamicTree;

    boolean campaignOrderModified; // Pour indiquer si l'ordre des campagnes a ete modifie
    boolean familyOrderModified; //Indique si l'ordre des familles a ete modifie

    ArrayList familiesTestListOrderModified; //Liste des familles dont l'ordre des suites a ete modifie
    ArrayList testListTestOrderModified; //Liste des suites dont l'ordre des tests a ete modifie
    ArrayList campaignFamilyOrderModified; // Liste des campagnes dont l'ordre des familles a ete modifie
    ArrayList campaignFamiliesTestListTestOrderModified; // Liste des campagnes dont l'ordre des tests a ete modifie
    ArrayList campaignFamiliesTestListOrderModified;

    boolean campaignMode = false;

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    /**
     * @method TestOrdering(DynamicTree testTreeModel, boolean _withCampaign)
     * @param testTreeModel
     * @param _withCampaign
     */
    public TestOrdering(DynamicTree testTreeModel, boolean _withCampaign) {
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        int t_x = 1024;
        int t_y = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width;
            t_y = r.height;

        } catch(Exception E){
            t_x = 1024;
            t_y = 768;
        }
        int decal_x = t_x/5;
        int decal_y = t_x/6;

        campaignMode = _withCampaign;
        pDynamicTree = testTreeModel;
        familyHashSet = new HashSet();
        campaignHashSet = new HashSet();

        /** North Panel **/
        familyButton = new JRadioButton(Language.getInstance().getText("Familles"));
        testListButton = new JRadioButton(Language.getInstance().getText("Suites"));
        testButton = new JRadioButton(Language.getInstance().getText("Tests"));
        campagneButton = new JRadioButton(Language.getInstance().getText("Campagnes"));
        campagneButton.addActionListener(this);
        familyButton.addActionListener(this);
        testListButton.addActionListener(this);
        testButton.addActionListener(this);
        /*Grouper les boutons radio*/
        ButtonGroup choiceGroup = new ButtonGroup();
        if (campaignMode) {
            choiceGroup.add(campagneButton);
        }
        choiceGroup.add(familyButton);
        choiceGroup.add(testListButton);
        choiceGroup.add(testButton);
        /*Put the radio buttons in a column in a panel*/
        JPanel radioPanel = new JPanel(new GridLayout(1, 0));
        if (campaignMode) {
            radioPanel.add(campagneButton);
        }
        radioPanel.add(familyButton);
        radioPanel.add(testListButton);
        radioPanel.add(testButton);



        /** Combo Panel **/
        if (!campaignMode){
            testListComboModel = new DefaultComboBoxModel();
        }
        comboModel = new DefaultComboBoxModel();
        choiceComboBox = new JComboBox(comboModel);
        choiceComboBox.setEnabled(false);
        choiceComboBox.addActionListener(this);
        comboLabel = new JLabel();

        JPanel comboPanel = new JPanel();
        comboPanel.setLayout(new BoxLayout(comboPanel, BoxLayout.X_AXIS));
        comboPanel.add(comboLabel);
        comboPanel.add(choiceComboBox);
        JPanel northPanel = new JPanel();
        northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
        northPanel.add(radioPanel);
        northPanel.add(comboPanel);
        northPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));


        /** Center Panel **/
        upButton = new JButton();
        Icon icon = Tools.createAppletImageIcon(PATH_TO_ARROW_UP_ICON,"");
        upButton.setIcon(icon);
        upButton.setEnabled(false);
        upButton.setToolTipText(Language.getInstance().getText("Monter_d_un_cran"));
        upButton.addActionListener(this);
        downButton = new JButton();
        icon = Tools.createAppletImageIcon(PATH_TO_ARROW_DOWN_ICON,"");
        downButton.setIcon(icon);
        downButton.setEnabled(false);
        downButton.setToolTipText(Language.getInstance().getText("Descendre_d_un_cran"));
        downButton.addActionListener(this);

        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(upButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
        buttonSet.add(downButton);

        listModel = new DefaultListModel();
        list = new JList(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(listModel.getSize());

        usersListScrollPane = new JScrollPane(list);
        list.setCellRenderer(new TestOrderingListCellRenderer(campaignMode));

        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(""));
        usersListScrollPane.setPreferredSize(new Dimension(t_x*3/5,(t_y*3/6) - 25));

        JPanel centerPanel = new JPanel();
        centerPanel.add(usersListScrollPane);
        centerPanel.add(buttonSet);


        /** SOUTH Panel **/
        validate = new JButton(Language.getInstance().getText("Terminer"));
        validate.addActionListener(this);
        JPanel southPanel = new JPanel();
        southPanel.add(validate);

        /** Main Panel **/
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.setLayout(new BorderLayout());
        contentPaneFrame.add(northPanel, BorderLayout.NORTH);
        contentPaneFrame.add(centerPanel, BorderLayout.CENTER);
        contentPaneFrame.add(southPanel, BorderLayout.SOUTH);


        initData();
        initToCurrentSelected(pDynamicTree.getSelectedNode());

        setSize(new Dimension(t_x*3/5, t_y*4/6));
        this.setTitle(Language.getInstance().getText("Ordonner"));

        /*this.pack();
          this.setLocationRelativeTo(this.getParent());
          this.setVisible(true);
        */
        centerScreen();
    } // Fin du constructeur Ordering/1
    //20100108 - Fin modification Forge ORTF V1.0.0

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }

    public void initData() {
        //listModel = new DefaultListModel();
        workingModel = pDynamicTree.getModel();
        comboModel.removeAllElements();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)workingModel.getRoot();
        if (campaignMode) {
            for (int i = 0; i < root.getChildCount(); i++) {
                TestListFamilyCampaignTreeNode tlfcampaign = new TestListFamilyCampaignTreeNode();
                tlfcampaign.setCampaignNode((DefaultMutableTreeNode)root.getChildAt(i));
                campaignHashSet.add(tlfcampaign);
                for (int j = 0; j < ((DefaultMutableTreeNode)root.getChildAt(i)).getChildCount(); j++) {
                    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)((DefaultMutableTreeNode)root.getChildAt(i)).getChildAt(j);
                    TestListFamilyCampaignTreeNode tlfamily = new TestListFamilyCampaignTreeNode();
                    tlfamily.setCampaignNode((DefaultMutableTreeNode)root.getChildAt(i));
                    tlfamily.setFamilyNode(familyNode);
                    familyHashSet.add(tlfamily);
                    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                    //                                  for (int k = 0; k < familyNode.getChildCount(); k++) {
                    //                                          TestListFamilyCampaignTreeNode tlfList = new TestListFamilyCampaignTreeNode();
                    //                                          tlfList.setCampaignNode((DefaultMutableTreeNode)root.getChildAt(i));
                    //                                          tlfList.setFamilyNode(familyNode);
                    //                                          tlfList.setTestListNode((DefaultMutableTreeNode)familyNode.getChildAt(k));
                    //                                          testListComboModel.addElement(tlfList);
                    //                                  }
                    //20100108 - Fin modification Forge ORTF V1.0.0
                }
            }
        } else {
            for (int i = 0; i < root.getChildCount(); i++) {
                for (int j = 0; j < ((DefaultMutableTreeNode)root.getChildAt(i)).getChildCount(); j++) {
                    TestListFamilyCampaignTreeNode tlfFamily = new TestListFamilyCampaignTreeNode();
                    tlfFamily.setFamilyNode((DefaultMutableTreeNode)root.getChildAt(i));
                    familyHashSet.add(tlfFamily);
                    TestListFamilyCampaignTreeNode tlfList = new TestListFamilyCampaignTreeNode();
                    tlfList.setFamilyNode((DefaultMutableTreeNode)root.getChildAt(i));
                    tlfList.setTestListNode((DefaultMutableTreeNode)((DefaultMutableTreeNode)root.getChildAt(i)).getChildAt(j));
                    testListComboModel.addElement(tlfList);

                }
            }
        }

    }


    void initToCurrentSelected(DefaultMutableTreeNode pNode){
        SimpleData pData = null;
        SimpleData parentData = null;
        if (pNode == null){
            return;
        } else {
            if (pNode.getUserObject() instanceof SimpleData)
                pData = (SimpleData) pNode.getUserObject();
        }
        DefaultMutableTreeNode parentNode  = (DefaultMutableTreeNode) pNode.getParent();

        if (parentNode != null){
            if (parentNode.getUserObject() instanceof SimpleData) {
                parentData = (SimpleData) parentNode.getUserObject();
            }
        }

        boolean trouve = false;
        int size = 0;
        boolean trouve2 = false;
        int size2 = 0;
        int i = 0;
        if (pData instanceof Campaign){
            campagneButton.setSelected(true);
            campagnePerformed(null);
            size2 = listModel.getSize();
            i = 0;
            while (i < size2 && !trouve2){
                DefaultMutableTreeNode pTempNode  = (DefaultMutableTreeNode)listModel.getElementAt(i);
                Campaign pCamp = (Campaign) pTempNode.getUserObject();
                if (pCamp.getIdBdd() == pData.getIdBdd()){
                    trouve2 = true;
                } else {
                    i++;
                }
            }
            if (trouve2){
                list.setSelectedIndex(i);
            }
        } else if (pData instanceof Family ){
            familyButton.setSelected(true);
            familyPerformed(null);
            if (campaignMode){
                size = comboModel.getSize();
                while (i < size && !trouve){
                    TestListFamilyCampaignTreeNode tlf  = (TestListFamilyCampaignTreeNode)comboModel.getElementAt(i);
                    Campaign pCamp = (Campaign) tlf.getCampaignNode().getUserObject();
                    if (pCamp.getIdBdd() == parentData.getIdBdd()){
                        trouve = true;
                    } else {
                        i++;
                    }
                }
                if (trouve){
                    choiceComboBox.setSelectedIndex(i);
                }

            }else {
                // NOTHING
            }
            size2 = listModel.getSize();
            i = 0;
            while (i < size2 && !trouve2){
                DefaultMutableTreeNode pTempNode  = (DefaultMutableTreeNode)listModel.getElementAt(i);
                Family pFamily  = (Family) pTempNode.getUserObject();
                if (pFamily.getIdBdd() == pData.getIdBdd()){
                    trouve2 = true;
                } else {
                    i++;
                }
            }
            if (trouve2){
                list.setSelectedIndex(i);
            }

        } else if (pData instanceof TestList){
            if (campaignMode){
                testListButton.setSelected(true);
                testListPerformed(null);
                size = comboModel.getSize();
                while (i < size && !trouve){
                    TestListFamilyCampaignTreeNode tlf  = (TestListFamilyCampaignTreeNode)comboModel.getElementAt(i);
                    Family pFamily  = (Family) tlf.getFamilyNode().getUserObject();
                    if (pFamily.getIdBdd() == parentData.getIdBdd()){
                        trouve = true;
                    } else {
                        i++;
                    }
                }
                if (trouve){
                    choiceComboBox.setSelectedIndex(i);
                }
                size2 = listModel.getSize();
                i = 0;
                while (i < size2 && !trouve2){
                    DefaultMutableTreeNode pTempNode  = (DefaultMutableTreeNode)listModel.getElementAt(i);
                    TestList pTestList  = (TestList) pTempNode.getUserObject();
                    if (pTestList.getIdBdd() == pData.getIdBdd()){
                        trouve2 = true;
                    } else {
                        i++;
                    }
                }
                if (trouve2){
                    list.setSelectedIndex(i);
                }
            } else {
                initToCurrentSelected((DefaultMutableTreeNode) pNode.getFirstChild());
            }
        } else if (pData instanceof Test){
            testButton.setSelected(true);
            testPerformed(null);
            size = comboModel.getSize();
            //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
            if(campaignMode){
                parentData = null;
                parentNode  = (DefaultMutableTreeNode) pNode.getParent();
                if (parentNode != null){
                    if (parentNode.getUserObject() instanceof SimpleData) {
                        parentData = (SimpleData) parentNode.getUserObject();
                        while (i < size && !trouve){
                            TestListFamilyCampaignTreeNode tlf  = (TestListFamilyCampaignTreeNode)comboModel.getElementAt(i);
                            Family pFamilyList  = (Family) tlf.getFamilyNode().getUserObject();
                            if (pFamilyList.getIdBdd() == parentData.getIdBdd()){
                                trouve = true;
                            } else {
                                i++;
                            }
                        }
                    }
                }
            } else {
                //20100108 - Fin modification Forge ORTF V1.0.0
                while (i < size && !trouve){
                    TestListFamilyCampaignTreeNode tlf  = (TestListFamilyCampaignTreeNode)comboModel.getElementAt(i);
                    TestList pTestList  = (TestList) tlf.getTestListNode().getUserObject();
                    if (pTestList.getIdBdd() == parentData.getIdBdd()){
                        trouve = true;
                    } else {
                        i++;
                    }
                }
            }
            if (trouve){
                choiceComboBox.setSelectedIndex(i);
            }
            size2 = listModel.getSize();
            i = 0;
            while (i < size2 && !trouve2){
                DefaultMutableTreeNode pTempNode  = (DefaultMutableTreeNode)listModel.getElementAt(i);
                Test pTest  = (Test) pTempNode.getUserObject();
                if (pTest.getIdBdd() == pData.getIdBdd()){
                    trouve2 = true;
                } else {
                    i++;
                }
            }
            if (trouve2){
                list.setSelectedIndex(i);
            }
        }
    }

    private boolean containsValue( TestListFamilyCampaignTreeNode tlf) {
        int size = comboModel.getSize();
        for (int i = 0; i < size; i++) {
            if (((TestListFamilyCampaignTreeNode)comboModel.getElementAt(i)).getFamilyNode().equals(tlf.getFamilyNode())) {
                return true;
            }
        }
        return false;
    }



    /********************** ActionListener ******************************/

    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(campagneButton)){
            campagnePerformed(e);
        } else if (e.getSource().equals(familyButton)){
            familyPerformed(e);
        } else if (e.getSource().equals(testListButton)){
            testListPerformed(e);
        } else if (e.getSource().equals(testButton)){
            testPerformed(e);
        } else if (e.getSource().equals(choiceComboBox)){
            choiceComboPerformed(e);
        } else if (e.getSource().equals(upButton)){
            upPerformed(e);
        } else if (e.getSource().equals(downButton)){
            downPerformed(e);
        } else if (e.getSource().equals(validate)){
            validatePerformed(e);
        }
    }

    void validatePerformed(ActionEvent e) {
        TestOrdering.this.dispose();
    }

    void campagnePerformed(ActionEvent e) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)workingModel.getRoot();
        listModel.clear();
        comboModel.removeAllElements();
        choiceComboBox.setEnabled(false);
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Campagnes")));
        for (int i = 0; i < root.getChildCount(); i++) {
            listModel.addElement(root.getChildAt(i));
        }
        comboLabel.setText("");
    }


    void familyPerformed(ActionEvent e) {
        if (campaignMode) {
            choiceComboBox.setEnabled(true);
            comboModel.removeAllElements();
            for (Iterator iter = campaignHashSet.iterator(); iter.hasNext();) {
                comboModel.addElement(iter.next());
            }
            comboLabel.setText(Language.getInstance().getText("Campagne") + " : ");
        } else {
            DefaultMutableTreeNode root = (DefaultMutableTreeNode)workingModel.getRoot();
            listModel.clear();
            comboModel.removeAllElements();
            choiceComboBox.setEnabled(false);
            for (int i = 0; i < root.getChildCount(); i++) {
                listModel.addElement(root.getChildAt(i));
            }
            comboLabel.setText("");
        }
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Familles")));
    }

    void testListPerformed(ActionEvent e) {
        choiceComboBox.setEnabled(true);
        listModel.clear();
        comboModel.removeAllElements();
        if (campaignMode) {
            comboLabel.setText(Language.getInstance().getText("Campagne") + "/" +Language.getInstance().getText("Famille")+ " : ");
        } else {
            comboLabel.setText(Language.getInstance().getText("Famille")+ " : ");
        }
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Suites")));
        for (Iterator iter = familyHashSet.iterator(); iter.hasNext();) {
            TestListFamilyCampaignTreeNode tlf = (TestListFamilyCampaignTreeNode)iter.next();
            if (!containsValue(tlf)) {
                comboModel.addElement(tlf);
            }
        }
    }

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    void testPerformed(ActionEvent e) {
        choiceComboBox.setEnabled(true);
        listModel.clear();
        comboModel.removeAllElements();
        usersListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Tests")));
        if (campaignMode) {
            comboLabel.setText(Language.getInstance().getText("Campagne") + "/" +Language.getInstance().getText("Famille") + " : ");
        } else {
            comboLabel.setText(Language.getInstance().getText("Famille")+ "/" +Language.getInstance().getText("Suite")+ " : ");
        }
        if (campaignMode) {
            for (Iterator iter = familyHashSet.iterator(); iter.hasNext();) {
                TestListFamilyCampaignTreeNode tlf = (TestListFamilyCampaignTreeNode)iter.next();
                if (!containsValue(tlf)) {
                    comboModel.addElement(tlf);
                }
            }
        } else {
            //DefaultMutableTreeNode root = (DefaultMutableTreeNode)workingModel.getRoot();
            for (int i = 0; i < testListComboModel.getSize(); i++) {
                comboModel.addElement(testListComboModel.getElementAt(i));
            }
        }
    }
    //20100108 - Fin modification Forge ORTF V1.0.0


    void choiceComboPerformed(ActionEvent e) {
        listModel.clear();
        TestListFamilyCampaignTreeNode selectedNode = ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem());;
        if (selectedNode != null) {
            if (familyButton.isSelected() && campaignMode) {
                for (int i = 0; i < selectedNode.getCampaignNode().getChildCount(); i++) {
                    listModel.addElement(selectedNode.getCampaignNode().getChildAt(i));
                }
            } else if (testListButton.isSelected()) {
                for (int i = 0; i < selectedNode.getFamilyNode().getChildCount(); i++) {
                    listModel.addElement(selectedNode.getFamilyNode().getChildAt(i));
                }
            } else if (testButton.isSelected()) {
                //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                if (campaignMode) {
                    DefaultMutableTreeNode testListNode;
                    for (int i = 0; i < selectedNode.getFamilyNode().getChildCount(); i++) {
                        testListNode = (DefaultMutableTreeNode) selectedNode.getFamilyNode().getChildAt(i);
                        for (int j = 0; j < testListNode.getChildCount(); j++) {
                            listModel.addElement(testListNode.getChildAt(j));
                        }
                    }
                } else {
                    //20100108 - Fin modification Forge ORTF V1.0.0
                    for (int i = 0; i < selectedNode.getTestListNode().getChildCount(); i++) {
                        listModel.addElement(selectedNode.getTestListNode().getChildAt(i));
                    }
                }
            }
        }
    }

    void upPerformed(ActionEvent e) {
        int selectedIndex = list.getSelectedIndex();
        if (selectedIndex != -1) {
            Object objToGoUp = listModel.getElementAt(selectedIndex);
            Object objToGoDown = listModel.getElementAt(selectedIndex - 1);
            listModel.setElementAt(objToGoUp, selectedIndex - 1);
            listModel.setElementAt(objToGoDown, selectedIndex);
            list.setSelectedIndex(selectedIndex - 1);
            DefaultMutableTreeNode nodeToGoUp = null;
            DefaultMutableTreeNode nodeToGoDown = null;
            if (campagneButton.isSelected()) {

                try {
                    ((Campaign)((DefaultMutableTreeNode)objToGoUp).getUserObject()).updateOrderInDBAndModel(false);

                    // IHM
                    nodeToGoUp = (DefaultMutableTreeNode)((DefaultMutableTreeNode)workingModel.getRoot()).getChildAt(selectedIndex);
                    Campaign pCamp = (Campaign)nodeToGoUp.getUserObject();

                    workingModel.removeNodeFromParent(nodeToGoUp);
                    workingModel.insertNodeInto(nodeToGoUp, ((DefaultMutableTreeNode)workingModel.getRoot()), selectedIndex - 1);
                    DataModel.getCurrentProject().getCampaignListFromModel().remove(pCamp);
                    DataModel.getCurrentProject().getCampaignListFromModel().add((selectedIndex-1), pCamp);

                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
            } else if (familyButton.isSelected()) {
                if (campaignMode) {
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode().getChildAt(selectedIndex - 1);
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode().getChildAt(selectedIndex);
                    ArrayList oldTestList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getTestListFromModel();
                    ArrayList oldSuiteList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getSuitListFromModel();
                    int firstTestIndex = oldTestList.indexOf(((DefaultMutableTreeNode)((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getFirstChild()).getUserObject());
                    int firstSuiteIndex =   oldSuiteList.indexOf(((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getUserObject());

                    try {
                        // BdD
                        Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
                        Family pFamilyUp = (Family)nodeToGoUp.getUserObject();
                        //Family pFamilyDown = (Family)nodeToGoUp.getUserObject();

                        camp.updateTestFamilyOrderInDB(pFamilyUp.getIdBdd(),false);

                        // IHM
                        camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());
                        /*for (int j = 0; j < pFamilyUp.getSuiteListFromModel().size(); j++) {
                          TestList testList = (TestList)pFamilyUp.getSuiteListFromModel().get(j);
                          camp.getSuitListFromModel().remove(pFamilyUp.getSuiteListFromModel().get(j));
                          for (int k = 0; k < testList.getTestListFromModel().size(); k++) {
                          camp.getTestListFromModel().remove(testList.getTestListFromModel().get(k));

                          }
                          }
                          int addIndex = firstTestIndex;
                          for (int j = 0; j < pFamilyUp.getSuiteListFromModel().size(); j++) {
                          TestList testList = (TestList)pFamilyUp.getSuiteListFromModel().get(j);
                          camp.getSuitListFromModel().add((j+firstSuiteIndex), pFamilyUp.getSuiteListFromModel().get(j));
                          for (int k = 0; k < testList.getTestListFromModel().size(); k++) {
                          camp.getTestListFromModel().add((k+addIndex), testList.getTestListFromModel().get(k));
                          addIndex++;
                          }
                          }*/


                        workingModel.removeNodeFromParent(nodeToGoUp);
                        workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode(), selectedIndex - 1);
                        //                                              campaignFamilyOrderModified.add(camp);
                        //camp.getFamilyListFromModel().remove(pFamilyUp);
                        //camp.getFamilyListFromModel().add((selectedIndex-1),pFamilyUp);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }

                } else {

                    try {
                        //Decrementation du selectionne
                        ((Family)((DefaultMutableTreeNode)objToGoUp).getUserObject()).updateOrderInBddAndModel(false);

                        //IHM
                        nodeToGoUp = (DefaultMutableTreeNode)((DefaultMutableTreeNode)workingModel.getRoot()).getChildAt(selectedIndex);
                        Family pFamilyUp = (Family)nodeToGoUp.getUserObject();
                        workingModel.removeNodeFromParent(nodeToGoUp);
                        workingModel.insertNodeInto(nodeToGoUp, ((DefaultMutableTreeNode)workingModel.getRoot()), selectedIndex - 1);
                        DataModel.getCurrentProject().getFamilyListFromModel().remove(pFamilyUp);
                        DataModel.getCurrentProject().getFamilyListFromModel().add((selectedIndex-1), pFamilyUp);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                }
            } else if (testListButton.isSelected()) {
                if(campaignMode){
                    moveTestListInCampaign(selectedIndex);
                } else {
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getChildAt(selectedIndex);
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getChildAt(selectedIndex - 1);
                    if (((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode() == null) {

                        try {
                            ((TestList)nodeToGoUp.getUserObject()).updateOrderInDBAndModel(false);

                            //IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            Family pFamily = (Family)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getUserObject();
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode(), selectedIndex - 1);
                            pFamily.getSuiteListFromModel().remove(nodeToGoUp.getUserObject());
                            pFamily.getSuiteListFromModel().add((selectedIndex-1), nodeToGoUp.getUserObject());
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else {
                        ArrayList oldTestList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getTestListFromModel();
                        // On recupere l'indice du premier test de la suite qui descend
                        int firstIndex = oldTestList.indexOf(((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getUserObject());
                        int i = 0;
                        if (firstIndex != -1) {

                            try {
                                // BdD
                                Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
                                camp.updateTestSuiteOrderInDB(((TestList)nodeToGoUp.getUserObject()).getIdBdd(), false);

                                // IHM
                                camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());

                                workingModel.removeNodeFromParent(nodeToGoUp);
                                workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode(), selectedIndex - 1);

                            } catch (Exception exception) {
                                Tools.ihmExceptionView(exception);
                            }

                        }
                    }
                }
            } else if (testButton.isSelected()) {
                //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                if (campaignMode) {
                    moveTestInCampaign(selectedIndex);
                } else {
                    //20100108 - Fin modification Forge ORTF V1.0.0
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getChildAt(selectedIndex);
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getChildAt(selectedIndex - 1);
                    if (((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode() == null) {
                        try {
                            TestList pSuite = (TestList)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getUserObject();
                            Test pTest = (Test)nodeToGoUp.getUserObject();
                            //BDD
                            pTest.updateOrderInDBAndModel(false);

                            //IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode(), selectedIndex - 1);
                            pSuite.getTestListFromModel().remove(pTest);
                            pSuite.getTestListFromModel().add((selectedIndex-1), pTest);
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else {
                        try {
                            // BdD
                            Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
                            camp.updateTestOrderInDB(((Test)nodeToGoUp.getUserObject()).getIdBdd(),false);

                            // IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode(), selectedIndex - 1);
                            camp.getTestListFromModel().remove(nodeToGoUp.getUserObject());
                            camp.getTestListFromModel().add((selectedIndex-1), nodeToGoUp.getUserObject());

                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    }
                }
            }
        }
    }


    void downPerformed(ActionEvent e) {
        int selectedIndex = list.getSelectedIndex();
        if (selectedIndex != -1) {
            Object objToGoUp = listModel.getElementAt(selectedIndex +1);
            Object objToGoDown = listModel.getElementAt(selectedIndex);
            listModel.setElementAt(objToGoUp, selectedIndex);
            listModel.setElementAt(objToGoDown, selectedIndex + 1);
            list.setSelectedIndex(selectedIndex + 1);
            DefaultMutableTreeNode nodeToGoUp = null;
            DefaultMutableTreeNode nodeToGoDown = null;
            if (campagneButton.isSelected()) {
                nodeToGoUp = (DefaultMutableTreeNode)((DefaultMutableTreeNode)workingModel.getRoot()).getChildAt(selectedIndex + 1);

                try {
                    // BdD
                    ((Campaign)((DefaultMutableTreeNode)objToGoDown).getUserObject()).updateOrderInDBAndModel(true);

                    // IHM
                    workingModel.removeNodeFromParent(nodeToGoUp);
                    workingModel.insertNodeInto(nodeToGoUp, ((DefaultMutableTreeNode)workingModel.getRoot()), selectedIndex);
                    DataModel.getCurrentProject().getCampaignListFromModel().remove(nodeToGoUp.getUserObject());
                    DataModel.getCurrentProject().getCampaignListFromModel().add((selectedIndex), nodeToGoUp.getUserObject());

                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }

            } else if (familyButton.isSelected()) {
                if (campaignMode) {
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode().getChildAt(selectedIndex);
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode().getChildAt(selectedIndex+1);
                    ArrayList oldTestList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getTestListFromModel();
                    ArrayList oldSuiteList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getSuitListFromModel();
                    int firstTestIndex = oldTestList.indexOf(((DefaultMutableTreeNode)((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getFirstChild()).getUserObject());
                    int firstSuiteIndex =   oldSuiteList.indexOf(((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getUserObject());


                    try {
                        // BdD
                        Campaign camp = (Campaign)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode().getUserObject();
                        Family pFamilyDown = (Family)nodeToGoDown.getUserObject();
                        Family pFamilyUp = (Family)nodeToGoUp.getUserObject();

                        camp.updateTestFamilyOrderInDB(pFamilyDown.getIdBdd(),true);

                        // IHM
                        //ArrayList testInModel = DataModel.getCurrentProject().getAlltestFromModel();
                        camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());
                        workingModel.removeNodeFromParent(nodeToGoUp);
                        workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode(), selectedIndex);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }

                } else {

                    try {
                        //IHM
                        ((Family)((DefaultMutableTreeNode)objToGoDown).getUserObject()).updateOrderInBddAndModel(true);

                        //BDD
                        nodeToGoUp = (DefaultMutableTreeNode)((DefaultMutableTreeNode)workingModel.getRoot()).getChildAt(selectedIndex + 1);
                        workingModel.removeNodeFromParent(nodeToGoUp);
                        workingModel.insertNodeInto(nodeToGoUp, ((DefaultMutableTreeNode)workingModel.getRoot()), selectedIndex);
                        Family family = (Family)nodeToGoUp.getUserObject();
                        DataModel.getCurrentProject().getFamilyListFromModel().remove(family);
                        DataModel.getCurrentProject().getFamilyListFromModel().add((selectedIndex), family);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                }
            } else if (testListButton.isSelected()) {
                //20100215 - D\ufffdbut modification Forge ORTF V1.0.0
                if(campaignMode){
                    moveTestListInCampaign(selectedIndex + 1);
                } else {
                    //20100215 - Fin modification Forge ORTF V1.0.0
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getChildAt(selectedIndex + 1);
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getChildAt(selectedIndex);
                    if (((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode() == null) {

                        try {
                            //BDD
                            Family pFamily = (Family)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode().getUserObject();
                            ((TestList)nodeToGoDown.getUserObject()).updateOrderInDBAndModel(true);

                            //IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode(), selectedIndex);
                            pFamily.getSuiteListFromModel().remove(nodeToGoUp.getUserObject());
                            pFamily.getSuiteListFromModel().add((selectedIndex), nodeToGoUp.getUserObject());
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else {
                        ArrayList oldTestList = ((Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject()).getTestListFromModel();
                        // On r?cup?re l'indice du premier test de la suite qui descend
                        int firstIndex = oldTestList.indexOf(((DefaultMutableTreeNode)nodeToGoDown.getFirstChild()).getUserObject());
                        int i = 0;
                        if (firstIndex != -1) {
                            try {
                                // BdD
                                Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
                                TestList pSuiteUp = (TestList)nodeToGoUp.getUserObject();
                                camp.updateTestSuiteOrderInDB(((TestList)nodeToGoDown.getUserObject()).getIdBdd(), true);

                                // IHM
                                camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());
                                workingModel.removeNodeFromParent(nodeToGoUp);
                                workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode(), selectedIndex);
                            } catch (Exception exception) {
                                Tools.ihmExceptionView(exception);
                            }
                        }
                    }
                }
            } else if (testButton.isSelected()) {
                //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                if (campaignMode) {
                    moveTestInCampaign(selectedIndex +1);
                } else {
                    //20100108 - Fin modification Forge ORTF V1.0.0
                    nodeToGoUp = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getChildAt(selectedIndex + 1);
                    nodeToGoDown = (DefaultMutableTreeNode)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getChildAt(selectedIndex);
                    if (((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode() == null) {
                        try {
                            //BDD
                            Test pTestDown = (Test)nodeToGoDown.getUserObject();
                            Test pTestUp = (Test)nodeToGoUp.getUserObject();
                            TestList pSuite = (TestList)((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode().getUserObject();
                            pTestDown.updateOrderInDBAndModel(true);

                            //IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode(), selectedIndex);
                            pSuite.getTestListFromModel().remove(pTestUp);
                            pSuite.getTestListFromModel().add((selectedIndex), pTestUp);

                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }

                    } else {


                        try {
                            // BdD
                            Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
                            camp.updateTestOrderInDB(((Test)nodeToGoDown.getUserObject()).getIdBdd(),true);

                            // IHM
                            workingModel.removeNodeFromParent(nodeToGoUp);
                            workingModel.insertNodeInto(nodeToGoUp, ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getTestListNode(), selectedIndex);
                            camp.getTestListFromModel().remove(nodeToGoUp.getUserObject());
                            camp.getTestListFromModel().add((selectedIndex), nodeToGoUp.getUserObject());

                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    }
                }
            }
        }
    }

    //20100215 - D\ufffdbut modification Forge ORTF V1.0.0
    private void moveTestListInCampaign(int selectedIndex){
        DefaultMutableTreeNode familyNode = ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode();
        List allTestListTest = new ArrayList();
        for (int k = 0; k < familyNode.getChildCount(); k++) {
            allTestListTest.add(familyNode.getChildAt(k));
        }
        DefaultMutableTreeNode nodeToGoUp = (DefaultMutableTreeNode)allTestListTest.get(selectedIndex);
        DefaultMutableTreeNode nodeToGoDown = (DefaultMutableTreeNode)allTestListTest.get(selectedIndex - 1);
        try {
            // BdD
            Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
            DefaultMutableTreeNode child;
            int size = nodeToGoDown.getChildCount();
            System.out.println("Down child cpunt = " + size);
            for(int i = 0; i < nodeToGoUp.getChildCount(); i++){
                child = (DefaultMutableTreeNode) nodeToGoUp.getChildAt(i);
                System.out.println("Update order of " + child);
                for(int j = 0; j < size; j++){
                    camp.updateTestOrderInDB(((Test)child.getUserObject()).getIdBdd(),false);
                }
            }
            camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());
            // IHM
            boolean upDone = false;
            if(selectedIndex > 1 ){
                DefaultMutableTreeNode nodeUp = (DefaultMutableTreeNode)allTestListTest.get(selectedIndex - 2);
                if(nodeUp.getUserObject().equals(nodeToGoUp.getUserObject())){
                    for(int i= 0; i < nodeToGoUp.getChildCount(); i++){
                        workingModel.insertNodeInto((DefaultMutableTreeNode)nodeToGoUp.getChildAt(i), nodeUp, nodeUp.getChildCount());
                    }
                    upDone = true;
                }
            }
            if(allTestListTest.size() > selectedIndex + 1){
                DefaultMutableTreeNode nodeDown = (DefaultMutableTreeNode)allTestListTest.get(selectedIndex +1);
                if(nodeDown.getUserObject().equals(nodeToGoDown.getUserObject())){
                    for(int i= 0; i < nodeDown.getChildCount(); i++){
                        workingModel.insertNodeInto((DefaultMutableTreeNode)nodeDown.getChildAt(i), nodeToGoDown, nodeToGoDown.getChildCount());
                    }
                    workingModel.removeNodeFromParent(nodeDown);
                    listModel.remove(selectedIndex +1);
                }
            }
            workingModel.removeNodeFromParent(nodeToGoUp);
            if(!upDone){
                workingModel.insertNodeInto(nodeToGoUp, (MutableTreeNode) nodeToGoDown.getParent(), selectedIndex -1);
            } else {
                listModel.remove(selectedIndex - 1);
            }
        } catch (Exception exception) {
            Tools.ihmExceptionView(exception);
        }
    }
    //20100215 - Fin modification Forge ORTF V1.0.0

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    /**
     * Move up the test from the selected index.
     * @method moveTestInCampaign(int selectedIndex)
     * @param selectedIndex
     */
    private void moveTestInCampaign(int selectedIndex){
        DefaultMutableTreeNode familyNode = ((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getFamilyNode();
        List allFamilyTest = new ArrayList();
        DefaultMutableTreeNode testSuiteNode;
        for (int k = 0; k < familyNode.getChildCount(); k++) {
            testSuiteNode = (DefaultMutableTreeNode)familyNode.getChildAt(k);
            for (int j = 0; j < testSuiteNode.getChildCount(); j++) {
                allFamilyTest.add(testSuiteNode.getChildAt(j));
            }
        }
        DefaultMutableTreeNode nodeToGoUp = (DefaultMutableTreeNode)allFamilyTest.get(selectedIndex);
        DefaultMutableTreeNode nodeToGoDown = (DefaultMutableTreeNode)allFamilyTest.get(selectedIndex - 1);
        try {
            // BdD
            Campaign camp = (Campaign)(((TestListFamilyCampaignTreeNode)choiceComboBox.getSelectedItem()).getCampaignNode()).getUserObject();
            camp.updateTestOrderInDB(((Test)nodeToGoUp.getUserObject()).getIdBdd(),false);

            camp.reloadTestCampain(DataModel.getCurrentProject().getAlltestFromModel());
            // IHM
            DefaultMutableTreeNode parentToGoUp = (DefaultMutableTreeNode) nodeToGoUp.getParent();
            DefaultMutableTreeNode parentToGoDown = (DefaultMutableTreeNode) nodeToGoDown.getParent();
            if(parentToGoUp.equals(parentToGoDown)){
                System.out.println("SAME PARENT OK");
                workingModel.removeNodeFromParent(nodeToGoUp);
                workingModel.insertNodeInto(nodeToGoUp, parentToGoUp, selectedIndex - 1);
            } else {
                DefaultMutableTreeNode granParent = (DefaultMutableTreeNode) parentToGoUp.getParent();
                int insertIndex = granParent.getIndex(parentToGoUp);
                boolean upDone = false;
                System.out.println("remove toGoup test");
                workingModel.removeNodeFromParent(nodeToGoUp);
                if(parentToGoUp.getChildCount() == 0){
                    System.out.println("remove toGoUp Parent test");
                    workingModel.removeNodeFromParent(parentToGoUp);
                }
                if(selectedIndex > 1){
                    DefaultMutableTreeNode upParent = (DefaultMutableTreeNode) ((DefaultMutableTreeNode)allFamilyTest.get(selectedIndex - 2)).getParent();
                    if(upParent.getUserObject().equals(parentToGoUp.getUserObject())){
                        System.out.println("toGoUp test same target parent -> combine");
                        workingModel.insertNodeInto(nodeToGoUp, upParent, upParent.getChildCount());
                        upDone = true;
                    }
                }
                if(!upDone) {
                    DefaultMutableTreeNode newParent = (DefaultMutableTreeNode) parentToGoUp.clone();
                    newParent.removeAllChildren();
                    System.out.println("Insert new up parent at index " + insertIndex);
                    workingModel.insertNodeInto(newParent, granParent, insertIndex);
                    workingModel.insertNodeInto(nodeToGoUp, newParent, 0);
                    insertIndex = insertIndex + 1;
                }
                boolean downDone = false;
                System.out.println("remove toGoDown test");
                workingModel.removeNodeFromParent(nodeToGoDown);
                if(parentToGoDown.getChildCount() == 0){
                    System.out.println("remove toGoDown Parent test");
                    workingModel.removeNodeFromParent(parentToGoDown);
                    insertIndex = insertIndex -1;
                }
                if(allFamilyTest.size() > selectedIndex + 1){
                    DefaultMutableTreeNode downParent = (DefaultMutableTreeNode) ((DefaultMutableTreeNode)allFamilyTest.get(selectedIndex +1)).getParent();
                    if(downParent.getUserObject().equals(parentToGoDown.getUserObject())){
                        System.out.println("toGoDown test same target parent -> combine");
                        workingModel.insertNodeInto(nodeToGoDown, downParent, 0);
                        downDone = true;
                    }
                }
                if(!downDone){
                    DefaultMutableTreeNode newParent = (DefaultMutableTreeNode) parentToGoDown.clone();
                    newParent.removeAllChildren();
                    System.out.println("Insert new down parent at index " + insertIndex);
                    workingModel.insertNodeInto(newParent, granParent, insertIndex);
                    workingModel.insertNodeInto(nodeToGoDown, newParent, 0);
                }
            }
        } catch (Exception exception) {
            Tools.ihmExceptionView(exception);
        }
    }
    //20100108 - Fin modification Forge ORTF V1.0.0

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(list)){
            listChanged(e);
        }
    }


    void listChanged(ListSelectionEvent e) {
        int selectedIndex = list.getSelectedIndex();
        if (selectedIndex != - 1) {
            if (selectedIndex == 0) {
                upButton.setEnabled(false);
                if (selectedIndex == listModel.getSize()-1) {
                    downButton.setEnabled(false);
                } else {
                    downButton.setEnabled(true);
                }
            } else if (selectedIndex == listModel.getSize()-1) {
                upButton.setEnabled(true);
                downButton.setEnabled(false);
            } else if (selectedIndex == listModel.getSize()) {
                upButton.setEnabled(false);
                downButton.setEnabled(false);
            } else {
                upButton.setEnabled(true);
                downButton.setEnabled(true);
            }
        } else {
            upButton.setEnabled(false);
            downButton.setEnabled(false);
        }
    }


} // Fin de la classe Ordering
