package org.objectweb.salome_tmf.ihm.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Properties;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import javax.jnlp.BasicService;
import javax.jnlp.ServiceManager;
import javax.jnlp.UnavailableServiceException;

import org.objectweb.salome_tmf.api.Util;

public class SalomeJWS_BootStrap {
    URL urlSalome;
    BasicService bs; 
        
    String tmpDir = System.getProperties().getProperty("java.io.tmpdir");
    String fs = System.getProperties().getProperty("file.separator");
    Properties salomeProperties = new Properties(); 
    String propertiesFile ;
         
    String commons_jars = "commons-logging.jar, jfreechart-1.0.1.jar, log4j-1.2.6.jar, driver.jar, jcommon-1.0.0.jar, jpf-0.3.jar, pg74.216.jdbc3.jar ";
    String jar_salome = "salome_tmf_data.jar,salome_tmf_api.jar,salome_tmf_ihm.jar,salome_tmf_sqlengine.jar,salome_tmf_coreplug.jar,salome_tmf_login.jar,salome_tmf_tools.jar";
    String htmlfiles ="index.html, Salome.html, Salome2.html";
    String configs ="cfg/DB_Connexion.properties,cfg/key.txt";
    
    String install_path ="SalomeV2";
    
    SalomeJWS_BootStrap(String installDir){
        bootStrap(installDir);
    }
   
    void bootStrap(String installDir){
        try {  
            /*JFrame f = new JFrame();
              f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
              f.show();
            */
            if (installDir != null){
                install_path += fs + installDir;
            }
            bs = (BasicService)ServiceManager.lookup("javax.jnlp.BasicService");
            urlSalome = bs.getCodeBase();
            propertiesFile  = tmpDir + fs + install_path +  fs +"cfg/DB_Connexion.properties";
                           
            Util.debug("Installed Salome is   = " + urlSalome);
            Util.debug("Installation Salome is   = " + install_path);
            if (create_salomeInstall()){
                /* Install Resources  */
                installRessource();
            } else {
                /*  Install Resources if needded HTML */
                updateRessource();
            }
            updateProperties();
                  
            /* Start Salome TMF */
            displayURL(new File(tmpDir + fs + install_path +  fs + "Salome2.html").toURL().toString());
                  
            /* Exit boot Strap */
            System.exit(0);
                   
        } catch (Exception exc) {
            Util.err(exc);
            System.exit(0);
        }
    }
   
   
    /****************************** Properties ****************************************/
    void updateProperties() throws Exception{
        if (loadPluginPropertie()){ 
            savePluginPropertie();
        }
    }
   
    boolean loadPluginPropertie() throws Exception{
        boolean needUpdate = false;
        FileInputStream ins = new FileInputStream(propertiesFile);
        salomeProperties.load(ins);
        ins.close();
        try {
            String SalomeCodeBase = salomeProperties.getProperty("CodeBase");
            if (!(SalomeCodeBase.trim().equals(urlSalome.toString().trim()))){
                salomeProperties.setProperty("CodeBase", urlSalome.toString().trim());  
                needUpdate = true;
                Util.debug("Code base in properties file"+ SalomeCodeBase + "need Update");
            }else {
                needUpdate = false; 
                Util.debug("Code base in properties file is OK");
            }
        } catch (Exception e){
            needUpdate = true;
            salomeProperties.setProperty("CodeBase", urlSalome.toString().trim());  
            Util.debug("Code base in properties file need Update with" + urlSalome.toString().trim() );
        }
        return needUpdate;
    }
   
    void savePluginPropertie() throws Exception{
                
        FileOutputStream out = new FileOutputStream(propertiesFile);
        salomeProperties.store(out,"");
        out.close();
    }
   
    /******************************* JAR/HTML ******************************************/
    void updateRessource() throws Exception {
        String[] jar_list = jar_salome.split(",");
        for (int i=0; i< jar_list.length; i++){
            String url =  urlSalome.toString()  + jar_list[i].trim();
                 
            String urlRemote =  "jar:" +   urlSalome.toString() ;
            urlRemote += jar_list[i].trim() + "!/";
            File fileLocal = new File(tmpDir + fs + install_path +  fs + jar_list[i].trim());
            if(needUpdate(urlRemote, fileLocal)){
                installFile(new URL(url), jar_list[i].trim());
            }
        }
    }
   
    boolean needUpdate(String remoteUrl, File localFile){
        boolean ret = false;
        try {
            URL jar = new URL(remoteUrl);
            JarURLConnection jarConnection = (JarURLConnection)jar.openConnection();
            Manifest remoteManifest = jarConnection.getManifest();
                   
            JarFile localeJar = new JarFile(localFile);
            Manifest localManifest = localeJar.getManifest();
                   
            if (!remoteManifest.equals(localManifest)){
                Util.debug("Need to Update : " + localFile);
                ret = true;
            } else {
                Util.debug("No Update for : " + localFile);
            }
                   
            //Util.debug("JarManifest  = " + manifest + " for " + jar);
        }catch (Exception e){
            ret = true;
        }
        return ret;
    }
   
   
    void installRessource( ) throws Exception {
        /* Les Jars*/
        String[] jar_list = commons_jars.split(",");
        for (int i=0; i< jar_list.length; i++){
            String urlJar = urlSalome.toString()  + jar_list[i].trim();
            Util.debug("Install JAR  = " + urlJar);
            URL url = new URL(urlJar);
            try {
                installFile(url, jar_list[i].trim());
            } catch(Exception e){
                         
            }
        }
           
        jar_list = jar_salome.split(",");
        for (int i=0; i< jar_list.length; i++){
            String urlJar = urlSalome.toString()  + jar_list[i].trim();
            Util.debug("Install JAR  = " + urlJar);
            URL url = new URL(urlJar);
            installFile(url, jar_list[i].trim());
        }
           
        /*Les htmls*/
        String[] text_files = htmlfiles.split(",");
        for (int i=0; i< text_files.length; i++){
            String urlfile =  urlSalome.toString()  + text_files[i].trim();
            URL url = new URL(urlfile); 
            Util.debug("Install HTML  = " + url);
            installFile(url, text_files[i].trim());
        }
        /*Les config*/
        String[] config_files = configs.split(",");
        for (int i=0; i< config_files.length; i++){
            String urlfile =  urlSalome.toString()  + config_files[i].trim();
            URL url = new URL(urlfile);
            Util.debug("Install Config  = " + url );
            installFile(url, config_files[i].trim());
        }
    }
   
    void installFile(URL pUrl, String name) throws Exception{
        try {
            URLConnection urlconn = pUrl.openConnection();
            InputStream is = urlconn.getInputStream();
            File file = new File(tmpDir + fs + install_path +fs + name);
            Util.debug("Write file " + file);
            writeStream(is, file);
        }catch (Exception  e){
            Util.err(e);
        }
    }
   
    void writeStream( InputStream is , File f) throws Exception{
        byte[] buf = new byte [102400];
        f.createNewFile();
        FileOutputStream fos = new FileOutputStream (f); 
        int len = 0;
        while ((len = is.read (buf)) != -1){
            fos.write (buf, 0, len);
        }
        fos.close();
        is.close();
    }
   
    boolean showDocument(URL toShow ){
        boolean ret = false;
        try {
            // Lookup the javax.jnlp.BasicService object
            BasicService bs = (BasicService)ServiceManager.lookup("javax.jnlp.BasicService");
            // Invoke the showDocument method
            ret = bs.showDocument(toShow);
         
        } catch(UnavailableServiceException ue) {
            // Service is not supported
        } 
        return ret;
    }
   
    public static void main(String arg[]){
        String install_dir = null;
        if (arg.length > 0){
            install_dir = arg[0];
        }
        new SalomeJWS_BootStrap(install_dir);
    }
   
   
   
    /**********************************************************************************************/
  
   
    boolean create_salomeInstall() throws Exception{
        boolean ret = false;
        File file = new File(tmpDir + fs + install_path);
        if (!file.exists()){
            file.mkdirs();
            ret = true;
        }
        file = new File(tmpDir + fs + install_path +  fs +"cfg");
        if (!file.exists()){
            file.mkdirs();
        }
        return ret;
    }
   
    /*************************************************************************************************/
    public boolean displayURL(String url) {

        // Opening a browser, even when running sandbox-restricted
        // in JavaWebStart.
        Util.debug("Try to open url with differents methode: " + url);
                
                
                        
        try {
            if (showDocument(new URL(url))){
                Util.debug("Url open with JavaWebStart");
                return true;
            }
        } catch (Exception e) {
            Util.err(e);
            // Not running in JavaWebStart or service is not supported.
            // We continue with the methods below ...
        }

        //String[] cmd = null;

        switch (getPlatform()) {
        case (WIN_ID): {
            Util.debug("Try Windows Command Line");
            return runCmdLine(replaceToken(WIN_CMDLINE, URLTOKEN, url));
        }
        case (MAC_ID): {
            Util.debug("Try Mac Command Line");
            return runCmdLine(replaceToken(MAC_CMDLINE, URLTOKEN, url));
        }
        default:
            Util.debug("Try Unix Command Line");
            for (int i = 0; i < OTHER_CMDLINES.length; i++) {
                if (runCmdLine(replaceToken(OTHER_CMDLINES[i], URLTOKEN, url),
                               replaceToken(OTHER_FALLBACKS[i], URLTOKEN, url)))
                    return true;
            }
        }

        return false;
    }

    /**
     * Try to determine whether this application is running under Windows or
     * some other platform by examing the "os.name" property.
     * 
     * @return the ID of the platform
     */
    private int getPlatform() {
        String os = System.getProperty("os.name");
        if (os != null && os.startsWith(WIN_PREFIX))
            return WIN_ID;
        if (os != null && os.startsWith(MAC_PREFIX))
            return MAC_ID;
        return OTHER_ID;
    }


    private String[] replaceToken(String[] target, String token,
                                  String replacement) {
        if (null == target)
            return null;
        String[] result = new String[target.length];

        for (int i = 0; i < target.length; i++)
            result[i] = target[i].replaceAll(token, replacement);

        return result;
    }

    private boolean runCmdLine(String[] cmdLine) {
        Util.debug("Try to execute commande line    = " + cmdLine);
        return runCmdLine(cmdLine, null);
    }

    private boolean runCmdLine(String[] cmdLine, String[] fallBackCmdLine) {
        try {
            Util.debug("Try to execute commande line    = " + cmdLine + " with " +fallBackCmdLine );
            /*
             * Util.debug( "Trying to invoke browser, cmd='" +
             * connectStringArray(cmdLine) + "' ... ");
             */
            Process p = Runtime.getRuntime().exec(cmdLine);

            if (null != fallBackCmdLine) {
                // wait for exit code -- if it's 0, command worked,
                // otherwise we need to start fallBackCmdLine.
                int exitCode = p.waitFor();
                if (exitCode != 0) {
                    /*
                     * Util.debug(exitCode); Util.debug();
                     */

                    /*
                     * Util.debug( "Trying to invoke browser, cmd='" +
                     * connectStringArray(fallBackCmdLine) + "' ...");
                     */
                    Runtime.getRuntime().exec(fallBackCmdLine);

                }
            }

            Util.debug();
            return true;

        } catch (InterruptedException e) {
            Util.debug("Caught: " + e);
        } catch (IOException e) {
            Util.debug("Caught: " + e);
        }

        return false;
    }

    // This token is a placeholder for the actual URL
    private final String URLTOKEN = "%URLTOKEN%";

    // Used to identify the windows platform.
    private final int WIN_ID = 1;

    // Used to discover the windows platform.
    private final String WIN_PREFIX = "Windows";

    // The default system browser under windows.
    // Once upon a time:
    //   for 'Windows 9' and 'Windows M': start
    //   for 'Windows': cmd /c start
    private final String[] WIN_CMDLINE = { "rundll32",
                                           "url.dll,FileProtocolHandler", URLTOKEN };

    // Used to identify the mac platform.
    private final int MAC_ID = 2;

    // Used to discover the mac platform.
    private final String MAC_PREFIX = "Mac";

    // The default system browser under mac.
    private  final String[] MAC_CMDLINE = { "open", URLTOKEN };

    // Used to identify the mac platform.
    private  final int OTHER_ID = -1;

    private  final String[][] OTHER_CMDLINES = {

        // The first guess for a browser under other systems (and unix):
        // Remote controlling firefox
        // (http://www.mozilla.org/unix/remote.html)
        { "firefox", "-remote", "openURL(" + URLTOKEN + ",new-window)" },
                        
        //      Remote controlling mozilla
        // (http://www.mozilla.org/unix/remote.html)
        { "mozilla", "-remote", "openURL(" + URLTOKEN + ",new-window)" },
        // The second guess for a browser under other systems (and unix):
        // The RedHat skript htmlview
        { "htmlview", URLTOKEN },

        // The third guess for a browser under other systems (and unix):
        // Remote controlling netscape
        // (http://wp.netscape.com/newsref/std/x-remote.html)
        { "netscape", "-remote", "openURL(" + URLTOKEN + ")" }

    };

    private  final String[][] OTHER_FALLBACKS = {

        // Fallback for remote controlling mozilla:
        //Starting up a new mozilla
        { "firefox", URLTOKEN },
                        
        // Starting up a new mozilla
        { "mozilla", URLTOKEN },

        // No fallback for htmlview
        null,

        // Fallback for remote controlling netscape:
        // Starting up a new netscape
        { "netscape", URLTOKEN }

    };

        
}
