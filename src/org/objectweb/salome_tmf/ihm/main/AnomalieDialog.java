package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.ihm.languages.Language;

public class AnomalieDialog extends JDialog implements ActionListener{
    JPanel contentPan;
    JButton closeButton;
        
    AnomalieDialog(){
        super(SalomeTMFContext.getInstance().getSalomeFrame(), true);
        setTitle(Language.getInstance().getText("Anomalies"));
        setModal(true);
        contentPan = new JPanel(new BorderLayout());
        closeButton = new JButton(Language.getInstance().getText("Fermer"));
        closeButton.addActionListener(this);
        
        AnomalieView pAnomalieView = new AnomalieView(this);
        contentPan.add(pAnomalieView, BorderLayout.CENTER);
        contentPan.add(closeButton, BorderLayout.SOUTH);
        setContentPane(contentPan); 
        //this.setLocation(400,300);
        /*pack();
          this.setLocationRelativeTo(this.getParent()); 
          setVisible(true);*/
        centerScreen();
    }
        
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
                 
        this.setVisible(true); 
        requestFocus();
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(closeButton)){
            dispose();
        }  
    }
}
