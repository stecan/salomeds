/*
  GNU Lesser General Public License

  EkitCore - Base Java Swing HTML Editor & Viewer Class (Core)
  Copyright (C) 2000 Howard Kistler

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

package org.objectweb.salome_tmf.ihm.main.htmleditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.ChangedCharSetException;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.JTextComponent;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.PlainDocument;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import javax.swing.text.rtf.RTFEditorKit;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.htmleditor.action.ListAutomationAction;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.ExtendedHTMLDocument;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.ExtendedHTMLEditorKit;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.HTMLUtilities;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.ImageFileChooser;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.ImageURLDialog;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.JButtonNoFocus;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.JComboBoxNoFocus;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.JToggleButtonNoFocus;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.MutableFilter;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.PropertiesDialog;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.SearchDialog;
import org.objectweb.salome_tmf.ihm.main.htmleditor.component.SimpleInfoDialog;
import org.objectweb.salome_tmf.ihm.main.htmleditor.print.DocumentRenderer;
import org.objectweb.salome_tmf.ihm.main.htmleditor.util.Base64Codec;

/**
 * EkitCore
 * Main application class for editing and saving HTML in a Java text component
 *
 * @author Howard Kistler
 * @version 1.1
 *
 * REQUIREMENTS
 * Java 2 (JDK 1.3 or 1.4)
 * Swing Library
 */

public class EkitCore extends JPanel implements ActionListener, KeyListener, FocusListener, DocumentListener {

    /* Components */
    private JSplitPane jspltDisplay;
    private JTextPane jtpMain;
    private ExtendedHTMLEditorKit htmlKit;
    private ExtendedHTMLDocument htmlDoc;
    private StyleSheet styleSheet;
    private JTextArea jtpSource;
    private JScrollPane jspSource;
    private JToolBar jToolBar;
    private JToolBar jToolBarMain;
    private JToolBar jToolBarFormat;
    private JToolBar jToolBarStyles;

    private JButtonNoFocus jbtnNewHTML;
    private JButtonNoFocus jbtnOpenHTML;
    private JButtonNoFocus jbtnSaveHTML;
    private JButtonNoFocus jbtnPrint;
    private JButtonNoFocus jbtnCut;
    private JButtonNoFocus jbtnCopy;
    private JButtonNoFocus jbtnPaste;
    private JButtonNoFocus jbtnUndo;
    private JButtonNoFocus jbtnRedo;
    private JButtonNoFocus jbtnBold;
    private JButtonNoFocus jbtnItalic;
    private JButtonNoFocus jbtnUnderline;
    //private JButtonNoFocus jbtnStrike;
    private JButtonNoFocus jbtnUList;
    private JButtonNoFocus jbtnOList;
    private JButtonNoFocus jbtnAlignLeft;
    private JButtonNoFocus jbtnAlignCenter;
    private JButtonNoFocus jbtnAlignRight;
    private JButtonNoFocus jbtnAlignJustified;
    private JButtonNoFocus jbtnFind;
    private JButtonNoFocus jbtnInsertTable;
    private JButtonNoFocus jbtnEditTable;
    private JButtonNoFocus jbtnEditCell;
    private JButtonNoFocus jbtnInsertRow;
    private JButtonNoFocus jbtnInsertColumn;
    private JButtonNoFocus jbtnDeleteRow;
    private JButtonNoFocus jbtnDeleteColumn;
    private JToggleButtonNoFocus jtbtnViewSource;

    //private Frame frameHandler;

    private HTMLUtilities htmlUtilities = new HTMLUtilities(this);

    /* Actions */
    private StyledEditorKit.BoldAction actionFontBold;
    private StyledEditorKit.ItalicAction actionFontItalic;
    private StyledEditorKit.UnderlineAction actionFontUnderline;
    private ListAutomationAction actionListUnordered;
    private ListAutomationAction actionListOrdered;
    private StyledEditorKit.AlignmentAction actionAlignLeft;
    private StyledEditorKit.AlignmentAction actionAlignCenter;
    private StyledEditorKit.AlignmentAction actionAlignRight;
    private StyledEditorKit.AlignmentAction actionAlignJustified;

    protected UndoManager undoMngr;
    protected UndoAction undoAction;
    protected RedoAction redoAction;


    // Tool Keys
    public static final String KEY_TOOL_SEP       = "SP";
    public static final String KEY_TOOL_NEW       = "NW";
    public static final String KEY_TOOL_OPEN      = "OP";
    public static final String KEY_TOOL_SAVE      = "SV";
    public static final String KEY_TOOL_PRINT     = "PR";
    public static final String KEY_TOOL_CUT       = "CT";
    public static final String KEY_TOOL_COPY      = "CP";
    public static final String KEY_TOOL_PASTE     = "PS";
    public static final String KEY_TOOL_UNDO      = "UN";
    public static final String KEY_TOOL_REDO      = "RE";
    public static final String KEY_TOOL_BOLD      = "BL";
    public static final String KEY_TOOL_ITALIC    = "IT";
    public static final String KEY_TOOL_UNDERLINE = "UD";
    //public static final String KEY_TOOL_STRIKE    = "SK";
    public static final String KEY_TOOL_ULIST     = "UL";
    public static final String KEY_TOOL_OLIST     = "OL";
    public static final String KEY_TOOL_ALIGNL    = "AL";
    public static final String KEY_TOOL_ALIGNC    = "AC";
    public static final String KEY_TOOL_ALIGNR    = "AR";
    public static final String KEY_TOOL_ALIGNJ    = "AJ";
    public static final String KEY_TOOL_FIND      = "FN";
    public static final String KEY_TOOL_SOURCE    = "SR";
    public static final String KEY_TOOL_INSTABLE  = "TI";
    public static final String KEY_TOOL_EDITTABLE = "TE";
    public static final String KEY_TOOL_EDITCELL  = "CE";
    public static final String KEY_TOOL_INSERTROW = "RI";
    public static final String KEY_TOOL_INSERTCOL = "CI";
    public static final String KEY_TOOL_DELETEROW = "RD";
    public static final String KEY_TOOL_DELETECOL = "CD";

    public static final String TOOLBAR_DEFAULT_MULTI  = "NW|SV|SP|CT|CP|PS|SP|UN|RE|SP|FN|SP|UC|UM|SP|SR|*|BL|IT|UD|SP|SK|SU|SB|SP|AL|AC|AR|AJ|SP|UL|OL|SP|LK|*|ST|SP|FO";
    public static final String TOOLBAR_DEFAULT_SINGLE = "NW|SV|SP|CT|CP|PS|SP|UN|RE|SP|BL|IT|UD|SP|AL|AC|AR|SP|AJ|SP|OL|UL|SP|TI|TE|CE";
    //public static final String TOOLBAR_DEFAULT_SINGLE = "NW|OP|SV|PR|SP|CT|CP|PS|SP|UN|RE|SP|BL|IT|UD|SP|AL|AC|AR|SP|AJ|SP|OL|UL|SP|TI|TE|CE|SR";

    public static final int TOOLBAR_SINGLE = 0;
    public static final int TOOLBAR_MAIN   = 1;
    public static final int TOOLBAR_FORMAT = 2;
    public static final int TOOLBAR_STYLES = 3;

    // Menu & Tool Key Arrays
    // private static Hashtable htMenus = new Hashtable();
    private static Hashtable htTools = new Hashtable();

    private final String appName = "Ekit";
    // private final String menuDialog = "..."; /* text to append to a MenuItem label when menu item opens a dialog */

    private final boolean useFormIndicator = true; /* Creates a highlighted background on a new FORM so that it may be more easily edited */
    private final String clrFormIndicator = "#cccccc";

    // System Clipboard Settings
    private java.awt.datatransfer.Clipboard sysClipboard;
    private SecurityManager secManager;

    /* Variables */
    private int iSplitPos = 0;

    private boolean exclusiveEdit = true;

    private String lastSearchFindTerm     = null;
    private String lastSearchReplaceTerm  = null;
    private boolean lastSearchCaseSetting = false;
    private boolean lastSearchTopSetting  = false;

    private File currentFile = null;
    private String imageChooserStartDir = ".";

    private int indent = 0;
    private final int indentStep = 4;

    // File extensions for MutableFilter
    private final String[] extsHTML = { "html", "htm", "shtml" };
    private final String[] extsCSS  = { "css" };
    private final String[] extsIMG  = { "gif", "jpg", "jpeg", "png" };
    private final String[] extsRTF  = { "rtf" };
    private final String[] extsB64  = { "b64" };
    private final String[] extsSer  = { "ser" };


    JTree pSimppleDataTree = null;

    /**
     * Master Constructor
     * @param sDocument         [String]  A text or HTML document to load in the editor upon startup.
     * @param sStyleSheet       [String]  A CSS stylesheet to load in the editor upon startup.
     * @param sRawDocument      [String]  A document encoded as a String to load in the editor upon startup.
     * @param sdocSource        [StyledDocument] Optional document specification, using javax.swing.text.StyledDocument.
     * @param urlStyleSheet     [URL]     A URL reference to the CSS style sheet.
     * @param includeToolBar    [boolean] Specifies whether the app should include the toolbar(s).
     * @param showViewSource    [boolean] Specifies whether or not to show the View Source window on startup.
     * @param showMenuIcons     [boolean] Specifies whether or not to show icon pictures in menus.
     * @param editModeExclusive [boolean] Specifies whether or not to use exclusive edit mode (recommended on).
     * @param sLanguage         [String]  The language portion of the Internationalization Locale to run Ekit in.
     * @param sCountry          [String]  The country portion of the Internationalization Locale to run Ekit in.
     * @param base64            [boolean] Specifies whether the raw document is Base64 encoded or not.
     * @param debugMode         [boolean] Specifies whether to show the Debug menu or not.
     * @param hasSpellChecker   [boolean] Specifies whether or not this uses the SpellChecker module
     * @param multiBar          [boolean] Specifies whether to use multiple toolbars or one big toolbar.
     * @param toolbarSeq        [String]  Code string specifying the toolbar buttons to show.
     */
    public EkitCore(String sDocument, String sStyleSheet, String sRawDocument, StyledDocument sdocSource, URL urlStyleSheet, boolean includeToolBar, boolean showViewSource, boolean showMenuIcons, boolean editModeExclusive, String sLanguage, boolean base64, boolean debugMode,  boolean multiBar, String toolbarSeq) {
        super();

        exclusiveEdit = editModeExclusive;

        //frameHandler = new Frame();

        // Determine if system clipboard is available
//        secManager = System.getSecurityManager();
//        if (secManager != null) {
//            try {
//                secManager.checkSystemClipboardAccess();
//                sysClipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
//            } catch (SecurityException se) {
//                sysClipboard = null;
//            }
//        }
        /* Create the editor kit, document, and stylesheet */
        jtpMain = new JTextPane();
        htmlKit = new ExtendedHTMLEditorKit();
        htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
        styleSheet = htmlDoc.getStyleSheet();
        htmlKit.setDefaultCursor(new Cursor(Cursor.TEXT_CURSOR));
        jtpMain.setCursor(new Cursor(Cursor.TEXT_CURSOR));

        /* Set up the text pane */
        jtpMain.setEditorKit(htmlKit);
        jtpMain.setDocument(htmlDoc);
        jtpMain.setMargin(new Insets(4, 4, 4, 4));
        jtpMain.addKeyListener(this);
        jtpMain.addFocusListener(this);
        // jtpMain.setDragEnabled(true); // this causes an error in older Java versions

        /* Create the source text area */
        if (sdocSource == null) {
            jtpSource = new JTextArea();
            jtpSource.setText(jtpMain.getText());
        } else {
            jtpSource = new JTextArea(sdocSource);
            jtpMain.setText(jtpSource.getText());
        }
        jtpSource.setBackground(new Color(212, 212, 212));
        jtpSource.setSelectionColor(new Color(255, 192, 192));
        jtpSource.setMargin(new Insets(4, 4, 4, 4));
        jtpSource.getDocument().addDocumentListener(this);
        jtpSource.addFocusListener(this);
        jtpSource.setCursor(new Cursor(Cursor.TEXT_CURSOR));

        /* Set up the undo features */
        undoMngr = new UndoManager();
        undoAction = new UndoAction();
        redoAction = new RedoAction();
        jtpMain.getDocument().addUndoableEditListener(new CustomUndoableEditListener());

        /* Insert raw document, if exists */
        if (sRawDocument != null && sRawDocument.length() > 0) {
            if (base64) {
                jtpMain.setText(Base64Codec.decode(sRawDocument));
            } else {
                jtpMain.setText(sRawDocument);
            }
        }
        jtpMain.setCaretPosition(0);
        jtpMain.getDocument().addDocumentListener(this);

        /* Import CSS from reference, if exists */
        if (urlStyleSheet != null) {
            try {
                String currDocText = jtpMain.getText();
                htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
                styleSheet = htmlDoc.getStyleSheet();
                BufferedReader br = new BufferedReader(new InputStreamReader(urlStyleSheet.openStream()));
                styleSheet.loadRules(br, urlStyleSheet);
                br.close();
                htmlDoc = new ExtendedHTMLDocument(styleSheet);
                registerDocument(htmlDoc);
                jtpMain.setText(currDocText);
                jtpSource.setText(jtpMain.getText());
            } catch (Exception e) {
                Util.err(e);
            }
        }

        /* Preload the specified HTML document, if exists */
        if (sDocument != null) {
            File defHTML = new File(sDocument);
            if (defHTML.exists()) {
                try {
                    openDocument(defHTML);
                } catch (Exception e) {
                    logException("Exception in preloading HTML document", e);
                }
            }
        }

        /* Preload the specified CSS document, if exists */
        if (sStyleSheet != null) {
            File defCSS = new File(sStyleSheet);
            if (defCSS.exists()) {
                try {
                    openStyleSheet(defCSS);
                } catch (Exception e) {
                    logException("Exception in preloading CSS stylesheet", e);
                }
            }
        }

        /* Collect the actions that the JTextPane is naturally aware of */
        Hashtable actions = new Hashtable();
        Action[] actionsArray = jtpMain.getActions();
        for (int i = 0; i < actionsArray.length; i++) {
            Action a = actionsArray[i];
            actions.put(a.getValue(Action.NAME), a);
        }

        /* Create shared actions */
        actionFontBold        = new StyledEditorKit.BoldAction();
        actionFontItalic      = new StyledEditorKit.ItalicAction();
        actionFontUnderline   = new StyledEditorKit.UnderlineAction();
        actionListUnordered   = new ListAutomationAction(this, Language.getInstance().getText("ListUnordered"), HTML.Tag.UL);
        actionListOrdered     = new ListAutomationAction(this, Language.getInstance().getText("ListOrdered"), HTML.Tag.OL);
        actionAlignLeft       = new StyledEditorKit.AlignmentAction(Language.getInstance().getText("AlignLeft"), StyleConstants.ALIGN_LEFT);
        actionAlignCenter     = new StyledEditorKit.AlignmentAction(Language.getInstance().getText("AlignCenter"), StyleConstants.ALIGN_CENTER);
        actionAlignRight      = new StyledEditorKit.AlignmentAction(Language.getInstance().getText("AlignRight"), StyleConstants.ALIGN_RIGHT);
        actionAlignJustified  = new StyledEditorKit.AlignmentAction(Language.getInstance().getText("AlignJustified"), StyleConstants.ALIGN_JUSTIFIED);

        /* Create toolbar tool objects */
        jbtnNewHTML = new JButtonNoFocus(getEkitIcon("new")); //OK
        jbtnNewHTML.setToolTipText(Language.getInstance().getText("NewDocument"));
        jbtnNewHTML.setActionCommand("newdoc");
        jbtnNewHTML.addActionListener(this);
        htTools.put(KEY_TOOL_NEW, jbtnNewHTML);
        jbtnOpenHTML = new JButtonNoFocus(getEkitIcon("open")); //OK
        jbtnOpenHTML.setToolTipText(Language.getInstance().getText("OpenDocument"));
        jbtnOpenHTML.setActionCommand("openhtml");
        jbtnOpenHTML.addActionListener(this);
        htTools.put(KEY_TOOL_OPEN, jbtnOpenHTML);
        jbtnSaveHTML = new JButtonNoFocus(getEkitIcon("save")); //ok
        jbtnSaveHTML.setToolTipText(Language.getInstance().getText("SaveDocument"));
        jbtnSaveHTML.setActionCommand("saveas");
        jbtnSaveHTML.addActionListener(this);
        htTools.put(KEY_TOOL_SAVE, jbtnSaveHTML);
        jbtnPrint = new JButtonNoFocus(getEkitIcon("Print")); //ok
        jbtnPrint.setToolTipText(Language.getInstance().getText("PrintDocument"));
        jbtnPrint.setActionCommand("print");
        jbtnPrint.addActionListener(this);
        htTools.put(KEY_TOOL_PRINT, jbtnPrint);
        jbtnCut = new JButtonNoFocus(new DefaultEditorKit.CutAction());
        jbtnCut.setIcon(getEkitIcon("couper")); //OK
        jbtnCut.setText(null);
        jbtnCut.setToolTipText(Language.getInstance().getText("Cut"));
        htTools.put(KEY_TOOL_CUT, jbtnCut);
        jbtnCopy = new JButtonNoFocus(new DefaultEditorKit.CopyAction());
        jbtnCopy.setIcon(getEkitIcon("copier")); //ok
        jbtnCopy.setText(null);
        jbtnCopy.setToolTipText(Language.getInstance().getText("Copy"));
        htTools.put(KEY_TOOL_COPY, jbtnCopy);
        jbtnPaste = new JButtonNoFocus(new DefaultEditorKit.PasteAction());
        jbtnPaste.setIcon(getEkitIcon("paste")); //OK
        jbtnPaste.setText(null);
        jbtnPaste.setToolTipText(Language.getInstance().getText("Paste"));
        htTools.put(KEY_TOOL_PASTE, jbtnPaste);
        jbtnUndo = new JButtonNoFocus(undoAction);
        jbtnUndo.setIcon(getEkitIcon("undo"));//OK
        jbtnUndo.setText(null);
        jbtnUndo.setToolTipText(Language.getInstance().getText("Undo"));
        htTools.put(KEY_TOOL_UNDO, jbtnUndo);
        jbtnRedo = new JButtonNoFocus(redoAction);
        jbtnRedo.setIcon(getEkitIcon("redo"));//ok
        jbtnRedo.setText(null);
        jbtnRedo.setToolTipText(Language.getInstance().getText("Redo"));
        htTools.put(KEY_TOOL_REDO, jbtnRedo);
        jbtnBold = new JButtonNoFocus(actionFontBold);
        jbtnBold.setIcon(getEkitIcon("bold")); //ok
        this.getActionMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.CTRL_MASK), actionFontBold);
        jbtnBold.setText(null);
        jbtnBold.setToolTipText(Language.getInstance().getText("FontBold"));
        htTools.put(KEY_TOOL_BOLD, jbtnBold);
        jbtnItalic = new JButtonNoFocus(actionFontItalic);
        jbtnItalic.setIcon(getEkitIcon("ITL")); //ok
        jbtnItalic.setText(null);
        jbtnItalic.setToolTipText(Language.getInstance().getText("FontItalic"));
        htTools.put(KEY_TOOL_ITALIC, jbtnItalic);
        jbtnUnderline = new JButtonNoFocus(actionFontUnderline);
        jbtnUnderline.setIcon(getEkitIcon("UNDRLN")); //ok
        jbtnUnderline.setText(null);
        jbtnUnderline.setToolTipText(Language.getInstance().getText("FontUnderline"));
        htTools.put(KEY_TOOL_UNDERLINE, jbtnUnderline);
        jbtnUList = new JButtonNoFocus(actionListUnordered);
        jbtnUList.setIcon(getEkitIcon("UList")); //OK
        jbtnUList.setText(null);
        jbtnUList.setToolTipText(Language.getInstance().getText("ListUnordered"));
        htTools.put(KEY_TOOL_ULIST, jbtnUList);
        jbtnOList = new JButtonNoFocus(actionListOrdered);
        jbtnOList.setIcon(getEkitIcon("OList")); //OK
        jbtnOList.setText(null);
        jbtnOList.setToolTipText(Language.getInstance().getText("ListOrdered"));
        htTools.put(KEY_TOOL_OLIST, jbtnOList);
        jbtnAlignLeft = new JButtonNoFocus(actionAlignLeft);
        jbtnAlignLeft.setIcon(getEkitIcon("leftjustify")); //ok
        jbtnAlignLeft.setText(null);
        jbtnAlignLeft.setToolTipText(Language.getInstance().getText("AlignLeft"));
        htTools.put(KEY_TOOL_ALIGNL, jbtnAlignLeft);
        jbtnAlignCenter = new JButtonNoFocus(actionAlignCenter);
        jbtnAlignCenter.setIcon(getEkitIcon("centerjustify")); //ok
        jbtnAlignCenter.setText(null);
        jbtnAlignCenter.setToolTipText(Language.getInstance().getText("AlignCenter"));
        htTools.put(KEY_TOOL_ALIGNC, jbtnAlignCenter);
        jbtnAlignRight = new JButtonNoFocus(actionAlignRight);
        jbtnAlignRight.setIcon(getEkitIcon("rightjustify")); //ok
        jbtnAlignRight.setText(null);
        jbtnAlignRight.setToolTipText(Language.getInstance().getText("AlignRight"));
        htTools.put(KEY_TOOL_ALIGNR, jbtnAlignRight);
        jbtnAlignJustified = new JButtonNoFocus(actionAlignJustified);
        jbtnAlignJustified.setIcon(getEkitIcon("AlignJustified")); //ok
        jbtnAlignJustified.setText(null);
        jbtnAlignJustified.setToolTipText(Language.getInstance().getText("AlignJustified"));
        htTools.put(KEY_TOOL_ALIGNJ, jbtnAlignJustified);
        jbtnFind = new JButtonNoFocus();
        jbtnFind.setActionCommand("find");
        jbtnFind.addActionListener(this);
        jbtnFind.setIcon(getEkitIcon("Find")); //ok
        jbtnFind.setText(null);
        jbtnFind.setToolTipText(Language.getInstance().getText("SearchFind"));
        htTools.put(KEY_TOOL_FIND, jbtnFind);
        jtbtnViewSource = new JToggleButtonNoFocus(getEkitIcon("Source")); //ok
        jtbtnViewSource.setText(null);
        jtbtnViewSource.setToolTipText(Language.getInstance().getText("ViewSource"));
        jtbtnViewSource.setActionCommand("viewsource");
        jtbtnViewSource.addActionListener(this);
        htTools.put(KEY_TOOL_SOURCE, jtbtnViewSource);
        jbtnInsertTable = new JButtonNoFocus();
        jbtnInsertTable.setActionCommand("inserttable");
        jbtnInsertTable.addActionListener(this);
        jbtnInsertTable.setIcon(getEkitIcon("TableCreate")); //ok
        jbtnInsertTable.setText(null);
        jbtnInsertTable.setToolTipText(Language.getInstance().getText("InsertTable"));
        htTools.put(KEY_TOOL_INSTABLE, jbtnInsertTable);
        jbtnEditTable = new JButtonNoFocus();
        jbtnEditTable.setActionCommand("edittable");
        jbtnEditTable.addActionListener(this);
        jbtnEditTable.setIcon(getEkitIcon("TableEdit")); //ok
        jbtnEditTable.setText(null);
        jbtnEditTable.setToolTipText(Language.getInstance().getText("TableEdit"));
        htTools.put(KEY_TOOL_EDITTABLE, jbtnEditTable);
        jbtnEditCell = new JButtonNoFocus();
        jbtnEditCell.setActionCommand("editcell");
        jbtnEditCell.addActionListener(this);
        jbtnEditCell.setIcon(getEkitIcon("CellEdit")); //ok
        jbtnEditCell.setText(null);
        jbtnEditCell.setToolTipText(Language.getInstance().getText("TableCellEdit"));
        htTools.put(KEY_TOOL_EDITCELL, jbtnEditCell);
        jbtnInsertRow = new JButtonNoFocus();
        jbtnInsertRow.setActionCommand("inserttablerow");
        jbtnInsertRow.addActionListener(this);
        jbtnInsertRow.setIcon(getEkitIcon("InsertRow")); //ok
        jbtnInsertRow.setText(null);
        jbtnInsertRow.setToolTipText(Language.getInstance().getText("InsertTableRow"));
        htTools.put(KEY_TOOL_INSERTROW, jbtnInsertRow);
        jbtnInsertColumn = new JButtonNoFocus();
        jbtnInsertColumn.setActionCommand("inserttablecolumn");
        jbtnInsertColumn.addActionListener(this);
        jbtnInsertColumn.setIcon(getEkitIcon("InsertColumn")); //ok
        jbtnInsertColumn.setText(null);
        jbtnInsertColumn.setToolTipText(Language.getInstance().getText("InsertTableColumn"));
        htTools.put(KEY_TOOL_INSERTCOL, jbtnInsertColumn);
        jbtnDeleteRow = new JButtonNoFocus();
        jbtnDeleteRow.setActionCommand("deletetablerow");
        jbtnDeleteRow.addActionListener(this);
        jbtnDeleteRow.setIcon(getEkitIcon("DeleteRow")); //ok
        jbtnDeleteRow.setText(null);
        jbtnDeleteRow.setToolTipText(Language.getInstance().getText("DeleteTableRow"));
        htTools.put(KEY_TOOL_DELETEROW, jbtnDeleteRow);
        jbtnDeleteColumn = new JButtonNoFocus();
        jbtnDeleteColumn.setActionCommand("deletetablecolumn");
        jbtnDeleteColumn.addActionListener(this);
        jbtnDeleteColumn.setIcon(getEkitIcon("DeleteColumn")); //ok
        jbtnDeleteColumn.setText(null);
        jbtnDeleteColumn.setToolTipText(Language.getInstance().getText("DeleteTableColumn"));
        htTools.put(KEY_TOOL_DELETECOL, jbtnDeleteColumn);

        /* Create the toolbar */
        if (multiBar) {
            jToolBarMain = new JToolBar(SwingConstants.HORIZONTAL);
            jToolBarMain.setFloatable(false);
            jToolBarFormat = new JToolBar(SwingConstants.HORIZONTAL);
            jToolBarFormat.setFloatable(false);
            jToolBarStyles = new JToolBar(SwingConstants.HORIZONTAL);
            jToolBarStyles.setFloatable(false);

            initializeMultiToolbars(toolbarSeq);
        } else {
            jToolBar = new JToolBar(SwingConstants.HORIZONTAL);
            jToolBar.setFloatable(false);

            initializeSingleToolbar(toolbarSeq);
        }

        /* Create the scroll area for the text pane */
        JScrollPane jspViewport = new JScrollPane(jtpMain);
        jspViewport.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jspViewport.setPreferredSize(new Dimension(400, 400));
        jspViewport.setMinimumSize(new Dimension(32, 32));

        /* Create the scroll area for the source viewer */
        jspSource = new JScrollPane(jtpSource);
        jspSource.setPreferredSize(new Dimension(400, 100));
        jspSource.setMinimumSize(new Dimension(32, 32));

        jspltDisplay = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        jspltDisplay.setTopComponent(jspViewport);
        if (showViewSource) {
            jspltDisplay.setBottomComponent(jspSource);
        } else {
            jspltDisplay.setBottomComponent(null);
        }

        iSplitPos = jspltDisplay.getDividerLocation();

        registerDocumentStyles();

        /* Add the components to the app */
        this.setLayout(new BorderLayout());
        if (includeToolBar) {
            this.add(jToolBar, BorderLayout.NORTH);
        }
        this.add(jspltDisplay, BorderLayout.CENTER);
    }

    /**
     * Raw/Base64 Document & Style Sheet URL Constructor (Ideal for EkitApplet)
     * @param sRawDocument      [String]  A document encoded as a String to load in the editor upon startup.
     * @param sRawDocument      [String]  A document encoded as a String to load in the editor upon startup.
     * @param includeToolBar    [boolean] Specifies whether the app should include the toolbar(s).
     * @param showViewSource    [boolean] Specifies whether or not to show the View Source window on startup.
     * @param showMenuIcons     [boolean] Specifies whether or not to show icon pictures in menus.
     * @param editModeExclusive [boolean] Specifies whether or not to use exclusive edit mode (recommended on).
     * @param sLanguage         [String]  The language portion of the Internationalization Locale to run Ekit in.
     * @param sCountry          [String]  The country portion of the Internationalization Locale to run Ekit in.
     * @param base64            [boolean] Specifies whether the raw document is Base64 encoded or not.
     * @param multiBar          [boolean] Specifies whether to use multiple toolbars or one big toolbar.
     * @param toolbarSeq        [String]  Code string specifying the toolbar buttons to show.
     */
    public EkitCore(String sRawDocument, URL urlStyleSheet, boolean includeToolBar, boolean showViewSource, boolean showMenuIcons, boolean editModeExclusive, String sLanguage, String sCountry, boolean base64,  boolean multiBar, String toolbarSeq) {
        this(null, null, sRawDocument, (StyledDocument)null, urlStyleSheet, includeToolBar, showViewSource, showMenuIcons, editModeExclusive, sLanguage, base64, false, multiBar, toolbarSeq);
    }

    /**
     * Empty Constructor
     */
    public EkitCore(JTree pTree, boolean editable) {
        this((String)null, (String)null, (String)null, (StyledDocument)null, (URL)null, editable, false, true, true, (String)null, false, false, false, TOOLBAR_DEFAULT_SINGLE);
        pSimppleDataTree = pTree;
        jtpMain.setEditable(editable);
    }

    /* ActionListener method */
    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            String command = ae.getActionCommand();
            if (command.equals("newdoc")) {
                //SimpleInfoDialog sidAsk = new SimpleInfoDialog(this.getFrame(), "", true, Language.getInstance().getText("AskNewDocument"), SimpleInfoDialog.QUESTION);
                SimpleInfoDialog sidAsk = new SimpleInfoDialog(null, "", true, Language.getInstance().getText("AskNewDocument"), SimpleInfoDialog.QUESTION);
                                
                String decision = sidAsk.getDecisionValue();
                if (decision.equals(Language.getInstance().getText("DialogAccept"))) {
                    if (styleSheet != null) {
                        htmlDoc = new ExtendedHTMLDocument(styleSheet);
                    } else {
                        htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
                    }
                    jtpMain.setText("<HTML><BODY></BODY></HTML>");
                    jtpSource.setText(jtpMain.getText());
                    registerDocument(htmlDoc);
                    currentFile = null;
                    updateTitle();
                }
            } else if (command.equals("openhtml")) {
                openDocument(null);
            } else if (command.equals("opencss")) {
                openStyleSheet(null);
            } else if (command.equals("openb64")) {
                openDocumentBase64(null);
            } else if (command.equals("save")) {
                /*writeOut((HTMLDocument)(jtpMain.getDocument()), currentFile);
                  updateTitle();*/
                saveDocumentInDB();
            } else if (command.equals("saveas"))        {
                //writeOut((HTMLDocument)(jtpMain.getDocument()), null);
                saveDocumentInDB();
            } else if (command.equals("savebody")) {
                writeOutFragment((HTMLDocument)(jtpMain.getDocument()),"body");
            } else if (command.equals("savertf")) {
                writeOutRTF((jtpMain.getStyledDocument()));
            } else if (command.equals("saveb64")) {
                writeOutBase64(jtpSource.getText());
            } else if (command.equals("textcut")) {
                if (jspSource.isShowing() && jtpSource.hasFocus()) {
                    jtpSource.cut();
                } else {
                    jtpMain.cut();
                }
            } else if (command.equals("textcopy")) {
                if (jspSource.isShowing() && jtpSource.hasFocus()) {
                    jtpSource.copy();
                } else {
                    jtpMain.copy();
                }
            } else if (command.equals("textpaste")) {
                if (jspSource.isShowing() && jtpSource.hasFocus()) {
                    jtpSource.paste();
                } else {
                    jtpMain.paste();
                }
            } else if (command.equals("print")) {
                DocumentRenderer dr = new DocumentRenderer();
                dr.print(htmlDoc);
            } else if (command.equals("describe")) {
                Util.debug("------------DOCUMENT------------");
                Util.debug("Content Type : " + jtpMain.getContentType());
                Util.debug("Editor Kit   : " + jtpMain.getEditorKit());
                Util.debug("Doc Tree     :");
                Util.debug("");
                describeDocument(jtpMain.getStyledDocument());
                Util.debug("--------------------------------");
                Util.debug("");
            } else if (command.equals("describecss")) {
                Util.debug("-----------STYLESHEET-----------");
                Util.debug("Stylesheet Rules");
                Enumeration rules = styleSheet.getStyleNames();
                while (rules.hasMoreElements()) {
                    String ruleName = (String)(rules.nextElement());
                    Style styleRule = styleSheet.getStyle(ruleName);
                    Util.debug(styleRule.toString());
                }
                Util.debug("--------------------------------");
                Util.debug("");
            } else if (command.equals("whattags")) {
                Util.debug("Caret Position : " + jtpMain.getCaretPosition());
                AttributeSet attribSet = jtpMain.getCharacterAttributes();
                Enumeration attribs = attribSet.getAttributeNames();
                Util.debug("Attributes     : ");
                while (attribs.hasMoreElements()) {
                    String attribName = attribs.nextElement().toString();
                    Util.debug("                 " + attribName + " | " + attribSet.getAttribute(attribName));
                }
            } else if (command.equals("viewsource"))    {
                toggleSourceWindow();
            } else if (command.equals("serialize")) {
                serializeOut((HTMLDocument)(jtpMain.getDocument()));
            } else if (command.equals("readfromser")) {
                serializeIn();
            } else if (command.equals("inserttable")) {
                /*String[] fieldNames  = { "rows", "cols", "border", "cellspacing", "cellpadding", "width", "valign" };
                  String[] fieldTypes  = { "text", "text", "text",   "text",        "text",        "text",  "combo" };
                  String[] fieldValues = { "3",    "3",    "1",    "2",           "4",           "100%",  "top,middle,bottom" };*/
                String[] fieldNames  = { "rows", "cols", "width", "border", "cellspacing", "frame"};
                String[] fieldTypes  = { "text", "text", "text",  "text"  , "text"       , "text"};
                String[] fieldValues = { "3",    "3",    "100%", "1"     , "0"           , "void"};
                insertTable((Hashtable)null, fieldNames, fieldTypes, fieldValues);
            } else if (command.equals("edittable")) {
                editTable();
            } else if (command.equals("editcell")) {
                editCell();
            } else if (command.equals("inserttablerow")) {
                insertTableRow();
            } else if (command.equals("inserttablecolumn")) {
                insertTableColumn();
            } else if (command.equals("deletetablerow")) {
                deleteTableRow();
            } else if (command.equals("deletetablecolumn")) {
                deleteTableColumn();
            } else if (command.equals("insertbreak")) {
                insertBreak();
            } else if (command.equals("insertnbsp")) {
                insertNonbreakingSpace();
            } else if (command.equals("insertlocalimage")) {
                insertLocalImage(null);
            } else if (command.equals("inserturlimage")) {
                insertURLImage();
            } else if (command.equals("insertform")) {
                String[] fieldNames  = { "name", "method",   "enctype" };
                String[] fieldTypes  = { "text", "combo",    "text" };
                String[] fieldValues = { "",     "POST,GET", "text" };
                insertFormElement(HTML.Tag.FORM, "form", (Hashtable)null, fieldNames, fieldTypes, fieldValues, true);
            } else if (command.equals("inserttextfield")) {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "text");
                String[] fieldNames = { "name", "value", "size", "maxlength" };
                String[] fieldTypes = { "text", "text",  "text", "text" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("inserttextarea")) {
                String[] fieldNames = { "name", "rows", "cols" };
                String[] fieldTypes = { "text", "text", "text" };
                insertFormElement(HTML.Tag.TEXTAREA, "textarea", (Hashtable)null, fieldNames, fieldTypes, true);
            } else if (command.equals("insertcheckbox")) {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "checkbox");
                String[] fieldNames = { "name", "checked" };
                String[] fieldTypes = { "text", "bool" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("insertradiobutton")) {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "radio");
                String[] fieldNames = { "name", "checked" };
                String[] fieldTypes = { "text", "bool" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("insertpassword"))        {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "password");
                String[] fieldNames = { "name", "value", "size", "maxlength" };
                String[] fieldTypes = { "text", "text",  "text", "text" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("insertbutton")) {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "button");
                String[] fieldNames = { "name", "value" };
                String[] fieldTypes = { "text", "text" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("insertbuttonsubmit"))    {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "submit");
                String[] fieldNames = { "name", "value" };
                String[] fieldTypes = { "text", "text" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("insertbuttonreset")) {
                Hashtable htAttribs = new Hashtable();
                htAttribs.put("type", "reset");
                String[] fieldNames = { "name", "value" };
                String[] fieldTypes = { "text", "text" };
                insertFormElement(HTML.Tag.INPUT, "input", htAttribs, fieldNames, fieldTypes, false);
            } else if (command.equals("find")) {
                doSearch((String)null, (String)null, false, lastSearchCaseSetting, lastSearchTopSetting);
            } else if (command.equals("findagain")) {
                doSearch(lastSearchFindTerm, (String)null, false, lastSearchCaseSetting, false);
            } else if (command.equals("replace")) {
                doSearch((String)null, (String)null, true, lastSearchCaseSetting, lastSearchTopSetting);
            } else if (command.equals("exit")) {
                this.dispose();
            } else if (command.equals("helpabout")) {
                //SimpleInfoDialog sidAbout = new SimpleInfoDialog(this.getFrame(), Language.getInstance().getText("About"), true, Language.getInstance().getText("AboutMessage"), SimpleInfoDialog.INFO);
                SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("About"), true, Language.getInstance().getText("AboutMessage"), SimpleInfoDialog.INFO);
            } else if (command.equals("spellcheck"))    {
                checkDocumentSpelling(jtpMain.getDocument());
            }
        } catch (IOException ioe) {
            logException("IOException in actionPerformed method", ioe);
            //SimpleInfoDialog sidAbout = new SimpleInfoDialog(this.getFrame(), Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorIOException"), SimpleInfoDialog.ERROR);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorIOException"), SimpleInfoDialog.ERROR);
        } catch (BadLocationException ble) {
            logException("BadLocationException in actionPerformed method", ble);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorBadLocationException"), SimpleInfoDialog.ERROR);
        } catch (NumberFormatException nfe) {
            logException("NumberFormatException in actionPerformed method", nfe);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorNumberFormatException"), SimpleInfoDialog.ERROR);
        } catch (ClassNotFoundException cnfe) {
            logException("ClassNotFound Exception in actionPerformed method", cnfe);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorClassNotFoundException "), SimpleInfoDialog.ERROR);
        } catch (RuntimeException re) {
            logException("RuntimeException in actionPerformed method", re);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorRuntimeException"), SimpleInfoDialog.ERROR);
        }
    }

    /* KeyListener methods */
    @Override
    public void keyTyped(KeyEvent ke) {
        Element elem;
        String selectedText;
        int pos = this.getCaretPosition();
        int repos = -1;
        if (ke.getKeyChar() == KeyEvent.VK_BACK_SPACE) {
            try {
                if (pos > 0) {
                    if ((selectedText = jtpMain.getSelectedText()) != null) {
                        htmlUtilities.delete();
                        return;
                    } else {
                        int sOffset = htmlDoc.getParagraphElement(pos).getStartOffset();
                        if (sOffset == jtpMain.getSelectionStart()) {
                            boolean content = true;
                            if (htmlUtilities.checkParentsTag(HTML.Tag.LI)) {
                                elem = htmlUtilities.getListItemParent();
                                content = false;
                                int so = elem.getStartOffset();
                                int eo = elem.getEndOffset();
                                if (so + 1 < eo) {
                                    char[] temp = jtpMain.getText(so, eo - so).toCharArray();
                                    for (int i=0; i < temp.length; i++) {
                                        if (!Character.isWhitespace(temp[i])) {
                                            content = true;
                                        }
                                    }
                                }
                                if (!content) {
                                    Element listElement = elem.getParentElement();
                                    htmlUtilities.removeTag(elem, true);
                                    this.setCaretPosition(sOffset - 1);
                                    return;
                                } else {
                                    jtpMain.setCaretPosition(jtpMain.getCaretPosition() - 1);
                                    jtpMain.moveCaretPosition(jtpMain.getCaretPosition() - 2);
                                    jtpMain.replaceSelection("");
                                    return;
                                }
                            } else if (htmlUtilities.checkParentsTag(HTML.Tag.TABLE)) {
                                jtpMain.setCaretPosition(jtpMain.getCaretPosition() - 1);
                                ke.consume();
                                return;
                            }
                        }
                        jtpMain.replaceSelection("");
                        return;
                    }
                }
            } catch (BadLocationException ble) {
                logException("BadLocationException in keyTyped method", ble);
                SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorBadLocationException"), SimpleInfoDialog.ERROR);
            } catch (IOException ioe) {
                logException("IOException in keyTyped method", ioe);
                SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorIOException"), SimpleInfoDialog.ERROR);
            }
        } else if (ke.getKeyChar() == KeyEvent.VK_ENTER)        {
            try {
                if (htmlUtilities.checkParentsTag(HTML.Tag.UL) == true | htmlUtilities.checkParentsTag(HTML.Tag.OL) == true) {
                    elem = htmlUtilities.getListItemParent();
                    int so = elem.getStartOffset();
                    int eo = elem.getEndOffset();
                    char[] temp = this.getTextPane().getText(so,eo-so).toCharArray();
                    boolean content = false;
                    for (int i=0;i<temp.length;i++) {
                        if (!Character.isWhitespace(temp[i])) {
                            content = true;
                        }
                    }
                    if (content) {
                        int end = -1;
                        int j = temp.length;
                        do {
                            j--;
                            new Character(temp[j]);
                            if (Character.isLetterOrDigit(temp[j])) {
                                end = j;
                            }
                        } while (end == -1 && j >= 0);
                        j = end;
                        do {
                            j++;
                            new Character(temp[j]);
                            if (!Character.isSpaceChar(temp[j])) {
                                repos = j - end -1;
                            }
                        } while (repos == -1 && j < temp.length);
                        if (repos == -1) {
                            repos = 0;
                        }
                    }
                    if (elem.getStartOffset() == elem.getEndOffset() || !content) {
                        manageListElement(elem);
                    } else {
                        if (this.getCaretPosition() + 1 == elem.getEndOffset()) {
                            insertListStyle(elem);
                            this.setCaretPosition(pos - repos);
                        } else {
                            int caret = this.getCaretPosition();
                            String tempString = this.getTextPane().getText(caret, eo - caret);
                            this.getTextPane().select(caret, eo - 1);
                            this.getTextPane().replaceSelection("");
                            htmlUtilities.insertListElement(tempString);
                            Element newLi = htmlUtilities.getListItemParent();
                            this.setCaretPosition(newLi.getEndOffset() - 1);
                        }
                    }
                } else {
                    insertBreak();
                }
            } catch (BadLocationException ble) {
                logException("BadLocationException in keyTyped method", ble);
                SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorBadLocationException"), SimpleInfoDialog.ERROR);
            } catch (IOException ioe) {
                logException("IOException in keyTyped method", ioe);
                SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorIOException"), SimpleInfoDialog.ERROR);
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {}
    @Override
    public void keyReleased(KeyEvent e) {}

    /* FocusListener methods */
    @Override
    public void focusGained(FocusEvent fe) {
        if (fe.getSource() == jtpMain) {
            setFormattersActive(true);
        } else if (fe.getSource() == jtpSource) {
            setFormattersActive(false);
        }
    }

    @Override
    public void focusLost(FocusEvent fe) {}

    /* DocumentListener methods */
    @Override
    public void changedUpdate(DocumentEvent de) { handleDocumentChange(de); }
    @Override
    public void insertUpdate(DocumentEvent de)  { handleDocumentChange(de); }
    @Override
    public void removeUpdate(DocumentEvent de)  { handleDocumentChange(de); }
    public void handleDocumentChange(DocumentEvent de) {
        if (!exclusiveEdit) {
            if (isSourceWindowActive()) {
                if (de.getDocument() instanceof HTMLDocument || de.getDocument() instanceof ExtendedHTMLDocument) {
                    jtpSource.getDocument().removeDocumentListener(this);
                    jtpSource.setText(jtpMain.getText());
                    jtpSource.getDocument().addDocumentListener(this);
                } else if (de.getDocument() instanceof PlainDocument || de.getDocument() instanceof DefaultStyledDocument) {
                    jtpMain.getDocument().removeDocumentListener(this);
                    jtpMain.setText(jtpSource.getText());
                    jtpMain.getDocument().addDocumentListener(this);
                }
            }
        }
    }

    /**
     * Method for setting a document as the current document for the text pane
     * and re-registering the controls and settings for it
     */
    public void registerDocument(ExtendedHTMLDocument htmlDoc) {
        jtpMain.setDocument(htmlDoc);
        jtpMain.getDocument().addUndoableEditListener(new CustomUndoableEditListener());
        jtpMain.getDocument().addDocumentListener(this);
        jtpMain.setCaretPosition(0);
        purgeUndos();
        registerDocumentStyles();
    }

    /**
     * Method for locating the available CSS style for the document and adding
     * them to the styles selector
     */
    public void registerDocumentStyles() {
        /*      if(jcmbStyleSelector == null || htmlDoc == null)
                {
                return;
                }
                jcmbStyleSelector.setEnabled(false);
                jcmbStyleSelector.removeAllItems();
                jcmbStyleSelector.addItem(Language.getInstance().getText("NoCSSStyle"));
                for(Enumeration e = htmlDoc.getStyleNames(); e.hasMoreElements();)
                {
                String name = (String) e.nextElement();
                if(name.length() > 0 && name.charAt(0) == '.')
                {
                jcmbStyleSelector.addItem(name.substring(1));
                }
                }
                jcmbStyleSelector.setEnabled(true);*/
    }

    /**
     * Method for inserting list elements
     */
    public void insertListStyle(Element element) throws BadLocationException,IOException {
        if (element.getParentElement().getName() == "ol") {
            actionListOrdered.actionPerformed(new ActionEvent(new Object(), 0, "newListPoint"));
        } else {
            actionListUnordered.actionPerformed(new ActionEvent(new Object(), 0, "newListPoint"));
        }
    }

    /**
     * Method for inserting an HTML Table
     */
    private void insertTable(Hashtable attribs, String[] fieldNames, String[] fieldTypes, String[] fieldValues)
        throws IOException, BadLocationException, RuntimeException, NumberFormatException {
        int caretPos = jtpMain.getCaretPosition();
        StringBuffer compositeElement = new StringBuffer("<TABLE");
        if (attribs != null && attribs.size() > 0) {
            Enumeration attribEntries = attribs.keys();
            while (attribEntries.hasMoreElements()) {
                Object entryKey   = attribEntries.nextElement();
                Object entryValue = attribs.get(entryKey);
                if (entryValue != null && entryValue != "") {
                    compositeElement.append(" " + entryKey + "=" + '"' + entryValue + '"');
                }
            }
        }
        int rows = 0;
        int cols = 0;
        if (fieldNames != null && fieldNames.length > 0) {
            PropertiesDialog propertiesDialog = new PropertiesDialog(null, fieldNames, fieldTypes, fieldValues, Language.getInstance().getText("TableDialogTitle"), true);
            propertiesDialog.setVisible(true);
            String decision = propertiesDialog.getDecisionValue();
            if (decision.equals(Language.getInstance().getText("DialogCancel"))) {
                propertiesDialog.dispose();
                propertiesDialog = null;
                return;
            } else {
                for (int iter = 0; iter < fieldNames.length; iter++) {
                    String fieldName = fieldNames[iter];
                    String propValue = propertiesDialog.getFieldValue(fieldName);
                    if (propValue != null && propValue != "" && propValue.length() > 0) {
                        if (fieldName.equals("rows")) {
                            rows = Integer.parseInt(propValue);
                        } else if (fieldName.equals("cols")) {
                            cols = Integer.parseInt(propValue);
                        } else {
                            compositeElement.append(" " + fieldName + "=" + '"' + propValue + '"');
                        }
                    }
                }
            }
            propertiesDialog.dispose();
            propertiesDialog = null;
        }
        compositeElement.append(">");
        for (int i = 0; i < rows; i++) {
            compositeElement.append("<TR>");
            for (int j = 0; j < cols; j++) {
                compositeElement.append("<TD></TD>");
            }
            compositeElement.append("</TR>");
        }
        compositeElement.append("</TABLE>&nbsp;");
        htmlKit.insertHTML(htmlDoc, caretPos, compositeElement.toString(), 0, 0, HTML.Tag.TABLE);
        jtpMain.setCaretPosition(caretPos + 1);
        refreshOnUpdate();
    }

    /**
     * Method for editing an HTML Table
     */
    private void editTable() {
        int caretPos = jtpMain.getCaretPosition();
        Element element = htmlDoc.getCharacterElement(caretPos);
        Element elementParent = element.getParentElement();
        while (elementParent != null && !elementParent.getName().equals("table")) {
            elementParent = elementParent.getParentElement();
        }
        if (elementParent != null) {
            HTML.Attribute[] fieldKeys = {
                HTML.Attribute.BORDER, HTML.Attribute.CELLSPACING,
                HTML.Attribute.CELLPADDING, HTML.Attribute.WIDTH, HTML.Attribute.VALIGN };
            String[] fieldNames  = { "border", "cellspacing", "cellpadding", "width", "valign" };
            String[] fieldTypes  = { "text",   "text",        "text",        "text",  "combo" };
            String[] fieldValues = { "",       "",            "",            "",      "top,middle,bottom," };
            MutableAttributeSet myatr = (MutableAttributeSet)elementParent.getAttributes();
            for (int i=0; i<fieldNames.length; i++) {
                if (myatr.isDefined(fieldKeys[i])) {
                    if (fieldTypes[i].equals("combo")) {
                        fieldValues[i] = myatr.getAttribute(fieldKeys[i]).toString() + "," + fieldValues[i];
                    } else {
                        fieldValues[i] = myatr.getAttribute(fieldKeys[i]).toString();
                    }
                }
            }
            PropertiesDialog propertiesDialog = new PropertiesDialog(null, fieldNames, fieldTypes, fieldValues, Language.getInstance().getText("TableEdit"), true);
            propertiesDialog.setVisible(true);
            if (!propertiesDialog.getDecisionValue().equals(Language.getInstance().getText("DialogCancel"))) {
                int myStart  = elementParent.getStartOffset();
                int myLength = elementParent.getEndOffset() - myStart;
                String myAtributes = "";
                SimpleAttributeSet mynew = new SimpleAttributeSet();
                for(int i = 0; i < fieldNames.length; i++) {
                    String propValue = propertiesDialog.getFieldValue(fieldNames[i]);
                    if(propValue != null && propValue.length() > 0) {
                        myAtributes=myAtributes+fieldNames[i] + "=\"" + propValue + "\" ";
                        mynew.addAttribute(fieldKeys[i],propValue);
                    }
                }
                htmlDoc.replaceAttributes(elementParent, mynew, HTML.Tag.TABLE);
                refreshOnUpdate();
            }
            propertiesDialog.dispose();
        } else {
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Table"), true, Language.getInstance().getText("CursorNotInTable"));
        }
    }

    /**
     * Method for editing HTML Table cells
     */
    private void editCell() {
        int caretPos = jtpMain.getCaretPosition();
        Element element = htmlDoc.getCharacterElement(caretPos);
        Element elementParent = element.getParentElement();
        while (elementParent != null && !elementParent.getName().equals("td")) {
            elementParent = elementParent.getParentElement();
        }
        if (elementParent != null) {
            HTML.Attribute[] fieldKeys = {
                HTML.Attribute.WIDTH,HTML.Attribute.HEIGHT,HTML.Attribute.ALIGN,HTML.Attribute.VALIGN,
                HTML.Attribute.BGCOLOR };
            String[] fieldNames  = { "width", "height", "align", "valign", "bgcolor" };
            String[] fieldTypes  = { "text",  "text",   "combo", "combo",  "combo" };
            String[] fieldValues = { "",      "",       "left,right,center", "top,middle,bottom", "none,aqua,black,fuchsia,gray,green,lime,maroon,navy,olive,purple,red,silver,teal,white,yellow" };
            MutableAttributeSet myatr = (MutableAttributeSet)elementParent.getAttributes();
            for (int i = 0; i < fieldNames.length; i++) {
                if (myatr.isDefined(fieldKeys[i])) {
                    if (fieldTypes[i].equals("combo")) {
                        fieldValues[i] = myatr.getAttribute(fieldKeys[i]).toString() + "," + fieldValues[i];
                    } else {
                        fieldValues[i] = myatr.getAttribute(fieldKeys[i]).toString();
                    }
                }
            }
            PropertiesDialog propertiesDialog = new PropertiesDialog(null, fieldNames, fieldTypes, fieldValues, Language.getInstance().getText("TableCellEdit"), true);
            propertiesDialog.setVisible(true);
            if (!propertiesDialog.getDecisionValue().equals(Language.getInstance().getText("DialogCancel"))) {
                int myStart  = elementParent.getStartOffset();
                int myLength = elementParent.getEndOffset()-myStart;
                String myAtributes = "";
                SimpleAttributeSet mynew = new SimpleAttributeSet();
                for (int i = 0; i < fieldNames.length; i++) {
                    String propValue = propertiesDialog.getFieldValue(fieldNames[i]);
                    if (propValue != null && propValue.length() > 0) {
                        myAtributes = myAtributes + fieldNames[i] + "=\"" + propValue + "\" ";
                        mynew.addAttribute(fieldKeys[i],propValue);
                    }
                }
                htmlDoc.replaceAttributes(elementParent, mynew, HTML.Tag.TD);
                refreshOnUpdate();
            }
            propertiesDialog.dispose();
        } else {
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Cell"), true, Language.getInstance().getText("CursorNotInCell"));
        }
    }

    /**
     * Method for inserting a row into an HTML Table
     */
    private void insertTableRow() {
        int caretPos = jtpMain.getCaretPosition();
        Element element = htmlDoc.getCharacterElement(jtpMain.getCaretPosition());
        Element elementParent = element.getParentElement();
        int startPoint  = -1;
        int columnCount = -1;
        while (elementParent != null && !elementParent.getName().equals("body")) {
            if (elementParent.getName().equals("tr")) {
                startPoint  = elementParent.getStartOffset();
                columnCount = elementParent.getElementCount();
                break;
            } else {
                elementParent = elementParent.getParentElement();
            }
        }
        if (startPoint > -1 && columnCount > -1) {
            jtpMain.setCaretPosition(startPoint);
            StringBuffer sRow = new StringBuffer();
            sRow.append("<TR>");
            for (int i = 0; i < columnCount; i++) {
                sRow.append("<TD></TD>");
            }
            sRow.append("</TR>");
            ActionEvent actionEvent = new ActionEvent(jtpMain, 0, "insertTableRow");
            new HTMLEditorKit.InsertHTMLTextAction("insertTableRow", sRow.toString(), HTML.Tag.TABLE, HTML.Tag.TR).actionPerformed(actionEvent);
            refreshOnUpdate();
            jtpMain.setCaretPosition(caretPos);
        }
    }

    /**
     * Method for inserting a column into an HTML Table
     */
    private void insertTableColumn() {
        int caretPos = jtpMain.getCaretPosition();
        Element element = htmlDoc.getCharacterElement(jtpMain.getCaretPosition());
        Element elementParent = element.getParentElement();
        int startPoint = -1;
        int rowCount   = -1;
        int cellOffset =  0;
        while (elementParent != null && !elementParent.getName().equals("body")) {
            if (elementParent.getName().equals("table")) {
                startPoint = elementParent.getStartOffset();
                rowCount   = elementParent.getElementCount();
                break;
            } else if(elementParent.getName().equals("tr")) {
                int rowStart = elementParent.getStartOffset();
                int rowCells = elementParent.getElementCount();
                for (int i = 0; i < rowCells; i++) {
                    Element currentCell = elementParent.getElement(i);
                    if (jtpMain.getCaretPosition() >= currentCell.getStartOffset()
                        && jtpMain.getCaretPosition() <= currentCell.getEndOffset()) {
                        cellOffset = i;
                    }
                }
                elementParent = elementParent.getParentElement();
            } else {
                elementParent = elementParent.getParentElement();
            }
        }
        if (startPoint > -1 && rowCount > -1) {
            jtpMain.setCaretPosition(startPoint);
            String sCell = "<TD></TD>";
            ActionEvent actionEvent = new ActionEvent(jtpMain, 0, "insertTableCell");
            for (int i = 0; i < rowCount; i++) {
                Element row = elementParent.getElement(i);
                Element whichCell = row.getElement(cellOffset);
                jtpMain.setCaretPosition(whichCell.getStartOffset());
                new HTMLEditorKit.InsertHTMLTextAction("insertTableCell", sCell, HTML.Tag.TR, HTML.Tag.TD, HTML.Tag.TH, HTML.Tag.TD).actionPerformed(actionEvent);
            }
            refreshOnUpdate();
            jtpMain.setCaretPosition(caretPos);
        }
    }

    /**
     * Method for inserting a cell into an HTML Table
     */
    private void insertTableCell() {
        String sCell = "<TD></TD>";
        ActionEvent actionEvent = new ActionEvent(jtpMain, 0, "insertTableCell");
        new HTMLEditorKit.InsertHTMLTextAction("insertTableCell", sCell, HTML.Tag.TR, HTML.Tag.TD, HTML.Tag.TH, HTML.Tag.TD).actionPerformed(actionEvent);
        refreshOnUpdate();
    }

    /**
     * Method for deleting a row from an HTML Table
     */
    private void deleteTableRow() throws BadLocationException {
        int caretPos = jtpMain.getCaretPosition();
        Element element = htmlDoc.getCharacterElement(jtpMain.getCaretPosition());
        Element elementParent = element.getParentElement();
        int startPoint = -1;
        int endPoint   = -1;
        while (elementParent != null && !elementParent.getName().equals("body")) {
            if (elementParent.getName().equals("tr")) {
                startPoint = elementParent.getStartOffset();
                endPoint   = elementParent.getEndOffset();
                break;
            } else {
                elementParent = elementParent.getParentElement();
            }
        }
        if (startPoint > -1 && endPoint > startPoint) {
            htmlDoc.remove(startPoint, endPoint - startPoint);
            jtpMain.setDocument(htmlDoc);
            registerDocument(htmlDoc);
            refreshOnUpdate();
            if (caretPos >= htmlDoc.getLength()) {
                caretPos = htmlDoc.getLength() - 1;
            }
            jtpMain.setCaretPosition(caretPos);
        }
    }

    /**
     * Method for deleting a column from an HTML Table
     */
    private void deleteTableColumn() throws BadLocationException {
        int caretPos = jtpMain.getCaretPosition();
        Element element       = htmlDoc.getCharacterElement(jtpMain.getCaretPosition());
        Element elementParent = element.getParentElement();
        Element elementCell   = null;
        Element elementRow    = null;
        Element elementTable  = null;
        // Locate the table, row, and cell location of the cursor
        while (elementParent != null && !elementParent.getName().equals("body")) {
            if (elementParent.getName().equals("td")) {
                elementCell = elementParent;
            } else if(elementParent.getName().equals("tr")) {
                elementRow = elementParent;
            } else if(elementParent.getName().equals("table")) {
                elementTable = elementParent;
            }
            elementParent = elementParent.getParentElement();
        }
        int whichColumn = -1;
        if (elementCell != null && elementRow != null && elementTable != null) {
            // Find the column the cursor is in
            int myOffset = 0;
            for (int i = 0; i < elementRow.getElementCount(); i++) {
                if (elementCell == elementRow.getElement(i)) {
                    whichColumn = i;
                    myOffset = elementCell.getEndOffset();
                }
            }
            if (whichColumn > -1) {
                // Iterate through the table rows, deleting cells from the indicated column
                int mycaretPos = caretPos;
                for (int i = 0; i < elementTable.getElementCount(); i++) {
                    elementRow  = elementTable.getElement(i);
                    elementCell = (elementRow.getElementCount() > whichColumn ? elementRow.getElement(whichColumn) : elementRow.getElement(elementRow.getElementCount() - 1));
                    int columnCellStart = elementCell.getStartOffset();
                    int columnCellEnd   = elementCell.getEndOffset();
                    int dif     = columnCellEnd - columnCellStart;
                    if (columnCellStart < myOffset) {
                        mycaretPos = mycaretPos - dif;
                        myOffset = myOffset-dif;
                    }
                    if (whichColumn==0) {
                        htmlDoc.remove(columnCellStart, dif);
                    } else {
                        htmlDoc.remove(columnCellStart-1, dif);
                    }
                }
                jtpMain.setDocument(htmlDoc);
                registerDocument(htmlDoc);
                refreshOnUpdate();
                if (mycaretPos >= htmlDoc.getLength()) {
                    mycaretPos = htmlDoc.getLength() - 1;
                }
                if (mycaretPos < 1) {
                    mycaretPos =  1;
                }
                jtpMain.setCaretPosition(mycaretPos);
            }
        }
    }

    /**
     * Method for inserting a break (BR) element
     */
    private void insertBreak() throws IOException, BadLocationException, RuntimeException {
        int caretPos = jtpMain.getCaretPosition();
        htmlKit.insertHTML(htmlDoc, caretPos, "<BR>", 0, 0, HTML.Tag.BR);
        jtpMain.setCaretPosition(caretPos + 1);
    }

    /**
     * Method for opening the Unicode dialog
     */
    /*private void insertUnicode(int index)
      throws IOException, BadLocationException, RuntimeException
      {
      UnicodeDialog unicodeInput = new UnicodeDialog(this, Language.getInstance().getText("UnicodeDialogTitle"), false, index);
      }*/

    /**
     * Method for inserting Unicode characters via the UnicodeDialog class
     */
    /*public void insertUnicodeChar(String sChar)
      throws IOException, BadLocationException, RuntimeException
      {
      int caretPos = jtpMain.getCaretPosition();
      if(sChar != null)
      {
      htmlDoc.insertString(caretPos, sChar, jtpMain.getInputAttributes());
      jtpMain.setCaretPosition(caretPos + 1);
      }
      }*/

    /**
     * Method for inserting a non-breaking space (&nbsp;)
     */
    private void insertNonbreakingSpace() throws IOException, BadLocationException, RuntimeException {
        int caretPos = jtpMain.getCaretPosition();
        htmlDoc.insertString(caretPos, "\240", jtpMain.getInputAttributes());
        jtpMain.setCaretPosition(caretPos + 1);
    }

    /**
     * Method for inserting a form element
     */
    private void insertFormElement(HTML.Tag baseTag, String baseElement, Hashtable attribs, String[] fieldNames,
                                   String[] fieldTypes, String[] fieldValues, boolean hasClosingTag)
        throws IOException, BadLocationException, RuntimeException {
        int caretPos = jtpMain.getCaretPosition();
        StringBuffer compositeElement = new StringBuffer("<" + baseElement);
        if (attribs != null && attribs.size() > 0) {
            Enumeration attribEntries = attribs.keys();
            while (attribEntries.hasMoreElements()) {
                Object entryKey   = attribEntries.nextElement();
                Object entryValue = attribs.get(entryKey);
                if (entryValue != null && entryValue != "") {
                    compositeElement.append(" " + entryKey + "=" + '"' + entryValue + '"');
                }
            }
        }
        if (fieldNames != null && fieldNames.length > 0) {
            PropertiesDialog propertiesDialog = new PropertiesDialog(null, fieldNames, fieldTypes, fieldValues, Language.getInstance().getText("FormDialogTitle"), true);
            propertiesDialog.setVisible(true);
            String decision = propertiesDialog.getDecisionValue();
            if (decision.equals(Language.getInstance().getText("DialogCancel"))) {
                propertiesDialog.dispose();
                propertiesDialog = null;
                return;
            } else {
                for (int iter = 0; iter < fieldNames.length; iter++) {
                    String fieldName = fieldNames[iter];
                    String propValue = propertiesDialog.getFieldValue(fieldName);
                    if (propValue != null && propValue.length() > 0) {
                        if (fieldName.equals("checked")) {
                            if (propValue.equals("true")) {
                                compositeElement.append(" " + fieldName + "=" + '"' + propValue + '"');
                            }
                        } else {
                            compositeElement.append(" " + fieldName + "=" + '"' + propValue + '"');
                        }
                    }
                }
            }
            propertiesDialog.dispose();
            propertiesDialog = null;
        }
        // --- Convenience for editing, this makes the FORM visible
        if (useFormIndicator && baseElement.equals("form")) {
            compositeElement.append(" bgcolor=" + '"' + clrFormIndicator + '"');
        }
        // --- END
        compositeElement.append(">");
        if (baseTag == HTML.Tag.FORM) {
            compositeElement.append("<P>&nbsp;</P>");
            compositeElement.append("<P>&nbsp;</P>");
            compositeElement.append("<P>&nbsp;</P>");
        }
        if (hasClosingTag) {
            compositeElement.append("</" + baseElement + ">");
        }
        if (baseTag == HTML.Tag.FORM) {
            compositeElement.append("<P>&nbsp;</P>");
        }
        htmlKit.insertHTML(htmlDoc, caretPos, compositeElement.toString(), 0, 0, baseTag);
        // jtpMain.setCaretPosition(caretPos + 1);
        refreshOnUpdate();
    }

    /**
     * Alternate method call for inserting a form element
     */
    private void insertFormElement(HTML.Tag baseTag, String baseElement, Hashtable attribs, String[] fieldNames,
                                   String[] fieldTypes, boolean hasClosingTag)  throws IOException, BadLocationException, RuntimeException {
        insertFormElement(baseTag, baseElement, attribs, fieldNames, fieldTypes, new String[fieldNames.length], hasClosingTag);
    }

    /**
     * Method that handles initial list insertion and deletion
     */
    public void manageListElement(Element element) {
        Element h = htmlUtilities.getListItemParent();
        Element listElement = h.getParentElement();
        if (h != null) {
            htmlUtilities.removeTag(h, true);
        }
    }

    /**
     * Method to initiate a find/replace operation
     */
    private void doSearch(String searchFindTerm, String searchReplaceTerm, boolean bIsFindReplace,
                          boolean bCaseSensitive, boolean bStartAtTop) {
        boolean bReplaceAll = false;
        JTextComponent searchPane = jtpMain;
        if (jspSource.isShowing() || jtpSource.hasFocus()) {
            searchPane = jtpSource;
        }
        if (searchFindTerm == null || (bIsFindReplace && searchReplaceTerm == null)) {
            SearchDialog sdSearchInput = new SearchDialog(null, Language.getInstance().getText("SearchDialogTitle"), true, bIsFindReplace, bCaseSensitive, bStartAtTop);
            searchFindTerm    = sdSearchInput.getFindTerm();
            searchReplaceTerm = sdSearchInput.getReplaceTerm();
            bCaseSensitive    = sdSearchInput.getCaseSensitive();
            bStartAtTop       = sdSearchInput.getStartAtTop();
            bReplaceAll       = sdSearchInput.getReplaceAll();
        }
        if (searchFindTerm != null && (!bIsFindReplace || searchReplaceTerm != null)) {
            if (bReplaceAll) {
                int results = findText(searchFindTerm, searchReplaceTerm, bCaseSensitive, 0);
                int findOffset = 0;
                if (results > -1) {
                    while (results > -1) {
                        findOffset = findOffset + searchReplaceTerm.length();
                        results    = findText(searchFindTerm, searchReplaceTerm, bCaseSensitive, findOffset);
                    }
                } else {
                    SimpleInfoDialog sidWarn = new SimpleInfoDialog(null, "", true, Language.getInstance().getText("ErrorNoOccurencesFound") + ":\n" + searchFindTerm, SimpleInfoDialog.WARNING);
                }
            } else {
                int results = findText(searchFindTerm, searchReplaceTerm, bCaseSensitive, (bStartAtTop ? 0 : searchPane.getCaretPosition()));
                if (results == -1) {
                    SimpleInfoDialog sidWarn = new SimpleInfoDialog(null, "", true, Language.getInstance().getText("ErrorNoMatchFound") + ":\n" + searchFindTerm, SimpleInfoDialog.WARNING);
                }
            }
            lastSearchFindTerm    = new String(searchFindTerm);
            if (searchReplaceTerm != null) {
                lastSearchReplaceTerm = new String(searchReplaceTerm);
            } else {
                lastSearchReplaceTerm = null;
            }
            lastSearchCaseSetting = bCaseSensitive;
            lastSearchTopSetting  = bStartAtTop;
        }
    }

    /**
     * Method for finding (and optionally replacing) a string in the text
     */
    private int findText(String findTerm, String replaceTerm, boolean bCaseSenstive, int iOffset) {
        JTextComponent jtpFindSource;
        if (isSourceWindowActive() || jtpSource.hasFocus())     {
            jtpFindSource = jtpSource;
        } else {
            jtpFindSource = jtpMain;
        }
        int searchPlace = -1;
        try     {
            Document baseDocument = jtpFindSource.getDocument();
            searchPlace =
                (bCaseSenstive ?
                 baseDocument.getText(0, baseDocument.getLength()).indexOf(findTerm, iOffset) :
                 baseDocument.getText(0, baseDocument.getLength()).toLowerCase().indexOf(findTerm.toLowerCase(), iOffset)
                 );
            if (searchPlace > -1) {
                if (replaceTerm != null) {
                    AttributeSet attribs = null;
                    if (baseDocument instanceof HTMLDocument) {
                        Element element = ((HTMLDocument)baseDocument).getCharacterElement(searchPlace);
                        attribs = element.getAttributes();
                    }
                    baseDocument.remove(searchPlace, findTerm.length());
                    baseDocument.insertString(searchPlace, replaceTerm, attribs);
                    jtpFindSource.setCaretPosition(searchPlace + replaceTerm.length());
                    jtpFindSource.requestFocus();
                    jtpFindSource.select(searchPlace, searchPlace + replaceTerm.length());
                } else {
                    jtpFindSource.setCaretPosition(searchPlace + findTerm.length());
                    jtpFindSource.requestFocus();
                    jtpFindSource.select(searchPlace, searchPlace + findTerm.length());
                }
            }
        } catch(BadLocationException ble) {
            logException("BadLocationException in actionPerformed method", ble);
            SimpleInfoDialog sidAbout = new SimpleInfoDialog(null, Language.getInstance().getText("Error"), true, Language.getInstance().getText("ErrorBadLocationException"), SimpleInfoDialog.ERROR);
        }
        return searchPlace;
    }

    /**
     * Method for inserting an image from a file
     */
    private void insertLocalImage(File whatImage) throws IOException, BadLocationException, RuntimeException {
        if (whatImage == null) {
            whatImage = getImageFromChooser(imageChooserStartDir, extsIMG, Language.getInstance().getText("FiletypeIMG"));
        }
        if (whatImage != null) {
            imageChooserStartDir = whatImage.getParent().toString();
            int caretPos = jtpMain.getCaretPosition();
            htmlKit.insertHTML(htmlDoc, caretPos, "<IMG SRC=\"" + whatImage + "\">", 0, 0, HTML.Tag.IMG);
            jtpMain.setCaretPosition(caretPos + 1);
            refreshOnUpdate();
        }
    }

    /**
     * Method for inserting an image from a URL
     */
    public void insertURLImage() throws IOException, BadLocationException, RuntimeException {
        ImageURLDialog iurlDialog = new ImageURLDialog(null, Language.getInstance().getText("ImageURLDialogTitle"), true);
        iurlDialog.pack();
        iurlDialog.setVisible(true);
        String whatImage = iurlDialog.getURL();
        if (whatImage != null) {
            int caretPos = jtpMain.getCaretPosition();
            htmlKit.insertHTML(htmlDoc, caretPos, "<IMG SRC=\"" + whatImage + "\">", 0, 0, HTML.Tag.IMG);
            jtpMain.setCaretPosition(caretPos + 1);
            refreshOnUpdate();
        }
    }

    /**
     * Empty spell check method, overwritten by spell checker extension class
     */
    public void checkDocumentSpelling(Document doc) {}

    /**
     * Method for saving text as a complete HTML document
     */
    private void writeOut(HTMLDocument doc, File whatFile) throws IOException, BadLocationException {
        if (whatFile == null) {
            whatFile = getFileFromChooser(".", JFileChooser.SAVE_DIALOG, extsHTML, Language.getInstance().getText("FiletypeHTML"));
        }
        if (whatFile != null) {
            FileWriter fw = new FileWriter(whatFile);
            htmlKit.write(fw, doc, 0, doc.getLength());
            fw.flush();
            fw.close();
            currentFile = whatFile;
            updateTitle();
        }
        refreshOnUpdate();
    }

    /**
     * Method for saving text as an HTML fragment
     */
    private void writeOutFragment(HTMLDocument doc, String containingTag) throws IOException, BadLocationException {
        File whatFile = getFileFromChooser(".", JFileChooser.SAVE_DIALOG, extsHTML, Language.getInstance().getText("FiletypeHTML"));
        if (whatFile != null) {
            FileWriter fw = new FileWriter(whatFile);
            // Element eleBody = locateElementInDocument((StyledDocument)doc, containingTag);
            // htmlKit.write(fw, doc, eleBody.getStartOffset(), eleBody.getEndOffset());
            String docTextCase = jtpSource.getText().toLowerCase();
            int tagStart       = docTextCase.indexOf("<" + containingTag.toLowerCase());
            int tagStartClose  = docTextCase.indexOf(">", tagStart) + 1;
            String closeTag    = "</" + containingTag.toLowerCase() + ">";
            int tagEndOpen     = docTextCase.indexOf(closeTag);
            if (tagStartClose < 0) {
                tagStartClose = 0;
            }
            if (tagEndOpen < 0 || tagEndOpen > docTextCase.length()) {
                tagEndOpen = docTextCase.length();
            }
            String bodyText = jtpSource.getText().substring(tagStartClose, tagEndOpen);
            fw.write(bodyText, 0, bodyText.length());
            fw.flush();
            fw.close();
        }
        refreshOnUpdate();
    }

    /**
     * Method for saving text as an RTF document
     */
    private void writeOutRTF(StyledDocument doc) throws IOException, BadLocationException {
        File whatFile = getFileFromChooser(".", JFileChooser.SAVE_DIALOG, extsRTF, Language.getInstance().getText("FiletypeRTF"));
        if (whatFile != null) {
            FileOutputStream fos = new FileOutputStream(whatFile);
            RTFEditorKit rtfKit = new RTFEditorKit();
            rtfKit.write(fos, doc, 0, doc.getLength());
            fos.flush();
            fos.close();
        }
        refreshOnUpdate();
    }

    /**
     * Method for saving text as a Base64 encoded document
     */
    private void writeOutBase64(String text) throws IOException, BadLocationException {
        File whatFile = getFileFromChooser(".", JFileChooser.SAVE_DIALOG, extsB64, Language.getInstance().getText("FiletypeB64"));
        if (whatFile != null) {
            String base64text = Base64Codec.encode(text);
            FileWriter fw = new FileWriter(whatFile);
            fw.write(base64text, 0, base64text.length());
            fw.flush();
            fw.close();
        }
        refreshOnUpdate();
    }

    /**
     * Method to invoke loading HTML into the app
     */
    private void openDocument(File whatFile) throws IOException, BadLocationException {
        if (whatFile == null) {
            whatFile = getFileFromChooser(".", JFileChooser.OPEN_DIALOG, extsHTML, Language.getInstance().getText("FiletypeHTML"));
        }
        if (whatFile != null) {
            try {
                loadDocument(whatFile, null);
            } catch(ChangedCharSetException ccse) {
                String charsetType = ccse.getCharSetSpec().toLowerCase();
                int pos = charsetType.indexOf("charset");
                if (pos == -1) {
                    throw ccse;
                }
                while (pos < charsetType.length() && charsetType.charAt(pos) != '=') {
                    pos++;
                }
                pos++; // Places file cursor past the equals sign (=)
                String whatEncoding = charsetType.substring(pos).trim();
                loadDocument(whatFile, whatEncoding);
            }
        }
        refreshOnUpdate();
    }

    /**
     * Method for loading HTML document
     */
    public void loadDocument(File whatFile)     throws IOException, BadLocationException {
        try {
            loadDocument(whatFile, null);
        } catch(ChangedCharSetException ccse) {
            String charsetType = ccse.getCharSetSpec().toLowerCase();
            int pos = charsetType.indexOf("charset");
            if (pos == -1) {
                throw ccse;
            }
            while (pos < charsetType.length() && charsetType.charAt(pos) != '=') {
                pos++;
            }
            pos++; // Places file cursor past the equals sign (=)
            String whatEncoding = charsetType.substring(pos).trim();
            loadDocument(whatFile, whatEncoding);
        }
        refreshOnUpdate();
    }

    /**
     * Method for loading HTML document into the app, including document encoding setting
     */
    private void loadDocument(File whatFile, String whatEncoding) throws IOException, BadLocationException {
        Reader r = null;
        htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
        htmlDoc.putProperty("com.hexidec.ekit.docsource", whatFile.toString());
        try     {
            if (whatEncoding == null) {
                r = new InputStreamReader(new FileInputStream(whatFile));
            } else {
                r = new InputStreamReader(new FileInputStream(whatFile), whatEncoding);
                htmlDoc.putProperty("IgnoreCharsetDirective", new Boolean(true));
            }
            htmlKit.read(r, htmlDoc, 0);
            r.close();
            registerDocument(htmlDoc);
            jtpSource.setText(jtpMain.getText());
            currentFile = whatFile;
            updateTitle();
        } finally {
            if (r != null) {
                r.close();
            }
        }
    }

    /**
     * Method for loading a Base64 encoded document
     */
    private void openDocumentBase64(File whatFile) throws IOException, BadLocationException {
        if (whatFile == null) {
            whatFile = getFileFromChooser(".", JFileChooser.OPEN_DIALOG, extsB64, Language.getInstance().getText("FiletypeB64"));
        }
        if (whatFile != null) {
            FileReader fr = new FileReader(whatFile);
            int nextChar = 0;
            StringBuffer encodedText = new StringBuffer();
            try {
                while ((nextChar = fr.read()) != -1) {
                    encodedText.append((char)nextChar);
                }
                fr.close();
                jtpSource.setText(Base64Codec.decode(encodedText.toString()));
                jtpMain.setText(jtpSource.getText());
                registerDocument((ExtendedHTMLDocument)(jtpMain.getDocument()));
            } finally {
                if (fr != null) {
                    fr.close();
                }
            }
        }
    }

    /**
     * Method for loading a Stylesheet into the app
     */
    private void openStyleSheet(File fileCSS) throws IOException {
        if (fileCSS == null) {
            fileCSS = getFileFromChooser(".", JFileChooser.OPEN_DIALOG, extsCSS, Language.getInstance().getText("FiletypeCSS"));
        }
        if (fileCSS != null) {
            String currDocText = jtpMain.getText();
            htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
            styleSheet = htmlDoc.getStyleSheet();
            URL cssUrl = fileCSS.toURL();
            InputStream is = cssUrl.openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            styleSheet.loadRules(br, cssUrl);
            br.close();
            htmlDoc = new ExtendedHTMLDocument(styleSheet);
            registerDocument(htmlDoc);
            jtpMain.setText(currDocText);
            jtpSource.setText(jtpMain.getText());
        }
        refreshOnUpdate();
    }

    /**
     * Method for serializing the document out to a file
     */
    public void serializeOut(HTMLDocument doc) throws IOException {
        File whatFile = getFileFromChooser(".", JFileChooser.SAVE_DIALOG, extsSer, Language.getInstance().getText("FiletypeSer"));
        if (whatFile != null) {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(whatFile));
            oos.writeObject(doc);
            oos.flush();
            oos.close();
        }
        refreshOnUpdate();
    }

    /**
     * Method for reading in a serialized document from a file
     */
    public void serializeIn() throws IOException, ClassNotFoundException {
        File whatFile = getFileFromChooser(".", JFileChooser.OPEN_DIALOG, extsSer, Language.getInstance().getText("FiletypeSer"));
        if (whatFile != null) {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(whatFile));
            htmlDoc = (ExtendedHTMLDocument)(ois.readObject());
            ois.close();
            registerDocument(htmlDoc);
            validate();
        }
        refreshOnUpdate();
    }

    /**
     * Method for obtaining a File for input/output using a JFileChooser dialog
     */
    private File getFileFromChooser(String startDir, int dialogType, String[] exts, String desc) {
        JFileChooser jfileDialog = new JFileChooser(startDir);
        jfileDialog.setDialogType(dialogType);
        jfileDialog.setFileFilter(new MutableFilter(exts, desc));
        int optionSelected = JFileChooser.CANCEL_OPTION;
        if (dialogType == JFileChooser.OPEN_DIALOG) {
            optionSelected = jfileDialog.showOpenDialog(this);
        } else if(dialogType == JFileChooser.SAVE_DIALOG) {
            optionSelected = jfileDialog.showSaveDialog(this);
        } else {// default to an OPEN_DIALOG
            optionSelected = jfileDialog.showOpenDialog(this);
        }
        if (optionSelected == JFileChooser.APPROVE_OPTION) {
            return jfileDialog.getSelectedFile();
        }
        return null;
    }

    /**
     * Method for obtaining an Image for input using a custom JFileChooser dialog
     */
    private File getImageFromChooser(String startDir, String[] exts, String desc) {
        ImageFileChooser jImageDialog = new ImageFileChooser(startDir);
        jImageDialog.setDialogType(JFileChooser.CUSTOM_DIALOG);
        jImageDialog.setFileFilter(new MutableFilter(exts, desc));
        jImageDialog.setDialogTitle(Language.getInstance().getText("ImageDialogTitle"));
        int optionSelected = JFileChooser.CANCEL_OPTION;
        optionSelected = jImageDialog.showDialog(this, Language.getInstance().getText("Insert"));
        if (optionSelected == JFileChooser.APPROVE_OPTION) {
            return jImageDialog.getSelectedFile();
        }
        return null;
    }

    /**
     * Method for describing the node hierarchy of the document
     */
    private void describeDocument(StyledDocument doc) {
        Element[] elements = doc.getRootElements();
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < elements.length; i++) {
            indent = indentStep;
            for (int j = 0; j < indent; j++) {
                buffer.append(" ");
            }
            buffer.append(elements[i]);
            traverseElement(elements[i]);
            Util.debug(buffer);
        }
    }

    /**
     * Traverses nodes for the describing method
     */
    private void traverseElement(Element element) {
        indent += indentStep;
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < element.getElementCount(); i++)     {
            for (int j = 0; j < indent; j++) {
                buffer.append(" ");
            }
            buffer.append(element.getElement(i));
            traverseElement(element.getElement(i));
        }
        indent -= indentStep;
        Util.debug(buffer);
    }

    /**
     * Method to locate a node element by name
     */
    private Element locateElementInDocument(StyledDocument doc, String elementName) {
        Element[] elements = doc.getRootElements();
        for (int i = 0; i < elements.length; i++) {
            if (elements[i].getName().equalsIgnoreCase(elementName)) {
                return elements[i];
            } else {
                Element rtnElement = locateChildElementInDocument(elements[i], elementName);
                if (rtnElement != null) {
                    return rtnElement;
                }
            }
        }
        return null;
    }

    /**
     * Traverses nodes for the locating method
     */
    private Element locateChildElementInDocument(Element element, String elementName) {
        for (int i = 0; i < element.getElementCount(); i++)     {
            if (element.getElement(i).getName().equalsIgnoreCase(elementName)) {
                return element.getElement(i);
            }
        }
        return null;
    }

    /**
     * Convenience method for obtaining the WYSIWYG JTextPane
     */
    public JTextPane getTextPane() {
        return jtpMain;
    }

    /**
     * Convenience method for obtaining the Source JTextPane
     */
    public JTextArea getSourcePane() {
        return jtpSource;
    }

    /**
     * Convenience method for obtaining the application as a Frame
     */
    /*public Frame getFrame() {
      return frameHandler;
      }*/

    /**
     * Convenience method for setting the parent Frame
     */
    /*public void setFrame(Frame parentFrame)   {
      frameHandler = parentFrame;
      }*/

    /**
     * Convenience method for obtaining the pre-generated menu bar
     */
    /*public JMenuBar getMenuBar()
      {
      return jMenuBar;
      }*/

    /**
     * Convenience method for obtaining a custom menu bar
     */
    /*public JMenuBar getCustomMenuBar(Vector vcMenus)
      {
      jMenuBar = new JMenuBar();
      for(int i = 0; i < vcMenus.size(); i++)
      {
      String menuToAdd = ((String)(vcMenus.elementAt(i))).toLowerCase();
      if(htMenus.containsKey(menuToAdd))
      {
      jMenuBar.add((JMenu)(htMenus.get(menuToAdd)));
      }
      }
      return jMenuBar;
      }*/

    /**
     * Convenience method for creating the multiple toolbar set from a sequence string
     */
    public void initializeMultiToolbars(String toolbarSeq) {
        Vector[] vcToolPicks = new Vector[3];
        vcToolPicks[0] = new Vector();
        vcToolPicks[1] = new Vector();
        vcToolPicks[2] = new Vector();

        int whichBar = 0;
        StringTokenizer stToolbars = new StringTokenizer(toolbarSeq.toUpperCase(), "|");
        while(stToolbars.hasMoreTokens()) {
            String sKey = stToolbars.nextToken();
            if (sKey.equals("*")) {
                whichBar++;
                if (whichBar > 2) {
                    whichBar = 2;
                }
            } else {
                vcToolPicks[whichBar].add(sKey);
            }
        }

        customizeToolBar(TOOLBAR_MAIN,   vcToolPicks[0], true);
        customizeToolBar(TOOLBAR_FORMAT, vcToolPicks[1], true);
        customizeToolBar(TOOLBAR_STYLES, vcToolPicks[2], true);
    }

    /**
     * Convenience method for creating the single toolbar from a sequence string
     */
    public void initializeSingleToolbar(String toolbarSeq) {
        Vector vcToolPicks = new Vector();
        StringTokenizer stToolbars = new StringTokenizer(toolbarSeq.toUpperCase(), "|");
        while (stToolbars.hasMoreTokens()) {
            String sKey = stToolbars.nextToken();
            if (sKey.equals("*")) {
                // ignore "next toolbar" symbols in single toolbar processing
            } else {
                vcToolPicks.add(sKey);
            }
        }

        customizeToolBar(TOOLBAR_SINGLE, vcToolPicks, true);
    }

    /**
     * Convenience method for obtaining the pre-generated toolbar
     */
    public JToolBar getToolBar(boolean isShowing) {
        if (jToolBar != null) {
            //jcbmiViewToolbar.setState(isShowing);
            return jToolBar;
        }
        return null;
    }

    /**
     * Convenience method for obtaining the pre-generated main toolbar
     */
    public JToolBar getToolBarMain(boolean isShowing) {
        if (jToolBarMain != null) {
            //jcbmiViewToolbarMain.setState(isShowing);
            return jToolBarMain;
        }
        return null;
    }

    /**
     * Convenience method for obtaining the pre-generated main toolbar
     */
    public JToolBar getToolBarFormat(boolean isShowing) {
        if (jToolBarFormat != null) {
            //jcbmiViewToolbarFormat.setState(isShowing);
            return jToolBarFormat;
        }
        return null;
    }

    /**
     * Convenience method for obtaining the pre-generated main toolbar
     */
    public JToolBar getToolBarStyles(boolean isShowing) {
        if (jToolBarStyles != null) {
            //jcbmiViewToolbarStyles.setState(isShowing);
            return jToolBarStyles;
        }
        return null;
    }

    /**
     * Convenience method for obtaining a custom toolbar
     */
    public JToolBar customizeToolBar(int whichToolBar, Vector vcTools, boolean isShowing) {
        JToolBar jToolBarX = new JToolBar(SwingConstants.HORIZONTAL);
        jToolBarX.setFloatable(false);
        for (int i = 0; i < vcTools.size(); i++) {
            String toolToAdd = ((String)(vcTools.elementAt(i))).toUpperCase();
            if (toolToAdd.equals(KEY_TOOL_SEP)) {
                jToolBarX.add(new JToolBar.Separator());
            } else if (htTools.containsKey(toolToAdd)) {
                if (htTools.get(toolToAdd) instanceof JButtonNoFocus) {
                    jToolBarX.add((JButtonNoFocus)(htTools.get(toolToAdd)));
                } else if (htTools.get(toolToAdd) instanceof JToggleButtonNoFocus) {
                    jToolBarX.add((JToggleButtonNoFocus)(htTools.get(toolToAdd)));
                } else if (htTools.get(toolToAdd) instanceof JComboBoxNoFocus) {
                    jToolBarX.add((JComboBoxNoFocus)(htTools.get(toolToAdd)));
                } else {
                    jToolBarX.add((JComponent)(htTools.get(toolToAdd)));
                }
            }
        }
        if (whichToolBar == TOOLBAR_SINGLE) {
            jToolBar = jToolBarX;
            jToolBar.setVisible(isShowing);
            //jcbmiViewToolbar.setSelected(isShowing);
            return jToolBar;
        } else if (whichToolBar == TOOLBAR_MAIN) {
            jToolBarMain = jToolBarX;
            jToolBarMain.setVisible(isShowing);
            //jcbmiViewToolbarMain.setSelected(isShowing);
            return jToolBarMain;
        } else if (whichToolBar == TOOLBAR_FORMAT) {
            jToolBarFormat = jToolBarX;
            jToolBarFormat.setVisible(isShowing);
            //jcbmiViewToolbarFormat.setSelected(isShowing);
            return jToolBarFormat;
        } else if (whichToolBar == TOOLBAR_STYLES) {
            jToolBarStyles = jToolBarX;
            jToolBarStyles.setVisible(isShowing);
            //jcbmiViewToolbarStyles.setSelected(isShowing);
            return jToolBarStyles;
        } else {
            jToolBarMain = jToolBarX;
            jToolBarMain.setVisible(isShowing);
            //jcbmiViewToolbarMain.setSelected(isShowing);
            return jToolBarMain;
        }
    }

    /**
     * Convenience method for activating/deactivating formatting commands
     * depending on the active editing pane
     */
    private void setFormattersActive(boolean state) {
        actionFontBold.setEnabled(state);
        actionFontItalic.setEnabled(state);
        actionFontUnderline.setEnabled(state);
        //actionFontStrike.setEnabled(state);
        //actionFontSuperscript.setEnabled(state);
        //actionFontSubscript.setEnabled(state);
        actionListUnordered.setEnabled(state);
        actionListOrdered.setEnabled(state);
        //actionSelectFont.setEnabled(state);
        actionAlignLeft.setEnabled(state);
        actionAlignCenter.setEnabled(state);
        actionAlignRight.setEnabled(state);
        actionAlignJustified.setEnabled(state);
        //actionInsertAnchor.setEnabled(state);
        //jbtnUnicode.setEnabled(state);
        //jbtnUnicodeMath.setEnabled(state);
        //jcmbStyleSelector.setEnabled(state);
        //jcmbFontSelector.setEnabled(state);
        /*jMenuFont.setEnabled(state);
          jMenuFormat.setEnabled(state);
          jMenuInsert.setEnabled(state);
          jMenuTable.setEnabled(state);
          jMenuForms.setEnabled(state);*/
    }

    /**
     * Convenience method for obtaining the current file handle
     */
    public File getCurrentFile() {
        return currentFile;
    }

    /**
     * Convenience method for obtaining the application name
     */
    public String getAppName() {
        return appName;
    }

    /**
     * Convenience method for obtaining the document text
     */
    public String getDocumentText() {
        if (isSourceWindowActive()) {
            return jtpSource.getText();
        } else {
            return jtpMain.getText();
        }
    }

    /**
     * Convenience method for obtaining the document text
     * contained within a tag pair
     */
    public String getDocumentSubText(String tagBlock) {
        return getSubText(tagBlock);
    }

    /**
     * Method for extracting the text within a tag
     */
    private String getSubText(String containingTag) {
        jtpSource.setText(jtpMain.getText());
        String docTextCase = jtpSource.getText().toLowerCase();
        int tagStart       = docTextCase.indexOf("<" + containingTag.toLowerCase());
        int tagStartClose  = docTextCase.indexOf(">", tagStart) + 1;
        String closeTag    = "</" + containingTag.toLowerCase() + ">";
        int tagEndOpen     = docTextCase.indexOf(closeTag);
        if (tagStartClose < 0) {
            tagStartClose = 0;
        }
        if (tagEndOpen < 0 || tagEndOpen > docTextCase.length()) {
            tagEndOpen = docTextCase.length();
        }
        return jtpSource.getText().substring(tagStartClose, tagEndOpen);
    }

    /**
     * Convenience method for obtaining the document text
     * contained within the BODY tags (a common request)
     */
    public String getDocumentBody() {
        return getSubText("body");
    }

    /**
     * Convenience method for setting the document text
     */
    public void setDocumentText(String sText) {
        jtpMain.setText(sText);
        ((HTMLEditorKit)(jtpMain.getEditorKit())).setDefaultCursor(new Cursor(Cursor.TEXT_CURSOR));
        jtpSource.setText(jtpMain.getText());
    }

    /**
     * Convenience method for setting the source document
     */
    public void setSourceDocument(StyledDocument sDoc) {
        jtpSource.getDocument().removeDocumentListener(this);
        jtpSource.setDocument(sDoc);
        jtpSource.getDocument().addDocumentListener(this);
        jtpMain.setText(jtpSource.getText());
        ((HTMLEditorKit)(jtpMain.getEditorKit())).setDefaultCursor(new Cursor(Cursor.TEXT_CURSOR));
    }

    /**
     * Convenience method for obtaining the document text
     */
    private void updateTitle() {
        //frameHandler.setTitle(appName + (currentFile == null ? "" : " - " + currentFile.getName()));
    }

    /**
     * Convenience method for clearing out the UndoManager
     */
    public void purgeUndos() {
        if (undoMngr != null) {
            undoMngr.discardAllEdits();
            undoAction.updateUndoState();
            redoAction.updateRedoState();
        }
    }

    /**
     * Convenience method for refreshing and displaying changes
     */
    public void refreshOnUpdate() {
        jtpMain.setText(jtpMain.getText());
        jtpSource.setText(jtpMain.getText());
        purgeUndos();
        //this.repaint();
    }

    /**
     * Convenience method for deallocating the app resources
     */
    public void dispose() {
        //frameHandler.dispose();
        System.exit(0);
    }

    /**
     * Convenience method for fetching icon images from jar file
     */
    private ImageIcon getEkitIcon(String iconName) {
        URL imageURL = this.getClass().getResource("/org/objectweb/salome_tmf/ihm/images/" + iconName + ".png");
        if (imageURL != null) {
            return new ImageIcon(imageURL);
        }
        imageURL = this.getClass().getResource("/org/objectweb/salome_tmf/ihm/images/" + iconName + ".gif");
        if (imageURL != null) {
            return new ImageIcon(imageURL);
        }
        return null;
    }

    /**
     * Convenience method for outputting exceptions
     */
    private void logException(String internalMessage, Exception e) {
        Util.err(internalMessage, e);
    }

    /**
     * Convenience method for determining if the source window is active
     */
    private boolean isSourceWindowActive() {
        return (jspSource != null && jspSource == jspltDisplay.getRightComponent());
    }

    /**
     * Method for toggling source window visibility
     */
    private void toggleSourceWindow() {
        if (!(isSourceWindowActive())) {
            jtpSource.setText(jtpMain.getText());
            jspltDisplay.setRightComponent(jspSource);
            if (exclusiveEdit) {
                jspltDisplay.setDividerLocation(0);
                jspltDisplay.setEnabled(false);
                jtpSource.requestFocus();
            } else {
                jspltDisplay.setDividerLocation(iSplitPos);
                jspltDisplay.setEnabled(true);
            }
        } else {
            jtpMain.setText(jtpSource.getText());
            iSplitPos = jspltDisplay.getDividerLocation();
            jspltDisplay.remove(jspSource);
            jtpMain.requestFocus();
        }
        this.validate();
        //jcbmiViewSource.setSelected(isSourceWindowActive());
        jtbtnViewSource.setSelected(isSourceWindowActive());
    }

    /**
     * Searches the specified element for CLASS attribute setting
     */
    private String findStyle(Element element) {
        AttributeSet as = element.getAttributes();
        if (as == null) {
            return null;
        }
        Object val = as.getAttribute(HTML.Attribute.CLASS);
        if (val != null && (val instanceof String)) {
            return (String)val;
        }
        for (Enumeration e = as.getAttributeNames(); e.hasMoreElements();) {
            Object key = e.nextElement();
            if (key instanceof HTML.Tag) {
                AttributeSet eas = (AttributeSet)(as.getAttribute(key));
                if (eas != null) {
                    val = eas.getAttribute(HTML.Attribute.CLASS);
                    if (val != null) {
                        return (String)val;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Utility methods
     */
    public ExtendedHTMLDocument getExtendedHtmlDoc() {
        return htmlDoc;
    }

    public int getCaretPosition() {
        return jtpMain.getCaretPosition();
    }

    public void setCaretPosition(int newPositon) {
        boolean end = true;
        do {
            end = true;
            try {
                jtpMain.setCaretPosition(newPositon);
            } catch (IllegalArgumentException iae) {
                end = false;
                newPositon--;
            }
        } while (!end && newPositon >= 0);
    }

    /* Inner Classes --------------------------------------------- */

    /**
     * Class for implementing Undo as an autonomous action
     */
    class UndoAction extends AbstractAction {

        public UndoAction()     {
            super(Language.getInstance().getText("Undo"));
            setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                undoMngr.undo();
            } catch(CannotUndoException ex)     {
                Util.err(ex);
            }
            updateUndoState();
            redoAction.updateRedoState();
        }

        protected void updateUndoState() {
            setEnabled(undoMngr.canUndo());
        }

    }

    /**
     * Class for implementing Redo as an autonomous action
     */
    class RedoAction extends AbstractAction {

        public RedoAction()     {
            super(Language.getInstance().getText("Redo"));
            setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                undoMngr.redo();
            } catch (CannotUndoException ex) {
                Util.err(ex);
            }
            updateRedoState();
            undoAction.updateUndoState();
        }

        protected void updateRedoState() {
            setEnabled(undoMngr.canRedo());
        }

    }

    /**
     * Class for implementing the Undo listener to handle the Undo and Redo actions
     */
    class CustomUndoableEditListener implements UndoableEditListener {

        @Override
        public void undoableEditHappened(UndoableEditEvent uee) {
            undoMngr.addEdit(uee.getEdit());
            undoAction.updateUndoState();
            redoAction.updateRedoState();
        }

    }

    /******************* FOR SALOME *************/
    public void setEditable(boolean bool) {
        jtpMain.setEditable(bool);
    }

    public void setText(String text) {
        //text = convertHtmlToRtf(text);
        try     {
            StringReader input = new StringReader(text);
            jtpMain.setText("");
                
            htmlDoc = (ExtendedHTMLDocument)(htmlKit.createDefaultDocument());
            styleSheet = htmlDoc.getStyleSheet();
            htmlKit.setDefaultCursor(new Cursor(Cursor.TEXT_CURSOR));
            jtpMain.setCursor(new Cursor(Cursor.TEXT_CURSOR));
            jtpMain.setEditorKit(htmlKit);
            jtpMain.setDocument(htmlDoc);
                        
                        
            htmlKit.read(input, htmlDoc, 0);
            input.close();
                        
            registerDocument(htmlDoc);
                        
            jtpSource.setText(jtpMain.getText());
            currentFile = null;
            undoMngr.die();
            //updateTitle();
            refreshOnUpdate();
        } catch (Exception ex) {
            Util.err(ex);
            Util.debug("Impossible de charger le document : " + ex.toString());
            jtpMain.setText("");
        }
    }

    public void saveDocumentInDB() {
        try     {
            Util.debug( "TEXT = " +  jtpMain.getText());
            String toWritre = "";
            StringWriter output = new StringWriter();

            htmlKit.write(output, htmlDoc, 0, htmlDoc.getLength());
            output.flush();
            output.close();
            toWritre = output.toString();

            /*toWritre = toWritre.replaceAll("<head>","");
              toWritre = toWritre.replaceAll("</head>","");
              toWritre = toWritre.replaceAll("<body>","");
              toWritre = toWritre.replaceAll("</body>","");
              toWritre = toWritre.replaceAll("<html>","");
              toWritre = toWritre.replaceAll("</html>","");
              toWritre = toWritre.trim();
            */
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) pSimppleDataTree.getLastSelectedPathComponent();
            Object pObject = node.getUserObject();
            if (pObject != null) {
                if (pObject instanceof SimpleData) {
                    SimpleData pData = (SimpleData) pObject;
                    pData.updateInDBAndModel(pData.getNameFromModel(), toWritre);
                    //Util.debug( " Write = " + toWritre);
                }
            }
        } catch (Exception ex) {
            Util.err(ex);
            Util.debug("Impossible de sauvegarder le document : " + ex.toString());
            return;
        }
    }
}
