/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.filtre.CampagneTreeFiltrePanel;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.htmleditor.EkitCore;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class CampainPanel extends JPanel implements ActionListener {

    JTabbedPane campagneSpace;
    JTabbedPane campaignFamilyWorkSpace;
    JTabbedPane campaignTestListWorkSpace;
    JTabbedPane campaignTestWorkSpace;

    JPanel workSpaceCampagne;

    DynamicTree campaignDynamicTree;
    CampagneTreeFiltrePanel pCampTreeFiltrePanel;

    JButton addTestInCampagne;
    JButton createCampagne;
    // JButton orderTest;
    JButton orderCampagne;
    JButton delCampagne;
    JButton renameCampaignButton;

    JMenu campToolsMenu;

    JLabel projetNameCampagne;
    JLabel campaignNameLabel;
    JLabel campaignDateLabel;
    JLabel campaignConceptorLabel;

    EkitCore pCampagneDetailsDescription;
    EkitCore pCampaignFamilyDescription;
    EkitCore pCampaignTestListDescription;
    EkitCore pCampaignTestDescription;

    /*
     * JTextPane campagneDetailsDescription; JTextPane
     * campaignFamilyDescription; JTextPane campaignTestListDescription;
     * JTextPane campaignTestDescription;
     */
    AttachmentView campaignAttachmentView;
    AssignedCamPanel pAssignedCamPanel;

    // changeListenerPanel campagneMultiUserChangeListenerPanel;
    BaseIHM pBaseIHM;
    SalomeTMFContext pSalomeTMFContext;
    int t_x = 1024;
    int t_y = 768;
    JTabbedPane tabs;
    JLabel projetName;
    JLabel userName;
    private JLabel testConceptorLabel;
    private JLabel testNameLabel;
    private JLabelLink testNameLabelLink;
    private JLabel testDateLabel;
    private JLabel testAssignedLabel;
    
    Config pConfig;

    public CampainPanel(SalomeTMFContext m_SalomeTMFContext, BaseIHM m_BaseIHM,
                        JTabbedPane m_tabs, int x, int y) {
        super(new BorderLayout());
        pSalomeTMFContext = m_SalomeTMFContext;
        pBaseIHM = m_BaseIHM;
        tabs = m_tabs;
        t_x = x;
        t_y = y;
    }

    public void initCoponent() {
        workSpaceCampagne = new JPanel();

        campToolsMenu = new JMenu(Language.getInstance().getText("Outils"));
        pSalomeTMFContext.UIComponentsMap.put(UICompCst.CAMP_TOOLS_MENU,
                                              campToolsMenu);
        //              namePanelCampagne.add(projetNameCampagne);

        JLabel projectLabel = new JLabel(Language.getInstance().getText(
                                                                        "Projet__"));
        projetName = new JLabel();
        userName = new JLabel();
        projetName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        userName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        JPanel namePanelCampagne = new JPanel(new FlowLayout(FlowLayout.LEFT));
        projectLabel.setFont(new Font(null, Font.BOLD, 16));
        namePanelCampagne.setLayout(new GridBagLayout());
        JLabel userLabel = new JLabel(Language.getInstance().getText(
                                                                     "Utilisateurs")
                                      + " : ");
        userLabel.setFont(new Font(null, Font.BOLD, 16));
        userLabel.setLayout(new GridBagLayout());
        namePanelCampagne.add(projectLabel, new GridBagConstraints(0, 0, 1, 1,
                                                                   0.5, 0.1, GridBagConstraints.NORTHWEST,
                                                                   GridBagConstraints.NONE, new Insets(10, 10, 10, 5), 0, 0));
        namePanelCampagne.add(projetName, new GridBagConstraints(1, 0, 1, 1,
                                                                 0.5, 0.1, GridBagConstraints.NORTHWEST,
                                                                 GridBagConstraints.NONE, new Insets(10, 5, 10, 20), 0, 0));
        namePanelCampagne.add(userLabel, new GridBagConstraints(2, 0, 1, 1,
                                                                0.5, 0.1, GridBagConstraints.NORTHWEST,
                                                                GridBagConstraints.NONE, new Insets(10, 20, 10, 5), 0, 0));
        namePanelCampagne.add(userName, new GridBagConstraints(3, 0, 1, 1, 0.5,
                                                               0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                               new Insets(10, 5, 10, 5), 0, 0));

        /*
         * campagneMultiUserChangeListenerPanel = new changeListenerPanel();
         * campagneMultiUserChangeListenerPanel.setPreferredSize(new
         * Dimension(70,25)); JPanel campagneMultiUserPanel = new JPanel(new
         * FlowLayout(FlowLayout.RIGHT));
         * campagneMultiUserPanel.add(campagneMultiUserChangeListenerPanel);
         */
        JPanel campagneTopPanel = new JPanel(new BorderLayout());
        campagneTopPanel.add(namePanelCampagne, BorderLayout.WEST);
        // campagneTopPanel.add(campagneMultiUserPanel, BorderLayout.EAST);

        add(campagneTopPanel, BorderLayout.NORTH);
        createCampagnePlane();
    }

    /**
     * Methode qui cree la vue sur les campagnes
     */
    private void createCampagnePlane() {

        // La liste des boutons de la vue (c?t? gauche de la vue)
        createCampagne = new JButton(Language.getInstance().getText("Creer_une_campagne"));
        createCampagne.setToolTipText(Language.getInstance().getText("Creer_une_campagne"));
        createCampagne.addActionListener(this);

        addTestInCampagne = new JButton(Language.getInstance().getText("Importer"));
        addTestInCampagne.setEnabled(false);
        addTestInCampagne.setToolTipText(Language.getInstance().getText(
                "Ajouter_des_tests_a_la_campagne"));
        addTestInCampagne.addActionListener(this);

        orderCampagne = new JButton(Language.getInstance().getText("Ordonner"));
        orderCampagne.setToolTipText(Language.getInstance().getText("Ordonner_une_campagne"));
        orderCampagne.addActionListener(this);

        JMenu refreshItem = new JMenu(Language.getInstance().getText("Rafraichir"));
        /*
         * JMenuItem refreshItem = new
         * JMenuItem(Language.getInstance().getText("Rafraichir"));
         * refreshItem.addActionListener(new ActionListener() { public void
         * actionPerformed(ActionEvent e) { tabs.setCursor(new
         * Cursor(Cursor.WAIT_CURSOR)); workSpaceCampagne.removeAll();
         * DataModel.reloadFromBase(true); tabs.setCursor(new
         * Cursor(Cursor.DEFAULT_CURSOR)); } });
         */

        JMenuItem refreshItemAll = new JMenuItem(Language.getInstance()
                                                 .getText("Tout"));
        refreshItemAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    workSpaceCampagne.removeAll();
                    DataModel.reloadFromBase(true);
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        JMenuItem refreshItemCamp = new JMenuItem(Language.getInstance()
                                                  .getText("Campagnes"));
        refreshItemCamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    workSpaceCampagne.removeAll();
                    try {
                        DataModel.reloadCampainPlan();
                    } catch (Exception ex) {
                        Util.err(ex);
                        DataModel.reloadFromBase(true);
                    }
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        refreshItem.add(refreshItemAll);
        refreshItem.add(refreshItemCamp);

        campToolsMenu.add(refreshItem);

        // Sous menu pour changer le mot de passe
        if (Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            JMenuItem changePwdItem = new JMenuItem(Language.getInstance()
                                                    .getText("Changer_le_mot_de_passe"));
            changePwdItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new ChangePwdWindow(pSalomeTMFContext.ptrFrame).show();
                    }
                });

            campToolsMenu.add(changePwdItem);
        }

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(campToolsMenu);

        delCampagne = new JButton(Language.getInstance().getText("Supprimer"));
        delCampagne.setEnabled(false);
        delCampagne.setToolTipText(Language.getInstance().getText("Supprimer"));
        delCampagne.addActionListener(this);

        renameCampaignButton = new JButton(Language.getInstance().getText(
                                                                          "Renommer"));
        renameCampaignButton.setEnabled(false);
        renameCampaignButton.addActionListener(this);

        // Panel regroupant tous les boutons
        JPanel listPanel = new JPanel(new GridLayout(1, 3));
        listPanel.add(addTestInCampagne);
        listPanel.add(orderCampagne);
        listPanel.add(renameCampaignButton);
        listPanel.add(delCampagne);
        listPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.CAMP_SECOND_BUTTONS_PANEL, listPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_SECOND_BUTTONS_PANEL);

        JPanel createPanel = new JPanel(new GridLayout(1, 3));
        createPanel.add(createCampagne);
        createPanel.add(menuBar);
        createPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.CAMP_FIRST_BUTTONS_PANEL, createPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_FIRST_BUTTONS_PANEL);

        campaignDynamicTree = new DynamicTree(Language.getInstance().getText(
                                                                             "Campagnes_de_tests"), DataConstants.CAMPAIGN);
        pSalomeTMFContext.addToUIComponentsMap(UICompCst.CAMPAIGN_DYNAMIC_TREE,
                                               campaignDynamicTree);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMPAIGN_DYNAMIC_TREE);

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new GridBagLayout());
        pCampTreeFiltrePanel = new CampagneTreeFiltrePanel(campaignDynamicTree
                                                           .getModel());
        JPanel buttonPanel = new JPanel(new GridBagLayout());
        JButton quitCampagneButton = new JButton(Language.getInstance()
                                                 .getText("Quitter"));
        quitCampagneButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pBaseIHM.quit(true, true);
                }
            });
        buttonPanel.add(quitCampagneButton, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1,
                                                                   GridBagConstraints.SOUTH, GridBagConstraints.NONE,
                                                                   new Insets(5, 5, 5, 5), 0, 0));
        southPanel.add(pCampTreeFiltrePanel, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1,
                                                                    GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
                                                                    new Insets(0, 0, 0, 0), 0, 0));
        southPanel.add(buttonPanel, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1,
                                                           GridBagConstraints.SOUTH, GridBagConstraints.NONE,
                                                           new Insets(0, 0, 0, 0), 0, 0));

        createCampagneWorkSpace();
        createCampaignFamily();
        createCampaignTestList();
        createCampaignTest();
        workSpaceCampagne.removeAll();

        // Construction finale
        JPanel allButtons = new JPanel();
        allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));

        allButtons.add(createPanel);
        allButtons.add(listPanel);

        JPanel buttonsAndTree = new JPanel(new BorderLayout());
        buttonsAndTree.add(allButtons, BorderLayout.NORTH);
        buttonsAndTree.add(campaignDynamicTree, BorderLayout.CENTER);
        // buttonsAndTree.add(quitCampagneButton, BorderLayout.SOUTH);
        buttonsAndTree.add(southPanel, BorderLayout.SOUTH);

        buttonsAndTree.setPreferredSize(new Dimension(t_x / 3, t_y));
        buttonsAndTree.setMinimumSize(new Dimension(t_x / 4, t_y));

        workSpaceCampagne.setPreferredSize(new Dimension(t_x / 3 * 2, t_y));
        workSpaceCampagne.setMinimumSize(new Dimension(t_x / 3, t_y));

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                                              buttonsAndTree, workSpaceCampagne);
        splitPane.setDividerLocation(380);
        add(splitPane);
    } // fin de la methode createCampagnePlane/0

    /**
     * Methode qui cree la vue de droite pour les campagnes
     */
    private void createCampagneWorkSpace() {
        workSpaceCampagne.removeAll();
        // Onglets
        campagneSpace = new JTabbedPane();

        JPanel details = new JPanel(new BorderLayout());
        createCampagneDetails(details);

        DataSetView data = new DataSetView();
        ExecutionView executionPanel = new ExecutionView();
        AnomalieView anomaliePanel = new AnomalieView(null);
        pAssignedCamPanel = new AssignedCamPanel();
        campaignAttachmentView = new AttachmentView(pBaseIHM,
                                                    DataConstants.CAMPAIGN, DataConstants.CAMPAIGN, null,
                                                    DataConstants.NORMAL_SIZE_FOR_ATTACH, null);

        campagneSpace
            .addTab(Language.getInstance().getText("Details"), details); // 0
        // campagneSpace.addTab(Language.getInstance().getText("Attachements"),
        // attachment); //1
        campagneSpace.addTab(Language.getInstance().getText("Attachements"),
                             campaignAttachmentView);
        campagneSpace.addTab(Language.getInstance().getText("Executions"),
                             executionPanel); // 2
        campagneSpace.addTab(Language.getInstance().getText("Jeux_de_donnees"),
                             data); // 3
        campagneSpace.addTab(Language.getInstance().getText("Anomalies"),
                             anomaliePanel); // 4
        campagneSpace.addTab(Language.getInstance().getText("Assigne_a"),
                             pAssignedCamPanel); // 5

        campagneSpace.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (tabs.getModel().getSelectedIndex() == 1) {
                        DataModel.initAttachmentTable(DataModel
                                                      .getCurrentCampaign().getAttachmentMapFromModel()
                                                      .values());
                    } else if (tabs.getModel().getSelectedIndex() == 4) {
                        AnomalieView pAnomaliePanel = (AnomalieView) tabs
                            .getSelectedComponent();
                        pAnomaliePanel.updateData(DataModel.getCurrentCampaign());
                    }
                }
            });

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.UIComponentsMap.put(
                                              UICompCst.CAMPAIGN_WORKSPACE_PANEL_FOR_TABS, campagneSpace);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.CAMPAIGN_WORKSPACE_PANEL_FOR_TABS);

        workSpaceCampagne.setLayout(new BorderLayout());
        workSpaceCampagne.add(campagneSpace, BorderLayout.CENTER);
    } // Fin de la m?thode createCampagneWorkSpace/0

    /**
     * Methode qui cree la vue de details sur les campagnes
     *
     * @param panel
     *            le panel qui contient la vue
     */
    private void createCampagneDetails(JPanel panel) {
        campaignNameLabel = new JLabel(Language.getInstance().getText("Nom_de_la_campagne")
                + " : ");
        campaignDateLabel = new JLabel(Language.getInstance().getText("Date_de_creation")
                + " : ");
        campaignConceptorLabel = new JLabel(Language.getInstance().getText("Concepteur")
                + " : ");

        JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLine.add(campaignNameLabel);

        firstLine.add(Box.createRigidArea(new Dimension(50, 20)));
        firstLine.add(campaignDateLabel);

        JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLine.add(campaignConceptorLabel);

        JPanel allButtons = new JPanel();
        allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
        allButtons.add(firstLine);
        allButtons.add(secondLine);

        // allButtons.add()
        allButtons.setBorder(BorderFactory.createEmptyBorder(10, 0, 30, 0));
        /*
         * campagneDetailsDescription = new JTextPane();
         * campagneDetailsDescription
         * .setBorder(BorderFactory.createTitledBorder(
         * BorderFactory.createLineBorder
         * (Color.BLACK),Language.getInstance().getText("Description")));
         * campagneDetailsDescription.addCaretListener(new
         * CampaignTreeDescriptionListener());
         * campagneDetailsDescription.addFocusListener(new
         * CampaignDescriptionFocusListener());
         */
        pCampagneDetailsDescription = new EkitCore(campaignDynamicTree
                                                   .getTree(), true);
        panel.add(allButtons, BorderLayout.NORTH);
        // panel.add(campagneDetailsDescription,BorderLayout.CENTER);
        panel.add(pCampagneDetailsDescription, BorderLayout.CENTER);

    } // Fin de la methode createCampagneDetails/1

    /**
     * Methode qui cree la vue sur les familles
     */
    private void createCampaignFamily() {

        workSpaceCampagne.removeAll();
        /*
         * campaignFamilyDescription = new JTextPane();
         * campaignFamilyDescription.addCaretListener(new
         * CampaignTreeDescriptionListener());
         * campaignFamilyDescription.setEditable(false);
         */
        pCampaignFamilyDescription = new EkitCore(
                                                  campaignDynamicTree.getTree(), false);

        // Onglets
        campaignFamilyWorkSpace = new JTabbedPane();
        // campaignFamilyWorkSpace.addTab(Language.getInstance().getText("Description"),
        // campaignFamilyDescription);
        campaignFamilyWorkSpace.addTab(Language.getInstance().getText(
                                                                      "Description"), pCampaignFamilyDescription);

        workSpaceCampagne.setLayout(new BorderLayout());
        workSpaceCampagne.add(campaignFamilyWorkSpace, BorderLayout.CENTER);

        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.FAMILY_CAMP_WORKSPACE_TABS_PANEL,
                                               campaignFamilyWorkSpace);
        UICompCst.staticUIComps.add(UICompCst.FAMILY_CAMP_WORKSPACE_TABS_PANEL);
    }

    /**
     * Methode qui cee la vue sur les suites des campagnes
     */
    private void createCampaignTestList() {

        workSpaceCampagne.removeAll();
        campaignTestListWorkSpace = new JTabbedPane();

        // Onglets
        /*
         * campaignTestListDescription = new JTextPane();
         * campaignTestListDescription.addCaretListener(new
         * CampaignTreeDescriptionListener());
         * campaignTestListDescription.setEditable(false);
         */
        pCampaignTestListDescription = new EkitCore(campaignDynamicTree
                                                    .getTree(), false);
        // campaignTestListWorkSpace.addTab(Language.getInstance().getText("Description"),
        // campaignTestListDescription);
        campaignTestListWorkSpace.addTab(Language.getInstance().getText(
                                                                        "Description"), pCampaignTestListDescription);

        workSpaceCampagne.setLayout(new BorderLayout());
        workSpaceCampagne.add(campaignTestListWorkSpace, BorderLayout.CENTER);

        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.TESTLIST_CAMP_WORKSPACE_TABS_PANEL,
                                               campaignTestListWorkSpace);
        UICompCst.staticUIComps
            .add(UICompCst.TESTLIST_CAMP_WORKSPACE_TABS_PANEL);
    } // Fin de la methode createCampaignTestList/0

    /**
     * Methode qui cree la vue sur les tests des campagne
     */
    private void createCampaignTest() {

        workSpaceCampagne.removeAll();

        // Onglets
        campaignTestWorkSpace = new JTabbedPane();

        testConceptorLabel = new JLabel(Language.getInstance().getText("Concepteur")
                + " : ");
        testNameLabel = new JLabel(Language.getInstance().getText("Nom_du_test")
                + " : ");
        if (pConfig.getPolarionLinks()) {
            // Polarion links are shown
            testNameLabelLink = new JLabelLink(Language.getInstance().getText("Link")
                    + " : ");
        }
        testDateLabel = new JLabel(Language.getInstance().getText("Date_de_creation")
                + " : ");
        testAssignedLabel = new JLabel(Language.getInstance().getText("Assigne_a")
                + " : ");

        JPanel allButtons = new JPanel();
        if (pConfig.getPolarionLinks()) {
            // Polarion links are shown
            JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            firstLine.add(testNameLabel);
            JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            secondLine.add(testNameLabelLink);
            JPanel thirdLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            thirdLine.add(testDateLabel);
            thirdLine.add(Box.createRigidArea(new Dimension(20, 20)));
            thirdLine.add(testConceptorLabel);
            thirdLine.add(Box.createRigidArea(new Dimension(20, 20)));
            thirdLine.add(testAssignedLabel);

            allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
            allButtons.add(firstLine);
            allButtons.add(secondLine);
            allButtons.add(thirdLine);
            // allButtons.add()
            allButtons.setBorder(BorderFactory.createEmptyBorder(10, 0, 30, 0));
        } else {
            // Polarion links are not shown
            JPanel firstLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            firstLine.add(testNameLabel);
            // JPanel secondLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            // secondLine.add(testNameLabelLink);
            JPanel thirdLine = new JPanel(new FlowLayout(FlowLayout.LEFT));
            thirdLine.add(testDateLabel);
            thirdLine.add(Box.createRigidArea(new Dimension(20, 20)));
            thirdLine.add(testConceptorLabel);
            thirdLine.add(Box.createRigidArea(new Dimension(20, 20)));
            thirdLine.add(testAssignedLabel);

            allButtons.setLayout(new BoxLayout(allButtons, BoxLayout.Y_AXIS));
            allButtons.add(firstLine);
            // allButtons.add(secondLine);
            allButtons.add(thirdLine);
            // allButtons.add()
            allButtons.setBorder(BorderFactory.createEmptyBorder(10, 0, 30, 0));
        }

        /*
         * campaignTestDescription = new JTextPane();
         * campaignTestDescription.setBorder
         * (BorderFactory.createTitledBorder(BorderFactory
         * .createLineBorder(Color
         * .BLACK),Language.getInstance().getText("Description")));
         * campaignTestDescription.addCaretListener(new
         * CampaignTreeDescriptionListener());
         * campaignTestDescription.setEditable(false);
         */
        pCampaignTestDescription = new EkitCore(campaignDynamicTree.getTree(),
                                                false);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(allButtons, BorderLayout.NORTH);
        // panel.add(campaignTestDescription,BorderLayout.CENTER);
        panel.add(pCampaignTestDescription, BorderLayout.CENTER);
        campaignTestWorkSpace.addTab(Language.getInstance().getText("Details"),
                                     panel);

        workSpaceCampagne.setLayout(new BorderLayout());
        workSpaceCampagne.add(campaignTestWorkSpace, BorderLayout.CENTER);

        pSalomeTMFContext
            .addToUIComponentsMap(UICompCst.TEST_CAMP_WORKSPACE_TABS_PANEL,
                                  campaignTestWorkSpace);
        UICompCst.staticUIComps.add(UICompCst.TEST_CAMP_WORKSPACE_TABS_PANEL);
    } // Fin de la methode createCampaignTest/0

    /********************************************************************************************************/

    void loadModel(String strProject, String strLogin) {
        // campagneMultiUserChangeListenerPanel.reset();
        campaignDynamicTree.givePopupMenuToTree();
        campaignAttachmentView.giveAccessToIhmScriptView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());

    }

    void reloadModel() {
        // campagneMultiUserChangeListenerPanel.reset();
        campaignDynamicTree.givePopupMenuToTree();
        campaignAttachmentView.giveAccessToIhmScriptView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());

    }

    void giveAccessToIhm() {
        if (Api.isConnected()) {
            if (!Permission.canUpdateTest()) {
                // orderTest.setEnabled(false);
                pCampaignFamilyDescription.setEditable(false);
                pCampaignTestListDescription.setEditable(false);
                pCampaignTestDescription.setEditable(false);

            }
            if (!Permission.canDeleteCamp()) {
                delCampagne.setEnabled(false);
            }
            if (!Permission.canCreateCamp()) {
                createCampagne.setEnabled(false);
                addTestInCampagne.setEnabled(false);
            }
            if (!Permission.canUpdateCamp()) {
                orderCampagne.setEnabled(false);
                renameCampaignButton.setEnabled(false);
                campaignDynamicTree.setEnabled(false);
                pCampagneDetailsDescription.setEditable(false);
            }
            if (!Permission.canExecutCamp()) {

            }
        }
    }

    /********************************************************************************************************************************/
    void reValidate() {
        validate();
        repaint();
    }

    void setWorkSpace(int type) {
        workSpaceCampagne.removeAll();
        if (type == DataConstants.FAMILY) {
            workSpaceCampagne.add(campaignFamilyWorkSpace);
        } else if (type == DataConstants.TESTLIST) {
            workSpaceCampagne.add(campaignTestListWorkSpace);
        } else if (type == DataConstants.TEST) {
            workSpaceCampagne.add(campaignTestWorkSpace);
        } else if (type == DataConstants.CAMPAIGN) {
            workSpaceCampagne.add(campagneSpace);
        }
    }

    void setDescription(int type, String text) {
        if (type == DataConstants.CAMPAIGN) {
            pCampagneDetailsDescription.setText(text);
        } else if (type == DataConstants.FAMILY) {
            pCampaignFamilyDescription.setText(text);
        } else if (type == DataConstants.TESTLIST) {
            pCampaignTestListDescription.setText(text);
        } else if (type == DataConstants.TEST) {
            pCampaignTestDescription.setText(text);
        }
    }

    void setTestInfo(String name, String conceptor, String date) {
        if (name != null) {
            campaignNameLabel.setText(Language.getInstance().getText(
                                                                     "Nom_de_la_campagne__")
                                      + name);
        }
        if (conceptor != null) {
            campaignConceptorLabel.setText(Language.getInstance().getText(
                                                                          "Concepteur__")
                                           + conceptor);
        }
        if (date != null) {
            campaignDateLabel.setText(Language.getInstance().getText(
                                                                     "Date_de_creation__")
                                      + date);
        }
    }

    void setTestInfoForTestPanel(String name, String conceptor, String date,
                                 int assigned_to_ID) {
        if (name != null) {
            testNameLabel.setText(Language.getInstance().getText("Nom_du_test")
                                  + " : " + name);
            if (pConfig.getPolarionLinks()) {
                // Polarion links are shown
                testNameLabelLink.generatePolarionLink(name);
            }
        }
        if (conceptor != null) {
            testConceptorLabel.setText(Language.getInstance().getText(
                                                                      "Concepteur")
                                       + " : " + conceptor);
        }
        if (date != null) {
            testDateLabel.setText(Language.getInstance().getText(
                                                                 "Date_de_creation")
                                  + " : " + date);
        }
        if (assigned_to_ID > 0) {
            String user_login = DataModel.getCurrentUser().getLoginFromModel();
            Project current_project = DataModel.getCurrentProject();
            try {
                boolean found = false;
                Vector users_wrapper = current_project.getAllUsersWrapper();
                int i = 0;
                while (i < users_wrapper.size() && !found) {
                    User user = new User((UserWrapper) users_wrapper
                                         .elementAt(i));
                    if (user.getIdBdd() == assigned_to_ID) {
                        user_login = user.getLoginFromModel();
                        found = true;
                    }
                    i++;
                }
            } catch (Exception e) {
                Util.err(e);
            }
            testAssignedLabel.setText(Language.getInstance().getText(
                                                                     "Assigne_a")
                                      + " : " + user_login);
        }

    }

    /********** Action Listener **************/

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addTestInCampagne)) {
            addTestInCampagnePerformed(e);
        } else if (e.getSource().equals(createCampagne)) {
            createCampagnePerformed(e);
        } else if (e.getSource().equals(orderCampagne)) {
            orderCampagnePerformed(e);
        } else if (e.getSource().equals(delCampagne)) {
            delCampagnePerformed(e);
        } else if (e.getSource().equals(renameCampaignButton)) {
            renameCampaignPerformed(e);
        }
    }

    void addTestInCampagnePerformed(ActionEvent e) {
        DataModel.importTestsToCampaign();
    }

    void createCampagnePerformed(ActionEvent e) {
        // if (pCampTreeFiltrePanel.isActived()){
        pCampTreeFiltrePanel.reInit(true);
        // }
        DataModel.addNewCampagne();
    }

    void orderCampagnePerformed(ActionEvent e) {
        new TestOrdering(campaignDynamicTree, true);
    }

    void delCampagnePerformed(ActionEvent e) {
        DataModel.deleteInCampaignTree();
    }

    void renameCampaignPerformed(ActionEvent e) {
        /*
         * if (pCampTreeFiltrePanel.isActived()){
         * pCampTreeFiltrePanel.reInit(true); }
         */
        DataModel.renameCampaign();
    }
}
