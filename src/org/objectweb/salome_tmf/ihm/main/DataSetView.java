/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;


public class DataSetView extends JPanel implements ActionListener{
    
    /**
     *
     */
    static JButton addButton;
    
    /**
     *
     */
    static JButton deleteButton;
    
    /**
     *
     */
    static JButton modifyButton;
    
    /**
     *
     */
    static JButton purgeButton;
    
    /**
     *
     */
    MyTableModel dataSetTableModel;
    TableSorter  sorter;
    
    /**
     *
     */
    JTable dataSetTable;
    
    /**
     * Mod?le de s?lection pour la table des environnements
     */
    ListSelectionModel rowSM;
    
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    
    /**
     * Constructeur de la vue
     *
     */
    public DataSetView() {
        addButton = new JButton(Language.getInstance().getText("Ajouter"));
        deleteButton = new JButton(Language.getInstance().getText("Supprimer"));
        modifyButton = new JButton(Language.getInstance().getText("Modifier"));
        dataSetTableModel = new MyTableModel();
        dataSetTable = new JTable();
        DataModel.setDataSetTableModel(dataSetTableModel);
        
        addButton.setToolTipText(Language.getInstance().getText("Ajouter_un_jeu_de_donnees"));
        addButton.addActionListener(this);
        
        
        modifyButton.setToolTipText(Language.getInstance().getText("Modifier_le_jeu_de_donnees"));
        modifyButton.setEnabled(false);
        modifyButton.addActionListener(this);
        
        
        deleteButton.setToolTipText(Language.getInstance().getText("Supprimer_un_jeu_de_donnees"));
        deleteButton.setEnabled(false);
        deleteButton.addActionListener(this);
        
        purgeButton = new JButton(Language.getInstance().getText("Purge"));
        purgeButton.setToolTipText(Language.getInstance().getText("Supprimer_les_jeux_de_donnees_non_utilises"));
        purgeButton.addActionListener(this);
        
        //JPanel allButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel allButtons = new JPanel(new GridLayout(1,4));
        allButtons.add(addButton);
        allButtons.add(modifyButton);
        allButtons.add(deleteButton);
        allButtons.add(purgeButton);
        allButtons.setBorder(BorderFactory.createRaisedBevelBorder());
        
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_DATASET_BUTTONS_PANEL,allButtons);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_DATASET_BUTTONS_PANEL);
        
        //  La liste des attachements
        
        dataSetTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        dataSetTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        
        sorter = new TableSorter(dataSetTableModel);
        dataSetTable.setModel(sorter);
        sorter.setTableHeader(dataSetTable.getTableHeader());
        
        
        
        dataSetTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        dataSetTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.CAMP_DATASETS_TABLE,dataSetTable);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.CAMP_DATASETS_TABLE);
        
        JScrollPane tablePane = new JScrollPane(dataSetTable);
        
        
        
        rowSM = dataSetTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting())
                        return;
                
                    int selectedRow = dataSetTable.getSelectedRow();
                    if (selectedRow != -1) {
                        if (Permission.canUpdateCamp() || Permission.canExecutCamp()) modifyButton.setEnabled(true);
                        if (Permission.canExecutCamp() || Permission.canExecutCamp()) deleteButton.setEnabled(true);
                    } else {
                        deleteButton.setEnabled(false);
                        modifyButton.setEnabled(false);
                    }
                
                }
            });
        
        
        this.setLayout(new BorderLayout());
        this.add(allButtons, BorderLayout.NORTH);
        this.add(tablePane, BorderLayout.CENTER);
    } // Fin du constructeur DataSetView/0
    
    /**
     * Permet d'activer ou non les boutons en fonction des permissions accord?es
     * ? l'utilisateur
     */
    public static void giveAccessToIhmDataSetView() {
        if (!Permission.canDeleteCamp() && !Permission.canExecutCamp()) {
        }
        if (!Permission.canCreateCamp() && !Permission.canExecutCamp()) {
            addButton.setEnabled(false);
        }
        if (!Permission.canUpdateCamp() && !Permission.canExecutCamp()) {
            modifyButton.setEnabled(false);
            purgeButton.setEnabled(false);
        }
        if (!Permission.canExecutCamp()) {
            deleteButton.setEnabled(false);
            purgeButton.setEnabled(false);
        }
    } // Fin de la m?thode giveAccessToIhmDataSetView
    
    
    //Action Event
    @Override
    public void actionPerformed(ActionEvent evt){
        if (evt.getSource().equals(addButton)) {
            addPerformed();
        } else if (evt.getSource().equals(modifyButton)){
            modifyPerformed();
        } else if (evt.getSource().equals(deleteButton)){
            deletePerformed();
        } else if (evt.getSource().equals(purgeButton)){
            purgePerformed();
        } 
    }
    
    void addPerformed(){
        AskNewDataSet askNewDataSet = new AskNewDataSet();
        if (askNewDataSet.getDataSet() != null) {
            int transNumber = -1;
            try {
                DataSet dataSet = askNewDataSet.getDataSet();
                // BdD
                transNumber = Api.beginTransaction(11, ApiConstants.INSERT_DATA_SET);
                //dataSet.add2DB(DataModel.getCurrentCampaign());
                DataModel.getCurrentCampaign().addDataSetInDBAndModel(dataSet);
                //dataSet.addToBddAndModel(DataModel.getCurrentCampaign());
                Set keysSet = dataSet.getParametersHashMapFromModel().keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    String name = (String)iter.next();
                    String value = dataSet.getParameterValueFromModel(name);
                    if (value == null) value = "";
                    //dataSet.addParamValue2DB(value,ProjectData.getCurrentProject().getParameter(name));
                    dataSet.addParamValueToDBAndModel(value,DataModel.getCurrentProject().getParameterFromModel(name));
                }
                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                ArrayList data = new ArrayList();
                data.add(dataSet.getNameFromModel());
                data.add(dataSet.getDescriptionFromModel());
                dataSetTableModel.addRow(data);
                //DataModel.getCurrentCampaign().addDataSet(dataSet);
                
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
        }
    }
    
    void modifyPerformed(){
        int selectedRow = dataSetTable.getSelectedRow();
        if( selectedRow != -1) {
            //DataSet dataSet = DataModel.getCurrentCampaign().getDataSet((String)dataSetTableModel.getValueAt(selectedRow,0));
            DataSet dataSet = DataModel.getCurrentCampaign().getDataSetFromModel((String)sorter.getValueAt(selectedRow,0));
            String oldDataSetName = dataSet.getNameFromModel();
            String oldDataSetDescription = dataSet.getDescriptionFromModel();
            HashMap oldParmaHashMap = (HashMap)dataSet.getParametersHashMapFromModel().clone();
            AskNewDataSet askNewDataSet = new AskNewDataSet(dataSet);
            if (askNewDataSet.getDataSet() != null) {
                int transNumber = -1;
                try {
                    DataSet dataset = askNewDataSet.getDataSet();
                    
                    // BdD
                    transNumber = Api.beginTransaction(11, ApiConstants.UPDATE_DATA_SET);
                    String newName = dataset.getNameFromModel();
                    dataset.updateInModel(oldDataSetName, dataset.getDescriptionFromModel());
                    dataset.updateInDBAndModel(newName, dataset.getDescriptionFromModel());
                    
                    Set keysSet = oldParmaHashMap.keySet();
                    //Set keysSet = askNewDataSet.getDataSet().getParametersHashMapFromModel().keySet();
                    for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                        String name = (String)iter.next();
                        String value = askNewDataSet.getDataSet().getParameterValueFromModel(name);
                        if (value == null) value = "";
                        //dataset.updateParamValueInDB(ProjectData.getCurrentProject().getParameter(name), value, "");
                        dataset.updateParamValueInDBAndModel(DataModel.getCurrentProject().getParameterFromModel(name), value, "");
                    }
                    
                    Api.commitTrans(transNumber);
                    transNumber = -1;
                    // IHM
                    sorter.setValueAt(askNewDataSet.getDataSet().getNameFromModel(), selectedRow, 0);
                    sorter.setValueAt(askNewDataSet.getDataSet().getDescriptionFromModel(), selectedRow, 1);
                    
                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    Tools.ihmExceptionView(exception);
                    dataSet.updateInModel(oldDataSetName, oldDataSetDescription);
                    dataSet.setParametersHashMapInModel(oldParmaHashMap);
                }
            }
        }
    }
    
    void deletePerformed(){
        int selectedRow = dataSetTable.getSelectedRow();
        if (selectedRow != -1) {
            String message = "";
            String text = "";
            ArrayList concernedExecutions = DataModel.getCurrentProject().getExecutionsOfDataSetInCamp(DataModel.getCurrentCampaign(),(String)sorter.getValueAt(selectedRow,0));
            if (concernedExecutions.size() > 0) {
                for (int i = 0; i < concernedExecutions.size(); i ++) {
                    message = message + "* " + ((Execution)concernedExecutions.get(i)).getCampagneFromModel().getNameFromModel() + "/" +((Execution)concernedExecutions.get(i)).getNameFromModel() + "\n";
                }
                
                //text = "Le jeu de donn?es <" + (String)dataSetTableModel.getValueAt(selectedRow,0) + "> est utilis? pour les ex?cutions :\n" + message;
                text = Language.getInstance().getText("Le_jeu_de_donnees_") + (String)sorter.getValueAt(selectedRow,0) + Language.getInstance().getText("_est_utilise_pour_les_executions_") + message;
                text = text + Language.getInstance().getText("Sa_suppression_entrainera_la_suppression_de_toutes_ces_executions");
            }
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            int choice = -1;
            //int actionCase = -1;
            choice = JOptionPane.showOptionDialog(DataSetView.this,
                                                  //text + "Etes vous s?r de vouloir supprimer le jeu de donn?es <" + (String)dataSetTableModel.getValueAt(selectedRow,0) + "> ?",
                                                  text + Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_le_jeu_de_donnees_") + (String)sorter.getValueAt(selectedRow,0) + "> ?",
                                                  Language.getInstance().getText("Attention_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
            if (choice == JOptionPane.YES_OPTION) {
                
                
                try {
                    // BdD
                    DataModel.getCurrentCampaign().deleteDataSetInDBAndModel((DataModel.getCurrentCampaign().getDataSetFromModel((String)dataSetTableModel.getValueAt(selectedRow,0))));
                    
                    // IHM
                    //DataModel.getCurrentCampaign().removeDataSetInModel((String)sorter.getValueAt(selectedRow,0));
                    dataSetTableModel.removeData(sorter.modelIndex(selectedRow));
                    for (int i = 0; i < concernedExecutions.size(); i++) {
                        Campaign camp = ((Execution)concernedExecutions.get(i)).getCampagneFromModel();
                        //camp.deleteExecutionInModel((Execution)concernedExecutions.get(i));
                        if (DataModel.getCurrentCampaign().equals(camp)) {
                            DataModel.getExecutionTableModel().removeRow(((Execution)concernedExecutions.get(i)).getNameFromModel());
                            DataModel.getExecutionResultTableModel().clearTable();
                        }
                    }
                    
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
            }
        }
    }
    
    void purgePerformed(){
        for (int i = DataModel.getCurrentCampaign().getDataSetListFromModel().size() - 1; i >= 0; i--) {
            DataSet dataSet = (DataSet)DataModel.getCurrentCampaign().getDataSetListFromModel().get(i);
            boolean toBeDeleted = true;
            for (int j = 0; j < DataModel.getCurrentCampaign().getExecutionListFromModel().size(); j++) {
                Execution exec = (Execution)DataModel.getCurrentCampaign().getExecutionListFromModel().get(j);
                if (exec.getDataSetFromModel().equals(dataSet)) toBeDeleted = false;
            }
            if (toBeDeleted) {
                try {
                    // BdD
                    DataModel.getCurrentCampaign().deleteDataSetInDBAndModel(dataSet);
                     
                    // IHM
                    //DataModel.getCurrentCampaign().getDataSetListFromModel().remove(i);
                    dataSetTableModel.removeRow(dataSet.getNameFromModel());
                     
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
            }
             
        }
    }
} // Fin de la classe DataSetView
