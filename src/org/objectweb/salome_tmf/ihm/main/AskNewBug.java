package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

public class AskNewBug extends JDialog implements IBugJDialog{
                
                
    /**
     * Constructeur de la fenetre pour l'ajout d'un bug
     */
    public AskNewBug(Frame pFrame,BugTracker bugTrack, boolean _autofill, String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes) {
        // Init des composants de la fenetre
        super(pFrame, true);    
        JPanel mainPanel = bugTrack.getBugViewPanel(this, actionName, actionDesc, actionAwatedRes, actionEffectiveRes);
        if (mainPanel == null){
            mainPanel = new AskNewBugDefaultPanel(this, bugTrack, _autofill, actionName, actionDesc, actionAwatedRes, actionEffectiveRes);
        }
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(mainPanel, BorderLayout.CENTER);
        this.setTitle(Language.getInstance().getText("Ajouter_un_bug_dans")+ " " + bugTrack.getBugTrackerName());
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);*/
        centerScreen();
    } // Fin du constructeur
                
    /**
     * Constructeur de la fenetre pour l'ajout d'un bug
     */
    public AskNewBug(Dialog pDialog,BugTracker bugTrack, boolean _autofill, String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes) {
        // Init des composants de la fenetre
        super(pDialog, true);   
        JPanel mainPanel = bugTrack.getBugViewPanel(this, actionName, actionDesc, actionAwatedRes, actionEffectiveRes);
        if (mainPanel == null){
            mainPanel = new AskNewBugDefaultPanel(this, bugTrack, _autofill, actionName, actionDesc, actionAwatedRes, actionEffectiveRes);
        }
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(mainPanel, BorderLayout.CENTER);
        this.setTitle(Language.getInstance().getText("Ajouter_un_bug_dans")+ " " + bugTrack.getBugTrackerName());
                        
        /*this.pack();
          this.setLocation(300,120);
          this.setVisible(true);**/
        centerScreen();
    } // Fin du constructeur
                
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
                
    /**
     * Constructeur de la fenetre pour montrer un bug 
     */
    /*public AskNewBug(BugTracker bugTrack, boolean editable, Attachment _theBug, String environement, String user, String plateforme, String os, 
      String priority, String severity, String status,String reproducibility,String resolution, String recipient, String url, String resume, String description) {
                                
                        
      // Init des composants de la fenetre
      super(SalomeTMFContext.getInstance().getSalomeFrame(), true);     
      JPanel mainPanel = new AskNewBugDefaultPanel(this, bugTrack, editable, _theBug, environement, user, plateforme, os, 
      priority, severity, status, reproducibility, resolution, recipient, url, resume, description);
                        
      Container contentPaneFrame = this.getContentPane();
      contentPaneFrame.add(mainPanel, BorderLayout.CENTER);
                        
      this.setLocation(300,120);
      this.setTitle(Language.getInstance().getText("Ajouter_un_bug_dans")+ " " + bugTrack.getBugTrackerName());
      this.pack();
      this.setVisible(true);

      } // Fin du constructeur
    */
                
                
    @Override
    public void onViewPerformed(){
        AskNewBug.this.dispose();
                        
    }
    @Override
    public void onModifyPerformed(){
        AskNewBug.this.dispose();
    }
                
    @Override
    public void onCancelPerformed(){
        AskNewBug.this.dispose();
    }
                
    @Override
    public void onCommitPerformed(Attachment bugURL){
        if (!bugURL.getNameFromModel().equals("")) {
            boolean isBugAdded = false;
            try { 
                ExecutionResult execResult = DataModel.getObservedExecutionResult();
                ExecutionTestResult executionTestResult = DataModel.getCurrentExecutionTestResult();
                if (executionTestResult != null) {
                    executionTestResult.addAttachementInModel(bugURL);
                } else {
                    execResult.addAttachementInModel(bugURL);
                }
                isBugAdded = true;
            } catch (Exception E) {
                Tools.ihmExceptionView(E);
                Util.err(E);
            }
            if (isBugAdded) {
                ArrayList data = new ArrayList();
                data.add(bugURL.getNameFromModel());
                if (bugURL instanceof FileAttachment){
                    data.add(((FileAttachment)bugURL).getSize().toString());
                    data.add(((FileAttachment)bugURL).getDate().toString());
                }else {
                    data.add("");
                    data.add("");
                }
                DataModel.getAttachmentTableModel().addRow(data);
            } else {
                Tools.ihmExceptionView(new Exception(Language.getInstance().getText("URL_already_exists")));
            }
            this.dispose();
        }
    }
}
