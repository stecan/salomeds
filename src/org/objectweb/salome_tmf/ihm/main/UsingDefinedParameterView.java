/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;



public class UsingDefinedParameterView extends JDialog {
    
    JTable parameterTable;
    
    ArrayList parameterList;
    
    Parameter parameter;
    
    String value;
    
    JButton okButton;
    
    JTextField valueTextField;
    
    boolean mustGiveValue;
    
    /**
     *
     * @param withValue
     * @param singleSelection
     */
    public UsingDefinedParameterView(boolean withValue, boolean singleSelection) {
        
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        parameterTable = new JTable();
        mustGiveValue = withValue;
        
        TableSorter  sorter = new TableSorter(DataModel.getParameterTableModel());
        parameterTable.setModel(sorter);
        sorter.setTableHeader(parameterTable.getTableHeader());
        
        
        
        parameterTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        if (singleSelection) {
            parameterTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        } else {
            parameterTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        }
        
        
        JScrollPane parametersScrollPane = new JScrollPane(parameterTable);
        parametersScrollPane.setPreferredSize(new Dimension(450,150));
        
        ListSelectionModel rowSM = parameterTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    int selectedRow = parameterTable.getSelectedRow();
                    if (selectedRow != -1) {
                        okButton.setEnabled(true);
                    } else {
                        okButton.setEnabled(false);
                    }
                }
            });
        
        
        
        okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setEnabled(false);
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int selectedRow = parameterTable.getSelectedRow();
                    if (mustGiveValue) {
                        if (selectedRow != -1) {
                            value = valueTextField.getText();
                            parameter = DataModel.getCurrentProject().getParameterFromModel((String)parameterTable.getModel().getValueAt(selectedRow, 0));
                        }
                    } else {
                        parameterList = new ArrayList();
                        int[] selectedRows = parameterTable.getSelectedRows();
                        for (int i = 0; i < selectedRows.length; i++) {
                            parameterList.add(DataModel.getCurrentProject().getParameterFromModel((String)parameterTable.getModel().getValueAt(selectedRows[i], 0)));
                        }
                    }
                    UsingDefinedParameterView.this.dispose();
                }
            });
        
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    UsingDefinedParameterView.this.dispose();
                }
            });
        
        valueTextField = new JTextField(30);
        JLabel valueLabel = new JLabel(Language.getInstance().getText("Valeur_"));
        
        JPanel valuePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        valuePanel.add(valueLabel);
        valuePanel.add(valueTextField);
        
        JPanel centerPanel = new JPanel(new BorderLayout());
        centerPanel.add(parametersScrollPane, BorderLayout.CENTER);
        if (withValue) {
            centerPanel.add(valuePanel, BorderLayout.SOUTH);
        }
        
        
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(centerPanel, BorderLayout.CENTER);
        contentPaneFrame.add(buttonPanel, BorderLayout.SOUTH);
        
        this.setTitle(Language.getInstance().getText("Utiliser_un_parametre_existant"));
        //this.setLocation(500,400);
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);
        */
        centerScreen();
    }
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    public ArrayList getParameterList() {
        return parameterList;
    }
    
    
    public Parameter getParameter() {
        return parameter;
    }
    
    public String getValue() {
        return value;
    }
    
} // Fin de la classe UsingDefinedParameterView
