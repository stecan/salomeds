/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.TestDriver;

/**
 * @author teaml039
 * @version : 0.1
 */
public class AutomaticTestScriptView extends JPanel implements ApiConstants,
                                                               ActionListener {

    static JLabel tscName;
    static JLabel plugArgLabel;
    static JLabel extensionLabel;

    static JButton openFile;
    static JButton delFile;

    static JMenuBar scriptMenuBar;
    static JMenu scriptMenu;
    static JMenuItem modifyScriptItem;
    static JMenuItem actualItem;
    static JMenuItem setUpEngineItem;
    static JMenuItem aboutItem;

    String extension;
    String plugArg;
    static AutomaticTest pTest = null;
    TestDriver driver = null;
    File file;

    /**
     *
     *
     */
    public AutomaticTestScriptView() {
        pTest = null;
        // script = new Script();

        tscName = new JLabel(Language.getInstance().getText("Nom_du_script_"));
        plugArgLabel = new JLabel(Language.getInstance().getText(
                                                                 "Arguments_du_driver_"));
        extensionLabel = new JLabel(Language.getInstance()
                                    .getText("extension_"));

        openFile = new JButton(Language.getInstance().getText("Ajouter"));
        openFile.setToolTipText(Language.getInstance().getText(
                                                               "Ajouter_un_fichier"));
        openFile.addActionListener(this);

        delFile = new JButton(Language.getInstance().getText("Supprimer"));
        delFile.setToolTipText(Language.getInstance().getText(
                                                              "Supprimer_le_fichier_de_script"));
        delFile.setEnabled(false);
        delFile.addActionListener(this);

        scriptMenu = new JMenu(Language.getInstance().getText("Script"));
        modifyScriptItem = new JMenuItem(Language.getInstance().getText(
                                                                        "Modifier_le_Script"));
        modifyScriptItem.addActionListener(this);
        modifyScriptItem.setEnabled(false);

        actualItem = new JMenuItem(Language.getInstance().getText(
                                                                  "Actualiser_le_Script"));
        actualItem.addActionListener(this);
        actualItem.setEnabled(false);

        setUpEngineItem = new JMenuItem(Language.getInstance().getText(
                                                                       "SetUp_Driver"));
        setUpEngineItem.addActionListener(this);
        setUpEngineItem.setEnabled(false);

        aboutItem = new JMenuItem(Language.getInstance().getText(
                                                                 "A_propos_du_driver"));
        aboutItem.addActionListener(this);
        aboutItem.setEnabled(true);

        scriptMenu.add(modifyScriptItem);
        scriptMenu.add(actualItem);
        scriptMenu.add(setUpEngineItem);
        scriptMenu.add(aboutItem);

        scriptMenuBar = new JMenuBar();
        scriptMenuBar.add(scriptMenu);

        // JPanel allButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JPanel allButtons = new JPanel(new GridLayout(1, 3));
        allButtons.add(openFile);
        allButtons.add(delFile);
        allButtons.add(scriptMenuBar);

        allButtons.setBorder(BorderFactory.createRaisedBevelBorder());

        // Mapping entre composants graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.AUTOMATED_TEST_SCRIPT_BUTTONS_PANEL, allButtons);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.AUTOMATED_TEST_SCRIPT_BUTTONS_PANEL);

        JPanel extensionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        extensionPanel.add(extensionLabel);

        // Mapping entre composants graphiques et constantes
        SalomeTMFContext.getInstance()
            .addToUIComponentsMap(
                                  UICompCst.AUTOMATED_TEST_SCRIPT_EXTENSION_PANEL,
                                  extensionPanel);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.AUTOMATED_TEST_SCRIPT_EXTENSION_PANEL);

        JPanel classPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        classPanel.add(plugArgLabel);

        // Mapping entre composants graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.AUTOMATED_TEST_SCRIPT_CLASSNAME_PANEL, classPanel);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.AUTOMATED_TEST_SCRIPT_CLASSNAME_PANEL);

        JPanel fileNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fileNamePanel.add(tscName);

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                                                            UICompCst.AUTOMATED_TEST_SCRIPT_FILENAME_PANEL, fileNamePanel);
        // Add this component as static component
        UICompCst.staticUIComps
            .add(UICompCst.AUTOMATED_TEST_SCRIPT_FILENAME_PANEL);

        JPanel globalPanel = new JPanel();
        globalPanel.setLayout(new BoxLayout(globalPanel, BoxLayout.Y_AXIS));
        globalPanel.add(fileNamePanel, BorderLayout.NORTH);
        globalPanel.add(classPanel);
        globalPanel.add(extensionPanel);

        JPanel hh = new JPanel(new BorderLayout());
        hh.add(globalPanel, BorderLayout.NORTH);

        this.setLayout(new BorderLayout());
        this.add(allButtons, BorderLayout.NORTH);
        this.add(hh, BorderLayout.CENTER);

    } // Fin du constructeur

    /**
     * @return
     */
    public static JLabel getTscName() {
        return tscName;
    }

    /**
     * @param label
     */
    public static void setTscName(JLabel label) {
        tscName = label;
    }

    /**
     * @return
     */
    public static JLabel getPlugArgLabel() {
        return plugArgLabel;
    }

    /**
     * @return
     */
    public static JLabel getExtensionLabel() {
        return extensionLabel;
    }

    public static void giveAccessToIhmScriptView() {
        if (!Permission.canCreateTest()) {
            openFile.setEnabled(false);
        }
        if (!Permission.canDeleteTest()) {
            delFile.setEnabled(false);
        }
        if (!Permission.canUpdateTest()) {
            modifyScriptItem.setEnabled(false);
            aboutItem.setEnabled(false);
            setUpEngineItem.setEnabled(false);
        }
    }

    /**
     * @return
     */
    public static JButton getDelFile() {
        return delFile;
    }

    /* Invoked when test is selected in the treewiew */
    public static void setTest(AutomaticTest test) {
        pTest = test;
        if (test.getScriptFromModel() != null) {
            tscName.setText(Language.getInstance().getText("Nom_du_script__")
                            + test.getScriptFromModel().getNameFromModel());
            if (Permission.canUpdateTest()) {
                setUpEngineItem.setEnabled(true);
                modifyScriptItem.setEnabled(true);
            }

            if (Permission.canDeleteTest()) {
                delFile.setEnabled(true);

                setUpEngineItem.setEnabled(true);
                modifyScriptItem.setEnabled(true);
            }
            if (test.getScriptFromModel().getScriptExtensionFromModel() == null) {
                extensionLabel.setText(Language.getInstance().getText(
                                                                      "Extension__"));
            } else {
                extensionLabel.setText(Tools.createHtmlString(Language
                                                              .getInstance().getText("Extension__")
                                                              + test.getScriptFromModel()
                                                              .getScriptExtensionFromModel(), 100));
            }
            if (test.getScriptFromModel().getPlugArgFromModel() != null) {
                plugArgLabel.setText(Language.getInstance().getText(
                                                                    "Arguments_du_driver__")
                                     + test.getScriptFromModel().getPlugArgFromModel());
            } else {
                plugArgLabel.setText(Language.getInstance().getText(
                                                                    "Arguments_du_driver__"));
            }
        } else {
            tscName.setText(Language.getInstance().getText("Nom_du_script__"));
            extensionLabel.setText(Language.getInstance()
                                   .getText("Extension__"));
            plugArgLabel.setText(Language.getInstance().getText(
                                                                "Arguments_du_driver__"));
            delFile.setEnabled(false);

            modifyScriptItem.setEnabled(false);
            setUpEngineItem.setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // pTest = (AutomaticTest) DataModel.getCurrentTest();
        Object source = e.getSource();
        if (pTest == null) {
            JOptionPane
                .showMessageDialog(
                                   AutomaticTestScriptView.this,
                                   Language
                                   .getInstance()
                                   .getText(
                                            "Impossible_de_trouver_le_test_correspond_dans_le_plan_de_test"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
            return;
        }
        Util
            .log("Activate Extention with : "
                 + SalomeTMFContext.getInstance().associatedExtension
                 .get(pTest.getExtensionFromModel()));
        Util.log("Activate Extention with : "
                 + SalomeTMFContext.getInstance().urlSalome);
        Util.log("Activate Extention with : "
                 + SalomeTMFContext.getInstance().jpf);

        driver = pTest.ActivateExtention((Extension) SalomeTMFContext
                                         .getInstance().associatedExtension.get(pTest
                                                                                .getExtensionFromModel()),
                                         SalomeTMFContext.getInstance().urlSalome, SalomeTMFContext
                                         .getInstance().jpf);
        if (driver == null
            && (!(source.equals(delFile)) || !(source.equals(openFile)))) {
            JOptionPane
                .showMessageDialog(
                                   AutomaticTestScriptView.this,
                                   Language
                                   .getInstance()
                                   .getText(
                                            "Impossible_d_activer_le_plugin_d_execution_de_tests_")
                                   + pTest.getExtensionFromModel(), Language
                                   .getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (source.equals(openFile)) {
            onpenFilePerformed(e);
        } else if (source.equals(delFile)) {
            delFilePerformed(e);
        } else if (source.equals(modifyScriptItem)) {
            modifyPerformed(e);
        } else if (source.equals(actualItem)) {
            commitPerformed(e);
        } else if (source.equals(setUpEngineItem)) {
            modifyArgPerformed(e);
        } else if (source.equals(aboutItem)) {
            plugInfoPerformed(e);
        }

    }

    private void onpenFilePerformed(ActionEvent e) {
        File file = driver.choiceTest(pTest);
        Script script = null;
        boolean update = false;
        if (pTest.getScriptFromModel() != null) {
            script = pTest.getScriptFromModel();
            update = true;
        } else {
            script = new Script(file.getName(), "");
        }
        if (file != null) {

            tscName.setText(Language.getInstance().getText("Nom_du_script__")
                            + file.getName());
            script.setLocalisation(file.getAbsolutePath());
            script.setTypeInModel(TEST_SCRIPT);
            script.setScriptExtensionInModel(pTest.getExtensionFromModel());
            // String[] fileNamePart = file.getName().split("[.]");
            if (!update)
                plugArg = driver.getDefaultTestDiverAgument();

            script.updatePlugArgInModel(plugArg);

            try {
                if (!file.exists()) {
                    file.createNewFile();
                }
                if (update) {
                    Util
                        .log("[AutomaticTestView:onpenFilePerformed] : is update");
                    script.updateInDB(file);
                    /*
                     * script.updateNameInDB(file.getName());
                     * script.updateContentInDB(file.getAbsolutePath());
                     * script.updateDateInDB(new Date(file.lastModified()));
                     * script.updateLengthInDB(file.length());
                     */
                } else {
                    ((AutomaticTest) DataModel.getCurrentTest())
                        .addScriptInDBAndModel(script, file);
                }
                // ((AutomaticTest)DataModel.getCurrentTest()).setScript(script);
                plugArgLabel.setText(Language.getInstance().getText(
                                                                    "Arguments_du_driver__")
                                     + plugArg);

                if (plugArg == null || plugArg.equals("")) {
                    setUpEngineItem.setEnabled(false);
                } else {
                    setUpEngineItem.setEnabled(true);
                }
                delFile.setEnabled(true);
                modifyScriptItem.setEnabled(true);
            } catch (Exception exception) {
                exception.printStackTrace();
                Tools.ihmExceptionView(exception);
            }
        }

    }

    private void delFilePerformed(ActionEvent e) {
        driver.onDeleteTestScript(pTest);
        Script script = pTest.getScriptFromModel();
        if (script == null)
            return;
        int choice = -1;
        Object[] options = { Language.getInstance().getText("Oui"),
                             Language.getInstance().getText("Non") };
        choice = SalomeTMFContext.getInstance().askQuestion(
                                                            Language.getInstance()
                                                            .getText("Supprimer_le_fichier_de_script")
                                                            + " : " + script.getNameFromModel() + " ?",
                                                            Language.getInstance().getText("Attention_"),
                                                            JOptionPane.WARNING_MESSAGE, options);
        if (choice != JOptionPane.YES_OPTION) {
            return;
        }

        try {
            // BdD
            pTest.deleteScriptInDBAndModel();
            // ((AutomaticTest)DataModel.getCurrentTest()).deleteScriptFromDB(script.getName());

            // IHM
            // pTest.setScript(null);
            tscName.setText(Language.getInstance().getText("Nom_du_script__"));
            plugArgLabel.setText(Language.getInstance().getText(
                                                                "Arguments_du_driver_"));
            extensionLabel.setText(Language.getInstance()
                                   .getText("extension__"));
            modifyScriptItem.setEnabled(false);
            actualItem.setEnabled(false);
            setUpEngineItem.setEnabled(true);
        } catch (Exception exception) {
            Tools.ihmExceptionView(exception);
        }
    }

    private void modifyPerformed(ActionEvent e) {
        try {
            Cursor c = new Cursor(Cursor.WAIT_CURSOR);
            AutomaticTestScriptView.this.setCursor(c);
            Hashtable param = new Hashtable();
            String plugSeting = "";
            param.put("salome_projectName", DataModel.getCurrentProject()
                      .getNameFromModel());
            param.put("salome_ProjectObject", DataModel.getCurrentProject());
            param.put("salome_debug", new Boolean(true));
            param.put("salome_CampagneName", "");
            param.put("salome_ExecName", ""); // Add MM
            param.put("salome_environmentName", "");
            param.put("salome_TestName", DataModel.getCurrentTest()
                      .getNameFromModel());
            param.put("salome_SuiteTestName", DataModel.getCurrentTest()
                      .getTestListFromModel().getNameFromModel());
            param.put("salome_FamilyName", DataModel.getCurrentTest()
                      .getTestListFromModel().getFamilyFromModel()
                      .getNameFromModel());
            param.put("testLog", "");
            param.put("Verdict", "");

            plugSeting = pTest.getScriptFromModel().getPlugArgFromModel();

            if (Api.isConnected()) {
                // file =
                // ((AutomaticTest)DataModel.getCurrentTest()).getTestScriptFromDB(((AutomaticTest)DataModel.getCurrentTest()).getScriptFromModel().getNameFromModel());
                file = ((AutomaticTest) DataModel.getCurrentTest())
                    .getTestScriptFromDB();
                driver.editTest(Tools.speedpurge(file.getAbsolutePath()),
                                pTest, param, plugSeting);
            } else {
                Script script = pTest.getScriptFromModel();
                if (script != null)
                    driver.editTest(
                                    Tools.speedpurge(script.getNameFromModel()), pTest,
                                    param, plugSeting);
            }
            actualItem.setEnabled(true);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(AutomaticTestScriptView.this,
                                          Language.getInstance().getText(
                                                                         "Erreur_pour_lancer_l_editeur_de_script__")
                                          + ex.getMessage(), Language.getInstance().getText(
                                                                                            "Erreur_"), JOptionPane.ERROR_MESSAGE);

            Util.err(ex);
            // return;
        }

        try {
            Cursor c = new Cursor(Cursor.DEFAULT_CURSOR);
            AutomaticTestScriptView.this.setCursor(c);
        } catch (Exception ee) {
            Util.err(ee);
        }
        // commitButton.setEnabled(true);

    }

    private void commitPerformed(ActionEvent e) {
        if (Api.isConnected()) {
            Date date = new Date(file.lastModified());
            try {

                Script script = ((AutomaticTest) DataModel.getCurrentTest())
                    .getScriptFromModel();
                script.updateInDB(file);
                /*
                 * script.updateContentInDB(file.getAbsolutePath());
                 * script.updateDateInDB(date);
                 * script.updateLengthInDB(file.length());
                 */

                JOptionPane.showMessageDialog(AutomaticTestScriptView.this,
                                              Language.getInstance().getText(
                                                                             "Le_fichier_a_ete_correctement_archive"),
                                              Language.getInstance().getText("Info"),
                                              JOptionPane.INFORMATION_MESSAGE);
                actualItem.setEnabled(false);

            } catch (FileNotFoundException fe) {
                JOptionPane
                    .showMessageDialog(
                                       AutomaticTestScriptView.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Impossible_de_trouver_le_fichier_Vous_devez_l_importer_en_cliquant_sur_le_bouton_Modifier"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
            }
        }
    }

    public void modifyArgPerformed(ActionEvent e) {
        Script script = pTest.getScriptFromModel();
        String newArg = driver.modifyTestDiverAgument(script
                                                      .getPlugArgFromModel());

        if (newArg != null && !newArg.equals("")) {

            try {
                script.updatePlugArgInDBAndModel(newArg);

                plugArgLabel.setText(Language.getInstance().getText(
                                                                    "Arguments_du_driver__")
                                     + newArg);

            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
            }
        }
    }

    public void plugInfoPerformed(ActionEvent e) {
        driver.getHelp();
    }

} // Fin de la classe AutomaticTestScriptView
