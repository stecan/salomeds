package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class DataPanel extends JPanel {
    JTabbedPane dataSpace;

    // JLabel projetNameData;

    // changeListenerPanel dataMultiUserChangeListenerPanel;

    JMenu dataToolsMenu;

    EnvironmentView pEnvView;
    ParameterView dataParameterView;
    AttachmentView projectAttachmentView;

    BaseIHM pBaseIHM;
    SalomeTMFContext pSalomeTMFContext;
    int t_x = 1024;
    int t_y = 768;
    JTabbedPane tabs;

    public DataPanel(SalomeTMFContext m_SalomeTMFContext, BaseIHM m_BaseIHM,
                     JTabbedPane m_tabs, int x, int y) {
        super(new BorderLayout());
        pSalomeTMFContext = m_SalomeTMFContext;
        pBaseIHM = m_BaseIHM;
        tabs = m_tabs;
        t_x = x;
        t_y = y;
    }

    JLabel projetName;
    JLabel userName;

    public void initCoponent() {

        dataToolsMenu = new JMenu(Language.getInstance().getText("Outils"));
        pSalomeTMFContext.UIComponentsMap.put(UICompCst.DATA_TOOLS_MENU,
                                              dataToolsMenu);

        // projetNameData = new JLabel();
        // projetNameData.setFont(new Font(null, Font.BOLD, 18));
        // JPanel namePanelData = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // namePanelData.add(projetNameData);

        JLabel projectLabel = new JLabel(Language.getInstance().getText(
                                                                        "Projet__"));
        projetName = new JLabel();
        userName = new JLabel();
        projetName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        userName.setFont(new Font(null, Font.ROMAN_BASELINE, 16));
        JPanel namePanelData = new JPanel(new FlowLayout(FlowLayout.LEFT));
        projectLabel.setFont(new Font(null, Font.BOLD, 16));
        namePanelData.setLayout(new GridBagLayout());
        JLabel userLabel = new JLabel(Language.getInstance().getText(
                                                                     "Utilisateurs")
                                      + " : ");
        userLabel.setFont(new Font(null, Font.BOLD, 16));
        userLabel.setLayout(new GridBagLayout());
        namePanelData.add(projectLabel, new GridBagConstraints(0, 0, 1, 1, 0.5,
                                                               0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                               new Insets(10, 10, 10, 5), 0, 0));
        namePanelData.add(projetName, new GridBagConstraints(1, 0, 1, 1, 0.5,
                                                             0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                             new Insets(10, 5, 10, 20), 0, 0));
        namePanelData.add(userLabel, new GridBagConstraints(2, 0, 1, 1, 0.5,
                                                            0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                            new Insets(10, 20, 10, 5), 0, 0));
        namePanelData.add(userName, new GridBagConstraints(3, 0, 1, 1, 0.5,
                                                           0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                           new Insets(10, 5, 10, 5), 0, 0));

        /*
         * dataMultiUserChangeListenerPanel = new changeListenerPanel();
         * dataMultiUserChangeListenerPanel.setPreferredSize(new
         * Dimension(70,25)); JPanel dataMultiUserPanel = new JPanel(new
         * FlowLayout(FlowLayout.RIGHT));
         * dataMultiUserPanel.add(dataMultiUserChangeListenerPanel);
         */

        JPanel dataTopPanel = new JPanel(new BorderLayout());
        dataTopPanel.add(namePanelData, BorderLayout.WEST);
        // dataTopPanel.add(dataMultiUserPanel, BorderLayout.EAST);

        add(dataTopPanel, BorderLayout.NORTH);
        createDataPlane();
    }

    /**
     * Methode qui cree la vue sur les donnees
     */
    public void createDataPlane() {

        // JPanel context = new JPanel();

        // JMenuItem refreshItem = new JMenuItem("Rafraichir");
        JMenu refreshItem = new JMenu(Language.getInstance().getText(
                                                                     "Rafraichir"));
        /*
         * JMenuItem refreshItem = new
         * JMenuItem(Language.getInstance().getText("Rafraichir"));
         * refreshItem.addActionListener(new ActionListener() { public void
         * actionPerformed(ActionEvent e) { tabs.setCursor(new
         * Cursor(Cursor.WAIT_CURSOR)); DataModel.reloadFromBase(true);
         * //TestMethods.refreshFromBase(); tabs.setCursor(new
         * Cursor(Cursor.DEFAULT_CURSOR)); } });
         */

        JMenuItem refreshItemAll = new JMenuItem(Language.getInstance()
                                                 .getText("Tout"));
        refreshItemAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    DataModel.reloadFromBase(true);
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        JMenuItem refreshItemCamp = new JMenuItem(Language.getInstance()
                                                  .getText("Vue_des_donnees"));
        refreshItemCamp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // TestMethods.refreshTestTree();
                    tabs.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                    try {
                        DataModel.reloadDataPlan();
                    } catch (Exception ex) {
                        Util.err(ex);
                        DataModel.reloadFromBase(true);
                    }
                    tabs.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        refreshItem.add(refreshItemAll);
        refreshItem.add(refreshItemCamp);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.DATA_MANAGEMENT_REFRESH_ITEM, refreshItem);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.DATA_MANAGEMENT_REFRESH_ITEM);

        dataToolsMenu.add(refreshItem);

        // Sous menu pour changer le mot de passe
        if (Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            // JMenuItem changePwdItem = new
            // JMenuItem("Changer le mot de passe");
            JMenuItem changePwdItem = new JMenuItem(Language.getInstance()
                                                    .getText("Changer_le_mot_de_passe"));
            changePwdItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new ChangePwdWindow(pSalomeTMFContext.ptrFrame).show();
                    }
                });

            dataToolsMenu.add(changePwdItem);
        }
        JMenuBar menuBar = new JMenuBar();
        menuBar.add(dataToolsMenu);

        JButton quitTestButton = new JButton(Language.getInstance().getText(
                                                                            "Quitter"));
        quitTestButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pBaseIHM.quit(true, true);
                }
            });

        JPanel upButtonsPanel = new JPanel();
        upButtonsPanel
            .setLayout(new BoxLayout(upButtonsPanel, BoxLayout.Y_AXIS));

        JPanel pnl = new JPanel();
        pnl.setBackground(Color.WHITE);

        // Mapping entre objets graphiques et constantes
        pSalomeTMFContext.addToUIComponentsMap(
                                               UICompCst.DATA_MANAGEMENT_BUTTONS_PANEL, upButtonsPanel);
        // Add this component as static component
        UICompCst.staticUIComps.add(UICompCst.DATA_MANAGEMENT_BUTTONS_PANEL);

        pEnvView = new EnvironmentView();
        ParameterView pParamview;
        dataParameterView = new ParameterView(false, DataConstants.PARAMETER);
        dataParameterView.setName(Language.getInstance().getText(
                                                                 "Vue_des_donnees"));
        projectAttachmentView = new AttachmentView(pBaseIHM,
                                                   DataConstants.PROJECT, DataConstants.PROJECT, null,
                                                   DataConstants.NORMAL_SIZE_FOR_ATTACH, null);

        dataSpace = new JTabbedPane();
        dataSpace.addTab(Language.getInstance().getText("Environnements"),
                         pEnvView);
        dataSpace.addTab(Language.getInstance().getText("Parametres"),
                         dataParameterView);
        dataSpace.addTab(Language.getInstance().getText("attachement_project"),
                         projectAttachmentView);
        dataSpace.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (dataSpace.getSelectedComponent().equals(
                                                                projectAttachmentView))
                        try {
                            DataModel.reloadProjectAttachement();
                        } catch (Exception ex) {

                        }
                    DataModel.initAttachmentTable(DataModel.getCurrentProject()
                                                  .getAttachmentMapFromModel().values());
                }
            });

        // createEnvironment(context);
        // createParameter(parameters, false);

        add(dataSpace, BorderLayout.CENTER);
        menuBar.setMaximumSize(new Dimension(t_x, 10));
        upButtonsPanel.add(menuBar);
        upButtonsPanel.add(dataSpace);

        add(upButtonsPanel, BorderLayout.CENTER);

    }

    /*
     * private JPanel createAttachmentView(int type) { //panel.setLayout(new
     * BorderLayout()); int realType; if (type == DataConstants.MANUAL_TEST ||
     * type == DataConstants.AUTOMATIC_TEST) { realType = DataConstants.TEST; }
     * else { realType = type; } //AttachmentView view = new
     * AttachmentView(this, type , realType, null, NORMAL_SIZE_FOR_ATTACH, null,
     * null, null); projectAttachmentView = new AttachmentView(pBaseIHM, type ,
     * realType, null, DataConstants.NORMAL_SIZE_FOR_ATTACH, null);
     *
     * } // Fin de la m?thode createAutomaticTestAttachment/1
     */

    /**
     * Methode qui cree la vue sur les environnements
     *
     * @param panel
     *            le panel qui contient la vue
     */
    /*
     * public void createEnvironment() { pEnvView = new EnvironmentView();
     * //panel.setLayout(new BorderLayout()); //panel.add(view,
     * BorderLayout.CENTER); }
     */

    /**
     * Methode qui cree la vue sur les param?tres des test manuels.
     *
     * @param panel
     *            le panel qui contient la vue.
     */
    /*
     * private JPanel createParameterPanel(boolean use) { ParameterView
     * pParamview;
     *
     * pParamview = new ParameterView(use, DataConstants.PARAMETER);
     * pParamview.setName(Language.getInstance().getText("Vue_des_donnees"));
     * return pParamview; //dataParameterView = pParamview;
     * //panel.setLayout(new BorderLayout()); //panel.add(pParamview,
     * BorderLayout.CENTER);
     *
     * }
     */

    /****************************************************************************************************************/

    void loadModel(String strProject, String strLogin) {
        // dataMultiUserChangeListenerPanel.reset();
        dataParameterView.giveAccessToIhmParameterView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());
        projectAttachmentView.giveAccessToIhmScriptView();
    }

    void reloadModel() {
        // dataMultiUserChangeListenerPanel.reset();
        dataParameterView.giveAccessToIhmParameterView();
        projetName.setText(DataModel.getCurrentProject().getNameFromModel());
        userName.setText(DataModel.getCurrentUser().getTwoNameFromModel());
        projectAttachmentView.giveAccessToIhmScriptView();
    }
}
