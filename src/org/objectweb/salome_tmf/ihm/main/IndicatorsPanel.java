package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.ReqManager;

public class IndicatorsPanel extends JPanel implements IHMConstants {

    private JTabbedPane indicatorsTabbedPane;

    private QsScorePanel qsScorePanel;

    private IcalPanel icalPanel;
        
    private BugTracker mantis;
        
    private ReqManager reqPlug;

    public IndicatorsPanel(BugTracker mantis, ReqManager req) {
        super(new BorderLayout());
        this.mantis = mantis;
        this.reqPlug = req;
    }

    void initComponents() {
        try {
            indicatorsTabbedPane = new JTabbedPane();
            add(indicatorsTabbedPane, BorderLayout.CENTER);
            indicatorsTabbedPane.addChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent evt) {
                        indicatorsTabbedPaneStateChanged(evt);
                    }
                });
            if (reqPlug != null){
                qsScorePanel = new QsScorePanel(mantis,reqPlug);
                indicatorsTabbedPane.addTab("QS SCORE", Tools.createAppletImageIcon(PATH_TO_QSSCORE_ICON,""), qsScorePanel, "QS SCORE");
            }
            icalPanel = new IcalPanel(mantis);
            indicatorsTabbedPane.addTab("ICAL", Tools.createAppletImageIcon(PATH_TO_ICAL_ICON,""), icalPanel, "ICAL");
        } catch (Exception e) {
            Util.err(e);
        }
    }

    private void indicatorsTabbedPaneStateChanged(ChangeEvent evt) {
        //TODO
    }

}
