package org.objectweb.salome_tmf.ihm.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.common.CommonLogin;
import org.objectweb.salome_tmf.ihm.languages.Language;

public class ProjectAdminLogin extends JDialog implements ActionListener{
    CommonLogin pCommonLogin;
    String usedLocale = "";
    String strProjet = null;
    String strUser = null;
    String strPassword = null;
        
    public ProjectAdminLogin(){
        setModal(true);
        pCommonLogin = new CommonLogin(false, true, false, this);
        setTitle("SalomeTMF Login");
        initComponents();
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getActionCommand().equals(CommonLogin.ACTION_START_PROJECT_ADMIN)){
            if (pCommonLogin.testAuthentification1()) {
                try {
                    usedLocale = pCommonLogin.getUsedLocal();
                    Api.saveLocale(usedLocale);
                    strProjet = pCommonLogin.getSelectedAdminSalomeProject();
                    strUser = pCommonLogin.getSelectedAdminSalomeUser().getLogin();     
                    strPassword = pCommonLogin.getPassword1();
                    setVisible(false);
                } catch (Exception me) {
                    Util.log("[LoginSalomeTMF->b_startActionPerformed]" + me);
                }
            }else {
                strProjet = null;
                strUser = null;
                pCommonLogin.error(Language.getInstance().getText("Mot_de_passe_invalide"));
            }
        }
    }
        
    public String getSelectedProject(){
        return strProjet;
    }
        
    public String getSelectedUser(){
        return strUser;
    }
        
    public String getSelectedPassword(){
        return strPassword;
    }
        
    private void initComponents() {    
        /*
          try {
          GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
          GraphicsDevice[] gs = ge.getScreenDevices();
          GraphicsDevice gd = gs[0];
          GraphicsConfiguration[] gc = gd.getConfigurations();
          Rectangle r = gc[0].getBounds();
          Point pt = new Point( r.width/2, r.height/2 );
          Point loc = new Point( pt.x - 200, pt.y - 150 );
                        
          // Affichage
          setLocation(loc);
          } catch (Exception e){
                        
          }*/
                
        this.setLocationRelativeTo(this.getParent()); 
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent we) {
                    System.exit(0);
                }
            });
                
        setContentPane(pCommonLogin);
        pack();
                
    }
}
