package org.objectweb.salome_tmf.ihm.main;

import java.awt.Container;
import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.util.Locale;

import javax.swing.JApplet;
import javax.swing.JOptionPane;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

//import com.sun.java.browser.dom.DOMService;


public class SalomeTMF_AppJWS  extends JApplet implements  BaseIHM, WindowListener {
    boolean problemURL = false;



    protected URL urlSalome;
    protected Frame ptrFrame;

    boolean closeDB = true;

    protected String strProject = null;
    protected String strLogin = null;
    int idConn = -1;

    static boolean exit = false;

    /***************************************************************************************/
    protected SalomeTMFContext pSalomeTMFContext;
    protected SalomeTMFPanels pSalomeTMFPanels;
    /****************************************************************************************/

    JApplet parentApplet = null;
    JavaScriptUtils pJSUtils;

    public void setParentApplet(JApplet _parentApplet) {
        parentApplet = _parentApplet;
        Util.debug("SalomeTMF_AppJWS = " + SalomeTMF_AppJWS.class.getClassLoader());
    }

    @Override
    public void init() {
        if (parentApplet == null){
            parentApplet = this;
        }
        Util.log("[SalomeTMF] - Applet init");
        onInit();
        exit = false;
    }

    void initComponent(){
        //Api.setUrlBase(getDocumentBase());
        Api.setUrlBase(urlSalome);
        try {
            Class lnfClass = Class.forName(UIManager.getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel)(lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
            Util.adaptFont();
        } catch (Exception exc) {
            Util.debug("Error loading L&F: " + exc);
        }
        Util.log("[SalomeTMF->initComponent] Used Local is "+ Api.getUsedLocale());
        Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));

        //ptrFrame = javax.swing.JOptionPane.getFrameForComponent(SalomeTMF_AppJWS.this);  //FOR MINI BOOT
        ptrFrame = javax.swing.JOptionPane.getFrameForComponent(parentApplet);
        if (parentApplet == this){
            ptrFrame.addWindowListener(this);
        }

        pSalomeTMFContext = new SalomeTMFContext(urlSalome,ptrFrame, this);
        pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, this);
        pSalomeTMFPanels.initComponentPanel();

        //Container cp = this.getContentPane();  //FOR MINI BOOT
        Container cp = parentApplet.getContentPane();
        cp.add(pSalomeTMFPanels.tabs);

        //waitView.dispose();
        /* try {
           Cursor c = new Cursor(Cursor.DEFAULT_CURSOR);
           this.setCursor(c);
           } catch (Exception e) {
           Util.err(e);
           }*/
    }

    void reInitComponent(){
        try {
            Class lnfClass = Class.forName(UIManager.getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel)(lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
        } catch (Exception exc) {
            Util.debug("Error loading L&F: " + exc);
        }

        ptrFrame = javax.swing.JOptionPane.getFrameForComponent(parentApplet);
        if (parentApplet == this){
            ptrFrame.addWindowListener(this);
        }


        //pSalomeTMFContext = new SalomeTMFContext(urlSalome,ptrFrame, this);
        SalomeTMFContext.getInstance().ptrFrame = ptrFrame;
        pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, this);
        pSalomeTMFPanels.initComponentPanel();

        //Container cp = this.getContentPane();  //FOR MINI BOOT
        Container cp = parentApplet.getContentPane();
        cp.add(pSalomeTMFPanels.tabs);
    }

    protected boolean connectToAPI(){
        Util.log("[SalomeTMF] - Try connection to API (NbConnect = " + Api.getNbConnect() +")");
        //if (Api.isConnected() && !reload) {
        pJSUtils = new JavaScriptUtils(parentApplet);
        if(Api.getNbConnect()>0){
            //JOptionPane.showMessageDialog(SalomeTMF_AppJWS.this, //FOR MINI BOOT
            JOptionPane.showMessageDialog(parentApplet,
                                          Language.getInstance().getText("Vous_avez_deja_une_session_Salome_ouverte_avec_ce_navigateur") +
                                          Language.getInstance().getText("Une_seule_session_est_autorisee_par_navigateur_afin_d_eviter_les_conflits_"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            quit(false, false);
            closeDB = false;
            return false;
        } else {
            //Api.openConnection(getDocumentBase());  //FOR MINI BOOT
            Api.openConnection(parentApplet.getCodeBase());
            if (!Api.isConnected()){

                JOptionPane.showMessageDialog(parentApplet,
                                              "Can't connect to the database",
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
                quit(true, false);
                return false;
            }
            strLogin= pJSUtils.getLoginCookies();
            strProject = pJSUtils.getProjectCookies();
            //Api.setUserAuthentification(pJSUtils.getUserAuthentficationCookies());

            if (strProject == null && strLogin == null){
                ProjectLogin dialog = new ProjectLogin(pJSUtils);
                dialog.setVisible(true);
                if(dialog.getSelectedProject() != null && dialog.getSelectedUser() != null){
                    strProject = dialog.getSelectedProject() ;
                    strLogin = dialog.getSelectedUser();
                    //                           Recuperation du login et du mot de passe pour l'authentification
                    try {
                        Api.setStrUsername(strLogin);
                        Api.setStrMD5Password(MD5paswd.getEncodedPassword(dialog.getSelectedPassword()));
                    } catch (Exception e) {
                        Util.err(e);
                    }
                    Api.initConnectionUser(strProject, strLogin);
                } else {
                    quit(true, true);
                    return false;
                }
            } else {
                Api.initConnectionUser(strProject, strLogin);
                Api.setUserAuthentification(pJSUtils.getUserAuthentficationCookies());

            }

        }
        //urlSalome = getDocumentBase();
        urlSalome = Api.getCodeBase();
        return true;
    }

    void loadPlugin(){
        pSalomeTMFContext.loadPlugin(pSalomeTMFPanels);
    }

    void reloadPlugin(){
        pSalomeTMFContext.reloadPlugin(pSalomeTMFPanels);
    }

    void loadModel(){
        if (strProject == null || strLogin == null) {
            quit(true, true);
        }
        DataModel.loadFromBase(strProject, strLogin, this);
        pSalomeTMFPanels.loadModel(strProject, strLogin);
    }

    void reloadModel(){
        if (strProject == null || strLogin == null) {
            quit(true, true);
        }
        DataModel.loadFromBase(strProject, strLogin, this);
        pSalomeTMFPanels.reloadModel();
    }
    /**
     * M?thode appel?e ? chaque chargement de la page
     */
    @Override
    public void start() {
        Util.log("[SalomeTMF] - Applet start");
        onStart();
    }


    /*********************************************/

    void onStart(){
        pSalomeTMFContext.startPlugin();
    }

    void onStop(){

    }

    public void reloadPanel(JApplet _parentApplet){
        parentApplet = _parentApplet;
        reInitComponent();
        String strP = strProject;
        String strL = strLogin;
        //loadingFromCookies();
        if (strLogin == null){
            strLogin = strL;
            strProject = strP;
        }
        loadModel();
        reloadPlugin();
    }

    void onInit(){
        Util.log("[SalomeTMF] - Applet init");
        System.runFinalization();
        System.gc();
        if (connectToAPI()){
            WaitView waitView = new WaitView();
            long l1 = System.currentTimeMillis();
            initComponent();
            loadModel();
            loadPlugin();
            long l2 = System.currentTimeMillis();
            Util.debug("Temps de chargement = " + (l2-l1));
            waitView.dispose();
        }

    }

    /**
     * M?thode appel? lorsque l'utilisateur quitte l'application.
     */
    @Override
    public void quit(boolean do_recup, boolean doclose) {
        if (parentApplet != this){
            return;
        }
        if (pSalomeTMFContext != null && doclose == true){
            pSalomeTMFContext.suspendPlugin();
        }
        Util.log("[SalomeTMF] - Applet quit (recup = " +do_recup +")" );
        if (Api.isConnected() && doclose) {
            Api.closeConnection();
        }
        ptrFrame.dispose();
        ptrFrame = null;
        System.gc();
        System.runFinalization();
        if (pJSUtils != null){
            pJSUtils.closeWindow();
        }
        exit = true;


    } // Fin de la m?thode quit();


    @Override
    public void stop() {
        Util.log("[SalomeTMF] - Applet stop");
    }

    @Override
    public void destroy() {
        Util.log("[SalomeTMF] Applet destroy (closeDB = " + closeDB +")");
        if (closeDB == true)
            Api.closeConnection();
    }

    /* void closeWindow(){
       try {
       //Class classJSObject = Class.forName("sun.plugin.javascript.JSObject");
       Class classJSObject = Class.forName("netscape.javascript.JSObject");
       Method meth_getWindow = classJSObject.getMethod("getWindow", new Class[] {Applet.class});
       Object pJSObject = meth_getWindow.invoke(null ,new Object[] {parentApplet});
       Util.debug("pJSObject = " + pJSObject);
       Method meth_call  = classJSObject.getMethod("call", new Class[] {String.class, Object[].class});
       meth_call.invoke(pJSObject, new Object[] {"close", null});

       } catch (Exception e) {
       Util.err(e);
       }
       }*/

    /**********************************************************************************************************************/

    @Override
    public SalomeTMFContext getSalomeTMFContext(){
        return pSalomeTMFContext;
    }

    @Override
    public SalomeTMFPanels getSalomeTMFPanels(){
        return pSalomeTMFPanels;
    }


    @Override
    public void showDocument(URL toShow , String where){
        //getAppletContext().showDocument(toShow, where); //FOR MINIBOOT
        parentApplet.getAppletContext().showDocument(toShow, where);
    }

    @Override
    public boolean isGraphique() {
        return true;
    }
    /***************** Windows Listener ***************/
    @Override
    public void windowClosing(WindowEvent e) {
        if (!exit)
            quit(true, true);
    }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowClosed(WindowEvent e) { }


    @Override
    public void windowDeactivated(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowOpened(WindowEvent e) { }

}
