/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.objectweb.salome_tmf.ihm.main;

import java.awt.Desktop;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URISyntaxException;
import javax.swing.JLabel;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

/**
 *
 * @author stecan
 */
public class JLabelLink extends JLabel {
    private static final String A_HREF = "<a href=\"";
    private static final String HREF_CLOSED = "\">";
    private static final String HREF_END = "</a>";
    private static final String HTML = "<html>";
    private static final String HTML_END = "</html>";
    
    private static URI lastURI;   // marker for the last URI

    private static final int TIMEOUT = 2000;  // Local timeout of 2 seconds!
    private static long timeStart = System.currentTimeMillis();
    private static long timeEnd = System.currentTimeMillis();
    
    public JLabelLink(String text) {
        this.setText(text);
    }

    public void generatePolarionLink(String text) {

        String fileName;
        String lineNumber;
        String link;
        PolarionLink polarionLink = new PolarionLink(
                DataModel.getCurrentProject().getIdBdd());
        link = polarionLink.convert(text);
        fileName = polarionLink.getFileName();
        lineNumber = polarionLink.getLineNumber();

        this.setText(link);
        if (isBrowsingSupported()) {
            makeLinkable(this, new LinkMouseListener(), fileName, lineNumber);
        }
    }

    private static void makeLinkable(JLabel c, MouseListener ml, 
            String fileName, String lineNumber) {
        assert ml != null;
        c.setText(htmlIfy(linkIfy(c.getText(), fileName, lineNumber)));
        c.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        c.addMouseListener(ml);
    }

    private static boolean isBrowsingSupported() {
        if (!Desktop.isDesktopSupported()) {
            return false;
        }
        boolean result = false;
        Desktop desktop = java.awt.Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.BROWSE)) {
            result = true;
        }
        return result;

    }

    private static class LinkMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            // Note:
            // This routine is invoked multiple times. Though, care have to
            // be taken to avoid the opening of multiple Browser tabs with
            // the same URI!
            JLabel l = (JLabel) evt.getSource();
            try {
                if ((timeEnd - timeStart) > TIMEOUT) {
                    // After 2 seconds, the URI marker is deleted. A new 
                    // Browser tab can be generated.
                    lastURI = null;
                }
                URI uri = new java.net.URI(JLabelLink.getPlainLink(l.getText()));
                if ((uri != null) && (!uri.equals(lastURI))) {
                    timeStart = System.currentTimeMillis();
                    (new LinkRunner(uri)).execute();
                }
                lastURI = uri;
                timeEnd = System.currentTimeMillis();
            } catch (URISyntaxException use) {
                Util.err(use);
            }
        }
    }

    private static String getPlainLink(String s) {
        return s.substring(s.indexOf(A_HREF) + A_HREF.length(), 
                s.indexOf(HREF_CLOSED));
    }

    //WARNING
    //This method requires that s is a plain string that requires
    //no further escaping
    private static String linkIfy(String s, 
            String fileName, String lineNumber) {
        if ((fileName == "") || (lineNumber == "")) {
            // normal usuage
            return A_HREF.concat(s).concat(HREF_CLOSED).concat(s).concat(HREF_END);
        } else {
            // usuage of shortened lxr-links
            return A_HREF.concat(s).concat(HREF_CLOSED).concat(fileName).
                    concat(lineNumber).concat(HREF_END);
        }
    }

    //WARNING
    //This method requires that s is a plain string that requires
    //no further escaping
    private static String htmlIfy(String s) {
        return HTML.concat(s).concat(HTML_END);
    }

}
