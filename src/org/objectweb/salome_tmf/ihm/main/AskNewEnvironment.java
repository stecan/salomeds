/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;


public class AskNewEnvironment extends JDialog implements ApiConstants, DataConstants, ActionListener, ListSelectionListener {
    
    /**
     * La zone texte pour saisir le nom de l'environnement
     */
    JTextField nameField;
    
    /**
     * La zone texte pour saisir la description
     */
    JTextArea descriptionArea;
    
    /**
     * Le nouvel environnement
     */
    Environment environment;
    
    /**
     * Zone de texte pour saisir le nom du script
     */
    JLabel scriptLabel;
    
    
    /**
     * Table des parametres
     */
    JTable parametersTable;
    
    /**
     * Modele de selection pour la table des parametres
     */
    ListSelectionModel rowSM;
    
    JButton deleteScriptButton;
    
    JButton modifyParameterButton;
    JButton deleteParameter;
    
    JButton okButton;
    
    JButton cancelButton;
    
    private File scriptFile;
    
    private Script script;
    
    JButton addParameterButton;
    
    JButton addScriptFileButton;
    
    JButton useDefinedParameterButton;
    
    TableSorter  sorter;
    
    boolean newFile = false;
    
    static ScriptFileFilter[] tabPlugScriptFileFilter = null;
    static JFileChooser fileChooser = new JFileChooser();
    
    AttachmentView attachmentPanel;
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    
    public AskNewEnvironment(Environment oldEnvironment) {
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        
        int t_x = 1024 - 100;
        int t_y = 768/3*2 - 50;
        int t_x2 = 1024;
        int t_y2 = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height/3*2 -50 ;
            t_x2 = r.width;
            t_y2 = r.height ;
        } catch(Exception E){
                        
        }
                
        nameField = new JTextField(10);
        descriptionArea = new JTextArea(15, 40);
        JLabel testNameLabel = new JLabel(Language.getInstance().getText("Nom_de_l_environnement__"));
        scriptLabel = new JLabel(Language.getInstance().getText("Nom_du_script__"));
        parametersTable = new JTable();
        modifyParameterButton = new JButton(Language.getInstance().getText("Modifier"));
        deleteParameter = new JButton(Language.getInstance().getText("Supprimer"));
        deleteScriptButton = new JButton(Language.getInstance().getText("Supprimer"));
        
        JPanel giveName = new JPanel();
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(testNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(nameField);
        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        descriptionScrollPane.setPreferredSize(new Dimension(100,100));
        
        addScriptFileButton = new JButton(Language.getInstance().getText("Chercher"));
        addScriptFileButton.setToolTipText(Language.getInstance().getText("Chercher_le_fichier_de_script"));
        addScriptFileButton.addActionListener(this);
        
        
        deleteScriptButton.setToolTipText(Language.getInstance().getText("Supprimer_le_fichier_de_script"));
        deleteScriptButton.addActionListener(this);
        
        
        
        JPanel giveScriptNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        giveScriptNamePanel.add(scriptLabel);
        
        JPanel scriptButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT ));
        scriptButtonPanel.add(addScriptFileButton);
        scriptButtonPanel.add(deleteScriptButton);
        
        JPanel giveScriptPanel = new JPanel();
        giveScriptPanel.setLayout(new BoxLayout(giveScriptPanel, BoxLayout.Y_AXIS));
        giveScriptPanel.add(giveScriptNamePanel);
        giveScriptPanel.add(scriptButtonPanel);
        
        sorter = new TableSorter(DataModel.getEnvironmentParameterTableModel());
        parametersTable.setModel(sorter);
        sorter.setTableHeader(parametersTable.getTableHeader());
        
        
        
        JScrollPane parametersScrollPane = new JScrollPane(parametersTable);
        parametersScrollPane.setPreferredSize(new Dimension(450,100));
        
        
        addParameterButton = new JButton(Language.getInstance().getText("Nouveau"));
        addParameterButton.setToolTipText(Language.getInstance().getText("Ajouter_un_nouveau_parametre"));
        addParameterButton.addActionListener(this);
        
        useDefinedParameterButton = new JButton(Language.getInstance().getText("Utiliser"));
        useDefinedParameterButton.setToolTipText(Language.getInstance().getText("Utiliser_un_parametre_existant"));
        useDefinedParameterButton.addActionListener(this);
        
        modifyParameterButton.setToolTipText(Language.getInstance().getText("Modifier_un_parametre"));
        modifyParameterButton.setEnabled(false);
        modifyParameterButton.addActionListener(this);
        
        deleteParameter.setToolTipText(Language.getInstance().getText("Supprimer_un_parametre"));
        deleteParameter.setEnabled(false);
        deleteParameter.addActionListener(this);
        
        //JPanel attachButtonPanel = new JPanel();
        //attachButtonPanel.setLayout(new BoxLayout(attachButtonPanel, BoxLayout.Y_AXIS));
        JPanel attachButtonPanel = new JPanel(new GridLayout(4,1));
        attachButtonPanel.add(addParameterButton);
        attachButtonPanel.add(useDefinedParameterButton);
        attachButtonPanel.add(modifyParameterButton);
        attachButtonPanel.add(deleteParameter);
        
        JPanel parameterPanel = new JPanel();
        parameterPanel.setLayout(new BoxLayout(parameterPanel, BoxLayout.X_AXIS));
        parametersScrollPane.setPreferredSize(new Dimension(t_x-50, t_y/3));
        parameterPanel.add(parametersScrollPane);
        attachButtonPanel.setMaximumSize(new Dimension(50, t_y/3));
        parameterPanel.add(attachButtonPanel);
        parameterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Parametres")));
        
        /*JPanel textPanel = new JPanel();
          textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
          textPanel.add(Box.createRigidArea(new Dimension(0,10)));
          textPanel.add(giveName);
          textPanel.add(Box.createRigidArea(new Dimension(0,10)));
          textPanel.add(giveScriptPanel);
          textPanel.add(Box.createRigidArea(new Dimension(1,30)));
          textPanel.add(descriptionScrollPane);
          textPanel.add(Box.createRigidArea(new Dimension(1,30)));
          textPanel.add(parameterPanel);
        */
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        giveScriptPanel.setMaximumSize(new Dimension(t_x,30));
        textPanel.add(giveScriptPanel);
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(descriptionScrollPane);
        
        
        //attachmentPanel = new AttachmentView(DataModel.getApplet(), ENVIRONMENT, ENVIRONMENT, oldEnvironment, SMALL_SIZE_FOR_ATTACH, null, null, null);
        attachmentPanel = new AttachmentView(DataModel.getApplet(), ENVIRONMENT, ENVIRONMENT, oldEnvironment, SMALL_SIZE_FOR_ATTACH, this);
        attachmentPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Attachements")));
        //textPanel.add(attachmentPanel);
        
        okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(this);
        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        
        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        
        //textPanel.add(buttonPanel);
        
        //Initialisation des donnees de la fenetre
        initData(oldEnvironment);
        
        /*
          Container contentPaneFrame = this.getContentPane();
          contentPaneFrame.add(textPanel, BorderLayout.CENTER);
        */
        /*
          Container contentPane = this.getContentPane(); 
          contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        
          giveName.setMaximumSize(new Dimension(t_x2, 30));
          contentPane.add(giveName);
        
          textPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
          contentPane.add(textPanel);
                
          parameterPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
          contentPane.add(parameterPanel);
                        
          attachmentPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
          contentPane.add(attachmentPanel);
                
          buttonPanel.setMaximumSize(new Dimension(t_x2, 40));
          contentPane.add(buttonPanel);
        */
                
        JTabbedPane pJTabbedPane = new JTabbedPane();
        Container contentPane = this.getContentPane(); 
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        JPanel envDetail =  new JPanel();
        envDetail.setLayout(new BoxLayout(envDetail, BoxLayout.Y_AXIS));
        giveName.setMaximumSize(new Dimension(t_x2, 30));
        envDetail.add(giveName);
        textPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
        envDetail.add(textPanel);
        parameterPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
        envDetail.add(parameterPanel);
                
        attachmentPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
                
        pJTabbedPane.add(Language.getInstance().getText("Environnement"), envDetail);
        pJTabbedPane.add(Language.getInstance().getText("Attachements"), attachmentPanel);
                
        buttonPanel.setMaximumSize(new Dimension(t_x2, 40));
        contentPane.add(pJTabbedPane);
        contentPane.add(buttonPanel);
                
        rowSM = parametersTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    environment = null;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        giveAccessToIhm();
        
       
        if (oldEnvironment == null) {
            this.setTitle(Language.getInstance().getText("Ajouter_un_environnement"));
        } else {
            this.setTitle(Language.getInstance().getText("Modifier_un_environnement"));
        }
        if (tabPlugScriptFileFilter == null){
            initTabPlugScriptFileFilter();
        }
       
        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_MANAGEMENT_NEW_ENV_WINDOW,this);
        // Activation des plugins associes
        PluginsTools.activateAssociatedPlgs(UICompCst.DATA_MANAGEMENT_NEW_ENV_WINDOW);
        //this.setLocation(200,20);
        setSize(t_x, t_y);
        //this.setLocation((t_x2-t_x)/2,50);
        /*this.setLocationRelativeTo(this.getParent()); 
          this.pack();
          this.setVisible(true);**/
        centerScreen();
        
        
        
    } // Fin du constructeur AskNewEnvironment/1
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    public AskNewEnvironment() {
        this(null);
    } // Fin du constructeur AskNewEnvironment/0
    
    
    private void  initTabPlugScriptFileFilter(){
        if (SalomeTMFContext.getInstance().associatedScriptEngine.size()==0){
            deleteScriptButton.setEnabled(false);
            addScriptFileButton.setEnabled(false);
            fileChooser = null;
            return;
        }
        tabPlugScriptFileFilter = new ScriptFileFilter[SalomeTMFContext.getInstance().associatedScriptEngine.size()];
        Enumeration e = SalomeTMFContext.getInstance().associatedScriptEngine.keys();
        int i = 0;
        while (e.hasMoreElements()){
            Extension ScriptExt = (Extension) e.nextElement();
            String extID = ScriptExt.getId();
            String extList = (String) SalomeTMFContext.getInstance().associatedScriptEngine.get(ScriptExt);
            StringTokenizer st = new StringTokenizer(extList, ",");
            String[] tabExt = new String[st.countTokens()];
            //Util.log("Add extension " + extID + ", with filter :" + extList);
            int j = 0;
            while (st.hasMoreTokens()) {
                tabExt[j] = st.nextToken();
                j++;
            }
            tabPlugScriptFileFilter[i] = new ScriptFileFilter(extID, tabExt);
            i++;
        }
        int tabFilterSize = tabPlugScriptFileFilter.length;
        for (i = 0 ; i < tabFilterSize ; i++){
            fileChooser.addChoosableFileFilter(tabPlugScriptFileFilter[i]);
        }
        fileChooser.setAcceptAllFileFilterUsed(false);
    }
    
    
    /**
     *
     * @return
     */
    public Environment getEnvironment() {
        return environment;
    } // Fin de la methode getEnvironment/0
    
    /**
     *
     * @param oldEnvironment
     */
    private void initData(Environment oldEnvironment) {
        if (oldEnvironment != null) {
            environment = oldEnvironment;
            nameField.setText(oldEnvironment.getNameFromModel());
            if (oldEnvironment.getInitScriptFromModel() != null) {
                scriptLabel.setText(Language.getInstance().getText("Nom_du_script__") + oldEnvironment.getInitScriptFromModel().getNameFromModel());
            }
            script = oldEnvironment.getInitScriptFromModel();
            descriptionArea.setText(oldEnvironment.getDescriptionFromModel());
            ((MyTableModel)((TableSorter)parametersTable.getModel()).getTableModel()).clearTable();
            int i = 0;
            Set keysSet = oldEnvironment.getParametersHashTableFromModel().keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                Parameter param = (Parameter)iter.next();
                ((MyTableModel)((TableSorter)parametersTable.getModel()).getTableModel()).addValueAt(param.getNameFromModel(), i, 0);
                ((MyTableModel)((TableSorter)parametersTable.getModel()).getTableModel()).addValueAt(oldEnvironment.getParameterValueFromModel(param), i, 1);
                ((MyTableModel)((TableSorter)parametersTable.getModel()).getTableModel()).addValueAt(param.getDescriptionFromModel(), i, 2);
            }
            Collection col = oldEnvironment.getAttachmentMapFromModel().values();
            DataModel.initAttachmentTable(col);
        } else {
            environment = new Environment("","");
            ((MyTableModel)((TableSorter)parametersTable.getModel()).getTableModel()).clearTable();
            DataModel.getAttachmentTableModel().clearTable();
        }
        DataModel.setCurrentEnvironment(environment);
    } // Fin de la methode initData/1
    
    private void createAndQuit() {
        environment.updateInModel(nameField.getText().trim(), descriptionArea.getText());
        environment.addScriptInModel(script);
        //environment.getAttachmentMapFromModel().clear();
        /*Collection values = attachmentPanel.getAttachmentMap().values();
          for (Iterator iter = values.iterator(); iter.hasNext();) {
          environment.addAttachementInModel((Attachment)iter.next());
          }*/
        
        AskNewEnvironment.this.dispose();
    }
    
    public File getScriptFile() {
        return scriptFile;
    }
    
    private void giveAccessToIhm() {
        if (!Permission.canDeleteCamp()) {
            deleteScriptButton.setEnabled(false);
            deleteParameter.setEnabled(false);
        }
        if (!Permission.canCreateCamp()) {
            addParameterButton.setEnabled(false);
            addScriptFileButton.setEnabled(false);
            useDefinedParameterButton.setEnabled(false);
        }
        if (!Permission.canUpdateCamp()) {
            modifyParameterButton.setEnabled(false);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object source = e.getSource();
        if (source.equals(addScriptFileButton)){
            addScriptPerformed(e);
        } else  if (source.equals(deleteScriptButton)){
            deleteScriptPerformed(e);
        } else if (source.equals(addParameterButton)){
            addParameterPerformed(e);
        } else if (source.equals(useDefinedParameterButton)){
            useDefinedParameterPerformed(e);
        } else if (source.equals(modifyParameterButton)){
            modifyParameterPerformed(e);
        } else if (source.equals(deleteParameter)){
            deleteParameterPerformed(e);
        } else if (source.equals(okButton)){
            okButtonPerformed(e);
        }  else if (source.equals(cancelButton)){
            cancelPerformed(e);
        }
    }
    
    
    public void addScriptPerformed(ActionEvent e) {
        if (fileChooser == null){
            addScriptFileButton.setEnabled(false);
            deleteScriptButton.setEnabled(false);
            return;
        }
        fileChooser.setApproveButtonText(Language.getInstance().getText("Valider"));
        
        int returnVal = fileChooser.showOpenDialog(AskNewEnvironment.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            ScriptFileFilter filter = (ScriptFileFilter) fileChooser.getFileFilter();
            
            String scriptPlugIns = filter.getDescription();
            scriptFile = fileChooser.getSelectedFile();
            try {
                if (!scriptFile.exists()){
                    scriptFile.createNewFile();
                    newFile = true;
                }
            } catch (Exception ex){
                return;
            }
            scriptLabel.setText(Language.getInstance().getText("Nom_du_script__") + scriptFile.getAbsolutePath());
            script = new Script(scriptFile.getName(),"");
            script.setLocalisation(scriptFile.getAbsolutePath());
            script.setScriptExtensionInModel(scriptPlugIns);
            script.updatePlugArgInModel("");
            script.setTypeInModel(INIT_SCRIPT);
        }
    }
    
    
    public void deleteScriptPerformed(ActionEvent e) {
        if (script != null){
            int choice = -1;
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText("Supprimer_le_fichier_de_script") + " : "+ script.getNameFromModel() +" ?",
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.WARNING_MESSAGE,
                                                                options);
            if (choice != JOptionPane.YES_OPTION){
                return;
            }
            scriptLabel.setText(Language.getInstance().getText("Nom_du_script__"));
            script = null;
        }
    }
    
    
    public void addParameterPerformed(ActionEvent e) {
        AskNewParameter askNewParameter = new AskNewParameter(environment);
        if (askNewParameter.getParameter() != null && askNewParameter.getParameter().getNameFromModel() != null && askNewParameter.getParameter().getNameFromModel() != "" ) {
            
            try {
                // BdD
                DataModel.getCurrentProject().addParameterToDBAndModel(askNewParameter.getParameter());
                //askNewParameter.getParameter().add2DB();
                // IHM
                ArrayList data = new ArrayList();
                data.add(askNewParameter.getParameter().getNameFromModel());
                data.add(askNewParameter.getValue());
                data.add(askNewParameter.getParameter().getDescriptionFromModel());
                DataModel.getEnvironmentParameterTableModel().addRow(data);
                environment.addParameterValueInModel(askNewParameter.getParameter(), askNewParameter.getValue());
                //ProjectData.getCurrentProject().addParameter(askNewParameter.getParameter());
                data.remove(1);
                DataModel.getParameterTableModel().addRow(data);
                
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
            }
        }
    }
    
    public void useDefinedParameterPerformed(ActionEvent e) {
        UsingDefinedParameterView usingDefinedParameterView = new UsingDefinedParameterView(true, true);
        if (usingDefinedParameterView.getParameter() != null) {
            if (!environment.containsParameterInModel(usingDefinedParameterView.getParameter())) {
                ArrayList data = new ArrayList();
                data.add(usingDefinedParameterView.getParameter().getNameFromModel());
                data.add(usingDefinedParameterView.getValue());
                data.add(usingDefinedParameterView.getParameter().getDescriptionFromModel());
                DataModel.getEnvironmentParameterTableModel().addRow(data);
                environment.addParameterValueInModel(usingDefinedParameterView.getParameter(), usingDefinedParameterView.getValue());
            } else {
                JOptionPane.showMessageDialog(AskNewEnvironment.this,
                                              Language.getInstance().getText("Ce_parametre_est_deja_present_dans_votre_environnement_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    
    public void modifyParameterPerformed(ActionEvent e) {
        int selectedRow = parametersTable.getSelectedRow();
        if( selectedRow != -1) {
            //Parameter param = environment.getParameter((String)TestData.getEnvironmentParameterTableModel().getValueAt(selectedRow,0));
            Parameter param = environment.getParameterFromModel((String)sorter.getValueAt(selectedRow,0));
            AskNewParameter askNewParameter = new AskNewParameter(environment, param);
            if (askNewParameter.getParameter() != null) {
                
                sorter.setValueAt(askNewParameter.getParameter().getNameFromModel(), selectedRow, 0);
                sorter.setValueAt(askNewParameter.getValue(), selectedRow, 1);
                sorter.setValueAt(askNewParameter.getParameter().getDescriptionFromModel(), selectedRow, 2);
                //environment.setNewValue(askNewParameter.getParameter(), askNewParameter.getValue());
                environment.updateParamValueInModel(askNewParameter.getParameter(), askNewParameter.getValue());
            }
        }
    }
    
    public void deleteParameterPerformed(ActionEvent e) {
        int[] selectedRows = parametersTable.getSelectedRows();
        for (int i = selectedRows.length -1; i >=0; i--) {
            //environment.removeParameter((String)sorter.getValueAt(selectedRows[i],0));
            environment.deleteDefParameterInModel((String)sorter.getValueAt(selectedRows[i],0));
            DataModel.getEnvironmentParameterTableModel().removeData(sorter.modelIndex(selectedRows[i]));
        }
    }
    
    
    public void okButtonPerformed(ActionEvent e) {
        if (!nameField.getText().trim().equals("")) {
            if (!DataModel.getCurrentProject().containsEnvironment(nameField.getText().trim()) && !nameField.getText().trim().equals(ApiConstants.EMPTY_NAME) && !Environment.isInBase(DataModel.getCurrentProject(), nameField.getText().trim())) {
                createAndQuit();
            } else if (DataModel.getCurrentProject().getEnvironmentFromModel(nameField.getText().trim()) != null && DataModel.getCurrentProject().getEnvironmentFromModel(nameField.getText().trim()).equals(environment)){
                createAndQuit();
            } else {
                JOptionPane.showMessageDialog(AskNewEnvironment.this,
                                              Language.getInstance().getText("Ce_nom_d_environnement_existe_deja_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(AskNewEnvironment.this,
                                          Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_a_l_environnement_"),
                                          Language.getInstance().getText("Attention_"),
                                          JOptionPane.WARNING_MESSAGE);
        }
        
    }
    
    public void cancelPerformed(ActionEvent e) {
        environment = null;
        if (newFile)
            scriptFile.delete();
        AskNewEnvironment.this.dispose();
    }
    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        int nbOfSelectedRows = parametersTable.getSelectedRowCount();
        int selectedRow = parametersTable.getSelectedRow();
        if (selectedRow != -1) {
            if (nbOfSelectedRows != 1) {
                if (Permission.canDeleteCamp()) deleteParameter.setEnabled(true);
                modifyParameterButton.setEnabled(false);
            } else {
                if (Permission.canUpdateCamp()) modifyParameterButton.setEnabled(true);
                if (Permission.canDeleteCamp()) deleteParameter.setEnabled(true);
            }
        } else {
            deleteParameter.setEnabled(false);
            modifyParameterButton.setEnabled(false);
        }
        
    }
    
} // Fin de la classe AskNewEnvironment
