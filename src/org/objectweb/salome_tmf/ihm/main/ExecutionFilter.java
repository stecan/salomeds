/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.objectweb.salome_tmf.ihm.languages.Language;


public class ExecutionFilter extends JDialog {
    
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la vue
     */
    public ExecutionFilter() {
        
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        
        JLabel message = new JLabel(Language.getInstance().getText("Filtrer_par__"));
        
        //Create the radio buttons.
        JRadioButton environment = new JRadioButton(Language.getInstance().getText("Environnement"));
        environment.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                
                }
            });
        
        JRadioButton data = new JRadioButton(Language.getInstance().getText("Jeu_de_donnees"));
        data.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                
                }
            });
        
        JRadioButton date = new JRadioButton(Language.getInstance().getText("Date_de_creation"));
        date.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                
                }
            });
        
        //Group the radio buttons.
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(environment);
        buttonGroup.add(data);
        buttonGroup.add(date);
        
        
        JPanel radioButtonPanel = new JPanel();
        radioButtonPanel.setLayout(new BoxLayout(radioButtonPanel,BoxLayout.Y_AXIS));
        radioButtonPanel.add(environment);
        radioButtonPanel.add(data);
        radioButtonPanel.add(date);
        
        
        JPanel radioPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        radioPanel.add(message);
        radioPanel.add(radioButtonPanel);
        
        //les boutons
        JButton validate = new JButton(Language.getInstance().getText("Valider"));
        validate.setToolTipText(Language.getInstance().getText("Valider"));
        validate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ExecutionFilter.this.dispose();
                }
            });
        
        
        JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
        cancel.setToolTipText(Language.getInstance().getText("Annuler"));
        cancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ExecutionFilter.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttonPanel.add(validate);
        buttonPanel.add(cancel);
        
        JPanel view = new JPanel(new BorderLayout());
        view.add(radioPanel,BorderLayout.NORTH);
        view.add(buttonPanel,BorderLayout.SOUTH);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(view, BorderLayout.CENTER);
        //this.setLocation(500,400);
        this.setTitle(Language.getInstance().getText("Filtrage"));
        /**this.pack();
           this.setLocationRelativeTo(this.getParent()); 
           this.setVisible(true);*/
        centerScreen();
    } // Fin du constructeur ExecutionFilter/0
    
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
} // Fin de la classe ExecutionFilter
