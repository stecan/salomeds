/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;


import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataLoader;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.datawrapper.TestMethods;
import org.objectweb.salome_tmf.ihm.models.ScriptFileFilter;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui definit la fenetre permettant de creer une nouvelle execution
 * @author teaml039
 * @version : 0.1
 */
public class AskNewExecution extends JDialog implements ApiConstants, DataConstants, ActionListener, Runnable {
    
    /**
     * Label pour recuperer le nom de l'execution
     */
    JLabel executionName;
    
    /**
     * Champ pour recuperer le nom de l'execution
     */
    JTextField executionNameTextField;
    
    /**
     * Label de la date de creation
     */
    JLabel creationDate;
    
    /**
     * Modele de donnees pour la liste deroulante des jeux de donnees
     */
    private DefaultComboBoxModel comboModelForDataSet;
    
    /**
     * Liste deroulante des jeux de donnees
     */
    JComboBox dataSetComboBox;
    
    /**
     * Bouton pour entrer un nouveau jeu de donnees
     */
    JButton newDataSetButton;
    
    /**
     * Label du script d'initialisation
     */
    JLabel initScriptLabel;
    
    /**
     * Label du script de restitution
     */
    JLabel restitutionScriptLabel;
    
    /**
     * Bouton pour ajouter un script d'initialisation
     */
    JButton addInitScriptButton;
    
    /**
     * Bouton pour supprimer un script d'initialisation
     */
    JButton removeInitScriptButton;
    
    /**
     * Bouton pour ajouter un script de restitution
     */
    JButton addRestitutionScriptButton;
    
    /**
     * Bouton pour supprimer un script de restitution
     */
    JButton removeRestitutionScriptButton;
    
    /**
     * Modele de donnees de la liste deroulante des environnements
     */
    private DefaultComboBoxModel comboModelForEnvironment;
    
    /**
     * Liste deroulante des environnements
     */
    JComboBox environmentComboBox;
    
    /**
     * Bouton pour entrer un nouvel environnement
     */
    JButton newEnvironmentButton;
    
    JButton validationButton;
    JButton cancelButton;
    
    /**
     * Date de creation
     */
    Date time;
    
    /**
     * Nouvelle execution
     */
    Execution execution;
    
    AttachmentView attachmentPanel;
    
    Script initScript;
    
    Script restitutionScript;
    
    File initScriptFile;
    
    File restitutionScriptFile;
    
    String message;
    
    static ScriptFileFilter[] tabPlugScriptFileFilter = null;
    static JFileChooser fileChooser = new JFileChooser();
    /**************************************************************************/
    /**                                                 CONSTRUCTEURS                                                           ***/
    /**************************************************************************/
    boolean initNewFile = false;
    boolean restitutionNewFile = false;
    /**
     * Constructeur de la fenetre
     */
    public AskNewExecution(Execution exec) {
        
        super(SalomeTMFContext.getInstance().ptrFrame, true);
        
        int t_x = 1024 - 100;
        int t_y = 768/3*2 - 50;
        int t_x2 = 1024;
        int t_y2 = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height/3*2 -50 ;
            t_x2 = r.width;
            t_y2 = r.height ;
        } catch(Exception E){
                        
        }
                
        executionName = new JLabel(Language.getInstance().getText("Nom_de_l_execution__"));
        executionNameTextField = new JTextField(20);
        creationDate = new JLabel();
        comboModelForDataSet = new DefaultComboBoxModel();
        dataSetComboBox = new JComboBox(comboModelForDataSet);
        newDataSetButton = new JButton(Language.getInstance().getText("Nouveau"));
        initScriptLabel = new JLabel(Language.getInstance().getText("Script_d_initialisation__"));
        restitutionScriptLabel = new JLabel(Language.getInstance().getText("Script_de_restitution__"));
        addInitScriptButton = new JButton(Language.getInstance().getText("Ajouter"));
        removeInitScriptButton = new JButton(Language.getInstance().getText("Supprimer"));
        addRestitutionScriptButton = new JButton(Language.getInstance().getText("Ajouter"));
        removeRestitutionScriptButton = new JButton(Language.getInstance().getText("Supprimer"));
        comboModelForEnvironment = new DefaultComboBoxModel();
        newEnvironmentButton = new JButton(Language.getInstance().getText("Nouveau"));
        environmentComboBox = new JComboBox(comboModelForEnvironment);
        JPanel namePanel = new JPanel();
        namePanel.add(executionName);
        namePanel.add(executionNameTextField);
        
        if (exec != null) {
            time = exec.getCreationDateFromModel();
        } else {
            time = new Date(Calendar.getInstance().getTimeInMillis());
            creationDate.setText(Language.getInstance().getText("Date_de_creation__") + time);
        }
        // Jeu de donnees
        
        JPanel comboDataSetPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        comboDataSetPanel.add(dataSetComboBox);
        
        JPanel newDataSetButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        newDataSetButtonPanel.add(newDataSetButton);
        
        JPanel dataSetPanel = new JPanel();
        dataSetPanel.setLayout(new BoxLayout(dataSetPanel, BoxLayout.Y_AXIS));
        dataSetPanel.add(comboDataSetPanel);
        dataSetPanel.add(Box.createRigidArea(new Dimension(1,5)));
        dataSetPanel.add(newDataSetButtonPanel);
        dataSetPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Jeu_de_donnees")));
        
        
        // Scripts
        addInitScriptButton.addActionListener(this);
        addRestitutionScriptButton.addActionListener(this);
        removeInitScriptButton.addActionListener(this);
        removeRestitutionScriptButton.addActionListener(this);
        
        JPanel initLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        initLabelPanel.add(initScriptLabel);
        
        JPanel initButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        initButtonPanel.add(addInitScriptButton);
        initButtonPanel.add(removeInitScriptButton);
        
        JPanel restLabelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        restLabelPanel.add(restitutionScriptLabel);
        
        JPanel restButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        restButtonPanel.add(addRestitutionScriptButton);
        restButtonPanel.add(removeRestitutionScriptButton);
        
        JPanel allScriptPanel = new JPanel();
        allScriptPanel.setLayout(new BoxLayout(allScriptPanel, BoxLayout.Y_AXIS));
        allScriptPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Scripts")));
        allScriptPanel.add(initLabelPanel);
        allScriptPanel.add(initButtonPanel);
        allScriptPanel.add(Box.createRigidArea(new Dimension(1,10)));
        allScriptPanel.add(restLabelPanel);
        allScriptPanel.add(restButtonPanel);
        
        
        newEnvironmentButton.setToolTipText(Language.getInstance().getText("Nouvel_environnement"));
        newEnvironmentButton.addActionListener(this);
        
        newDataSetButton.setToolTipText(Language.getInstance().getText("Nouveau_jeu_de_donnees"));
        newDataSetButton.addActionListener(this);
        // Context
        JPanel comboPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        comboPanel.add(environmentComboBox);
        
        JPanel contextButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        contextButtonPanel.add(newEnvironmentButton);
        
        
        JPanel contextPanel = new JPanel();
        contextPanel.setLayout(new BoxLayout(contextPanel, BoxLayout.Y_AXIS));
        contextPanel.add(comboPanel);
        contextPanel.add(Box.createRigidArea(new Dimension(1,5)));
        contextPanel.add(contextButtonPanel);
        contextPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Environnement")));
        
        // validation
        validationButton = new JButton(Language.getInstance().getText("Valider"));
        validationButton.addActionListener(this);
        
        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        
        JPanel validationPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        validationPanel.add(validationButton);
        validationPanel.add(cancelButton);
       
        initData(exec);
        DataModel.setObservedExecution(execution);
        //attachmentPanel = new AttachmentView(DataModel.getApplet(), EXECUTION, EXECUTION, exec, SMALL_SIZE_FOR_ATTACH, null, null, null);
        attachmentPanel = new AttachmentView(DataModel.getApplet(), EXECUTION, EXECUTION, execution, SMALL_SIZE_FOR_ATTACH, this);
        attachmentPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Attachements")));
        
        /*
          JPanel allPanel = new JPanel();
          allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.Y_AXIS));
          allPanel.add(namePanel);
          allPanel.add(creationDate);
          allPanel.add(dataSetPanel);
          allPanel.add(contextPanel);
          allPanel.add(allScriptPanel);
          allPanel.add(attachmentPanel);
          allPanel.add(validationPanel);
        */
        JTabbedPane pJTabbedPane = new JTabbedPane();
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.setLayout(new BoxLayout(contentPaneFrame, BoxLayout.Y_AXIS));
        contentPaneFrame.setSize(t_x,t_y);
        JPanel allPanel = new JPanel();
        allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.Y_AXIS));
        allPanel.add(namePanel);
        allPanel.add(creationDate);
        allPanel.add(dataSetPanel);
        allPanel.add(contextPanel);
        allPanel.add(allScriptPanel);
        
        pJTabbedPane.add(Language.getInstance().getText("Execution"), allPanel);
        pJTabbedPane.add(Language.getInstance().getText("Attachements"), attachmentPanel);
        ChangeListener changeListener = new ChangeListener() {
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane sourceTabbedPane = (JTabbedPane) changeEvent.getSource();
                int index = sourceTabbedPane.getSelectedIndex();
                switch (index) {
                    case 0:
                        executionNameTextField.requestFocus();
                        break;
                    default:
                        break;
                }
            }
        };
        pJTabbedPane.addChangeListener(changeListener);
                
        contentPaneFrame.add(pJTabbedPane);
        contentPaneFrame.add(validationPanel);
                
        
        //initData(exec);
        giveAccessToIhmExecutionView();
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    execution = null;
                    DataModel.initAttachmentTable(DataModel.getCurrentCampaign().getAttachmentMapFromModel().values());
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        if (tabPlugScriptFileFilter == null){
            initTabPlugScriptFileFilter();
        }
        // this.setLocation((t_x2 - t_x)/2,20);
        //this.setLocation(300,20);
        if (exec != null){
            this.setTitle(Language.getInstance().getText("Modifier_l_execution"));
        } else {
            this.setTitle(Language.getInstance().getText("Nouvelle_execution"));
        }

        Thread t;
        t = new Thread(this);
        t.start();
        
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);**/
        centerScreen();

    } // Fin du constructeur AskNewExecution/0
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
    
    /**
     * Retourne l'execution creee
     * @return l'execution creee
     */
    public Execution getExecution() {
        return execution;
    } // Fin de la methode getExecution/0
    
    
    private void  initTabPlugScriptFileFilter(){
        if (SalomeTMFContext.getInstance().associatedScriptEngine.size()==0){
            removeInitScriptButton.setEnabled(false);
            addInitScriptButton.setEnabled(false);
            addRestitutionScriptButton.setEnabled(false);
            removeRestitutionScriptButton.setEnabled(false);
            fileChooser = null;
            return;
        }
        
        tabPlugScriptFileFilter = new ScriptFileFilter[SalomeTMFContext.getInstance().associatedScriptEngine.size()];
        Enumeration e = SalomeTMFContext.getInstance().associatedScriptEngine.keys();
        int i = 0;
        while (e.hasMoreElements()){
            Extension ScriptExt = (Extension) e.nextElement();
            String extID = ScriptExt.getId();
            String extList = (String) SalomeTMFContext.getInstance().associatedScriptEngine.get(ScriptExt);
            StringTokenizer st = new StringTokenizer(extList, ",");
            String[] tabExt = new String[st.countTokens()];
            //Util.debug("Add extension " + extID + ", with filter :" + extList);
            int j = 0;
            while (st.hasMoreTokens()) {
                tabExt[j] = st.nextToken();
                j++;
            }
            tabPlugScriptFileFilter[i] = new ScriptFileFilter(extID, tabExt);
            i++;
        }
        int tabFilterSize = tabPlugScriptFileFilter.length;
        for (i = 0 ; i < tabFilterSize ; i++){
            fileChooser.addChoosableFileFilter(tabPlugScriptFileFilter[i]);
        }
        fileChooser.setAcceptAllFileFilterUsed(false);
    }
    
    /**
     *
     *
     */
    public void initData(Execution exec) {
        DataModel.getAttachmentTableModel().clearTable();
        comboModelForDataSet.addElement(DataLoader.getEmptyDataSet());
        for (int i = 0; i < DataModel.getCurrentCampaign().getDataSetListFromModel().size(); i++) {
            comboModelForDataSet.addElement(DataModel.getCurrentCampaign().getDataSetListFromModel().get(i));
        }
        //comboModelForEnvironment.addElement(TestData.getEmptyEnvironment());
        for (int j = 0; j < DataModel.getCurrentProject().getEnvironmentListFromModel().size(); j++) {
            comboModelForEnvironment.addElement(DataModel.getCurrentProject().getEnvironmentListFromModel().get(j));
        }
        if (exec != null) {
            execution = exec;
            
            executionNameTextField.setText(exec.getNameFromModel());
            
            if (exec.getDataSetFromModel() != null) {
                comboModelForDataSet.setSelectedItem(exec.getDataSetFromModel());
            }
            
            comboModelForEnvironment.setSelectedItem(exec.getEnvironmentFromModel());
            
            if (exec.getExecutionResultListFromModel().size() != 0) {
                dataSetComboBox.setEnabled(false);
                newDataSetButton.setEnabled(false);
                environmentComboBox.setEnabled(false);
                newEnvironmentButton.setEnabled(false);
                addInitScriptButton.setEnabled(false);
                removeInitScriptButton.setEnabled(false);
                addRestitutionScriptButton.setEnabled(false);
                removeRestitutionScriptButton.setEnabled(false);
            }
            
            if (exec.getPreScriptFromModel() != null) {
                initScriptLabel.setText(Language.getInstance().getText("Script_d_initialisation__") + exec.getPreScriptFromModel().getNameFromModel());
                initScript = exec.getPreScriptFromModel();
                
            }
            
            if (exec.getPostScriptFromModel() != null) {
                restitutionScriptLabel.setText(Language.getInstance().getText("Script_de_restitution__") + exec.getPostScriptFromModel().getNameFromModel());
                restitutionScript = exec.getPostScriptFromModel();
            }
            
            Collection col = exec.getAttachmentMapFromModel().values();
            DataModel.initAttachmentTable(col);
            
        } else {
            execution = new Execution("","");
            int i = DataModel.getExecutionTableModel().getRowCount();
            Campaign camp = DataModel.getCurrentCampaign();
            while (camp.getExecutionListFromModel().contains(camp.getExecutionFromModel("Exec"+i))) {
                i++;
            }
            executionNameTextField.setText("Exec" + i);
        }
    } // Fin de la methode initData/0
    
    /**
     * Liste les parametres du jeux de donnees value par l'eNvironnement
     * @param dataSet
     * @param env
     * @return
     */
    private Vector parameterConflict(DataSet dataSet, Environment env) {
        //String result = "";
        Vector result = new Vector();
        if (dataSet != null && env != null) {
            Set keysSet = dataSet.getParametersHashMapFromModel().keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                String paramName = (String)iter.next();
                Parameter param = DataModel.getCurrentProject().getParameterFromModel(paramName);
                if (env.containsParameterInModel(param)) {
                    //result = result + " " + param.getNameFromModel() +"\n";
                    result.add(param);
                }
            }
        }
        return result;
    }
    /**
     * @return
     */
    public File getInitScriptFile() {
        return initScriptFile;
    }
    
    /**
     * @return
     */
    public File getRestitutionScriptFile() {
        return restitutionScriptFile;
    }
    
    private DataSet createDataSetFromCampaign(HashSet setOfParam, Environment env, ArrayList notValuedParamList) {
        DataSet dataSet = new DataSet("","");
        
        for (Iterator iter = setOfParam.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            if (env.getParameterFromModel(param.getNameFromModel()) != null) {
                //dataSet.addParameterValueInModel(param.getNameFromModel(), env.getParameterValueFromModel(param));
                if (Api.isDYNAMIC_VALUE_DATASET()) {
                    dataSet.addParameterValueInModel(param.getNameFromModel(), DataConstants.PARAM_VALUE_FROM_ENV);
                } else {
                    dataSet.addParameterValueInModel(param.getNameFromModel(), env.getParameterValueFromModel(param));
                }
            } else {
                dataSet.addParameterValueInModel(param.getNameFromModel(), "");
                notValuedParamList.add(param);
                message = message + "* " + param.getNameFromModel() + "\n";
            }
        }
        int i = 0;
        String name = "dataSet_" + i;
        while (DataModel.getCurrentCampaign().getDataSetFromModel(name) != null || DataSet.isInBase(DataModel.getCurrentCampaign(), name)) {
            i ++;
            name = "dataSet_"+i;
        }
        dataSet.updateInModel(name, Language.getInstance().getText("Jeu_de_donnees_genere_automatiquement"));
        //DataModel.getCurrentCampaign().addDataSetInModel(dataSet);
        ArrayList dataView = new ArrayList();
        dataView.add(dataSet.getNameFromModel());
        dataView.add(dataSet.getDescriptionFromModel());
        DataModel.getDataSetTableModel().addRow(dataView);
        int transNumber = -1;
        try {
            // BdD
            transNumber = Api.beginTransaction(11,ApiConstants.INSERT_DATA_SET);
            //dataSet.add2DB( DataModel.getCurrentCampaign());
            //dataSet.addToBddAndModel( DataModel.getCurrentCampaign());
            DataModel.getCurrentCampaign().addDataSetInDBAndModel(dataSet);
            Set keysSet = dataSet.getParametersHashMapFromModel().keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                String paramName = (String)iter.next();
                String value = dataSet.getParameterValueFromModel(paramName);
                if (value == null) value = "";
                //dataSet.addParamValue2DB(value, ProjectData.getCurrentProject().getParameter(paramName));
                //dataSet.addParamValueToBddAndModel(value, DataModel.getCurrentProject().getParameterFromModel(paramName));
                dataSet.addParamValueToDBAndModel(value, DataModel.getCurrentProject().getParameterFromModel(paramName));
            }
            Api.commitTrans(transNumber);
            
            return dataSet;
        } catch (Exception exception) {
            Api.forceRollBackTrans(transNumber);
            Tools.ihmExceptionView(exception);
        }
        
        return null;
    }
    
    
    
    private void createAndQuit() {
        if (environmentComboBox.getSelectedItem() != null) {
            //String conflicts = parameterConflict((DataSet)dataSetComboBox.getSelectedItem(), (Environment)environmentComboBox.getSelectedItem());
            DataSet ptrDataSet = (DataSet)dataSetComboBox.getSelectedItem();
            Environment ptrEnv = (Environment)environmentComboBox.getSelectedItem();
            Vector conflicts = parameterConflict(ptrDataSet, ptrEnv);
            int conflictsSize = conflicts.size();
            //if (conflicts.length() > 0) {
            if (conflictsSize > 0) {
                String reste="";
                
                for (int i = 0; i < conflictsSize ; i++){
                    Parameter ptrParam = (Parameter) conflicts.elementAt(i);
                    String valueDataSet = ptrDataSet.getParameterValueFromModel(ptrParam.getNameFromModel()).trim();
                    String valueEnv = ptrEnv.getParameterValueFromModel(ptrParam).trim();
                    if (valueDataSet.equals("") && !valueEnv.equals("")){
                        //Question
                        //ptrDataSet.addParameterValueInModel(ptrParam.getNameFromModel(),valueEnv);
                    } else if (!valueDataSet.equals(DataConstants.PARAM_VALUE_FROM_ENV)){
                        reste += ptrParam.getNameFromModel() + " ";
                    }
                        
                }
                
                if (!reste.equals("")){
                    JOptionPane.showMessageDialog(AskNewExecution.this,
                                                  Language.getInstance().getText("Lors_de_l_execution_les_valeurs_des_parametres_") + reste + Language.getInstance().getText("_seront_celles_du_jeu_de_donnees"),
                                                  Language.getInstance().getText("Attention_"),
                                                  JOptionPane.WARNING_MESSAGE);
                }
            }
            DataModel.getCurrentCampaign().updateExecutionInModel(execution); //A verifier pourquoi ?????
            //execution.setCampagne(DataModel.getCurrentCampaign());
            execution.updateNameInModel(executionNameTextField.getText().trim());
            execution.setCreationDateInModel(time);
            
            HashSet setOfParam = new HashSet();
            // Recuperation de tous les parametres de la campagne
            for (int i = 0; i < DataModel.getCurrentCampaign().getTestListFromModel().size(); i++) {
                Test test = (Test)DataModel.getCurrentCampaign().getTestListFromModel().get(i);
                for (int j = 0; j < test.getParameterListFromModel().size(); j++) {
                    setOfParam.add(test.getParameterListFromModel().get(j));
                }
            }
            ArrayList notValuedParamList = new ArrayList();
            message = "";
            if (ptrDataSet.getNameFromModel().equals(ApiConstants.EMPTY_NAME)) {
                if (!setOfParam.isEmpty()) {
                    execution.updateDatasetInModel(createDataSetFromCampaign(setOfParam, ptrEnv, notValuedParamList));
                } else {
                    execution.updateDatasetInModel(ptrDataSet);
                }
            } else {
                message = TestMethods.notValuedParamListCreation(setOfParam, ptrEnv, ptrDataSet, notValuedParamList);
                execution.updateDatasetInModel(ptrDataSet);
            }
            if (notValuedParamList.size() > 0) {
                JOptionPane.showMessageDialog(AskNewExecution.this,
                                              Language.getInstance().getText("Attention_Les_parametres_suivants_") + " " + message + " " + Language.getInstance().getText("ne_sont_pas_values"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.INFORMATION_MESSAGE);
            }
            
            execution.updateEnvInModel(ptrEnv);
            //execution.setInitScript(initScript);
            //execution.setPostScript(restitutionScript);
            execution.addPreScriptInModel(initScript);
            execution.addPostScriptInModel(restitutionScript);
            //execution.getAttachmentMapFromModel().clear();
            /*Collection values = attachmentPanel.getAttachmentMap().values();
              for (Iterator iter = values.iterator(); iter.hasNext();) {
              execution.addAttachementInModel((Attachment)iter.next());
              }*/
            DataModel.initAttachmentTable(DataModel.getCurrentCampaign().getAttachmentMapFromModel().values());
            AskNewExecution.this.dispose();
        } else {
            JOptionPane.showMessageDialog(AskNewExecution.this,
                                          Language.getInstance().getText("Une_execution_doit_obligatoirement_avoir_un_environnement_"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void giveAccessToIhmExecutionView() {
        if (!Permission.canDeleteCamp()) {
            removeRestitutionScriptButton.setEnabled(false);
            removeInitScriptButton.setEnabled(false);
        }
        if (!Permission.canCreateCamp()) {
            addInitScriptButton.setEnabled(false);
            addRestitutionScriptButton.setEnabled(false);
        }
        if (!Permission.canUpdateCamp()) {
            
        }
        
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        Object source = e.getSource();
        if (source.equals(addInitScriptButton)){
            addInitScriptPerformed(e);
        } else if (source.equals(addRestitutionScriptButton)){
            addRestitutionScriptPerformed(e);
        } else if (source.equals(removeInitScriptButton)){
            removeInitScriptPerformed(e);
        }  else if (source.equals(removeRestitutionScriptButton)){
            removeRestitutionScriptPerformed(e);
        } else if (source.equals(newEnvironmentButton)){
            newEnvironmentPerformed(e);
        }  else if (source.equals(newDataSetButton)){
            newDataSetPerformed(e);
        }  else if (source.equals(validationButton)){
            validationPerformed(e);
        }  else if (source.equals(cancelButton)){
            cancelPerformed(e);
        }
    }
    
    public void  addInitScriptPerformed(ActionEvent e) {
        if (fileChooser == null){
            removeInitScriptButton.setEnabled(false);
            addInitScriptButton.setEnabled(false);
            return;
        }
        fileChooser.setApproveButtonText(Language.getInstance().getText("Valider"));
        int returnVal = fileChooser.showOpenDialog(AskNewExecution.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            ScriptFileFilter filter = (ScriptFileFilter) fileChooser.getFileFilter();
            String scriptPlugIns = filter.getDescription();
            initScriptFile = fileChooser.getSelectedFile();
            try {
                if (!initScriptFile.exists()){
                    initScriptFile.createNewFile();
                    initNewFile = true;
                }
            } catch (Exception ex){
                return;
            }
            initScript = new Script(initScriptFile.getName(), "");
            initScript.setLocalisation((initScriptFile.getAbsolutePath()));
            initScript.setScriptExtensionInModel(scriptPlugIns);
            initScript.updatePlugArgInModel("");
            initScript.setTypeInModel(PRE_SCRIPT);
            initScriptLabel.setText(Language.getInstance().getText("Script_d_initialisation__") + initScriptFile.getName());
        }
    }
    
    public void addRestitutionScriptPerformed(ActionEvent e) {
        if (fileChooser == null){
            addRestitutionScriptButton.setEnabled(false);
            removeRestitutionScriptButton.setEnabled(false);
            return;
        }
        fileChooser.setApproveButtonText(Language.getInstance().getText("Valider"));
        int returnVal = fileChooser.showOpenDialog(AskNewExecution.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            ScriptFileFilter filter = (ScriptFileFilter) fileChooser.getFileFilter();
            String scriptPlugIns = filter.getDescription();
            restitutionScriptFile = fileChooser.getSelectedFile();
            try {
                if (!restitutionScriptFile.exists()){
                    restitutionScriptFile.createNewFile();
                    restitutionNewFile = true;
                }
            } catch (Exception ex){
                return;
            }
            restitutionScript = new Script(restitutionScriptFile.getName(), "");
            restitutionScript.setLocalisation(restitutionScriptFile.getAbsolutePath());
            restitutionScript.setScriptExtensionInModel(scriptPlugIns);
            restitutionScript.updatePlugArgInModel("");
            restitutionScript.setTypeInModel(POST_SCRIPT);
            restitutionScriptLabel.setText(Language.getInstance().getText("Script_de_restitution__") + restitutionScriptFile.getName());
        }
    }
    
    public void removeInitScriptPerformed(ActionEvent e) {
        if (initScript != null){
            int choice = -1;
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText("Supprimer_le_fichier_de_script") + " : "+ initScript.getNameFromModel() +" ?",
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.WARNING_MESSAGE,
                                                                options);
            if (choice != JOptionPane.YES_OPTION){
                return;
            }
            initScriptLabel.setText(Language.getInstance().getText("Script_d_initialisation__"));
            initScript = null;
        }
        
    }
    
    public void removeRestitutionScriptPerformed(ActionEvent e) {
        if (restitutionScript != null){
            int choice = -1;
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText("Supprimer_le_fichier_de_script") + " : "+ restitutionScript.getNameFromModel() +" ?",
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.WARNING_MESSAGE,
                                                                options);
            if (choice != JOptionPane.YES_OPTION){
                return;
            }
            restitutionScriptLabel.setText(Language.getInstance().getText("Script_de_restitution__"));
            restitutionScript = null;
        }
        
    }
    
    public void newEnvironmentPerformed(ActionEvent e) {
        AskNewEnvironment askNewEnvironment = new AskNewEnvironment();
        if (askNewEnvironment.getEnvironment() != null) {
            int transNumber = -1;
            try {
                Environment env = askNewEnvironment.getEnvironment();
                
                // BdD
                transNumber = Api.beginTransaction(11,ApiConstants.INSERT_ENVIRONMENT);
                //env.add2DB();
                DataModel.getCurrentProject().addEnvironmentInDBAndModel(env);
                Set keysSet = askNewEnvironment.getEnvironment().getParametersHashTableFromModel().keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    Parameter param = (Parameter)iter.next();
                    //env.addParamValue2DB(param);
                    env.addParamValueInDBAndModel(param, env.getParameterValueFromModel(param));
                }
                if (askNewEnvironment.getScriptFile() != null) {
                    //env.addScript2BddAndModel(env.getInitScriptFromModel(),askNewEnvironment.getScriptFile());
                    env.addScriptInDBAndModel(env.getInitScriptFromModel(),askNewEnvironment.getScriptFile());
                }
                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                comboModelForEnvironment.addElement(askNewEnvironment.getEnvironment());
                ArrayList data = new ArrayList();
                data.add(askNewEnvironment.getEnvironment().getNameFromModel());
                String initScriptName = "";
                if (askNewEnvironment.getEnvironment().getInitScriptFromModel() != null) {
                    initScriptName = askNewEnvironment.getEnvironment().getInitScriptFromModel().getNameFromModel();
                }
                data.add(initScriptName);
                data.add(askNewEnvironment.getEnvironment().getParametersHashTableFromModel());
                data.add(askNewEnvironment.getEnvironment().getDescriptionFromModel());
                DataModel.getEnvironmentTableModel().addRow(data);
                //ProjectData.getCurrentProject().addEnvironment(askNewEnvironment.getEnvironment());
                environmentComboBox.setSelectedItem(askNewEnvironment.getEnvironment());
                
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
        }
    }
    
    public void newDataSetPerformed(ActionEvent e) {
        AskNewDataSet askNewDataSet = new AskNewDataSet();
        if (askNewDataSet.getDataSet() != null) {
            int transNumber = -1;
            try {
                DataSet dataSet = askNewDataSet.getDataSet();
                // BdD
                transNumber = Api.beginTransaction(11,ApiConstants.INSERT_ENVIRONMENT);
                //dataSet.add2DB(DataModel.getCurrentCampaign());
                //dataSet.addToBddAndModel(DataModel.getCurrentCampaign());
                DataModel.getCurrentCampaign().addDataSetInDBAndModel(dataSet);
                Set keysSet = askNewDataSet.getDataSet().getParametersHashMapFromModel().keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    String paramName = (String)iter.next();
                    String value = askNewDataSet.getDataSet().getParameterValueFromModel(paramName);
                    if (value == null) value = "";
                    //dataSet.addParamValue2DB(value, ProjectData.getCurrentProject().getParameter(paramName));
                    dataSet.addParamValueToDBAndModel(value, DataModel.getCurrentProject().getParameterFromModel(paramName));
                }
                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                ArrayList data = new ArrayList();
                data.add(dataSet.getNameFromModel());
                data.add(dataSet.getDescriptionFromModel());
                DataModel.getDataSetTableModel().addRow(data);
                //DataModel.getCurrentCampaign().addDataSet(dataSet);
                comboModelForDataSet.addElement(dataSet);
                dataSetComboBox.setSelectedItem(dataSet);
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                Tools.ihmExceptionView(exception);
            }
        }
    }
    
    
    
    public void validationPerformed(ActionEvent e) {
        if (executionNameTextField.getText() != null && !executionNameTextField.getText().trim().equals("")) {
            if (!DataModel.getCurrentCampaign().containsExecutionInModel(executionNameTextField.getText().trim()) && !Execution.isInBase(DataModel.getCurrentCampaign(), executionNameTextField.getText().trim())) {
                createAndQuit();
            } else if (DataModel.getCurrentCampaign().getExecutionFromModel(executionNameTextField.getText().trim()) != null && DataModel.getCurrentCampaign().getExecutionFromModel(executionNameTextField.getText().trim()).equals(execution)){
                createAndQuit();
            } else {
                JOptionPane.showMessageDialog(AskNewExecution.this,
                                              Language.getInstance().getText("Ce_nom_d_execution_existe_deja_pour_cette_campagne_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(AskNewExecution.this,
                                          Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_a_l_execution_"),
                                          Language.getInstance().getText("Attention_"),
                                          JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void cancelPerformed(ActionEvent e) {
        DataModel.initAttachmentTable(DataModel.getCurrentCampaign().getAttachmentMapFromModel().values());
        execution = null;
        if (initNewFile)
            initScriptFile.delete();
        
        if (restitutionNewFile)
            restitutionScriptFile.delete();
        
        AskNewExecution.this.dispose();
    }
    
    public void focusToExecutionName() {
        executionNameTextField.requestFocus();
    }

    @Override
    public void run() {
        try {
            Thread.sleep(300);
            executionNameTextField.requestFocus();
            executionNameTextField.selectAll();
            Thread.sleep(500);
            executionNameTextField.requestFocus();
        } catch (InterruptedException ex) {
            // do nothing!
        }
    }
} // Fin de la classe AskNewExecution
