package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

public class AskNewBugDefaultPanel extends JPanel implements ActionListener {
    /**
     * Label de l'utilisateur
     */
    JLabel userNameLabel;
        
    /**
     * Label pour la plateforme
     */
    JLabel platformLabel;
        
    /**
     * Label pour l'OS
     */
    JLabel osLabel;
        
    /**
     * Label pour les priorites
     */
    JLabel priorityLabel;
        
    /**
     * Label pour l'importance
     */
    JLabel severityLabel;

    /**
     * Label pour le produit
     */
    JLabel productLabel;
        
    /**
     * Label pour le destinataire
     */
    JLabel assignedToLabel;

    /**
     * Label pour l'url
     */
    JLabel urlLabel;

    /**
     * Label pour le resume
     */
    JLabel summaryLabel;

    /**
     * Label pour la description
     */
    JLabel descriptionLabel;
        
    /**
     * Label pour le status
     */
    JLabel statusLabel;
        
    /**
     * Label pour la reproductibilite
     */
    JLabel reproducibilityLabel;
    /**
     * Label pour la resolution
     */
    JLabel resolutionLabel;
        
    /**
     * Modele pour la comboBox de la plateforme 
     */
    DefaultComboBoxModel assignedToComboBoxModel;
        
    /**
     * Modele pour la comboBox de la plateforme 
     */
    DefaultComboBoxModel platformComboBoxModel;
        
    /**
     * Modele pour la comboBox des priorites
     */
    DefaultComboBoxModel priorityComboBoxModel;
        
    /**
     * Modele pour la comboBox de l'OS
     */
    DefaultComboBoxModel osComboBoxModel;
        
    /**
     * Modele pour la comboBox de l'importance
     */
    DefaultComboBoxModel severityComboBoxModel;
        
    /**
     * Modele pour la comboBox de status
     */
    DefaultComboBoxModel statusComboBoxModel;
        
    /**
     * Modele pour la comboBox de reproducibility
     */
    DefaultComboBoxModel reproducibilityComboBoxModel;
        
    /**
     * Modele pour la comboBox de resolution
     */
    DefaultComboBoxModel resolutionComboBoxModel;
        
    /**
     * Bouton d'envoi
     */
    JButton commitButton;
        
    /**
     * Bouton de modification
     */
    JButton modifyButton;
        
        
    /**
     * Bouton de Visualisation
     */
    JButton viewButton;
        
        
    /**
     * Bouton d'annulation
     */
    JButton cancelButton;
        
    /**
     * Champ texte pour le destinataire 
     */
    JTextField assignedToTextField;     
        
    /**
     * Champ texte pour l'url
     */
    JTextField urlTextField;
        
    /**
     * Champ texte pour le resume
     */
    JTextField summaryTextField;
        
    /**
     * Champ texte pour la description 
     */
    JTextArea descriptionArea;
        
    /**
     * La liste des plateformes 
     */
    JComboBox platformComboBox;

    /**
     * La liste des OS
     */
    JComboBox osComboBox;

    /**
     * La liste des priorites
     */
    JComboBox priorityComboBox;

    /**
     * La liste des importances
     */
    JComboBox severityComboBox;
        
    /**
     * La liste des destinataires
     */
    JComboBox assignedToComboBox;
        
    /**
     * La liste des status
     */
    JComboBox statusToComboBox;
        
    /**
     * La liste des reproductibilites
     */
    JComboBox reproducibilityComboBox;
        
    /**
     * La liste des resolutions
     */
    JComboBox resolutionComboBox;
        
    /**
     * Le resultat d'execution concerne
     */
    ExecutionResult execResult;
        
    /**
     * L'environnement concerne
     */
    Environment environment;
        
    /**
     * Test courant
     */
    Test currentTest;
        
    Execution exec;
        
    ExecutionTestResult executionTestResult;
        
    BugTracker bugTracker;
        
    /* Add for modify functionnality */
    Attachment theBug;
    String strEnvironement;
        
    boolean autofill = true;
    IBugJDialog pAskNewBug ;
    public AskNewBugDefaultPanel(IBugJDialog pAskNewBug ,BugTracker bugTrack, boolean _autofill, String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes) {
        super (new BorderLayout());
        // Init des composants de la fenetre
        autofill = _autofill;
        this.bugTracker = bugTrack;
        this.pAskNewBug =  pAskNewBug;
                        
        execResult = DataModel.getObservedExecutionResult();
        executionTestResult = DataModel.getCurrentExecutionTestResult();
                        
        if (DataModel.getObservedExecution() == null) {
            environment = null;
        }
        else {
            environment = DataModel.getObservedExecution().getEnvironmentFromModel();
        }
                
        if (DataModel.getCurrentExecutionTestResult() == null) {
            currentTest = null;
        }
        else {
            currentTest = DataModel.getCurrentExecutionTestResult().getTestFromModel();
        }
                        
        exec = DataModel.getObservedExecution();
                        
        initComponent();
                        
        if (environment != null){
            productLabel.setText(productLabel.getText() + environment.getNameFromModel());
        } else {
            productLabel.setText(Language.getInstance().getText("Produit__Salome"));
        }
                        
        userNameLabel.setText(userNameLabel.getText() + " : " + DataModel.getCurrentUser().getEmailFromModel());
                        
        // OS, priorities, plateforms and severities
        Vector osNames = bugTracker.getBugOSList();
        if (osNames != null) { 
            for (int i=0 ; i<osNames.size() ; i++) {
                osComboBoxModel.addElement(osNames.elementAt(i));
            }
        }
                
        Vector prioritiesNames = bugTracker.getBugPriorityList();
        if (prioritiesNames != null) {
            for (int i=0 ; i<prioritiesNames.size() ; i++) {
                priorityComboBoxModel.addElement(prioritiesNames.elementAt(i));
            }
        }
                
        Vector platformsNames = bugTracker.getBugPlateformList();
        if (platformsNames != null) {
            for (int i=0 ; i<platformsNames.size() ; i++) {
                platformComboBoxModel.addElement(platformsNames.elementAt(i));
            }
        }
                
        Vector severityNames = bugTracker.getBugSeverityList();
        if (severityNames != null) {
            for (int i=0 ; i<severityNames.size() ; i++) {
                severityComboBoxModel.addElement(severityNames.elementAt(i));
            }
        }

        Vector assignedToVector = bugTracker.getBugTrackerAllUsers();
        if (assignedToVector != null) {
            for (int i = 0; i < assignedToVector.size(); i++) {
                assignedToComboBoxModel.addElement(assignedToVector.get(i));
            }
        }
                        
        platformComboBox = new JComboBox(platformComboBoxModel);
        platformComboBox.setEditable(bugTrack.isEditablePlateForme());
        osComboBox = new JComboBox(osComboBoxModel);
        osComboBox.setEditable(bugTrack.isEditableOS());
                        
        priorityComboBox = new JComboBox(priorityComboBoxModel);
        severityComboBox = new JComboBox(severityComboBoxModel);
        assignedToComboBox = new JComboBox(assignedToComboBoxModel);
                        
                        
        assignedToTextField = new JTextField(50);
        urlTextField = new JTextField(50);
        summaryTextField = new JTextField(50);
                        
        Test testInExec = PluginsTools.getTestForCurrentTestExecResult();
        String bugDescription = "";
                        
        // Convert HTML description to text for a test
        String testHtmlDesc = testInExec.getDescriptionFromModel();
        String testTextDesc = "";
                        
        EditorKit kit = new HTMLEditorKit();
        Document doc = kit.createDefaultDocument();
        // The Document class does not yet handle charset's properly.
        doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
        try {
            // Create a reader on the HTML content.
            Reader rd = new StringReader(testHtmlDesc);
            // Parse the HTML.
            kit.read(rd, doc, 0);
            testTextDesc = doc.getText(0, doc.getLength());
        } catch (Exception e) {
            Util.err(e);
        }
                        
        if (autofill){
            bugDescription = bugDescription +
                Language.getInstance().getText("__Campagne_") + PluginsTools.getCurrentCampExecution().getCampagneFromModel().getNameFromModel()+ "\n"+
                Language.getInstance().getText("__Execution_") + PluginsTools.getCurrentCampExecution().getNameFromModel()+ "\n"+
                Language.getInstance().getText("__Test_famille_") + testInExec.getTestListFromModel().getFamilyFromModel().getNameFromModel()+
                Language.getInstance().getText("_suite_") + testInExec.getTestListFromModel().getNameFromModel()+
                Language.getInstance().getText("_test_") + testInExec.getNameFromModel()+"]" + "\n" +
                Language.getInstance().getText("__Description_du_test_") + testTextDesc + "\n";
                                
            DataSet dataSet = PluginsTools.getCurrentCampExecution().getDataSetFromModel();
            String dataSetStr = "\n- " + Language.getInstance().getText("Jeu_de_donnees") + " : " + dataSet.getNameFromModel() +  "\n";
            Set dataSetKeysSet = dataSet.getParametersHashMapFromModel().keySet();
            Environment env = PluginsTools.getCurrentCampExecution().getEnvironmentFromModel();
            if (!dataSetKeysSet.isEmpty()){
                for (Iterator iter = dataSetKeysSet.iterator(); iter.hasNext();) {
                    String paramName = ((String)iter.next()).trim();
                    String value = dataSet.getParameterValueFromModel(paramName).trim();
                    if (value != null && !value.equals("")){
                        if (value.startsWith(DataConstants.PARAM_VALUE_FROM_ENV)){
                            if (env != null){
                                value = env.getParameterValue(paramName);
                            }
                        }
                        dataSetStr += "\t" + paramName + " = " + value + "\n";
                    }
                                                
                } 
                bugDescription += dataSetStr +"\n";
            }
        }
        if (actionName != null) {
            bugDescription += Language.getInstance().getText("_Action") + actionName + "\n" +
                Language.getInstance().getText("_Action_desc") + actionDesc + "\n" +
                Language.getInstance().getText("_Action_res_attendu") + actionAwatedRes + "\n" +
                Language.getInstance().getText("_Action_res_effectif") + actionEffectiveRes;
        }
                        
        String bugAdditionalDesc = bugTracker.getAdditionalBugDesc();
        if ((bugAdditionalDesc != null)&&(bugAdditionalDesc != "")) {
            bugDescription += bugAdditionalDesc;
        }
                        
        descriptionArea = new JTextArea(bugDescription);
                        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setPreferredSize(new Dimension(500,200));
                        
        commitButton = new JButton(Language.getInstance().getText("Envoyer"));
        commitButton.addActionListener(this);
                        
        modifyButton =  new JButton(Language.getInstance().getText("Modifier"));
        viewButton =  new JButton(Language.getInstance().getText("Visualiser"));
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);

                        
        JPanel firstLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLinePanel.add(productLabel);
        //firstLinePanel.add(Box.createRigidArea(new Dimension(350,1)));
        JPanel firstLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLinePanel2.add(userNameLabel);
                        
        JPanel secondLinePanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLinePanel1.add(platformLabel);
        secondLinePanel1.add(platformComboBox);
        //secondLinePanel.add(Box.createRigidArea(new Dimension(180,0)));
        JPanel secondLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLinePanel2.add(osLabel);
        secondLinePanel2.add(osComboBox);
                        
        JPanel thirdLinePanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        thirdLinePanel1.add(priorityLabel);
        thirdLinePanel1.add(priorityComboBox);
        //thirdLinePanel.add(Box.createRigidArea(new Dimension(180,0)));
        JPanel thirdLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        thirdLinePanel2.add(severityLabel);
        thirdLinePanel2.add(severityComboBox);
                        
        JPanel gridPanel = new JPanel(new GridLayout(3,2));
        gridPanel.add(firstLinePanel);
        gridPanel.add(firstLinePanel2);
        gridPanel.add(secondLinePanel1);
        gridPanel.add(secondLinePanel2);
        gridPanel.add(thirdLinePanel2);
        gridPanel.add(thirdLinePanel1); 
                        
        JPanel fourthLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fourthLinePanel.add(assignedToLabel);
        fourthLinePanel.add(assignedToComboBox);
                        
        JPanel fourthLinePanel2 = null;
                                        
        JPanel fifthLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fifthLinePanel.add(urlLabel);
        fifthLinePanel.add(urlTextField);
                        
        JPanel sixthLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sixthLinePanel.add(summaryLabel);
        sixthLinePanel.add(summaryTextField);
                        
        JPanel seventhLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        seventhLinePanel.add(descriptionLabel);
        seventhLinePanel.add(descriptionScrollPane);
                        
        JPanel heightLinePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        heightLinePanel.add(commitButton);
        heightLinePanel.add(cancelButton);
                        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        //mainPanel.add(firstLinePanel);
                        
        //mainPanel.add(secondLinePanel);
        //mainPanel.add(thirdLinePanel);
        mainPanel.add(gridPanel);
        mainPanel.add(fourthLinePanel);
                        
        // Adding bug reproducibility if the bugtracker allows it
        Vector reproducibilityNames = null;
        if (bugTracker.isUsesBugReproducibily()) {
            reproducibilityLabel = new JLabel(Language.getInstance().getText("reproducibility"));
            reproducibilityComboBoxModel = new DefaultComboBoxModel();
                                
            reproducibilityNames = bugTracker.getBugTrackerReproductibilityList();
            if (reproducibilityNames != null) {
                for (int i = 0; i < reproducibilityNames.size(); i++) {
                    reproducibilityComboBoxModel.addElement(reproducibilityNames.get(i));
                }
                reproducibilityComboBox = new JComboBox(reproducibilityComboBoxModel);
            }
                                
            fourthLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
            fourthLinePanel2.add(reproducibilityLabel);
            fourthLinePanel2.add(reproducibilityComboBox);
            mainPanel.add(fourthLinePanel2);
        }
                        
        mainPanel.add(fifthLinePanel);
        mainPanel.add(sixthLinePanel);
        mainPanel.add(seventhLinePanel);
        mainPanel.add(heightLinePanel);
                        
        add(mainPanel, BorderLayout.CENTER);

    } // Fin du constructeur
                
                
    void initComponent(){
        userNameLabel = new JLabel(Language.getInstance().getText("Utilisateurs"));
        platformLabel = new JLabel(Language.getInstance().getText("Plateforme__"));
                        
        productLabel = new JLabel(Language.getInstance().getText("Bug_Environnement"));
        osLabel = new JLabel(Language.getInstance().getText("OS_"));
        priorityLabel = new JLabel(Language.getInstance().getText("Priorite__"));
        severityLabel = new JLabel(Language.getInstance().getText("Importance__"));
        assignedToLabel = new JLabel(Language.getInstance().getText("Destinataire__"));
        urlLabel = new JLabel(Language.getInstance().getText("URL_____________"));
        summaryLabel = new JLabel(Language.getInstance().getText("Resume_______"));
        descriptionLabel = new JLabel(Language.getInstance().getText("Description__"));
        statusLabel =  new JLabel(Language.getInstance().getText("Statut") + " : ");
                        
        platformComboBoxModel = new DefaultComboBoxModel();
        priorityComboBoxModel = new DefaultComboBoxModel();
        osComboBoxModel = new DefaultComboBoxModel();
        severityComboBoxModel = new DefaultComboBoxModel();
        assignedToComboBoxModel = new DefaultComboBoxModel();
        statusComboBoxModel = new DefaultComboBoxModel();
                        
    }
                
    /**
     * Constructeur de la fenetre pour montrer un bug 
     */
    public AskNewBugDefaultPanel(IBugJDialog pAskNewBug, BugTracker bugTrack, boolean editable, Attachment _theBug, String environement, String user, String plateforme, String os, 
                                 String priority, String severity, String status,String reproducibility,String resolution, String recipient, String url, String resume, String description) {
                                
                        
        // Init des composants de la fenetre
        super (new BorderLayout());
                        
        this.bugTracker = bugTrack;
        this.pAskNewBug =  pAskNewBug;
        theBug = _theBug;
        strEnvironement = environement;
        initComponent();
        
        productLabel.setText(productLabel.getText()+ environement);
        userNameLabel.setText(userNameLabel.getText() + " : " + user);
                
        // OS, priorities, plateforms and severities
        Vector osNames = bugTracker.getBugOSList();
        Object selectedItem = null;
        if (osNames != null && editable) { 
            for (int i=0 ; i<osNames.size() ; i++) {
                if (osNames.elementAt(i).equals(os)){
                    selectedItem = osNames.elementAt(i);
                }
                osComboBoxModel.addElement(osNames.elementAt(i));
            }
        } else {
            osComboBoxModel.addElement(os);
            selectedItem = os;
        }
                
        if(selectedItem != null){
            osComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                
        Vector prioritiesNames = bugTracker.getBugPriorityList();
        if (prioritiesNames != null && editable) {
            for (int i=0 ; i<prioritiesNames.size() ; i++) {
                if (prioritiesNames.elementAt(i).equals(priority)){
                    selectedItem = prioritiesNames.elementAt(i);
                }
                priorityComboBoxModel.addElement(prioritiesNames.elementAt(i));
            }
        } else {
            priorityComboBoxModel.addElement(priority);
            selectedItem = priority;
        }
                
        if(selectedItem != null){
            priorityComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                
                
        Vector platformsNames = bugTracker.getBugPlateformList();
        if (platformsNames != null && editable) {
            for (int i=0 ; i<platformsNames.size() ; i++) {
                if (platformsNames.elementAt(i).equals(plateforme)){
                    selectedItem = platformsNames.elementAt(i);
                }
                platformComboBoxModel.addElement(platformsNames.elementAt(i));
            }
        } else {
            platformComboBoxModel.addElement(plateforme);
            selectedItem = plateforme;
        }
        if(selectedItem != null){
            platformComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                
                
        Vector severityNames = bugTracker.getBugSeverityList();
        if (severityNames != null && editable) {
            for (int i=0 ; i<severityNames.size() ; i++) {
                if (severityNames.elementAt(i).equals(severity)){
                    selectedItem = severityNames.elementAt(i);
                }
                severityComboBoxModel.addElement(severityNames.elementAt(i));
            }
        } else {
            severityComboBoxModel.addElement(severity);
            selectedItem = severity;
        }
        if(selectedItem != null){
            severityComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                
        Vector assignedToVector = bugTracker.getBugTrackerAllUsers();
        if (assignedToVector != null && editable) {
            for (int i = 0; i < assignedToVector.size(); i++) {
                if (assignedToVector.get(i).equals(recipient)){
                    selectedItem = assignedToVector.get(i);
                }
                assignedToComboBoxModel.addElement(assignedToVector.get(i));
            }
        } else {
            assignedToComboBoxModel.addElement(recipient);
            selectedItem = recipient;
        }
        if(selectedItem != null){
            assignedToComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                        
                
        Vector statusToVector = bugTracker.getBugTrackerStatusList();
        if (statusToVector != null && editable) {
            for (int i = 0; i < statusToVector.size(); i++) {
                if (statusToVector.get(i).equals(status)){
                    selectedItem = statusToVector.get(i);
                }
                statusComboBoxModel.addElement(statusToVector.get(i));
            }
        } else {
            statusComboBoxModel.addElement(status);
            selectedItem = status;
        }
        if(selectedItem != null){
            statusComboBoxModel.setSelectedItem(selectedItem);
        }
        selectedItem = null;
                
        platformComboBox = new JComboBox(platformComboBoxModel);
        platformComboBox.setEditable(bugTrack.isEditablePlateForme() && editable);
        osComboBox = new JComboBox(osComboBoxModel);
        osComboBox.setEditable(bugTrack.isEditableOS() && editable);
                        
        priorityComboBox = new JComboBox(priorityComboBoxModel);
        severityComboBox = new JComboBox(severityComboBoxModel);
        assignedToComboBox = new JComboBox(assignedToComboBoxModel);
        statusToComboBox = new JComboBox(statusComboBoxModel);
                        
        assignedToTextField = new JTextField(50);
        assignedToTextField.setEditable(editable);
                        
        urlTextField = new JTextField(50);
        urlTextField.setText(url);
        urlTextField.setEditable(editable);
                        
        summaryTextField = new JTextField(50);
        summaryTextField.setText(resume);
        summaryTextField.setEditable(editable);
                        
        descriptionArea = new JTextArea(description);
        descriptionArea.setEditable(editable);
                        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setPreferredSize(new Dimension(500,200));
                        
        commitButton = new JButton(Language.getInstance().getText("Modifier"));
        //commitButton.addActionListener(this);
                        
        modifyButton =  new JButton(Language.getInstance().getText("Modifier"));
        modifyButton.addActionListener(this);
        viewButton =  new JButton(Language.getInstance().getText("Visualiser") + " " + bugTrack.getBugTrackerName());
        viewButton.addActionListener(this);
                        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);

                        
        JPanel firstLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLinePanel.add(productLabel);
        //firstLinePanel.add(Box.createRigidArea(new Dimension(350,1)));
        JPanel firstLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        firstLinePanel2.add(userNameLabel);
                        
        JPanel secondLinePanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLinePanel1.add(platformLabel);
        secondLinePanel1.add(platformComboBox);
        //secondLinePanel.add(Box.createRigidArea(new Dimension(180,0)));
        JPanel secondLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        secondLinePanel2.add(osLabel);
        secondLinePanel2.add(osComboBox);
                        
        JPanel thirdLinePanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        thirdLinePanel1.add(priorityLabel);
        thirdLinePanel1.add(priorityComboBox);
        //thirdLinePanel.add(Box.createRigidArea(new Dimension(180,0)));
        JPanel thirdLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        thirdLinePanel2.add(severityLabel);
        thirdLinePanel2.add(severityComboBox);
                        
                        
        JPanel fourthLinePanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fourthLinePanel1.add(statusLabel);
        fourthLinePanel1.add(statusToComboBox);
                        
        JPanel fourthLinePanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fourthLinePanel2.add(assignedToLabel);
        fourthLinePanel2.add(assignedToComboBox);
                        
        JPanel gridPanel = new JPanel(new GridLayout(4,2));
        gridPanel.add(firstLinePanel);
        gridPanel.add(firstLinePanel2);
        gridPanel.add(secondLinePanel1);
        gridPanel.add(secondLinePanel2);
        gridPanel.add(thirdLinePanel2);
        gridPanel.add(thirdLinePanel1);                 
        gridPanel.add(fourthLinePanel1);
        gridPanel.add(fourthLinePanel2);
                        
        // Adding bug reproducibility if the bug tracker allows it
        JPanel gridPanel2 = null;
        JPanel reproducibilityPanel = null;
        Vector reproducibilityNames = null;
        if (bugTracker.isUsesBugReproducibily()) {
            gridPanel2 = new JPanel(new GridLayout(1,2));
            reproducibilityLabel = new JLabel(Language.getInstance().getText("reproducibility"));
            reproducibilityComboBoxModel = new DefaultComboBoxModel();
                                
            reproducibilityNames = bugTracker.getBugTrackerReproductibilityList();
            if (reproducibilityNames != null) {
                for (int i = 0; i < reproducibilityNames.size(); i++) {
                    if (reproducibilityNames.elementAt(i).equals(reproducibility)) {
                        selectedItem = reproducibilityNames.elementAt(i);
                    }
                    reproducibilityComboBoxModel.addElement(reproducibilityNames.get(i));
                }
            } else {
                reproducibilityComboBoxModel.addElement(reproducibility);
                selectedItem = reproducibility;
            }

            if (selectedItem != null) {
                reproducibilityComboBoxModel.setSelectedItem(selectedItem);
            }
                                
            selectedItem = null;
            reproducibilityComboBox = new JComboBox(reproducibilityComboBoxModel);
            reproducibilityPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            reproducibilityPanel.add(reproducibilityLabel);
            reproducibilityPanel.add(reproducibilityComboBox);
            gridPanel2.add(reproducibilityPanel);
        }
                        
        // Adding bug resolution if the begtrcker allows it
        JPanel resolutionPanel = null;
        Vector resolutionNames = null;
        if (bugTracker.isUsesBugResolution()) {
            resolutionLabel = new JLabel(Language.getInstance().getText("resolution"));
            resolutionComboBoxModel = new DefaultComboBoxModel();
                        
            resolutionNames = bugTracker.getBugTrackerResolutionList();
            if (resolutionNames != null) {
                for (int i = 0; i < resolutionNames.size(); i++) {
                    if (resolutionNames.elementAt(i).equals(resolution)) {
                        selectedItem = resolutionNames.elementAt(i);
                    }
                    resolutionComboBoxModel.addElement(resolutionNames.get(i));
                }
            } else {
                resolutionComboBoxModel.addElement(resolution);
                selectedItem = resolution;
            }
                                
            if (selectedItem != null) {
                resolutionComboBoxModel.setSelectedItem(selectedItem);
            }
                                
            selectedItem = null;
            resolutionComboBox = new JComboBox(resolutionComboBoxModel);
            resolutionPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            resolutionPanel.add(resolutionLabel);
            resolutionPanel.add(resolutionComboBox);
            if (gridPanel2 == null) {
                gridPanel2 = resolutionPanel;
            } else {
                gridPanel2.add(resolutionPanel);
            }
        }
                        
        JPanel fifthLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fifthLinePanel.add(urlLabel);
        fifthLinePanel.add(urlTextField);
                        
        JPanel sixthLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        sixthLinePanel.add(summaryLabel);
        sixthLinePanel.add(summaryTextField);
                        
        JPanel seventhLinePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        seventhLinePanel.add(descriptionLabel);
        seventhLinePanel.add(descriptionScrollPane);
                        
        JPanel heightLinePanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
                        
                        
        heightLinePanel.add(cancelButton);
        if (editable) {
            heightLinePanel.add(modifyButton);
        }
        heightLinePanel.add(viewButton);
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
        //mainPanel.add(firstLinePanel);
                        
        //mainPanel.add(secondLinePanel);
        //mainPanel.add(thirdLinePanel);
        mainPanel.add(gridPanel);
        if (gridPanel2 != null) {
            mainPanel.add(gridPanel2);
        }
        mainPanel.add(fifthLinePanel);
        mainPanel.add(sixthLinePanel);
        mainPanel.add(seventhLinePanel);
        mainPanel.add(heightLinePanel);
                        
                        
        add(mainPanel, BorderLayout.CENTER);

    } // Fin du constructeur
                
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(cancelButton)){
            cancelPerformed(e);
        } else if (e.getSource().equals(commitButton)){
            commitPerformed(e);
        }else if (e.getSource().equals(modifyButton)){
            modifyPerformed(e);
        }else if (e.getSource().equals(viewButton)){
            viewPerformed(e);
        }
    }
                
    void viewPerformed(ActionEvent e){
        pAskNewBug.onViewPerformed();
        bugTracker.showBugInBugTracker(theBug);
    }
    void modifyPerformed(ActionEvent e){
                        
        String summary = summaryTextField.getText().trim(); 
        if ((summary == null)||(summary.equals(""))) {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("Must_fill_summary"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }
                        
        String long_desc = descriptionArea.getText();
        String assigned_to = (String) assignedToComboBox.getSelectedItem();
        String url_attach = urlTextField.getText().trim();
        String bug_severity = (String) severityComboBox.getSelectedItem();
        String bug_satus = (String) statusToComboBox.getSelectedItem();
        String short_desc = summaryTextField.getText();
        String bug_OS = ((String) osComboBox.getSelectedItem()).trim();
        String bug_priority = (String) priorityComboBox.getSelectedItem();
        String bug_platform = ((String) platformComboBox.getSelectedItem()).trim();
        String bug_env = strEnvironement;
        String bug_reproducibility = null;
        String bug_resolution = null;
        if ((bugTracker.isUsesBugReproducibily())&&(bugTracker.getBugTrackerReproductibilityList() != null)) {
            bug_reproducibility = ((String) reproducibilityComboBox.getSelectedItem()).trim();
        }
        if ((bugTracker.isUsesBugResolution())&&(bugTracker.getBugTrackerResolutionList()!= null)) {
            bug_resolution = ((String) resolutionComboBox.getSelectedItem()).trim();
        }
                        
        //TODO
        try {
            bugTracker.modifyBug(theBug, long_desc, assigned_to, url_attach, bug_severity, bug_satus, short_desc, bug_OS, 
                                 bug_priority, bug_platform, bug_env,bug_reproducibility,bug_resolution);
            pAskNewBug.onModifyPerformed();
        } catch (Exception E) {
            pAskNewBug.onModifyPerformed();
            Tools.ihmExceptionView(E);
            Util.err(E);
        }
                        
    }
                
    void cancelPerformed(ActionEvent e){
        pAskNewBug.onCancelPerformed();
    }
                
    void commitPerformed(ActionEvent e) {
        String summary = summaryTextField.getText().trim(); 
        if ((summary == null)||(summary.equals(""))) {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("Must_fill_summary"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            return;
        }
                        
        Attachment bugURL = null;
        // Adding the bug
        try {
            String id = "["
                + currentTest.getTestListFromModel().getFamilyFromModel()
                .getNameFromModel() + "."
                + currentTest.getTestListFromModel().getNameFromModel()
                + "." + currentTest + " | "
                + DataModel.getCurrentCampaign().getNameFromModel()
                + "." + exec + "]";
            String long_desc = descriptionArea.getText();
            long_desc = id + "\n" + long_desc;
                                
            String bug_reproducibility = null;
            if ((bugTracker.isUsesBugReproducibily())&&(bugTracker.getBugTrackerReproductibilityList() != null)) {
                bug_reproducibility = ((String) reproducibilityComboBox.getSelectedItem()).trim();
            }
                                
            //TODO
            bugURL = bugTracker.addBug(long_desc,(String) assignedToComboBox.getSelectedItem(),urlTextField.getText().trim(),
                                       (String) severityComboBox.getSelectedItem(),summaryTextField.getText(),(String) osComboBox.getSelectedItem(),
                                       (String) priorityComboBox.getSelectedItem(),(String) platformComboBox.getSelectedItem(), bug_reproducibility,
                                       DataModel.getObservedExecution(), DataModel.getObservedExecutionResult(), currentTest );
        } catch (Exception E) {
            Util.err(E);
        }
                        
        pAskNewBug.onCommitPerformed(bugURL);
    }
}
