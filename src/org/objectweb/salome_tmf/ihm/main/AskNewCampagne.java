/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
/**
 * Classe qui definit la fenetre permettant de creer une nouvelle campagne
 * @author teaml039
 * @version 0.1
 */
public class AskNewCampagne extends JDialog {
    
    /**
     * Le champ du nom de campagne
     */
    JTextField campagneNameField;
    
    /**
     * Le champ du concepteur
     */
    JComboBox conceptorNameField;
    
    /**
     * La description
     */
    JTextArea descriptionArea;
    
    /**
     * La campagne creee
     */
    Campaign campagne;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre.
     * @param textToBePrompt chaine correspondant a la demande.
     */
    public AskNewCampagne(Component parent, String textToBePrompt) {
        
        //Pour bloquer le focus sur la boite de dialogue
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        campagneNameField = new JTextField(10);
        conceptorNameField = new JComboBox();
        descriptionArea = new JTextArea(10, 20);
        JPanel page = new JPanel();
        
        JLabel testNameLabel = new JLabel(Language.getInstance().getText("Nom_de_la_campagne__"));
        JLabel conceptorNameLabel = new JLabel(Language.getInstance().getText("Nom_du_concepteur_"));
        
        
        JPanel giveName = new JPanel();
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(testNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(campagneNameField);
        
        JPanel giveConceptorName = new JPanel();
        giveConceptorName.setLayout(new BoxLayout(giveConceptorName, BoxLayout.X_AXIS));
        giveConceptorName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveConceptorName.add(Box.createHorizontalGlue());
        giveConceptorName.add(conceptorNameLabel);
        giveConceptorName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveConceptorName.add(conceptorNameField);
        
        //descriptionArea.setPreferredSize(new Dimension(100,150));
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(giveName);
        
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(descriptionScrollPane);
        
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                
                    if (campagneNameField.getText() != null && !campagneNameField.getText().trim().equals("")) {
                        campagne = new Campaign(campagneNameField.getText().trim(), descriptionArea.getText());
                        campagne.setConceptorInModel(DataModel.getCurrentUser().getLoginFromModel());
                        AskNewCampagne.this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(AskNewCampagne.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_a_la_campagne_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        
        campagneNameField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AskNewCampagne.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);
        
        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        this.setTitle(textToBePrompt);
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);*/
        centerScreen();
    } // Fin du constructeur AskNewCampagne/2
    
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Retourne la campagne
     * @return la campagne
     */
    public Campaign getCampagne() {
        return campagne;
    } // Fin de la methode getCampagne/0
    
} // Fin de la classe AskNewCampagne
