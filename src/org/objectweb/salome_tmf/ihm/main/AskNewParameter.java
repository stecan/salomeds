/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.models.UsingParameterMouseListener;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer les
 * informations pour un nouveau parametre, un nouveau groupe ou une nouvelle
 * famille.
 * @author teaml039
 * @version 0.1
 */
public class AskNewParameter extends JDialog {
    
    /**
     * le champ pour entrer le nom du parametre
     */
    JTextField nameField;
    
    /**
     * le champ pour entrer la valeur du parametre
     */
    JTextField valueField;
    
    /**
     * le champ description
     */
    JTextArea descriptionArea;
    
    /**
     * Un parametre
     */
    Parameter parameter;
    
    /**
     * Valeur du parametre
     */
    String value;
    
    /**
     * Environnment courant
     */
    Environment environment;
    
    JTable parameterTable;
    TableSorter  sorter;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre permettant de demander deux informations.
     * @param param un parametre
     */
    public AskNewParameter(Environment env, Parameter param) {
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        
        nameField = new JTextField(15);
        valueField = new JTextField(15);
        descriptionArea = new JTextArea(10,20);
        parameterTable = new JTable();
        JLabel promptFirst = new JLabel(Language.getInstance().getText("Nom_du_parametre__"));
        
        
        JPanel onlyTextPanel = new JPanel();
        onlyTextPanel.setLayout(new BoxLayout(onlyTextPanel,BoxLayout.X_AXIS));
        onlyTextPanel.add(Box.createRigidArea(new Dimension(0,10)));
        onlyTextPanel.add(promptFirst);
        onlyTextPanel.add(nameField);
        
        JLabel valueLabel = new JLabel(Language.getInstance().getText("Valeur_du_parametre__"));
        
        JPanel valuePanel = new JPanel();
        valuePanel.setLayout(new BoxLayout(valuePanel,BoxLayout.X_AXIS));
        valuePanel.add(Box.createRigidArea(new Dimension(0,10)));
        valuePanel.add(valueLabel);
        valuePanel.add(valueField);
        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(onlyTextPanel);
        textPanel.add(Box.createRigidArea(new Dimension(10,10)));
        if (env != null) {
            textPanel.add(valuePanel);
            textPanel.add(Box.createRigidArea(new Dimension(10,10)));
        }
        textPanel.add(descriptionScrollPane);
        
        sorter = new TableSorter(DataModel.getParameterTableModel());
        parameterTable.setModel(sorter);
        sorter.setTableHeader(parameterTable.getTableHeader());
        
        
        parameterTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        
        JScrollPane parametersScrollPane = new JScrollPane(parameterTable);
        parametersScrollPane.setPreferredSize(new Dimension(450,150));
        parametersScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Parametres_existants")));
        
        if (env == null) {
            parameterTable.addMouseListener(new UsingParameterMouseListener(true, true));
        } else {
            parameterTable.addMouseListener(new UsingParameterMouseListener(false, false));
        }
        
        
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!nameField.getText().trim().equals("")) {
                        if (!DataModel.getCurrentProject().containsParameterInModel(nameField.getText().trim()) && !Parameter.isInBase(DataModel.getCurrentProject(), nameField.getText().trim())) {
                            parameter.updateNameInModel(nameField.getText().trim());
                            value = valueField.getText();
                            parameter.updateDescriptionInModel(descriptionArea.getText());
                            AskNewParameter.this.dispose();
                        } else if (DataModel.getCurrentProject().getParameterFromModel(nameField.getText().trim()).equals(parameter)) {
                            parameter.updateNameInModel(nameField.getText().trim());
                            value = valueField.getText();
                            parameter.updateDescriptionInModel(descriptionArea.getText());
                            AskNewParameter.this.dispose();
                        } else {
                            JOptionPane.showMessageDialog(AskNewParameter.this,
                                                          Language.getInstance().getText("Ce_nom_de_parametre_existe_deja_"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(AskNewParameter.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_au_parametre_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    parameter = null;
                    AskNewParameter.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);
        
        textPanel.add(buttonPanel);
        textPanel.add(Box.createRigidArea(new Dimension(40,10)));
        
        
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(textPanel, BorderLayout.NORTH);
        contentPaneFrame.add(parametersScrollPane, BorderLayout.CENTER);
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    parameter = null;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        initData(env, param);
        
        this.setTitle(Language.getInstance().getText("Nouveau_parametre"));
        //this.setLocation(400,250);
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);*/
        centerScreen();
    } // Fin du constructeur AskNewParameter/5
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    /**
     * Le nom et la description sont vides.
     * @param type le type de donnee a recuperer
     * @param title le titre de la fenetre
     * @param question le label proposant le texte a entrer
     */
    public AskNewParameter(Environment env) {
        this(env, null);
    } // Fin du constructeur AskNewParameter/3
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Retourne le parametre cree
     * @return le parametre cree
     */
    public Parameter getParameter() {
        return parameter;
    } // Fin de la methode getParameter/0
    
    /**
     *
     * @param param
     */
    private void initData(Environment env, Parameter param) {
        if (env != null && param != null) {
            valueField.setText(env.getParameterValueFromModel(param));
        }
        if (param != null) {
            parameter = param;
            nameField.setText(param.getNameFromModel());
            nameField.setEnabled(false);
            descriptionArea.setText(param.getDescriptionFromModel());
        } else {
            parameter = new Parameter("","");
        }
        environment = env;
    } // Fin de la methode initData/1
    
    /**
     *
     * @return
     */
    public String getValue() {
        return value;
    }
    
    public Environment getEnvironment() {
        return environment;
    }
} // Fin de la classe AskNewParameter
