package org.objectweb.salome_tmf.ihm.main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTree;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;

public class PopupMenuFactory {
    static JMenuItem importToCampMenuItem = null;
    static JMenuItem delTestMenuItem = null;
    static JMenuItem delCampMenuItem = null;
    static JMenuItem synchronizaMenuItem = null;
    static JMenuItem reloadCampMenuItem = null;
    static JMenuItem viewMenuItem = null;
    static JMenuItem assignMenuItem;

    static JMenuItem copieMenuItem = null;
    static JMenuItem collerMenuItem = null;

    static JMenuItem copieCampMenuItem = null;
    static JMenuItem collerCampMenuItem = null;

    static SimpleData toCopie = null;
    static SimpleData toCampCopie = null;

    /**
     * Methode qui cree un menu PopUp pour le clic droit de la souris sur
     * l'arbre des suites
     * @return un menu PopUp
     */
    public static JPopupMenu createTreePopUpForList() {
        //Creation du menu PopUp de la table
        JPopupMenu popup = new JPopupMenu();

        JMenuItem ExpandAllMenuItem = new JMenuItem(Language.getInstance().getText("Expand_All"));
        ExpandAllMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    JTree root = pDynamicTree.getTree();
                    pDynamicTree.expandTree(root, true);
                }
            });
        popup.add(ExpandAllMenuItem);
        
        JMenuItem ExpandMenuItem = new JMenuItem(Language.getInstance().getText("Expand"));
        ExpandMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    JTree root = pDynamicTree.getTree();
                    pDynamicTree.expandAll(root, root.getSelectionPath(), true);
                }
            });
        popup.add(ExpandMenuItem);
        
        JMenuItem CollapseAllMenuItem = new JMenuItem(Language.getInstance().getText("Collapse_All"));
        CollapseAllMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    JTree root = pDynamicTree.getTree();
                    pDynamicTree.expandTree(root, false);
                }
            });
        popup.add(CollapseAllMenuItem);
        
        JMenuItem CollapseMenuItem = new JMenuItem(Language.getInstance().getText("Collapse"));
        CollapseMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    JTree root = pDynamicTree.getTree();
                    pDynamicTree.expandAll(root, root.getSelectionPath(), false);
                }
            });
        popup.add(CollapseMenuItem);
        
        JMenuItem newFamilyMenuItem = new JMenuItem(Language.getInstance().getText("Nouvelle_famille"));
        newFamilyMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.addNewFamily();
                }
            });
        popup.add(newFamilyMenuItem);

        JMenuItem newListMenuItem = new JMenuItem(Language.getInstance().getText("Nouvelle_suite"));
        newListMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.addNewTestList();
                }
            });
        popup.add(newListMenuItem);

        JMenuItem newTestMenuItem = new JMenuItem(Language.getInstance().getText("Nouveau_test"));
        newTestMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.addNewTest();
                }
            });
        popup.add(newTestMenuItem);

        JMenuItem reloadtMenuItem = new JMenuItem(Language.getInstance().getText("Rafraichir"));
        reloadtMenuItem.setEnabled(true);
        reloadtMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.reloadData();

                }
            });
        //importToCampMenuItem = importMenuItem;
        popup.add(reloadtMenuItem);


        delTestMenuItem = new JMenuItem(Language.getInstance().getText("Supprimer"));
        delTestMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.deleteInTestTree();
                }
            });
        popup.add(delTestMenuItem);

        // V?rification des droits de l'utilisateur
        if (!Permission.canDeleteTest()) {
            delTestMenuItem.setEnabled(false);
        }
        if (!Permission.canCreateTest()) {
            newFamilyMenuItem.setEnabled(false);
            newTestMenuItem.setEnabled(false);
            newListMenuItem.setEnabled(false);
        }


        JSeparator sep = new JSeparator();
        popup.add(sep);

        copieMenuItem = new JMenuItem(Language.getInstance().getText("Copier"));
        copieMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //DataModel.deleteInTestTree();
                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    Object fromTree = pDynamicTree.getSelectedNode().getUserObject();
                    if (fromTree instanceof SimpleData) {
                        toCopie = (SimpleData) fromTree;
                        collerMenuItem.setEnabled(true);
                    } else {
                        toCopie = null;
                        collerMenuItem.setEnabled(false);
                    }
                }
            });
        popup.add(copieMenuItem);


        collerMenuItem = new JMenuItem(Language.getInstance().getText("Coller"));
        collerMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // DataModel.deleteInTestTree();
                    //e.getSource().setCursor(new Cursor(Cursor.WAIT_CURSOR));

                    DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
                    SimpleData from = null;
                    Object fromTree = pDynamicTree.getSelectedNode().getUserObject();
                    if (fromTree instanceof SimpleData) {
                        from = (SimpleData) fromTree;
                    }

                    collerMenuItem.setEnabled(false);
                    int transcode = -1;
                    try {
                        pDynamicTree.setCursorWait();
                        transcode = Api.beginTransaction(101, ApiConstants.INSERT_TEST);
                        DataModel.makeCopie(toCopie, from);
                        Api.commitTrans(transcode);
                        pDynamicTree.setCursorNormal();
                    } catch (Exception ex) {
                        Api.forceRollBackTrans(transcode);
                        pDynamicTree.setCursorNormal();
                    }
                    toCopie = null;
                }
            });
        collerMenuItem.setEnabled(false);
        popup.add(collerMenuItem);
        return popup;
    } // Fin de la m?thode createTreePopUpForList






    /**
     * Methode qui cree un menu PopUp pour le clic droit de la souris sur l'arbre des campagnes
     * @return un menu PopUp
     */
    public static JPopupMenu createTreePopUpForCampagne() {

        //Creation du menu PopUp de la table
        JPopupMenu popup = new JPopupMenu();

        JMenuItem newMenuItem = new JMenuItem(Language.getInstance().getText("Nouvelle_campagne"));
        newMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.addNewCampagne();
                }
            });
        popup.add(newMenuItem);

        assignMenuItem = new JMenuItem(Language.getInstance().getText("Assign_to"));
        assignMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new AskChooseAssignedUser();
                }
            });
        popup.add(assignMenuItem);

        copieCampMenuItem = new JMenuItem(Language.getInstance().getText("Copier") + " " +Language.getInstance().getText("Campagne"));
        copieCampMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getCampaignDynamicTree();
                    Object fromTree = pDynamicTree.getSelectedNode().getUserObject();
                    if (fromTree instanceof Campaign) {
                        toCampCopie = (SimpleData) fromTree;
                        collerCampMenuItem.setEnabled(true);
                    } else {
                        toCampCopie = null;
                        collerCampMenuItem.setEnabled(false);
                    }

                }
            });
        popup.add(copieCampMenuItem);


        collerCampMenuItem = new JMenuItem(Language.getInstance().getText("Coller")+ " " +Language.getInstance().getText("Campagne"));
        collerCampMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // DataModel.deleteInTestTree();
                    //e.getSource().setCursor(new Cursor(Cursor.WAIT_CURSOR));

                    DynamicTree pDynamicTree = SalomeTMFPanels.getCampaignDynamicTree();
                    SimpleData fromCamp = null;
                    Object fromTree = pDynamicTree.getSelectedNode().getUserObject();
                    if (fromTree instanceof Campaign) {
                        fromCamp = (SimpleData) fromTree;
                    }

                    collerCampMenuItem.setEnabled(false);
                    int transcode = -1;
                    try {
                        pDynamicTree.setCursorWait();
                        transcode = Api.beginTransaction(101, ApiConstants.INSERT_CAMPAIGN);
                        DataModel.makeCopie(toCampCopie, fromCamp);

                        Api.commitTrans(transcode);
                        pDynamicTree.setCursorNormal();
                    } catch (Exception ex) {
                        Api.forceRollBackTrans(transcode);
                        pDynamicTree.setCursorNormal();
                    }
                    toCampCopie = null;

                }
            });
        collerCampMenuItem.setEnabled(false);
        popup.add(collerCampMenuItem);



        reloadCampMenuItem = new JMenuItem(Language.getInstance().getText("Rafraichir") + " " + Language.getInstance().getText("Campagne"));
        reloadCampMenuItem.setEnabled(true);
        reloadCampMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.reloadCampaign(false);

                }
            });
        //importToCampMenuItem = importMenuItem;
        popup.add(reloadCampMenuItem);


        synchronizaMenuItem = new JMenuItem(Language.getInstance().getText("Synchroniser_testplan"));
        synchronizaMenuItem.setEnabled(true);
        synchronizaMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.synchronizeCampaign(DataModel.getCurrentCampaign());

                }
            });
        //importToCampMenuItem = importMenuItem;
        popup.add(synchronizaMenuItem);

        JMenuItem importMenuItem = new JMenuItem(Language.getInstance().getText("Importer") + " " + Language.getInstance().getText("Tests"));
        importMenuItem.setEnabled(false);
        importMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.importTestsToCampaign();
                }
            });
        importToCampMenuItem = importMenuItem;
        popup.add(importMenuItem);

        delCampMenuItem = new JMenuItem(Language.getInstance().getText("Supprimer"));
        delCampMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DataModel.deleteInCampaignTree();
                }
            });
        popup.add(delCampMenuItem);


        JSeparator jSeparator = new JSeparator();
        popup.add(jSeparator);

        viewMenuItem = new JMenuItem(Language.getInstance().getText("Visualiser"));
        viewMenuItem.setEnabled(false);
        viewMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicTree pDynamicTree = SalomeTMFPanels.getCampaignDynamicTree();
                    try {
                        DataModel.view((SimpleData)pDynamicTree.getSelectedNode().getUserObject());
                    } catch(Exception ex){
                        Util.err(ex);
                    }
                }
            });
        popup.add(viewMenuItem);

        // V?rification des droits
        if (!Permission.canDeleteCamp()) {
            delCampMenuItem.setEnabled(false);

        }
        if (!Permission.canCreateCamp()) {
            newMenuItem.setEnabled(false);
            importMenuItem.setEnabled(false);
        }
        if (!Permission.canUpdateCamp()) {
            assignMenuItem.setEnabled(false);
        }

        return popup;
    } // Fin de la m?thode createTreePopUpForCampagne/0

    public static JMenuItem getImportToCampMenuItem() {
        return importToCampMenuItem;
    }

    public static JMenuItem getviewMenuItem() {
        return viewMenuItem;
    }

    public static JMenuItem getDelTestMenuItem() {
        return delTestMenuItem;
    }

    public static JMenuItem getDelCampMenuItem() {
        return delCampMenuItem;
    }

    public static JMenuItem getAssignCampMenuItem() {
        return assignMenuItem;
    }

    public static void setRootCampAction(boolean isRoot) {
        if (isRoot){
            importToCampMenuItem.setEnabled(false);
            delCampMenuItem.setEnabled(false);
            assignMenuItem.setEnabled(false);
            synchronizaMenuItem.setEnabled(false);
            copieCampMenuItem.setEnabled(false);
            reloadCampMenuItem.setEnabled(false);
        } else {
            importToCampMenuItem.setEnabled(true);
            delCampMenuItem.setEnabled(true);
            assignMenuItem.setEnabled(true);
            synchronizaMenuItem.setEnabled(true);
            copieCampMenuItem.setEnabled(true);
            reloadCampMenuItem.setEnabled(true);
        }
    }


}
