/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;

/**
 * Classe qui represente la vue pour ajouter/editer une action

 */
public class AskNewAction extends JDialog implements DataConstants, IHMConstants {

    //20100106 - D\ufffdbut modification Forge ORTF v1.0.0
    final static int ADD_ACTION = 0;
    final static int MODIFY_ACTION_LOCK_TEST = 1;
    final static int MODIFY_ACTION = 2;
    //20100106 - Fin modification Forge ORTF v1.0.0

    /**
     * La zone texte pour saisir le nom de l'action
     */
    JTextField nameField;


    /**
     * La zone texte pour saisir la description
     */
    JTextPane descriptionArea;

    /**
     * La zone texte pour saisir le resultat attendu
     */
    JTextPane awaitedResultArea;

    Style paramStyleDescription;
    Style paramStyleAwaitedResult;

    Style def;

    Style regularDescription;
    Style regularAwaitedResult;
    StyledDocument descriptionDoc;

    StyledDocument awaitedResultDoc;

    /**
     * L'action cree
     */
    Action action;

    AttachmentView attachmentPanel;

    //20100106 - D\ufffdbut modification Forge ORTF v1.0.0
    private Hashtable oldParameter;

    private HashMap oldAttachMap;

    private String oldActionName;

    private String oldDescription;

    private String oldAwaitedResult;

    private int newType;

    private int selectedRowIndex;
    //20100106 - Fin modification Forge ORTF v1.0.0
    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/

    /**
     * Constructeur de la vue pour ajouter/editer une action
     * @param parent le composant pere
     * @param title le titre du composant
     * @param name le nom de l'action
     * @param descr la description de l'action
     * @param result le resultat de l'action
     * @param attach la liste des attachements
     */
    public AskNewAction(Component parent, String title, Action oldAction, int newType, int selectedRowIndex) {

        //Pour bloquer le focus sur la boite de dialogue
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        int t_x = 1024 - 100;
        int t_y = 768/3*2 - 50;
        int t_x2 = 1024;
        int t_y2 = 768;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width - 100;
            t_y = r.height/3*2 -50 ;
            t_x2 = r.width;
            t_y2 = r.height ;
        } catch(Exception E){

        }


        nameField = new JTextField(40);
        descriptionArea = new JTextPane();
        awaitedResultArea = new JTextPane();
        descriptionDoc = descriptionArea.getStyledDocument();
        awaitedResultDoc = descriptionArea.getStyledDocument();
        //20100106 - D\ufffdbut modification Forge ORTF v1.0.0
        if(oldAction != null){
            oldActionName = oldAction.getNameFromModel();
            oldDescription = oldAction.getDescriptionFromModel();
            oldAwaitedResult = oldAction.getAwaitedResultFromModel();
            oldAttachMap = oldAction.getCopyOfAttachmentMapFromModel();
            oldParameter = oldAction.getCopyOfParameterHashTableFromModel();
        }
        this.newType = newType;
        this.selectedRowIndex = selectedRowIndex;
        //20100106 - Fin modification Forge ORTF v1.0.0

        // Cas ou le test est utilise dans une campagne de test
        String message = DataModel.getCurrentProject().getCampaignWithExecResultWhereTestIsUse(DataModel.getCurrentTest());
        if ( (!message.equals("")) && (Api.isLockExecutedTest()) ){
            nameField.setEditable(false);
            descriptionArea.setEditable(false);
            awaitedResultArea.setEditable(false);
        }

        JLabel testNameLabel = new JLabel(Language.getInstance().getText("Nom_de_l_action__"));

        JPanel giveName = new JPanel(new FlowLayout(FlowLayout.LEFT));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(testNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(nameField);



        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        //descriptionScrollPane.setPreferredSize(new Dimension(500,100));




        JScrollPane awaitedResultScrollPane = new JScrollPane(awaitedResultArea);
        awaitedResultScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Resultat_attendu")));
        //awaitedResultScrollPane.setPreferredSize(new Dimension(500,100));




        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!nameField.getText().trim().equals("")) {
                        if (!((ManualTest)DataModel.getCurrentTest()).hasActionNameInModel(nameField.getText().trim()) && !nameField.getText().trim().equals(ApiConstants.EMPTY_NAME) && !Action.isInBase(DataModel.getCurrentTest(), nameField.getText().trim())) {
                            createAndQuit();
                        } else if (((ManualTest)DataModel.getCurrentTest()).getActionFromModel(nameField.getText().trim()) != null && ((ManualTest)DataModel.getCurrentTest()).getActionFromModel(nameField.getText().trim()).equals(action)){
                            createAndQuit();
                        } else {
                            JOptionPane.showMessageDialog(AskNewAction.this,
                                                          Language.getInstance().getText("Ce_nom_d_action_existe_deja_"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(AskNewAction.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_a_l_action_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });


        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    action = null;
                    DataModel.initAttachmentTable(DataModel.getCurrentTest().getAttachmentMapFromModel().values());
                    AskNewAction.this.dispose();
                    cancelResult();
                }
            });


        JButton descriptionAddParameterButton = new JButton(Language.getInstance().getText("Parametre"));
        descriptionAddParameterButton.setIcon(Tools.createAppletImageIcon(PATH_TO_PARAM_ICON,""));
        descriptionAddParameterButton.setToolTipText(Language.getInstance().getText("Ajouter_un_parametre_a_la_description"));
        descriptionAddParameterButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addParameterInText(descriptionArea);
                }
            });
        // Cas ou le test est utilise dans une campagne de test
        if ((!message.equals(""))&& (Api.isLockExecutedTest())) {
            descriptionAddParameterButton.setEnabled(false);
        }

        JButton resultAddParameterButton = new JButton(Language.getInstance().getText("Parametre"));
        resultAddParameterButton.setIcon(Tools.createAppletImageIcon(PATH_TO_PARAM_ICON,""));
        resultAddParameterButton.setToolTipText(Language.getInstance().getText("Ajouter_un_parametre_au_resultat_attendu"));
        resultAddParameterButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    addParameterInText(awaitedResultArea);
                }
            });
        // Cas ou le test est utilise dans une campagne de test
        if ((!message.equals(""))&& (Api.isLockExecutedTest())) {
            resultAddParameterButton.setEnabled(false);
        }

        JPanel completeDescriptionPane = new JPanel();
        completeDescriptionPane.setLayout(new BoxLayout(completeDescriptionPane, BoxLayout.X_AXIS));
        completeDescriptionPane.add(descriptionScrollPane);
        descriptionScrollPane.setPreferredSize(new Dimension(t_x, t_y/3*2));
        completeDescriptionPane.add(descriptionAddParameterButton);
        completeDescriptionPane.setPreferredSize(new Dimension(t_x, t_y/3*2));

        JPanel completeAwaitedResultPane = new JPanel();
        completeAwaitedResultPane.setLayout(new BoxLayout(completeAwaitedResultPane, BoxLayout.X_AXIS));
        completeAwaitedResultPane.add(awaitedResultScrollPane);
        awaitedResultScrollPane.setPreferredSize(new Dimension(t_x, t_y/3*2));
        completeAwaitedResultPane.add(resultAddParameterButton);
        completeAwaitedResultPane.setPreferredSize(new Dimension(t_x, t_y/3*2));

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);


        /*JPanel textPanel = new JPanel();
          textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
          textPanel.add(Box.createRigidArea(new Dimension(0,10)));
          textPanel.add(giveName);
          textPanel.add(Box.createRigidArea(new Dimension(1,30)));
          textPanel.add(completeDescriptionPane);
          textPanel.add(Box.createRigidArea(new Dimension(1,30)));
          textPanel.add(completeAwaitedResultPane);
          textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        */

        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(1,1)));
        textPanel.add(completeDescriptionPane);
        textPanel.add(Box.createRigidArea(new Dimension(1,1)));
        textPanel.add(completeAwaitedResultPane);
        //textPanel.add(Box.createRigidArea(new Dimension(1,1)));



        //attachmentPanel = new AttachmentView(DataModel.getApplet(), ACTION, ACTION, oldAction, SMALL_SIZE_FOR_ATTACH, null, null, null);
        attachmentPanel = new AttachmentView(DataModel.getApplet(), ACTION, ACTION, oldAction, SMALL_SIZE_FOR_ATTACH, this);
        attachmentPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Attachements")));
        //textPanel.add(attachmentPanel);


        /* page.setLayout(new BorderLayout());
           page.add(textPanel);
           page.add(buttonPanel, BorderLayout.SOUTH);
        */

        //Container contentPaneFrame = this.getContentPane();
        //contentPaneFrame.add(page, BorderLayout.CENTER);


        def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        regularDescription = descriptionDoc.addStyle("regularD", def);
        StyleConstants.setForeground(regularDescription, Color.BLACK);
        //                Style souligne
        paramStyleDescription = descriptionDoc.addStyle("parameterD", regularDescription);
        StyleConstants.setForeground(paramStyleDescription, Color.red);

        regularAwaitedResult = descriptionDoc.addStyle("regularAR", def);
        StyleConstants.setForeground(regularAwaitedResult, Color.BLACK);
        //                Style souligne
        paramStyleAwaitedResult = descriptionDoc.addStyle("parameterAR", regularAwaitedResult);
        StyleConstants.setForeground(paramStyleAwaitedResult, Color.red);

        initData(oldAction);

        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    action = null;
                    DataModel.initAttachmentTable(DataModel.getCurrentTest().getAttachmentMapFromModel().values());
                    if(attachmentPanel.screenShootValided)
                        cancelResult();
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });


        /*
          Container contentPane = this.getContentPane();
          Dimension d = giveName.getSize();
          contentPane.add(giveName);
          giveName.setMaximumSize(new Dimension(t_x2, 40));
          contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

          textPanel.setPreferredSize(new Dimension(t_x, t_y/3*2 - d.height));
          contentPane.add(textPanel);

          attachmentPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
          contentPane.add(attachmentPanel);

          buttonPanel.setMaximumSize(new Dimension(t_x2, 40));
          contentPane.add(buttonPanel);
        */
        JTabbedPane pJTabbedPane = new JTabbedPane();
        Container contentPane = this.getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        Dimension d = giveName.getSize();
        JPanel actionDetail =  new JPanel();
        actionDetail.setLayout(new BoxLayout(actionDetail, BoxLayout.Y_AXIS));
        giveName.setMaximumSize(new Dimension(t_x2, 40));
        actionDetail.add(giveName);
        textPanel.setPreferredSize(new Dimension(t_x, t_y/3*2 - d.height));
        actionDetail.add(textPanel);

        attachmentPanel.setMaximumSize(new Dimension(t_x2, t_y2/3));
        pJTabbedPane.add(Language.getInstance().getText("Action"), actionDetail);
        pJTabbedPane.add(Language.getInstance().getText("Attachements"), attachmentPanel);

        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.ACTION_NEW_TAB,pJTabbedPane);
        // Activation des plugins associes
        PluginsTools.activateAssociatedPlgs(UICompCst.ACTION_NEW_TAB);

        buttonPanel.setMaximumSize(new Dimension(t_x2, 40));
        contentPane.add(pJTabbedPane);
        contentPane.add(buttonPanel);


        setSize(t_x, t_y);
        this.setTitle(title);

        /*this.setLocationRelativeTo(this.getParent());
          this.pack();
          this.setVisible(true);*/
        centerScreen();

    } // Fin du constructeur AddAction/6

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }


    /**
     * Constructeur
     * @param parent le composant pere
     * @param title le titre du composant
     */
    public AskNewAction(Component parent, String title) {
        this(parent, title, null, ADD_ACTION, 0);
    } // Fin du constructeur AddAction/2


    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/

    /**
     * Accesseur de l'action construite
     * @return une action
     */
    public Action getAction() {
        return action;
    } // Fin de la methode getAction/0

    private void initData(Action oldAction) {
        if (oldAction != null) {
            action = oldAction;
            //Initialisation pour edition
            nameField.setText(oldAction.getNameFromModel());
            descriptionArea.setText(oldAction.getDescriptionFromModel());
            awaitedResultArea.setText(oldAction.getAwaitedResultFromModel());
            Collection col = oldAction.getAttachmentMapFromModel().values();
            DataModel.initAttachmentTable(col);
        } else {
            ManualTest test = (ManualTest)DataModel.getCurrentTest();
            //action = new Action(test.getIdBDD());

            int i = DataModel.getActionTableModel().getRowCount();
            while (test.getActionListFromModel(false).contains(test.getActionFromModel("A" + i))) {
                i++;
            }
            action = new Action(test,"A" + i,"");
            nameField.setText("A" + i);
            DataModel.getAttachmentTableModel().clearTable();
        }
        DataModel.setCurrentAction(action);

    }

    /**
     *
     * @param area
     */
    private void addParameterInText(JTextPane textPane) {
        ParameterInsertionView insertView = new ParameterInsertionView();
        if (insertView.getParameter() != null) {
            // action.addParameter(insertView.getParameter());
            action.setUseParamInModel(insertView.getParameter());
            try {
                StyledDocument doc = (StyledDocument)textPane.getDocument();
                if (textPane.equals(awaitedResultArea)) {

                    doc.insertString(textPane.getCaretPosition() , "$" + insertView.getParameter().getNameFromModel() + "$", doc.getStyle("parameterAR"));
                    doc.insertString(doc.getLength(), " ", doc.getStyle("regularAR"));
                } else {
                    doc.insertString(textPane.getCaretPosition() , "$" + insertView.getParameter().getNameFromModel() + "$", doc.getStyle("parameterD"));
                    doc.insertString(doc.getLength(), " ", doc.getStyle("regularD"));
                }
            } catch (BadLocationException ble) {
            }
        }
    } // Fin de la methode addParameterInText/1


    private void createAndQuit() {

        action.updateInModel(nameField.getText().trim(), descriptionArea.getText(), awaitedResultArea.getText());
        //action.getAttachmentMapFromModel().clear();
        /*Collection values = attachmentPanel.getAttachmentMap().values();
          for (Iterator iter = values.iterator(); iter.hasNext();) {
          action.addAttachementInModel((Attachment)iter.next());
          }*/
        ArrayList paramInDesc = Tools.getParametersInDescription(descriptionArea.getText(), action.getParameterHashSetFromModel());
        ArrayList paramInAwaitedResult = Tools.getParametersInDescription(awaitedResultArea.getText(), action.getParameterHashSetFromModel());
        ArrayList toBeDelete = new ArrayList();
        HashSet paramSet = new HashSet(action.getParameterHashSetFromModel().values());//ADD - Hashtable2HashSet
        for (Iterator iter = paramSet.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            if (!paramInDesc.contains(param) && !paramInAwaitedResult.contains(param)) toBeDelete.add(param);
        }
        for (int i =0; i < toBeDelete.size(); i++) {
            action.deleteUseParamInModel((Parameter)toBeDelete.get(i));
        }
        DataModel.initAttachmentTable(DataModel.getCurrentTest().getAttachmentMapFromModel().values());
        AskNewAction.this.dispose();
        pushResult();
    }

    //20100106 - D\ufffdbut modification Forge ORTF v1.0.0
    /**
     * @method cancelResult()
     * @covers EDF-2.4.8
     * @jira FORTF-1
     */
    private void cancelResult(){
        if(this.newType == ADD_ACTION){
            action.setAttachmentMapInModel(oldAttachMap);
            if(this.newType == MODIFY_ACTION){
                action.setParameterHashtableInModel(oldParameter);
            }
        }
    }

    /**
     * @method pushResult()
     * @covers SFG_ForgeORTF_TST_ACT_000030 - \ufffd2.4.8
     * @covers EDF-2.4.8
     * @jira FORTF-1
     */
    private void pushResult(){
        switch(this.newType){
        case ADD_ACTION:
            if (!((ManualTest) DataModel.getCurrentTest()).hasActionNameInModel(action.getNameFromModel())) {
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(101,ApiConstants.INSERT_ACTION);
                    //action.setOrderIndex(DataModel.getActionTableModel()
                    //          .getRowCount());
                    ((ManualTest) DataModel.getCurrentTest()).addActionInDBAndModel(action);

                    Set keySet = action.getAttachmentMapFromModel().keySet();
                    for (Iterator iter = keySet.iterator(); iter.hasNext();) {
                        Attachment attach = (Attachment) action.getAttachmentMapFromModel().get(iter.next());
                        action.addAttachementInDB(attach);
                    }
                    HashSet pHashSet = new HashSet(action.getParameterHashSetFromModel().values()); //FOR HashTable2HashSet
                    for (Iterator iterator = pHashSet.iterator(); iterator.hasNext();) {
                        Parameter param = (Parameter) iterator.next();
                        action.setUseParamInDBAndModel(param);
                    }
                    Api.commitTrans(transNumber);
                    transNumber = -1;
                    ArrayList dataList = new ArrayList();
                    dataList.add(action.getNameFromModel());
                    dataList.add(action.getDescriptionFromModel());
                    dataList.add(action.getAwaitedResultFromModel());
                    dataList.add(action.getAttachmentMapFromModel().keySet());
                    DataModel.getActionTableModel().addRow(dataList);
                    //action.setOrderIndex(DataModel.getActionTableModel().getRowCount()-1);
                    //((ManualTest) DataModel.getCurrentTest())
                    //          .addAction(action);

                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    ((ManualTest) DataModel.getCurrentTest()).deleteActionInModel(action);
                    Tools.ihmExceptionView(exception);
                }
            }else {
                JOptionPane
                    .showMessageDialog(
                                       AskNewAction.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Ce_nom_d_action_existe_deja_pour_ce_test_"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
            }
            break;
        case MODIFY_ACTION_LOCK_TEST:
            int transNumber = -1;
            try {
                // BdD
                transNumber = Api.beginTransaction(100, ApiConstants.UPDATE_ACTION);
                //ConnectionData.getSuiteTestUpdate().updateAction(DataModel.getCurrentTest().getTestList().getFamily().getName(), DataModel.getCurrentTest().getTestList().getName(), DataModel.getCurrentTest().getName(), oldActionName, addAction.getAction().getName(), addAction.getAction().getDescription(), addAction.getAction().getAwaitedResult(), addAction.getAction().getOrderIndex());
                String newName = action.getNameFromModel();
                action.updateInModel(oldActionName, action.getDescriptionFromModel(), action.getAwaitedResultFromModel());
                action.updateInDBAndModel(newName, action.getDescriptionFromModel(), action.getAwaitedResultFromModel());

                // Liste des anciens fichiers attach?s
                action.updateAttachement(oldAttachMap);

                Api.commitTrans(transNumber);
                transNumber = -1;
                // IHM
                ArrayList dataList = new ArrayList();
                dataList.add(action.getNameFromModel());
                dataList.add(action.getDescriptionFromModel());
                dataList.add(action.getAwaitedResultFromModel());
                //HashMap newMap = addAction.getAction().getAttachmentMap();
                dataList.add(action.getAttachmentMapFromModel().keySet());
                DataModel.getActionTableModel().modifyData(dataList,selectedRowIndex);
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                action.setAttachmentMapInModel(oldAttachMap);
                Tools.ihmExceptionView(exception);
            }
            break;
        case MODIFY_ACTION:
            HashSet oldParamSet = new HashSet(oldParameter.values());//FOR HashTable2HashSet
            int transNbr = -1;
            try {
                // BdD
                transNbr = Api.beginTransaction(101, ApiConstants.UPDATE_ACTION);
                action.updateInDBAndModel(action.getNameFromModel(), action.getDescriptionFromModel(), action.getAwaitedResultFromModel());

                /* Les attachement */
                action.updateAttachement(oldAttachMap);


                /* Les Paramtres */
                Hashtable newParameter = action.getParameterHashSetFromModel();
                Set keysSet = newParameter.keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    Object paramName =  iter.next();
                    Parameter param = (Parameter) oldParameter.get(paramName);
                    Parameter pParam = (Parameter)newParameter.get(paramName);
                    if (param == null){
                        action.setUseParamInDB(pParam.getIdBdd());
                    } else {
                        /* Update */
                    }
                }

                keysSet = oldParameter.keySet();
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    Object paramName =  iter.next();
                    Parameter param = (Parameter) newParameter.get(paramName);
                    Parameter pParam = (Parameter)oldParameter.get(paramName);
                    if (param == null) {
                        action.deleteUseParamInDB(pParam.getIdBdd());
                    }
                }


                Api.commitTrans(transNbr);

                // IHM
                ArrayList dataList = new ArrayList();
                dataList.add(action.getNameFromModel());
                dataList.add(action.getDescriptionFromModel());
                dataList.add(action.getAwaitedResultFromModel());
                //HashMap newMap = addAction.getAction().getAttachmentMap();
                dataList.add(action.getAttachmentMapFromModel().keySet());
                DataModel.getActionTableModel().modifyData(dataList,selectedRowIndex);

            } catch (Exception exception) {
                Api.forceRollBackTrans(transNbr);
                action.updateInModel(oldActionName, oldDescription,oldAwaitedResult);
                action.setAttachmentMapInModel(oldAttachMap);
                action.setParameterHashSetInModel(oldParamSet);
                Tools.ihmExceptionView(exception);
            }
            break;
        }
    }
    //20100106 - Fin modification Forge ORTF v1.0.0

} // Fin de la classe AddAction
