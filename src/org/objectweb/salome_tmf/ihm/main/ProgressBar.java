package org.objectweb.salome_tmf.ihm.main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.awt.event.WindowListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;

//public class ProgressBar extends      JFrame  {
public class ProgressBar extends        JDialog implements WindowListener ,  WindowFocusListener {
    private     JProgressBar    progress;
    private     JLabel          label1;
    private     JPanel          topPanel;
        
        
    
    int iCtr  = 0;
    int nbTask = 0;
    boolean isDisposed = false;
        
    public ProgressBar(String title, String label, int nbTask) {
        super(SalomeTMFContext.getInstance().getSalomeFrame());
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        setTitle(title);
        //setModal(true);
        setSize( 310, 130 );
        setBackground( Color.gray );
        this.nbTask = nbTask;
        topPanel = new JPanel();
        topPanel.setPreferredSize( new Dimension( 310, 130 ) );
        getContentPane().add( topPanel );

        // Create a label and progress bar
        label1 = new JLabel( label );
        label1.setPreferredSize( new Dimension( 280, 24 ) );
        topPanel.add( label1 );

        progress = new JProgressBar();
        progress.setPreferredSize( new Dimension( 300, 20 ) );
        progress.setMinimum( 0 );
        progress.setMaximum( nbTask );
        progress.setValue( 0 );
        progress.setBounds( 20, 35, 260, 20 );
        topPanel.add( progress );
        addWindowListener(this);
                
        //this.setLocation(300,300);
        this.setLocationRelativeTo(this.getParent()); 
        pack();
                
                
    }

    public void Show(){
        if (SalomeTMFContext.getInstance().getSalomeFrame() != null){
            show();
        }
    }
        
    public void doTask(String task){
        iCtr++;
        // Update the progress indicator and label
        label1.setText( "Performing task " + task);
        Rectangle labelRect = label1.getBounds();
        labelRect.x = 0;
        labelRect.y = 0;
        label1.paintImmediately( labelRect );

        progress.setValue( iCtr );
        Rectangle progressRect = progress.getBounds();
        progressRect.x = 0;
        progressRect.y = 0;
        progress.paintImmediately( progressRect );
    }
        
    /*public synchronized void show(){
      super.show();
      }*/
        
    public void finishAllTask(){
        isDisposed = true;
        dispose();
    }
        
    @Override
    public void windowActivated(WindowEvent e) {}
    @Override
    public void windowClosed(WindowEvent e) {
        if (!isDisposed){
            setVisible(true);
            toFront();
        }
    }
    @Override
    public void windowClosing(WindowEvent e) {
        if (!isDisposed){
            setVisible(true);
            toFront();
        }
    }
        
    @Override
    public void windowDeactivated(WindowEvent e) {
        toFront();
    }
    @Override
    public void windowDeiconified(WindowEvent e) {}
    @Override
    public void windowIconified(WindowEvent e) {
        toFront();
    }
    @Override
    public void windowOpened(WindowEvent e) {}
        
    @Override
    public void windowGainedFocus(WindowEvent e) {
        toFront();
    }

    @Override
    public void windowLostFocus(WindowEvent e) {
        toFront();
    }
}
