package org.objectweb.salome_tmf.ihm.main;

import java.net.URL;

public interface BaseIHM {
        
    /**
     * M?thode appel? lorsque l'utilisateur quitte l'application.
     */
    public void quit(boolean do_recup, boolean doclose);
    
    public SalomeTMFContext getSalomeTMFContext();
    
    public SalomeTMFPanels getSalomeTMFPanels();
   
    public void showDocument(URL toShow , String where);
    
    public boolean isGraphique();
}
