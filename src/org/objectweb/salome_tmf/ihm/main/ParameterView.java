/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.common.AskNameAndDescription;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;


public class ParameterView extends JPanel implements DataConstants, ActionListener, ListSelectionListener {
    
        
    JPanel paramsPanel;
    
    JButton refreshButton;
    
    JButton modifyParameterButton;
    
    JButton deleteParameterButton;
    
    JButton addParameter;
    
    JButton useParameterButton;
    //  MyTableModel manualParameterTableModel = new MyTableModel();
    
    JTable parametersTable;
    
    boolean usingFonctionEnable;
    
    TableSorter  sorter;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    
    /**
     *
     * type is use for plugin UI = AUTOMATIC_TEST or MANUAL_TEST or PARAMETER or -1 for Dynamic
     */
    
    public ParameterView(boolean use, int type) {
        paramsPanel = new JPanel();
        
        refreshButton = new JButton(Language.getInstance().getText("Rafraichir"));
        refreshButton.addActionListener(this);
        
        modifyParameterButton = new JButton(Language.getInstance().getText("Modifier"));
        deleteParameterButton = new JButton(Language.getInstance().getText("Supprimer"));
        parametersTable = new JTable();
        
        addParameter = new JButton(Language.getInstance().getText("Nouveau"));
        addParameter.setToolTipText(Language.getInstance().getText("Ajouter_un_nouveau_parametre"));
        addParameter.addActionListener(this);
        
        useParameterButton = new JButton(Language.getInstance().getText("Utiliser"));
        useParameterButton.setToolTipText(Language.getInstance().getText("Utiliser_des_parametres_existants"));
        useParameterButton.addActionListener(this);
        
        modifyParameterButton.setEnabled(false);
        modifyParameterButton.setToolTipText(Language.getInstance().getText("Modifier_un_parametre"));
        modifyParameterButton.addActionListener(this);
        
        deleteParameterButton.setToolTipText(Language.getInstance().getText("Supprimer_un_parametre"));
        deleteParameterButton.setEnabled(false);
        deleteParameterButton.addActionListener(this);
        
        JPanel buttonsPanel = new JPanel(new GridLayout(1,4));
        buttonsPanel.add(refreshButton);
        buttonsPanel.add(addParameter);
        if (use) {
            buttonsPanel.add(useParameterButton);
        }
        
        
        buttonsPanel.add(modifyParameterButton);
        buttonsPanel.add(deleteParameterButton);
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        ListSelectionModel rowSM = parametersTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        //sorter = new TableSorter();
        if (use) {
            sorter = new TableSorter(DataModel.getTestParameterTableModel());
            //parametersTable.setModel(DataModel.getTestParameterTableModel());
        } else {
            sorter = new TableSorter(DataModel.getParameterTableModel());
            //parametersTable.setModel(DataModel.getParameterTableModel());
        }
        parametersTable.setModel(sorter);
        sorter.setTableHeader(parametersTable.getTableHeader());
        
        parametersTable.setPreferredScrollableViewportSize(new Dimension(600, 200));
        parametersTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        
        JScrollPane tablePane = new JScrollPane(parametersTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
        
        initData(use);
        
        paramsPanel.setLayout(new BorderLayout());
        paramsPanel.add(buttonsPanel, BorderLayout.NORTH);
        paramsPanel.add(tablePane, BorderLayout.CENTER);
        
        
        this.setLayout(new BorderLayout());
        this.add(paramsPanel);
        
        mapUIComponents(paramsPanel, parametersTable, buttonsPanel, type);
                   
    } // Fin du constructeur ManualParameterView/0
    
    
    void mapUIComponents(JPanel paramsPanel, JTable parametersTable, JPanel buttonsPanel, int type){
        switch (type) {
        case MANUAL_TEST : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_PARAMS_PANEL,paramsPanel);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_PARAMS_PANEL);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_PARAMS_TABLE,parametersTable);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_PARAMS_TABLE);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.MANUAL_TEST_PARAMS_BUTTONS_PANEL,buttonsPanel);
            UICompCst.staticUIComps.add(UICompCst.MANUAL_TEST_PARAMS_BUTTONS_PANEL);
            break;
        }
        case AUTOMATIC_TEST : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_PARAMS_PANEL,paramsPanel);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_PARAMS_PANEL);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_PARAMS_TABLE,parametersTable);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_PARAMS_TABLE);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.AUTOMATED_TEST_PARAMS_BUTTONS_PANEL,buttonsPanel);
            UICompCst.staticUIComps.add(UICompCst.AUTOMATED_TEST_PARAMS_BUTTONS_PANEL);
            break;
        }
        case PARAMETER : {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_PARAMS_PANEL,paramsPanel);
            UICompCst.staticUIComps.add(UICompCst.DATA_PARAMS_PANEL);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_PARAMS_TABLE,parametersTable);
            UICompCst.staticUIComps.add(UICompCst.DATA_PARAMS_TABLE);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.DATA_PARAMS_BUTTONS_PANEL,buttonsPanel);
            UICompCst.staticUIComps.add(UICompCst.DATA_PARAMS_BUTTONS_PANEL);
            break;
        }
        
        default: {
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PARAMS_PANEL,paramsPanel);
            PluginsTools.activateAssociatedPlgs(UICompCst.PARAMS_PANEL);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PARAMS_TABLE,parametersTable);
            PluginsTools.activateAssociatedPlgs(UICompCst.PARAMS_TABLE);
                
            SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PARAMS_BUTTONS_PANEL,buttonsPanel);
            PluginsTools.activateAssociatedPlgs(UICompCst.PARAMS_BUTTONS_PANEL);   
        }
        }
    }
    /**
     *
     * @param use
     */
    private void initData(boolean use) {
        usingFonctionEnable = use;
    }
    
    /**
     *
     * @param use
     */
    public static void addNewParameter(boolean use) {
        AskNewParameter askNewParameter = new AskNewParameter(null);
        Parameter param = askNewParameter.getParameter();
        if (param != null && param.getNameFromModel() != null && param.getNameFromModel() != "") {
            if (!DataModel.getCurrentProject().containsParameterInModel(param.getNameFromModel().trim())) {
                
                try {
                    //param.add2DB();
                    DataModel.getCurrentProject().addParameterToDBAndModel(param);
                    ArrayList dataList = new ArrayList();
                    dataList.add(param.getNameFromModel());
                    dataList.add(param.getDescriptionFromModel());
                    //ProjectData.getCurrentProject().addParameter(param);
                    DataModel.getParameterTableModel().addRow(dataList);
                    if (use) {
                        if (Api.isConnected()) {
                            try {
                                DataModel.getCurrentTest().setUseParamInDBAndModel(param);
                                
                                DataModel.getTestParameterTableModel().addRow(dataList);
                            } catch (Exception exception) {
                                Tools.ihmExceptionView(exception);
                            }
                        }
                        
                    }
                    
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
                
            } else {
                JOptionPane.showMessageDialog(null,
                                              Language.getInstance().getText("Ce_nom_de_parametre_existe_deja_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     *
     *
     */
    public static void usingParameter() {
        UsingDefinedParameterView usingDefinedParameterView = new UsingDefinedParameterView(false, false);
        if (usingDefinedParameterView.getParameterList() != null) {
            for (int i =0; i < usingDefinedParameterView.getParameterList().size(); i++) {
                Parameter param = (Parameter)usingDefinedParameterView.getParameterList().get(i);
                if (!DataModel.getCurrentTest().hasUsedParameterNameFromModel(param.getNameFromModel())) {
                    
                    try {
                        DataModel.getCurrentTest().setUseParamInDBAndModel(param);
                        ArrayList dataList = new ArrayList();
                        dataList.add(param.getNameFromModel());
                        dataList.add(param.getDescriptionFromModel());
                        DataModel.getTestParameterTableModel().addRow(dataList);
                        //DataModel.getCurrentTest().addParameter(param);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                    
                } else {
                    JOptionPane.showMessageDialog(null,
                                                  Language.getInstance().getText("Le_parametre__")+ param.getNameFromModel() +Language.getInstance().getText("__existe_deja_pour_ce_test_"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    
    /**
     *
     * @param currentTest
     * @param paramName
     */
    /*private static void cleanActionOfParameter(Test currentTest, String paramName) {
      if (currentTest instanceof ManualTest) {
      for (int i = 0; i < ((ManualTest)currentTest).getActionList().size(); i++) {
                
      //Action action = (Action)((ManualTest)DataModel.getCurrentTest()).getActionList().get(i);
      Action action = (Action)((ManualTest)currentTest).getActionList().get(i);
                
      try {
      String newDesc = Tools.clearStringOfParameter(action.getDescription(), paramName);
      String newResult = Tools.clearStringOfParameter(action.getAwaitedResult(), paramName);
                    
      // DB
      action.updateInDB(action.getName(), newDesc, newResult, action.getOrderIndex());
                    
      // Data
                        
      action.getParameterHashSet().remove(DataModel.getCurrentProject().getParameter(paramName));
                    
      if (DataModel.getCurrentTest() != null) {
      if (DataModel.getCurrentTest().equals(currentTest)) {
      int row = DataModel.getActionTableModel().findRow(action.getName());
      DataModel.getActionTableModel().setValueAt(newDesc, row, 1);
      DataModel.getActionTableModel().setValueAt(newResult, row, 2);
      }
      }
      } catch (Exception exception) {
      Tools.ihmExceptionView(exception.toString());
      }
      }
      }
      } // Fin de la m?thode cleanActionOfParameter/2
    */
    
    private static void upadateActionOfParameterView(Test currentTest, String paramName) {
        if (currentTest instanceof ManualTest) {
            ArrayList actionList = ((ManualTest)currentTest).getActionListFromModel(false);
            for (int i = 0; i < actionList.size(); i++) {
                Action action = (Action)actionList.get(i); 
                try {
                    if (DataModel.getCurrentTest() != null && DataModel.getCurrentTest().equals(currentTest)) {
                        int row = DataModel.getActionTableModel().findRow(action.getNameFromModel());
                        DataModel.getActionTableModel().setValueAt(action.getDescriptionFromModel(), row, 1);
                        DataModel.getActionTableModel().setValueAt(action.getAwaitedResultFromModel(), row, 2);
                    }
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
            }
        }
    } // Fin de la m?thode cleanActionOfParameter/2
    
    /**
     *
     *
     */
    public void giveAccessToIhmParameterView() {
        if (usingFonctionEnable) {
            if (!Permission.canDeleteTest()) {
                deleteParameterButton.setEnabled(false);
                
            }
            if (!Permission.canCreateTest()) {
                addParameter.setEnabled(false);
                useParameterButton.setEnabled(false);
                
            }
            if (!Permission.canUpdateTest()) {
                modifyParameterButton.setEnabled(false);
                
            }
            
        } else {
            if (!Permission.canDeleteCamp()) {
                deleteParameterButton.setEnabled(false);
            }
            if (!Permission.canCreateCamp() && !Permission.canExecutCamp()) {
                addParameter.setEnabled(false);
                useParameterButton.setEnabled(false);
            }
            if (!Permission.canUpdateCamp() && !Permission.canExecutCamp()) {
                modifyParameterButton.setEnabled(false);
            }
        }
        
    }
    /***************** Event Managment ************************/
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(addParameter)){
            addParameterPerformed();
        } else if (e.getSource().equals(useParameterButton)) {
            useParameterButtonPerformed();
        } else if (e.getSource().equals(modifyParameterButton)) {
            modifyParameterButtonPerformed();
        } else if (e.getSource().equals(deleteParameterButton)){
            deleteParameterButtonPerformed();
        } else if (e.getSource().equals(refreshButton)){
            refreshPerformed();
        }
    }
    
    void refreshPerformed(){
        try {
            DataModel.reloadParameters();
            DataModel.reloadEnvironnements();
        } catch (Exception e) {
            // TODO: handle exception
        }
    }
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(parametersTable.getSelectionModel())){
            parametersTablevalueChanged(e);
        }
    }
    
    
    public void addParameterPerformed() {
        /*String message = DataModel.getCurrentProject().getCampaignWithExecResultWhereTestIsUse(DataModel.getCurrentTest());
          if (!message.equals("") && usingFonctionEnable && (Api.isLockExecutedTest())) {
          JOptionPane.showMessageDialog(ParameterView.this,
          Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message + Language.getInstance().getText("qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_ajouter_un_nouveau_parametre"),
          Language.getInstance().getText("Erreur_"),
          JOptionPane.ERROR_MESSAGE);
          } else {
          if ((!message.equals(""))&& (!Api.isLockExecutedTest())) { 
          Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
          int choice = SalomeTMFContext.getInstance().askQuestion(
          Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message +  Language.getInstance().getText("IntegiteCheck"),
          Language.getInstance().getText("Attention_"),
          JOptionPane.WARNING_MESSAGE,
          options);
          if (choice == JOptionPane.NO_OPTION) {
          return;
          }
          }
          addNewParameter(usingFonctionEnable);
          }*/
        addNewParameter(usingFonctionEnable);
    }
   
    
    public void useParameterButtonPerformed() {
        String message = DataModel.getCurrentProject().getCampaignWithExecResultWhereTestIsUse(DataModel.getCurrentTest());
        if (!message.equals("") && usingFonctionEnable&& (Api.isLockExecutedTest())) {
            JOptionPane.showMessageDialog(ParameterView.this,
                                          Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message + Language.getInstance().getText("qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_ajouter_un_parametre_deja_defini"),
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
        } else {
            if ((!message.equals(""))&& (!Api.isLockExecutedTest())) { 
                Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
                int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message +  Language.getInstance().getText("IntegiteCheck"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE,
                                                                        options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            }
            usingParameter();
        }
    }
    
    public void modifyParameterButtonPerformed() {
        int selectedRowIndex = parametersTable.getSelectedRow();
        if (selectedRowIndex != -1) {
            String message = DataModel.getCurrentProject().getCampaignWithExecResultWhereTestIsUse(DataModel.getCurrentTest());
            if (!message.equals("") && usingFonctionEnable && (Api.isLockExecutedTest())) {
                JOptionPane.showMessageDialog(ParameterView.this,
                                              Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message + Language.getInstance().getText("qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_modifier_ce_parametre"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            } else {
                if ((!message.equals(""))&& (!Api.isLockExecutedTest())) { 
                    Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
                    int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                            Language.getInstance().getText("Ce_test_est_utilise_dans_les_campagnes__") + message +  Language.getInstance().getText("IntegiteCheck"),
                                                                            Language.getInstance().getText("Attention_"),
                                                                            JOptionPane.WARNING_MESSAGE,
                                                                            options);
                    if (choice == JOptionPane.NO_OPTION) {
                        return;
                    }
                }
                ArrayList paramList;
                if (usingFonctionEnable) {
                    //paramList = DataModel.getTestParameterTableModel().getData(selectedRowIndex);
                    paramList = DataModel.getTestParameterTableModel().getData(sorter.modelIndex(selectedRowIndex));
                                
                } else {
                    //paramList = DataModel.getParameterTableModel().getData(selectedRowIndex);
                    paramList = DataModel.getParameterTableModel().getData(sorter.modelIndex(selectedRowIndex));
                }
                AskNameAndDescription askNewParameter = null;
                if (paramList != null && paramList.size() == parametersTable.getColumnCount()) {
                    askNewParameter = new AskNameAndDescription(PARAMETER, Language.getInstance().getText("Modifier_un_parametre"), Language.getInstance().getText("Nom_du_parametre__"), DataModel.getCurrentProject().getParameterFromModel((String)paramList.get(0)),SalomeTMFContext.getInstance().ptrFrame, null);
                }
                if (askNewParameter != null) {
                    Parameter param = askNewParameter.getNewParameter();
                    if (param != null) {
                                        
                                        
                        try {
                            // BdD
                            param.updateDescriptionInDBAndModel(param.getDescriptionFromModel());
                                                
                            // IHM
                            ArrayList dataList = new ArrayList();
                            dataList.add(param.getNameFromModel());
                            dataList.add(param.getDescriptionFromModel());
                            if (usingFonctionEnable) {
                                DataModel.getTestParameterTableModel().modifyData(dataList,sorter.modelIndex(selectedRowIndex));
                                Parameter oldParam = ((ManualTest)DataModel.getCurrentTest()).getUsedParameterFromModel((String)paramList.get(0));
                                oldParam.updateNameInModel(param.getNameFromModel());
                                oldParam.updateDescriptionInModel(param.getDescriptionFromModel());
                            } else {
                                DataModel.getParameterTableModel().modifyData(dataList, sorter.modelIndex(selectedRowIndex));
                            }
                        } catch(Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    }
                }
            }
        }
    }   
    
    
    
    
    public void deleteParameterButtonPerformed() {
        //int selectedRowIndex = parametersTable.getSelectedRow();
        int[] selectedRows = parametersTable.getSelectedRows();
        String paramName;
        if (usingFonctionEnable) {
            Object[] options = { Language.getInstance().getText("Oui"),
                                 Language.getInstance().getText("Non") };
            int choice = -1;
            choice = JOptionPane
                .showOptionDialog(
                                  ParameterView.this,
                                  Language
                                  .getInstance()
                                  .getText(
                                           "Etes_vous_sur_de_vouloir_supprimer_les_parametre_du_test_")
                                  + DataModel.getCurrentTest() + "> ?",
                                  Language.getInstance().getText("Attention_"),
                                  JOptionPane.YES_NO_OPTION,
                                  JOptionPane.QUESTION_MESSAGE, null, options,
                                  options[1]);
            if (choice == JOptionPane.YES_OPTION) {

                String message = DataModel.getCurrentProject()
                    .getCampaignWithExecResultWhereTestIsUse(
                                                             DataModel.getCurrentTest());
                if (!message.equals("") && usingFonctionEnable
                    && (Api.isLockExecutedTest())) {
                    JOptionPane
                        .showMessageDialog(
                                           ParameterView.this,
                                           Language
                                           .getInstance()
                                           .getText(
                                                    "Ce_test_est_utilise_dans_les_campagnes_:_\n")
                                           + message
                                           + Language
                                           .getInstance()
                                           .getText(
                                                    "qui_possedent_des_resultats_d_executions_Afin_de_conserver_l_integrite_des_resultats_vous_ne_pouvez_pas_supprimer_ce_parametre"),
                                           Language.getInstance().getText("Erreur_"),
                                           JOptionPane.ERROR_MESSAGE);
                } else {
                    if ((!message.equals("")) && (!Api.isLockExecutedTest())) {
                        choice = SalomeTMFContext
                            .getInstance()
                            .askQuestion(
                                         Language
                                         .getInstance()
                                         .getText(
                                                  "Ce_test_est_utilise_dans_les_campagnes_:_\n")
                                         + message
                                         + Language
                                         .getInstance()
                                         .getText(
                                                  "IntegiteCheck"),
                                         Language.getInstance().getText(
                                                                        "Attention_"),
                                         JOptionPane.WARNING_MESSAGE, options);
                        if (choice == JOptionPane.NO_OPTION) {
                            return;
                        }
                    }
                    for (int selectedRowIndex = selectedRows.length - 1; selectedRowIndex >= 0; selectedRowIndex--) {
                        // paramName =
                        // (String)DataModel.getTestParameterTableModel().getValueAt(selectedRowIndex,
                        // 0);
                        paramName = (String) sorter.getValueAt(
                                                               selectedRows[selectedRowIndex], 0);

                        try {
                            Test test = DataModel.getCurrentTest();
                            Parameter param = test
                                .getUsedParameterFromModel(paramName);

                            // BdD & Model
                            // test.deleteUseParamFromDB(param);
                            test.deleteUseParamInDBAndModel(param);
                            // DataModel.getCurrentTest().deleteParameter(paramName);
                            // IHM
                            /*DataModel.getTestParameterTableModel().removeData(
                              sorter.modelIndex(selectedRows[selectedRowIndex]));
                              if (DataModel.getTestParameterTableModel()
                              .getRowCount() == 0) {
                              deleteParameterButton.setEnabled(false);
                              }
                              upadateActionOfParameterView(test, paramName);*/

                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    }
                                        
                    // IHM
                    DataModel.getTestParameterTableModel().clearTable();
                    Test test = DataModel.getCurrentTest();
                    for (int i =0; i < test.getParameterListFromModel().size(); i++) {
                        Parameter param = (Parameter)test.getParameterListFromModel().get(i);
                        DataModel.getTestParameterTableModel().addValueAt(param.getNameFromModel(), i, 0);
                        DataModel.getTestParameterTableModel().addValueAt(param.getDescriptionFromModel(), i, 1);
                        upadateActionOfParameterView(test, param.getNameFromModel());
                    }                                   
                    if (DataModel.getTestParameterTableModel()
                        .getRowCount() == 0) {
                        deleteParameterButton.setEnabled(false);
                    }
                }
            }
        } else {
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            int choice = -1;
            choice = JOptionPane.showOptionDialog(ParameterView.this,
                                                  //"Etes vous s?r de vouloir supprimer le param?tre <" +(String)DataModel.getParameterTableModel().getValueAt(selectedRowIndex, 0) + "> du projet ?",
                                                  Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_les_parametre"),
                                                  Language.getInstance().getText("Attention_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
            if (choice == JOptionPane.YES_OPTION) {
                for (int selectedRowIndex = selectedRows.length - 1; selectedRowIndex >= 0; selectedRowIndex--) {
                    //paramName = (String)DataModel.getParameterTableModel().getValueAt(selectedRowIndex, 0);
                    paramName = (String)sorter.getValueAt(selectedRows[selectedRowIndex], 0);
                    ArrayList testsOfParam = DataModel.getCurrentProject().getTestOfParameterFromModel(paramName);
                    ArrayList envOfParam = DataModel.getCurrentProject().getEnvironmentOfParameterFromModel(paramName);
                    if (testsOfParam.size() > 0 || envOfParam.size() > 0) {
                        choice = -1;
                        //int actionCase = -1;
                        String message = Language.getInstance().getText("Le_parametre__") + paramName + Language.getInstance().getText("_est_utilise_dans_les_tests_");
                        String campaignMessage = "";
                        ArrayList concernedCampaign = new ArrayList();
                        String testsMessage = "";
                        String envMessage = "";
                        //String toBeDelete = "";
                        for (int i = 0; i < testsOfParam.size(); i++) {
                            testsMessage = testsMessage + "* " + ((Test)testsOfParam.get(i)).getTestListFromModel().getFamilyFromModel().getNameFromModel() + "/" + ((Test)testsOfParam.get(i)).getTestListFromModel().getNameFromModel() + "/" + ((Test)testsOfParam.get(i)).getNameFromModel()+"\n";
                            ArrayList campaignOfTest = DataModel.getCurrentProject().getCampaignOfTest((Test)testsOfParam.get(i));
                            if (campaignOfTest.size() > 0) {
                                for (int j = 0; j < campaignOfTest.size(); j++) {
                                    if (!concernedCampaign.contains(campaignOfTest.get(j))) {
                                        campaignMessage = campaignMessage + "* " + ((Campaign)campaignOfTest.get(j)).getNameFromModel() + "\n";
                                        concernedCampaign.add(campaignOfTest.get(j));
                                    }
                                }
                            }
                        }
                        for (int k = 0; k < envOfParam.size(); k++) {
                            if (((Environment)envOfParam.get(k)).containsParameterInModel(paramName)) {
                                envMessage = envMessage + "* " + ((Environment)envOfParam.get(k)).getNameFromModel() + "\n";
                            }
                        }
                                        
                        message = message + testsMessage + Language.getInstance().getText("dans_les_campagnes__") +campaignMessage + Language.getInstance().getText("dans_les_environnements_") + envMessage;
                        message = message + Language.getInstance().getText("Le_parametre_sera_supprime_des_tests_et_des_environnements_et_des_jeux_de_donnees_concernes_Les_resultats_d_executions_des_campagnes_seront_perdus");
                        choice = JOptionPane.showOptionDialog(ParameterView.this,
                                                              message + Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_le_parametre__") + paramName + " > ?",
                                                              Language.getInstance().getText("Attention_"),
                                                              JOptionPane.YES_NO_OPTION,
                                                              JOptionPane.WARNING_MESSAGE,
                                                              null,
                                                              options,
                                                              options[1]);
                        if (choice == JOptionPane.YES_OPTION) {
                                                
                            try {
                                // BdD and Model
                                Parameter pParam = DataModel.getCurrentProject().getParameterFromModel(paramName);
                                DataModel.getCurrentProject().deleteParamInDBndModel(pParam);
                                                                
                                // IHM
                                //Suppression du param?tre des tests et action de tests (si manuel)
                                for (int j = 0; j < testsOfParam.size(); j++) {
                                    Test pTest = (Test)testsOfParam.get(j);
                                    // Si le test courant est trait?, on nettoie la table des param?tres
                                    /*if (DataModel.getCurrentTest() != null && DataModel.getCurrentTest().equals(pTest)) {
                                      int rowIndex = DataModel.getTestParameterTableModel().findRow(paramName);
                                      if (rowIndex != -1) {
                                      DataModel.getTestParameterTableModel().removeData(sorter.modelIndex(rowIndex));
                                      }
                                      }
                 
                                      upadateActionOfParameterView(pTest, paramName);*/
                                    //Suppression en cascade dans l'arbre des campagnes
                                    for (int k = 0; k < concernedCampaign.size(); k++) {
                                        Campaign campaign = (Campaign)concernedCampaign.get(k);
                                                                        
                                        DefaultMutableTreeNode testNodeInCampaignTree = SalomeTMFPanels.getCampaignDynamicTree().findRemoveTestNodeInCampagneTree(pTest.getNameFromModel(), pTest.getTestListFromModel().getNameFromModel(),pTest.getTestListFromModel().getFamilyFromModel().getNameFromModel(),campaign.getNameFromModel() ,false);
                                        if (testNodeInCampaignTree != null){
                                            DefaultMutableTreeNode testListParent = (DefaultMutableTreeNode)testNodeInCampaignTree.getParent();
                                            SalomeTMFPanels.getCampaignDynamicTree().removeNode(testNodeInCampaignTree);
                                            if (testListParent.getChildCount() == 0) {
                                                DefaultMutableTreeNode familyParent = (DefaultMutableTreeNode)testListParent.getParent();
                                                SalomeTMFPanels.getCampaignDynamicTree().removeNode(testListParent);
                                                //DataModel.deleteTestListFromCampaign((TestList)testListParent.getUserObject(),campaign);
                                                if (familyParent.getChildCount() == 0) {
                                                    SalomeTMFPanels.getCampaignDynamicTree().removeNode(familyParent);
                                                    //DataModel.deleteFamilyFromCampaign((Family)familyParent.getUserObject(), campaign);
                                                }
                                            }
                                        }
                                        //Si la campagne traitee est la campagne courant, nettoyage de la table des r?sultats d'ex?cutions
                                        if (DataModel.getCurrentCampaign() != null && DataModel.getCurrentCampaign().equals(campaign) ) {       
                                            DataModel.getExecutionResultTableModel().clearTable();
                                        }
                                    }
                                }
                                //DataModel.getParameterTableModel().removeData(selectedRowIndex);
                                //DataModel.getParameterTableModel().removeData(sorter.modelIndex(selectedRows[selectedRowIndex]));
                                                        
                                                        
                            } catch (Exception exception) {
                                Tools.ihmExceptionView(exception);
                            }
                        }
                                        
                                        
                    } else {
                                                
                        try {
                            // BdD
                            Parameter pParam = DataModel.getCurrentProject().getParameterFromModel(paramName);
                            DataModel.getCurrentProject().deleteParamInDBndModel(pParam);
                            //DataModel.getCurrentProject().getParameter(paramName).deleteFromDB();
                                                        
                            // IHM
                            //DataModel.getParameterTableModel().removeData(sorter.modelIndex(selectedRows[selectedRowIndex]));
                            //DataModel.getCurrentProject().removeParameter(paramName);
                                                        
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    }
                }
                                
                //IHM
                try {
                    DataModel.getParameterTableModel().clearTable();
                    Vector projectParam  = DataModel.getCurrentProject().getParametersWrapperFromDB();
                    for (int m = 0; m < projectParam.size(); m++) {
                        ParameterWrapper paramBdd = (ParameterWrapper) projectParam.get(m);
                        Parameter param =  new Parameter(paramBdd);
                        ArrayList data = new ArrayList();
                        data.add(param.getNameFromModel());
                        data.add(param.getDescriptionFromModel());
                        DataModel.getParameterTableModel().addRow(data);
                                                
                        DataModel.getTestParameterTableModel().clearTable();
                        Test test = DataModel.getCurrentTest();
                        if (test != null){
                            ArrayList testParameterList = test.getParameterListFromModel();
                            for (int i =0; i < testParameterList.size(); i++) {
                                Parameter paramTest = (Parameter)test.getParameterListFromModel().get(i);
                                DataModel.getTestParameterTableModel().addValueAt(paramTest.getNameFromModel(), i, 0);
                                DataModel.getTestParameterTableModel().addValueAt(paramTest.getDescriptionFromModel(), i, 1);
                                upadateActionOfParameterView(test, paramTest.getNameFromModel());
                            }                                   
                        }
                        if (DataModel.getTestParameterTableModel()
                            .getRowCount() == 0) {
                            deleteParameterButton.setEnabled(false);
                        }
                                                
                    }
                } catch (Exception e) {
                    Tools.ihmExceptionView(e);
                }
            }
        }
    }
    
    
    
    public void parametersTablevalueChanged(ListSelectionEvent e) {
        
        if (e.getValueIsAdjusting())
            return;
        
        int nbOfSelectedRows = parametersTable.getSelectedRowCount();
        int selectedRow = parametersTable.getSelectedRow();
        if (selectedRow != -1) {
            if (nbOfSelectedRows != 1) {
                if (usingFonctionEnable) {
                    if (Permission.canDeleteTest()) deleteParameterButton.setEnabled(true);
                } else {
                    if (Permission.canDeleteCamp()) deleteParameterButton.setEnabled(true);
                }
                modifyParameterButton.setEnabled(false);
                        
            } else {
                if (usingFonctionEnable) {
                    if (Permission.canUpdateTest()) modifyParameterButton.setEnabled(true);
                    if (Permission.canDeleteTest()) deleteParameterButton.setEnabled(true);
                } else {
                    if (Permission.canUpdateCamp() || Permission.canExecutCamp()) modifyParameterButton.setEnabled(true);
                    if (Permission.canDeleteCamp()) deleteParameterButton.setEnabled(true);
                }
            }
        } else {
            deleteParameterButton.setEnabled(false);
            modifyParameterButton.setEnabled(false);
        }
        
    }
    
} // Fin de la classe ManualParameterView
