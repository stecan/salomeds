/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class AssignedCamPanel extends JPanel implements IAssignedCampAction,  ListSelectionListener, TreeSelectionListener, ActionListener {
        
    JTable assigendTable;
    TableSorter  sorter;
    DynamicTree campaignDynamicTree ;
    Campaign pCamp = null;
    Vector ligneModel = new Vector();
    SimpleTableModel model;
    JButton assignButton;
    Hashtable userByID = null;
    Vector userList = null;
        
        
    public AssignedCamPanel(){
        super(new BorderLayout());
        //initData();
        initComponent();
    }
        
        
    void initData(){
        userByID = new Hashtable();
        userList = new Vector();
        Project current_project = DataModel.getCurrentProject();
        try {
            Vector users_wrapper = current_project.getAllUsersWrapper();
            for (int i=0; i<users_wrapper.size() ; i++) {
                User user = new User((UserWrapper)users_wrapper.elementAt(i));
                userList.add(user);
                //Util.debug("-----------> Add user " + user.getNameFromModel() + " ID = " +user.getIdBdd() + " <------------------");
                userByID.put(new Integer(user.getIdBdd()), user);
            }
        } catch (Exception e) {
            Util.err(e);
        }
    }
        
    void initComponent(){
        model = new SimpleTableModel();
        model.addColumn(Language.getInstance().getText("Utilisateurs"));
        model.addColumn(Language.getInstance().getText("Familles"));
        model.addColumn(Language.getInstance().getText("Suites"));
        model.addColumn(Language.getInstance().getText("Tests"));
        sorter = new TableSorter(model);
                
        assigendTable = new JTable(sorter);
                
        ListSelectionModel rowSM = assigendTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        setColumnSize(assigendTable);
        sorter.setTableHeader(assigendTable.getTableHeader());
                
        assigendTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane tablePane = new JScrollPane(assigendTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());
                
        add(tablePane, BorderLayout.CENTER);
                
                
        JPanel panelButton = new JPanel(new FlowLayout());
        assignButton = new JButton(Language.getInstance().getText("Assign_to"));
        assignButton.setEnabled(false);
        assignButton.addActionListener(this);
        panelButton.add(assignButton);
        add(panelButton, BorderLayout.SOUTH);
                
        campaignDynamicTree = (DynamicTree) SalomeTMFContext.getInstance().getUIComponent(UICompCst.CAMPAIGN_DYNAMIC_TREE);
        campaignDynamicTree.getTree().addTreeSelectionListener(this);
        DataModel.setAssignedCampAction(this);
    }
        
    @Override
    public void updateData(Campaign pCamp) {
        try {
            boolean isSorting = sorter.isSorting();
            int statusSort_col0 = -1;
            int statusSort_col1 = -1;
            int statusSort_col2 = -1;
            int statusSort_col3 = -1;
            if (isSorting) {
                statusSort_col0 = sorter.getSortingStatus(0);
                statusSort_col1 = sorter.getSortingStatus(1);
                statusSort_col2 = sorter.getSortingStatus(2);
                statusSort_col3 = sorter.getSortingStatus(3);
            }
            model.clearTable();
            ligneModel.clear();
            ArrayList testList = pCamp.getTestListFromModel();
            for (int i = 0; i < testList.size(); i++) {
                Test pTest = (Test) testList.get(i);
                ligneModel.add(pTest);
                Vector data = new Vector();
                int assigendID = pCamp.getAssignedUserID(pTest);
                User pUser = (User) userByID.get(new Integer(assigendID));
                Util.debug("User assigned is = " + pUser
                           + " , assigendID = " + assigendID);
                data.add(((User) userByID.get(new Integer(assigendID)))
                         .getLoginFromModel()); // TO CHANGE
                data.add(pTest.getTestListFromModel().getFamilyFromModel()
                         .getNameFromModel());
                data.add(pTest.getTestListFromModel().getNameFromModel());
                data.add(pTest.getNameFromModel());

                model.insertRow(i, data);
            }
            setColumnSize(assigendTable);
            if (isSorting) {
                sorter.setSortingStatus(0, statusSort_col0);
                sorter.setSortingStatus(1, statusSort_col1);
                sorter.setSortingStatus(2, statusSort_col2);
                sorter.setSortingStatus(3, statusSort_col3);
            }
        } catch (Exception e) {
            Util.log("[AssignedCamPanel->updateData] error on loading assigned tester (Data come from old version of Salome ?)");
            Util.err(e);
        }
    }
        
        
    /**
     * redefine size of table columns
     */
    public void setColumnSize(JTable table){   
        FontMetrics fm = table.getFontMetrics(table.getFont());
        for (int i = 0 ; i < table.getColumnCount() ; i++)
            {
                int max = 0;
                for (int j = 0 ; j < table.getRowCount() ; j++)
                    {
                        int taille = fm.stringWidth((String)table.getValueAt(j,i));
                        if (taille > max)
                            max = taille;
                    }
                String nom = (String)table.getColumnModel().getColumn(i).getIdentifier();
                int taille = fm.stringWidth(nom);
                if (taille > max)
                    max = taille;
                table.getColumnModel().getColumn(i).setPreferredWidth(max+10);
            }
    }
    /*********************** Interfaces ***************************/
    /********************** ListSelectionListener  ***************************/
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(assigendTable.getSelectionModel())){
            assigendTableValueChanged(e);
        }
    }
        
    void assigendTableValueChanged(ListSelectionEvent e) {
                
        if (e.getValueIsAdjusting())
            return;
                
        int nbOfSelectedRows = assigendTable.getSelectedRowCount();
        int selectedRow = assigendTable.getSelectedRow();
        if (selectedRow != -1) {
            assignButton.setEnabled(true);
            if (nbOfSelectedRows != 1) {
                /* Multi Selection */
                                
            } else {
                /* Mono Selection */
                                
            }
        } else {
            /* Pas de Selection */
            assignButton.setEnabled(false);
        }
                
    }
    /**********************  TreeSelectionListener ***************************/
    @Override
    public void valueChanged(TreeSelectionEvent e) {
        if (userByID == null ){
            initData();
        }
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) campaignDynamicTree.getTree().getLastSelectedPathComponent();
        Campaign pCampTmp = getSelectedCamp(node);
        if (pCampTmp == null){
            return;
        }
        if (pCamp == null){
            pCamp = pCampTmp;
            updateData(pCamp);
        } else if (pCamp.getIdBdd() != pCampTmp.getIdBdd()){
            pCamp = pCampTmp;
            updateData(pCamp);
        }
    }
        
    Campaign getSelectedCamp(DefaultMutableTreeNode node){
        Campaign pCamp = null;
                
        if (node == null){
            return null;
        }
        if (node.getUserObject() instanceof Campaign){
            pCamp = (Campaign) node.getUserObject();
        }
        return pCamp;
    }
        
    /********************************** Implements ActionListener *********************************/
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(assignButton)){
            assignPerformed();
        }
    }
        
    void assignPerformed(){
        AskChooseAssignedUser pAskChooseAssignedUser = new AskChooseAssignedUser(userList, false);
        pAskChooseAssignedUser.setVisible(true);
        User selectedUser = pAskChooseAssignedUser.getSelectedUser();
        Util.debug("Selected User is " +  selectedUser);
        if (selectedUser != null){
            assign(selectedUser);
        }
    }
        
    void assign(User selectedUser){
        int[] selectedRows = assigendTable.getSelectedRows();
        boolean doUpdateData = true;
        for (int i = 0 ; i < selectedRows.length ;  i++){
            try {
                int modeIndex = sorter.modelIndex(selectedRows[i]);
                Test pTest = (Test) ligneModel.elementAt(modeIndex);
                Util.debug("Change assignation for "+ pTest.getNameFromModel()+ " with user " +  selectedUser + " in camp " + pCamp.getNameFromModel());
                pCamp.updateTestAssignationInDBAndModel(pTest.getIdBdd(), selectedUser.getIdBdd());
                //assigendTable.setValueAt(selectedUser.getLoginFromModel(),modeIndex ,0);
                if (selectedRows.length == 1){
                    sorter.setValueAt(selectedUser.getLoginFromModel(),selectedRows[i] ,0);
                    doUpdateData = false;
                } 
            } catch (Exception e){
                Util.err(e);
            }
        }
        if (doUpdateData){
            updateData(pCamp);
        }
                
    }
        
    class SimpleTableModel extends DefaultTableModel {
        @Override
        public boolean isCellEditable(int row, int col) {
            return false;
        } // Fin de la m?thode isCellEditable/2 
                
        public void clearTable() {
            int nbRow = getRowCount();
            for(int i=0; i < nbRow; i++) {
                removeRow(0);
            }
            fireTableStructureChanged();
        } 
    }
        
}
