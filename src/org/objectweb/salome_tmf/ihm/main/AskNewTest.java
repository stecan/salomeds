/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

/**
 * Classe permettant de creer une vue pour entrer un noouveau test
 * @author teaml039
 * @version 0.1
 */
public class AskNewTest extends JDialog {

    /**
     * Le nom du test
     */
    JTextField testNameField;

    /**
     * Le concepteur du test
     */
    JComboBox conceptorNameList;

    /**
     * Types de tests
     */
    String[] types;
    //String[] types = {"Manuel", "Automatique"};

    //Extension[] tabExtension;
    /**
     * Liste permettant de choisir le test
     */
    JComboBox typesList;

    /**
     * La decsription du test
     */
    JTextArea descriptionArea;

    /**
     * Le nouveau test
     */
    Test test;

    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/

    /**
     * Constructeur de la fenetre.
     * @param textToBePrompt chaine correspondant a la demande.
     */
    public AskNewTest(Component parent, String textToBePrompt) {

        //Pour bloquer le focus sur la boite de dialogue
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        initType();
        JPanel page = new JPanel();
        testNameField = new JTextField(10);
        //descriptionArea = new JTextArea();
        descriptionArea = new JTextArea(10,20);
        typesList = new JComboBox(types);
        conceptorNameList = new JComboBox();
        JLabel testNameLabel = new JLabel(Language.getInstance().getText("Nom"));
        JLabel conceptorNameLabel = new JLabel(Language.getInstance().getText("Nom_du_concepteur_"));
        //JLabel descriptionLabel = new JLabel("Description");

        JPanel giveName = new JPanel();
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(testNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(testNameField);

        JPanel giveConceptorName = new JPanel();
        giveConceptorName.setLayout(new BoxLayout(giveConceptorName, BoxLayout.X_AXIS));
        giveConceptorName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveConceptorName.add(Box.createHorizontalGlue());
        giveConceptorName.add(conceptorNameLabel);
        giveConceptorName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveConceptorName.add(conceptorNameList);

        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));



        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(giveName);
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(typesList);
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(descriptionScrollPane);


        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    if (testNameField.getText() != null && !testNameField.getText().trim().equals("")) {
                        if (((String)typesList.getSelectedItem()).equalsIgnoreCase(Language.getInstance().getText("Manuel"))) {
                            test = new ManualTest("","");
                        } else {
                            test = new AutomaticTest("","", (String)typesList.getSelectedItem());
                        }
                        test.updateInModel(testNameField.getText().trim(), descriptionArea.getText());
                        test.setConceptorLoginInModel(DataModel.getCurrentUser().getLoginFromModel());
                        AskNewTest.this.dispose();
                    } else {
                        JOptionPane.showMessageDialog(AskNewTest.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_au_test_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });

        testNameField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                }
            });

        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AskNewTest.this.dispose();
                }
            });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);

        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);

        //this.setLocation(400,300);
        this.setTitle(textToBePrompt);
        /*this.pack();
          this.setLocationRelativeTo(this.getParent());
          this.setVisible(true);**/
        centerScreen();

    } // Fin du constructeur AskName/1


    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/

    /**
     * Methode qui retourne le test cree
     * @return le test, <code>null</code> si aucun test cree
     */
    public Test getTest() {
        return test;
    } // Fin de la methode getTest/0

    private void initType(){
        int i = 1;
        types = new String[SalomeTMFContext.getInstance().associatedTestDriver.size()+1];
        types[0] = Language.getInstance().getText("Manuel");
        Enumeration e = SalomeTMFContext.getInstance().associatedTestDriver.keys();
        while (e.hasMoreElements()){
            Extension testDriverExt = (Extension) e.nextElement();
            //tabExtension[i-1] = testDriverExt;
            types[i] = testDriverExt.getId();
            i++;
        }
    }

} // Fin de la classe AskNewTest
