/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.objectweb.salome_tmf.ihm.main;

import java.net.URL;
import java.net.URLConnection; 
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.tools.sql.DirectConnect;


/**
 *
 * @author stefan
 */
public class PolarionLink {

    private String fileName = "";
    private String lineNumber = "";
    private int idProject;
    private String link;
    private Config pConfig;
    private int polarionRevision;
    private String polarionPrefix;
    
    public PolarionLink(int idProject) {
        pConfig = new Config();
        this.idProject = idProject;
        List<String> hyperlinks = pConfig.getPolarionHyperlinks();
        for (String hyperlink : hyperlinks) {
            try {
                URL url = new URL(hyperlink);

                URLConnection connection = url.openConnection();
                connection.connect();   
                link = hyperlink;   // Connection available. Select link
                break;
             } catch (Exception e){
                 // No connection available. Select first link.
                 link = hyperlinks.get(0); 
            }              
        }
        if (link.endsWith("/")) {
            // it's OK
        } else {
            link = link + "/";
        }
        polarionRevision = DirectConnect.getPolarionRevision(idProject);
        polarionPrefix = DirectConnect.getPolarionPrefix(idProject);
    }
    
   
    /**
     * This routine converts a string of the form 
     *        XGPF-53537 - This is a requirement
     * to a string which represents a real hyperlink to the Polarion
     * workitem
     * 
     * @param requirementString String of the form "XGPF-53537 - This is a 
     *                          requirment"
     * @return link hyperlink to the polarion workitem
     */
    public String convert (String requirementString) {
        String key = "";
        String regexp;
        Pattern pattern;
        Matcher matcher = null;
        
        regexp = polarionPrefix + "\\d+";
        pattern = Pattern.compile(regexp, Pattern.CASE_INSENSITIVE);
        matcher = pattern.matcher(requirementString);
        if (matcher.find()) {
            key = matcher.group();
        }
        
        String hyperlink;
        if (polarionRevision == -1) {
            // error or head revision: Do not print a tailing revision number.
            hyperlink = link + "polarion/#/project/" + 
                    polarionPrefix.substring(0, polarionPrefix.length()-1) + 
                    "/workitem?id=" + key;
        } else {
            hyperlink = link + "polarion/#/project/" + 
                    polarionPrefix.substring(0, polarionPrefix.length()-1) + 
                    "/workitem?id=" + key + "&revision=" +
                    polarionRevision;
        }
        return  hyperlink;
    }
    
    public String getFileName () {
        return fileName;
    }
    
    public String getLineNumber() {
        return lineNumber;
    }
    
    public void setFileName (String name) {
        fileName = name;
    }
    
    public void setLineNumber (String line) {
        lineNumber = line;
    }

}
