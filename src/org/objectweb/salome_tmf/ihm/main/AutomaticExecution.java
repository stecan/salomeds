/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.datawrapper.TestMethods;
import org.objectweb.salome_tmf.ihm.models.ScriptExecutionException;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * @author teaml039
 */
public class AutomaticExecution extends JDialog implements ApiConstants, Runnable, IHMConstants {
    
    /**
     * Label du test
     */
    JLabel testName;
    
    /**
     * Vrai si l'utilisateur a interrompu l'ex?cution
     */
    boolean executionInterruption;
    
    /**
     * Label du r?sultat
     */
    JLabel resultLabel;
    
    Execution exec;
    
    ArrayList automaticTestList;
    
    // REMOVE FOR V2 EXEC
    //ArrayList execResultList;
    
    
    Thread t;
    
    //ExecutionView execView;
    
    boolean stop_exec = false;
    /**
     * Constructeur.
     * @param list
     */
    ExecutionResult finalExecResult;
    
    boolean finished = false;
    
    public AutomaticExecution(ArrayList autoTestList, Execution execution,  ExecutionResult finalExecResult) {
        
        
        
        super(SalomeTMFContext.getInstance().ptrFrame);
        this.finalExecResult = finalExecResult;
        resultLabel = new JLabel();
        exec = execution;
        finished = false;
        // REMOVE FOR V2 EXEC
        //this.execResultList = executionResultList;
        
        this.automaticTestList =autoTestList;
        //this.execView = execView;
        testName = new JLabel("" );
        testName.setBackground(Color.WHITE);
        
        JLabel runLabel = new JLabel(Language.getInstance().getText("est_en_cours_d_execution"));
        runLabel.setBackground(Color.WHITE);
        
        // Bouton d'annulation
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TestMethods.annulScript();
                    stop_exec = true;
                    finished = true;
                    AutomaticExecution.this.finalExecResult.setExecutionStatusInModel(INTERRUPT);
                    AutomaticExecution.this.dispose();
                }
            });
        
        // Bouton d'annulation
        JButton termineButton = new JButton(Language.getInstance().getText("Terminer"));
        termineButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    TestMethods.annulScript();
                    stop_exec = true;
                    finished = true;
                    AutomaticExecution.this.dispose();
                }
            });
        
        JPanel cancelButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        cancelButtonPanel.add(cancelButton);
        cancelButtonPanel.add(termineButton);
        cancelButtonPanel.setBackground(Color.WHITE);
        
        
        // Modification du curseur
        try {
            Cursor c = new Cursor(Cursor.WAIT_CURSOR);
            this.setCursor(c);
        } catch (Exception e) {
            
        }
        // Images des engrenages
        JLabel imageLabel = new JLabel();
        imageLabel.setIcon(Tools.createAppletImageIcon(PATH_TO_ENGRENAGE_ICON,""));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel, BoxLayout.Y_AXIS));
        textPanel.add(testName);
        textPanel.add(Box.createRigidArea(new Dimension(10,5)));
        textPanel.add(runLabel);
        textPanel.add(Box.createRigidArea(new Dimension(10,5)));
        textPanel.add(resultLabel);
        textPanel.add(Box.createRigidArea(new Dimension(10,5)));
        textPanel.add(cancelButtonPanel);
        textPanel.setBackground(Color.WHITE);
        
        
        
        JPanel page = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        page.add(imageLabel);
        page.add(Box.createRigidArea(new Dimension(10,1)));
        page.add(textPanel);
        page.setBackground(Color.WHITE);
        page.setPreferredSize(new Dimension(500,100));
        
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    TestMethods.annulScript();
                    finished = true;
                    stop_exec = true;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        this.setTitle(Language.getInstance().getText("Execution_d_un_test_automatique_exec_en_cours__") + execution.getNameFromModel() + ")");
        /**this.setLocationRelativeTo(this.getParent()); 
           this.pack();
        */
        centerScreen();
    }
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        //this.setVisible(true); 
        //requestFocus();
    }
    
    public void lauchAutomaticExecution() {
        t = new Thread(this);
        t.start();
    }
    
    // methode de l'interface Runnable
    // lance un nouveau thread qui va executer le code de la methode longTraitement
    @Override
    public void run() {
        if (SalomeTMFContext.getInstance().getSalomeFrame() != null){
            this.setVisible(true);
        }
        stop_exec = false;
        this.automaticExecution();
    }
    
    public void automaticExecution() {
        ExecutionResult execResult = finalExecResult;
        String result = null;
        int i = 0;
        while (i < automaticTestList.size() && !stop_exec) {
            AutomaticTest test = (AutomaticTest)automaticTestList.get(i);
            ExecutionTestResult execTestResult = null;
                
            execTestResult = finalExecResult. getExecutionTestResultFromModel(test);
            if (execTestResult == null){
                execTestResult = new ExecutionTestResult(test);
            }
            
            //Maj de la fen?tre
            this.setTestNameLabel(((AutomaticTest)automaticTestList.get(i)).getNameFromModel());
            this.setTestResultLabel(Language.getInstance().getText("Resultat__"));
            Script script = test.getScriptFromModel();
            if (script != null && !script.getNameFromModel().equals("")) {
                try {
                    execTestResult.setStatusInModel("");
                    result = TestMethods.callScript(exec.getDataSetFromModel().getParametersHashMapFromModel(), script, test, DataModel.getCurrentCampaign(), DataModel.getCurrentProject(), exec.getEnvironmentFromModel(), execResult, exec, execTestResult);
                    execTestResult.setStatusInModel(result);
                } catch (ScriptExecutionException see) {
                    // ne peut pas se produire en principe
                    TestMethods.fileCreationAndAttachToExecResult(execResult, execTestResult, see.getMessage(),exec, Language.getInstance().getText("erreur_exec_") + test.getNameFromModel());
                }
                
                
                this.setTestResultLabel(Language.getInstance().getText("Resultat__") + result);
                execResult.setTestExecutionTestResultInModel(((AutomaticTest)automaticTestList.get(i)), execTestResult, exec.getCampagneFromModel().getTestOrderInModel(((AutomaticTest)automaticTestList.get(i))));
            } else {
                this.setTestResultLabel(Language.getInstance().getText("Resultat__") + SUCCESS);
                execTestResult.setStatusInModel(SUCCESS);
                execResult.setTestExecutionTestResultInModel(((AutomaticTest)automaticTestList.get(i)), execTestResult, exec.getCampagneFromModel().getTestOrderInModel(((AutomaticTest)automaticTestList.get(i))));
                
            }
            
            i++;
        }
        
        /*if (stop_exec == true) {
          execResult.setExecutionStatus(INTERRUPT);
          }
        */
        
        finished = true;
        this.dispose();
    }
    
    
    /**
     * @param label
     */
    public void setTestNameLabel(String text) {
        testName.setText(text);
    }
    
    /**
     * @param label
     */
    public void setTestResultLabel(String text) {
        resultLabel.setText(text);
    }
    
    public boolean isFinished() {
        return finished;
    }
    
    public boolean isStoped() {
        return stop_exec;
    }
    
    public void setFinished(boolean b) {
        finished = b;
    }
    
} // Fin de la classe AutomaticExecution
