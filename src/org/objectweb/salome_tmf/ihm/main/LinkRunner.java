/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.objectweb.salome_tmf.ihm.main;

import java.awt.Desktop;
import java.net.URI;
import java.util.concurrent.ExecutionException;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 *
 * @author stecan
 */
public class LinkRunner extends SwingWorker<Void, Void> {

    private final URI uri;

    public LinkRunner(URI u) {
        if (u == null) {
            throw new NullPointerException();
        }
        uri = u;
    }

    @Override
    protected Void doInBackground() throws Exception {
        Desktop desktop = java.awt.Desktop.getDesktop();
        desktop.browse(uri);
        return null;
    }

    @Override
    protected void done() {
        try {
            get();
        } catch (ExecutionException | InterruptedException ee) {
            handleException(uri, ee);
        }
    }

    private void handleException(URI u, Exception e) {
        JOptionPane.showMessageDialog(null, 
                Language.getInstance().getText("BrowserProblem"),
                Language.getInstance().getText("Erreur_"),
                JOptionPane.ERROR_MESSAGE);
    }
}
