/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer deux
 * informations.
 */
public class AskTwoThings extends JDialog {
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    
    /**
     * Constructeur de la fenetre permettant de demander deux informations.
     * @param textToBePromptFirst texte pour demander la premiere information
     * @param textToBePromptSecond texte pour demander la seconde information
     */
    public AskTwoThings(String textToBePromptFirst, String textToBePromptSecond, Frame owner) {
        super(owner,true);
        
        JPanel page = new JPanel();
        
        //JLabel text = new JLabel("");
        
        JLabel promptFirst = new JLabel(textToBePromptFirst);
        JLabel promptSecond = new JLabel(textToBePromptSecond);
        
        JTextField textFirst = new JTextField(10);
        JTextField textSecond = new JTextField(10);
        
        JPanel onlyTextPanel = new JPanel();
        onlyTextPanel.setLayout(new BoxLayout(onlyTextPanel,BoxLayout.Y_AXIS));
        onlyTextPanel.add(Box.createRigidArea(new Dimension(0,10)));
        onlyTextPanel.add(promptFirst);
        onlyTextPanel.add(promptSecond);
        
        JPanel onlyTextFieldPanel = new JPanel();
        onlyTextFieldPanel.setLayout(new BoxLayout(onlyTextFieldPanel,BoxLayout.Y_AXIS));
        onlyTextFieldPanel.add(Box.createRigidArea(new Dimension(0,10)));
        onlyTextFieldPanel.add(textFirst);
        onlyTextFieldPanel.add(textSecond);
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.X_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(onlyTextPanel);
        textPanel.add(Box.createRigidArea(new Dimension(10,10)));
        textPanel.add(onlyTextFieldPanel);
        
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AskTwoThings.this.dispose();
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AskTwoThings.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);
        
        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        //this.setLocation(500,400);
        this.setTitle(Language.getInstance().getText("Informations"));
        /*this.pack();
          this.setLocationRelativeTo(this.getParent()); 
          this.setVisible(true);*/
        centerScreen();
        
    } // Fin du constructeur AskTwoThings/2
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
} // Fin de la classe AskTwoThings
