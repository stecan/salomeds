/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Iterator;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.TableColumn;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DataSetCellEditor;
import org.objectweb.salome_tmf.ihm.models.ParameterTableModel;
import org.objectweb.salome_tmf.ihm.models.TableSorter;

//import org.objectweb.salome_tmf.ihm.datawrapper.TestData;


public class AskNewDataSet extends JDialog implements ActionListener, WindowListener{
    
    /**
     * La zone texte pour saisir le nom du jeu de donnees
     */
    JTextField nameField;
    
    /**
     * La zone texte pour saisir la description
     */
    JTextArea descriptionArea;
    
    /**
     * Le nouvel jeu de donnees
     */
    DataSet dataSet;
    
    ParameterTableModel parametersTableModel;
    TableSorter sorter;
    
    /**
     * Table des parametres
     */
    JTable parametersTable;
    
    DataSetCellEditor dataCellEditor;
    
    int row  = 0;
    
    /**
     * Button
     */
    JButton okButton;
    JButton cancelButton;
    //JButton updateButton;
    /**************************************************************************/
    /**                                                 CONSTRUCTEUR                                                            ***/
    /**************************************************************************/
    
    public AskNewDataSet(DataSet oldDataSet) {
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        nameField = new JTextField(10);
        descriptionArea = new JTextArea(15, 40);
        parametersTableModel = new ParameterTableModel();
        parametersTable = new JTable();
        JLabel testNameLabel = new JLabel(Language.getInstance().getText("Nom_du_jeu_de_donnees__"));
        
        JPanel giveName = new JPanel();
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(testNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(nameField);
        
        descriptionArea.setPreferredSize(new Dimension(100,100));
        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        descriptionScrollPane.setPreferredSize(new Dimension(100,150));
        
        
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Valeur"));
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        
        sorter = new TableSorter(parametersTableModel);
        parametersTable.setModel(sorter);
        sorter.setTableHeader(parametersTable.getTableHeader());
        
        TableColumn col = parametersTable.getColumnModel().getColumn(1);
        dataCellEditor = new DataSetCellEditor();
        col.setCellEditor(dataCellEditor);
        
        JScrollPane parametersScrollPane = new JScrollPane(parametersTable);
        parametersScrollPane.setPreferredSize(new Dimension(450,150));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(giveName);
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(descriptionScrollPane);
        textPanel.add(Box.createRigidArea(new Dimension(1,30)));
        textPanel.add(parametersScrollPane);
        
        
        okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(this);
        
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
        
        /*if (oldDataSet != null){
          updateButton = new JButton("Update");
          updateButton.addActionListener(this);
          }*/
        JPanel buttonPanel = new JPanel(new FlowLayout());
        
       
        buttonPanel.add(cancelButton);
        /*if (oldDataSet != null){
          buttonPanel.add(updateButton);
          }*/
        buttonPanel.add(okButton);
        textPanel.add(buttonPanel);
        
        //Initialisation des donnees de la fenetre
        initData(oldDataSet);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(textPanel, BorderLayout.CENTER);
        
        
        parametersScrollPane.setPreferredSize(new Dimension(450,150));
        this.addWindowListener(this);
        
        //this.setLocation(350,200);
        this.setTitle(Language.getInstance().getText("Ajouter_un_jeu_de_donnees"));
        /*this.setLocationRelativeTo(this.getParent()); 
          this.pack();
          this.setVisible(true);*/
        centerScreen();
        
    } // Fin du constructeur AskNewDataSet/1
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    
    public AskNewDataSet() {
        this(null);
    } // Fin du constructeur AskNewDataSet/0
    
    
    /**
     *
     * @return
     */
    public DataSet getDataSet() {
        return dataSet;
    } // Fin de la methode getDataSet/0
    
    /**
     *
     * @param oldDataSet
     */
    private void initData(DataSet oldDataSet) {
        if (oldDataSet != null) {
            dataSet = oldDataSet;
            nameField.setText(oldDataSet.getNameFromModel());
            descriptionArea.setText(oldDataSet.getDescriptionFromModel());
            //int i = 0;
            Set keysSet = oldDataSet.getParametersHashMapFromModel().keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                String paramName = (String)iter.next();
                Parameter param = DataModel.getCurrentProject().getParameterFromModel(paramName);
                //parametersTableModel.addValueAt(param.getNameFromModel(), i, 0);
                parametersTableModel.addValueAt(param.getNameFromModel(), row, 0);
                JTextField valueField = new JTextField();
                valueField.setText(oldDataSet.getParameterValueFromModel(param.getNameFromModel()));
                //parametersTableModel.addValueAt(oldDataSet.getParameterValueFromModel(param.getNameFromModel()), i, 1);
                //parametersTableModel.addValueAt(param.getDescriptionFromModel(), i, 2);
                //i++;
                parametersTableModel.addValueAt(oldDataSet.getParameterValueFromModel(param.getNameFromModel()), row, 1);
                parametersTableModel.addValueAt(param.getDescriptionFromModel(), row, 2);
                row ++;
            }
        } else {
            dataSet = new DataSet("","");
            //int row  = 0;
            for (int j = 0; j < DataModel.getCurrentCampaign().getTestListFromModel().size(); j++) {
                Test test = (Test)DataModel.getCurrentCampaign().getTestListFromModel().get(j);
                for (int i = 0; i < test.getParameterListFromModel().size(); i++) {
                    if (!dataSet.getParametersHashMapFromModel().containsKey(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel())) {
                        parametersTableModel.addValueAt(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(), row, 0);
                        
                        //parametersTableModel.addValueAt("", row, 1);
                        if (Api.isDYNAMIC_VALUE_DATASET()) {
                            parametersTableModel.addValueAt(DataConstants.PARAM_VALUE_FROM_ENV, row, 1);
                        } else {
                            parametersTableModel.addValueAt("", row, 1);
                        }
                        parametersTableModel.addValueAt(((Parameter)test.getParameterListFromModel().get(i)).getDescriptionFromModel(), row, 2);
                        row++;
                        //dataSet.addParameterValueInModel(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(), "");
                        if (Api.isDYNAMIC_VALUE_DATASET()) {
                            dataSet.addParameterValueInModel(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(),DataConstants.PARAM_VALUE_FROM_ENV);
                        } else {
                            dataSet.addParameterValueInModel(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(), "");
                        }
                    }
                }
            }
            
        }
    } // Fin de la methode initData/1
    
    private void createAndQuit() {
        dataSet.updateInModel(nameField.getText().trim(), descriptionArea.getText());
        
        dataSet.getParametersHashMapFromModel().clear();
        
        for (int i = 0; i < parametersTableModel.getRowCount(); i++) {
            
            if (sorter.getValueAt(i,0) != null && !sorter.getValueAt(i,0).equals("") && !nameField.getText().trim().equals(ApiConstants.EMPTY_NAME)) {
                if (!parametersTable.isCellSelected(i,1)) {
                    dataSet.addParameterValueInModel(DataModel.getCurrentProject().getParameterFromModel((String)sorter.getValueAt(i,0)).getNameFromModel(),(String)sorter.getValueAt(i,1));
                } else {
                    dataSet.addParameterValueInModel(DataModel.getCurrentProject().getParameterFromModel((String)sorter.getValueAt(i,0)).getNameFromModel(),(String)dataCellEditor.getCellEditorValue());
                }
                
            }
        }
        AskNewDataSet.this.dispose();
    }
    
    // Event //
    @Override
    public void actionPerformed(ActionEvent evt){
        if (evt.getSource().equals(okButton)){
            okPerformed();
        } else if (evt.getSource().equals(cancelButton)){
            cancelPerformed();
        } /*else if (evt.getSource().equals(updateButton)){
            updatePerformed();
            }*/
    }
    
    void okPerformed(){
        if (!nameField.getText().trim().equals("")) {
            if (DataModel.getCurrentCampaign().getDataSetFromModel(nameField.getText().trim()) == null && !nameField.getText().trim().equals(ApiConstants.EMPTY_NAME) && !DataSet.isInBase(DataModel.getCurrentCampaign(), nameField.getText().trim())) {
                createAndQuit();
            } else if (DataModel.getCurrentCampaign().getDataSetFromModel(nameField.getText().trim()) != null && DataModel.getCurrentCampaign().getDataSetFromModel(nameField.getText().trim()).equals(dataSet)){
                createAndQuit();
            } else {
                JOptionPane.showMessageDialog(AskNewDataSet.this,
                                              Language.getInstance().getText("Ce_nom_d_environnement_existe_deja_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            }
            
        } else {
            JOptionPane.showMessageDialog(AskNewDataSet.this,
                                          Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_au_jeu_de_donnees_"),
                                          Language.getInstance().getText("Attention_"),
                                          JOptionPane.WARNING_MESSAGE);
        }
    }
    
    void cancelPerformed(){
        dataSet = null;
        AskNewDataSet.this.dispose();
    }
    
    /*void updatePerformed(){
    //int row  = 0;
    for (int j = 0; j < DataModel.getCurrentCampaign().getTestListFromModel().size(); j++) {
    Test test = (Test)DataModel.getCurrentCampaign().getTestListFromModel().get(j);
    for (int i = 0; i < test.getParameterListFromModel().size(); i++) {
    if (!dataSet.getParametersHashMapFromModel().containsKey(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel())) {
    parametersTableModel.addValueAt(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(), row, 0);
                    
    parametersTableModel.addValueAt("", row, 1);
                    
    parametersTableModel.addValueAt(((Parameter)test.getParameterListFromModel().get(i)).getDescriptionFromModel(), row, 2);
    row++;
    //dataSet.addParameterValueInModel(((Parameter)test.getParameterListFromModel().get(i)).getNameFromModel(), "");
    }
    }
    }
    }*/
    
    // Windows Listener
    @Override
    public void windowClosing(WindowEvent e) {
        dataSet = null;
    }
    @Override
    public void windowDeiconified(WindowEvent e) {
    }
    @Override
    public void windowOpened(WindowEvent e) {
    }
    @Override
    public void windowActivated(WindowEvent e) {
    }
    @Override
    public void windowDeactivated(WindowEvent e) {
    }
    @Override
    public void windowClosed(WindowEvent e) {
    }
    @Override
    public void windowIconified(WindowEvent e) {
    }
} // Fin de la classe AskNewDataSet
