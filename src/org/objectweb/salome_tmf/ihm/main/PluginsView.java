/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;


import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.java.plugin.Extension;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.RowTransferHandler;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.Common;


public class PluginsView extends javax.swing.JPanel implements ActionListener, ListSelectionListener {
    
    // Variables declaration
    JButton activateButton;
    JButton freezeButton;
    //MyTableModel pluginsTableModel = null;
    TableSorter  sorter = null;
    //TableSorter  sorter2 = null;
    JTable pluginsTable = null;
    ListSelectionModel rowSM = null;
    Extension currentExt = null;
    Common currentCommonPlg = null;
    String currentPlgID = null;
    PluginManager pluginMgr = null;
    Map extensions = new HashMap();
    int selectedRow = -1;
    Vector commonExtensions = new Vector();
    Vector testDriverExtensions = new Vector();
    Vector scriptEngineExtensions = new Vector();
    Vector bugTrackingExtensions = new Vector();    
    Vector reqMgrExtensions = new Vector();
    Vector statistiquesExtensions = new Vector();
    
    //FOR TEST
    RowTransferHandler transferHandler;
   
    /** Creates new form PluginsView */
    public PluginsView(Vector commonExtensions,Vector testDriverExtensions, 
                       Vector scriptEngineExtensions,Vector bugTrackingExtensions,Vector reqMgrExtensions, PluginManager plgMgr) {
        
        pluginMgr = plgMgr;
        this.commonExtensions = commonExtensions;
        this.testDriverExtensions = testDriverExtensions;
        this.scriptEngineExtensions = scriptEngineExtensions;
        this.bugTrackingExtensions = bugTrackingExtensions;
        this.reqMgrExtensions = reqMgrExtensions;
                           
        activateButton = new JButton(Language.getInstance().getText("Activer"));
        activateButton.setEnabled(false);
        activateButton.addActionListener(this);
        
        freezeButton = new JButton(Language.getInstance().getText("Geler"));
        freezeButton.setEnabled(false);
        freezeButton.addActionListener(this);

        
        
        sorter = new TableSorter(createPluginTableModel(commonExtensions, testDriverExtensions, scriptEngineExtensions, plgMgr ));
        pluginsTable = new JTable(sorter);
        sorter.setTableHeader(pluginsTable.getTableHeader());
        //sorter.addMouseListenerToHeaderInTable(pluginsTable); 
         
        /*sorter = new MyTableSorter(createPluginTableModel(commonExtensions, testDriverExtensions, scriptEngineExtensions, plgMgr ));
          pluginsTable = new JTable(sorter);
          //sorter.setTableHeader(pluginsTable.getTableHeader());
          sorter.addMouseListenerToHeaderInTable(pluginsTable); 
        */  
        pluginsTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        pluginsTable.setTransferHandler(transferHandler);
        pluginsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
         
        
       
        JScrollPane pluginsTableScrollPane = new JScrollPane(pluginsTable);
        // Gestion des selection dans la table
        rowSM = pluginsTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        

        JPanel buttonsPanel = new JPanel();

        buttonsPanel.add(activateButton);
        buttonsPanel.add(freezeButton);
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        this.setLayout(new BorderLayout());
        this.add(BorderLayout.NORTH, buttonsPanel);
        this.add(BorderLayout.CENTER, pluginsTableScrollPane);
        
        // Mapping GUI Object for plugins//
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_BUTTONS_PANEL,buttonsPanel);
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_BUTTONS_PANEL);
                
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_TABLE,pluginsTable);
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_TABLE);
                
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_PANEL,this);
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_PANEL);
                       
    }
 
    /** Creates new form PluginsView */    
    public PluginsView(Vector commonExtensions,Vector testDriverExtensions,                        
                       Vector scriptEngineExtensions,Vector bugTrackingExtensions,Vector statistiquesExtensions,Vector reqMgrExtensions, PluginManager plgMgr) {                
        pluginMgr = plgMgr;        
        this.commonExtensions = commonExtensions;        
        this.testDriverExtensions = testDriverExtensions;        
        this.scriptEngineExtensions = scriptEngineExtensions;       
        this.bugTrackingExtensions = bugTrackingExtensions;        
        this.statistiquesExtensions = statistiquesExtensions;        
        this.reqMgrExtensions = reqMgrExtensions;                     
        
        activateButton = new JButton(Language.getInstance().getText("Activer"));        
        activateButton.setEnabled(false);        
        activateButton.addActionListener(this);                
        
        freezeButton = new JButton(Language.getInstance().getText("Geler"));        
        freezeButton.setEnabled(false);        
        freezeButton.addActionListener(this);                        
        
        sorter = new TableSorter(createPluginTableModel(commonExtensions, testDriverExtensions, scriptEngineExtensions, plgMgr ));        
        pluginsTable = new JTable(sorter);        
        sorter.setTableHeader(pluginsTable.getTableHeader());                
        
        pluginsTable.setPreferredScrollableViewportSize(new Dimension(700, 350));        
        pluginsTable.setTransferHandler(transferHandler);        
        pluginsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);                   
        
        JScrollPane pluginsTableScrollPane = new JScrollPane(pluginsTable);       
        // Gestion des selection dans la table        
        rowSM = pluginsTable.getSelectionModel();        
        rowSM.addListSelectionListener(this);                        
        
        JPanel buttonsPanel = new JPanel();        
        buttonsPanel.add(activateButton);        
        buttonsPanel.add(freezeButton);        
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());        
        
        this.setLayout(new BorderLayout());        
        this.add(BorderLayout.NORTH, buttonsPanel);        
        this.add(BorderLayout.CENTER, pluginsTableScrollPane);                
        
        // Mapping GUI Object for plugins//        
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_BUTTONS_PANEL,buttonsPanel);              
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_BUTTONS_PANEL);                           
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_TABLE,pluginsTable);              
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_TABLE);                           
        SalomeTMFContext.getInstance().addToUIComponentsMap(UICompCst.PLUGINS_PANEL,this);              
        UICompCst.staticUIComps.add(UICompCst.PLUGINS_PANEL);                      
    }
      
    MyTableModel createPluginTableModel(Vector commonExtensions,Vector testDriverExtensions, 
                                        Vector scriptEngineExtensions, PluginManager plgMgr){
        MyTableModel pluginsTableModel =  new MyTableModel();
        
                
        pluginsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Plugin"));
        pluginsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom_d_extension"));
        pluginsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        pluginsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Extension"));
        pluginsTableModel.addColumnNameAndColumn(Language.getInstance().getText("Statut"));
        
        
        // Common Plugins
        for (int i=0; i<commonExtensions.size(); i++) {
            ArrayList row = new ArrayList(5);
            Extension commonExt = (Extension)commonExtensions.elementAt(i);
            row.add(0,commonExt.getDeclaringPluginDescriptor().getId());
            row.add(1,commonExt.getId());
            row.add(2,commonExt.getParameter(Language.getInstance().getText("description")).valueAsString());
            row.add(3,commonExt.getExtendedPointId());
            String activated = Language.getInstance().getText("Inactif");
            if (plgMgr.isPluginActivated(commonExt.getDeclaringPluginDescriptor())) {
                activated = Language.getInstance().getText("Actif");
            }
                
            row.add(4,activated);
            pluginsTableModel.addRow(row);
            extensions.put(commonExt.getParameter("name").valueAsString()+commonExt.getExtendedPointId(),commonExt);
        }
        
        // TestDriver Plugins
        for (int i=0; i<testDriverExtensions.size(); i++) {
            ArrayList row = new ArrayList(5);
            Extension testDriverExt = (Extension)testDriverExtensions.elementAt(i);
            row.add(0,testDriverExt.getDeclaringPluginDescriptor().getId());
            row.add(1,testDriverExt.getId());
            row.add(2,testDriverExt.getParameter(Language.getInstance().getText("description")).valueAsString());
            row.add(3,testDriverExt.getExtendedPointId());
            String activated = Language.getInstance().getText("Inactif");
            if (plgMgr.isPluginActivated(testDriverExt.getDeclaringPluginDescriptor())) {
                activated = Language.getInstance().getText("Actif");
            }
                
            row.add(4,activated);
            pluginsTableModel.addRow(row);
            extensions.put(testDriverExt.getParameter("name").valueAsString()+testDriverExt.getExtendedPointId(),testDriverExt);
            
        }
        
        // ScriptEngine Plugins
        for (int i=0; i<scriptEngineExtensions.size(); i++) {
            ArrayList row = new ArrayList(5);
            Extension scriptEngineExt = (Extension)scriptEngineExtensions.elementAt(i);
            row.add(0,scriptEngineExt.getDeclaringPluginDescriptor().getId());
            row.add(1,scriptEngineExt.getId());
            row.add(2,scriptEngineExt.getParameter(Language.getInstance().getText("description")).valueAsString());
            row.add(3,scriptEngineExt.getExtendedPointId());
            String activated = Language.getInstance().getText("Inactif");
            if (plgMgr.isPluginActivated(scriptEngineExt.getDeclaringPluginDescriptor())) {
                activated = Language.getInstance().getText("Actif");
            }
                
            row.add(4,activated);
            pluginsTableModel.addRow(row);
            extensions.put(scriptEngineExt.getParameter("name").valueAsString()+scriptEngineExt.getExtendedPointId(),scriptEngineExt);
            
        }
        
        // Bug tracking Plugins
        for (int i=0; i<bugTrackingExtensions.size(); i++) {
            ArrayList row = new ArrayList(5);
            Extension bTrackExt = (Extension)bugTrackingExtensions.elementAt(i);
            row.add(0,bTrackExt.getDeclaringPluginDescriptor().getId());
            row.add(1,bTrackExt.getId());
            row.add(2,bTrackExt.getParameter(Language.getInstance().getText("description")).valueAsString());
            row.add(3,bTrackExt.getExtendedPointId());
            String activated = Language.getInstance().getText("Inactif");
            if (plgMgr.isPluginActivated(bTrackExt.getDeclaringPluginDescriptor())) {
                activated = Language.getInstance().getText("Actif");
            }
                
            row.add(4,activated);
            pluginsTableModel.addRow(row);
            extensions.put(bTrackExt.getParameter("name").valueAsString()+bTrackExt.getExtendedPointId(),bTrackExt);
            
        }
        
        // ReqManager Plugins
        for (int i=0; i<reqMgrExtensions.size(); i++) {
            ArrayList row = new ArrayList(5);
            Extension reqMgrExt = (Extension)reqMgrExtensions.elementAt(i);
            row.add(0,reqMgrExt.getDeclaringPluginDescriptor().getId());
            row.add(1,reqMgrExt.getId());
            row.add(2,reqMgrExt.getParameter(Language.getInstance().getText("description")).valueAsString());
            row.add(3,reqMgrExt.getExtendedPointId());
            String activated = Language.getInstance().getText("Inactif");
            if (plgMgr.isPluginActivated(reqMgrExt.getDeclaringPluginDescriptor())) {
                activated = Language.getInstance().getText("Actif");
            }
                
            row.add(4,activated);
            pluginsTableModel.addRow(row);
            extensions.put(reqMgrExt.getParameter("name").valueAsString()+reqMgrExt.getExtendedPointId(),reqMgrExt);
        }  
        
        // Statistiques Plugins        
        for (int i=0; i<statistiquesExtensions.size(); i++) {            
            ArrayList row = new ArrayList(5);            
            Extension statExt = (Extension)statistiquesExtensions.elementAt(i);            
            row.add(0,statExt.getDeclaringPluginDescriptor().getId());            
            row.add(1,statExt.getId());            
            row.add(2,statExt.getParameter(Language.getInstance().getText("description")).valueAsString());            
            row.add(3,statExt.getExtendedPointId());            
            String activated = Language.getInstance().getText("Inactif");            
            if (plgMgr.isPluginActivated(statExt.getDeclaringPluginDescriptor())) {                
                activated = Language.getInstance().getText("Actif");            
            }                            
            row.add(4,activated);            
            pluginsTableModel.addRow(row);           
            extensions.put(statExt.getParameter("name").valueAsString()+statExt.getExtendedPointId(),statExt);                    
        }
        
        
        return pluginsTableModel;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source.equals(activateButton)){
            // activatePerformed(e);
        } else if (source.equals(freezeButton)){
            // freezePerformed(e);
        }
    }
    
    public void activatePerformed(ActionEvent e) {
        try {
            if ((currentPlgID != null)&&(currentExt != null)) {
                JButton button = (JButton)e.getSource();
                if (button.getText() == Language.getInstance().getText("Activer")) {
                    SalomeTMFContext.getInstance().jpf.activateExtension(currentExt);
                    activateButton.setText(Language.getInstance().getText("Desactiver"));
                    for (int i=0 ; i<extensions.size() ; i++) {
                        if (sorter.getValueAt(i,0).equals(currentExt.getParameter("name").valueAsString()))
                            sorter.setValueAt(Language.getInstance().getText("Actif"), i, 4);
                    }
                }
                else if (button.getText() == Language.getInstance().getText("Desactiver")) {
                    /*
                      pluginMgr.deactivatePlugin(currentPlgID);
                      activateButton.setText("Activer");
                      for (int i=0 ; i<extensions.size() ; i++) {
                      if (sorter.getValueAt(i,0).equals(currentExt.getParameter("name").valueAsString()))
                      sorter.setValueAt("Inactif", i, 3);
                      }*/
                }
            }
        } catch (Exception E) {
            Util.err(E);
        }
    }
    
    public void freezePerformed(ActionEvent e) {
        try {
            if (currentCommonPlg != null) {
                JButton button = (JButton)e.getSource();
                if (button.getText() == Language.getInstance().getText("Geler")) {
                    currentCommonPlg.freeze();
                    freezeButton.setText(Language.getInstance().getText("Degeler"));
                }
                else if (button.getText() == Language.getInstance().getText("Degeler")) {
                    currentCommonPlg.unFreeze();
                    freezeButton.setText(Language.getInstance().getText("Geler"));
                }
            }
        } catch (Exception E) {
            Util.err(E);
        }
    }
    
    public void refreshPluginsActivation() {
        for (int row=0 ; row < sorter.getRowCount() ; row++) {
            if (pluginMgr.isPluginActivated(pluginMgr.getRegistry().getPluginDescriptor(sorter.getValueAt(row,0).toString()))) {
                sorter.setValueAt(Language.getInstance().getText("Actif"), row, 4);
            } else {
                sorter.setValueAt(Language.getInstance().getText("Inactif"), row, 4);
            }
        }
    }
    
    
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource().equals(pluginsTable.getSelectionModel())){
            //pluginsTableValueChanged(e);
        }
    }
    
    
    public void pluginsTableValueChanged(ListSelectionEvent e) {
        try {
            if (e.getValueIsAdjusting())
                return;
                
            selectedRow = pluginsTable.getSelectedRow();
            if (selectedRow != -1) {
                activateButton.setEnabled(true);
                currentPlgID = sorter.getValueAt(selectedRow,0).toString();
                currentExt = (Extension)extensions.get(currentPlgID+sorter.getValueAt(selectedRow,3).toString());
                //String activated = "Inactif";
                        
                if (sorter.getValueAt(selectedRow,4).equals(Language.getInstance().getText("Actif"))) {
                    activateButton.setText(Language.getInstance().getText("Desactiver"));
                                
                    if (sorter.getValueAt(selectedRow,3).equals(Language.getInstance().getText("Common"))) {
                        Common common = (Common)pluginMgr.getPlugin(currentPlgID);
                        currentCommonPlg = common;
                                        
                        if (common.isFreezable()) {
                            freezeButton.setEnabled(true);
                            if (!common.isFreezed()) {
                                freezeButton.setText(Language.getInstance().getText("Geler"));
                            } else {
                                freezeButton.setText(Language.getInstance().getText("Degeler"));
                            }
                        } else {
                            freezeButton.setEnabled(false);
                        }
                    } else {
                        freezeButton.setEnabled(false);
                    } 
                }
                else {
                    freezeButton.setEnabled(false);
                    activateButton.setText(Language.getInstance().getText("Activer"));
                }
            } else {
                activateButton.setEnabled(false);
                freezeButton.setEnabled(false);
            }
        } catch (Exception E) {
            Util.err(E);
        }
        
    }

}
