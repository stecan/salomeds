package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.RowTransferHandler;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

public class IcalPanel extends JPanel implements ActionListener, ListSelectionListener {
        
    JButton deleteICALValueButton;
    JButton calculateICALValueButton;
    JButton visualizeICALValueButton;
    JButton refreshICALValueButton;
        
    JTable icalTable;
        
    TableSorter  sorter;

    ListSelectionModel rowSM;
    
    RowTransferHandler transferHandler;
    
    MyTableModel icalTableModel;
    
    BugTracker mantis;
        
    public IcalPanel(BugTracker mantis) {
                
        super(); 
        this.mantis = mantis;
                
        deleteICALValueButton = new JButton(Language.getInstance().getText("Supprimer"));
        calculateICALValueButton = new JButton(Language.getInstance().getText("ICAL_calculation"));
        visualizeICALValueButton = new JButton(Language.getInstance().getText("ICAL_graph"));
        refreshICALValueButton = new JButton(Language.getInstance().getText("Rafraichir"));
                
        icalTable = new JTable();
        transferHandler = new RowTransferHandler();
                
        deleteICALValueButton.setEnabled(false);
        deleteICALValueButton.setToolTipText(Language.getInstance().getText("Delete_ICAL_Value"));
        deleteICALValueButton.addActionListener(this);
        calculateICALValueButton.setToolTipText(Language.getInstance().getText("ICAL_calculation"));
        calculateICALValueButton.addActionListener(this);
        visualizeICALValueButton.setToolTipText(Language.getInstance().getText("ICAL_graph"));
        visualizeICALValueButton.addActionListener(this);
        refreshICALValueButton.setToolTipText(Language.getInstance().getText("Rafraichir"));
        refreshICALValueButton.addActionListener(this);
                
        createICALTableModel();
        sorter = new TableSorter(icalTableModel);
        icalTable = new JTable(sorter);
        sorter.setTableHeader(icalTable.getTableHeader());
        icalTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        icalTable.setTransferHandler(transferHandler);
        icalTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        JScrollPane pluginsTableScrollPane = new JScrollPane(icalTable);
        // Gestion des selection dans la table
        rowSM = icalTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        JPanel buttonsPanel = new JPanel();

        buttonsPanel.add(calculateICALValueButton);
        buttonsPanel.add(visualizeICALValueButton);
        buttonsPanel.add(refreshICALValueButton);
        buttonsPanel.add(deleteICALValueButton);
        
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        setLayout(new BorderLayout());
        add(BorderLayout.NORTH, buttonsPanel);
        add(BorderLayout.CENTER, pluginsTableScrollPane);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        Object source = arg0.getSource();
        if (source.equals(deleteICALValueButton)){
            deleteICALValuePerformed();
        } else if (source.equals(calculateICALValueButton)){
            calculateICALValuePerformed();
        } else if (source.equals(visualizeICALValueButton)){
            visualizeICALValuePerformed();
        } else if (source.equals(refreshICALValueButton)){
            refreshICALValuePerformed();
        }
                
    }

    @Override
    public void valueChanged(ListSelectionEvent arg0) {
        if (arg0.getSource().equals(icalTable.getSelectionModel())){
            if (arg0.getValueIsAdjusting())
                return;
            int nbOfSelectedRows = icalTable.getSelectedRowCount();
            int selectedRow = icalTable.getSelectedRow();
            if ((selectedRow != -1) && (nbOfSelectedRows != 0)) {
                deleteICALValueButton.setEnabled(true);
            } else {
                deleteICALValueButton.setEnabled(false);
            }
        }
    }
        
    void createICALTableModel(){
        icalTableModel =  new MyTableModel();
        
        icalTableModel.addColumnNameAndColumn(Language.getInstance().getText("Environnement")); 
        icalTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date"));
        icalTableModel.addColumnNameAndColumn(Language.getInstance().getText("valeur") + " ICAL" + " (%)");
        
        try {
            Vector icalKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_ICAL")) {
                        icalKeys.add(key);
                    }
                }
                        
                if ((icalKeys != null)&&(icalKeys.size() != 0)) {
                                
                    for (int i=0 ; i<icalKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = icalKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String icalValues = res[j];
                            String[] icals = icalValues.split("[_]");
                            if (icals.length == 2) {
                                                
                                Float ical = new Float(Float.parseFloat(icals[1])); 
                                        
                                ArrayList row = new ArrayList(2);
                                row.add(0,env.getNameFromModel());
                                row.add(1,icals[0]);
                                row.add(2,ical);
                                    
                                icalTableModel.addRow(row);
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
    }
        
    void refreshICALValuePerformed(){
        icalTableModel.clearTable();
        
        try {
            Vector icalKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_ICAL")) {
                        icalKeys.add(key);
                    }
                }
                        
                if ((icalKeys != null)&&(icalKeys.size() != 0)) {
                                
                    for (int i=0 ; i<icalKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = icalKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String icalValues = res[j];
                            String[] icals = icalValues.split("[_]");
                            if (icals.length == 2) {
                                                
                                Float ical = new Float(Float.parseFloat(icals[1])); 
                                        
                                ArrayList row = new ArrayList(2);
                                row.add(0,env.getNameFromModel());
                                row.add(1,icals[0]);
                                row.add(2,ical);
                                    
                                icalTableModel.addRow(row);
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
    }
        
    public void deleteICALValuePerformed() {
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        int choice = -1;
        int[] selectedRows = icalTable.getSelectedRows();
                
        choice = JOptionPane.showOptionDialog(IcalPanel.this,
                                              Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_les_valeurs_ICAL_selectionnees"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[1]);
        if (choice == JOptionPane.YES_OPTION) {
            for (int i = selectedRows.length - 1 ; i >= 0 ; i--) {
                                
                try {
                    // DB
                    ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
                    if (pISQLConfig != null){
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRows[i],0));
                        String key = mantis.getBugTrackerName()+"_"+Language.getInstance().getText("ICAL")+"_"+env.getIdBdd();
                        String icalValuesForEnv = pISQLConfig.getProjectConf(key,DataModel.getCurrentProject().getIdBdd());
                        icalValuesForEnv = icalValuesForEnv.replaceFirst((String)sorter.getValueAt(selectedRows[i],1)+"_"+
                                                                         sorter.getValueAt(selectedRows[i],2),"");
                        icalValuesForEnv = icalValuesForEnv.replaceFirst(",,",",");
                                        
                        if ((icalValuesForEnv.equals(""))||(icalValuesForEnv.equals(","))) {
                            pISQLConfig.deleteProjectConf(key,DataModel.getCurrentProject().getIdBdd());
                        } else {
                            pISQLConfig.updateProjectConf(key,icalValuesForEnv,DataModel.getCurrentProject().getIdBdd());
                        }
                                        
                    }
                    // IHM
                    /*if (DataModel.getActionTableModel().getRowCount() == 0) {
                      deleteICALValueButton.setEnabled(false);
                      }
                      icalTableModel.removeData(selectedRows[i]);*/
                } catch (Exception exception) {
                    Util.err(exception);
                    Tools.ihmExceptionView(exception);
                }
            }
                        
            // IHM
            refreshICALValuePerformed();
        }
    }
        
    private void visualizeICALValuePerformed() {
        JDialog dialog = new JDialog(SalomeTMFContext.getInstance().ptrFrame,true);
        dialog.setTitle(Language.getInstance().getText("ICAL_graph"));
        JPanel pnl = new JPanel(new BorderLayout()); 
        dialog.setContentPane(pnl); 
        dialog.setSize(600,400); 

        try {
            Vector icalKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_ICAL")) {
                        icalKeys.add(key);
                    }
                }
                        
                if ((icalKeys != null)&&(icalKeys.size() != 0)) {
                                
                    TimeSeriesCollection dataset = new TimeSeriesCollection();
                                
                    for (int i=0 ; i<icalKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = icalKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        TimeSeries ts = new TimeSeries(env.getNameFromModel(),Second.class);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String icalValues = res[j];
                            String[] icals = icalValues.split("[_]");
                            if (icals.length == 2) {
                                Locale locale = Locale.getDefault();
                                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                                Date date = dateFormat.parse(icals[0]);
                                                                                
                                float ical = Float.parseFloat(icals[1]); 
                                ts.add(new TimeSeriesDataItem(new Second(date),ical));
                            }
                        }
                        dataset.addSeries(ts);
                    }
                                
                    JFreeChart chart = ChartFactory.createTimeSeriesChart(Language.getInstance().getText("ICALgraphTitle"), "Date", 
                                                                          "ICAL (%)", dataset, true, true, false);
                    chart.setBackgroundPaint(Color.white);
                    XYPlot plot = (XYPlot) chart.getPlot();
                    plot.setBackgroundPaint(Color.lightGray);
                    plot.setDomainGridlinePaint(Color.white);
                    plot.setRangeGridlinePaint(Color.white);
                    XYItemRenderer r = plot.getRenderer();
                    if (r instanceof XYLineAndShapeRenderer) {
                        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
                        renderer.setShapesVisible(true);
                        renderer.setShapesFilled(true);
                    }
                    DateAxis axis = (DateAxis) plot.getDomainAxis();
                    axis.setDateFormatOverride(new SimpleDateFormat("dd/MM/yy"));
                    ValueAxis vAxis = plot.getRangeAxis();
                    vAxis.setRange(0,100);
                    ChartPanel cPanel = new ChartPanel(chart); 
                    pnl.add(cPanel);
                            
                } else {
                    JLabel label = new JLabel(Language.getInstance().getText("No_ICAL_values_in_DB"));
                    pnl.add(label);
                }
                        
            } else {
                Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
                
        dialog.setVisible(true);
    }

    private void calculateICALValuePerformed() {
        ArrayList environments = DataModel.getCurrentProject().getEnvironmentListFromModel();
        String message = "";
        String mess_env = "";
        Map envsICAL = new Hashtable();
        int choice = -1;
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
                
        for (int l=0 ; l<environments.size() ; l++) {
            Environment env = (Environment)environments.get(l);
                        
            /*int n0 = bugTracker.getEnvironmentNbBugs(env,BugTracker.CRITICAL,false);
              Util.log("Nb Critical Bugs = " + n0);
                        
              int n1 = bugTracker.getEnvironmentNbBugs(env,BugTracker.MAJOR,false);
              Util.log("Nb Major Bugs = " + n1);
                        
              int nc0 = bugTracker.getEnvironmentNbBugs(env,BugTracker.CRITICAL,true);
              Util.log("Nb Corrected Critical Bugs = " + nc0);
                        
              int nc1 = bugTracker.getEnvironmentNbBugs(env,BugTracker.MAJOR,true);
              Util.log("Nb Corrected Major Bugs = " + nc1);*/
                        
            int nbMajorBugs = mantis.getEnvNbBugs(env,false);
            Util.log("Nb Major Bugs = " + nbMajorBugs);
                        
            int nbCorMajorBugs = mantis.getEnvNbBugs(env,true);
            Util.log("Nb Corrected Major Bugs = " + nbCorMajorBugs);
                                                
            if (nbMajorBugs==0) {
                mess_env += "\n -" + env.getNameFromModel();
                                
            } else {
                float ical_temp = Math.round(((float)nbCorMajorBugs/(float)nbMajorBugs)*10000); 
                float ical = ical_temp/100;
                message += "\n -ICAL = "+ ical + " (env. '" + env.getNameFromModel()+ "')";
                envsICAL.put(env,new Float(ical));
            }
        }
                
        if (mess_env != "") {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("ICAL_problem") + mess_env + Language.getInstance().getText("ICAL_problem2"),
                                          Language.getInstance().getText("Information_"),
                                          JOptionPane.INFORMATION_MESSAGE);
        }
                
        if (message != "") {
            choice = JOptionPane.showOptionDialog(new Frame(),
                                                  message + Language.getInstance().getText("ICAL_saved_in_DB"),
                                                  Language.getInstance().getText("Information_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
                        
            if (choice == JOptionPane.YES_OPTION) {
                                
                for (Enumeration e = ((Hashtable)envsICAL).keys() ; e.hasMoreElements() ;) {
                    try {
                        Environment env = (Environment)e.nextElement();
                        float ical = ((Float)envsICAL.get(env)).floatValue();
                        ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
                        if (pISQLConfig != null){
                            Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                            Locale locale = Locale.getDefault();
                            DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                            Date date = new Date();
                            String myDate = dateFormat.format(date);
                            String key = mantis.getBugTrackerName()+"_ICAL_"+env.getIdBdd();
                                                
                            if (projectConfs.containsKey(key)) {
                                pISQLConfig.updateProjectConf(key,(String)projectConfs.get(key)+","+myDate+"_"+Float.toString(ical),
                                                              DataModel.getCurrentProject().getIdBdd());
                            } else {
                                pISQLConfig.insertProjectConf(mantis.getBugTrackerName()+"_ICAL_"+env.getIdBdd(), 
                                                              myDate+"_"+Float.toString(ical), DataModel.getCurrentProject().getIdBdd());
                            }
                                                
                            // IHM
                            ArrayList row = new ArrayList(2);
                            row.add(0,env.getNameFromModel());
                            row.add(1,myDate);
                            row.add(2,new Float(ical));
                                    
                            icalTableModel.addRow(row);
                                                
                        } else {
                            Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
                        }
                    } catch (Exception E){
                        Util.err(E);
                        Tools.ihmExceptionView(E);
                    }
                }
                                
                JOptionPane.showMessageDialog(new Frame(),
                                              Language.getInstance().getText("ICAL_saved_confirmation"),
                                              Language.getInstance().getText("Information_"),
                                              JOptionPane.INFORMATION_MESSAGE);
                                
            }
        }
    }

}
