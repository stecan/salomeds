/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer les
 * informations pour les suites de tests
 * @author teaml039
 * @version 0.1
 */
public class AskNewTestList extends JDialog {
    
    /**
     * La nouvelle suite de tests
     */
    private TestList testList;
    
    /**
     * Le Field permettant de recuperer la chaine
     */
    private JTextField testListNameField;
    
    /**
     * La zone de texte permettant de recuperer la description
     */
    private JTextArea descriptionArea;
    
    
    /**
     * Nom de la famille
     */
    //private String familyName;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre.
     * @param title le titre de la fenetre
     * @param textToBePrompt chaine correspondant a la demande.
     */
    public AskNewTestList(String title, String textToBePrompt) {
        
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        testListNameField = new JTextField(10);
        //familyName = "";
        descriptionArea = new JTextArea(10,20);
        JPanel page = new JPanel();
        
        JLabel prompt = new JLabel(textToBePrompt);
        
        JPanel giveName = new JPanel();
        
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(prompt);
        giveName.add(Box.createRigidArea(new Dimension(10, 0)));
        giveName.add(testListNameField);
        
        //descriptionArea.setPreferredSize(new Dimension(100,150));
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(giveName);
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(descriptionScrollPane);
        
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (testListNameField.getText() != null && (!testListNameField.getText().trim().equals(""))) {
                        testList = new TestList(testListNameField.getText().trim(), descriptionArea.getText());
                    
                        AskNewTestList.this.dispose();
                    } else {
                        testListNameField.selectAll();
                        JOptionPane.showMessageDialog(
                                                      AskNewTestList.this,
                                                      Language.getInstance().getText("Vous_devez_entrez_un_nom"),
                                                      Language.getInstance().getText("Erreur_"),
                                                      JOptionPane.ERROR_MESSAGE);
                        testListNameField.requestFocusInWindow();
                    }
                }
            });
        
        testListNameField.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                
                }
            });
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    testList = null;
                    AskNewTestList.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);
        
        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    testList = null;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        this.setTitle(title);
        /*this.setLocationRelativeTo(this.getParent()); 
          this.pack();
          this.setVisible(true);**/
        centerScreen();
    } // Fin du constructeur AskNewTestList/2
    
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Recupere la nouvelle suite de tests
     * @return la nouvelle suite de tests
     */
    public TestList getTestList() {
        return testList;
    } // Fin de la methode getTestList/0
    
} // Fin de la classe AskNewTestList
