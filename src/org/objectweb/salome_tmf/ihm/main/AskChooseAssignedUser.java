package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.tools.Tools;

class AskChooseAssignedUser extends JDialog implements ActionListener {
        
    JPanel panel = new JPanel(new BorderLayout());
    JButton validateButton ;
    JButton cancelButton;
    JComboBox usersComboBox;
        
        
    Vector userList;
    User selectedUser = null;
    boolean doActionOnValidate = true;
        
    /** Creates a new instance of AskChooseEnvironment */
    public AskChooseAssignedUser() {
                
        super(SalomeTMFContext.getInstance().ptrFrame,true);
                
        userList = new Vector();
        Project current_project = DataModel.getCurrentProject();
        try {
            Vector users_wrapper = current_project.getAllUsersWrapper();
            /*if (users_wrapper.size() == 0){
              current_project.loadProjectInfo();
              users_wrapper = current_project.getAllUsersWrapper();
              }*/
            for (int i=0; i<users_wrapper.size() ; i++) {
                User user = new User((UserWrapper)users_wrapper.elementAt(i));
                userList.add(user);
            }
        } catch (Exception e) {
            Util.err(e);
        }
        initComponent();
    }
        
    public AskChooseAssignedUser(Vector userList, boolean doActionOnValidate) {
        super(SalomeTMFContext.getInstance().ptrFrame,true);
        this.userList = userList;
        this.doActionOnValidate = doActionOnValidate;
        initComponent();
    }
        
    void initComponent(){
        usersComboBox = new JComboBox();
        for (int i=0; i<userList.size() ; i++) {
            User user = (User) userList.elementAt(i);
            usersComboBox.addItem(user.getLoginFromModel());
        }
                
        validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton.setToolTipText(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(this);
                
        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);
                
        JLabel envLabel = new JLabel(Language.getInstance().getText("Utilisateur") + " : ");
                
        JPanel textFieldPane = new JPanel();
        textFieldPane.setLayout(new BoxLayout(textFieldPane,BoxLayout.Y_AXIS));
        textFieldPane.add(usersComboBox);
                
        JPanel textPane = new JPanel();
        textPane.setLayout(new BoxLayout(textPane,BoxLayout.Y_AXIS));
        textPane.add(envLabel);
                
        JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
        textPaneAll.add(textPane);
        textPaneAll.add(textFieldPane);
                
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(validateButton);
        buttonPanel.add(cancelButton);
                
        JPanel labelSet = new JPanel();
        labelSet.add(textPaneAll);
                
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(labelSet);
        panel.add(buttonPanel);
        panel.setBorder(BorderFactory.createRaisedBevelBorder());
                
        this.getContentPane().add(panel);
                
        this.setTitle(Language.getInstance().getText("Utilisateur"));
        //this.setLocation(300,300);
        /* this.setLocationRelativeTo(this.getParent()); 
           this.pack();*/
        centerScreen();
    }
        
    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);  
        this.setVisible(true); 
        requestFocus();
    }
        
    public User getSelectedUser(){
        return selectedUser;
    }
    /********************************** Implements ActionListener *********************************/
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(cancelButton)){
            cancelPerformed();
        } else if (e.getSource().equals(validateButton)){
            validatePerformed();
        }
    }
        
        
        
    void cancelPerformed(){
        selectedUser = null;
        dispose();
    }
        
    void validatePerformed(){
        DynamicTree pDynamicTree = SalomeTMFPanels.getCampaignDynamicTree();

        int user_index = usersComboBox.getSelectedIndex();
        selectedUser = (User) userList.elementAt(user_index);
        if (doActionOnValidate){
            try {

                DataModel.assignCampaignTo((SimpleData)pDynamicTree.getSelectedNode().getUserObject(),selectedUser);
                SalomeTMFPanels.setCampPanelDetailsForTest(null,null,null,selectedUser.getIdBdd());
                IAssignedCampAction pIAssignedCampAction  = DataModel.getAssignedCampAction();
                if (pIAssignedCampAction!= null){
                    if (DataModel.getCurrentCampaign() != null){
                        pIAssignedCampAction.updateData(DataModel.getCurrentCampaign());
                    }
                }
            } catch (Exception ex) {
                Tools.ihmExceptionView(ex);
            }
        }
        dispose();
                
        /*      
                String user_login = (String)usersComboBox.getSelectedItem();
                User user = new User(user_login);
                DataModel.assignCampaignTo((SimpleData)pDynamicTree.getSelectedNode().getUserObject(),user);
                SalomeTMFPanels.setCampPanelDetailsForTest(null,null,null,user.getIdBdd());
                } catch (Exception ex) {
                Tools.ihmExceptionView(ex);
                }
                dispose();
        */
    }
        
        
        
}
