/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.ihm.models.ActionDetailsResultMouseListener;
import org.objectweb.salome_tmf.ihm.models.AutomaticTestResultMouseListener;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.UICompCst;

/**
 * Classe repr?sentant la vue sur les ex?cutions de test
 * @author teaml039
 * @version 0.1
 */
public class ExecutionResultView extends JDialog implements DataConstants,IHMConstants {

    /**
     * Mod?le de donn?es de la table
     */
    // MyTableModel testResultTableModel;
    DefaultTableModel testResultTableModel;

    /**
     * Table des r?sultats sur les tests
     */
    JTable testResultTable;
    TableSorter  sorter;

    ListSelectionModel rowSM;


    ExecutionResult currentExecResult;

    JButton detailsButton;
    
    /**************************************************************************/
    /** CONSTRUCTEUR                                                        ***/
    /**************************************************************************/


    /**
     * Constructeur de la vue sur les ex?cutions de tests
     * @param name le label d'information sur l'ex?cution
     */
    public ExecutionResultView(String name, ExecutionResult executionResult) {

        super(SalomeTMFContext.getInstance().ptrFrame, true);

        if (Config.getPolarionLinks()) {
            String[] columnNames = {Language.getInstance().getText("Famille"), 
                Language.getInstance().getText("Suite"),
                Language.getInstance().getText("Test"),
                Language.getInstance().getText("Link"),
                Language.getInstance().getText("Resultats")
            };
            Object[][] data = null;
            testResultTableModel = new DefaultTableModel(data, columnNames);
        } else {
            String[] columnNames = {Language.getInstance().getText("Famille"), 
                Language.getInstance().getText("Suite"),
                Language.getInstance().getText("Test"),
                Language.getInstance().getText("Resultats")
            };
            Object[][] data = null;
            testResultTableModel = new DefaultTableModel(data, columnNames);
        }

        testResultTable = new JTable( testResultTableModel )
        {
            //  Returning the Class of each column will allow different
            //  renderers to be used based on Class
            public Class getColumnClass(int column)
            {
                return getValueAt(0, column).getClass();
            }
        };
        
        
        JLabel executionName = new JLabel(name);
        executionName.setFont(new Font(null, Font.BOLD,18));

        sorter = new TableSorter(testResultTableModel);
        testResultTable.setModel(sorter);
        //testResultTable.setModel(testResultTableModel);

        sorter.setTableHeader(testResultTable.getTableHeader());

        testResultTable.setPreferredScrollableViewportSize(new Dimension(800, 200));
        testResultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // Mapping entre objets graphiques et constantes
        SalomeTMFContext.getInstance().addToUIComponentsMap(
                UICompCst.EXECUTION_RESULT_DETAILS_TESTS_TABLE,testResultTable);
        // Activation des plugins associ?s
        PluginsTools.activateAssociatedPlgs(UICompCst.EXECUTION_RESULT_DETAILS_TESTS_TABLE);

        JScrollPane tablePane = new JScrollPane(testResultTable);
        tablePane.setBorder(BorderFactory.createRaisedBevelBorder());

        testResultTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int row = testResultTable.rowAtPoint(new Point(e.getX(), e.getY()));
                int col = testResultTable.columnAtPoint(new Point(e.getX(), e.getY()));
                
                String url = (String) testResultTable.getModel().getValueAt(row, col);

                // DO here what you want to do with your url
                if (Config.getPolarionLinks()) {
                    if (col == 3) {
                        URI uri;
                            try {
                                uri = new java.net.URI(url);
                                (new LinkRunner(uri)).execute();
                            } catch (URISyntaxException ex) {
                                Logger.getLogger(ExecutionResultView.class.
                                        getName()).log(Level.SEVERE, null, ex);
                            }
                    }
                } 
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                int col = testResultTable.columnAtPoint(new Point(e.getX(), e.getY()));
                if (col == 0) {
                    testResultTable.setCursor(new Cursor(Cursor.HAND_CURSOR));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                int col = testResultTable.columnAtPoint(new Point(e.getX(), e.getY()));
                if (col != 0) {
                    testResultTable.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            }
        });

        if (Config.getPolarionLinks()) {
            // A cell renderer for Polarion links is only settled, if 
            // the Polarion links are available. In the other case, no 
            // renderer is settled (because no link is available)
            testResultTable.getColumnModel().getColumn(3).setCellRenderer(new TableCellRenderer() {
                @Override
                public Component getTableCellRendererComponent(JTable table, 
                        final Object value, boolean arg2, boolean arg3, int arg4, int arg5) {
                    final JLabel lab = new JLabel("<html><a href=\"" + value + "\">" + value + "</a>");
                    return lab;
                }
            });
        }
        
        rowSM = testResultTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    //                if (e.getValueIsAdjusting())
                    //                    return;

                    int selectedRowIndex = testResultTable.getSelectedRow();
                    if (selectedRowIndex != -1 && testResultTableModel.getRowCount() > 0) {
                        Test test = DataModel.getCurrentProject().getTestFromModel(
                                (String)sorter.getValueAt(selectedRowIndex, 0),
                                (String)sorter.getValueAt(selectedRowIndex, 1),
                                (String)sorter.getValueAt(selectedRowIndex, 2)
                                );
                        DataModel.setTestObservedInExecution(test);
                        DataModel.setCurrentExecutionTestResult(
                                currentExecResult.getExecutionTestResultFromModel(test));
                        detailsButton.setEnabled(true);
                    } else {
                        detailsButton.setEnabled(false);
                    }
                }
            });


        // Gestion de la souris
        testResultTable.addMouseListener(new ActionDetailsResultMouseListener());
        testResultTable.addMouseListener(new AutomaticTestResultMouseListener());
        testResultTable.setRowHeight(25);

        JButton quitButton = new JButton(Language.getInstance().getText("Terminer"));
        quitButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ExecutionResultView.this.dispose();
                }
            });


        detailsButton = new JButton(Language.getInstance().getText("Details"));
        detailsButton.setEnabled(false);
        detailsButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean attachUpdate = false;
                    boolean automatic = false;
                    ExecutionTestResult pExecutionTestResult = DataModel.
                            getCurrentExecutionTestResult();
                    HashMap oldAttachMap = pExecutionTestResult.getCopyOfAttachmentMapFromModel();

                    if (DataModel.getTestObservedInExecution() instanceof ManualTest) {
                        Util.log("[ExecutionResultView->detailsButton] -> view manual test result");
                        new ActionDetailsView((ManualTest)DataModel.getTestObservedInExecution(), 
                                DataModel.getObservedExecution(), 
                                DataModel.getObservedExecutionResult(), 
                                pExecutionTestResult, ExecutionResultView.this);
                        //attachUpdate = false;
                    } else {
                        Util.log("[ExecutionResultView->detailsButton] -> view automatic test result");
                        SalomeTMFContext.getInstance();
                        AttachmentViewWindow attachWindow = 
                                new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(), 
                                        EXECUTION_RESULT_TEST, 
                                        DataModel.getCurrentExecutionTestResult());
                        automatic = true;
                        if (!attachWindow.cancelPerformed){
                            attachUpdate = true;
                        }
                    }
                    if (attachUpdate && automatic){
                        int transNumber = -1;
                        Util.log("[ExecutionResultView->detailsButton] -> valide Perfomed");
                        try {
                            Util.log("[ExecutionResultView] Api.INSERT_ATTACHMENT");
                            transNumber = Api.beginTransaction(10, 
                                    ApiConstants.INSERT_ATTACHMENT);

                            pExecutionTestResult.updateAttachement(oldAttachMap);

                            Api.commitTrans(transNumber);

                        } catch (Exception exception) {
                            Api.forceRollBackTrans(transNumber);
                            DataModel.getCurrentExecutionTestResult().
                                    setAttachmentMapInModel(oldAttachMap);
                            Util.err(exception);
                            Tools.ihmExceptionView(exception);
                        }
                    } else if (automatic){
                        Util.log("[ExecutionResultView->detailsButton] -> cancel Perfomed");
                        DataModel.getCurrentExecutionTestResult().
                                setAttachmentMapInModel(oldAttachMap);
                    }

                }

                //}
            });

        JPanel upPart = new JPanel(new BorderLayout());
        upPart.add(executionName, BorderLayout.NORTH);

        JPanel allView = new JPanel(new BorderLayout());

        allView.add(upPart, BorderLayout.NORTH);
        allView.add(tablePane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(detailsButton);
        buttonPanel.add(quitButton);


        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(allView);
        panel.add(buttonPanel);



        initTestResults(executionResult);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(panel, BorderLayout.CENTER);
        //this.setLocation(400,300);
        this.setTitle(Language.getInstance().getText("Resultats_d_execution"));
        /*this.pack();
          this.setLocationRelativeTo(this.getParent());
          this.setVisible(true);
        */
        centerScreen();
    } // Fin du constructeur ExecutionResult/1

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }
    /**
     *
     * @param executionResult
     */
    public void initTestResults(ExecutionResult executionResult) {

        Test[] pTabTest =  executionResult.getTestOrderedFromModel();
        for (int j = 0; j < pTabTest.length ; j++){
            Test test = pTabTest[j];
            Vector data = new Vector();
            data.add(test.getTestListFromModel().getFamilyFromModel().toString());
            data.add(test.getTestListFromModel().toString());
            data.add(test.toString());
            if (Config.getPolarionLinks()) {
                PolarionLink polarionLink = new PolarionLink(
                        DataModel.getCurrentProject().getIdBdd()); 
                String link = polarionLink.convert(test.toString());
                data.add(link);
            } 
            
            ImageIcon image = Tools.getActionStatusIcon(executionResult.
                    getTestResultStatusFromModel(test));
            if (image == null) {
                data.add(Tools.createAppletImageIcon(PATH_TO_GOON_ICON, ""));
            } else {
                data.add(Tools.getActionStatusIcon(executionResult.
                        getTestResultStatusFromModel(test)));


            }
            currentExecResult = executionResult;
            testResultTableModel.addRow(data);

        }
    } // Fin de la m?thode initTestResults/1

} // Fin de la classe ExecutionResult
