package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.models.RowTransferHandler;
import org.objectweb.salome_tmf.ihm.models.TableSorter;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.ReqManager;

public class QsScorePanel extends JPanel implements ActionListener, ListSelectionListener {

    JButton deleteQsScoreValueButton;
    JButton calculateQsScoreValueButton;
    JButton visualizeQsScoreValueButton;
    JButton refreshQsScoreValueButton;
        
    JTable qsScoreTable;
        
    TableSorter  sorter;

    ListSelectionModel rowSM;
    
    RowTransferHandler transferHandler;
    
    MyTableModel qsScoreTableModel;
    
    BugTracker mantis;
    
    ReqManager reqPlug;
    
    AskChooseEnvironment chooseEnvWindow;
        
    public QsScorePanel(BugTracker mantis, ReqManager reqPlug) {
                
        super(); 
        this.mantis = mantis;
        this.reqPlug = reqPlug;
                
        deleteQsScoreValueButton = new JButton(Language.getInstance().getText("Supprimer"));
        calculateQsScoreValueButton = new JButton(Language.getInstance().getText("QsScore_calculation"));
        visualizeQsScoreValueButton = new JButton(Language.getInstance().getText("QsScore_graph"));
        refreshQsScoreValueButton = new JButton(Language.getInstance().getText("Rafraichir"));
                
        qsScoreTableModel =  new MyTableModel();
        
        qsScoreTableModel.addColumnNameAndColumn(Language.getInstance().getText("Environnement"));      
        qsScoreTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date"));
        qsScoreTableModel.addColumnNameAndColumn(Language.getInstance().getText("valeur") + " QS Score");
                
        qsScoreTable = new JTable();
        transferHandler = new RowTransferHandler();
                
        deleteQsScoreValueButton.setEnabled(false);
        deleteQsScoreValueButton.setToolTipText(Language.getInstance().getText("Delete_QsScore_Value"));
        deleteQsScoreValueButton.addActionListener(this);
        calculateQsScoreValueButton.setToolTipText(Language.getInstance().getText("QsScore_calculation"));
        calculateQsScoreValueButton.addActionListener(this);
        visualizeQsScoreValueButton.setToolTipText(Language.getInstance().getText("QsScore_graph"));
        visualizeQsScoreValueButton.addActionListener(this);
        refreshQsScoreValueButton.setToolTipText(Language.getInstance().getText("Rafraichir"));
        refreshQsScoreValueButton.addActionListener(this);
                
        sorter = new TableSorter(qsScoreTableModel);
        qsScoreTable = new JTable(sorter);
        sorter.setTableHeader(qsScoreTable.getTableHeader());
        qsScoreTable.setPreferredScrollableViewportSize(new Dimension(700, 350));
        qsScoreTable.setTransferHandler(transferHandler);
        qsScoreTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        JScrollPane pluginsTableScrollPane = new JScrollPane(qsScoreTable);
        // Gestion des selection dans la table
        rowSM = qsScoreTable.getSelectionModel();
        rowSM.addListSelectionListener(this);
        
        createQsScoreTableModel();
        
        JPanel buttonsPanel = new JPanel();

        buttonsPanel.add(calculateQsScoreValueButton);
        buttonsPanel.add(visualizeQsScoreValueButton);
        buttonsPanel.add(refreshQsScoreValueButton);
        buttonsPanel.add(deleteQsScoreValueButton);
        
        buttonsPanel.setBorder(BorderFactory.createRaisedBevelBorder());

        setLayout(new BorderLayout());
        add(BorderLayout.NORTH, buttonsPanel);
        add(BorderLayout.CENTER, pluginsTableScrollPane);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        Object source = arg0.getSource();
        if (source.equals(deleteQsScoreValueButton)){
            deleteQsScoreValuePerformed();
        } else if (source.equals(calculateQsScoreValueButton)){
            calculateQsScoreValuePerformed();
        } else if (source.equals(visualizeQsScoreValueButton)){
            visualizeQsScoreValuePerformed();
        } else if (source.equals(refreshQsScoreValueButton)){
            refreshQsScoreValuePerformed();
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent arg0) {
        if (arg0.getSource().equals(qsScoreTable.getSelectionModel())){
            if (arg0.getValueIsAdjusting())
                return;
            int nbOfSelectedRows = qsScoreTable.getSelectedRowCount();
            int selectedRow = qsScoreTable.getSelectedRow();
            if ((selectedRow != -1) && (nbOfSelectedRows != 0)) {
                deleteQsScoreValueButton.setEnabled(true);
            } else {
                deleteQsScoreValueButton.setEnabled(false);
            }
        }
    }
        
    void createQsScoreTableModel(){
        try {
            Vector qsScoreKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_QsScore")) {
                        qsScoreKeys.add(key);
                    }
                }
                        
                if ((qsScoreKeys != null)&&(qsScoreKeys.size() != 0)) {
                                
                    for (int i=0 ; i<qsScoreKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = qsScoreKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String qsScoreValues = res[j];
                            String[] qsScores = qsScoreValues.split("[_]");
                            if (qsScores.length == 2) {
                                                
                                Float qsScore = new Float(Float.parseFloat(qsScores[1])); 
                                        
                                ArrayList row = new ArrayList(2);
                                row.add(0,env.getNameFromModel());
                                row.add(1,qsScores[0]);
                                row.add(2,qsScore);
                                    
                                qsScoreTableModel.addRow(row);
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
    }
        
    public void refreshQsScoreValuePerformed() {
        qsScoreTableModel.clearTable();
        
        try {
            Vector qsScoreKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_QsScore")) {
                        qsScoreKeys.add(key);
                    }
                }
                        
                if ((qsScoreKeys != null)&&(qsScoreKeys.size() != 0)) {
                                
                    for (int i=0 ; i<qsScoreKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = qsScoreKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String qsScoreValues = res[j];
                            String[] qsScores = qsScoreValues.split("[_]");
                            if (qsScores.length == 2) {
                                                
                                Float qsScore = new Float(Float.parseFloat(qsScores[1])); 
                                        
                                ArrayList row = new ArrayList(2);
                                row.add(0,env.getNameFromModel());
                                row.add(1,qsScores[0]);
                                row.add(2,qsScore);
                                    
                                qsScoreTableModel.addRow(row);
                            }
                        }
                    }
                }
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
    }
        
    public void deleteQsScoreValuePerformed() {
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        int choice = -1;
        int[] selectedRows = qsScoreTable.getSelectedRows();
                
        choice = JOptionPane.showOptionDialog(QsScorePanel.this,
                                              Language.getInstance().getText("Etes_vous_sur_de_vouloir_supprimer_les_valeurs_QsScore_selectionnees"),
                                              Language.getInstance().getText("Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE,
                                              null,
                                              options,
                                              options[1]);
        if (choice == JOptionPane.YES_OPTION) {
            for (int i = selectedRows.length - 1 ; i >= 0 ; i--) {
                                
                try {
                    // DB
                    ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
                    if (pISQLConfig != null){
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel((String)sorter.getValueAt(selectedRows[i],0));
                        String key = mantis.getBugTrackerName()+"_QsScore_"+env.getIdBdd();
                        String qsScoreValuesForEnv = pISQLConfig.getProjectConf(key,DataModel.getCurrentProject().getIdBdd());
                        qsScoreValuesForEnv = qsScoreValuesForEnv.replaceFirst((String)sorter.getValueAt(selectedRows[i],1)+"_"+
                                                                               sorter.getValueAt(selectedRows[i],2),"");
                        qsScoreValuesForEnv = qsScoreValuesForEnv.replaceFirst(",,",",");
                                        
                        if ((qsScoreValuesForEnv.equals(""))||(qsScoreValuesForEnv.equals(","))) {
                            pISQLConfig.deleteProjectConf(key,DataModel.getCurrentProject().getIdBdd());
                        } else {
                            pISQLConfig.updateProjectConf(key,qsScoreValuesForEnv,DataModel.getCurrentProject().getIdBdd());
                        }
                                        
                    }
                    // IHM
                    /*qsScoreTableModel.removeData(selectedRows[i]);
                      if (qsScoreTableModel.getRowCount() == 0) {
                      deleteQsScoreValueButton.setEnabled(false);
                      }*/
                } catch (Exception exception) {
                    Util.err(exception);
                    Tools.ihmExceptionView(exception);
                }
            }
                        
            //IHM
            refreshQsScoreValuePerformed();
        }
    }
        
    private void visualizeQsScoreValuePerformed() {
        JDialog dialog = new JDialog(SalomeTMFContext.getInstance().ptrFrame,true);
        dialog.setTitle(Language.getInstance().getText("QsScore_graph"));
        JPanel pnl = new JPanel(new BorderLayout()); 
        dialog.setContentPane(pnl); 
        dialog.setSize(600,400); 

        try {
            Vector qsScoreKeys = new Vector();
            ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
            if (pISQLConfig != null){
                Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                for (Enumeration e = projectConfs.keys() ; e.hasMoreElements() ;) {
                    String key = (String)e.nextElement();
                    if (key.startsWith(mantis.getBugTrackerName()+"_QsScore")) {
                        qsScoreKeys.add(key);
                    }
                }
                        
                if ((qsScoreKeys != null)&&(qsScoreKeys.size() != 0)) {
                                
                    TimeSeriesCollection dataset = new TimeSeriesCollection();
                                
                    for (int i=0 ; i<qsScoreKeys.size() ; i++) {
                        int idEnv = -1;
                        Object key = qsScoreKeys.elementAt(i);
                        String[] tmp = ((String)key).split("[_]");
                        if (tmp.length == 3) {
                            idEnv = Integer.parseInt(tmp[2]);
                        }
                        Environment env = DataModel.getCurrentProject().getEnvironmentFromModel(idEnv);
                        TimeSeries ts = new TimeSeries(env.getNameFromModel(),Second.class);
                        String value = (String)projectConfs.get(key);
                        String[] res = value.split("[,]");
                        for (int j=0 ; j<res.length ; j++) {
                            String qsScoreValues = res[j];
                            String[] qsScores = qsScoreValues.split("[_]");
                            if (qsScores.length == 2) {
                                Locale locale = Locale.getDefault();
                                DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                                Date date = dateFormat.parse(qsScores[0]);
                                                                                
                                float qsScore = Float.parseFloat(qsScores[1]); 
                                ts.add(new TimeSeriesDataItem(new Second(date),qsScore));
                            }
                        }
                        dataset.addSeries(ts);
                    }
                                
                    JFreeChart chart = ChartFactory.createTimeSeriesChart(Language.getInstance().getText("QsScoregraphTitle"), "Date", 
                                                                          "QsScore", dataset, true, true, false);
                    chart.setBackgroundPaint(Color.white);
                    XYPlot plot = (XYPlot) chart.getPlot();
                    plot.setBackgroundPaint(Color.lightGray);
                    plot.setDomainGridlinePaint(Color.white);
                    plot.setRangeGridlinePaint(Color.white);
                    XYItemRenderer r = plot.getRenderer();
                    if (r instanceof XYLineAndShapeRenderer) {
                        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
                        renderer.setShapesVisible(true);
                        renderer.setShapesFilled(true);
                    }
                    DateAxis axis = (DateAxis) plot.getDomainAxis();
                    axis.setDateFormatOverride(new SimpleDateFormat("dd/MM/yy"));
                    ValueAxis vAxis = plot.getRangeAxis();
                    vAxis.setRange(0,3);
                    ChartPanel cPanel = new ChartPanel(chart); 
                    pnl.add(cPanel);
                            
                } else {
                    JLabel label = new JLabel(Language.getInstance().getText("No_QsScore_values_in_DB"));
                    pnl.add(label);
                }
                        
            } else {
                Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
            }
        } catch (Exception e){
            Util.err(e);
            Tools.ihmExceptionView(e);
        }
                
        dialog.setVisible(true);
    }

    private void calculateQsScoreValuePerformed() {
        ArrayList environments = DataModel.getCurrentProject().getEnvironmentListFromModel();
        chooseEnvWindow = new AskChooseEnvironment();
        if ((environments == null)&&(environments.size()==0)) {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("QsScore_problem"),
                                          Language.getInstance().getText("Information_"),
                                          JOptionPane.INFORMATION_MESSAGE);
        } else {
            chooseEnvWindow.show();
        }
    }
        
    private void calculateQsScore(Environment env) {
        int choice = -1;
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        chooseEnvWindow.dispose();
        if (env != null) {
            int[] all_score_risk = new int[8];
            all_score_risk[0] = getQsScoreRiskForCat(0);
            all_score_risk[1] = getQsScoreRiskForCat(1);
            all_score_risk[2] = getQsScoreRiskForCat(2);
            all_score_risk[3] = getQsScoreRiskForCat(3);
            all_score_risk[4] = getQsScoreRiskForCat(4);
            all_score_risk[5] = getQsScoreRiskForCat(5);
            all_score_risk[6] = getQsScoreRiskForCat(6);
            all_score_risk[7] = getQsScoreRiskForCat(7);
                        
            int[] all_score_defect = new int[8];
            all_score_defect[0] = getQsScoreDefectForCat(0,env);
            all_score_defect[1] = getQsScoreDefectForCat(1,env);
            all_score_defect[2] = getQsScoreDefectForCat(2,env);
            all_score_defect[3] = getQsScoreDefectForCat(3,env);
            all_score_defect[4] = getQsScoreDefectForCat(4,env);
            all_score_defect[5] = getQsScoreDefectForCat(5,env);
            all_score_defect[6] = getQsScoreDefectForCat(6,env);
            all_score_defect[7] = getQsScoreDefectForCat(7,env);
                        
            int[] all_score = new int[8];
                        
            float qsScore = 3;
            boolean score_null = false;
            int i=0;
            int sum = 0;
            int nb_score = 0;
                        
            String message = "";
                        
            while ((i<8)&&(!score_null)) {
                int tmp_score = Math.min(all_score_risk[i],all_score_defect[i]);
                if (tmp_score == 0) {
                    score_null = true;
                    sum = 0;
                    nb_score++;
                } else if (tmp_score > 0) {
                    sum += tmp_score;
                    nb_score++;
                }
                all_score[i] = tmp_score;
                i++;
            }
            float score_tmp = Math.round(((float)sum/(float)nb_score)*100);
            qsScore = score_tmp/100;
                        
            for (int j=0; j<8 ; j++) {
                message += "- " + Language.getInstance().getText("Categorie") + " \"" + getReqCatString(j) + "\" : sr=" + 
                    getQsScoreString(all_score_risk[j]) + ", sa=" + getQsScoreString(all_score_defect[j]) + ", score=" + 
                    getQsScoreString(all_score[j]) + "\n";
            }
            message += "\n -------> QS Score = " + qsScore + "";
                        
            choice = JOptionPane.showOptionDialog(new Frame(),
                                                  message + Language.getInstance().getText("QsScore_saved_in_DB"),
                                                  Language.getInstance().getText("Information_"),
                                                  JOptionPane.YES_NO_OPTION,
                                                  JOptionPane.QUESTION_MESSAGE,
                                                  null,
                                                  options,
                                                  options[1]);
                        
            if (choice == JOptionPane.YES_OPTION) {
                try {
                    ISQLConfig pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
                    if (pISQLConfig != null){
                        Hashtable projectConfs = pISQLConfig.getAllProjectConf(DataModel.getCurrentProject().getIdBdd());
                        Locale locale = Locale.getDefault();
                        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.LONG, locale);
                        Date date = new Date();
                        String myDate = dateFormat.format(date);
                        String key = mantis.getBugTrackerName()+"_QsScore_"+env.getIdBdd();
                                        
                        if (projectConfs.containsKey(key)) {
                            pISQLConfig.updateProjectConf(key,(String)projectConfs.get(key)+","+myDate+"_"+qsScore,
                                                          DataModel.getCurrentProject().getIdBdd());
                        } else {
                            pISQLConfig.insertProjectConf(mantis.getBugTrackerName()+"_QsScore_"+env.getIdBdd(), 
                                                          myDate+"_"+qsScore, DataModel.getCurrentProject().getIdBdd());
                        }
                                        
                        // IHM
                        ArrayList row = new ArrayList(2);
                        row.add(0,env.getNameFromModel());
                        row.add(1,myDate);
                        row.add(2,new Float(qsScore));
                            
                        qsScoreTableModel.addRow(row);
                                        
                    } else {
                        Util.log("[LookAndFeel] WARNING pISQLConfig = " + pISQLConfig);
                    }
                } catch (Exception E){
                    Util.err(E);
                    Tools.ihmExceptionView(E);
                }
                                
                JOptionPane.showMessageDialog(new Frame(),
                                              Language.getInstance().getText("QsScore_saved_confirmation"),
                                              Language.getInstance().getText("Information_"),
                                              JOptionPane.INFORMATION_MESSAGE);
                                
            }   
        }
    }
        
    private String getQsScoreString(int score) {
        String res = "N/A";
        if (score >= 0) {
            res = ""+score;
        } 
        return res;
    }
        
    private int getQsScoreRiskCovForCat(int cat) {
        int res = 0;
                
        if (reqPlug.isAllReqCovered(cat,1000)) {
            if (reqPlug.isAllReqCovered(cat,100)) {
                if (reqPlug.isAllReqCovered(cat,10)) {
                    res = 3;
                } else {
                    res = 2;
                }
            } else {
                res = 1;
            }
        }
        return res;
    }
        
    private int getQsScoreRiskExecForCat(int cat) {
        int res = 0;
                
        if (reqPlug.isAllReqCoveredExecuted(cat,1000)) {
            if (reqPlug.isAllReqCoveredExecuted(cat,100)) {
                if (reqPlug.isAllReqCoveredExecuted(cat,10)) {
                    res = 3;
                } else {
                    res = 2;
                }
            } else {
                res = 1;
            }
        }
        return res;
    }
        
    private int getQsScoreRiskForCat(int cat) {
        int res = 0;
                
        if (reqPlug.getNbReqForCat(cat) == 0) {
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                    Language.getInstance().getText("No_Req_For_Cat") + " \"" + getReqCatString(cat) + "\".\n" + Language.getInstance().getText("Ask_NA_For_Cat"),
                                                                    Language.getInstance().getText("Attention_"),
                                                                    JOptionPane.WARNING_MESSAGE,
                                                                    options);
            if (choice == JOptionPane.NO_OPTION) {
                res = 0;
            } else {
                res = -1;
            }
                        
        } else {
            res = Math.min(getQsScoreRiskCovForCat(cat),getQsScoreRiskExecForCat(cat));
        }
                
        return res;
    }
        
    private int getQsScoreDefectForCat(int cat, Environment env) {
        int res = 3;
        if (reqPlug.getNbReqForCat(cat)!=0) {
            Vector associatedTests = reqPlug.getAssociatedTestsForReqCat(cat);
            if (associatedTests != null && associatedTests.size()>0) {
                int j=0;
                while ((j < associatedTests.size()) && res>0) {
                    int qs_tmp = 3;
                    Test test = (Test)associatedTests.elementAt(j);
                    qs_tmp = mantis.getQsScoreDefectForTest(test,env);
                    if (qs_tmp<res) {
                        res = qs_tmp;
                    }
                    j++;
                }
            }
        }
                
        return res;
    }
        
    private String getReqCatString(int index) {
        String cat = Language.getInstance().getText("Cat_Autre");
                
        if (index == 0) {
            cat = Language.getInstance().getText("Cat_Fonctionnel");
        } else if (index == 1) {
            cat = Language.getInstance().getText("Cat_Interoperabilite");
        } else if (index == 2) {
            cat = Language.getInstance().getText("Cat_Charge");
        } else if (index == 3) {
            cat = Language.getInstance().getText("Cat_Performance");
        } else if (index == 4) {
            cat = Language.getInstance().getText("Cat_Disponibilite");
        } else if (index == 5) {
            cat = Language.getInstance().getText("Cat_Securite");
        } else if (index == 6) {
            cat = Language.getInstance().getText("Cat_Exploitabilite");
        } else if (index == 7) {
            cat = Language.getInstance().getText("Cat_Autre");
        }
                
        return cat;
    }
        
    class AskChooseEnvironment extends JDialog {
        JPanel panel = new JPanel(new BorderLayout());
            
        JComboBox envComboBox = new JComboBox();
            
        /** Creates a new instance of AskChooseEnvironment */
        public AskChooseEnvironment() {
                
            super(SalomeTMFContext.getInstance().ptrFrame,true);
                
            ArrayList envs = DataModel.getCurrentProject().getEnvironmentListFromModel();
            for (int i=0 ; i<envs.size() ; i++) {
                Environment currentEnv = (Environment)envs.get(i);
                envComboBox.addItem(currentEnv.getNameFromModel());
            }
                
            JButton validateButton = new JButton(Language.getInstance().getText("Valider"));
            validateButton.setToolTipText(Language.getInstance().getText("Valider"));
            validateButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String envString = (String)envComboBox.getSelectedItem();
                        calculateQsScore(DataModel.getCurrentProject().getEnvironmentFromModel(envString));
                        AskChooseEnvironment.this.dispose();
                    }
                });
                
            JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
            cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
            cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        AskChooseEnvironment.this.dispose();
                        
                    }
                });
                
            JLabel envLabel = new JLabel(Language.getInstance().getText("Environnement") + " : ");
                
            JPanel textFieldPane = new JPanel();
            textFieldPane.setLayout(new BoxLayout(textFieldPane,BoxLayout.Y_AXIS));
            textFieldPane.add(envComboBox);
                
            JPanel textPane = new JPanel();
            textPane.setLayout(new BoxLayout(textPane,BoxLayout.Y_AXIS));
            textPane.add(envLabel);
                        
            JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
            textPaneAll.add(textPane);
            textPaneAll.add(textFieldPane);
                
            JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            buttonPanel.add(validateButton);
            buttonPanel.add(cancelButton);
                
            JPanel labelSet = new JPanel();
            labelSet.add(textPaneAll);
                
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(labelSet);
            panel.add(buttonPanel);
            panel.setBorder(BorderFactory.createRaisedBevelBorder());
                
            this.getContentPane().add(panel);
                
            this.setTitle(Language.getInstance().getText("Environnement"));
            //this.setLocation(300,300);
            this.setLocationRelativeTo(this.getParent()); 
            this.pack();
        }

    }
        
}
