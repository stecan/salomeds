/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.filtre.ISimpleDataFilter;
import org.objectweb.salome_tmf.ihm.filtre.TestTreeFiltrePanel;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;
import org.objectweb.salome_tmf.ihm.models.TreeRenderer;

/**
 * Classe qui construit la fen?tre permettant de choisir les tests ? ins?rer
 * dans une campagne
 */
public class FillCampagne extends JDialog {
    /**
     * Mod?le de l'arbre des tests
     */
    protected TestTreeModel testTreeModel;

    /**
     * Mod?le de l'arbre des campagnes
     */
    protected TestTreeModel campagneTreeModel;

    /**
     * l'arbre des campagnes
     */
    JTree campagneTree;

    /**
     * l'arbre des tests
     */
    JTree testTree;
    ISimpleDataFilter testFilter;
    /**
     * Noeud s?lectionn? dans l'arbre des campagnes
     */
    DefaultMutableTreeNode campagneSelectedNode;

    /**
     * Noeud s?lectionn? dans l'arbre des tests
     */
    DefaultMutableTreeNode testSelectedNode;

    /**
     * Racine de l'arbre temporaire des campagnes
     */
    DefaultMutableTreeNode temporaryCampagneRootNode;

    /**
     * Racine de l'arbre final des campagnes
     */
    DefaultMutableTreeNode realCampagneRoot;

    /**
     * Toolkit
     */
    private Toolkit toolkit = Toolkit.getDefaultToolkit();

    /**
     * Liste des tests s?lectionn?s
     */
    ArrayList temporaryTestList;

    /**
     * Liste des suites s?lectionn?es
     */
    //ArrayList temporaryTestListList;

    /**
     * Liste des familles s?lectionn?es
     */
    //ArrayList temporaryFamilyList;

    /**
     * Mod?le de l'arbre qui sera finalement conserv?
     */
    TestTreeModel realModel;


    ArrayList testSelectedNodes;

    ArrayList campaignSelectedNodes;
    /******************************************************************************/
    /**                                                         CONSTRUCTEURS                                                           ***/
    /******************************************************************************/

    boolean okSelected = false;

    /**
     * Constructeur de la fen?tre pour l'ajout de tests ? une campagne.
     * @param campagneRoot le noeud racine de la campagne
     * @param model le mod?le de donn?es des tests
     * @param testRoot la racine de l'arbre des tests
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    public FillCampagne(DefaultMutableTreeNode campagneRoot, TestTreeModel model, DefaultMutableTreeNode testRoot) {

        super(SalomeTMFContext.getInstance().ptrFrame,true);

        temporaryTestList = new ArrayList();
        //temporaryTestListList = new ArrayList();
        //temporaryFamilyList = new ArrayList();

        campaignSelectedNodes = new ArrayList();
        testSelectedNodes = new ArrayList();

        realCampagneRoot = campagneRoot;
        realModel = model;

        TreeRenderer campagneRenderer = new TreeRenderer();
        TreeRenderer testRenderer = new TreeRenderer();


        temporaryCampagneRootNode = new DefaultMutableTreeNode(campagneRoot.getUserObject());

        campagneSelectedNode = temporaryCampagneRootNode;
        campagneTree = new JTree();
        campagneTreeModel = new TestTreeModel(temporaryCampagneRootNode, campagneTree, null);
        campagneTree.setModel(campagneTreeModel);

        initTemporaryTree(campagneRoot);

        campagneTree.setCellRenderer(campagneRenderer);
        campagneTree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    campaignSelectedNodes.clear();
                    TreePath[] pathTab = campagneTree.getSelectionPaths();
                    if (pathTab != null) {
                        for (int i = 0; i < pathTab.length; i++) {
                            campaignSelectedNodes.add(pathTab[i].getLastPathComponent());
                        }
                    }
                }
            });
        testTree = new JTree();
        testTreeModel = new TestTreeModel(testRoot,  testTree, null);
        testTree.setModel(testTreeModel);
        TestTreeFiltrePanel  pTestTreeFiltrePanel = new TestTreeFiltrePanel(testTreeModel);
        testFilter = pTestTreeFiltrePanel.getFilter();
        testTree.setCellRenderer(testRenderer);
        testTree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    testSelectedNodes.clear();
                    TreePath[] pathTab = testTree.getSelectionPaths();
                    if (pathTab != null) {
                        for (int i = 0; i < pathTab.length; i++) {
                            testSelectedNodes.add(pathTab[i].getLastPathComponent());
                        }
                    }
                }
            });


        JButton removeButton = new JButton("<");
        removeButton.setToolTipText(Language.getInstance().getText("Retirer_de_la_campagne"));
        removeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int i = campaignSelectedNodes.size() - 1;
                    while(i >=0) {
                        DefaultMutableTreeNode node = (DefaultMutableTreeNode)campaignSelectedNodes.get(i);
                        ((DefaultTreeModel)campagneTree.getModel()).removeNodeFromParent(node);
                        removeFromModel(node);
                        i = campaignSelectedNodes.size() - 1;
                    }
                }
            });

        JButton addButton = new JButton(">");
        addButton.setToolTipText(Language.getInstance().getText("Ajouter_a_la_campagne"));
        //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
        addButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    for (int i = 0; i < testSelectedNodes.size(); i ++) {
                        Object nodeValue = ((DefaultMutableTreeNode)testSelectedNodes.get(i)).getUserObject();
                        testSelectedNode = ((DefaultMutableTreeNode)testSelectedNodes.get(i));
                        if (nodeValue instanceof Family) {
                            addNodeToNode(testSelectedNode, temporaryCampagneRootNode, false);

                        } else if (nodeValue instanceof TestList) {
                            DefaultMutableTreeNode familyNode = findFamilyNodeInCampagneTree(((TestList)nodeValue).getFamilyFromModel().getNameFromModel());
                            if (familyNode != null) {
                                addNodeToNode(testSelectedNode, familyNode, false);
                            } else {
                                DefaultMutableTreeNode newFamilyNode = addObject(temporaryCampagneRootNode, ((TestList)nodeValue).getFamilyFromModel(), true);
                                addNodeToNode(testSelectedNode, newFamilyNode, false);
                            }
                        } else if (nodeValue instanceof Test){
                            List testListNodes = findTestListNodeInCampagneTree(((Test)nodeValue).getTestListFromModel().getNameFromModel(), ((Test)nodeValue).getTestListFromModel().getFamilyFromModel().getNameFromModel());
                            if (testListNodes.size() != 0) {
                                if(!findTestNodeInCampagneTree(testListNodes, (Test) nodeValue))
                                    addNodeToNode(testSelectedNode, (DefaultMutableTreeNode) testListNodes.get(0), false);
                            } else {
                                DefaultMutableTreeNode familyNode = findFamilyNodeInCampagneTree(((Test)nodeValue).getTestListFromModel().getFamilyFromModel().getNameFromModel());
                                if (familyNode != null) {
                                    DefaultMutableTreeNode newListNode = addObject(familyNode, ((Test)nodeValue).getTestListFromModel(), true);
                                    addNodeToNode(testSelectedNode, newListNode, false);
                                }else {
                                    DefaultMutableTreeNode newFamilyNode = addObject(temporaryCampagneRootNode, ((Test)nodeValue).getTestListFromModel().getFamilyFromModel(), true);
                                    DefaultMutableTreeNode newList = addObject(newFamilyNode, ((Test)nodeValue).getTestListFromModel() , true);
                                    addNodeToNode(testSelectedNode, newList, false);
                                }
                            }
                        }
                    }
                }
            });
        //20100108 - Fin modification Forge ORTF V1.0.0

        JPanel buttonSet = new JPanel();
        buttonSet.setLayout(new BoxLayout(buttonSet, BoxLayout.Y_AXIS));
        buttonSet.add(addButton);
        buttonSet.add(Box.createRigidArea(new Dimension(1,25)));
        buttonSet.add(removeButton);

        JScrollPane campagneScrollPane = new JScrollPane(campagneTree);
        campagneScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Campagne")));
        campagneScrollPane.setPreferredSize(new Dimension(300,450));

        JScrollPane testScrollPane = new JScrollPane(testTree);
        testScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Tests")));
        testScrollPane.setPreferredSize(new Dimension(300,450));

        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BorderLayout());
        southPanel.add(testScrollPane, BorderLayout.CENTER);
        southPanel.add(pTestTreeFiltrePanel, BorderLayout.SOUTH);


        JPanel windowPanel = new JPanel();
        windowPanel.setLayout(new BoxLayout(windowPanel, BoxLayout.X_AXIS));
        //windowPanel.add(testScrollPane);
        windowPanel.add(southPanel);
        windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
        windowPanel.add(buttonSet);
        windowPanel.add(Box.createRigidArea(new Dimension(20,50)));
        windowPanel.add(campagneScrollPane);

        JButton validate = new JButton(Language.getInstance().getText("Valider"));
        validate.setToolTipText(Language.getInstance().getText("Valider"));
        validate.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    cleanTempCampTree();
                    int nbChildren = temporaryCampagneRootNode.getChildCount();
                    for (int k=realCampagneRoot.getChildCount()-1; k >= 0 ; k--) {
                        realModel.removeNodeFromParent((DefaultMutableTreeNode)realCampagneRoot.getChildAt(k));
                    }
                    for (int i= 0; i < nbChildren; i++) {
                        //realCampagneRoot.add((DefaultMutableTreeNode)temporaryCampagneRootNode.getChildAt(0));
                        realModel.insertNodeInto((DefaultMutableTreeNode)temporaryCampagneRootNode.getChildAt(0), realCampagneRoot, realCampagneRoot.getChildCount());
                        //realModel.insertNodeInto((DefaultMutableTreeNode)temporaryCampagneRootNode.getChildAt(0), realCampagneRoot, realCampagneRoot.getChildCount());
                    }

                    cleanTree(realModel, realCampagneRoot );
                    //DataModel.getCurrentCampaign().setFamilyList(temporaryFamilyList);
                    //DataModel.getCurrentCampaign().setTestListList(temporaryTestListList);
                    ArrayList newTestList = new ArrayList() ;

                    int nbFamille = realCampagneRoot.getChildCount();
                    for (int i = 0 ; i < nbFamille; i++){
                        DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) realCampagneRoot.getChildAt(i);
                        int nbSuite = familyNode.getChildCount();
                        for (int j = 0 ; j < nbSuite; j++){
                            DefaultMutableTreeNode suiteNode = (DefaultMutableTreeNode) familyNode.getChildAt(j);
                            Util.log("[FillCampaign] with suite node " + suiteNode.getUserObject());
                            int nbTest = suiteNode.getChildCount();
                            for (int k = 0 ; k < nbTest; k++){
                                DefaultMutableTreeNode testNode = (DefaultMutableTreeNode) suiteNode.getChildAt(k);
                                Util.log("[FillCampaign] with test node " + testNode.getUserObject());
                                Test pTest = (Test) testNode.getUserObject();
                                newTestList.add(pTest);
                            }
                        }
                    }
                    //Util.debug("fill campagne " + DataModel.getCurrentCampaign().getNameFromModel());
                    DataModel.getCurrentCampaign().fillCampInModel(newTestList, DataModel.getCurrentUser());
                    //DataModel.getCurrentCampaign().fillCampInModel(temporaryTestList);
                    okSelected = true;
                    FillCampagne.this.dispose();
                }
            });

        JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
        cancel.setToolTipText(Language.getInstance().getText("Annuler"));
        cancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    okSelected = false;
                    FillCampagne.this.dispose();
                }
            });

        JPanel secondButtonSet = new JPanel();
        secondButtonSet.add(validate);
        secondButtonSet.add(cancel);

        JPanel center = new JPanel();
        center.add(windowPanel);


        JPanel page = new JPanel();
        page.setLayout(new BoxLayout(page, BoxLayout.Y_AXIS));
        page.add(center);
        page.add(secondButtonSet);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        this.setTitle(Language.getInstance().getText("Ajouter_des_tests_a_une_campagne"));
        /*this.pack();
          this.setLocationRelativeTo(this.getParent());
          this.setVisible(true);
        */
        centerScreen();
    } // Fin du constructeur FillCampagne/2

    void centerScreen() {
        Dimension dim = getToolkit().getScreenSize();
        this.pack();
        Rectangle abounds = getBounds();
        setLocation((dim.width - abounds.width) / 2,
                    (dim.height - abounds.height) / 2);
        this.setVisible(true);
        requestFocus();
    }

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    /**
     * M?thode R?cursive qui ajoute un noeud ? un autre noeud. Tous les fils du
     * noeud ? ajouter sont aussi ajouter r?cursivement.
     * @param nodeToBeAdded le noeud ? ajouter
     * @param nodeToReceive le noeud qui re?oit
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    private void addNodeToNode(DefaultMutableTreeNode nodeToBeAdded, DefaultMutableTreeNode nodeToReceive, boolean allowDuplicate) {
        DefaultMutableTreeNode newNode = null;
        if(!allowDuplicate){
            for (int j=0; j < nodeToReceive.getChildCount(); j++) {
                if (((DefaultMutableTreeNode)nodeToReceive.getChildAt(j)).getUserObject().equals(nodeToBeAdded.getUserObject())) {
                    newNode = (DefaultMutableTreeNode)nodeToReceive.getChildAt(j);
                }
            }
        }
        if (newNode == null) {
            SimpleData data = (SimpleData) nodeToBeAdded.getUserObject();
            if (testFilter != null) {
                if (testFilter.isFiltred(data)){
                    newNode = new DefaultMutableTreeNode(nodeToBeAdded.getUserObject());
                    campagneTreeModel.insertNodeInto(newNode, nodeToReceive, nodeToReceive.getChildCount());
                    campagneTree.scrollPathToVisible(new TreePath(newNode.getPath()));
                    /*if (nodeToBeAdded.getUserObject() instanceof Family) {
                      temporaryFamilyList.add(nodeToBeAdded.getUserObject());
                      } else if (nodeToBeAdded.getUserObject() instanceof TestList) {
                      temporaryTestListList.add(nodeToBeAdded.getUserObject());
                      } else */if (nodeToBeAdded.getUserObject() instanceof Test) {
                        temporaryTestList.add(nodeToBeAdded.getUserObject());
                    }
                }
            } else {
                newNode = new DefaultMutableTreeNode(nodeToBeAdded.getUserObject());
                campagneTreeModel.insertNodeInto(newNode, nodeToReceive, nodeToReceive.getChildCount());
                campagneTree.scrollPathToVisible(new TreePath(newNode.getPath()));
                /*if (nodeToBeAdded.getUserObject() instanceof Family) {
                  temporaryFamilyList.add(nodeToBeAdded.getUserObject());
                  } else if (nodeToBeAdded.getUserObject() instanceof TestList) {
                  temporaryTestListList.add(nodeToBeAdded.getUserObject());
                  } else */if (nodeToBeAdded.getUserObject() instanceof Test) {
                    temporaryTestList.add(nodeToBeAdded.getUserObject());
                }
            }

        }
        DefaultMutableTreeNode child;
        Object nodeValue;
        for (int i = 0; i < nodeToBeAdded.getChildCount(); i++) {
            child = (DefaultMutableTreeNode)nodeToBeAdded.getChildAt(i);
            nodeValue = child.getUserObject();
            if(nodeValue instanceof Test){
                List testListNodes = findTestListNodeInCampagneTree(((Test)nodeValue).getTestListFromModel().getNameFromModel(), ((Test)nodeValue).getTestListFromModel().getFamilyFromModel().getNameFromModel());
                if (testListNodes.size() != 0) {
                    if(!findTestNodeInCampagneTree(testListNodes, (Test) nodeValue))
                        addNodeToNode(child, newNode, allowDuplicate);
                }
            } else {
                addNodeToNode(child, newNode, allowDuplicate);
            }
        }
    } // Fin de la m?thode addNodeToNode/2
    //20100108 - Fin modification Forge ORTF V1.0.0

    /**
     * M?thode qui retourne le noeud correspondant ? une famille si le nom pass?
     * en param?tre est le nom d'une famille pr?sente dans l'arbre des campagnes
     * @param familyName un nom
     * @return le noeud correspondant ? une famille si le nom pass?
     * en param?tre est le nom d'une famille pr?sente dans l'arbre des campagnes,
     * <code>null</code> sinon.
     */
    public DefaultMutableTreeNode findFamilyNodeInCampagneTree(String familyName) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)campagneTreeModel.getRoot();
        for (int j = 0; j < root.getChildCount(); j++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(j);
            if (familyNode.getUserObject() instanceof Family && ((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
                return familyNode;
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    /**
     * M?thode qui retourne le noeud correspondant ? une suite si le nom pass?
     * en param?tre est le nom d'une suite pr?sente dans l'arbre des campagnes
     * @param testListName un nom
     * @return le noeud correspondant ? une famille si le nom pass?
     * en param?tre est le nom d'une famille pr?sente dans l'arbre des campagnes,
     * <code>null</code> sinon.
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    public List findTestListNodeInCampagneTree(String testListName, String familyName) {
        List result = new ArrayList();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)campagneTreeModel.getRoot();
        for (int j = 0; j < root.getChildCount(); j++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(j);
            if (familyNode.getUserObject() instanceof Family && ((Family)familyNode.getUserObject()).getNameFromModel().equals(familyName)) {
                for (int k = 0; k < familyNode.getChildCount(); k ++) {
                    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode)familyNode.getChildAt(k);
                    if (testListNode.getUserObject() instanceof TestList && ((TestList)testListNode.getUserObject()).getNameFromModel().equals(testListName)) {
                        result.add(testListNode);
                    }
                }
            }
        }
        return result;
    } // Fin de la m?thode findFamilyNode/1
    //20100108 - Fin modification Forge ORTF V1.0.0

    public boolean findTestNodeInCampagneTree(List testListNodes, Test test){
        for(Iterator iter = testListNodes.iterator(); iter.hasNext();){
            DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) iter.next();
            for(int i = 0; i< testListNode.getChildCount(); i ++){
                if(((DefaultMutableTreeNode)testListNode.getChildAt(i)).getUserObject().equals(test))
                    return true;
            }
        }
        return false;
    }

    /**
     * M?thode d'ajout d'un noeud dans l'arbre sous le parent.
     * @param parent le parent
     * @param child le noeud ? ajouter
     * @param shouldBeVisible visible ou non
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent, Object child, boolean shouldBeVisible) {

        DefaultMutableTreeNode childNode =      new DefaultMutableTreeNode(child);
        if (parent == null) {
            parent = temporaryCampagneRootNode;
        }

        // Insertion du noeud
        campagneTreeModel.insertNodeInto(childNode, parent, parent.getChildCount());

        // on s'assure que le noeud est visible
        if (shouldBeVisible) {
            campagneTree.scrollPathToVisible(new TreePath(childNode.getPath()));
        }
        return childNode;
    } // Fin de la classe addObject/3

    /**
     * Suppression du noeud courant.
     */
    public void removeCurrentNode() {
        TreePath currentSelection = campagneTree.getSelectionPath();
        if (currentSelection != null) {
            DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)(currentSelection.getLastPathComponent());
            MutableTreeNode parent = (MutableTreeNode)(currentNode.getParent());
            if (parent != null) {
                campagneTreeModel.removeNodeFromParent(currentNode);
                removeFromModel(currentNode);
                return;
            }
        }
        // Si aucune s?lection ou si la racine est s?lectionn?e
        toolkit.beep();
    } // Fin de la classe removeCurrentNode/0

    /**
     * Supprime du mod?le les ?l?ments retir?s de la campagne.
     * @param node le noeud ? partir duquel on retire les ?l?ments
     */
    private void removeFromModel(DefaultMutableTreeNode node) {
        for (int i = 0; i < node.getChildCount(); i++) {
            removeFromModel((DefaultMutableTreeNode)node.getChildAt(i));
        }
        /*if (node.getUserObject() instanceof Family) {
          temporaryFamilyList.remove(node.getUserObject());
          } else if (node.getUserObject() instanceof TestList) {
          temporaryTestListList.remove(node.getUserObject());
          } else */if (node.getUserObject() instanceof Test) {
            temporaryTestList.remove(node.getUserObject());
        }
    } // Fin de la m?thode removeFromModel/1

    /**
     * M?thode qui initialise l'arbre temporaire ? partir de la racine pass?
     * en param?tre.
     * @param root la racine du nouvel arbre temporaire
     */
    public void initTemporaryTree(DefaultMutableTreeNode root) {
        for (int i = 0 ; i < root.getChildCount(); i ++) {
            addNodeToNode((DefaultMutableTreeNode)root.getChildAt(i), temporaryCampagneRootNode, true);
        }
    } // Fin de la m?thode initTemporaryTree/1




    public void cleanTree(TestTreeModel model, DefaultMutableTreeNode root) {

        /*for (int i = root.getChildCount()-1; i >=0 ; i--) {
          DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode)root.getChildAt(i);
          for (int j = familyNode.getChildCount()-1; j >= 0 ; j--) {
          if (familyNode.getChildAt(j).getChildCount() == 0) {
          temporaryTestListList.remove(((DefaultMutableTreeNode)familyNode.getChildAt(j)).getUserObject());
          ((Family)familyNode.getUserObject()).getTestListList().remove(((DefaultMutableTreeNode)familyNode.getChildAt(j)).getUserObject());
          model.removeNodeFromParent((DefaultMutableTreeNode)familyNode.getChildAt(j));

          }
          }
          if (familyNode.getChildCount() == 0) {
          temporaryFamilyList.remove((familyNode).getUserObject());
          model.removeNodeFromParent(familyNode);

          }
          }*/
    }

    public boolean okSelected(){
        return okSelected;
    }

    //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
    /**
     * Remove All empty suite/family from temporary campagne tree.
     * Concat test suite if necessary.
     *
     * @method cleanTempCampTree()
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    public void cleanTempCampTree(){
        DefaultMutableTreeNode familyNode;
        Enumeration testSuiteEnum;
        DefaultMutableTreeNode testSuiteNode;
        DefaultMutableTreeNode precTestSuite = null;
        Enumeration familyEnum = temporaryCampagneRootNode.children();
        while(familyEnum.hasMoreElements()){
            familyNode = (DefaultMutableTreeNode) familyEnum.nextElement();
            testSuiteEnum = familyNode.children();
            while(testSuiteEnum.hasMoreElements()){
                testSuiteNode = (DefaultMutableTreeNode) testSuiteEnum.nextElement();
                if((precTestSuite != null) && (precTestSuite.getUserObject().equals(testSuiteNode.getUserObject()))){
                    int index = precTestSuite.getChildCount();
                    for(int i=0; i < testSuiteNode.getChildCount(); i++){
                        campagneTreeModel.insertNodeInto((MutableTreeNode) testSuiteNode.getChildAt(i),precTestSuite, index);
                        index++;
                    }
                    testSuiteNode.removeAllChildren();
                }
                if(testSuiteNode.getChildCount() == 0){
                    campagneTreeModel.removeNodeFromParent(testSuiteNode);
                } else {
                    precTestSuite = testSuiteNode;
                }
            }
            if(familyNode.getChildCount() == 0)
                campagneTreeModel.removeNodeFromParent(familyNode);
        }
    }
    //20100108 - Fin modification Forge ORTF V1.0.0
} // Fin de la classe FillCampagne
