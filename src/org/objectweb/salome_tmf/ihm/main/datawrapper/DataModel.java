/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main.datawrapper;

import java.awt.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import org.apache.log4j.Category;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AutomaticTestWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.ManualTestWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataLoader;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.IDataModelWrapper;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.admin.AskName;
import org.objectweb.salome_tmf.ihm.common.AskNameAndDescription;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.AskNewCampagne;
import org.objectweb.salome_tmf.ihm.main.AskNewTest;
import org.objectweb.salome_tmf.ihm.main.AskNewTestList;
import org.objectweb.salome_tmf.ihm.main.AutomaticTestScriptView;
import org.objectweb.salome_tmf.ihm.main.BaseIHM;
import org.objectweb.salome_tmf.ihm.main.ExecutionView;
import org.objectweb.salome_tmf.ihm.main.FillCampagne;
import org.objectweb.salome_tmf.ihm.main.IAssignedCampAction;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.models.DynamicTree;
import org.objectweb.salome_tmf.ihm.models.ExecutionTableModel;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import static org.objectweb.salome_tmf.ihm.tools.Tools.completeExecutionResult;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.login.LoginSalomeTMF;
import org.objectweb.salome_tmf.plugins.UICompCst;

public class DataModel implements ApiConstants, DataConstants,
                                  IDataModelWrapper {

    /**
     * Logger for this class
     */
    static final Category logger = Category.getInstance(DataModel.class);
    
    private static BaseIHM applet;

    /* Les Models de tables de donnees */
    /**
     * Modele de donnees des tables contenant l'ensemble de tous les parametres
     */
    static private MyTableModel parameterTableModel;
    static private MyTableModel dataSetTableModel;
    static private MyTableModel environmentTableModel;

    /**
     * Modele de donnees des tables contenant les parametres d'un test
     */
    static private MyTableModel testParameterTableModel;

    /**
     * Modeles de donnees pour les parametres d'un environnement
     */
    static private MyTableModel environmentParameterTableModel;

    /**
     * Modele de donnees des tables contenant les actions d'un test
     */
    static private MyTableModel actionTableModel;

    /**
     * Modele de donnees des tables contenant les executions d'une campagne
     */
    static private ExecutionTableModel executionTableModel;

    /**
     * Modele de donnees des tables contenant les resultats des executions dans
     * la vue des executions
     */
    static private MyTableModel executionResultTableModel;

    /**
     * Modele de donnees des tables contenant les attachements
     */
    static private MyTableModel attachmentTableModel;

    /**
     * Modele de donnees des tables contenant les attachements
     */
    static private MyTableModel defectTableModel;

    /* Les donnees courantes de l'IHM */
    static Test currentTest;
    static TestList currentTestList;
    static Campaign currentCampaign;
    static Family currentFamily;
    static Project currentProject;
    static User currentUser;

    static Environment currentEnvironment;
    static Action currentAction;
    static Test testObservedInExecution;
    static Execution observedExecution;
    static ExecutionResult obervedExecutionResult;
    static ArrayList selectedExecution = new ArrayList();
    static private ExecutionTestResult currentExecutionTestResult;

    static private boolean badDirectoryView;

    static private String caretDescriptionValue;
    static private boolean campaignChange;
    static private boolean testChange;
    static private boolean testListChange;
    static private boolean familyChange;

    static private String oldCaretDescription;
    static private boolean beginDescriptionModification;
    static private HashSet testModifications;
    static private HashSet campaignModifications;
    static private HashSet dataModifications;

    static private IAssignedCampAction pIAssignedCampAction;

    Hashtable dynamicNodeMap = new Hashtable();
    Hashtable campaignNodeMap = new Hashtable();

    // static DataLoader pProjectData;
    static DataModel pDataModel = null;

    /**************************************************************************/
    /** METHODES PUBLIQUES ***/
    /**************************************************************************/
    public static User getCurrentUser() {
        return currentUser;
    }

    public static Project getCurrentProject() {
        return currentProject;
    }

    /**
     * @return la campagne en cours de traitement
     */
    public static Campaign getCurrentCampaign() {
        return currentCampaign;
    } // Fin de la methode getCurrentCampaign/0

    /**
     * @return le test en cours de traitement
     */
    public static Test getCurrentTest() {
        return currentTest;
    } // Fin de la methode getCurrentTest/0

    /**
     * @return la suite de tests en cours de traitement
     */
    public static TestList getCurrentTestList() {
        return currentTestList;
    } // Fin de la methode getCurrentTestList/0

    /**
     * @return la suite de tests en cours de traitement
     */
    public static Family getCurrentFamily() {
        return currentFamily;
    } // Fin de la methode getCurrentFamily/0

    /**
     * @param campagne
     *            courante
     */
    public static void setCurrentCampaign(Campaign campaign) {
        currentCampaign = campaign;
    } // Fin de la methode setCurrentcampaign/1

    /**
     * @param testList
     *            courantes
     */
    public static void setCurrentTestList(TestList testList) {
        currentTestList = testList;
    } // Fin de la methode setCurrentTestList/1

    /**
     * @param test
     *            courant
     */
    public static void setCurrentTest(Test test) {
        currentTest = test;
    } // Fin de la methode setCurrentTest/1

    /**
     * @return
     */
    public static Environment getCurrentEnvironment() {
        return currentEnvironment;
    }

    /**
     * @param environment
     */
    public static void setCurrentEnvironment(Environment environment) {
        currentEnvironment = environment;
    }

    /**
     * @return
     */
    public static Action getCurrentAction() {
        return currentAction;
    }

    /**
     * @param action
     */
    public static void setCurrentAction(Action action) {
        currentAction = action;
    }

    /**
     * @param Famille
     *            courante
     */
    public static void setCurrentFamily(Family family) {
        currentFamily = family;
    } // Fin de la methode setCurrentFamily/1

    /**
     * @return
     */
    public static Test getTestObservedInExecution() {
        return testObservedInExecution;
    }

    /**
     * @param test
     */
    public static void setTestObservedInExecution(Test test) {
        testObservedInExecution = test;
    }

    /**
     * @return
     */
    public static ExecutionResult getObservedExecutionResult() {
        return obervedExecutionResult;
    }

    /**
     * @return
     */
    public static Execution getObservedExecution() {
        return observedExecution;
    }

    /**
     * @param result
     */
    public static void setObervedExecutionResult(ExecutionResult result) {
        obervedExecutionResult = result;
    }

    /**
     * @param execution
     */
    public static void setObservedExecution(Execution execution) {
        observedExecution = execution;
    }

    public static void addSelectedExecution(Execution exec) {
        selectedExecution.add(exec);
    }

    public static void removeSelectedExecution(Execution exec) {
        selectedExecution.remove(exec);
    }

    /**
     * @return
     */
    public static ArrayList getSelectedExecution() {
        return selectedExecution;
    }

    /**
     * @param list
     */
    public static void setSelectedExecution(ArrayList list) {
        selectedExecution = list;
    }

    /**
     * @return
     */
    public static ExecutionTestResult getCurrentExecutionTestResult() {
        return currentExecutionTestResult;
    }

    /**
     * @param result
     */
    public static void setCurrentExecutionTestResult(ExecutionTestResult result) {
        currentExecutionTestResult = result;
    }

    /**
     * @return le modele de la table contenant tous les parametres
     */
    public static MyTableModel getParameterTableModel() {
        return parameterTableModel;
    }

    /**
     * @param model
     *            le modele de la
     */
    public static void setParameterTableModel(MyTableModel model) {
        parameterTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getDataSetTableModel() {
        return dataSetTableModel;
    }

    /**
     * @return
     */
    public static MyTableModel getEnvironmentTableModel() {
        return environmentTableModel;
    }

    /**
     * @param model
     */
    public static void setDataSetTableModel(MyTableModel model) {
        dataSetTableModel = model;
    }

    /**
     * @param model
     */
    public static void setEnvironmentTableModel(MyTableModel model) {
        environmentTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getTestParameterTableModel() {
        return testParameterTableModel;
    }

    /**
     * @param model
     */
    public static void setTestParameterTableModel(MyTableModel model) {
        testParameterTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getEnvironmentParameterTableModel() {
        return environmentParameterTableModel;
    }

    /**
     * @param model
     */
    public static void setEnvironmentParameterTableModel(MyTableModel model) {
        environmentParameterTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getActionTableModel() {
        return actionTableModel;
    }

    /**
     * @param model
     */
    public static void setActionTableModel(MyTableModel model) {
        actionTableModel = model;
    }

    /**
     * @return
     */
    public static ExecutionTableModel getExecutionTableModel() {
        return executionTableModel;
    }

    /**
     * @param model
     */
    public static void setExecutionTableModel(ExecutionTableModel model) {
        executionTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getExecutionResultTableModel() {
        return executionResultTableModel;
    }

    /**
     * @param model
     */
    public static void setExecutionResultTableModel(MyTableModel model) {
        executionResultTableModel = model;
    }

    /**
     * @return
     */
    public static MyTableModel getAttachmentTableModel() {
        return attachmentTableModel;
    }

    /**
     * @param model
     */
    public static void setAttachmentTableModel(MyTableModel model) {
        attachmentTableModel = model;
    }

    public static MyTableModel getDefectTableModel() {
        return defectTableModel;
    }

    /**
     * @param model
     */
    public static void setDefectTableModel(MyTableModel model) {
        defectTableModel = model;
    }

    /**
     * @return
     */
    public static BaseIHM getApplet() {
        return applet;
    }

    /**
     * @param applet
     */
    public static void setApplet(BaseIHM pApplet) {
        applet = pApplet;
    }

    /**
     * @return
     */
    public static boolean isBadDirectoryView() {
        return badDirectoryView;
    }

    /**
     * @param b
     */
    public static void setBadDirectoryView(boolean b) {
        badDirectoryView = b;
    }

    /**
     * @return
     */
    public static String getCaretDescriptionValue() {
        return caretDescriptionValue;
    }

    /**
     * @param string
     */
    public static void setCaretDescriptionValue(String string) {
        caretDescriptionValue = string;
    }

    /**
     * @return
     */
    /*
     * public static boolean isCampaignChange() { return campaignChange; }
     *
     *
     * public static void setCampaignChange(boolean b) { campaignChange = b; }
     */

    /*
     * public static boolean isTestChange() { return testChange; }
     */

    /*
     * public static void setTestChange(boolean b) { testChange = b; }
     */

    /*
     * public static boolean isTestListChange() { return testListChange; }
     *
     *
     * public static void setTestListChange(boolean b) { testListChange = b; }
     */

    /**
     * @return
     */
    /*
     * public static String getOldCaretDescription() { return
     * oldCaretDescription; }
     *
     *
     * public static void setOldCaretDescription(String string) {
     * oldCaretDescription = string; }
     */

    /**
     * @return
     */
    /*
     * public static boolean isBeginDescriptionModification() { return
     * beginDescriptionModification; }
     *
     *
     * public static void setBeginDescriptionModification(boolean b) {
     * beginDescriptionModification = b; }
     */

    /**
     * @return
     */
    public static boolean isFamilyChange() {
        return familyChange;
    }

    /**
     * @param b
     */
    /*
     * public static void setFamilyChange(boolean b) { familyChange = b; }
     */

    public static void setAssignedCampAction(
                                             IAssignedCampAction _pIAssignedCampAction) {
        pIAssignedCampAction = _pIAssignedCampAction;
    }

    public static IAssignedCampAction getAssignedCampAction() {
        return pIAssignedCampAction;
    }

    /*****************************************************************************************/

    public static void addTestModification(Object obj) {
        testModifications.add(obj);
    }

    public static void removeTestModification(Object obj) {
        testModifications.remove(obj);
    }

    public static HashSet getTestModification() {
        return testModifications;
    }

    public static boolean containsTestModification(Object obj) {
        return testModifications.contains(obj);
    }

    public static boolean containsTestModification(int i) {
        for (Iterator iter = testModifications.iterator(); iter.hasNext();) {
            Integer elem = (Integer) iter.next();
            if (elem.intValue() == i)
                return true;
        }
        return false;
    }

    public static void addCampaignModification(Object obj) {
        campaignModifications.add(obj);
    }

    public static void removeCampaignModification(Object obj) {
        campaignModifications.remove(obj);
    }

    public static HashSet getCampaignModification() {
        return campaignModifications;
    }

    public static boolean containsCampaignModification(Object obj) {
        return campaignModifications.contains(obj);
    }

    public static boolean containsCampaignModification(int i) {
        for (Iterator iter = campaignModifications.iterator(); iter.hasNext();) {
            Integer elem = (Integer) iter.next();
            if (elem.intValue() == i)
                return true;
        }
        return false;
    }

    public static void addDataModification(Object obj) {
        dataModifications.add(obj);
    }

    public static void removeDataModification(Object obj) {
        dataModifications.remove(obj);
    }

    public static HashSet getDataModification() {
        return dataModifications;
    }

    public static boolean containsDataModification(Object obj) {
        return dataModifications.contains(obj);
    }

    public static boolean containsDataModification(int i) {
        for (Iterator iter = dataModifications.iterator(); iter.hasNext();) {
            Integer elem = (Integer) iter.next();
            if (elem.intValue() == i)
                return true;
        }
        return false;
    }

    // **** DataModelWrapper Implementation *******//
    public static DataModel getInstance() {
        if (pDataModel == null) {
            pDataModel = new DataModel();
        }
        return pDataModel;

    }

    @Override
    public void addParameterToModel(ArrayList data) {
        parameterTableModel.addRow(data);
    }

    @Override
    public void clearParameterToModel() {
        parameterTableModel.clearTable();
    }

    @Override
    public void addEnvToModel(ArrayList data) {
        environmentTableModel.addRow(data);
    }

    @Override
    public void clearEnvModel() {
        environmentTableModel.clearTable();
    }

    @Override
    public void addFamilyToModel(Family pFamily) {
        DefaultMutableTreeNode familyNode;
        familyNode = SalomeTMFPanels.getTestDynamicTree().addObject(null,
                                                                    pFamily, true);
        dynamicNodeMap.put(pFamily, familyNode);
    }

    @Override
    public void updateFamilyToModel(Family pFamily) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveFamilyNode(
                                                                  pFamily.getNameFromModel(), true);
        dynamicNodeMap.remove(pFamily);
        addFamilyToModel(pFamily);
    }

    @Override
    public void clearFamilyToModel(Family pFamily) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveFamilyNode(
                                                                  pFamily.getNameFromModel(), false);
    }

    @Override
    public void refreshFamilyToModel(Family pFamily) {
        if (pFamily.equals(currentFamily)) {
            initFamily(pFamily);
        }
    }

    @Override
    public void addTestListToModel(Family pFamily, TestList pList) {
        DefaultMutableTreeNode testListNode;
        DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) dynamicNodeMap
            .get(pFamily);
        testListNode = SalomeTMFPanels.getTestDynamicTree().addObject(
                                                                      familyNode, pList, false);
        dynamicNodeMap.put(pList, testListNode);
    }

    @Override
    public void updateTestListToModel(TestList pList) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveTestListNode(
                                                                    pList.getNameFromModel(),
                                                                    pList.getFamilyFromModel().getNameFromModel(), true);
        dynamicNodeMap.remove(pList);
        addTestListToModel(pList.getFamilyFromModel(), pList);
    }

    @Override
    public void clearTestListToModel(TestList pList) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveTestListNode(
                                                                    pList.getNameFromModel(),
                                                                    pList.getFamilyFromModel().getNameFromModel(), false);

    }

    @Override
    public void refreshTestListToModel(TestList pList) {
        if (pList.equals(currentTestList)) {
            initTestList(pList);
        }
    }

    @Override
    public void addTestToModel(TestList pList, Test pTest) {
        DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) dynamicNodeMap
            .get(pList);
        DefaultMutableTreeNode testNode = SalomeTMFPanels.getTestDynamicTree()
            .addObject(testListNode, pTest, false);
        dynamicNodeMap.put(pTest, testNode);
    }

    @Override
    public void updateTestToModel(TestList pList, Test pTest) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveTestNode(
                                                                pTest.getNameFromModel(), pList.getNameFromModel(),
                                                                pList.getFamilyFromModel().getNameFromModel(), true);
        dynamicNodeMap.remove(pTest);
        addTestToModel(pList, pTest);
    }

    @Override
    public void refreshTestToModel(Test pTest) {
        if (pTest.equals(currentTest)) {
            // le test traite est affiche a l'ecran, il faut modifier
            // l'affichage avec les nouvelles valeurs
            initTest(pTest);
            if (pTest instanceof ManualTest) {
                initActionTable((ManualTest) pTest);
            }
        }
    }

    @Override
    public void clearTestToModel(Test pTest) {
        SalomeTMFPanels.getTestDynamicTree().findRemoveTestNode(
                                                                pTest.getNameFromModel(),
                                                                pTest.getTestListFromModel().getNameFromModel(),
                                                                pTest.getTestListFromModel().getFamilyFromModel()
                                                                .getNameFromModel(), true);
    }

    @Override
    public void addCampaignToModel(Campaign campaign) {
        DefaultMutableTreeNode campaignNode;
        campaignNode = SalomeTMFPanels.getCampaignDynamicTree().addObject(null,
                                                                          campaign, true);
        campaignNodeMap.put(campaign, campaignNode);
    }

    @Override
    public void upadateCampaignToModel(Campaign campaign) {
        DefaultMutableTreeNode campaignNode = SalomeTMFPanels
            .getCampaignDynamicTree().findRemoveCampaignNode(
                                                             campaign.getNameFromModel(), false);
        campaignNodeMap.remove(campaign);
        campaignNodeMap.put(campaign, campaignNode);
    }

    @Override
    public void refreshCampaignToModel(Campaign campaign) {
        if (campaign.equals(getCurrentCampaign())) {
            initCampaign(campaign);
        }
    }

    @Override
    public void addCampaignFamilyToModel(Campaign campaign, Family pFamily) {
        DefaultMutableTreeNode campaignNode = (DefaultMutableTreeNode) campaignNodeMap
            .get(campaign);
        DefaultMutableTreeNode familyNode = SalomeTMFPanels
            .getCampaignDynamicTree().addObject(campaignNode, pFamily,
                                                false);
        campaignNodeMap.put(pFamily, familyNode);
    }

    @Override
    public void upadateCampaignFamilyToModel(Campaign campaign, Family pFamily) {
        DefaultMutableTreeNode familyNode = SalomeTMFPanels
            .getCampaignDynamicTree().findRemoveFamilyNodeInCampagneTree(
                                                                         pFamily.getNameFromModel(),
                                                                         campaign.getNameFromModel(), false);
        campaignNodeMap.remove(pFamily);
        campaignNodeMap.put(pFamily, familyNode);
    }

    @Override
    public void addCampaignTestListToModel(Family pFamily, TestList pList) {
        DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) campaignNodeMap
            .get(pFamily);
        DefaultMutableTreeNode testListNode = SalomeTMFPanels
            .getCampaignDynamicTree().addObject(familyNode, pList, false);
        campaignNodeMap.put(pList, testListNode);
    }

    @Override
    public void upadateCampaignTestListToModel(Campaign campaign,
                                               Family pFamily, TestList pList) {
        DefaultMutableTreeNode testListNode = SalomeTMFPanels
            .getCampaignDynamicTree().findRemoveTestListNodeInCampagneTree(
                                                                           pList.getNameFromModel(), pFamily.getNameFromModel(),
                                                                           campaign.getNameFromModel(), false);
        campaignNodeMap.remove(pList);
        campaignNodeMap.put(pList, testListNode);
    }

    @Override
    public void addCampaignTestToModel(TestList pList, Test pTest) {
        DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) campaignNodeMap
            .get(pList);
        SalomeTMFPanels.getCampaignDynamicTree().addObject(testListNode, pTest,
                                                           false);
        campaignNodeMap.put(pTest, testListNode);
    }

    @Override
    public void upadateCampaignTestToModel(TestList pList, Test pTest) {

    }

    // /////////////////////////////////////////////////////////////////////////

    public static void clearData() {
        dataClear();
        tableClear();
    }

    public static void initData() {
        dataClear();
        initTableModel();
    }

    private static void dataClear() {
        currentTest = null;
        currentTestList = null;
        currentCampaign = null;
        currentFamily = null;
        currentEnvironment = null;
        currentAction = null;
        currentExecutionTestResult = null;

        campaignChange = false;
        testChange = false;
        testListChange = false;
        beginDescriptionModification = false;
        badDirectoryView = true;

        selectedExecution.clear();
        testModifications = new HashSet();
        campaignModifications = new HashSet();
        dataModifications = new HashSet();
    }

    private static void tableClear() {
        parameterTableModel.clearTable();
        dataSetTableModel.clearTable();
        environmentTableModel.clearTable();
        testParameterTableModel.clearTable();
        environmentParameterTableModel.clearTable();
        actionTableModel.clearTable();
        executionTableModel.clearTable();
        executionResultTableModel.clearTable();
        attachmentTableModel.clearTable();
        defectTableModel.clearTable();
    }

    public static void initTableModel() {
        if (parameterTableModel == null) {
            parameterTableModel = new MyTableModel();
            parameterTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Nom"));
            parameterTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Description"));
        }
        // parameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // parameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));

        if (testParameterTableModel == null) {
            testParameterTableModel = new MyTableModel();
            testParameterTableModel.addColumnNameAndColumn(Language
                                                           .getInstance().getText("Nom"));
            testParameterTableModel.addColumnNameAndColumn(Language
                                                           .getInstance().getText("Description"));
        }
        // testParameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // testParameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));

        if (environmentParameterTableModel == null) {
            environmentParameterTableModel = new MyTableModel();
            environmentParameterTableModel.addColumnNameAndColumn(Language
                                                                  .getInstance().getText("Nom"));
            environmentParameterTableModel.addColumnNameAndColumn(Language
                                                                  .getInstance().getText("Valeur"));
            environmentParameterTableModel.addColumnNameAndColumn(Language
                                                                  .getInstance().getText("Description"));
        }
        // environmentParameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // environmentParameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Valeur"));
        // environmentParameterTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));

        if (actionTableModel == null) {
            actionTableModel = new MyTableModel();
            actionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Nom"));
            actionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Description"));
            actionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Resultat_attendu"));
            actionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Attachements"));
        }
        // actionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // actionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        // actionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resultat_attendu"));
        // actionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Attachements"));

        if (executionTableModel == null) {
            executionTableModel = new ExecutionTableModel();
            executionTableModel.addColumnNameAndColumn("");
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Nom"));
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Environnement"));
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Statut"));
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Date_de_creation"));
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Attachements"));
            executionTableModel.addColumnNameAndColumn(Language.getInstance()
                                                       .getText("Derniere_Execution"));
        }
        // executionTableModel.addColumnNameAndColumn("");
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Environnement"));
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Jeu_de_donnees"));
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date_de_creation"));
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Attachements"));
        // executionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Derniere_Execution"));

        if (executionResultTableModel == null) {
            executionResultTableModel = new MyTableModel();
            executionResultTableModel.addColumnNameAndColumn(Language
                                                             .getInstance().getText("Nom"));
            executionResultTableModel.addColumnNameAndColumn(Language
                                                             .getInstance().getText("Date"));
            executionResultTableModel.addColumnNameAndColumn(Language
                                                             .getInstance().getText("Testeur"));
            executionResultTableModel.addColumnNameAndColumn(Language
                                                             .getInstance().getText("Resultat"));
            executionResultTableModel.addColumnNameAndColumn(Language
                                                             .getInstance().getText("Statistiques"));
        }
        // executionResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // executionResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date"));
        // executionResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Testeur"));
        // executionResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Resultat"));
        // executionResultTableModel.addColumnNameAndColumn(Language.getInstance().getText("Statistiques"));

        if (attachmentTableModel == null) {
            attachmentTableModel = new MyTableModel();
            attachmentTableModel.addColumnNameAndColumn(Language.getInstance()
                                                        .getText("Nom"));
            attachmentTableModel.addColumnNameAndColumn(Language.getInstance()
                                                        .getText("Taille"));
            attachmentTableModel.addColumnNameAndColumn(Language.getInstance()
                                                        .getText("Date_de_creation"));
        }
        // attachmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        // attachmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Taille"));
        // attachmentTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date_de_creation"));
        if (defectTableModel == null) {
            defectTableModel = new MyTableModel();
            defectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Execution"));
            defectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Test"));
            defectTableModel.addColumnNameAndColumn("Id");
            defectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Nom"));
            defectTableModel.addColumnNameAndColumn(/*
                                                     * Language.getInstance().getText
                                                     * (
                                                     */"Type"/* ) */);
            defectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Priorite__"));
            defectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                    .getText("Status"));
        }
    }

    /********************************************* Init Data ************************************************************/

    // public static void initDataTests(String p, String userLogin, JApplet
    // applet) {
    public static void loadFromBase(String p, String userLogin, BaseIHM applet) {
        setApplet(applet);
        // initData();
        if (applet.isGraphique()) {
            DataLoader.setDataModelWrapper(getInstance());
        }
        try {
            if (DataLoader.dataConnected()) {
                if (applet.isGraphique()) {
                    clearData();
                    SalomeTMFPanels.getTestDynamicTree().clear();
                    SalomeTMFPanels.getCampaignDynamicTree().clear();
                }
                DataLoader.loadData(p, userLogin, SalomeTMFContext
                                    .getInstance().getUrlBase(), true);
                currentProject = DataLoader.getCurrentProject();
                currentUser = DataLoader.getCurrentUser();
            }
        } catch (Exception e) {
            if (applet.isGraphique()) {
                Tools.ihmExceptionView(e);
                SalomeTMFContext
                    .getInstance()
                    .showMessage(
                                 Language
                                 .getInstance()
                                 .getText(
                                          "Impossible_de_se_connecter_a_la_base__on_travaille_en_local_"),
                                 Language.getInstance().getText("Erreur_"),
                                 JOptionPane.ERROR_MESSAGE);
            } else {
                Util.err(e);
            }
        }
    }

    // public static void refreshFromBase() {
    public static void reloadFromBase(boolean dynamicLoad) {
        if (Api.isConnected()) {
            int transNumber = -1;
            try {
                transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
                // DATA and IHM
                if (applet.isGraphique()) {
                    clearData();
                    SalomeTMFPanels.clearTrees();
                }
                // transNumber = Api.beginTransaction(ApiConstants.LOADING);
                DataLoader.reloadData(dynamicLoad);

                // SalomeTMFPanels.getTestMultiUserChangeListenerPanel().reset();
                currentProject = DataLoader.getCurrentProject();
                currentUser = DataLoader.getCurrentUser();
                afterRefresh();
                System.gc();
                Api.commitTrans(transNumber);
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                if (applet.isGraphique()) {
                    Tools.ihmExceptionView(exception);
                } else {
                    Util.err(exception);
                }
            }
        } else {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText(
                                                                                      "Impossible__Vous_n_etes_pas_connecte_a_la_base"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            /*
             * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame
             * (),Language.getInstance().getText(
             * "Impossible__Vous_n_etes_pas_connecte_a_la_base"),
             * Language.getInstance().getText("Erreur_"),
             * JOptionPane.ERROR_MESSAGE);
             */
        }

    }

    /**
     * Methode qui reinitialise la table des actions pour les tests manuels
     */
    public static void initActionTable(ManualTest test) {
        if (test != null) {
            ArrayList actionList = test.getActionListFromModel(false);
            if ((actionList != null) && (actionList.size() > 0)) {
                actionTableModel.clearTable();
                for (int i = 0; i < actionList.size(); i++) {
                    ArrayList actionParameters = new ArrayList();
                    actionParameters.add(((Action) actionList.get(i))
                                         .getNameFromModel());
                    actionParameters.add(((Action) actionList.get(i))
                                         .getDescriptionFromModel());
                    actionParameters.add(((Action) actionList.get(i))
                                         .getAwaitedResultFromModel());
                    actionParameters.add(((Action) actionList.get(i))
                                         .getAttachmentMapFromModel().keySet());
                    actionTableModel.addRow(actionParameters);
                }
            } else {
                actionTableModel.clearTable();
            }
        }
        // .
        // setColumnSize(actionTableModel.get);
        // setColumnSize(actionParameters.get);
    } // Fin de la methode initActionTable/0

    /**
     * M?thode qui initialise les d?tails d'un test pass? en param?tre, ses
     * attachements et ses param?tres.
     *
     * @param test
     *            un test
     */
    public static void initTest(Test test) {
        if (test != null && test instanceof ManualTest) {
            /*
             * SalomeTMFPanels.getManualTestDateLabel().setText(Language.getInstance
             * ().getText("Date_de_creation__") +
             * test.getCreationDateFromModel().toString());
             * SalomeTMFPanels.getManualTestConceptorLabel
             * ().setText(Language.getInstance().getText("Concepteur__") +
             * test.getConceptorFromModel());
             * SalomeTMFPanels.getManualTestNameLabel
             * ().setText(Language.getInstance().getText("Nom_du_test__") +
             * test.getNameFromModel());
             */
            SalomeTMFPanels.setTestPanelTestInfo(DataConstants.MANUAL_TEST,
                                                 test.getNameFromModel(), test.getConceptorFromModel(), test
                                                 .getCreationDateFromModel().toString());
            SalomeTMFPanels.setTestPanelDescription(DataConstants.MANUAL_TEST,
                                                    test.getDescriptionFromModel());
            boolean executed = false;
            ArrayList campaignList = currentProject.getCampaignOfTest(test);
            if (campaignList != null && campaignList.size() > 0) {
                int i = 0;
                int size = campaignList.size();
                SalomeTMFPanels.getManualTestButtonCampaignDetails()
                    .setEnabled(true);
                while (i < size && !executed) {
                    Campaign pCampaign = (Campaign) campaignList.get(i);
                    if (pCampaign.containsExecutionResultInModel()) {
                        executed = true;
                    }
                    i++;
                }
            } else {
                SalomeTMFPanels.getManualTestButtonCampaignDetails()
                    .setEnabled(false);
            }
            // SalomeTMFPanels.getManualTestExecutedLabel().setText(Language.getInstance().getText("Execute")
            // +" : " + executed);
            SalomeTMFPanels.setTestPanelTestExecutedInfo(
                                                         DataConstants.MANUAL_TEST, "" + executed);
            DataModel.initActionTable((ManualTest) test);

        } else if (test != null && test instanceof AutomaticTest) {
            /*
             * SalomeTMFPanels.getAutomaticTestDateLabel().setText(Language.getInstance
             * ().getText("Date_de_creation__") +
             * test.getCreationDateFromModel().toString());
             * SalomeTMFPanels.getAutomaticTestConceptorLabel
             * ().setText(Language.getInstance().getText("Concepteur__") +
             * test.getConceptorFromModel());
             * SalomeTMFPanels.getAutomaticTestNameLabel
             * ().setText(Language.getInstance().getText("Nom_du_test__") +
             * test.getNameFromModel());
             */
            SalomeTMFPanels.setTestPanelTestInfo(DataConstants.AUTOMATIC_TEST,
                                                 test.getNameFromModel(), test.getConceptorFromModel(), test
                                                 .getCreationDateFromModel().toString());
            SalomeTMFPanels.setTestPanelDescription(
                                                    DataConstants.AUTOMATIC_TEST, test
                                                    .getDescriptionFromModel());
            boolean executed = false;
            ArrayList campaignList = currentProject.getCampaignOfTest(test);
            if (campaignList != null && campaignList.size() > 0) {
                int i = 0;
                int size = campaignList.size();
                SalomeTMFPanels.getAutomaticButtonCampaignDetails().setEnabled(
                                                                               true);
                while (i < size && !executed) {
                    Campaign pCampaign = (Campaign) campaignList.get(i);
                    if (pCampaign.containsExecutionResultInModel()) {
                        executed = true;
                    }
                    i++;
                }
            } else {
                SalomeTMFPanels.getAutomaticButtonCampaignDetails().setEnabled(
                                                                               false);
            }
            // SalomeTMFPanels.getAutomaticTestExecutedLabel().setText(Language.getInstance().getText("Execute")
            // +" : " + executed);
            SalomeTMFPanels.setTestPanelTestExecutedInfo(
                                                         DataConstants.AUTOMATIC_TEST, "" + executed);
            DataModel.initTestScript((AutomaticTest) test);
        }
        testParameterTableModel.clearTable();
        for (int i = 0; i < test.getParameterListFromModel().size(); i++) {
            testParameterTableModel.addValueAt(((Parameter) test
                                                .getParameterListFromModel().get(i)).getNameFromModel(), i,
                                               0);
            testParameterTableModel.addValueAt(((Parameter) test
                                                .getParameterListFromModel().get(i))
                                               .getDescriptionFromModel(), i, 1);
        }
        initAttachmentTable(test.getAttachmentMapFromModel().values());
    } // Fin de la methode initDetails/1

    /**
     * M?thode qui initialise les d?tails de la campagne pass?e en param?tre
     * ainsi que ses ex?cutions
     *
     * @param campagne
     *            une campagne
     */
    public static void initCampaign(Campaign campaign) {
        /*
         * SalomeTMFPanels.getCampaignConceptorLabel().setText(Language.getInstance
         * ().getText("Concepteur__") + campaign.getConceptorFroModel());
         * SalomeTMFPanels
         * .getCampaignDateLabel().setText(Language.getInstance().
         * getText("Date_de_creation__") +
         * campaign.getDateFromModel().toString());
         * SalomeTMFPanels.getCampaignNameLabel
         * ().setText(Language.getInstance().getText("Nom_de_la_campagne__") +
         * campaign.getNameFromModel());
         */
        SalomeTMFPanels
            .setCampPanelInfo(campaign.getNameFromModel(), campaign
                              .getConceptorFroModel(), campaign.getDateFromModel()
                              .toString());
        SalomeTMFPanels.setCampPanelDescription(DataConstants.CAMPAIGN,
                                                campaign.getDescriptionFromModel());

        dataSetTableModel.clearTable();
        for (int i = 0; i < campaign.getDataSetListFromModel().size(); i++) {
            dataSetTableModel
                .addValueAt(((DataSet) campaign.getDataSetListFromModel()
                             .get(i)).getNameFromModel(), i, 0);
            dataSetTableModel.addValueAt(((DataSet) campaign
                                          .getDataSetListFromModel().get(i))
                                         .getDescriptionFromModel(), i, 1);
        }
        ArrayList executionList = campaign.getExecutionListFromModel();
        executionTableModel.clearTable();
        if ((executionList != null) && (executionList.size() > 0)) {
            for (int i = 0; i < executionList.size(); i++) {
                try {
                    ArrayList executionParameters = new ArrayList();
                    executionParameters.add(new Boolean(false));
                    executionParameters.add(((Execution) executionList.get(i))
                                            .getNameFromModel());
                    executionParameters.add(((Execution) executionList.get(i))
                                            .getEnvironmentFromModel());
                    executionParameters.add(completeExecutionResult(((Execution) executionList.get(i))));
                    executionParameters.add(((Execution) executionList.get(i))
                                            .getCreationDateFromModel());
                    executionParameters.add(((Execution) executionList.get(i))
                                            .getAttachmentMapFromModel().keySet());
                    if (((Execution) executionList.get(i)).getLastDateFromModel() != null) {
                        executionParameters.add(((Execution) executionList.get(i))
                                                .getLastDateFromModel().toString());
                    } else {
                        executionParameters.add("");
                    }

                    executionTableModel.addRow(executionParameters);
                    ExecutionView.getTable().getColumnModel().getColumn(0)
                        .setMaxWidth(18);
                
                } catch (Exception ex) {
                    // This exception occurs, if a result is missing (e.g. deleted by user).
                    Util.err("Missing Result!");
                }
            }
        }
        executionResultTableModel.clearTable();
        initAttachmentTable(campaign.getAttachmentMapFromModel().values());

    } // Fin de la methode initDetails/1

    /**
     * R?initialise la description des familles en fonction de la famille pass?e
     * en param?tre
     *
     * @param family
     *            une famille
     */
    public static void initFamily(Family family) {
        SalomeTMFPanels.setTestPanelDescription(DataConstants.FAMILY, family
                                                .getDescriptionFromModel());
        initAttachmentTable(family.getAttachmentMapFromModel().values());
    } // Fin de la m?thode initDescription/1

    /**
     * R?initialise la description des suites en fonction de la suite pass?e en
     * param?tre
     *
     * @param list
     *            une suite de tests
     */
    public static void initTestList(TestList list) {
        SalomeTMFPanels.setTestPanelDescription(DataConstants.TESTLIST, list
                                                .getDescriptionFromModel());
        initAttachmentTable(list.getAttachmentMapFromModel().values());
    } // Fin de la m?thode initTestList/1

    public static void initAttachmentTable(Collection col) {
        attachmentTableModel.clearTable();
        int rowIndex = 0;
        for (Iterator iter = col.iterator(); iter.hasNext();) {
            Attachment attach = (Attachment) iter.next();
            attachmentTableModel.addValueAt(attach.getNameFromModel(),
                                            rowIndex, 0);
            if (attach instanceof FileAttachment) {
                attachmentTableModel.addValueAt(((FileAttachment) attach)
                                                .getSize().toString(), rowIndex, 1);
                attachmentTableModel.addValueAt(((FileAttachment) attach)
                                                .getDate().toString(), rowIndex, 2);
            } else {
                attachmentTableModel.addValueAt("", rowIndex, 1);
                attachmentTableModel.addValueAt("", rowIndex, 2);
            }
            rowIndex++;
        }
    }

    // ********************************** Refresh
    // ***********************************************************//
    public static void reloadData() {
        DefaultMutableTreeNode selectedNode = SalomeTMFPanels
            .getTestDynamicTree().getSelectedNode();
        if (selectedNode != null) {
            if (selectedNode.getUserObject() instanceof SimpleData) {
                try {
                    reloadData((SimpleData) selectedNode.getUserObject());
                } catch (Exception e) {
                    // TODO: handle exception
                    Util.err(e);
                }
            }
        }
    }

    public static void reloadData(SimpleData data) throws Exception {
        int transNumber = -1;
        try {
            // transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            currentProject.setLoad(true);

            if (data instanceof Family) {
                reloadParameters();
                reloadFamily((Family) data, true);
            } else if (data instanceof TestList) {
                reloadParameters();
                reloadSuite((TestList) data, true);
            } else if (data instanceof Test) {
                reloadParameters();
                reloadTest((Test) data, true);
            } else if (data instanceof Campaign) {
                reloadCampaign((Campaign) data);
            }

            currentProject.setLoad(false);
            // Api.commitTrans(transNumber);

        } catch (Exception e) {
            currentProject.setLoad(false);
            Util.err(e);
            // Api.forceRollBackTrans(transNumber);
        }
    }

    public static void reloadDataPlan() throws Exception {
        int transNumber = -1;
        try {
            // transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            currentProject.setLoad(true);

            reloadParameters();
            reloadEnvironnements();
            reloadParameters();
            reloadEnvironnements();

            currentProject.setLoad(false);
            // Api.commitTrans(transNumber);

        } catch (Exception e) {
            currentProject.setLoad(false);
            Util.err(e);
            // Api.forceRollBackTrans(transNumber);
            throw e;
        }
    }

    public static void reloadTestPlan() throws Exception {
        int transNumber = -1;
        try {
            // transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            currentProject.setLoad(true);
            reloadParameters();

            ArrayList familleInModel = currentProject.getFamilyListFromModel();
            Vector familleInDB = currentProject
                .getProjectFamiliesWrapperFromDB();
            int sizeModel = familleInModel.size();
            int sizeDB = familleInDB.size();
            Vector famille2Delete = new Vector();
            Vector famille2Add = new Vector();

            for (int i = 0; i < sizeModel; i++) {
                Family pFamily = (Family) familleInModel.get(i);
                boolean trouve = false;
                int j = 0;
                while (j < sizeDB && !trouve) {
                    FamilyWrapper familleDB = (FamilyWrapper) familleInDB
                        .get(j);
                    if (pFamily.getIdBdd() == familleDB.getIdBDD()) {
                        trouve = true;
                    }
                    j++;
                }
                if (!trouve) {
                    famille2Delete.add(pFamily);
                } else {
                    reloadFamily(pFamily, false);
                }
            }

            /* Ajout des nouveaux */
            for (int i = 0; i < sizeDB; i++) {
                FamilyWrapper familyDB = (FamilyWrapper) familleInDB.get(i);
                Family pFamilyDB = new Family(familyDB);
                boolean trouve = false;
                int j = 0;
                while (j < sizeModel && !trouve) {
                    Family pFamily = (Family) familleInModel.get(j);
                    if (familyDB.getIdBDD() == pFamily.getIdBdd()) {
                        trouve = true;
                    }
                    j++;
                }
                if (!trouve) {
                    famille2Add.add(pFamilyDB);
                }
            }

            // DynamicTree testTree = SalomeTMFPanels.getTestDynamicTree();
            DefaultMutableTreeNode pParentNode = SalomeTMFPanels
                .getTestDynamicTree().getRoot();

            boolean refresh = false;
            int size2 = famille2Delete.size();
            for (int i = 0; i < size2; i++) {
                Family pFamily = (Family) famille2Delete.get(i);
                ArrayList campaignList = currentProject
                    .getCampaignOfFamily(pFamily);
                if (campaignList.size() > 0) {
                    refresh = true;
                }
                currentProject.deleteFamilyInModel(pFamily);
                // IHM
                // DefaultMutableTreeNode pSuiteNode =
                // SalomeTMFPanels.getTestDynamicTree().findRemoveTestListNode(pSuite.getNameFromModel(),pFamily.getNameFromModel(),
                // false);
                // SalomeTMFPanels.getTestDynamicTree().removeNode(pSuiteNode);

            }

            size2 = famille2Add.size();
            for (int i = 0; i < size2; i++) {
                Family pFamily = (Family) famille2Add.get(i);
                currentProject.addFamilyInModel(pFamily);
                SalomeTMFPanels.getTestDynamicTree().addObject(pParentNode,
                                                               pFamily, true);
                reloadFamily(pFamily, false);
            }

            currentProject.triFamilleInModel();
            sizeModel = familleInModel.size();
            Vector listeNode = new Vector();
            for (int i = 0; i < sizeModel; i++) {
                Family pFamily = (Family) familleInModel.get(i);
                DefaultMutableTreeNode pFamilyNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);
                // DefaultMutableTreeNode pSuiteNode = reloadSuite(pSuite,
                // false);
                listeNode.add(pFamilyNode);
            }
            // IHM
            SalomeTMFPanels.getTestDynamicTree().removeChildFromNode(
                                                                     pParentNode);
            sizeModel = listeNode.size();
            for (int i = 0; i < sizeModel; i++) {
                DefaultMutableTreeNode pFamilyNode = (DefaultMutableTreeNode) listeNode
                    .get(i);
                SalomeTMFPanels.getTestDynamicTree().addNode(pParentNode,
                                                             pFamilyNode, true);
            }

            if (refresh) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
            }

            if (pParentNode != null) {
                SalomeTMFPanels.getTestDynamicTree().scrollPathToVisible(
                                                                         pParentNode);
            }
            SalomeTMFPanels.getTestDynamicTree().repaint();
            SalomeTMFPanels.setTestPanelWorkSpace(-1);
            SalomeTMFPanels.reValidateTestPanel();
            currentProject.setLoad(false);
            // Api.commitTrans(transNumber);

        } catch (Exception e) {
            currentProject.setLoad(false);
            Util.err(e);
            // Api.forceRollBackTrans(transNumber);
            throw e;
        }

    }

    public static void reloadCampainPlan() throws Exception {
        int transNumber = -1;
        try {
            // transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            currentProject.setLoad(true);
            reloadParameters();
            reloadEnvironnements();

            ArrayList campagneInModel = currentProject
                .getCampaignListFromModel();
            Vector campagneInDB = currentProject
                .getCampaignsWrapperListFromDB();
            int sizeModel = campagneInModel.size();
            int sizeDB = campagneInDB.size();
            Vector campagne2Delete = new Vector();
            Vector campagne2Add = new Vector();

            for (int i = 0; i < sizeModel; i++) {
                Campaign pCamp = (Campaign) campagneInModel.get(i);
                boolean trouve = false;
                int j = 0;
                while (j < sizeDB && !trouve) {
                    CampaignWrapper campagneDB = (CampaignWrapper) campagneInDB
                        .get(j);
                    if (pCamp.getIdBdd() == campagneDB.getIdBDD()) {
                        trouve = true;
                    }
                    j++;
                }
                if (!trouve) {
                    campagne2Delete.add(pCamp);
                } else {
                    reloadCamp(pCamp, false);
                }
            }

            /* Ajout des nouveaux */
            for (int i = 0; i < sizeDB; i++) {
                CampaignWrapper campagneDB = (CampaignWrapper) campagneInDB
                    .get(i);
                Campaign pCampDB = new Campaign(campagneDB);
                boolean trouve = false;
                int j = 0;
                while (j < sizeModel && !trouve) {
                    Campaign pCamp = (Campaign) campagneInModel.get(j);
                    if (campagneDB.getIdBDD() == pCamp.getIdBdd()) {
                        trouve = true;
                    }
                    j++;
                }
                if (!trouve) {
                    campagne2Add.add(pCampDB);
                }
            }

            // DynamicTree testTree = SalomeTMFPanels.getTestDynamicTree();
            DefaultMutableTreeNode pParentNode = SalomeTMFPanels
                .getCampaignDynamicTree().getRoot();

            boolean refresh = false;
            int size2 = campagne2Delete.size();
            for (int i = 0; i < size2; i++) {
                Campaign pCamp = (Campaign) campagne2Delete.get(i);
                currentProject.deleteCampaignInModel(pCamp);
            }

            size2 = campagne2Add.size();
            for (int i = 0; i < size2; i++) {
                Campaign pCamp = (Campaign) campagne2Add.get(i);
                currentProject.addCampaignInModel(pCamp);
                SalomeTMFPanels.getCampaignDynamicTree().addObject(pParentNode,
                                                                   pCamp, true);
                reloadCamp(pCamp, false);
            }

            currentProject.triCampagneInModel();

            sizeModel = campagneInModel.size();
            Vector listeNode = new Vector();
            for (int i = 0; i < sizeModel; i++) {
                Campaign pCamp = (Campaign) campagneInModel.get(i);
                DefaultMutableTreeNode pCampNode = SalomeTMFPanels
                    .getCampaignDynamicTree().findRemoveCampaignNode(
                                                                     pCamp.getNameFromModel(), false);
                // DefaultMutableTreeNode pSuiteNode = reloadSuite(pSuite,
                // false);
                listeNode.add(pCampNode);
            }
            // IHM
            SalomeTMFPanels.getCampaignDynamicTree().removeChildFromNode(
                                                                         pParentNode);
            sizeModel = listeNode.size();
            for (int i = 0; i < sizeModel; i++) {
                DefaultMutableTreeNode pCampNode = (DefaultMutableTreeNode) listeNode
                    .get(i);
                SalomeTMFPanels.getCampaignDynamicTree().addNode(pParentNode,
                                                                 pCampNode, true);
            }

            if (refresh) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
            }

            if (pParentNode != null) {
                SalomeTMFPanels.getCampaignDynamicTree().scrollPathToVisible(
                                                                             pParentNode);
            }
            SalomeTMFPanels.getCampaignDynamicTree().repaint();
            SalomeTMFPanels.setCampPanelWorkSpace(-1);
            SalomeTMFPanels.reValidateCampainPanel();
            currentProject.setLoad(false);
            // Api.commitTrans(transNumber);

        } catch (Exception e) {
            // currentProject.setLoad(false);
            Util.err(e);
            Api.forceRollBackTrans(transNumber);
            throw e;
        }

    }

    static DefaultMutableTreeNode reloadFamily(Family pFamily, boolean ihm)
        throws Exception {
        ArrayList suiteInModel = pFamily.getSuiteListFromModel();
        Vector suiteInDB = pFamily.getSuitesWrapperFromDB();
        int sizeModel = suiteInModel.size();
        int sizeDB = suiteInDB.size();
        Vector suite2Delete = new Vector();
        Vector suite2Add = new Vector();

        pFamily.reloadBaseFromDB();
        pFamily.reloadAttachmentDataFromDB(true);

        for (int i = 0; i < sizeModel; i++) {
            TestList pSuite = (TestList) suiteInModel.get(i);
            boolean trouve = false;
            int j = 0;
            while (j < sizeDB && !trouve) {
                SuiteWrapper suiteDB = (SuiteWrapper) suiteInDB.get(j);
                if (pSuite.getIdBdd() == suiteDB.getIdBDD()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                suite2Delete.add(pSuite);
            } else {
                reloadSuite(pSuite, false);
            }
        }

        /* Ajout des nouveaux */
        for (int i = 0; i < sizeDB; i++) {
            SuiteWrapper suiteDB = (SuiteWrapper) suiteInDB.get(i);
            TestList pSuiteDB = new TestList(suiteDB);
            boolean trouve = false;
            int j = 0;
            while (j < sizeModel && !trouve) {
                TestList pSuite = (TestList) suiteInModel.get(j);
                if (suiteDB.getIdBDD() == pSuite.getIdBdd()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                suite2Add.add(pSuiteDB);
            }
        }

        // DynamicTree testTree = SalomeTMFPanels.getTestDynamicTree();
        DefaultMutableTreeNode pFamilyNode = SalomeTMFPanels
            .getTestDynamicTree().findRemoveFamilyNode(
                                                       pFamily.getNameFromModel(), false);

        boolean refresh = false;
        int size2 = suite2Delete.size();
        for (int i = 0; i < size2; i++) {
            TestList pSuite = (TestList) suite2Delete.get(i);
            ArrayList campaignList = currentProject
                .getCampaignOfTestList(pSuite);
            if (campaignList.size() > 0) {
                refresh = true;
            }
            currentProject.deleteTestListInModel(pSuite);
            // IHM
            // DefaultMutableTreeNode pSuiteNode =
            // SalomeTMFPanels.getTestDynamicTree().findRemoveTestListNode(pSuite.getNameFromModel(),pFamily.getNameFromModel(),
            // false);
            // SalomeTMFPanels.getTestDynamicTree().removeNode(pSuiteNode);

        }

        size2 = suite2Add.size();
        for (int i = 0; i < size2; i++) {
            TestList pSuite = (TestList) suite2Add.get(i);
            currentProject.addTestListInFamilyInModel(pSuite, pFamily);

            SalomeTMFPanels.getTestDynamicTree().addObject(pFamilyNode, pSuite,
                                                           true);
            reloadSuite(pSuite, false);
        }

        pFamily.triTestListInModel();
        sizeModel = suiteInModel.size();
        Vector listeNode = new Vector();
        for (int i = 0; i < sizeModel; i++) {
            TestList pSuite = (TestList) suiteInModel.get(i);
            DefaultMutableTreeNode pSuiteNode = SalomeTMFPanels
                .getTestDynamicTree().findRemoveTestListNode(
                                                             pSuite.getNameFromModel(),
                                                             pFamily.getNameFromModel(), false);
            // DefaultMutableTreeNode pSuiteNode = reloadSuite(pSuite, false);
            listeNode.add(pSuiteNode);
        }
        // IHM
        SalomeTMFPanels.getTestDynamicTree().removeChildFromNode(pFamilyNode);
        sizeModel = listeNode.size();
        for (int i = 0; i < sizeModel; i++) {
            DefaultMutableTreeNode pSuiteNode = (DefaultMutableTreeNode) listeNode
                .get(i);
            SalomeTMFPanels.getTestDynamicTree().addNode(pFamilyNode,
                                                         pSuiteNode, true);
            // SalomeTMFPanels.getTestDynamicTree().addObject(pFamilyNode,
            // pSuite, true);
        }

        if (refresh && ihm) {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }

        if (ihm) {
            SalomeTMFPanels.getTestDynamicTree().repaint();
            DataModel.setCurrentFamily(pFamily);
            DataModel.initFamily(pFamily);
            SalomeTMFPanels.setTestPanelWorkSpace(DataConstants.FAMILY);
            SalomeTMFPanels.reValidateTestPanel();
        }
        return pFamilyNode;

    }

    static DefaultMutableTreeNode reloadSuite(TestList pSuite, boolean message)
        throws Exception {
        ArrayList testInModel = pSuite.getTestListFromModel();
        Vector testInDB = pSuite.getTestsWrapperFromDB();
        int sizeModel = testInModel.size();
        int sizeDB = testInDB.size();
        Vector test2Delete = new Vector();
        Vector test2Add = new Vector();

        pSuite.reloadBaseFromDB();
        pSuite.reloadAttachmentDataFromDB(true);

        for (int i = 0; i < sizeModel; i++) {
            Test pTest = (Test) testInModel.get(i);
            boolean trouve = false;
            int j = 0;
            while (j < sizeDB && !trouve) {
                TestWrapper testDB = (TestWrapper) testInDB.get(j);
                if (pTest.getIdBdd() == testDB.getIdBDD()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                test2Delete.add(pTest);
            } else {
                pTest.reloadFromDB(true, currentProject
                                   .getParameterSetFromModel(), false);
                if (pTest instanceof ManualTest) {
                    ((ManualTest) pTest).reloadActionFromDB(currentProject
                                                            .getParameterSetFromModel(), true);
                }
            }
        }
        /* Ajout des nouveaux */
        for (int i = 0; i < sizeDB; i++) {
            TestWrapper testDB = (TestWrapper) testInDB.get(i);
            Test test;
            if (testDB.getType().equals(ApiConstants.MANUAL)) {
                test = new ManualTest(new ManualTestWrapper(testDB));
            } else {
                test = new AutomaticTest(new AutomaticTestWrapper(testDB));
            }

            boolean trouve = false;
            int j = 0;
            while (j < sizeModel && !trouve) {
                Test pTest = (Test) testInModel.get(j);
                if (testDB.getIdBDD() == pTest.getIdBdd()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                test.reloadFromDB(false, currentProject
                                  .getParameterSetFromModel(), false);
                test2Add.add(test);
            }
        }

        DefaultMutableTreeNode pSuiteNode = SalomeTMFPanels
            .getTestDynamicTree().findRemoveTestListNode(
                                                         pSuite.getNameFromModel(),
                                                         pSuite.getFamilyFromModel().getNameFromModel(), false);

        int size2 = test2Add.size();
        for (int i = 0; i < size2; i++) {
            Test pTest = (Test) test2Add.get(i);
            currentProject.addTestInListInModel(pTest, pSuite);
            // IHM
            // SalomeTMFPanels.getTestDynamicTree().addObject(pSuiteNode,
            // pTest,true);
        }

        boolean refresh = false;
        size2 = test2Delete.size();
        for (int i = 0; i < size2; i++) {
            Test pTest = (Test) test2Delete.get(i);
            ArrayList campaignList = currentProject.getCampaignOfTest(pTest);
            if (campaignList.size() > 0) {
                refresh = true;
            }
            currentProject.deleteTestInModel(pTest);
            // IHM
            // DefaultMutableTreeNode pTestNode =
            // SalomeTMFPanels.getTestDynamicTree().findRemoveTestNode(pTest.getNameFromModel(),pSuite.getNameFromModel(),pSuite.getFamilyFromModel().getNameFromModel(),
            // false);
            // SalomeTMFPanels.getTestDynamicTree().removeNode(pTestNode);
        }

        // IHM
        // pSuiteNode.removeAllChildren();
        if (pSuiteNode != null) {
            SalomeTMFPanels.getTestDynamicTree()
                .removeChildFromNode(pSuiteNode);

            pSuite.triTestInModel();
            sizeModel = testInModel.size();
            for (int i = 0; i < sizeModel; i++) {
                Test pTest = (Test) testInModel.get(i);
                SalomeTMFPanels.getTestDynamicTree().addObject(pSuiteNode,
                                                               pTest, true);
            }
        }
        /*
         * if (pSuiteNode.getChildCount() == 0){ pSuiteNode.removeFromParent();
         * }
         */

        if (refresh && message) {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }

        // SalomeTMFPanels.setTestPanelWorkSpace(-1); //remove all
        if (message) {
            // SalomeTMFPanels.setTestPanelWorkSpace(-1); //remove all
            SalomeTMFPanels.getTestDynamicTree().repaint();

            if (pSuiteNode != null) {
                SalomeTMFPanels.getTestDynamicTree().scrollPathToVisible(
                                                                         pSuiteNode);
            }
            DataModel.setCurrentTestList(pSuite);
            DataModel.initTestList(pSuite);
            SalomeTMFPanels.setTestPanelWorkSpace(DataConstants.TESTLIST);
            SalomeTMFPanels.reValidateTestPanel();
        }
        return pSuiteNode;
    }

    static void reloadTest(Test pTest, boolean ihm) throws Exception {
        // int transNumber = -1;
        try {
            // transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            // currentProject.setLoad(true);

            pTest.reloadFromDB(true, currentProject.getParameterSetFromModel(),
                               false);
            if (pTest instanceof ManualTest) {
                ((ManualTest) pTest).reloadActionFromDB(currentProject
                                                        .getParameterSetFromModel(), true);
            }
            if (ihm) {
                DefaultMutableTreeNode pTestNode = SalomeTMFPanels
                    .getTestDynamicTree()
                    .findRemoveTestNode(
                                        pTest.getNameFromModel(),
                                        pTest.getTestListFromModel().getNameFromModel(),
                                        pTest.getTestListFromModel()
                                        .getFamilyFromModel()
                                        .getNameFromModel(), false);
                // SalomeTMFPanels.setTestPanelWorkSpace(-1); //remove all
                if (pTestNode != null) {
                    SalomeTMFPanels.getTestDynamicTree().scrollPathToVisible(
                                                                             pTestNode);
                }
                DataModel.setCurrentTest(pTest);
                DataModel.initTest(pTest);
                if (pTest instanceof ManualTest) {
                    SalomeTMFPanels
                        .setTestPanelWorkSpace(DataConstants.MANUAL_TEST);
                } else {
                    SalomeTMFPanels
                        .setTestPanelWorkSpace(DataConstants.AUTOMATIC_TEST);
                }
                SalomeTMFPanels.reValidateTestPanel();
            }
            // currentProject.setLoad(false);

            // Api.commitTrans(transNumber);
        } catch (Exception e) {
            // Api.forceRollBackTrans(transNumber);
            // currentProject.setLoad(false);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void reloadEnvironnements() throws Exception {
        ArrayList envInModel = currentProject.getEnvironmentListFromModel();
        Vector environmentOfProject = currentProject
            .getEnvironmentWrapperFromDB();
        int sizeModel = envInModel.size();
        int sizeDB = environmentOfProject.size();
        Vector env2Delete = new Vector();
        Vector env2Add = new Vector();

        for (int i = 0; i < sizeModel; i++) {
            Environment pEnv = (Environment) envInModel.get(i);
            boolean trouve = false;
            int j = 0;
            while (j < sizeDB && !trouve) {
                EnvironmentWrapper envDB = (EnvironmentWrapper) environmentOfProject
                    .get(j);
                if (pEnv.getIdBdd() == envDB.getIdBDD()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                env2Delete.add(pEnv);
            } else {
                pEnv.reloadFromDB(true, currentProject
                                  .getParameterSetFromModel());
            }
        }

        // Ajout des nouveaux
        for (int i = 0; i < sizeDB; i++) {
            EnvironmentWrapper envDB = (EnvironmentWrapper) environmentOfProject
                .get(i);
            Environment env = new Environment(envDB);
            boolean trouve = false;
            int j = 0;
            while (j < sizeModel && !trouve) {
                Environment pEnv = (Environment) envInModel.get(j);
                if (envDB.getIdBDD() == pEnv.getIdBdd()) {
                    trouve = true;
                }
                j++;
            }
            if (!trouve) {
                env.reloadFromDB(false, currentProject
                                 .getParameterSetFromModel());
                env2Add.add(env);
            }
        }

        int size2 = env2Add.size();
        for (int i = 0; i < size2; i++) {
            Environment pEnv = (Environment) env2Add.get(i);
            currentProject.addEnvironmentInModel(pEnv);
        }

        boolean refresh = false;
        size2 = env2Delete.size();
        for (int i = 0; i < size2; i++) {
            Environment pEnv = (Environment) env2Delete.get(i);
            ArrayList concernedExecutions = currentProject
                .getExecutionOfEnvironmentInModel(pEnv.getNameFromModel());
            if (concernedExecutions.size() > 0) {
                refresh = true;
            }
            currentProject.deleteEnvironmentInModel(pEnv);
        }
        if (refresh) {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }

        // IHM
        envInModel = currentProject.getEnvironmentListFromModel();
        sizeModel = envInModel.size();
        environmentTableModel.clearTable();
        for (int i = 0; i < sizeModel; i++) {
            ArrayList data = new ArrayList();
            Environment pEnv = (Environment) envInModel.get(i);
            data.add(pEnv.getNameFromModel());
            String initScriptName = "";
            if (pEnv.getInitScriptFromModel() != null) {
                initScriptName = pEnv.getInitScriptFromModel()
                    .getNameFromModel();
            }
            data.add(initScriptName);
            data.add(pEnv.getParametersHashTableFromModel());
            data.add(pEnv.getDescriptionFromModel());
            environmentTableModel.addRow(data);
        }

    }

    public static void reloadProjectAttachement() throws Exception {
        currentProject.reloadAttachmentDataFromDB(true);
    }

    public static void reloadParameters() throws Exception {
        Hashtable projectParamModel = currentProject.getParameterSetFromModel();
        Vector projectParamDB = currentProject.getParametersWrapperFromDB();
        Vector param2Delete = new Vector();
        Vector param2Add = new Vector();
        int size = projectParamDB.size();

        Enumeration enumParam = projectParamModel.elements();
        while (enumParam.hasMoreElements()) {
            Parameter pParam = (Parameter) enumParam.nextElement();
            boolean trouve = false;
            int i = 0;
            while (i < size && !trouve) {
                ParameterWrapper paramBdd = (ParameterWrapper) projectParamDB
                    .get(i);
                if (paramBdd.getIdBDD() == pParam.getIdBdd()) {
                    trouve = true;
                }
                i++;
            }
            if (!trouve) {
                param2Delete.add(pParam);
            }
        }

        // Ajout des nouveaux
        for (int i = 0; i < size; i++) {
            ParameterWrapper paramBdd = (ParameterWrapper) projectParamDB
                .get(i);
            Parameter param = new Parameter(paramBdd);
            enumParam = projectParamModel.elements();
            boolean trouve = false;
            while (enumParam.hasMoreElements() && !trouve) {
                Parameter pParam = (Parameter) enumParam.nextElement();
                if (paramBdd.getIdBDD() == pParam.getIdBdd()) {
                    trouve = true;
                }
            }
            if (!trouve) {
                param2Add.add(param);
            }
        }
        int size2 = param2Add.size();
        for (int i = 0; i < size2; i++) {
            Parameter pParam = (Parameter) param2Add.get(i);
            currentProject.addParameterToModel(pParam);
        }

        // Suppression des parametres
        size2 = param2Delete.size();
        boolean refresh = false;
        for (int i = 0; i < size2; i++) {
            Parameter pParam = (Parameter) param2Delete.get(i);
            // Si il sont utilise --> forcer le refresh
            ArrayList testsOfParam = DataModel.getCurrentProject()
                .getTestOfParameterFromModel(pParam.getNameFromModel());
            ArrayList envOfParam = DataModel.getCurrentProject()
                .getEnvironmentOfParameterFromModel(
                                                    pParam.getNameFromModel());
            if (testsOfParam.size() > 0 || envOfParam.size() > 0) {
                refresh = true;
            } else {
                currentProject.delParamInModel(pParam);
            }
        }
        if (refresh) {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }

        // IHM
        getParameterTableModel().clearTable();
        projectParamModel = currentProject.getParameterSetFromModel();
        enumParam = projectParamModel.elements();
        while (enumParam.hasMoreElements()) {
            Parameter pParam = (Parameter) enumParam.nextElement();
            ArrayList dataList = new ArrayList();
            dataList.add(pParam.getNameFromModel());
            dataList.add(pParam.getDescriptionFromModel());
            DataModel.getParameterTableModel().addRow(dataList);
        }
    }

    public static void reloadCampaign(Campaign pCamp) {
        Campaign pCampSav = currentCampaign;
        currentCampaign = pCamp;
        reloadCampaign(false);
        currentCampaign = pCampSav;
        if (pIAssignedCampAction != null) {
            pIAssignedCampAction.updateData(pCamp);
        }

    }

    public static void synchronizeCampaign(Campaign pCamp) {
        if (pCamp == null) {
            return;
        }

        if (currentCampaign.containsExecutionResultInModel()) {
            if (!Api.isLockExecutedTest()) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext
                    .getInstance()
                    .askQuestion(
                                 Language
                                 .getInstance()
                                 .getText(
                                          "Cette_campagne_contient_des_resultat_dexecution_plus_possible_de_les_modifier")
                                 + "\n"
                                 + Language.getInstance().getText(
                                                                  "continuer"),
                                 Language.getInstance().getText("Attention_"),
                                 JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
            } else {
                JOptionPane
                    .showMessageDialog(
                                       SalomeTMFContext.getInstance().getSalomeFrame(),
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Cette_campagne_contient_deja_des_resultat_d_execution_Il_n_est_plus_possible_de_la_modifier"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        try {
            DefaultMutableTreeNode pNode = SalomeTMFPanels
                .getCampaignDynamicTree().getSelectedNode();
            SimpleData pData = null;
            if (pNode == null) {
                return;
            } else {
                if (pNode.getParent() == null) {
                    /*
                     * DefaultMutableTreeNode pRoot =
                     * SalomeTMFPanels.getCampaignDynamicTree().getRoot(); int
                     * nbChild = pRoot.getChildCount(); for (int i = 0; i <
                     * nbChild ; i++ ){ pData = ((SimpleData)
                     * ((DefaultMutableTreeNode
                     * )pRoot.getChildAt(i)).getUserObject());
                     * synchronizeCampaign((Campaign)pData); }
                     */
                    return;
                } else {
                    pData = (SimpleData) pNode.getUserObject();
                }

            }

            boolean refresh = pCamp.synchronizeWithTestPlanFromModel(pData,
                                                                     currentUser.getIdBdd());
            if (refresh) {
                reloadCamp(pCamp, true);
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }

    }

    /**
     * @param all
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    public static void reloadCampaign(boolean all) {
        if (all && currentCampaign == null) {
            return;
        }
        try {
            reloadParameters();
            reloadEnvironnements();
        } catch (Exception e) {
            Util.err(e);
        }
        ArrayList testInModel = currentProject.getAlltestFromModel();
        ArrayList envInModel = currentProject.getEnvironmentListFromModel();
        Hashtable paramInModel = currentProject.getParameterSetFromModel();
        DynamicTree campaignDynamicTree = SalomeTMFPanels
            .getCampaignDynamicTree();
        ArrayList campList = null;
        if (all) {
            campList = currentProject.getCampaignListFromModel();
        } else {
            campList = new ArrayList();
            campList.add(currentCampaign);
        }
        DefaultMutableTreeNode pCampNode = null;
        int transNumber = -1;
        try {
            transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            currentProject.setLoad(true);

            int size = campList.size();
            for (int i = 0; i < size; i++) {
                Campaign pCamp = (Campaign) campList.get(i);

                // BDD
                boolean reload = pCamp.reloadCampain(testInModel, envInModel,
                                                     paramInModel);
                pCamp.reloadAttachmentDataFromDB(true);
                if (reload) {
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("Update_data"),
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);
                    i = size;
                }

                // IHM
                pCampNode = campaignDynamicTree.findRemoveCampaignNode(pCamp
                                                                       .getNameFromModel(), false);

                campaignDynamicTree.removeChildFromNode(pCampNode);
                ArrayList newTestList = pCamp.getTestListFromModel();
                Family pCurrentFamily = null;
                TestList pCurrentTestList = null;
                DefaultMutableTreeNode pFamilyNode = null;
                DefaultMutableTreeNode pSuiteNode = null;
                // DefaultMutableTreeNode pTestNode = null;
                int size2 = newTestList.size();
                for (int j = 0; j < size2; j++) {
                    Test pTest = (Test) newTestList.get(j);
                    TestList pTestList = pTest.getTestListFromModel();
                    Family pFamily = pTestList.getFamilyFromModel();
                    // 20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                    if (!pFamily.equals(pCurrentFamily)) {
                        // pFamilyNode =
                        // campaignDynamicTree.findRemoveFamilyNodeInCampagneTree(pFamily.getNameFromModel(),
                        // pCamp.getNameFromModel(), false);
                        // if (pFamilyNode == null){
                        pFamilyNode = campaignDynamicTree.addObject(pCampNode,
                                                                    pFamily, true);
                        // }
                        pCurrentFamily = pFamily;
                    }
                    if (!pTestList.equals(pCurrentTestList)) {
                        // pSuiteNode =
                        // campaignDynamicTree.findRemoveTestListNodeInCampagneTree(pTestList.getNameFromModel(),
                        // pFamily.getNameFromModel(), pCamp.getNameFromModel(),
                        // false);
                        // if (pSuiteNode == null){
                        pSuiteNode = campaignDynamicTree.addObject(pFamilyNode,
                                                                   pTestList, true);
                        // }
                        pCurrentTestList = pTestList;
                    }
                    // 20100108 - Fin modification Forge ORTF V1.0.0
                    SalomeTMFPanels.getCampaignDynamicTree().addObject(
                                                                       pSuiteNode, pTest, true);

                }

            }

            currentProject.setLoad(false);

            Api.commitTrans(transNumber);
        } catch (Exception exception) {
            Api.forceRollBackTrans(transNumber);
            currentProject.setLoad(false);
            Util.err(exception);
            Tools.ihmExceptionView(exception);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (pCampNode != null) {
            campaignDynamicTree.scrollPathToVisible(pCampNode);
            DataModel.setCurrentCampaign((Campaign) pCampNode.getUserObject());
            DataModel.initCampaign((Campaign) pCampNode.getUserObject());
            SalomeTMFPanels.setCampPanelWorkSpace(DataConstants.CAMPAIGN);
            SalomeTMFPanels.reValidateCampainPanel();
        }
        campaignDynamicTree.repaint();
    }

    /**
     * @param pCamp
     * @param ihm
     * @return
     * @throws Exception
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    static DefaultMutableTreeNode reloadCamp(Campaign pCamp, boolean ihm)
        throws Exception {
        DefaultMutableTreeNode pCampNode = null;
        ArrayList testInModel = currentProject.getAlltestFromModel();
        ArrayList envInModel = currentProject.getEnvironmentListFromModel();
        Hashtable paramInModel = currentProject.getParameterSetFromModel();
        DynamicTree campaignDynamicTree = SalomeTMFPanels
            .getCampaignDynamicTree();
        // BDD
        boolean reload = pCamp.reloadCampain(testInModel, envInModel,
                                             paramInModel);
        pCamp.reloadAttachmentDataFromDB(true);
        if (reload && ihm) {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        }

        // IHM
        pCampNode = campaignDynamicTree.findRemoveCampaignNode(pCamp
                                                               .getNameFromModel(), false);

        campaignDynamicTree.removeChildFromNode(pCampNode);
        ArrayList newTestList = pCamp.getTestListFromModel();
        Family pCurrentFamily = null;
        TestList pCurrentTestList = null;
        DefaultMutableTreeNode pFamilyNode = null;
        DefaultMutableTreeNode pSuiteNode = null;
        // DefaultMutableTreeNode pTestNode = null;
        int size2 = newTestList.size();
        for (int j = 0; j < size2; j++) {
            Test pTest = (Test) newTestList.get(j);
            TestList pTestList = pTest.getTestListFromModel();
            Family pFamily = pTestList.getFamilyFromModel();
            // 20100108 - D\ufffdbut modification Forge ORTF V1.0.0
            if (!pFamily.equals(pCurrentFamily)) {
                // pFamilyNode =
                // campaignDynamicTree.findRemoveFamilyNodeInCampagneTree(pFamily.getNameFromModel(),
                // pCamp.getNameFromModel(), false);
                // if (pFamilyNode == null){
                pFamilyNode = campaignDynamicTree.addObject(pCampNode, pFamily,
                                                            true);
                // }
                pCurrentFamily = pFamily;
            }
            if (!pTestList.equals(pCurrentTestList)) {
                // pSuiteNode =
                // campaignDynamicTree.findRemoveTestListNodeInCampagneTree(pTestList.getNameFromModel(),
                // pFamily.getNameFromModel(), pCamp.getNameFromModel(), false);
                // if (pSuiteNode == null){
                pSuiteNode = campaignDynamicTree.addObject(pFamilyNode,
                                                           pTestList, true);
                // }
                pCurrentTestList = pTestList;
            }
            // 20100108 - Fin modification Forge ORTF V1.0.0
            SalomeTMFPanels.getCampaignDynamicTree().addObject(pSuiteNode,
                                                               pTest, true);
        }
        return pCampNode;
    }

    // ********************************** Delete in tree
    // ****************************************************//

    /**
     * Methode de suppression de tests, suites de tests, familles ou campagnes.
     *
     * @return vrai si la suppression s'est correctement deroulee, faux sinon.
     */
    public static boolean deleteInTestTree() {
        DefaultMutableTreeNode selectedNode = SalomeTMFPanels
            .getTestDynamicTree().getSelectedNode();

        boolean result = true;
        if (selectedNode == null) {
            return result;
        }

        Object[] options = { Language.getInstance().getText("Oui"),
                             Language.getInstance().getText("Non") };
        int choice = -1;
        int actionCase = -1;
        String message = "";
        ArrayList campaignList = new ArrayList();
        Vector reqManagers = SalomeTMFContext.getInstance().getReqManagers();

        if ((selectedNode.getUserObject() instanceof Test)) {
            campaignList = currentProject.getCampaignOfTest((Test) selectedNode
                                                            .getUserObject());
            if (campaignList.size() > 0) {
                message = Language.getInstance().getText("Le_test__")
                    + ((Test) selectedNode.getUserObject())
                    .getNameFromModel()
                    + Language.getInstance().getText(
                                                     "_est_utilise_dans_les_campagnes_");
                for (int i = 0; i < campaignList.size(); i++) {
                    message = message
                        + "* "
                        + ((Campaign) campaignList.get(i))
                        .getNameFromModel() + "\n";
                }
                message = message
                    + Language
                    .getInstance()
                    .getText(
                             "Le_test_sera_purge_des_resultats_d_execution_de_ces_campagnes_");
            }
            choice = SalomeTMFContext
                .getInstance()
                .askQuestion(
                             message
                             + Language
                             .getInstance()
                             .getText(
                                      "Etes_vous_sur_de_vouloir_supprimer_le_test__")
                             + ((Test) selectedNode.getUserObject())
                             .getNameFromModel() + " > ?",
                             Language.getInstance().getText("Attention_"),
                             JOptionPane.WARNING_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(), message +Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_le_test__") +
             * ((Test)selectedNode.getUserObject()).getNameFromModel() + " > ?",
             * Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = TEST;
        } else if (selectedNode.getUserObject() instanceof TestList) {
            campaignList = currentProject
                .getCampaignOfTestList((TestList) selectedNode
                                       .getUserObject());
            if (campaignList.size() > 0) {
                message = Language.getInstance().getText("La_suite_de_tests__")
                    + ((TestList) selectedNode.getUserObject())
                    .getNameFromModel()
                    + Language.getInstance().getText(
                                                     "_est_utilisee_dans_les_campagnes_");
                for (int i = 0; i < campaignList.size(); i++) {
                    message = message
                        + "* "
                        + ((Campaign) campaignList.get(i))
                        .getNameFromModel() + "\n";
                }
                message = message
                    + Language
                    .getInstance()
                    .getText(
                             "Les_tests_de_la_suite_seront_purges_des_resultats_d_execution_de_ces_campagnes_");
            }
            choice = SalomeTMFContext
                .getInstance()
                .askQuestion(
                             message
                             + Language
                             .getInstance()
                             .getText(
                                      "Etes_vous_sur_de_vouloir_supprimer_la_suite__")
                             + ((TestList) selectedNode.getUserObject())
                             .getNameFromModel() + " > ?",
                             Language.getInstance().getText("Attention_"),
                             JOptionPane.WARNING_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(), message +Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_la_suite__") +
             * ((TestList)selectedNode.getUserObject()).getNameFromModel()
             * +" > ?", Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = TESTLIST;
        } else if (selectedNode.getUserObject() instanceof Family) {
            campaignList = currentProject
                .getCampaignOfFamily((Family) selectedNode.getUserObject());
            if (campaignList.size() > 0) {
                message = Language.getInstance().getText("La_famille__")
                    + ((Family) selectedNode.getUserObject())
                    .getNameFromModel()
                    + Language.getInstance().getText(
                                                     "_est_utilisee_dans_les_campagnes_");
                for (int i = 0; i < campaignList.size(); i++) {
                    /*
                     * if
                     * (((Campaign)campaignList.get(i)).getTestListList().size()
                     * == 1) { toBeDelete = "* " +
                     * ((Campaign)campaignList.get(i)).getName() + "\n"; }
                     */
                    message = message
                        + "* "
                        + ((Campaign) campaignList.get(i))
                        .getNameFromModel() + "\n";
                }
                // message = message + "Parmi celles-ci seront supprim?es :\n" +
                // toBeDelete + "car elles ne contiennent que ce test.\n\n";
                message = message
                    + Language
                    .getInstance()
                    .getText(
                             "Les_tests_de_la_famille_seront_purges_des_resultats_d_execution_de_ces_campagnes_");
            }
            choice = SalomeTMFContext
                .getInstance()
                .askQuestion(
                             message
                             + Language
                             .getInstance()
                             .getText(
                                      "Etes_vous_sur_de_vouloir_supprimer_la_famille__")
                             + ((Family) selectedNode.getUserObject())
                             .getNameFromModel() + " > ?",
                             Language.getInstance().getText("Attention_"),
                             JOptionPane.WARNING_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(), message +Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_la_famille__") +
             * ((Family)selectedNode.getUserObject()).getNameFromModel() +
             * " > ?", Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = FAMILY;
        }

        if (choice == JOptionPane.YES_OPTION) {
            // LE CAS DES TESTS
            SalomeTMFPanels.getTestDynamicTree().getTree().setCursor(
                                                                     new Cursor(Cursor.WAIT_CURSOR));

            boolean deleted = false;
            if (actionCase == TEST) {
                try {
                    // Delete req links
                    /*
                     * if (reqManagers!=null && reqManagers.size() != 0) { for
                     * (int i=0; i<reqManagers.size(); i++) { ReqManager req =
                     * (ReqManager)reqManagers.elementAt(i);
                     * req.deleteReqLinkWithTest
                     * (((Test)selectedNode.getUserObject()).getIdBdd()); } }
                     */

                    // BDD & DATA
                    currentProject.deleteTestInDBandModel(((Test) selectedNode
                                                           .getUserObject()));
                    deleted = true;

                    // IHM
                    for (int j = 0; j < campaignList.size(); j++) {
                        // Suppression en cascade dans l'arbre des campagnes
                        DefaultMutableTreeNode testNodeInCampaignTree = SalomeTMFPanels
                            .getCampaignDynamicTree()
                            .findRemoveTestNodeInCampagneTree(
                                                              ((Test) selectedNode.getUserObject())
                                                              .getNameFromModel(),
                                                              ((Test) selectedNode.getUserObject())
                                                              .getTestListFromModel()
                                                              .getNameFromModel(),
                                                              ((Test) selectedNode.getUserObject())
                                                              .getTestListFromModel()
                                                              .getFamilyFromModel()
                                                              .getNameFromModel(),
                                                              ((Campaign) campaignList.get(j))
                                                              .getNameFromModel(), false);
                        DefaultMutableTreeNode testListParent = (DefaultMutableTreeNode) testNodeInCampaignTree
                            .getParent();
                        SalomeTMFPanels.getCampaignDynamicTree().removeNode(
                                                                            testNodeInCampaignTree);
                        if (testListParent.getChildCount() == 0) {
                            DefaultMutableTreeNode familyParent = (DefaultMutableTreeNode) testListParent
                                .getParent();
                            SalomeTMFPanels.getCampaignDynamicTree()
                                .removeNode(testListParent);
                            if (familyParent.getChildCount() == 0) {
                                SalomeTMFPanels.getCampaignDynamicTree()
                                    .removeNode(familyParent);
                            }
                        }
                        // on nettoie la table des resultats des executions si
                        // la campagne
                        // traitee est selectionnee dans l'arbre des campagnes
                        if ((SalomeTMFPanels.getCampaignDynamicTree()
                             .getSelectedNode()) != null
                            && (SalomeTMFPanels.getCampaignDynamicTree()
                                .getSelectedNode()).getUserObject() instanceof Campaign
                            && ((Campaign) (SalomeTMFPanels
                                            .getCampaignDynamicTree()
                                            .getSelectedNode()).getUserObject())
                            .equals((campaignList.get(j)))) {
                            executionResultTableModel.clearTable();
                            // TestData.getExecutionTableModel().clearTable();
                        }

                    }

                } catch (Exception exception) {
                    Util.err(exception);
                    if (deleted) {
                        // Erreur au niveau de L'IHM -> faire un reload
                        reloadFromBase(true);
                        result = true;
                    } else {
                        // Erreur au niveau BDD
                        result = false;
                        Tools.ihmExceptionView(exception);
                    }
                }
                // LE CAS DES FAMILLES
            } else if (actionCase == FAMILY) {
                try {
                    // // Delete req links
                    /*
                     * Family family = (Family)selectedNode.getUserObject();
                     * ArrayList testLists = family.getSuiteListFromModel(); for
                     * (int j=0; j<testLists.size(); j++) { TestList testList =
                     * (TestList)testLists.get(j); ArrayList tests =
                     * testList.getTestListFromModel(); for (int k=0;
                     * k<tests.size(); k++) { Test test = (Test)tests.get(k); if
                     * (reqManagers!=null && reqManagers.size() != 0) { for (int
                     * i=0; i<reqManagers.size(); i++) { ReqManager req =
                     * (ReqManager)reqManagers.elementAt(i);
                     * req.deleteReqLinkWithTest(test.getIdBdd()); } } } }
                     */

                    // BDD & DATA
                    currentProject
                        .deleteFamilyInDBAndModel((Family) selectedNode
                                                  .getUserObject());
                    deleted = true;

                    // IHM
                    for (int j = 0; j < campaignList.size(); j++) {
                        // Nettoyage de l'arbre des campagnes
                        DefaultMutableTreeNode familyNodeInCampaignTree = SalomeTMFPanels
                            .getCampaignDynamicTree()
                            .findRemoveFamilyNodeInCampagneTree(
                                                                ((Family) selectedNode.getUserObject())
                                                                .getNameFromModel(),
                                                                ((Campaign) campaignList.get(j))
                                                                .getNameFromModel(), false);
                        SalomeTMFPanels.getCampaignDynamicTree().removeNode(
                                                                            familyNodeInCampaignTree);
                        // on nettoie la table des resultats des executions si
                        // la campagne
                        // traitee est selectionnee dans l'abre des campagnes
                        if ((SalomeTMFPanels.getCampaignDynamicTree()
                             .getSelectedNode()) != null
                            && (SalomeTMFPanels.getCampaignDynamicTree()
                                .getSelectedNode()).getUserObject() instanceof Campaign
                            && ((Campaign) (SalomeTMFPanels
                                            .getCampaignDynamicTree()
                                            .getSelectedNode()).getUserObject())
                            .equals((campaignList.get(j)))) {
                            getExecutionResultTableModel().clearTable();
                            // TestData.getExecutionTableModel().clearTable();
                        }
                    }

                } catch (Exception exception) {
                    if (deleted) {
                        // Erreur au niveau de L'IHM -> faire un reload
                        reloadFromBase(true);
                        result = true;
                    } else {
                        result = false;
                        Tools.ihmExceptionView(exception);
                    }
                }
            } else {
                try {
                    // Delete req links
                    /*
                     * TestList testList =
                     * (TestList)selectedNode.getUserObject(); ArrayList tests =
                     * testList.getTestListFromModel(); for (int k=0;
                     * k<tests.size(); k++) { Test test = (Test)tests.get(k); if
                     * (reqManagers!=null && reqManagers.size() != 0) { for (int
                     * i=0; i<reqManagers.size(); i++) { ReqManager req =
                     * (ReqManager)reqManagers.elementAt(i);
                     * req.deleteReqLinkWithTest(test.getIdBdd()); } } }
                     */

                    // DB
                    currentProject
                        .deleteTestListInDBandModel((TestList) selectedNode
                                                    .getUserObject());
                    deleted = true;

                    // IHM
                    for (int j = 0; j < campaignList.size(); j++) {

                        // Nettoyage de l'arbre des campagnes
                        DefaultMutableTreeNode testListNodeInCampaignTree = SalomeTMFPanels
                            .getCampaignDynamicTree()
                            .findRemoveTestListNodeInCampagneTree(
                                                                  ((TestList) selectedNode
                                                                   .getUserObject())
                                                                  .getNameFromModel(),
                                                                  ((TestList) selectedNode
                                                                   .getUserObject())
                                                                  .getFamilyFromModel()
                                                                  .getNameFromModel(),
                                                                  ((Campaign) campaignList.get(j))
                                                                  .getNameFromModel(), false);
                        DefaultMutableTreeNode familyParent = (DefaultMutableTreeNode) testListNodeInCampaignTree
                            .getParent();
                        SalomeTMFPanels.getCampaignDynamicTree().removeNode(
                                                                            testListNodeInCampaignTree);
                        if (familyParent.getChildCount() == 0) {
                            SalomeTMFPanels.getCampaignDynamicTree()
                                .removeNode(familyParent);
                        }
                        // on nettoie la table des r?sultats des executions si
                        // la campagne
                        // traitee est selectionnee dans l'abre des campagnes
                        if ((SalomeTMFPanels.getCampaignDynamicTree()
                             .getSelectedNode()) != null
                            && (SalomeTMFPanels.getCampaignDynamicTree()
                                .getSelectedNode()).getUserObject() instanceof Campaign
                            && ((Campaign) (SalomeTMFPanels
                                            .getCampaignDynamicTree()
                                            .getSelectedNode()).getUserObject())
                            .equals((campaignList.get(j)))) {
                            executionResultTableModel.clearTable();
                            // TestData.getExecutionTableModel().clearTable();
                        }

                    }

                } catch (Exception exception) {
                    if (deleted) {
                        // Erreur au niveau de L'IHM -> faire un reload
                        reloadFromBase(true);
                        result = true;
                    } else {
                        result = false;
                        Tools.ihmExceptionView(exception);
                    }
                }
            }
            if (deleted) {
                SalomeTMFPanels.setTestPanelWorkSpace(-1); // remove all
                SalomeTMFPanels.getTestDynamicTree().removeCurrentNode();
                SalomeTMFPanels.getDelTestOrTestList().setEnabled(false);
                SalomeTMFPanels.reValidateTestPanel();
            }
            SalomeTMFPanels.getTestDynamicTree().getTree().setCursor(
                                                                     new Cursor(Cursor.DEFAULT_CURSOR));
        }

        return result;
    } // Fin de la methode deleteReally/0

    /**
     * Methode de suppression de tests, suites de tests, familles ou campagnes
     * de l'arbre des campagnes.
     *
     * @return vrai si la suppression s'est correctement deroulee, faux sinon.
     */
    public static boolean deleteInCampaignTree() {
        DefaultMutableTreeNode selectedNode;
        boolean result = true;
        boolean isCampEmpty = false;
        boolean deleted = false;

        selectedNode = SalomeTMFPanels.getCampaignDynamicTree()
            .getSelectedNode();
        if (selectedNode == null) {
            return true;
        }
        Object[] options = { Language.getInstance().getText("Oui"),
                             Language.getInstance().getText("Non") };
        int choice = -1;
        int actionCase = -1;
        if ((selectedNode.getUserObject() instanceof Test)) {
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText(
                                                                                               "Etes_vous_sur_de_vouloir_supprimer_le_test_")
                                                                + ((Test) selectedNode.getUserObject())
                                                                .getNameFromModel()
                                                                + Language.getInstance().getText(
                                                                                                 "_de_cette_campagne_"),
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.QUESTION_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(),Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_le_test_") +
             * ((Test)selectedNode.getUserObject()).getNameFromModel() +
             * Language.getInstance().getText("_de_cette_campagne_"),
             * Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = TEST;
        } else if (selectedNode.getUserObject() instanceof TestList) {
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText(
                                                                                               "Etes_vous_sur_de_vouloir_supprimer_la_suite_")
                                                                + ((TestList) selectedNode.getUserObject())
                                                                .getNameFromModel()
                                                                + Language.getInstance().getText(
                                                                                                 "_de_cette_campagne_"),
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.QUESTION_MESSAGE, options);
            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(),Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_la_suite_") +
             * ((TestList)selectedNode.getUserObject()).getNameFromModel()+
             * Language.getInstance().getText("_de_cette_campagne_"),
             * Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = TESTLIST;
        } else if (selectedNode.getUserObject() instanceof Family) {
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText(
                                                                                               "Etes_vous_sur_de_vouloir_supprimer_la_famille_")
                                                                + ((Family) selectedNode.getUserObject())
                                                                .getNameFromModel()
                                                                + Language.getInstance().getText(
                                                                                                 "_de_cette_campagne_"),
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.QUESTION_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(),Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_la_famille_") +
             * ((Family)selectedNode.getUserObject()).getNameFromModel()+
             * Language.getInstance().getText("_de_cette_campagne_"),
             * Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = FAMILY;
        } else if (selectedNode.getUserObject() instanceof Campaign) {
            choice = SalomeTMFContext.getInstance().askQuestion(
                                                                Language.getInstance().getText(
                                                                                               "Etes_vous_sur_de_vouloir_supprimer_la_campagne_")
                                                                + ((Campaign) selectedNode.getUserObject())
                                                                .getNameFromModel() + "> ?",
                                                                Language.getInstance().getText("Attention_"),
                                                                JOptionPane.QUESTION_MESSAGE, options);

            /*
             * choice =
             * JOptionPane.showOptionDialog(SalomeTMFContext.getInstance
             * ().getSalomeFrame(),Language.getInstance().getText(
             * "Etes_vous_sur_de_vouloir_supprimer_la_campagne_") +
             * ((Campaign)selectedNode.getUserObject()).getNameFromModel() +
             * "> ?", Language.getInstance().getText("Attention_"),
             * JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
             * options, options[1]);
             */
            actionCase = CAMPAIGN;
        }

        if (choice == JOptionPane.YES_OPTION) {
            if (actionCase == CAMPAIGN) {
                try {
                    // BdD
                    currentProject
                        .deleteCampaignInDBAndModel((Campaign) selectedNode
                                                    .getUserObject());
                    deleted = true;

                    // Model
                    setCurrentCampaign(null);
                    SalomeTMFPanels.setCampPanelWorkSpace(-1);
                    SalomeTMFPanels.getAddTestInCampagne().setEnabled(false);
                    SalomeTMFPanels.getOrderCampagne().setEnabled(false);
                    SalomeTMFPanels.getDelCampagne().setEnabled(false);
                    SalomeTMFPanels.reValidateCampainPanel();
                    SalomeTMFPanels.getCampaignDynamicTree()
                        .removeCurrentNode();
                    
                    logger.info(LoggingData.getData() + "Campaign = " + 
                            (Campaign) selectedNode.getUserObject() + " Successfully deleted!");

                } catch (Exception exception) {
                    if (deleted) {
                        // Erreur au niveau de L'IHM -> faire un reload
                        reloadFromBase(true);
                        result = true;
                    } else {
                        Tools.ihmExceptionView(exception);
                        result = false;
                    }
                }
            } else {
                if (actionCase == TEST) {
                    try {
                        // BDD and Model
                        isCampEmpty = currentProject
                            .deleteTestFromCampaignInDBAndModel(
                                                                (Test) selectedNode.getUserObject(),
                                                                currentCampaign);
                        deleted = true;

                        // IHM
                        DefaultMutableTreeNode testListParent = (DefaultMutableTreeNode) selectedNode
                            .getParent();
                        SalomeTMFPanels.getCampaignDynamicTree().removeNode(
                                                                            selectedNode);
                        if (testListParent.getChildCount() == 0) {
                            DefaultMutableTreeNode familyParent = (DefaultMutableTreeNode) testListParent
                                .getParent();
                            SalomeTMFPanels.getCampaignDynamicTree()
                                .removeNode(testListParent);
                            if (familyParent.getChildCount() == 0) {
                                SalomeTMFPanels.getCampaignDynamicTree()
                                    .removeNode(familyParent);
                            }
                        }
                    } catch (Exception exception) {
                        if (deleted) {
                            // Erreur au niveau de L'IHM -> faire un reload
                            reloadFromBase(true);
                            result = true;
                        } else {
                            Tools.ihmExceptionView(exception);
                            result = false;
                        }
                    }

                } else if (actionCase == FAMILY) {
                    try {
                        // BdD
                        isCampEmpty = currentProject
                            .deleteFamilyFromCampaignInDBAndModel(
                                                                  (Family) selectedNode.getUserObject(),
                                                                  currentCampaign);
                        deleted = true;

                        // IHM
                        SalomeTMFPanels.getCampaignDynamicTree()
                            .removeCurrentNode();
                    } catch (Exception exception) {
                        if (deleted) {
                            // Erreur au niveau de L'IHM -> faire un reload
                            reloadFromBase(true);
                            result = true;
                        } else {
                            Tools.ihmExceptionView(exception);
                            result = false;
                        }
                    }
                } else {
                    try {
                        // BDD and Model
                        isCampEmpty = currentProject
                            .deleteTestListFromCampaignInDBAndModel(
                                                                    (TestList) selectedNode.getUserObject(),
                                                                    currentCampaign);
                        deleted = true;

                        // IHM
                        DefaultMutableTreeNode familyParent = (DefaultMutableTreeNode) selectedNode
                            .getParent();
                        SalomeTMFPanels.getCampaignDynamicTree().removeNode(
                                                                            selectedNode);
                        if (familyParent.getChildCount() == 0) {
                            SalomeTMFPanels.getCampaignDynamicTree()
                                .removeNode(familyParent);
                        }
                    } catch (Exception exception) {
                        if (deleted) {
                            // Erreur au niveau de L'IHM -> faire un reload
                            reloadFromBase(true);
                            result = true;
                        } else {
                            Tools.ihmExceptionView(exception);
                            result = false;
                        }
                    }
                }

                // Si la campagne est vide, suppression de ses resultats
                // d'execution
                if (isCampEmpty) {
                    SalomeTMFContext
                        .getInstance()
                        .showMessage(
                                     Language
                                     .getInstance()
                                     .getText(
                                              "Les_resultats_d_execution_de_cette_campagne_ont_ete_supprimes_car_elle_ne_contient_plus_de_tests"),
                                     Language.getInstance().getText(
                                                                    "Information_"),
                                     JOptionPane.INFORMATION_MESSAGE);
                    /*
                     * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance
                     * ().getSalomeFrame(),Language.getInstance().getText(
                     * "Les_resultats_d_execution_de_cette_campagne_ont_ete_supprimes_car_elle_ne_contient_plus_de_tests"
                     * ), Language.getInstance().getText("Information_"),
                     * JOptionPane.INFORMATION_MESSAGE);
                     */
                }
                SalomeTMFPanels.setTestPanelWorkSpace(-1); // remove all
                SalomeTMFPanels.getDelTestOrTestList().setEnabled(false);
                SalomeTMFPanels.reValidateTestPanel();
            }
            // return true;
        }
        // return false;
        return result;
    } // Fin de la methode deleteAction/0

    // **************************************** Ajout
    // *********************************************//
    public static void makeCopie(SimpleData toCopie, SimpleData from)
        throws Exception {
        if (toCopie == null) {
            return;
        }

        if (toCopie instanceof ManualTest) {
            ManualTest pTest;
            if (from instanceof Family) {
                Family pFamily = (Family) from;
                TestList pTestList = DataModel
                    .getDefaultTestList(pFamily, true);
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveTestListNode(
                                                                 ApiConstants.DEFAULT_TESTLIST_NAME,
                                                                 pTestList.getFamilyFromModel()
                                                                 .getNameFromModel(), false);

                pTest = ManualTest.copieIn((ManualTest) toCopie, pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);
            } else if (from instanceof TestList) {
                TestList pTestList = (TestList) from;
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveTestListNode(
                                                                 pTestList.getNameFromModel(),
                                                                 pTestList.getFamilyFromModel()
                                                                 .getNameFromModel(), false);

                pTest = ManualTest.copieIn((ManualTest) toCopie, pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);
            } else if (from instanceof Test) {

                TestList pTestList = ((Test) from).getTestListFromModel();
                if (pTestList != null) {
                    DefaultMutableTreeNode pNode = SalomeTMFPanels
                        .getTestDynamicTree().findRemoveTestListNode(
                                                                     pTestList.getNameFromModel(),
                                                                     pTestList.getFamilyFromModel()
                                                                     .getNameFromModel(), false);

                    pTest = ManualTest.copieIn((ManualTest) toCopie, pTestList);

                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }
            } else {
                Family pFamily = DataModel.getDefaultFamily(true);
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                TestList pTestList = DataModel
                    .getDefaultTestList(pFamily, true);
                pNode = SalomeTMFPanels.getTestDynamicTree()
                    .findRemoveTestListNode(
                                            ApiConstants.DEFAULT_TESTLIST_NAME,
                                            pTestList.getFamilyFromModel()
                                            .getNameFromModel(), false);

                pTest = ManualTest.copieIn((ManualTest) toCopie, pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);

            }
        } else if (toCopie instanceof AutomaticTest) {
            AutomaticTest pTest;
            if (from instanceof Family) {
                Family pFamily = (Family) from;
                TestList pTestList = DataModel
                    .getDefaultTestList(pFamily, true);
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveTestListNode(
                                                                 ApiConstants.DEFAULT_TESTLIST_NAME,
                                                                 pTestList.getFamilyFromModel()
                                                                 .getNameFromModel(), false);

                pTest = AutomaticTest.copieIn((AutomaticTest) toCopie,
                                              pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);
            } else if (from instanceof TestList) {
                TestList pTestList = (TestList) from;
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveTestListNode(
                                                                 pTestList.getNameFromModel(),
                                                                 pTestList.getFamilyFromModel()
                                                                 .getNameFromModel(), false);

                pTest = AutomaticTest.copieIn((AutomaticTest) toCopie,
                                              pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);
            } else if (from instanceof Test) {

                TestList pTestList = ((Test) from).getTestListFromModel();
                if (pTestList != null) {
                    DefaultMutableTreeNode pNode = SalomeTMFPanels
                        .getTestDynamicTree().findRemoveTestListNode(
                                                                     pTestList.getNameFromModel(),
                                                                     pTestList.getFamilyFromModel()
                                                                     .getNameFromModel(), false);

                    pTest = AutomaticTest.copieIn((AutomaticTest) toCopie,
                                                  pTestList);

                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }
            } else {
                Family pFamily = DataModel.getDefaultFamily(true);
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                TestList pTestList = DataModel
                    .getDefaultTestList(pFamily, true);
                pNode = SalomeTMFPanels.getTestDynamicTree()
                    .findRemoveTestListNode(
                                            ApiConstants.DEFAULT_TESTLIST_NAME,
                                            pTestList.getFamilyFromModel()
                                            .getNameFromModel(), false);

                pTest = AutomaticTest.copieIn((AutomaticTest) toCopie,
                                              pTestList);

                SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest,
                                                               true);

            }
        } else if (toCopie instanceof TestList) {
            if (from instanceof Family) {
                Family pFamily = (Family) from;
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                TestList pTestList = TestList.copieIn((TestList) toCopie,
                                                      pFamily);

                pNode = SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                       pTestList, true);
                // Ajout des test ?? dans l'arbre ???
                ArrayList listOfTest = pTestList.getTestListFromModel();
                int size = listOfTest.size();
                for (int i = 0; i < size; i++) {
                    Test pTest = (Test) listOfTest.get(i);
                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }
            } else if (from instanceof TestList) {
                TestList pTestList = (TestList) from;
                Family pFamily = pTestList.getFamilyFromModel();

                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                pTestList = TestList.copieIn((TestList) toCopie, pFamily);

                pNode = SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                       pTestList, true);
                // Ajout des test ?? dans l'arbre ???
                ArrayList listOfTest = pTestList.getTestListFromModel();
                int size = listOfTest.size();
                for (int i = 0; i < size; i++) {
                    Test pTest = (Test) listOfTest.get(i);
                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }

            } else if (from instanceof Test) {
                TestList pTestList = ((Test) from).getTestListFromModel();
                Family pFamily = pTestList.getFamilyFromModel();

                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                pTestList = TestList.copieIn((TestList) toCopie, pFamily);

                pNode = SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                       pTestList, true);
                // Ajout des test ?? dans l'arbre ???
                ArrayList listOfTest = pTestList.getTestListFromModel();
                int size = listOfTest.size();
                for (int i = 0; i < size; i++) {
                    Test pTest = (Test) listOfTest.get(i);
                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }
            } else {
                Family pFamily = DataModel.getDefaultFamily(true);
                DefaultMutableTreeNode pNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               pFamily.getNameFromModel(), false);

                TestList pTestList = TestList.copieIn((TestList) toCopie,
                                                      pFamily);

                pNode = SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                       pTestList, true);
                // Ajout des test ?? dans l'arbre ???
                ArrayList listOfTest = pTestList.getTestListFromModel();
                int size = listOfTest.size();
                for (int i = 0; i < size; i++) {
                    Test pTest = (Test) listOfTest.get(i);
                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                                   pTest, true);
                }
            }
        } else if (toCopie instanceof Family) {
            Family newFamily = Family.copieIn((Family) toCopie);
            // IHM
            DefaultMutableTreeNode pNode = SalomeTMFPanels.getTestDynamicTree()
                .addObject(null, newFamily, true);
            ArrayList listOfTestList = newFamily.getSuiteListFromModel();
            int size = listOfTestList.size();
            for (int i = 0; i < size; i++) {
                TestList pTestList = (TestList) listOfTestList.get(i);
                DefaultMutableTreeNode pNode2 = SalomeTMFPanels
                    .getTestDynamicTree().addObject(pNode, pTestList, true);
                // Ajout des test ?? dans l'arbre ???
                ArrayList listOfTest = pTestList.getTestListFromModel();
                int size2 = listOfTest.size();
                for (int j = 0; j < size2; j++) {
                    Test pTest = (Test) listOfTest.get(j);
                    SalomeTMFPanels.getTestDynamicTree().addObject(pNode2,
                                                                   pTest, true);
                }
            }
        } else if (toCopie instanceof Campaign) {
            Campaign pCamp = Campaign.copieIn((Campaign) toCopie, currentUser);
            DataModel.getCurrentProject().addCampaignInModel(pCamp);
            DefaultMutableTreeNode pCampNode = SalomeTMFPanels
                .getCampaignDynamicTree().addObject(null, pCamp, true);
            // ajouter les tests
            ArrayList testList = pCamp.getTestListFromModel();
            Family pCurrentFamily = null;
            TestList pCurrentTestList = null;
            DefaultMutableTreeNode pFamilyNode = null;
            DefaultMutableTreeNode pSuiteNode = null;
            for (int i = 0; i < testList.size(); i++) {
                Test pTest = (Test) testList.get(i);
                TestList pTestList = pTest.getTestListFromModel();
                Family pFamily = pTestList.getFamilyFromModel();
                if (!pFamily.equals(pCurrentFamily)) {
                    pFamilyNode = SalomeTMFPanels.getCampaignDynamicTree()
                        .addObject(pCampNode, pFamily, true);
                    pCurrentFamily = pFamily;
                }
                if (!pTestList.equals(pCurrentTestList)) {
                    pSuiteNode = SalomeTMFPanels.getCampaignDynamicTree()
                        .addObject(pFamilyNode, pTestList, true);
                    pCurrentTestList = pTestList;
                }
                SalomeTMFPanels.getCampaignDynamicTree().addObject(pSuiteNode,
                                                                   pTest, true);
            }
            SalomeTMFPanels.getCampaignDynamicTree().repaint();
            SalomeTMFPanels.getCampaignDynamicTree().scrollPathToVisible(
                                                                         pCampNode);
        }
    }

    public static TestList getDefaultTestList(Family pFamily, boolean toAdd)
        throws Exception {
        Project currentProject = DataModel.getCurrentProject();
        TestList pTestList = pFamily
            .getTestListInModel(ApiConstants.DEFAULT_TESTLIST_NAME);
        DefaultMutableTreeNode pNode = SalomeTMFPanels.getTestDynamicTree()
            .findRemoveFamilyNode(pFamily.getNameFromModel(), false);
        if (pTestList == null) {

            if (TestList.isInBase(pFamily, ApiConstants.DEFAULT_TESTLIST_NAME)) {
                // Add only in Model
                Vector suiteVector = pFamily.getSuitesWrapperFromDB();
                int i = 0;
                boolean found = false;
                while ((i < suiteVector.size()) && (!found)) {
                    SuiteWrapper suiteBdd = (SuiteWrapper) suiteVector.get(i);
                    if (suiteBdd.getName().equals(
                                                  ApiConstants.DEFAULT_TESTLIST_NAME)) {
                        found = true;
                        pTestList = new TestList(pFamily, suiteBdd);
                        currentProject.addTestListInFamilyInModel(pTestList,
                                                                  pFamily);
                    }
                    i++;
                }
            } else {
                if (toAdd) {
                    // Add in BDD & MODEL
                    pTestList = new TestList(
                                             ApiConstants.DEFAULT_TESTLIST_NAME,
                                             ApiConstants.DEFAULT_TESTLIST_DESC);
                    currentProject.addTestListInFamilyInDBAndModel(pTestList,
                                                                   pFamily);
                }
            }
            if (toAdd) {
                // IHM
                SalomeTMFPanels.getTestDynamicTree().addObject(pNode,
                                                               pTestList, true);
            }

        }

        return pTestList;
    }

    public static Family getDefaultFamily(boolean toAdd) throws Exception {
        Project currentProject = DataModel.getCurrentProject();
        Family pFamily = currentProject
            .getFamilyFromModel(ApiConstants.DEFAULT_FAMILY_NAME);
        if (pFamily == null) {

            if (Family.isInBase(currentProject,
                                ApiConstants.DEFAULT_FAMILY_NAME)) {
                // Add only in Model
                Vector familyVector = currentProject
                    .getProjectFamiliesWrapperFromDB();
                int i = 0;
                boolean found = false;
                while ((i < familyVector.size()) && (!found)) {
                    FamilyWrapper familyBdd = (FamilyWrapper) familyVector
                        .get(i);
                    if (familyBdd.getName().equals(
                                                   ApiConstants.DEFAULT_FAMILY_NAME)) {
                        found = true;
                        pFamily = new Family(currentProject, familyBdd);
                        currentProject.addFamilyInModel(pFamily);
                    }
                    i++;
                }
            } else {
                // Add in BDD & MODEL
                if (toAdd) {
                    pFamily = new Family(ApiConstants.DEFAULT_FAMILY_NAME,
                                         ApiConstants.DEFAULT_FAMILY_DESC);
                    currentProject.addFamilyInDBAndModel(pFamily);
                }
            }

            if (toAdd) {
                SalomeTMFPanels.getTestDynamicTree().addObject(null, pFamily,
                                                               true);
            }

        }
        return pFamily;
    }

    /**
     * Methode qui permet d'ajouter une nouvelle campagne de tests
     */
    public static void addNewCampagne() {
        AskNewCampagne askNewCampagne = new AskNewCampagne(SalomeTMFContext
                                                           .getInstance().getSalomeFrame(), Language.getInstance()
                                                           .getText("Nouvelle_campagne_de_tests"));
        if (askNewCampagne.getCampagne() == null)
            return;
        if (currentProject.containsCampaignInModel(askNewCampagne.getCampagne()
                                                   .getNameFromModel())
            || Campaign.isInBase(currentProject, askNewCampagne
                                 .getCampagne())) {
            SalomeTMFContext.getInstance().showMessage(
                                                       "Le nom de la campagne existe deja !",
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);

            /*
             * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame
             * (), "Le nom de la campagne existe deja !",
             * Language.getInstance().getText("Erreur_"),
             * JOptionPane.ERROR_MESSAGE);
             */
        } else {

            try {
                Campaign camp = askNewCampagne.getCampagne();
                // BdD
                currentProject.addCampaignInDBandModel(camp);

                // IHM
                SalomeTMFPanels.getCampaignDynamicTree().addObject(null, camp,
                                                                   true);
                logger.info(LoggingData.getData() + "Campaign = " + 
                        camp.getNameFromModel().toString() + " successfully created!");
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
            }
        }
    } // Fin de la m?thode addNewCampagne/0

    /**
     * Methode qui permet d'ajouter une nouvelle suite de tests
     */
    public static void addNewTestList() {
        // Fen?tre pour construire la nouvelle suite
        AskNewTestList testListDialog = new AskNewTestList(Language
                                                           .getInstance().getText("Nouvelle_suite"), Language
                                                           .getInstance().getText("Nom"));
        // La nouvelle suite en cours de cr?ation
        TestList newTestList = testListDialog.getTestList();
        // Le noeud selectionne
        DefaultMutableTreeNode node = SalomeTMFPanels.getTestDynamicTree()
            .getSelectedNode();
        // l'utilisateur n'a pas annule
        if (newTestList == null) {
            return;
        }
        DefaultMutableTreeNode familyNode = null;
        Family pFamily = null;

        if (node != null) {
            if (node.getUserObject() instanceof Family) {
                pFamily = (Family) node.getUserObject();
            } else if (node.getUserObject() instanceof TestList) {
                pFamily = (((TestList) node.getUserObject())
                           .getFamilyFromModel());
            } else if (node.getUserObject() instanceof Test) {
                pFamily = (((Test) node.getUserObject()).getTestListFromModel()
                           .getFamilyFromModel());
            }
        }
        if (pFamily == null) {
            // Ajout de la famille par defaut
            try {
                pFamily = getDefaultFamily(true);
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
                return;
            }
            /*
             * pFamily =
             * currentProject.getFamilyFromModel(ApiConstants.DEFAULT_FAMILY_NAME
             * ); if (pFamily == null){
             *
             * //if (!TestPlanData.containsFamily(pFamily.getName())){ try {
             *
             * if
             * (Family.isInBase(currentProject,ApiConstants.DEFAULT_FAMILY_NAME
             * )) { // Add only in Model Vector familyVector =
             * currentProject.getProjectFamiliesWrapperFromDB(); int i = 0;
             * boolean found = false; while ((i<familyVector.size())&&(!found))
             * { FamilyWrapper familyBdd = (FamilyWrapper) familyVector.get(i);
             * if (familyBdd.getName().equals(ApiConstants.DEFAULT_FAMILY_NAME))
             * { found = true; pFamily = new Family(currentProject, familyBdd);
             * currentProject.addFamilyInModel(pFamily); } i++; } } else { //
             * Add in BDD & MODEL pFamily = new
             * Family(ApiConstants.DEFAULT_FAMILY_NAME,
             * ApiConstants.DEFAULT_FAMILY_DESC);
             * currentProject.addFamilyInDBAndModel(pFamily); }
             *
             * //IHM familyNode =
             * SalomeTMFPanels.getTestDynamicTree().addObject(null, pFamily,
             * true); } catch (Exception exception) {
             * Tools.ihmExceptionView(exception); return; } }
             */
        }

        // newTestList.setFamily(pFamily);
        try {
            if (currentProject.containsTestListInModel(pFamily, newTestList
                                                       .getNameFromModel())
                || TestList.isInBase(pFamily, newTestList)) {
                SalomeTMFContext
                    .getInstance()
                    .showMessage(
                                 Language
                                 .getInstance()
                                 .getText(
                                          "Le_nom_de_la_suite_existe_deja_dans_cette_famille_"),
                                 Language.getInstance().getText("Erreur_"),
                                 JOptionPane.ERROR_MESSAGE);
                /*
                 * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame
                 * (),Language.getInstance().getText(
                 * "Le_nom_de_la_suite_existe_deja_dans_cette_famille_"),
                 * Language.getInstance().getText("Erreur_"),
                 * JOptionPane.ERROR_MESSAGE);
                 */
                return;
            }

            // BDD & MODEL
            currentProject
                .addTestListInFamilyInDBAndModel(newTestList, pFamily);

            // IHM
            if (familyNode == null) {
                DefaultMutableTreeNode fNode = SalomeTMFPanels
                    .getTestDynamicTree().findRemoveFamilyNode(
                                                               newTestList.getFamilyFromModel()
                                                               .getNameFromModel(), false);
                if (fNode == null) {
                    familyNode = SalomeTMFPanels.getTestDynamicTree()
                        .addObject(null, newTestList.getFamilyFromModel(),
                                   true);
                } else {
                    familyNode = fNode;
                }
            }
            SalomeTMFPanels.getTestDynamicTree().addObject(familyNode,
                                                           newTestList, true);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
    } // Fin de la m?thode addNewTestList/0

    public static void addNewFamily() {
        AskNameAndDescription askNewFamily = new AskNameAndDescription(FAMILY,
                                                                       Language.getInstance().getText("Nouvelle_famille"), Language
                                                                       .getInstance().getText("Nom")+" ", SalomeTMFContext
                                                                       .getInstance().getSalomeFrame(), null);
        Family newFamily = askNewFamily.getFamily();
        if (newFamily != null) {
            if (currentProject.containsFamilyInModel(newFamily
                                                     .getNameFromModel())
                || Family.isInBase(currentProject, newFamily)) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText(
                                                                                          "Cette_famille_existe_deja_"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);

                /*
                 * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame
                 * (),
                 * Language.getInstance().getText("Cette_famille_existe_deja_"),
                 * Language.getInstance().getText("Erreur_"),
                 * JOptionPane.ERROR_MESSAGE);
                 */
            } else {
                try {
                    // BDD & MODEL
                    currentProject.addFamilyInDBAndModel(newFamily);
                    // IHM
                    SalomeTMFPanels.getTestDynamicTree().addObject(null,
                                                                   newFamily, true);
                } catch (DataUpToDateException e1) {
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("Update_data"),
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }

            }
        }
    } // Fin de la methode addNewTestList/0

    /**
     * M?thode qui permet d'ajouter un nouveau test.
     */
    public static void addNewTest() {
        // String type ="";
        AskNewTest askNewTest = new AskNewTest(SalomeTMFContext.getInstance()
                                               .getSalomeFrame(), Language.getInstance().getText(
                                                                                                 "Nouveau_Test"));
        DefaultMutableTreeNode node = SalomeTMFPanels.getTestDynamicTree()
            .getSelectedNode();
        Test pTest = askNewTest.getTest();
        if (pTest == null)
            return;

        TestList pTestList;
        Family pFamily;
        // DefaultMutableTreeNode testListNode;
        // DefaultMutableTreeNode familyNode ;

        DefaultMutableTreeNode pNode = null;
        if (node == null
            || node == SalomeTMFPanels.getTestDynamicTree().getRoot()) {
            // Rien n'est selectionne
            // Ajout de la famille par defaut
            // pFamily = TestPlanData.getDefaultFamily();

            try {
                pFamily = getDefaultFamily(true);
                pNode = SalomeTMFPanels
                    .getTestDynamicTree()
                    .findRemoveFamilyNode(pFamily.getNameFromModel(), false);
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
                return;
            }

            /*
             * pFamily =
             * currentProject.getFamilyFromModel(ApiConstants.DEFAULT_FAMILY_NAME
             * ); if (pFamily == null){ //pFamily = new
             * Family(ApiConstants.DEFAULT_FAMILY_NAME,
             * ApiConstants.DEFAULT_FAMILY_DESC); try { if
             * (Family.isInBase(currentProject
             * ,ApiConstants.DEFAULT_FAMILY_NAME)) { // Add only in Model Vector
             * familyVector = currentProject.getProjectFamiliesWrapperFromDB();
             * int i = 0; boolean found = false; while
             * ((i<familyVector.size())&&(!found)) { FamilyWrapper familyBdd =
             * (FamilyWrapper) familyVector.get(i); if
             * (familyBdd.getName().equals(ApiConstants.DEFAULT_FAMILY_NAME)) {
             * found = true; pFamily = new Family(currentProject, familyBdd);
             * currentProject.addFamilyInModel(pFamily); } i++; }q } else { //
             * Add in BDD & MODEL pFamily = new
             * Family(ApiConstants.DEFAULT_FAMILY_NAME,
             * ApiConstants.DEFAULT_FAMILY_DESC);
             * currentProject.addFamilyInDBAndModel(pFamily); }
             *
             * //IHM pNode =
             * SalomeTMFPanels.getTestDynamicTree().addObject(null, pFamily,
             * true); } catch (Exception exception) {
             * Tools.ihmExceptionView(exception); return; } } else { pNode =
             * SalomeTMFPanels
             * .getTestDynamicTree().findRemoveFamilyNode(pFamily.
             * getNameFromModel(), false); }
             */

            try {
                pTestList = getDefaultTestList(pFamily, true);
                pNode = SalomeTMFPanels.getTestDynamicTree()
                    .findRemoveTestListNode(
                                            ApiConstants.DEFAULT_TESTLIST_NAME,
                                            pFamily.getNameFromModel(), false);
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
                return;
            }

            /*
             * pTestList =
             * pFamily.getTestListInModel(ApiConstants.DEFAULT_TESTLIST_NAME);
             * if (pTestList == null) { //Ajout de la suite par defaut
             * //pTestList = new TestList(ApiConstants.DEFAULT_TESTLIST_NAME,
             * ApiConstants.DEFAULT_TESTLIST_DESC);
             *
             *
             * try {
             *
             * if
             * (TestList.isInBase(pFamily,ApiConstants.DEFAULT_TESTLIST_NAME)) {
             * // Add only in Model Vector suiteVector =
             * pFamily.getSuitesWrapperFromDB(); int i = 0; boolean found =
             * false; while ((i<suiteVector.size())&&(!found)) { SuiteWrapper
             * suiteBdd = (SuiteWrapper) suiteVector.get(i); if
             * (suiteBdd.getName().equals(ApiConstants.DEFAULT_TESTLIST_NAME)) {
             * found = true; pTestList = new TestList(pFamily, suiteBdd);
             * currentProject.addTestListInFamilyInModel(pTestList,pFamily); }
             * i++; } } else { // Add in BDD & MODEL pTestList = new
             * TestList(ApiConstants.DEFAULT_TESTLIST_NAME,
             * ApiConstants.DEFAULT_TESTLIST_DESC);
             * currentProject.addTestListInFamilyInDBAndModel(pTestList,
             * pFamily); }
             *
             * //IHM pNode =
             * SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTestList,
             * true); } catch (Exception exception) {
             * Tools.ihmExceptionView(exception); return; } } else { pNode =
             * SalomeTMFPanels
             * .getTestDynamicTree().findRemoveTestListNode(ApiConstants
             * .DEFAULT_TESTLIST_NAME,
             * pTestList.getFamilyFromModel().getNameFromModel(), false); }
             */
        } else if (node.getUserObject() instanceof Family) {
            pFamily = (Family) node.getUserObject();
            try {
                pTestList = getDefaultTestList(pFamily, true);
                pNode = SalomeTMFPanels.getTestDynamicTree()
                    .findRemoveTestListNode(
                                            ApiConstants.DEFAULT_TESTLIST_NAME,
                                            pFamily.getNameFromModel(), false);
            } catch (Exception exception) {
                Tools.ihmExceptionView(exception);
                return;
            }

            /*
             * pTestList =
             * pFamily.getTestListInModel(ApiConstants.DEFAULT_TESTLIST_NAME);
             * pNode = node; if (pTestList == null) { //Ajout de la suite par
             * defaut //pTestList = new
             * TestList(ApiConstants.DEFAULT_TESTLIST_NAME,
             * ApiConstants.DEFAULT_TESTLIST_DESC); try {
             *
             * if
             * (TestList.isInBase(pFamily,ApiConstants.DEFAULT_TESTLIST_NAME)) {
             * // Add only in Model Vector suiteVector =
             * pFamily.getSuitesWrapperFromDB(); int i = 0; boolean found =
             * false; while ((i<suiteVector.size())&&(!found)) { SuiteWrapper
             * suiteBdd = (SuiteWrapper) suiteVector.get(i); if
             * (suiteBdd.getName().equals(ApiConstants.DEFAULT_TESTLIST_NAME)) {
             * found = true; pTestList = new TestList(pFamily, suiteBdd);
             * currentProject.addTestListInFamilyInModel(pTestList,pFamily); }
             * i++; } } else { // Add in BDD & MODEL pTestList = new
             * TestList(ApiConstants.DEFAULT_TESTLIST_NAME,
             * ApiConstants.DEFAULT_TESTLIST_DESC);
             * currentProject.addTestListInFamilyInDBAndModel(pTestList,
             * pFamily); }
             *
             * //IHM pNode =
             * SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTestList,
             * true); } catch (Exception exception) {
             * Tools.ihmExceptionView(exception); return; } } else { pNode =
             * SalomeTMFPanels
             * .getTestDynamicTree().findRemoveTestListNode(DEFAULT_TESTLIST_NAME
             * , pFamily.getNameFromModel(), false); }
             */
        } else if (node.getUserObject() instanceof TestList) {
            pTestList = (TestList) node.getUserObject();
            pFamily = pTestList.getFamilyFromModel();
            pNode = node;
        } else if (node.getUserObject() instanceof Test) {
            pTestList = ((Test) node.getUserObject()).getTestListFromModel();
            pFamily = pTestList.getFamilyFromModel();
            pNode = (DefaultMutableTreeNode) node.getParent();
        } else {
            // ???????????????????
            return;
        }

        try {
            if ((pTestList.getTestFromModel(pTest.getNameFromModel()) != null)
                || Test.isInBase(pTestList, pTest)) {
                SalomeTMFContext
                    .getInstance()
                    .showMessage(
                                 Language
                                 .getInstance()
                                 .getText(
                                          "Ce_nom_de_test_existe_deja_pour_cette_suite_"),
                                 Language.getInstance().getText("Erreur_"),
                                 JOptionPane.ERROR_MESSAGE);
                /*
                 * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance().getSalomeFrame
                 * (),Language.getInstance().getText(
                 * "Ce_nom_de_test_existe_deja_pour_cette_suite_"),
                 * Language.getInstance().getText("Erreur_"),
                 * JOptionPane.ERROR_MESSAGE);
                 */
                return;
            }

            // BDD & MODELL
            currentProject.addTestInListInDBAndModel(pTest, pTestList);
            // IHM
            SalomeTMFPanels.getTestDynamicTree().addObject(pNode, pTest, true);
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
        }
        // ???????????????
        pTest.setConceptorInModel(currentUser.getFirstNameFromModel() + " "
                                  + currentUser.getLastNameFromModel());

    } // Fin de la m?thode addNewTest/0

    public static void importTestsToCampaign() {
        if (currentCampaign == null) {
            return;
        }
        try {
            if (!currentCampaign.isValideModel()) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Update_data"),
                                                           Language.getInstance().getText("Erreur_"),
                                                           JOptionPane.ERROR_MESSAGE);
                return;
            }
        } catch (Exception e) {
            Tools.ihmExceptionView(e);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return;
        }

        DynamicTree campaignDynamicTree = SalomeTMFPanels
            .getCampaignDynamicTree();
        DynamicTree testDynamicTree = SalomeTMFPanels.getTestDynamicTree();
        DefaultMutableTreeNode selectedCampagneNode = campaignDynamicTree
            .getSelectedNode();
        ArrayList oldTestList = new ArrayList();
        Hashtable oldAssignedUser = new Hashtable();
        for (int k = 0; k < currentCampaign.getTestListFromModel().size(); k++) {
            Test pTest = (Test) currentCampaign.getTestListFromModel().get(k);
            int idUserAssigned = currentCampaign.getAssignedUserID(pTest);
            if (idUserAssigned == -1) {
                idUserAssigned = currentUser.getIdBdd();
            }
            oldAssignedUser.put(new Integer(pTest.getIdBdd()), new Integer(
                                                                           idUserAssigned));
            oldTestList.add(pTest);
        }

        ArrayList testsToBeKeeped = null;

        if (selectedCampagneNode == null) {
            return;
        }
        boolean doAnUpdate = false;
        if (currentCampaign.containsExecutionResultInModel()) {
            if (!Api.isLockExecutedTest()) {
                Object[] options = { Language.getInstance().getText("Oui"),
                                     Language.getInstance().getText("Non") };
                int choice = SalomeTMFContext
                    .getInstance()
                    .askQuestion(
                                 Language
                                 .getInstance()
                                 .getText(
                                          "Cette_campagne_contient_des_resultat_dexecution_plus_possible_de_les_modifier")
                                 + "\n"
                                 + Language.getInstance().getText(
                                                                  "continuer"),
                                 Language.getInstance().getText("Attention_"),
                                 JOptionPane.WARNING_MESSAGE, options);
                if (choice == JOptionPane.NO_OPTION) {
                    return;
                }
                doAnUpdate = true;
            } else {
                JOptionPane
                    .showMessageDialog(
                                       SalomeTMFContext.getInstance().getSalomeFrame(),
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Cette_campagne_contient_deja_des_resultat_d_execution_Il_n_est_plus_possible_de_la_modifier"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        FillCampagne pFillCampagne = new FillCampagne(selectedCampagneNode,
                                                      campaignDynamicTree.getModel(), testDynamicTree.getRoot());
        if (!pFillCampagne.okSelected()) {
            return;
        }
        boolean newDataSetCreated = false;
        ArrayList datasetsCreated;

        int transNumber = -1;
        try {
            // BDD & MODEL

            transNumber = Api.beginTransaction(110,
                                               ApiConstants.INSERT_TEST_INTO_CAMPAIGN);
            // On vide la campagne
            for (int j = 0; j < oldTestList.size(); j++) {
                currentCampaign.deleteTestFromCampInDB(((Test) oldTestList
                                                        .get(j)).getIdBdd(), false);
            }
            currentCampaign.clearAssignedUserForTest();

            // Puis on lui ajoute les tests
            testsToBeKeeped = currentCampaign.getTestListFromModel();
            ArrayList dataSets = currentCampaign.getDataSetListFromModel();

            if (testsToBeKeeped != null) {
                int size = testsToBeKeeped.size();
                for (int j = 0; j < size; j++) {
                    Test pTest = (Test) testsToBeKeeped.get(j);
                    Util
                        .log("[DataModel->importTestsToCampaign] test "
                             + pTest);
                    int id = currentUser.getIdBdd();
                    Integer userID = (Integer) oldAssignedUser.get(new Integer(
                                                                               pTest.getIdBdd()));
                    if (userID != null) {
                        id = userID.intValue();
                    }
                    datasetsCreated = currentProject
                        .addTestInCampaignInDBAndModel(pTest,
                                                       currentCampaign, id);

                    if (datasetsCreated.size() > 0) {
                        newDataSetCreated = true;
                        for (int k = 0; k < currentCampaign
                                 .getExecutionListFromModel().size(); k++) {
                            executionTableModel.setValueAt(
                                                           ((DataSet) datasetsCreated.get(k))
                                                           .getNameFromModel(), k, 3);
                        }
                    }
                }
            }
            /* IHM */
            campaignDynamicTree.repaint();
            if (newDataSetCreated) {
                for (int i = 0; i < dataSets.size(); i++) {
                    DataSet newDataSet = (DataSet) dataSets.get(i);
                    ArrayList dataView = new ArrayList();
                    dataView.add(newDataSet.getNameFromModel());
                    dataView.add(newDataSet.getDescriptionFromModel());
                    dataSetTableModel.addRow(dataView);
                }
            }
            if (pIAssignedCampAction != null) {
                pIAssignedCampAction.updateData(currentCampaign);
            }

            Api.commitTrans(transNumber);
            logger.info(LoggingData.getData() + "Campaign = " + 
                    currentCampaign.getNameFromModel().toString() + 
                    " Tests successfully imported!");
        } catch (Exception exception) {
            Api.forceRollBackTrans(transNumber);
            currentCampaign.setTestListInModel(oldTestList);
            Tools.ihmExceptionView(exception);
        }
        if (doAnUpdate) {
            // DataModel.reloadCamp()
        }

    }

    /********************************* RENAME *************************************************/

    /**
     * Methode permettant de renommer une campagne
     *
     */
    public static void renameCampaign() {
        DefaultMutableTreeNode selectedNode = SalomeTMFPanels
            .getCampaignDynamicTree().getSelectedNode();
        if (selectedNode != null
            && selectedNode.getUserObject() instanceof Campaign) {
            AskName askName = new AskName(Language.getInstance().getText(
                                                                         "Nouveau_nom_"),
                                          Language.getInstance().getText("Renommer"), Language
                                          .getInstance().getText("nom"),
                                          ((Campaign) selectedNode.getUserObject())
                                          .getNameFromModel(), SalomeTMFContext.getInstance()
                                          .getSalomeFrame());
            if (askName.getResult() != null) {
                if (!currentProject
                    .containsCampaignInModel(askName.getResult())
                    && !Campaign.isInBase(currentProject, askName
                                          .getResult())) {
                    try {
                        // BdD
                        ((Campaign) selectedNode.getUserObject())
                            .updateInDBAndModel(askName.getResult(),
                                                currentCampaign
                                                .getDescriptionFromModel());

                        // IHM
                        selectedNode
                            .setUserObject(selectedNode.getUserObject());
                        // SalomeTMFPanels.getCampaignNameLabel().setText(Language.getInstance().getText("Nom_de_la_campagne__")
                        // + askName.getResult());
                        SalomeTMFPanels.setCampPanelInfo(askName.getResult(),
                                                         null, null);
                        SalomeTMFPanels.getCampaignDynamicTree().refreshNode(
                                                                             selectedNode);
                        // SalomeTMFPanels.getCampaignDynamicTree().repaint();

                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }

                } else if (currentProject.getCampaignFromModel(askName
                                                               .getResult()) == null
                           || !currentProject.getCampaignFromModel(
                                                                   askName.getResult()).equals(
                                                                                               selectedNode.getUserObject())) {
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText(
                                                                                              "Le_nom_de_la_campagne_existe_deja_"),
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);

                    /*
                     * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance
                     * ().getSalomeFrame(),Language.getInstance().getText(
                     * "Le_nom_de_la_campagne_existe_deja_"),
                     * Language.getInstance().getText("Erreur_"),
                     * JOptionPane.ERROR_MESSAGE);
                     */
                }
            }
        }
    }

    /**
     * Methode permettant le renommage des tests, familles, suites de tests.
     *
     */
    public static void renameTest() {
        DefaultMutableTreeNode selectedNode = SalomeTMFPanels
            .getTestDynamicTree().getSelectedNode();
        if (selectedNode != null
            && selectedNode.getUserObject() instanceof SimpleData) {
            // String oldName =
            // ((Element)selectedNode.getUserObject()).getName();
            AskName askName = new AskName(Language.getInstance().getText(
                                                                         "Nouveau_nom_"),
                                          Language.getInstance().getText("Renommer"), Language
                                          .getInstance().getText("nom"),
                                          ((SimpleData) selectedNode.getUserObject())
                                          .getNameFromModel(), SalomeTMFContext.getInstance()
                                          .getSalomeFrame());
            if (askName.getResult() != null) {
                if (selectedNode.getUserObject() instanceof Test) {
                    if (!currentProject.containsTestInModel(
                                                            ((TestList) ((DefaultMutableTreeNode) selectedNode
                                                                         .getParent()).getUserObject()), askName
                                                            .getResult())
                        && !Test
                        .isInBase(
                                  ((TestList) ((DefaultMutableTreeNode) selectedNode
                                               .getParent())
                                   .getUserObject()), askName
                                  .getResult())) {

                        try {
                            ((Test) selectedNode.getUserObject())
                                .updateInDBAndModel(askName.getResult(),
                                                    ((Test) selectedNode
                                                     .getUserObject())
                                                    .getDescriptionFromModel());
                            // SalomeTMFPanels.getManualTestNameLabel().setText(Language.getInstance().getText("Nom_du_test__")
                            // +
                            // ((Test)selectedNode.getUserObject()).getNameFromModel());
                            SalomeTMFPanels.setTestPanelTestInfo(
                                                                 DataConstants.MANUAL_TEST,
                                                                 ((Test) selectedNode.getUserObject())
                                                                 .getNameFromModel(), null, null);
                            selectedNode.setUserObject(selectedNode
                                                       .getUserObject());
                            SalomeTMFPanels.getTestDynamicTree().refreshNode(
                                                                             selectedNode);
                            // SalomeTMFPanels.getTestDynamicTree().repaint();
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else if (currentProject.getTestFromModel(
                                                               ((TestList) ((DefaultMutableTreeNode) selectedNode
                                                                            .getParent()).getUserObject())
                                                               .getFamilyFromModel().getNameFromModel(),
                                                               ((TestList) ((DefaultMutableTreeNode) selectedNode
                                                                            .getParent()).getUserObject())
                                                               .getNameFromModel(), askName.getResult()) == null
                               || !currentProject
                               .getTestFromModel(
                                                 ((TestList) ((DefaultMutableTreeNode) selectedNode
                                                              .getParent())
                                                  .getUserObject())
                                                 .getFamilyFromModel()
                                                 .getNameFromModel(),
                                                 ((TestList) ((DefaultMutableTreeNode) selectedNode
                                                              .getParent())
                                                  .getUserObject())
                                                 .getNameFromModel(),
                                                 askName.getResult()).equals(
                                                                             selectedNode.getUserObject())) {
                        SalomeTMFContext
                            .getInstance()
                            .showMessage(
                                         Language
                                         .getInstance()
                                         .getText(
                                                  "Ce_nom_de_test_existe_deja_dans_cette_suite_"),
                                         Language.getInstance().getText(
                                                                        "Erreur_"),
                                         JOptionPane.ERROR_MESSAGE);

                        /*
                         * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance
                         * ().getSalomeFrame(),Language.getInstance().getText(
                         * "Ce_nom_de_test_existe_deja_dans_cette_suite_"),
                         * Language.getInstance().getText("Erreur_"),
                         * JOptionPane.ERROR_MESSAGE);
                         */
                    }
                } else if (selectedNode.getUserObject() instanceof TestList) {
                    if (!currentProject.containsTestListInModel(
                                                                ((TestList) selectedNode.getUserObject())
                                                                .getFamilyFromModel(), askName.getResult())
                        && !TestList.isInBase(((TestList) selectedNode
                                               .getUserObject()).getFamilyFromModel(),
                                              askName.getResult())) {

                        try {
                            ((TestList) selectedNode.getUserObject())
                                .updateInDBAndModel(askName.getResult(),
                                                    ((TestList) selectedNode
                                                     .getUserObject())
                                                    .getDescriptionFromModel());

                            selectedNode.setUserObject(selectedNode
                                                       .getUserObject());
                            SalomeTMFPanels.getTestDynamicTree().refreshNode(
                                                                             selectedNode);
                            // SalomeTMFPanels.getTestDynamicTree().repaint();
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else if (currentProject.getTestListFromModel(
                                                                   ((TestList) selectedNode.getUserObject())
                                                                   .getFamilyFromModel().getNameFromModel(),
                                                                   askName.getResult()) == null
                               || !currentProject.getTestListFromModel(
                                                                       ((TestList) selectedNode.getUserObject())
                                                                       .getFamilyFromModel()
                                                                       .getNameFromModel(),
                                                                       askName.getResult()).equals(
                                                                                                   selectedNode.getUserObject())) {
                        SalomeTMFContext
                            .getInstance()
                            .showMessage(
                                         Language
                                         .getInstance()
                                         .getText(
                                                  "Le_nom_de_la_suite_existe_deja_dans_cette_famille_"),
                                         Language.getInstance().getText(
                                                                        "Erreur_"),
                                         JOptionPane.ERROR_MESSAGE);

                        /*
                         * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance
                         * ().getSalomeFrame(),Language.getInstance().getText(
                         * "Le_nom_de_la_suite_existe_deja_dans_cette_famille_"
                         * ), Language.getInstance().getText("Erreur_"),
                         * JOptionPane.ERROR_MESSAGE);
                         */
                    }
                } else if (selectedNode.getUserObject() instanceof Family) {
                    if (!currentProject.containsFamilyInModel(askName
                                                              .getResult())
                        && !Family.isInBase(currentProject, askName
                                            .getResult())) {

                        try {
                            ((Family) selectedNode.getUserObject())
                                .updateInDBAndModel(askName.getResult(),
                                                    ((Family) selectedNode
                                                     .getUserObject())
                                                    .getDescriptionFromModel());

                            selectedNode.setUserObject(selectedNode
                                                       .getUserObject());
                            SalomeTMFPanels.getTestDynamicTree().refreshNode(
                                                                             selectedNode);
                            // SalomeTMFPanels.getTestDynamicTree().repaint();
                        } catch (Exception exception) {
                            Tools.ihmExceptionView(exception);
                        }
                    } else if (currentProject.getFamilyFromModel(askName
                                                                 .getResult()) == null
                               || !currentProject.getFamilyFromModel(
                                                                     askName.getResult()).equals(
                                                                                                 selectedNode.getUserObject())) {
                        SalomeTMFContext.getInstance().showMessage(
                                                                   Language.getInstance().getText(
                                                                                                  "Cette_famille_existe_deja_"),
                                                                   Language.getInstance().getText("Erreur_"),
                                                                   JOptionPane.ERROR_MESSAGE);

                        /*
                         * JOptionPane.showMessageDialog(SalomeTMFContext.getInstance
                         * ().getSalomeFrame(),Language.getInstance().getText(
                         * "Cette_famille_existe_deja_"),
                         * Language.getInstance().getText("Erreur_"),
                         * JOptionPane.ERROR_MESSAGE);
                         */

                    }
                }
            }
        }

    } // Fin de la methode renameTest/0

    // //////////////////////////////////// Divers
    // ///////////////////////////////////////////////

    public static void initTestScript(AutomaticTest test) {
        // Util.debug("Init Test Script");
        AutomaticTestScriptView.setTest(test);

    }

    static void afterRefresh() {
        SalomeTMFPanels.getAddTestInCampagne().setEnabled(false);
        SalomeTMFPanels.getDelCampagne().setEnabled(false);
        SalomeTMFPanels.getRenameCampaignButton().setEnabled(false);
        SalomeTMFPanels.getRenameTestButton().setEnabled(false);
        SalomeTMFPanels.getDelTestOrTestList().setEnabled(false);
        SalomeTMFPanels.resetTreeSelection();
    }

    static public boolean isTestExecuted(Test pTest) {
        boolean executed = false;
        if (pTest == null) {
            return executed;
        }
        ArrayList campaignList = currentProject.getCampaignOfTest(pTest);
        if (campaignList != null && campaignList.size() > 0) {
            int i = 0;
            int size = campaignList.size();
            while (i < size && !executed) {
                Campaign pCampaign = (Campaign) campaignList.get(i);
                if (pCampaign.containsExecutionResultInModel()) {
                    executed = true;
                }
                i++;
            }
        }
        return executed;
    }

    /************************ HyperLink *****************/
    static public void view(SimpleData data) {
        if (data == null) {
            return;
        }
        if (data instanceof Test) {
            Test pTest = (Test) data;
            DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
            DefaultMutableTreeNode node = pDynamicTree.findRemoveTestNode(pTest
                                                                          .getNameFromModel(), pTest.getTestListFromModel()
                                                                          .getNameFromModel(), pTest.getTestListFromModel()
                                                                          .getFamilyFromModel().getNameFromModel(), false);
            if (node != null) {
                // pReqTree.refreshNode(node);
                JTabbedPane tabs = (JTabbedPane) SalomeTMFContext.getInstance()
                    .getUIComponent(UICompCst.MAIN_TABBED_PANE);
                tabs.setSelectedIndex(0);
                pDynamicTree.getTree().setSelectionPath(
                                                        new TreePath(node.getPath()));

            }
        } else if (data instanceof TestList) {
            TestList pTestList = (TestList) data;
            DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
            DefaultMutableTreeNode node = pDynamicTree.findRemoveTestListNode(
                                                                              pTestList.getNameFromModel(), pTestList
                                                                              .getFamilyFromModel().getNameFromModel(), false);
            if (node != null) {
                // pReqTree.refreshNode(node);
                JTabbedPane tabs = (JTabbedPane) SalomeTMFContext.getInstance()
                    .getUIComponent(UICompCst.MAIN_TABBED_PANE);
                tabs.setSelectedIndex(0);
                pDynamicTree.getTree().setSelectionPath(
                                                        new TreePath(node.getPath()));

            }
        } else if (data instanceof Family) {
            Family pFamily = (Family) data;
            DynamicTree pDynamicTree = SalomeTMFPanels.getTestDynamicTree();
            DefaultMutableTreeNode node = pDynamicTree.findRemoveFamilyNode(
                                                                            pFamily.getNameFromModel(), false);
            if (node != null) {
                // pReqTree.refreshNode(node);
                JTabbedPane tabs = (JTabbedPane) SalomeTMFContext.getInstance()
                    .getUIComponent(UICompCst.MAIN_TABBED_PANE);
                tabs.setSelectedIndex(0);
                pDynamicTree.getTree().setSelectionPath(
                                                        new TreePath(node.getPath()));

            }
        } else if (data instanceof Campaign) {
            Campaign pCamp = (Campaign) data;
            DynamicTree pDynamicTree = SalomeTMFPanels.getCampaignDynamicTree();
            DefaultMutableTreeNode node = pDynamicTree.findRemoveCampaignNode(
                                                                              pCamp.getNameFromModel(), false);
            if (node != null) {
                // pReqTree.refreshNode(node);
                JTabbedPane tabs = (JTabbedPane) SalomeTMFContext.getInstance()
                    .getUIComponent(UICompCst.MAIN_TABBED_PANE);
                tabs.setSelectedIndex(1);
                pDynamicTree.getTree().setSelectionPath(
                                                        new TreePath(node.getPath()));
            }
        }
    }

    static public void assignCampaignTo(SimpleData data, User user) {
        if (data == null || user == null || currentCampaign == null) {
            return;
        }
        try {
            if (data instanceof Test) {
                Test pTest = (Test) data;
                currentCampaign.updateTestAssignationInDBAndModel(pTest
                                                                  .getIdBdd(), user.getIdBdd());
            } else if (data instanceof TestList) {
                TestList pTestList = (TestList) data;
                currentCampaign.updateSuiteAssignationInDBAndModel(pTestList
                                                                   .getIdBdd(), user.getIdBdd());
            } else if (data instanceof Family) {
                Family pFamily = (Family) data;
                currentCampaign.updateFamilyAssignationInDBAndModel(pFamily
                                                                    .getIdBdd(), user.getIdBdd());
            } else if (data instanceof Campaign) {
                Campaign pCamp = (Campaign) data;
                pCamp.updateCampaignAssignationInDBAndModel(user.getIdBdd());
            }
        } catch (Exception e) {
            Util.err(e);
        }
    }

    // //////////// Pas utilise ????? ////////////////////////////
    /*
     * public static void refreshEnvironmentAndParameter() { if
     * (ConnectionData.isConnected()) { try { // DATA
     * pDataLoader.reloadEnvironmentAndParameterData();
     *
     * // IHM getDataModification().clear();
     * SalomeTMFPanels.getDataMultiUserChangeListenerPanel().reset(); } catch
     * (Exception exception) { Tools.ihmExceptionView(exception.toString()); }
     *
     * } else { JOptionPane.showMessageDialog(applet,
     * Language.getInstance().getText
     * ("Impossible__Vous_n_etes_pas_connecte_a_la_base"),
     * Language.getInstance().getText("Erreur_"), JOptionPane.ERROR_MESSAGE); }
     * } // Fin de la m?thode refreshEnvironmentAndParameter/0
     */

} // Fin de la classe TestData

