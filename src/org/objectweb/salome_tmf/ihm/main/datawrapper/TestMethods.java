/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main.datawrapper;

import java.awt.Component;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Script;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.AutomaticExecution;
import org.objectweb.salome_tmf.ihm.main.ExecutionView;
import org.objectweb.salome_tmf.ihm.main.ManualExecution;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.models.ScriptExecutionException;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.plugins.core.ScriptEngine;
import org.objectweb.salome_tmf.plugins.core.TestDriver;



public class TestMethods implements ApiConstants, DataConstants {
    
    //  static CallScript c = null;
    static ScriptEngine pEngine = null;
    static TestDriver driver = null;
    
   
    public static String notValuedParamListCreation(HashSet setOfParam, Environment env, DataSet dataSet, ArrayList notValuedParamList) {
        String message = "";
        for (Iterator iter = setOfParam.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            if (!env.containsParameterInModel(param) && !dataSet.getParametersHashMapFromModel().containsKey(param.getNameFromModel())) {
                notValuedParamList.add(param);
                message = message + "* " + param.getNameFromModel() + "\n";
            } else if (!env.containsParameterInModel(param) && dataSet.getParametersHashMapFromModel().containsKey(param.getNameFromModel()) && dataSet.getParameterValueFromModel(param.getNameFromModel()).equals(DataConstants.PARAM_VALUE_FROM_ENV)){
                notValuedParamList.add(param);
                message = message + "* " + param.getNameFromModel() + "\n";
            }
        }
        return message;
    }
    
    public static String notValuedParamListInDataSet(HashSet setOfParam, DataSet dataSet, ArrayList notValuedParamList) {
        String message = "";
        for (Iterator iter = setOfParam.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            if (!dataSet.getParametersHashMapFromModel().containsKey(param.getNameFromModel())) {
                notValuedParamList.add(param);
                message = message + "* " + param.getNameFromModel() + "\n";
            }
        }
        return message;
    }
   
    /**
     * M?thode pour appeler un script bsh
     * @param args les param?tres du script
     * @param script le script
     */
    public static String callScript(HashMap params, Script script, AutomaticTest test, Campaign campaign, Project project, Environment env, ExecutionResult execResult, Execution exec, ExecutionTestResult execTestResult)  throws ScriptExecutionException {
        String result = null;
        File file = null;
        String scriptFile = "";
        
        try {
            if (Api.isConnected()) {
                try {
                    if (script.getTypeFromModel().equals(POST_SCRIPT)) {
                        //file = exec.getExecScriptFromDB(script.getNameFromModel(),POST_SCRIPT);
                        file = exec.getExecScriptFromDB(POST_SCRIPT);
                    } else if (script.getTypeFromModel().equals(PRE_SCRIPT)) {
                        //file = exec.getExecScriptFromDB(script.getNameFromModel(), PRE_SCRIPT);
                        file = exec.getExecScriptFromDB(PRE_SCRIPT);
                    } else if (script.getTypeFromModel().equals(INIT_SCRIPT)) {
                        //file = env.getEnvScriptFromDB(script.getNameFromModel());
                        file = env.getEnvScriptFromDB();
                    } else {
                        //file = test.getTestScriptFromDB(script.getNameFromModel());
                        file = test.getTestScriptFromDB();
                    }
                    //c = new CallScript(Tools.speedpurge(file.getAbsolutePath()));
                    scriptFile = Tools.speedpurge(file.getAbsolutePath());
                } catch (Exception exception) {
                    Tools.ihmExceptionView(exception);
                }
                
            } else {
                //c = new CallScript(Tools.speedpurge(script.getName()));
                scriptFile = Tools.speedpurge(script.getNameFromModel());
            }
        }  catch (Exception e) {
            Util.err(e);
            if (script.getTypeFromModel().equals(TEST_SCRIPT)) {
                result = UNKNOWN;
                fileCreationAndAttachToExecResult(execResult, execTestResult, e.toString(), exec, Language.getInstance().getText("erreur_exec_") + test.getNameFromModel());
            } else {
                throw new ScriptExecutionException(e);
            }
            return result;
        }
        
        // Hashtable pParams = new Hashtable(params);
        Hashtable pParams = getParamHashtable(params, exec, script.getTypeFromModel());
        //if (script.getClassPath() != null) {
        //      obj.eval("addClassPath(\""+ Tools.speedpurge(script.getClassPath()) +"\")");
        //}
        pParams.put("date", new Date(Calendar.getInstance().getTimeInMillis()));
        pParams.put("time", new Time(Calendar.getInstance().getTimeInMillis()));
        //On ajoute une r?f?rence sur l'obejt de r?sultat de test
        
        //obj.eval("setAccessibility(true)"); // turn off access restrictions
        pParams.put("salome_projectName", project.getNameFromModel());
        pParams.put("salome_ProjectObject", project);
        pParams.put("salome_debug", new Boolean(false));
        if (execResult != null){
            pParams.put("salome_ExecResultObject", execResult);
        }
        if (execTestResult != null){
            pParams.put("salome_ExecTestResultObject", execTestResult);
        }
        if (campaign != null){
            pParams.put("salome_CampagneName", campaign.getNameFromModel());
            pParams.put("salome_CampagneObject", campaign);
        } else {
            pParams.put("salome_CampagneName", "");
        }
        if (env != null){
            pParams.put("salome_environmentName", env.getNameFromModel()); // Add MM
            pParams.put("salome_environmentObject", env); // Add MM
        } else {
            pParams.put("salome_environmentName", ""); // Add MM
        }
        if (exec != null) {
            pParams.put("salome_ExecName",exec.getNameFromModel()); // Add MM
            pParams.put("salome_ExecObject", exec); // Add MM
        } else {
            pParams.put("salome_ExecName","");
        }
        if (test != null) {
            pParams.put("salome_TestName", test.getNameFromModel());
            pParams.put("salome_TestObject", test);
            if (test.getTestListFromModel() != null) {
                pParams.put("salome_SuiteTestName", test.getTestListFromModel().getNameFromModel());
                pParams.put("salome_SuiteTestObject", test.getTestListFromModel());
            } else {
                pParams.put("salome_SuiteTestName", "");
            }
            if (test.getTestListFromModel().getFamilyFromModel() != null) {
                pParams.put("salome_FamilyName", test.getTestListFromModel().getFamilyFromModel().getNameFromModel());
                pParams.put("salome_FamilyObject", test.getTestListFromModel().getFamilyFromModel());
            } else {
                pParams.put("salome_FamilyName", "");
            }
        } else {
            pParams.put("salome_TestName", "");
            pParams.put("salome_SuiteTestName", "");
        }
        
        pParams.put("testLog","");
        pParams.put("Verdict", "");
        
        // valuation des parametres de tests
        if (params != null) {
            Set keysSet = params.keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                String paramName;
                Object o = iter.next();
                if (o instanceof SimpleData){
                    paramName = ((SimpleData)o).getNameFromModel();
                } else {
                    paramName = (String)o;
                }
                Object val = params.get(o);
                Object exitval = pParams.get(paramName);
                
                if (exitval == null){
                    pParams.put(paramName, params.get(o));
                    Util.log("[callScript] " +Language.getInstance().getText("valeur_de_")+  paramName + " = " + val);
                } else {
                    Util.log("[callScript] " +Language.getInstance().getText("valeur_de_")+  paramName + " = " + exitval);
                }
                
                
            }
        }
        
        // Lancement du script
        int plugScriptType = -1;
        //Script pScript = null;
        pEngine = null;
        driver = null;
        String plugArg = "";
        String log;
        int status = -3;
        try  {
            if (script.getTypeFromModel().equals(PRE_SCRIPT)) {
                //script = exec.getInitScript();
                pEngine = script.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(script. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().getUrlBase(), SalomeTMFContext.getInstance().getPluginManager());
                plugScriptType = ScriptEngine.PRE_SCRIPT;
                plugArg = script.getPlugArgFromModel();
            } else if (script.getTypeFromModel().equals(POST_SCRIPT)) {
                //script = exec.getPostScript();
                pEngine = script.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(script. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().getUrlBase(), SalomeTMFContext.getInstance().getPluginManager());
                plugScriptType = ScriptEngine.POST_SCRIPT;
                plugArg = script.getPlugArgFromModel();
            } else if (script.getTypeFromModel().equals(INIT_SCRIPT)) {
                //script = exec.getPostScript();
                pEngine = script.getScriptEngine((Extension)SalomeTMFContext.getInstance().associatedExtension.get(script. getScriptExtensionFromModel() ), SalomeTMFContext.getInstance().getUrlBase(), SalomeTMFContext.getInstance().getPluginManager());
                plugScriptType = ScriptEngine.ENV_SCRIPT;
                plugArg = script.getPlugArgFromModel();
            } else {
                driver = test.ActivateExtention((Extension)SalomeTMFContext.getInstance().associatedExtension.get(test.getExtensionFromModel() ), SalomeTMFContext.getInstance().getUrlBase(), SalomeTMFContext.getInstance().getPluginManager());
                plugArg = test.getScriptFromModel().getPlugArgFromModel();
            }
            
           
            if (driver != null) {
                status = driver.runTest(scriptFile, test, pParams, plugArg );
                log = driver.getTestLog();
                // Erreur; -1 : Inconclusif; 2 : Fail; 1 : Pass 0
                if (status == -1){
                    result = FAIL;
                    //execResult.addFailInModel2(1);
                    if (log == null) {
                        log = "unknow";
                    }
                    fileCreationAndAttachToExecResult(execResult, execTestResult, log, exec, Language.getInstance().getText("erreur_exec_") + test.getNameFromModel());
                } else  if (status == 0){
                    result = SUCCESS;
                    //execResult.addSuccessInModel2(1);
                } if (status == 1){
                    result = FAIL;
                    //execResult.addFailInModel2(1);
                } if (status == 2){
                    result = UNKNOWN;
                    //execResult.addUnknowInModel2(1);
                } if (status == 3){
                    result = NOTAPPLICABLE;
                    //execResult.addUnknowInModel2(1);
                } if (status == 4){
                    result = BLOCKED;
                    //execResult.addUnknowInModel2(1);
                } if (status == 5){
                    result = NONE;
                    //execResult.addUnknowInModel2(1);
                }
                if (log != null && !log.equals("")){
                    fileCreationAndAttachToExecResult(execResult, execTestResult, log, exec, "log_exec_" + test.getNameFromModel());
                }
            } else if (pEngine != null) {
                try {
                    status = pEngine.runScript(plugScriptType, scriptFile, script, pParams, plugArg);
                } catch(Exception e){
                    Util.err(e);
                    status = -1;
                }
                //log = pEngine.
                if (status != 0) {
                    throw new ScriptExecutionException(new Exception(pEngine.getScriptLog()));
                }
                
            } else throw new Exception("No plugin foud");
        } catch (Exception ePlug) {
            Util.err(ePlug);
            log = ePlug.toString();
            if (driver != null) {
                if (status == -3) {
                    result = FAIL;
                    //execResult.addFailInModel2(1);
                }
                fileCreationAndAttachToExecResult(execResult, execTestResult, log, exec, Language.getInstance().getText("erreur_exec_") + test.getNameFromModel());
            } else {
                if (status == -3) {
                    result = UNKNOWN;
                    //execResult.addUnknowInModel2(1);
                }
                throw new ScriptExecutionException(ePlug);
            }
        }
        Util.log("[callScript] stop");
        //if (file != null) file.delete();
        
        return result;
    }
    
    public static void fileCreationAndAttachToExecResult(ExecutionResult execResult, ExecutionTestResult execTestResult, String text, Execution exec, String fileName) {
        
        Properties sys = System.getProperties();
        String tempDir = sys.getProperty("java.io.tmpdir");
        String fs = sys.getProperty("file.separator");
        tempDir = tempDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP;
        int num = 0;
        String pFileName = tempDir + fs + fileName + ".txt";
        File errorFile = new File(pFileName);
        while (errorFile.exists()) {
            pFileName = tempDir + fs + fileName + num + ".txt";
            errorFile = new File(pFileName);
            num++;
            //errorFile.delete();
        }
        try {
            errorFile.createNewFile();
            
            //BufferedWriter a besoin d un FileWriter,
            //les 2 vont ensemble, on donne comme argument le nom du fichier
            //true signifie qu on ajoute dans le fichier (append), on ne marque pas par dessus
            FileWriter fw = new FileWriter(pFileName, true);
            
            // le BufferedWriter output auquel on donne comme argument le FileWriter fw cree juste au dessus
            BufferedWriter output = new BufferedWriter(fw);
            
            
            //on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
            output.write(text);
            //on peut utiliser plusieurs fois methode write
            
            output.flush();
            //ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
            
            output.close();
            //et on le ferme
            
        }
        catch(IOException ioe){
            Util.err(ioe);
            Util.log(Language.getInstance().getText("erreur_lors_de_la_creation_du_fichier_d_erreur_") + ioe );
            //on "catch" l exception ici si il y en a une, et on l affiche sur la console
            return;
        }
        
        if (execTestResult == null && execResult != null) {
            execResult.addAttchmentInModel(errorFile);
            //execResult.setAttachmentMap(attachMap);
        } else if (execTestResult != null){
            //execTestResult.setAttachmentMap(attachMap);
            execTestResult.addAttchmentInModel(errorFile);
        }
        
        
    }
    /**
     * @return
     */
    public static void annulScript() {
        try {
            if (driver != null){
                driver.stopTest();
            } else if (pEngine != null){
                pEngine.stopScript();
            }
        } catch (Exception e){
            Util.err(e);
        }
    }
    
    /**
     * Reprise d'une execution
     * @param exec
     * @param finalExecResult
     * @param pComponent
     * @param stopOnError
     */
    public static void continueExecution(Execution exec, ExecutionResult finalExecResult, Component pComponent, boolean stopOnError, boolean onlyNotExec, boolean onlyAssigned,  boolean forModif){
        //Campaign pCamp = DataModel.getCurrentCampaign();
        Campaign pCamp = exec.getCampagneFromModel();
        if (finalExecResult.getTestsResultMapFromModel().size() != pCamp.getTestListFromModel().size()){
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return;
        }
        finalExecResult.setExecutionDateInModel(new Date(Calendar.getInstance().getTimeInMillis()));
        finalExecResult.setTesterInModel(DataModel.currentUser.getLastNameFromModel() + " " + DataModel.currentUser.getFirstNameFromModel());
        
        Vector pAllTestToExecute = new Vector();
        //Liste des tests groupes en sous-listes (manuels/automatiques)
        ArrayList splittedListOfTest = Tools.getListOfTestManualOrAutomatic(pCamp.getTestListFromModel(),   finalExecResult, pAllTestToExecute, true, onlyNotExec, onlyAssigned,  forModif);
        Vector listExec = new Vector();
        // On parcourt la liste des sous-listes
        try {
            for (int h =0; h < splittedListOfTest.size(); h++) {
                if (((ArrayList)splittedListOfTest.get(h)).get(0) instanceof ManualTest) {
                    listExec.add(new ManualExecution((ArrayList)splittedListOfTest.get(h), exec,  finalExecResult, true));
                        
                } else {
                    //Sous liste de tests automatiques
                    ArrayList automaticTestList = (ArrayList)splittedListOfTest.get(h);
                    listExec.add(new AutomaticExecution(automaticTestList, exec, finalExecResult));
                }
            }
        } catch (Exception e){
            Util.err(e);
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
            return;
        }
        //new callExecThread(listExec, exec, splittedListOfTest, selectedRowIndex, attachToBeSuppress, false, finalExecResult, null, pAllTestToExecute.toArray()).start();
        if (listExec.size() > 0){
            new CallExecThread(listExec, exec, splittedListOfTest, false, finalExecResult, null, pAllTestToExecute.toArray(), pComponent, stopOnError, false).start();
        } else {
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("aucun_test_assigne"),
                                                       Language.getInstance().getText("Attention_"),
                                                       JOptionPane.WARNING_MESSAGE);
            return;
        }
    }
    
    
    /**
     * Lancement des executions  
     * @param pListExec
     * @param pComponent
     * @param stopOnError
     */
    public static void runExecution(ArrayList pListExec, Component pComponent, boolean stopOnError, boolean onlyAssigned) {
        // Campaign pCapaign = exec.getCampagne();
        Execution pExec;
        Campaign pCapaign;
        // il doit y avoir au moins une execution selectionnee
        int nbExec = pListExec.size();
        if (nbExec > 0) {
            String message = "";
            for (int i = 0; i < nbExec; i++) { // FOR 1
                HashSet setOfParam = new HashSet();
                pExec = (Execution) pListExec.get(i);
                pCapaign = pExec.getCampagneFromModel();
                // Recuperation de tous les parametres de la campagne
                for (int k = 0; k < pCapaign.getTestListFromModel().size(); k++) {
                    Test test = (Test) pCapaign.getTestListFromModel().get(k);
                    for (int j = 0; j < test.getParameterListFromModel().size(); j++) {
                        setOfParam.add(test.getParameterListFromModel().get(j));
                    }
                }
                // On verifie que tous les parametres ont une valuation;
                ArrayList notValuedParamList = new ArrayList();
                message = TestMethods.notValuedParamListInDataSet(setOfParam,
                                                                  pExec.getDataSetFromModel(), notValuedParamList);
                if (notValuedParamList.size() > 0) {
                    message = Language.getInstance().getText(
                                                             "Les_parametres_")
                        + message
                        + Language.getInstance()
                        .getText("de_l_execution_")
                        + pExec.getNameFromModel()
                        + Language.getInstance().getText(
                                                         "_ne_sont_pas_values");
                }
            }

            if (!message.equals("") && pComponent != null) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Attention_")
                                                           + message,
                                                           Language.getInstance().getText("Attention_"),
                                                           JOptionPane.INFORMATION_MESSAGE);
                /*
                 * JOptionPane.showMessageDialog(pComponent,
                 * Language.getInstance().getText("Attention_") + message,
                 * Language.getInstance().getText("Attention_"),
                 * JOptionPane.INFORMATION_MESSAGE);
                 */
            }

            CallExecThread precCallExecThread = null;
            String listres = "";
            // on parcourt la liste des executions selectionnees
            for (int i = nbExec - 1; i >= 0; i--) { // FOR 1
                // Execution courante
                pExec = (Execution) pListExec.get(i);
                pCapaign = pExec.getCampagneFromModel();
                Util.log(">>>>EXEC LANCEE = " + pExec.getNameFromModel());
                // R?sultat d'ex?cution final
                String prefix = "";
                if (onlyAssigned) {
                    prefix = "user-";
                }

                ExecutionResult finalExecResult = new ExecutionResult(prefix
                                                                      + pExec.getNameFromModel() + "_"
                                                                      + pExec.getExecutionResultListFromModel().size(), "",
                                                                      pExec);
                listres += finalExecResult.getNameFromModel() + ", ";
                /*
                 * finalExecResult.setNumberOfFailInModel(0);
                 * finalExecResult.setNumberOfSuccessInModel(0);
                 * finalExecResult.setNumberOfUnknowInModel(0);
                 */
                // finalExecResult.initStatus();
                finalExecResult.setExecutionDateInModel(new Date(
                                                                 Calendar.getInstance().getTimeInMillis()));
                finalExecResult.setTesterInModel(DataModel.currentUser
                                                 .getLastNameFromModel()
                                                 + " " + DataModel.currentUser.getFirstNameFromModel());

                Tools.initExecutionResultMap(pCapaign.getTestListFromModel(),
                                             finalExecResult, pCapaign);
                boolean continueExec = false;
                boolean noError = true;
                //                              if (onlyAssigned) {
                // create execResultINDB and continue execution
                finalExecResult.setExecutionStatusInModel(INCOMPLETED);
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(110,
                                                       ApiConstants.INSERT_EXECUTION_RESULT);
                    pExec.addExecutionResultInDB(finalExecResult,
                                                 DataModel.currentUser);
                    ArrayList list = pCapaign.getTestListFromModel();
                    for (int j = 0; j < list.size(); j++) {
                        Test pTest = (Test) list.get(j);
                        finalExecResult.addExecTestResultInDB(pTest);
                        if (pTest instanceof ManualTest) {
                            ArrayList actionList = ((ManualTest) pTest)
                                .getActionListFromModel(false);
                            for (int k = 0; k < actionList.size(); k++) {
                                Action act = (Action) actionList.get(k);
                                finalExecResult.addExecActionResultInDB(
                                                                        pTest, act);
                            }
                        }
                    }
                    Api.commitTrans(transNumber);
                    continueExec = true;
                } catch (Exception e) {
                    noError = false;
                    Util.err(e);
                    message = e.getMessage();
                    Api.forceRollBackTrans(transNumber);
                }
                //                              }
                if (noError) {
                    Vector pAllTestToExecute = new Vector();
                    ArrayList splittedListOfTest;
                    if (onlyAssigned) {
                        splittedListOfTest = Tools
                            .getListOfTestManualOrAutomatic(pCapaign
                                                            .getTestListFromModel(),
                                                            finalExecResult, pAllTestToExecute,
                                                            continueExec, false, onlyAssigned,
                                                            false);
                    } else {
                        splittedListOfTest = Tools
                            .getListOfTestManualOrAutomatic(pCapaign
                                                            .getTestListFromModel(), null,
                                                            pAllTestToExecute, continueExec, false,
                                                            onlyAssigned, false);
                    }
                    Vector listExec = new Vector();
                    // On parcourt la liste des sous-listes
                    for (int h = 0; h < splittedListOfTest.size(); h++) {
                        if (((ArrayList) splittedListOfTest.get(h)).get(0) instanceof ManualTest) {
                            // Sous liste de tests manuels
                            listExec.add(new ManualExecution(
                                                             (ArrayList) splittedListOfTest.get(h),
                                                             pExec, finalExecResult, false));

                        } else {
                            // Sous liste de tests automatiques
                            ArrayList automaticTestList = (ArrayList) splittedListOfTest
                                .get(h);
                            listExec.add(new AutomaticExecution(
                                                                automaticTestList, pExec, finalExecResult));
                        }
                    }

                    precCallExecThread = new CallExecThread(listExec, pExec,
                                                            splittedListOfTest, true, finalExecResult,
                                                            precCallExecThread, pAllTestToExecute.toArray(),
                                                            pComponent, stopOnError, onlyAssigned);
                }

                // } // FIN FOR 1 ?
                if (noError) {
                    if (onlyAssigned) {
                        SalomeTMFContext.getInstance().showMessage(
                                                                   Language.getInstance().getText(
                                                                                                  "Messages_para_exec1")
                                                                   + "\n"
                                                                   + listres
                                                                   + "\n"
                                                                   + Language.getInstance().getText(
                                                                                                    "Messages_para_exec2"),
                                                                   Language.getInstance().getText("Attention_"),
                                                                   JOptionPane.INFORMATION_MESSAGE);
                        // Update IHM
                        ((ExecutionView) pComponent).updateIHMTable(true,
                                                                    pExec, finalExecResult);
                        pExec.addExecutionResultInModel(finalExecResult);
                    } else {
                        precCallExecThread.start();
                    }
                } else {
                    SalomeTMFContext.getInstance().showMessage(
                                                               Language.getInstance().getText("Insert_data") + "\n" + message,
                                                               Language.getInstance().getText("Erreur_"),
                                                               JOptionPane.ERROR_MESSAGE);
                }
            } // FIN FOR 1
        } else {
            if (pComponent != null) {
                SalomeTMFContext.getInstance().showMessage(
                                                           Language.getInstance().getText("Vous_devez_selectionner_au_moins_une_execution"),
                                                           Language.getInstance().getText("Attention_"),
                                                           JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    static Hashtable getParamHashtable(HashMap map, Execution ptrExec, String type){
        if (type.equals(INIT_SCRIPT)){
            return new Hashtable(map);
        }
        Environment ptrEnv = ptrExec.getEnvironmentFromModel();
        DataSet dataSet =  ptrExec.getDataSetFromModel();
        Hashtable valuedParam = new Hashtable();
        if (dataSet !=null) {
            Set dataSetKeysSet = dataSet.getParametersHashMapFromModel().keySet();
            for (Iterator iter = dataSetKeysSet.iterator(); iter.hasNext();) {
                String paramName = (String)iter.next();
                Parameter element = DataModel.getCurrentProject().getParameterFromModel(paramName);
                String value = dataSet.getParameterValueFromModel(element.getNameFromModel());
                if (value.startsWith(DataConstants.PARAM_VALUE_FROM_ENV)){
                    value = ptrEnv.getParameterValue(paramName);
                    if (value == null){
                        value = "";
                    }
                }
                valuedParam.put(paramName, value);
            }
        }
        return valuedParam;
    }
} // Fin de la classe TestMethods
