/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.main.datawrapper;

import java.awt.Component;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.JDialog;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.FileAttachment;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.UrlAttachment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.AutomaticExecution;
import org.objectweb.salome_tmf.ihm.main.ExecutionView;
import org.objectweb.salome_tmf.ihm.main.ManualExecution;
import org.objectweb.salome_tmf.ihm.main.ProgressBar;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.models.ScriptExecutionException;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.loggingData.LoggingData;

public class CallExecThread  extends Thread implements ApiConstants {
    
    final static Logger logger = Logger.getLogger(CallExecThread.class);
    
    Vector listeExec;
    //ArrayList execResultList;
    Execution exec;
    ArrayList splittedListOfTest;
    //int selectedRowIndex;
    //HashMap attachToBeSuppress;
    boolean newLaunch;
    ExecutionResult finalExecResult;
    CallExecThread execToLaunch;
    boolean stoped;
    Object[] ptrTestToBeExec = null;
        
    Campaign pCampaign;
    Component pComponent = null;
    boolean stopOnError;
    boolean onlySave;
        
    public CallExecThread(Vector listeExec, Execution exec, ArrayList splittedListOfTest,
                          boolean newLaunch,  ExecutionResult finalExecResult, 
                          CallExecThread execToLaunch, Object[] ptrTestToBeExec, Component pComponent,
                          boolean stopOnError, boolean onlySave) {
                
        pCampaign = exec.getCampagneFromModel();
        this.stopOnError = stopOnError;
        this.finalExecResult = finalExecResult;
        this.listeExec = listeExec;
        this.exec = exec;
        this.splittedListOfTest = splittedListOfTest;
        //this.selectedRowIndex = selectedRowIndex;
        //this.attachToBeSuppress = attachToBeSuppress;
        this.newLaunch = newLaunch;
        this.execToLaunch = execToLaunch;
        this.pComponent = pComponent;
        stoped = false;
        this.ptrTestToBeExec = ptrTestToBeExec;
        this.onlySave = onlySave;
    }
        
    @Override
    public void run(){
        // on lock l'arbre des tests
                
        try {
            Api.lockTestPlan();
        } catch(Exception e){
            Util.err("[CallExecThread->run] : Exception ", e);
        }
        if (!onlySave){
            Util.log("[CallExecThread] : start");
            AutomaticExecution  automaticExecution;
            ManualExecution manualExecution;
            preExec();
                
            while (!listeExec.isEmpty() && !stoped){
                        
                Util.log("[CallExecThread] exec un test ");
                Object o = listeExec.remove(0);
                if (o instanceof AutomaticExecution){
                    Util.log("[CallExecThread] Auto Test");
                    automaticExecution  = (AutomaticExecution)o;
                    automaticExecution.lauchAutomaticExecution();
                    Util.log("[CallExecThread] wait begin " + exec.getNameFromModel());
                    while (!automaticExecution.isFinished()){
                        //wait(500);
                        yield();
                        try {
                            Thread.sleep(500);
                        } catch (Exception e){
                                                
                        }
                    }
                    Util.log("[CallExecThread] wait end " + exec.getNameFromModel());
                    automaticExecution.setFinished(false);
                } else {
                    Util.log("[CallExecThread] Manual Test");
                    manualExecution = (ManualExecution) o;
                    manualExecution.lauchManualExecution();
                    Util.log("[CallExecThread] wait begin " + exec.getNameFromModel());
                    int i = 0;
                    while (!manualExecution.isFinished()){
                        i++;
                        //wait(500);
                        yield();
                        try {
                            Thread.sleep(500);
                            if (i<3) {
                                manualExecution.focusToEffectiveResult();
                            }
                        } catch (Exception e){
                                                        
                        }
                    }
                    Util.log("[CallExecThread] wait end " + exec.getNameFromModel());
                    manualExecution.setFinished(false);
                                
                }
                if (finalExecResult.getExecutionStatusFromModel() != null){
                    stoped = finalExecResult.getExecutionStatusFromModel().equals(INTERRUPT);
                }
            }
            Util.log("[CallExecThread] do postexec");
        }
        postexec();
    }
        
    public void do_stop() {
        stoped = true;
    }
        
    private void preExec() {
        Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
        // On lance le script de l'environnement
        if (exec.getEnvironmentFromModel().getInitScriptFromModel() != null) {
            ProgressBar pProgressBar = new ProgressBar("Env script execution", "", 2);
            pProgressBar.Show();
            try {
                HashMap map = new HashMap();
                map.putAll(exec.getEnvironmentFromModel().getParametersHashTableFromModel());
                pProgressBar.doTask("execute " + exec.getEnvironmentFromModel().getInitScriptFromModel().getNameFromModel());
                TestMethods.callScript(map, exec.getEnvironmentFromModel().getInitScriptFromModel(), null, pCampaign, DataModel.currentProject, exec.getEnvironmentFromModel(), finalExecResult, exec, null);
                pProgressBar.finishAllTask();
            } catch (ScriptExecutionException see) {
                pProgressBar.finishAllTask();
                Util.err(see);
                if (stopOnError) {
                    this.do_stop();
                } else {
                    int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                            Language.getInstance().getText("Le_script_d_initialisation_de_l_environnement_ne_s_est_pas_deroule_correctement_Souhaitez_vous_continuer_"),
                                                                            Language.getInstance().getText("Attention_"),
                                                                            JOptionPane.WARNING_MESSAGE,
                                                                            options);
                                        
                    /*int choice = JOptionPane.showOptionDialog(pComponent,
                      Language.getInstance().getText("Le_script_d_initialisation_de_l_environnement_ne_s_est_pas_deroule_correctement_Souhaitez_vous_continuer_"),
                      Language.getInstance().getText("Attention_"),
                      JOptionPane.YES_NO_OPTION,
                      JOptionPane.WARNING_MESSAGE,
                      null,
                      options,
                      options[1]);
                    */
                    if (choice == JOptionPane.NO_OPTION) {
                        try {
                            Api.unLockTestPlan();
                        } catch(Exception e){
                            Util.err("[CallExecThread->preExec] : Exception ", e);
                        }
                        this.do_stop();
                    }
                }
            }
                        
        }
        // On lance le script PRE_SCRIPT de l'ex?cution
        if (exec.getPreScriptFromModel() != null) {
            ProgressBar pProgressBar = new ProgressBar("Pre script execution", "", 2);
            pProgressBar.Show();
            try {
                pProgressBar.doTask("execute " + exec.getPreScriptFromModel().getNameFromModel());
                TestMethods.callScript(exec.getDataSetFromModel().getParametersHashMapFromModel(), exec.getPreScriptFromModel(), null, pCampaign, DataModel.getCurrentProject(), exec.getEnvironmentFromModel(), finalExecResult, exec, null);
                pProgressBar.finishAllTask();
            } catch (ScriptExecutionException see) {
                pProgressBar.finishAllTask();
                if (stopOnError) {
                    this.do_stop();
                } else {
                    int choice = SalomeTMFContext.getInstance().askQuestion(
                                                                            Language.getInstance().getText("Le_script_d_initialisation_de_l_execution_ne_s_est_pas_deroule_correctement_Souhaitez_vous_continuer_"),
                                                                            Language.getInstance().getText("Attention_"),
                                                                            JOptionPane.WARNING_MESSAGE,
                                                                            options);
                    /*int choice = JOptionPane.showOptionDialog(pComponent,
                      Language.getInstance().getText("Le_script_d_initialisation_de_l_execution_ne_s_est_pas_deroule_correctement_Souhaitez_vous_continuer_"),
                      Language.getInstance().getText("Attention_"),
                      JOptionPane.YES_NO_OPTION,
                      JOptionPane.WARNING_MESSAGE,
                      null,
                      options,
                      options[1]);
                    */
                    if (choice == JOptionPane.NO_OPTION) {
                        try {
                            Api.unLockTestPlan();
                        } catch(Exception e){
                            Util.err("[CallExecThread->preExec] : Exception ", e);
                        }
                                                
                        //this.stop();
                        this.do_stop();
                    }
                }       
            }
                        
        }
                
    }
        
        
    private void postexec() {
                
        /*finalExecResult.setNumberOfFailInModel(0);
          finalExecResult.setNumberOfSuccessInModel(0);
          finalExecResult.setNumberOfUnknowInModel(0);*/
        //finalExecResult.initStatus();
                
        if (!finalExecResult.allTestsHaveStatusInModel())
            finalExecResult.setExecutionStatusInModel(INCOMPLETED);
        else
            finalExecResult.setExecutionStatusInModel(FINISHED);
        ProgressBar pProgressBar = new ProgressBar("Data Saving", "", ptrTestToBeExec.length +2);
        int transNumber = -1;
        try {
            /* S */
            pProgressBar.Show();
            /* BdD */
            transNumber = Api.beginTransaction(110, ApiConstants.INSERT_EXECUTION_RESULT);
            boolean isOnlyAssigned = false;
            if (finalExecResult.isInBase()){
                if(newLaunch){
                    isOnlyAssigned = true;
                }
                newLaunch = false;
            } else {
                newLaunch = true;
            }
                        
            if (newLaunch) {
                pProgressBar.doTask("Save execution");
                //finalExecResult.addInDB(exec, DataModel.currentUser);
                exec.addExecutionResultInDB(finalExecResult, DataModel.currentUser);
            } else {
                pProgressBar.doTask("update execution");
                finalExecResult.updateInDB(exec, DataModel.currentUser);
            }
                        
            for (int index_test = 0; index_test < ptrTestToBeExec.length; index_test++){
                pProgressBar.doTask("save test result");
                Test testKey = (Test)ptrTestToBeExec[index_test];
                Util.log("[CallExecThread] Set status for : " + testKey.getNameFromModel());
                if (!newLaunch) {
                    finalExecResult.updateExecTestResultInDB(testKey);
                } else {
                    finalExecResult.addExecTestResultInDB(testKey);
                }
                if (testKey instanceof ManualTest) {
                    ArrayList actionList = ((ManualTest)testKey).getActionListFromModel(false);
                    for (int j =0; j < actionList.size(); j++) {
                        Action act = (Action)actionList.get(j);
                        if (newLaunch) {
                            finalExecResult.addExecActionResultInDB(testKey, act);
                        }
                        finalExecResult.updateEffectiveResForActionInDB(testKey,act);
                    }
                }
                Collection col = finalExecResult.getExecutionTestResultFromModel(testKey).getAttachmentMapFromModel().values();
                for (Iterator iterator = col.iterator(); iterator.hasNext();) {
                    Attachment attachToTest = (Attachment)iterator.next();
                    if (! attachToTest.isInBase()){
                        if (attachToTest instanceof FileAttachment) {
                            finalExecResult.addAttachFileForExecTestInDB(testKey.getIdBdd(), (FileAttachment)attachToTest);
                        } else {
                            finalExecResult.addAttachUrlForExecTestInDB(testKey.getIdBdd(), (UrlAttachment)attachToTest);
                        }
                    }
                }
                                
                Vector oldAttachFileToExecResult = finalExecResult.getAttachFilesFromDB();
                Vector oldAttachUrlToExecResult = finalExecResult.getAttachUrlsFromDB();
                                
                col = finalExecResult.getAttachmentMapFromModel().values();
                for (Iterator iterAttach = col.iterator(); iterAttach.hasNext();) {
                    Attachment attachToExecRes = (Attachment)iterAttach.next();
                    if (!attachToExecRes.isInBase()){
                        if (attachToExecRes instanceof FileAttachment) {
                            if (oldAttachFileToExecResult == null || !oldAttachFileToExecResult.contains(attachToExecRes.getNameFromModel())) {
                                finalExecResult.addAttachementInDB(attachToExecRes);
                            }
                        } else {
                            if (oldAttachUrlToExecResult == null || !oldAttachUrlToExecResult.contains(attachToExecRes.getNameFromModel())) {
                                finalExecResult.addAttachementInDB(attachToExecRes);
                            }
                        }
                    }
                }
            }
            pProgressBar.doTask("commit in database");
            exec.updateLastExecDateInDBAndModel(finalExecResult.getExecutionDateFromModel());
            Vector timeVector = exec.getResultsTimeFromDB();
            finalExecResult.setTimeInModel((Time)timeVector.get(timeVector.size()-1));
                        
            /* TEST COMIT/ROLBACK*/
            /*int exp = 0;
              if (exp ==0){
              throw new Exception();
              }*/
                        
            /*SalomeTMFContext.getInstance().showMessage(
              Language.getInstance().getText("Le_script_de_restitution_de_l_execution_ne_s_est_pas_deroule_correctement"),
              Language.getInstance().getText("Attention_"),
              JOptionPane.WARNING_MESSAGE);*/
            /*int exp = 0;
              if (exp ==0){
              throw new Exception();
              }*/
            Api.commitTrans(transNumber);
                        
                        
            /* EN Cas d'execution paralle*/
            try {
                if (finalExecResult.getNameFromModel().startsWith("user") && !onlySave) {
                    //finalExecResult.reloadFromDB(true);
                    String status = finalExecResult.getExecutionStatusFromModel();
                    finalExecResult.reloadTestResult();
                    if (!finalExecResult.allTestsHaveStatusInModel() ) {
                        finalExecResult.setExecutionStatusInModel(INCOMPLETED);
                        //Util.debug("INCOMPLETED");
                        finalExecResult.updateInDB(exec, DataModel.currentUser);
                    } else {
                        finalExecResult.setExecutionStatusInModel(FINISHED);
                        //Util.debug("FINISHED");
                        finalExecResult.updateInDB(exec, DataModel.currentUser);
                    }
                                        
                } else {
                    finalExecResult.reloadTestResult();
                }
            } catch (Exception  e1){
                Util.err(e1);
                // submit message to user
                String msg = "Error! " + e1.getMessage();
                JOptionPane optionPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
                JDialog dialog = optionPane.createDialog("Error!");
                dialog.setAlwaysOnTop(true); // to show top of all other application
                dialog.setVisible(true); // to visible the dialog
                // submit message to log4j (database)
                logger.info(LoggingData.getData() + msg);
            }
            /* EN Cas d'execution paralle*/
                        
            transNumber = -1;
            // IHM
            // On modifie l'affichage pour l'ex?cution sel?ctionn?e si besoin
            if (pComponent != null && pComponent instanceof ExecutionView) {
                boolean realNewLauch = newLaunch || isOnlyAssigned;
                //Util.debug("............." + newLaunch);
                ((ExecutionView)pComponent).updateIHMTable(realNewLauch, exec, finalExecResult);
                                
            } else {
                Util.log("[CallExecThread] : no IHM update  with  " + pComponent);
            }
            if (newLaunch || isOnlyAssigned) { 
                exec.addExecutionResultInModel(finalExecResult);
            }
                        
            // On modifie la date de derni?re ex?cution
            //exec.setLastDate(finalExecResult.getExecutionDate());
            pProgressBar.finishAllTask();            
        } catch (Exception exception) {
            // submit message to user
            String msg = "Error! " + exception.getMessage();
            JOptionPane optionPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
            JDialog dialog = optionPane.createDialog("Error!");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true); // to visible the dialog
            // submit message to log4j (database)
            logger.info(LoggingData.getData() + msg);
            
            Api.forceRollBackTrans(transNumber);
            pProgressBar.finishAllTask();
//            if (pComponent != null){
//                Tools.ihmExceptionView(exception);
//            }
                                
        }
                        
        // On lance le script POST_SCRIPT de l'ex?cution
        if (exec.getPostScriptFromModel() != null) {
            Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
            int choice = -1;
            try {
                if (stoped) {
                    choice = SalomeTMFContext.getInstance().askQuestion(
                                                                        Language.getInstance().getText("L_execution_est_stoppee_voulez_vous_executer_le_script_de_restitution_"),
                                                                        Language.getInstance().getText("Attention_"),
                                                                        JOptionPane.WARNING_MESSAGE,
                                                                        options);
                    /*choice = JOptionPane.showOptionDialog(pComponent,
                      Language.getInstance().getText("L_execution_est_stoppee_voulez_vous_executer_le_script_de_restitution_"),
                      Language.getInstance().getText("Attention_"),
                      JOptionPane.YES_NO_OPTION,
                      JOptionPane.WARNING_MESSAGE,
                      null,
                      options,
                      options[1]);
                    */
                    if (choice == JOptionPane.YES_OPTION) {
                        TestMethods.callScript(exec.getDataSetFromModel().getParametersHashMapFromModel(), exec.getPostScriptFromModel(), null, pCampaign, DataModel.currentProject, null, null, exec, null);
                    }
                } else {
                    pProgressBar = new ProgressBar("Post script execution", "", 2);
                    pProgressBar.Show();
                    pProgressBar.doTask("execute " + exec.getPostScriptFromModel().getNameFromModel());
                    TestMethods.callScript(exec.getDataSetFromModel().getParametersHashMapFromModel(), exec.getPostScriptFromModel(), null, pCampaign, DataModel.currentProject, null, null, exec, null);
                    pProgressBar.finishAllTask();
                }
            }
            catch (ScriptExecutionException see) {
                // submit message to user
                String msg = "Error! " + see.getMessage();
                JOptionPane optionPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
                JDialog dialog = optionPane.createDialog("Error!");
                dialog.setAlwaysOnTop(true); // to show top of all other application
                dialog.setVisible(true); // to visible the dialog
                // submit message to log4j (database)
                logger.info(LoggingData.getData() + msg);
                
                pProgressBar.finishAllTask();
//                if (pComponent != null) {
//                    SalomeTMFContext.getInstance().showMessage(
//                                                               Language.getInstance().getText("Le_script_de_restitution_de_l_execution_ne_s_est_pas_deroule_correctement"),
//                                                               Language.getInstance().getText("Attention_"),
//                                                               JOptionPane.WARNING_MESSAGE);
//                    /*JOptionPane.showMessageDialog(pComponent,
//                      Language.getInstance().getText("Le_script_de_restitution_de_l_execution_ne_s_est_pas_deroule_correctement"),
//                      Language.getInstance().getText("Attention_"),
//                      JOptionPane.WARNING_MESSAGE);*/
//                }
            }
        }
        // on unlock l'arbre des tests
        try {
            Api.unLockTestPlan();
        } catch(Exception e){
            // submit message to user
            String msg = "Error! " + e.getMessage();
            JOptionPane optionPane = new JOptionPane(msg, JOptionPane.ERROR_MESSAGE);
            JDialog dialog = optionPane.createDialog("Error!");
            dialog.setAlwaysOnTop(true); // to show top of all other application
            dialog.setVisible(true); // to visible the dialog
            // submit message to log4j (database)
            logger.info(LoggingData.getData() + msg);
            
            Util.err("[CallExecThread->postexec] : Exception ", e);
        }
                
        if (execToLaunch != null  && !stoped)
            execToLaunch.start();
    } // Fin de la methode postExec/0
}
