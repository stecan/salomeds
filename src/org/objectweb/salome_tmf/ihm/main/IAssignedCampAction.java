package org.objectweb.salome_tmf.ihm.main;

import org.objectweb.salome_tmf.data.Campaign;

public interface IAssignedCampAction {
    public void updateData(Campaign pCamp);
}
