/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.ihm.main.AttachmentViewWindow;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * @author teaml039
 */
public class AutomaticTestResultMouseListener extends MouseAdapter implements DataConstants {
    
    /**
     * Appel?e si on appuie sur un bouton de la souris
     * @param e un ?v?nement venant de la souris
     */
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getClickCount() > 1) {
            if (DataModel.getTestObservedInExecution() instanceof AutomaticTest) {
                //HashMap oldAttachMap = (HashMap)DataModel.getCurrentExecutionTestResult().getAttachmentMapFromModel().clone();
                HashMap oldAttachMap = DataModel.getCurrentExecutionTestResult().getCopyOfAttachmentMapFromModel();
                SalomeTMFContext.getInstance();
                //AttachmentViewWindow attachWindow = new AttachmentViewWindow(SalomeTMF.ptrSalomeTMF, EXECUTION_RESULT_TEST, DataModel.getCurrentExecutionTestResult(), DataModel.getObservedExecution(), DataModel.getObservedExecutionResult(), DataModel.getTestObservedInExecution());
                AttachmentViewWindow attachWindow = new AttachmentViewWindow(SalomeTMFContext.getBaseIHM(), EXECUTION_RESULT_TEST, DataModel.getCurrentExecutionTestResult());
                int transNumber= -1;
                if (!attachWindow.cancelPerformed){
                    try {
                        Util.log("[AutomaticTestResultMouseListener] Api.INSERT_ATTACHMENT");
                        transNumber = Api.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
                    
                        DataModel.getCurrentExecutionTestResult().updateAttachement(oldAttachMap);
                   
                        Api.commitTrans(transNumber);
                    } catch (Exception exception) {
                        Api.forceRollBackTrans(transNumber);
                        DataModel.getCurrentExecutionTestResult().setAttachmentMapInModel(oldAttachMap);
                        Tools.ihmExceptionView(exception);
                    }
                }
            }
        }
    } // Fin de la m?thode mousePressed/1
} // Fin de la classe AutomaticTestResultMouseListener
