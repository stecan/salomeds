package org.objectweb.salome_tmf.ihm.models;

 
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.View;
 
public class MyTextAreaRenderer extends JTextArea implements TableCellRenderer {

    private static int INIT_MAX_ROW_HEIGHT = 150;
        
    private static String doneRows = "";
    private final DefaultTableCellRenderer adaptee =
        new DefaultTableCellRenderer();

    public MyTextAreaRenderer() {
        super();
        setLineWrap(true);
        setWrapStyleWord(true);
        setAutoscrolls(false);
    }
        
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        adaptee.getTableCellRendererComponent(table, value,
                                              isSelected, hasFocus, row, column);
        setForeground(adaptee.getForeground());
        setBackground(adaptee.getBackground());
        setBorder(adaptee.getBorder());
        setFont(adaptee.getFont());
        setText(adaptee.getText());

        View v = getUI().getRootView(this);
        int preferredRowHeight = Math.round(v.getPreferredSpan(View.Y_AXIS));
        if (doneRows.lastIndexOf(""+row+column)==-1 && table.getRowHeight(row) < preferredRowHeight 
            && table.getRowHeight(row) < INIT_MAX_ROW_HEIGHT) {
            //                  Util.debug("row="+row+" | column="+column+" | doneRows="+doneRows+" | preferredRowHeight="+preferredRowHeight);
            table.setRowHeight(row, Math.min (preferredRowHeight, INIT_MAX_ROW_HEIGHT));
            doneRows = doneRows.concat(" "+row+column);
        }
        return this;
    }

    public static void reinitRows() {
        doneRows = "";
    }
}
