/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.Cursor;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.PopupMenuFactory;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

/**
 * @author teaml039
 */
/**
 * Classe repr?sentant un arbre modfiable dynamiquement
 *
 * @author teaml039
 * @version 0.1
 */
public class DynamicTree extends JPanel implements ApiConstants, DataConstants {

    /**
     * La racine de l'arbre
     */
    protected DefaultMutableTreeNode rootNode;

    /**
     * Mod?le de l'arbre
     */
    protected TestTreeModel treeModel;

    /**
     * L'arbre
     */
    protected JTree tree;

    /**
     *
     */
    // private Toolkit toolkit = Toolkit.getDefaultToolkit();

    /**
     * Le noeud selectionn?
     */
    private DefaultMutableTreeNode selectedNode;

    /**
     * Type de l'arbre "campagne" ou "suite"
     */
    private int treeType;

    String campaignName;

    String familyName;

    String testName;

    String testListName;

    SimpleData oldData = null;

    /**************************************************************************/
    /** CONSTRUCTEUR ***/
    /**************************************************************************/
    /**
     * Le constructeur de l'arbre
     */
    public DynamicTree(String rootName, int type) {
        super(new GridLayout(1, 0));

        // Le support graphique sur lequel est fond? l'arbre
        TreeRenderer renderer = new TreeRenderer();

        treeType = type;

        // la racine de l'arbre de test
        rootNode = new DefaultMutableTreeNode(rootName);

        tree = new JTree();
        treeModel = new TestTreeModel(rootNode, tree, null);
        tree.setModel(treeModel);
        // tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.setCellRenderer(renderer);
        tree.getSelectionModel().setSelectionMode(
                                                  TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.setShowsRootHandles(true);
        tree.setRowHeight(23);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
                @Override
                public void valueChanged(TreeSelectionEvent e) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree
                        .getLastSelectedPathComponent();
                    if (node == null) {
                        selectedNode = null;
                        return;
                    } else {
                        tree.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                        if (oldData != null) {
                            oldData.clearCache();
                        }
                        if (getTreeType() == CAMPAIGN) {
                            selectedNode = node;
                            if (node.getUserObject() instanceof Test) {
                                Test pTest = (Test) node.getUserObject();
                                oldData = pTest;
                                PopupMenuFactory.setRootCampAction(false);
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentTest(pTest);
                                DataModel.setCurrentTestList(pTest
                                                             .getTestListFromModel());
                                DataModel.setCurrentFamily(pTest
                                                           .getTestListFromModel()
                                                           .getFamilyFromModel());
                                DataModel
                                    .setCurrentCampaign((Campaign) ((DefaultMutableTreeNode) node
                                                                    .getParent().getParent()
                                                                    .getParent()).getUserObject());
                                if (Permission.canDeleteCamp()) {
                                    SalomeTMFPanels.getDelCampagne().setEnabled(
                                                                                true);
                                    PopupMenuFactory.getDelCampMenuItem()
                                        .setEnabled(true);
                                }
                                SalomeTMFPanels.getAddTestInCampagne().setEnabled(
                                                                                  false);
                                PopupMenuFactory.getImportToCampMenuItem()
                                    .setEnabled(false);
                                PopupMenuFactory.getviewMenuItem().setEnabled(true);
                                SalomeTMFPanels.getRenameCampaignButton()
                                    .setEnabled(false);
                                if (Permission.canUpdateCamp()) {
                                    SalomeTMFPanels.getOrderCampagne().setEnabled(
                                                                                  true);
                                    PopupMenuFactory.getAssignCampMenuItem()
                                        .setEnabled(true);
                                }
                                SalomeTMFPanels
                                    .setCampPanelWorkSpace(DataConstants.TEST);

                                SalomeTMFPanels.setCampPanelDescription(
                                                                        DataConstants.TEST, pTest
                                                                        .getDescriptionFromModel());
                                SalomeTMFPanels.setCampPanelDetailsForTest(pTest
                                                                           .getNameFromModel(), pTest
                                                                           .getConceptorFromModel(), pTest
                                                                           .getCreationDateFromModel().toString(),
                                                                           DataModel.getCurrentCampaign()
                                                                           .getAssignedUserID(pTest));
                                testName = pTest.getNameFromModel();
                            } else if (node.getUserObject() instanceof Family) {
                                Family pFamily = (Family) node.getUserObject();
                                oldData = pFamily;
                                PopupMenuFactory.setRootCampAction(false);
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentFamily(pFamily);
                                DataModel
                                    .setCurrentCampaign((Campaign) ((DefaultMutableTreeNode) node
                                                                    .getParent()).getUserObject());
                                SalomeTMFPanels.getAddTestInCampagne().setEnabled(
                                                                                  false);
                                PopupMenuFactory.getImportToCampMenuItem()
                                    .setEnabled(false);
                                PopupMenuFactory.getviewMenuItem().setEnabled(true);
                                if (Permission.canDeleteCamp()) {
                                    SalomeTMFPanels.getDelCampagne().setEnabled(
                                                                                true);
                                    PopupMenuFactory.getDelCampMenuItem()
                                        .setEnabled(true);
                                }
                                if (Permission.canUpdateCamp()) {
                                    SalomeTMFPanels.getOrderCampagne().setEnabled(
                                                                                  true);
                                    PopupMenuFactory.getAssignCampMenuItem()
                                        .setEnabled(true);
                                }
                                SalomeTMFPanels
                                    .setCampPanelWorkSpace(DataConstants.FAMILY);
                                SalomeTMFPanels.setCampPanelDescription(
                                                                        DataConstants.FAMILY, pFamily
                                                                        .getDescriptionFromModel());
                                SalomeTMFPanels.getRenameCampaignButton()
                                    .setEnabled(false);
                                familyName = pFamily.getNameFromModel();
                            } else if (node.getUserObject() instanceof TestList) {
                                TestList pTestList = (TestList) node
                                    .getUserObject();
                                oldData = pTestList;
                                PopupMenuFactory.setRootCampAction(false);
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentTestList(pTestList);
                                DataModel.setCurrentFamily(pTestList
                                                           .getFamilyFromModel());
                                DataModel
                                    .setCurrentCampaign((Campaign) ((DefaultMutableTreeNode) node
                                                                    .getParent().getParent())
                                                        .getUserObject());
                                SalomeTMFPanels.getAddTestInCampagne().setEnabled(
                                                                                  false);
                                PopupMenuFactory.getImportToCampMenuItem()
                                    .setEnabled(false);
                                PopupMenuFactory.getviewMenuItem().setEnabled(true);
                                if (Permission.canDeleteCamp()) {
                                    SalomeTMFPanels.getDelCampagne().setEnabled(
                                                                                true);
                                    PopupMenuFactory.getDelCampMenuItem()
                                        .setEnabled(true);
                                }
                                if (Permission.canUpdateCamp()) {
                                    SalomeTMFPanels.getOrderCampagne().setEnabled(
                                                                                  true);
                                    PopupMenuFactory.getAssignCampMenuItem()
                                        .setEnabled(true);
                                }
                                SalomeTMFPanels
                                    .setCampPanelWorkSpace(DataConstants.TESTLIST);
                                SalomeTMFPanels.setCampPanelDescription(
                                                                        DataConstants.TESTLIST, pTestList
                                                                        .getDescriptionFromModel());
                                SalomeTMFPanels.getRenameCampaignButton()
                                    .setEnabled(false);
                                testListName = pTestList.getNameFromModel();
                            } else if (node.getUserObject() instanceof Campaign) {
                                Campaign pCamp = (Campaign) node.getUserObject();
                                oldData = pCamp;
                                PopupMenuFactory.setRootCampAction(false);
                                if (Permission.canDeleteCamp()) {
                                    SalomeTMFPanels.getDelCampagne().setEnabled(
                                                                                true);
                                    PopupMenuFactory.getDelCampMenuItem()
                                        .setEnabled(true);
                                }
                                PopupMenuFactory.getviewMenuItem()
                                    .setEnabled(false);
                                if (Permission.canUpdateCamp())
                                    SalomeTMFPanels.getRenameCampaignButton()
                                        .setEnabled(true);
                                if (Permission.canCreateCamp()) {
                                    SalomeTMFPanels.getAddTestInCampagne()
                                        .setEnabled(true);
                                    PopupMenuFactory.getImportToCampMenuItem()
                                        .setEnabled(true);
                                }
                                if (Permission.canUpdateCamp()) {
                                    SalomeTMFPanels.getOrderCampagne().setEnabled(
                                                                                  true);
                                    PopupMenuFactory.getAssignCampMenuItem()
                                        .setEnabled(true);
                                }
                                /*
                                 * if (DataModel.isBeginDescriptionModification()) {
                                 * DataModel.setCampaignChange(true);
                                 * DataModel.setBeginDescriptionModification(false);
                                 * }
                                 */

                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentCampaign(pCamp);
                                DataModel.initCampaign(pCamp);
                                SalomeTMFPanels
                                    .setCampPanelWorkSpace(DataConstants.CAMPAIGN);
                                campaignName = pCamp.getNameFromModel();
                            } else {
                                oldData = null;
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                SalomeTMFPanels.getDelCampagne().setEnabled(false);
                                SalomeTMFPanels.getRenameCampaignButton()
                                    .setEnabled(false);
                                SalomeTMFPanels.getOrderCampagne()
                                    .setEnabled(false);
                                SalomeTMFPanels.getAddTestInCampagne().setEnabled(
                                                                                  false);
                                PopupMenuFactory.setRootCampAction(true);
                                /*
                                 * PopupMenuFactory.getImportToCampMenuItem().setEnabled
                                 * (false);
                                 * PopupMenuFactory.getDelCampMenuItem().setEnabled
                                 * (false);
                                 * PopupMenuFactory.getAssignCampMenuItem().
                                 * setEnabled(false);
                                 */
                                SalomeTMFPanels.setCampPanelWorkSpace(-1);
                            }
                        } else {
                            selectedNode = node;

                            if (node.getUserObject() instanceof Test) {
                                Test pTest = (Test) node.getUserObject();
                                oldData = pTest;
                                if (Permission.canDeleteTest()) {
                                    if (SalomeTMFPanels.getDelTestOrTestList() != null)
                                        SalomeTMFPanels.getDelTestOrTestList()
                                            .setEnabled(true);
                                    PopupMenuFactory.getDelTestMenuItem()
                                        .setEnabled(true);
                                }
                                if (Permission.canUpdateTest())
                                    SalomeTMFPanels.getRenameTestButton()
                                        .setEnabled(true);
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentTest(pTest);
                                DataModel.initTest(pTest);
                                testName = pTest.getNameFromModel();
                                if (pTest instanceof ManualTest) {
                                    SalomeTMFPanels
                                        .setTestPanelWorkSpace(DataConstants.MANUAL_TEST);
                                } else {
                                    SalomeTMFPanels
                                        .setTestPanelWorkSpace(DataConstants.AUTOMATIC_TEST);
                                }
                                /*
                                 * if (DataModel.isBeginDescriptionModification()) {
                                 * DataModel.setTestChange(true);
                                 * DataModel.setBeginDescriptionModification(false);
                                 * }
                                 */

                            } else if (node.getUserObject() instanceof Family) {
                                Family pFamily = (Family) node.getUserObject();
                                oldData = pFamily;
                                // if
                                // (!((Family)node.getUserObject()).getName().equals(DEFAULT_FAMILY_NAME))
                                // {
                                if (Permission.canUpdateTest())
                                    SalomeTMFPanels.getRenameTestButton()
                                        .setEnabled(true);
                                if (Permission.canDeleteTest()) {
                                    SalomeTMFPanels.getDelTestOrTestList()
                                        .setEnabled(true);
                                    PopupMenuFactory.getDelTestMenuItem()
                                        .setEnabled(true);
                                }
                                /*
                                 * } else {
                                 * SalomeTMF.getRenameTestButton().setEnabled
                                 * (false);
                                 * SalomeTMF.getDelTestOrTestList().setEnabled
                                 * (false);
                                 * TestMethods.getDelTestMenuItem().setEnabled
                                 * (false); }
                                 */
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentFamily(pFamily);
                                DataModel.initFamily(pFamily);
                                SalomeTMFPanels
                                    .setTestPanelWorkSpace(DataConstants.FAMILY);
                                familyName = pFamily.getNameFromModel();
                                /*
                                 * if (DataModel.isBeginDescriptionModification()) {
                                 * DataModel.setFamilyChange(true);
                                 * DataModel.setBeginDescriptionModification(false);
                                 * }
                                 */

                            } else if (node.getUserObject() instanceof TestList) {
                                TestList pTestList = (TestList) node
                                    .getUserObject();
                                oldData = pTestList;
                                if (Permission.canUpdateTest())
                                    SalomeTMFPanels.getRenameTestButton()
                                        .setEnabled(true);
                                if (Permission.canDeleteTest()) {
                                    SalomeTMFPanels.getDelTestOrTestList()
                                        .setEnabled(true);
                                    PopupMenuFactory.getDelTestMenuItem()
                                        .setEnabled(true);
                                }
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                DataModel.setCurrentTestList(pTestList);
                                DataModel.initTestList(pTestList);
                                SalomeTMFPanels
                                    .setTestPanelWorkSpace(DataConstants.TESTLIST);
                                testListName = pTestList.getNameFromModel();
                                /*
                                 * if (DataModel.isBeginDescriptionModification()) {
                                 * DataModel.setTestListChange(true);
                                 * DataModel.setBeginDescriptionModification(false);
                                 * }
                                 */

                            } else {
                                oldData = null;
                                // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
                                SalomeTMFPanels.getRenameTestButton().setEnabled(
                                                                                 false);
                                SalomeTMFPanels.getDelTestOrTestList().setEnabled(
                                                                                  false);
                                PopupMenuFactory.getDelTestMenuItem().setEnabled(
                                                                                 false);
                                SalomeTMFPanels.setTestPanelWorkSpace(-1); // remove
                                // all
                            }
                        }
                        if (getTreeType() == CAMPAIGN) {
                            SalomeTMFPanels.reValidateCampainPanel();
                        } else {
                            SalomeTMFPanels.reValidateTestPanel();
                        }
                    }
                    tree.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            });

        JScrollPane scrollPane = new JScrollPane(tree,
                                                 ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                                                 ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        add(scrollPane);
    } // Fin du constructeur DynamicTree/0

    /**
     * Supprime tout de l'arbre, sauf la racine
     */
    public void clear() {
        rootNode.removeAllChildren();
        treeModel.reload();
        // treeModel.reload();
    } // Fin de la m?thode clear/0

    /**
     * Suppression du noeud courant.
     */
    public void removeCurrentNode() {
        TreePath currentSelection = tree.getSelectionPath();
        if (currentSelection != null) {
            DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode) (currentSelection
                                                                           .getLastPathComponent());
            MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
            if (parent != null) {
                treeModel.removeNodeFromParent(currentNode);
                if (parent instanceof DefaultMutableTreeNode) {
                    refreshNode((DefaultMutableTreeNode) parent);
                }
                // tree.scrollPathToVisible(new
                // TreePath(((DefaultMutableTreeNode)parent).getPath()));
                return;
            }
        }
        // Si aucune s?lection ou si la racine est s?lectionn?e
        // toolkit.beep();
    } // Fin de la classe removeCurrentNode/0

    public void setCursorWait() {
        tree.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    }

    public void setCursorNormal() {
        tree.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void removeNode(DefaultMutableTreeNode node) {
        treeModel.removeNodeFromParent(node);
    }

    public void removeChildFromNode(DefaultMutableTreeNode node) {
        Vector nodeToRemove = new Vector();
        for (int i = 0; i < node.getChildCount(); i++) {
            DefaultMutableTreeNode pNode = (DefaultMutableTreeNode) node
                .getChildAt(i);
            nodeToRemove.add(pNode);
        }
        int size = nodeToRemove.size();
        for (int i = 0; i < size; i++) {
            DefaultMutableTreeNode pNode = (DefaultMutableTreeNode) nodeToRemove
                .get(i);
            treeModel.removeNodeFromParent(pNode);
        }
    }

    /**
     * Cherche de mani?re r?cursive et supprime le noeud correspond ? celui
     * pass? en param?tre. A l'appel le premier param?tre est le noeud de d?but
     * de la recherche.
     *
     * @param currentNode
     *            le noeud courant
     * @param node
     *            le noeud ? retirer.
     */
    public void searchAndRemove(DefaultMutableTreeNode currentNode,
                                DefaultMutableTreeNode node) {
        if (currentNode.equals(node)) {
            MutableTreeNode parent = (MutableTreeNode) (currentNode.getParent());
            if (parent != null) {
                treeModel.removeNodeFromParent(currentNode);
            }
        } else {
            for (int i = 0; i < currentNode.getChildCount(); i++) {
                searchAndRemove((DefaultMutableTreeNode) currentNode
                                .getChildAt(i), node);
            }
        }
    } // Fin de la m?thode searchAndRemove/2

    /**
     * M?thode d'ajout d'un noeud dans l'arbre
     *
     * @param child
     *            l'objet ajout? dans l'arbre
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(Object child) {
        DefaultMutableTreeNode parentNode = null;
        TreePath parentPath = tree.getSelectionPath();

        if (parentPath == null) {
            parentNode = rootNode;
        } else {
            parentNode = (DefaultMutableTreeNode) (parentPath
                                                   .getLastPathComponent());
        }
        return addObject(parentNode, child, true);
    } // Fin de la classe addObject/1

    /**
     * M?thode d'ajout d'un noeud invisible sous un parent
     *
     * @param parent
     *            le parent
     * @param child
     *            le noeud ajout?
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
                                            Object child) {
        return addObject(parent, child, false);
    } // Fin de la classe addObject/2

    /**
     * M?thode d'ajout d'un noeud dans l'arbre sous le parent.
     *
     * @param parent
     *            le parent
     * @param child
     *            le noeud ? ajouter
     * @param shouldBeVisible
     *            visible ou non
     * @return le nouveau noeud de l'arbre
     */
    public DefaultMutableTreeNode addObject(DefaultMutableTreeNode parent,
                                            Object child, boolean shouldBeVisible) {
        // Util.debug("Ajout de : " + child + " ? " + parent);
        DefaultMutableTreeNode childNode = new DefaultMutableTreeNode(child);
        return addNode(parent, childNode, shouldBeVisible);

    } // Fin de la classe addObject/3

    public DefaultMutableTreeNode addNode(DefaultMutableTreeNode parent,
                                          DefaultMutableTreeNode childNode, boolean shouldBeVisible) {
        // Util.debug("Ajout de : " + child + " ? " + parent);
        if (parent == null) {
            parent = rootNode;
        }
        try {
            // Insertion du noeud
            treeModel.insertNodeInto(childNode, parent, parent.getChildCount());
        } catch (ArrayIndexOutOfBoundsException exception) {
            treeModel.reload();
        }

        // on s'assure que le noeud est visible
        if (shouldBeVisible) {
            tree.scrollPathToVisible(new TreePath(childNode.getPath()));
        }

        return childNode;
    } // Fin de la classe addObject/3

    /**
     * M?thode qui retourne le noeud de l'arbre qui est s?lectionn?
     *
     * @return un noeud de l'arbre
     */
    public DefaultMutableTreeNode getSelectedNode() {
        return selectedNode;
    } // Fin de la m?thode getSelectedNode/0

    /**
     * Methode qui retourne tous les noeuds de l'arbre qui sont selectionne
     *
     * @return un noeud de l'arbre
     */
    public List<SimpleData> getSelectedNodes() {
        TreePath[] paths = tree.getSelectionPaths();
        List<SimpleData> simpleDatas = new ArrayList<SimpleData>();
        for (int i = 0; i < paths.length; i++) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i]
                .getLastPathComponent();
            simpleDatas.add((SimpleData) node.getUserObject());
        }
        // System.out.println(paths[0].getLastPathComponent().getClass());
        return simpleDatas;
    }

    /**
     * Retourne la racine de l'arbre
     *
     * @return la racine de l'arbre
     */
    public DefaultMutableTreeNode getRoot() {
        return rootNode;
    } // Fin de la m?thode getRoot/0

    /**
     * Retourne le type de l'arbre : "campagne" ou "suite"
     *
     * @return le type de l'arbre
     */
    public int getTreeType() {
        return treeType;
    } // Fin de la m?thode getTreeType/0

    /**
     * Retourne le noeud correspondant ? la famille dont le nom est pass? en
     * param?tre. Retourne <code>null</code>, si le nom ne correspond pas ? une
     * famille.
     *
     * @param familyName
     *            un nom
     * @return le noeud correspondant ? la famille dont le nom est pass? en
     *         param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une famille.
     */
    public DefaultMutableTreeNode findRemoveFamilyNode(String familyName,
                                                       boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (familyNode.getUserObject() instanceof Family
                && ((Family) familyNode.getUserObject()).getNameFromModel()
                .equals(familyName)) {
                if (toRemove) {
                    familyNode.removeFromParent();
                }
                return familyNode;
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est pass?
     * en param?tre. Retourne <code>null</code>, si le nom ne correspond pas ?
     * une suite de tests
     *
     * @param testListName
     *            un nom
     * @param familyName
     *            un nom de famille
     * @param toRemove
     *            pour indiquer que le noeud doit ?tre supprim?
     * @return le noeud correspondant ? la suite de tests dont le nom est pass?
     *         en param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveTestListNode(String testListName,
                                                         String familyName, boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (familyNode.getUserObject() instanceof Family
                && ((Family) familyNode.getUserObject()).getNameFromModel()
                .equals(familyName)) {
                for (int j = 0; j < familyNode.getChildCount(); j++) {
                    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) familyNode
                        .getChildAt(j);
                    if (testListNode.getUserObject() instanceof TestList
                        && ((TestList) testListNode.getUserObject())
                        .getNameFromModel().equals(testListName)) {
                        if (toRemove) {
                            testListNode.removeFromParent();
                        }
                        return testListNode;
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est pass?
     * en param?tre. Retourne <code>null</code>, si le nom ne correspond pas ?
     * une suite de tests
     *
     * @param testListName
     *            un nom
     * @param toRemove
     *            pour supprimer le noeud une fois trouv?
     * @return le noeud correspondant ? la suite de tests dont le nom est pass?
     *         en param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveTestNode(String testName,
                                                     String testListName, String familyName, boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (familyNode.getUserObject() instanceof Family
                && ((Family) familyNode.getUserObject()).getNameFromModel()
                .equals(familyName)) {
                for (int j = 0; j < familyNode.getChildCount(); j++) {
                    DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) familyNode
                        .getChildAt(j);
                    if (testListNode.getUserObject() instanceof TestList
                        && ((TestList) testListNode.getUserObject())
                        .getNameFromModel().equals(testListName)) {
                        for (int k = 0; k < testListNode.getChildCount(); k++) {
                            DefaultMutableTreeNode testNode = (DefaultMutableTreeNode) testListNode
                                .getChildAt(k);
                            if (testNode.getUserObject() instanceof Test
                                && ((Test) testNode.getUserObject())
                                .getNameFromModel()
                                .equals(testName)) {
                                if (toRemove) {
                                    testNode.removeFromParent();
                                }
                                return testNode;
                            }
                        }

                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant ? la campagne dont le nom est pass? en
     * param?tre. Retourne <code>null</code>, si le nom ne correspond pas ? une
     * campagne. Si le bool?en est mis ? vrai, le noeud est supprim? de l'arbre.
     *
     * @param familyName
     *            un nom
     * @param toRemove
     *            vrai pour supprimer le noeud, faux sinon.
     * @return le noeud correspondant ? la campagne dont le nom est pass? en
     *         param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une campagne.
     */
    public DefaultMutableTreeNode findRemoveCampaignNode(String campaignName,
                                                         boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode campaignNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (campaignNode.getUserObject() instanceof Campaign
                && ((Campaign) campaignNode.getUserObject())
                .getNameFromModel().equals(campaignName)) {
                if (toRemove) {
                    campaignNode.removeFromParent();
                }
                return campaignNode;
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est pass?
     * en param?tre. Retourne <code>null</code>, si le nom ne correspond pas ?
     * une suite de tests
     *
     * @param testListName
     *            un nom
     * @return le noeud correspondant ? la suite de tests dont le nom est pass?
     *         en param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveFamilyNodeInCampagneTree(
                                                                     String familyName, String campaignName, boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode campaignNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (campaignNode.getUserObject() instanceof Campaign
                && ((Campaign) campaignNode.getUserObject())
                .getNameFromModel().equals(campaignName)) {
                for (int j = 0; j < campaignNode.getChildCount(); j++) {
                    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) campaignNode
                        .getChildAt(j);
                    if (familyNode.getUserObject() instanceof Family
                        && ((Family) familyNode.getUserObject())
                        .getNameFromModel().equals(familyName)) {
                        if (toRemove) {
                            familyNode.removeFromParent();
                        }
                        return familyNode;
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant ? la suite de tests dont le nom est pass?
     * en param?tre. Retourne <code>null</code>, si le nom ne correspond pas ?
     * une suite de tests
     *
     * @param testListName
     *            un nom
     * @return le noeud correspondant ? la suite de tests dont le nom est pass?
     *         en param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? une suite de tests.
     */
    public DefaultMutableTreeNode findRemoveTestListNodeInCampagneTree(
                                                                       String testListName, String familyName, String campaignName,
                                                                       boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode campaignNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (campaignNode.getUserObject() instanceof Campaign
                && ((Campaign) campaignNode.getUserObject())
                .getNameFromModel().equals(campaignName)) {
                for (int j = 0; j < campaignNode.getChildCount(); j++) {
                    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) campaignNode
                        .getChildAt(j);
                    if (familyNode.getUserObject() instanceof Family
                        && ((Family) familyNode.getUserObject())
                        .getNameFromModel().equals(familyName)) {
                        for (int k = 0; k < familyNode.getChildCount(); k++) {
                            DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) familyNode
                                .getChildAt(k);
                            if (testListNode.getUserObject() instanceof TestList
                                && ((TestList) testListNode.getUserObject())
                                .getNameFromModel().equals(
                                                           testListName)) {
                                if (toRemove) {
                                    testListNode.removeFromParent();
                                }
                                return testListNode;
                            }
                        }
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le noeud correspondant au test dont le nom est pass? en
     * param?tre. Retourne <code>null</code>, si le nom ne correspond pas ? un
     * test
     *
     * @param testName
     *            un nom de test
     * @param testListName
     *            un nom de suite de tests
     * @param familyName
     *            un nom de famille
     * @param campaignName
     *            un nom de campagne
     * @param toRemove
     *            pour indiquer s'il doit ?tre supprim?
     * @return le noeud correspondant au test dont le nom est pass? en
     *         param?tre. Retourne <code>null</code>, si le nom ne correspond
     *         pas ? un test.
     */
    public DefaultMutableTreeNode findRemoveTestNodeInCampagneTree(
                                                                   String testName, String testListName, String familyName,
                                                                   String campaignName, boolean toRemove) {
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) treeModel
            .getRoot();
        for (int i = 0; i < root.getChildCount(); i++) {
            DefaultMutableTreeNode campaignNode = (DefaultMutableTreeNode) root
                .getChildAt(i);
            if (campaignNode.getUserObject() instanceof Campaign
                && ((Campaign) campaignNode.getUserObject())
                .getNameFromModel().equals(campaignName)) {
                for (int j = 0; j < campaignNode.getChildCount(); j++) {
                    DefaultMutableTreeNode familyNode = (DefaultMutableTreeNode) campaignNode
                        .getChildAt(j);
                    if (familyNode.getUserObject() instanceof Family
                        && ((Family) familyNode.getUserObject())
                        .getNameFromModel().equals(familyName)) {
                        for (int k = 0; k < familyNode.getChildCount(); k++) {
                            DefaultMutableTreeNode testListNode = (DefaultMutableTreeNode) familyNode
                                .getChildAt(k);
                            if (testListNode.getUserObject() instanceof TestList
                                && ((TestList) testListNode.getUserObject())
                                .getNameFromModel().equals(
                                                           testListName)) {
                                for (int h = 0; h < testListNode
                                         .getChildCount(); h++) {
                                    DefaultMutableTreeNode testNode = (DefaultMutableTreeNode) testListNode
                                        .getChildAt(h);
                                    if (testNode.getUserObject() instanceof Test
                                        && ((Test) testNode.getUserObject())
                                        .getNameFromModel().equals(
                                                                   testName)) {
                                        if (toRemove) {
                                            testNode.removeFromParent();
                                        }
                                        return testNode;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    } // Fin de la m?thode findFamilyNode/1

    /**
     * Retourne le mod?le de donn?es de l'arbre
     *
     * @return le mod?le de donn?es de l'arbre
     */
    public TestTreeModel getModel() {
        return treeModel;
    } // Fin de la m?thode getModel/0

    public void refreshNode(DefaultMutableTreeNode node) {
        if (node != null) {
            treeModel.reload(node);
        }
    }

    public void refreshCurent() {
        if (selectedNode != null) {
            treeModel.reload(selectedNode);
        }
    }

    /**
     * Rend visible le chemin jusqu'au noeud pass? en param?tre
     *
     * @param node
     *            un noeud
     */
    public void scrollPathToVisible(DefaultMutableTreeNode node) {
        tree.scrollPathToVisible(new TreePath(node.getPath()));
    } // Fin de la m?thode scrollPathToVisible/1

    public void makeVisible(DefaultMutableTreeNode node) {
        tree.makeVisible(new TreePath(node.getPath()));
    }

    public void resetToRoot() {
        tree.scrollPathToVisible(new TreePath(rootNode.getPath()));
        if (getTreeType() == CAMPAIGN) {
            // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
            SalomeTMFPanels.getDelCampagne().setEnabled(false);
            SalomeTMFPanels.getRenameCampaignButton().setEnabled(false);
            SalomeTMFPanels.getOrderCampagne().setEnabled(false);
            SalomeTMFPanels.getAddTestInCampagne().setEnabled(false);
            /*
             * PopupMenuFactory.getImportToCampMenuItem().setEnabled(false);
             * PopupMenuFactory.getDelCampMenuItem().setEnabled(false);
             */
            PopupMenuFactory.setRootCampAction(true);
            SalomeTMFPanels.setCampPanelWorkSpace(-1);
        } else {
            // DataModel.setOldCaretDescription(DataModel.getCaretDescriptionValue());
            SalomeTMFPanels.getRenameTestButton().setEnabled(false);
            SalomeTMFPanels.getDelTestOrTestList().setEnabled(false);
            PopupMenuFactory.getDelTestMenuItem().setEnabled(false);
            SalomeTMFPanels.setTestPanelWorkSpace(-1); // remove all
        }
    }

    /**************************************************************************/
    /** CLASSE INTERNE ***/
    /**************************************************************************/

    /**
     * Classe repr?sentant le listener associ? ? l'arbre
     *
     * @author teaml039
     * @version 0.1
     */
    // class MyTreeModelListener implements TreeModelListener {

    /**
     * Apppel? lorsque les noeuds sont modifi?s
     *
     * @param e
     *            un ?v?nement sur l'arbre
     */
    /*
     * public void treeNodesChanged(TreeModelEvent e) {
     *
     *
     * } // Fin de la m?thode treeNodesChanged/1
     *
     * public void treeNodesInserted(TreeModelEvent e) { } public void
     * treeNodesRemoved(TreeModelEvent e) { } public void
     * treeStructureChanged(TreeModelEvent e) { } } // Fin de la classe
     * MyTreeModelListener
     */

    /**
     * @return
     */
    public JTree getTree() {
        return tree;
    }

    public void givePopupMenuToTree() {
        // Menu PopUp pour la souris
        JPopupMenu popUp;
        if (getTreeType() == CAMPAIGN) {
            popUp = PopupMenuFactory.createTreePopUpForCampagne();
        } else {
            popUp = PopupMenuFactory.createTreePopUpForList();
        }
        tree.addMouseListener(new PopupListener(popUp, tree));
    }
    
    public void expandTree(JTree tree, boolean expand) {
        TreeNode root = (TreeNode) tree.getModel().getRoot();
        expandAll(tree, new TreePath(root), expand);
    }
 
    public void expandAll(JTree tree, TreePath path, boolean expand) {
        TreeNode node = (TreeNode) path.getLastPathComponent();
 
        if (node.getChildCount() >= 0) {
            Enumeration enumeration = node.children();
            while (enumeration.hasMoreElements()) {
                TreeNode n = (TreeNode) enumeration.nextElement();
                TreePath p = path.pathByAddingChild(n);
 
                expandAll(tree, p, expand);
            }
        }
 
        if (expand) {
            tree.expandPath(path);
        } else {
            tree.collapsePath(path);
        }
    }    

} // Fin de la classe DynamicTree
