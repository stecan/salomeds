/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;

/**
 * @author teaml039
 * @version : 0.1
 */
public class RowTransferHandler extends TransferHandler {
    
    DataFlavor stringFlavor = DataFlavor.stringFlavor;
    JTable sourcePic;
    boolean shouldRemove;
    
    @Override
    public boolean importData(JComponent c, Transferable t) {
        
        if (canImport(c, t.getTransferDataFlavors())) {
            ArrayList data;
            JTable table = (JTable)c;
            //Don't drop on myself.
            if (sourcePic == table) {
                shouldRemove = false;
                return true;
            }
            try {
                data = (ArrayList)t.getTransferData(stringFlavor);
                //Set the component to the new picture.
                ((MyTableModel)table.getModel()).addRow(data);
                return true;
            } catch (UnsupportedFlavorException ufe) {
                //Util.debug("importData: unsupported data flavor");
            } catch (IOException ioe) {
                //Util.debug("importData: I/O exception");
            }
        }
        return false;
    }
    
    
    @Override
    protected Transferable createTransferable(JComponent c) {
        sourcePic = (JTable)c;
        shouldRemove = true;
        //int selectedRow = sourcePic.getSelectedRow();
        return new PictureTransferable(((MyTableModel)sourcePic.getModel()).getData());
    }
    
    @Override
    public int getSourceActions(JComponent c) {
        return COPY_OR_MOVE;
    }
    
    @Override
    protected void exportDone(JComponent c, Transferable data, int action) {
        if (shouldRemove && (action == MOVE)) {
            sourcePic.remove(sourcePic.getSelectedRow());
        }
        sourcePic = null;
    }
    
    @Override
    public boolean canImport(JComponent c, DataFlavor[] flavors) {
        for (int i = 0; i < flavors.length; i++) {
            if (stringFlavor.equals(flavors[i])) {
                return true;
            }
        }
        return false;
    }
    
    class PictureTransferable implements Transferable {
        private ArrayList data;
        
        PictureTransferable(ArrayList list) {
            data = list;
        }
        
        @Override
        public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException {
            if (!isDataFlavorSupported(flavor)) {
                throw new UnsupportedFlavorException(flavor);
            }
            return data;
        }
        
        @Override
        public DataFlavor[] getTransferDataFlavors() {
            return new DataFlavor[] { stringFlavor };
        }
        
        @Override
        public boolean isDataFlavorSupported(DataFlavor flavor) {
            return stringFlavor.equals(flavor);
        }
    }
    @Override
    public void exportToClipboard(JComponent comp, Clipboard clip, int action) {
        boolean exportSuccess = false;
        Transferable t = null;
        
        int clipboardAction = getSourceActions(comp) & action;
        if (clipboardAction != NONE) {
            t = createTransferable(comp);
            if (t != null) {
                clip.setContents(t, null);
                exportSuccess = true;
            }
        }
        
        if (exportSuccess) {
            exportDone(comp, t, clipboardAction);
        } else {
            exportDone(comp, null, NONE);
        }
    }
    
    
    
} // Fin de la classe RowTransferHandler
