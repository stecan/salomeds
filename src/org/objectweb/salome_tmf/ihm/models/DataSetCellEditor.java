/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;

/**
 * @author teaml039
 */
public class DataSetCellEditor extends AbstractCellEditor implements TableCellEditor  {
    
    /**
     * Composant prenant en compte la modification de la cellule
     */
    JComponent component = new JTextField();
    
    /**
     * M?thode appel?e lorsque l'utilisateur ?dite la cellule
     */
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int rowIndex, int vColIndex) {
        
        // Configure le composant avec la valeur sp?cifi?e
        ((JTextField)component).setText((String)value);
        
        // Retourne le composant
        return component;
    }
    
    /**
     * M?thode appel?e lorsque l'?dition est termin?e.
     * @return la nouvelle valeur entr?e dans la cellule
     */
    @Override
    public Object getCellEditorValue() {
        return ((JTextField)component).getText();
    }
    
}
