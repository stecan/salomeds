/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.util.ArrayList;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

import org.objectweb.salome_tmf.api.Util;

/**
 * Classe qui repr?sente le mod?le d'un table
 */
public class MyTableModel extends AbstractTableModel {

    /**
     * Mode debug
     */
    private boolean DEBUG = false;

    /**
     * Liste des noms de colonnes
     */
    private ArrayList columnIdentifiers;

    /**
     * Listes des donn?es
     */
    public ArrayList dataArrayList;

    /******************************************************************************/
    /** CONSTRUCTEUR ***/
    /******************************************************************************/

    /**
     * Constructeur du mod?le de table
     */
    public MyTableModel() {
        columnIdentifiers = new ArrayList();
        dataArrayList = new ArrayList();
    } // Fin du constructeur MyTableModel/0

    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/

    /**
     * Retourne le nombre de colonnes
     */
    @Override
    public int getColumnCount() {
        return columnIdentifiers.size();
    } // Fin de la m?thode getColumnCount/0

    /**
     * Retourne le nombre de lignes
     */
    @Override
    public int getRowCount() {
        if (getColumnCount() == 0) {
            return 0;
        } else {
            return ((ArrayList) dataArrayList.get(0)).size();
        }
    } // Fin de la m?thode getRowCount/0

    /**
     * Retourne le nom de la colonne dont l'indice est pass? en param?tre.
     *
     * @param col
     *            indice de la colonne
     * @return le nom de la colonne
     */
    @Override
    public String getColumnName(int col) {
        return (String) columnIdentifiers.get(col);
    } // Fin de la m?thode getColumnName/1

    /**
     * R?cup?re la valeur de la table ? un num?ro de ligne et un num?ro de
     * colonne donn?s
     *
     * @param row
     *            le num?ro de ligne
     * @param col
     *            le num?ro de colonne
     * @return l'objet de la table
     */
    @Override
    public Object getValueAt(int row, int col) {
        Object obj;
        int i;
        try {
            ArrayList arrayList = (ArrayList) dataArrayList.get(col);
            if (row == -1) {
                i = 0;
                obj = (arrayList).get(i);
                while ((i < (arrayList).size()) && ((obj == null) || (obj == ""))) {
                    obj = (arrayList).get(i);
                    i++;
                } 
            } else {
                obj = (arrayList).get(row);
            }
            return obj;
        } catch (IndexOutOfBoundsException e) {
            return null;
        }

    } // Fin de la m?thode getValueAt/2

    /**
     * JTable utilise cette m?thode pour d?terminer le renderer pour chaque
     * cellule. Si la valeur de la cellule on consid?re qu'il s'agit d'une
     * String
     *
     * @param c
     *            la colonne choisie
     * @return la classe des cellules de la colonne
     */
    @Override
    public Class getColumnClass(int c) {
        if (getValueAt(-1, c) == null) {
            return String.class;
        }
        return getValueAt(-1, c).getClass();
    } // Fin de la m?thode getColumnClass/1

    /**
     * Rend vrai si les cellules de la table sont ?ditables
     *
     * @return toujours faux
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    } // Fin de la m?thode isCellEditable/2

    /**
     * Permet de modifier un objet dans la table
     *
     * @param value
     *            le nouvel objet? ins?rer
     * @param row
     *            la ligne de modification
     * @param col
     *            la colonne de modification
     */
    @Override
    public void setValueAt(Object value, int row, int col) {
        if (DEBUG) {
            Util.debug("Valeur ? " + row + "," + col + " = " + value
                       + " (une instance de " + value.getClass() + ")");
        }

        ((ArrayList) dataArrayList.get(col)).set(row, value);
//        
//        for (int i=0; i<row; i++) {
//            if ((((ArrayList) dataArrayList.get(col)).get(i) == null) || (((ArrayList) dataArrayList.get(col)).get(i) == "")) {
//                ((ArrayList) dataArrayList.get(col)).set(i, value);
//            }
//        }
//        
//        
        fireTableCellUpdated(row, col);

        if (DEBUG) {
            Util.debug("Nouvelle valeur de la donn?e :");
            printDebugData();
        }
    } // Fin de la m?thode setValue/3

    /**
     * Permet d'ajouter un objet dans la table
     *
     * @param value
     *            l'objet ? ajouter
     * @param row
     *            la ligne o? ajouter
     * @param col
     *            la colonne o? ajouter
     */
    public void addValueAt(Object value, int row, int col) {
        if (DEBUG) {
            Util.debug("Valeur ? " + row + "," + col + " = " + value
                       + " (une instance de " + value.getClass() + ")");
        }

        ((ArrayList) dataArrayList.get(col)).add(row, value);
        fireTableCellUpdated(row, col);
    } // Fin de la m?thode addValueAt/3

    /**
     * M?thode qui affiche sur la sortie standard, l'ensemble des donn?es de la
     * table
     */
    public void printDebugData() {
        int numRows = getRowCount();
        int numCols = getColumnCount();

        for (int i = 0; i < numRows; i++) {
            System.out.print("    ligne " + i + ":");
            for (int j = 0; j < numCols; j++) {
                System.out.print("  "
                                 + ((ArrayList) dataArrayList.get(j)).get(i));
            }
            Util.debug();
        }
    } // Fin de la m?thode printDebugData/0

    /**
     * Mutateur des donn?es de la table
     *
     * @param data
     *            les donn?es
     */
    public void setData(ArrayList data) {
        dataArrayList = data;
        fireTableStructureChanged();
    } // Fin de la m?thode setData/1

    /**
     * M?thode qui ajoute une colonne
     */
    public void addColumn() {
        dataArrayList.add(new ArrayList());
    } // Fin de la m?thode addColumn/0

    /**
     * M?thode qui ajoute un nom de colonne
     *
     * @param name
     *            un nom de colonne
     */
    public void addColumnName(String name) {
        columnIdentifiers.add(name);
    } // Fin de la m?thode addColumnName/1

    /**
     * Retourne l'ensemble des donn?es de la table
     *
     * @return l'ensemble des donn?es de la table
     */
    public ArrayList getData() {
        return dataArrayList;
    } // Fin de la m?thode getData/0

    /**
     * M?thode qui retourne toutes les donn?es sur une ligne de la table dont
     * l'indice est pass? en param?tre.
     *
     * @param index
     *            num?ro de la colonne
     * @return une liste contenant les valeurs des diff?rentes colonnes de la
     *         ligne en question
     */
    public ArrayList getData(int index) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < getColumnCount(); i++) {
            result.add(getValueAt(index, i));
        }
        return result;
    } // Fin de la m?thode getData/1

    /**
     * Ajoute une colonne avec son nom
     *
     * @param name
     *            le nom de la colonne
     */
    public void addColumnNameAndColumn(String name) {
        addColumnName(name);
        addColumn();
        // justifyRows(0, getRowCount());
        fireTableStructureChanged();
    } // Fin de la m?thode addColumnNameAndColumn/1

    /**
     * Permet d'ajouter une liste de donn?es dans la table. La liste fournie en
     * param?tre doit avoir une taille inf?rieure ou ?gale au nombre de
     * colonnes. Le premier ?l?ment de la liste est mis dans la premi?re colonne
     * et ainsi de suite.
     *
     * @param list
     *            une liste de donn?es
     */
    public void addRow(ArrayList list) {
        if (list.size() <= getColumnCount()) {
            int ligneIndex = getRowCount();
            for (int i = 0; i < list.size(); i++) {
                addValueAt(list.get(i), ligneIndex, i);
            }
            fireTableStructureChanged();
        }
    } // Fin de la m?thode addData/1

    /**
     * Permet d'ajouter une liste de donn?es dans la table. La liste fournie en
     * param?tre doit avoir une taille inf?rieure ou ?gale au nombre de
     * colonnes. Le premier ?l?ment de la liste est mis dans la premi?re colonne
     * et ainsi de suite.
     *
     * @param list
     *            une liste de donn?es
     */
    public void addDataColumn(Vector vector, int columnIndex) {

        for (int i = 0; i < vector.size(); i++) {
            if (vector.get(i) != null) {
                addValueAt(vector.get(i).toString(), i, columnIndex);
            } else {
                addValueAt("", i, columnIndex);
            }
        }
        fireTableStructureChanged();
    } // Fin de la m?thode addData/1

    /**
     * M?thode qui modifie une ligne de la table
     *
     * @param list
     *            la liste des nouvelles valeurs
     * @param row
     *            la ligne ? modifier
     */
    public void modifyData(ArrayList list, int row) {
        if (list.size() <= getColumnCount()) {
            for (int i = 0; i < list.size(); i++) {
                setValueAt(list.get(i), row, i);
            }
            // fireTableStructureChanged();
        }
    } // Fin de la m?thode modifyData/2

    /**
     * M?thode qui supprime toute une ligne
     *
     * @param row
     *            l'indice de la ligne
     */
    private void removeRow(int row) {
        for (int j = 0; j < getColumnCount(); j++) {
            if (row < ((ArrayList) dataArrayList.get(j)).size()) {
                ((ArrayList) dataArrayList.get(j)).remove(row);
            }
        }
    } // Fin de la m?thode removeRow/1

    /**
     * M?thode qui supprime une ligne de la table
     *
     * @param row
     *            indice de la ligne
     */
    public void removeData(int row) {
        removeRow(row);
        fireTableStructureChanged();
    } // Fin de la m?thode removeData/1

    /**
     * M?thode qui nettoie toute la table
     */
    public void clearTable() {
        int nbRow = getRowCount();
        for (int i = 0; i < nbRow; i++) {
            removeRow(0);
        }
        fireTableStructureChanged();
    } // Fin de la m?thode clearTable/0

    /**
     * Recherche la ligne o? le nom pass? en param?tre correspond au nom de la
     * colonne 0, -1 si aucune correspondance.
     *
     * @param name
     * @return
     */
    public int findRow(String name) {
        for (int i = 0; i < getRowCount(); i++) {
            if (getValueAt(i, 0).equals(name)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Supprime la ligne dont l'?l?ment de la colonne 0 correspond ? la cha?ne
     * pass?e en param?tre.
     *
     * @param key
     */
    public void removeRow(String key) {
        for (int i = 0; i < getRowCount(); i++) {
            if (getValueAt(i, 0).equals(key)) {
                removeData(i);
                break;
            }
        }
    }
} // Fin de la classe MyTableModel
