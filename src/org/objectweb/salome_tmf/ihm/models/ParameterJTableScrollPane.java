/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;


import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * @author teaml039
 * @version : 0.1
 */
public class ParameterJTableScrollPane extends JScrollPane {
    
    
    /**
     * Le mod?le de liste d'ex?cutions
     */
    ParameterTableModel parametersTableModel;
    
    JTable parametersTable;
    
    ListSelectionModel rowSelectionModel;
    
    ListSelectionModel colSelectionModel;
    
    boolean addNewLine = false;
    
    boolean addNewParam = false;
    
    boolean wrongName = false;
    
    boolean otherKey = false;
    
    /**
     *
     * @param table
     */
    public ParameterJTableScrollPane(JTable table) {
        super(table);
        parametersTableModel = new ParameterTableModel();
        parametersTable = table;
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom"));
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Valeur"));
        parametersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Description"));
        
        parametersTable.setModel(parametersTableModel);
        parametersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        parametersTable.setCellSelectionEnabled(true);
        parametersTable.setColumnSelectionAllowed(true);
        parametersTable.setRowSelectionAllowed(true);
        
        rowSelectionModel = parametersTable.getSelectionModel();
        rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    int rowIndex = e.getLastIndex();
                
                    if (addNewParam) {
                        rowSelectionModel.setSelectionInterval(rowIndex, rowIndex);
                        addNewParam = false;
                    }
                    if (wrongName) {
                        rowSelectionModel.setSelectionInterval(rowIndex, rowIndex);
                        colSelectionModel.setSelectionInterval(0,0);
                        wrongName = false;
                    }
                
                }
            });
        
        colSelectionModel = parametersTable.getColumnModel().getSelectionModel();
        colSelectionModel.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (addNewLine) {
                        colSelectionModel.setSelectionInterval(0,0);
                        addNewLine = false;
                    }
                }
            });
        parametersTable.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {
                
                }
            
                @Override
                public void keyPressed(KeyEvent e) {
                
                    if (e.getKeyCode() == KeyEvent.VK_ENTER || e.getKeyCode() == KeyEvent.VK_TAB) {
                        if (parametersTable .getSelectedColumn() == 0 &&
                            parametersTableModel.getValueAt(parametersTable.getSelectedRow(), parametersTable.getSelectedColumn()) == null &&
                            !otherKey) {
                            wrongName = true;
                            otherKey = false;
                        }
                    
                        if (parametersTable.getSelectedColumn() == 1 &&
                            parametersTableModel.getValueAt(parametersTable.getSelectedRow(), parametersTable.getSelectedColumn()) == null &&
                            !otherKey &&
                            parametersTableModel.getValueAt(parametersTable.getSelectedRow(), 0) != null &&
                            !parametersTableModel.getValueAt(parametersTable.getSelectedRow(), 0).equals("")) {
                            parametersTableModel.setValueAt("", parametersTable.getSelectedRow(), parametersTable.getSelectedColumn());
                            otherKey = false;
                        }
                        if (parametersTable.getSelectedColumn() == 2 && parametersTableModel.getValueAt(parametersTable.getSelectedRow(), parametersTable.getSelectedColumn()) == null && !otherKey
                            &&
                            parametersTableModel.getValueAt(parametersTable.getSelectedRow(), 0 ) != null &&
                            !parametersTableModel.getValueAt(parametersTable.getSelectedRow(), 0).equals("")) {
                            parametersTableModel.setValueAt("", parametersTable.getSelectedRow(), parametersTable.getSelectedColumn());
                            otherKey = false;
                        }
                    } else {
                        otherKey = true;
                    }
                }
            
                @Override
                public void keyReleased(KeyEvent e) {
                }
            
            });
        
        parametersTableModel.addTableModelListener(new TableModelListener() {
                @Override
                public void tableChanged(TableModelEvent e) {
                
                    int row = e.getFirstRow();
                    int column = e.getColumn();
                    if (row != -1 && column != -1) {
                    
                        Object data = parametersTableModel.getValueAt(row, column);
                        if (column == 0 && data instanceof String && containsParamName((String)data, row)) {
                            JOptionPane.showMessageDialog(new Frame(),
                                                          Language.getInstance().getText("Ce_nom_de_parametre_existe_deja_"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                            parametersTableModel.setValueAt(null, row, column);
                            wrongName = true;
                        } else if (parametersTableModel.getValueAt(row,0) != null && !parametersTableModel.getValueAt(row,0).equals("")){
                            addNewParam = true;
                            otherKey =false;
                        
                        }
                    
                        if (column == 2 && parametersTableModel.getValueAt(row,0) != null && !parametersTableModel.getValueAt(row,0).equals("") && row == parametersTableModel.getRowCount() -1) {
                            newEntry();
                            addNewLine = true;
                        }
                    }
                }
            });
    }
    
    /**
     *
     * @param paramName
     * @param selectedRow
     * @return
     */
    private boolean containsParamName(String paramName, int selectedRow) {
        for (int i = 0; i < parametersTableModel.getRowCount(); i++) {
            if (i != selectedRow && parametersTableModel.getValueAt(i,0) != null && parametersTableModel.getValueAt(i,0).equals(paramName)) {
                return true;
            }
        }
        return false;
    } // Fin de la m?thode containsParamName/2
    
    /**
     *
     *
     */
    public void newEntry() {
        ArrayList dataList = new ArrayList();
        dataList.add(null);
        dataList.add(null);
        dataList.add(null);
        parametersTableModel.addRow(dataList);
    } // Fin de la m?thode newEntry/0
    
} // Fin de la classe ParameterJTable
