/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;



import java.awt.event.MouseAdapter;

import java.awt.event.MouseEvent;



import javax.swing.JPopupMenu;

import javax.swing.JTree;

import javax.swing.tree.TreePath;



/**

 * Classe repr?sentant un nouvel ?couteur pour un menu popup

 * @author teaml039

 * @version 0.1

 */

public class PopupListener extends MouseAdapter {

    

    /**

     * Menu popup

     */

    private JPopupMenu _popup;

    

    /**

     * Arbre sur lequel le menu est affich?

     */

    private JTree _tree;

    

    /******************************************************************************/

    /**                                                         CONSTRUCTEUR                                                            ***/

    /******************************************************************************/

    /**

     * Constructeur du listener

     * @param popupMenu le menu ? afficher

     * @param tree l'arbre sur lequel on affihce le menu

     */

    public PopupListener(JPopupMenu popupMenu, JTree tree) {

        _popup = popupMenu;

        _tree = tree;

    } // Fin du constructeur PopupListener/2

    

    

    /******************************************************************************/

    /**                                                         METHODES PUBLIQUES                                                      ***/

    /******************************************************************************/

    

    /**

     * Appel?e si on appuie sur un bouton de la souris

     * @param e un ?v?nement venant de la souris

     */

    @Override
    public void mousePressed(MouseEvent e) {

        maybeShowPopup(e);

    } // Fin de la m?thode mousePressed/1

    

    /**

     * Appel?e si on relache un bouton de la souris

     * @param e un ?v?nement venant de la souris

     */

    @Override
    public void mouseReleased(MouseEvent e) {

        maybeShowPopup(e);

    } // Fin de la m?thode mouseReleased/1

    

    /**

     * Traite le clic du bouton droit pour afficher le menu popup.

     * @param e un ?v?nement venant de la souris

     */

    private void maybeShowPopup(MouseEvent e) {

        if (e.getButton() == MouseEvent.BUTTON3) { //click droit isPopupTrigger()

            TreePath path = _tree.getClosestPathForLocation(e.getX(),e.getY());

            _tree.makeVisible(path);

            _tree.setSelectionRow(_tree.getRowForPath(path));

            _popup.show(e.getComponent(), e.getX(), e.getY());

        }

    } // Fin de la m?thode maybeShowPopup/1

} // Fin de la classe PopupListener
