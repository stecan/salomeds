/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;



import java.io.File;



import javax.swing.filechooser.FileFilter;



/**

 * @author teaml039

 */

/** Cette classe permet d'ajouter un filtre bas? sur l'extension des fichiers. */

public class ScriptFileFilter extends FileFilter {

    

    /** Le texte qui sera affich? dans le comboBox du JFileChooser. */

    private String description;

    /** Extension de notre fichier (inclus le '.') */

    private String[] extensions;

    

    

    /**

     * Constructeur.

     * @param description, le texte de description pour l'utilisateur.

     * @param extentions, les extension du fichier

     */

    public ScriptFileFilter(String description, String[] extensions){

        super();

        this.description = description;

        this.extensions = extensions;

    }

    /**

     * Constructeur.

     * @param description, le texte de description pour l'utilisateur.

     * @param extention, l'extention du fichier (commencant par '.')

     */

    public ScriptFileFilter( String description, String extension ) {

        this(description,new String[]{extension});

    }

    

    /**

     * Indique si le fichier est accept? par le filtre.

     * @return vrai si le fichier est accept?.

     */

    @Override
    public boolean accept(File file) {

        if(file.isDirectory()) {

            return true;

        }

        

        String nomFichier = file.getPath().toLowerCase();

        int n = extensions.length;

        for(int i=0; i<n; i++) {

            if(nomFichier.endsWith(extensions[i])) {

                return true;

            }

        }

        return false;

    }

    

    @Override
    public String getDescription() {

        return(this.description);

    }

}
