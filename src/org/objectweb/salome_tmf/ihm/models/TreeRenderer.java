/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;

import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import static org.objectweb.salome_tmf.ihm.tools.Tools.campaignTerminated;


/**
 * Classe qui d?finit le support pour la visualisation de l'arbre.
 * @author teaml039
 * @version 0.1
 */
public class TreeRenderer extends JLabel implements TreeCellRenderer, IHMConstants {
    
    /**
     * S?parateur de fichier
     */
    String fileSeparator = "/";
    
    /**
     * L'ancien arbre
     */
    //private JTree tree;
    
    /**
     * Vrai si une valeur est cours de s?lection
     */
    protected boolean selected;
    
    /**
     * Vrai s'il a le focus
     */
    protected boolean hasFocus;
    
    /**
     * Vrai pour dessiner le bord des icones
     */
    private boolean drawsFocusBorderAroundIcon;
    // Icons
    /**
     * Icone pour les noeuds ferm?s (pas les feuilles)
     */
    transient protected Icon closedIcon;
    /**
     * Icone pour les feuilles
     */
    transient protected Icon leafIcon;
    
    /**
     * Icone pour les noeuds ouverts
     */
    transient protected Icon openIcon;
    
    // Colors
    /**
     *  Couleur utilis? pour le foreground pour les noeuds s?lectionn?s
     */
    protected Color textSelectionColor;
    /**
     *  Couleur utilis? pour le foreground pour les noeuds non s?lectionn?s
     */
    protected Color textNonSelectionColor;
    
    /**
     *  Couleur utilis? pour le background pour les noeuds s?lectionn?s
     */
    protected Color backgroundSelectionColor;
    /**
     *  Couleur utilis? pour le background pour les noeuds non s?lectionn?s
     */
    protected Color backgroundNonSelectionColor;
    
    /**
     *  Couleur pour indiquer que le noeud a le focus
     */
    protected Color borderSelectionColor;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur du TreeRenderer
     */
    public TreeRenderer() {
        setHorizontalAlignment(SwingConstants.LEFT);
        setLeafIcon(UIManager.getIcon("Tree.leafIcon"));
        setClosedIcon(UIManager.getIcon("Tree.closedIcon"));
        setOpenIcon(UIManager.getIcon("Tree.openIcon"));
        
        setTextSelectionColor(UIManager.getColor("Tree.selectionForeground"));
        setTextNonSelectionColor(UIManager.getColor("Tree.textForeground"));
        setBackgroundSelectionColor(UIManager.getColor("Tree.selectionBackground"));
        setBackgroundNonSelectionColor(UIManager.getColor("Tree.textBackground"));
        setBorderSelectionColor(UIManager.getColor("Tree.selectionBorderColor"));
        Object value = UIManager.get("Tree.drawsFocusBorderAroundIcon");
        drawsFocusBorderAroundIcon = (value != null && ((Boolean)value).
                                      booleanValue());
    } // Fin du constructeur TreeRenderer
    
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
    
    /**
     * Mutateur de l'icone de noeud ouvert
     * @param newIcon la nouvelle icone
     */
    public void setOpenIcon(Icon newIcon) {
        openIcon = newIcon;
    } // Fin de la m?thode setOpenIcon/1
    
    /**
     * Accesseur de l'icone de noeud ouvert
     * @return une icone
     */
    public Icon getOpenIcon() {
        return openIcon;
    } // Fin de la m?thode getOpenIcon/0
    
    /**
     * Mutateur de l'icone de noeud ferm?
     * @param newIcon la nouvelle icone
     */
    public void setClosedIcon(Icon newIcon) {
        closedIcon = newIcon;
    } // Fin de la m?thode setClosedIcon/1
    
    /**
     * Accesseur de l'icone de noeud ferm?
     * @return une icone
     */
    public Icon getClosedIcon() {
        return closedIcon;
    } // Fin de la m?thode getClosedIcon/0
    
    /**
     * Mutateur de l'icone de noeud feuille
     * @param newIcon la nouvelle icone
     */
    public void setLeafIcon(Icon newIcon) {
        leafIcon = newIcon;
    } // Fin de la m?thode setLeafIcon/1
    
    /**
     * Accesseur de l'icone de noeud feuille
     * @return une icone
     */
    public Icon getLeafIcon() {
        return leafIcon;
    } // Fin de la m?thode getLeafIcon/0
    
    /**
     * Mutateur de foreground quand le noeud est s?lectionn?
     * @param newIcon la nouvelle couleur
     */
    public void setTextSelectionColor(Color newColor) {
        textSelectionColor = newColor;
    } // Fin de la m?thode setTextSelectionColor/1
    
    /**
     * Accesseur de foreground quand le noeud est s?lectionn?
     * @return une couleur
     */
    public Color getTextSelectionColor() {
        return textSelectionColor;
    } // Fin de la m?thode getTextSelectionColor/0
    
    /**
     * Mutateur de foreground quand le noeud n'est pas s?lectionn?
     * @param newIcon la nouvelle couleur
     */
    public void setTextNonSelectionColor(Color newColor) {
        textNonSelectionColor = newColor;
    } // Fin de la m?thode setTextNonSelectionColor/1
    
    /**
     * Accesseur de foreground quand le noeud n'est pas s?lectionn?
     * @return une couleur
     */
    public Color getTextNonSelectionColor() {
        return textNonSelectionColor;
    } // Fin de la m?thode getTextNonSelectionColor/0
    
    /**
     * Mutateur de background quand le noeud est s?lectionn?
     * @param newIcon la nouvelle couleur
     */
    public void setBackgroundSelectionColor(Color newColor) {
        backgroundSelectionColor = newColor;
    } // Fin de la m?thode setBackgroundSelectionColor/1
    
    
    /**
     * Accesseur de background quand le noeud est s?lectionn?
     * @return une couleur
     */
    public Color getBackgroundSelectionColor() {
        return backgroundSelectionColor;
    } // Fin de la m?thode getBackgroundSelectionColor/0
    
    /**
     * Mutateur de background quand le noeud n'est pas s?lectionn?
     * @param newIcon la nouvelle couleur
     */
    public void setBackgroundNonSelectionColor(Color newColor) {
        backgroundNonSelectionColor = newColor;
    } // Fin de la m?thode setBackgroundNonSelectionColor/1
    
    /**
     * Accesseur de background quand le noeud n'est pas s?lectionn?
     * @return une couleur
     */
    public Color getBackgroundNonSelectionColor() {
        return backgroundNonSelectionColor;
    } // Fin de la m?thode getBackgroundNonSelectionColor/0
    
    /**
     * Mutateur pour la couleur du border
     * @param newIcon la nouvelle couleur
     */
    public void setBorderSelectionColor(Color newColor) {
        borderSelectionColor = newColor;
    } // Fin de la m?thode setBorderSelectionColor/1
    
    /**
     * Accesseur de la couleur du border
     * @return une couleur
     */
    public Color getBorderSelectionColor() {
        return borderSelectionColor;
    } // Fin de la m?thode getBorderSelectionColor/0
    
    /**
     * Configuration du Renderer sur lequel est bas? l'arbre.
     * Configures the renderer based on the passed in components.
     * The value is set from messaging the tree with
     * <code>convertValueToText</code>, which ultimately invokes
     * <code>toString</code> on <code>value</code>.
     * The foreground color is set based on the selection and the icon
     * is set based on on leaf and expanded.
     */
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value,
                                                  boolean sel,
                                                  boolean expanded,
                                                  boolean leaf, int row,
                                                  boolean hasFocus) {
        String         stringValue = tree.convertValueToText(value, sel,
                                                             expanded, leaf, row, hasFocus);
        
        //this.tree = tree;
        this.hasFocus = hasFocus;
        setText(stringValue);
        if(sel)
            setForeground(getTextSelectionColor());
        else
            setForeground(getTextNonSelectionColor());
        // There needs to be a way to specify disabled icons.
        if (!tree.isEnabled()) {
            setEnabled(false);
        }
        else {
            setEnabled(true);
        }
        DefaultMutableTreeNode dmtcr = (DefaultMutableTreeNode)value;
        ImageIcon icon = new ImageIcon();
        if (dmtcr.getUserObject() instanceof Family && expanded) {
            icon = Tools.createAppletImageIcon(PATH_TO_GREEN_ICON,"");
        } else if (dmtcr.getUserObject() instanceof Family) {
            icon = Tools.createAppletImageIcon(PATH_TO_RED_ICON,"");
        } else if (dmtcr.getUserObject() instanceof TestList && expanded) {
            icon = Tools.createAppletImageIcon(PATH_TO_GRAY_ICON,"");
        } else if (dmtcr.getUserObject() instanceof TestList) {
            icon = Tools.createAppletImageIcon(PATH_TO_PINK_ICON,"");
        } else if (dmtcr.getUserObject() instanceof Test) {
            if (((Test)((DefaultMutableTreeNode)value).getUserObject()) instanceof ManualTest) {
                icon = Tools.createAppletImageIcon(PATH_TO_YELLOW_ICON,"");
            } else {
                icon = Tools.createAppletImageIcon(PATH_TO_BLUE_ICON,"");
            }
            
        } else if (dmtcr.getUserObject() instanceof Campaign && expanded) {
            icon = Tools.createAppletImageIcon(PATH_TO_BROWN_ICON,"");
        } else if (dmtcr.getUserObject() instanceof Campaign) {
            Campaign campaign = ((Campaign) dmtcr.getUserObject());
            if (campaignTerminated(campaign)) {
                icon = Tools.createAppletImageIcon(PATH_TO_SUCCESS_ICON,"");
            } else {
                icon = Tools.createAppletImageIcon(PATH_TO_PURPLE_ICON,"");
            }
        } else if (expanded){
            icon = Tools.createAppletImageIcon(PATH_TO_GREEN_ICON,"");
        } else {
            icon = Tools.createAppletImageIcon(PATH_TO_RED_ICON,"");
        }
        
        setIcon(icon);
        setComponentOrientation(tree.getComponentOrientation());
        
        selected = sel;
        
        return this;
    } // Fin de la m?thode getTreeCellRendererComponent/7
    
    
    /**
     * Red?finition de la m?thode paint.
     * @param g un objet Graphics
     */
    @Override
    public void paint(Graphics g) {
        Color bColor;
        
        if(selected) {
            bColor = getBackgroundSelectionColor();
        } else {
            bColor = getBackgroundNonSelectionColor();
            if(bColor == null)
                bColor = getBackground();
        }
        int imageOffset = -1;
        if(bColor != null) {
            //Icon currentI = getIcon();
            
            imageOffset = getLabelStart();
            g.setColor(bColor);
            if(getComponentOrientation().isLeftToRight()) {
                g.fillRect(imageOffset, 0, getWidth() - 1 - imageOffset,
                           getHeight());
            } else {
                g.fillRect(0, 0, getWidth() - 1 - imageOffset,
                           getHeight());
            }
        }
        
        if (hasFocus) {
            if (drawsFocusBorderAroundIcon) {
                imageOffset = 0;
            }
            else if (imageOffset == -1) {
                imageOffset = getLabelStart();
            }
            Color       bsColor = getBorderSelectionColor();
            
            if (bsColor != null) {
                g.setColor(bsColor);
                if(getComponentOrientation().isLeftToRight()) {
                    g.drawRect(imageOffset, 0, getWidth() - 1 - imageOffset,
                               getHeight() - 1);
                } else {
                    g.drawRect(0, 0, getWidth() - 1 - imageOffset,
                               getHeight() - 1);
                }
            }
        }
        super.paint(g);
    } // Fin de la m?thode paint/1
    
    /**
     * M?thode qui calcule le d?but du label (por prendre en compte l'icone)
     * @return l'indice de d?but du label
     */
    private int getLabelStart() {
        Icon currentI = getIcon();
        if(currentI != null && getText() != null) {
            return currentI.getIconWidth() + Math.max(0, getIconTextGap() - 1);
        }
        return 0;
    } // Fin de la m?thode getLabelStart/0
    
    
} // Fin de la classe TreeRenderer
