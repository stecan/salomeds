/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.ihm.admin.AskName;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.AskNewParameter;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * @author teaml039
 * @version : 0.1
 */
public class UsingParameterMouseListener extends MouseAdapter {
    
    boolean mustGiveValue;
    
    boolean singleSelection;
    
    public UsingParameterMouseListener(boolean withValue, boolean single) {
        super();
        mustGiveValue = withValue;
        singleSelection = single;
    }
    
    /**
     * Appel?e si on appuie sur un bouton de la souris
     * @param e un ?v?nement venant de la souris
     */
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getClickCount() > 1) {
            Object source = e.getSource();
            try {
                Component component = e.getComponent().getParent().getParent().getParent().getParent().getParent().getParent();
                if (component != null && component instanceof AskNewParameter) {
                    if (source instanceof JTable) {
                        int selectedRow = ((JTable)source).getSelectedRow();
                        if (selectedRow != -1) {
                            Parameter param = DataModel.getCurrentProject().getParameterFromModel((String)((JTable)source).getModel().getValueAt(selectedRow,0));
                            if (((AskNewParameter)component).getEnvironment() != null) {
                                AskName askName = new AskName(Language.getInstance().getText("Entrez_une_valeur__"), Language.getInstance().getText("Nouvelle_valeur"), Language.getInstance().getText("valeur"), null,SalomeTMFContext.getInstance().getSalomeFrame());
                                if (askName.getResult() != null) {
                                    if (param != null && !((AskNewParameter)component).getEnvironment().containsParameterInModel(param)) {
                                        ArrayList data = new ArrayList();
                                        data.add(param.getNameFromModel());
                                        data.add(askName.getResult());
                                        data.add(param.getDescriptionFromModel());
                                        DataModel.getEnvironmentParameterTableModel().addRow(data);
                                        ((AskNewParameter)component).getEnvironment().addParameterValueInModel(param, askName.getResult());
                                        ((AskNewParameter)component).dispose();
                                    } else {
                                        JOptionPane.showMessageDialog(component,
                                                                      Language.getInstance().getText("Le_parametre__")+ param.getNameFromModel() +Language.getInstance().getText("__existe_deja_pour_cet_environnement_"),
                                                                      Language.getInstance().getText("Erreur_"),
                                                                      JOptionPane.ERROR_MESSAGE);
                                    }
                                }
                            } else {
                                
                                if (param != null && !DataModel.getCurrentTest().getParameterListFromModel().contains(param)) {
                                    try {
                                        DataModel.getCurrentTest().setUseParamInDBAndModel(param);
                                    
                                        //DataModel.getCurrentTest().addParameter(param);
                                        ArrayList data = new ArrayList();
                                        data.add(param.getNameFromModel());
                                        data.add(param.getDescriptionFromModel());
                                        DataModel.getTestParameterTableModel().addRow(data);
                                        ((AskNewParameter)component).dispose();
                                    
                                    } catch (Exception exception) {
                                        Tools.ihmExceptionView(exception);
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(component,
                                                                  Language.getInstance().getText("Le_parametre_")+ param.getNameFromModel() +Language.getInstance().getText("_existe_deja_pour_ce_test_"),
                                                                  Language.getInstance().getText("Erreur_"),
                                                                  JOptionPane.ERROR_MESSAGE);
                                }
                            }
                            
                        }
                    }
                    
                }
            } catch (NullPointerException npe) {
            }
        }
    } // Fin de la m?thode mousePressed/1
    
} // Fin de la classe ExecutionResultMouseListener
