/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import org.objectweb.salome_tmf.ihm.main.ExecutionView;



/**
 * Classe qui repr?sente le mod?le d'un table
 * @author teaml039
 * @version 0.1
 */
public class ExecutionTableModel extends MyTableModel {
    
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur du mod?le de table
     */
    public ExecutionTableModel(){
        super();
    } // Fin du constructeur ExecutionTableModel/0
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    
    /**
     * Rend vrai si les cellules de la table sont ?ditables
     * @return toujours faux
     */
    @Override
    public boolean isCellEditable(int row, int col) {
        return (col == 0);
    } // Fin de la m?thode isCellEditable/2
    
    /**
     * Supprime la ligne dont l'?l?ment de la colonne 1 correspond ? la cha?ne
     * pass?e en param?tre.
     * @param key
     */
    @Override
    public void removeRow(String key) {
        for (int i=0; i < getRowCount(); i++) {
            if (getValueAt(i,1).equals(key)) {
                removeData(i);
                break;
            }
        }
        
        ExecutionView.getTable().getColumnModel().getColumn(0).setMaxWidth(18);
        ExecutionView.getTable().getColumnModel().getColumn(0).setPreferredWidth(18);
        
    }
    
    
} // Fin de la classe MyTableModel
