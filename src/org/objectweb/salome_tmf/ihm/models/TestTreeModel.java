/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;

import java.awt.Frame;
import java.util.Hashtable;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.filtre.ISimpleDataFilter;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui d?finit le mod?le de donn?es de l'arbre.
 *
 * @author teaml039
 * @version 0.1
 */
public class TestTreeModel extends DefaultTreeModel implements ApiConstants,
                                                               IHMConstants {

    /******************************************************************************/
    /** CONSTRUCTEUR ***/
    /******************************************************************************/
    protected ISimpleDataFilter m_filter;
    protected JTree testTree;

    /**
     * Constructeur du mod?le de donn?es de l'arbre
     *
     * @param root
     *            la racine de l'arbre
     */
    public TestTreeModel(DefaultMutableTreeNode root, JTree testTree,
                         ISimpleDataFilter filter) {
        super(root);
        m_filter = filter;
        this.testTree = testTree;
    } // Fin du constructeur TreeModel/1

    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/

    /**
     * On surcharge la m?thode de DefaultTreeModel afin de prendre en compte la
     * nature sp?cifique des noeuds de l'arbre.
     */
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

        DefaultMutableTreeNode aNode = (DefaultMutableTreeNode) path
            .getLastPathComponent();
        if (Permission.canUpdateTest()
            && ((DefaultMutableTreeNode) this.getRoot()).getUserObject()
            .equals(TEST_ROOT_NAME)) {

            if (aNode.getUserObject() instanceof Test) {

                if (!DataModel.getCurrentProject()
                    .containsTestInModel(
                                         ((TestList) ((DefaultMutableTreeNode) aNode
                                                      .getParent()).getUserObject()),
                                         (String) newValue)
                    && !Test.isInBase(
                                      (TestList) ((DefaultMutableTreeNode) aNode
                                                  .getParent()).getUserObject(),
                                      (String) newValue)) {
                    // String oldName = ((Test)aNode.getUserObject()).getName();
                    try {
                        ((Test) aNode.getUserObject()).updateInDBAndModel(
                                                                          (String) newValue, ((Test) aNode
                                                                                              .getUserObject())
                                                                          .getDescriptionFromModel());

                        if (aNode.getUserObject() instanceof ManualTest) {
                            // SalomeTMFPanels.getManualTestNameLabel().setText(Language.getInstance().getText("Nom_du_test__")
                            // + (String)newValue);
                            SalomeTMFPanels.setTestPanelTestInfo(
                                                                 DataConstants.MANUAL_TEST,
                                                                 (String) newValue, null, null);
                        } else {
                            // SalomeTMFPanels.getAutomaticTestNameLabel().setText(Language.getInstance().getText("Nom_du_test__")
                            // + (String)newValue);
                            SalomeTMFPanels.setTestPanelTestInfo(
                                                                 DataConstants.AUTOMATIC_TEST,
                                                                 (String) newValue, null, null);
                        }
                        // ((Test)aNode.getUserObject()).setName((String)newValue);
                        nodeChanged(aNode);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                } else if (DataModel.getCurrentProject()
                           .getTestFromModel(
                                             ((TestList) ((DefaultMutableTreeNode) aNode
                                                          .getParent()).getUserObject())
                                             .getFamilyFromModel()
                                             .getNameFromModel(),
                                             ((TestList) ((DefaultMutableTreeNode) aNode
                                                          .getParent()).getUserObject())
                                             .getNameFromModel(), (String) newValue) == null
                           || !DataModel.getCurrentProject().getTestFromModel(
                                                                              ((TestList) ((DefaultMutableTreeNode) aNode
                                                                                           .getParent()).getUserObject())
                                                                              .getFamilyFromModel()
                                                                              .getNameFromModel(),
                                                                              ((TestList) ((DefaultMutableTreeNode) aNode
                                                                                           .getParent()).getUserObject())
                                                                              .getNameFromModel(), (String) newValue)
                           .equals(aNode.getUserObject())) {
                    JOptionPane
                        .showMessageDialog(
                                           new Frame(),
                                           Language
                                           .getInstance()
                                           .getText(
                                                    "Ce_nom_de_test_existe_deja_dans_cette_suite_"),
                                           Language.getInstance().getText("Erreur_"),
                                           JOptionPane.ERROR_MESSAGE);
                }

            } else if (aNode.getUserObject() instanceof TestList
                       && !((TestList) aNode.getUserObject())
                       .equals(DEFAULT_TESTLIST_NAME)) {

                if (!DataModel.getCurrentProject()
                    .containsTestListInModel(
                                             ((TestList) aNode.getUserObject())
                                             .getFamilyFromModel(),
                                             (String) newValue)
                    && !TestList.isInBase(
                                          ((TestList) aNode.getUserObject())
                                          .getFamilyFromModel(),
                                          (String) newValue)) {
                    // String oldName =
                    // ((TestList)aNode.getUserObject()).getName();
                    try {
                        ((TestList) aNode.getUserObject()).updateInDBAndModel(
                                                                              (String) newValue, ((TestList) aNode
                                                                                                  .getUserObject())
                                                                              .getDescriptionFromModel());
                        // ((TestList)aNode.getUserObject()).setName((String)newValue);
                        nodeChanged(aNode);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                } else if (DataModel.getCurrentProject().getTestListFromModel(
                                                                              ((TestList) aNode.getUserObject()).getFamilyFromModel()
                                                                              .getNameFromModel(), (String) newValue) == null
                           || !DataModel.getCurrentProject().getTestListFromModel(
                                                                                  ((TestList) aNode.getUserObject())
                                                                                  .getFamilyFromModel()
                                                                                  .getNameFromModel(), (String) newValue)
                           .equals(aNode.getUserObject())) {
                    JOptionPane
                        .showMessageDialog(
                                           new Frame(),
                                           Language
                                           .getInstance()
                                           .getText(
                                                    "Le_nom_de_la_suite_existe_deja_dans_cette_famille_"),
                                           Language.getInstance().getText("Erreur_"),
                                           JOptionPane.ERROR_MESSAGE);
                }

            } else if (aNode.getUserObject() instanceof Family
                       && !((Family) aNode.getUserObject()).getNameFromModel()
                       .equals(DEFAULT_FAMILY_NAME)) {
                if (!DataModel.getCurrentProject().containsFamilyInModel(
                                                                         (String) newValue)
                    && !Family.isInBase(DataModel.getCurrentProject(),
                                        (String) newValue)) {
                    // String oldName =
                    // ((Family)aNode.getUserObject()).getName();
                    try {
                        ((Family) aNode.getUserObject()).updateInDBAndModel(
                                                                            (String) newValue, ((Family) aNode
                                                                                                .getUserObject())
                                                                            .getDescriptionFromModel());
                        // ((Family)aNode.getUserObject()).setName((String)newValue);
                        nodeChanged(aNode);
                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                } else if (DataModel.getCurrentProject().getFamilyFromModel(
                                                                            (String) newValue) == null
                           || !DataModel.getCurrentProject().getFamilyFromModel(
                                                                                (String) newValue)
                           .equals(aNode.getUserObject())) {
                    JOptionPane.showMessageDialog(new Frame(), Language
                                                  .getInstance()
                                                  .getText("Cette_famille_existe_deja_"), Language
                                                  .getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }

            }
        } else if (Permission.canUpdateCamp()
                   && ((DefaultMutableTreeNode) this.getRoot()).getUserObject()
                   .equals(CAMPAIGN_ROOT_NAME)) {
            if (aNode.getUserObject() instanceof Campaign) {
                if (!DataModel.getCurrentProject().containsCampaignInModel(
                                                                           (String) newValue)
                    && !Campaign.isInBase(DataModel.getCurrentProject(),
                                          (String) newValue)) {
                    // SalomeTMFPanels.getCampaignNameLabel().setText(Language.getInstance().getText("Nom_de_la_campagne__")
                    // +(String)newValue);
                    SalomeTMFPanels.setCampPanelInfo((String) newValue, null,
                                                     null);
                    // String oldName =
                    // ((Campaign)aNode.getUserObject()).getName();
                    try {
                        Campaign camp = (Campaign) aNode.getUserObject();

                        // BdD
                        camp.updateInDBAndModel((String) newValue, camp
                                                .getDescriptionFromModel());

                        // IHM
                        nodeChanged(aNode);

                    } catch (Exception exception) {
                        Tools.ihmExceptionView(exception);
                    }
                } else if (DataModel.getCurrentProject().getCampaignFromModel(
                                                                              (String) newValue) == null
                           || !DataModel.getCurrentProject().getCampaignFromModel(
                                                                                  (String) newValue)
                           .equals(aNode.getUserObject())) {
                    // ((Campaign)aNode.getUserObject()).setName(oldName);
                    JOptionPane.showMessageDialog(new Frame(), Language
                                                  .getInstance().getText(
                                                                         "Le_nom_de_la_campagne_existe_deja_"),
                                                  Language.getInstance().getText("Erreur_"),
                                                  JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    } // Fin de la m?thode valueForPathChanged

    /*************** Methode pour le filtrage des elements *****************************/
    public void setFilter(ISimpleDataFilter filter) {
        m_filter = filter;
    }

    public void setFiltered(boolean pass) {
        TreePath pTreePath = testTree.getSelectionPath();
        Hashtable<TreePath, Boolean> state = getExpansionState(testTree);
        // Util.debug("TreePath : " + pTreePath);
        m_filter.setActived(pass);
        Object[] path = { root };
        int[] childIndices = new int[root.getChildCount()];
        Object[] children = new Object[root.getChildCount()];
        for (int i = 0; i < root.getChildCount(); i++) {
            childIndices[i] = i;
            children[i] = root.getChildAt(i);
        }
        fireTreeStructureChanged(this, path, childIndices, children);
        if (pTreePath != null) {

            DefaultMutableTreeNode pNode = (DefaultMutableTreeNode) pTreePath
                .getLastPathComponent();

            if (pNode.getUserObject() instanceof SimpleData) {
                SimpleData data = (SimpleData) pNode.getUserObject();

                testTree.setSelectionPath(pTreePath);
                if (!m_filter.isFiltred(data)) {
                    boolean trouve = false;
                    while (!trouve && pTreePath != null) {
                        pTreePath = pTreePath.getParentPath();
                        if (pTreePath != null) {
                            pNode = (DefaultMutableTreeNode) pTreePath
                                .getLastPathComponent();
                            Object obj = pNode.getUserObject();
                            if (obj instanceof SimpleData) {
                                data = (SimpleData) obj;
                                if (m_filter.isFiltred(data)) {
                                    trouve = true;
                                }
                            }
                        }
                    }
                    testTree.setSelectionPath(pTreePath);
                }
            }
        }
        restoreExpanstionState(testTree, state);
    }

    @Override
    public int getChildCount(Object parent) {
        if (m_filter == null) {
            return super.getChildCount(parent);
        }
        int realCount = super.getChildCount(parent), filterCount = 0;
        ;
        for (int i = 0; i < realCount; i++) {
            DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode) super
                .getChild(parent, i);
            if (m_filter.pass(dmtn))
                filterCount++;
        }
        return filterCount;
    }

    @Override
    public Object getChild(Object parent, int index) {
        if (m_filter == null) {
            return super.getChild(parent, index);
        }
        int cnt = -1;
        for (int i = 0; i < super.getChildCount(parent); i++) {
            Object child = super.getChild(parent, i);
            if (m_filter.pass(child))
                cnt++;
            if (cnt == index)
                return child;
        }
        return null;
    }

    /********************* UTIL **************************/

    // is path1 descendant of path2
    public boolean isDescendant(TreePath path1, TreePath path2) {
        int count1 = path1.getPathCount();
        int count2 = path2.getPathCount();
        if (count1 <= count2)
            return false;
        while (count1 != count2) {
            path1 = path1.getParentPath();
            count1--;
        }
        return path1.equals(path2);
    }

    public Hashtable<TreePath, Boolean> getExpansionState(JTree tree) {
        Hashtable<TreePath, Boolean> statePath = new Hashtable<TreePath, Boolean>();
        int nbRow = testTree.getRowCount();
        for (int row = 0; row < nbRow; row++) {
            TreePath rowPath = tree.getPathForRow(row);
            int rowCount = tree.getRowCount();
            for (int i = row; i < rowCount; i++) {
                TreePath path = tree.getPathForRow(i);
                if (i == row || isDescendant(path, rowPath)) {
                    if (tree.isExpanded(path)) {
                        statePath.put(path, true);
                    }
                } else
                    break;
            }
        }
        return statePath;
    }

    public void restoreExpanstionState(JTree tree,
                                       Hashtable<TreePath, Boolean> statePath) {
        int nbRow = testTree.getRowCount();
        for (int row = 0; row < nbRow; row++) {
            // TreePath rowPath = tree.getPathForRow(row);
            int rowCount = tree.getRowCount();
            for (int i = row; i < rowCount; i++) {
                TreePath path = tree.getPathForRow(i);
                Boolean expanded = statePath.get(path);
                if (expanded != null) {
                    tree.expandRow(tree.getRowForPath(path));
                }
            }
        }
    }
}// Fin de la classe TreeModel

