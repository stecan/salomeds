/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.models;


import javax.swing.tree.DefaultMutableTreeNode;

public class TestListFamilyCampaignTreeNode {

    DefaultMutableTreeNode campaignNode;

    DefaultMutableTreeNode familyNode;
        
    DefaultMutableTreeNode testListNode;
        
    /**
     * @return
     */
    public DefaultMutableTreeNode getFamilyNode() {
        return familyNode;
    }

    /**
     * @return
     */
    public DefaultMutableTreeNode getTestListNode() {
        return testListNode;
    }

    /**
     * @param node
     */
    public void setFamilyNode(DefaultMutableTreeNode node) {
        familyNode = node;
    }

    /**
     * @param node
     */
    public void setTestListNode(DefaultMutableTreeNode node) {
        testListNode = node;
    }

    @Override
    public String toString() {
        if (campaignNode == null) {
            if (testListNode == null) {
                return familyNode.toString();   
            } else {
                return familyNode.toString() + " / " + testListNode.toString();
            }
        } else if (testListNode == null) {
            if (familyNode == null) {
                return campaignNode.toString();
            } else {
                return campaignNode.toString() + " / " + familyNode.toString();
            }
        } else {
            return campaignNode.toString() + " / " + familyNode.toString() + " / " + testListNode.toString(); 
        }
                 
    }
    /**
     * @return
     */
    public DefaultMutableTreeNode getCampaignNode() {
        return campaignNode;
    }

    /**
     * @param node
     */
    public void setCampaignNode(DefaultMutableTreeNode node) {
        campaignNode = node;
    }

} // Fin de la classe TestListFamilyTreeNode
