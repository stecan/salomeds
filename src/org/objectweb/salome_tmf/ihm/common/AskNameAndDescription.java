/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.objectweb.salome_tmf.data.AdminProjectData;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Group;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer les
 * informations pour un nouveau parametre, un nouveau groupe ou une nouvelle
 * famille.
 * @author teaml039
 * @version 0.1
 */
public class AskNameAndDescription extends JDialog implements DataConstants {
    
    /**
     * le champ pour entrer le nom du parametre
     */
    JTextField textFirst;
    
    /**
     * le champ description
     */
    JTextArea descriptionArea;
    
    /**
     * Un parametre
     */
    Parameter parameter;
    
    /**
     * Le type de donnee a recuperer
     */
    int givenType;
    
    /**
     * Un groupe
     */
    Group group;
    
    /**
     * Une famille
     */
    Family family;
    
    AdminProjectData pAdminProjectData;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre permettant de demander deux informations.
     * @param type le type de donnee a recuperer
     * @param title le titre de la fenetre
     * @param question le label proposant le texte a entrer
     * @param name le nom a placer dans le texteField
     * @param descr texte a placer dans la description
     */
    public AskNameAndDescription(int type, String title, String question, SimpleData elem, Frame owner, AdminProjectData adminProjectData) {
        super(owner, true);
        pAdminProjectData = adminProjectData;
        textFirst = new JTextField(15);
        descriptionArea = new JTextArea(10,20);
        JPanel page = new JPanel();
        
        
        
        JLabel promptFirst = new JLabel(question);
        
        
        JPanel onlyTextPanel = new JPanel();
        onlyTextPanel.setLayout(new BoxLayout(onlyTextPanel,BoxLayout.X_AXIS));
        onlyTextPanel.add(Box.createRigidArea(new Dimension(0,10)));
        onlyTextPanel.add(promptFirst);
        onlyTextPanel.add(textFirst);
        
        
        JScrollPane descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,10)));
        textPanel.add(onlyTextPanel);
        textPanel.add(Box.createRigidArea(new Dimension(10,10)));
        textPanel.add(descriptionScrollPane);
        
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (!textFirst.getText().trim().equals("")) {
                        if (givenType == GROUP) {
                            if (pAdminProjectData.getCurrentProjectFromModel().getGroupFromModel(textFirst.getText().trim()) == null) {
                                group.updateNameInModel(textFirst.getText().trim());
                                group.updateDescriptionInModel(descriptionArea.getText());
                                AskNameAndDescription.this.dispose();
                            } else if (pAdminProjectData.getCurrentProjectFromModel().getGroupFromModel(textFirst.getText().trim()).equals(group)) {
                                group.updateNameInModel(textFirst.getText().trim());
                                group.updateDescriptionInModel(descriptionArea.getText());
                                AskNameAndDescription.this.dispose();
                            } else {
                                JOptionPane.showMessageDialog(AskNameAndDescription.this,
                                                              Language.getInstance().getText("Ce_nom_de_groupe_existe_deja_"),
                                                              Language.getInstance().getText("Erreur_"),
                                                              JOptionPane.ERROR_MESSAGE);
                            }
                        } else if (givenType == FAMILY) {
                            family.updateInModel(textFirst.getText().trim(), descriptionArea.getText());
                            AskNameAndDescription.this.dispose();
                        } else {
                            parameter.updateDescriptionInModel(descriptionArea.getText());
                            parameter.updateNameInModel(textFirst.getText().trim());
                            AskNameAndDescription.this.dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(AskNameAndDescription.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    parameter = null;
                    family = null;
                    group = null;
                    AskNameAndDescription.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);
        
        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);
        
        initData(elem, type);
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    parameter = null;
                    family = null;
                    group = null;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        this.setTitle(title);
        //this.setLocation(400,300);
        this.setLocationRelativeTo(this.getParent()); 
        this.pack();
        this.setVisible(true);
    } // Fin du constructeur AskNewParameter/5
    
    /**
     * Le nom et la description sont vides.
     * @param type le type de donnee a recuperer
     * @param title le titre de la fenetre
     * @param question le label proposant le texte a entrer
     */
    public AskNameAndDescription(int type, String title, String question, Frame owner, AdminProjectData adminProjectData) {
        this(type, title, question, null, owner, adminProjectData);
    } // Fin du constructeur AskNewParameter/3
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Retourne le parametre cree
     * @return le parametre cree
     */
    public Parameter getNewParameter() {
        return parameter;
    } // Fin de la methode getNewParameter/0
    
    /**
     * Retourne le groupe cree
     * @return le groupe cree
     */
    public Group getGroup() {
        return group;
    } // Fin de la methode getGroup/0
    
    /**
     * Retourne la famille creee
     * @return la famille creee
     */
    public Family getFamily() {
        return family;
    } // Fin de la methode getFamily/0
    
    private void initData(SimpleData elem, int type) {
        givenType = type;
        if (elem != null) {
            textFirst.setText(elem.getNameFromModel());
            descriptionArea.setText(elem.getDescriptionFromModel());
            if (elem instanceof Group) {
                group = (Group)elem;
            } else if (elem instanceof Parameter) {
                parameter = (Parameter)elem;
            } else if (elem instanceof Family) {
                family = (Family)elem;
            }
            if (givenType == PARAMETER) {
                textFirst.setEnabled(false);
            }
        } else {
            if (givenType == GROUP) {
                group = new Group("","");
            } else if (givenType == PARAMETER) {
                parameter = new Parameter("", "");
            } else if (givenType == FAMILY) {
                family = new Family("", "");
            }
        }
    }
} // Fin de la classe AskNewParameter
