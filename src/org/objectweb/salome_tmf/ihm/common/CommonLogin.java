package org.objectweb.salome_tmf.ihm.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;
import javax.naming.ldap.LdapContext;

import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.api.sql.ISQLSession;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.login.ActiveDirectory;

public class CommonLogin  extends JPanel implements HyperlinkListener {
    static int id = 0;
    public static String ACTION_START_SALOME = "ACTION_START_SALOME"; 
    public static String ACTION_START_PROJECT_ADMIN = "ACTION_START_PROJECT_ADMIN";
    public static String ACTION_START_ADMIN_SALOME = "ACTION_START_ADMIN_SALOME"; 
        
        
        
    private javax.swing.JButton b_start;
    private javax.swing.JButton b_start1;
    private javax.swing.JButton b_start2;
    private javax.swing.JEditorPane jEditorPane2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JComboBox languages_List;
    private javax.swing.JPasswordField password_Field;
    private javax.swing.JPasswordField password_Field1;
    private javax.swing.JPasswordField password_Field2;
    private javax.swing.JComboBox project_List;
    private javax.swing.JComboBox project_List1;
    private javax.swing.JComboBox user_List;
    private javax.swing.JComboBox user_List1;
        
        
    Vector porjectsList;
    Vector usersProject;
    Vector adminsProject;
    UserWrapper superUser = null;
        
    Vector languages = new Vector(); 
        
    ISQLSession pISQLSession;
    ISQLProject pISQLProject;
    ISQLPersonne pISQLPersonne;
        
    //File salome_html = null;
        
    boolean init;
    String proj;
    String proj1;
    String admin_pwd = "";
    String usedLocale = "";
    boolean closeDB = true;
        
        
    boolean projectEnable;
    boolean adminProjectEnable;
    boolean adminSalomeEnable;
    ActionListener pActionListener;
        
    public CommonLogin(boolean _projectEnable, boolean _adminProjectEnable, boolean _adminSalomeEnable, ActionListener _pActionListener) {
                
        projectEnable = _projectEnable;
        adminProjectEnable = _adminProjectEnable;
        adminSalomeEnable = _adminSalomeEnable;
        pActionListener = _pActionListener;
        connectToAPI();
        System.runFinalization();
        System.gc();
        if (Api.isConnected()){
            initData();
            initComponents();
            init = true;
            initList();
            init = false;
        }
    }
        
        
    /*********************************************************************************************/
        
    void connectToAPI() {
                
        Util.log("[LoginSalomeTMF->connectToAPI] nb connection active : " + Api.getNbConnect());
        pISQLSession = Api.getISQLObjectFactory().getISQLSession();
        pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
                
        usedLocale = Api.getUsedLocale();
        Util.log("[LoginSalomeTMF->connectToAPI] Used Local is "+ usedLocale);
        Language.getInstance().setLocale(new Locale(usedLocale));
        languages = Api.getLocales();
        Util.log("[LoginSalomeTMF->connectToAPI] Available Languages is "+ languages);
        if (languages == null){
            Util.log("[LoginSalomeTMF->connectToAPI] Set default languages list to fr");
            languages = new Vector();
            languages.add("fr");
        }
    }
        
        
        
    void initData(){
        //
        try {
            ProjectWrapper[] tmpProjectArray = pISQLProject.getAllProjects();
            Vector tmpProjectVector = new Vector();
            for(int i = 0; i < tmpProjectArray.length; i++) {
                tmpProjectVector.add(tmpProjectArray[i]);
            }
            porjectsList = tmpProjectVector;
            Util.log("[LoginSalomeTMF->initData] Projects list is "+ porjectsList);
            if (porjectsList.size()>0) {
                ProjectWrapper pProjectWrapper = (ProjectWrapper) porjectsList.elementAt(0);
                                
                UserWrapper[] tmpUserArray = pISQLProject.getUsersOfProject(pProjectWrapper.getName());
                Vector tmpUserVector = new Vector();
                for(int i = 0; i < tmpUserArray.length; i++) {
                    tmpUserVector.add(tmpUserArray[i]);
                }
                usersProject = tmpUserVector;
                                
                Util.log("[LoginSalomeTMF->initData] Users Project list is "+  usersProject + ", for project "+ pProjectWrapper.getName());
                                
                UserWrapper[] tmpAdminArray = pISQLProject.getUsersOfProject(pProjectWrapper.getName());
                Vector tmpAdminVector = new Vector();
                for(int i = 0; i < tmpAdminArray.length; i++) {
                    tmpAdminVector.add(tmpAdminArray[i]);
                }
                adminsProject = tmpAdminVector;
                                
                Util.log("[LoginSalomeTMF->initData] Admins project list is "+  adminsProject+ ", for project "+ pProjectWrapper.getName());
            } else {
                usersProject = new Vector();
                adminsProject = new Vector();
            }
            superUser = pISQLPersonne.getUserByLogin("AdminSalome");
            if (superUser != null){
                admin_pwd = superUser.getPassword();
                Util.log("[LoginSalomeTMF->initData] superUser is "+ superUser);
            }
        } catch (Exception e ){
            Util.log("[LoginSalomeTMF->initData]" + e);
            porjectsList = null;
        }
    }
        
        
        
        
        
        
    private void projectChange(ProjectWrapper pProjectWrapper){
        String project = pProjectWrapper.getName();
        proj = project;
        if (!init){
            try {
                proj = project;
                //users = db_AdminVT.getAllUserOfProject(proj);
                                
                UserWrapper[] tmpUserArray = pISQLProject.getUsersOfProject(project);
                Vector tmpUserVector = new Vector();
                for(int i = 0; i < tmpUserArray.length; i++) {
                    tmpUserVector.add(tmpUserArray[i]);
                }
                usersProject = tmpUserVector;
                                
                                
                user_List.removeAllItems();
                                
                //Enumeration e = users.keys();
                Enumeration e = usersProject.elements();
                while (e.hasMoreElements() ) {
                    Object pUser = e.nextElement();
                    Util.log("[LoginSalomeTMF->projectChange] add user_List " + pUser);
                    user_List.addItem(pUser);
                }
            } catch (Exception e ){
                                
            }
        }
    }
        
    private void projectChange1(ProjectWrapper pProjectWrapper){
        String project = pProjectWrapper.getName();
        proj1 = project;
        if (!init){
            try {
                proj1 = project;
                //users1 = db_AdminVT.getAllAdminOfProject(proj1);
                                
                UserWrapper[] tmpAdminArray = pISQLProject.getAdminsOfProject(project);
                Vector tmpAdminVector = new Vector();
                for(int i = 0; i < tmpAdminArray.length; i++) {
                    tmpAdminVector.add(tmpAdminArray[i]);
                }
                adminsProject = tmpAdminVector;
                user_List1.removeAllItems();
                                
                //Enumeration e = users1.keys();
                Enumeration e = adminsProject.elements();
                while (e.hasMoreElements() ) {
                    Object pUser = e.nextElement();
                    Util.log("[LoginSalomeTMF->projectChange1] add user_List1 " + pUser);
                    user_List1.addItem(pUser);
                }       
            }
            catch (Exception e ){
                                
            }
        }
    }
        
    private UserWrapper getUser(){
        return  (UserWrapper) user_List.getSelectedItem();
    }
        
    private UserWrapper getUser1(){
        return  (UserWrapper) user_List1.getSelectedItem();
    }
        
    public String getPassword() {
        return new String(password_Field.getPassword());
    }
        
    public String getPassword1() {
        return new String(password_Field1.getPassword());
    }
        
    public String getPassword2() {
        return new String(password_Field2.getPassword());
    }
        
    private  void initList(){
        project_List.removeAllItems();
        project_List1.removeAllItems();
        user_List.removeAllItems();
        user_List1.removeAllItems();
        languages_List.removeAllItems();
        user_List.removeAllItems();
        for (int i = 0 ; i < porjectsList.size(); i++) {
            project_List.addItem(porjectsList.elementAt(i));
            project_List1.addItem(porjectsList.elementAt(i));
        } 
                
        Enumeration e = usersProject.elements();
        while (e.hasMoreElements() ) {
            Object pUser = e.nextElement();
            Util.log("[LoginSalomeTMF->initList] add user_List " + pUser);
            user_List.addItem(pUser);
        }
        e = adminsProject.elements();
        while (e.hasMoreElements() ) {
            Object pUser = e.nextElement();
            Util.log("[LoginSalomeTMF->initList] add user_List1 " + pUser);
            user_List1.addItem(pUser);
        }
                
        for(int j=0 ; j < languages.size() ; j++) {
            /*Locale locale = new Locale((String)languages.elementAt(j));
              languages_List.addItem(locale.getDisplayName());*/
            languages_List.addItem(((String)languages.elementAt(j)).trim());
        }
        //languages_List.setSelectedItem(new Locale(Api.LOCALE).getDisplayName());
        languages_List.setSelectedItem(usedLocale);
                
    }
        
    /** This method is called from within the init() method to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        project_List = new javax.swing.JComboBox();
        user_List = new javax.swing.JComboBox();
        password_Field = new javax.swing.JPasswordField();
        password_Field.addKeyListener(new passwordKeyListener(0));
                
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        b_start = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        project_List1 = new javax.swing.JComboBox();
        user_List1 = new javax.swing.JComboBox();
        password_Field1 = new javax.swing.JPasswordField();
        password_Field1.addKeyListener(new passwordKeyListener(1));
                
        jPanel8 = new javax.swing.JPanel();
        b_start1 = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        password_Field2 = new javax.swing.JPasswordField();
        password_Field2.addKeyListener(new passwordKeyListener(2));
                
        jPanel12 = new javax.swing.JPanel();
        b_start2 = new javax.swing.JButton();
        jPanel13 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        languages_List = new javax.swing.JComboBox();
        jLabel19 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jEditorPane2 = new javax.swing.JEditorPane();
                
        setForeground(new java.awt.Color(51, 51, 51));
        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(1, 1, 1, 1)));
        jTabbedPane1.setForeground(new java.awt.Color(255, 102, 0));
        jTabbedPane1.setOpaque(true);
        jPanel4.setLayout(new java.awt.BorderLayout());
                
        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setForeground(new java.awt.Color(51, 51, 51));
        jPanel1.setLayout(new java.awt.GridLayout(2, 3, 10, 0));
                
        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setText(Language.getInstance().getText("Projet"));
        jLabel2.setOpaque(true);
        jPanel1.add(jLabel2);
                
        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setText(Language.getInstance().getText("Utilisateur"));
        jLabel3.setOpaque(true);
        jPanel1.add(jLabel3);
                
        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel4.setOpaque(true);
        jPanel1.add(jLabel4);
                
        project_List.setBackground(new java.awt.Color(255, 204, 0));
        project_List.setName("l_projet");
                
        project_List.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    project_ListItemStateChanged(evt);
                }
            });
                
        jPanel1.add(project_List);
                
        user_List.setBackground(new java.awt.Color(255, 204, 0));
                
                
        jPanel1.add(user_List);
                
        jPanel1.add(password_Field);
                
        jPanel4.add(jPanel1, java.awt.BorderLayout.CENTER);
                
        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel1.setText(Language.getInstance().getText("Se_connecter_a_Salome_TMF"));
        jPanel2.add(jLabel1);
                
        jPanel4.add(jPanel2, java.awt.BorderLayout.NORTH);
                
        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setForeground(new java.awt.Color(51, 51, 51));
                
        b_start.setText(Language.getInstance().getText("Demarrer_Salome_TMF"));
        b_start.setActionCommand(ACTION_START_SALOME);
        b_start.addActionListener(pActionListener);
                
                
                
                
        jPanel3.add(b_start);
                
        jPanel4.add(jPanel3, java.awt.BorderLayout.SOUTH);
        if (projectEnable) {
            jTabbedPane1.addTab(Language.getInstance().getText("Demarrer_Salome_TMF"), jPanel4);
        }
        jPanel5.setLayout(new java.awt.BorderLayout());
                
        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setForeground(new java.awt.Color(51, 51, 51));
        jPanel7.setLayout(new java.awt.GridLayout(2, 3, 10, 0));
                
        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setText(Language.getInstance().getText("Projet"));
        jLabel5.setOpaque(true);
        jPanel7.add(jLabel5);
                
        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setText(Language.getInstance().getText("Utilisateur"));
        jLabel6.setOpaque(true);
        jPanel7.add(jLabel6);
                
        jLabel7.setBackground(new java.awt.Color(255, 255, 255));
        jLabel7.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel7.setOpaque(true);
        jPanel7.add(jLabel7);
                
        project_List1.setBackground(new java.awt.Color(255, 204, 0));
        project_List1.setName("l_projet");
        project_List1.addItemListener(new java.awt.event.ItemListener() {
                @Override
                public void itemStateChanged(java.awt.event.ItemEvent evt) {
                    project_List1ItemStateChanged(evt);
                }
            });
                
        jPanel7.add(project_List1);
                
        user_List1.setBackground(new java.awt.Color(255, 204, 0));
        jPanel7.add(user_List1);
                
        jPanel7.add(password_Field1);
                
        jPanel5.add(jPanel7, java.awt.BorderLayout.CENTER);
                
        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
                
        b_start1.setText(Language.getInstance().getText("Administrer_le_projet"));
        b_start1.setActionCommand(ACTION_START_PROJECT_ADMIN);
        b_start1.addActionListener(pActionListener);
                
                
                
                
                
        jPanel8.add(b_start1);
                
        jPanel5.add(jPanel8, java.awt.BorderLayout.SOUTH);
                
        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel8.setText(Language.getInstance().getText("Se_connecter_en_Administrateur_de_projet"));
        jPanel9.add(jLabel8);
        jLabel8.getAccessibleContext().setAccessibleName(Language.getInstance().getText("Se_connecter_en_Administrateur_de_projet"));
                
        jPanel5.add(jPanel9, java.awt.BorderLayout.NORTH);
                
        if (adminProjectEnable) {
            jTabbedPane1.addTab(Language.getInstance().getText("Administrer_un_projet"), jPanel5);
        }
                
        jPanel6.setLayout(new java.awt.BorderLayout());
                
        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setForeground(new java.awt.Color(51, 51, 51));
        jPanel10.setLayout(new java.awt.BorderLayout());
                
        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setLayout(new java.awt.GridLayout(2, 2, 10, 0));
                
        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setText(Language.getInstance().getText("Utilisateur"));
        jLabel10.setOpaque(true);
        jPanel11.add(jLabel10);
                
        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setText(Language.getInstance().getText("Mot_de_passe"));
        jLabel11.setOpaque(true);
        jPanel11.add(jLabel11);
                
        jTextField1.setBackground(new java.awt.Color(255, 204, 0));
        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Dialog", 1, 12));
        jTextField1.setText("AdminSalome");
        jPanel11.add(jTextField1);
                
        jPanel11.add(password_Field2);
                
        jPanel10.add(jPanel11, java.awt.BorderLayout.CENTER);
                
        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
                
        b_start2.setText(Language.getInstance().getText("Administrer_Salome_TMF"));
        b_start2.setActionCommand("ACTION_START_ADMIN_SALOME");
        b_start2.addActionListener(pActionListener);
                
                
        jPanel12.add(b_start2);
                
        jPanel10.add(jPanel12, java.awt.BorderLayout.SOUTH);
                
        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel12.setText(Language.getInstance().getText("Se_connecter_en_administrateur_de_Salome_TMF"));
        jPanel13.add(jLabel12);
                
        jPanel10.add(jPanel13, java.awt.BorderLayout.NORTH);
                
        jPanel6.add(jPanel10, java.awt.BorderLayout.CENTER);
                
        if (adminSalomeEnable){
            jTabbedPane1.addTab(Language.getInstance().getText("Administrer_Salome_TMF"), jPanel6);
        }
        jPanel14.setLayout(new java.awt.BorderLayout());
                
        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setLayout(new java.awt.BorderLayout());
                
        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Dialog", 1, 24));
        jLabel15.setText(Language.getInstance().getText("Choissisez_votre_langue_"));
        jPanel19.add(jLabel15);
                
        jPanel16.add(jPanel19, java.awt.BorderLayout.NORTH);
                
        jPanel17.setLayout(new java.awt.GridLayout(2, 3, 10, 0));
                
        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel17.setForeground(new java.awt.Color(51, 51, 51));
        jPanel17.add(jLabel13);
                
        jLabel14.setText(Language.getInstance().getText("Langue_"));
        jPanel17.add(jLabel14);
                
        jPanel17.add(jLabel16);
                
        jPanel17.add(jLabel17);
                
        languages_List.setBackground(new java.awt.Color(255, 204, 0));
        jPanel17.add(languages_List);
                
        jPanel17.add(jLabel19);
                
        jPanel16.add(jPanel17, java.awt.BorderLayout.CENTER);
                
        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setPreferredSize(new java.awt.Dimension(15, 33));
        jPanel18.add(jLabel9);
                
        jPanel18.add(jLabel18);
                
        jPanel16.add(jPanel18, java.awt.BorderLayout.SOUTH);
                
        jPanel14.add(jPanel16, java.awt.BorderLayout.CENTER);
                
        jTabbedPane1.addTab(Language.getInstance().getText("Langues"), jPanel14);
                
        jPanel15.setLayout(new java.awt.BorderLayout());
                
        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.add(jEditorPane2, java.awt.BorderLayout.CENTER);
                
        /*jEditorPane2.setContentType("text/html");
          jEditorPane2.setFont(new Font("Serif", Font.PLAIN, 10));
          jEditorPane2.setForeground(Color.BLACK);
          jEditorPane2.setText(getAbout());
        */
        jEditorPane2.setEditable(false);
        HTMLDocument m_oDocument;
        HTMLEditorKit m_oHTML;
        m_oDocument = new HTMLDocument();
        m_oHTML  = new HTMLEditorKit(); 
        jEditorPane2.setEditorKit(m_oHTML);
        jEditorPane2.setDocument(m_oDocument);
        /*try {
          URL aboutHtml = getClass().getResource("/org/objectweb/salome_tmf/ihm/common/about SalomeTMF.html");
          jEditorPane2.setPage(aboutHtml);
          }catch(Exception e){ */
        jEditorPane2.setText(htmlAbout());
        //}
                
        jTabbedPane1.addTab(Language.getInstance().getText("A_propos"), jPanel15);
                
        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
                
    }
        
    String htmlAbout(){
        return  "<body>"+
            "<div style=\"text-align: center;\">"+
            "<div style=\"text-align: left;\">"+
            "<div style=\"text-align: center;\"><small><small><span style=\"font-weight: bold;\"><big><big>Salome-TMF</big></big></span>"+
            "</small></small></div>"+
            "<div style=\"text-align: center;\"><small><small><span style=\"font-weight: bold;\">version 3 <a href=\"https://wiki.objectweb.org/salome-tmf/\">https://wiki.objectweb.org/salome-tmf/</a></span>"+
            "<span style=\"font-weight: bold;\"></span></small></small></div>"+
            "<div style=\"text-align: left;\"><small><small><span style=\"font-weight: bold;\">SalomeTMF is a Test Management Framework, Copyright (C) 2005 France Telecom R&amp;D"+
            " <li>Mika&euml;l Marche (Project Manager, Developer, Plug-in developer, Documentation)</li>"+
            "<li>Fay&ccedil;al Sougrati (Developer, Plug-in developer, Documentation)</li>"+
            "<li>Aurore Penault (Developer, Plug-in developer, Documentation)</li>"+
            "<li>Yves-Marie Quemener (English documentation)</li>"+
            "<li>Vincent Pautret (Developer)</li>"+
            "<li>Jean-Marie Hallou&euml;t (Localization and Validation)</li>"+
            "</span>"+
            "</body>";
        //"</html>"; 
    }
        
        
    /******************************* METHODE PLUBLIC ***************************/
    public void error(String err){
        javax.swing.JOptionPane.showMessageDialog(this,
                                                  err,
                                                  Language.getInstance().getText("Authentification"),
                                                  javax.swing.JOptionPane.ERROR_MESSAGE);
    }
        
        
    public String getUsedLocal(){
        return  ((String)languages_List.getSelectedItem()).trim();
    }
        
    public String getSelectedSalomeProject(){
        return proj;
    }
        
    public String getSelectedAdminSalomeProject(){
        return proj1;
    }
        
    public UserWrapper getSelectedSalomeUser(){
        return getUser();
    }
        
    public UserWrapper getSelectedAdminSalomeUser(){
        return getUser1();
    }
        
        
    public boolean validPassword(){
        //String paswd = (String) users.get(getUser());
        String paswd =  getUser().getPassword();
        String typed_paswd = new String(password_Field.getPassword()); 
        if (paswd != null){
            try {
                return org.objectweb.salome_tmf.api.MD5paswd.testPassword(typed_paswd, paswd);
            }catch(Exception e){
                Util.log("[LoginSalomeTMF->validPassword]"+ e);
            }
        }
        return false;
    }
        
    public boolean testAuthentification(){
        boolean retourPassword=false;
        if (Api.getUserAuthentification().equalsIgnoreCase("Database")){
            retourPassword =validPassword();
        }else if (Api.getUserAuthentification().equalsIgnoreCase("Ldap")){
            retourPassword =validPasswordLdap();
        }else if(Api.getUserAuthentification().equalsIgnoreCase("both")){
            retourPassword = validPassword();
            if (retourPassword){
                Api.setUserAuthentification("Database");
            }else {
                retourPassword = validPasswordLdap();   
                Api.setUserAuthentification("Ldap");
            }
        }
        return retourPassword;
    }
        
    public boolean testAuthentification1(){
        boolean retourPassword1=false;
        if (Api.getUserAuthentification().equalsIgnoreCase("Database")){
            retourPassword1 =validPassword1();
        }else if (Api.getUserAuthentification().equalsIgnoreCase("Ldap")){
            retourPassword1 =validPasswordLdap1();
        }else if(Api.getUserAuthentification().equalsIgnoreCase("both")){
            retourPassword1 = validPassword1();
            if (retourPassword1){
                Api.setUserAuthentification("Database");
            }else {
                retourPassword1 = validPasswordLdap1(); 
                Api.setUserAuthentification("Ldap");
            }
        }
        return retourPassword1;
    }
        
    private boolean validPasswordLdap() {

        String typed_paswd = new String(password_Field.getPassword());
        String login = getUser().getLogin();
        boolean isConnected = false;
        
        if (typed_paswd != null) {
            try{
                LdapContext ctx = ActiveDirectory.getConnection(login, typed_paswd);
                ctx.close();
                isConnected = true;
            }
            catch(Exception e){
                //Failed to authenticate user!
            }
        }
        return isConnected;
    }
        
    public boolean validPassword1(){
        //String paswd = (String) users1.get(getUser1());
        String paswd =  getUser1().getPassword();
        String typed_paswd = new String(password_Field1.getPassword()); 
        if (paswd != null){
            try {
                return org.objectweb.salome_tmf.api.MD5paswd.testPassword(typed_paswd, paswd);
            }catch(Exception e){
                Util.log("[LoginSalomeTMF->validPassword1]"+ e);
            }
        }
        return false;
    }
        
    private boolean validPasswordLdap1() {

        String typed_paswd = new String(password_Field1.getPassword());
        String login = getUser1().getLogin();
        boolean isConnected = false;

        if (typed_paswd != null) {
            try{
                LdapContext ctx = ActiveDirectory.getConnection(login, typed_paswd);
                ctx.close();
                isConnected = true;
            }
            catch(Exception e){
                //Failed to authenticate user!
                }
        }
        return isConnected;
    }
        
    public boolean validPassword2(){
        String typed_paswd = new String(password_Field2.getPassword()); 
        Util.log("[LoginSalomeTMF->validPassword2] verify paswd "+ typed_paswd + " with " + admin_pwd);
        if (admin_pwd != null){
            try {
                return org.objectweb.salome_tmf.api.MD5paswd.testPassword(typed_paswd, admin_pwd);
            }catch(Exception e){
                Util.log("[LoginSalomeTMF->validPassword2]"+ e);
            }
                        
        }
        return false;
    }
        
        
    /******************************* Action *****************************/
        
    private void project_List1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_project_List1ItemStateChanged
        projectChange1((ProjectWrapper) evt.getItem());
    }
        
        
        
    private void project_ListItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_project_ListItemStateChanged
        projectChange((ProjectWrapper) evt.getItem());
    }
        
    class passwordKeyListener implements KeyListener {
        int paswdType = -1;
                
        passwordKeyListener(int t){
            paswdType  = t;
        }
                
        @Override
        public void keyTyped(KeyEvent ke){
                        
        }
                
        @Override
        public void keyPressed(KeyEvent ke){
                        
        }
                
        @Override
        public void keyReleased(KeyEvent ke){
                        
            if (ke.getKeyCode() == KeyEvent.VK_ENTER){
                if (paswdType == 0){
                    ActionEvent pActionEvent = new ActionEvent(b_start,id++, ACTION_START_SALOME);
                    pActionListener.actionPerformed(pActionEvent);
                } else if (paswdType == 1) {
                    ActionEvent pActionEvent = new ActionEvent(b_start1,id++, ACTION_START_PROJECT_ADMIN);
                    pActionListener.actionPerformed(pActionEvent);
                }else if (paswdType == 2) {
                    ActionEvent pActionEvent = new ActionEvent(b_start2,id++, ACTION_START_ADMIN_SALOME);
                    pActionListener.actionPerformed(pActionEvent);
                }
            }
        }
    }
    @Override
    public void hyperlinkUpdate(HyperlinkEvent event) {
        if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            SalomeTMFContext pSalomeTMFContext =  SalomeTMFContext.getInstance();
            if (pSalomeTMFContext != null){
                SalomeTMFContext.getBaseIHM().showDocument(event.getURL(), "SalomeTMF Wiki");
            }
        }
    }

        
        
    /* ACTION : START_SALOME*/
    /*private void b_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_startActionPerformed
    // TODO add your handling code here:
    if (validPassword()) {
    URL recup;
    try {
    usedLocale = ((String)languages_List.getSelectedItem()).trim();
    Api.saveLocale(usedLocale);
    //int idConnection = Api.addConnection(proj, getUser());
    int idConnection = pISQLSession.addSession(proj, getUser().getLogin());
    //Api.closeConnection();
    //dbClosed = true;
    //salome_redirect = true;
    recup = new URL(getDocumentBase(), "Salome.html?id=" + idConnection);
    getAppletContext().showDocument(recup);
    repaint();
    } catch (Exception me) {
    Util.log("[LoginSalomeTMF->b_startActionPerformed]" + me);
    }
    }else {
    error(Language.getInstance().getText("Mot_de_passe_invalide"));
    }
    }//GEN-LAST:event_b_startActionPerformed
    */
        
    /* ACTION : START_PROJECT_ADMIN */
    /*
      private void b_start1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_start1ActionPerformed
      // TODO add your handling code here:
      if (validPassword1()) {
      URL recup;
      try {
      usedLocale = ((String)languages_List.getSelectedItem()).trim();
      Api.saveLocale(usedLocale);
      int idConnection = pISQLSession.addSession(proj1, getUser1().getLogin());
      //Api.closeConnection();
      //dbClosed = true;
      //salome_redirect = true;
      recup = new URL(getDocumentBase(), "AdminProject.html?id=" + idConnection);
      getAppletContext().showDocument(recup);
      repaint();
      } catch (Exception me) {
      }
      }else {
      error(Language.getInstance().getText("Mot_de_passe_invalide"));
      }
             
      }//GEN-LAST:event_b_start1ActionPerformed
    */
    /* ACTION : START_ADMIN_SALOME */
    /* private void b_start2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_b_start2ActionPerformed
    // TODO add your handling code here:
    if (validPassword2()) {
    URL recup;
    try {
    usedLocale = ((String)languages_List.getSelectedItem()).trim();
    Api.saveLocale(usedLocale);
    //int idConnection = Api.addConnection("ALL", "AdminSalome");
    int idConnection = pISQLSession.addSession("ALL", "AdminSalome");
    recup = new URL(getDocumentBase(), "Administration.html?id=" + idConnection );
    //Api.closeConnection();
    //dbClosed = true;
    //salome_redirect = true;
    getAppletContext().showDocument(recup);
    repaint();
    } catch (Exception me) {
    Util.log("[LoginSalomeTMF->b_start2ActionPerformed]" + me);
    }
    }else {
    error(Language.getInstance().getText("Mot_de_passe_invalide"));
    }
    }//GEN-LAST:event_b_start2ActionPerformed
              
    */
}
