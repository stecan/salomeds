/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AdminProjectData;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Group;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.databaseSQL.SQLEngine;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.admin.models.GroupListRenderer;
import org.objectweb.salome_tmf.ihm.admin.models.UserListRenderer;
import org.objectweb.salome_tmf.ihm.common.AskNameAndDescription;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.BaseIHM;
import org.objectweb.salome_tmf.ihm.main.ProjectAdminLogin;
import org.objectweb.salome_tmf.ihm.main.SalomeLocksPanel;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.login.LoginSalomeTMF;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.Common;

/**
 * Classe representant la fenetre d'administration d'un projet.
 *
 * @author teaml039
 * @version 0.1
 */
public class AdministrationProject extends JApplet implements IPlugObject,
        ApiConstants, DataConstants, IHMConstants, WindowListener {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger
        .getLogger(AdministrationProject.class);

    boolean problemURL = false;

    public static JPFManager jpf;
    
    BaseIHM pBaseIHM;
    
    
    public AdministrationProject(BaseIHM m_BaseIHM) {
        pBaseIHM = m_BaseIHM;
    }
    

    /**
     * Mapping between UI components and constants defined in
     * org.objectweb.salome_tmf.ihm.UICompCst
     */
    static Map UIComponentsMap;

    /**
     * Les onglets d'administration
     */
    JTabbedPane tabs;

    public JTabbedPane getTabs() {
        if (logger.isDebugEnabled()) {
            logger.debug("getTabs() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getTabs() - end");
        }
        return tabs;
    }

    /**
     * Fenetre totale
     */
    JPanel projectSupervision;

    /**
     * Vue droite de la fenetre
     */
    JPanel projectView;

    /**
     * Vue pour le changement de mot de passe
     */
    JPanel pwdView;

    /**
     * Vue pour la gestion des utilisateurs
     */
    JPanel userView;

    /**
     * Vue pour la gestion des groupes
     */
    JPanel groupView;

    // field utilises pour le changement de mot de passe
    /**
     * Ancien mot de passe
     */
    JPasswordField oldPassword;

    /**
     * Nouveau mot de passe : premier
     */
    JPasswordField newPasswordFirst;

    /**
     * Nouveau mot de passe : second
     */
    JPasswordField newPasswordSecond;

    /**
     * Modele regroupant les groupes pour la liste
     */
    DefaultListModel groupListModel;

    /**
     * la liste des groupes
     */
    JList groupList;

    /**
     * Liste des utilisateurs ne faisant pas partie du groupe
     */
    DefaultListModel notMemberListModel;

    /**
     * Liste des utilisateurs du groupe
     */
    DefaultListModel memberListModel;

    /**
     * Liste des groupes dont un utilisateur est membre
     */
    JList memberOf;

    /**
     * Liste des groupes dont un utilisateur n'est pas membre
     */
    JList notMemberOf;

    /**
     * Le modele de donnees de la table des utilisateurs
     */
    MyTableModel usersTableModel;

    /**
     * La table des utilisateurs
     */
    JTable table;

    /**
     * Bouton pour supprimer un utilisateur
     */
    JButton deleteUserButton;

    /**
     * Champ pour entrer le prenom
     */
    JLabel firstNameLabel;

    /**
     * Champ pour entrer le nom de famille
     */
    JLabel lastNameLabel;

    /**
     * Champ pour entrer l'email
     */
    JLabel emailLabel;

    /**
     * Champ pour entrer le numero de telephone
     */
    JLabel phoneLabel;

    /**
     * L'utilisateur selectionne
     */
    User selectedUser;

    /**
     * Ligne de l'utilisateur selectionne
     */
    private int userSelectedRow;

    public static Hashtable<Object, Object> associatedExtension;

    /**
     * Modele de donnees de la liste des utilisateurs ne faisant pas aprtie du
     * groupe
     */
    DefaultListModel outGroupListModel;
    /**
     * Modele de donnees de la liste des utilisateurs du groupe
     */
    DefaultListModel inGroupListModel;

    /**
     * Liste des utilisateurs du groupe
     */
    JList inGrouplist;

    /**
     * Liste des utilisateurs ne faisant pas partie du groupe
     */
    JList outGrouplist;

    /**
     * Bouton pour modifier un groupe
     */
    JButton renameButton;

    /**
     * Ligne du groupe selectionne dans la liste des groupes de la vue des
     * groupes
     */
    private int groupSelectedRow;

    /**
     * Bouton pour supprimer un groupe
     */
    JButton groupDeleteButton;

    /**
     * Le nom du projet courant
     */
    JLabel projectName;

    /**
     * Description d'un groupe
     */
    JTextArea groupDescriptionArea;

    JButton modifyButton;

    JButton viewButton;

    JTextArea userDescriptionArea;

    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/
    /*
     * dbClosed = true if connection to database is close normaly (no reload or
     * back from browser)
     */
    // boolean dbClosed = true;
    /*
     * reload = true if applet is start again from same JVM (reload or back from
     * browser)
     */
    // static boolean reload = false;

    public static AdminProjectData pAdminProjectData;

    boolean closeDB = true;
    String strProject = null;
    String strUser = null;
    int idConn = -1;
    boolean standAlone = false;
    static boolean exit = false;
    JavaScriptUtils pJSUtils;

    void onInit() {
        if (logger.isDebugEnabled()) {
            logger.debug("onInit() - start");
        }

        standAlone = false;
        if (connectToAPI()) {
            closeDB = true;
            initComponent();
            loadModel();
        }
        exit = false;

        if (logger.isDebugEnabled()) {
            logger.debug("onInit() - end");
        }
    }

    void onStart() {

    }

    /*
     * void closeWindow(){ try { //Class classJSObject =
     * Class.forName("sun.plugin.javascript.JSObject"); Class classJSObject =
     * Class.forName("netscape.javascript.JSObject"); Method meth_getWindow =
     * classJSObject.getMethod("getWindow", new Class[] {Applet.class}); Object
     * pJSObject = meth_getWindow.invoke(null ,new Object[] {this});
     * Util.debug("pJSObject = " + pJSObject); Method meth_call =
     * classJSObject.getMethod("call", new Class[] {String.class,
     * Object[].class}); meth_call.invoke(pJSObject, new Object[] {"close",
     * null});
     *
     * } catch (Exception e) { Util.err(e); } }
     */

    void initComponent() {
        if (logger.isDebugEnabled()) {
            logger.debug("initComponent() - start");
        }

        // Mapping between UI components and constants
        UIComponentsMap = new HashMap();

        Util.log("[AdministrationProject->initComponent]");
        System.runFinalization();
        System.gc();

        javax.swing.JOptionPane
            .getFrameForComponent(AdministrationProject.this)
            .addWindowListener(this);

        // Api.setUrlBase(getDocumentBase());

        Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
        try {
            Class lnfClass = Class.forName(UIManager
                                           .getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel) (lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
            Util.adaptFont();
        } catch (Exception exc) {
            logger.error("initComponent()", exc);

            Util.err("Error loading L&F: " + exc);
        }
        tabs = new JTabbedPane();

        projectSupervision = new JPanel(new BorderLayout());

        projectView = new JPanel(new BorderLayout());

        pwdView = new JPanel(new BorderLayout());

        userView = new JPanel(new BorderLayout());

        groupView = new JPanel();

        // field utilises pour le changement de mot de passe
        oldPassword = new JPasswordField(20);

        newPasswordFirst = new JPasswordField(20);

        newPasswordSecond = new JPasswordField(20);

        groupListModel = new DefaultListModel();

        groupList = new JList(groupListModel);

        notMemberListModel = new DefaultListModel();

        memberListModel = new DefaultListModel();

        memberOf = new JList(memberListModel);

        notMemberOf = new JList(notMemberListModel);

        usersTableModel = new MyTableModel();

        table = new JTable();

        deleteUserButton = new JButton(Language.getInstance().getText("Supprimer"));

        firstNameLabel = new JLabel(Language.getInstance().getText("Prenom__"));

        lastNameLabel = new JLabel(Language.getInstance().getText("Nom__"));

        emailLabel = new JLabel(Language.getInstance().getText("Email_"));

        phoneLabel = new JLabel(Language.getInstance().getText("Telephone__"));

        userSelectedRow = 0;

        outGroupListModel = new DefaultListModel();
        inGroupListModel = new DefaultListModel();

        inGrouplist = new JList(inGroupListModel);

        outGrouplist = new JList(outGroupListModel);

        renameButton = new JButton(Language.getInstance().getText("Modifier"));

        groupSelectedRow = 0;

        groupDeleteButton = new JButton(Language.getInstance().getText("Supprimer"));

        projectName = new JLabel();

        groupDescriptionArea = new JTextArea();

        // Onglets
        tabs.addTab(Language.getInstance()
                    .getText("Administration_d_un_projet"), projectSupervision);
        tabs.setPreferredSize(new Dimension(500, 500));

        projectName.setFont(new Font(null, Font.BOLD, 18));
        JPanel projectNamePanel = new JPanel(new BorderLayout());
        projectNamePanel.add(BorderLayout.NORTH, projectName);

        JRadioButton changePwdProjectButton = new JRadioButton(Language
                .getInstance().getText("Changer_le_mot_de_passe"));
        changePwdProjectButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    projectView.removeAll();
                    projectView.add(pwdView, BorderLayout.CENTER);
                    projectView.validate();
                    projectView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JRadioButton usersProjectButton = new JRadioButton(Language
                .getInstance().getText("Gerer_les_utilisateurs"));
        usersProjectButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    projectView.removeAll();
                    projectView.add(userView, BorderLayout.CENTER);
                    projectView.validate();
                    projectView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JRadioButton groupsProjectButton = new JRadioButton(Language
                .getInstance().getText("Gerer_les_groupes"));
        groupsProjectButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    projectView.removeAll();
                    projectView.add(groupView, BorderLayout.CENTER);
                    projectView.validate();
                    projectView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JRadioButton manageLocksButton = new JRadioButton(Language
                .getInstance().getText("Gerer_les_locks"));
        manageLocksButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    projectView.removeAll();
                    SalomeLocksPanel locksView = new SalomeLocksPanel(
                            pAdminProjectData.getCurrentProjectFromModel()
                            .getIdBdd());
                    locksView.loadDataFromDB();
                    projectView.add(locksView, BorderLayout.CENTER);
                    projectView.validate();
                    projectView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton backButton = new JButton(Language.getInstance().getText("Quitter"));
        backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pBaseIHM.quit(true, true);
                }
            });

        // on regroupe les radio buttons.
        ButtonGroup adminProjectButtonGroup = new ButtonGroup();
        if (Api.getUserAuthentification() == null
            || Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            adminProjectButtonGroup.add(changePwdProjectButton);
        }
        adminProjectButtonGroup.add(usersProjectButton);
        adminProjectButtonGroup.add(groupsProjectButton);
        adminProjectButtonGroup.add(manageLocksButton);
        UIComponentsMap.put(UICompCst.ADMINPROJECT_BUTTONGROUP,
                            adminProjectButtonGroup);

        UIComponentsMap.put(UICompCst.ADMINPROJECT_PANEL, projectView);

        if (Api.getLockMeth() != 1)
            manageLocksButton.setEnabled(false);

        JPanel radioProjectPanel = new JPanel(new GridLayout(0, 1));

        radioProjectPanel.add(projectNamePanel);
        radioProjectPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        if (Api.getUserAuthentification() == null
            || Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            radioProjectPanel.add(changePwdProjectButton);
        }
        radioProjectPanel.add(Box.createRigidArea(new Dimension(1, 15)));
        radioProjectPanel.add(usersProjectButton);
        radioProjectPanel.add(Box.createRigidArea(new Dimension(1, 15)));
        radioProjectPanel.add(groupsProjectButton);
        radioProjectPanel.add(Box.createRigidArea(new Dimension(1, 15)));
        radioProjectPanel.add(manageLocksButton);

        UIComponentsMap.put(UICompCst.ADMINPROJECT_BUTTONS_PANEL,
                            radioProjectPanel);

        JPanel testPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        testPanel.add(radioProjectPanel);

        JPanel radioProjectPanelAll = new JPanel(new BorderLayout());
        radioProjectPanelAll.add(testPanel);
        radioProjectPanelAll.add(backButton, BorderLayout.SOUTH);

        // radioProjectPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        radioProjectPanelAll.setBorder(BorderFactory.createRaisedBevelBorder());

        // On remplit le panel adminVoiceTesting
        projectSupervision.add(BorderLayout.WEST, radioProjectPanelAll);

        createChangePwdView();
        createUsersViewForOneProject();
        createGroupsViewForOneProject();
        projectView.removeAll();

        projectSupervision.add(BorderLayout.CENTER, projectView);
        projectSupervision.validate();

        Container cp = this.getContentPane();
        cp.add(tabs);

        // System.out.println("Dans Salome :"+DataModel.getCurrentProject());

        if (logger.isDebugEnabled()) {
            logger.debug("initComponent() - end");
        }
    }

    boolean connectToAPI() {
        if (logger.isDebugEnabled()) {
            logger.debug("connectToAPI() - start");
        }

        Util.log("[AdministrationProject->connectToAPI]");
        pJSUtils = new JavaScriptUtils(this);
        if (Api.isConnected()) {
            // JOptionPane
            // .showMessageDialog(
            // AdministrationProject.this,
            // Language
            // .getInstance()
            // .getText(
            // "Vous_avez_deja_une_session_Salome_ouverte_avec_ce_navigateur")
            // + Language
            // .getInstance()
            // .getText(
            // "Une_seule_session_est_autorisee_par_navigateur_afin_d_eviter_les_conflits_"),
            // Language.getInstance().getText("Erreur_"),
            // JOptionPane.ERROR_MESSAGE);
            // quit(false, false);
            // closeDB = false;
            // return true;
        } else {
            super.start();
            Api.openConnection(getDocumentBase());
            closeDB = true;
            if (!Api.isConnected()) {
                JOptionPane.showMessageDialog(AdministrationProject.this,
                        "Can't connect to the database", Language.getInstance()
                        .getText("Erreur_"), JOptionPane.ERROR_MESSAGE);
                closeDB = false;
                quit(true, false);

                if (logger.isDebugEnabled()) {
                    logger.debug("connectToAPI() - end");
                }
                return false;
            }

            String[] params = new String[2];
            /*
             * if (!Api.isIDE_DEV()){ params =
             * Tools.chooseProjectAndUser(getDocumentBase()); }
             */

            params[1] = pJSUtils.getLoginCookies();
            params[0] = pJSUtils.getProjectCookies();
            Api.setUserAuthentification(pJSUtils
                                        .getUserAuthentficationCookies());

            if ((params[0] == null) || (params[0].equals(""))) {
                ProjectAdminLogin dialog = new ProjectAdminLogin();
                dialog.setVisible(true);
                if (dialog.getSelectedProject() != null
                    && dialog.getSelectedUser() != null) {
                    params[0] = dialog.getSelectedProject();
                    params[1] = dialog.getSelectedUser();
                    standAlone = true;

                    // Recuperation du login et du mot de passe pour
                    // l'authentification
                    try {
                        Api.setStrUsername(dialog.getSelectedUser());
                        Api.setStrMD5Password(MD5paswd
                                .getEncodedPassword(dialog
                                                    .getSelectedPassword()));
                    } catch (Exception e) {
                        logger.error("connectToAPI()", e);

                        Util.err(e);
                    }
                }
                /*
                 * JOptionPane.showMessageDialog(AdministrationProject.this,
                 * Language
                 * .getInstance().getText("Probleme_dans_l_URL_saisie_"),
                 * Language.getInstance().getText("Erreur_"),
                 * JOptionPane.ERROR_MESSAGE); problemURL = true; problemURL =
                 * true; closeDB = true; quit(true, true); return false;
                 */
            }
            strProject = params[0];
            strUser = params[1];
            Api.initConnectionUser(params[0], params[1]);
            /*
             * try { String[] tab = getDocumentBase().toString().split("[?=]");
             * idConn = Integer.parseInt(tab[2]); } catch(Exception e){
             * Util.log(
             * "[AdministrationProject->connectToAPI] WARNING idConn can't be found"
             * ); }
             */
        }

        if (logger.isDebugEnabled()) {
            logger.debug("connectToAPI() - end");
        }
        return true;
    }

    public void setStrProject(String strProject) {
        if (logger.isDebugEnabled()) {
            logger.debug("setStrProject(String) - start");
        }

        this.strProject = strProject;

        if (logger.isDebugEnabled()) {
            logger.debug("setStrProject(String) - end");
        }
    }

    public void setStrUser(String strUser) {
        if (logger.isDebugEnabled()) {
            logger.debug("setStrUser(String) - start");
        }

        this.strUser = strUser;

        if (logger.isDebugEnabled()) {
            logger.debug("setStrUser(String) - end");
        }
    }

    void loadModel() {
        if (logger.isDebugEnabled()) {
            logger.debug("loadModel() - start");
        }

        try {
            initDataVoiceTesting(strProject, strUser);

        } catch (Exception e) {
            logger.error("loadModel()", e);

            Util.err(e);
            JOptionPane.showMessageDialog(AdministrationProject.this, e,
                                          Language.getInstance().getText("Erreur_"),
                                          JOptionPane.ERROR_MESSAGE);
            problemURL = true;
            quit(true, true);
        }
        projectName.setText(pAdminProjectData.getCurrentProjectFromModel()
                            .getNameFromModel());
        if (Api.isALLOW_PLUGINS()) {
            jpf = new JPFManager();
            // jpf.

            jpf.startJPFInAdminProject(documentBase, UIComponentsMap, this,
                                       strProject, strUser);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("loadModel() - end");
        }
    }

    private URL documentBase;

    public void setDocumentBase(URL documentBase) {
        if (logger.isDebugEnabled()) {
            logger.debug("setDocumentBase(URL) - start");
        }

        this.documentBase = documentBase;

        if (logger.isDebugEnabled()) {
            logger.debug("setDocumentBase(URL) - end");
        }
    }

    /**
     * Methode d'initialisation de l'applet
     */
    @Override
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug("init() - start");
        }

        Util.log("[AdministrationProject->init]");
        onInit();

        if (logger.isDebugEnabled()) {
            logger.debug("init() - end");
        }
    }

    /**
     * Methode appelee a chaque chargement de la page
     */
    @Override
    public void start() {
        if (logger.isDebugEnabled()) {
            logger.debug("start() - start");
        }

        Util.log("[AdministrationProject->start]");
        onStart();

        if (logger.isDebugEnabled()) {
            logger.debug("start() - end");
        }
    }

    /**
     * Methode appele a la fermeture de l'applet
     */
    @Override
    public void stop() {
        if (logger.isDebugEnabled()) {
            logger.debug("stop() - start");
        }

        Util.log("[AdministrationProject->stop]");

        if (logger.isDebugEnabled()) {
            logger.debug("stop() - end");
        }
    }

    /**
     * Methode qui creer l'ecran permettant de changer le mot de passe.
     */
    public void createChangePwdView() {
        if (logger.isDebugEnabled()) {
            logger.debug("createChangePwdView() - start");
        }

        projectView.removeAll();

        JButton validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton
            .setToolTipText(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (Api.isConnected()) {
                        User pUser = pAdminProjectData.getCurrentProjectFromModel()
                            .getAdministratorFromModel();

                        boolean _valid = false;
                        try {
                            String pass = pUser.getPasswordFromDB();
                            _valid = org.objectweb.salome_tmf.api.MD5paswd
                                .testPassword(new String(oldPassword
                                                         .getPassword()).trim(), pass);
                        } catch (Exception ex) {
                            logger.error("actionPerformed(ActionEvent)", ex);
                        }
                        if (_valid) {
                            if (new String(newPasswordFirst.getPassword()).trim()
                                .equals(
                                        new String(newPasswordSecond
                                                   .getPassword()).trim())) {
                                try {
                                    pUser.updatePasswordInDB(
                                                new String(newPasswordSecond.getPassword())
                                                .trim());

                                    oldPassword.setText("");
                                    newPasswordFirst.setText("");
                                    newPasswordSecond.setText("");
                                    JOptionPane.showMessageDialog(
                                        AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText(
                                                 "Votre_mot_de_passe_a_ete_modifie"),
                                        Language.getInstance().getText("Succes_"),
                                            JOptionPane.INFORMATION_MESSAGE);
                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }

                            } else {
                                newPasswordFirst.setText("");
                                newPasswordSecond.setText("");
                                JOptionPane
                                    .showMessageDialog(AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText("Impossible_de_changer_le_mot_de_passe_Vous_n_avez_pas_entrer_deux_fois_le_meme_mot_de_passe"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            oldPassword.setText("");
                            JOptionPane.showMessageDialog(AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText("Impossible_de_changer_le_mot_de_passe_L_ancien_mot_de_passe_est_incorrect"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane
                            .showMessageDialog(
                                               AdministrationProject.this,
                                               Language
                                               .getInstance()
                                               .getText(
                                                        "Impossible_de_changer_le_mot_de_passe_Vous_n_etes_pas_connecter_a_la_base"),
                                               Language.getInstance().getText("Erreur_"),
                                               JOptionPane.ERROR_MESSAGE);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    oldPassword.setText("");
                    newPasswordFirst.setText("");
                    newPasswordSecond.setText("");

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JLabel oldQuestion = new JLabel(Language.getInstance().getText(
                "Entrez_votre_ancien_mot_de_passe_"));

        JLabel question = new JLabel(Language.getInstance().getText(
                "Entrez_votre_nouveau_mot_de_passe_"));

        JLabel confirmation = new JLabel(Language.getInstance().getText(
                "Confirmer_le_nouveau_mot_de_passe_"));

        JPanel textFieldPane = new JPanel();
        textFieldPane.setLayout(new BoxLayout(textFieldPane, BoxLayout.Y_AXIS));
        textFieldPane.add(oldPassword);
        textFieldPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textFieldPane.add(newPasswordFirst);
        textFieldPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textFieldPane.add(newPasswordSecond);

        JPanel textPane = new JPanel();
        textPane.setLayout(new BoxLayout(textPane, BoxLayout.Y_AXIS));
        textPane.add(oldQuestion);
        textPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textPane.add(question);
        textPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textPane.add(confirmation);

        JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
        textPaneAll.add(textPane);
        textPaneAll.add(textFieldPane);
        textPaneAll.setBorder(BorderFactory.createEmptyBorder(100, 10, 10, 20));

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(validateButton);
        buttonPanel.add(cancelButton);

        pwdView.setLayout(new BoxLayout(pwdView, BoxLayout.Y_AXIS));
        pwdView.add(textPaneAll);
        pwdView.add(buttonPanel);
        pwdView.setBorder(BorderFactory.createRaisedBevelBorder());
        pwdView.validate();

        if (logger.isDebugEnabled()) {
            logger.debug("createChangePwdView() - end");
        }
    } // Fin de la classe createChangePwdView/0

    /**
     * Methode qui permet de creer l'ecran permettant de gerer les utilisateurs.
     *
     */
    public void createUsersViewForOneProject() {
        if (logger.isDebugEnabled()) {
            logger.debug("createUsersViewForOneProject() - start");
        }

        projectView.removeAll();
        // Les boutons
        JButton createButton = new JButton(Language.getInstance().getText("Ajouter"));
        createButton.setIcon(Tools.createAppletImageIcon(PATH_TO_ADD_PROJECT_ICON, ""));
        createButton.setToolTipText(Language.getInstance().getText(
                "Ajouter_un_nouvel_utilisateur"));
        createButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    AddUser addUser = new AddUser(pAdminProjectData);
                    if (addUser.getUserList() != null) {
                        for (int i = 0; i < addUser.getUserList().size(); i++) {
                            try {
                                User pUser = (User) addUser.getUserList().get(i);
                                Group pGroup = pAdminProjectData
                                    .getCurrentProjectFromModel()
                                    .getGroupFromModel(ApiConstants.GUESTNAME);
                                pAdminProjectData.addUserInGroupInDBAndModel(pUser,
                                                                             pGroup);
                                pAdminProjectData.setCurrentGroupInModel(pGroup);

                                usersTableModel.clearTable();
                                // initialize "userTableModel"
                                initDataVoiceTesting(strProject, strUser);

                            } catch (Exception exception) {
                                logger.error("actionPerformed(ActionEvent)",
                                             exception);

                                Tools.ihmExceptionView(exception);
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        deleteUserButton.setToolTipText(Language.getInstance().getText(
                "Supprimer_un_utilisateur"));
        deleteUserButton.setEnabled(false);
        deleteUserButton.setIcon(Tools.createAppletImageIcon(
                                                             PATH_TO_DELETE_ICON, ""));
        deleteUserButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (selectedUser != null) {
                        Object[] options = { Language.getInstance().getText("Oui"),
                                             Language.getInstance().getText("Non") };
                        int choice = -1;
                        choice = JOptionPane
                            .showOptionDialog(AdministrationProject.this,
                                Language
                                .getInstance()
                                .getText("Etes_vous_sur_de_vouloir_supprimer_l_utilisateur__")
                                + selectedUser.getLoginFromModel()
                                + " > ?", Language.getInstance()
                                .getText("Attention_"),
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.WARNING_MESSAGE, null, options,
                                options[1]);
                        if (choice == JOptionPane.YES_OPTION) {
                            Group group = pAdminProjectData.getGroupFromModel(
                                    selectedUser, ADMINNAME);
                            if (group != null
                                && pAdminProjectData.getGroupUsersFromModel(
                                        group).size() == 1) {
                                JOptionPane
                                    .showMessageDialog(AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText("Impossible_Un_projet_doit_avoir_au_moins_un_administrateur"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                try {
                                    SQLEngine.setSpecialAllow(true);

                                    pAdminProjectData
                                        .removeUserFromCurrentProjectInDBAndModel(selectedUser);
                                    SQLEngine.setSpecialAllow(false);

                                    memberListModel.clear();
                                    notMemberListModel.clear();
                                    usersTableModel.removeData(userSelectedRow);
                                    deleteUserButton.setEnabled(false);
                                    if (inGroupListModel.contains(selectedUser)) {
                                        inGroupListModel
                                            .removeElement(selectedUser);
                                    }
                                    if (outGroupListModel.contains(selectedUser)) {
                                        outGroupListModel
                                            .removeElement(selectedUser);
                                    }

                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(createButton);
        buttonSet.add(deleteUserButton);
        buttonSet.setBorder(BorderFactory.createRaisedBevelBorder());

        // La table des utilisateurs

        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Nom_complet"));
        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText("Login"));

        table.setModel(usersTableModel);
        table.setGridColor(Color.BLACK);
        table.setPreferredScrollableViewportSize(new Dimension(300, 600));
        JScrollPane tablePane = new JScrollPane(table);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tablePane.setBackground(new Color(255, 255, 153));

        ListSelectionModel rowSM = table.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - start");
                    }

                    // Ignore extra messages.

                    ListSelectionModel lsm = (ListSelectionModel) e.getSource();
                    if (lsm.isSelectionEmpty()) {
                        deleteUserButton.setEnabled(false);

                    } else {

                        userSelectedRow = lsm.getMinSelectionIndex();

                        // selectedRow is selected
                        deleteUserButton.setEnabled(true);

                        selectedUser = pAdminProjectData
                            .getUserFromModel((String) usersTableModel
                                              .getValueAt(userSelectedRow, 1));
                        pAdminProjectData.setCurrentUserInModel(selectedUser);
                        if (selectedUser != null) {
                            memberListModel.clear();
                            notMemberListModel.clear();

                            lastNameLabel.setText(Language.getInstance().getText("Nom__")
                                                  + selectedUser.getLastNameFromModel());
                            firstNameLabel.setText(Language.getInstance().getText("Prenom__")
                                                   + selectedUser.getFirstNameFromModel());
                            emailLabel.setText(Language.getInstance().getText("Email_")
                                               + selectedUser.getEmailFromModel());
                            phoneLabel.setText(Language.getInstance().getText("Telephone__")
                                               + selectedUser.getPhoneNumberFromModel());
                            userDescriptionArea.setText(selectedUser
                                                        .getDescriptionFromModel());
                            HashSet groupSet = pAdminProjectData
                                .getUserGroupsFromModel(selectedUser);
                            if (groupSet != null && groupSet.size() > 0) {
                                for (Iterator iter = groupSet.iterator(); iter
                                         .hasNext();) {
                                    memberListModel.addElement(iter.next());
                                }
                            }
                            for (int i = 0; i < pAdminProjectData
                                     .getGroupCountFromModel(); i++) {
                                if (pAdminProjectData.getGroupFromModel(i) != null) {
                                    if (!groupSet.contains(pAdminProjectData
                                                           .getGroupFromModel(i))) {
                                        notMemberListModel
                                            .addElement(pAdminProjectData
                                                        .getGroupFromModel(i));
                                    }
                                }
                            }
                            notMemberOf.setSelectedIndex(0);
                            memberOf.setSelectedIndex(0);

                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - end");
                    }
                }
            });

        JPanel users = new JPanel(new BorderLayout());
        users.setLayout(new BoxLayout(users, BoxLayout.Y_AXIS));
        users.add(tablePane);
        users.add(Box.createRigidArea(new Dimension(1, 25)));
        users.add(buttonSet);

        users.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createLineBorder(Color.BLACK), Language.getInstance().
                        getText("Utilisateurs")));

        // La liste des groupes ou l'utilisateur est membre

        memberOf
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        memberOf.setSelectedIndex(0);
        memberOf.setCellRenderer(new GroupListRenderer());
        memberOf.setVisibleRowCount(8);
        JScrollPane memberListScrollPane = new JScrollPane(memberOf);
        memberListScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.
                createLineBorder(Color.BLACK), Language.getInstance().getText("Appartient_a")));
        memberListScrollPane.setPreferredSize(new Dimension(200, 180));

        // La liste des groupes ou l'utilisateur n'est pas membre
        notMemberOf
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        notMemberOf.setSelectedIndex(0);
        notMemberOf.setCellRenderer(new GroupListRenderer());
        notMemberOf.setVisibleRowCount(8);
        JScrollPane notMemberListScrollPane = new JScrollPane(notMemberOf);
        notMemberListScrollPane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), Language
                .getInstance().getText("N_appartient_pas_a")));
        notMemberListScrollPane.setPreferredSize(new Dimension(200, 180));
        JButton addOne = new JButton("<");
        addOne.setToolTipText(Language.getInstance().getText("Ajouter_un_groupe"));
        addOne.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (table.getSelectedRow() != -1) {
                        if (notMemberOf.getSelectedValue() == null) {
                            JOptionPane
                                .showMessageDialog(AdministrationProject.this,
                                    Language
                                    .getInstance()
                                    .getText("Vous_devez_selectionner_un_groupe_avant_de_proceder_a_cette_operation"),
                                    Language.getInstance().getText("Erreur_"),
                                    JOptionPane.ERROR_MESSAGE);
                        } else {
                            if (Api.isConnected()) {
                                try {
                                    Group pGroup = (Group) notMemberOf
                                        .getSelectedValue();
                                    pAdminProjectData.addUserInGroupInDBAndModel(
                                            selectedUser, pGroup);

                                    if (!memberListModel.contains(notMemberOf
                                                                  .getSelectedValue())) {
                                        memberListModel.addElement(notMemberOf
                                                                   .getSelectedValue());
                                    }
                                    if (pAdminProjectData
                                        .getCurrentGroupFromModel() != null
                                        && pAdminProjectData
                                        .getCurrentGroupFromModel()
                                        .equals(
                                                notMemberOf
                                                .getSelectedValue())) {
                                        if (!inGroupListModel
                                            .contains(selectedUser)) {
                                            inGroupListModel
                                                .addElement(selectedUser);
                                        }
                                        outGroupListModel
                                            .removeElement(selectedUser);
                                    }
                                    notMemberListModel.removeElement(notMemberOf
                                                                     .getSelectedValue());
                                    memberOf.setSelectedIndex(0);
                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton addAll = new JButton("<<");
        addAll.setToolTipText(Language.getInstance().getText(
                "Ajouter_tous_les_groupes"));
        addAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (table.getSelectedRow() != -1) {
                        for (int i = 0; i < notMemberListModel.size(); i++) {
                            Group group = (Group) notMemberListModel.get(i);
                            try {
                                pAdminProjectData.addUserInGroupInDBAndModel(
                                        selectedUser, group);
                                memberListModel.addElement(group);

                                if (pAdminProjectData.getCurrentGroupFromModel() != null
                                    && pAdminProjectData
                                    .getCurrentGroupFromModel().equals(
                                                                       group)) {
                                    if (!inGroupListModel.contains(selectedUser)) {
                                        inGroupListModel.addElement(selectedUser);
                                    }
                                    outGroupListModel.removeElement(selectedUser);
                                }
                            } catch (Exception exception) {
                                logger.error("actionPerformed(ActionEvent)",
                                             exception);

                                Tools.ihmExceptionView(exception);
                            }
                        }
                        notMemberListModel.clear();
                        memberOf.setSelectedIndex(0);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton removeOne = new JButton(">");
        removeOne.setToolTipText(Language.getInstance().getText(
                                                                "Retirer_un_groupe"));
        removeOne.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (table.getSelectedRow() != -1) {
                        if (memberOf.getSelectedValue() == null) {
                            JOptionPane
                                .showMessageDialog(AdministrationProject.this,
                                    Language
                                    .getInstance()
                                    .getText("Vous_devez_selectionner_un_groupe_avant_de_proceder_a_cette_operation"),
                                    Language.getInstance().getText("Erreur_"),
                                    JOptionPane.ERROR_MESSAGE);
                        } else {
                            if (((Group) memberOf.getSelectedValue())
                                .getNameFromModel().equals(ADMINNAME)
                                && pAdminProjectData.getGroupUsersFromModel(
                                        (Group) memberOf.getSelectedValue())
                                .size() == 1) {
                                JOptionPane
                                    .showMessageDialog(AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText("Impossible_Un_projet_doit_avoir_au_moins_un_administrateur"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                try {
                                    Group pGroup = (Group) memberOf
                                        .getSelectedValue();
                                    pAdminProjectData
                                        .removeUserFromGroupInDBAndModel(
                                                selectedUser, pGroup);

                                    if (!notMemberListModel.contains(
                                            memberOf.getSelectedValue())) {
                                        notMemberListModel.addElement(
                                                memberOf.getSelectedValue());
                                    }
                                    if (pAdminProjectData
                                        .getCurrentGroupFromModel() != null
                                        && pAdminProjectData
                                        .getCurrentGroupFromModel()
                                        .equals(memberOf.getSelectedValue())) {
                                            inGroupListModel
                                                .removeElement(selectedUser);
                                            if (!outGroupListModel
                                                .contains(selectedUser)) {
                                                outGroupListModel
                                                    .addElement(selectedUser);
                                            }
                                    }
                                    memberListModel.removeElement(memberOf
                                            .getSelectedValue());

                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton removeAll = new JButton(">>");
        removeAll.setToolTipText(Language.getInstance().getText("Retirer_tous_les_groupes"));
        removeAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (table.getSelectedRow() != -1) {
                        Group admin = null;
                        for (int i = 0; i < memberListModel.size(); i++) {
                            Group group = (Group) memberListModel.get(i);
                            if (group.getNameFromModel().equals(ADMINNAME)
                                && pAdminProjectData.getGroupUsersFromModel(
                                        group).size() == 1) {
                                admin = group;
                                JOptionPane
                                    .showMessageDialog(AdministrationProject.this,
                                        Language
                                        .getInstance()
                                        .getText("Impossible_de_supprimer_l_utilisateur_du_groupe_Administrateur_")
                                        + Language
                                        .getInstance()
                                        .getText("Un_projet_doit_avoir_au_moins_un_administrateur"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                try {
                                    pAdminProjectData
                                        .removeUserFromGroupInDBAndModel(
                                                selectedUser, group);
                                    notMemberListModel.addElement(group);
                                    if (pAdminProjectData
                                        .getCurrentGroupFromModel() != null
                                        && pAdminProjectData
                                        .getCurrentGroupFromModel()
                                        .equals(group)) {
                                        inGroupListModel
                                            .removeElement(selectedUser);
                                        if (!outGroupListModel
                                            .contains(selectedUser)) {
                                            outGroupListModel
                                                .addElement(selectedUser);
                                        }
                                    }
                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                        memberListModel.clear();
                        if (admin != null)
                            memberListModel.addElement(admin);

                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JPanel moveButtons = new JPanel();
        moveButtons.setLayout(new BoxLayout(moveButtons, BoxLayout.Y_AXIS));
        moveButtons.setAlignmentX(Component.CENTER_ALIGNMENT);
        moveButtons.add(addOne);
        moveButtons.add(addAll);
        moveButtons.add(Box.createRigidArea(new Dimension(1, 25)));
        moveButtons.add(removeOne);
        moveButtons.add(removeAll);

        JPanel memberPanel = new JPanel();
        memberPanel.setLayout(new BoxLayout(memberPanel, BoxLayout.X_AXIS));
        memberPanel.add(memberListScrollPane);
        memberPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        memberPanel.add(moveButtons);
        memberPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        memberPanel.add(notMemberListScrollPane);

        firstNameLabel = new JLabel(Language.getInstance().getText("Prenom_"));
        lastNameLabel = new JLabel(Language.getInstance().getText("Nom_"));
        emailLabel = new JLabel(Language.getInstance().getText("Email_"));
        phoneLabel = new JLabel(Language.getInstance().getText("Telephone_"));

        JPanel textFieldPanel = new JPanel();
        textFieldPanel
            .setLayout(new BoxLayout(textFieldPanel, BoxLayout.Y_AXIS));
        textFieldPanel.add(Box.createRigidArea(new Dimension(20, 40)));
        textFieldPanel.add(firstNameLabel);
        textFieldPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        textFieldPanel.add(lastNameLabel);
        textFieldPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        textFieldPanel.add(emailLabel);
        textFieldPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        textFieldPanel.add(phoneLabel);

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new BoxLayout(labelPanel, BoxLayout.Y_AXIS));
        labelPanel.add(Box.createRigidArea(new Dimension(20, 40)));
        labelPanel.add(firstNameLabel);
        labelPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        labelPanel.add(lastNameLabel);
        labelPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        labelPanel.add(emailLabel);
        labelPanel.add(Box.createRigidArea(new Dimension(20, 10)));
        labelPanel.add(phoneLabel);

        JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
        textPaneAll.add(labelPanel);
        textPaneAll.add(textFieldPanel);

        userDescriptionArea = new JTextArea();
        userDescriptionArea.setPreferredSize(new Dimension(100, 50));
        JScrollPane descriptionScrollPane = new JScrollPane(userDescriptionArea);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), Language
                .getInstance().getText("Description")));

        userDescriptionArea
            .addCaretListener(new UserDescriptionCaretListener());
        // userDescriptionArea.addCaretListener(new
        // UserDescriptionCaretListener(pAdminProjectData));

        JPanel informations = new JPanel();
        informations.setLayout(new BoxLayout(informations, BoxLayout.X_AXIS));
        informations.add(textPaneAll);
        informations.add(Box.createRigidArea(new Dimension(20, 10)));
        informations.add(descriptionScrollPane);

        JPanel properties = new JPanel();
        properties.setLayout(new BoxLayout(properties, BoxLayout.Y_AXIS));
        properties.add(memberPanel);
        properties.add(Box.createRigidArea(new Dimension(1, 25)));
        properties.add(informations);
        properties.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createLineBorder(Color.BLACK), Language.getInstance()
                        .getText("Proprietes")));

        JPanel jPproperties = new JPanel(new BorderLayout());
        jPproperties.add(properties, BorderLayout.EAST);

        JPanel usersViewPanel = new JPanel();
        usersViewPanel
            .setLayout(new BoxLayout(usersViewPanel, BoxLayout.X_AXIS));

        usersViewPanel.add(users);
        usersViewPanel.add(Box.createRigidArea(new Dimension(10, 10)));
        usersViewPanel.add(properties);
        usersViewPanel.setBorder(BorderFactory.createEmptyBorder(20, 0, 20, 0));
        userView.add(usersViewPanel, BorderLayout.CENTER);

        if (logger.isDebugEnabled()) {
            logger.debug("createUsersViewForOneProject() - end");
        }
    } // Fin de la methode createUsersViewForOneProject/0

    public void setProjectName(JLabel projectName) {
        if (logger.isDebugEnabled()) {
            logger.debug("setProjectName(JLabel) - start");
        }

        this.projectName = projectName;

        if (logger.isDebugEnabled()) {
            logger.debug("setProjectName(JLabel) - end");
        }
    }

    /**
     * Methode qui permet de creer l'ecran permettant de gerer les groupes.
     *
     */
    public void createGroupsViewForOneProject() {
        if (logger.isDebugEnabled()) {
            logger.debug("createGroupsViewForOneProject() - start");
        }

        projectView.removeAll();
        groupView.setLayout(new BoxLayout(groupView, BoxLayout.Y_AXIS));

        // Les boutons
        JButton createButton = new JButton(Language.getInstance().getText("Nouveau"));
        createButton.setToolTipText(Language.getInstance().getText("Creer_un_nouveau_groupe"));
        createButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    AskNameAndDescription askParam = new AskNameAndDescription(
                            GROUP,
                            Language.getInstance().getText("Nouveau_groupe"),
                            Language.getInstance().getText("Nouveau_groupe__"),
                            javax.swing.JOptionPane
                            .getFrameForComponent(AdministrationProject.this),
                            pAdminProjectData);
                    Group group = askParam.getGroup();
                    if (group != null) {
                        try {
                            pAdminProjectData.addGroupInDBAndModel(group);

                            groupListModel.addElement(group);
                            if (table.getSelectedRow() != -1) {
                                notMemberListModel.addElement(group);
                            }
                        } catch (Exception exception) {
                            logger.error("actionPerformed(ActionEvent)", exception);

                            Tools.ihmExceptionView(exception);
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        renameButton.setToolTipText(Language.getInstance().getText("Modifier_un_groupe"));
        renameButton.setEnabled(false);
        renameButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    Group group = (Group) groupList.getSelectedValue();
                    String oldName = group.getNameFromModel();
                    String oldDescription = group.getDescriptionFromModel();
                    AskNameAndDescription askParam = new AskNameAndDescription(
                            GROUP,
                            Language.getInstance().getText("Modifier_un_groupe"),
                            Language.getInstance().getText("Nom_du_groupe__"),
                            group,
                            javax.swing.JOptionPane
                            .getFrameForComponent(AdministrationProject.this),
                            pAdminProjectData);
                    if (askParam.getGroup() == null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("actionPerformed(ActionEvent) - end");
                        }
                        return;
                    }
                    if (Api.isConnected()) {
                        try {
                            Util.log("[AdministrationProject->update group] name =  "
                                     + askParam.getGroup()
                                     .getNameFromModel()
                                     + ", desc = "
                                     + askParam.getGroup()
                                     .getDescriptionFromModel());
                            // pAdminProjectData.addGroupInDBAndModel(group);
                            group.updateInDBAndModel(askParam.getGroup()
                                        .getNameFromModel(), askParam.getGroup()
                                        .getDescriptionFromModel());
                            groupDescriptionArea.setText(askParam.getGroup()
                                                         .getDescriptionFromModel());
                            groupList.repaint();
                        } catch (Exception exception) {
                            logger.error("actionPerformed(ActionEvent)", exception);

                            group.updateInModel(oldName, oldDescription);
                            Tools.ihmExceptionView(exception);
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        groupDeleteButton.setToolTipText(Language.getInstance().getText("Supprimer_un_groupe"));
        groupDeleteButton.setEnabled(false);
        groupDeleteButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    int selectedIndex = groupList.getSelectedIndex();
                    Group group = (Group) groupList.getSelectedValue();
                    if (group != null) {
                        Object[] options = { Language.getInstance().getText("Oui"),
                                             Language.getInstance().getText("Non") };
                        int choice = -1;

                        choice = JOptionPane
                            .showOptionDialog(
                                    AdministrationProject.this,
                                    Language
                                    .getInstance()
                                    .getText(
                                             "Etes_vous_sur_de_vouloir_supprimer_le_groupe__")
                                    + group.getNameFromModel() + " > ?",
                                    Language.getInstance()
                                    .getText("Attention_"),
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.WARNING_MESSAGE, null, options,
                                    options[1]);
                        if (choice == JOptionPane.YES_OPTION) {
                            if (Api.isConnected()) {
                                try {
                                    pAdminProjectData
                                        .removeGroupInDBAndModel(group);

                                    groupListModel.remove(selectedIndex);
                                    for (int i = 0; i < group
                                             .getUserListFromModel().size(); i++) {
                                        User user = (User) group
                                            .getUserListFromModel().get(i);
                                        if (pAdminProjectData
                                            .getUserGroupsFromModel(user) == null) {
                                            if (Api.isConnected()) {
                                                try {
                                                    Group pGroup = pAdminProjectData
                                                        .getCurrentProjectFromModel()
                                                        .getGroupFromModel(GUESTNAME);
                                                    pAdminProjectData
                                                        .addUserInGroupInDBAndModel(user, pGroup);

                                                    if (pAdminProjectData
                                                        .getCurrentUserFromModel() != null
                                                        && group
                                                        .getUserListFromModel()
                                                        .contains(pAdminProjectData
                                                                  .getCurrentUserFromModel())) {
                                                        memberListModel
                                                            .addElement(pAdminProjectData
                                                                        .getCurrentProjectFromModel()
                                                                        .getGroupFromModel(GUESTNAME));
                                                        notMemberListModel
                                                            .removeElement(pAdminProjectData
                                                                           .getCurrentProjectFromModel()
                                                                           .getGroupFromModel(GUESTNAME));
                                                    }
                                                } catch (Exception exception) {
                                                    logger.error(
                                                        "actionPerformed(ActionEvent)",
                                                        exception);
                                                    Tools.ihmExceptionView(exception);
                                                }

                                            }
                                        }
                                    }
                                    if (pAdminProjectData.getCurrentUserFromModel() != null
                                        && group
                                        .getUserListFromModel()
                                        .contains(
                                                  pAdminProjectData
                                                  .getCurrentUserFromModel())) {
                                        memberListModel.removeElement(group);
                                    }

                                    inGroupListModel.clear();
                                    outGroupListModel.clear();

                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(createButton);
        buttonSet.add(renameButton);
        buttonSet.add(groupDeleteButton);

        // Description du groupe

        groupDescriptionArea.setPreferredSize(new Dimension(100, 50));
        groupDescriptionArea.setEditable(false);
        JScrollPane groupDescriptionScrollPane = new JScrollPane(groupDescriptionArea);
        groupDescriptionScrollPane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), Language
                .getInstance().getText("Description")));

        // Permission
        viewButton = new JButton(Language.getInstance().getText("__Voir__"));
        viewButton.setEnabled(false);
        viewButton.setToolTipText(Language.getInstance().getText("Visualiser_les_permissions"));
        viewButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    new GroupAccess(false,
                            (Group) groupList.getSelectedValue(),
                            javax.swing.JOptionPane
                            .getFrameForComponent(AdministrationProject.this),
                            false);

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        modifyButton = new JButton(Language.getInstance().getText("Modifier"));
        modifyButton.setEnabled(false);
        modifyButton.setToolTipText(Language.getInstance().getText("Modifier_les_permissions"));
        modifyButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    GroupAccess groupAccess = new GroupAccess(true,
                            (Group) groupList.getSelectedValue(),
                            javax.swing.JOptionPane
                            .getFrameForComponent(AdministrationProject.this),
                            true);
                    if (groupAccess.getObservedGroup() != null) {
                        if (Api.isConnected()) {
                            try {
                                boolean[] tab = ((Group) groupList
                                                 .getSelectedValue())
                                    .getTestsPermissionsFromModel();
                                int perm = 0;
                                if (tab[0])
                                    perm |= Permission.ALLOW_CREATE_TEST;
                                if (tab[1])
                                    perm |= Permission.ALLOW_UPDATE_TEST;
                                if (tab[2])
                                    perm |= Permission.ALLOW_DELETE_TEST;
                                tab = ((Group) groupList.getSelectedValue())
                                    .getCampaignPermissionsFromModel();
                                if (tab[0])
                                    perm |= Permission.ALLOW_CREATE_CAMP;
                                if (tab[1])
                                    perm |= Permission.ALLOW_UPDATE_CAMP;
                                if (tab[2])
                                    perm |= Permission.ALLOW_DELETE_CAMP;
                                if (tab[3])
                                    perm |= Permission.ALLOW_EXECUT_CAMP;

                                Group pGroup = (Group) groupList.getSelectedValue();
                                pGroup.updatePermissionInDB(perm);

                            } catch (Exception exception) {
                                logger.error("actionPerformed(ActionEvent)",
                                             exception);
                                Tools.ihmExceptionView(exception);
                            }
                        } else {
                            JOptionPane
                                .showMessageDialog(AdministrationProject.this,
                                        Language.getInstance()
                                        .getText("Impossible_de_changer_les_droits_Vous_n_etes_pas_connecte_a_la_base"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JPanel permissionPanel = new JPanel();
        permissionPanel.setLayout(new BoxLayout(permissionPanel,
                                                BoxLayout.Y_AXIS));
        permissionPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), Language
                .getInstance().getText("Permissions")));
        permissionPanel.add(Box.createRigidArea(new Dimension(1, 10)));
        permissionPanel.add(viewButton);
        permissionPanel.add(Box.createRigidArea(new Dimension(1, 15)));
        permissionPanel.add(modifyButton);
        permissionPanel.add(Box.createRigidArea(new Dimension(1, 10)));

        // La liste des groupes

        groupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        groupList.setSelectedIndex(0);
        groupList.setCellRenderer(new GroupListRenderer());
        groupList.setVisibleRowCount(10);
        JScrollPane groupListScrollPane = new JScrollPane(groupList);

        groupList.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - start");
                    }

                    // Ignore extra messages.
                    if (e.getValueIsAdjusting())
                        return;
                    groupSelectedRow = groupList.getSelectedIndex();
                    if (groupSelectedRow != -1
                        && groupSelectedRow < groupListModel.size()) {
                        pAdminProjectData.setCurrentGroupInModel((Group) groupList
                                                                 .getSelectedValue());
                        // String nameFromModel = ((Group)
                        // groupList.getSelectedValue())
                        // .getNameFromModel();
                        // if (!((Group) groupList.getSelectedValue())
                        // .getNameFromModel().equals(ADMINNAME)
                        // && !((Group) groupList.getSelectedValue())
                        // .getNameFromModel().equals(DEVNAME)
                        // && !((Group) groupList.getSelectedValue())
                        // .getNameFromModel().equals(GUESTNAME)) {
                        renameButton.setEnabled(true);
                        groupDeleteButton.setEnabled(true);
                        modifyButton.setEnabled(true);

                        // } else {
                        // renameButton.setEnabled(false);
                        // groupDeleteButton.setEnabled(false);
                        // modifyButton.setEnabled(false);
                        // }
                        viewButton.setEnabled(true);
                        inGroupListModel.clear();
                        outGroupListModel.clear();

                        for (Iterator iter = pAdminProjectData
                                 .getGroupUsersFromModel((Group) groupList.getSelectedValue())
                                 .iterator(); iter.hasNext();) {
                            inGroupListModel.addElement(iter.next());
                        }
                        HashSet projectUsersSet = pAdminProjectData
                            .getUsersOfCurrentProjectFromModel();
                        for (Iterator iter = projectUsersSet.iterator(); iter
                                 .hasNext();) {
                            User projectUser = (User) iter.next();
                            if (!inGroupListModel.contains(projectUser)) {
                                outGroupListModel.addElement(projectUser);
                            }
                        }
                        inGrouplist.setSelectedIndex(0);
                        outGrouplist.setSelectedIndex(0);

                        groupDescriptionArea.setText(((Group) groupList
                                .getSelectedValue()).getDescriptionFromModel());
                    } else {
                        viewButton.setEnabled(false);
                        modifyButton.setEnabled(false);
                        renameButton.setEnabled(false);
                        groupDeleteButton.setEnabled(false);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - end");
                    }
                }
            });

        JPanel groupPanel = new JPanel();
        groupPanel.setLayout(new BoxLayout(groupPanel, BoxLayout.Y_AXIS));
        groupPanel.add(groupListScrollPane);
        groupPanel.add(buttonSet);

        JPanel allGroup = new JPanel();
        allGroup.setLayout(new BoxLayout(allGroup, BoxLayout.X_AXIS));
        allGroup.add(groupPanel);
        allGroup.add(Box.createRigidArea(new Dimension(30, 1)));
        allGroup.add(permissionPanel);
        allGroup.add(Box.createRigidArea(new Dimension(40, 1)));
        allGroup.add(groupDescriptionScrollPane);
        allGroup.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createLineBorder(Color.BLACK), Language.getInstance().getText("Groupes")));

        // Creation de la liste des utilisateurs
        inGrouplist
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        inGrouplist.setSelectedIndex(0);
        inGrouplist.setCellRenderer(new UserListRenderer());
        inGrouplist.setVisibleRowCount(5);
        JScrollPane inGroupListScrollPane = new JScrollPane(inGrouplist);

        inGroupListScrollPane.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), Language
                .getInstance().getText("Appartiennent_au_groupe")));

        // Creation de la liste des utilisateurs

        outGrouplist
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        outGrouplist.setSelectedIndex(0);
        outGrouplist.setCellRenderer(new UserListRenderer());
        outGrouplist.setVisibleRowCount(5);
        JScrollPane outGroupListScrollPane = new JScrollPane(outGrouplist);
        outGroupListScrollPane.setBorder(BorderFactory
                .createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
                        Language.getInstance().getText("N_appartiennent_pas_au_groupe")));

        // Boutons utilisateurs

        JButton addOne = new JButton("<");
        addOne.setToolTipText(Language.getInstance().getText("Ajouter_un_utilisateur"));
        addOne.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (groupList.getSelectedValue() != null) {
                        if (outGrouplist.getSelectedValue() == null) {
                            JOptionPane
                                .showMessageDialog(AdministrationProject.this,
                                        Language.getInstance()
                                        .getText("Vous_devez_selectionner_un_utilisateur_avant_de_proceder_a_cette_operation"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                        } else {
                            if (Api.isConnected()) {
                                try {
                                    User pUser = (User) outGrouplist
                                        .getSelectedValue();
                                    Group pGroup = (Group) groupList
                                        .getSelectedValue();
                                    pAdminProjectData.addUserInGroupInDBAndModel(pUser, pGroup);

                                    if (!inGroupListModel.contains(outGrouplist
                                                    .getSelectedValue())) {
                                        inGroupListModel.addElement(outGrouplist
                                                    .getSelectedValue());
                                    }
                                    if (pAdminProjectData.getCurrentUserFromModel() != null
                                        && pAdminProjectData.getCurrentUserFromModel()
                                        .equals(outGrouplist.getSelectedValue())) 
                                    {
                                        if (!memberListModel.contains(groupList
                                                        .getSelectedValue())) {
                                            memberListModel.addElement(groupList
                                                        .getSelectedValue());
                                        }
                                        notMemberListModel.removeElement(groupList
                                                        .getSelectedValue());
                                    }
                                    outGroupListModel.removeElement(outGrouplist
                                                        .getSelectedValue());
                                    inGrouplist.setSelectedIndex(0);

                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton addAll = new JButton("<<");
        addAll.setToolTipText(Language.getInstance().getText("Ajouter_tous_les_utilisateurs"));
        addAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (groupList.getSelectedValue() != null) {
                        for (int i = 0; i < outGroupListModel.size(); i++) {
                            User user = (User) outGroupListModel.get(i);
                            if (Api.isConnected()) {
                                try {
                                    Group pGroup = (Group) groupList
                                        .getSelectedValue();
                                    pAdminProjectData.addUserInGroupInDBAndModel(user, pGroup);

                                    inGroupListModel.addElement(user);
                                    if (pAdminProjectData.getCurrentUserFromModel() != null
                                        && pAdminProjectData
                                        .getCurrentUserFromModel()
                                        .equals(user)) 
                                    {
                                        if (!memberListModel.contains(groupList
                                                    .getSelectedValue())) {
                                            memberListModel.addElement(groupList
                                                    .getSelectedValue());
                                        }
                                        notMemberListModel.removeElement(groupList
                                                    .getSelectedValue());
                                    }
                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);

                                    Tools.ihmExceptionView(exception);
                                }
                            }
                        }
                        outGroupListModel.clear();
                        inGrouplist.setSelectedIndex(0);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton removeOne = new JButton(">");
        removeOne.setToolTipText(Language.getInstance().getText("Retirer_un_utilisateur"));
        removeOne.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (groupList.getSelectedValue() != null) {
                        if (inGrouplist.getSelectedValue() == null) {
                            JOptionPane
                                .showMessageDialog(AdministrationProject.this,
                                        Language.getInstance()
                                        .getText("Vous_devez_selectionner_un_utilisateur_avant_de_proceder_a_cette_operation"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                        } else {
                            if (((Group) groupList.getSelectedValue())
                                .getNameFromModel().equals(ADMINNAME)
                                && pAdminProjectData.getGroupUsersFromModel(
                                        (Group) groupList.getSelectedValue())
                                .size() == 1) 
                            {
                                JOptionPane
                                    .showMessageDialog(
                                        AdministrationProject.this,
                                        Language.getInstance()
                                        .getText("Impossible_Un_projet_doit_avoir_au_moins_un_administrateur"),
                                        Language.getInstance().getText("Erreur_"),
                                        JOptionPane.ERROR_MESSAGE);
                            } else {
                                if (Api.isConnected()) {
                                    try {
                                        Group pGroup = (Group) groupList
                                            .getSelectedValue();
                                        User pUser = (User) inGrouplist
                                            .getSelectedValue();
                                        pAdminProjectData
                                            .removeUserFromGroupInDBAndModel(pUser, pGroup);

                                        if (pAdminProjectData
                                            .getCurrentUserFromModel() != null
                                            && pAdminProjectData
                                            .getCurrentUserFromModel()
                                            .equals(
                                                    inGrouplist
                                                    .getSelectedValue())) {
                                            memberListModel.removeElement(groupList
                                                                          .getSelectedValue());
                                            if (!notMemberListModel
                                                .contains(groupList
                                                          .getSelectedValue())) {
                                                notMemberListModel
                                                    .addElement(groupList
                                                                .getSelectedValue());
                                            }
                                        }
                                        if (!outGroupListModel.contains(inGrouplist
                                                    .getSelectedValue())) 
                                        {
                                            outGroupListModel
                                                .addElement(inGrouplist
                                                            .getSelectedValue());
                                        }
                                        inGroupListModel.removeElement(inGrouplist
                                                                       .getSelectedValue());
                                    } catch (Exception exception) {
                                        logger.error("actionPerformed(ActionEvent)",
                                                     exception);
                                        Tools.ihmExceptionView(exception);
                                    }
                                }
                            }
                        }
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JButton removeAll = new JButton(">>");
        removeAll.setToolTipText(Language.getInstance().getText("Retirer_tous_les_utilisateurs"));
        removeAll.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    if (((Group) groupList.getSelectedValue()).getNameFromModel()
                        .equals(ADMINNAME)) {
                        JOptionPane
                            .showMessageDialog(
                                    AdministrationProject.this,
                                    Language.getInstance()
                                    .getText("Impossible_Un_projet_doit_avoir_au_moins_un_administrateur"),
                                    Language.getInstance().getText("Erreur_"),
                                    JOptionPane.ERROR_MESSAGE);
                    } else {
                        for (int i = 0; i < inGroupListModel.size(); i++) {
                            User user = (User) inGroupListModel.get(i);
                            if (Api.isConnected()) {
                                try {
                                    Group pGroup = (Group) groupList
                                        .getSelectedValue();
                                    pAdminProjectData
                                        .removeUserFromGroupInDBAndModel(user,
                                                                         pGroup);

                                    if (pAdminProjectData.getCurrentUserFromModel() != null
                                        && pAdminProjectData
                                        .getCurrentUserFromModel()
                                        .equals(user)) {
                                        memberListModel.removeElement(groupList
                                                    .getSelectedValue());
                                        if (!notMemberListModel.contains(groupList
                                                    .getSelectedValue())) 
                                        {
                                            notMemberListModel.addElement(groupList
                                                    .getSelectedValue());
                                        }
                                    }
                                    if (!outGroupListModel.contains(user)) {
                                        outGroupListModel.addElement(user);
                                    }
                                } catch (Exception exception) {
                                    logger.error("actionPerformed(ActionEvent)",
                                                 exception);
                                    Tools.ihmExceptionView(exception);
                                }

                            }
                        }
                        inGroupListModel.clear();
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JPanel moveButtons = new JPanel();
        moveButtons.setLayout(new BoxLayout(moveButtons, BoxLayout.Y_AXIS));
        moveButtons.setAlignmentX(Component.CENTER_ALIGNMENT);
        moveButtons.add(addOne);
        moveButtons.add(addAll);
        moveButtons.add(Box.createRigidArea(new Dimension(1, 25)));
        moveButtons.add(removeOne);
        moveButtons.add(removeAll);

        // Panel utilisateur
        JPanel usersPanel = new JPanel();
        usersPanel.setLayout(new BoxLayout(usersPanel, BoxLayout.X_AXIS));
        usersPanel.add(inGroupListScrollPane);
        usersPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        usersPanel.add(moveButtons);
        usersPanel.add(Box.createRigidArea(new Dimension(20, 50)));
        usersPanel.add(outGroupListScrollPane);
        usersPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory
                .createLineBorder(Color.BLACK), Language.getInstance().getText("Utilisateurs")));

        groupView.setLayout(new BoxLayout(groupView, BoxLayout.Y_AXIS));
        groupView.add(allGroup);
        groupView.add(Box.createRigidArea(new Dimension(1, 15)));
        groupView.add(usersPanel);
        groupView.setBorder(BorderFactory.createEmptyBorder(30, 10, 10, 10));

        if (logger.isDebugEnabled()) {
            logger.debug("createGroupsViewForOneProject() - end");
        }
    } // Fin de la methode createGroupsViewForOneProject/0

    /**
     * Methode d'initialisation des donnees a la connection sur la base de
     * donnees.
     */
    private void initDataVoiceTesting(String projectName, String adminLogin)
        throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("initDataVoiceTesting(String, String) - start");
        }

        Util.log("[AdministrationProject->initDataVoiceTesting] for project "
                 + projectName + " admin " + adminLogin);
        pAdminProjectData = new AdminProjectData();
        pAdminProjectData.loadData(adminLogin, projectName);

        Util
            .log("[AdministrationProject->initDataVoiceTesting] project loaded is "
                 + pAdminProjectData.getCurrentProjectFromModel());

        ArrayList usersList;             // new local variable
        usersList = new ArrayList();     // initialize first element
        for (Iterator iter = pAdminProjectData.getUsersOfCurrentProjectFromModel().iterator(); iter.hasNext(); ) {
            usersList.add(iter.next());  // build list of users
        }
        Collections.sort(usersList, User.Comparators.LASTNAME);     // sort list of users
        
        Vector logins = new Vector();
        Vector completedNames = new Vector();
        for (int i = 0; i < usersList.size(); i++) {
            User user = (User) usersList.get(i);
            logins.add(user.getLoginFromModel());
            completedNames.add(user.getLastNameFromModel() + " "
                               + user.getFirstNameFromModel());
        }
        usersTableModel.addDataColumn(logins, 1);
        usersTableModel.addDataColumn(completedNames, 0);

        groupListModel.clear();
        int nbGroup = pAdminProjectData.getGroupCountFromModel();
        for (int i = 0; i < nbGroup; i++) {
            groupListModel.add(i, pAdminProjectData.getGroupFromModel(i));
        }

        if (logger.isDebugEnabled()) {
            logger.debug("initDataVoiceTesting(String, String) - end");
        }
    } // Fin de la methode initDataVoiceTesting/0

    /*
     * public void update(Observable observable, Object obj) { if (obj
     * instanceof Vector) { if (((Vector)obj).size() > 0)
     * DataModel.apiExceptionView((Vector)obj); } }
     */

    @Override
    public void destroy() {
        if (logger.isDebugEnabled()) {
            logger.debug("destroy() - start");
        }

        Util.log("[AdministrationProjet] Destroy");
        // Api.setConnected(false);
        if (closeDB) {
            Api.closeConnection();
        }

        if (logger.isDebugEnabled()) {
            logger.debug("destroy() - end");
        }
    }

    LoginSalomeTMF appletParent;

    public void setAppletParent(LoginSalomeTMF appletParent) {
        if (logger.isDebugEnabled()) {
            logger.debug("setAppletParent(LoginSalomeTMF) - start");
        }

        this.appletParent = appletParent;

        if (logger.isDebugEnabled()) {
            logger.debug("setAppletParent(LoginSalomeTMF) - end");
        }
    }

    public void quit(boolean do_recup, boolean doclose) {
        if (logger.isDebugEnabled()) {
            logger.debug("quit(boolean, boolean) - start");
        }
        // pAdminProjectData.s

        // if (Api.isALLOW_PLUGINS()) {
        // int size = bugTrackers.size();
        // for (int i = 0; i < size; i++) {
        // BugTracker tracker = (BugTracker) bugTrackers.elementAt(i);
        // tracker.suspend();
        // }
        // }

        URL recup;
        try {
            if (Api.isConnected() && doclose) {
                for (Iterator iter = pAdminProjectData.getUsersFromModel()
                         .iterator(); iter.hasNext();) {
                    User user = (User) iter.next();
                    if (pAdminProjectData.getUserGroupsFromModel(user) == null
                        || pAdminProjectData.getUserGroupsFromModel(user)
                        .size() == 0) {
                        Group pGroup = pAdminProjectData
                            .getGroupFromModel(GUESTNAME);
                        try {
                            pAdminProjectData.addUserInGroupInDB(user, pGroup);
                        } catch (Exception e) {
                            logger.error("quit(boolean, boolean)", e);
                        }
                    }
                }
                if (!problemURL && !Api.isIDE_DEV() && idConn != -1) {
                    Api.deleteConnection(idConn);
                }
                closeDB = false;
                Api.closeConnection();
            }

            /* recupere l'URL a partir du document courant et "page.html" */
            if (!standAlone) {
                if (do_recup) {
                    appletParent.getAppletContext().showDocument(
                            new URL(documentBase, "index.html"));
                } else {
                    appletParent.getAppletContext().showDocument(
                            new URL("http://wiki.objectweb.org/salome-tmf"));
                }

            } else {
                if (pJSUtils != null) {
                    pJSUtils.closeWindow();
                }
            }
            if (!appletParent.isInBrowser())
                appletParent.getDialogSalome().dispose();

            javax.swing.JOptionPane
                .getFrameForComponent(appletParent.getRoot()).dispose();
            exit = true;
            System.exit(0);

        } catch (MalformedURLException me) {
            logger.error("quit(boolean, boolean)", me);
            Util.err(me);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("quit(boolean, boolean) - end");
        }
    }

    // /////////////////////////////////////// INNER CLASS
    // ////////////////////////////////

    public class UserDescriptionCaretListener implements CaretListener {
        /**
         * Logger for this class
         */
        private final Logger logger = Logger
            .getLogger(UserDescriptionCaretListener.class);

        @Override
        public void caretUpdate(CaretEvent e) {
            if (logger.isDebugEnabled()) {
                logger.debug("caretUpdate(CaretEvent) - start");
            }

            // Tools.ihmExceptionView(new Exception("Not yet implemented"));

            if (logger.isDebugEnabled()) {
                logger.debug("caretUpdate(CaretEvent) - end");
            }
            return;

        }

    }

    /***************** Windows Listener ***************/
    @Override
    public void windowClosing(WindowEvent e) {
        if (logger.isDebugEnabled()) {
            logger.debug("windowClosing(WindowEvent) - start");
        }

        if (!exit)
            quit(true, true);

        if (logger.isDebugEnabled()) {
            logger.debug("windowClosing(WindowEvent) - end");
        }
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void addBugTrackers(Object bugTracker) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addPlgToUICompList(Integer id, Common plugin) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addReqManager(Object reqManager) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addStatistiquess(Object statistiques) {
        // TODO Auto-generated method stub

    }

    @Override
    public void addXMLPrinterExtension(Object xmlPrinter) {
        // TODO Auto-generated method stub

    }

    @Override
    public Object associatedExtension(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedExtension(Object, Object) - start");
        }

        if (associatedExtension == null)
            associatedExtension = new Hashtable<Object, Object>();

        Object returnObject = associatedExtension.put(key, value);
        if (logger.isDebugEnabled()) {
            logger.debug("associatedExtension(Object, Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object associatedScriptEngine(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedScriptEngine(Object, Object) - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("associatedScriptEngine(Object, Object) - end");
        }
        return null;
    }

    @Override
    public Object associatedTestDriver(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedTestDriver(Object, Object) - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("associatedTestDriver(Object, Object) - end");
        }
        return null;
    }

    @Override
    public Object get1ssociatedTestDriver(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("get1ssociatedTestDriver(Object) - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("get1ssociatedTestDriver(Object) - end");
        }
        return null;
    }

    @Override
    public Object getAssociatedExtension(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedExtension(Object) - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedExtension(Object) - end");
        }
        return null;
    }

    @Override
    public Object getAssociatedScriptEngine(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedScriptEngine(Object) - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedScriptEngine(Object) - end");
        }
        return null;
    }

    @Override
    public Vector getBugTracker() {
        if (logger.isDebugEnabled()) {
            logger.debug("getBugTracker() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getBugTracker() - end");
        }
        return null;
    }

    @Override
    public JPFManager getPluginManager() {
        if (logger.isDebugEnabled()) {
            logger.debug("getPluginManager() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getPluginManager() - end");
        }
        return null;
    }

    @Override
    public Vector getReqManagers() {
        if (logger.isDebugEnabled()) {
            logger.debug("getReqManagers() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getReqManagers() - end");
        }
        return null;
    }

    @Override
    public Vector getStatistiques() {
        if (logger.isDebugEnabled()) {
            logger.debug("getStatistiques() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getStatistiques() - end");
        }
        return null;
    }

    @Override
    public URL getUrlBase() {
        if (logger.isDebugEnabled()) {
            logger.debug("getUrlBase() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getUrlBase() - end");
        }
        return null;
    }

    @Override
    public Vector getXMLPrintersExtension() {
        if (logger.isDebugEnabled()) {
            logger.debug("getXMLPrintersExtension() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getXMLPrintersExtension() - end");
        }
        return null;
    }

    @Override
    public void initExtsionScriptEngine(ExtensionPoint ext) {
        // TODO Auto-generated method stub

    }

    @Override
    public void initExtsionTestDriver(ExtensionPoint ext) {
        // TODO Auto-generated method stub

    }

    @Override
    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackerE,
                               ExtensionPoint reqMgr) {
        // TODO Auto-generated method stub

    }

    public void ii(PluginManager pluginManager, ExtensionPoint commonE,
                   ExtensionPoint testE, ExtensionPoint scriptE,
                   ExtensionPoint bugTrackerE, ExtensionPoint statistiquesE,
                   ExtensionPoint reqMgr) {
        // TODO Auto-generated method stub

    }

    @Override
    public void showMessage(String msg) {
        // TODO Auto-generated method stub

    }

    public SalomeTMFContext getSalomeTMFContext() {
        if (logger.isDebugEnabled()) {
            logger.debug("getSalomeTMFContext() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getSalomeTMFContext() - end");
        }
        return null;
    }

    public SalomeTMFPanels getSalomeTMFPanels() {
        if (logger.isDebugEnabled()) {
            logger.debug("getSalomeTMFPanels() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("getSalomeTMFPanels() - end");
        }
        return null;
    }

    public boolean isGraphique() {
        if (logger.isDebugEnabled()) {
            logger.debug("isGraphique() - start");
        }

        // TODO Auto-generated method stub

        if (logger.isDebugEnabled()) {
            logger.debug("isGraphique() - end");
        }
        return true;
    }

    public void showDocument(URL toShow, String where) {
        // TODO Auto-generated method stub

    }

    @Override
    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackerE,
                               ExtensionPoint statistiquesE, ExtensionPoint reqMgr) {
        // TODO Auto-generated method stub

    }
} // Fin de la classe AdminProject
