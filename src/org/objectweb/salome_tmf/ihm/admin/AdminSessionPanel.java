/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

/* TODO */
/* Rendre accessible aux plugins */
/* ex : // Mapping entre composants graphiques et constantes
   UIComponentsMap.put(UICompCst.ADMIN_PROJECT_MANAGEMENT_BUTTONS_PANEL,buttonSet);
*/

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.data.ConnectionWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLSession;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;

public class AdminSessionPanel extends JPanel implements ActionListener {
        
    JTable sessiontTable;
        
    MyTableModel sessionTableModel;
        
    JButton refreshSessionButton;
    JButton deleteSessionButton;
    JButton deleteAllSessionButton;
        
    ISQLSession pISQLSession;
        
    Vector datasessionTable;
        
    public AdminSessionPanel(){
        super(new BorderLayout());
        initComponent();
    }
    void initComponent(){
        sessionTableModel = new MyTableModel();
        sessiontTable = new JTable();
                
                
        deleteSessionButton = new JButton(Language.getInstance().getText("Supprimer"));
        deleteSessionButton.addActionListener(this);
                
        deleteAllSessionButton = new JButton(Language.getInstance().getText("Supprimer_les_sessions"));
        deleteAllSessionButton.addActionListener(this);
                
        refreshSessionButton = new JButton(Language.getInstance().getText("Actualiser"));
        refreshSessionButton.addActionListener(this);
                
        sessionTableModel.addColumnNameAndColumn("Id");
        sessionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Projet"));
        sessionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Login"));
        sessionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Host"));
        sessionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Date"));
        sessionTableModel.addColumnNameAndColumn(Language.getInstance().getText("Time"));
                
                
        sessiontTable.setModel(sessionTableModel);
                
        sessiontTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        JScrollPane tablePane = new JScrollPane(sessiontTable);
                
                
        //loadDataFromDB();
                
        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(refreshSessionButton);
        buttonSet.add(deleteSessionButton);
        buttonSet.add(deleteAllSessionButton);
        buttonSet.setBorder(BorderFactory.createRaisedBevelBorder());
                
        JLabel sessionsTableLabel = new JLabel(Language.getInstance().getText("Liste_des_sessions_existantes__"));
        sessionsTableLabel.setFont(new Font(null,Font.BOLD,20));
        sessionsTableLabel.setSize(300,60);
                
        JPanel center = new JPanel(new BorderLayout());
        center.add(BorderLayout.NORTH, sessionsTableLabel);
        center.add(BorderLayout.CENTER, tablePane);
        center.setBorder(BorderFactory.createEmptyBorder(20,10,10,10));   
        this.add(BorderLayout.NORTH,buttonSet);
        this.add(BorderLayout.CENTER,center);
    }
        
    public void loadDataFromDB(){
        // On cree une instance pour les methodes de selection
        pISQLSession = Api.getISQLObjectFactory().getISQLSession();
        refreshData();
    }
        
    void refreshData(){
        try {
            ConnectionWrapper[] tmpArray = pISQLSession.getAllSession();
            Vector tmpVector = new Vector();
            for(int i = 0; i < tmpArray.length; i++) {
                tmpVector.add(tmpArray[i]);
            }
                        
            datasessionTable =  tmpVector;
            sessionTableModel.clearTable();
            int nbData = datasessionTable.size();
            for (int i = 0 ; i < nbData ; i++ ) {
                ConnectionWrapper pConnectionWrapper = (ConnectionWrapper)datasessionTable.elementAt(i);
                sessionTableModel.addValueAt(new Integer(pConnectionWrapper.getId()), i, 0);
                sessionTableModel.addValueAt(pConnectionWrapper.getProjectConnected(), i, 1);
                sessionTableModel.addValueAt(pConnectionWrapper.getLoginConnected(), i, 2);
                sessionTableModel.addValueAt(pConnectionWrapper.getHostConnected(), i, 3);
                sessionTableModel.addValueAt(pConnectionWrapper.getDateConnected(), i, 4);
                sessionTableModel.addValueAt(new Time(pConnectionWrapper.getTimeConnected()), i, 5);
            }    
        }catch (Exception ex){}
    }
        
    @Override
    public void actionPerformed(ActionEvent e){
        if (e.getSource().equals(deleteSessionButton)) {
            try { 
                int selectedRow = sessiontTable.getSelectedRow();
                if (selectedRow != -1) {
                    Integer value = (Integer) sessionTableModel.getValueAt(selectedRow,0);
                    deleteSession(value.intValue());
                }
            }catch (Exception ex){}
                        
        } else if (e.getSource().equals(deleteAllSessionButton)) {
            deleteAllSession();
        } else if (e.getSource().equals(refreshSessionButton)){
            refreshData();
        }
    }
        
    void deleteSession(int id_session){
        try {
            pISQLSession.deleteSession(id_session);
            refreshData();
        } catch (Exception e){          
        }
                
    }
        
    void deleteAllSession(){     
        try {
            pISQLSession.deleteAllSession();
            refreshData();
        } catch (Exception e){
        }
    }
}
