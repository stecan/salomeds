/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer les
 * informations pour les nouveaux utilisateurs
 */
public class AskNewUser extends JDialog {

    /**
     * Le Field permettant de recuperer le login
     */
    private JTextField loginField;

    /**
     * Le Field permettant de recuperer le prenom
     */
    private JTextField firstNameField;

    /**
     * Le Field permettant de recuperer le nom
     */
    private JTextField lastNameField;

    /**
     * Le Field permettant de recuperer l'email
     */
    private JTextField emailField;

    /**
     * Le Field permettant de recuperer la numero de telephone
     */
    private JTextField phoneField;

    /**
     * Le Field permettant de recuperer le mot de passe
     */
    private JPasswordField pwdField;

    /**
     * L'utilisateur cree
     */
    private User user;

    /**
     * Le mot de passe par defaut pour le nouvel utilisateur
     */
    private char[] password;

    boolean newUser = false;
    String oldUserLogin = "";

    AdminVTData pAdminVTData;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/

    /**
     * Constructeur de la fenetre.
     * @param textToBePrompt chaine correspondant a la demande.
     */
    public AskNewUser(User oldUser, Frame owner,  AdminVTData adminVTData) {

        super(owner,true);
        this.pAdminVTData = adminVTData;

        if (oldUser == null) {
            newUser = true;
        } else {
            oldUserLogin = oldUser.getLoginFromModel();
        }

        loginField = new JTextField(10);
        firstNameField = new JTextField(10);
        lastNameField = new JTextField(10);
        emailField = new JTextField(10);
        phoneField = new JTextField(10);
        pwdField = new JPasswordField(10);
        JPanel page = new JPanel();

        JLabel obligation = new JLabel(Language.getInstance().getText("_champ_obligatoire"));

        JLabel loginLabel = new JLabel(Language.getInstance().getText("Login__"));
        JLabel lastNameLabel = new JLabel(Language.getInstance().getText("NomEtoile"));
        JLabel firstNameLabel = new JLabel(Language.getInstance().getText("PrenomEtoile"));
        JLabel emailLabel = new JLabel(Language.getInstance().getText("Email__"));
        JLabel phoneLabel = new JLabel(Language.getInstance().getText("Telephone__"));
        JLabel pwdLabel = new JLabel(Language.getInstance().getText("Mot_de_passe__"));

        JPanel allField = new JPanel();
        allField.setLayout(new BoxLayout(allField, BoxLayout.Y_AXIS));
        allField.add(Box.createVerticalGlue());
        allField.add(loginField);
        allField.add(Box.createRigidArea(new Dimension(1,15)));
        allField.add(lastNameField);
        allField.add(Box.createRigidArea(new Dimension(1,15)));
        allField.add(firstNameField);
        allField.add(Box.createRigidArea(new Dimension(1,15)));
        allField.add(emailField);
        allField.add(Box.createRigidArea(new Dimension(1,15)));
        allField.add(phoneField);
        if (newUser && (Api.getUserAuthentification().equalsIgnoreCase("DataBase") || 
                Api.getUserAuthentification().equalsIgnoreCase("both"))) {
            allField.add(Box.createRigidArea(new Dimension(1,15)));
            allField.add(pwdField);
        }

        JPanel giveName = new JPanel();
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.Y_AXIS));
        giveName.add(Box.createVerticalGlue());
        giveName.add(loginLabel);
        giveName.add(Box.createRigidArea(new Dimension(1, 20)));
        giveName.add(lastNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(1, 20)));
        giveName.add(firstNameLabel);
        giveName.add(Box.createRigidArea(new Dimension(1, 20)));
        giveName.add(emailLabel);
        giveName.add(Box.createRigidArea(new Dimension(1, 20)));
        giveName.add(phoneLabel);
        if (newUser && (Api.getUserAuthentification().equalsIgnoreCase("DataBase") || 
                Api.getUserAuthentification().equalsIgnoreCase("both"))) {
            giveName.add(Box.createRigidArea(new Dimension(1, 20)));
            giveName.add(pwdLabel);
        }  
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.X_AXIS));

        textPanel.add(giveName);
        textPanel.add(allField);


        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    boolean ok = true;
                    user = new User("", "");
                    if ((!loginField.getText().trim().equals("")) && (!loginField.getText().trim().equals(""))) {
                        if ((!pAdminVTData.containsLoginInModel(loginField.getText().trim())) ||
                            (oldUserLogin.equals(loginField.getText().trim()))) {
                            user.setLoginInModel(loginField.getText().trim());
                            if (!lastNameField.getText().equals("")) {
                                user.setLastNameInModel(lastNameField.getText());
                                if (!firstNameField.getText().equals("")) {
                                    user.setFirstNameInModel(firstNameField.getText());
                                    if (!emailField.getText().equals("")) {
                                        user.setEmailInModel(emailField.getText());
                                        if (Api.getUserAuthentification().equalsIgnoreCase("Ldap")){
                                            // For Ldap, a password is not used. 
                                            // To serve the subsequent calls, 
                                            // a default password is given.
                                            pwdField.setText(" ");
                                            password = new char[7];
                                            password[0] = '9';
                                            password[1] = 'd';
                                            password[2] = '4';
                                            password[3] = 'p';
                                            password[4] = 'O';
                                            password[5] = 'U';
                                            password[6] = 'ß';
                                        } else {
                                            if (pwdField.getPassword().length != 0) {
                                                password = pwdField.getPassword();
                                            } else if (newUser) {
                                                warning(pwdField, Language.getInstance().getText("Vous_devez_entrer_un_mot_de_passe_par_defaut_"));
                                                ok = false;
                                            }
                                        }
                                    } else {
                                        warning(emailField, Language.getInstance().getText("Vous_devez_entrer_un_email_"));
                                        ok = false;
                                    }
                                } else {
                                    warning(firstNameField, Language.getInstance().getText("Vous_devez_entrer_un_prenom_"));
                                    ok = false;
                                }
                            } else {
                                warning(lastNameField, Language.getInstance().getText("Vous_devez_entrer_un_nom_"));
                                ok = false;
                            }
                        } else {
                            JOptionPane.showMessageDialog(AskNewUser.this,
                                                          Language.getInstance().getText("Ce_login_existe_deja_"),
                                                          Language.getInstance().getText("Erreur_"),
                                                          JOptionPane.ERROR_MESSAGE);
                            ok = false;
                        }
                    } else {
                        warning(loginField, Language.getInstance().getText("Vous_devez_entrer_un_nom_de_login_"));
                        ok = false;
                    }
                    if (phoneField.getText() != "") {
                        user.setPhoneNumberInModel(phoneField.getText());
                    }
                    if (ok) AskNewUser.this.dispose();
                }
            });

        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    user = null;
                    password = null;
                    AskNewUser.this.dispose();
                }
            });

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);

        JPanel obligationPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        obligationPanel.add(obligation);

        page.add(textPanel,BorderLayout.WEST);
        page.add(Box.createRigidArea(new Dimension(40,10)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);

        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        contentPaneFrame.add(obligationPanel, BorderLayout.SOUTH);

        initData(oldUser);
        if (oldUser != null) {
            this.setTitle(Language.getInstance().getText("Modifier_un_utilisateur"));
        } else {
            this.setTitle(Language.getInstance().getText("Creer_un_nouvel_utilisateur"));
        }

        //this.setLocation(400,300);
        this.setLocationRelativeTo(this.getParent()); 
        this.pack();
        this.setVisible(true);
    } // Fin du constructeur AskName/1

    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/

    /**
     * Recupere le nouvel utilisateur
     * @return
     */
    public User getUser() {
        return user;
    } // Fin de la methode getUser/0

    private void initData(User oldUser) {
        if (oldUser != null) {
            user = oldUser;
            loginField.setText(oldUser.getLoginFromModel());
            firstNameField.setText(oldUser.getFirstNameFromModel());
            lastNameField.setText(oldUser.getLastNameFromModel());
            emailField.setText(oldUser.getEmailFromModel());
            phoneField.setText(oldUser.getPhoneNumberFromModel());

        } else {
            user = new User("", "");
        }
    }

    /**
     * Affiche une fenetre de warning para rapport au field passe en parametre
     * avec le message passe en parametre
     * @param field le field concerne
     * @param msg message a afficher
     */
    public void warning(JTextField field, String msg) {
        field.selectAll();
        JOptionPane.showMessageDialog(
                                      AskNewUser.this,
                                      msg,
                                      Language.getInstance().getText("Erreur_"),
                                      JOptionPane.ERROR_MESSAGE);
        field.requestFocusInWindow();
    } // Fin de la methode warning/2

    /**
     * Retourne le mot de passe
     * @return le mot de passe
     */
    public char[] getPassword() {
        return password;
    } // Fin de la methode getPassword/0
} // Fin de la classe AskNewUser
