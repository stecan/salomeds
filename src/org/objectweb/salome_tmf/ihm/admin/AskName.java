/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui creer une fenetre permettant de recuperer une chaine de
 * caractere.
 */
public class AskName extends JDialog {
    
    /**
     * La chaine recuperee
     */
    private String result;
    
    /**
     * Le Field permettant de recuperer la chaine
     */
    private JTextField nomProjet;
    
    private String message;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre.
     * @param textToBePrompt chaine correspondant a la demande.
     * @param title le titre de la fenetre
     */
    public AskName(String textToBePrompt, String title, String type, String oldValue, Frame owner) {
        
        super(owner,true);
        
        int t_x = 1024;
        int t_y = 768 ;
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle r = gc[0].getBounds();
            t_x = r.width ;
            t_y = r.height  ;
        } catch(Exception E){
                        
        }
        
        nomProjet  = new JTextField(25);
        JPanel page = new JPanel();
        
        JLabel text = new JLabel("");
        
        JLabel prompt = new JLabel(textToBePrompt);
        
        JPanel giveName = new JPanel();
        
        initData(type, oldValue);
        
        
        giveName.setLayout(new BoxLayout(giveName, BoxLayout.X_AXIS));
        giveName.setBorder(BorderFactory.createEmptyBorder(0, 15, 15, 15));
        giveName.add(Box.createHorizontalGlue());
        giveName.add(prompt);
        giveName.add(Box.createRigidArea(new Dimension(15, 0)));
        giveName.add(nomProjet);
        
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new BoxLayout(textPanel,BoxLayout.Y_AXIS));
        textPanel.add(Box.createRigidArea(new Dimension(0,15)));
        textPanel.add(giveName);
        text.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        JButton okButton = new JButton(Language.getInstance().getText("Valider"));
        okButton.setToolTipText(Language.getInstance().getText("Valider"));
        okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (nomProjet.getText() != null) {
                        result = nomProjet.getText().trim();
                    } else {
                        result = "";
                    }
                    AskName.this.dispose();
                }
            });
        
        nomProjet.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if ((nomProjet.getText() != null) && (!nomProjet.getText().trim().equals(""))) {
                        result = nomProjet.getText().trim();
                        AskName.this.dispose();
                    } else if ((nomProjet.getText() != null) && (nomProjet.getText().trim().equals("")) && message.equals("valeur")) {
                        result = nomProjet.getText().trim();
                        AskName.this.dispose();
                    } else {
                        nomProjet.selectAll();
                        JOptionPane.showMessageDialog(
                                                      AskName.this,
                                                      "Vous devez entrez " + message + ".",
                                                      "Erreur !",
                                                      JOptionPane.ERROR_MESSAGE);
                        result = null;
                        nomProjet.requestFocusInWindow();
                    
                    }
                
                }
            });
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    result = null;
                    AskName.this.dispose();
                }
            });
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BorderLayout());
        buttonPanel.add(okButton,BorderLayout.NORTH);
        buttonPanel.add(cancelButton,BorderLayout.SOUTH);
        
        page.add(textPanel,BorderLayout.WEST);
        //page.add(Box.createRigidArea(new Dimension(50,15)),BorderLayout.CENTER);
        page.add(buttonPanel,BorderLayout.EAST);
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.add(page, BorderLayout.CENTER);
        
        int longeur = contentPaneFrame.getSize().width;
        int hauteur = contentPaneFrame.getSize().height;
        //this.setLocation((t_x -longeur)/3 ,(t_y -hauteur)/2);
        this.setLocationRelativeTo(this.getParent()); 
        this.setTitle(title);
        this.pack();
        this.setVisible(true);
    } // Fin du constructeur AskName/1
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Recupere le nom entre
     * @return le nom entre par l'utilisateur
     */
    public String getResult() {
        return result;
    } // Fin de la methode getResult/0
    
    private void initData(String type, String oldValue) {
        if (oldValue != null) {
            nomProjet.setText(oldValue);
        }
        if (type.equals("valeur")) {
            message = "une valeur";
        } else if (type.equals("passe")) {
            message = "un mot de passe";
        } else if (type.equals("url")) {
            message = "une url";
        } else if (type.equals("classpath")) {
            message = " un classPath";
        } else if (type.equals("class")) {
            message = "une classe";
        } else {
            message = "un nom";
        }
    }
} // Fin de la classe AskName
