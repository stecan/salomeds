package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.log4j.Logger;
import org.java.plugin.Extension;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.JavaScriptUtils;
import org.objectweb.salome_tmf.api.MD5paswd;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.AdminLogin;
import org.objectweb.salome_tmf.ihm.main.BaseIHM;
import org.objectweb.salome_tmf.ihm.models.MyTableModel;
import org.objectweb.salome_tmf.ihm.tools.Tools;
import org.objectweb.salome_tmf.login.LoginSalomeTMF;
import org.objectweb.salome_tmf.plugins.IPlugObject;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.UICompCst;
import org.objectweb.salome_tmf.plugins.core.Admin;
import org.objectweb.salome_tmf.plugins.core.Common;

public class Administration extends JApplet implements IPlugObject,
                                                       ApiConstants, IHMConstants, ActionListener, WindowListener {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(Administration.class);

    boolean problemURL = false;

    JPasswordField oldPassword;
    JPasswordField newPasswordFirst;
    JPasswordField newPasswordSecond;

    BaseIHM pBaseIHM;
    
    public Administration(BaseIHM m_BaseIHM) {
        pBaseIHM = m_BaseIHM;
    }
    
    
    /**
     * Les onglets d'administration
     */
    JTabbedPane tabs;

    /**
     * Le panel de la fenetre dans son ensemble
     */
    JPanel adminVoiceTesting;

    /**
     * Vue pour changer les mots de passe
     */
    JPanel changePwdView;

    /**
     * Vue pour la gestion des projets
     */
    JPanel projectView;

    /**
     * Vue pour la gestion des utilisateurs
     */
    JPanel usersView;

    /**
     * Vue pour la gestion des plugins
     */
    JPanel pluginsView;

    /**
     * Vue de droite de la fenetre
     */
    JPanel adminView;

    /**
     * Vue de droite de la fenetre pour le controle des sessions
     */
    AdminSessionPanel sessionView;
    /**
     * Modele de donnees de la table des projets
     */
    MyTableModel projectTableModel;

    /**
     * Table des projets
     */
    JTable projectTable;

    /**
     * Modele de donnees de la table des utilisateurs
     */
    MyTableModel usersTableModel;

    /**
     * Table des utilisateurs
     */
    JTable userTable;

    /**
     * Bouton pour supprimer un projet
     */
    JButton deleteProjectButton;

    /**
     * Bouton pour la suppression d'un utilisateur
     */
    JButton deleteUserButton;

    /**
     * Bouton pour le changement de mot de passe d'un utilisateur
     */

    Color normalColor;

    Color freezeColor;

    Color localColor;

    JButton modifyProjectButton;
    JButton createProjectButton;
    JButton freezeButton;
    JButton createUserButton;
    JButton changeUserPwdButton;
    JButton modifyUserButton;
    JButton validateButton;
    JButton cancelButton;
    /**
     * Mapping between UI components and constants defined in
     * org.objectweb.salome_tmf.ihm.UICompCst
     */
    static Map UIComponentsMap;

    public static Frame ptrFrame;

    public static Hashtable associatedTestDriver;
    public static Hashtable associatedScriptEngine;
    public static Hashtable associatedExtension;
    public static Vector bugTrackers;
    public static Vector statistiquess;
    public static Vector reqManagers;
    public Vector xmlPrinters = new Vector();
    public static JPFManager jpf;

    public static URL urlAdmin = null;

    URL recup;

    /*
     * dbClosed = true if connection to database is close normaly (no reload or
     * back from browser)
     */
    boolean closeDB = true;
    /*
     * reload = true if applet is start again from same JVM (reload or back from
     * browser)
     */
    // static boolean reload = false;

    String adminSalome = null;
    int idConn = -1;

    static public AdminVTData pAdminVTData;
    boolean standAlone = false;
    static boolean exit = false;
    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/
    JavaScriptUtils pJSUtils;

    void onInit() {
        if (logger.isDebugEnabled()) {
            logger.debug("onInit() - start");
        }

        standAlone = false;
        if (connectToAPI()) {
            closeDB = true;
            initComponent();
            loadModel();
        }
        exit = false;

        if (logger.isDebugEnabled()) {
            logger.debug("onInit() - end");
        }
    }

    void onStart() {

    }

    private URL documentBase;

    public void setDocumentBase(URL documentBase) {
        if (logger.isDebugEnabled()) {
            logger.debug("setDocumentBase(URL) - start");
        }

        this.documentBase = documentBase;

        if (logger.isDebugEnabled()) {
            logger.debug("setDocumentBase(URL) - end");
        }
    }

    void initComponent() {
        if (logger.isDebugEnabled()) {
            logger.debug("initComponent() - start");
        }

        System.runFinalization();
        System.gc();

        javax.swing.JOptionPane.getFrameForComponent(Administration.this)
            .addWindowListener(this);

        Api.setUrlBase(documentBase);
        urlAdmin = documentBase;

        Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
        ptrFrame = javax.swing.JOptionPane
            .getFrameForComponent(Administration.this);
        try {
            Class lnfClass = Class.forName(UIManager
                                           .getSystemLookAndFeelClassName());
            LookAndFeel newLAF = (LookAndFeel) (lnfClass.newInstance());
            UIManager.setLookAndFeel(newLAF);
            Util.adaptFont();
        } catch (Exception exc) {
            logger.error("initComponent()", exc);

            Util.err("Error loading L&F: " + exc);
        }

        oldPassword = new JPasswordField(20);

        newPasswordFirst = new JPasswordField(20);

        newPasswordSecond = new JPasswordField(20);

        tabs = new JTabbedPane();

        adminVoiceTesting = new JPanel(new BorderLayout());

        changePwdView = new JPanel(new BorderLayout());

        projectView = new JPanel(new BorderLayout());

        usersView = new JPanel(new BorderLayout());

        sessionView = new AdminSessionPanel();

        adminView = new JPanel(new BorderLayout());

        projectTableModel = new MyTableModel();

        usersTableModel = new MyTableModel();

        userTable = new JTable();

        associatedTestDriver = new Hashtable();
        associatedScriptEngine = new Hashtable();
        associatedExtension = new Hashtable();
        bugTrackers = new Vector();
        statistiquess = new Vector();
        reqManagers = new Vector();

        deleteProjectButton = new JButton(Language.getInstance().getText(
                                                                         "Supprimer"));

        deleteUserButton = new JButton(Language.getInstance().getText(
                                                                      "Supprimer"));

        changeUserPwdButton = new JButton(Language.getInstance().getText(
                                                                         "Changer_le_mot_de_passe"));

        normalColor = new Color(204, 255, 204);
        freezeColor = new Color(204, 204, 255);
        localColor = Color.RED;

        // Mapping between UI components and constants
        UIComponentsMap = new HashMap();

        // Onglets
        tabs.addTab(Language.getInstance().getText("Administration_SalomeTMF"),
                    adminVoiceTesting);
        tabs.setPreferredSize(new Dimension(500, 500));

        // Create the radio buttons.
        JRadioButton changePwdButton = new JRadioButton(Language.getInstance()
                                                        .getText("Changer_le_mot_de_passe"));
        changePwdButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    adminView.removeAll();
                    adminView.add(changePwdView, BorderLayout.CENTER);
                    adminView.validate();
                    adminView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JRadioButton projectButton = new JRadioButton(Language.getInstance()
                                                      .getText("Gerer_les_projets"));
        projectButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    adminView.removeAll();
                    adminView.add(projectView, BorderLayout.CENTER);
                    adminView.validate();
                    adminView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        JRadioButton usersButton = new JRadioButton(Language.getInstance()
                                                    .getText("Gerer_les_utilisateurs"));
        usersButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    adminView.removeAll();
                    adminView.add(usersView, BorderLayout.CENTER);
                    adminView.validate();
                    adminView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        // JRadioButton pluginsButton = new
        // JRadioButton("Installation de plugins");
        // pluginsButton.addActionListener(new ActionListener() {
        // public void actionPerformed(ActionEvent e) {
        // if (logger.isDebugEnabled()) {
        // logger.debug("actionPerformed(ActionEvent) - start");
        // }
        //
        // adminView.removeAll();
        // createPluginsView();
        // adminView.add(pluginsView);
        // adminView.validate();
        // adminView.repaint();
        //
        // if (logger.isDebugEnabled()) {
        // logger.debug("actionPerformed(ActionEvent) - end");
        // }
        // }
        // });
        // ***************** Panel de controle des sessions
        // *********************//
        JRadioButton sessionButton = new JRadioButton(Language.getInstance()
                                                      .getText("Gerer_les_sessions"));
        sessionButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    adminView.removeAll();
                    adminView.add(sessionView, BorderLayout.CENTER);
                    adminView.validate();
                    adminView.repaint();

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        // ////////////////////////////////////////////////////////////////////////
        // On groupe les radiobuttons
        ButtonGroup adminButtonGroup = new ButtonGroup();
        adminButtonGroup.add(changePwdButton);
        adminButtonGroup.add(projectButton);
        adminButtonGroup.add(usersButton);
        adminButtonGroup.add(sessionButton);
        // adminButtonGroup.add(pluginsButton);

        JPanel radioPanel = new JPanel(new GridLayout(0, 1));
        radioPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        radioPanel.add(changePwdButton);
        radioPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        radioPanel.add(projectButton);
        radioPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        radioPanel.add(usersButton);
        radioPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        radioPanel.add(sessionButton);
        radioPanel.add(Box.createRigidArea(new Dimension(1, 40)));
        // radioPanel.add(pluginsButton);

        JPanel testPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        testPanel.add(radioPanel);

        JButton backButton = new JButton(Language.getInstance().getText(
                                                                        "Quitter"));
        backButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    pBaseIHM.quit(true, true);
                    
                    // quit(true, true);
                }
            });

        JPanel radioPanelAll = new JPanel(new BorderLayout());
        radioPanelAll.add(testPanel);
        radioPanelAll.add(backButton, BorderLayout.SOUTH);
        radioPanelAll.setBorder(BorderFactory.createRaisedBevelBorder());

        // AdminProjectData.initData();
        // AdminVTData.initData();
        createChangePwdView();
        createUsersView();
        createProjectView();
        adminView.removeAll();

        // On remplit le panel adminVoiceTesting
        adminVoiceTesting.add(BorderLayout.WEST, radioPanelAll);
        adminVoiceTesting.add(BorderLayout.CENTER, adminView);

        /* Ajout de la partie installation de plugin */
        // tabs.add("IFGRZ", new JLabel("fnezilfnezife"));

        Container cp = this.getContentPane();
        cp.add(tabs);

        if (logger.isDebugEnabled()) {
            logger.debug("initComponent() - end");
        }
    }

    boolean connectToAPI() {
        if (logger.isDebugEnabled()) {
            logger.debug("connectToAPI() - start");
        }

        Util.log("[Administration->connectToAPI] open connection");
        pJSUtils = new JavaScriptUtils(this);
        if (Api.getNbConnect() > 0) {
            // JOptionPane
            // .showMessageDialog(
            // Administration.this,
            // Language
            // .getInstance()
            // .getText(
            // "Vous_avez_deja_une_session_Salome_ouverte_avec_ce_navigateur")
            // + Language
            // .getInstance()
            // .getText(
            // "Une_seule_session_est_autorisee_par_navigateur_afin_d_eviter_les_conflits_"),
            // Language.getInstance().getText("Erreur_"),
            // JOptionPane.ERROR_MESSAGE);
            // quit(false, false);
            // closeDB = false;

            if (logger.isDebugEnabled()) {
                logger.debug("connectToAPI() - end");
            }
            return true;
        } else {
            Api.openConnection(documentBase);
            if (!Api.isConnected()) {
                JOptionPane.showMessageDialog(Administration.this,
                                              "Can't connect to the database", Language.getInstance()
                                              .getText("Erreur_"), JOptionPane.ERROR_MESSAGE);
                closeDB = false;
                quit(true, false);

                if (logger.isDebugEnabled()) {
                    logger.debug("connectToAPI() - end");
                }
                return false;
            }

            String[] params = new String[2];
            ;
            /*
             * if (!Api.isIDE_DEV()) { params =
             * Tools.chooseProdocumentBasecumentBase());
             * Util.log("[Administration.start()] conn. with project = " +
             * params[0]);
             * Util.log("[Administration.start()] conn. with user = " +
             * params[1]); } else { params = new String[2]; params[0] = "ALL";
             * params[1] = ADMIN_SALOME_NAME;
             *
             * }
             */
            params[1] = pJSUtils.getLoginCookies();
            params[0] = pJSUtils.getProjectCookies();
            Api.setUserAuthentification(pJSUtils
                                        .getUserAuthentficationCookies());
            if ((params[0] == null) || (params[0].equals(""))) {
                AdminLogin dialog = new AdminLogin();
                dialog.setVisible(true);
                if (dialog.isLoged()) {
                    params[0] = "ALL";
                    params[1] = ADMIN_SALOME_NAME;
                    standAlone = true;

                    // Recuperation du login et du mot de passe pour
                    // l'authentification
                    try {
                        Api.setStrUsername(ADMIN_SALOME_NAME);
                        Api.setStrMD5Password(MD5paswd
                                              .getEncodedPassword(dialog
                                                                  .getSelectedPassword()));
                    } catch (Exception e) {
                        logger.error("connectToAPI()", e);

                        Util.err(e);
                    }
                }
            }
            adminSalome = ADMIN_SALOME_NAME;
            Api.initConnectionUser("ALL", ADMIN_SALOME_NAME);
            /*
             * try { String[] tab = documentBase.toString().split("[?=]");
             * idConn = Integer.parseInt(tab[2]); } catch(Exception e){
             * Util.log(
             * "[Administration->connectToAPI] WARNING idConn can't be found");
             * } if ((params[0] == null)||(params[0].equals("")) ||
             * (!params[0].equals("ALL"))) {
             * JOptionPane.showMessageDialog(Administration.this,
             * Language.getInstance().getText("Probleme_dans_l_URL_saisie_"),
             * Language.getInstance().getText("Erreur_"),
             * JOptionPane.ERROR_MESSAGE);
             *
             * problemURL = true; closeDB = true; quit(true, true); return
             * false; }
             */

            if (logger.isDebugEnabled()) {
                logger.debug("connectToAPI() - end");
            }
            return true;
        }
    }

    void loadModel() {
        loadSalomeData();
//        if (Api.isALLOW_PLUGINS()) {
//            jpf = new JPFManager();
//            jpf.startJPFInAdmin(this.documentBase, UIComponentsMap, this);
//        }
    }

    /**
     * Methode d'initialisation de l'applet
     */
    @Override
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug("init() - start");
        }

        onInit();

        if (logger.isDebugEnabled()) {
            logger.debug("init() - end");
        }
    }

    /**
     * Methode appelee a chaque chargement de la page
     */

    @Override
    public void start() {
        if (logger.isDebugEnabled()) {
            logger.debug("start() - start");
        }

        onStart();

        if (logger.isDebugEnabled()) {
            logger.debug("start() - end");
        }
    }

    /**
     * Methode qui creer l'ecran permettant de changer le mot de passe.
     */
    public void createChangePwdView() {
        if (logger.isDebugEnabled()) {
            logger.debug("createChangePwdView() - start");
        }

        adminView.removeAll();

        validateButton = new JButton(Language.getInstance().getText("Valider"));
        validateButton
            .setToolTipText(Language.getInstance().getText("Valider"));
        validateButton.addActionListener(this);

        cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(this);

        JLabel oldQuestion = new JLabel(Language.getInstance().getText(
                                                                       "Entrez_votre_ancien_mot_de_passe_"));
        JLabel question = new JLabel(Language.getInstance().getText(
                                                                    "Entrez_votre_nouveau_mot_de_passe_"));
        JLabel confirmation = new JLabel(Language.getInstance().getText(
                                                                        "Confirmer_le_nouveau_mot_de_passe_"));

        JPanel textFieldPane = new JPanel();
        textFieldPane.setLayout(new BoxLayout(textFieldPane, BoxLayout.Y_AXIS));
        textFieldPane.add(oldPassword);
        textFieldPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textFieldPane.add(newPasswordFirst);
        textFieldPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textFieldPane.add(newPasswordSecond);

        JPanel textPane = new JPanel();
        textPane.setLayout(new BoxLayout(textPane, BoxLayout.Y_AXIS));
        textPane.add(oldQuestion);
        textPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textPane.add(question);
        textPane.add(Box.createRigidArea(new Dimension(1, 50)));
        textPane.add(confirmation);

        JPanel textPaneAll = new JPanel(new FlowLayout(FlowLayout.CENTER));
        textPaneAll.add(textPane);
        textPaneAll.add(textFieldPane);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        buttonPanel.add(validateButton);
        buttonPanel.add(cancelButton);

        JPanel labelSet = new JPanel();
        labelSet.add(textPaneAll);
        labelSet.setBorder(BorderFactory.createEmptyBorder(100, 10, 10, 100));

        changePwdView.setLayout(new BoxLayout(changePwdView, BoxLayout.Y_AXIS));
        changePwdView.add(labelSet);
        changePwdView.add(buttonPanel);
        changePwdView.setBorder(BorderFactory.createRaisedBevelBorder());
        changePwdView.validate();

        if (logger.isDebugEnabled()) {
            logger.debug("createChangePwdView() - end");
        }
    }

    /**
     * Methode qui permet de creer l'ecran permettant de gerer les projets.
     *
     */
    public void createProjectView() {
        if (logger.isDebugEnabled()) {
            logger.debug("createProjectView() - start");
        }

        adminView.removeAll();

        // Les boutons
        createProjectButton = new JButton(Language.getInstance().getText(
                                                                         "Creer"));
        createProjectButton.setToolTipText(Language.getInstance().getText(
                                                                          "Creer_un_nouveau_projet"));
        createProjectButton.addActionListener(this);

        modifyProjectButton = new JButton(Language.getInstance().getText(
                                                                         "Modifier"));
        modifyProjectButton.setEnabled(false);
        modifyProjectButton.setToolTipText(Language.getInstance().getText(
                                                                          "Modifier_un_projet"));
        modifyProjectButton.addActionListener(this);

        freezeButton = new JButton(Language.getInstance().getText("Geler"));
        freezeButton.setEnabled(false);
        freezeButton.setToolTipText(Language.getInstance().getText(
                                                                   "Geler_un_projet"));
        freezeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - start");
                    }

                    int selectedRow = projectTable.getSelectedRow();
                    if (selectedRow != -1) {
                        /*
                         * Project project =
                         * pAdminVTData.getProjectFromModel((String
                         * )projectTableModel.getValueAt(selectedRow,1)); if
                         * (project.getLock() == null) {
                         * ProjectTableCellRenderer.setColor(selectedRow,
                         * freezeColor);
                         * projectTableModel.setValueAt(Tools.createAppletImageIcon
                         * (PATH_TO_FREEZED_PROJECT_ICON,""), selectedRow, 0);
                         * project.setLock(new Lock()); } else {
                         * ProjectTableCellRenderer.setColor(selectedRow,
                         * normalColor);
                         * projectTableModel.setValueAt(Tools.createAppletImageIcon
                         * (PATH_TO_PROJECT_ICON,""), selectedRow, 0);
                         * project.setLock(null); }
                         */

                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("actionPerformed(ActionEvent) - end");
                    }
                }
            });

        deleteProjectButton.setToolTipText(Language.getInstance().getText(
                                                                          "Supprimer_un_projet"));
        deleteProjectButton.setEnabled(false);
        deleteProjectButton.addActionListener(this);

        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(createProjectButton);
        buttonSet.add(modifyProjectButton);
        //              buttonSet.add(freezeButton);
        buttonSet.add(deleteProjectButton);
        buttonSet.setBorder(BorderFactory.createRaisedBevelBorder());
        // Mapping entre composants graphiques et constantes
        UIComponentsMap.put(UICompCst.ADMIN_PROJECT_MANAGEMENT_BUTTONS_PANEL,
                            buttonSet);

        // La table des projets
        JLabel projectsTableLabel = new JLabel(Language.getInstance().getText(
                                                                              "Liste_des_projets_existants__"));
        projectsTableLabel.setFont(new Font(null, Font.BOLD, 20));
        projectsTableLabel.setSize(300, 60);

        projectTableModel.addColumnNameAndColumn("");
        projectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Nom_du_projet"));
        projectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Administrateur"));
        projectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Date_de_creation"));
        projectTableModel.addColumnNameAndColumn(Language.getInstance()
                                                 .getText("Description"));

        projectTable = new JTable();
        // projectTable.setDefaultRenderer(String.class, new
        // ProjectTableCellRenderer());
        projectTable.setModel(projectTableModel);
        projectTable
            .setPreferredScrollableViewportSize(new Dimension(600, 200));
        projectTable
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        // Mapping between UI components and constants
        UIComponentsMap.put(UICompCst.ADMIN_PROJECT_MANAGEMENT_TABLE,
                            projectTable);

        projectTable.setModel(projectTableModel);

        JScrollPane tablePane = new JScrollPane(projectTable);

        ListSelectionModel rowSMForUserTable = projectTable.getSelectionModel();
        rowSMForUserTable.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - start");
                    }

                    int[] selectedRows = projectTable.getSelectedRows();
                    if (selectedRows.length > 0) {
                        if (selectedRows.length == 1) {
                            modifyProjectButton.setEnabled(true);
                        } else {
                            modifyProjectButton.setEnabled(false);
                        }
                        deleteProjectButton.setEnabled(true);
                        freezeButton.setEnabled(true);
                    } else {
                        deleteProjectButton.setEnabled(false);
                        modifyProjectButton.setEnabled(false);
                        freezeButton.setEnabled(false);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - end");
                    }
                }
            });

        JPanel center = new JPanel(new BorderLayout());
        center.add(BorderLayout.NORTH, projectsTableLabel);
        center.add(BorderLayout.CENTER, tablePane);
        center.setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

        projectView.add(BorderLayout.NORTH, buttonSet);
        projectView.add(BorderLayout.CENTER, center);

        if (logger.isDebugEnabled()) {
            logger.debug("createProjectView() - end");
        }
    } // Fin de la classe createProjectView/0

    String avaiblePugin = "";
    String currentPlugin;
    Properties pluginProperties;
    FileInputStream ins;

    /**
     * Methode qui permet de creer l'ecran permettant de gerer les plugins
     *
     */
    public void createPluginsView() {
        if (logger.isDebugEnabled()) {
            logger.debug("createPluginsView() - start");
        }

        JPanel container = new JPanel();

        /**
         * Je dois d'abord r�pertorier tous les plugins de Salome.
         */

        /**
         * Je parcours le dossier de plugin
         */
        File pluginsFolder = new File("plugins");
        String plugins[] = pluginsFolder.list();
        Set<String> pluginList = new HashSet<String>();
        for (int i = 0; i < plugins.length; i++) {
            /**
             * Pour chaque fichier si c'est un dossier ou un fichier .zip je le
             * consid�re comme un plugin
             **/
            File plugin = new File("plugins/" + plugins[i]);
            if (plugin.isDirectory() || plugins[i].endsWith(".zip"))
                pluginList.add(plugins[i]);
        }

        /**
         * Je r�cup�re les plugins install�
         */
        try {
            ins = new FileInputStream("plugins/CfgPlugins.properties");
            pluginProperties = new Properties();
            pluginProperties.load(ins);
            ins.close();
            avaiblePugin = pluginProperties.getProperty("pluginsList", "core");
        } catch (FileNotFoundException e) {
            logger.error("createPluginsView()", e);

            e.printStackTrace();
        } catch (IOException e) {
            logger.error("createPluginsView()", e);

            e.printStackTrace();
        }
        String[] installedPlugin = avaiblePugin.split(",");

        /**
         * Puis je cr�e l'affichage correspondant
         */
        container.setLayout(new GridBagLayout());
        container.setBorder(BorderFactory.createTitledBorder(BorderFactory
                                                             .createLineBorder(Color.BLACK), "plugins"));
        Iterator<String> iterPlugin = pluginList.iterator();

        /**
         * Je parcours tous les plugins trouv�
         * */
        for (int gridy = 0; iterPlugin.hasNext(); gridy++) {
            currentPlugin = iterPlugin.next();
            container.add(new JLabel(currentPlugin.replace(".zip", "")),
                          new GridBagConstraints(0, gridy, 1, 1, 0.1, 0.1,
                                                 GridBagConstraints.NORTHWEST,
                                                 GridBagConstraints.NONE, new Insets(10, 10, 0, 10),
                                                 0, 0));

            boolean isInstalled = false;
            /**
             * je regarde si le plugin est install�
             */
            for (int i = 0; i < installedPlugin.length; i++) {
                if (installedPlugin[i].trim().equals(currentPlugin)) {
                    isInstalled = true;
                    break;
                }
            }
            JButton button = new JButton();
            if (isInstalled) {
                button.setText("D�sactiver");
                button.addActionListener(new ActionListener() {
                        String current = currentPlugin;

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (logger.isDebugEnabled()) {
                                logger
                                    .debug("actionPerformed(ActionEvent) - start");
                            }

                            String[] pluginList = avaiblePugin.split(",");
                            avaiblePugin = "core";
                            for (int i = 0; i < pluginList.length; i++) {
                                if (!(pluginList[i].trim().equals(current))
                                    && !(pluginList[i].trim().equals("core")))
                                    avaiblePugin += "," + pluginList[i];
                            }
                            pluginProperties.setProperty("pluginsList",
                                                         avaiblePugin);
                            FileOutputStream out;
                            try {
                                out = new FileOutputStream(
                                                           "plugins/CfgPlugins.properties");
                                pluginProperties.store(out, "");
                                out.close();
                            } catch (FileNotFoundException e1) {
                                logger.error("actionPerformed(ActionEvent)", e1);

                                e1.printStackTrace();
                            } catch (IOException e1) {
                                logger.error("actionPerformed(ActionEvent)", e1);

                                e1.printStackTrace();
                            }
                            adminView.removeAll();
                            createPluginsView();
                            adminView.add(pluginsView);
                            adminView.validate();
                            adminView.repaint();

                            if (logger.isDebugEnabled()) {
                                logger.debug("actionPerformed(ActionEvent) - end");
                            }
                        }
                    });
            } else {
                if (currentPlugin.endsWith(".zip"))
                    button.setText("Installer");
                else
                    button.setText("Activer");
                button.addActionListener(new ActionListener() {
                        String current = currentPlugin;

                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (logger.isDebugEnabled()) {
                                logger
                                    .debug("actionPerformed(ActionEvent) - start");
                            }

                            /**
                             * Si c'est un fichier zip
                             */
                            if (current.endsWith(".zip")) {
                                try {
                                    boolean firstDir = true;
                                    boolean toWrite = true;
                                    ZipFile pZipFile = new ZipFile("plugins/"
                                                                   + current);
                                    Enumeration<ZipEntry> pZipEntries = (Enumeration<ZipEntry>) pZipFile
                                        .entries();
                                    while (pZipEntries.hasMoreElements()) {
                                        ZipEntry pEntry = pZipEntries.nextElement();
                                        // System.out.println("Found entry : " +
                                        // pEntry.getName());
                                        if (pEntry.isDirectory()) {
                                            if (firstDir) {
                                                firstDir = false;
                                                String plgName = pEntry.getName();
                                                if (plgName.endsWith("/")
                                                    || plgName.endsWith("\\")) {
                                                    plgName = plgName.substring(0,
                                                                                plgName.length() - 1);
                                                }
                                                StringTokenizer st = new StringTokenizer(
                                                                                         avaiblePugin, ",");
                                                while (st.hasMoreTokens()
                                                       && toWrite) {
                                                    String token = st.nextToken()
                                                        .trim();
                                                    if (token.equals(plgName)) {
                                                        toWrite = false;
                                                    }
                                                }
                                                if (toWrite) {
                                                    avaiblePugin += ", " + plgName;
                                                    pluginProperties.setProperty(
                                                                                 "pluginsList",
                                                                                 avaiblePugin);
                                                }
                                            }
                                            (new File("plugins/" + pEntry.getName()))
                                                .mkdir();
                                        } else {
                                            copyInputStream(
                                                            pZipFile.getInputStream(pEntry),
                                                            new BufferedOutputStream(
                                                                                     new FileOutputStream(
                                                                                                          "plugins/"
                                                                                                          + pEntry
                                                                                                          .getName())));

                                        }
                                    }
                                    pZipFile.close();
                                } catch (IOException e1) {
                                    logger
                                        .error("actionPerformed(ActionEvent)",
                                               e1);

                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }
                            } else {
                                avaiblePugin += ", " + current;
                                pluginProperties.setProperty("pluginsList",
                                                             avaiblePugin);
                            }
                            FileOutputStream out;
                            try {
                                out = new FileOutputStream(
                                                           "plugins/CfgPlugins.properties");
                                pluginProperties.store(out, "");
                                out.close();
                            } catch (FileNotFoundException e1) {
                                logger.error("actionPerformed(ActionEvent)", e1);

                                e1.printStackTrace();
                            } catch (IOException e1) {
                                logger.error("actionPerformed(ActionEvent)", e1);

                                e1.printStackTrace();
                            }

                            adminView.removeAll();
                            createPluginsView();
                            adminView.add(pluginsView);
                            adminView.validate();
                            adminView.repaint();

                            if (logger.isDebugEnabled()) {
                                logger.debug("actionPerformed(ActionEvent) - end");
                            }
                        }
                    });
            }

            container.add(button, new GridBagConstraints(1, gridy, 1, 1, 0.1,
                                                         0.1, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                                                         new Insets(10, 10, 0, 10), 0, 0));
        }

        pluginsView = new JPanel();
        pluginsView.add(container);

        if (logger.isDebugEnabled()) {
            logger.debug("createPluginsView() - end");
        }
    }

    public static final void copyInputStream(InputStream in, OutputStream out)
        throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("copyInputStream(InputStream, OutputStream) - start");
        }

        byte[] buffer = new byte[1024];
        int len;
        while ((len = in.read(buffer)) >= 0)
            out.write(buffer, 0, len);

        in.close();
        out.close();

        if (logger.isDebugEnabled()) {
            logger.debug("copyInputStream(InputStream, OutputStream) - end");
        }
    }

    /**
     * Methode qui permet de creer l'ecran permettant de gerer les utilisateurs.
     *
     */
    public void createUsersView() {
        if (logger.isDebugEnabled()) {
            logger.debug("createUsersView() - start");
        }

        // Les boutons
        createUserButton = new JButton(Language.getInstance().getText("Creer"));
        createUserButton.setToolTipText(Language.getInstance().getText(
                                                                       "Creer_un_nouvel_utilisateur"));
        createUserButton.addActionListener(this);

        modifyUserButton = new JButton(Language.getInstance().getText(
                                                                      "Modifier"));
        modifyUserButton.setToolTipText(Language.getInstance().getText(
                                                                       "Modifier_un_utilisateur"));
        modifyUserButton.setEnabled(false);
        modifyUserButton.addActionListener(this);

        deleteUserButton.setToolTipText(Language.getInstance().getText(
                                                                       "Supprimer_un_utilisateur"));
        deleteUserButton.setEnabled(false);
        deleteUserButton.addActionListener(this);

        changeUserPwdButton.setToolTipText(Language.getInstance().getText(
                                                                          "Changer_le_mot_de_passe"));
        changeUserPwdButton.setEnabled(false);
        changeUserPwdButton.addActionListener(this);

        JPanel buttonSet = new JPanel(new FlowLayout());
        buttonSet.setAlignmentY(FlowLayout.LEFT);
        buttonSet.add(createUserButton);
        buttonSet.add(modifyUserButton);
        buttonSet.add(deleteUserButton);
        if (Api.getUserAuthentification() == null
            || Api.getUserAuthentification().equalsIgnoreCase("DataBase")) {
            buttonSet.add(changeUserPwdButton);
        }
        buttonSet.setBorder(BorderFactory.createRaisedBevelBorder());

        // La table des projets
        JLabel projectsTableLabel = new JLabel(Language.getInstance().getText(
                                                                              "Liste_des_utilisateurs__"));
        projectsTableLabel.setFont(new Font(null, Font.BOLD, 20));
        projectsTableLabel.setSize(300, 60);

        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                              "Login"));
        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                              "Nom"));
        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                              "Prenom"));
        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                              "Email"));
        usersTableModel.addColumnNameAndColumn(Language.getInstance().getText(
                                                                              "Telephone"));

        userTable.setModel(usersTableModel);

        userTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        JScrollPane tablePane = new JScrollPane(userTable);
        userTable
            .setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        ListSelectionModel rowSMForUserTable = userTable.getSelectionModel();
        rowSMForUserTable.addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - start");
                    }

                    int[] selectedRows = userTable.getSelectedRows();
                    if (selectedRows.length > 0) {
                        if (selectedRows.length == 1) {
                            changeUserPwdButton.setEnabled(true);
                            modifyUserButton.setEnabled(true);
                        } else {
                            changeUserPwdButton.setEnabled(false);
                            modifyUserButton.setEnabled(false);
                        }
                        deleteUserButton.setEnabled(true);
                    } else {
                        deleteUserButton.setEnabled(false);
                        changeUserPwdButton.setEnabled(false);
                        modifyUserButton.setEnabled(false);
                    }

                    if (logger.isDebugEnabled()) {
                        logger.debug("valueChanged(ListSelectionEvent) - end");
                    }
                }
            });

        JPanel center = new JPanel(new BorderLayout());
        center.add(BorderLayout.NORTH, projectsTableLabel);
        center.add(BorderLayout.CENTER, tablePane);
        center.setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

        usersView.add(BorderLayout.NORTH, buttonSet);
        usersView.add(BorderLayout.CENTER, center);

        if (logger.isDebugEnabled()) {
            logger.debug("createUsersView() - end");
        }
    } // Fin de la methode createUsersView/0

    /**
     * Methode d'initialisation des donnees a la connection sur la base de
     * donnees.
     */
    private void loadSalomeData() {
        if (logger.isDebugEnabled()) {
            logger.debug("loadSalomeData() - start");
        }

        try {
            // DB
            pAdminVTData = new AdminVTData();

            pAdminVTData.loadData();
            // pAdminVTData.setUserAdminVTData(ADMIN_SALOME_NAME);
            pAdminVTData.setUserAdminVTData(ADMIN_SALOME_NAME);
            // IHM
            usersTableModel.clearTable();
            ArrayList allUser = pAdminVTData.getAllUsersFromModel();
            Vector logins = new Vector();
            Vector names = new Vector();
            Vector firstNames = new Vector();
            Vector phones = new Vector();
            Vector emails = new Vector();

            // Vector descriptions = new Vector();

            for (int i = 0; i < allUser.size(); i++) {
                User pUser = (User) allUser.get(i);
                logins.add(pUser.getLoginFromModel());
                names.add(pUser.getLastNameFromModel());
                firstNames.add(pUser.getFirstNameFromModel());
                phones.add(pUser.getPhoneNumberFromModel());
                emails.add(pUser.getEmailFromModel());
            }
            usersTableModel.addDataColumn(logins, 0);
            usersTableModel.addDataColumn(names, 1);
            usersTableModel.addDataColumn(firstNames, 2);
            usersTableModel.addDataColumn(emails, 3);
            usersTableModel.addDataColumn(phones, 4);

            projectTableModel.clearTable();
            ArrayList allProjects = pAdminVTData.getProjectListFromModel();

            Vector projectNames = new Vector();
            Vector projectDescs = new Vector();
            Vector projectDate = new Vector();
            Vector projectAdminLogins = new Vector();
            int nbProj = allProjects.size();
            for (int i = 0; i < nbProj; i++) {
                Project pProject = (Project) allProjects.get(i);
                projectNames.add(pProject.getNameFromModel());
                projectDescs.add(pProject.getDescriptionFromModel());
                projectDate.add(pProject.getCreationDateFromModel());
                projectAdminLogins.add(pProject.getAdministratorFromModel()
                                       .getLoginFromModel());

            }

            projectTableModel.addDataColumn(projectNames, 1);
            projectTableModel.addDataColumn(projectAdminLogins, 2);
            projectTableModel.addDataColumn(projectDate, 3);
            projectTableModel.addDataColumn(projectDescs, 4);

            for (int i = 0; i < nbProj; i++) {
                projectTableModel.addValueAt(Tools.createAppletImageIcon(
                                                                         PATH_TO_PROJECT_ICON, ""), i, 0);
            }

            projectTable.getColumnModel().getColumn(0).setMaxWidth(18);
            sessionView.loadDataFromDB();
        } catch (Exception e) {
            logger.error("loadSalomeData()", e);

            Util.err(e);
            // ConnectionData.setConnected(false);
            JOptionPane
                .showMessageDialog(
                                   Administration.this,
                                   Language
                                   .getInstance()
                                   .getText(
                                            "Impossible_de_se_connecter_a_la_base__on_travaille_en_local_"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("loadSalomeData() - end");
        }
    } // Fin de la methode initDataVoiceTesting/0

    /**
     *
     * @param project
     * @return
     */
    public int getIndexOfProject(Project project) {
        if (logger.isDebugEnabled()) {
            logger.debug("getIndexOfProject(Project) - start");
        }

        for (int i = 0; i < projectTableModel.getRowCount(); i++) {
            if (projectTableModel.getValueAt(i, 1).equals(
                                                          project.getNameFromModel())) {
                if (logger.isDebugEnabled()) {
                    logger.debug("getIndexOfProject(Project) - end");
                }
                return i;
            }
        }
        int returnint = -1;
        if (logger.isDebugEnabled()) {
            logger.debug("getIndexOfProject(Project) - end");
        }
        return returnint;
    }

    /*
     * public void update(Observable observable, Object obj) { if (obj
     * instanceof Vector) { if (((Vector)obj).size() > 0)
     * DataModel.apiExceptionView((Vector)obj); } }
     */

    @Override
    public void destroy() {
        if (logger.isDebugEnabled()) {
            logger.debug("destroy() - start");
        }

        Util.log("[Administration] Destroy");
        // Api.isAlreadyConnected=false;
        if (closeDB) {
            Api.closeConnection();
        }
        Util.log("[Administration] End Destroy");

        if (logger.isDebugEnabled()) {
            logger.debug("destroy() - end");
        }
    }

    LoginSalomeTMF appletParent;

    public void setAppletParent(LoginSalomeTMF appletParent) {
        if (logger.isDebugEnabled()) {
            logger.debug("setAppletParent(LoginSalomeTMF) - start");
        }

        this.appletParent = appletParent;

        if (logger.isDebugEnabled()) {
            logger.debug("setAppletParent(LoginSalomeTMF) - end");
        }
    }

    public void quit(boolean do_recup, boolean doclose) {
        if (logger.isDebugEnabled()) {
            logger.debug("quit(boolean, boolean) - start");
        }

        Util.log("[Administration] Quit");

        if (Api.isConnected() && doclose) {
            if (!problemURL && !Api.isIDE_DEV() && idConn != -1) {
                Api.deleteConnection(idConn);
            }
            closeDB = false;
            Api.closeConnection();
        }

        Frame ptrFrame = javax.swing.JOptionPane
            .getFrameForComponent(appletParent);
        ptrFrame.dispose();
        ptrFrame = null;


        try {
            if (!standAlone) {
                if (do_recup) {
                    /*
                     * recupere l'URL a partir du document courant et
                     * "page.html"
                     */
                    recup = new URL(documentBase, "index.html");
                    /*
                     * Affiche le document apres avoir recuperer le contexte
                     * courant
                     */
                    appletParent.getAppletContext().showDocument(recup);
                    repaint();
                } else {
                    getAppletContext().showDocument(
                                                    new URL("http://wiki.objectweb.org/salome-tmf"));
                }
            } else {
                if (pJSUtils != null) {
                    pJSUtils.closeWindow();
                }
            }
            if (!appletParent.isInBrowser())
                appletParent.getDialogSalome().dispose();

            javax.swing.JOptionPane
                .getFrameForComponent(appletParent.getRoot()).dispose();
        } catch (MalformedURLException me) {
            logger.error("quit(boolean, boolean)", me);

            recup = null;
            Util.err(me);
        }
        exit = true;

        if (logger.isDebugEnabled()) {
            logger.debug("quit(boolean, boolean) - end");
        }
        System.exit(0);
    }

    /*
     * void closeWindow(){ try { //Class classJSObject =
     * Class.forName("sun.plugin.javascript.JSObject"); Class classJSObject =
     * Class.forName("netscape.javascript.JSObject"); Method meth_getWindow =
     * classJSObject.getMethod("getWindow", new Class[] {Applet.class}); Object
     * pJSObject = meth_getWindow.invoke(null ,new Object[] {this});
     * Util.debug("pJSObject = " + pJSObject); Method meth_call =
     * classJSObject.getMethod("call", new Class[] {String.class,
     * Object[].class}); meth_call.invoke(pJSObject, new Object[] {"close",
     * null});
     *
     * } catch (Exception e) { Util.err(e); } }
     */

    /**
     * Methode qui renvoie l'objet graphique associ? ? une constante
     *
     * @return
     * @param uiConst
     */
    public static Object getUIComponent(Integer uiConst) {
        if (logger.isDebugEnabled()) {
            logger.debug("getUIComponent(Integer) - start");
        }

        Object comp = null;
        if (UIComponentsMap != null)
            comp = UIComponentsMap.get(uiConst);

        if (logger.isDebugEnabled()) {
            logger.debug("getUIComponent(Integer) - end");
        }
        return comp;
    }

    /******************************** IPlugObject Implementation ********************/
    @Override
    public Object associatedExtension(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedExtension(Object, Object) - start");
        }

        // implementation by Hastable
        Object returnObject = associatedExtension.put(key, value);
        if (logger.isDebugEnabled()) {
            logger.debug("associatedExtension(Object, Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object associatedScriptEngine(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedScriptEngine(Object, Object) - start");
        }

        // implementation by Hastable
        Object returnObject = associatedScriptEngine.put(key, value);
        if (logger.isDebugEnabled()) {
            logger.debug("associatedScriptEngine(Object, Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object associatedTestDriver(Object key, Object value) {
        if (logger.isDebugEnabled()) {
            logger.debug("associatedTestDriver(Object, Object) - start");
        }

        // implementation by Hastable
        Object returnObject = associatedTestDriver.put(key, value);
        if (logger.isDebugEnabled()) {
            logger.debug("associatedTestDriver(Object, Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object getAssociatedExtension(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedExtension(Object) - start");
        }

        Object returnObject = associatedExtension.get(key);
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedExtension(Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object getAssociatedScriptEngine(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedScriptEngine(Object) - start");
        }

        Object returnObject = associatedScriptEngine.get(key);
        if (logger.isDebugEnabled()) {
            logger.debug("getAssociatedScriptEngine(Object) - end");
        }
        return returnObject;
    }

    @Override
    public Object get1ssociatedTestDriver(Object key) {
        if (logger.isDebugEnabled()) {
            logger.debug("get1ssociatedTestDriver(Object) - start");
        }

        Object returnObject = associatedTestDriver.get(key);
        if (logger.isDebugEnabled()) {
            logger.debug("get1ssociatedTestDriver(Object) - end");
        }
        return returnObject;
    }

    @Override
    public void initExtsionTestDriver(ExtensionPoint ext) {
        if (logger.isDebugEnabled()) {
            logger.debug("initExtsionTestDriver(ExtensionPoint) - start");
        }

        if (!ext.getConnectedExtensions().isEmpty()) {
            for (Iterator it = ext.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                try {
                    Extension testDriverExt = (Extension) it.next();
                    String extList = testDriverExt.getParameter("extensions")
                        .valueAsString();
                    associatedTestDriver(testDriverExt, extList);
                    associatedExtension(testDriverExt.getId(), testDriverExt);
                    Util.log("Add testDriverExt : " + testDriverExt.getId());
                } catch (Exception e) {
                    logger.error("initExtsionTestDriver(ExtensionPoint)", e);

                    // TODO
                    Util.err(e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("initExtsionTestDriver(ExtensionPoint) - end");
        }
    }

    @Override
    public void initExtsionScriptEngine(ExtensionPoint pExtsionScriptEngine) {
        if (logger.isDebugEnabled()) {
            logger.debug("initExtsionScriptEngine(ExtensionPoint) - start");
        }

        if (!pExtsionScriptEngine.getConnectedExtensions().isEmpty()) {
            for (Iterator it = pExtsionScriptEngine.getConnectedExtensions()
                     .iterator(); it.hasNext();) {
                try {
                    Extension scriptEngineExt = (Extension) it.next();
                    String extList = scriptEngineExt.getParameter("extensions")
                        .valueAsString();
                    associatedScriptEngine(scriptEngineExt, extList);
                    associatedExtension(scriptEngineExt.getId(),
                                        scriptEngineExt);
                    Util.log("Add ExtsionScriptEngine : "
                             + scriptEngineExt.getId());
                } catch (Exception e) {
                    logger.error("initExtsionScriptEngine(ExtensionPoint)", e);

                    // TODO
                    Util.err(e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("initExtsionScriptEngine(ExtensionPoint) - end");
        }
    }

    @Override
    public void showMessage(String msg) {
        // initExtsionScriptEngine
    }

    @Override
    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackE,
                               ExtensionPoint reqMgr) {

    }

    @Override
    public void addPlgToUICompList(Integer id, Common plugin) {

    }

    @Override
    public URL getUrlBase() {
        if (logger.isDebugEnabled()) {
            logger.debug("getUrlBase() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getUrlBase() - end");
        }
        return urlAdmin;
    }

    @Override
    public JPFManager getPluginManager() {
        if (logger.isDebugEnabled()) {
            logger.debug("getPluginManager() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getPluginManager() - end");
        }
        return jpf;
    }

    @Override
    public void addXMLPrinterExtension(Object xmlPrinter) {
        if (logger.isDebugEnabled()) {
            logger.debug("addXMLPrinterExtension(Object) - start");
        }

        if (!xmlPrinters.contains(xmlPrinter)) {
            xmlPrinters.add(xmlPrinter);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("addXMLPrinterExtension(Object) - end");
        }
    }

    @Override
    public Vector getXMLPrintersExtension() {
        if (logger.isDebugEnabled()) {
            logger.debug("getXMLPrintersExtension() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getXMLPrintersExtension() - end");
        }
        return xmlPrinters;
    }

    /******************************** ActionListener Implemntation ******************/

    @Override
    public void actionPerformed(ActionEvent e) {
        if (logger.isDebugEnabled()) {
            logger.debug("actionPerformed(ActionEvent) - start");
        }

        if (e.getSource().equals(deleteProjectButton)) {
            deleteProjectPerformed();
        } else if (e.getSource().equals(modifyProjectButton)) {
            modifyProjectPerformed();
        } else if (e.getSource().equals(validateButton)) {
            validatePerformed();
        } else if (e.getSource().equals(cancelButton)) {
            cancelPerformed();
        } else if (e.getSource().equals(createProjectButton)) {
            createProjectPerformed();
        } else if (e.getSource().equals(createUserButton)) {
            createUserPerformed();
        } else if (e.getSource().equals(modifyUserButton)) {
            modifyUserPerformed();
        } else if (e.getSource().equals(deleteUserButton)) {
            deleteUserPerformed();
        } else if (e.getSource().equals(changeUserPwdButton)) {
            changeUserPwdPerformed();
        }

        if (logger.isDebugEnabled()) {
            logger.debug("actionPerformed(ActionEvent) - end");
        }
    }

    void changeUserPwdPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("changeUserPwdPerformed() - start");
        }

        AskName askName = new AskName(Language.getInstance().getText(
                                                                     "Nouveau_mot_de_passe__"), Language.getInstance().getText(
                                                                                                                               "Changer_le_mot_de_passe"), Language.getInstance().getText(
                                                                                                                                                                                          "passe"), null, javax.swing.JOptionPane
                                      .getFrameForComponent(Administration.this));
        if (askName.getResult() != null) {
            if (Api.isConnected()) {
                int selectedIndex = userTable.getSelectedRow();
                if (selectedIndex != -1) {
                    try {
                        String userLogin = (String) usersTableModel.getValueAt(
                                                                               selectedIndex, 0);
                        User pUser = pAdminVTData.getUserFromModel(userLogin);
                        pUser.updatePasswordInDB(askName.getResult());
                    } catch (Exception exception) {
                        logger.error("changeUserPwdPerformed()", exception);

                        Tools.ihmExceptionView(exception);
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("changeUserPwdPerformed() - end");
        }
    }

    void deleteUserPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("deleteUserPerformed() - start");
        }

        int[] selectedRowsTab = userTable.getSelectedRows();
        String message = "";
        for (int i = selectedRowsTab.length - 1; i >= 0; i--) {
            message = message + " * "
                + usersTableModel.getValueAt(selectedRowsTab[i], 0);
            if (i == 0) {
                message = message + "?";
            } else {
                message = message + "\n";
            }
        }
        Object[] options = { Language.getInstance().getText("Oui"),
                             Language.getInstance().getText("Non") };
        int choice = JOptionPane
            .showOptionDialog(
                              Administration.this,
                              Language
                              .getInstance()
                              .getText(
                                       "Etes_vous_sur_de_vouloir_supprimer_les_utilisateurs__")
                              + message, Language.getInstance().getText(
                                                                        "Attention_"), JOptionPane.YES_NO_OPTION,
                              JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (choice == JOptionPane.YES_OPTION) {
            for (int i = selectedRowsTab.length - 1; i >= 0; i--) {
                ArrayList projectsList = pAdminVTData
                    .getProjectsWithThisUserAdminFromModel((String) usersTableModel
                                                           .getValueAt(selectedRowsTab[i], 0));
                ArrayList prjsWithUsrAsUniqueAdmin = new ArrayList();
                if (projectsList.size() > 0) {
                    message = "";
                    boolean prjWithUniqueAdmin = false;
                    for (int k = projectsList.size() - 1; k >= 0; k--) {
                        if (pAdminVTData
                            .getAdminCountFromModel((Project) projectsList
                                                    .get(k)) == 1) {
                            message = message
                                + " * "
                                + ((Project) projectsList.get(k))
                                .getNameFromModel() + "\n";
                            prjsWithUsrAsUniqueAdmin.add(projectsList.get(k));
                            prjWithUniqueAdmin = true;
                        }
                    }
                    if (prjWithUniqueAdmin) {
                        choice = JOptionPane
                            .showOptionDialog(
                                              Administration.this,
                                              Language.getInstance().getText(
                                                                             "L_utilisateur_")
                                              + (String) usersTableModel
                                              .getValueAt(
                                                          selectedRowsTab[i],
                                                          0)
                                              + Language
                                              .getInstance()
                                              .getText(
                                                       "_est_le_seul_administrateur_des_projets_")
                                              + message
                                              + Language
                                              .getInstance()
                                              .getText(
                                                       "Sa_suppression_entrainera_la_suppression_des_projets_dans_leur_ensemble_Etes_vous_sur_de_vouloir_supprimer_cet_utilisateur_"),
                                              Language.getInstance().getText(
                                                                             "Attention_"),
                                              JOptionPane.YES_NO_OPTION,
                                              JOptionPane.QUESTION_MESSAGE, null,
                                              options, options[1]);
                        if (choice == JOptionPane.YES_OPTION) {
                            removeUserAtIndex(i, selectedRowsTab);
                            for (int k = prjsWithUsrAsUniqueAdmin.size() - 1; k >= 0; k--) {
                                Project pProject = (Project) prjsWithUsrAsUniqueAdmin
                                    .get(k);
                                projectTableModel
                                    .removeData(getIndexOfProject(pProject));
                            }
                            removeUserAtIndex(i, selectedRowsTab);
                        }
                    } else {
                        removeUserAtIndex(i, selectedRowsTab);
                    }
                } else {
                    removeUserAtIndex(i, selectedRowsTab);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("deleteUserPerformed() - end");
        }
    }

    void removeUserAtIndex(int i, int[] selectedRowsTab) {
        if (logger.isDebugEnabled()) {
            logger.debug("removeUserAtIndex(int, int[]) - start");
        }

        try {
            String userLogin = (String) usersTableModel.getValueAt(
                                                                   selectedRowsTab[i], 0);
            User pUser = pAdminVTData.getUserFromModel(userLogin);
            pAdminVTData.delteUserInDBAndModel(pUser);
            if (usersTableModel.getRowCount() == 0) {
                deleteUserButton.setEnabled(false);
            }
            usersTableModel.removeData(selectedRowsTab[i]);
        } catch (Exception exception) {
            logger.error("removeUserAtIndex(int, int[])", exception);

            Tools.ihmExceptionView(exception);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("removeUserAtIndex(int, int[]) - end");
        }
    }

    void modifyUserPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("modifyUserPerformed() - start");
        }

        int selectedRow = userTable.getSelectedRow();
        if (selectedRow != -1) {
            User user = pAdminVTData.getUserFromModel((String) usersTableModel
                                                      .getValueAt(selectedRow, 0));
            AskNewUser askNewUser = new AskNewUser(user,
                                                   javax.swing.JOptionPane
                                                   .getFrameForComponent(Administration.this),
                                                   pAdminVTData);
            if (askNewUser.getUser() != null) {
                if (Api.isConnected()) {
                    try {
                        User usr = askNewUser.getUser();
                        user.updateInDBModel(usr.getLoginFromModel(), usr
                                             .getLastNameFromModel(), usr
                                             .getFirstNameFromModel(), usr
                                             .getDescriptionFromModel(), usr
                                             .getEmailFromModel(), usr
                                             .getPhoneNumberFromModel());

                        ArrayList data = new ArrayList();
                        data.add(usr.getLoginFromModel());
                        data.add(usr.getLastNameFromModel());
                        data.add(usr.getFirstNameFromModel());
                        data.add(usr.getEmailFromModel());
                        data.add(usr.getPhoneNumberFromModel());
                        usersTableModel.modifyData(data, selectedRow);

                    } catch (Exception e) {
                        logger.error("modifyUserPerformed()", e);

                        Tools.ihmExceptionView(e);
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("modifyUserPerformed() - end");
        }
    }

    void createUserPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("createUserPerformed() - start");
        }

        AskNewUser askNewUser = new AskNewUser(null, javax.swing.JOptionPane
                                               .getFrameForComponent(Administration.this), pAdminVTData);
        if (askNewUser.getUser() != null) {
            if (pAdminVTData.containsLoginInModel(askNewUser.getUser()
                                                  .getLoginFromModel())) {
                JOptionPane.showMessageDialog(Administration.this, Language
                                              .getInstance().getText("Ce_login_existe_deja_"),
                                              Language.getInstance().getText("Erreur_"),
                                              JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    User pUser = askNewUser.getUser();
                    pUser.addInDB(new String(askNewUser.getPassword()));

                    pAdminVTData.addUserInModel(pUser);
                    ArrayList data = new ArrayList();
                    data.add(pUser.getLoginFromModel());
                    data.add(pUser.getLastNameFromModel());
                    data.add(pUser.getFirstNameFromModel());
                    data.add(pUser.getEmailFromModel());
                    data.add(pUser.getPhoneNumberFromModel());
                    usersTableModel.addRow(data);

                } catch (Exception exception) {
                    logger.error("createUserPerformed()", exception);

                    Tools.ihmExceptionView(exception);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("createUserPerformed() - end");
        }
    }

    void createProjectPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("createProjectPerformed() - start");
        }

        AskNewProject askProject = new AskNewProject(pAdminVTData, null,
                                                     javax.swing.JOptionPane
                                                     .getFrameForComponent(Administration.this));
        ArrayList data = new ArrayList();
        Project pProject = askProject.getProject();
        if (pProject != null) {

            try {
                if (!askProject.isFromProjet()) {
                    pAdminVTData.addProjectInDBAndModel(pProject);
                } else {
                    String fromProjectName = askProject.getFromProjectName();
                    pAdminVTData.addProjectInDBAndModelByCopy(pProject,
                                                              pAdminVTData.getProjectFromModel(fromProjectName),
                                                              askProject.isFromProjectSuites(), askProject
                                                              .isFromProjectCampaigns(), askProject
                                                              .isFromProjectUsers(), askProject
                                                              .isFromProjetGroups());
                }
                data.add(Tools.createAppletImageIcon(PATH_TO_PROJECT_ICON, ""));
                data.add(pProject.getNameFromModel().trim());
                data.add(pProject.getAdministratorFromModel()
                         .getLoginFromModel());
                data.add(pProject.getCreationDateFromModel().toString());
                data.add(pProject.getDescriptionFromModel());
                projectTableModel.addRow(data);
                // ProjectTableCellRenderer.setColor(normalColor);
                projectTable.getColumnModel().getColumn(0).setMaxWidth(18);
                deleteProjectButton.setEnabled(true);
            } catch (Exception exception) {
                logger.error("createProjectPerformed()", exception);

                Tools.ihmExceptionView(exception);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("createProjectPerformed() - end");
        }
    }

    void cancelPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("cancelPerformed() - start");
        }

        oldPassword.setText(null);
        newPasswordFirst.setText(null);
        newPasswordSecond.setText(null);

        if (logger.isDebugEnabled()) {
            logger.debug("cancelPerformed() - end");
        }
    }

    void deleteProjectPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("deleteProjectPerformed() - start");
        }

        int selectedRow = projectTable.getSelectedRow();
        if (selectedRow != -1) {
            Object[] options = { Language.getInstance().getText("Oui"),
                                 Language.getInstance().getText("Non") };

            int choice = JOptionPane.showOptionDialog(Administration.this,
                                                      Language.getInstance().getText(
                                                                                     "Etes_vous_sur_de_vouloir_supprimer_le_projet__")
                                                      + projectTableModel.getData(selectedRow).get(1),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                                                      null, options, options[1]);
            if (choice == JOptionPane.YES_OPTION) {
                if (Api.isConnected()) {
                    try {
                        Project pProject = pAdminVTData
                            .getProjectFromModel((String) projectTableModel
                                                 .getValueAt(selectedRow, 1));
                        pAdminVTData.deleteProjectInDBAndModel(pProject);

                        // Call delete project on plugin
                        try {
                            Enumeration adminPlugins = associatedExtension
                                .elements();
                            while (adminPlugins.hasMoreElements()) {
                                Extension pluginExt = (Extension) adminPlugins
                                    .nextElement();
                                if (pluginExt.getExtendedPointId().equals(
                                                                          "Admin")) {
                                    Admin plugin = (Admin) jpf
                                        .activateExtension(pluginExt);
                                    plugin.onDeleteProject(pProject);
                                }
                            }
                        } catch (Exception exception) {
                            logger.error("deleteProjectPerformed()", exception);

                        }
                        projectTableModel.removeData(selectedRow);
                        projectTable.getColumnModel().getColumn(0).setMaxWidth(
                                                                               18);
                        if (projectTableModel.getRowCount() == 0) {
                            deleteProjectButton.setEnabled(false);
                        }
                    } catch (Exception exception) {
                        logger.error("deleteProjectPerformed()", exception);

                        Tools.ihmExceptionView(exception);
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("deleteProjectPerformed() - end");
        }
    }

    void modifyProjectPerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("modifyProjectPerformed() - start");
        }

        int selectedRow = projectTable.getSelectedRow();
        if (selectedRow != -1) {
            Project project = pAdminVTData
                .getProjectFromModel((String) projectTableModel.getValueAt(
                                                                           selectedRow, 1));
            AskNewProject askProject = new AskNewProject(pAdminVTData, project,
                                                         javax.swing.JOptionPane
                                                         .getFrameForComponent(Administration.this));
            if (askProject.getProject() != null) {
                try {
                    project.updateInDBAndModel(askProject.getProject()
                                               .getNameFromModel(), askProject.getProject()
                                               .getDescriptionFromModel());

                    ArrayList data = new ArrayList();
                    data.add(Tools.createAppletImageIcon(PATH_TO_PROJECT_ICON,
                                                         ""));
                    data.add(project.getNameFromModel().trim());
                    data.add(project.getAdministratorFromModel()
                             .getLoginFromModel());
                    data.add(project.getCreationDateFromModel().toString());
                    data.add(project.getDescriptionFromModel());
                    projectTableModel.modifyData(data, selectedRow);
                    projectTable.getColumnModel().getColumn(0).setMaxWidth(18);
                } catch (Exception exception) {
                    logger.error("modifyProjectPerformed()", exception);

                    Tools.ihmExceptionView(exception);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("modifyProjectPerformed() - end");
        }
    }

    void validatePerformed() {
        if (logger.isDebugEnabled()) {
            logger.debug("validatePerformed() - start");
        }

        if (Api.isConnected()) {
            User admin = pAdminVTData.getUserAdminVTData();
            boolean _valid = false;
            try {
                String pass = admin.getPasswordFromDB();
                _valid = org.objectweb.salome_tmf.api.MD5paswd.testPassword(
                                                                            new String(oldPassword.getPassword()).trim(), pass);
            } catch (Exception ex) {
                logger.error("validatePerformed()", ex);
            }
            if (_valid) {
                if (new String(newPasswordFirst.getPassword()).trim().equals(
                                                                             new String(newPasswordSecond.getPassword()).trim())) {
                    try {
                        admin.updatePasswordInDB(new String(newPasswordSecond
                                                            .getPassword()).trim());
                        oldPassword.setText("");
                        newPasswordFirst.setText("");
                        newPasswordSecond.setText("");
                        JOptionPane.showMessageDialog(Administration.this,
                                                      Language.getInstance().getText(
                                                                                     "Votre_mot_de_passe_a_ete_modifie"),
                                                      Language.getInstance().getText("Succes_"),
                                                      JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception exception) {
                        logger.error("validatePerformed()", exception);

                        Tools.ihmExceptionView(exception);
                    }

                } else {
                    newPasswordFirst.setText("");
                    newPasswordSecond.setText("");
                    JOptionPane
                        .showMessageDialog(
                                           Administration.this,
                                           Language
                                           .getInstance()
                                           .getText(
                                                    "Impossible_de_changer_le_mot_de_passe_Vous_n_avez_pas_entrer_deux_fois_le_meme_mot_de_passe"),
                                           Language.getInstance().getText("Erreur_"),
                                           JOptionPane.ERROR_MESSAGE);
                }
            } else {
                oldPassword.setText("");
                JOptionPane
                    .showMessageDialog(
                                       Administration.this,
                                       Language
                                       .getInstance()
                                       .getText(
                                                "Impossible_de_changer_le_mot_de_passe_L_ancien_mot_de_passe_est_incorrect"),
                                       Language.getInstance().getText("Erreur_"),
                                       JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane
                .showMessageDialog(
                                   Administration.this,
                                   Language
                                   .getInstance()
                                   .getText(
                                            "Impossible_de_changer_le_mot_de_passe_Vous_n_etes_pas_connecter_a_la_base"),
                                   Language.getInstance().getText("Erreur_"),
                                   JOptionPane.ERROR_MESSAGE);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("validatePerformed() - end");
        }
    }

    @Override
    public void addBugTrackers(Object bugTracker) {
        if (logger.isDebugEnabled()) {
            logger.debug("addBugTrackers(Object) - start");
        }

        bugTrackers.add(bugTracker);

        if (logger.isDebugEnabled()) {
            logger.debug("addBugTrackers(Object) - end");
        }
    }

    @Override
    public Vector getBugTracker() {
        if (logger.isDebugEnabled()) {
            logger.debug("getBugTracker() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getBugTracker() - end");
        }
        return bugTrackers;
    }

    @Override
    public void addStatistiquess(Object statistiques) {
        if (logger.isDebugEnabled()) {
            logger.debug("addStatistiquess(Object) - start");
        }

        statistiquess.add(statistiques);

        if (logger.isDebugEnabled()) {
            logger.debug("addStatistiquess(Object) - end");
        }
    }

    @Override
    public Vector getStatistiques() {
        if (logger.isDebugEnabled()) {
            logger.debug("getStatistiques() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getStatistiques() - end");
        }
        return statistiquess;
    }

    @Override
    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackE,
                               ExtensionPoint statistiquesE, ExtensionPoint reqMgr) {

    }

    /*
     * public void addJarToClassLoader(URL jar){ Class pclC = null; try { pclC =
     * (this.getClass().getClassLoader()).getClass();
     * pclC.getMethod("addLocalJar", new Class[] { URL.class })
     * .invoke(this.getClass().getClassLoader(), new Object[] { jar}); } catch
     * (Exception e){ //pPluginClassLoader.AddJar(UrlLib); } }
     */
    /***************** Windows Listener ***************/

    @Override
    public void windowClosing(WindowEvent e) {
        if (logger.isDebugEnabled()) {
            logger.debug("windowClosing(WindowEvent) - start");
        }

        if (!exit)
            quit(true, true);

        if (logger.isDebugEnabled()) {
            logger.debug("windowClosing(WindowEvent) - end");
        }
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void addReqManager(Object reqManager) {
        if (logger.isDebugEnabled()) {
            logger.debug("addReqManager(Object) - start");
        }

        reqManagers.add(reqManager);

        if (logger.isDebugEnabled()) {
            logger.debug("addReqManager(Object) - end");
        }
    }

    @Override
    public Vector getReqManagers() {
        if (logger.isDebugEnabled()) {
            logger.debug("getReqManagers() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getReqManagers() - end");
        }
        return reqManagers;
    }

    public JTabbedPane getTabs() {
        if (logger.isDebugEnabled()) {
            logger.debug("getTabs() - start");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("getTabs() - end");
        }
        return tabs;
    }

}
