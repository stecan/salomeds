/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import org.objectweb.salome_tmf.data.AdminProjectData;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.admin.models.UserListRenderer;
import org.objectweb.salome_tmf.ihm.languages.Language;


/**
 * Classe qui representant la fenetre de dialogue permettant d'ajouter un
 * nouvel utilisateur a un groupe
 * @author teaml039
 * @version : 0.1
 */
public class AddUser extends JDialog {
    
    /**
     * Liste des utilisateurs a ajouter
     */
    private ArrayList userListResult;
    
    /**
     * Modele de donnees pour la liste d'utilisateurs
     */
    private DefaultListModel listModel;
    
    /**
     * Liste des utilisateurs
     */
    private JList userList;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    AdminProjectData pAdminProjectData;
    /**
     * Constructeur de la fenetre
     */
    public AddUser(AdminProjectData adminProjectData) {
        
        // Pour maintenir le focus sur la fenetre
        super(new Frame(),true);
        pAdminProjectData = adminProjectData;
        listModel = new DefaultListModel();
        userList = new JList(listModel);
        JButton addButton = new JButton(Language.getInstance().getText("Ajouter"));
        addButton.setToolTipText(Language.getInstance().getText("Ajouter_un_utilisateur"));
        addButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    userListResult = new ArrayList();
                    for (int i = 0; i < userList.getSelectedValues().length; i++) {
                        userListResult.add(userList.getSelectedValues()[i]);
                    }
                    AddUser.this.dispose();
                }
            });
        
        JButton cancelButton = new JButton(Language.getInstance().getText("Annuler"));
        cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    AddUser.this.dispose();
                }
            });
        
        JPanel buttonGroup = new JPanel();
        buttonGroup.setLayout(new BoxLayout(buttonGroup, BoxLayout.X_AXIS));
        buttonGroup.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        buttonGroup.add(Box.createHorizontalGlue());
        buttonGroup.add(addButton);
        buttonGroup.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonGroup.add(cancelButton);
        
        ArrayList addUsersList;             // new local variable
        addUsersList = new ArrayList();     // initialize first element
        for (Iterator iter = pAdminProjectData.getNotInProjectUserSetFromModel().iterator(); iter.hasNext(); ) {
            addUsersList.add(iter.next());  // build list of users
        }
        
        Collections.sort(addUsersList, User.Comparators.LASTNAME);     // sort list of users
        
        for (int i = 0; i < addUsersList.size(); i++) {
            listModel.addElement(addUsersList.get(i));
        }
        userList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        userList.setSelectedIndex(0);
        userList.setCellRenderer(new UserListRenderer());
        userList.setVisibleRowCount(5);
        JScrollPane userListScrollPane = new JScrollPane(userList);
        
        userListScrollPane.setBorder(BorderFactory.createTitledBorder(Language.getInstance().getText("Utilisateurs_possibles")));
        
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.setLayout(new BoxLayout(contentPaneFrame, BoxLayout.Y_AXIS));
        contentPaneFrame.add(userListScrollPane, BorderLayout.CENTER);
        contentPaneFrame.add(buttonGroup);
        
        this.pack();
        this.setLocationRelativeTo(this.getParent()); 
        this.setVisible(true);
        this.setTitle(Language.getInstance().getText("Ajouter_un_nouvel_utilisateur"));
        this.setFocusableWindowState(true);
        
    } // Fin du constructeur AddUser/0
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Retourne la liste des utilisateurs a ajouter
     * @return la liste des utilisateurs a ajouter
     */
    public ArrayList getUserList() {
        return userListResult;
    } // Fin de la methode getUserList/0
    
} // Fin de la class AddUser
