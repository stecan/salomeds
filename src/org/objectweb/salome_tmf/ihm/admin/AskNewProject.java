/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AdminVTData;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.admin.models.UserListRenderer;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet d'afficher une fenetre demandant d'entrer les
 * informations pour un nouveau projet.
 * @author teaml039
 * @version 0.1
 */
public class AskNewProject extends JDialog {
    
    /**
     * le nouveau projet
     */
    private Project project;
    
    /**
     * Champ pour recuperer le nom du projet
     */
    private JTextField projetNameTextField;
    
    /**
     * Modele de donnees pour la liste des utilisateurs pouvant etre admin
     */
    private DefaultComboBoxModel comboModel;
    
    /**
     * La description du projet
     */
    private JTextPane descriptionArea;
    
    /**
     * La liste des utilisateurs pouvant etre admin
     */
    JComboBox adminNameComboBox;
    
    
    DefaultComboBoxModel comboModelForProjects;
    
    JCheckBox oldOrNewProject;
    
    JComboBox fromProjetNameComboBox;
    
    JCheckBox testListChoice;
    JCheckBox testCampagneChoice;
    JCheckBox usersChoice;
    JCheckBox groupsChoice;
    
    JScrollPane descriptionScrollPane;
    
    JLabel dataToBeCopied;
    
    JLabel fromProjectNameLabel;
    
    JLabel newProjectNameLabel;
    
    boolean fromProjet;
    
    String fromProjectName;
    
    boolean fromProjetGroups;
    boolean fromProjectUsers;
    boolean fromProjectCampaigns;
    boolean fromProjectSuites;
    
    boolean newProject = false;
    String oldProjectName = "";
    
    AdminVTData pAdminVTData;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Constructeur de la fenetre permettant d'entrer un nouveau projet
     *
     */
    public AskNewProject(AdminVTData adminVTData, Project oldProject, Frame owner) {
        super(owner,true);
        this.pAdminVTData = adminVTData;
        if (oldProject == null) {
            newProject = true;
        } else {
            oldProjectName = oldProject.getNameFromModel();
        }
        
        projetNameTextField = new JTextField(20);
        comboModel = new DefaultComboBoxModel();
        descriptionArea = new JTextPane();
        adminNameComboBox = new JComboBox(comboModel);
        // Partie superieure
        JLabel adminNameLabel = new JLabel(Language.getInstance().getText("Administrateur_du_projet"));
        
        adminNameComboBox.setRenderer(new UserListRenderer());
        for (int i = 0; i < pAdminVTData.getAllUsersCountFromModel(); i ++) {
            comboModel.addElement(pAdminVTData.getAllUsersFromModel().get(i));
        }
        
        JPanel adminPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        adminPanel.add(adminNameLabel);
        adminPanel.add(adminNameComboBox);
        adminPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        
        JLabel newProjectLabel = new JLabel(Language.getInstance().getText("Nouveau_Projet"));
        newProjectLabel.setFont(new Font(null,Font.BOLD,18));
        newProjectNameLabel = new JLabel(Language.getInstance().getText("Nom_du_nouveau_projet_"));
        
        
        JPanel newProjectNamePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        newProjectNamePanel.add(newProjectNameLabel);
        newProjectNamePanel.add(projetNameTextField);
        
        
        descriptionArea.setPreferredSize(new Dimension(100,150));
        descriptionScrollPane = new JScrollPane(descriptionArea, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        descriptionScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK),Language.getInstance().getText("Description")));
        
        
        JPanel newProjectPanel = new JPanel();
        newProjectPanel.setLayout(new BoxLayout(newProjectPanel, BoxLayout.Y_AXIS));
        newProjectPanel.add(newProjectLabel);
        newProjectPanel.add(newProjectNamePanel);
        newProjectPanel.add(descriptionScrollPane);
        newProjectPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        // Partie inferieure
        
        JLabel copyLabel = new JLabel(Language.getInstance().getText("Copier_a_partir_d_un_projet_existant"));
        copyLabel.setFont(new Font(null,Font.BOLD,18));
        fromProjectNameLabel = new JLabel(Language.getInstance().getText("A_partir_du_projet_"));
        fromProjectNameLabel.setEnabled(false);
        
        comboModelForProjects = new DefaultComboBoxModel();
        
        fromProjetNameComboBox = new JComboBox(comboModelForProjects);
        fromProjetNameComboBox.setEnabled(false);
        dataToBeCopied = new JLabel(Language.getInstance().getText("Donnees_a_copier_"));
        dataToBeCopied.setEnabled(false);
        
        
        JPanel fromProjectNamePanelData = new JPanel(new FlowLayout(FlowLayout.LEFT));
        fromProjectNamePanelData.add(fromProjectNameLabel);
        fromProjectNamePanelData.add(fromProjetNameComboBox);
        
        oldOrNewProject = new JCheckBox();
        oldOrNewProject.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (oldOrNewProject.isSelected()) {
                        fromProjetNameComboBox.setEnabled(true);
                        testListChoice.setEnabled(true);
                        testCampagneChoice.setEnabled(true);
                        usersChoice.setEnabled(true);
                        groupsChoice.setEnabled(true);
                        dataToBeCopied.setEnabled(true);
                        fromProjectNameLabel.setEnabled(true);
                    } else {
                        fromProjetNameComboBox.setEnabled(false);
                        testListChoice.setEnabled(false);
                        testCampagneChoice.setEnabled(false);
                        usersChoice.setEnabled(false);
                        groupsChoice.setEnabled(false);
                        dataToBeCopied.setEnabled(false);
                        fromProjectNameLabel.setEnabled(false);
                    }
                }
            });
        
        JPanel oldPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        oldPanel.add(copyLabel);
        oldPanel.add(oldOrNewProject);
        
        
        JPanel names = new JPanel(new GridLayout(0,1));
        names.setAlignmentX(Component.LEFT_ALIGNMENT);
        //names.add(copyLabel);
        names.add(oldPanel);
        names.add(fromProjectNamePanelData);
        
        ///////////////////////////////////////////
        
        testListChoice = new JCheckBox(Language.getInstance().getText("Suites_de_test"));
        testListChoice.setEnabled(false);
        testCampagneChoice = new JCheckBox(Language.getInstance().getText("Campagnes_de_test"));
        testCampagneChoice.setEnabled(false);
        usersChoice = new JCheckBox(Language.getInstance().getText("Utilisateurs"));
        usersChoice.setEnabled(false);
        groupsChoice = new JCheckBox(Language.getInstance().getText("Groupes"));
        groupsChoice.setEnabled(false);
        
        JPanel checkPanel = new JPanel(new GridLayout(0, 1));
        checkPanel.add(testListChoice);
        checkPanel.add(testCampagneChoice);
        checkPanel.add(usersChoice);
        checkPanel.add(groupsChoice);
        
        JPanel dataPanel = new JPanel(new GridLayout(1,0));
        dataPanel.add(dataToBeCopied);
        dataPanel.add(checkPanel);
        
        ///////////////////////////////////////////
        JButton validation = new JButton(Language.getInstance().getText("Valider"));
        validation.setToolTipText(Language.getInstance().getText("Valider"));
        validation.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (projetNameTextField.getText().trim() != null && !projetNameTextField.getText().trim().equals("")) {
                        if ((!pAdminVTData.containsProjectInModel(projetNameTextField.getText().trim())) ||
                            (oldProjectName.equals(projetNameTextField.getText().trim()))) {
                            if (adminNameComboBox.getSelectedItem() != null) {
                                if (oldOrNewProject.isSelected()) {
                                    fromProjet = true;
                                    if (testListChoice.isSelected()) fromProjectSuites = true;
                                    if (testCampagneChoice.isSelected()) fromProjectCampaigns = true;
                                    if (usersChoice.isSelected()) fromProjectUsers = true;
                                    if (groupsChoice.isSelected()) fromProjetGroups = true;
                                    fromProjectName = fromProjetNameComboBox.getSelectedItem().toString();
                                }
                                Util.log("[AskNewProject->validation] name = " + projetNameTextField.getText().trim() + "admin id is " + ((User)adminNameComboBox.getSelectedItem()).getIdBdd());
                                project.updateInModel(projetNameTextField.getText().trim(), descriptionArea.getText());
                                project.setAdministratorInModel((User)adminNameComboBox.getSelectedItem());
                                AskNewProject.this.dispose();
                            } else {
                                JOptionPane.showMessageDialog(AskNewProject.this,
                                                              Language.getInstance().getText("Il_faut_obligatoirement_nommer_un_administrateur_au_projet_"),
                                                              Language.getInstance().getText("Attention_"),
                                                              JOptionPane.WARNING_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(AskNewProject.this,
                                                          Language.getInstance().getText("Ce_nom_de_projet_existe_deja_"),
                                                          Language.getInstance().getText("Attention_"),
                                                          JOptionPane.WARNING_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(AskNewProject.this,
                                                      Language.getInstance().getText("Il_faut_obligatoirement_donner_un_nom_au_projet_"),
                                                      Language.getInstance().getText("Attention_"),
                                                      JOptionPane.WARNING_MESSAGE);
                    }
                }
            });
        
        
        JButton cancel = new JButton(Language.getInstance().getText("Annuler"));
        cancel.setToolTipText(Language.getInstance().getText("Annuler"));
        cancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    project = null;
                    AskNewProject.this.dispose();
                }
            });
        
        JPanel buttonsSet = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttonsSet.add(validation);
        buttonsSet.add(cancel);
        
        // Assemblage des composants
        
        JPanel copyPanel = new JPanel(new BorderLayout());
        copyPanel.add(BorderLayout.NORTH, names);
        copyPanel.add(BorderLayout.CENTER, dataPanel);
        //copyPanel.add(BorderLayout.SOUTH, buttonsSet);
        copyPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        
        JPanel upPanel = new JPanel();
        upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.Y_AXIS));
        if (newProject)
            upPanel.add(adminPanel);
        
        upPanel.add(newProjectPanel);
        
        JPanel allPanel = new JPanel();
        allPanel.setLayout(new BoxLayout(allPanel, BoxLayout.Y_AXIS));
        allPanel.add(upPanel);
        if (newProject)
            allPanel.add(copyPanel);
        
        allPanel.add(buttonsSet);
        
        Container contentPane = this.getContentPane();
        contentPane.add(allPanel, BorderLayout.CENTER);
        
        initData(oldProject);
        
        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    project = null;
                
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });
        
        
        if (oldProject != null) {
            this.setTitle(Language.getInstance().getText("Modifier_un_projet"));
        } else {
            this.setTitle(Language.getInstance().getText("Creer_un_nouveau_projet"));
        }
        this.pack();
        //this.setLocation(300,100);
        this.setLocationRelativeTo(this.getParent()); 
        this.setVisible(true);
        this.setResizable(false);
    } // Fin du constructeur NewProject/0
    
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
    
    /**
     * Retourne le projet cree
     * @return le projet cree
     */
    public Project getProject() {
        return project;
    } // Fin de la methode getProject/0
    
    
    private void initData(Project oldProject) {
        if (oldProject != null) {
            project = oldProject;
            projetNameTextField.setText(oldProject.getNameFromModel());
            descriptionArea.setText(oldProject.getDescriptionFromModel());
            User u = oldProject.getAdministratorFromModel();
            comboModel.setSelectedItem(u);
            adminNameComboBox.setSelectedItem(u);
        } else {
            project = new Project("","");
        }
        for (int j = 0; j < pAdminVTData.getProjectListFromModel().size(); j++) {
            comboModelForProjects.addElement(pAdminVTData.getProjectListFromModel().get(j));
        }
        //boolean fromProjet = false;
        
        //String fromProjectName = "";
        
        //boolean fromProjetGroups = false;
        //boolean fromProjectUsers = false;
        //boolean fromProjectCampaigns = false;
        //boolean fromProjectSuites = false;
        
    }
    /**
     * @return
     */
    public boolean isFromProjectCampaigns() {
        return fromProjectCampaigns;
    }
    
    /**
     * @return
     */
    public String getFromProjectName() {
        return fromProjectName;
    }
    
    /**
     * @return
     */
    public boolean isFromProjectSuites() {
        return fromProjectSuites;
    }
    
    /**
     * @return
     */
    public boolean isFromProjectUsers() {
        return fromProjectUsers;
    }
    
    /**
     * @return
     */
    public boolean isFromProjet() {
        return fromProjet;
    }
    
    /**
     * @return
     */
    public boolean isFromProjetGroups() {
        return fromProjetGroups;
    }
    
} //Fin de la classe NewProject
