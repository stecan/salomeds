/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.EventObject;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.event.CellEditorListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;

import org.objectweb.salome_tmf.data.Group;
import org.objectweb.salome_tmf.ihm.languages.Language;

/**
 * Classe qui permet de cr?er une fen?tre peremttant de g?rer les droits des
 * groupes.

 */
public class GroupAccess extends JDialog {

    /**
     * Le noeud s?lectionn?
     */
    DefaultMutableTreeNode selectedNode;

    /**
     * La check box racine de l'arbre des tests
     */
    JCheckBox testCheckroot;

    /**
     * la racine de l'arbre pour les tests
     */
    DefaultMutableTreeNode testRootNode;

    /**
     * Mod?le de l'arbre pour les tests
     */
    DefaultTreeModel testModel;

    /**
     * Arbre pour les tests
     */
    JTree testTree;

    // Campagne
    /**
     * La checkBox racine de l'arbre des campagnes
     */
    JCheckBox campagneCheckroot;

    /**
     * La racine de l'arbre des campagnes
     */
    DefaultMutableTreeNode campagneRootNode;

    /**
     * Mod?le de donn?es pour l'arbre des campagnes
     */
    DefaultTreeModel campagneModel;

    /**
     * L'arbre des campagnes
     */
    JTree campagneTree;

    //Admin
    /**
     * La checkBox racine de l'arbre de l'admin
     */
    JCheckBox adminCheckroot;

    /**
     * La racine de l'arbre d'administration
     */
    DefaultMutableTreeNode adminRootNode;

    /**
     * Le mod?le de donn?es de l'arbre d'administration
     */
    DefaultTreeModel adminModel;

    /**
     * Arbre d'administration
     */
    JTree adminTree;

    /**
     * Tableau des permissions pour les suites de tests
     */
    boolean[] permissionsOfTest;

    /**
     * Tableau pour les permissions pour les campagnes
     */
    boolean[] permissionsOfCampaign;

    /**
     * Groupe courant
     */
    Group observedGroup;
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/

    /**
     * Le constructeur de la vue sur les droits des groupes
     */
    public GroupAccess(boolean modify, Group group, Frame owner, boolean canBeModified) {

        super(owner,true);
        testCheckroot = new JCheckBox(Language.getInstance().getText("Toutes_les_permissions"));
        testRootNode = new DefaultMutableTreeNode(testCheckroot);
        testModel = new DefaultTreeModel(testRootNode);
        testTree = new JTree(testModel);
        campagneCheckroot = new JCheckBox(Language.getInstance().getText("Toutes_les_permissions"));
        campagneRootNode = new DefaultMutableTreeNode(campagneCheckroot);
        campagneModel = new DefaultTreeModel(campagneRootNode);
        campagneTree = new JTree(campagneModel);
        //              adminCheckroot = new JCheckBox("Toutes les permissions");
        //              adminRootNode = new DefaultMutableTreeNode(adminCheckroot);
        //              adminModel = new DefaultTreeModel(adminRootNode);
        //              adminTree = new JTree(adminModel);
        JTabbedPane tabs = new JTabbedPane();
        JPanel tests = new JPanel();
        tests.setLayout(new BoxLayout(tests, BoxLayout.Y_AXIS));
        JPanel campagne = new JPanel();
        campagne.setLayout(new BoxLayout(campagne, BoxLayout.Y_AXIS));
        JPanel admin = new JPanel();
        admin.setLayout(new BoxLayout(admin, BoxLayout.Y_AXIS));
        permissionsOfTest = new boolean[3];
        permissionsOfCampaign = new boolean[4];


        // Onglets
        tabs.addTab(Language.getInstance().getText("Suites_de_test"),   tests);
        tabs.addTab(Language.getInstance().getText("Campagne_de_test"), campagne);
        //      tabs.addTab("Administration",   admin);
        tabs.setPreferredSize(new Dimension(500,350));

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        //tests.add(principalListPanel1,BorderLayout.WEST);
        if (canBeModified) {
            JButton validateButton = new JButton(Language.getInstance().getText("Valider"));
            validateButton.setToolTipText(Language.getInstance().getText("Valider"));
            validateButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        for(int i = 0; i  < testRootNode.getChildCount(); i++) {
                            JCheckBox box = (JCheckBox)((DefaultMutableTreeNode)testRootNode.getChildAt(i)).getUserObject();
                            observedGroup.changeTestPermissionInModel(i,box.isSelected());
                        }
                        for (int i = 0; i < campagneRootNode.getChildCount(); i++) {
                            JCheckBox box = (JCheckBox)((DefaultMutableTreeNode)campagneRootNode.getChildAt(i)).getUserObject();
                            observedGroup.changeCampaignPermissionInModel(i,box.isSelected());
                        }
                        GroupAccess.this.dispose();
                    }
                });
            buttonPanel.add(validateButton);
        }

        JButton cancelButton = new JButton();
        if (canBeModified) {
            cancelButton.setText(Language.getInstance().getText("Annuler"));
            cancelButton.setToolTipText(Language.getInstance().getText("Annuler"));
        } else {
            cancelButton.setText(Language.getInstance().getText("Fermer"));
            cancelButton.setToolTipText(Language.getInstance().getText("Fermer_la_fenetre"));
        }

        cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    observedGroup = null;
                    GroupAccess.this.dispose();
                }
            });

        buttonPanel.add(cancelButton);


        // Arbre pour les permissions des tests

        CheckBoxRenderer testRenderer = new CheckBoxRenderer();
        CheckBoxCellEditor testEditor = new CheckBoxCellEditor();
        createTestsTree(testRootNode);

        testTree.setCellRenderer(testRenderer);
        testTree.setCellEditor(testEditor);
        if (modify) {
            testTree.setEditable(true);
        } else {
            testTree.setEditable(false);
        }

        testTree.setRootVisible(true);
        testCheckroot.addMouseListener(new checkBoxMouseListener(testTree));
        JScrollPane testTreeScrollPane = new JScrollPane(testTree);

        tests.add(testTreeScrollPane);


        // Arbre pour les permissions des campagnes

        CheckBoxRenderer campagneRenderer = new CheckBoxRenderer();
        CheckBoxCellEditor campagneEditor = new CheckBoxCellEditor();
        createCampagneTree(campagneRootNode);

        campagneTree.setCellRenderer(campagneRenderer);
        campagneTree.setCellEditor(campagneEditor);
        if (modify) {
            campagneTree.setEditable(true);
        } else {
            campagneTree.setEditable(false);
        }

        campagneTree.setRootVisible(true);
        campagneCheckroot.addMouseListener(new checkBoxMouseListener(campagneTree));
        JScrollPane campagneTreeScrollPane = new JScrollPane(campagneTree);

        campagne.add(campagneTreeScrollPane);



        this.addWindowListener(new WindowListener() {
                @Override
                public void windowClosing(WindowEvent e) {
                    observedGroup = null;
                }
                @Override
                public void windowDeiconified(WindowEvent e) {
                }
                @Override
                public void windowOpened(WindowEvent e) {
                }
                @Override
                public void windowActivated(WindowEvent e) {
                }
                @Override
                public void windowDeactivated(WindowEvent e) {
                }
                @Override
                public void windowClosed(WindowEvent e) {
                }
                @Override
                public void windowIconified(WindowEvent e) {
                }
            });

        // Initialisation des donn?es
        initData(group);

        // Affichage
        Container contentPaneFrame = this.getContentPane();
        contentPaneFrame.setLayout(new BoxLayout(contentPaneFrame, BoxLayout.Y_AXIS));
        contentPaneFrame.add(tabs);
        contentPaneFrame.add(buttonPanel);
        this.setTitle(Language.getInstance().getText("Permissions"));
        //this.setLocation(400,300);
        this.setLocationRelativeTo(this.getParent());
        this.pack();
        this.setVisible(true);

    } // Fin du constructeur GroupAccess/0

    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/

    /**
     * Cr?ation de l'arbre des tests
     * @param root la racine de l'arbre
     */
    private void createTestsTree(DefaultMutableTreeNode root) {
        String[] mainNodes = {Language.getInstance().getText("Ajouter"), Language.getInstance().getText("Modifier"),
                              Language.getInstance().getText("Supprimer")};


        //              String[] listTestsModification = {"Nom de la suite", "Attachements", "Description", "Famille de tests", "Ordre de la suite"};
        //              String[] testsModification = {"Nom du test"};
        for (int i = 0; i < mainNodes.length; i ++) {
            JCheckBox box = new JCheckBox(mainNodes[i]);
            box.addMouseListener(new checkBoxMouseListener(testTree));
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(box, true);
            root.add(node);
            //
        }
        testModel.setRoot(root);
    } // Fin de la m?thode createTestsTree/1

    /**
     * Cr?ation de l'arbre des campagnes
     * @param root la racine de l'arbre
     */
    private void createCampagneTree(DefaultMutableTreeNode root) {
        String[] mainNodes = {Language.getInstance().getText("Ajouter"), Language.getInstance().getText("Modifier"), Language.getInstance().getText("Supprimer"), Language.getInstance().getText("Executer")};


        //              String[] listTestsModification = {"Nom de la suite", "Attachements", "Description", "Famille de tests", "Ordre de la suite"};
        //              String[] testsModification = {"Nom du test"};
        for (int i = 0; i < mainNodes.length; i ++) {
            JCheckBox box = new JCheckBox(mainNodes[i]);
            box.addMouseListener(new checkBoxMouseListener(campagneTree));
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(box, true);
            root.add(node);
            //
        }
        campagneModel.setRoot(root);
    } // Fin de la m?thode createCampagneTree/1

    /**
     * Cr?ation de l'arbre de l'administration
     * @param root la racine de l'arbre
     */
    /*private void createAdminTree(DefaultMutableTreeNode root) {
      String[] mainNodes = {"Ajouter un utilisateur", "Suprimer un utilisateur",
      "Ajouter un groupe", "Supprimer un groupe"};

      for (int i = 0; i < mainNodes.length; i ++) {
      JCheckBox box = new JCheckBox(mainNodes[i]);
      box.addMouseListener(new checkBoxMouseListener(adminTree));

      DefaultMutableTreeNode node = new DefaultMutableTreeNode(new JCheckBox(mainNodes[i]), true);
      root.add(node);
      }
      adminModel.setRoot(root);
      } // Fin de la m?thode createAdminTree/1
    */

    /**
     * Classe interne d?finissant le renderer de l'arbre (pour prendre en compte
     * les checkBox)
     * @author teaml039
     * @version : 0.1
     */
    class CheckBoxRenderer implements TreeCellRenderer{
        /**
         * Surcharge de la m?thode qui calcule le renderer des noeuds de
         * l'arbre
         */
        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object obj,
                                                      boolean selected, boolean expanded, boolean leaf,
                                                      int row, boolean hasFocus){

            DefaultMutableTreeNode node = (DefaultMutableTreeNode)obj;

            JCheckBox box = (JCheckBox)node.getUserObject();
            box.setBackground(new Color(255,255,255));
            return box;
        } // Fin de la m?thode getTreeCellRendererComponent/7
    } // Fin de la classe CheckBoxRenderer

    /**
     * Classe interne qui d?finit un listener de la souris qui permet de
     * g?rer la s?lection des checkbox
     * @author teaml039
     * @version : 0.1
     */
    class checkBoxMouseListener extends MouseAdapter {

        /**
         * l'arbre de checkbox
         */
        JTree _tree;

        /**
         * Constructeur du listener
         * @param tree l'arbre sur lequel on place le listener
         */
        public checkBoxMouseListener(JTree tree) {
            _tree = tree;
        } // Fin du constructeur checkBoxMouseListener/1

        /**
         *  Appel?e lorsque l'on appuie sur un bouton de la souris
         * @param e un ?v?nement relatif ? la souris
         */
        @Override
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        } // Fin de la m?thode mousePressed/1

        /**
         * Appel?e lorsque l'on relache un bouton de la souris
         * @param e un ?v?nement relatif ? la souris
         */
        @Override
        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        } // Fin de la m?thode mouseReleased/1

        /**
         * Traite les diff?rents clics de la souris
         * @param e un ?v?nement relatif ? la souris
         */
        private void maybeShowPopup(MouseEvent e) {
            DefaultMutableTreeNode node_actif = (DefaultMutableTreeNode)_tree.getLastSelectedPathComponent();
            if (e.getButton() == MouseEvent.BUTTON1) {
                JCheckBox box = (JCheckBox)node_actif.getUserObject();
                if (node_actif.getChildCount() > 0) {
                    changeCheckBoxValuesDown(node_actif);
                    ((DefaultTreeModel)_tree.getModel()).nodeChanged(node_actif);
                }
                DefaultMutableTreeNode parent = (DefaultMutableTreeNode)node_actif.getParent();

                if (((JCheckBox)node_actif.getUserObject()).getText().equals(Language.getInstance().getText("Supprimer"))) {
                    boolean value = ((JCheckBox)node_actif.getUserObject()).isSelected();
                    if (value) {
                        DefaultMutableTreeNode brother = node_actif.getPreviousNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                        brother = brother.getPreviousNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                    }
                } else if (((JCheckBox)node_actif.getUserObject()).getText().equals(Language.getInstance().getText("Ajouter"))) {
                    boolean value = ((JCheckBox)node_actif.getUserObject()).isSelected();
                    if (value) {
                        DefaultMutableTreeNode brother = node_actif.getNextNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                    } else {
                        DefaultMutableTreeNode brother = node_actif.getNextNode().getNextNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                    }
                } else if (((JCheckBox)node_actif.getUserObject()).getText().equals(Language.getInstance().getText("Modifier"))) {
                    boolean value = ((JCheckBox)node_actif.getUserObject()).isSelected();
                    if (!value) {
                        DefaultMutableTreeNode brother = node_actif.getPreviousNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                        brother = node_actif.getNextNode();
                        ((JCheckBox)brother.getUserObject()).setSelected(value);
                    }
                }

                if (!box.isSelected()) {
                    if (parent != null) {
                        //((JCheckBox)parent.getUserObject()).setSelected(false);
                        changeValueUp(parent);
                        ((DefaultTreeModel)_tree.getModel()).nodeChanged((DefaultMutableTreeNode)_tree.getModel().getRoot());
                    }
                } else {
                    DefaultMutableTreeNode upNode = changeCheckBoxValuesUp(parent);
                    if (upNode != null) {
                        ((DefaultTreeModel)_tree.getModel()).nodeChanged(upNode);
                    } else {
                        ((DefaultTreeModel)_tree.getModel()).nodeChanged((DefaultMutableTreeNode)_tree.getModel().getRoot());
                    }
                }
            }
        } // Fin de la m?thode maybeShowPopup/1
        /**
         * M?thode qui permet de changer r?cursivement les valeurs des checkbox
         * d'un noeud vers ses fils.
         * @param node un noeud dont on change la valeur
         */
        public void changeCheckBoxValuesDown(DefaultMutableTreeNode node) {
            for(int i=0; i < node.getChildCount(); i ++) {
                boolean value = ((JCheckBox)node.getUserObject()).isSelected();
                ((JCheckBox)((DefaultMutableTreeNode)node.getChildAt(i)).getUserObject()).setSelected(value);
                changeCheckBoxValuesDown((DefaultMutableTreeNode)node.getChildAt(i));
            }
        } // Fin de la m?thode changeCheckBoxValuesDown/1

        /**
         * Changement des valeurs des checkbox vers les parents lorsque l'on
         * passe la valeur ? vrai
         * @param parent le noeud
         * @return le dernier noeud dans la remont?e
         */
        public DefaultMutableTreeNode changeCheckBoxValuesUp(DefaultMutableTreeNode parent) {
            if (parent != null) {
                int nb = 0;
                for (int i =0; i < parent.getChildCount(); i ++) {
                    if (((JCheckBox)((DefaultMutableTreeNode)parent.getChildAt(i)).getUserObject()).isSelected()) {
                        nb++;
                    }
                }
                if (nb == parent.getChildCount()) {
                    ((JCheckBox)parent.getUserObject()).setSelected(true);
                }
                return changeCheckBoxValuesUp((DefaultMutableTreeNode)parent.getParent());
            } else {
                return null;
            }
        } // Fin de la m?thode changeCheckBoxValuesUp/1


        /**
         * Changement des valeurs des checkbox vers les parents lorsque l'on
         * passe la valeur ? faux
         * @param parent un noeud
         */
        public void changeValueUp(DefaultMutableTreeNode parent) {
            ((JCheckBox)parent.getUserObject()).setSelected(false);
            if (parent.getParent() != null) {
                changeValueUp((DefaultMutableTreeNode)parent.getParent());
            }
        } // Fin de la m?thode changeValueUp/1

    }// Fin de la classe checkBoxMouseListener


    /**
     * Classe interne d?finissant un nouvel ?diteur pour l'arbre des checkbox
     * @author teaml039
     * @version : 0.1
     */
    class CheckBoxCellEditor implements TreeCellEditor{

        @Override
        public void addCellEditorListener(CellEditorListener l){ }
        @Override
        public void cancelCellEditing() { }

        /**
         * Retourne la valeur contenue dans l'?diteur
         * @return la valeur contenue dans l'?diteur
         */
        @Override
        public Object getCellEditorValue(){
            return this;
        } // Fin de la m?thode getCellEditorValue/0

        /**
         * Demande ? l'?diteur s'il peut commencer ? ?diter en utilisant un
         * ?v?nement. On rend vrai, si on d?tecte un clic de souris, faux
         * sinon.
         * @param evt un ?v?nement
         * @return vrai si on d?tecte un clic de souris, faux sinon.
         */
        @Override
        public boolean isCellEditable(EventObject evt){
            if(evt instanceof MouseEvent){
                MouseEvent mevt = (MouseEvent) evt;
                if (mevt.getClickCount() == 1){
                    return true;
                }
            }
            return false;
        } // Fin de la m?thode isCellEditable/1

        @Override
        public void removeCellEditorListener(CellEditorListener l){}

        /**
         * Retourne vrai : la cellule doit toujours ?tre s?lectionn?e.
         * @param anEvent un ?v?nement
         * @return toujours vrai
         */
        @Override
        public boolean shouldSelectCell(EventObject anEvent){
            return true;
        } // Fin de la m?thode shouldSelectCell/1

        /**
         * Retourne faux : on ne peut pas emp?cher l'?dition
         * @return toujours faux
         */
        @Override
        public boolean stopCellEditing(){
            return false;
        } // Fin de la m?thode stopCellEditing/0

        /**
         * Donne une nouvelle valeur ? l'?diteur. Retourne le composant
         * (checkbox) qui va ?tre ajout? dans l'arbre.
         * @param       tree l'arbre des checkbox;
         * @param       obj objet de la cellule devant ?tre ?diter
         * @param       isSelected      vrai si la cellule est s?lectionn?e
         * @param       expanded        vrai si le noeud est d?ploy?
         * @param       leaf            vrai si le noeud est une feuille
         * @param       row             l'indice de la ligne en cours d'?dition
         */
        @Override
        public Component getTreeCellEditorComponent(JTree tree, Object obj, boolean isSelected, boolean expanded, boolean leaf, int row){
            DefaultMutableTreeNode dmtn = (DefaultMutableTreeNode)obj;
            JCheckBox box=(JCheckBox)dmtn.getUserObject();
            box.setEnabled(true);
            return box;
        } // Fin de la m?thode getTreeCellEditorComponent/7
    } // Fin de la classe CheckBoxCellEditor

    /**
     * @return
     */
    public boolean[] getPermissionsOfCampaign() {
        return permissionsOfCampaign;
    }

    /**
     * @return
     */
    public boolean[] getPermissionsOfTest() {
        return permissionsOfTest;
    }

    private void initData(Group group) {
        observedGroup = group;
        boolean oneFalse = false;
        for (int i = 0;  i < group.getTestsPermissionsFromModel().length; i++) {
            ((JCheckBox)((DefaultMutableTreeNode)testRootNode.getChildAt(i)).getUserObject()).setSelected(group.getTestsPermissionsFromModel()[i]);
            if (!group.getTestsPermissionsFromModel()[i]) {
                oneFalse = true;
            }
        }
        if (!oneFalse) {
            ((JCheckBox)(testRootNode).getUserObject()).setSelected(true);
        }
        for (int i = 0;  i < group.getCampaignPermissionsFromModel().length; i++) {
            ((JCheckBox)((DefaultMutableTreeNode)campagneRootNode.getChildAt(i)).getUserObject()).setSelected(group.getCampaignPermissionsFromModel()[i]);
            if (!group.getCampaignPermissionsFromModel()[i]) {
                oneFalse = true;
            }
        }
        if (!oneFalse) {
            ((JCheckBox)(campagneRootNode).getUserObject()).setSelected(true);
        }

    }

    /**
     * @return
     */
    public Group getObservedGroup() {
        return observedGroup;
    }

} // Fin de la classe GroupAccess




