/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin.models;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.UIManager;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.AdminProjectData;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.Tools;

/**
 * Classe qui d?finit le renderer pour les listes d'utilisateurs
 */
public class UserListRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object>, ApiConstants , IHMConstants{
    
    /**
     * Le s?parateur de fichier
     */
    /**
     * M?thode qui red?finit le renderer des ?l?ments de la liste
     * @param list la liste concern?e
     * @param value l'objet dans la liste
     * @param index indice dans la liste
     * @param isSelected si l'objet est s?lectionn?
     * @param cellHasFocus si le focus est sur la cellule
     */
    @Override
    public Component getListCellRendererComponent(JList list, Object value,     int index,
                                                  boolean isSelected,
                                                  boolean cellHasFocus) {
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        }
        else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        Icon icon;
        if (value instanceof User) {
            icon = Tools.createAppletImageIcon(PATH_TO_ADMIN_ICON,"");
            setIcon(icon);
            setText(((User)value).getLoginFromModel() + " / " + ((User)value).getLastNameFromModel() + " " + ((User)value).getFirstNameFromModel());
        } else if (value instanceof Icon) {
            setIcon((Icon)value);
            setText("");
        } else {
            setIcon(null);
            setText((value == null) ? "" : value.toString());
        }
        
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setBorder((cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);
        
        return this;
        
    } // Fin de la m?thode getListCellRendererComponent/5
} // Fin de la classe UserListRenderer
