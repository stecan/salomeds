/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin.models;

import java.awt.Color;
import java.awt.Component;
import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.*;

/**
 * Classe qui d?finit le renderer pour la table du projet
 * @author teaml039
 * @version : 0.1
 */
public class ProjectTableCellRenderer extends JLabel implements IHMConstants, TableCellRenderer, Serializable {
    
    /**
     * Liste des couleurs pour chaque ligne
     */
    static ArrayList rowColors;
    
    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
    
    /**
     * Construit un nouveau renderer
     */
    public ProjectTableCellRenderer() {
        super();
        setOpaque(true);
        rowColors = new ArrayList();
    } // Fin du constructeur ProjectTableCellRenderer/0
    
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    
    /**
     * Retourne le composant utilis? pour dessiner la cellule.
     * @param table la table sur laquelle on utilise le renderer
     * @param value valeur de l'objet dans la cellule
     * @param isSelected vrai si la cellule est s?lectionn?e
     * @param hasFocus vrai si la cellule a le focus
     * @param row le num?ro de ligne
     * @param column le num?ro de colonne
     */
    @Override
    public Component getTableCellRendererComponent(
                                                   JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus,
                                                   int row, int column) {
        
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
            
        } else if (rowColors.size() > 0) {
            
            setBackground((Color)rowColors.get(row));
            if (rowColors.get(row).equals(Color.BLUE)) {
                setForeground(Color.WHITE);
            } else {
                setForeground(Color.BLACK);
            }
        }
        
        setFont(table.getFont());
        if (value == null) {
            setText("");
        } else if (value instanceof Icon) {
            setIcon((Icon)value);
            setText("");
        } else {
            setText(value.toString());
        }
        setValue(value);
        if (hasFocus) {
            setBorder( UIManager.getBorder("Table.focusCellHighlightBorder") );
            if ( table.isCellEditable( row, column ) ) {
                super.setForeground( UIManager.getColor("Table.focusCellForeground") );
                super.setBackground( UIManager.getColor("Table.focusCellBackground") );
            }
        } else {
            setBorder( new EmptyBorder(1, 2, 1, 2) );
        }
        if (table.getValueAt(row, column) instanceof String) {
            setToolTipText(Tools.createHtmlString((String)table.getValueAt(row, column), 60));
        }
        
        return this;
    } // Fin de la m?thode getTableCellRendererComponent/6
    
    /**
     * Modifie le contenu de la cellule
     * @param value valeur de la cellule
     */
    protected void setValue(Object value)       {
        setText( ( value == null ) ? "" : value.toString() );
    } // Fin de la m?thode setValue/1
    
    /**
     * Ajoute une couleur dans la liste des couleurs
     * @param color une couleur
     */
    public static void setColor(Color color) {
        rowColors.add(color);
    } // Fin de la m?thode setColor/1
    
    /**
     * Modifie la couleur de la ligne pass?e en param?tre
     * @param row un num?ro de ligne
     * @param color une couleur
     */
    public static void setColor(int row, Color color) {
        rowColors.remove(row);
        rowColors.add(row, color);
    } // Fin de la m?thode setColor/2
    
} // Fin de la classe ProjectTableRenderer
