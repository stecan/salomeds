/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE, Fayçal SOUGRATI, Vincent PAUTRET
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.admin.models;



import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.UIManager;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.data.Group;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.tools.Tools;



/**

 * Classe qui d?finit le renderer pour les listes de groupes

 * @author teaml039

 * @version : 0.1

 */

public class GroupListRenderer extends DefaultListCellRenderer implements ApiConstants, IHMConstants {

    

    /**

     * S?parateur de fichier

     */

    //private final String fileSeparator = "/"  ;

    

    /**

     * M?thode qui red?finit le renderer des ?l?ments de la liste

     * @param list la liste concern?e

     * @param value l'objet dans la liste

     * @param index indice de l'objet trait?

     * @param iseSelected s'il est s?lectionn? ou non

     * @param cellHasFocus s'il a le focus

     */

    @Override
    public Component getListCellRendererComponent(JList list, Object value,     int index,

                                                  boolean isSelected,

                                                  boolean cellHasFocus) {

        if (isSelected) {

            setBackground(list.getSelectionBackground());

            setForeground(list.getSelectionForeground());

        }

        else {

            setBackground(list.getBackground());

            setForeground(list.getForeground());

        }

        Icon icon;

        if (value instanceof Group && ((Group)value).getNameFromModel().equals(ADMINNAME)) {

            icon = Tools.createAppletImageIcon(PATH_TO_ADMIN_GRP_ICON,"");

            setIcon(icon);

            setText(((Group)value).getNameFromModel());

        } else if (value instanceof Group && (((Group)value).getNameFromModel().equals(GUESTNAME) || ((Group)value).getNameFromModel().equals(DEVNAME))) {

            icon = Tools.createAppletImageIcon(PATH_TO_DEFAULT_GRP_ICON,"");

            setIcon(icon);

            setText(((Group)value).getNameFromModel());

        } else if (value instanceof Group) {

            icon = Tools.createAppletImageIcon(PATH_TO_OTHER_GRP_ICON,"");

            setIcon(icon);

            setText(((Group)value).getNameFromModel());

        }else {

            setIcon(null);

            setText((value == null) ? "" : value.toString());

        }

        

        setEnabled(list.isEnabled());

        setFont(list.getFont());

        setBorder((cellHasFocus) ? UIManager.getBorder("List.focusCellHighlightBorder") : noFocusBorder);

        

        return this;

        

    } // Fin de la m?thode getListCellRendererComponent/5

    

} // Fin de la classe GroupListRenderer
