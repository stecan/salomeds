package org.objectweb.salome_tmf.ihm;

import org.objectweb.salome_tmf.ihm.languages.Language;

public interface IHMConstants {
    public String TEST_ROOT_NAME = Language.getInstance().getText("Suites_de_tests");
    
    public String CAMPAIGN_ROOT_NAME = Language.getInstance().getText("Campagnes_de_tests");
    
    /**
     * Icons paths 
     */
    
    public String PATH_TO_GOON_ICON = "/org/objectweb/salome_tmf/ihm/images/goOn.gif";
    
    public String PATH_TO_PARAM_ICON = "/org/objectweb/salome_tmf/ihm/images/param.gif";
    
    public String PATH_TO_ARROW_BACK_ICON = "/org/objectweb/salome_tmf/ihm/images/greenArrowBack.gif";
    
    public String PATH_TO_ARROW_ICON = "/org/objectweb/salome_tmf/ihm/images/greenArrow.gif";
    
    public String PATH_TO_STOP_ICON = "/org/objectweb/salome_tmf/ihm/images/stop.gif";
    
    public String PATH_TO_MOVE_ICON = "/org/objectweb/salome_tmf/ihm/images/move.gif";
    
    public String PATH_TO_ARROW_UP_ICON = "/org/objectweb/salome_tmf/ihm/images/arrowUp.gif";
    
    public String PATH_TO_ARROW_DOWN_ICON = "/org/objectweb/salome_tmf/ihm/images/arrowDown.gif";
    
    public String PATH_TO_ENGRENAGE_ICON = "/org/objectweb/salome_tmf/ihm/images/engrenages.gif";
    
    public String PATH_TO_CAUTION_ICON = "/org/objectweb/salome_tmf/ihm/images/caution.gif";
    
    public String PATH_TO_SALOME_INTRO_ICON = "/org/objectweb/salome_tmf/ihm/images/salome_intro.jpg";
    
    public String PATH_TO_PROJECT_ICON = "/org/objectweb/salome_tmf/ihm/images/greencube.gif";
    
    public String PATH_TO_FREEZED_PROJECT_ICON = "/org/objectweb/salome_tmf/ihm/images/bluecube.gif";
    
    public String PATH_TO_ADD_PROJECT_ICON = "/org/objectweb/salome_tmf/ihm/images/addition.gif";
    
    public String PATH_TO_VIEW_ATTACH_ICON = "/org/objectweb/salome_tmf/ihm/images/eye.gif";
    
    public String PATH_TO_FILE_ATTACH_ICON = "/org/objectweb/salome_tmf/ihm/images/file.gif";
    
    public String PATH_TO_URL_ATTACH_ICON = "/org/objectweb/salome_tmf/ihm/images/globe.gif";
    
    public String PATH_TO_BDD_ATTACH_ICON = "/org/objectweb/salome_tmf/ihm/images/inbase.gif";
    
    public String PATH_TO_FAIL2_ICON = "/org/objectweb/salome_tmf/ihm/images/fail.gif";
    
    public String PATH_TO_DELETE_ICON = "/org/objectweb/salome_tmf/ihm/images/deleteUser.gif";
    
    public String PATH_TO_USER_ICON = "/org/objectweb/salome_tmf/ihm/images/user.gif";
    
    public String PATH_TO_ADMIN_ICON = "/org/objectweb/salome_tmf/ihm/images/admin.gif";
   
    public String PATH_TO_ADMIN_GRP_ICON = "/org/objectweb/salome_tmf/ihm/images/adminGroup.gif";
    
    public String PATH_TO_DEFAULT_GRP_ICON = "/org/objectweb/salome_tmf/ihm/images/defaultGroup.gif";
    
    public String PATH_TO_OTHER_GRP_ICON = "/org/objectweb/salome_tmf/ihm/images/otherGroup.gif";
    
    /**
     * Chemin pour acceder a l'image pour l'icone inconclusif
     */
    public String PATH_TO_UNKNOW_ICON = "/org/objectweb/salome_tmf/ihm/images/inconclusif2.png";
    
    /**
     * Chemin pour acceder a l'image pour l'icone succ?s
     */
    public String PATH_TO_SUCCESS_ICON = "/org/objectweb/salome_tmf/ihm/images/greenCheck.png";
    
    /**
     * Chemin pour acceder a l'image pour l'icone ?chec
     */
    public String PATH_TO_FAIL_ICON = "/org/objectweb/salome_tmf/ihm/images/failed.png";

    // Dentsplysirona icons
    public String PATH_TO_NOTAPPLICABLE_ICON = "/org/objectweb/salome_tmf/ihm/images/highImportanceCheck.png";
    public String PATH_TO_BLOCKED_ICON = "/org/objectweb/salome_tmf/ihm/images/errorCheck.png";
    public String PATH_TO_NONE_ICON = "/org/objectweb/salome_tmf/ihm/images/questionCheck.png";
    
    public String PATH_TO_BLUE_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleBleue.gif";
    public String PATH_TO_PINK_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleRose.gif";
    public String PATH_TO_RED_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleRouge.gif";
    public String PATH_TO_GREEN_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleVerte.gif";
    public String PATH_TO_YELLOW_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleJaune.gif";
    public String PATH_TO_GRAY_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleGrise.gif";
    public String PATH_TO_BROWN_ICON = "/org/objectweb/salome_tmf/ihm/images/bouleMarron.gif";
    public String PATH_TO_PURPLE_ICON = "/org/objectweb/salome_tmf/ihm/images/boulePourpre.gif";
    
    public String PATH_TO_ICAL_ICON = "/org/objectweb/salome_tmf/ihm/images/ICAL.jpg";
    public String PATH_TO_QSSCORE_ICON = "/org/objectweb/salome_tmf/ihm/images/qsscore.gif";
    
    public String PATH_TO_SCREENSHOT_ICON = "/org/objectweb/salome_tmf/ihm/images/photo.png";
}
