/*
 * Created on 10 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.ihm.languages;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.objectweb.salome_tmf.api.Api;

public class Language {
    /** Static variable */
    private static Language language = null;

    /** Local variable */
    Locale currentLocale = null;

    /** For the resource file */
    ResourceBundle i18n = null;

    /*
     * ResourceBundle i18nErr = null; ResourceBundle i18nLog = null;
     */
    /** Get the instance of the language */
    public static Language getInstance() {
        if (language == null) {
            language = new Language();
        }
        // language.setLocale(new Locale(Api.getUsedLocale()));
        return language;
    }

    /** Constructor of the language */
    private Language() {
        // Locale locale ;
        if (currentLocale == null) {
            currentLocale = new Locale(Api.getUsedLocale());
        }
        setLocale(currentLocale);
    }

    /** Set the local values */
    public void setLocale(Locale locale) {
        currentLocale = locale;

        // String name = "classpath:lang/i18n";
        // name += "_" + locale;
        // // Load resource as inputStream.
        // java.io.InputStream is = Language.class.getResourceAsStream(name
        // + ".properties");
        // Construct PropertyResourceBundle
        // PropertyResourceBundle propertyBundle = new
        // PropertyResourceBundle(is);

        try {
            // System.out.println("load external properties");
            i18n = ResourceBundle.getBundle("lang/i18n", currentLocale);
            // Construct PropertyResourceBundle
            // i18n = new PropertyResourceBundle(is);
            // Here, if you give a ResourceBundle Object, you cannot gain access
            // to the handleGetObject method
            // because it is protected in ResourceBundle while it is public in
            // PropertyResourceBundle.
            // You must have access to this method.
        } catch (Exception e1) {
            // System.out.println("failed to load external properties");
            i18n = ResourceBundle.getBundle(
                                            "org/objectweb/salome_tmf/ihm/languages/i18n",
                                            currentLocale);

        }
    }

    /*
     * \brief Return the text corresponding to the key \param
     */
    public String getText(String key) {
        return i18n.getString(key);
    }

    /*
     * \brief Return the log text corresponding to the key \param key : a log
     * key belong i18nLog
     */
    /*
     * public String getTextLog(String key){ return i18nLog.getString(key); }
     */

    /*
     * \brief Return the error text corresponding to the key \param key : an
     * error key belong i18nErr
     */
    /*
     * public String getTextErr(String key){ return i18nErr.getString(key); }
     */
}
