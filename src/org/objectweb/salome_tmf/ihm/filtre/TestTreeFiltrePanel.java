package org.objectweb.salome_tmf.ihm.filtre;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;

public class TestTreeFiltrePanel extends JPanel implements ActionListener {

    JRadioButton bFamille;
    JRadioButton bSuite;
    JRadioButton bTest;
    JTextField filtreStr;

    int filtre = -1;
    StringSimpleDataFiltre m_dataFilter;
    TestTreeModel m_FilterTreeModel;

    public TestTreeFiltrePanel(TestTreeModel pFilterTreeModel) {
        super();
        m_FilterTreeModel = pFilterTreeModel;
        m_dataFilter = new StringSimpleDataFiltre();
        m_FilterTreeModel.setFilter(m_dataFilter);
        initComponent();
    }

    public ISimpleDataFilter getFilter() {
        return m_dataFilter;
    }

    void initComponent() {
        setLayout(new GridLayout(1, 4));
        setBorder(BorderFactory.createTitledBorder(BorderFactory
                                                   .createLineBorder(Color.BLACK), Language.getInstance().getText(
                                                                                                                  "Filtre")));
        filtreStr = new JTextField();
        bFamille = new JRadioButton(Language.getInstance().getText("Famille"),
                                    true);
        bSuite = new JRadioButton(Language.getInstance().getText("Suite"), true);
        bTest = new JRadioButton(Language.getInstance().getText("Test"), true);

        bFamille.setSelected(false);
        bSuite.setSelected(false);
        bTest.setSelected(false);

        bFamille.addActionListener(this);
        bSuite.addActionListener(this);
        bTest.addActionListener(this);

        filtreStr.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent keyEvent) {

                }

                @Override
                public void keyPressed(KeyEvent keyEvent) {
                }

                @Override
                public void keyReleased(KeyEvent keyEvent) {
                    reloadFilter();
                }

            });

        add(filtreStr);
        add(bFamille);
        add(bSuite);
        add(bTest);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource().equals(bFamille)) {
            bFamillePerformed();
        } else if (evt.getSource().equals(bSuite)) {
            bSuitePerformed();
        } else if (evt.getSource().equals(bTest)) {
            bTestPerformed();
        }
    }

    void bFamillePerformed() {
        filtre = -1;
        if (bFamille.isSelected()) {
            bSuite.setSelected(false);
            bTest.setSelected(false);
            filtre = DataConstants.FAMILY;
        }
        applyFilter();
    }

    void bSuitePerformed() {
        filtre = -1;
        if (bSuite.isSelected()) {
            bFamille.setSelected(false);
            bTest.setSelected(false);
            filtre = DataConstants.TESTLIST;
        }

        applyFilter();
    }

    void bTestPerformed() {
        filtre = -1;
        if (bTest.isSelected()) {
            bFamille.setSelected(false);
            bSuite.setSelected(false);
            filtre = DataConstants.TEST;
        }
        applyFilter();
    }

    void reloadFilter() {
        if (filtre != -1) {
            /*
             * try { Thread.sleep(100); } catch (Exception e){ Util.err(e); }
             */
            applyFilter();
        }
    }

    void applyFilter() {
        // Util.debug("Filtre : " + filtreStr.getText() + ", type = " + filtre);
        if (m_dataFilter != null) {
            m_dataFilter.setFilterType(filtre);
            m_dataFilter.setFilterStr(filtreStr.getText());
        }
        m_FilterTreeModel.setFiltered(true);
    }

    public int getFiltreType() {
        return filtre;
    }

    public void reInit(boolean filter) {
        bFamille.setSelected(false);
        bSuite.setSelected(false);
        bTest.setSelected(false);
        if (filter) {
            m_dataFilter.reInit();
        }
    }
}
