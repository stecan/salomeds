package org.objectweb.salome_tmf.ihm.filtre;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.models.TestTreeModel;

/**
 * @covers SFG_ForgeORTF_TST_FIL_000010 - \ufffd2.4.9 Jira : FORTF-8
 */
public class CampagneTreeFiltrePanel extends JPanel implements ActionListener {
    // 20100105 - D\ufffdbut modification Forge ORTF v1.0.0
    JRadioButton bCampagne;
    JRadioButton bToutes;
    JRadioButton bTerminees;
    JRadioButton bIncompletes;
    JRadioButton bAnoOuvertes;
    JRadioButton bNonExecutees;
    // 20100105 - Fin modification Forge ORTF v1.0.0
    JTextField filtreStr;

    int filtre = -1;
    StringSimpleDataFiltre m_dataFilter;
    TestTreeModel m_FilterTreeModel;

    public CampagneTreeFiltrePanel(TestTreeModel pFilterTreeModel) {
        super();
        m_FilterTreeModel = pFilterTreeModel;
        m_dataFilter = new StringSimpleDataFiltre();
        m_FilterTreeModel.setFilter(m_dataFilter);
        initComponent();
    }

    void initComponent() {
        // setLayout(new GridLayout(3, 3));
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        setBorder(BorderFactory.createTitledBorder(BorderFactory
                                                   .createLineBorder(Color.BLACK), Language.getInstance().getText(
                                                                                                                  "Filtre")));
        filtreStr = new JTextField();
        // 20100105 - D\ufffdbut modification Forge ORTF v1.0.0
        bCampagne = new JRadioButton(
                                     Language.getInstance().getText("Campagne"), true);
        bCampagne.setSelected(false);
        bCampagne.addActionListener(this);

        bToutes = new JRadioButton(Language.getInstance().getText("Toutes"),
                                   true);
        bToutes.setSelected(false);
        bToutes.addActionListener(this);

        bTerminees = new JRadioButton(Language.getInstance().getText(
                                                                     "Terminees"), true);
        bTerminees.setSelected(false);
        bTerminees.addActionListener(this);

        bIncompletes = new JRadioButton(Language.getInstance().getText(
                                                                       "Incompletes"), true);
        bIncompletes.setSelected(false);
        bIncompletes.addActionListener(this);

        bAnoOuvertes = new JRadioButton(Language.getInstance().getText(
                                                                       "Avec_anomalies_ouvertes"), true);
        bAnoOuvertes.setSelected(false);
        bAnoOuvertes.addActionListener(this);

        bNonExecutees = new JRadioButton(Language.getInstance().getText(
                                                                        "Non_executees"), true);
        bNonExecutees.setSelected(false);
        bNonExecutees.addActionListener(this);

        filtreStr.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent keyEvent) {

                }

                @Override
                public void keyPressed(KeyEvent keyEvent) {
                }

                @Override
                public void keyReleased(KeyEvent keyEvent) {
                    reloadFilter();
                }

            });

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(filtreStr, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        gbc.gridy = 0;
        add(bCampagne, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(bToutes, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 1;
        gbc.gridx = 1;
        gbc.gridy = 1;
        add(bTerminees, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        gbc.gridy = 1;
        add(bIncompletes, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 2;
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(bAnoOuvertes, gbc);

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weightx = 1.0;
        gbc.weighty = 1.0;
        gbc.gridwidth = 1;
        gbc.gridx = 2;
        gbc.gridy = 2;
        add(bNonExecutees, gbc);
        // 20100105 - Fin modification Forge ORTF v1.0.0

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        // 20100105 - D\ufffdbut modification Forge ORTF v1.0.0
        if (evt.getSource().equals(bToutes)) {
            bTerminees.setSelected(false);
            bIncompletes.setSelected(false);
            bAnoOuvertes.setSelected(false);
            bNonExecutees.setSelected(false);
            bCampagne.setSelected(false);
            bCampagneAllPerformed();
        }
        if (evt.getSource().equals(bTerminees)) {
            bToutes.setSelected(false);
            bIncompletes.setSelected(false);
            bAnoOuvertes.setSelected(false);
            bNonExecutees.setSelected(false);
            bCampagne.setSelected(false);
            bCampagneEnded();
        }
        if (evt.getSource().equals(bIncompletes)) {
            bToutes.setSelected(false);
            bTerminees.setSelected(false);
            bAnoOuvertes.setSelected(false);
            bNonExecutees.setSelected(false);
            bCampagne.setSelected(false);
            bCampagneIncomplete();
        }
        if (evt.getSource().equals(bAnoOuvertes)) {
            bToutes.setSelected(false);
            bTerminees.setSelected(false);
            bIncompletes.setSelected(false);
            bNonExecutees.setSelected(false);
            bCampagne.setSelected(false);
            bCampagneWithAnomaly();
        }
        if (evt.getSource().equals(bNonExecutees)) {
            bToutes.setSelected(false);
            bTerminees.setSelected(false);
            bIncompletes.setSelected(false);
            bAnoOuvertes.setSelected(false);
            bCampagne.setSelected(false);
            bCampagneNotExecuted();
        }
        if (evt.getSource().equals(bCampagne)) {
            bToutes.setSelected(false);
            bTerminees.setSelected(false);
            bIncompletes.setSelected(false);
            bAnoOuvertes.setSelected(false);
            bNonExecutees.setSelected(false);
            bCampagnePerformed();
        }
        // 20100105 - Fin modification Forge ORTF v1.0.0
    }

    // 20100105 - D\ufffdbut modification Forge ORTF v1.0.0
    void bCampagnePerformed() {
        filtre = -1;
        if (bCampagne.isSelected()) {
            filtre = DataConstants.CAMPAIGN;
        }
        applyFilter();
    }

    void bCampagneAllPerformed() {
        filtre = -1;
        if (bToutes.isSelected()) {
            filtre = DataConstants.CAMPAIGN_ALL;
        }
        applyFilter();
    }

    void bCampagneEnded() {
        filtre = -1;
        if (bTerminees.isSelected()) {
            filtre = DataConstants.CAMPAIGN_ENDED;
        }
        applyFilter();
    }

    void bCampagneIncomplete() {
        filtre = -1;
        if (bIncompletes.isSelected()) {
            filtre = DataConstants.CAMPAIGN_INCOMPLETE;
        }
        applyFilter();
    }

    void bCampagneWithAnomaly() {
        filtre = -1;
        if (bAnoOuvertes.isSelected()) {
            filtre = DataConstants.CAMPAIGN_WITH_ANOMALY;
        }
        applyFilter();
    }

    void bCampagneNotExecuted() {
        filtre = -1;
        if (bNonExecutees.isSelected()) {
            filtre = DataConstants.CAMPAIGN_NOT_EXECUTED;
        }
        applyFilter();
    }

    // 20100105 - Fin modification Forge ORTF v1.0.0

    void reloadFilter() {
        if (filtre != -1) {
            /*
             * try { Thread.sleep(100); } catch (Exception e){ Util.err(e); }
             */
            applyFilter();
        }
    }

    void applyFilter() {
        // Util.debug("Filtre : " + filtreStr.getText() + ", type = " + filtre);
        if (m_dataFilter != null) {
            m_dataFilter.setFilterType(filtre);
            m_dataFilter.setFilterStr(filtreStr.getText());
        }
        m_FilterTreeModel.setFiltered(true);
    }

    public void reInit(boolean filter) {
        // 20100105 - D\ufffdbut modification Forge ORTF v1.0.0
        bToutes.setSelected(false);
        bTerminees.setSelected(false);
        bIncompletes.setSelected(false);
        bAnoOuvertes.setSelected(false);
        bNonExecutees.setSelected(false);
        bCampagne.setSelected(false);
        // 20100105 - Fin modification Forge ORTF v1.0.0
        filtre = -1;
        if (filter) {
            m_dataFilter.reInit();
        }
        m_FilterTreeModel.setFiltered(false);
    }

    public boolean isActived() {
        return (filtre != -1);
    }
}
