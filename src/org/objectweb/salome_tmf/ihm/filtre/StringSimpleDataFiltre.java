package org.objectweb.salome_tmf.ihm.filtre;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.tree.DefaultMutableTreeNode;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualExecutionResult;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.SimpleData;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.plugins.core.BugTracker;

public class StringSimpleDataFiltre implements ISimpleDataFilter {
    boolean actived = true;

    String strFiltre = "";
    int filtreType = -1;

    @Override
    public boolean pass(Object obj) {
        if (!actived)
            return true;
        if (obj instanceof DefaultMutableTreeNode) {
            Object data = ((DefaultMutableTreeNode) obj).getUserObject();
            if (data instanceof SimpleData) {
                return isFiltred((SimpleData) data);
            }
        }
        return false;
    }

    @Override
    public void setActived(boolean actived) {
        this.actived = actived;
    }

    @Override
    public boolean isActived() {
        return actived;
    }

    @Override
    public void setFilterType(int filtre) {
        filtreType = filtre;
    }

    @Override
    public int getFilterType() {
        return filtreType;
    }

    @Override
    public void setFilterStr(String str) {
        strFiltre = str;
    }

    @Override
    public String getFilterStr() {
        return strFiltre;
    }

    /**
     * @covers SFG_ForgeORTF_TST_FIL_000010 - �2.4.9 Jira : FORTF-8
     */
    @Override
    public boolean isFiltred(SimpleData data) {
        /* Filtre sur les campagnes */
        // 20100105 - D�but modification Forge ORTF v1.0.0
        /* R�cup�rer la campagne */
        if (filtreType == DataConstants.CAMPAIGN) {
            if (data instanceof Campaign) {
                String name = data.getNameFromModel();
                Pattern pattern = Pattern.compile(strFiltre, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(name);
                if (matcher.find()) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        }
        if (filtreType == DataConstants.CAMPAIGN_ALL) {
            return true;
        }
        if (filtreType == DataConstants.CAMPAIGN_ENDED) {
            if (data instanceof Campaign) {
                List executionList = ((Campaign) data)
                    .getExecutionListFromModel();
                if (executionList.size() != 0) {
                    for (int i = 0; i < executionList.size(); i++) {
                        Execution lastExecution = (Execution) executionList
                            .get(i);
                        List executionResultList = lastExecution
                            .getExecutionResultListFromModel();
                        if (executionResultList.size() != 0) {
                            ExecutionResult executionResult = (ExecutionResult) executionResultList
                                .get(executionResultList.size() - 1);
                            if (!executionResult.getExecutionStatusFromModel()
                                .equals("TERMINEE")) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
            return true;
        }
        if (filtreType == DataConstants.CAMPAIGN_INCOMPLETE) {
            if (data instanceof Campaign) {
                List executionList = ((Campaign) data)
                    .getExecutionListFromModel();
                if (executionList.size() != 0) {
                    for (int i = 0; i < executionList.size(); i++) {
                        Execution lastExecution = (Execution) executionList
                            .get(i);
                        List executionResultList = lastExecution
                            .getExecutionResultListFromModel();
                        if (executionResultList.size() != 0) {
                            ExecutionResult executionResult = (ExecutionResult) executionResultList
                                .get(executionResultList.size() - 1);
                            if (!executionResult.getExecutionStatusFromModel()
                                .equals("TERMINEE")) {
                                HashMap testResultMap = executionResult
                                    .getTestsResultMapFromModel();
                                Set tests = testResultMap.keySet();
                                Iterator testIt = tests.iterator();
                                while (testIt.hasNext()) {
                                    ManualTest test = (ManualTest) testIt
                                        .next();
                                    ManualExecutionResult testResult = (ManualExecutionResult) testResultMap
                                        .get(test);
                                    ArrayList actionList = test
                                        .getActionListFromModel(false);
                                    boolean noActionMade = true;
                                    boolean noFail = true;
                                    for (int j = 0; j < actionList.size(); j++) 
                                    {
                                        if (testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.FAIL)) 
                                        {
                                            noActionMade = false;
                                            noFail = false;
                                            break;
                                        }
                                        if (testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.SUCCESS)
                                            || testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.UNKNOWN)
                                            || testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.NOTAPPLICABLE)
                                            || testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.BLOCKED) 
                                            || testResult.getActionStatusInModel(((Action) actionList.get(j)))
                                            .equals(ApiConstants.NONE)) 
                                        {
                                            noActionMade = false;
                                        }
                                    }
                                    if (noActionMade || noFail) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    return false;
                }
            }
            return false;
        }
        if (filtreType == DataConstants.CAMPAIGN_WITH_ANOMALY) {
            if (data instanceof Campaign) {
                List executionList = ((Campaign) data)
                    .getExecutionListFromModel();
                boolean openAno = false;
                if (executionList.size() != 0) {
                    for (int i = 0; i < executionList.size(); i++) {
                        Execution lastExecution = (Execution) executionList
                            .get(i);
                        List executionResultList = lastExecution
                            .getExecutionResultListFromModel();
                        if (executionResultList.size() != 0) {
                            for (int n = 0; n < executionResultList.size(); n++) {

                                ExecutionResult executionResult = (ExecutionResult) executionResultList
                                    .get(n);
                                Attachment pAttachment = null;
                                try {
                                    Vector vectorAttach = executionResult
                                        .getAllResExecTestAttachFromDB();
                                    if (vectorAttach.size() != 0) {
                                        for (int j = 0; j < vectorAttach.size(); j++) {
                                            Vector attach = (Vector) vectorAttach
                                                .get(j);
                                            for (int k = 0; k < attach.size(); k++) {
                                                pAttachment = (Attachment) attach
                                                    .elementAt(1);
                                                BugTracker pBugTracker = SalomeTMFContext
                                                    .getInstance()
                                                    .getBugTrackerFromAttachment(
                                                                                 pAttachment);
                                                if (pBugTracker != null) {
                                                    if (pBugTracker
                                                        .isOpenDefect(pAttachment)) {
                                                        openAno = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (openAno) {
                                                break;
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            return false;
                        }

                        if (openAno) {
                            break;
                        }
                    }
                } else {
                    return false;
                }
                if (!openAno) {
                    return false;
                }
            }
            return true;
        }
        if (filtreType == DataConstants.CAMPAIGN_NOT_EXECUTED) {
            List executionList = ((Campaign) data).getExecutionListFromModel();
            if (executionList.size() != 0) {
                for (int i = 0; i < executionList.size(); i++) {
                    Execution lastExecution = (Execution) executionList.get(i);
                    List executionResultList = lastExecution
                        .getExecutionResultListFromModel();
                    if (executionResultList.size() != 0) {
                        ExecutionResult executionResult = (ExecutionResult) executionResultList
                            .get(executionResultList.size() - 1);
                        HashMap testResultMap = executionResult
                            .getTestsResultMapFromModel();
                        Set tests = testResultMap.keySet();
                        Iterator testIt = tests.iterator();
                        while (testIt.hasNext()) {
                            ManualTest test = (ManualTest) testIt.next();
                            ManualExecutionResult testResult = (ManualExecutionResult) testResultMap
                                .get(test);
                            ArrayList actionList = test
                                .getActionListFromModel(false);
                            for (int j = 0; j < actionList.size(); j++) {
                                if (testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.FAIL)
                                    || testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.SUCCESS)
                                    || testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.UNKNOWN)
                                    || testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.NOTAPPLICABLE)
                                    || testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.BLOCKED)
                                    || testResult.getActionStatusInModel(((Action) actionList.get(j))).equals(ApiConstants.NONE)) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            } else {
                return false;
            }
            return true;
        }
        // 20100105 - Fin modification Forge ORTF v1.0.0

        /* Filtre sur le plan de test */
        if (data instanceof Family) {
            String name = data.getNameFromModel();
            Pattern pattern = Pattern.compile(strFiltre, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(name);
            if (filtreType == DataConstants.FAMILY) {
                if (matcher.find()) {
                    return true;
                } else {
                    return false;
                }
            } else if (filtreType == DataConstants.TESTLIST
                       || filtreType == DataConstants.TEST) {
                ArrayList listOfSuite = ((Family) data).getSuiteListFromModel();
                int size = listOfSuite.size();
                boolean trouve = false;
                int cpt = 0;
                while (cpt < size && !trouve) {
                    TestList pTestList = (TestList) listOfSuite.get(cpt);
                    if (isFiltred(pTestList)) {
                        trouve = true; // Au moins une suite est filtree
                    }
                    cpt++;
                }
                return trouve;
            }
            return true; // FILTRE SUITE,TEST
        }
        if (data instanceof TestList) {
            String name = null;
            if (filtreType == DataConstants.TESTLIST) {
                name = data.getNameFromModel();
                if (name != null) {
                    Pattern pattern = Pattern.compile(strFiltre, Pattern.CASE_INSENSITIVE);
                    Matcher matcher = pattern.matcher(name);
                    if (matcher.find()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if (filtreType == DataConstants.FAMILY) {
                name = ((TestList) data).getFamilyFromModel()
                    .getNameFromModel();
                Pattern pattern = Pattern.compile(strFiltre, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(name);
                
                if (name != null) {
                    if (matcher.find()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else if (filtreType == DataConstants.TEST) {
                ArrayList listOfTest = ((TestList) data).getTestListFromModel();
                int size = listOfTest.size();
                boolean trouve = false;
                int cpt = 0;
                while (cpt < size && !trouve) {
                    Test pTest = (Test) listOfTest.get(cpt);
                    if (isFiltred(pTest)) {
                        trouve = true; // Au moins un Test est filtree
                    }
                    cpt++;
                }
                return trouve;
            }

            return true; // FILTRE TEST

        }
        if (data instanceof Test) {
            String name = null;
            if (filtreType == DataConstants.TEST) {
                name = data.getNameFromModel();
            } else if (filtreType == DataConstants.TESTLIST) {
                name = ((Test) data).getTestListFromModel().getNameFromModel();
            } else if (filtreType == DataConstants.TESTLIST) {
                name = ((Test) data).getTestListFromModel()
                    .getFamilyFromModel().getNameFromModel();
            }
            try {
                Pattern pattern = Pattern.compile(strFiltre, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(name);
                if (name != null) {
                    if (matcher.find()) {
                        return true;
                    } else {
                        return false;
                    }
                }
                return true; // ??
            } catch (Exception ex) {
                Util.log("Ignore!");
            }
            
        }
        return true; // campagne ....
    }

    @Override
    public void reInit() {
        filtreType = -1;
        actived = false;
    }

}
