package org.objectweb.salome_tmf.ihm.filtre;

import org.objectweb.salome_tmf.data.SimpleData;


public interface ISimpleDataFilter {
    public boolean pass(Object obj);
    public void setActived(boolean actived);
    public boolean isActived();
    public void setFilterType(int filtre);
    public int getFilterType();
    public void setFilterStr(String str);
    public String getFilterStr();
    public boolean isFiltred (SimpleData data) ;
    public void reInit();

}
