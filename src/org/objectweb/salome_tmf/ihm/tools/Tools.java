/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.ihm.tools;

import java.awt.Frame;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.sql.LockException;
import org.objectweb.salome_tmf.data.AutomaticTest;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataConstants;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.ManualTest;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.IHMConstants;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeLocksPanel;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

/**
 * Classe regroupant des m?thodes utilitaires
 */
public class Tools implements ApiConstants, IHMConstants {

    /**
     * Methode permettant de recuperer des icones qui peuvent ?tre utilis?es
     * dans l'interface.
     * @param path le chemin complet permettant d'acc?der au fichier image.
     * @param description la chaine de caracteres pouvant etre associee a l'icone
     * @return un objet <code>ImageIcon</code>
     */
    public static ImageIcon createAppletImageIcon(String path, String description) {
        int MAX_IMAGE_SIZE = 10000;
        int count = 0;

        BufferedInputStream imgStream = new BufferedInputStream(Tools.class.getResourceAsStream(path));
        if (imgStream != null) {
            byte buf[] = new byte[MAX_IMAGE_SIZE];
            try {
                count = imgStream.read(buf);
            } catch (IOException ieo) {
                Util.err(Language.getInstance().getText("Impossible_de_lire_le_flux_depuis_le_fichier__") + path);
            }

            try {
                imgStream.close();
            } catch (IOException ieo) {
                Util.err(Language.getInstance().getText("Impossible_de_fermer_le_fichier_") + path);
            }

            if (count <= 0) {
                Util.err(Language.getInstance().getText("Le_fichier_est_vide__") + path);
                return null;
            }

            return new ImageIcon(Toolkit.getDefaultToolkit().createImage(buf), description);
        } else {
            Util.err(Language.getInstance().getText("Impossible_de_trouver_le_fichier__") + path);
            return null;
        }
    } // Fin de la m?thode createAppletImageIcon/2


    /**
     * M?thode permettant de r?cup?rer des icones qui peuvent ?tre utilis?es
     * dans l'interface.
     * @param path le chemin complet permettant d'acc?der au fichier image.
     * @param description la cha?ne de caract?res pouvant ?tre associ?e ? l'icone
     * @return un objet <code>ImageIcon</code>
     */
    public static Image createImage(String path, String description) {
        int IMAGE_BUF = 2048;
        int count = 0;
        int pos = 0;
        Image pImage = null;
        ByteArrayOutputStream pBufer = new ByteArrayOutputStream(IMAGE_BUF);
        byte buf[] = new byte[IMAGE_BUF];

        BufferedInputStream imgStream = new BufferedInputStream(Tools.class.getResourceAsStream(path));
        if (imgStream != null) {
            try {
                while((count=imgStream.read(buf,0,IMAGE_BUF))!=-1){
                    pBufer.write(buf,0,count);
                    pos+=count;
                }
                if (pos != 0) {
                    imgStream.close();
                    pImage = Toolkit.getDefaultToolkit().createImage(pBufer.toByteArray());
                    pBufer.close();
                }
            } catch (IOException ieo) {
                Util.err(Language.getInstance().getText("Impossible_de_lire_le_flux_depuis_le_fichier__") + path);
            }
        } else {
            Util.err(Language.getInstance().getText("Impossible_de_trouver_le_fichier__") + path);
        }
        return pImage;
    } // Fin de la m?thode createAppletImageIcon/2
    /**
     *
     * @param name
     * @param familyList
     * @return
     */
    public static Family familyInList(String name, ArrayList familyList) {
        for(int i = 0; i  < familyList.size(); i++) {
            if (((Family)familyList.get(i)).getNameFromModel().equals(name)) {
                return (Family)familyList.get(i);
            }
        }
        return null;
    } // Fin de la m?thode familyInList/2


    /**
     *
     * @param name
     * @param testListList
     * @return
     */
    public static TestList testListInList(String name, ArrayList testListList) {
        for(int i = 0; i  < testListList.size(); i++) {
            if (((TestList)testListList.get(i)).getNameFromModel().equals(name)) {
                return (TestList)testListList.get(i);
            }
        }
        return null;
    } // Fin de la m?thode familyInList/2

    /**
     *
     * @param name
     * @param testList
     * @return
     */
    public static Test testInList(Test test, ArrayList testList) {
        for(int i = 0; i  < testList.size(); i++) {
            if (((Test)testList.get(i)).getNameFromModel().equals(test.getNameFromModel()) &&
                ((Test)testList.get(i)).getTestListFromModel().getNameFromModel().equals(test.getTestListFromModel().getNameFromModel()) &&
                ((Test)testList.get(i)).getTestListFromModel().getFamilyFromModel().getNameFromModel().equals((test.getTestListFromModel().getFamilyFromModel().getNameFromModel()))
                ) {
                return (Test)testList.get(i);
            }
        }
        return null;
    } // Fin de la m?thode familyInList/2

    /**
     * Transforme le texte pass? en param?tre en un texte au format html d?coup?
     * en ligne dont la longueur est pass?e en param?tre
     * @param text le texte
     * @param lineSize la longueur des lignes
     * @return un texte au format html
     */
    public static String createHtmlString(String text, int lineSize) {
        String begin = "<html>";
        String end = "</html>";
        String beginCut = "<p>";
        String endCut = "</p>";

        String resul = begin;
        int i = 0;
        int j = lineSize;
        while (j <= text.length()) {
            resul = resul + beginCut + text.substring(i,j) + endCut;
            i = j;
            j = j + lineSize;
        }
        if (i != text.length()) {
            resul = resul + beginCut + text.substring(i, text.length()) + endCut;
        }
        return resul + end;
    } // Fin de la m?thode createHtmlString/2

    public static String createStringWithReturn(String text, int lineSize) {

        text.replaceAll("[\n]","");

        String resul = "";
        int i = 0;
        int j = lineSize;
        while (j <= text.length()) {
            resul = resul + "\n" + text.substring(i,j);
            i = j;
            j = j + lineSize;
        }
        if (i != text.length()) {
            resul = resul + "\n" + text.substring(i, text.length());
        }

        return resul;

    }

    /**
     * Retourne l'icone associ?e au type pass? au param?tre. On retourne <code>null</code>
     * si le type n'est pas SUCCESS, FAIL ou UNKNOW.
     * @param type
     * @return
     */
    public static ImageIcon getActionStatusIcon(String type) {
        if (type == null)
            return null;
        if (type.equals(SUCCESS)) {
            return Tools.createAppletImageIcon(PATH_TO_SUCCESS_ICON,"");
        } else if (type.equals(FAIL)) {
            return Tools.createAppletImageIcon(PATH_TO_FAIL_ICON,"");
        } else if (type.equals(UNKNOWN)) {
            return Tools.createAppletImageIcon(PATH_TO_UNKNOW_ICON,"");
        } else if (type.equals(NOTAPPLICABLE)) {
            return Tools.createAppletImageIcon(PATH_TO_NOTAPPLICABLE_ICON,"");
        } else if (type.equals(BLOCKED)) {
            return Tools.createAppletImageIcon(PATH_TO_BLOCKED_ICON,"");
        } else if (type.equals(NONE)) {
            return Tools.createAppletImageIcon(PATH_TO_NONE_ICON,"");
        }
        return null;
    } // Fin de la m?thode getActionStatusIcon/1


    /**
     *
     * @param url
     * @return
     */
    public static String[] chooseProjectAndUser(URL url) {
        String[] result = { "", "" };
        String urlString = url.toString();
        String[] tab = urlString.split("[?=]");
        Util.log("[Tools.chooseProjectAndUser()] tab.length = " + tab.length);
        for (int i = 0 ; i < tab.length ; i++)
            Util.log("[Tools.chooseProjectAndUser()] tab[" + i + "] = " + tab[i]);
        if (tab.length == 3) {
            int idConnection = Integer.parseInt(tab[2]);
            Util.log("[Tools.chooseProjectAndUser()] set ID Connection = " + idConnection);
            result[0] = Api.getConnectionProject(idConnection);
            Util.log("[Tools.chooseProjectAndUser()] Connection with project = " + result[0]);
            result[1] = Api.getConnectionUser(idConnection);
            Util.log("[Tools.chooseProjectAndUser()] Connection with user = " + result[1]);
        }
        return result;
    }

    /**
     *
     * @param descr
     * @return
     */
    public static ArrayList getParametersInDescription(String descr) {
        ArrayList result = new ArrayList();
        StringTokenizer tokenizer = new StringTokenizer(descr);
        while(tokenizer.hasMoreTokens()) {
            String element = tokenizer.nextToken();
            if (element.startsWith("$") && DataModel.getCurrentTest().hasUsedParameterNameFromModel(element.substring(1,(element.length()-1))) && element.endsWith("$")) {
                result.add(element.substring(1,(element.length()-1)));
            }
        }
        return result;

    }


    public static ArrayList getParametersInDescription(String descr, Hashtable paramTable) {
        HashSet paramSet = new HashSet(paramTable.values()); //ADD - Hashtable2HashSet
        ArrayList result = new ArrayList();
        for (Iterator iter = paramSet.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            if (containsString(descr,param.getNameFromModel())) result.add(param);
        }
        return result;
    }

    public static boolean containsString(String descr, String paramName) {
        char[] tabOfChar = descr.toCharArray();
        char[] paramCharTab = paramName.toCharArray();
        int i = 0;
        boolean find = false;
        while (i < tabOfChar.length) {
            System.out.print(tabOfChar[i]);
            if (tabOfChar[i] == '$') {
                i++;
                int oldValue = i;
                int j =0;
                while (j < paramCharTab.length && i < tabOfChar.length && paramCharTab[j] == tabOfChar[i]) {
                    j++;
                    i++;
                }
                if (j == paramCharTab.length && i < tabOfChar.length && tabOfChar[i] == '$') {
                    find = true;
                    break;
                } else {
                    i = oldValue- 1;
                    Util.debug();
                }
            }
            i++;
        }
        Util.debug();
        return find;
    }

    /**
     *
     * @param description
     * @param dataSet
     * @param env
     * @return
     */
    //public static String getInstantiedDescription(String description, DataSet dataSet) {
    public static String getInstantiedDescription(String description, Execution pExec) {
        String result = description;
        DataSet dataSet =  pExec.getDataSetFromModel();
        Environment ptrEnv  =  pExec.getEnvironmentFromModel();
        if (dataSet !=null) {
            Set dataSetKeysSet = dataSet.getParametersHashMapFromModel().keySet();
            for (Iterator iter = dataSetKeysSet.iterator(); iter.hasNext();) {
                String paramName = (String)iter.next();
                Parameter element = DataModel.getCurrentProject().getParameterFromModel(paramName);
                String value = dataSet.getParameterValueFromModel(element.getNameFromModel());
                if (value.startsWith(DataConstants.PARAM_VALUE_FROM_ENV)){
                    value = ptrEnv.getParameterValue(paramName);
                    if (value == null){
                        value = "";
                    }
                }
                try {
                    result = result.replaceAll("[$]" + element.getNameFromModel() + "[$]", value);
                } catch (Exception e) {}
            }
        }
        return result;
    } // Fin de la m?thode getInstantiedDescription/3


    /**
     *
     * @param description
     * @param paramName
     * @param test
     * @return
     */
    public static String clearStringOfParameter(String description, String paramName) {
        String result = description;
        result = result.replaceAll("[$]" + paramName + "[$]", "");
        return result;
    } // Fin de la m?thode clearStringOfParameter/3

    /**
     *
     * @param tsfVector
     * @param tsf
     * @return
     */
    /*public static boolean containsTSF(Vector tsfVector, TestSuiteFamily tsf) {
      for (int i = 0; i < tsfVector.size(); i++) {
      TestSuiteFamily tsfOfVector = (TestSuiteFamily)tsfVector.get(i);
      if (tsfOfVector.getFamilyName().equals(tsf.getFamilyName()) &&
      tsfOfVector.getTestName().equals(tsf.getTestName()) &&
      tsfOfVector.getSuiteName().equals(tsf.getSuiteName())) {
      return true;
      }
      }
      return false;
      }*/

    public static Image loadImages( JFrame frm, String imageFile )
    {
        try
            {
                MediaTracker mTrack = new MediaTracker( frm ); // load les image avant de les afficher
                Image image = createImage(PATH_TO_SALOME_INTRO_ICON, "");
                mTrack.addImage( image, 0 );
                mTrack.waitForAll();
                return image;
            }
        catch (Exception e)
            {
                Util.debug( " getimages : " + e );
            }
        return null;
    }

    public static URL getURL( String file )
        throws MalformedURLException
    {
        URL documentBase = new URL("file:///" + System.getProperty("user.dir") + "/");
        return new URL( documentBase, file );
    }

    /**
     * Methodes qui retourne une liste de liste contenant des tests soit manuels
     * soit automatiques. L'ordre est preserve par rapport a la liste donnee en
     * parametre.
     * @param list une liste de tests quelconques
     * @param une liste de liste de tests, regroupes selon leur type (manuel ou
     * autmatique)
     */
    public static ArrayList getListOfTestManualOrAutomatic(ArrayList list, ExecutionResult pExecutionResult, Vector pAllTestToExecute, boolean continueExec,  boolean onlyNotExec, boolean onlyAssigned,  boolean forModif) {
        ArrayList result = new ArrayList();
        ArrayList tempList = new ArrayList();
        boolean manual = false;
        if (list.get(0) instanceof ManualTest) {
            manual = true;
        } else {
            manual = false;
        }
        for (int i = 0; i < list.size(); i++) {
            boolean add = true;
            if(pExecutionResult != null){
                Test pTest = (Test)list.get(i);
                ExecutionTestResult pExecutionTestResult = pExecutionResult.getExecutionTestResultFromModel(pTest);
                if (pExecutionTestResult != null){
                    String status = pExecutionTestResult.getStatusFromModel();

                    //Util.debug("Previous status for "+pTest.getNameFromModel()+" is : " + status);
                    if (status != null && !status.equals("") && continueExec && onlyNotExec){
                        add = false;
                        //Util.debug("Not add test "+ pTest.getNameFromModel()+" with : " + status);
                    } else if (onlyAssigned){
                        Campaign pCamp = pExecutionResult.getExecution().getCampagneFromModel();
                        User currentUser = DataModel.getCurrentUser();
                        if ( currentUser.getIdBdd() != pCamp.getAssignedUserID(pTest)){
                            add = false;
                            //Util.debug("Not add test "+pTest.getNameFromModel()+", not assigned to user  : " + currentUser.getLoginFromModel());
                        }
                    } else {
                        //Util.debug("Add test "+ pTest.getNameFromModel()+" with : " + status);
                    }
                }
                if (forModif && pTest instanceof AutomaticTest) { //Pas possible de Modifier des resultats automatique
                    add = false;
                }
            }
            if (add){
                pAllTestToExecute.add(list.get(i));
                if (list.get(i) instanceof ManualTest) {
                    if (!manual) {
                        if (tempList.size()>0) {
                            result.add(tempList);
                            tempList = new ArrayList();
                        }
                    }
                    manual = true;
                    tempList.add(list.get(i));
                } else if (list.get(i) instanceof AutomaticTest) {
                    if (manual) {
                        if (tempList.size()>0){
                            result.add(tempList);
                            tempList = new ArrayList();
                        }
                    }
                    manual = false;
                    tempList.add(list.get(i));
                }
            }
        }
        if (tempList.size() != 0) {
            result.add(tempList);
        }
        return result;
    }

    /**
     *
     * @param testList
     */
    public static void initExecutionResultMap(ArrayList testList, ExecutionResult execResult, Campaign c) {
        for (int i = 0; i < testList.size(); i ++) {
            execResult.initTestResultStatusInModel((Test)testList.get(i), "", i, c);
        }
    } // Fin de la m?thode initMap/1

    /**
     *
     * @param str
     * @return
     */
    public static String speedpurge(String str) {
        String res_str;
        byte[] str_b = str.getBytes();
        int taille = str_b.length;
        byte[] res = new byte[taille];
        int j = 0;
        for (int i=0; i< taille  ; i++) {
            if (str_b[i] != 92) {
                res[j] = str_b[i];
                j++;
            } else {
                res[j] = 47;
                j++;
            }

        }
        res_str = new String(res,0, j);
        return res_str;

    }

    /**
     * M?thode d'affichage d'un message sous forme de fen?tre lorsqu'une
     * exception est lev?e
     * @param message un message ? afficher
     */
    public static void ihmExceptionView(Exception exception) {
        exception.printStackTrace();
        Util.err(exception);
        if (exception instanceof DataUpToDateException){
            SalomeTMFContext.getInstance().showMessage(
                                                       Language.getInstance().getText("Update_data"),
                                                       Language.getInstance().getText("Erreur_"),
                                                       JOptionPane.ERROR_MESSAGE);
        } else if (exception instanceof LockException) {
            LockException lockException = (LockException)exception;
            if (lockException.code == LockException.DOLOCK) {
                int admin_id_bdd = -1;
                try {
                    admin_id_bdd = DataModel.getCurrentProject().getAdministratorWrapperFromDB().getIdBDD();
                } catch (Exception e) {
                    Util.err(e);
                }
                if (admin_id_bdd == DataModel.getCurrentUser().getIdBdd()) {
                    Object[] options = {Language.getInstance().getText("Oui"), Language.getInstance().getText("Non")};
                    int choice = -1;
                    //int actionCase = -1;
                    choice = JOptionPane.showOptionDialog(new Frame(),
                                                          Language.getInstance().getText("Lock_Problem_User_Is_Admin"),
                                                          Language.getInstance().getText("Attention_"),
                                                          JOptionPane.YES_NO_OPTION,
                                                          JOptionPane.QUESTION_MESSAGE,
                                                          null,
                                                          options,
                                                          options[1]);
                    if (choice == JOptionPane.YES_OPTION) {
                        JDialog dialog = new JDialog(SalomeTMFContext.getInstance().getSalomeFrame(), true);
                        SalomeLocksPanel locksPanel = new SalomeLocksPanel(DataModel.getCurrentProject().getIdBdd());
                        locksPanel.loadDataFromDB();
                        dialog.getContentPane().add(locksPanel);
                        dialog.setTitle("Salome locks");
                        dialog.pack();
                        dialog.setLocation(400,300);
                        dialog.setVisible(true);
                    }
                } else {
                    JOptionPane.showMessageDialog(new Frame(),
                                                  Language.getInstance().getText("Lock_Problem_User_IsNot_Admin"),
                                                  Language.getInstance().getText("Information_"),
                                                  JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(new Frame(),
                                          Language.getInstance().getText("Probleme_inconnu__") + exception.toString(),
                                          Language.getInstance().getText("Information_"),
                                          JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void RefreshView(String name , String type) {
        JOptionPane.showMessageDialog(new Frame(),
                                      Language.getInstance().getText("Les_donnees_de_") + name  + "[" + type + Language.getInstance().getText("_ne_sont_pas_a_jour_veuillez_faire_un_rafraichir_avant_toute_autre_commande"),
                                      Language.getInstance().getText("Information_"),
                                      JOptionPane.INFORMATION_MESSAGE);
    }
    public static void writeFile(InputStream input, String path) {
        try {
            // directory creation if it does not exist
            File destFile = new File(path);
            if (!destFile.getParentFile().exists()) destFile.getParentFile().mkdir();

            // Flux d'?criture sur le fichier
            FileOutputStream fos = new FileOutputStream(path);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            BufferedInputStream bis = new BufferedInputStream(input);

            // Copie du flux dans le fichier
            int car = bis.read();
            while (car > -1) {
                bos.write(car);
                car = bis.read();
            }
            //  Fermeture des flux de donn?es
            bos.flush();
            bos.close();
            bis.close();
        } catch (Exception e) {
            Util.err(e);
        }
    }
    
    /**
     * The method parses all execution results of the given execution 
     * and returns their global state.
     * 
     * @param execution     The execution of the test campaign
     * @return result       = FINISEHD (if all results are FINISHED)
     *                      = INCOMPLETED (if one result is INCOMPLETED)
     */
    public static String completeExecutionResult (Execution execution) {
        String result;
        ArrayList array;
        ExecutionResult executionResult;
        
        array = execution.getExecutionResultListFromModel();
        result = ApiConstants.FINISHED;
        if (array.size() == 0) {
            // No execution result is available!
            result = EMPTY;
        } else {
            // parse the execution results
            for (int i = 0; i < array.size();i++) {
                executionResult = (ExecutionResult) (execution).getExecutionResultListFromModel().get(i);
                if (!executionResult.getExecutionStatusFromModel().equals(ApiConstants.FINISHED)) {
                    result = ApiConstants.INCOMPLETED;
                }
            }
        }
        return result;
    }
    
    /**
     * The method parses all execution results of the given campaign
     * 
     * @param campaign  Campaign of the test
     * @return result   = true  (all executions are TERMINATED)
     *                  = false (more than one execution are INCOMPLETED)
     */
    public static boolean campaignTerminated (Campaign campaign) {
        boolean result = true;
        String state;
        ArrayList executions = campaign.getExecutionListFromModel();
        if ((executions != null) && (executions.size() > 0)) {
            for (int i = 0; i < executions.size(); i++) {
                state = completeExecutionResult(((Execution) executions.get(i)));
                if (state.equals(ApiConstants.INCOMPLETED) || state.equals(ApiConstants.EMPTY)) {
                    result = false;
                    break;
                }
            } 
        } else {
            result = false;
        }
        return result;
    }
    
} // Fin de la classe Tools
