/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLParameter;

public class SQLParameter implements ISQLParameter{
        
    /**
     * Add parameter  in a project
     * @param idProject  : id in Dabase of the project
     * @param name : name of the parameter 
     * @param description : description of the parameter
     * @return the id of the parameter in the dadabase
     * need permission canCreateTest
     */
    @Override
    public int insert(int IdProject, String name, String description) throws Exception {
        if (IdProject <1) {
            throw new Exception("[SQLParameter->insert] entry data are not valid");
        }
        int transNumber = -1;
        int idParam = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLPamameter : updateDescription -> canCreateTest]");
            }
        }
        try {   
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.INSERT_PARAMETER); 
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addParamToProject"); //ok
            prep.setInt(1,IdProject);
            prep.setString(2,name);
            prep.setString(3,description);
            SQLEngine.runAddQuery(prep);
                        
            idParam = selectID(IdProject, name);
            if (idParam <1) {
                throw new Exception("[SQLParameter->insert] id is not valid");
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e){
            Util.err("[SQLParameter->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idParam;
    }
        
    /**
     * get database id for a  parameter identified by name in the project IdProject
     * @param idProject
     * @param name
     * @return the database id of the parameter identified by name in the project IdProject
     * @throws Exception
     */
    @Override
    public int selectID(int IdProject, String name) throws Exception {
        if (IdProject <1) {
            throw new Exception("[SQLParameter->selectID] entry data are not valid");
        }
        int idParam = -1;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdParam"); //ok
        prep.setString(1, name);
        prep.setInt(2, IdProject);
        ResultSet result = SQLEngine.runSelectQuery(prep);
        if (result.next()) {
            idParam = result.getInt("id_param_test");
        }
                
        return idParam;
    }
        
    /**
     * Update parameter (identifed by idParameter) description in DataBase 
     * @param idParameter
     * @param description : the new description of the parameter
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void updateDescription(int idParameter,  String description) throws Exception {
        if (idParameter <1) {
            throw new Exception("[SQLParameter->updateDescription] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLPamameter : updateDescription --> canUpdateTest]");
            }
        }
        try {   
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.UPDATE_PARAMETER); 
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateParamProjectUsingID"); //ok
            prep.setString(1, description);
            prep.setInt(2,idParameter);
                        
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e){
            Util.err("[SQLParameter->updateDescription]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete a parameter in Salome Database, include the suppresion of the parameter
     * in test, and in action
     * @param idParameter
     * @throws Exception
     * need permission canDeleteTest
     */
    @Override
    public void delete(int idParameter) throws Exception {
        int transNumber = -1;
        if (idParameter <1) {
            throw new Exception("[SQLParameter->delete] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLPamameter : updateDescription --> canDeleteTest]");
            }
        }
        try {   
            transNumber = SQLEngine.beginTransaction(111, ApiConstants.DELETE_PARAMETER);       
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteParamUsingID"); //ok
            prep.setInt(1, idParameter);
            SQLEngine.runDeleteQuery(prep);
                        
            prep =  prep = SQLEngine.getSQLDeleteQuery("deleteParamFromAllTest"); //ok
            prep.setInt(1, idParameter);
            SQLEngine.runDeleteQuery(prep);
                        
            prep = prep = SQLEngine.getSQLDeleteQuery("deleteParamFromAllAction"); //ok
            prep.setInt(1, idParameter);
            SQLEngine.runDeleteQuery(prep);
                        
            prep = prep = SQLEngine.getSQLDeleteQuery("deleteParamFromAllEnvDataSet"); //ok
            prep.setInt(1, idParameter);
            SQLEngine.runDeleteQuery(prep);
                        
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e){
            Util.err("[SQLParameter->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get a ParameterWrapper representing the parameter idParameter in the database 
     * @param idParameter
     * @return
     * @throws Exception
     */
    @Override
    public ParameterWrapper getParameterWrapper(int idParameter) throws Exception {
        if (idParameter <1) {
            throw new Exception("[SQLParameter->getParameterWrapper] entry data are not valid");
        } 
                
        ParameterWrapper pParameterWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(1, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectParamByID"); //ok
            prep.setInt(1, idParameter);
            ResultSet result = SQLEngine.runSelectQuery(prep);
            if (result.next()) {
                pParameterWrapper = new ParameterWrapper();
                pParameterWrapper.setName(result.getString("nom_param_test"));
                pParameterWrapper.setDescription(result.getString("desc_param_test"));
                pParameterWrapper.setIdBDD(result.getInt("id_param_test")); 
            }   
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pParameterWrapper;       
    }
}
