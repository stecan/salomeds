/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLAutomaticTest;



public class SQLAutomaticTest extends SQLTest implements ISQLAutomaticTest {
        
    /**
     * Insert an Automatic test in table CAS_TEST
     * @param idTestList : the id of the testlist which contain the inserted test
     * @param name : the name of the test
     * @param description : the description of the tests
     * @param conceptor : the conceptor of the test
     * @param extension : the plug-in extension of the test
     * @return the id of the test in table CAS_TEST
     * @throws Exception
     * need permission canCreateTest
     */
    @Override
    public int insert(int idTestList, String name, String description, String conceptor, String extension) throws Exception {
        int testId = -1;
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLTest : insert -> canCreateTest]");
            }
        }
        if (idTestList <1) {
            throw new Exception("[SQLAutomaticTest->insert] suite have no id");
        } 
        if (SQLObjectFactory.getInstanceOfISQLTestList().getTestList(idTestList) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_TEST);
                        
            int order = SQLObjectFactory.getInstanceOfISQLTestList().getNumberOfTest(idTestList);
            order --; //Because index begin at 0
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addTest"); //ok
            int idPers =  SQLObjectFactory.getInstanceOfISQLPersonne().getID(conceptor);
            if (idPers <1) {
                throw new Exception("[SQLAutomaticTest->insert] user have no id");
            } 
            prep.setInt(1,idPers);
            prep.setInt(2,idTestList);
            prep.setString(3,name);
            prep.setDate(4,Util.getCurrentDate());
            prep.setTime(5,Util.getCurrentTime());
            prep.setString(6,ApiConstants.AUTOMATIC);
            prep.setString(7,description);
            prep.setInt(8, order + 1);
            prep.setString(9, extension);
            SQLEngine.runAddQuery(prep);
                        
            testId = getID(idTestList, name);
            if (testId <1) {
                throw new Exception("[SQLAutomaticTest->insert] test have no id");
            }           
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAutomaticTest->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return testId;   
    }
        
    /**
     * Insert a Script to the test
     * @param idTest : id of the test to insert the script
     * @param file : the file of the script
     * @param description : the description of the file
     * @param name : the name of the script (localisation/name) (url_script in BDD)
     * @param arg1 : argument 1 of the script (free use for plug-in) (classe_autom_script in BDD)
     * @param extension : argument 1 of the script (plug-in extenstion)  (classpath_script in BDD)
     * @return the id of script inserted in the table SCRIPT_ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * @see ISQLScript.addAttach(int, int);
     * need permission canUpdateTest
     */
    @Override
    public int addScript(int idTest, SalomeFileWrapper file, String description,  String name, String arg1, String extension) throws Exception {
        int idScript = -1;
        int idAttach = -1; 
        int transNumber = -1;
        if (idTest <1) {
            throw new Exception("[SQLAutomaticTest->addScript] test have no id");
        } 
        if (file == null) {
            throw new Exception("[SQLAutomaticTest->addScript] file is null");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : deleteScript -> canUpdateTest]");
            }
        }       
        if (getTest(idTest) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_SCRIPT);
            idScript = SQLObjectFactory.getInstanceOfISQLScript().insert(name, arg1, extension, ApiConstants.TEST_SCRIPT);
                        
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file, description);
                        
            SQLObjectFactory.getInstanceOfISQLScript().addAttach(idScript, idAttach);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("attachScriptToTest");  //ok
            prep.setInt(1,idScript);
            prep.setInt(2,idTest);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAutomaticTest->addScript]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idScript;
    }
    /**
     * Delete the test in the database, this include :
     * delete test script, and all reference (Parameter, Campaign) and the test attachments
     * @param idTest
     * @throws Exception
     * need permission canDeleteTest (do a special allow)
     */
    @Override
    public void delete(int idTest)  throws Exception {
        int transNumber = -1;
        boolean dospecialAllow = false;
        if (idTest <1) {
            throw new Exception("[SQLAutomaticTest->delete] test have no id");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLTest : updateOrder -> canDeleteTest]");
            }
            //Require specialAllow
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }
                
        try {
                        
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_TEST);
                        
            // Begin Delete
                        
            //Delete Script Test if test is Automatic
            deleteScript(idTest);
                        
            // Suppression du test
            deleteRef(idTest, true); //reorder
                        
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLAutomaticTest->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }
        
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test
     * @param idTest
     * @param paramId
     * @throws Exception
     * need permission  canUpdateTest
     * @see deleteUseParamRef
     */
    @Override
    public void deleteUseParam(int idTest, int paramId) throws Exception {
        int transNumber = -1;
        if (idTest <1) {
            throw new Exception("[SQLAutomaticTest->deleteUseParam] test have no id");
        } 
        if (paramId <1) {
            throw new Exception("[SQLAutomaticTest->deleteUseParam] param have no id");
        }
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLAutomaticTest : deleteUseParam -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_PARAMETER_FROM_TEST);
                        
            deleteUseParamRef(idTest, paramId);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAutomaticTest->deleteUseParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete the script used by the tests, this include the removing of Attachement and all reference
     * @param testId
     * @throws Exception
     * need permission  canUpdateTest
     * @see ISQLScript().delete(int, int)
     */
    @Override
    public void deleteScript(int testId) throws Exception {
        if (testId <1) {
            throw new Exception("[SQLAutomaticTest->deleteScript] test have no id");
        } 
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : deleteScript -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectScriptAttchOfTest");
            prep.setInt(1,testId);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()) {
                int scriptId = stmtRes.getInt("SCRIPT_id_script");
                //int attachId = stmtRes.getInt("ATTACHEMENT_id_attach");
                                
                SQLObjectFactory.getInstanceOfISQLScript().delete(scriptId);
                //SQLObjectFactory.getInstanceOfISQLAttachment().delete(attachId);
                prep = SQLEngine.getSQLUpdateQuery("updateScriptFromTest"); //ok
                prep.setInt(1, testId);
                SQLEngine.runUpdateQuery(prep);
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAutomaticTest->deleteScript]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Get a ScriptWrapper representing the script for the tests testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     * @see ScriptWrapper
     */
    @Override
    public ScriptWrapper getTestScript(int testId) throws Exception {
        ScriptWrapper pScript = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestScript"); //ok
        prep.setInt(1,testId);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {
            pScript = new ScriptWrapper();
            pScript.setName(stmtRes.getString("url_script"));
            pScript.setScriptExtension(stmtRes.getString("classpath_script"));
            pScript.setPlugArg(stmtRes.getString("classe_autom_script"));
            pScript.setType(stmtRes.getString("type_script"));
            pScript.setIdBDD(stmtRes.getInt("id_script"));
        }
        return pScript;
    }
        
    /**
     * Get the Script File atached to the script for the tests testId
     * @param testId
     * @return
     * @throws Exception
     * @see ISQLScript.getFile(int)
     * no permission needed
     */
    @Override
    public SalomeFileWrapper getTestScriptFile(int testId) throws Exception {
        ScriptWrapper pScript = getTestScript(testId);
        return SQLObjectFactory.getInstanceOfISQLScript().getFile(pScript.getIdBDD());
    }
}
