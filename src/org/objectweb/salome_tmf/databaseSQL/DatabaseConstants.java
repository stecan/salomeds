package org.objectweb.salome_tmf.databaseSQL;

public interface DatabaseConstants {
    /**
     * SQL statements files paths
     */
    final static public String SQLBASE = "/org/objectweb/salome_tmf/databaseSQL/sql/";
    final static public String ADD_SQL_QUERY_FILE_PATH = "addSqlQuery.properties";
    final static public String SELECT_SQL_QUERY_FILE_PATH = "selectSqlQuery.properties";
    final static public String COMMON_SQL_QUERY_FILE_PATH = "commonSqlQuery.properties";
    final static public String UPDATE_SQL_QUERY_FILE_PATH = "updateSqlQuery.properties";
    final static public String DELETE_SQL_QUERY_FILE_PATH = "deleteSqlQuery.properties";
}
