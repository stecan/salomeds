/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLProject;

public class SQLProject implements ISQLProject {

    /**
     * Insert a project in the DB and create default group and permission for
     * the project
     *
     * @param name
     * @param description
     * @return
     * @throws Exception
     */
    @Override
    public int insert(String name, String description, int idAdmin)
        throws Exception {
        int id = -1;
        int transNumber = -1;
        if (idAdmin < 1) {
            throw new Exception("[SQLProject->insert] user have no id");
        }
        try {
            Date dateActuelle = Util.getCurrentDate();

            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_PROJECT);
            Util.log("[SQLProject] add project " + name + ", admin id : "
                     + idAdmin);
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addProject"); // ok
            prep.setString(1, name);
            prep.setDate(2, dateActuelle);
            prep.setString(3, description);
            SQLEngine.runAddQuery(prep);

            ProjectWrapper pProject = getProject(name);
            id = pProject.getIdBDD();

            int perm = 0;
            SQLObjectFactory.getInstanceOfISQLGroup().insert(id,
                                                             ApiConstants.GUESTNAME, ApiConstants.GUESTDESC, perm);

            perm |= Permission.ALLOW_CREATE_TEST;
            perm |= Permission.ALLOW_CREATE_CAMP;
            perm |= Permission.ALLOW_UPDATE_CAMP;
            perm |= Permission.ALLOW_UPDATE_TEST;
            perm |= Permission.ALLOW_EXECUT_CAMP;

            SQLObjectFactory.getInstanceOfISQLGroup().insert(id,
                                                             ApiConstants.DEVNAME, ApiConstants.DEVDESC, perm);

            perm |= Permission.ALLOW_DELETE_CAMP;
            perm |= Permission.ALLOW_DELETE_TEST;
            int idGrpAdm = SQLObjectFactory.getInstanceOfISQLGroup().insert(id,
                                                                            ApiConstants.ADMINNAME, ApiConstants.ADMINDESC, perm);

            SQLObjectFactory.getInstanceOfISQLGroup().insertUser(idGrpAdm,
                                                                 idAdmin);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return id;
    }

    /**
     * Create a project by copy information with an other project idFromProject
     *
     * @param name
     * @param description
     *            : the description of the new project
     * @param idAdmin
     *            : the administator of the new project
     * @param idFromProject
     *            : the project where find the informations to create the new
     *            project
     * @param suite
     *            : true for copy tests information
     * @param campagne
     *            : true for copy campaigns information
     * @param users
     *            : true for copy users information
     * @param groupe
     *            : true for copy groups information
     * @return
     * @throws Exception
     */
    @Override
    public int copyProject(String name, String description, int idAdmin,
                           int idFromProject, boolean suite, boolean campagne, boolean users,
                           boolean groupe) throws Exception {
        SQLEngine.setSpecialAllow(true);
        int project_id_new = -1;
        int transNumber = -1;
        if (idAdmin < 1) {
            throw new Exception("[SQLProject->copyProject] user have no id");
        }
        if (idFromProject < 1) {
            throw new Exception(
                                "[SQLProject->copyProject] from project have no id");
        }
        try {
            Hashtable mapGroupe = new Hashtable();
            Hashtable mapFamille = new Hashtable();
            Hashtable mapSuite = new Hashtable();
            Hashtable mapCas = new Hashtable();
            Hashtable mapAction = new Hashtable();
            Hashtable mapParam = new Hashtable();
            // Hashtable mapCampagne = new Hashtable();

            project_id_new = insert(name, description, idAdmin);
            GroupWrapper[] projectGroups;
            projectGroups = SQLObjectFactory.getInstanceOfISQLGroup()
                .getGroupWrapperInProject(idFromProject);
            if (groupe) {
                for (int i = 0; i < projectGroups.length; i++) {
                    GroupWrapper pGroupWrapper = projectGroups[i];
                    int id_g = pGroupWrapper.getIdBDD();
                    String nom = pGroupWrapper.getName();
                    String des = pGroupWrapper.getDescription();
                    int perm = pGroupWrapper.getPermission();
                    if (!(nom.equals(ApiConstants.ADMINNAME))
                        && !(nom.equals(ApiConstants.DEVNAME))
                        && !(nom.equals(ApiConstants.GUESTNAME))) {
                        int id_new_g = SQLObjectFactory
                            .getInstanceOfISQLGroup().insert(
                                                             project_id_new, nom, des, perm);
                        mapGroupe.put(new Integer(id_g), new Integer(id_new_g));
                    }
                }
            }

            if (users) {
                for (int i = 0; i < projectGroups.length; i++) {
                    GroupWrapper pGroupWrapper = projectGroups[i];
                    int idGroupe = pGroupWrapper.getIdBDD();
                    UserWrapper[] projectUsersGroup = SQLObjectFactory
                        .getInstanceOfISQLGroup().getUserWrappersInGroup(
                                                                         idGroupe);
                    for (int j = 0; j < projectUsersGroup.length; j++) {
                        UserWrapper pUserWrapper = projectUsersGroup[j];
                        int idUser = pUserWrapper.getIdBDD();
                        if (idAdmin != idUser) {
                            if (groupe
                                && mapGroupe.get(new Integer(idGroupe)) != null) {

                                idGroupe = (Integer) mapGroupe.get(new Integer(
                                                                               idGroupe));
                            } else {
                                idGroupe = SQLObjectFactory
                                    .getInstanceOfISQLGroup().getID(
                                                                    project_id_new,
                                                                    ApiConstants.GUESTNAME);
                            }
                            SQLObjectFactory.getInstanceOfISQLGroup()
                                .insertUser(idGroupe, idUser);
                        }
                    }
                }
            }

            if (suite) {
                /* Ajout des parametres */
                ParameterWrapper[] listOfParam = getProjectParams(idFromProject);
                for (int i = 0; i < listOfParam.length; i++) {
                    ParameterWrapper pParameterWrapper = listOfParam[i];
                    String nom = pParameterWrapper.getName();
                    String des = pParameterWrapper.getDescription();
                    int id = pParameterWrapper.getIdBDD();
                    int idNew_parm = SQLObjectFactory
                        .getInstanceOfISQLParameter().insert(
                                                             project_id_new, nom, des);
                    mapParam.put(new Integer(id), new Integer(idNew_parm));
                }

                /* Ajout des familles */
                FamilyWrapper[] listofFamily = getFamily(idFromProject);
                for (int i = 0; i < listofFamily.length; i++) {
                    FamilyWrapper pFamilyWrapper = listofFamily[i];
                    int id_fam = pFamilyWrapper.getIdBDD();
                    String nom = pFamilyWrapper.getName();
                    String des = pFamilyWrapper.getDescription();
                    int idNew_fam = SQLObjectFactory.getInstanceOfISQLFamily()
                        .insert(project_id_new, nom, des);
                    mapFamille.put(new Integer(id_fam), new Integer(idNew_fam));

                    /* Ajout des suites */
                    SuiteWrapper[] listofSuite = SQLObjectFactory
                        .getInstanceOfISQLFamily().getTestList(id_fam);
                    for (int j = 0; j < listofSuite.length; j++) {
                        SuiteWrapper pSuiteWrapper = listofSuite[j];
                        int id_suite = pSuiteWrapper.getIdBDD();
                        // int famille_id =
                        // stmtRes.getResults().getInt("FAMILLE_TEST_id_famille");
                        nom = pSuiteWrapper.getName();
                        des = pSuiteWrapper.getDescription();
                        int idNew_suite = SQLObjectFactory
                            .getInstanceOfISQLTestList().insert(idNew_fam,
                                                                nom, des);
                        mapSuite.put(new Integer(id_suite), new Integer(
                                                                        idNew_suite));

                        /* Ajout des tests */
                        TestWrapper[] listOfTest = SQLObjectFactory
                            .getInstanceOfISQLTestList().getTestsWrapper(
                                                                         id_suite);
                        for (int k = 0; k < listOfTest.length; k++) {
                            TestWrapper pTestWrapper = listOfTest[k];
                            int id_cas = pTestWrapper.getIdBDD();
                            String conceptor = SQLObjectFactory
                                .getInstanceOfISQLPersonne().getLogin(
                                                                      idAdmin);
                            // if (users) {
                            // conceptor = pTestWrapper.getConceptor();
                            // }
                            nom = pTestWrapper.getName();
                            des = pTestWrapper.getDescription();
                            int idNewTest;
                            if (pTestWrapper.getType().equals(
                                                              ApiConstants.MANUAL)) {
                                idNewTest = SQLObjectFactory
                                    .getInstanceOfISQLManualTest().insert(
                                                                          idNew_suite, nom, des,
                                                                          conceptor, "");

                            } else {
                                String ext = pTestWrapper.getTestExtension();
                                idNewTest = SQLObjectFactory
                                    .getInstanceOfISQLAutomaticTest()
                                    .insert(idNew_suite, nom, des,
                                            conceptor, ext);
                            }
                            mapCas.put(new Integer(id_cas), new Integer(
                                                                        idNewTest));
                            /* Ajout des parametres */
                            listOfParam = SQLObjectFactory
                                .getInstanceOfISQLTest().getAllUseParams(
                                                                         id_cas);
                            for (int l = 0; l < listOfParam.length; l++) {
                                ParameterWrapper pParameterWrapper = listOfParam[l];
                                int old_id_param_test = pParameterWrapper
                                    .getIdBDD();
                                int new_id_param_test = ((Integer) mapParam
                                                         .get(new Integer(old_id_param_test)))
                                    .intValue();
                                SQLObjectFactory.getInstanceOfISQLTest()
                                    .addUseParam(idNewTest,
                                                 new_id_param_test);
                            }
                            if (pTestWrapper.getType().equals(
                                                              ApiConstants.MANUAL)) {
                                /* Ajout des Actions */
                                ActionWrapper[] listOfAction = SQLObjectFactory
                                    .getInstanceOfISQLManualTest()
                                    .getTestActions(id_cas);
                                for (int m = 0; m < listOfAction.length; m++) {
                                    ActionWrapper pActionWrapper = listOfAction[m];
                                    int id_action = pActionWrapper.getIdBDD();
                                    nom = pActionWrapper.getName();
                                    des = pActionWrapper.getDescription();
                                    String res_attendu_action = pActionWrapper
                                        .getAwaitedResult();
                                    int idNew_action = SQLObjectFactory
                                        .getInstanceOfISQLAction().insert(
                                                                          idNewTest, nom, des,
                                                                          res_attendu_action);
                                    mapAction.put(new Integer(id_action),
                                                  new Integer(idNew_action));
                                    listOfParam = SQLObjectFactory
                                        .getInstanceOfISQLAction()
                                        .getParamsUses(id_action);
                                    for (int l = 0; l < listOfParam.length; l++) {
                                        ParameterWrapper pParameterWrapper = listOfParam[l];
                                        int old_id_param_test = pParameterWrapper
                                            .getIdBDD();
                                        int new_id_param_test = ((Integer) mapParam
                                                                 .get(new Integer(
                                                                                  old_id_param_test)))
                                            .intValue();
                                        SQLObjectFactory
                                            .getInstanceOfISQLAction()
                                            .addUseParam(idNew_action,
                                                         new_id_param_test);
                                    }
                                }
                            } else {
                                /* Ajout des script */
                                ScriptWrapper pScriptWrapper = SQLObjectFactory
                                    .getInstanceOfISQLAutomaticTest()
                                    .getTestScript(id_cas);
                                des = pScriptWrapper.getDescription();
                                nom = pScriptWrapper.getName();
                                String arg = pScriptWrapper.getPlugArg();
                                String ext = pScriptWrapper
                                    .getScriptExtension();
                                SalomeFileWrapper f = SQLObjectFactory
                                    .getInstanceOfISQLAutomaticTest()
                                    .getTestScriptFile(id_cas);
                                SQLObjectFactory
                                    .getInstanceOfISQLAutomaticTest()
                                    .addScript(idNewTest, f, des, nom, ext,
                                               arg);
                            }

                        }
                    }
                }

            }

            if (campagne && suite) {
                CampaignWrapper[] listOfCampaign = getPrjectCampaigns(idFromProject);
                for (int i = 0; i < listOfCampaign.length; i++) {
                    CampaignWrapper pCampaignWrapper = listOfCampaign[i];
                    int idCamp = pCampaignWrapper.getIdBDD();
                    String nom = pCampaignWrapper.getName();
                    String des = pCampaignWrapper.getDescription();
                    String conceptor = pCampaignWrapper.getConceptor();
                    int idUser = idAdmin;
                    // if (users) {
                    // idUser = SQLObjectFactory.getInstanceOfISQLPersonne()
                    // .getID(conceptor);
                    // }
                    int idNew_Camp = SQLObjectFactory
                        .getInstanceOfISQLCampaign().insert(project_id_new,
                                                            nom, des, idUser);
                    TestCampWrapper[] testInCamp = SQLObjectFactory
                        .getInstanceOfISQLCampaign()
                        .getTestsByOrder(idCamp);
                    for (int j = 0; j < testInCamp.length; j++) {
                        TestCampWrapper pTestCampWrapper = testInCamp[j];
                        int idoldtest = pTestCampWrapper.getIdBDD();
                        int idnewtest = ((Integer) mapCas.get(new Integer(
                                                                          idoldtest))).intValue();
                        SQLObjectFactory.getInstanceOfISQLCampaign()
                            .importTest(idNew_Camp, idnewtest, idUser);
                    }
                }

            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->copyProject]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return project_id_new;
    }

    /**
     * Attach a file to the Project identified by idProject (Table
     * PROJET_VOICE_TESTING_ATTACHEMENT )
     *
     * @param idProject
     * @param f
     *            the file
     * @param description
     *            of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String) no permission needed
     */
    @Override
    public int addAttachFile(int idProject, SalomeFileWrapper f,
                             String description) throws Exception {
        if (idProject < 1 || f == null) {
            throw new Exception(
                                "[SQLProject->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment()
                .insert(f, description);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("addAttachToProject"); // ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idProject);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }

    /**
     * Attach an Url to the Project identified by idProject (Table
     * PROJET_VOICE_TESTING_ATTACHEMENT )
     *
     * @param idProject
     * @param url
     * @param description
     *            of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String) no permission needed
     */
    @Override
    public int addAttachUrl(int idProject, String url, String description)
        throws Exception {
        if (idProject < 1 || url == null) {
            throw new Exception(
                                "[SQLProject->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        try {

            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_ATTACHMENT);

            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment()
                .insert(url, description);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("addAttachToProject"); // ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idProject);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }

    /**
     * Update project name and description in the database for the project
     * idProject
     *
     * @param idProject
     * @param newName
     * @param newDesc
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void update(int idProject, String newName, String newDesc)
        throws Exception {
        int transNumber = -1;
        if (idProject < 1) {
            throw new Exception("[SQLProject->update] project have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_PROJECT);
            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateProject"); // ok

            prep.setString(1, newName);
            prep.setString(2, newDesc);
            prep.setInt(3, idProject);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete a project in the database the delete
     *
     * @param idProject
     * @param name
     * @throws Exception
     */
    @Override
    public void delete(int idProject, String name) throws Exception {
        int transNumber = -1;
        if (idProject < 1) {
            throw new Exception("[SQLProject->delete] project have no id");
        }
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }

        try {
            transNumber = SQLEngine.beginTransaction(111,
                                                     ApiConstants.DELETE_PROJECT);

            GroupWrapper[] projectGroups;
            FamilyWrapper[] projectFamilies;
            CampaignWrapper[] projectCampaigns;
            EnvironmentWrapper[] projectEnv;
            ParameterWrapper[] projectParam;

            /* Suppression de toutes campagnes de test du projet */
            projectCampaigns = getPrjectCampaigns(idProject);
            for (int i = 0; i < projectCampaigns.length; i++) {
                CampaignWrapper pCampaignWrapper = projectCampaigns[i];
                SQLObjectFactory.getInstanceOfISQLCampaign().delete(
                                                                    pCampaignWrapper.getIdBDD(), false); // NOREORDER
            }

            /* Suppression de tous les environnements */
            projectEnv = getProjectEnvs(idProject);
            for (int i = 0; i < projectEnv.length; i++) {
                EnvironmentWrapper pEnvironmentWrapper = projectEnv[i];
                SQLObjectFactory.getInstanceOfISQLEnvironment().delete(
                                                                       pEnvironmentWrapper.getIdBDD());
            }

            /* Suppression de toutes les familles de test du projet */
            projectFamilies = getFamily(idProject);
            for (int i = 0; i < projectFamilies.length; i++) {
                FamilyWrapper pFamilyWrapper = projectFamilies[i];
                // SQLObjectFactory.getInstanceOfISQLFamily().delete(pFamilyWrapper.getIdBDD());
                SQLObjectFactory.getInstanceOfISQLFamily().delete(
                                                                  pFamilyWrapper.getIdBDD(), false); // NOREORDER
            }

            /* Suppression des parametres */
            projectParam = getProjectParams(idProject);
            for (int i = 0; i < projectParam.length; i++) {
                ParameterWrapper pParameterWrapper = projectParam[i];
                SQLObjectFactory.getInstanceOfISQLParameter().delete(
                                                                     pParameterWrapper.getIdBDD());
            }

            /* Suppression de tous les groupes d'utilisateurs du projet */
            projectGroups = getProjectGroups(idProject);
            for (int i = 0; i < projectGroups.length; i++) {
                GroupWrapper pGroupWrapper = projectGroups[i];
                SQLObjectFactory.getInstanceOfISQLGroup().delete(
                                                                 pGroupWrapper.getIdBDD());
            }

            /* Suppression des attachements */
            deleteAllAttach(idProject);

            /* Suppression du projet */
            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteProject");
            prep.setInt(1, idProject);
            SQLEngine.runDeleteQuery(prep);

            /* Suppression des config */
            try {
                if (Api.getLockMeth() == 0) {
                    prep = SQLEngine.getSQLCommonQuery("lockCONFIGWRITE");
                    SQLEngine.runSelectQuery(prep);
                }
                SQLObjectFactory.getInstanceOfISQLConfig()
                    .deleteAllProjectConf(idProject);
            } catch (Exception e1) {
                Util.err(e1);
                /* WARNING */
            }
            /* Suppression des log d'action du projet */// et unlock ??
            /*
             * prep = SQLEngine.getSQLCommonQuery("lockAction");
             * SQLEngine.runSelectQuery(prep);
             */

            try {
                if (Api.getLockMeth() == 0) {
                    prep = SQLEngine.getSQLCommonQuery("lockLDBAWRITE");
                    SQLEngine.runSelectQuery(prep);
                }
                prep = SQLEngine.getSQLCommonQuery("deleteLastChange"); // ok
                prep.setString(1, name);
                SQLEngine.runDeleteQuery(prep);
            } catch (Exception e1) {
                Util.err(e1);
                /* WARNING */
            }
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }

            SQLEngine.commitTrans(transNumber);
            try {
                SQLObjectFactory.getInstanceOfISQLSalomeLock()
                    .delete(idProject);
            } catch (Exception e1) {

            }
        } catch (Exception e) {
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLProject->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete all attchements of the project identified by idProject
     *
     * @param idProject
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void deleteAllAttach(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_ATTACHMENT);

            AttachementWrapper[] attachList = getAllAttachemnt(idProject);
            for (int i = 0; i < attachList.length; i++) {
                AttachementWrapper pAttachementWrapper = attachList[i];
                deleteAttach(idProject, pAttachementWrapper.getIdBDD());
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete an attchement idAttach of the project identified by idProject
     *
     * @param idProject
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int) no permission needed
     */
    @Override
    public void deleteAttach(int idProject, int idAttach) throws Exception {
        if (idProject < 1 || idAttach < 1) {
            throw new Exception(
                                "[SQLProject->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteAttachFromProject"); // ok
            prep.setInt(1, idProject);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);

            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper,
     * UrlAttachementWrapper) for the project identified by idProject
     *
     * @param idProject
     *            : id of the project
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAllAttachemnt(int idProject)
        throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getAllAttachemnt] entry data are not valid");
        }
        FileAttachementWrapper[] fileList = getAllAttachFiles(idProject);
        UrlAttachementWrapper[] urlList = getAllAttachUrls(idProject);

        AttachementWrapper[] result = new AttachementWrapper[fileList.length
                                                             + urlList.length];

        for (int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for (int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }

        return result;
    }

    /**
     * Get a Vector of FileAttachementWrapper for the project identified by
     * idProject
     *
     * @param idProject
     *            : id of the project
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAllAttachFiles(int idProject)
        throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getAllAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectAttachFiles"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result
                                                                       .size()];
        for (int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }

    /**
     * Get a Vector of UrlAttachementWrapper for the project identified by
     * idProject
     *
     * @param idProject
     *            : id of the project
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAllAttachUrls(int idProject)
        throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectAttachUrls"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            // pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes
                                          .getString("description_attach"));
            ;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result
                                                                     .size()];
        for (int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }

    /**
     * Get the number of family in the project identified by idProject
     *
     * @param idProject
     * @throws Exception
     */
    @Override
    public int getNumberOfFamily(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getNumberOfFamily] project have no id");
        }
        int result = 0;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectFamilies"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            result++;
        }

        return result;
    }

    /**
     * Get a FamilyWrapper representing a family at order in the project
     * identified by idProject
     *
     * @param idProject
     * @param order
     * @throws Exception
     */
    @Override
    public FamilyWrapper getFamilyByOrder(int idProject, int order)
        throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getFamilyByOrder] project have no id");
        }
        FamilyWrapper pFamily = null;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectFamilyByOrder"); // ok

        prep.setInt(1, idProject);
        prep.setInt(2, order);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        if (stmtRes.next()) {
            pFamily = new FamilyWrapper();
            pFamily.setName(stmtRes.getString("nom_famille"));
            pFamily.setDescription(stmtRes.getString("description_famille"));
            pFamily.setIdBDD(stmtRes.getInt("id_famille"));
            pFamily.setOrder(stmtRes.getInt("ordre_famille"));
            pFamily.setIdProject(idProject);
        }
        return pFamily;
    }

    /**
     * Get a Vector of FamilyWrapper representing all family defined in the
     * project idProject
     *
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public FamilyWrapper[] getFamily(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception("[SQLProject->getFamily] project have no id");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectFamilies"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            FamilyWrapper pFamily = new FamilyWrapper();
            pFamily.setName(stmtRes.getString("nom_famille"));
            pFamily.setDescription(stmtRes.getString("description_famille"));
            pFamily.setIdBDD(stmtRes.getInt("id_famille"));
            pFamily.setOrder(stmtRes.getInt("ordre_famille"));
            result.addElement(pFamily);
        }
        FamilyWrapper[] fwArray = new FamilyWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            fwArray[i] = (FamilyWrapper) result.get(i);
        }
        return fwArray;
    }

    /**
     * Get a ProjectWrapper representing the project identified by name
     *
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public ProjectWrapper getProject(String name) throws Exception {
        ProjectWrapper pProjectWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(0, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectProjectByName"); // ok
            prep.setString(1, name);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()) {
                pProjectWrapper = new ProjectWrapper();
                pProjectWrapper.setIdBDD(stmtRes.getInt("id_projet"));
                pProjectWrapper.setName(stmtRes.getString("nom_projet"));
                pProjectWrapper.setDescription(stmtRes
                                               .getString("description_projet"));
                pProjectWrapper.setCreatedDate(stmtRes
                                               .getDate("date_creation_projet"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pProjectWrapper;
    }

    /**
     * Get a Vector of ProjectWrapper representing all the project in the
     * database
     *
     * @throws Exception
     */
    @Override
    public ProjectWrapper[] getAllProjects() throws Exception {
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectAllProjects"); // OK
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            ProjectWrapper pProjectWrapper = new ProjectWrapper();
            pProjectWrapper.setIdBDD(stmtRes.getInt("id_projet"));
            pProjectWrapper.setName(stmtRes.getString("nom_projet"));
            pProjectWrapper.setDescription(stmtRes
                                           .getString("description_projet"));
            pProjectWrapper.setCreatedDate(stmtRes
                                           .getDate("date_creation_projet"));
            result.add(pProjectWrapper);
        }
        ProjectWrapper[] pwArray = new ProjectWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            pwArray[i] = (ProjectWrapper) result.get(i);
        }
        return pwArray;
    }

    /**
     * Return a Vector of UserWrapper representing all available User in
     * Salome-TMF
     *
     * @return
     * @throws Exception
     */
    @Override
    public UserWrapper[] getAllUser() throws Exception {
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectAllUsers"); // OK
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            try {
                pUserWrapper.setCreateDate(stmtRes
                                           .getDate("date_creation_personne"));
            } catch (Exception e) {
                pUserWrapper.setCreateDate(Util.getCurrentDate());
            }
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            result.add(pUserWrapper);
        }
        UserWrapper[] uwArray = new UserWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            uwArray[i] = (UserWrapper) result.get(i);
        }
        return uwArray;
    }

    /**
     * Get a Vector of UserWrapper representing all the Users in the project
     * projectName
     *
     * @param projectName
     * @throws Exception
     */
    @Override
    public UserWrapper[] getUsersOfProject(String projectName) throws Exception {
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectAllProjectUsers"); // ok
        prep.setString(1, projectName);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            pUserWrapper.setCreateDate(stmtRes
                                       .getDate("date_creation_personne"));
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            result.add(pUserWrapper);
        }
        UserWrapper[] uwArray = new UserWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            uwArray[i] = (UserWrapper) result.get(i);
        }
        return uwArray;
    }

    /**
     * Get a Vector of UserWrapper representing all admins in the project
     * projectName
     *
     * @param idProject
     * @throws Exception
     */
    @Override
    public UserWrapper[] getAdminsOfProject(String projectName)
        throws Exception {
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectAllProjectAdmin"); // ok
        prep.setString(1, projectName);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            pUserWrapper.setCreateDate(stmtRes
                                       .getDate("date_creation_personne"));
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            result.add(pUserWrapper);
        }
        UserWrapper[] uwArray = new UserWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            uwArray[i] = (UserWrapper) result.get(i);
        }
        return uwArray;
    }

    /**
     * Get a Vector of UserWrapper representing all user in groupName in the
     * project projectName
     *
     * @param idProject
     * @throws Exception
     */
    @Override
    public UserWrapper[] getUserOfGroupInProject(String projectName,
                                                 String groupName) throws Exception {
        // A Supprimer afin d'utiliser celle de ISQLGroup
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectGroupUsers"); // ok
        prep.setString(1, groupName);
        prep.setString(2, projectName);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            pUserWrapper.setCreateDate(stmtRes
                                       .getDate("date_creation_personne"));
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            result.add(pUserWrapper);
        }
        UserWrapper[] uwArray = new UserWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            uwArray[i] = (UserWrapper) result.get(i);
        }
        return uwArray;
    }

    /**
     * Get a Vector of ParameterWrapper for the project idProject
     *
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public ParameterWrapper[] getProjectParams(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getProjectParams] project have no id");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectParams"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            ParameterWrapper pParameter = new ParameterWrapper();
            pParameter.setName(stmtRes.getString("nom_param_test"));
            pParameter.setDescription(stmtRes.getString("desc_param_test"));
            pParameter.setIdBDD(stmtRes.getInt("id_param_test"));
            result.addElement(pParameter);
        }
        ParameterWrapper[] pwArray = new ParameterWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            pwArray[i] = (ParameterWrapper) result.get(i);
        }
        return pwArray;
    }

    /**
     * Get a Vector of EnvironmentWrapper representing the environnment of the
     * project idProject
     *
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public EnvironmentWrapper[] getProjectEnvs(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getProjectEnvs] project have no id");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectEnvs"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            EnvironmentWrapper env = new EnvironmentWrapper();
            env.setIdBDD(stmtRes.getInt("id_env"));
            env.setName(stmtRes.getString("nom_env"));
            env.setDescription(stmtRes.getString("description_env"));
            result.addElement(env);
        }
        EnvironmentWrapper[] ewArray = new EnvironmentWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            ewArray[i] = (EnvironmentWrapper) result.get(i);
        }
        return ewArray;
    }

    /**
     * Get a vector of CampaignWrapper representing the campaigns of the project
     * idProject
     *
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public CampaignWrapper[] getPrjectCampaigns(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getPrjectCampaigns] project have no id");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectProjectCampaigns"); // ok by CAMPAIGN
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            CampaignWrapper pCampaign = new CampaignWrapper();
            pCampaign.setName(stmtRes.getString("nom_camp"));
            pCampaign.setIdBDD(stmtRes.getInt("id_camp"));
            pCampaign.setConceptor(SQLObjectFactory.getInstanceOfISQLPersonne()
                                   .getTwoName(stmtRes.getInt("PERSONNE_id_personne")));
            pCampaign.setDescription(stmtRes.getString("description_camp"));
            pCampaign.setDate(stmtRes.getDate("date_creation_camp"));
            result.addElement(pCampaign);
        }
        CampaignWrapper[] cwArray = new CampaignWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            cwArray[i] = (CampaignWrapper) result.get(i);
        }
        return cwArray;
    }

    /**
     * Get a vector of GroupWrapper representing groups in the project idProject
     *
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public GroupWrapper[] getProjectGroups(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLProject->getProjectGroups] project have no id");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectAllProjectGroups"); // ok
        prep.setInt(1, idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            GroupWrapper pGroupWrapper = new GroupWrapper();
            pGroupWrapper.setIdBDD(stmtRes.getInt("id_groupe"));
            pGroupWrapper.setIdProject(stmtRes
                                       .getInt("PROJET_VOICE_TESTING_id_projet"));
            pGroupWrapper.setName(stmtRes.getString("nom_groupe"));
            pGroupWrapper.setDescription(stmtRes.getString("desc_groupe"));
            pGroupWrapper.setPermission(stmtRes.getInt("permission"));
            result.addElement(pGroupWrapper);
        }
        GroupWrapper[] gwArray = new GroupWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            gwArray[i] = (GroupWrapper) result.get(i);
        }
        return gwArray;

    }
}
