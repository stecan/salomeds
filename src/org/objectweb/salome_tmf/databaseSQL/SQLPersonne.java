/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;

public class SQLPersonne implements ISQLPersonne {

    /**
     * Insert a user to the datadase
     *
     * @param login
     * @param name
     * @param firstName
     * @param desc
     * @param email
     * @param tel
     * @param pwd
     * @return
     * @throws Exception
     *             no permission needed
     */
    @Override
    public int insert(String login, String name, String firstName, String desc,
                      String email, String tel, String pwd, boolean crypth)
        throws Exception {
        int id = -1;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_USER);
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addPerson"); // ok
            Date dateActuelle = Util.getCurrentDate();
            Time heureActuelle = Util.getCurrentTime();
            prep.setString(1, login);
            prep.setString(2, name);
            prep.setString(3, firstName);
            prep.setString(4, desc);
            prep.setString(5, email);
            prep.setString(6, tel);
            prep.setDate(7, dateActuelle);
            prep.setTime(8, heureActuelle);
            if (crypth) {
                pwd = org.objectweb.salome_tmf.api.MD5paswd
                    .getEncodedPassword(pwd);
            }
            prep.setString(9, pwd);
            SQLEngine.runAddQuery(prep);
            id = getID(login);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLPersonne->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return id;
    }

    /**
     * Update information about user identified by idUser
     *
     * @param idUser
     * @param newLogin
     * @param newName
     * @param newFirstName
     * @param newDesc
     * @param newEmail
     * @param newTel
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void update(int idUser, String newLogin, String newName,
                       String newFirstName, String newDesc, String newEmail, String newTel)
        throws Exception {
        int transNumber = -1;
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->update] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_USER);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updatePerson"); // ok
            prep.setString(1, newLogin);
            prep.setString(2, newName);
            prep.setString(3, newFirstName);
            prep.setString(4, newDesc);
            prep.setString(5, newEmail);
            prep.setString(6, newTel);
            prep.setInt(7, idUser);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLPersonne->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update the password for User userLogin with newPassword
     *
     * @param userLogin
     * @param newPassword
     * @return the new crypted password
     * @throws Exception
     *             no permission needed
     */
    @Override
    public String updatePassword(String userLogin, String newPassword,
                                 boolean crypth) throws Exception {
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_PASSWORD);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updatePassword"); // ok
            if (crypth) {
                newPassword = org.objectweb.salome_tmf.api.MD5paswd
                    .getEncodedPassword(newPassword);
            }
            prep.setString(1, newPassword);
            prep.setString(2, userLogin);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLPersonne->updatePassword]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return newPassword;
    }

    /**
     * Delete an user in the database the clean all reference about user in
     * project and group if user is the unique admin of an project, the project
     * is deleted
     *
     * @param userLogin
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void deleteByLogin(String userLogin) throws Exception {
        deleteById(getID(userLogin));
        /*
         * int transNumber = -1; try { transNumber =
         * SQLEngine.beginTransaction(ApiConstants.DELETE_USER);
         *
         * int id = getID(userLogin); PreparedStatement prep =
         * SQLEngine.getSQLDeleteQuery("deleteUserByLogin"); //ok
         * prep.setString(1, userLogin); SQLEngine.runDeleteQuery(prep);
         *
         * cleanUserReference(id);
         *
         * SQLEngine.commitTrans(transNumber); } catch (Exception e ){
         * Util.log("[SQLPersonne->delete]" + e); if (Api.isDEBUG()){
         * Util.err(e); } SQLEngine.rollBackTrans(transNumber); throw e; }
         */
    }

    /**
     * Delete an user in the database the clean all reference about user in
     * project and group if user is the unique admin of an project, the project
     * is deleted
     *
     * @param idUser
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void deleteById(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->delete] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_USER);

            cleanUserReference(idUser);

            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteUserByID"); // ok
            prep.setInt(1, idUser);
            SQLEngine.runDeleteQuery(prep);

            /* Suppression des config */
            try {
                if (Api.getLockMeth() == 0) {
                    prep = SQLEngine.getSQLCommonQuery("lockCONFIGWRITE");
                    SQLEngine.runSelectQuery(prep);
                }
                SQLObjectFactory.getInstanceOfISQLConfig().deleteAllUserConf(
                                                                             idUser);
            } catch (Exception e1) {
                Util.err(e1);
                /* WARNING */
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLPersonne->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete user reference in project
     *
     * @param idUser
     * @param projectName
     * @throws Exception
     */
    @Override
    public void deleteInProject(int idUser, String projectName)
        throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->cleanUserReference] entry data are not valid");
        }
        boolean projectDeleted = false;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_USER);
            ProjectWrapper pProjectWrapper = SQLObjectFactory
                .getInstanceOfISQLProject().getProject(projectName);
            int idProjet = pProjectWrapper.getIdBDD();
            if (idProjet < 1) {
                throw new Exception(
                                    "[SQLPersonne->cleanUserReference] entry data are not valid");
            }
            UserWrapper adminProjet = null;
            UserWrapper user2del = getUserById(idUser);
            UserWrapper[] projectAdmins = SQLObjectFactory
                .getInstanceOfISQLProject().getAdminsOfProject(projectName);
            if (projectAdmins.length == 0) {
                SQLObjectFactory.getInstanceOfISQLProject().delete(idProjet,
                                                                   projectName);
                projectDeleted = true;
            } else {
                projectDeleted = true;
                int i = 0;
                while (projectDeleted && i < projectAdmins.length) {
                    adminProjet = projectAdmins[i];
                    if (adminProjet.getIdBDD() != idUser) {
                        projectDeleted = false;
                    }
                    i++;
                }
            }

            GroupWrapper[] userGroupInProject = SQLObjectFactory
                .getInstanceOfISQLGroup().getGroupsForUser(idProjet,
                                                           user2del.getLogin());
            for (int j = 0; j < userGroupInProject.length; j++) {
                GroupWrapper pGroupWrapper = userGroupInProject[j];
                SQLObjectFactory.getInstanceOfISQLGroup().deleteUserInGroup(
                                                                            pGroupWrapper.getIdBDD(), idUser);
            }
            if (!projectDeleted) {
                FamilyWrapper[] projectFamilies = SQLObjectFactory
                    .getInstanceOfISQLProject().getFamily(idProjet);
                for (int k = 0; k < projectFamilies.length; k++) {
                    FamilyWrapper pFamilyWrapper = projectFamilies[k];
                    int idFamily = pFamilyWrapper.getIdBDD();
                    SuiteWrapper[] projectSuites = SQLObjectFactory
                        .getInstanceOfISQLFamily().getTestList(idFamily);
                    for (int l = 0; l < projectSuites.length; l++) {
                        SuiteWrapper pSuiteWrapper = projectSuites[l];
                        int idSuite = pSuiteWrapper.getIdBDD();
                        SQLObjectFactory.getInstanceOfISQLTest().updateUserRef(
                                                                               idSuite, idUser, adminProjet.getIdBDD());
                    }
                }
                CampaignWrapper[] projectCamps = SQLObjectFactory
                    .getInstanceOfISQLProject()
                    .getPrjectCampaigns(idProjet);
                for (int k = 0; k < projectCamps.length; k++) {
                    CampaignWrapper pCampaignWrapper = projectCamps[k];
                    int idCamp = pCampaignWrapper.getIdBDD();
                    SQLObjectFactory.getInstanceOfISQLCampaign().updateUserRef(
                                                                               idCamp, idUser, adminProjet.getIdBDD());
                    SQLObjectFactory.getInstanceOfISQLCampaign()
                        .updateTestAssignationRef(idCamp, idUser,
                                                  adminProjet.getIdBDD());
                    SQLObjectFactory.getInstanceOfISQLExecution()
                        .updateUserRef(idCamp, idUser,
                                       adminProjet.getIdBDD());
                    ExecutionWrapper[] campagneExec = SQLObjectFactory
                        .getInstanceOfISQLCampaign().getExecutions(idCamp);
                    for (int l = 0; l < campagneExec.length; l++) {
                        ExecutionWrapper pExecutionWrapper = campagneExec[l];
                        int idExec = pExecutionWrapper.getIdBDD();
                        SQLObjectFactory.getInstanceOfISQLExecutionResult()
                            .updateUserRef(idExec, idUser,
                                           adminProjet.getIdBDD());
                    }

                }
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLPersonne->deleteInProject]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    void cleanUserReference(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->cleanUserReference] entry data are not valid");
        }
        ProjectWrapper[] allProject = SQLObjectFactory
            .getInstanceOfISQLProject().getAllProjects();
        boolean projectDeleted = false;
        UserWrapper user2del = getUserById(idUser);
        UserWrapper adminProjet = null;
        for (int i = 0; i < allProject.length; i++) {
            ProjectWrapper pProjectWrapper = allProject[i];
            int idProjet = pProjectWrapper.getIdBDD();
            String projName = pProjectWrapper.getName();
            UserWrapper[] projectAdmins = SQLObjectFactory
                .getInstanceOfISQLProject().getAdminsOfProject(projName);
            if (projectAdmins.length == 0) {
                SQLObjectFactory.getInstanceOfISQLProject().delete(idProjet,
                                                                   projName);
                projectDeleted = true;
            } else {
                adminProjet = projectAdmins[0];
                projectDeleted = false;
            }
            GroupWrapper[] userGroupInProject = SQLObjectFactory
                .getInstanceOfISQLGroup().getGroupsForUser(idProjet,
                                                           user2del.getLogin());
            for (int j = 0; j < userGroupInProject.length; j++) {
                GroupWrapper pGroupWrapper = userGroupInProject[j];
                SQLObjectFactory.getInstanceOfISQLGroup().deleteUserInGroup(
                                                                            pGroupWrapper.getIdBDD(), idUser);
            }
            if (!projectDeleted) {
                FamilyWrapper[] projectFamilies = SQLObjectFactory
                    .getInstanceOfISQLProject().getFamily(idProjet);
                for (int k = 0; k < projectFamilies.length; k++) {
                    FamilyWrapper pFamilyWrapper = projectFamilies[k];
                    int idFamily = pFamilyWrapper.getIdBDD();
                    SuiteWrapper[] projectSuites = SQLObjectFactory
                        .getInstanceOfISQLFamily().getTestList(idFamily);
                    for (int l = 0; l < projectSuites.length; l++) {
                        SuiteWrapper pSuiteWrapper = projectSuites[l];
                        int idSuite = pSuiteWrapper.getIdBDD();
                        SQLObjectFactory.getInstanceOfISQLTest().updateUserRef(
                                                                               idSuite, idUser, adminProjet.getIdBDD());
                    }
                }
                CampaignWrapper[] projectCamps = SQLObjectFactory
                    .getInstanceOfISQLProject()
                    .getPrjectCampaigns(idProjet);
                for (int k = 0; k < projectCamps.length; k++) {
                    CampaignWrapper pCampaignWrapper = projectCamps[k];
                    int idCamp = pCampaignWrapper.getIdBDD();
                    SQLObjectFactory.getInstanceOfISQLCampaign().updateUserRef(
                                                                               idCamp, idUser, adminProjet.getIdBDD());
                    SQLObjectFactory.getInstanceOfISQLCampaign()
                        .updateTestAssignationRef(idCamp, idUser,
                                                  adminProjet.getIdBDD());
                    SQLObjectFactory.getInstanceOfISQLExecution()
                        .updateUserRef(idCamp, idUser,
                                       adminProjet.getIdBDD());
                    ExecutionWrapper[] campagneExec = SQLObjectFactory
                        .getInstanceOfISQLCampaign().getExecutions(idCamp);
                    for (int l = 0; l < campagneExec.length; l++) {
                        ExecutionWrapper pExecutionWrapper = campagneExec[l];
                        int idExec = pExecutionWrapper.getIdBDD();
                        SQLObjectFactory.getInstanceOfISQLExecutionResult()
                            .updateUserRef(idExec, idUser,
                                           adminProjet.getIdBDD());
                    }
                }
            }
        }
    }

    /**
     * Get the permission of an user in a projet idProject
     *
     * @param idProject
     * @param userLogin
     * @return
     * @throws Exception
     */
    @Override
    public int getPermissionOfUser(int idProject, String userLogin)
        throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLPersonne->getPermissionOfUser] entry data are not valid");
        }
        GroupWrapper[] groupe_List = SQLObjectFactory.getInstanceOfISQLGroup()
            .getGroupsForUser(idProject, userLogin);
        int res = -1;
        if ((groupe_List.length != 0)) {
            res = 0;
            for (int i = 0; i < groupe_List.length; i++) {
                GroupWrapper pGroupWrapper = groupe_List[i];
                res |= pGroupWrapper.getPermission();
            }
        }
        return res;
    }

    /**
     * Get The Id of a person using login
     *
     * @param login
     * @return
     * @throws Exception
     */
    @Override
    public int getID(String login) throws Exception {
        int idPerson = -1;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectPersByLogin"); // ok
        prep.setString(1, login);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        if (stmtRes.next()) {
            idPerson = stmtRes.getInt("id_personne");
        }
        return idPerson;
    }

    /**
     * Get the login of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    @Override
    public String getLogin(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->getLogin] entry data are not valid");
        }
        return getUserById(idUser).getLogin();
    }

    /**
     * Get the name of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    @Override
    public String getName(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->getName] entry data are not valid");
        }
        return getUserById(idUser).getName();
    }

    /**
     * Get the last and the fist name of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    @Override
    public String getTwoName(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->getTwoName] entry data are not valid");
        }
        UserWrapper pUserWrapper = getUserById(idUser);
        return pUserWrapper.getName() + " " + pUserWrapper.getPrenom();
    }

    /**
     * Get an UserWrapper of the user identified by login
     *
     * @param login
     * @return
     * @throws Exception
     */
    @Override
    public UserWrapper getUserByLogin(String login) throws Exception {
        UserWrapper pUserWrapper = null;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectPersByLogin"); // ok
        prep.setString(1, login);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {
            pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            try {
                pUserWrapper.setCreateDate(stmtRes
                                           .getDate("date_creation_personne"));
            } catch (Exception e) {
                pUserWrapper.setCreateDate(Util.getCurrentDate());
            }
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
        }
        return pUserWrapper;
    }

    /**
     * Get an UserWrapper of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    @Override
    public UserWrapper getUserById(int idUser) throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLPersonne->getUser] entry data are not valid");
        }
        UserWrapper pUserWrapper = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectPersByID"); // ok
        prep.setInt(1, idUser);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {
            pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            try {
                pUserWrapper.setCreateDate(stmtRes
                                           .getDate("date_creation_personne"));
            } catch (Exception e) {
                pUserWrapper.setCreateDate(Util.getCurrentDate());
            }
            pUserWrapper.setCreateTime(stmtRes.getTime(
                                                       "heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
        }
        return pUserWrapper;
    }

    @Override
    public int addAttachUrl(int idUser, String url, String description)
        throws Exception {
        if (idUser < 1 || url == null) {
            throw new Exception(
                                "[SQLProject->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_ATTACHMENT);

            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment()
                .insert(url, description);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("addAttachToPerson"); // ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idUser);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }

    @Override
    public void deleteAttach(int idUser, int idAttach) throws Exception {
        if (idUser < 1 || idAttach < 1) {
            throw new Exception(
                                "[SQLProject->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteAttachFromUser"); // ok
            prep.setInt(1, idUser);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);

            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLProject->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }

    }

    @Override
    public UrlAttachementWrapper[] getAllAttachUrls(int idUser)
        throws Exception {
        if (idUser < 1) {
            throw new Exception(
                                "[SQLProject->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectPersAttachUrls"); // ok
        prep.setInt(1, idUser);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes
                                          .getString("description_attach"));
            ;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result
                                                                     .size()];
        for (int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }
}
