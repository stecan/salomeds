package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Hashtable;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;

public class SQLConfig implements ISQLConfig {
        
    @Override
    public void insertSalomeConf(String key, String value) throws Exception {
        insertConf(key, value, 0, 0);
    }
        
    @Override
    public void insertUserConf(String key, String value, int idUser) throws Exception {
        insertConf(key, value, 0, idUser);
    }
        
    @Override
    public void insertProjectConf(String key, String value, int idProject) throws Exception {
        insertConf(key, value, idProject, 0);
    }
        
    synchronized void insertConf(String key, String value,  int idProject,  int idUser) throws Exception { 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("insertConfig"); //ok
            prep.setString(1,key);
            prep.setString(2,value);
            prep.setInt(3,idProject);
            prep.setInt(4,idUser);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public void updateSalomeConf(String key, String value) throws Exception {
        updateConf(key, value, 0, 0);
    }
        
    @Override
    public void updateUserConf(String key, String value, int idUser) throws Exception {
        updateConf(key, value, 0, idUser);
    }
        
    @Override
    public void updateProjectConf(String key, String value, int idProject) throws Exception {
        updateConf(key, value, idProject, 0);
    }
        
    synchronized void updateConf(String key, String value,  int idProject,  int idUser) throws Exception { 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("updateConfig"); //ok
            prep.setString(1,value);
            prep.setInt(2,idProject);
            prep.setInt(3,idUser);
            prep.setString(4,key);
            prep.setInt(5,idProject);
            prep.setInt(6,idUser);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public void deleteSalomeConf(String key) throws Exception {
        deleteConf(key, 0, 0);
    }
        
    @Override
    public void deleteUserConf(String key, int idUser) throws Exception {
        deleteConf(key, 0, idUser);
    }
        
    @Override
    public void deleteProjectConf(String key, int idProject) throws Exception  {
        deleteConf(key, idProject, 0);
    }
        
    synchronized void deleteConf(String key, int idProject,  int idUser) throws Exception { 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("deleteConfig"); //ok
            prep.setString(1,key);
            prep.setInt(2,idProject);
            prep.setInt(3,idUser);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public void deleteAllUserConf(int idUser) throws Exception {
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("deleteAllUserConfig"); //ok
            prep.setInt(1,idUser);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public void deleteAllProjectConf(int idProject) throws Exception {
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("deleteAllProjectConfig"); //ok
            prep.setInt(1,idProject);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public String getSalomeConf(String key) throws Exception {
        return getOneConfig(key, 0, 0);
    }
        
    @Override
    public String getUserConf(String key, int idUser) throws Exception {
        return getOneConfig(key, 0, idUser);
    }
        
    @Override
    public String getProjectConf(String key, int idProject) throws Exception {
        return getOneConfig(key, idProject, 0);
    }
        
    public synchronized String getOneConfig(String key,  int idProject, int idUser) throws Exception {
        String res = null;
        PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectOneConfig"); //ok
        prep.setString(1,key);
        prep.setInt(2,idProject);
        prep.setInt(3,idUser);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {  
            res = stmtRes.getString("VALEUR");
        }
        return res;
    }
        
    @Override
    public Hashtable getAllSalomeConf() throws Exception {
        return getOneConfig(0, 0);
    }
        
    @Override
    public Hashtable getAllUserConf(int idUser) throws Exception {
        return getOneConfig(0, idUser);
    }
        
    @Override
    public Hashtable getAllProjectConf(int idProject) throws Exception {
        return getOneConfig(idProject, 0);
    }
        
    public synchronized Hashtable getOneConfig(int idProject, int idUser) throws Exception {
        Hashtable res = new Hashtable();
        PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectAllConfig"); //ok
        prep.setInt(1,idProject);
        prep.setInt(2,idUser);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {  
            String key = stmtRes.getString("CLE");
            String value = stmtRes.getString("VALEUR");
            res.put(key, value);
        }
        return res;
    }
}
