/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;

public class SQLGroup implements ISQLGroup {
        
    /**
     * Insert a group for the project idProject
     * @param idProject
     * @param name of the group
     * @param description of the group
     * @param perm of the group in the project
     * @return the id of the group
     * @exception
     * no permission needed
     */
    @Override
    public int insert(int idProject, String name, String description, int perm) throws Exception {
        int id = -1;
        int transNumber = -1; 
        if (idProject <1) {
            throw new Exception("[SQLGroup->insert] Project have no id");
        } 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_GROUP);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addGroup"); //ok
            prep.setInt(1, idProject);
            prep.setString(2, name);
            prep.setString(3, description);
            prep.setInt(4, perm);
            SQLEngine.runAddQuery(prep);
                        
            id = getID(idProject, name);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
        return id;
    }
        
    /**
     * Insert an user idUser in the group idGroup
     * @param
     * @param
     * @exception
     * no permission needed
     */
    @Override
    public void insertUser(int idGroup, int idUser ) throws Exception {
        int transNumber = -1; 
        if (idUser <1) {
            throw new Exception("[SQLGroup->insertUser] user have no id");
        } 
        if (idGroup <1) {
            throw new Exception("[SQLGroup->insertUser] group have no id");
        } 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_USER_INTO_GROUP);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUserToGroup"); //ok
            prep.setInt(1,idUser);
            prep.setInt(2,idGroup);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.err("[SQLGroup->insertUser]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
                
    }
        
    /**
     * delete the group idGroup and all reference about user in group
     * @param idGroup
     * @exception
     * no permission needed
     */
    @Override
    public void delete(int idGroup) throws Exception {
        if (idGroup <1) {
            throw new Exception("[SQLGroup->delete] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_GROUP);
                        
            deleteUserGroup(idGroup);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteGroup"); //ok
            prep.setInt(1, idGroup);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * update the permission of the group idGroup by perm
     * @param idGroup
     * @param perm
     * @exception
     * no permission needed
     */
    @Override
    public void updatePermission(int idGroup, int perm) throws Exception {
        if (idGroup <1) {
            throw new Exception("[SQLGroup->updatePermission] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_PERMISSION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateAllowGroupByID"); //ok
            prep.setInt(1,perm);
            prep.setInt(2,idGroup);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->updatePermission]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * update the name and the description of the group idGroup
     * @param idGroup
     * @param name
     * @param description
     * @exception
     * no permission needed
     */
    @Override
    public void updateGroup(int idGroup, String name, String description) throws Exception {
        if (idGroup <1) {
            throw new Exception("[SQLGroup->updateGroup] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_GROUP);
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateGroupByID"); //ok       
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3, idGroup);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->updateGroup]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Update the description of idUser in the group idGroup
     * @param idGroup
     * @param idUser
     * @param description
     * @exception
     * no permission needed
     */
    @Override
    public void updateUserDescInGroup(int idGroup, int idUser, String description ) throws Exception  {
        if (idGroup <1 || idUser < 1) {
            throw new Exception("[SQLGroup->updateUserDescInGroup] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_GROUP);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateUserDescInGroupe"); //ok
            prep.setString(1,description);
            prep.setInt(2,idUser);
            prep.setInt(3,idGroup);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.err("[SQLGroup->updateUserDescInGroup]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    void updatePermissionForGroup(int idProject, String groupName, int permission ) throws Exception {
        if (idProject <1) {
            throw new Exception("[SQLGroup->updatePermissionForGroup] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_PERMISSION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateAllowGroup2"); //ok
            prep.setInt(1,permission);
            prep.setInt(2,idProject);
            prep.setString(3,groupName);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->updatePermissionForGroup]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Delete idUser in the group idGroup
     * @param idGroup
     * @param idUser
     * @exception
     * no permission needed
     */
    @Override
    public void deleteUserInGroup(int idGroup, int idUser) throws Exception {
        if (idGroup <1 || idUser < 1) {
            throw new Exception("[SQLGroup->deleteUserInGroup] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_USER_FROM_GROUP);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteUserFromGroup"); //ok
            prep.setInt(1, idUser);
            prep.setInt(2, idGroup);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->deleteUserInGroup]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Delete all reference of group in user - group mapping
     * @param idGroup
     * @throws Exception
     */
    @Override
    public void deleteUserGroup(int idGroup) throws Exception {
        if (idGroup <1 ) {
            throw new Exception("[SQLGroup->deleteUserGroup] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_USER_FROM_GROUP);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteGroupUsers"); //ok
            prep.setInt(1, idGroup);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLGroup->deleteUserGroup]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * get the id of an group  groupName in project idProject 
     * @param idProject
     * @param groupName
     * @exception
     * no permission needed
     */
    @Override
    public int getID(int idProject, String groupName)  throws Exception {
        if (idProject <1 ) {
            throw new Exception("[SQLGroup->getID] entry data are not valid");
        }
        int idGroup = -1;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdGroup"); //ok
        prep.setInt(1,idProject);
        prep.setString(2,groupName);
        ResultSet DS = SQLEngine.runSelectQuery(prep);
                
        if (DS.next()) {
            idGroup = DS.getInt("id_groupe");
        }
        return idGroup;
    }
        
    /**
     * Get an vector of GroupWrapper representing all groups in project idProject 
     * @param idProject
     * @exception
     * no permission needed
     */
    @Override
    public GroupWrapper[] getGroupWrapperInProject(int idProject) throws Exception {
        if (idProject <1 ) {
            throw new Exception("[SQLGroup->getGroupWrapperInProject] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectGroupInfo"); //ok
        prep.setInt(1,idProject);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while (stmtRes.next()) {
            GroupWrapper pGroupWrapper = new GroupWrapper();
            pGroupWrapper.setIdProject(idProject);
            pGroupWrapper.setIdBDD(stmtRes.getInt("id_groupe"));
            pGroupWrapper.setName(stmtRes.getString("nom_groupe"));
            pGroupWrapper.setDescription(stmtRes.getString("desc_groupe"));
            pGroupWrapper.setPermission(stmtRes.getInt("permission"));
            result.add(pGroupWrapper);
        }
        GroupWrapper[] gwArray = new GroupWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            gwArray[i] = (GroupWrapper) result.get(i);
        }
        return gwArray;
    }
        
    /**
     * Get an vector of GroupWrapper representing all groups where  userLogin is 
     * @param idProject
     * @param userLogin
     * @exception
     * no permission needed
     */
    @Override
    public GroupWrapper[] getGroupsForUser(int idProject, String userLogin) throws Exception {
        if (idProject <1 ) {
            throw new Exception("[SQLGroup->getGroupsForUser] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectGroupsForUser"); // ok
        prep.setInt(1,idProject);
        prep.setString(2,userLogin);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while (stmtRes.next()) {
            GroupWrapper pGroupWrapper = new GroupWrapper();
            pGroupWrapper.setIdProject(idProject);
            pGroupWrapper.setIdBDD(stmtRes.getInt("id_groupe"));
            pGroupWrapper.setName(stmtRes.getString("nom_groupe"));
            pGroupWrapper.setDescription(stmtRes.getString("desc_groupe"));
            pGroupWrapper.setPermission(stmtRes.getInt("permission"));
            result.add(pGroupWrapper);
        }
        GroupWrapper[] gwArray = new GroupWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            gwArray[i] = (GroupWrapper) result.get(i);
        }
        return gwArray;
    } 
        
    /**
     * Get an vector of UserWrapper representing all users in group idGroup
     * @param idGroup
     * @exception
     * no permission needed
     */
    @Override
    public UserWrapper[] getUserWrappersInGroup(int idGroup) throws Exception {
        if (idGroup <1 ) {
            throw new Exception("[SQLGroup->getUserWrappersInGroup] entry data are not valid");
        } 
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectUserGroupeInfo"); //ok
        prep.setInt(1,idGroup);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("PERSONNE_id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            pUserWrapper.setCreateDate(stmtRes.getDate("date_creation_personne"));
            pUserWrapper.setCreateTime(stmtRes.getTime("heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            result.add(pUserWrapper);
        }
        UserWrapper[] uwArray = new UserWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uwArray[i] = (UserWrapper) result.get(i);
        }
        return uwArray;
    }
}
