package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLDataset;


public class SQLDataset implements ISQLDataset{

    /**
     * Insert a dataset (table JEU_DONNEES) for the campaign identified by idCamp
     * @param idCamp : id of the campaign wich will caontain the dataset
     * @param name of the dataset
     * @param description of the dataset
     * @return id of the dataset created in table JEU_DONNEES
     * need permission canExecutCamp or canCreateCamp
     * @throws Exception
     */
    @Override
    public int insert(int idCamp, String name, String description) throws Exception {
        int transNumber = -1;
        int idDataset = -1;
        if (idCamp <1 ) {
            throw new Exception("[SQLDataset->insert] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateCamp() || !Permission.canExecutCamp()){
                throw new SecurityException("[SQLDataset : insert -> canCreateCamp or canExecutCamp]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLCampaign().getCampaign(idCamp) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.INSERT_DATA_SET);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addJeuDonneesToCamp"); //ok
            prep.setInt(1, idCamp);
            prep.setString(2, name);
            prep.setString(3, description);
            SQLEngine.runAddQuery(prep);
                        
            idDataset = getID(idCamp, name);
            if (idDataset <1 ) {
                throw new Exception("[SQLDataset->insert] id are not valid");
            } 
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLDataset->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idDataset;
    }
        
    /**
     * Map a value for the parameter idParam in the table VALEUR_PARAM
     * @param idDataset : id of the dataset in the table JEU_DONNEES
     * @param idParam : id of the parameter in the table PARAM_TEST
     * @param value 
     * @throws Exception
     * need permission canExecutCamp or canCreateCamp
     */
    @Override
    public void addParamValue(int idDataset, int idParam, String value) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canCreateCamp())){
                throw new SecurityException("[SQLDataset : addParamValue -> canExecutCamp]");
            }
        }
        if (idDataset <1 ) {
            throw new Exception("[SQLDataset->addParamValue] entry data are not valid");
        } 
        if (SQLObjectFactory.getInstanceOfISQLParameter().getParameterWrapper(idParam) == null){
            throw new DataUpToDateException();
        }
        if (getWrapper(idDataset) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.UPDATE_DATA_SET);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addParamValueToJeuDonneesUsingID"); //ok
            prep.setInt(1,idDataset);
            prep.setInt(2,idParam);
            prep.setString(3,value);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLDataset->addParamValue]", e);
            throw e;
        }
    }
         
    /**
     * Update the name and the description of the dataset idDataset
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @param name
     * @param description
     * need permission canExecutCamp or canUpdateCamp
     * @throws Exception
     */
    @Override
    public void update(int idDataset, String name, String description) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canUpdateCamp())){
                throw new SecurityException("[SQLDataset : update -> canExecutCamp]");
            }
        }
        if (idDataset <1 ) {
            throw new Exception("[SQLDataset->update] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_DATA_SET);
                   
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateJeuDonneeUsingID"); //ok
            prep.setString(1, name);
            prep.setString(2,description);
            prep.setInt(3, idDataset);
            SQLEngine.runUpdateQuery(prep);
                
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLDataset->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update a value maped to the parameter idParam for the dataset idDataset
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @param idParam
     * @param value
     * @param description
     * @throws Exception
     * need permission canExecutCamp or canUpdateCamp
     */
    @Override
    public void updateParamValue(int idDataset, int idParam, String value, String description) throws Exception {
        int transNumber = -1;
        if (idDataset <1 || idParam < 1 ) {
            throw new Exception("[SQLDataset->updateParamValue] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp() || Permission.canExecutCamp())){
                throw new SecurityException("[SQLDataset : update -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.UPDATE_DATA_SET);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateParamValueToJeuDonnees"); //ok
            prep.setString(1, value);
            prep.setString(2, description);
            prep.setInt(3, idDataset);
            prep.setInt(4, idParam);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLDataset->updateParamValue]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete the dataset and all mapped values in the database
     * if the dataset is used by executions, the executions are deleted
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @throws Exception
     * @see ISQLExecution.delete(int)
     * need permission canExecutCamp or canDeleteCamp
     */
    @Override
    public void delete(int idDataset) throws Exception {
        int transNumber = -1;
        if (idDataset <1 ) {
            throw new Exception("[SQLDataset->delete] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canDeleteCamp())){
                throw new SecurityException("[SQLDataset : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_DATA_SET);
            /* Delete execution wich using the dataset */
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionFromJeuDeDonnee"); //OK
            prep.setInt(1,idDataset);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while  (stmtRes.next()) {
                int idExec = stmtRes.getInt("id_exec_camp");
                SQLObjectFactory.getInstanceOfISQLExecution().delete(idExec);
            }
            /* Delete associated value */
            prep = SQLEngine.getSQLDeleteQuery("deleteJeuDonneesValue"); //ok
            prep.setInt(1, idDataset);
            SQLEngine.runDeleteQuery(prep);
           
            /* Delete dataset reference*/
            prep = SQLEngine.getSQLDeleteQuery("deleteJeuDonneesUsingID"); //ok
            prep.setInt(1, idDataset);
            SQLEngine.runDeleteQuery(prep);
                
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLDataset->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get the Id of the dataset identified by name for the campaign idCamp
     * @param idCamp
     * @param name
     * @return id of the dataset created in table JEU_DONNEES
     * @throws Exception
     */
    @Override
    public int getID(int idCamp, String name) throws Exception {
        if (idCamp <1 ) {
            throw new Exception("[SQLDataset->getID] entry data are not valid");
        }
        int idDataset = -1;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdJeuDonnees"); //ok
            prep.setInt(1,idCamp);
            prep.setString(2,name);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()) {
                idDataset = stmtRes.getInt("id_jeu_donnees");
            } 
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idDataset;
    }
        
    /**
     * Get A DataSetWrapper representing the dataset idDataSet in database
     * @param idDataSet
     * @return
     * @throws Exception
     */
    @Override
    public DataSetWrapper getWrapper(int idDataSet) throws Exception {
        if (idDataSet <1 ) {
            throw new Exception("[SQLDataset->getWrapper] entry data are not valid");
        }
        DataSetWrapper pDataSet = null;
                
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectJeuDonneesByID"); //ok
            prep.setInt(1,idDataSet);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()) {
                pDataSet = new DataSetWrapper();
                pDataSet.setDescription(stmtRes.getString("desc_jeu_donnees"));
                pDataSet.setName(stmtRes.getString("nom_jeu_donnees"));
                pDataSet.setIdBDD(stmtRes.getInt("id_jeu_donnees"));
            } 
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pDataSet;
    }
        
    /**
     * Get a Vector of ValuedParameterWrapper for the he dataset idDataSet
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public ValuedParameterWrapper[] getDefinedParameters(int idDataSet)  throws Exception {
        if (idDataSet <1 ) {
            throw new Exception("[SQLDataset->getDefinedParameters] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectParamsInDataSet"); //ok
        prep.setInt(1, idDataSet);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            ValuedParameterWrapper pValuedParameterWrapper = new ValuedParameterWrapper();
            ParameterWrapper pParameter = new ParameterWrapper();
            pParameter.setName(stmtRes.getString("nom_param_test"));
            pParameter.setDescription(stmtRes.getString("desc_param_test"));
            pParameter.setIdBDD(stmtRes.getInt("id_param_test")); 
            pValuedParameterWrapper.setParameterWrapper(pParameter);
            pValuedParameterWrapper.setIdBDD(stmtRes.getInt("id_valeur"));
            pValuedParameterWrapper.setDescription(stmtRes.getString("desc_valeur"));
            pValuedParameterWrapper.setValue(stmtRes.getString("valeur"));
            result.add(pValuedParameterWrapper);
        }
        ValuedParameterWrapper[] vpwArray = new ValuedParameterWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            vpwArray[i] = (ValuedParameterWrapper) result.get(i);
        }
        return vpwArray;
    }
        
}
