/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.ExecutionResultTestWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionTestResult;

public class SQLExecutionTestResult implements ISQLExecutionTestResult {
        
    /**
     * Insert an execution test result (table EXEC_CAS) for the execution result idExecRes and test idTest
     * @param idExecRes
     * @param idTest
     * @param result ('PASSED', 'FAILED', 'INCONCLUSIF'  @see ApiConstants)     
     * @return the id of the execution test result in the table EXEC_CAS
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public int insert(int idExecRes, int idTest, String result) throws Exception {
        if (idExecRes <1 || idTest < 1 ) {
            throw new Exception("[SQLExecutionTestResult->insert] entry data are not valid");
        } 
        int idRestestExec = -1;
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : insert -> canExecutCamp]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLExecutionResult().getWrapper(idExecRes) == null){
            throw new DataUpToDateException();
        }
        if (SQLObjectFactory.getInstanceOfISQLTest().getTest(idTest)== null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.INSERT_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addResExecCas"); //ok
            prep.setInt(1, idExecRes);
            prep.setInt(2, idTest);
            prep.setString(3, result);
            SQLEngine.runAddQuery(prep);
                        
            idRestestExec = getID(idExecRes, idTest);   
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idRestestExec;
    }
        
    /**
     * Attach an url to the execution test result  identified by idExecRes and idTest(table EXEC_CAS_ATTACH)
     * @param idExecRes
     * @param idTest
     * @param url
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idExecRes, int idTest, String url, String description) throws Exception {
        int idAttach = -1;
        int idRestestExec = getID(idExecRes, idTest);
        if (idRestestExec != -1){
            idAttach = addAttachUrl(idRestestExec, url, description);
        }
        return idAttach;
    }
        
    /**
     * Attach an url to the execution test result identified by idRestestExec (table EXEC_CAS_ATTACH)
     * @param idRestestExec
     * @param url
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idRestestExec, String url, String description) throws Exception {
        if (idRestestExec <1 || url == null ) {
            throw new Exception("[SQLExecutionTestResult->addAttachUrl] entry data are not valid");
        } 
        int transNumber = -1;
        int idAttach = -1;
        if (getWrapper(idRestestExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToExecTestResult"); //ok
            prep.setInt(1,idRestestExec);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
                
        return idAttach;
    }
        
    /**
     * Attach a file to the execution test result identified by idExecRes and idTest (table EXEC_CAS_ATTACH)
     * @param idExecRes
     * @param idTest
     * @param file
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idExecRes, int idTest, SalomeFileWrapper file, String description) throws Exception {
        int idAttach = -1;
        int idRestestExec = getID(idExecRes, idTest);
        if (idRestestExec != -1){
            idAttach = addAttachFile(idRestestExec, file, description);
        }
        return idAttach;
    }
        
    /**
     * Attach a file to the execution test result identified by idRestestExec (table EXEC_CAS_ATTACH)
     * @param idRestestExec
     * @param file
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idRestestExec, SalomeFileWrapper file, String description) throws Exception {
        if (idRestestExec <1 || file == null ) {
            throw new Exception("[SQLExecutionTestResult->addAttachFile] entry data are not valid " +idRestestExec + ", " + file);
        } 
        int transNumber = -1;
        int idAttach = -1;
        if (getWrapper(idRestestExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToExecTestResult"); //ok
            prep.setInt(1,idRestestExec);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }
        
        
    /**
     * Update the result of an execution test result identified by idExecRes and idTest 
     * @param idExecRes
     * @param idTest
     * @param result
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idExecRes, int idTest, String result) throws Exception {
        int idRestestExec = getID(idExecRes, idTest);
        if (idRestestExec != -1){
            update(idRestestExec, result);
        }
    }
        
    /**
     * Update the result of an execution test result identified by idRestestExec
     * @param idRestestExec
     * @param result
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idRestestExec, String result) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->update] entry data are not valid");
        } 
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : insert -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateTestStatusForExecResult2"); //ok
            prep.setString(1, result);
            prep.setInt(2, idRestestExec);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Delete the referenced execution test result identified by idExecRes and idTest
     * @param idResExec
     * @param idTest
     * @throws Exception
     * @see delete(int)
     * need permission canExecutCamp
     */
    @Override
    public void delete(int idResExec, int idTest) throws Exception {
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : delete -> canExecutCamp]");
            }
        }
        int idRestestExec = getID(idResExec, idTest);
        if (idRestestExec != -1){
            delete(idRestestExec);
        }
    }
        
    /**
     * Delete all referenced execution test result for the execution result idExecRes
     * @param idExecRes
     * @throws Exception
     * @see delete(int)
     * need permission canExecutCamp
     */
    @Override
    public void deleteAllFrom(int idExecRes) throws Exception {
        if (idExecRes <1 ) {
            throw new Exception("[SQLExecutionTestResult->deleteAllFrom] entry data are not valid");
        } 
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : deleteAllFrom -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecCampTests");  //ok
            prep.setInt(1,idExecRes);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            while (stmtRes.next()){
                int idRestestExec       = stmtRes.getInt("id_exec_cas");
                delete  (idRestestExec);
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->deleteAllFrom]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
                
    }
        
    /**
     * Delete the execution test result identified by idRestestExec
     * Then delete all the attachements and Action results.
     * @param idRestestExec
     * @throws Exception
     * @see ISQLExecutionActionResult.deleteAll(int)
     * need permission canExecutCamp
     */
    @Override
    public void delete(int idRestestExec) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->delete] entry data are not valid");
        } 
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_TEST_RESULT);
                        
            //Delete Action results
            SQLObjectFactory.getInstanceOfISQLExecutionActionResult().deleteAll(idRestestExec); 
                        
            //DeleteAttachs
            deleteAllAttach(idRestestExec);
                        
            PreparedStatement prep  = SQLEngine.getSQLDeleteQuery("deleteResExecTest"); //ok
            prep.setInt(1,idRestestExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all attchements of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idRestestExec) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->deleteAllAttach] entry data are not valid");
        } 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);
                        
            AttachementWrapper[] attachList = getAttachs(idRestestExec);
            for (int i =0; i < attachList.length; i++){
                AttachementWrapper  pAttachementWrapper = attachList[i];
                deleteAttach(idRestestExec, pAttachementWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete an attchement idAttach of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @param idAttach
     * @throws Exception
     */
    @Override
    public void deleteAttach(int idResExec, int idTest, int  idAttach) throws Exception {
        if (idResExec <1 || idTest < 1 || idAttach < 1) {
            throw new Exception("[SQLExecutionTestResult->deleteAttach] entry data are not valid");
        } 
        int idRestestExec = getID(idResExec, idTest);
        if (idRestestExec != -1){
            deleteAttach(idRestestExec, idAttach);
        }
    }
        
    /**
     * Delete an attchement idAttach of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @param idAttach
     * @throws Exception
     */
    @Override
    public void deleteAttach(int idRestestExec, int  idAttach) throws Exception {
        if (idRestestExec <1  || idAttach < 1) {
            throw new Exception("[SQLExecutionTestResult->deleteAttach] entry data are not valid");
        } 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);
                        
            PreparedStatement prep  = SQLEngine.getSQLDeleteQuery("deleteAttachFromExecTestResult"); //ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idRestestExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionTestResult->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get a vector of FileAttachementWrapper representing the files attachment
     * execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idResExec, int idTest) throws Exception {
        if (idResExec <1  || idTest < 1) {
            throw new Exception("[SQLExecutionTestResult->getAttachFiles] entry data are not valid");
        } 
        int idRestestExec = getID(idResExec, idTest);
        return getAttachFiles(idRestestExec);
    }
        
    /**
     * Get a vector of FileAttachementWrapper representing the files attachment 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idRestestExec) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->getAttachFiles] entry data are not valid");
        } 
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectAllExecTestResultAttchFile"); //ok
        prep.setInt(1,idRestestExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
                
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     * Get a vector of UrlAttachementWrapper representing the Urls attachment 
     * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idResExec, int idTest) throws Exception {
        if (idResExec <1  || idTest < 1) {
            throw new Exception("[SQLExecutionTestResult->getAttachUrls] entry data are not valid");
        } 
        int idRestestExec = getID(idResExec, idTest);
        return getAttachUrls(idRestestExec);
    }
        
    /**
     * Get a vector of UrlAttachementWrapper representing the Urls attachment 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idRestestExec) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->getAttachUrls] entry data are not valid");
        }  
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectAllExecTestResultAttchURL"); //ok
        prep.setInt(1,idRestestExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
                
        return uawArray;
    }
        
    /**
     * Get a vector of all attachments (AttachementWrapper, File or Url) 
     * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAttachs(int idResExec, int idTest) throws Exception {
        if (idResExec <1  || idTest < 1) {
            throw new Exception("[SQLExecutionTestResult->getAttachs] entry data are not valid");
        } 
        int idRestestExec = getID(idResExec, idTest);
        return getAttachs(idRestestExec);
    }
        
    /**
     * Get a vector of all attachments (AttachementWrapper, File or Url) 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAttachs(int idRestestExec) throws Exception {
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->getAttachs] entry data are not valid");
        }  
                
        FileAttachementWrapper[] fileList =  getAttachFiles(idRestestExec);
        UrlAttachementWrapper[] urlList = getAttachUrls(idRestestExec);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
                
        return result;
    }
        
    /**
     * Get the id * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idResExec, int idTest) throws Exception {
        if (idResExec <1 || idTest < 1) {
            throw new Exception("[SQLExecutionTestResult->getID] entry data are not valid");
        }  
        int idRestestExec = -1;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecTest"); //ok from execution result
        prep.setInt(1,idResExec);
        prep.setInt(2,idTest);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if (stmtRes.next()){
            idRestestExec = stmtRes.getInt("id_exec_cas");
        }
        return idRestestExec;
    }
        
    ExecutionResultTestWrapper getWrapper(int idResExecTest) throws Exception {
        if (idResExecTest < 1 ) {
            throw new Exception("[SQLExecutionTestResult->getWrapper] entry data are not valid");
        } 
        ExecutionResultTestWrapper pExcutionResultTestWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectResExecTestByID"); //ok
            prep.setInt(1, idResExecTest);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                pExcutionResultTestWrapper = new ExecutionResultTestWrapper();
                pExcutionResultTestWrapper.setIdBDD(idResExecTest);
                pExcutionResultTestWrapper.setOrder(stmtRes.getInt("ordre_exec_cas"));
                pExcutionResultTestWrapper.setIdResExec(stmtRes.getInt("RES_EXEC_CAMP_id_res_exec_camp"));
                pExcutionResultTestWrapper.setStatus(stmtRes.getString("res_exec_cas"));
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pExcutionResultTestWrapper;
    }
}
