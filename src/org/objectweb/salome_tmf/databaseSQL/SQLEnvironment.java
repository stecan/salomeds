/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLEnvironment;

public class SQLEnvironment implements ISQLEnvironment  {
        
    /**
     * Insert a Environment in the database (table ENVIRONNEMENT)
     * @param idProject : id of the project wich contain the environment 
     * @param name of the environment
     * @param description of the environment
     * @return id of the environment in the table ENVIRONNEMENT
     * @throws Exception
     * need permission canCreateCamp or canExecutCamp
     */
    @Override
    public int insert(int idProject, String name, String description) throws Exception {
        int envId = -1;
        int transNumber = -1; 
        if (idProject <1 ) {
            throw new Exception("[SQLEnvironment->insert] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canCreateCamp()) && !(Permission.canExecutCamp())){
                throw new SecurityException("[SQLEnvironment : insert -> canCreateCamp && canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.INSERT_ENVIRONMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addEnvironment"); //ok
            prep.setInt(1, idProject);
            prep.setString(2,name);
            prep.setString(3,description);
            SQLEngine.runAddQuery(prep);
                        
            envId = getID(idProject, name) ;
            if (envId <1 ) {
                throw new Exception("[SQLEnvironment->insert] id are not valid");
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return envId;    
    }
        
    /**
     * Attach a file to the environment (table ENV_ATTACHEMENT)
     * @param idEnv
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idEnv, SalomeFileWrapper f, String description ) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idEnv <1  || f == null) {
            throw new Exception("[SQLEnvironment->addAttachFile] entry data are not valid");
        }
        if (getWrapper(idEnv) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(1,ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(f,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToEnvironment"); //ok
            prep.setInt(1,idEnv);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;        
    }
        
    /**
     * Attach an Url to the environment (table ENV_ATTACHEMENT)
     * @param idEnv
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idEnv, String url,  String description ) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idEnv <1  || url == null) {
            throw new Exception("[SQLEnvironment->addAttachUrl] entry data are not valid");
        }
        if (getWrapper(idEnv) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToEnvironment"); //ok
            prep.setInt(1,idEnv);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
                
        return idAttach;        
    }
    /**
     * Insert a script (type ApiConstants.INIT_SCRIPT) to the environment idEnv
     * If a script already exist, this previous script is deleted 
     * @param idExec
     * @param file of the script
     * @param description : the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg : argument  of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    @Override
    public int addScript(int idEnv, SalomeFileWrapper file, String description,  String name, String extention, String arg) throws Exception {
        if (idEnv <1  || file == null) {
            throw new Exception("[SQLEnvironment->addScript] entry data are not valid");
        }
        int idScript = -1;
        int idAttach = -1; 
        int transNumber = -1;
        if (getWrapper(idEnv) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.INSERT_SCRIPT);
                        
            deleteScript(idEnv);
            idScript = SQLObjectFactory.getInstanceOfISQLScript().insert( name, arg, extention, ApiConstants.INIT_SCRIPT, idEnv);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);
            SQLObjectFactory.getInstanceOfISQLScript().addAttach(idScript, idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->addScript]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idScript;
    }
        
        
    /**
     * Map a value for the parameter idParam in the environment idEnv (table  VALEUR_PARAM)
     * @param idEnv
     * @param idParam
     * @param value
     * @param description
     * @throws Exception
     * need permission canCreateCamp or canExecutCamp
     */
    @Override
    public void addParamValue(int idEnv, int idParam, String value, String description) throws Exception {
        int transNumber = -1;
        if (idEnv <1  || idParam < 1) {
            throw new Exception("[SQLEnvironment->addParamValue] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canCreateCamp()) && !(Permission.canExecutCamp())){
                throw new SecurityException("[SQLEnvironment : insert -> canCreateCamp && canExecutCamp ]");
            }
        }
        if (getWrapper(idEnv) == null){
            throw new DataUpToDateException();
        }
        if (SQLObjectFactory.getInstanceOfISQLParameter().getParameterWrapper(idParam) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.INSERT_PARAMETER_INTO_ENV);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addParamValueToEnv"); //ok
            prep.setInt(1,idEnv);
            prep.setInt(2,idParam);
            prep.setString(3,value);
            prep.setString(4,description);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->addParamValue]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update the name and the description of the environment idEnv
     * @param idEnv
     * @param name
     * @param description
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public void update(int idEnv, String name, String description) throws Exception {
        int transNumber = -1;
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->update] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp()) && !(Permission.canExecutCamp())){
                throw new SecurityException("[SQLEnvironment : update -> canUpdateCamp && canExecutCamp ]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.UPDATE_ENVIRONMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateEnvironment"); //ok
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3,idEnv);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update a mapped value for the parameter idParam in the environment idEnv
     * @param idEnv
     * @param idParam
     * @param value : the new value
     * @param description : the new description
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public void updateParamValue(int idEnv, int idParam, String value, String description) throws Exception {
        int transNumber = -1;
        if (idEnv <1 || idParam < 1) {
            throw new Exception("[SQLEnvironment->updateParamValue] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp()) && !(Permission.canExecutCamp())){
                throw new SecurityException("[SQLEnvironment : insert -> canUpdateCamp && canExecutCamp ]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.UPDATE_PARAMETER_INTO_ENV);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateParamValueToEnv"); //ok
            prep.setString(1,value);
            prep.setString(2,description);
            prep.setInt(3,idEnv);
            prep.setInt(4,idParam);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->updateParamValue]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete the environment idEnv in the Database
     * then delete mapped parameters, mapped attachments and script and  related execution 
     * @param idEnv
     * @throws Exception
     * @see ISQLExecution.delete(int)
     * need permission canExecutCamp or canDeleteCamp
     */
    @Override
    public void delete(int idEnv ) throws Exception {
        int transNumber = -1; 
        if (idEnv <1) {
            throw new Exception("[SQLEnvironment->delete] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!( Permission.canExecutCamp() || Permission.canDeleteCamp() )){
                throw new SecurityException("[SQLEnvironment : delete -> canCreateCamp && canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.DELETE_ENVIRONMENT);
                        
            //Delete used parameters
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectEnvParamsUsingID"); //ok 
            prep.setInt(1,idEnv);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while  (stmtRes.next()) {
                deleteDefParam(idEnv,stmtRes.getInt("PARAM_TEST_id_param_test"));
            }
                        
            //Delete attach files
            deleteAllAttach(idEnv);
                        
            //Delete script
            deleteScript(idEnv);
                        
            //Delete Exceutions
            prep = SQLEngine.getSQLSelectQuery("selectExecutionWithEnv"); //ok
            prep.setInt(1,idEnv);
            stmtRes = SQLEngine.runSelectQuery(prep);
            while  (stmtRes.next()) {
                int idExec = stmtRes.getInt("id_exec_camp");
                SQLObjectFactory.getInstanceOfISQLExecution().delete(idExec);
                /*
                  int campId = stmtRes.getResults().getInt("id_camp");
                  int execCampId = stmtRes.getResults().getInt("id_exec_camp");
                  String execName = stmtRes.getResults().getString("nom_exec_camp");
                  String campName = stmtRes.getResults().getString("nom_camp");
                  org.objectweb.salome_tmf.api.Api.log("suppression de l'executions " + execName +" et son resultat a partir de : " + campName);
                  deleteExecFromCampaign(campId, execCampId, execName, campName);
                */
            }
            prep = SQLEngine.getSQLDeleteQuery("deleteEnvUsingID"); //ok
            prep.setInt(1,idEnv);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Deleted mapped reference to the parameter paramId in the environment idEnv
     * @param idEnv
     * @param paramId
     * @throws Exception
     * need permission canExecutCamp or canDeleteCamp
     */
    @Override
    public void deleteDefParam(int idEnv, int paramId) throws Exception {
        if (idEnv <1 || paramId < 1) {
            throw new Exception("[SQLEnvironment->deleteDefParam] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canDeleteCamp())){
                throw new SecurityException("[SQLEnvironment : deleteDefParam -> canExecutCamp ]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.DELETE_PARAMETER_INTO_ENV);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteParamFromEnv");  //ok
            prep.setInt(1,idEnv);
            prep.setInt(2,paramId);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->deleteDefParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all attchements of the environments idEnv
     * @param idEnv
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idEnv) throws Exception {
        int transNumber = -1;
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->deleteAllAttach] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.DELETE_ATTACHMENT);
                        
            AttachementWrapper[] attachList = getAttachs(idEnv);
            for (int i =0; i < attachList.length; i++){
                AttachementWrapper  pAttachementWrapper = attachList[i];
                deleteAttach(idEnv, pAttachementWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete an attchement idAttach of the environments idEnv
     * @param idEnv
     * @param attachId
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    @Override
    public void deleteAttach(int idEnv, int idAttach) throws Exception {
        int transNumber = -1;
        if (idEnv <1 ||  idAttach < 1) {
            throw new Exception("[SQLEnvironment->deleteAttach] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.DELETE_ATTACHMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromEnv");  //ok
            prep.setInt(1, idEnv);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
        
        
    /**
     * Delete the script of the environnement idEnv
     * then delete reference in SCRIPT, SCRIPT_ATTACHEMENT, ATTACHEMENT
     * @param idExec
     * @throws Exception
     * no permission canExecutCamp or canDeleteCamp
     */
    @Override
    public void deleteScript(int idEnv)throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->deleteScript] entry data are not valid");
        }
        int idScript = getIdScript(idEnv);
        if (idScript == -1){
            return;
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canDeleteCamp())){
                throw new SecurityException("[SQLEnvironement : deleteScript -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(1, ApiConstants.UPDATE_EXECUTION);
                        
            SQLObjectFactory.getInstanceOfISQLScript().delete(idScript);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLEnvironment->deleteScript]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Get a vector of FileAttachementWrapper representing the files attachment of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idEnv) throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectEnvAttachFiles");  //ok
        prep.setInt(1,idEnv);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;  
    }
        
    /**
     * Get a vector of UrlAttachementWrapper representing the Urls attachment of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idEnv) throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectEnvAttachUrls"); //ok
        prep.setInt(1,idEnv);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;  
    }
        
    /**
     * Get a vector of all attachments (AttachementWrapper, File or Url) of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAttachs(int idEnv) throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getAttachs] entry data are not valid");
        }
                
        FileAttachementWrapper[] fileList =  getAttachFiles(idEnv);
        UrlAttachementWrapper[] urlList = getAttachUrls(idEnv);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
        return result;
    }
        
        
    /**
     * Get the java.io.File of the script in the Environnement idEnv
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public SalomeFileWrapper getScript(int idEnv) throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getScript] entry data are not valid");
        }
        int idScript = getIdScript(idEnv);
        if (idScript == -1){
            return null;
        } 
        return SQLObjectFactory.getInstanceOfISQLScript().getFile(idScript);
    }
        
    /**
     * Get a ScriptWrapper representing the script of the Environnement
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public ScriptWrapper getScriptWrapper(int idEnv)throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getScriptWrapper] entry data are not valid");
        }
        ScriptWrapper pScript = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectEnvironmentScript"); //ok
        prep.setInt(1,idEnv);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {
            pScript = new ScriptWrapper();
            pScript.setName(stmtRes.getString("url_script"));
            pScript.setScriptExtension(stmtRes.getString("classpath_script"));
            pScript.setPlugArg(stmtRes.getString("classe_autom_script"));
            pScript.setType(stmtRes.getString("type_script"));
            pScript.setIdBDD(stmtRes.getInt("id_script"));
        }
        return pScript;
    }
        
    int getIdScript(int idEnv)throws Exception {
        int idScript = -1;
        ScriptWrapper pScript = getScriptWrapper(idEnv);
        if (pScript != null){
            idScript = pScript.getIdBDD();
        }
        return idScript;
    }
        
    /**
     * Get the id of the environment name in the project idProject
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idProject, String name) throws Exception {
        if (idProject <1 ) {
            throw new Exception("[SQLEnvironment->getID] entry data are not valid");
        }
        int idEnv = -1;
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdEnv"); //ok
        prep.setInt(1, idProject);
        prep.setString(2, name);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if (stmtRes.next()){
            idEnv = stmtRes.getInt("id_env");
        }
        return idEnv;
    }
        
        
    /**
     * Get an EnvironmentWrapper reprensting the environnement idEnv
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public EnvironmentWrapper getWrapper(int idEnv) throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getWrapper] entry data are not valid");
        }
        EnvironmentWrapper env = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(1, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectEnvById"); //ok
            prep.setInt(1, idEnv);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                env = new EnvironmentWrapper();
                env.setIdBDD(stmtRes.getInt("id_env"));
                env.setName(stmtRes.getString("nom_env"));
                env.setDescription(stmtRes.getString("description_env")); 
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return env;
    }
        
    /**
     * Get the value of the parameter idParameter in the environnement idEnv
     * @param idEnv
     * @param idParameter
     * @return the value or null if the parameter is not use by the environement
     * @throws Exception
     */
    @Override
    public String getParameterValue(int idEnv, int idParameter)  throws Exception {
        if (idEnv <1 || idParameter < 1) {
            throw new Exception("[SQLEnvironment->getParameterValue] entry data are not valid");
        }
        String value = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectParamValueInEnv"); //ok
        prep.setInt(1, idEnv);
        prep.setInt(1, idParameter);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if (stmtRes.next()){
            value = stmtRes.getString("valeur");
        }
        return value;
    }
        
    /**
     * Get a Vector of ValuedParameterWrapper for the environnement idEnv
     * @param idEnv
     * @return
     * @throws Exception
     */
    @Override
    public ValuedParameterWrapper[] getDefinedParameters(int idEnv)  throws Exception {
        if (idEnv <1 ) {
            throw new Exception("[SQLEnvironment->getDefinedParameters] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectParamsInEnv"); //ok
        prep.setInt(1, idEnv);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            ValuedParameterWrapper pValuedParameterWrapper = new ValuedParameterWrapper();
            ParameterWrapper pParameter = new ParameterWrapper();
            pParameter.setName(stmtRes.getString("nom_param_test"));
            pParameter.setDescription(stmtRes.getString("desc_param_test"));
            pParameter.setIdBDD(stmtRes.getInt("id_param_test")); 
            pValuedParameterWrapper.setParameterWrapper(pParameter);
            pValuedParameterWrapper.setIdBDD(stmtRes.getInt("id_valeur"));
            pValuedParameterWrapper.setDescription(stmtRes.getString("desc_valeur"));
            pValuedParameterWrapper.setValue(stmtRes.getString("valeur"));
            result.add(pValuedParameterWrapper);
        }
        ValuedParameterWrapper[] vpwArray = new ValuedParameterWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            vpwArray[i] = (ValuedParameterWrapper) result.get(i);
        }
        return vpwArray;
    }
}
