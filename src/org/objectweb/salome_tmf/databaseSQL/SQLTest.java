/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLTest;


public class SQLTest  implements ISQLTest{
        
        
    /**
     * Add in database the reference of use paramter paramId for test idTest in table CAS_PARAM_TEST
     * @param idTest  : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void addUseParam(int idTest, int paramId) throws Exception {
        if (idTest <1 || paramId < 1) {
            throw new Exception("[SQLTest->addUseParam] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : addUseParam -> canUpdateTest]");
            }
        }
        if (getTest(idTest) == null){
            throw new DataUpToDateException();
        }
        if (SQLObjectFactory.getInstanceOfISQLParameter().getParameterWrapper(paramId) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(101, ApiConstants.INSERT_PARAMETER_INTO_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addParamToTest"); //ok
            prep.setInt(1,idTest);
            prep.setInt(2,paramId);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Attach a file attach to a test in table ATTACHEMENT and reference in table CAS_ATTACHEMENT
     * @param idTest : id of the test
     * @param f : the file
     * @param description of the file
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @See ISQLFileAttachment.insert(File, String);
     * no permission needed
     */
    @Override
    public int addAttachFile(int idTest, SalomeFileWrapper f, String description) throws Exception {
        if (idTest <1 || f == null) {
            throw new Exception("[SQLTest->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getTest(idTest) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
                        
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(f,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToTest"); //ok
            prep.setInt(1,idTest);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;        
    }
        
    /**
     * Attach a url to a test in table ATTACHEMENT and reference in table CAS_ATTACHEMENT
     * @param idTest : id of the test
     * @param url
     * @param description of the url
     * @return the id of the attachment in the table ATTACHEMENT
     * @see ISQLUrlAttachment.insert(String, String);
     * @throws Exception
     */
    @Override
    public int addAttachUrl(int idTest, String url, String description) throws Exception {
        if (idTest <1 || url == null) {
            throw new Exception("[SQLTest->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
                
        if (getTest(idTest) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToTest"); //ok
            prep.setInt(1,idTest);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
                
        return idAttach;        
    }
        
    /**
     * Update the test with new name and description
     * @param idTest : id of the test
     * @param newName : the new name
     * @param newDesc : the new description
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void update(int idTest, String newName, String newDesc) throws Exception {
        if (idTest <1) {
            throw new Exception("[SQLTest->update] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : update -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateTest2"); //ok
            prep.setString(1, newName);
            prep.setString(2, newDesc);
            prep.setInt(3, idTest);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update order of the test
     * @param idTest : id of the test
     * @param order
     * @throws Exception
     */
    void updateOrder(int idTest, int order) throws Exception {
        if (idTest <1 || order < 0) {
            throw new Exception("[SQLTest->updateOrder] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateTestOrder"); //ok
            prep.setInt(1, order);
            prep.setInt(2, idTest);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.err("[SQLTest->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update order of the test by incrementation if increment = true or decrementation
     * @param idTest : id of the test
     * @param increment
     * @return the new order of the test in database
     * @throws Exception
     */
    @Override
    public int updateOrder(int idTest, boolean increment) throws Exception {
        if (idTest <1 ) {
            throw new Exception("[SQLTest->updateOrder] entry data are not valid");
        }
        int transNumber = -1;
        int order = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_TEST);
                        
            TestWrapper pTest = getTest(idTest);
            order =  pTest.getOrder();
            if (increment){
                int maxOrder = SQLObjectFactory.getInstanceOfISQLTestList().getNumberOfTest(pTest.getIdSuite());
                maxOrder --; //Because index begin at 0
                if (order < maxOrder) {
                    TestWrapper pTest2 = SQLObjectFactory.getInstanceOfISQLTestList().getTestByOrder(pTest.getIdSuite(),order + 1);
                    updateOrder(idTest, order + 1);
                    updateOrder(pTest2.getIdBDD(), order);
                    order++;
                }
            } else {
                if (order > 0) {
                    TestWrapper pTest2 = SQLObjectFactory.getInstanceOfISQLTestList().getTestByOrder(pTest.getIdSuite(), order - 1);
                    updateOrder(idTest, order - 1);
                    updateOrder(pTest2.getIdBDD(), order);
                    order--;
                }
            }  
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return  order;
    }
        
    /**
     * replace all reference of user oldIdUser by newIdUser in the table (CAS_TEST) where suite = idSuite
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updateUserRef(int idSuite, int oldIdUser, int newIdUser)  throws Exception {
        if (idSuite <1 || oldIdUser < 1 || newIdUser< 1) {
            throw new Exception("[SQLTest->updateUserRef] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateTestUser"); //OK
            prep.setInt(1, newIdUser);
            prep.setInt(2, oldIdUser);
            prep.setInt(3, idSuite);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->updateUserRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete the test in table CAS_TEST and all reference (Parameter, Campaign) and the test attachments
     * @param idTest : id of the test
     * @throws Exception
     * @see deleteAllUseParam(int)
     * @see deleteAllAttach(int)
     * @see ISQLCampaign.deleteTestInAllCampaign(int)
     * need permission canDeleteTest
     */
    protected void deleteRef(int idTest, boolean reorder)  throws Exception {
        if (idTest <1) {
            throw new Exception("[SQLTest->deleteRef] entry data are not valid");
        }
        TestWrapper pTest = getTest(idTest);
        int order =  pTest.getOrder();
        int maxOrder = 1;
        if (reorder) {
            maxOrder = SQLObjectFactory.getInstanceOfISQLTestList().getNumberOfTest(pTest.getIdSuite());
            maxOrder --; //Because index begin at 0
        }
        //Delete All Parameter
        deleteAllUseParam(idTest);
                
        //Delete All Attachement
        deleteAllAttach(idTest);
                
        //Delete Test From Campaign
        SQLObjectFactory.getInstanceOfISQLCampaign().deleteTestInAllCampaign(idTest);
                
        // Suppression du test
        PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteTestUsingID"); //ok
        prep.setInt(1, idTest);
        SQLEngine.runDeleteQuery(prep);
                
        // Update Order
        if (reorder) {
            if (order < maxOrder) {
                for (int i = order + 1; i <= maxOrder ; i++){
                    TestWrapper pTest2 = SQLObjectFactory.getInstanceOfISQLTestList().getTestByOrder(pTest.getIdSuite(), i);
                    updateOrder(pTest2.getIdBDD(), i-1);
                }
            }
        }
    }
    /**
     * Delete the test in table CAS_TEST and all reference (Parameter, Campaign) and the test attachments
     * delete test actions (if manual test), delete test script (if automatic test)
     * @param idTest : id of the test
     * @throws Exception
     * @see deleteAllUseParam(int)
     * @see deleteAllAttach(int)
     * @see ISQLCampaign.deleteTestInAllCampaign(int)
     * need permission canDeleteTest
     */
    @Override
    public void delete(int idTest)  throws Exception {
        delete(idTest, true);
    }
        
    /**
     * Delete the test in table CAS_TEST and all reference (Parameter, Campaign) and the test attachments
     * delete test actions (if manual test), delete test script (if automatic test)
     * Update order of test in the suite if reorder = true
     * @param idTest : id of the test
     * @param reorder reorder the test in the suite
     * @throws Exception
     * @see deleteAllUseParam(int)
     * @see deleteAllAttach(int)
     * @see ISQLCampaign.deleteTestInAllCampaign(int)
     * need permission canDeleteTest
     * @TODO SOAP
     */
    @Override
    public void delete(int idTest, boolean reorder)  throws Exception {
        if (idTest <1) {
            throw new Exception("[SQLTest->delete] entry data are not valid");
        }
        int transNumber = -1;
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLTest : delete -> canDeleteTest]");
            }
            //Require specialAllow
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }
        try {
                        
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_TEST);
                        
            //Delete All test actions if test is manual
            ActionWrapper[] actionList = SQLObjectFactory.getInstanceOfISQLManualTest().getTestActions(idTest);
            for (int i = 0; i< actionList.length ; i++){
                int actionId = (actionList[i]).getIdBDD();
                //SQLObjectFactory.getInstanceOfISQLAction().delete(actionId);
                SQLObjectFactory.getInstanceOfISQLAction().delete(actionId, false);
            }
            //Delete Script Test if test is Automatic
            SQLObjectFactory.getInstanceOfISQLAutomaticTest().deleteScript(idTest);
                        
            deleteRef(idTest, reorder);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLTest->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }
        
    /**
     * Delete all attachment in table (CAS_ATTACHEMENT and ATTACHEMENT) for the test
     * @param idTest : id of the test
     * @throws Exception
     * @see deleteAttach(int, int)
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idTest) throws Exception {
        if (idTest <1) {
            throw new Exception("[SQLTest->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
            AttachementWrapper[] attachList = getAllAttachemnt(idTest);
                        
            for (int i = 0 ; i < attachList.length; i++){
                AttachementWrapper pAttachementWrapper = attachList[i];
                deleteAttach(idTest, pAttachementWrapper.getIdBDD());
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all reference about using parameter (table CAS_PARAM_TEST) for the test
     * if the test is manual, parameters are deleted in action  
     * @param idTest : id of the test
     * @throws Exception
     * need permission  canUpdateTest
     */
    @Override
    public void deleteAllUseParam(int idTest) throws Exception {
        if (idTest <1) {
            throw new Exception("[SQLTest->deleteAllUseParam] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : deleteAllUseParam -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_PARAMETER_FROM_TEST);
                        
            ParameterWrapper[] paramList = getAllUseParams(idTest);
            for (int i = 0 ; i < paramList.length; i++){
                ParameterWrapper pParameterWrapper  = paramList[i];
                deleteUseParam(idTest, pParameterWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->deleteAllUseParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test
     * @param idTest : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * need permission  canUpdateTest
     */
    protected void deleteUseParamRef(int idTest, int paramId) throws Exception {
        if (idTest <1 || paramId < 1 ) {
            throw new Exception("[SQLTest->deleteUseParamRef] entry data are not valid");
        }
        PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteParamFromTest"); //ok
        prep.setInt(1, idTest);
        prep.setInt(2, paramId);                
        SQLEngine.runDeleteQuery(prep);
    }
        
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test
     * if the test is manual, parameters are deleted in action  
     * @param idTest : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * @see ISQLAction.deleteParamUse(int, int)
     * need permission  canUpdateTest
     */
    @Override
    public void deleteUseParam(int idTest, int paramId) throws Exception {
        if (idTest <1 || paramId < 1 ) {
            throw new Exception("[SQLTest->deleteUseParam] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : deleteUseParam -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_PARAMETER_FROM_TEST);
                        
            ActionWrapper[] actionList = SQLObjectFactory.getInstanceOfISQLManualTest().getTestActions(idTest);
            for (int i = 0 ; i < actionList.length; i++){
                ActionWrapper pActionWrapper = actionList[i];
                SQLObjectFactory.getInstanceOfISQLAction().deleteParamUse(pActionWrapper.getIdBDD(),paramId);
            }
                        
            deleteUseParamRef(idTest, paramId);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->deleteUseParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete attachement for the test and the attachement (tables CAS_ATTACHEMENT, ATTACHEMENT)
     * @param idTest : id of the test
     * @param attachId : id of the attachment
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    @Override
    public void deleteAttach(int idTest, int attachId) throws Exception {
        if (idTest <1 || attachId < 1 ) {
            throw new Exception("[SQLTest->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
                        
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromTest");
            prep.setInt(1, idTest);
            prep.setInt(2, attachId);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(attachId);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTest->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Get a TestWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     * @see TestWrapper
     */
    @Override
    public TestWrapper getTest(int testId) throws Exception {
        if (testId < 1) {
            throw new Exception("[SQLTest->getTest] entry data are not valid");
        }
        TestWrapper pTest = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestUsingID"); //ok
            prep.setInt(1,testId);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                pTest = new TestWrapper();
                pTest.setName(stmtRes.getString("nom_cas"));
                pTest.setDescription(stmtRes.getString("description_cas"));
                pTest.setIdBDD(stmtRes.getInt("id_cas"));
                pTest.setOrder(stmtRes.getInt("ordre_cas"));
                pTest.setIdSuite(stmtRes.getInt("SUITE_TEST_id_suite"));
                pTest.setType(stmtRes.getString("type_cas"));                   
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTest;
    }
        
    /**
     * Get the id of the test identified by name in the testlist identified by idTestList
     * @param idTestList : id of the testlist
     * @param name : the test name
     * @return a test id
     * @throws Exception
     */
    @Override
    public int getID(int idTestList, String name) throws Exception {
        if (idTestList < 1) {
            throw new Exception("[SQLTest->getID] entry data are not valid");
        }
        int idTest = -1;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdTest"); //ok
            prep.setString(1,name);
            prep.setInt(2,idTestList);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            Util.log("[SQLTest->getID] with " + idTestList + ", and " + name + " = " + stmtRes);
            if (stmtRes.next()){
                idTest = stmtRes.getInt("id_cas");
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idTest;
    }
        
    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAllAttachemnt(int testId) throws Exception {
        if (testId < 1) {
            throw new Exception("[SQLTest->getAllAttachemnt] entry data are not valid");
        }
        FileAttachementWrapper[] fileList =  getAllAttachFiles(testId);
        UrlAttachementWrapper[] urlList = getAllAttachUrls(testId);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
                
        return result;
    }
        
    /**
     * Get a Vector of FileAttachementWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAllAttachFiles(int testId) throws Exception {
        if (testId < 1) {
            throw new Exception("[SQLTest->getAllAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestAttachFiles"); //ok
        prep.setInt(1, testId);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     * Get a Vector of UrlAttachementWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAllAttachUrls(int testId) throws Exception {
        if (testId < 1) {
            throw new Exception("[SQLTest->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestAttachUrls"); //ok
        prep.setInt(1, testId);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }
        
    /**
     * Get a Vector of ParameterWrapper (only id is set) using by the test identifed by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public ParameterWrapper[] getAllUseParams(int testId) throws Exception {
        if (testId < 1) {
            throw new Exception("[SQLTest->ParameterWrapper] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestParams"); //ok
        prep.setInt(1, testId);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            ParameterWrapper pParameterWrapper = new ParameterWrapper();
            pParameterWrapper.setIdBDD( stmtRes.getInt("PARAM_TEST_id_param_test"));
            pParameterWrapper.setName(stmtRes.getString("nom_param_test"));
            pParameterWrapper.setDescription(stmtRes.getString("desc_param_test"));
            result.addElement(pParameterWrapper);
        }
        ParameterWrapper[] pwArray = new ParameterWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            pwArray[i] = (ParameterWrapper) result.get(i);
        }
        return pwArray;
    }
}
