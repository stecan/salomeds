package org.objectweb.salome_tmf.databaseSQL;

public class AttachException extends Exception {
        
    public AttachException(String message){
        super(message);
    }

}
