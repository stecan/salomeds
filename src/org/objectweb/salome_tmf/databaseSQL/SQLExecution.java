/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecution;

public class SQLExecution implements ISQLExecution{
        
    /**
     * Insert an Exceution in the campaign idCamp (table EXEC_CAMP)
     * @param idCamp
     * @param name of the execution
     * @param idEnv used by the execution
     * @param idDataSet used by the execution
     * @param idUser who created the execution
     * @param description of the execution
     * @return the id of the execution 
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public int insert(int idCamp, String name, int idEnv, int idDataSet, int idUser, String description) throws Exception {
        int idExec = -1;
        int transNumber = -1; 
        if (idCamp <1 ) {
            throw new Exception("[SQLExecution->insert] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : insert -> canExecutCamp]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLCampaign().getCampaign(idCamp) == null){
            throw new DataUpToDateException();
        }
        if (SQLObjectFactory.getInstanceOfISQLEnvironment().getWrapper(idEnv) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.INSERT_EXECUTION);
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addExecCampaign"); //ok
            prep.setInt(1, idEnv);
            prep.setInt(2, idDataSet);
            prep.setInt(3, idUser);
            prep.setInt(4, idCamp);
            prep.setString(5, name);
            prep.setDate(6, Util.getCurrentDate());
            prep.setTime(7, Util.getCurrentTime());
            prep.setString(8, description);
            SQLEngine.runAddQuery(prep);
                        
            idExec = getID(idCamp, name);
            if (idExec <1 ) {
                throw new Exception("[SQLExecution->insert] id are not valid");
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return  idExec;
    }
        
    /**
     * Attach a file to the Exceution (table EXEC_CAMP_ATTACH)
     * @param idExec
     * @param file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idExec, SalomeFileWrapper file, String description) throws Exception {
        if (idExec <1 || file == null) {
            throw new Exception("[SQLExecution->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
                
        if (getWrapper(idExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToExec"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idExec);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;        
    }
        
        
        
        
    /**
     * Attach an Url to the Exceution (table EXEC_CAMP_ATTACH)
     * @param idExec
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idExec, String url, String description) throws Exception {
        if (idExec <1 || url == null) {
            throw new Exception("[SQLExecution->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getWrapper(idExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToExec"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idExec);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
                
        return idAttach;        
    }
        
        
    int addScript(int idExec, SalomeFileWrapper file, String description,  String name, String arg1, String extension, String type) throws Exception {
        if (idExec <1 || file == null) {
            throw new Exception("[SQLExecution->addScript] entry data are not valid");
        }
        int idScript = -1;
        int idAttach = -1; 
        int transNumber = -1;
        if (getWrapper(idExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_SCRIPT);
                        
            idScript = SQLObjectFactory.getInstanceOfISQLScript().insert(idExec, name, arg1, extension, type);
                        
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);
                        
            SQLObjectFactory.getInstanceOfISQLScript().addAttach(idScript, idAttach);
                        
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->addScript]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idScript;
    }
        
    /**
     * Insert a pre-scrit (type ApiConstants.PRE_SCRIPT) to the execution idExec
     * @param idExec
     * @param file of the script
     * @param description the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg : argument 2 of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    @Override
    public int addPreScript(int idExec, SalomeFileWrapper file, String description,  String name, String extention, String arg) throws Exception {
        deletePreScript(idExec);
        return addScript(idExec, file, description, name, arg, extention, ApiConstants.PRE_SCRIPT);
    }
        
    /**
     * Insert a pre-scrit (type ApiConstants.POST_SCRIPT) to the execution idExec 
     * @param idExec
     * @param file of the script
     * @param description the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg : argument 2 of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    @Override
    public int addPostScript(int idExec, SalomeFileWrapper file, String description,  String name, String extention, String arg) throws Exception {
        deletePostScript(idExec);
        return addScript(idExec, file, description, name, arg , extention, ApiConstants.POST_SCRIPT);
    }
        
        
    /**
     * Update the name and the description of the execution idExec
     * @param idExec
     * @param name
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public void updateName(int idExec, String name) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->updateName] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            //if (!(Permission.canUpdateCamp()) && !(Permission.canExecutCamp())){
            if (!(Permission.canUpdateCamp() || Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : updateName -> canUpdateCamp && canExecutCamp ]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateExecCampagneUsingID"); //ok
            prep.setString(1, name);
            prep.setInt(2, idExec);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->updateName]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update the date of the execution idExec
     * @param idExec
     * @param date
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updateDate(int idExec, Date date) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->updateDate] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateExecutionLastDate"); //ok
            prep.setDate(1, date);
            prep.setInt(2, idExec);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->updateDate]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update the environment mapped to the execution idExec
     * @param idExec
     * @param idEnv the new env to use
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void updateEnv(int idExec, int idEnv) throws Exception {
        if (idExec <1 || idEnv < 1) {
            throw new Exception("[SQLExecution->updateEnv] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : updateEnv -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(11, ApiConstants.UPDATE_EXECUTION);
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateEnvForExec"); //ok
            prep.setInt(1, idEnv);
            prep.setInt(2, idExec);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->updateEnv]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update the dataset mapped to the execution idExec
     * @param idExec
     * @param idDataset the new dataset to use
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void updateDataset(int idExec, int idDataset) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->updateDataset] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : updateDataset -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateDataSetForExec"); //ok
            prep.setInt(1, idDataset);
            prep.setInt(2, idExec);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->updateDataset]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * replace all reference of user oldIdUser by newIdUser in the table (EXEC_CAMP) where campagne = idCamp
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updateUserRef(int idCamp, int oldIdUser, int newIdUser)  throws Exception {
        if (idCamp <1 || oldIdUser < 1 || newIdUser < 1) {
            throw new Exception("[SQLExecution->updateUserRef] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateExecUser"); //OK
            prep.setInt(1, newIdUser);
            prep.setInt(2, oldIdUser);
            prep.setInt(3, idCamp);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->updateUserRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all attchements of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);
                        
            AttachementWrapper[] attachList = getAttachs(idExec);
            for (int i =0; i < attachList.length; i++){
                AttachementWrapper  pAttachementWrapper = attachList[i];
                deleteAttach(idExec, pAttachementWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete the execution idExec in the Database
     * Then delete all attachemnts, the script, and all related execution result
     * @param idExec
     * @see ISQLExecutionResult.delete(int)
     * need permission canExecutCamp
     */
    @Override
    public void delete(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->delete] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION);
                        
            deleteAllAttach(idExec);
            deleteScripts(idExec);
                        
            ExecutionResultWrapper[] execResList = getExecResults(idExec);
            for (int i = 0; i < execResList.length; i++){
                ExecutionResultWrapper pExecResult  = execResList[i];
                SQLObjectFactory.getInstanceOfISQLExecutionResult().delete(pExecResult.getIdBDD());
            }
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteExecCampUsingID"); //ok
            prep.setInt(1, idExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all execution result for the execution idExec in the Database
     * @param idExec
     * @see ISQLExecutionResult.delete(int)
     * need permission canExecutCamp
     */
    @Override
    public void deleteAllExecResult(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->delete] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecution : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_RESULT);
                        
            ExecutionResultWrapper[] execResList = getExecResults(idExec);
            for (int i = 0; i < execResList.length; i++){
                ExecutionResultWrapper pExecResult  = execResList[i];
                SQLObjectFactory.getInstanceOfISQLExecutionResult().delete(pExecResult.getIdBDD());
            }
                        
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Delete an attchement idAttach of the execution idExec
     * @param idExec
     * @param idAttach
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAttach(int idExec, int idAttach)throws Exception {
        if (idExec <1 || idAttach < 1) {
            throw new Exception("[SQLExecution->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromExec"); //ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecution->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Delete the pre-script of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deletePreScript(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->deletePreScript] entry data are not valid");
        }
        int idScript = -1;
        idScript = getIdPreScript(idExec);
        if (idScript == -1) {
            return ;
        }
        SQLObjectFactory.getInstanceOfISQLScript().delete(idScript);
    }
        
    /**
     * Delete the post-script of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deletePostScript(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->deletePostScript] entry data are not valid");
        }
        int idScript = -1;
        idScript = getIdPostScript(idExec);
        if (idScript == -1) {
            return ;
        }
        SQLObjectFactory.getInstanceOfISQLScript().delete(idScript);
    }
        
    /**
     * Delete pre-script and post-script of the execution idEnv
     * @param idEnv
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteScripts(int idExec) throws Exception {
        deletePreScript(idExec);
        deletePostScript(idExec);
    }
        
        
    /**
     * Get a vector of FileAttachementWrapper representing the files attachment of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idExec)throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecAttachFiles"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     *  Get a vector of UrlAttachementWrapper representing the Urls attachment of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idExec)throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecAttachUrls"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
                
        return uawArray;
    }
        
    /**
     *  Get a vector of all attachments (AttachementWrapper, File or Url) of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAttachs(int idExec)throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getAttachs] entry data are not valid");
        }
        FileAttachementWrapper[] fileList =  getAttachFiles(idExec);
        UrlAttachementWrapper[] urlList = getAttachUrls(idExec);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
                
        return result;
    }
        
        
    /**
     * Get an array (lenth 2) of ScriptWrapper representing the pre and post script of the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public ScriptWrapper[] getExecutionScripts(int idExec) throws Exception  {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getExecutionScripts] entry data are not valid");
        }
        ScriptWrapper[] pScript = new ScriptWrapper[2];
        pScript[0] = null;
        pScript[1] = null;
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionScript"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        int i = 0;
        while  (stmtRes.next() && i < 2) {
            pScript[i] = new ScriptWrapper();
            pScript[i].setName(stmtRes.getString("url_script"));
            pScript[i].setScriptExtension(stmtRes.getString("classpath_script"));
            pScript[i].setPlugArg(stmtRes.getString("classe_autom_script"));
            pScript[i].setType(stmtRes.getString("type_script"));
            pScript[i].setIdBDD(stmtRes.getInt("id_script"));
            i++;
        }       
        return pScript;
    }
        
    int getIdScript (int idEnv, String type) throws Exception {
        if (idEnv <1) {
            throw new Exception("[SQLExecution->getIdScript] entry data are not valid");
        }
        int id = -1;
        int i = 0;
        ScriptWrapper[] pScript = getExecutionScripts(idEnv); 
        while  (i < 2) {
            ScriptWrapper pScriptWrapper = pScript[i];
            if (pScript[i] != null) {
                if (pScriptWrapper.getType().equals(type)){
                    id = pScriptWrapper.getIdBDD();
                    i = 2;
                } 
            } 
            i++;
        }
        return id;
    }
        
    int getIdPreScript (int idEnv) throws Exception {
        return getIdScript(idEnv,ApiConstants.PRE_SCRIPT);
    }
        
        
    int getIdPostScript (int idEnv) throws Exception {
        return getIdScript(idEnv,ApiConstants.POST_SCRIPT);
    }
        
    /**
     * Get the java.io.File of the pre-script in the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public SalomeFileWrapper getPreScript(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getPreScript] entry data are not valid");
        }
        int idScript = -1;
        idScript = getIdPreScript(idExec);
        if (idScript == -1) {
            return null;
        }
        return  SQLObjectFactory.getInstanceOfISQLScript().getFile(idScript);
                
    }
        
    /**
     * Get the java.io.File of the post-script in the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public SalomeFileWrapper getPostScript(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getPostScript] entry data are not valid");
        }
        int idScript = -1;
        idScript = getIdPostScript(idExec);
        if (idScript == -1) {
            return null;
        }
        return  SQLObjectFactory.getInstanceOfISQLScript().getFile(idScript);
                
    }
        
    /**
     * Get a vector of ExecutionResultWrapper representing  the execution result of the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionResultWrapper[] getExecResults(int idExec)throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getExecResults] entry data are not valid");
        }
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionResults"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            ExecutionResultWrapper pExecResult = new ExecutionResultWrapper();
            pExecResult.setName(stmtRes.getString("nom_res_exec_camp"));
            pExecResult.setExecutionDate(stmtRes.getDate("date_res_exec_camp"));
            pExecResult.setTime(stmtRes.getTime("heure_res_exec_camp").getTime());
            int  userId = stmtRes.getInt("PERSONNE_id_personne");
            pExecResult.setTester(SQLObjectFactory.getInstanceOfISQLPersonne().getTwoName(userId));
            pExecResult.setExecutionStatus(stmtRes.getString("resultat_res_exec_camp"));
            int res_execId = stmtRes.getInt("id_res_exec_camp");
            pExecResult.setIdBDD(res_execId);
            pExecResult.setNumberOfFail(SQLObjectFactory.getInstanceOfISQLExecutionResult().getNumberOfFail(res_execId));
            pExecResult.setNumberOfSuccess(SQLObjectFactory.getInstanceOfISQLExecutionResult().getNumberOfPass(res_execId));
            pExecResult.setNumberOfUnknow(SQLObjectFactory.getInstanceOfISQLExecutionResult().getNumberOfInc(res_execId));
                        
            result.addElement(pExecResult);
        }
        ExecutionResultWrapper[] erwArray = new ExecutionResultWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            erwArray[i] = (ExecutionResultWrapper) result.get(i);
        }
        return erwArray;
    }
        
    /**
     * Get the id of the execution name in the campaign idCamp
     * @param idCamp
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idCamp, String name) throws Exception {
        if (idCamp <1) {
            throw new Exception("[SQLExecution->getID] entry data are not valid");
        }
        int idExec = -1;
                
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdExec"); //ok
            prep.setInt(1, idCamp);
            prep.setString(2, name);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                idExec = stmtRes.getInt("id_exec_camp");
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idExec;
    }
        
    /**
     * Get a wrapper of the execution represented by idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionWrapper getWrapper(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getWrapper] entry data are not valid");
        }
        ExecutionWrapper pExecutionWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecByID"); //ok
            prep.setInt(1, idExec);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                pExecutionWrapper = new ExecutionWrapper();
                pExecutionWrapper.setName(stmtRes.getString("nom_exec_camp"));
                pExecutionWrapper.setLastDate(stmtRes.getDate("last_exec_date"));
                pExecutionWrapper.setCreationDate(stmtRes.getDate("date_exec_camp"));
                pExecutionWrapper.setIdBDD(stmtRes.getInt("id_exec_camp"));
                pExecutionWrapper.setDescription(stmtRes.getString("desc_exec_camp"));
                pExecutionWrapper.setCampId(stmtRes.getInt("CAMPAGNE_TEST_id_camp"));
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pExecutionWrapper;
    }
    /**
     * Get a Vect of java.sql.Date reprenting all date (sorted) where the execution idExec was executed
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public long[] getAllExecDate(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getAllExecDate] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionResults"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            result.addElement(stmtRes.getTime("heure_res_exec_camp"));
        }
        long[] tArray = new long[result.size()];
        for(int i = 0; i < result.size(); i++) {
            tArray[i] = ((Time) result.get(i)).getTime();
        }
        return tArray;
    } 
        
    /**
     * Get an EnvironmentWrapper representing the environnment used by the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public EnvironmentWrapper getEnvironmentWrapper(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getEnvironmentWrapper] entry data are not valid");
        }
        EnvironmentWrapper pEnvironmentWrapper = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionEnv"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if  (stmtRes.next()) {
            pEnvironmentWrapper = new EnvironmentWrapper();
            pEnvironmentWrapper.setIdBDD(stmtRes.getInt("id_env"));
            pEnvironmentWrapper.setName(stmtRes.getString("nom_env"));
            pEnvironmentWrapper.setDescription(stmtRes.getString("description_env"));
        }
        return pEnvironmentWrapper;
    }
        
    /**
     * Get an DataSetWrapper representing the dataset used by the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    @Override
    public DataSetWrapper getDataSetWrapper(int idExec) throws Exception {
        if (idExec <1) {
            throw new Exception("[SQLExecution->getDataSetWrapper] entry data are not valid");
        }
        DataSetWrapper pDataSetWrapper = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecutionJeuDonnees"); //ok
        prep.setInt(1,idExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if  (stmtRes.next()) {
            pDataSetWrapper = new DataSetWrapper();
            pDataSetWrapper.setIdBDD(stmtRes.getInt("id_jeu_donnees"));
            pDataSetWrapper.setName(stmtRes.getString("nom_jeu_donnees"));
            pDataSetWrapper.setDescription(stmtRes.getString("desc_jeu_donnees"));
        }
        return pDataSetWrapper;
    }
}

