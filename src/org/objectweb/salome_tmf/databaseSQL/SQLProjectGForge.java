package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;

public class SQLProjectGForge extends SQLProject {
        
        
    /**
     * Get a Vector of UserWrapper representing all the Users in the project projectName
     * And Upate Table Persone with GFORGE user of the project
     * @param projectName
     * @throws Exception
     */
    @Override
    public UserWrapper[] getUsersOfProject(String projectName) throws Exception {
        Vector resultSalome = new Vector();
                
        Hashtable userSalome = new Hashtable();
        Hashtable userGforge = new Hashtable();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectAllProjectUsers"); //ok
        prep.setString(1,projectName);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("id_personne"));
            pUserWrapper.setLogin(stmtRes.getString("login_personne"));
            pUserWrapper.setName(stmtRes.getString("nom_personne"));
            pUserWrapper.setPrenom(stmtRes.getString("prenom_personne"));
            pUserWrapper.setDescription(stmtRes.getString("desc_personne"));
            pUserWrapper.setEmail(stmtRes.getString("email_personne"));
            pUserWrapper.setTel(stmtRes.getString("tel_personne"));
            pUserWrapper.setCreateDate(stmtRes.getDate("date_creation_personne"));
            pUserWrapper.setCreateTime(stmtRes.getTime("heure_creation_personne").getTime());
            pUserWrapper.setPassword(stmtRes.getString("mot_de_passe"));
            resultSalome.add(pUserWrapper);
            userSalome.put(pUserWrapper.getLogin().trim(), pUserWrapper);
        }
                
        /*prep = SQLEngineGForge.getSQLSelectGForgeQuery("selectAllGForgeUsers");
          stmtRes = SQLEngineGForge.runSelectQuery(prep);
          while  (stmtRes.next()) {
          Util.debug(stmtRes);
          }*/
                
        Vector resultGforge = new Vector();
        prep = SQLEngineGForge.getSQLSelectGForgeQuery("selectAllGForgeProjectUsers");
        prep.setString(1,projectName);
        //prep.setString(1,"salome_demo");
                
        stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            //Util.debug("Add user GFORGE " + stmtRes);
            UserWrapper pUserWrapper = new UserWrapper();
            pUserWrapper.setIdBDD(stmtRes.getInt("user_id"));
            pUserWrapper.setLogin(stmtRes.getString("user_name"));
            pUserWrapper.setName(stmtRes.getString("lastname"));
            pUserWrapper.setPrenom(stmtRes.getString("firstname"));
            pUserWrapper.setDescription(stmtRes.getString("title"));
            pUserWrapper.setEmail(stmtRes.getString("email"));
            pUserWrapper.setTel(stmtRes.getString("phone"));
            pUserWrapper.setPassword(stmtRes.getString("user_pw"));
            resultGforge.add(pUserWrapper);
            userGforge.put(pUserWrapper.getLogin().trim(), pUserWrapper);
        }
                
        //Ajout des Users GForge qui ne sont pas dans Salome 
        SQLEngineGForge.closeGforgeDB();
                
        int transNumber = -1; 
        int groupID;
        ISQLGroup pISQISQLGroup = SQLObjectFactory.getInstanceOfISQLGroup();
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_GROUP);
                        
            int projectID = getProject(projectName).getIdBDD();
            GroupWrapper[] groupsOfTheProject = getProjectGroups(projectID);
            GroupWrapper pGForgeGroupWrapper = null;
            int i = 0;
            boolean find = false;
            while (!find && i < groupsOfTheProject.length){
                GroupWrapper pGroupWrapper = groupsOfTheProject[i];
                if (pGroupWrapper.getName().trim().equals("GForge")){
                    find = true;
                    pGForgeGroupWrapper = pGroupWrapper;
                }
                i++;
            }
                        
            if (pGForgeGroupWrapper == null) {
                // Ajout du group GForge
                groupID = pISQISQLGroup.insert(projectID, "GForge", "Users registred in GForge", 254);
            } else {
                groupID = pGForgeGroupWrapper.getIdBDD();
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err(e);
            SQLEngine.rollBackTrans(transNumber);
                        
            UserWrapper[] uwArray = new UserWrapper[resultSalome.size()];
            for(int i = 0; i < resultSalome.size(); i++) {
                uwArray[i] = (UserWrapper) resultSalome.get(i);
            }
            return uwArray;
        }
                
                
        Enumeration loginGforgeEnum = userGforge.keys();
        ISQLPersonne pISQLPersonne = SQLObjectFactory.getInstanceOfISQLPersonne();
        while (loginGforgeEnum.hasMoreElements()){
            try {
                transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_GROUP);
                String login = (String) loginGforgeEnum.nextElement();
                UserWrapper pSalomeUserWrapper = (UserWrapper) userSalome.get(login);
                UserWrapper pGForgeUserWrapper = (UserWrapper) userGforge.get(login);
                if (pSalomeUserWrapper == null){
                    int userID = pISQLPersonne.insert(pGForgeUserWrapper.getLogin(), pGForgeUserWrapper.getName(), 
                                                      pGForgeUserWrapper.getPrenom(), pGForgeUserWrapper.getDescription(), 
                                                      pGForgeUserWrapper.getEmail(), pGForgeUserWrapper.getTel(), 
                                                      pGForgeUserWrapper.getPassword(), false);
                                        
                    pISQISQLGroup.insertUser(groupID, userID);
                                        
                    resultSalome.add(userGforge.get(login));
                } else {
                    //update password
                    pISQLPersonne.updatePassword(login, pGForgeUserWrapper.getPassword(), false);
                }
                SQLEngine.commitTrans(transNumber);
            } catch (Exception e ){
                Util.err(e);
                SQLEngine.rollBackTrans(transNumber);
            }
        }
                
                
        UserWrapper[] uwArray = new UserWrapper[resultSalome.size()];
        for(int i = 0; i < resultSalome.size(); i++) {
            uwArray[i] = (UserWrapper) resultSalome.get(i);
        }
        return uwArray;
    }
}
