/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.IDataBase;

public class DataBase implements IDataBase {
    /**
     * Connexion e la BdD
     */
    Connection cnt = null;
    // private String driver = "com.mysql.jdbc.Driver";
    private String driver = "org.gjt.mm.mysql.Driver";
    // Savepoint pSavepoint;

    private String poolUrl;
    private Properties poolInfo;
    private static String proxoolMaximumActiveTime = "14400000";

    private String url;
    private String userName;
    private String password;

    public DataBase(String driverJDBC) throws Exception {
        driver = driverJDBC;
        Class.forName(driver).newInstance();
        Util.log("[DataBase] connection to database with jdbc " + driver
                 + " is OK");
    }

    private void configurePoolConnection(String url, String username,
                                         String password) throws Exception {
        String poolAlias = "SalomePool";

        poolInfo = new Properties();
        poolInfo.setProperty("proxool.maximum-connection-count", "20");
        poolInfo.setProperty("proxool.house-keeping-test-sql",
                             "select CURRENT_DATE");
        poolInfo.setProperty("proxool.trace", "true");
        poolInfo.setProperty("proxool.maximum-active-time",
                             proxoolMaximumActiveTime);
        Util.log("[DataBase] pool connection maximum active time is "
                 + proxoolMaximumActiveTime + " ms");
        poolInfo.setProperty("user", username);
        poolInfo.setProperty("password", password);
        String alias = poolAlias;
        String driverClass = driver;
        String driverUrl = url;
        poolUrl = "proxool." + alias + ":" + driverClass + ":" + driverUrl;

        Class.forName("org.logicalcobwebs.proxool.ProxoolDriver").newInstance();
    }

    @Override
    public void openPoolConnection(String url, String username, String password)
        throws Exception {
        try {
            Util.log("DB " + url + ", user : " + username + ", paswd : "
                     + password);

            // Pool de connexion par DBCP
            // BasicDataSource ds = new BasicDataSource();
            // ds.setDriverClassName(driver);
            // ds.setUsername(username);
            // ds.setPassword(password);
            // ds.setUrl(url);
            // cnt = ds.getConnection();

            // Pool de connexion par Proxool
            if (poolUrl == null || poolInfo == null) {
                configurePoolConnection(url, username, password);
            }
            cnt = DriverManager.getConnection(poolUrl, poolInfo);

            cnt.setAutoCommit(false);

            Util.log("[DataBase->open] Pool Connection (proxool) : " + cnt);

        } catch (SQLException ex) {
            Util.log("Echec d'ouverture :" + ex.getMessage());
            throw ex;
        }
    }

    /**
     * Fonction qui ouvre la connexion e la base de donnees
     *
     * @param url
     * @param username
     * @param password
     */
    @Override
    public void open(String url, String username, String password)
        throws Exception {
        try {

            this.url = url;
            this.userName = username;
            this.password = password;

            Util.log("DB " + url + ", user : " + username + ", paswd : "
                     + password);
            cnt = DriverManager.getConnection(url, username, password);
            cnt.setAutoCommit(false);

            Util.log("[DataBase->open] Connection : " + cnt);

        } catch (SQLException ex) {
            Util.log("Echec d'ouverture :" + ex.getMessage());
            throw ex;
        }
    }

    /**
     *Fonction qui permet d'essayer de se reconnecter dans le cas de perte de
     * connexion
     * @throws SQLException
     */
    public void reconnect() throws SQLException {
        try {
            Util.log("DB " + url + ", user : " + userName + ", paswd : "
                     + password);
            cnt = DriverManager.getConnection(url, userName, password);
            cnt.setAutoCommit(false);

            Util.log("[DataBase->open] Connection : " + cnt);
        } catch (SQLException ex) {
            Util.log("Echec d'ouverture :" + ex.getMessage());
            throw ex;
        }
    }

    /**
     * Fonction qui ouvre la connexion e la base de donnees
     *
     * @param url
     * @param username
     * @param password
     */
    @Override
    public void open(String url, String username, String password,
                     java.util.Properties prop_proxy) throws Exception {
        try {
            Util
                .log("DB " + url + ", user : " + username + "paswd "
                     + password);
            if (prop_proxy != null) {
                prop_proxy.put("user", username);
                prop_proxy.put("password", password);
                Util.log("Connect to DB with properties");
                cnt = DriverManager.getConnection(url, prop_proxy);
            } else
                cnt = DriverManager.getConnection(url, username, password);
            // dmd = cnt.getMetaData();
            // results = new DataSet(dmd.getCatalogs());
            cnt.setAutoCommit(false);
        } catch (SQLException ex) {
            Util.log("Echec d'ouverture :" + ex.getMessage());
            throw ex;
        }
                
    }

    void loadJDBCDriver(String driver) throws Exception {
        Class.forName(driver).newInstance();
    }

    /*
     * Run SQL Query in DataBase
     */
    @Override
    public ResultSet executeQuery(String sql) throws Exception {
        Statement stmt = cnt.createStatement();
        return stmt.executeQuery(sql);
    }

    /*
     * Run SQL Update in DataBase
     */
    @Override
    public void executeUpdate(String sql) throws Exception {
        Statement stmt = cnt.createStatement();
        stmt.executeUpdate(sql);
    }

    /*
     * Compile SQL
     */
    @Override
    public PreparedStatement prepareStatement(String sql) throws Exception {
        PreparedStatement prep = null;
        prep = cnt.prepareStatement(sql);
        return prep;
    }

    @Override
    public void beginTrans() throws SQLException {
        // pSavepoint = cnt.setSavepoint();
        PreparedStatement prep = null;
        // prep = cnt.prepareStatement("BEGIN TRANSACTION");
        prep = cnt.prepareStatement("BEGIN");
        prep.execute();
        // cnt.setTransactionIsolation(Connection.TRANSACTION_REPEATABLE_READ);
    }

    @Override
    public void commit() throws SQLException {
        // cnt.commit();
        PreparedStatement prep = null;
        prep = cnt.prepareStatement("COMMIT");
        prep.execute();
        // pSavepoint = null;
    }

    @Override
    public void rollback() throws SQLException {
        // cnt.rollback();
        PreparedStatement prep = null;
        prep = cnt.prepareStatement("ROLLBACK");
        prep.execute();
        // cnt.rollback(pSavepoint);
    }

    @Override
    public void close() throws Exception {
        cnt.close();
    }

    @Override
    public Connection getCnt() throws Exception {
        return cnt;
    }

    @Override
    public void setCnt(Connection cnt) throws Exception {
        this.cnt = cnt;
    }

    public static void setProxoolMaximumActiveTime(
                                                   String proxoolMaximumActiveTime) throws Exception {
        DataBase.proxoolMaximumActiveTime = proxoolMaximumActiveTime;
    }
}
