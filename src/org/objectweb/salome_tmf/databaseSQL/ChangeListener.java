
/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.IChangeDispatcher;
import org.objectweb.salome_tmf.api.ISafeThread;
import org.objectweb.salome_tmf.api.Util;

public class ChangeListener extends Thread implements ISafeThread{
    private int pid;
    private boolean stop;
    private IChangeDispatcher changeDispatcher;
        
    private int last_pid = - 1;
    private int last_action = -1;
    private Date last_date = Util.getCurrentDate();
    private Time last_time = Util.getCurrentTime();
    private Object jeton = new Object();
    private String projet = null;
        
    private int error = 0;
    public ChangeListener(int _pid, IChangeDispatcher _changeDispatcher){
        pid  = _pid;
        stop = false;
        changeDispatcher = _changeDispatcher;
    }
        
    public void addChange(int code) {
        Util.log("addChange -> projet : " + projet);
        if (projet == null)
            return;
        try {
            synchronized (jeton) {
                //int transNumber = -1;
                try {
                    //transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                    PreparedStatement prep;
                    prep = SQLEngine.getStatement("BEGIN");
                    prep.execute();
                                        
                    prep = SQLEngine.getSQLCommonQuery("updateLastChange");
                    prep.setInt(1, pid);
                    prep.setInt(2, code);
                    prep.setDate(3, Util.getCurrentDate());
                    prep.setTime(4, Util.getCurrentTime());
                    prep.setString(5, projet);
                    SQLEngine.runUpdateQuery(prep);
                                        
                    //SQLEngine.commitTrans(transNumber);
                    prep = SQLEngine.getStatement("COMMIT");
                    prep.execute();
                } catch (Exception e ){
                    //SQLEngine.rollBackTrans(transNumber);
                    try {
                        PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
                        prep.execute();
                    } catch(Exception e2){}
                    throw e;
                }
            }
        }catch (Exception e){
            Util.err(e);
        }
    }
        
    public boolean isChange(){
        int _last_pid = - 1;
        int _last_action = -1;
        Date _last_date = null;
        Time _last_time = null;
        boolean ret = false;
        try {
            if (SQLEngine.isLock){
                return false;
            }
            synchronized (jeton) {
                PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectLastChange");
                prep.setString(1, projet);
                ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                Util.log("Before get change, pid "+ last_pid  + ",action " + last_action + ", Time " + last_date + " : " +  last_time);
                if (stmtRes.next()) {
                    _last_pid = stmtRes.getInt("pid");
                    _last_action = stmtRes.getInt("action_type");
                    _last_date = stmtRes.getDate("action_date");
                    _last_time = stmtRes.getTime("action_time");
                    Util.log("After get change, pid "+ _last_pid  + ",action " + _last_action + ", Time " + _last_date + " : " +  _last_time);
                    if ((_last_pid != pid) && (_last_pid != last_pid || _last_date.after(last_date) || _last_time.after(last_time))){
                        last_pid = _last_pid;
                        last_action =_last_action;
                        last_date = _last_date;
                        last_time = _last_time;
                        ret = true;
                    }
                }
                error = 0;
            }
        }catch (Exception e){
            error ++;
            Util.err(e);
            try{
                if (error > 2) {
                    Util.log("Try to resolve error by unlock");
                    if (Api.getLockMeth() == 1){
                        PreparedStatement prep = SQLEngine.getSQLCommonQuery("unlock");
                        SQLEngine.runUpdateQuery(prep);
                    }
                }
            }catch(Exception e1){
                Util.err(e);
            }  
        }
        return ret;
    }
        
    // updateLastChange pid = ?, action_type = ?, action_date = ?, action_time = ? \  
    @Override
    public void run() {
        while(!stop) {
            if (projet != null) {
                if (isChange()) {
                    Util.log("PID " + last_pid  + " do action " + last_action + " at Time " + last_date + " : " +  last_time);
                    changeDispatcher.setChange();
                    changeDispatcher.notifyObservers(new Integer(last_action));
                }
                if (!stop)
                    waitTime();
            }
        }
    }
        
    @Override
    public synchronized void safe_stop(){
        stop = true;
        //notify();
    }
        
    @Override
    public synchronized void safe_restart(){
        stop = false;
    }
        
    public void setProjet(String _projet){
        projet = _projet;
        try {
            PreparedStatement prep;
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLCommonQuery("countLastChange");
            prep.setString(1, projet);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (!stmtRes.next()){
                last_pid = pid;
                last_action = -1;
                last_date = Util.getCurrentDate();
                last_time = Util.getCurrentTime();
                                
                prep = SQLEngine.getSQLCommonQuery("insertLastChange");
                prep.setInt(1, pid);
                prep.setInt(2, -1);
                prep.setDate(3, last_date);
                prep.setTime(4, last_time);
                prep.setString(5, projet);
                SQLEngine.runUpdateQuery(prep);
                                
                                
            }else {
                isChange();
            }
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
        }catch (Exception e){
            try {
                PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
                prep.execute();
            } catch(Exception e2){}
            Util.err(e);        
            projet = null;
        }
    }
        
    private synchronized void waitTime(){
        try{
            for(int i = 0 ; i < 6 ; i++){
                yield();
                Thread.sleep(10000);
            }
        }catch(Exception e){
            Util.err(e);
        }
    }
}
