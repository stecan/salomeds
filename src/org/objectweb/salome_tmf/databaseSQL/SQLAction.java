/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLAction;

public class SQLAction implements ISQLAction {
        
    ///////////////////////////////////INSERT-ADD/////////////////////////////////////////////

    /**
     * Insert an Action in the table ACTION_TEST for the test identified by idBddTest
     * @param idBddTest
     * @param name : name of the action
     * @param description : description of the action
     * @param awaitedResult : awaited result of the action
     * @return the id of the new action
     * @throws Exception
     * need canCreateTest permission
     */
    @Override
    public int insert (int idBddTest, String name, String description, String awaitedResult) throws Exception {
        int transNumber = -1;
        int id;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLAction : insert -> canCreateTest]");
            }
        }
        if (idBddTest <1) {
            throw new Exception("[SQLAction->insert] test have no id");
        }
        if (SQLObjectFactory.getInstanceOfISQLTest().getTest(idBddTest) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ACTION);
            int nbTestAction = SQLObjectFactory.getInstanceOfISQLManualTest().getNumberOfAction(idBddTest);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addAction"); //OK
                        
            prep.setInt(1,idBddTest);
            prep.setString(2,name);
            prep.setString(3,description);
            prep.setString(4,awaitedResult);
            prep.setInt(5,nbTestAction);//Because index begin at 0
                        
            SQLEngine.runAddQuery(prep);
                        
            id = getID(idBddTest, name);
            if (id <1) {
                throw new Exception("[SQLAction->insert] Action have no id");
            } 
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return id;
    }
    /**
     * insert in table ACTION_PARAM_TEST the use of the parameter identifed by idBddParam for the action idBddAction
     * WARNING This methode don't insert the reference of idBddParam int the table CAS_PARAM_TEST
     * @param idBddAction
     * @param idBddParam
     * @throws Exception
     * @see 
     */
    @Override
    public void addUseParam(int idBddAction, int idBddParam) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLAction : update -> canUpdateTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->addUseParam] Action have no id");
        }
        if (idBddParam <1) {
            throw new Exception("[SQLAction->addUseParam] Param have no id");
        }
        if (getActionWrapper(idBddAction) == null){
            throw new DataUpToDateException();
        }
        if (SQLObjectFactory.getInstanceOfISQLParameter().getParameterWrapper(idBddParam) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_PARAMETER_INTO_ACTION);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addParamToAction"); //OK
            prep.setInt(1,idBddAction);
            prep.setInt(2,idBddParam);
            SQLEngine.runAddQuery(prep);
            
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->addUseParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Insert a file to the action identifeid by idBddAction
     * @param idBddAction
     * @param file
     * @param description
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * no permission needed
     * @see SQLFileAttachment.insert(File, String)
     */
    @Override
    public int addFileAttach(int idBddAction, SalomeFileWrapper file, String description) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idBddAction <1) {
            throw new Exception("[SQLAction->addFileAttach] Action have no id");
        }
        if (file == null) {
            throw new Exception("[SQLAction->addFileAttach] File is null");
        }
        if (getActionWrapper(idBddAction) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToAction"); //ok
            prep.setInt(1,idBddAction);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->addFileAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }
        
    /**
     * Insert a UrlAttachment to the action identifeid by idBddAction
     * Table used are ATTACHEMENT and ACTION_ATTACHEMENT
     * @param idBddAction
     * @param strUrl
     * @param description
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * no permission needed
     * @see SQLUrlAttachment.insert(String, String)
     */
    @Override
    public int addUrlAttach(int idBddAction, String strUrl,  String description) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idBddAction <1) {
            throw new Exception("[SQLAction->addUrlAttach] Action have no id");
        }
        if (strUrl == null) {
            throw new Exception("[SQLAction->addUrlAttach] url is null");
        }
        if (getActionWrapper(idBddAction) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(strUrl, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToAction"); //ok
            prep.setInt(1,idBddAction);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->addUrlAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
        
    }
        
    //////////////////////////////////////UPDATE///////////////////////////////////////////
        
        
    /**
     * Update the information of an action identified by idBddAction in database (ACTION_TEST)
     * @param idBddAction
     * @param newActionName
     * @param newActionDesc
     * @param newActionResAttendu
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void update(int idBddAction, String newActionName, String newActionDesc, String newActionResAttendu) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLAction : update -> canUpdateTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->update] Action have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_ACTION);
            ActionWrapper pAction = getActionWrapper(idBddAction);
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateAction"); //ok
            prep.setString(1, newActionName);
            prep.setString(2, newActionDesc);
            prep.setString(3, newActionResAttendu);
            prep.setInt(4, pAction.getOrder());
            prep.setInt(5, idBddAction);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Increment or decrement the order of the action identified by idBddAction in the test
     * Then, reorder other action to preserve a correct order   
     * @param idBddAction
     * @param inc true for doing a decrementation (+1) or false (-1)
     * @return the new order of the action
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public int updateOrder(int idBddAction, boolean inc) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLAction : updateOrder -> canUpdateTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->updateOrder] Action have no id");
        }
        int orderIndex = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_ACTION);
            ActionWrapper pAction = getActionWrapper(idBddAction);
            int idTest = pAction.getIdTest();
            orderIndex =  pAction.getOrder();
            if (inc){
                int maxOrder = SQLObjectFactory.getInstanceOfISQLManualTest().getNumberOfAction(idTest);
                maxOrder --; //Because index begin at 0
                if (orderIndex < maxOrder) {
                    ActionWrapper pAction2 =SQLObjectFactory.getInstanceOfISQLManualTest().getActionByOrder(idTest, orderIndex + 1);
                    updateOrder(idBddAction, orderIndex + 1);
                    updateOrder(pAction2.getIdBDD(), orderIndex);
                    orderIndex++;
                }
            } else {
                if (orderIndex > 0) {
                    ActionWrapper pAction2 = SQLObjectFactory.getInstanceOfISQLManualTest().getActionByOrder(idTest, orderIndex - 1);
                    updateOrder(idBddAction, orderIndex - 1);
                    updateOrder(pAction2.getIdBDD(), orderIndex);
                    orderIndex--;
                }
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return orderIndex;
    }
        
        
    protected void updateOrder(int idBddAction, int neworder) throws Exception {
        int transNumber = -1;
        if (idBddAction <1) {
            throw new Exception("[SQLAction->updateOrder] Action have no id");
        }
        if (neworder < 0) {
            throw new Exception("[SQLAction->updateOrder] Order is < 0");
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_ACTION);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateActionOrder"); //ok
            prep.setInt(1, neworder);
            prep.setInt(2, idBddAction);
            SQLEngine.runUpdateQuery(prep);
                
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /////////////////////////////////DELETE/////////////////////////////////////////////////////
    /**
     * Delete in database the action identified by idBddAction
     * this delete all Attachemnts, reference of using parameters,
     * and update the order of the actions which are referenced in the same test if reorder = true
     * @param idBddAction
     * @throws Exception
     * @see deleteAllAttachment(int)
     * need permission canDeleteTest
     */
    @Override
    public void delete(int idBddAction) throws Exception {
        delete(idBddAction, true);
    }
        
    /**
     * Delete in database the action identified by idBddAction
     * this delete all Attachemnts, reference of using parameters,
     * and update the order of the actions which are referenced in the same test if reorder = true
     * @param idBddAction
     * @param reorder re-order the actions in the test
     * @throws Exception
     * @see deleteAllAttachment(int)
     * need permission canDeleteTest
     * @TODO SOAP
     */
    @Override
    public void delete(int idBddAction, boolean reorder) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLAction : deleteAttachment -> canDeleteTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->delete] Action have no id");
        }
        try {
            int maxOrder = -1;
            int orderIndex = -1;
            int idTest = -1;
            PreparedStatement prep;
                        
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_ACTION);
                        
            //Get Information about order
            if (reorder){
                ActionWrapper pAction = getActionWrapper(idBddAction);
                orderIndex =  pAction.getOrder();
                idTest = pAction.getIdTest();
                maxOrder = SQLObjectFactory.getInstanceOfISQLManualTest().getNumberOfAction(idTest);
                maxOrder --; //Because index begin at 0
            }
            //delete all attachment
            deleteAllAttachment(idBddAction);
                        
            //delete referenced  paramaters
            ParameterWrapper[] listOfUsedParameter = getParamsUses(idBddAction);
            for (int i = 0; i < listOfUsedParameter.length; i++){
                ParameterWrapper pParameterWrapper = listOfUsedParameter[i];
                int idParameter =  pParameterWrapper.getIdBDD();
                deleteParamUse(idBddAction, idParameter);
            }
                        
            //delete action
            prep = SQLEngine.getSQLDeleteQuery("deleteActionUsingID"); //ok
            prep.setInt(1, idBddAction);
            SQLEngine.runDeleteQuery(prep);
                        
            //Update order
            if (reorder){
                if (orderIndex < maxOrder) {
                    for (int j = orderIndex + 1; j <= maxOrder ; j++){
                        ActionWrapper pAction2 = SQLObjectFactory.getInstanceOfISQLManualTest().getActionByOrder(idTest, j);
                        updateOrder(pAction2.getIdBDD(), j-1);
                    }           
                }
            }
    
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Delete the reference of the use parameter idBddParam in action, but not delete the 
     * parameter to the database
     * @param idBddAction : unique identifier of the action in database
     * @param idBddParam : unique identifier of the parameter in database
     * @throws Exception
     * need permission canDeleteTest
     */
    @Override
    public void deleteParamUse(int idBddAction, int idBddParam) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLAction : deleteAttachment -> canDeleteTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->deleteParamUse] Action have no id");
        }
        if (idBddParam <1) {
            throw new Exception("[SQLAction->deleteParamUse] param have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_PARAMETER_FROM_TEST);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteParamFromAction"); //ok
            prep.setInt(1, idBddAction);
            prep.setInt(2, idBddParam);
            SQLEngine.runDeleteQuery(prep);
                        
            try {
                ActionWrapper pAction = getActionWrapper(idBddAction);
                String awaited = pAction.getAwaitedResult();
                String descrip = pAction.getDescription();
                ParameterWrapper pParameterWrapper = SQLObjectFactory.getInstanceOfISQLParameter().getParameterWrapper(idBddParam);
                awaited = clearStringOfParameter(awaited, pParameterWrapper.getName());
                descrip = clearStringOfParameter(descrip, pParameterWrapper.getName());
                update(idBddAction,pAction.getName(), descrip, awaited );
            } catch(Exception e2) {
                //WARNING
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->deleteParamUse]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all Attachement of an action identied by idBddAction in database
     * Delete reference in table : ATTACHEMENT and ACTION_ATTACHEMENT
     * @param idBddAction
     * @throws Exception
     */
    @Override
    public void deleteAllAttachment(int idBddAction) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLAction : deleteAttachment -> canDeleteTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->deleteAllAttachment] Action have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
            int idAttachFile;
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestAllAttachAction"); //ok
            prep.setInt(1, idBddAction);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep); 
                         
            while  (stmtRes.next()) {
                idAttachFile = stmtRes.getInt("ATTACHEMENT_id_attach");
                                 
                prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromAction"); //ok
                prep.setInt(1, idBddAction);
                prep.setInt(2, idAttachFile);
                SQLEngine.runDeleteQuery(prep);
                                 
                SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttachFile);
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->deleteAllAttachment]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }   
    }
        
    /**
     * Delete Attchement identidied by idBddAttach int ATTACHEMNT table and  reference in ACTION_ATTACHEMENT
     * @param idBddAction : unique identifier of the action in database
     * @param idBddAttach : unique identifier of the attachment in database
     * @throws Exception
     * need permission canDeleteTest
     */
    @Override
    public void deleteAttachment(int idBddAction, int idBddAttach) throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLAction : deleteAttachment -> canDeleteTest]");
            }
        }
        if (idBddAction <1) {
            throw new Exception("[SQLAction->deleteAttachment] Action have no id");
        }
        if (idBddAttach <1) {
            throw new Exception("[SQLAction->deleteAttachment] Attach have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
            // Suppression de l'attachement de la suite de test
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromAction"); //ok
            prep.setInt(1, idBddAction);
            prep.setInt(2, idBddAttach);
            SQLEngine.runDeleteQuery(prep);
                
            // Suppression de l'attachement de la BdD
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idBddAttach);
                
                
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLAction->deleteAttachment]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    ////////////////////////////////////GET/////////////////////////////////////////
        
    /**
     * Return a wrapped action represented by idBddAction in database
     * @param idBddAction : unique identifier of the action in database
     * @return Return a wrapped action represented by idBddAction in database
     * @throws Exception
     * no permission needed 
     */
    @Override
    public ActionWrapper getActionWrapper(int idBddAction) throws Exception {
        ActionWrapper pAction = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectActionUsingID"); //ok
            prep.setInt(1,idBddAction);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep); 
            // Ajoute les element un par un au resultat
            if(stmtRes.next()) {
                pAction = new ActionWrapper();
                pAction.setName(stmtRes.getString("nom_action"));
                pAction.setDescription(stmtRes.getString("description_action"));
                pAction.setIdBDD(stmtRes.getInt("id_action"));
                pAction.setOrder(stmtRes.getInt("num_step_action"));
                pAction.setIdTest(stmtRes.getInt("CAS_TEST_id_cas"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pAction;
    }
        
    /**
     * Return a Vector of ParameterWrapper used by an action identified by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector of java.lang.String contening all parameters names used by this action 
     * @throws Exception
     * no permission needed
     */
    @Override
    public ParameterWrapper[] getParamsUses(int idBddAction) throws Exception {
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectActionParams"); //ok
        prep.setInt(1,idBddAction);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        // Ajoute les elements un par un au resultat
        while  (stmtRes.next()) {
            ParameterWrapper pParameterWrapper = new ParameterWrapper();
            pParameterWrapper.setName(stmtRes.getString("nom_param_test"));
            pParameterWrapper.setIdBDD(stmtRes.getInt("id_param_test"));
            pParameterWrapper.setDescription(stmtRes.getString("desc_param_test"));
            result.addElement(pParameterWrapper);
        }
        ParameterWrapper[] pwArray = new ParameterWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            pwArray[i] = (ParameterWrapper) result.get(i);
        }
        return pwArray;
    }
        
    /**
     * Get all FileAttachment of an Action idntifed by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector contening all FileAttachment (wrapped by FileAttachementWrapper)  of action idBddActio
     * @throws Exception
     * @see org.objectweb.salome_tmf.api.data.FileAttachementWrapper
     * no permission needed
     */
    @Override
    public FileAttachementWrapper[] getAllAttachFile(int idBddAction) throws Exception {
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectActionAttachFiles"); //ok
        prep.setInt(1,idBddAction);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
                        
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
                        
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     * Get all UrlAttachment of an Action idntifed by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector contening all UrlAttachment (wrapped by UrlAttachementWrapper)  of action idBddAction
     * @throws Exception
     * @see org.objectweb.salome_tmf.api.data.UrlAttachementWrapper
     * no permission needed
     */
    @Override
    public UrlAttachementWrapper[] getAllAttachUrl(int idBddAction) throws Exception {
        Vector result = new Vector();
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectActionAttachUrls"); //ok
        prep.setInt(1,idBddAction);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }
        
    /**
     * Return the Id of an action called name in the test identified by testId
     * @param testId
     * @param actionName
     * @return the Id of an action called name in the test identified by testId
     * @throws Exception
     * no permission needed
     */
    @Override
    public int getID(int testId, String actionName) throws Exception {
        int idAction = -1;
                
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdAction"); //ok
            prep.setString(1,actionName);
            prep.setInt(2,testId);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if (stmtRes.next()){
                idAction = stmtRes.getInt("id_action");
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idAction;
    }

    protected String clearStringOfParameter(String prtString, String paramName) {
        String result = prtString;
        result = result.replaceAll("[$]" + paramName + "[$]", "");
        return result;
    }
}
