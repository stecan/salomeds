/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.net.InetAddress;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Time;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ConnectionWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLSession;

public class SQLSession implements ISQLSession{
        
    /**
     * Insert a session on table SESSION
     * @param project
     * @param login 
     * @throws Exception
     */
    @Override
    public int addSession(String project, String login) throws Exception {
        int idSession = -1;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            Date dateActuelle = Util.getCurrentDate();
            Time heureActuelle = Util.getCurrentTime();
            String host = InetAddress.getLocalHost().getCanonicalHostName();
            Util.log("[SQLSession->addSession] " + project + ", " + login + ", " + host + ", " + dateActuelle +  ", " + heureActuelle);
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("insertConnection"); //ok
            prep.setString(1, project);
            prep.setString(2, login);
            prep.setString(3, host);
            prep.setDate(4, dateActuelle);
            prep.setTime(5, heureActuelle);
            SQLEngine.runAddQuery(prep);
                        
            idSession = getID(project, login, host, dateActuelle, heureActuelle);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.log("[SQLSession->addSession]" + e);
            Util.err(e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idSession;
    }
        
    int getID(String project, String login, String host, Date date, Time time) throws Exception {
        int idSession = -1;
        PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectConnection"); //ok
        prep.setString(1, project);
        prep.setString(2, login);
        prep.setString(3, host);
        prep.setDate(4, date);
        prep.setTime(5, time);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()){
            idSession = stmtRes.getInt("id_connection");
        }
        return idSession;
    }
        
    /**
     * Delete a session id in table SESSION
     * @param id
     * @throws Exception
     */
    @Override
    public void deleteSession(int id) throws Exception {
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("deleteAConnection"); //ok
            prep.setInt(1, id);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.log("[SQLSession->deleteSession]" + e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete all session in table SESSION
     * @throws Exception
     */
    @Override
    public void deleteAllSession() throws Exception {
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.COMMON_REQ);
                        
            PreparedStatement prep = SQLEngine.getSQLCommonQuery("deleteAllConnections"); //ok
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.log("[SQLSession->deleteAllSession]" + e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get a vector of ConnectionWrapper representing all session in the databse  (table SESSION)
     * @return
     * @throws Exception
     */
    @Override
    public ConnectionWrapper[] getAllSession() throws Exception {
        Vector res = new Vector();
        PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectAllConnections"); //ok
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            ConnectionWrapper pConnectionWrapper = new ConnectionWrapper();
            pConnectionWrapper.setId(stmtRes.getInt("id_connection"));
            pConnectionWrapper.setProjectConnected(stmtRes.getString("project_connec"));
            pConnectionWrapper.setLoginConnected(stmtRes.getString("login_connec"));
            pConnectionWrapper.setHostConnected(stmtRes.getString("host_connec"));
            pConnectionWrapper.setDateConnected(stmtRes.getDate("date_connec"));
            pConnectionWrapper.setTimeConnected(stmtRes.getTime("hour_connec").getTime());
            res.add(pConnectionWrapper);
        }
        ConnectionWrapper[] cwArray = new ConnectionWrapper[res.size()];
        for(int i = 0; i < res.size(); i++) {
            cwArray[i] = (ConnectionWrapper) res.get(i);
        }
                
        return cwArray;
    } 
        
    /**
     *  Get a ConnectionWrapper representing a session idSession in the databse  (table SESSION)
     * @param idSession
     * @return
     * @throws Exception
     */
    @Override
    public ConnectionWrapper getSession(int idSession)  throws Exception {
        ConnectionWrapper pConnectionWrapper = null;
        PreparedStatement prep = SQLEngine.getSQLCommonQuery("selectConnectionInfos"); //ok
        prep.setInt(1, idSession);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if  (stmtRes.next()) {
            pConnectionWrapper = new ConnectionWrapper();
            pConnectionWrapper.setId(stmtRes.getInt("id_connection"));
            pConnectionWrapper.setProjectConnected(stmtRes.getString("project_connec"));
            pConnectionWrapper.setLoginConnected(stmtRes.getString("login_connec"));
            pConnectionWrapper.setHostConnected(stmtRes.getString("host_connec"));
            pConnectionWrapper.setDateConnected(stmtRes.getDate("date_connec"));
            pConnectionWrapper.setTimeConnected(stmtRes.getTime("hour_connec").getTime());
        }
        return pConnectionWrapper;
    }
}
