/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLScript;


public class SQLScript implements ISQLScript{
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped)
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    @Override
    public int insert(String name, String arg1, String extension, String type) throws Exception {
        int idScript = -1;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_SCRIPT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addScriptToTest");
            prep.setString(1, name);
            prep.setString(2, arg1);
            prep.setString(3, extension);
            prep.setString(4, type);
            SQLEngine.runAddQuery(prep);
            idScript = getLastIdAttach();
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
                
        return  idScript;
    }
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped) for an Environnement
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * @param idEnv : id of the Environnement mapped with the script
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    @Override
    public int insert(String name, String arg1, String extension, String type, int idEnv) throws Exception {
        int idScript = -1;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_SCRIPT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("attachScriptToEnvironment"); //ok
            prep.setString(1, name);
            prep.setString(2, arg1);
            prep.setString(3, extension);
            prep.setString(4, type);
            prep.setInt(5, idEnv);
            SQLEngine.runAddQuery(prep);
            idScript = getLastIdAttach();
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
                
        return  idScript;
    }
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped) for an EXECUTION
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * @param idEnv : id of the Environnement mapped with the script
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    @Override
    public int insert(int idExec, String name, String arg1, String extension, String type) throws Exception {
        if (idExec <1 ) {
            throw new Exception("[SQLScript->insert] entry data are not valid");
        }
        int idScript = -1;
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_SCRIPT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("attachScriptToExecution"); //ok
            prep.setString(1, name);
            prep.setString(2, arg1);
            prep.setString(3, extension);
            prep.setString(4, type);
            prep.setInt(5, idExec);
                        
            SQLEngine.runAddQuery(prep);
            idScript = getLastIdAttach();
            if (idScript <1 ) {
                throw new Exception("[SQLScript->insert] id is not valid");
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
                
        return  idScript;
    }
        
    /**
     * Map the attachment attachId to the script scriptId in the table SCRIPT_ATTACHEMENT
     * @param scriptId
     * @param attachId
     * @throws Exception
     */
    @Override
    public void addAttach(int scriptId, int attachId) throws Exception {
        if (scriptId <1 || attachId < 1) {
            throw new Exception("[SQLScript->addAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_ATTACHMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToScript"); //ok
            prep.setInt(1,scriptId);
            prep.setInt(2,attachId);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->addAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Update the length of the script idScript in the database
     * @param idScript
     * @param length
     * @throws Exception
     * no permission needed
     */
    @Override
    public void  updateLength(int idScript, long length)throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->updateLength] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
            int idAttach = getScriptIdAttach(idScript);
            if (idAttach != -1){
                SQLObjectFactory.getInstanceOfISQLFileAttachment().updateFileLength(idAttach, length);
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->updateLength]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Update the date of the script idScript in the database
     * @param idScript
     * @param date
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updateDate(int idScript, Date date) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->updateDate] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
                        
            int idAttach = getScriptIdAttach(idScript);
            if (idAttach != -1){
                SQLObjectFactory.getInstanceOfISQLFileAttachment().updateFileDate(idAttach, date);
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->updateDate]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Update the argument reserved for plugin in the script idScript in the database (classe_autom_script)
     * @param idScript
     * @param arg
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updatePlugArg(int idScript, String arg) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->updatePlugArg] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
                        
            PreparedStatement prep =  SQLEngine.getSQLUpdateQuery("updateScriptClassToBeExecuted");
            prep.setString(1, arg);
            prep.setInt(2, idScript);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->updatePlugArg]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
        
    /**
     * Update the conetent of the script id with the content of the file with path filePath
     * @param idScript
     * @param filePath
     * @throws Exception
     * @see ISQLFileAttachment.updateFileContent(int,BufferedInputStream);
     * no permission needed
     */
    @Override
    public void updateContent(int idScript, String filePath) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->updateContent] entry data are not valid");
        }
        int transNumber = -1;
        BufferedInputStream bis = null ;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
                        
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            byte[] fileContent = null;
            bis.read(fileContent);
                        
            int scriptAttachId = getScriptIdAttach(idScript); 
            SQLObjectFactory.getInstanceOfISQLFileAttachment().updateFileContent(scriptAttachId,fileContent);
            bis.close();        
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->updateContent]", e);
            SQLEngine.rollBackTrans(transNumber);
            if (bis != null){
                bis.close();
            }
            throw e;
        }       
    }
        
    /**
     * Update the script attachement (content, date size, and name) In Database
     * @param idScript
     * @param filePath
     * @throws Exception 
     * @see ISQLFileAttachment.updateFile(int, File);
     * no permission needed
     */
    @Override
    public void update(int idScript, String filePath) throws Exception {
        update(idScript, new SalomeFileWrapper(new File(filePath)));
    }
        
    /**
     * Update the script attachement (content, date size, and name) In Database
     * @param idScript
     * @param File
     * @throws Exception 
     * @see ISQLFileAttachment.updateFile(int, File);
     * no permission needed
     */
    @Override
    public void update(int idScript, SalomeFileWrapper file) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->update] entry data are not valid");
        }
        if (file == null || file.exists() == false ) {
            throw new Exception("[SQLScript->update] entry data are not valid");
        } 
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
            int scriptAttachId = getScriptIdAttach(idScript); 
            SQLObjectFactory.getInstanceOfISQLFileAttachment().updateFile(scriptAttachId, file);
                        
            PreparedStatement prep =  SQLEngine.getSQLUpdateQuery("updateScriptName"); //ok
            prep.setString(1, file.getName());
            prep.setInt(2, idScript);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.err("[SQLScript->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * Update the Script name the table SCRIPT and ATTACHEMENT
     * @param idScript
     * @param name
     * @throws Exception
     * @see ISQLFileAttachment.updateFileName(int, String);
     * no permission needed
     */
    @Override
    public void updateName(int idScript, String name) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->updateName] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.UPDATE_ATTACHMENT);
                        
            int scriptAttachId = getScriptIdAttach(idScript); 
            SQLObjectFactory.getInstanceOfISQLFileAttachment().updateFileName(scriptAttachId, name);
                        
            PreparedStatement prep =  SQLEngine.getSQLUpdateQuery("updateScriptName"); //ok
            prep.setString(1, name);
            prep.setInt(2, idScript);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->updateName]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
    /**
     * Delete the script and mapped file in the tables ( SCRIPT, SCRIPT_ATTACHEMENT, ATTACHEMENT)
     * @param idScript
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment().delete(int)
     */
    @Override
    public void delete(int idScript) throws Exception {
        int transNumber = -1;
        if (idScript <1) {
            throw new Exception("[SQLScript->delete] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_SCRIPT);
                        
            int idAttach = getScriptIdAttach(idScript);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromScript"); //ok
            prep.setInt(1, idScript);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);
                        
            prep = SQLEngine.getSQLDeleteQuery("deleteScriptFromDB");
            prep.setInt(1, idScript);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLScript->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }       
    }
        
    /**
     * @return the last id of an attachment in the table SCRIPT_ATTACHEMENT
     * @throws Exception
     */
    @Override
    public int getLastIdAttach() throws Exception {
        int maxIdAttach = -1;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectMaxIdScript"); //ok
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()){
            maxIdAttach = stmtRes.getInt("max_id_script");
        } else {
            throw new Exception("[SQLScript->getLastIdAttach] id not exist");
        }
        return maxIdAttach;
    }
        
    /**
     * @param idScript : id of the script in the database
     * @return the id of the attachment mapped to the script in the database
     * @throws Exception
     */
    @Override
    public int getScriptIdAttach(int idScript) throws Exception{
        if (idScript <1) {
            throw new Exception("[SQLScript->getScriptIdAttach] entry data are not valid");
        }
        int fileAttachId = -1;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectScriptAttachId"); //ok
        prep.setInt(1,idScript);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()){
            fileAttachId = stmtRes.getInt("ATTACHEMENT_id_attach");
        }
        return fileAttachId;
    }
        
    /**
     * @param idScript : id of the script in the database
     * @return a File attached to the script
     * @throws Exception
     * @see ISQLFileAttachment.getFile(int)
     */
    @Override
    public SalomeFileWrapper getFile(int idScript) throws Exception {
        if (idScript <1) {
            throw new Exception("[SQLScript->getFile] entry data are not valid");
        }
        SalomeFileWrapper f = null;
        int fileAttachId = getScriptIdAttach(idScript);
        f = SQLObjectFactory.getInstanceOfISQLFileAttachment().getFile(fileAttachId);
        return f;
    }
        
    /**
     * @param idScript : id of the script in the database
     * @return a File attached to the script
     * @throws Exception
     * @see ISQLFileAttachment.getFile(int)
     */
    /*public SalomeFileWrapper getFile(int idScript) throws Exception {
      if (idScript <1) {
      throw new Exception("[SQLScript->getFile] entry data are not valid");
      }
      SalomeFileWrapper f = null;
      int fileAttachId = getScriptIdAttach(idScript);
      f = SQLObjectFactory.getInstanceOfISQLFileAttachment().getFile(fileAttachId);
      return f;
      }*/
}
