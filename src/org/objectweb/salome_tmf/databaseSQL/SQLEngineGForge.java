package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;

import org.objectweb.salome_tmf.api.Util;

public class SQLEngineGForge extends SQLEngine {
    static DataBase db_gforge; 
        
    static String driver_postgres = "org.postgresql.Driver";
    static String gforge_host = "g-salome.rd.francetelecom.fr"; 
    static String gforge_bdd = "gforge";        
    static String gforge_user = "salome";
    static String gforge_pwd = "sa10me" ;
        
        
    static void initSQLEngine(DataBase database, boolean doLockOnQuery, int locktype) throws Exception{
        SQLEngine.initSQLEngine(database, doLockOnQuery, null, locktype);
        initGForgeDB();
    }
        
    static void initGForgeDB() throws Exception {
        String URLDB_Gforge = "jdbc:postgresql://"+gforge_host+"/"+ gforge_bdd;
        if (db_gforge == null){
            db_gforge = new DataBase(driver_postgres);
            db_gforge.open(URLDB_Gforge, gforge_user, gforge_pwd);
        }
        //Util.debug("GFORGE DB = "+ db_gforge);
                
    }
    static void initDB(DataBase database) throws Exception{
        if (database == null){
            throw new Exception("[SQLEngine:initSQLEngine] Database is null");
        }
        db = database;
        initGForgeDB();
    }
        
    static PreparedStatement getSQLSelectGForgeQuery (String queryName)  throws Exception{
        if (db_gforge == null){
            initGForgeDB();
        }
        Util.log("[SQLEngine->getSQLSelectQuery]" + queryName);
        String sql = selectSqlQuery.getProperty(queryName);
        Util.log("[SQLEngine->getSQLSelectQuery] sql is " + sql);
        PreparedStatement prep = db_gforge.prepareStatement(sql);
        return prep;
    }
        
    @Override
    public void close() throws Exception{
        Util.log("[SQLEngine->close] close connection " + db.cnt);
        //Util.log("[SQLEngine->close] close connection " + db_gforge.cnt);
        db.cnt.close(); 
        db = null;
        closeGforgeDB();
    }
        
    static void closeGforgeDB() throws Exception{
        //Util.log("[SQLEngine->close] close connection " + db_gforge.cnt);
        if (db_gforge!=null){
            db_gforge.cnt.close();
            db_gforge = null;
        }
    }
        
}
