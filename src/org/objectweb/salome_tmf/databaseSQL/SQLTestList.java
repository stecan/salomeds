/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLTestList;

public class SQLTestList implements ISQLTestList {
        
    /**
     * Insert a TestList for the family idFamily
     * @param idFamily
     * @param name of the TestList
     * @param description of the TestList
     * @return the id of the new TestList
     * @throws Exception
     * need permission canCreateTest
     */
    @Override
    public int insert (int idFamily, String name, String description) throws Exception {
        int suiteId = -1;
        if ( idFamily <1 ) {
            throw new Exception("[SQLTestList->insert] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLTestList : insert -> canCreateTest]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLFamily().getFamily(idFamily) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_SUITE);
            //PreparedStatement prep2 = SQLEngine.getStatement("LOCK TABLES  SUITE_TEST WRITE");
            //SQLEngine.runSelectQuery(prep2);
            int bddOrder = SQLObjectFactory.getInstanceOfISQLFamily().getNumberOfTestList(idFamily);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addSuite"); //ok
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3, idFamily);
            prep.setInt(4, bddOrder);//Because index begin at 0
            SQLEngine.runAddQuery(prep);
                        
            suiteId = getID(idFamily, name);
            if ( suiteId <1 ) {
                throw new Exception("[SQLTestList->insert] id is not valid");
            }
            //prep2 = SQLEngine.getStatement("UNLOCK TABLES;");
            //SQLEngine.runSelectQuery(prep2);
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return suiteId;  
                
    }
        
    /**
     * Attach a file to the TestList identified by idSuite (Table SUITE_ATTACHEMENT)
     * @param idSuite
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idSuite, SalomeFileWrapper f, String description) throws Exception {
        if ( idSuite <1  || f == null) {
            throw new Exception("[SQLTestList->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getTestList(idSuite) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(f,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToSuite"); //ok
            prep.setInt(1,idSuite);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }
        
    /**
     * Attach an Url to the TestList identified by idSuite (Table SUITE_ATTACHEMENT)
     * @param idSuite
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idSuite, String url, String description) throws Exception {
        if ( idSuite <1  || url == null) {
            throw new Exception("[SQLTestList->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getTestList(idSuite) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToSuite"); //ok
            prep.setInt(1,idSuite);
            prep.setInt(2,idAttach);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;        
    }
        
    /**
     * Update the name and the description of the TestList identified by idSuite
     * @param idSuite
     * @param name
     * @param description
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void update(int idSuite, String name, String description)throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->update] entry data are not valid");
        }
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTestList : update -> canUpdateTest]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_SUITE);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateSuite"); //ok
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3, idSuite);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    void updateOrder(int idSuite, int order) throws Exception {
        if ( idSuite <1  || order < 0 ) {
            throw new Exception("[SQLTestList->updateOrder] entry data are not valid");
        }
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_SUITE);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateSuiteOrder"); //ok
            prep.setInt(1, order);
            prep.setInt(2, idSuite);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
                        
        } catch (Exception e ){
            Util.err("[SQLTestList->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Increment or decrement the order of the TestList identified by idSuite in the Family
     * Then, reorder other estList to preserve a correct order   
     * @param idSuite
     * @param increment  true (+1) or false (-1)
     * @return the new order
     * @throws Exception
     * no permission needed
     */
    @Override
    public int updateOrder(int idSuite, boolean increment) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->updateOrder] entry data are not valid");
        }
        int transNumber = -1;
        int order = -1; 
        /*if (!SQLEngine.specialAllow) {
          if (!Permission.canUpdateTest()){
          throw new SecurityException("[SQLTestList : updateOrder -> canUpdateTest]");
          }
          }*/
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_SUITE);
                        
            SuiteWrapper pSuite = getTestList(idSuite);
            order =  pSuite.getOrder();
            if (increment){
                int maxOrder = SQLObjectFactory.getInstanceOfISQLFamily().getNumberOfTestList(pSuite.getIdFamille());
                maxOrder --; //Because index begin at 0
                if (order < maxOrder) {
                    SuiteWrapper pSuite2 = SQLObjectFactory.getInstanceOfISQLFamily().getTestListByOrder(pSuite.getIdFamille(), order + 1);
                    updateOrder(idSuite, order + 1);
                    updateOrder(pSuite2.getIdBDD(), order);
                    order++;
                }
            } else {
                if (order > 0) {
                    SuiteWrapper pSuite2 = SQLObjectFactory.getInstanceOfISQLFamily().getTestListByOrder(pSuite.getIdFamille(), order - 1);
                    updateOrder(idSuite, order - 1);
                    updateOrder(pSuite2.getIdBDD(), order);
                    order--;
                }
            }  
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return  order;
    }
        
    /**
     * Delete the TestList identified by idSuite in the database
     * then all attachments and all tests in the testlist, and reoder the other testlist in the family 
     * @param idSuite
     * @throws Exception
     * @see ISQLTest.delete(int)
     * need permission canDeleteTest (do a special allow)
     */
    @Override
    public void delete(int idSuite) throws Exception {
        delete(idSuite, true);
    }
        
    /**
     * Delete the TestList identified by idSuite in the database
     * then all attachments and all tests in the testlist, and reoder the other testlist in the family if reorder = true
     * @param idSuite
     * @param reorder re-order testlist in the family
     * @throws Exception
     * @see ISQLTest.delete(int)
     * need permission canDeleteTest (do a special allow)
     * @TDOD SOAP
     */
    @Override
    public void delete(int idSuite, boolean reorder) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->delete] entry data are not valid");
        }
        int transNumber = -1;
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLTestList : delete -> canDeleteTest]");
            }
            //Require specialAllow
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        
        }
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_SUITE);
            int maxOrder = -1;
            int order = -1;
            SuiteWrapper pSuite = null;
            if (reorder) {
                pSuite = getTestList(idSuite);
                order =  pSuite.getOrder();
                maxOrder = SQLObjectFactory.getInstanceOfISQLFamily().getNumberOfTestList(pSuite.getIdFamille());
                maxOrder --; //Because index begin at 0
            }
            //Begin delete
                        
            //Delete all Test
            TestWrapper[] testsList = getTestsWrapper(idSuite);
            for (int i = 0; i < testsList.length; i++){
                TestWrapper pTest = testsList[i];
                //SQLObjectFactory.getInstanceOfISQLTest().delete(pTest.getIdBDD());
                SQLObjectFactory.getInstanceOfISQLTest().delete(pTest.getIdBDD(), false); //NO reorder
            }
                        
            //Delete Attachemnt
            deleteAllAttach(idSuite);
                        
            //Delete the test suite
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteSuiteUsingID"); //ok
            prep.setInt(1, idSuite);
            SQLEngine.runDeleteQuery(prep);
                        
            //Update Order
            if (reorder){
                if (order < maxOrder) {
                    for (int i = order + 1; i <= maxOrder ; i++){
                        SuiteWrapper pSuite2 = SQLObjectFactory.getInstanceOfISQLFamily().getTestListByOrder(pSuite.getIdFamille(), i);
                        updateOrder(pSuite2.getIdBDD(), i-1);
                    }
                }
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLTestList->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }
        
    /**
     * Delete all attchements of the TestList identified by idSuite
     * @param idSuite
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
                        
            AttachementWrapper[] attachList = getAllAttachemnt(idSuite);
            for (int i = 0 ; i < attachList.length; i++){
                AttachementWrapper pAttachementWrapper = attachList[i];
                deleteAttach(idSuite, pAttachementWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete an attchement idAttach of the TestList identified by idSuite
     * @param idSuite
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    @Override
    public void deleteAttach(int idSuite, int idAttach) throws Exception {
        if ( idSuite <1 || idAttach < 1 ) {
            throw new Exception("[SQLTestList->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
                        
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromSuite"); //ok
            prep.setInt(1, idSuite);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLTestList->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the testlist identified by idSuite
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAllAttachemnt(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getAllAttachemnt] entry data are not valid");
        }
        FileAttachementWrapper[] fileList =  getAllAttachFiles(idSuite);
        UrlAttachementWrapper[] urlList = getAllAttachUrls(idSuite);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
                
        return result;
    }
        
    /**
     * Get a Vector of FileAttachementWrapper for the testlist identified by idSuite
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAllAttachFiles(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getAllAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteAttachFiles"); //ok
        prep.setInt(1, idSuite);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach")); 
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     * Get a Vector of UrlAttachementWrapper for the testlist identified by idSuite
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAllAttachUrls(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteAttachUrls"); //ok
        prep.setInt(1, idSuite);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }
        
    /**
     * Get the id of the TestList name in the family idFamily
     * @param idFamily
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idFamily, String name) throws Exception {
        if ( idFamily <1 ) {
            throw new Exception("[SQLTestList->getID] entry data are not valid");
        }
                
        int idSuite = -1;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdSuite"); //ok
            prep.setString(1,name);
            prep.setInt(2,idFamily);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()){
                idSuite = stmtRes.getInt("id_suite");
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idSuite;
    }
        
    /**
     * Get the number of test in the TestList identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    @Override
    public int getNumberOfTest(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getNumberOfTest] entry data are not valid");
        }
        int result = 0;
        //???? result = -1 in old version of the api (dans tous les NumberOf) -> see //because Index begin at 0
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteTests"); //ok
        prep.setInt(1,idSuite);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            result ++;
        }
        return result;
    }
        
    /**
     * Get TestWrapper representing the test at order in the TestList identified by idSuite
     * @param idSuite
     * @param order
     * @return
     * @throws Exception
     */
    @Override
    public TestWrapper getTestByOrder(int idSuite, int order) throws Exception {
        if ( idSuite <1 || order < 0) {
            throw new Exception("[SQLTestList->getTestByOrder] entry data are not valid");
        }
        TestWrapper pTest = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestByOrder"); //ok
        prep.setInt(1,idSuite);
        prep.setInt(2,order);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if(stmtRes.next()) {
            pTest = new TestWrapper();
            pTest.setName(stmtRes.getString("nom_cas"));
            pTest.setDescription(stmtRes.getString("description_cas"));
            pTest.setIdBDD(stmtRes.getInt("id_cas"));
            pTest.setOrder(stmtRes.getInt("ordre_cas"));  
            pTest.setIdSuite(stmtRes.getInt("SUITE_TEST_id_suite"));
            pTest.setType(stmtRes.getString("type_cas"));       
        }
        return pTest;
    }
        
    /**
     * Get SuiteWrapper  representing the TestList identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    @Override
    public SuiteWrapper getTestList(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getTestList] entry data are not valid");
        }
        SuiteWrapper pSuite = null;
                
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteUsingID"); //ok
            prep.setInt(1,idSuite);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()){
                pSuite = new SuiteWrapper();
                pSuite.setName(stmtRes.getString("nom_suite"));
                pSuite.setDescription(stmtRes.getString("description_suite"));
                pSuite.setIdBDD(stmtRes.getInt("id_suite"));
                pSuite.setOrder(stmtRes.getInt("ordre_suite"));
                pSuite.setIdFamille(stmtRes.getInt("FAMILLE_TEST_id_famille"));
            }   SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pSuite;
    }
        
    /**
     * Get a vector of TestWrapper (ManualTestWrapper or AutomaticTestWrapper) 
     * representing all tests in the suite identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    @Override
    public TestWrapper[] getTestsWrapper(int idSuite) throws Exception {
        if ( idSuite <1 ) {
            throw new Exception("[SQLTestList->getTestsWrapper] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteTests"); //ok
        prep.setInt(1,idSuite);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            TestWrapper test;
            String type_test = stmtRes.getString("type_cas");
            //                  if (type_test.equals(ApiConstants.MANUAL)){
            //                          test = new ManualTestWrapper();
            //                  } else {
            //                          test = new AutomaticTestWrapper();
            //                          ((AutomaticTestWrapper)test).setTestExtension(stmtRes.getString("plug_ext"));
            //                  }
            test = new TestWrapper();
            test.setTestExtension(stmtRes.getString("plug_ext"));
                        
            test.setName(stmtRes.getString("nom_cas"));
            test.setDescription(stmtRes.getString("description_cas"));
            test.setConceptor(SQLObjectFactory.getInstanceOfISQLPersonne().getTwoName(stmtRes.getInt("PERSONNE_id_personne")));
            test.setCreationDate(stmtRes.getDate("date_creation_cas"));
            test.setIdBDD(stmtRes.getInt("id_cas"));
            test.setOrder(stmtRes.getInt("ordre_cas"));
            test.setIdSuite(stmtRes.getInt("SUITE_TEST_id_suite"));
            test.setType(stmtRes.getString("type_cas"));        
            result.addElement(test);
        }
        TestWrapper[] twArray = new TestWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            twArray[i] = (TestWrapper) result.get(i);
        }
        return twArray;
    }
}
