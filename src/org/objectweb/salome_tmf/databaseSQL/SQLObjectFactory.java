/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;


import java.sql.PreparedStatement;

import org.objectweb.salome_tmf.api.IChangeDispatcher;
import org.objectweb.salome_tmf.api.ISafeThread;
import org.objectweb.salome_tmf.api.sql.IDataBase;
import org.objectweb.salome_tmf.api.sql.ISQLAction;
import org.objectweb.salome_tmf.api.sql.ISQLAttachment;
import org.objectweb.salome_tmf.api.sql.ISQLAutomaticTest;
import org.objectweb.salome_tmf.api.sql.ISQLCampaign;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.api.sql.ISQLDataset;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLEnvironment;
import org.objectweb.salome_tmf.api.sql.ISQLExecution;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionActionResult;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionResult;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionTestResult;
import org.objectweb.salome_tmf.api.sql.ISQLFamily;
import org.objectweb.salome_tmf.api.sql.ISQLFileAttachment;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;
import org.objectweb.salome_tmf.api.sql.ISQLManualTest;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.api.sql.ISQLParameter;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLProject;
import org.objectweb.salome_tmf.api.sql.ISQLSalomeLock;
import org.objectweb.salome_tmf.api.sql.ISQLScript;
import org.objectweb.salome_tmf.api.sql.ISQLSession;
import org.objectweb.salome_tmf.api.sql.ISQLTest;
import org.objectweb.salome_tmf.api.sql.ISQLTestList;
import org.objectweb.salome_tmf.api.sql.ISQLUrlAttachment;

public class SQLObjectFactory implements ISQLObjectFactory {
        
    static SQLManualTest pISQLManualTest = null;
    static SQLAction pISQLAction = null;
    static ISQLParameter pISQLParameter = null;
    static ISQLAttachment pISQLAttachment = null;
    static ISQLUrlAttachment pISQLUrlAttachment = null;
    static ISQLFileAttachment pISQLFileAttachment  = null;
    static ISQLTestList pISQLTestList = null;
    static ISQLAutomaticTest pISQLAutomaticTest = null;
    static ISQLCampaign pISQLCampaign = null;
    static ISQLScript pISQLScript = null;
    static ISQLPersonne pISQLPersonne = null;
    static ISQLFamily pISQLFamily = null;
    static ISQLTest pISQLTest = null;
    static ISQLProject pISQLProject = null;
    static ISQLEnvironment pISQLEnvironment = null;
    static ISQLExecution pISQLExecution = null;
    static ISQLExecutionResult pISQLExecutionResult = null;
    static ISQLExecutionTestResult pISQLExecutionTestResult = null;
    static ISQLExecutionActionResult pISQLExecutionActionResult = null;
    static ISQLDataset pISQLDataset = null;
    static SQLEngine pSQLEngine= null;
    static ChangeListener pChangeListener = null;
    static ISQLSession pISQLSession = null;
    protected static ISQLGroup pSQLGroup = null;
    static protected ISQLConfig pSQLConfig = null;
    static ISQLSalomeLock pSQLSalomeLock = null;
        
    static int projectID = 0;
    static int personneID = 0;
    DataBase pDB;
        
    static String version = "3.0";
        
        
    @Override
    public ISQLEngine getInstanceOfSQLEngine(String url, String username, String password, IChangeDispatcher pIChangeDispatcher, int pid, String driverJDBC, int locktype) throws Exception {
        if (pSQLEngine == null){
            pDB = new DataBase(driverJDBC);
            pDB.open(url, username, password);
            pSQLEngine = new SQLEngine();
            SQLEngine.initSQLEngine(pDB, true, null, locktype);
            autoUpdateDB();
            pChangeListener = new ChangeListener(pid, pIChangeDispatcher);
        } else {
            pDB = new DataBase(driverJDBC);
            pDB.open(url, username, password);
            SQLEngine.initDB(pDB);
        }
        return pSQLEngine;
    }
        
    void autoUpdateDB() throws Exception {
        getInstanceOfISQLConfig();
        String salome_version = pSQLConfig.getSalomeConf("VERSION");
        if (salome_version != null ){
            //Float floatVersion = Float.valueOf("salome_version");
            version = salome_version;
        } else {
            pSQLConfig.insertSalomeConf("VERSION", version);
            //Update DataBase 2.3 -> 3.0
            PreparedStatement statement = SQLEngine.getSQLCommonQuery("addAssignedField");
            SQLEngine.runUpdateQuery(statement);
        }
    }
        
        
    @Override
    public String getSalomeVersion() {
        return version +"[JDBC]";
    }
        
    @Override
    public ISQLEngine getCurrentSQLEngine() {
        return pSQLEngine;
    }
        
    @Override
    public IDataBase getInstanceOfDataBase(String driver) throws Exception{
        DataBase db =  new DataBase(driver);
        return db;
    }
        
    @Override
    public IDataBase getInstanceOfSalomeDataBase()throws Exception{             
        return pDB;
    }
        
    @Override
    public ISafeThread getChangeListener(String projet) {
        pChangeListener.setProjet(projet);
        return pChangeListener;
    }
        
    @Override
    public void changeListenerProject(String projet){
        pChangeListener.setProjet(projet);
    }
        
        
    @Override
    public ISQLSalomeLock getISQLSalomeLock(){
        return getInstanceOfISQLSalomeLock();
    }
        
    static  ISQLSalomeLock getInstanceOfISQLSalomeLock() {
        if (pSQLSalomeLock == null){
            pSQLSalomeLock = new SQLSalomeLock();
        }
        return pSQLSalomeLock;
    }
        
    @Override
    public ISQLConfig getISQLConfig(){
        return getInstanceOfISQLConfig();
    }
    static  ISQLConfig getInstanceOfISQLConfig() {
        if (pSQLConfig == null){
            pSQLConfig = new SQLConfig();
        }
        return pSQLConfig;
    }
        
    @Override
    public ISQLGroup getISQLGroup(){
        return getInstanceOfISQLGroup();
    }
    static  ISQLGroup getInstanceOfISQLGroup() {
        if (pSQLGroup == null){
            pSQLGroup = new SQLGroup();
        }
        return pSQLGroup;
    }
        
    @Override
    public ISQLSession getISQLSession(){
        return getInstanceOfISQLSession();
    }
    static  ISQLSession getInstanceOfISQLSession() {
        if (pISQLSession == null){
            pISQLSession = new SQLSession();
        }
        return pISQLSession;
    }
        
        
        
    @Override
    public ISQLDataset getISQLDataset(){
        return getInstanceOfISQLDataset();
    }
    static  ISQLDataset getInstanceOfISQLDataset() {
        if (pISQLDataset == null){
            pISQLDataset = new SQLDataset();
        }
        return pISQLDataset;
    }
        
        
    @Override
    public ISQLExecutionActionResult getISQLExecutionActionResult() {
        return getInstanceOfISQLExecutionActionResult();
    }
    static  ISQLExecutionActionResult getInstanceOfISQLExecutionActionResult() {
        if (pISQLExecutionActionResult == null){
            pISQLExecutionActionResult = new SQLExecutionActionResult();
        }
        return pISQLExecutionActionResult;
    }
        
        
    @Override
    public ISQLExecutionTestResult getISQLExecutionTestResult() {
        return getInstanceOfISQLExecutionTestResult();
    }
    static  ISQLExecutionTestResult getInstanceOfISQLExecutionTestResult() {
        if (pISQLExecutionTestResult == null){
            pISQLExecutionTestResult = new SQLExecutionTestResult();
        }
        return pISQLExecutionTestResult;
    }
        
        
    @Override
    public ISQLExecutionResult getISQLExecutionResult() {
        return getInstanceOfISQLExecutionResult();
    }
    static ISQLExecutionResult getInstanceOfISQLExecutionResult() {
        if (pISQLExecutionResult == null){
            pISQLExecutionResult = new SQLExecutionResult();
        }
        return pISQLExecutionResult;
    }
        
    @Override
    public ISQLExecution getISQLExecution() {
        return getInstanceOfISQLExecution();
    }
    static ISQLExecution getInstanceOfISQLExecution() {
        if (pISQLExecution == null){
            pISQLExecution = new SQLExecution();
        }
        return pISQLExecution;
    }
        
    @Override
    public ISQLEnvironment getISQLEnvironment() {
        return getInstanceOfISQLEnvironment();
    }
    static ISQLEnvironment getInstanceOfISQLEnvironment() {
        if (pISQLEnvironment == null){
            pISQLEnvironment = new SQLEnvironment();
        }
        return pISQLEnvironment;
    }
        
    @Override
    public ISQLProject getISQLProject() {
        return getInstanceOfISQLProject();
    }
        
    static ISQLProject getInstanceOfISQLProject() {
        if (pISQLProject == null){
            pISQLProject = new SQLProject();
        }
        return pISQLProject;
    }
        
    @Override
    public ISQLTest getISQLTest() {
        return getInstanceOfISQLTest();
    }
    static ISQLTest getInstanceOfISQLTest() {
        if (pISQLTest == null){
            pISQLTest = new SQLTest();
        }
        return pISQLTest;
    }
        
    @Override
    public ISQLFamily getISQLFamily() {
        return getInstanceOfISQLFamily();
    }
    static ISQLFamily getInstanceOfISQLFamily() {
        if (pISQLFamily == null){
            pISQLFamily = new SQLFamily();
        }
        return pISQLFamily;
    }
        
    @Override
    public ISQLPersonne getISQLPersonne() {
        return getInstanceOfISQLPersonne();
    }
    static ISQLPersonne getInstanceOfISQLPersonne() {
        if (pISQLPersonne == null){
            pISQLPersonne = new SQLPersonne();
        }
        return pISQLPersonne;
    }
        
    @Override
    public ISQLScript getISQLScript() {
        return getInstanceOfISQLScript();
    }
    static ISQLScript getInstanceOfISQLScript() {
        if (pISQLScript == null){
            pISQLScript = new SQLScript();
        }
        return pISQLScript;
    }
        
    @Override
    public ISQLCampaign getISQLCampaign() {
        return getInstanceOfISQLCampaign();
    }
    static ISQLCampaign getInstanceOfISQLCampaign() {
        if (pISQLCampaign == null){
            pISQLCampaign = new SQLCampaign();
        }
        return pISQLCampaign;
    }
        
    @Override
    public ISQLAutomaticTest getISQLAutomaticTest() {
        return getInstanceOfISQLAutomaticTest();
    }
    static ISQLAutomaticTest getInstanceOfISQLAutomaticTest() {
        if (pISQLAutomaticTest == null){
            pISQLAutomaticTest = new SQLAutomaticTest();
        }
        return pISQLAutomaticTest;
    }
        
    @Override
    public ISQLTestList getISQLTestList() {
        return getInstanceOfISQLTestList();
    }
    static ISQLTestList getInstanceOfISQLTestList() {
        if (pISQLTestList == null){
            pISQLTestList = new SQLTestList();
        }
        return pISQLTestList;
    }
        
    @Override
    public ISQLFileAttachment getISQLFileAttachment() {
        return getInstanceOfISQLFileAttachment();
    }
    static ISQLFileAttachment getInstanceOfISQLFileAttachment() {
        if (pISQLFileAttachment == null){
            pISQLFileAttachment = new SQLFileAttachment();
        }
        return pISQLFileAttachment;
    }
        
    @Override
    public ISQLUrlAttachment getISQLUrlAttachment() {
        return getInstanceOfISQLUrlAttachment();
    }
    static ISQLUrlAttachment getInstanceOfISQLUrlAttachment() {
        if (pISQLUrlAttachment == null){
            pISQLUrlAttachment = new SQLUrlAttachment();
        }
        return pISQLUrlAttachment;
    }
        
    @Override
    public ISQLAttachment getISQLAttachment() {
        return getInstanceOfISQLAttachment();
    }
    static ISQLAttachment getInstanceOfISQLAttachment() {
        if (pISQLAttachment == null){
            pISQLAttachment = new SQLAttachment();
        }
        return pISQLAttachment;
    }
        
    @Override
    public ISQLParameter getISQLParameter() {
        return getInstanceOfISQLParameter();
    }
    static ISQLParameter getInstanceOfISQLParameter(){
        if (pISQLParameter == null){
            pISQLParameter = new SQLParameter();
        }
        return pISQLParameter;
    }
        
    @Override
    public ISQLAction getISQLAction() {
        return getInstanceOfISQLAction();
    }
    static public ISQLAction getInstanceOfISQLAction() {
        if (pISQLAction == null) {
            pISQLAction = new SQLAction();
        }
        return pISQLAction;
    }
        
    @Override
    public ISQLManualTest getISQLManualTest() {
        return getInstanceOfISQLManualTest();
    }
    static public ISQLManualTest getInstanceOfISQLManualTest() {
        if (pISQLManualTest == null){
            pISQLManualTest = new SQLManualTest();
        }
        return pISQLManualTest;
    }
        
    @Override
    public void setConnexionInfo(int _projectID, int _personneID){
        projectID = _projectID;
        personneID = _personneID;
        pSQLEngine.setConnexionInfo(projectID, personneID);
    }
        
    @Override
    public int getProjectID(){
        return projectID;
    }
        
    @Override
    public int getPersonneID(){
        return personneID;
    }
        
}
