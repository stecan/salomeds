package org.objectweb.salome_tmf.databaseSQL;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.jackrabbit.webdav.DavConstants;
import org.apache.jackrabbit.webdav.MultiStatus;
import org.apache.jackrabbit.webdav.MultiStatusResponse;
import org.apache.jackrabbit.webdav.client.methods.DavMethod;
import org.apache.jackrabbit.webdav.client.methods.DeleteMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.client.methods.PutMethod;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;

public class WebDavUtils {
        
    private String login;
        
    private String password;
        
    private String defaultHost;
        
    private HttpClient client;
        
    private String host;
        
    public WebDavUtils(String repository){
        loadProperties();
        init(repository, defaultHost);
    }
        
    public WebDavUtils(String repository, String host){
        loadProperties();
        init(repository, host);
    }
        
    private void loadProperties(){
        defaultHost = Api.getSalomeHost();
        login = Api.getWebdavLogin();
        password = Api.getWebdavPasswd();
    }
        
    private void init(String repository, String host){
        this.host = host;
        Util.debug("Connecting with webdav on " + host + repository);
        HostConfiguration hostConfig = new HostConfiguration();
        hostConfig.setHost(host + repository); 
        HttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
        HttpConnectionManagerParams params = new HttpConnectionManagerParams();
        int maxHostConnections = 20;
        params.setMaxConnectionsPerHost(hostConfig, maxHostConnections);
        connectionManager.setParams(params);    
        this.client = new HttpClient(connectionManager);
        Credentials creds = new UsernamePasswordCredentials(login, password);
        this.client.getState().setCredentials(AuthScope.ANY, creds);
        this.client.setHostConfiguration(hostConfig);
        Util.debug("Connection etablished");
    }
        
    public boolean searchFile(String name, String path){
        try {
            Util.debug("Search file "+ name +" using webdav");
            DavMethod pFind = new PropFindMethod(host + path, DavConstants.PROPFIND_ALL_PROP, DavConstants.DEPTH_1);
            client.executeMethod(pFind);
            Util.debug(pFind.getStatusCode() + " "+ pFind.getStatusText());
            Util.debug("Name to search " + name);
            MultiStatus multiStatus = pFind.getResponseBodyAsMultiStatus();
            MultiStatusResponse[] responses = multiStatus.getResponses();
            MultiStatusResponse currResponse;
            for (int i=0; i<responses.length; i++) {
                currResponse = responses[i];
                Util.debug(URLDecoder.decode(currResponse.getHref(), "UTF-8"));
                if ((URLDecoder.decode(currResponse.getHref(), "UTF-8").equals(path + name))) {
                    Util.debug("File found");
                    return true;
                }
            }  
                        
        } catch (Exception e) {
            Util.err(e);
        }
        return false;
    }
        
    public boolean uploadFile(String filePath, String dest){
        try{
            Util.debug("Uploading file "+ filePath  +" using webdav");
            File file = new File(filePath);
            PutMethod pm = new PutMethod(host + dest + URLEncoder.encode(file.getName(),"UTF-8"));
            InputStream in = new FileInputStream(file);
                          
            RequestEntity requestEntity=null;
            requestEntity = new InputStreamRequestEntity(in);
            pm.setRequestEntity(requestEntity);
            client.executeMethod(pm);
            Util.debug(pm.getStatusCode() + " "+ pm.getStatusText());
            return pm.getStatusCode() == 201;
        } catch(Exception e){
            Util.err(e);
        }
        return false;
    }
        
    public boolean downloadFile(String filePath, String destFile, String fileName){
        try{
            Util.debug("Download file " + filePath + URLEncoder.encode(fileName, "UTF-8") + " with webdav");
            GetMethod gm = new GetMethod(host + filePath + URLEncoder.encode(fileName, "UTF-8"));
            client.executeMethod(gm);
            Util.debug(gm.getStatusCode() + " "+ gm.getStatusText());
            byte[] fileContent = gm.getResponseBody();
            // Flux d'ecriture sur le fichier
            FileOutputStream fos = new FileOutputStream(destFile);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
            for (int i = 0; i < fileContent.length; i++) {
                bos.write(fileContent[i]);
            }
            // Fermeture des flux de donnees
            bos.flush();
            bos.close();
            return gm.getStatusCode() == 200;
        } catch(Exception e){
            Util.err(e);
        }
        return false;
    }
        
    public void deleteFile(String filePath, String fileName){
        try{
            Util.debug("Delete file " + filePath + URLEncoder.encode(fileName, "UTF-8") + " with webdav");
            DeleteMethod dm = new DeleteMethod(host + filePath + URLEncoder.encode(fileName, "UTF-8"));
            client.executeMethod(dm);
            Util.debug(dm.getStatusCode() + " "+ dm.getStatusText());
        } catch(Exception e){
            Util.err(e);
        }
    }

}
