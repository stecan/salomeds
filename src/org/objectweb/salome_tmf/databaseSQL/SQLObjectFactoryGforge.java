package org.objectweb.salome_tmf.databaseSQL;

import org.objectweb.salome_tmf.api.IChangeDispatcher;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLProject;

public class SQLObjectFactoryGforge extends SQLObjectFactory {
        
        
    @Override
    public ISQLEngine getInstanceOfSQLEngine(String url, String username, String password, IChangeDispatcher pIChangeDispatcher, int pid, String driverJDBC, int locktype) throws Exception {
        if (pSQLEngine == null){
            pDB = new DataBase(driverJDBC);
            pDB.open(url, username, password);
            pSQLEngine = new SQLEngineGForge();
            SQLEngineGForge.initSQLEngine(pDB, true, locktype);
            autoUpdateDB();
            pChangeListener = new ChangeListener(pid, pIChangeDispatcher);
        } else {
            pDB = new DataBase(driverJDBC);
            pDB.open(url, username, password);
            //Util.debug("INIT DB");
            SQLEngineGForge.initDB(pDB);
        }
        return pSQLEngine;
    }
        
        
    @Override
    public ISQLProject getISQLProject() {
        return SQLObjectFactoryGforge.getInstanceOfISQLProject();
    }
        
    static ISQLProject getInstanceOfISQLProject() {
        if (pISQLProject == null){
            pISQLProject = new SQLProjectGForge();
        }
        return pISQLProject;
    }
}
