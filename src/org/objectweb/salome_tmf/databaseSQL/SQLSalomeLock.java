package org.objectweb.salome_tmf.databaseSQL;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.data.LockInfoWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLSalomeLock;


public class SQLSalomeLock implements ISQLSalomeLock{

    static boolean tableExist = false;
    public SQLSalomeLock() {
        if (!tableExist){
            try {
                PreparedStatement prep;
                prep = SQLEngine.getStatement("BEGIN");
                prep.execute();
                                
                prep = SQLEngine.getSQLCommonQuery("showTable"); //ok
                ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                boolean trouve = false;
                while  (stmtRes.next() && !trouve) {
                    String tableName = stmtRes.getString(1);
                    String nomTable = "SALOME_LOCK";
                    if (tableName.equals(nomTable) || tableName.equals(nomTable.toLowerCase()) ){
                        trouve = true;
                    }
                }
                if (trouve == false){
                    prep = SQLEngine.getSQLCommonQuery("createLockTable"); //ok
                    SQLEngine.runAddQuery(prep);                        
                }
                tableExist = true;
                prep = SQLEngine.getStatement("COMMIT");
                prep.execute();
            }catch(Exception e){
                try {
                    PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
                    prep.execute();
                } catch(Exception e2){
                                        
                }
            }
        }
    }
        
    @Override
    public LockInfoWrapper isLock(int projectID) throws Exception {
        LockInfoWrapper pLockInfo = null;
        try {
            //transNumber = SQLEngine.beginTransaction(0, ApiConstants.READ_LOCK);
            //LOCK IN SHARE MODE
            PreparedStatement prep;
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLSelectQuery("selectSalomeLock"); //ok
            prep.setInt(1, projectID);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            int lock_code = 0;
            while (stmtRes.next()){
                int personne_id = stmtRes.getInt("id_personne");
                lock_code = lock_code | stmtRes.getInt("lock_code"); 
                int action_code = stmtRes.getInt("action_code");
                int pid = stmtRes.getInt("pid"); 
                String info = stmtRes.getString("info"); 
                Date date = stmtRes.getDate("lock_date");
                pLockInfo = new LockInfoWrapper(projectID, personne_id, pid, lock_code, action_code, info, date);
            }
                        
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
            //SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
            prep.execute();
            //SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return pLockInfo;
    }
        
    @Override
    public LockInfoWrapper[] getAllProjectLocks(int projectID) throws Exception {
        Vector lockInfoList = new Vector();
        try {
            //transNumber = SQLEngine.beginTransaction(0, ApiConstants.READ_LOCK);
            //LOCK IN SHARE MODE
            PreparedStatement prep;
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLSelectQuery("selectSalomeLock"); //ok
            prep.setInt(1, projectID);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()){
                int personne_id = stmtRes.getInt("id_personne");
                int lock_code = stmtRes.getInt("lock_code"); 
                int action_code = stmtRes.getInt("action_code");
                int pid = stmtRes.getInt("pid"); 
                String info = stmtRes.getString("info"); 
                Date date = stmtRes.getDate("lock_date");
                LockInfoWrapper pLockInfo = new LockInfoWrapper(projectID, personne_id, pid, lock_code, action_code, info, date);
                lockInfoList.addElement(pLockInfo);
            }
                        
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
            //SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
            prep.execute();
            //SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        LockInfoWrapper[] liwArray = new LockInfoWrapper[lockInfoList.size()];
        for(int i = 0; i < lockInfoList.size(); i++) {
            liwArray[i] = (LockInfoWrapper) lockInfoList.get(i);
        }
        return liwArray;
    }
        
    @Override
    public void insert(int projectID, int personneId, int lock_code, int action_code, String info, int pid) throws Exception {
        try {
            PreparedStatement prep;
            //transNumber = SQLEngine.beginTransaction(0, ApiConstants.INSERT_LOCK);
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLAddQuery("addSalomeLock"); 
            prep.setInt(1, projectID);
            prep.setInt(2, personneId);
            prep.setInt(3, pid);
            prep.setInt(4, lock_code);
            prep.setInt(5, action_code);
            prep.setString(6, info);
            prep.setDate(7, org.objectweb.salome_tmf.api.Util.getCurrentDate());
            SQLEngine.runAddQuery(prep);
                        
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
        } catch (Exception e ){
            PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
            prep.execute();
            //SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    @Override
    public void delete(int projectID, int pid) throws Exception {
        //int transNumber = -1;
        try {
            PreparedStatement prep;
            //transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_LOCK);
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLDeleteQuery("deleteSalomeLock"); //ok
            prep.setInt(1, projectID);
            prep.setInt(2, pid);
            SQLEngine.runDeleteQuery(prep);
                        
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
        } catch (Exception e ){
            PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
            prep.execute();
            throw e;
        }
    }
        
    @Override
    public void delete(int projectID) throws Exception {
        //int transNumber = -1;
        try {
            PreparedStatement prep;
            //transNumber = SQLEngine.beginTransaction(0, ApiConstants.DELETE_LOCK);
            prep = SQLEngine.getStatement("BEGIN");
            prep.execute();
                        
            prep = SQLEngine.getSQLDeleteQuery("deleteAllSalomeLock"); //ok
            prep.setInt(1, projectID);
            SQLEngine.runDeleteQuery(prep);
                        
            prep = SQLEngine.getStatement("COMMIT");
            prep.execute();
            //SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            PreparedStatement prep = SQLEngine.getStatement("ROLLBACK");
            prep.execute();
            //SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
}
