/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.LockInfoWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLSalomeLock;
import org.objectweb.salome_tmf.api.sql.LockException;

//import com.mysql.jdbc.CommunicationsException;

public class SQLEngine implements ISQLEngine {

    static Properties addSqlQuery;
    static Properties deleteSqlQuery;
    static Properties updateSqlQuery;
    static Properties selectSqlQuery;
    static Properties comonQuery;

    static DataBase db;

    static int mysqlLock = 1; // -1 = none , 0 = mysql, 1 = salome,
    static boolean isLock = false;
    static boolean specialLock = false;
    static boolean specLock = false;
    static boolean lockOnQuery = true;
    static int _code = -1;

    // static int lockCode = 0;
    static int nbLockTry = 3;
    static int waitLockTime = 1000;
    static ISQLSalomeLock pSQLSalomeLock = null;
    static int projectID = 0;
    static int personneID = 0;
    static int pid = 0;

    static int idTrans = -1;
    public static int tmpIdTrans = -1;
    static java.util.Random idCalcul = new java.util.Random(1000000);

    static String addToLockWrite = "";
    static String addToLockRead = "";
    static boolean exitOnErrorComit = true;

    // static File logFile;
    // static FileOutputStream logFileOutput;

    static void initSQLEngine(DataBase database, boolean doLockOnQuery,
                              String _SQLBase, int locktype) throws Exception {
        String SQLBase = DatabaseConstants.SQLBASE;
        if (_SQLBase != null) {
            SQLBase = _SQLBase;
        }
        mysqlLock = locktype;
        if (idTrans != -1) {
            idTrans = -1;
            isLock = false;
            specialLock = false;
            specLock = false;
            lockOnQuery = true;
            _code = -1;
            // throw new Exception ("Previous Session not correct");
        }
        Util.debug("[SQLEngine->initSQLEngine] get addSqlQuery properties "
                   + DatabaseConstants.ADD_SQL_QUERY_FILE_PATH);
        addSqlQuery = Util.getPropertiesStream(SQLEngine.class
                                               .getResourceAsStream(SQLBase
                                                                    + DatabaseConstants.ADD_SQL_QUERY_FILE_PATH));
        Util.debug("[SQLEngine->initSQLEngine] get comonQuery properties "
                   + DatabaseConstants.COMMON_SQL_QUERY_FILE_PATH);
        comonQuery = Util.getPropertiesStream(SQLEngine.class
                                              .getResourceAsStream(SQLBase
                                                                   + DatabaseConstants.COMMON_SQL_QUERY_FILE_PATH));
        Util.debug("[SQLEngine->initSQLEngine] get updateSqlQuery properties "
                   + DatabaseConstants.UPDATE_SQL_QUERY_FILE_PATH);
        updateSqlQuery = Util.getPropertiesStream(SQLEngine.class
                                                  .getResourceAsStream(SQLBase
                                                                       + DatabaseConstants.UPDATE_SQL_QUERY_FILE_PATH));
        Util.debug("[SQLEngine->initSQLEngine] get selectSqlQuery properties "
                   + DatabaseConstants.SELECT_SQL_QUERY_FILE_PATH);
        selectSqlQuery = Util.getPropertiesStream(SQLEngine.class
                                                  .getResourceAsStream(SQLBase
                                                                       + DatabaseConstants.SELECT_SQL_QUERY_FILE_PATH));
        Util.debug("[SQLEngine->initSQLEngine] get deleteSqlQuery properties "
                   + DatabaseConstants.DELETE_SQL_QUERY_FILE_PATH);
        deleteSqlQuery = Util.getPropertiesStream(SQLEngine.class
                                                  .getResourceAsStream(SQLBase
                                                                       + DatabaseConstants.DELETE_SQL_QUERY_FILE_PATH));

        if (database == null) {
            throw new Exception("[SQLEngine:initSQLEngine] Database is null");
        }
        lockOnQuery = doLockOnQuery;
        db = database;

        pid = (int) ((database.hashCode()) * (System.currentTimeMillis()));
        /*
         * if (Api.isDEBUG()){ try { String tmpDir =
         * System.getProperties().getProperty("java.io.tmpdir"); String fs =
         * System.getProperties().getProperty("file.separator"); logFile = new
         * File(tmpDir + fs + "database.log"); logFile.createNewFile();
         * logFileOutput = new FileOutputStream (logFile);
         *
         * } catch (Exception e){
         *
         * } }
         */
    }

    static void initDB(DataBase database) throws Exception {
        if (database == null) {
            throw new Exception("[SQLEngine:initSQLEngine] Database is null");
        }
        if (idTrans != -1) {
            idTrans = -1;
            isLock = false;
            specialLock = false;
            specLock = false;
            lockOnQuery = true;
            _code = -1;
            // throw new Exception ("Previous Session not correct");
        }
        db = database;
        pid = (int) ((database.hashCode()) * (System.currentTimeMillis()));
    }

    static PreparedStatement getSQLAddQuery(String queryName) throws Exception {
        Util.debug("[SQLEngine->getSQLAddQuery]" + queryName);
        String sql = addSqlQuery.getProperty(queryName);
        Util.debug("[SQLEngine->getSQLAddQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLCommonQuery(String queryName)
        throws Exception {
        Util.debug("[SQLEngine->getSQLCommonQuery]" + queryName);
        String sql = comonQuery.getProperty(queryName);
        Util.debug("[SQLEngine->getSQLCommonQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLSelectQuery(String queryName)
        throws Exception {
        Util.debug("[SQLEngine->getSQLSelectQuery]" + queryName);
        String sql = selectSqlQuery.getProperty(queryName);
        Util.debug("[SQLEngine->getSQLSelectQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLUpdateQuery(String queryName)
        throws Exception {
        Util.debug("[SQLEngine->getSQLUpdateQuery]" + queryName);
        String sql = updateSqlQuery.getProperty(queryName);
        Util.debug("[SQLEngine->getSQLUpdateQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getSQLDeleteQuery(String queryName)
        throws Exception {
        Util.debug("[SQLEngine->getSQLDeleteQuery]" + queryName);
        String sql = deleteSqlQuery.getProperty(queryName);
        Util.debug("[SQLEngine->getSQLDeleteQuery] sql is " + sql);
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static PreparedStatement getStatement(String sql) throws Exception {
        PreparedStatement prep = db.prepareStatement(sql);
        return prep;
    }

    static synchronized int runAddQuery(PreparedStatement prep)
        throws Exception {
        try {
            return prep.executeUpdate();
        } catch (Exception communicationsException) {
            db.reconnect();
            return prep.executeUpdate();
        }
    }

    static synchronized int runDeleteQuery(PreparedStatement prep)
        throws Exception {
        try {
            return prep.executeUpdate();
        } catch (Exception communicationsException) {
            db.reconnect();
            return prep.executeUpdate();
        }
    }

    static synchronized int runUpdateQuery(PreparedStatement prep)
        throws Exception {
        try {
            return prep.executeUpdate();
        } catch (Exception communicationsException) {
            db.reconnect();
            return prep.executeUpdate();
        }
    }

    static synchronized ResultSet runSelectQuery(PreparedStatement prep)
        throws Exception {
        try {
            return prep.executeQuery();
        } catch (Exception communicationsException) {
            db.reconnect();
            return prep.executeQuery();
        }
    }

    @Override
    public synchronized int beginTransDB(int lock_code, int type)
        throws Exception {
        return beginTransaction(lock_code, type);
    }

    @Override
    public synchronized void commitTransDB(int type) throws Exception {
        commitTrans(type);
    }

    public synchronized void rollBackTransDB(int type) throws Exception {
        rollBackTrans(type);
    }

    @Override
    public synchronized void rollForceBackTransDB(int type) {
        // writeFileLog("Force RollBack Trans : " + type);
        try {
            rollBackTrans(type);
        } catch (Exception e) {
            try {
                rollBackTrans(type);
            } catch (Exception e1) {
                try {
                    rollBackTrans(type);
                } catch (Exception e2) {

                }
            }
        }
    }

    /*
     * public synchronized void doComit() throws Exception { int transNumber =
     * beginTransaction(ApiConstants.LOADING); commitTrans(transNumber); }
     *
     * static public synchronized void doComitData() throws Exception { int
     * transNumber = beginTransaction(ApiConstants.LOADING);
     * commitTrans(transNumber); }
     */

    static public synchronized int beginTransaction(int lock_code, int type)
        throws Exception {
        if (idTrans == -1) {
            _code = type;

            // if mysqlock = 1
            if (lockOnQuery) {
                if (isLock == false && specLock == false) {
                    if (_code != ApiConstants.COMMON_REQ && canLock(_code)) {
                        lockReadWrite(lock_code, type);
                    }
                }
            }

            idTrans = idCalcul.nextInt();
            Util.log("[SQLEngine] Start the Transition : " + idTrans
                     + ", with code " + _code);
            // if (_code != ApiConstants.LOADING) {
            try {
                db.beginTrans(); // if mysqlock = -1 or 1
            } catch (java.sql.SQLException eofException) {
                db.reconnect();
                db.beginTrans();
            }
            // }

            // if mysqlock = 0 -> lock table
            return idTrans;
        }
        return -1;
    }

    static synchronized public void commitTrans(int id) throws Exception {
        if (idTrans != -1) {
            if (id == idTrans) {
                Util.log("[SQLEngine] Commit the Transition : " + id);
                idTrans = -1;

                try {
                    // if (_code != ApiConstants.LOADING) {
                    db.commit();
                    // }
                } catch (Exception e) {
                    if (exitOnErrorComit) {
                        Util.err(e);
                        System.exit(1);
                    }
                }
                try {
                    if (lockOnQuery) {
                        if (isLock && specLock == false) {
                            if (_code != ApiConstants.COMMON_REQ
                                && canLock(_code)) {
                                unLock();
                            }
                        }
                    }

                } catch (Exception e) {
                    if (exitOnErrorComit) {
                        Util.err(e);
                        System.exit(1);
                    }
                }
                // writeFileLog("Commit Trans : " + id);

                if (_code != -1 && _code != ApiConstants.COMMON_REQ) {
                    Util.log("[SQLEngine] dispatchChange : " + _code);
                    // Api.dispatchChange(_code);
                    if (Api.isNET_CHANGE_TRACK()) {
                        // if (SQLObjectFactory.pChangeListener != null) {
                        SQLObjectFactory.pChangeListener.addChange(_code);
                    }
                }
            }
        }
    }

    static synchronized public void rollBackTrans(int id) throws Exception {
        if (idTrans != -1) {
            if (id == idTrans) {
                Util.log("[SQLEngine] RollBack the Transition : " + id);
                idTrans = -1;
                try {
                    // if (_code != ApiConstants.LOADING) {
                    db.rollback();
                    // }
                } catch (Exception e) {
                    if (exitOnErrorComit) {
                        Util.err(e);
                        System.exit(1);
                    }
                }
                try {
                    if (isLock && specLock == false) {
                        if (_code != ApiConstants.COMMON_REQ && canLock(_code)) {
                            unLock();
                        }
                    }
                } catch (Exception e) {
                    if (exitOnErrorComit) {
                        Util.err(e);
                        System.exit(1);
                    }
                }
                // writeFileLog("RollBack Trans : " + id);

            }
        }
    }

    static void lockReadWrite(int lock_code, int action_code) throws Exception {
        if (isLock == true) {
            throw new Exception(
                                "[SQLEngine->lockReadWrite] Database is already locked");
        }
        if (lock_code == 0) {
            return;
        }
        if (!specialLock) {
            if (mysqlLock == 0) {
                Util.log("[SQLEngine] Do Lock for transition code : " + _code);
                lockRead();
                lockWrite();
            } else if (mysqlLock == 1) {
                isLock = lockApplicative(lock_code, action_code);
            } else {
                isLock = false;
            }
        }
    }

    static void lockRead() throws Exception { // Called only from lockReadWrite
        Util.log("[SQLEngine] Do lock Read");
        if (mysqlLock == 0) {
            PreparedStatement prep;
            String sql = comonQuery.getProperty("lockALLREAD");
            sql += addToLockRead;
            Util.debug("[SQLEngine->getSQLCommonQuery] sql is " + sql);
            prep = db.prepareStatement(sql);
            prep.executeUpdate();
            isLock = true;
        } else {

        }
    }

    static void lockWrite() throws Exception {// Called only from lockReadWrite
        Util.log("[SQLEngine] Do lock Write");
        if (mysqlLock == 0) {
            PreparedStatement prep;

            String sql = comonQuery.getProperty("lockALLWRITE");
            sql += addToLockWrite;
            Util.debug("[SQLEngine->getSQLCommonQuery] sql is " + sql);
            prep = db.prepareStatement(sql);

            prep.executeUpdate();
            isLock = true;
        } else {

        }
    }

    static void lockTestPlan() throws Exception { // Called only from lockTPlan
        Util.log("[SQLEngine] Do lock All");
        if (mysqlLock == 0) {
            PreparedStatement prep;
            prep = getSQLCommonQuery("lock");
            prep.executeUpdate();
            isLock = true;
            specialLock = true;
        } else {
            /* TODO */
        }
    }

    static void unLock() throws Exception {
        if (mysqlLock == 0) {
            if (isLock == false) {
                throw new Exception(
                                    "[SQLEngine->unLock] Database is not locked");
            }
            if (!specialLock) {
                Util
                    .log("[SQLEngine] Do UnLock for transition code : "
                         + _code);
                PreparedStatement prep;
                prep = getSQLCommonQuery("unlock");
                prep.executeUpdate();
                isLock = false;
            }
        } else if (mysqlLock == 1) {
            if (isLock) {
                unLockApplicativeLock();
            }
            isLock = false;
        } else {
            isLock = false;
        }
    }

    /************ Applicative lock *******************/

    static boolean canLock(int code) {
        if (code != ApiConstants.READ_LOCK && code != ApiConstants.DELETE_LOCK
            && code != ApiConstants.INSERT_LOCK) {
            return true;
        }
        return false;
    }

    @Override
    public void setConnexionInfo(int _projectID, int _personneID) {
        projectID = _projectID;
        personneID = _personneID;
        pSQLSalomeLock = Api.getISQLObjectFactory().getISQLSalomeLock();
    }

    @Override
    public int getProjectID() {
        return projectID;
    }

    @Override
    public int getPersonneID() {
        return personneID;
    }

    static void unLockApplicativeLock() throws Exception {
        if (pSQLSalomeLock != null && projectID > 0) {
            try {
                Util
                    .log("[SQLEngine] Do UnLock for transition code : "
                         + _code);
                pSQLSalomeLock.delete(projectID, pid);
            } catch (Exception e) {
                try {
                    Util.log("[SQLEngine] Do UnLock for transition code : "
                             + _code);
                    pSQLSalomeLock.delete(projectID, pid);
                } catch (Exception e1) {
                    try {
                        Util.log("[SQLEngine] Do UnLock for transition code : "
                                 + _code);
                        pSQLSalomeLock.delete(projectID, pid);
                    } catch (Exception e2) {
                        throw new LockException(
                                                "Cannot unlock the DataBase, restart salome",
                                                LockException.DELETE_LOCK);
                        // DO SYSTEM EXIT
                    }
                }
            }
        }
    }

    static boolean lockApplicative(int lock_code, int action_code)
        throws Exception {
        if (lock_code == 0) {
            return false;
        }

        boolean doLock = false;
        Util.log("[SQLEngine] Do UnLock " + lock_code
                 + " for transition code : " + _code);
        if (pSQLSalomeLock != null && projectID > 0) {
            LockInfoWrapper pLockInfo = pSQLSalomeLock.isLock(projectID);
            if (pLockInfo != null) {
                if (pid == pLockInfo.getPid()) {
                    throw new LockException(
                                            "Cannot lock the DataBase, please restart salome",
                                            LockException.DOLOCK);
                }
                if (canLock(lock_code, pLockInfo.getLock_code())) {
                    insertApplicativeLock(lock_code, action_code);
                    doLock = true;
                } else {
                    int tryLock = 0;
                    while (!doLock && tryLock < nbLockTry) {
                        try {
                            Thread.sleep(waitLockTime);
                            pLockInfo = pSQLSalomeLock.isLock(projectID);
                            if (pLockInfo != null) {
                                if (pid == pLockInfo.getPid()) {
                                    throw new LockException(
                                                            "Cannot lock the DataBase, please restart salome",
                                                            LockException.DOLOCK);
                                }
                                if (canLock(lock_code, pLockInfo.getLock_code())) {
                                    insertApplicativeLock(lock_code,
                                                          action_code);
                                    doLock = true;
                                } else {
                                    tryLock++;
                                }
                            } else {
                                insertApplicativeLock(lock_code, action_code);
                                doLock = true;
                            }
                        } catch (Exception e) {
                            tryLock++;
                        }
                    }
                }
            } else {
                insertApplicativeLock(lock_code, action_code);
                doLock = true;
            }
            if (doLock == false) {
                throw new LockException(
                                        "DataBase is locked, please try later or delete lock",
                                        LockException.DOLOCK);
            }
        }
        return doLock;
    }

    static void insertApplicativeLock(int lock_code, int action_code)
        throws Exception {
        if (pSQLSalomeLock != null && projectID > 0) {
            try {
                Util.log("[SQLEngine] Do Lock for transition code : " + _code);
                pSQLSalomeLock.insert(projectID, personneID, lock_code,
                                      action_code, "", pid);
            } catch (Exception e) {
                try {
                    Util.log("[SQLEngine] Do Lock for transition code : "
                             + _code);
                    pSQLSalomeLock.insert(projectID, personneID, lock_code,
                                          action_code, "", pid);
                } catch (Exception e1) {
                    try {
                        Util.log("[SQLEngine] Do Lock for transition code : "
                                 + _code);
                        pSQLSalomeLock.insert(projectID, personneID, lock_code,
                                              action_code, "", pid);
                    } catch (Exception e2) {
                        Util.err(e2);
                        throw new LockException(
                                                "Cannot lock the DataBase, please try later or restart salome",
                                                LockException.INSERT_LOCK);
                    }
                }
            }
        }
    }

    static boolean canLock(int lock_code_todo, int lock_code_exist) {
        int mask = (lock_code_todo & lock_code_exist);
        Util.log("[SQLEngine] Lock " + lock_code_exist
                 + " existe,  an try lock : " + lock_code_todo + "(mask = "
                 + (lock_code_todo & lock_code_exist) + ")");
        if (mask == 0) {
            return true;
        }
        return false;
    }

    /************ Special Allow use in case of cascade delete or update *******************/

    static boolean specialAllow = false;

    public static void setSpecialAllow(boolean special_allow) {
        specialAllow = special_allow;
    }

    static boolean isSpecialAllow() {
        return specialAllow;
    }

    /******************** ISQLEngine Implentation ****************************************/

    @Override
    public boolean isClose() {
        try {
            if (db != null) {
                return db.cnt.isClosed();
            } else {
                return true;
            }
        } catch (Exception e) {
            return true;
        }
    }

    public boolean isOpen() {
        return !isClose();
    }

    @Override
    public void close() throws Exception {
        /*
         * try { if (logFileOutput != null){ logFileOutput.flush();
         * logFileOutput.close(); } } catch (Exception e){
         *
         * }
         */
        Util.log("[SQLEngine->close] close connection " + db.cnt);
        db.cnt.close();
        db = null;
    }

    @Override
    public void addSQLLoackRead(String table) {
        if (!(addToLockRead.indexOf(table) > 0)) {
            addToLockRead += "," + table;
        }
    }

    @Override
    public void addSQLLoackWrite(String table) {
        if (!(addToLockWrite.indexOf(table) > 0)) {
            addToLockWrite += "," + table;
        }
    }

    @Override
    public synchronized boolean isLock() {
        return isLock;
    }

    /*
     * public void lockAll() throws Exception{ if (mysqlLock){ lockReadWrite();
     * } else { //TODO return; } }
     */

    @Override
    public void lockTPlan() throws Exception {
        Util.log("[SQLEngine] Lock Test plan");
        if (mysqlLock == 0) {
            lockTestPlan();
        } else if (mysqlLock == 1) {
            /* TODO */
            return;
        } else {

        }
    }

    /*
     * public void unLockAll() throws Exception{ if (mysqlLock == 0){ unLock();
     * } else if (mysqlLock ==1) { //TODO return; } else { return; } }
     */

    @Override
    public void unLockTPlan() throws Exception {
        if (mysqlLock == 0) {
            Util.log("[SQLEngine] unLock Test plan");
            specialLock = false;
            unLock();
        } else if (mysqlLock == 1) {
            /* TODO */
            return;
        } else {
            return;
        }
    }

    public static DataBase getDb() {
        return db;
    }

    public static void setDb(DataBase db) {
        SQLEngine.db = db;
    }

    public static int getIdTrans() {
        return idTrans;
    }

    public static void setIdTrans(int idTrans) {
        SQLEngine.idTrans = idTrans;
    }

    // ///////////////////////////////////////////////////////////////////////

    /*
     * static private void writeFileLog(String s){ try { String toWrite = s +
     * "\n"; logFileOutput.write(toWrite.getBytes()); } catch(Exception e){
     *
     * } }
     */
}
