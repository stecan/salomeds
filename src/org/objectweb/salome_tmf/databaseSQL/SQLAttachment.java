/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.sql.ISQLAttachment;

public class SQLAttachment implements ISQLAttachment {

    /**
     * Delete attchment idetified by idAttach in database from table ATTACHEMENT
     *
     * @param idAttach
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void delete(int idAttach) throws Exception {
        int transNumber = -1;
        if (idAttach < 1) {
            throw new Exception("[SQLAttachment->delete] attach have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.DELETE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteAttachFromDB"); // ok
            prep.setInt(1, idAttach);
            SQLEngine.runDeleteQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLAttachment->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update the description of an attachment (identified by idAttach) in the
     * database
     *
     * @param idAttach
     * @param description
     *            : the new description
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateDescription(int idAttach, String description)
        throws Exception {
        int transNumber = -1;
        if (idAttach < 1) {
            throw new Exception("[SQLAttachment->delete] attach have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateAttachDescription"); // ok
            prep.setString(1, description);
            prep.setInt(2, idAttach);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLAttachment->updateDescription]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Return the last id of an attachment in the table ATTACHEMENT no
     * permission needed
     */
    @Override
    public int getLastIdAttach() throws Exception {
        int maxIdAttach = -1;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectMaxIdAttach"); // ok
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()) {
            maxIdAttach = stmtRes.getInt("max_id_attach");
        } else {
            throw new Exception("[SQLAttachment->getLastIdAttach] id not exist");
        }
        return maxIdAttach;
    }
}
