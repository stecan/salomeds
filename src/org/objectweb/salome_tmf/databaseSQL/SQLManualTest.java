/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.sql.ISQLManualTest;

public class SQLManualTest extends SQLTest implements ISQLManualTest{
        
    /**
     * Insert a Manual test in table CAS_TEST
     * @param idTestList : the id of the testlist which contain the inserted test
     * @param name : the name of the test
     * @param description : the description of the tests
     * @param conceptor : the conceptor of the test
     * @param extension : the plug-in extension of the test
     * @return the id of the test in table CAS_TEST
     * @throws Exception
     * need permission canCreateTest
     */
    @Override
    public int insert(int idTestList, String name, String description, String conceptor, String extension) throws Exception {
        int testId = -1;
        int transNumber = -1; 
        if (idTestList <1 ) {
            throw new Exception("[SQLManualTest->insert] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLTest : insert -> canCreateTest]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLTestList().getTestList(idTestList) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_TEST);
                        
            int order = SQLObjectFactory.getInstanceOfISQLTestList().getNumberOfTest(idTestList);
            order --; //Because index begin at 0
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addTest"); //ok
            int idPers =  SQLObjectFactory.getInstanceOfISQLPersonne().getID(conceptor);
            if (idPers <1 ) {
                throw new Exception("[SQLManualTest->insert] id is not valid : " + idPers + ", conceptor is :" + conceptor);
            } 
            prep.setInt(1,idPers);
            prep.setInt(2,idTestList);
            prep.setString(3,name);
            prep.setDate(4,Util.getCurrentDate());
            prep.setTime(5,Util.getCurrentTime());
            prep.setString(6,ApiConstants.MANUAL);
            prep.setString(7,description);
            prep.setInt(8, order + 1);
            prep.setString(9, extension);
            SQLEngine.runAddQuery(prep);
                        
            testId = getID(idTestList, name);
            if (testId <1 ) {
                throw new Exception("[SQLManualTest->insert] id is not valid : " + testId + ", TestList is :" + idTestList + ", test : " + name);
            } 
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLManualTest->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return testId;   
    }
        
    /**
     * Delete the test in the database, this include :
     * delete test actions, and all reference (Parameter, Campaign) and the test attachments
     * @param idTest : id of the test
     * @throws Exception
     * @see ISQLAction.delete(int)
     * need permission canDeleteTest (do a special allow)
     */
    @Override
    public void delete(int idTest)  throws Exception {
        if (idTest <1 ) {
            throw new Exception("[SQLManualTest->delete] entry data are not valid");
        } 
        int transNumber = -1;
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLTest : delete -> canDeleteTest]");
            }
            //Require specialAllow
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }
        try {
                        
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_TEST);
                        
            // Begin Delete
            //Delete All test actions if test is manual
            ActionWrapper[] actionList = getTestActions(idTest);
            for (int i = 0; i< actionList.length ; i++){
                int actionId = (actionList[i]).getIdBDD();
                SQLObjectFactory.getInstanceOfISQLAction().delete(actionId, false);
            }
                        
            deleteRef(idTest, true);
                        
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLManualTest->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }
        
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test and his actions
     * @param idTest : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * need permission  canUpdateTest
     * @see deleteUseParamRef
     * @see ISQLAction.deleteParamUse(int, int)
     */
    @Override
    public void deleteUseParam(int idTest, int paramId) throws Exception {
        if (idTest <1 || paramId < 1 ) {
            throw new Exception("[SQLManualTest->deleteUseParam] entry data are not valid");
        } 
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLTest : deleteUseParam -> canUpdateTest]");
            }
        }       
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_PARAMETER_FROM_TEST);
                        
            ActionWrapper[] actionList = getTestActions(idTest);
            for (int i = 0 ; i < actionList.length; i++){
                ActionWrapper pActionWrapper = actionList[i];
                SQLObjectFactory.getInstanceOfISQLAction().deleteParamUse(pActionWrapper.getIdBDD(),paramId);
            }
            deleteUseParamRef(idTest, paramId);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLManualTest->deleteUseParam]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    /**
     * Get a Vector of ActionWrapper representing all Action of the test
     * @param idTest : id of the test
     * @return
     * @throws Exception
     */
    @Override
    public ActionWrapper[] getTestActions(int idTest) throws Exception {
        if (idTest <1 ) {
            throw new Exception("[SQLManualTest->getTestActions] entry data are not valid");
        } 
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectTestActions"); //ok
        prep.setInt(1,idTest);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            ActionWrapper pAction = new ActionWrapper();
            pAction.setName(stmtRes.getString("nom_action"));
            pAction.setDescription(stmtRes.getString("description_action"));
            pAction.setAwaitedResult(stmtRes.getString("res_attendu_action"));
            pAction.setOrderIndex(stmtRes.getInt("num_step_action"));
            pAction.setIdBDD(stmtRes.getInt("id_action"));
            pAction.setOrder(stmtRes.getInt("num_step_action"));
            pAction.setIdTest(stmtRes.getInt("CAS_TEST_id_cas"));
            result.addElement(pAction);
        }
        ActionWrapper[] awArray = new ActionWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            awArray[i] = (ActionWrapper) result.get(i);
        }
        return awArray;
    }
        
    /**
     * Get the number of action contening in the test identified by idTest
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public int getNumberOfAction(int idTest) throws Exception {
        return getTestActions(idTest).length;
    }
        
    /**
     * Get a ActionWrapper representing an action in position order for the test idTest
     * @param idTest : id of the test
     * @param order : order of the action
     * @return a ActionWrapper
     * @throws Exception
     */
    @Override
    public ActionWrapper getActionByOrder(int idTest, int order) throws Exception {
        if (idTest <1 || order < 0 ) {
            throw new Exception("[SQLManualTest->getActionByOrder] entry data are not valid");
        } 
        ActionWrapper pAction = null;
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectActionByOrder"); //ok
        prep.setInt(1,idTest);
        prep.setInt(2,order);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            pAction = new ActionWrapper();
            pAction.setName(stmtRes.getString("nom_action"));
            pAction.setDescription(stmtRes.getString("description_action"));
            pAction.setIdBDD(stmtRes.getInt("id_action"));
            pAction.setOrder(stmtRes.getInt("num_step_action"));  
        }
        return pAction;
    }
}
