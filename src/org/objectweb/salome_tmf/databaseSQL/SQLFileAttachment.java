/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLFileAttachment;

public class SQLFileAttachment extends SQLAttachment implements
                                                         ISQLFileAttachment {

    /**
     * Insert an attachment as file in the table ATTACHEMENT
     *
     * @param f
     * @param description
     * @throws Exception
     * @return the id of the new attached file no permission needed
     */
    @Override
    public int insert(SalomeFileWrapper f, String description) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.INSERT_ATTACHMENT);
            // FileInputStream fis = new FileInputStream(f);
            // BufferedInputStream bis = new BufferedInputStream(fis);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("insertFileAttachIntoDB"); // ok
            prep.setString(1, f.getName());
            prep.setBinaryStream(2, f.openInputStream(), f.getLength());
            prep.setString(3, description);
            prep.setLong(4, f.getLength());
            prep.setDate(5, new Date(f.getLastModified()));
            SQLEngine.runAddQuery(prep);
            // bis.close();
            idAttach = getLastIdAttach();
            if (idAttach < 1) {
                throw new Exception(
                                    "[SQLFileAttachment : insert -> campaign have no id]");
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }

    /**
     * Update the file in database table ATTACHEMENT, this include the update of
     * content, date and length
     *
     * @param idBdd
     *            : id of the file in the table ATTACHEMENT
     * @param file
     *            : the java file representing the new content
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateFile(int idBdd, SalomeFileWrapper file) throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->updateFile] entry data are not valid");
        }
        if (file == null || file.exists() == false) {
            throw new Exception(
                                "[SQLFileAttachment->updateFile] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            InputStream fis = file.openInputStream();
            BufferedInputStream bis = new BufferedInputStream(fis);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateFileAttach2"); // ok
            prep.setBinaryStream(1, bis, bis.available());
            prep.setLong(2, file.getLength());
            prep.setDate(3, new Date(file.getLastModified()));
            prep.setString(4, file.getName());
            prep.setInt(5, idBdd);
            SQLEngine.runUpdateQuery(prep);

            bis.close();

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->updateFileName]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        /*
         * FileInputStream fis = new FileInputStream(file); BufferedInputStream
         * bis = new BufferedInputStream(fis); updateFileContent(idBdd, bis);
         * updateFileDate(idBdd, new Date(file.lastModified()));
         * updateFileLength(idBdd, file.length()); bis.close();
         */
    }

    /**
     * Update the file name of the attachements identified by idBdd
     *
     * @param idBdd
     * @param name
     *            : the new name
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateFileName(int idBdd, String name) throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->updateFileName] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateFileAttachName"); // ok
            prep.setString(1, name);
            prep.setInt(2, idBdd);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->updateFileName]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Change the content of an existing file in the database
     *
     * @param idBdd
     *            : id of the file in the table ATTACHMENT
     * @param fileContent
     *            : the new content
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateFileContent(int idBdd, byte[] fileContent)
        throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->updateFileContent] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateFileAttach"); // ok
            prep.setBytes(1, fileContent);
            // prep.setBinaryStream(1,fileContent,fileContent.available());
            prep.setInt(2, idBdd);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->updateFileContent]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Change the date of the file in the database
     *
     * @param idBdd
     *            : id of the file in the table ATTACHMENT
     * @param date
     *            : the new date
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateFileDate(int idBdd, Date date) throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->updateFileDate] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateFileAttachDate"); // ok
            prep.setDate(1, date);
            prep.setInt(2, idBdd);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->updateFileDate]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Change the length of the file in the database
     *
     * @param idBdd
     *            : id of the file in the table ATTACHMENT
     * @param length
     *            : the new length no permission needed
     */
    @Override
    public void updateFileLength(int idBdd, long length) throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->updateFileLength] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(0,
                                                     ApiConstants.UPDATE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateFileAttachLength"); // ok
            prep.setLong(1, length);
            prep.setInt(2, idBdd);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLFileAttachment->updateFileLength]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Get the file on local disk (at path specified) for the attachment
     * identified by idBdd
     *
     * @param idBdd
     *            : id of the file in the table ATTACHMENT
     * @return the java file object representing localy the file
     * @throws Exception
     */
    @Override
    public SalomeFileWrapper getFile(int idBdd) throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->getFile] entry data are not valid");
        }
        File file = null;
        InputStream is = null;
        String tmpDir;
        Properties sys = System.getProperties();
        String fs = sys.getProperty("file.separator");

        // Repertoire temporaire systeme
        tmpDir = sys.getProperty("java.io.tmpdir");
        // tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP+"_soap" + fs;
        tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP + fs;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectFileAttachFromDB"); // ok
        prep.setInt(1, idBdd);
        ResultSet res = SQLEngine.runSelectQuery(prep);
        if (res.next()) {
            // file = new File(res.getString("nom_attach"));
            file = new File(tmpDir + res.getString("nom_attach"));
            File fPath = new File(file.getCanonicalPath().substring(0,
                                                                    file.getCanonicalPath().lastIndexOf(fs) + 1));
            if (!fPath.exists()) {
                fPath.mkdirs();
            }
            file.createNewFile();
            // Flux d'ecriture sur le fichier
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            // Flux du fichier stocke dans la BdD
            is = res.getBinaryStream("contenu_attach");
            BufferedInputStream bis = new BufferedInputStream(is);

            // Copie du flux dans le fichier
            int car = bis.read();
            while (car > -1) {
                bos.write(car);
                car = bis.read();
            }
            // Fermeture des flux de donnees
            bos.flush();
            bos.close();
            bis.close();
        }

        return new SalomeFileWrapper(file);
    }

    /**
     * Get the file on local disk (at path specified) for the attachment
     * identified by idBdd
     *
     * @param idBdd
     *            : id of the file in the table ATTACHMENT
     * @param file
     *            ;
     * @return the java file object representing localy the file
     * @throws Exception
     */
    @Override
    public SalomeFileWrapper getFileIn(int idBdd, SalomeFileWrapper file)
        throws Exception {
        if (idBdd < 1) {
            throw new Exception(
                                "[SQLFileAttachment->getFile] entry data are not valid");
        }
        InputStream is = null;
        String tmpDir;
        Properties sys = System.getProperties();
        String fs = sys.getProperty("file.separator");
        // Repertoire temporaire systeme
        tmpDir = sys.getProperty("java.io.tmpdir");
        // tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP+"_soap" + fs;
        tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP + fs;
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectFileAttachFromDB"); // ok
        prep.setInt(1, idBdd);
        ResultSet res = SQLEngine.runSelectQuery(prep);
        if (res.next()) {
            // File f = new File(file.getFileName());
            File f = new File(tmpDir + res.getString("nom_attach"));
            File fPath = new File(f.getCanonicalPath().substring(0,
                                                                 f.getCanonicalPath().lastIndexOf(fs) + 1));
            if (!fPath.exists()) {
                fPath.mkdirs();
            }
            f.createNewFile();

            // Flux d'ecriture sur le fichier
            FileOutputStream fos = new FileOutputStream(f);
            BufferedOutputStream bos = new BufferedOutputStream(fos);

            // Flux du fichier stocke dans la BdD
            is = res.getBinaryStream("contenu_attach");
            BufferedInputStream bis = new BufferedInputStream(is);

            // Copie du flux dans le fichier
            int car = bis.read();
            while (car > -1) {
                bos.write(car);
                car = bis.read();
            }
            // Fermeture des flux de donnees
            bos.flush();
            bos.close();
            bis.close();
            return new SalomeFileWrapper(f);
        }

        return null;
    }

}
