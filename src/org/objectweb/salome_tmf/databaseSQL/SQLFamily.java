package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLFamily;

public class SQLFamily implements ISQLFamily{
        
    /**
     * Insert a Family in the database (table FAMILLE_TEST)
     * @param idProject: id of the project contening the family
     * @param name of the Family
     * @param description of the Family
     * @return the id of the family in the database
     * @throws Exception
     * need permission canCreateTest
     */
    @Override
    public int insert(int idProject, String name, String description) throws Exception {
        int idFamily = -1;
        int transNumber = -1; 
        if (idProject <1 ) {
            throw new Exception("[SQLFamily->insert] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canCreateTest()){
                throw new SecurityException("[SQLFamily : insert -> canCreateTest]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_FAMILY);
            int order = SQLObjectFactory.getInstanceOfISQLProject().getNumberOfFamily(idProject);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFamily"); //ok
            prep.setString(1, name);
            prep.setInt(2, idProject);
            prep.setString(3, description);
            prep.setInt(4, order); //Because index begin at 0
            SQLEngine.runAddQuery(prep);
                        
            idFamily = getID(idProject, name);
            if (idFamily < 1 ) {
                throw new Exception("[SQLFamily->insert] id is not valid");
            } 
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return  idFamily;
    }
        
    /**
     * Attach a file to the Family identified by idFamily (Table FAMILY_ATTACHEMENT)
     * @param idSuite
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idFamily, SalomeFileWrapper f, String description) throws Exception {
        if ( idFamily <1  || f == null) {
            throw new Exception("[SQLFamily->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
                
        if (getFamily(idFamily) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(f,description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addAttachToFamily"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idFamily);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }
        
    /**
     * Attach an Url to the Family identified by idFamily (Table FAMILY_ATTACHEMENT)
     * @param idFamily
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idFamily, String url, String description) throws Exception {
        if ( idFamily <1  || url == null) {
            throw new Exception("[SQLFamily->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getFamily(idFamily) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addAttachToFamily"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idFamily);
            SQLEngine.runAddQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }
        
        
    /**
     * Update a Family identified by idFamily in the database
     * @param idFamily
     * @param name : new name of the family
     * @param description : new description of the family
     * @throws Exception
     * need permission canUpdateTest
     */
    @Override
    public void update(int idFamily, String name, String description) throws Exception {
        if (idFamily <1 ) {
            throw new Exception("[SQLFamily->update] entry data are not valid");
        } 
        int transNumber = -1; 
        if (!SQLEngine.specialAllow) {
            if (!Permission.canUpdateTest()){
                throw new SecurityException("[SQLFamily : update -> canUpdateTest]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_FAMILY);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateFamily"); //ok
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3, idFamily);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
        
    void updateOrder(int idFamily, int order)  throws Exception {
        if (idFamily <1 || order < 0) {
            throw new Exception("[SQLFamily->updateOrder] entry data are not valid");
        } 
        int transNumber = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_FAMILY);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateFamilyOrder"); //ok
            prep.setInt(1, order);
            prep.setInt(2, idFamily);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Increment or decrement the order of the family identified by idFamily in the project
     * Then, reorder other Family to preserve a correct order 
     * @param idFamily
     * @param increment true for doing a decrementation (+1) or false (-1)
     * @return the new order of the family
     * @throws Exception
     */
    @Override
    public int updateOrder(int idFamily, boolean increment)  throws Exception {
        if (idFamily <1) {
            throw new Exception("[SQLFamily->updateOrder] entry data are not valid");
        } 
        int transNumber = -1;
        int order = -1; 
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.UPDATE_FAMILY);
            FamilyWrapper pFamily = getFamily(idFamily);
            order =  pFamily.getOrder();
            if (increment){
                int maxOrder  = SQLObjectFactory.getInstanceOfISQLProject().getNumberOfFamily(pFamily.getIdProject());
                maxOrder --; //Because index begin at 0
                if (order < maxOrder) {
                    FamilyWrapper pFamily2 = SQLObjectFactory.getInstanceOfISQLProject().getFamilyByOrder(pFamily.getIdProject() , order + 1);
                    updateOrder(idFamily, order + 1);
                    updateOrder(pFamily2.getIdBDD(), order);
                    order++;
                }
            } else {
                if (order > 0) {
                    FamilyWrapper pFamily2 = SQLObjectFactory.getInstanceOfISQLProject().getFamilyByOrder(pFamily.getIdProject() , order - 1);
                    updateOrder(idFamily, order - 1);
                    updateOrder(pFamily2.getIdBDD(), order);
                    order--;
                }
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return  order;
    }
        
    /**
     * Delete the Family identified by idProject in the database,
     * Then delete all TestList in the family, an reoder the families in the project
     * @param idFamily
     * @throws Exception
     * @see ISQLTestList.delete(int);
     * need permission canDeleteTest
     */
    @Override
    public void delete(int idFamily) throws Exception {
        delete(idFamily, true);
    }
        
    /**
     * Delete the Family identified by idProject in the database,
     * Then delete all TestList in the family, an reoder the families in the project if reorder = true
     * @param idFamily
     * @param reorder re-order the family in the project
     * @throws Exception
     * @see ISQLTestList.delete(int);
     * need permission canDeleteTest
     * @TODO SOAP
     */
    @Override
    public void delete(int idFamily, boolean reorder) throws Exception {
        if (idFamily <1) {
            throw new Exception("[SQLFamily->delete] entry data are not valid");
        } 
        int transNumber = -1;
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!Permission.canDeleteTest()){
                throw new SecurityException("[SQLFamily : delete -> canDeleteTest]");
            }
            //Require specialAllow
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }
                
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.DELETE_FAMILY);
                        
            FamilyWrapper pFamily = null;
            int order =  -1;
            int maxOrder = -1;
                        
            if (reorder) {
                pFamily = getFamily(idFamily);
                order =  pFamily.getOrder();
                maxOrder = SQLObjectFactory.getInstanceOfISQLProject().getNumberOfFamily(pFamily.getIdProject());
                maxOrder --; //Because index begin at 0
            }
            Util.log("[SQLFamily->delete] maxOrder is " + maxOrder + " and order to delete is " +order);
            //Delete all the Family
            SuiteWrapper[] suiteList = getTestList(idFamily);
            for (int i = 0; i<suiteList.length; i++){
                SuiteWrapper pTestList = suiteList[i];
                SQLObjectFactory.getInstanceOfISQLTestList().delete(pTestList.getIdBDD(), false); //No reorder
            }
                        
            //delete Attachement
            deleteAllAttach(idFamily);
                        
            //delete the family
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteFamilyUsingID"); //ok
            prep.setInt(1, idFamily);
            SQLEngine.runDeleteQuery(prep);
                        
            if (reorder){
                //Update order
                if (order < maxOrder) {
                    for (int i = order + 1; i <= maxOrder ; i++){
                        FamilyWrapper pFamily2 = SQLObjectFactory.getInstanceOfISQLProject().getFamilyByOrder(pFamily.getIdProject(), i);
                        updateOrder(pFamily2.getIdBDD(), i-1);
                    }
                }
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLFamily->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }
        
    /**
     * Delete all attchements of the family identified by idFamily
     * @param idFamily
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idFamily) throws Exception {
        if ( idFamily <1 ) {
            throw new Exception("[SQLFamily->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
                        
            AttachementWrapper[] attachList = getAllAttachemnt(idFamily);
            for (int i = 0 ; i < attachList.length; i++){
                AttachementWrapper pAttachementWrapper = attachList[i];
                deleteAttach(idFamily, pAttachementWrapper.getIdBDD());
            }
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete an attchement idAttach of the family identified by idFamily
     * @param idFamily
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    @Override
    public void deleteAttach(int idFamily, int idAttach) throws Exception {
        if ( idFamily <1 || idAttach < 1 ) {
            throw new Exception("[SQLFamily->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(100, ApiConstants.DELETE_ATTACHMENT);
                        
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromFamily"); //ok
            prep.setInt(1, idFamily);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLFamily->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the family identified by idFamily
     * @param idFamily : id of the family
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAllAttachemnt(int idFamily) throws Exception {
        if ( idFamily <1 ) {
            throw new Exception("[SQLFamily->getAllAttachemnt] entry data are not valid");
        }
        //              Vector result = new Vector();
        //              
        //              Vector fileList =  getAllAttachFiles(idFamily);
        //              Vector urlList = getAllAttachUrls(idFamily);
        //              result.addAll(fileList);
        //              result.addAll(urlList);
        //              
        //              return result;
                
        FileAttachementWrapper[] fileList =  getAllAttachFiles(idFamily);
        UrlAttachementWrapper[] urlList = getAllAttachUrls(idFamily);
                
        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];
                
        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }
                
        return result;
    }
        
    /**
     * Get a Vector of FileAttachementWrapper for the family identified by idFamily
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAllAttachFiles(int idFamily) throws Exception {
        if ( idFamily <1 ) {
            throw new Exception("[SQLFamily->getAllAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectFamilyAttachFiles"); //ok
        prep.setInt(1, idFamily);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach")); 
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }
        
    /**
     * Get a Vector of UrlAttachementWrapper for the family identified by idFamily
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAllAttachUrls(int idFamily) throws Exception {
        if ( idFamily <1 ) {
            throw new Exception("[SQLFamily->getAllAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectFamilyAttachUrls"); //ok
        prep.setInt(1, idFamily);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {  
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }
        
        
    /**
     * Get the Unique ID of the Family identified by name in the project idProject
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idProject, String name) throws Exception {
        if (idProject <1) {
            throw new Exception("[SQLFamily->getID] entry data are not valid");
        } 
        int idFamily = -1;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectIdFamily");
            prep.setString(1,name);
            prep.setInt(2,idProject);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()){
                idFamily = stmtRes.getInt("id_famille");
            }   
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return  idFamily;
    }
        
    /**
     * Get the number of TestList in the Family identified by idFamily
     * @param idFamily
     * @return
     * @throws Exception
     */
    @Override
    public int getNumberOfTestList(int idFamily) throws Exception {
        if (idFamily <1) {
            throw new Exception("[SQLFamily->getNumberOfTestList] entry data are not valid");
        } 
        int result = 0;
        //???? result = -1 in old version of the api (dans tous les NumberOf) -> see //because Index begin at 0
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectFamilySuites"); //ok
        prep.setInt(1,idFamily);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        while  (stmtRes.next()) {
            result ++;
        }
        return  result;
    }
        
    /**
     * Get a SuiteWrapper representing a TestList at order in the Family identified by idFamily
     * @param idFamily
     * @param order
     * @return
     * @throws Exception
     */
    @Override
    public SuiteWrapper getTestListByOrder(int idFamily, int order) throws Exception {
        if (idFamily <1 || order < 0) {
            throw new Exception("[SQLFamily->getTestListByOrder] entry data are not valid");
        } 
        SuiteWrapper pSuite = null;
                
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectSuiteByOrder"); //ok
        prep.setInt(1,idFamily);
        prep.setInt(2,order);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if(stmtRes.next()) {
            pSuite = new SuiteWrapper();
            pSuite.setName(stmtRes.getString("nom_suite"));
            pSuite.setDescription(stmtRes.getString("description_suite"));
            pSuite.setIdBDD(stmtRes.getInt("id_suite"));
            pSuite.setOrder(stmtRes.getInt("ordre_suite"));
            pSuite.setIdFamille(stmtRes.getInt("FAMILLE_TEST_id_famille"));
        }
        return pSuite;
    } 
        
    /**
     * Get a FamilyWrapper representing the family identified by idFamily
     * @param idFamily
     * @return
     * @throws Exception
     */
    @Override
    public FamilyWrapper getFamily(int idFamily)throws Exception {
        if (idFamily <1 ) {
            throw new Exception("[SQLFamily->getFamily] entry data are not valid");
        } 
        FamilyWrapper pFamily = null;
                
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(100, ApiConstants.LOADING);
                        
            PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectFamilyUsingID"); //ok
            prep.setInt(1,idFamily);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                        
            if(stmtRes.next()) {
                pFamily = new FamilyWrapper();
                pFamily.setName(stmtRes.getString("nom_famille"));
                pFamily.setDescription(stmtRes.getString("description_famille"));
                pFamily.setIdBDD(stmtRes.getInt("id_famille"));
                pFamily.setOrder(stmtRes.getInt("ordre_famille"));
                pFamily.setIdProject(stmtRes.getInt("PROJET_VOICE_TESTING_id_projet")); 
            }
                        
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
                
        return pFamily;
    }
        
    /**
     * Get a Vector contening SuiteWrappers representing all testList in the family
     * @param idFamily
     * @return 
     * @throws Exception 
     */
    @Override
    public SuiteWrapper[] getTestList(int idFamily) throws Exception {
        if (idFamily <1 ) {
            throw new Exception("[SQLFamily->getTestList] entry data are not valid");
        } 
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectFamilySuites"); //ok
        prep.setInt(1,idFamily);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while  (stmtRes.next()) {
            SuiteWrapper pTestList = new SuiteWrapper();
            pTestList.setIdBDD(stmtRes.getInt("id_suite"));
            pTestList.setDescription(stmtRes.getString("description_suite"));
            pTestList.setName(stmtRes.getString("nom_Suite"));
            pTestList.setOrder(stmtRes.getInt("ordre_suite"));
            pTestList.setIdFamille(stmtRes.getInt("FAMILLE_TEST_id_famille"));
            result.addElement(pTestList);
        }
        SuiteWrapper[] swArray = new SuiteWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            swArray[i] = (SuiteWrapper) result.get(i);
        }
        return swArray;
    }
        
}
