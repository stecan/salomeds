/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ExecutionActionWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionActionResult;

/* TODO 
 * ATTACHEMENT MANAGEMENT (table  EXEC_ACTION_ATTACH)
 */

public class SQLExecutionActionResult implements ISQLExecutionActionResult{
        
    /**
     * Insert a result of the execution of the action idAction in the execution result idExecRes
     * @param idExecRes id of the ExecutionResult (table RES_EXEC_CAMP)
     * @param idTest : id of the test
     * @param idAction : id of the action
     * @param description : description of the action
     * @param awaitedRes : awaited result of the action
     * @param effectiveRes : the effective result of the action
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @return the id of the row in the table EXEC_ACTION
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public int insert(int idExecRes, int idTest, int idAction, String description, String awaitedRes, String effectiveRes,  String result) throws Exception {
        int idResActionExec = -1;
        int idRestestExec = SQLObjectFactory.getInstanceOfISQLExecutionTestResult().getID(idExecRes, idTest);
        if (idRestestExec != -1){
            idResActionExec = insert(idRestestExec, idAction, description, awaitedRes, effectiveRes, result);
        }
        return idResActionExec;
    }
        
        
    /**
     * Insert a result of the execution of the action idAction in the execution result idExecRes
     * @param idRestestExec : id of the ExecutionTestResult (EXEC_CAS)
     * @param idAction : id of the action
     * @param description : description of the action
     * @param awaitedRes : awaited result of the action
     * @param effectiveRes : the effective result of the action
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @return the id of the row in the table EXEC_ACTION
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public int insert(int idRestestExec, int idAction, String description, String awaitedRes,  String effectiveRes, String result) throws Exception {
        if (idRestestExec <1 || idAction < 1) {
            throw new Exception("[SQLExecutionTestResult->insert] entry data are not valid");
        } 
        int idResActionExec = -1;
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : insert -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(110, ApiConstants.INSERT_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep = SQLEngine.getSQLAddQuery("addResExecAction2"); //ok
            prep.setInt(1, idRestestExec);
            prep.setInt(2, idAction);
            prep.setString(3, result);
            prep.setString(4, effectiveRes);
            prep.setString(5, description);
            prep.setString(6, awaitedRes);
            SQLEngine.runAddQuery(prep);
                        
            idResActionExec = getID(idRestestExec, idAction);
            if (idResActionExec <1 ) {
                throw new Exception("[SQLExecutionTestResult->insert] id are not valid");
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionActionResult->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idResActionExec;
    }
        
        
    /**
     * Update the effective result of the execution of the action
     * @param idResActionExec
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idResActionExec, String effectiveResult, String result) throws Exception {
        ExecutionActionWrapper pExecutionActionWrapper =  getExecAction(idResActionExec);
        if (pExecutionActionWrapper != null){
            update(pExecutionActionWrapper.getIdExecTest(),pExecutionActionWrapper.getIdAction(), effectiveResult, result);
        }
    }
        
    /**
     * Update the effective result of the execution of the action
     * @param idExecRes
     * @param idTest
     * @param idAction
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idExecRes, int idTest, int idAction, String effectiveResult, String result) throws Exception {
        int idRestestExec = SQLObjectFactory.getInstanceOfISQLExecutionTestResult().getID(idExecRes, idTest);
        if (idRestestExec != -1){
            update(idRestestExec, idAction, effectiveResult, result);
        }
    }
        
        
    /**
     * Update the effective result of the execution of the action
     * @param idRestestExec
     * @param idAction
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idRestestExec, int idAction, String effectiveResult, String result) throws Exception {
        int transNumber = -1;
        if (idRestestExec <1 || idAction < 1) {
            throw new Exception("[SQLExecutionTestResult->update] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : update -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateActionEffectivResult"); //ok    
            prep.setString(1, effectiveResult);
            prep.setString(2, result);
            prep.setInt(3,idRestestExec);
            prep.setInt(4,idAction);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionActionResult->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Update the description, the awaited result and effective result of the execution of the action
     * @param idRestestExec
     * @param idAction
     * @param description
     * @param awaitedResult
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idRestestExec, int idAction, String description, String awaitedResult, String effectiveResult, String result) throws Exception {
        int transNumber = -1;
        if (idRestestExec <1 || idAction < 1) {
            throw new Exception("[SQLExecutionTestResult->update] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : update -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateActionExecution"); //ok 
            prep.setString(1, description);
            prep.setString(2, awaitedResult);
            prep.setString(3, effectiveResult);
            prep.setString(4, result);
            prep.setInt(5,idRestestExec);
            prep.setInt(6,idAction);
            SQLEngine.runUpdateQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionActionResult->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete result of all actions in the ExecResulTest 
     * @param idRestestExec
     * need permission canExecutCamp
     */
    @Override
    public void deleteAll(int idRestestExec) throws Exception {
        int transNumber = -1;
        if (idRestestExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->deleteAll] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : deleteAll -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_TEST_RESULT);
                        
            //TODO DeleteAttachment
            PreparedStatement prep  = SQLEngine.getSQLDeleteQuery("deleteResExecActionsFromResExecCas"); //ok
            prep.setInt(1,idRestestExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionActionResult->deleteAll]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Delete result of the action in the ExecResulAction identified by idResActionExec   
     * @param idResActionExec
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void delete(int idResActionExec) throws Exception {
        int transNumber = -1;
        if (idResActionExec <1 ) {
            throw new Exception("[SQLExecutionTestResult->delete] entry data are not valid");
        } 
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionTestResult : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_TEST_RESULT);
                        
            PreparedStatement prep  = SQLEngine.getSQLDeleteQuery("deleteResExecAction"); //ok
            prep.setInt(1,idResActionExec);
            SQLEngine.runDeleteQuery(prep);
                        
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionActionResult->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }
        
    /**
     * Get the id of the ExecutionActionResult name in the project idProject
     * @param idRestestExec
     * @param idAction
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idRestestExec, int idAction) throws Exception {
        if (idRestestExec <1  || idAction < 1) {
            throw new Exception("[SQLExecutionTestResult->getID] entry data are not valid");
        }  
        int idResActionExec = -1;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecActionID"); //ok
        prep.setInt(1,idRestestExec);
        prep.setInt(2,idAction);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if (stmtRes.next()){
            idResActionExec = stmtRes.getInt("id_exec_action");
        }
        return idResActionExec;
    }
        
    /**
     * Get an ExecutionActionWrapper representing the row idResActionExec in the table EXEC_ACTION 
     * @param idResActionExec
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionActionWrapper getExecAction(int idResActionExec) throws Exception {
        if (idResActionExec <1) {
            throw new Exception("[SQLExecutionTestResult->getExecAction] entry data are not valid");
        } 
        ExecutionActionWrapper pExecutionActionWrapper = null;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecAction"); //ok
        prep.setInt(1,idResActionExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
                
        if (stmtRes.next()){
            pExecutionActionWrapper = new ExecutionActionWrapper();
            pExecutionActionWrapper.setIdBDD(idResActionExec);
            pExecutionActionWrapper.setIdExecTest(stmtRes.getInt("EXEC_CAS_id_exec_cas"));
            pExecutionActionWrapper.setIdAction(stmtRes.getInt("ACTION_TEST_id_action"));
            pExecutionActionWrapper.setResult(stmtRes.getString("res_exec_action"));
            pExecutionActionWrapper.setEffectivResult(stmtRes.getString("effectiv_res_action"));
            pExecutionActionWrapper.setDescription(stmtRes.getString("ACTION_TEST_description_action"));
            pExecutionActionWrapper.setAwaitedResult(stmtRes.getString("ACTION_TEST_res_attendu_action"));
        }
        return pExecutionActionWrapper;
    }
        
}
