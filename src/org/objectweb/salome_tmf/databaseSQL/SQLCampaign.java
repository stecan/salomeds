/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLCampaign;

public class SQLCampaign implements ISQLCampaign {

    /**
     * Inset a campaign in table CAMPAGNE_TEST
     * 
     * @param idProject
     *            : id of the projet where to insert the campaign
     * @param name
     *            of the campaign
     * @param description
     *            of the campaign
     * @param idPers
     *            : id of the user who insert the campaign
     * @return the id (id_camp) of new campaign
     * @throws Exception
     *             need permission canCreateCamp
     */
    @Override
    public int insert(int idProject, String name, String description, int idPers)
        throws Exception {
        int idCamp = -1;
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canCreateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : insert -> canCreateCamp]");
            }
        }
        if (idProject < 1) {
            throw new Exception("[SQLCampaign->insert] project have no id");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.INSERT_CAMPAIGN);
            int nbTestCamp = -1;
            nbTestCamp = getNumberOfCampaign(idProject);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addCampaign"); // ok
            prep.setString(1, name);
            prep.setDate(2, Util.getCurrentDate());
            prep.setTime(3, Util.getCurrentTime());
            prep.setString(4, description);
            prep.setInt(5, idPers);
            prep.setInt(6, idProject);
            prep.setInt(7, nbTestCamp); // because order index begin at 0
            SQLEngine.runAddQuery(prep);

            idCamp = getID(idProject, name);
            if (idCamp < 1) {
                throw new Exception(
                                    "[SQLCampaign : insert -> campaign have no id]");
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->insert] " + name + " " + " " + description
                     + " " + idPers + " " + idProject, e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idCamp;

    }

    /**
     * Import a test identified by idTest (in table CAS_TEST) in the campaign
     * identified by idCamp in table CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @return the order of the test in the campaign
     * @throws Exception
     *             need permission canCreateCamp
     */
    @Override
    public int importTest(int idCamp, int idTest, int userID) throws Exception {
        int transNumber = -1;
        int order = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : importTest -> canCreateCamp]");
            }
        }
        if (idCamp < 1 || idTest < 1) {
            throw new Exception(
                                "[SQLCampaign->importTest] entry data are not valid");
        }

        try {
            transNumber = SQLEngine.beginTransaction(110,
                                                     ApiConstants.INSERT_TEST_INTO_CAMPAIGN);
            order = getNumberOfTestInCampaign(idCamp);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addTestToCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idTest);
            prep.setInt(3, order);// because order index begin at 0
            prep.setInt(4, userID);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->importTest]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return order;
    }

    /**
     * Attach a file to a campaign identified by idCamp (table
     * CAMPAGNE_ATTACHEMENT)
     * 
     * @param idCamp
     * @param file
     *            : the file to insert in the database
     * @param description
     * @return the id of the attachement in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String) no permission needed
     */
    @Override
    public int addAttachFile(int idCamp, SalomeFileWrapper file,
                             String description) throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idCamp < 1 || file == null) {
            throw new Exception(
                                "[SQLCampaign->addAttachFile] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment()
                .insert(file, description);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("addFileAttachToCampaign"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idAttach);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }

    /**
     * Attach a URL to a campaign identified by idCamp (table
     * CAMPAGNE_ATTACHEMENT)
     * 
     * @param idCamp
     * @param url
     *            : the url to insert in the database
     * @param description
     * @return the id of the attachement in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String String) no permission needed
     */
    @Override
    public int addAttachUrl(int idCamp, String url, String description)
        throws Exception {
        int transNumber = -1;
        int idAttach = -1;
        if (idCamp < 1 || url == null) {
            throw new Exception(
                                "[SQLCampaign->addAttachUrl] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment()
                .insert(url, description);

            PreparedStatement prep = SQLEngine
                .getSQLAddQuery("addUrlAttachToCampaign"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idAttach);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }

        return idAttach;
    }

    /**
     * Update the name and the description of the campaign identified by idCamp
     * 
     * @param idCamp
     *            of the campaign
     * @param name
     *            : new name of the campaign
     * @param description
     *            : new description of the campaign
     * @throws Exception
     *             need permission canUpdateCamp
     */
    @Override
    public void update(int idCamp, String name, String description)
        throws Exception {
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : update -> canUpdateCamp]");
            }
        }
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->update] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);
            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateCampaign"); // ok
            prep.setString(1, name);
            prep.setString(2, description);
            prep.setInt(3, idCamp);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    void updateOrder(int idCamp, int order) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || order < 0) {
            throw new Exception(
                                "[SQLCampaign->updateOrder] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateCampaignOrder"); // ok
            prep.setInt(1, order);
            prep.setInt(2, idCamp);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update the order of the campaign by incrementation (if increment = true)
     * or decrementation
     * 
     * @param idCamp
     *            : id of the campaign
     * @param increment
     * @return
     * @throws Exception
     *             need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public int updateOrder(int idCamp, boolean increment) throws Exception {
        int transNumber = -1;
        int orderIndex = -1;
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->updateOrder] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp() || Permission.canExecutCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateOrder -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            CampaignWrapper pCamp = getCampaign(idCamp);
            orderIndex = pCamp.getOrder();
            if (increment) {
                int maxOrder = getNumberOfCampaign(pCamp.getIdProject());
                maxOrder--; // because order index begin at 0
                if (orderIndex < maxOrder) {
                    CampaignWrapper pCamp2 = getCampaignByOrder(pCamp
                                                                .getIdProject(), orderIndex + 1);
                    updateOrder(idCamp, orderIndex + 1);
                    updateOrder(pCamp2.getIdBDD(), orderIndex);
                    orderIndex++;
                }
            } else {
                if (orderIndex > 0) {
                    CampaignWrapper pCamp2 = getCampaignByOrder(pCamp
                                                                .getIdProject(), orderIndex - 1);
                    updateOrder(idCamp, orderIndex - 1);
                    updateOrder(pCamp2.getIdBDD(), orderIndex);
                    orderIndex--;
                }
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return orderIndex;
    }

    void updateTestOrder(int idCamp, int idTest, int order) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idTest < 1 || order < 0) {
            throw new Exception(
                                "[SQLCampaign->updateTestOrder] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateTestCampaignOrder"); // ok
            prep.setInt(1, order);
            prep.setInt(2, idCamp);
            prep.setInt(3, idTest);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * replace all reference of user oldIdUser by newIdUser in the table
     * (CAMPAGNE_CAS) whrerre campagne = idCamp
     * 
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     *             no permission needed
     */
    @Override
    public void updateUserRef(int idCamp, int oldIdUser, int newIdUser)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || oldIdUser < 1 || newIdUser < 1) {
            throw new Exception(
                                "[SQLCampaign->updateUserRef] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateCampagneUser"); // OK
            prep.setInt(1, newIdUser);
            prep.setInt(2, oldIdUser);
            prep.setInt(3, idCamp);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateUserRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update test suite order in the campaign (+1 if increment = true) or -1
     * 
     * @param idCamp
     *            : id of the campaign
     * @param idSuite
     *            : id of the suite in table SUITE_TEST
     * @param increment
     * @throws Exception
     *             need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public void updateTestSuiteOrder(int idCamp, int idSuite, boolean increment)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idSuite < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestSuiteOrder] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp() || Permission.canExecutCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateTestSuiteOrder -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            TestCampWrapper pLastInSuite = getInCampaignLastTestInSuite(idCamp,
                                                                        idSuite);
            TestCampWrapper pFirstInSuite = getInCampaignFirstTestInSuite(
                                                                          idCamp, idSuite);
            int firstorder = pFirstInSuite.getOrder();
            int lastorder = pLastInSuite.getOrder();
            int nbTesttoMove = (lastorder - firstorder) + 1;
            if (increment) { // desc
                TestCampWrapper pFirstInSuite2 = getTestCampByOrder(idCamp,
                                                                    lastorder + 1);
                TestWrapper pTest = SQLObjectFactory.getInstanceOfISQLTest()
                    .getTest(pFirstInSuite2.getIdBDD());
                int size = getSizeOfSuiteInCampaign(idCamp, pTest.getIdSuite());

                int tmpOrder = lastorder;
                for (int i = 0; i < nbTesttoMove; i++) {
                    int idTest = getTestCampByOrder(idCamp, tmpOrder)
                        .getIdBDD();
                    for (int j = 0; j < size; j++) {
                        updateTestOrder(idCamp, idTest, true);
                    }
                    tmpOrder--;
                }
            } else { // mont
                TestCampWrapper pLastInSuite2 = getTestCampByOrder(idCamp,
                                                                   firstorder - 1);
                TestWrapper pTest = SQLObjectFactory.getInstanceOfISQLTest()
                    .getTest(pLastInSuite2.getIdBDD());
                int size = getSizeOfSuiteInCampaign(idCamp, pTest.getIdSuite());

                int tmpOrder = firstorder;
                for (int i = 0; i < nbTesttoMove; i++) {
                    int idTest = getTestCampByOrder(idCamp, tmpOrder)
                        .getIdBDD();
                    for (int j = 0; j < size; j++) {
                        updateTestOrder(idCamp, idTest, false);
                    }
                    tmpOrder++;
                }
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestSuiteOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update a family order in the campaign (+1 if increment = true) or -1
     * 
     * @param idCamp
     * @param idFamily
     *            : id of the suite in table FAMILLE_TEST
     * @param increment
     * @throws Exception
     *             need permission canUpdateCamp or canExecutCamp
     */
    @Override
    public void updateTestFamilyOrder(int idCamp, int idFamily,
                                      boolean increment) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idFamily < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestFamilyOrder] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp() || Permission.canExecutCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateTestFamilyOrder -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            TestCampWrapper pLastInSuite = getInCampaignLastTestInFamily(
                                                                         idCamp, idFamily);
            TestCampWrapper pFirstInSuite = getInCampaignFirstTestInFamily(
                                                                           idCamp, idFamily);
            int firstorder = pFirstInSuite.getOrder();
            int lastorder = pLastInSuite.getOrder();
            int nbTesttoMove = (lastorder - firstorder) + 1;
            if (increment) { // desc
                TestCampWrapper pFirstInSuite2 = getTestCampByOrder(idCamp,
                                                                    lastorder + 1);
                TestWrapper pTest = SQLObjectFactory.getInstanceOfISQLTest()
                    .getTest(pFirstInSuite2.getIdBDD());
                SuiteWrapper pSuite = SQLObjectFactory
                    .getInstanceOfISQLTestList().getTestList(
                                                             pTest.getIdSuite());
                int size = getSizeOfFamilyInCampaign(idCamp, pSuite
                                                     .getIdFamille());
                int tmpOrder = lastorder;
                for (int i = 0; i < nbTesttoMove; i++) {
                    int idTest = getTestCampByOrder(idCamp, tmpOrder)
                        .getIdBDD();
                    for (int j = 0; j < size; j++) {
                        updateTestOrder(idCamp, idTest, true);
                    }
                    tmpOrder--;
                }
            } else { // mont
                TestCampWrapper pLastInSuite2 = getTestCampByOrder(idCamp,
                                                                   firstorder - 1);
                TestWrapper pTest = SQLObjectFactory.getInstanceOfISQLTest()
                    .getTest(pLastInSuite2.getIdBDD());
                SuiteWrapper pSuite = SQLObjectFactory
                    .getInstanceOfISQLTestList().getTestList(
                                                             pTest.getIdSuite());
                int size = getSizeOfFamilyInCampaign(idCamp, pSuite
                                                     .getIdFamille());
                int tmpOrder = firstorder;
                for (int i = 0; i < nbTesttoMove; i++) {
                    int idTest = getTestCampByOrder(idCamp, tmpOrder)
                        .getIdBDD();
                    for (int j = 0; j < size; j++) {
                        updateTestOrder(idCamp, idTest, false);
                    }
                    tmpOrder++;
                }

            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestFamilyOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update a test order in the campaign (+1 if increment = true) or -1
     * 
     * @param idCamp
     * @param idTest
     *            : id of the test in the table CAS_TEST
     * @param increment
     * @return
     * @throws Exception
     *             need permission canUpdateCamp
     */
    @Override
    public int updateTestOrder(int idCamp, int idTest, boolean increment)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idTest < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestOrder] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateTestOrder -> canUpdateCamp]");
            }
        }
        int orderIndex = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            TestCampWrapper pTestCas = getTestCampById(idCamp, idTest);
            orderIndex = pTestCas.getOrder();
            if (increment) {
                int maxOrder = getNumberOfTestInCampaign(idCamp);
                maxOrder--; // because order index begin at 0
                if (orderIndex < maxOrder) {
                    TestCampWrapper pTestCas2 = getTestCampByOrder(idCamp,
                                                                   orderIndex + 1);
                    updateTestOrder(idCamp, idTest, orderIndex + 1);
                    updateTestOrder(idCamp, pTestCas2.getIdBDD(), orderIndex);
                    orderIndex++;
                }
            } else {
                if (orderIndex > 0) {
                    TestCampWrapper pTestCas2 = getTestCampByOrder(idCamp,
                                                                   orderIndex - 1);
                    updateTestOrder(idCamp, idTest, orderIndex - 1);
                    updateTestOrder(idCamp, pTestCas2.getIdBDD(), orderIndex);
                    orderIndex--;
                }
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestOrder]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return orderIndex;
    }

    /**
     * Update test Assignation (field assigned_user_id) in CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    @Override
    public void updateTestAssignation(int idCamp, int idTest, int assignedID)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idTest < 1 || assignedID < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestAssignation] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateTestAssignation -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateAssignedTestInCampagne"); // OK
            prep.setInt(1, assignedID);
            prep.setInt(2, idCamp);
            prep.setInt(3, idTest);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestAssignation]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update Suite Assignation (field assigned_user_id for all tests in the
     * suite) in CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    @Override
    public void updateSuiteAssignation(int idCamp, int idSuite, int assignedID)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idSuite < 1 || assignedID < 1) {
            throw new Exception(
                                "[SQLCampaign->updateSuiteAssignation] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateSuiteAssignation -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateAssignedSuiteInCampagne"); // OK
            prep.setInt(1, assignedID);
            prep.setInt(2, idCamp);
            prep.setInt(3, idSuite);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateSuiteAssignation]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update Suite Assignation (field assigned_user_id for all tests in the
     * suite) in CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    @Override
    public void updateFamilyAssignation(int idCamp, int idFamille,
                                        int assignedID) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idFamille < 1 || assignedID < 1) {
            throw new Exception(
                                "[SQLCampaign->updateFamilyAssignation] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateFamilyAssignation -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateAssignedFamilyInCampagne"); // OK
            prep.setInt(1, assignedID);
            prep.setInt(2, idCamp);
            prep.setInt(3, idFamille);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateFamilyAssignation]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update test Assignation for all reference of old assignation (field
     * assigned_user_id) in CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    @Override
    public void updateTestAssignationRef(int idCamp, int assignedOldID,
                                         int assignedNewID) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || assignedOldID < 1 || assignedNewID < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestAssignationRef] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateTestAssignationRef -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateAllAssignedTestInCampagne"); // OK
            prep.setInt(1, assignedNewID);
            prep.setInt(2, idCamp);
            prep.setInt(3, assignedOldID);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateTestAssignationRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Update All test Assignation (field assigned_user_id) in CAMPAGNE_CAS
     * 
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    @Override
    public void updateCampagneAssignationRef(int idCamp, int assignedNewID)
        throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || assignedNewID < 1) {
            throw new Exception(
                                "[SQLCampaign->updateTestAssignationRef] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canUpdateCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : updateCampagneAssignedTest -> canUpdateCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.UPDATE_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLUpdateQuery("updateCampagneAssignedTest"); // OK
            prep.setInt(1, assignedNewID);
            prep.setInt(2, idCamp);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->updateCampagneAssignationRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete a campaign and it's related links in the database include : delete
     * test in campaign, dataset, execution (and result), and attachment reorder
     * the campaign in the project
     * 
     * @param idCamp
     * @throws Exception
     *             need permission canDeleteCamp
     * @see deleteTest(int,int)
     * @see deleteAllExec(int)
     * @see deleteAllAttach(int)
     * @see deleteAllDataset(int) need permission canDeleteCamp
     */
    @Override
    public void delete(int idCamp) throws Exception {
        delete(idCamp, true);
    }

    /**
     * Delete a campaign and it's related links in the database include : delete
     * test in campaign, dataset, execution (and result), and attachment reorder
     * the campaign in the project if reorder = true
     * 
     * @param idCamp
     * @throws Exception
     *             need permission canDeleteCamp
     * @see deleteTest(int,int)
     * @see deleteAllExec(int)
     * @see deleteAllAttach(int)
     * @see deleteAllDataset(int) need permission canDeleteCamp
     * @TODO SOAP
     */
    @Override
    public void delete(int idCamp, boolean reorder) throws Exception {
        int transNumber = -1;
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->delete] entry data are not valid");
        }
        boolean dospecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canDeleteCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : delete -> canDeleteCamp]");
            }
            dospecialAllow = true;
            SQLEngine.setSpecialAllow(true);
        }

        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_CAMPAIGN);

            CampaignWrapper pCamp = null;
            int orderIndex = -1;
            int maxOrder = -1;
            maxOrder--; // because order index begin at 0

            if (reorder) {
                pCamp = getCampaign(idCamp);
                orderIndex = pCamp.getOrder();
                maxOrder = getNumberOfCampaign(pCamp.getIdProject());
                maxOrder--; // because order index begin at 0
            }

            /* ConnectionData.getCampTestDelete().deleteCampaignUsingID(idBdd); */
            /* Delete test in campaign */
            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignTestsUsingID"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()) {
                int idTest = stmtRes.getInt("CAS_TEST_id_cas");
                purgeTest(idCamp, idTest, true);
            }

            /* Delete execution in campaign */
            deleteAllExec(idCamp);

            // Suppression des attachements
            deleteAllAttach(idCamp);

            /* Delete dataset */
            deleteAllDataset(idCamp);

            // Suppression de la campagne de test
            prep = SQLEngine.getSQLDeleteQuery("deleteCampaign"); // ok
            prep.setInt(1, idCamp);
            SQLEngine.runDeleteQuery(prep);

            if (reorder) {
                if (orderIndex < maxOrder) {
                    for (int i = orderIndex + 1; i <= maxOrder; i++) {
                        CampaignWrapper pCamp2 = getCampaignByOrder(pCamp
                                                                    .getIdProject(), i);
                        if (pCamp2 != null) {
                            updateOrder(pCamp2.getIdBDD(), i - 1);
                        }
                    }
                }
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            if (dospecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLCampaign->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (dospecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }

    /**
     * Delete all executions reference (and it's result) for the campaign
     * identified by idCamp
     * 
     * @param idCamp
     * @throws Exception
     * @see ISQLExecution.delete(int) need permission canExecutCamp or
     *      canDeleteCamp
     */
    @Override
    public void deleteAllExec(int idCamp) throws Exception {
        int transNumber = -1;
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteAllExec] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canDeleteCamp())) {
                throw new SecurityException(
                                            "[SQLDataset : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_EXECUTION);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignExecutions"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()) {
                int idExec = stmtRes.getInt("id_exec_camp");
                SQLObjectFactory.getInstanceOfISQLExecution().delete(idExec);
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->deleteAllExec]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete all datasets reference for the campaign identified by idCamp
     * 
     * @param idCamp
     * @throws Exception
     * @see ISQLDataset.delete(int) need permission canExecutCamp or
     *      canDeleteCamp
     */
    @Override
    public void deleteAllDataset(int idCamp) throws Exception {
        int transNumber = -1;
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteAllDataset] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp() || Permission.canDeleteCamp())) {
                throw new SecurityException(
                                            "[SQLDataset : delete -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_DATA_SET);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampJeuxDonnees"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                int idDataset = stmtRes.getInt("id_jeu_donnees");
                SQLObjectFactory.getInstanceOfISQLDataset().delete(idDataset);
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->deleteAllDataset]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    @Override
    public void deleteTest(int idCamp, int testId, boolean deleteExec)
        throws Exception {
        deleteTest(idCamp, testId, deleteExec, true);
    }

    /**
     * Delete a test reference for the campaign identified by idCamp then reoder
     * the test in the campaign, and clean result on Excecution then delete
     * execution if empty and deleteExec = true
     * 
     * @param idCamp
     * @param testId
     * @param deleteExec
     * @throws Exception
     *             need permission canDeleteCamp
     */
    public void deleteTest(int idCamp, int testId, boolean deleteExec,
                           boolean reorder) throws Exception {
        int transNumber = -1;
        int orderIndex = -1;
        if (idCamp < 1 || testId < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteTest] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canDeleteCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : deleteTest -> canDeleteCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_TEST_FROM_CAMPAIGN);
            int maxOrder = -1;
            TestCampWrapper pTestCamp = null;
            if (reorder) {
                pTestCamp = getTestCampById(idCamp, testId);
                orderIndex = pTestCamp.getOrder();
                maxOrder = getNumberOfTestInCampaign(idCamp);
                maxOrder--; // because index begin at 0
            }

            purgeTest(idCamp, testId, deleteExec);

            if (reorder) {
                if (orderIndex < maxOrder) {
                    for (int i = orderIndex + 1; i <= maxOrder; i++) {
                        TestCampWrapper pTestCamp2 = getTestCampByOrder(idCamp,
                                                                        i);
                        updateTestOrder(idCamp, pTestCamp2.getIdBDD(), i - 1);
                    }
                }
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->deleteTest]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    void purgeTest(int idCamp, int idTest, boolean deleteExec) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idTest < 1) {
            throw new Exception(
                                "[SQLCampaign->purgeTest] entry data are not valid");
        }
        boolean doSpecialAllow = false;
        if (!SQLEngine.isSpecialAllow()) {
            SQLEngine.setSpecialAllow(true);
            doSpecialAllow = true;
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_TEST_FROM_CAMPAIGN);
            PreparedStatement prep;
            int id_cas_exec = 0;

            /*
             * Suppression de tous les resultats d'execution du test dans la
             * campagne
             */
            if (deleteExec) {
                prep = SQLEngine
                    .getSQLSelectQuery("selectAllResExecTestInCamp"); // ok
                prep.setInt(1, idTest);
                prep.setInt(2, idCamp);
                ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

                while (stmtRes.next()) {
                    id_cas_exec = stmtRes.getInt("id_exec_cas");
                    SQLObjectFactory.getInstanceOfISQLExecutionTestResult()
                        .delete(id_cas_exec);
                }
            }
            /* Suppression du test de la campagagne de tests */
            prep = SQLEngine.getSQLDeleteQuery("deleteTestFromCampaign"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idTest);
            SQLEngine.runDeleteQuery(prep);

            /*
             * Si la campagne de test est vide, on supprime ses resultats
             * d'execution
             */
            prep = SQLEngine.getSQLSelectQuery("SelectAllTestFromCampagne"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes2 = SQLEngine.runSelectQuery(prep);
            if (!stmtRes2.next()) {
                prep = SQLEngine
                    .getSQLSelectQuery("selectExecutionWithCampagne"); // ok
                prep.setInt(1, idCamp);
                ResultSet stmtRes3 = SQLEngine.runSelectQuery(prep);
                while (stmtRes3.next()) {
                    int id_exec_camp = stmtRes3.getInt("id_exec_camp");
                    if (deleteExec) {
                        SQLObjectFactory.getInstanceOfISQLExecution().delete(
                                                                             id_exec_camp);
                    } else {
                        // SQLObjectFactory.getInstanceOfISQLExecution().deleteAllExecResult(id_exec_camp);
                    }
                }
            }
            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            if (doSpecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLCampaign->purgeTest]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (doSpecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }

    /**
     * Delete an attachment idAttach in the campaign (table CAMPAGNE_ATTACHEMENT
     * and ATTACHEMENT)
     * 
     * @param idCamp
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int) no permission needed
     */
    @Override
    public void deleteAttach(int idCamp, int idAttach) throws Exception {
        int transNumber = -1;
        if (idCamp < 1 || idAttach < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteAttach] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_ATTACHMENT);

            PreparedStatement prep = SQLEngine
                .getSQLDeleteQuery("deleteAttachFromCampaign"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idAttach);
            SQLEngine.runDeleteQuery(prep);

            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete all attachments in the campaign (table CAMPAGNE_ATTACHEMENT and
     * ATTACHEMENT)
     * 
     * @param idCamp
     * @see deleteAttach(int, int)
     * @throws Exception
     */
    @Override
    public void deleteAllAttach(int idCamp) throws Exception {
        int transNumber = -1;
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteAllAttach] entry data are not valid");
        }
        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_ATTACHMENT);

            AttachementWrapper[] attachList = getAttachs(idCamp);
            for (int i = 0; i < attachList.length; i++) {
                AttachementWrapper pAttachementWrapper = attachList[i];
                deleteAttach(idCamp, pAttachementWrapper.getIdBDD());
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            Util.err("[SQLCampaign->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete a test reference in all campaign and then reorder the tests in the
     * campaign
     * 
     * @param idTest
     * @see deleteTest(int, int) need permission canDeleteCamp
     * @throws Exception
     */
    @Override
    public void deleteTestInAllCampaign(int idTest) throws Exception {
        int transNumber = -1;
        if (idTest < 1) {
            throw new Exception(
                                "[SQLCampaign->deleteTestInAllCampaign] entry data are not valid");
        }
        boolean doSpecialAllow = false;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canDeleteCamp())) {
                throw new SecurityException(
                                            "[SQLCampaign : deleteTestInAllCampaign -> canDeleteCamp]");
            }
            SQLEngine.setSpecialAllow(true);
            doSpecialAllow = true;
        }

        try {
            transNumber = SQLEngine.beginTransaction(10,
                                                     ApiConstants.DELETE_TEST_FROM_CAMPAIGN);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("SelectCampagneFormTest"); // ok
            prep.setInt(1, idTest);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                int idCamp = stmtRes.getInt("CAMPAGNE_TEST_id_camp");
                // purgeTest(idCamp, idTest, true);
                deleteTest(idCamp, idTest, true);
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e) {
            if (doSpecialAllow) {
                SQLEngine.setSpecialAllow(false);
            }
            Util.err("[SQLCampaign->deleteTestInAllCampaign]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        if (doSpecialAllow) {
            SQLEngine.setSpecialAllow(false);
        }
    }

    /**
     * Get a Vector of AttachementWrapper (FileAttachementWrapper,
     * UrlAttachementWrapper) for the campaign identified by idCamp
     * 
     * @param idCamp
     *            : id of the campaign
     * @return
     * @throws Exception
     */

    @Override
    public AttachementWrapper[] getAttachs(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getAttachs] entry data are not valid");
        }

        FileAttachementWrapper[] fileList = getAttachFiles(idCamp);
        UrlAttachementWrapper[] urlList = getAttachUrls(idCamp);

        AttachementWrapper[] result = new AttachementWrapper[fileList.length
                                                             + urlList.length];

        for (int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for (int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }

        return result;
    }

    /**
     * Get a Vector of FileAttachementWrapper for the campaign identified by
     * idCamp
     * 
     * @param idCamp
     *            : id of the campaign
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectCampaignAttachFiles"); // ok
        prep.setInt(1, idCamp);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result
                                                                       .size()];
        for (int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }
        return fawArray;
    }

    /**
     * Get a Vector of UrlAttachementWrapper for the campaign identified by
     * idCamp
     * 
     * @param idCamp
     *            : id of the campaign
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectCampaignAttachUrls"); // ok
        prep.setInt(1, idCamp);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            // pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes
                                          .getString("description_attach"));
            ;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result
                                                                     .size()];
        for (int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }

    /**
     * Return the number of campaign in the project identified by idProject
     * 
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public int getNumberOfCampaign(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLCampaign->getNumberOfCampaign] entry data are not valid");
        }
        int numberOfCampaign = 0;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectProjectCampaigns"); // ok
            prep.setInt(1, idProject);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                numberOfCampaign++;
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return numberOfCampaign;
    }

    /**
     * Return the number of tests in the campaign identified by idCamp
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public int getNumberOfTestInCampaign(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getNumberOfTestInCampaign] entry data are not valid");
        }
        int numberOftest = 0;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignTestsUsingID"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()) {
                numberOftest++;
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return numberOftest;
    }

    /**
     * Return a Vector of CampaignWrapper representing the campaign of the
     * project identified by idProject
     * 
     * @param idProject
     * @return
     * @throws Exception
     */
    @Override
    public CampaignWrapper[] getAllCampaigns(int idProject) throws Exception {
        if (idProject < 1) {
            throw new Exception(
                                "[SQLCampaign->getAllCampaigns] entry data are not valid");
        }
        Vector result = new Vector();
        // TODO set conceptor
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectProjectCampaigns"); // ok
            prep.setInt(1, idProject);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                CampaignWrapper pCampaign = new CampaignWrapper();
                pCampaign.setName(stmtRes.getString("nom_camp"));
                pCampaign.setIdBDD(stmtRes.getInt("id_camp"));
                pCampaign.setIdProject(idProject);
                pCampaign.setConceptor(SQLObjectFactory
                                       .getInstanceOfISQLPersonne().getTwoName(
                                                                               stmtRes.getInt("PERSONNE_id_personne")));
                pCampaign.setDescription(stmtRes.getString("description_camp"));
                pCampaign.setDate(stmtRes.getDate("date_creation_camp"));
                result.addElement(pCampaign);
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        CampaignWrapper[] cwArray = new CampaignWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            cwArray[i] = (CampaignWrapper) result.get(i);
        }
        return cwArray;
    }

    /**
     * Return a CampaignWrapper representing a campaign at order 'order' in the
     * project identified by idProject
     * 
     * @param idProject
     * @param order
     * @return
     * @throws Exception
     */
    @Override
    public CampaignWrapper getCampaignByOrder(int idProject, int order)
        throws Exception {
        if (idProject < 1 || order < 0) {
            throw new Exception(
                                "[SQLCampaign->CampaignWrapper] entry data are not valid");
        }
        CampaignWrapper pCampaign = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignByOrder"); // ok
            prep.setInt(1, idProject);
            prep.setInt(2, order);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                pCampaign = new CampaignWrapper();
                pCampaign.setName(stmtRes.getString("nom_camp"));
                pCampaign.setDescription(stmtRes.getString("description_camp"));
                pCampaign.setIdBDD(stmtRes.getInt("id_camp"));
                pCampaign.setOrder(stmtRes.getInt("ordre_camp"));
                pCampaign.setIdProject(idProject);
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pCampaign;
    }

    /**
     * Return a CampaignWrapper for the campaign identified by idCamp
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public CampaignWrapper getCampaign(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getCampaign] entry data are not valid");
        }
        CampaignWrapper pCampaign = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);
            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignUsingID"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            if (stmtRes.next()) {
                pCampaign = new CampaignWrapper();
                pCampaign.setName(stmtRes.getString("nom_camp"));
                pCampaign.setDescription(stmtRes.getString("description_camp"));
                pCampaign.setIdBDD(stmtRes.getInt("id_camp"));
                pCampaign.setOrder(stmtRes.getInt("ordre_camp"));
                pCampaign.setIdProject(stmtRes
                                       .getInt("PROJET_VOICE_TESTING_id_projet"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pCampaign;
    }

    TestCampWrapper getInCampaignLastTestInSuite(int idCamp, int idSuite)
        throws Exception {
        if (idCamp < 1 || idSuite < 1) {
            throw new Exception(
                                "[SQLCampaign->getInCampaignLastTestInSuite] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectSuiteTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idSuite);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            // stmtRes.last();
            while (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    TestCampWrapper getInCampaignFirstTestInSuite(int idCamp, int idSuite)
        throws Exception {
        if (idCamp < 1 || idSuite < 1) {
            throw new Exception(
                                "[SQLCampaign->getInCampaignFirstTestInSuite] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectSuiteTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idSuite);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    TestCampWrapper getInCampaignLastTestInFamily(int idCamp, int idFamily)
        throws Exception {
        if (idCamp < 1 || idFamily < 1) {
            throw new Exception(
                                "[SQLCampaign->getInCampaignLastTestInFamily] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectFamilyTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idFamily);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            // stmtRes.last();
            while (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }

            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    TestCampWrapper getInCampaignFirstTestInFamily(int idCamp, int idFamily)
        throws Exception {
        if (idCamp < 1 || idFamily < 1) {
            throw new Exception(
                                "[SQLCampaign->getInCampaignFirstTestInFamily] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectFamilyTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idFamily);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    /**
     * Get a TestCampWrapper representing the test at defined order in the
     * campagne idCamp
     * 
     * @param idCamp
     * @param order
     * @return
     * @throws Exception
     */
    @Override
    public TestCampWrapper getTestCampByOrder(int idCamp, int order)
        throws Exception {
        if (idCamp < 1 || order < 0) {
            throw new Exception(
                                "[SQLCampaign->getTestCampByOrder] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectTestCampByOrder"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, order);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    /**
     * Get a TestCampWrapper representing the test idTest in the campagne idCamp
     * 
     * @param idCamp
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public TestCampWrapper getTestCampById(int idCamp, int idTest)
        throws Exception {
        if (idCamp < 1 || idTest < 1) {
            throw new Exception(
                                "[SQLCampaign->getTestCampById] entry data are not valid");
        }
        TestCampWrapper pTestCampWrapper = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectTestCampUsingID"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idTest);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pTestCampWrapper;
    }

    /**
     * Get an ordered Vector of TestCampWrapper representing test available in
     * the campaign idCamp
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public TestCampWrapper[] getTestsByOrder(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getTestsByOrder] entry data are not valid");
        }
        Vector result = new Vector();
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignTestsOrdered"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                TestCampWrapper pTestCampWrapper = new TestCampWrapper();
                pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
                pTestCampWrapper.setIdCamp(stmtRes
                                           .getInt("CAMPAGNE_TEST_id_camp"));
                pTestCampWrapper.setOrder(stmtRes.getInt("ordre_cas_camp"));
                pTestCampWrapper.setIdUser(stmtRes.getInt("assigned_user_id"));
                result.add(pTestCampWrapper);
            }

            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        TestCampWrapper[] tcwArray = new TestCampWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            tcwArray[i] = (TestCampWrapper) result.get(i);
        }
        return tcwArray;
    }

    /**
     * Get the number of tests in the family idFamily in the campaign idCamp
     * 
     * @param idCamp
     * @param idFamily
     * @return
     * @throws Exception
     */
    @Override
    public int getSizeOfFamilyInCampaign(int idCamp, int idFamily)
        throws Exception {
        if (idCamp < 1 || idFamily < 1) {
            throw new Exception(
                                "[SQLCampaign->getSizeOfFamilyInCampaign] entry data are not valid");
        }
        int size = 0;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectFamilyTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idFamily);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                size++;
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return size;
    }

    /**
     * Get the number of tests in the suite idSuite in the campaign idCamp
     * 
     * @param idCamp
     * @param idSuite
     * @return
     * @throws Exception
     */
    @Override
    public int getSizeOfSuiteInCampaign(int idCamp, int idSuite)
        throws Exception {
        if (idCamp < 1 || idSuite < 1) {
            throw new Exception(
                                "[SQLCampaign->getSizeOfSuiteInCampaign] entry data are not valid");
        }
        int size = 0;

        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectSuiteTestsInCamp"); // ok
            prep.setInt(1, idCamp);
            prep.setInt(2, idSuite);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            while (stmtRes.next()) {
                size++;
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return size;
    }

    /**
     * Get the id of the campaign identified by name in the project identified
     * by idProject
     * 
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idProject, String name) throws Exception {
        if (idProject < 1) {
            throw new Exception("[SQLCampaign->getID] entry data are not valid");
        }
        int idCamp = -1;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampId"); // ok
            prep.setString(1, name);
            prep.setInt(2, idProject);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()) {
                idCamp = stmtRes.getInt("id_camp");
            }
            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return idCamp;
    }

    /**
     * Get a vector of DataSetWrapper used by the campaign idCamp
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public DataSetWrapper[] getDatsets(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getDatsets] entry data are not valid");
        }
        Vector result = new Vector();
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampJeuxDonnees"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()) {
                DataSetWrapper pDataSet = new DataSetWrapper();
                pDataSet.setDescription(stmtRes.getString("desc_jeu_donnees"));
                pDataSet.setName(stmtRes.getString("nom_jeu_donnees"));
                pDataSet.setIdBDD(stmtRes.getInt("id_jeu_donnees"));
                result.addElement(pDataSet);
            }

            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        DataSetWrapper[] dswArray = new DataSetWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            dswArray[i] = (DataSetWrapper) result.get(i);
        }
        return dswArray;
    }

    /**
     * Get a vector of ExecutionWrapper used by the campaign idCamp
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionWrapper[] getExecutions(int idCamp) throws Exception {
        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getExecutions] entry data are not valid");
        }
        Vector result = new Vector();
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep = SQLEngine
                .getSQLSelectQuery("selectCampaignExecutions"); // ok
            prep.setInt(1, idCamp);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
            while (stmtRes.next()) {
                ExecutionWrapper exec = new ExecutionWrapper();
                exec.setName(stmtRes.getString("nom_exec_camp"));
                exec.setLastDate(stmtRes.getDate("last_exec_date"));
                exec.setCreationDate(stmtRes.getDate("date_exec_camp"));
                exec.setIdBDD(stmtRes.getInt("id_exec_camp"));
                exec.setDescription(stmtRes.getString("desc_exec_camp"));
                exec.setCampId(idCamp);
                result.addElement(exec);
            }

            SQLEngine.commitTrans(transNuber);
        } catch (Exception e) {
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        ExecutionWrapper[] ewArray = new ExecutionWrapper[result.size()];
        for (int i = 0; i < result.size(); i++) {
            ewArray[i] = (ExecutionWrapper) result.get(i);
        }
        return ewArray;
    }

    /**
     * Get an Array of ExecutionAttachmentWrapper representing all attachement
     * of all execution result
     * 
     * @param idCamp
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionAttachmentWrapper[] getResExecutionsAttachment(int idCamp)
        throws Exception {

        if (idCamp < 1) {
            throw new Exception(
                                "[SQLCampaign->getExecutions] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep = SQLEngine
            .getSQLSelectQuery("selectResExecAttachFromCamp"); // ok
        prep.setInt(1, idCamp);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()) {
            ExecutionWrapper exec = new ExecutionWrapper();
            exec.setName(stmtRes.getString("nom_exec_camp"));
            exec.setIdBDD(stmtRes.getInt("id_exec_camp"));
            exec.setCampId(idCamp);
            TestCampWrapper pTestCampWrapper = new TestCampWrapper();
            pTestCampWrapper.setIdBDD(stmtRes.getInt("CAS_TEST_id_cas"));
            pTestCampWrapper.setIdCamp(idCamp);
            FileAttachementWrapper fileAttach = null;
            UrlAttachementWrapper pUrlAttachment = null;
            // 20100112 - D\ufffdbut modification Forge ORTF v1.0.0
            if (stmtRes.getString("nom_attach") != null) {
                // 20100112 - Fin modification Forge ORTF v1.0.0
                fileAttach = new FileAttachementWrapper();
                fileAttach.setName(stmtRes.getString("nom_attach"));
                fileAttach.setLocalisation("");
                fileAttach.setDate(stmtRes.getDate("date_attachement"));
                fileAttach.setSize(new Long(stmtRes
                                            .getLong("taille_attachement")));
                fileAttach.setDescription(stmtRes
                                          .getString("description_attach"));
                fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            } else {
                pUrlAttachment = new UrlAttachementWrapper();
                String url = stmtRes.getString("url_attach");
                // pUrlAttachment.setUrl(url);
                pUrlAttachment.setName(url);
                pUrlAttachment.setDescription(stmtRes
                                              .getString("description_attach"));
                ;
                pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            }
            ExecutionAttachmentWrapper executionAttach = new ExecutionAttachmentWrapper();
            executionAttach.setExecution(exec);
            executionAttach.setTestCamp(pTestCampWrapper);
            executionAttach.setFileAttachment(fileAttach);
            executionAttach.setUrlAttachment(pUrlAttachment);

            result.add(executionAttach);
        }
        ExecutionAttachmentWrapper[] eawArray = new ExecutionAttachmentWrapper[result
                                                                               .size()];
        for (int i = 0; i < result.size(); i++) {
            eawArray[i] = (ExecutionAttachmentWrapper) result.get(i);
        }

        return eawArray;
    }
}
