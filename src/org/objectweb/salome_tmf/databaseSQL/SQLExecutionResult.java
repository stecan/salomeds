/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.databaseSQL;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataUpToDateException;
import org.objectweb.salome_tmf.api.data.ExecutionResultTestWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionActionWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionResult;

public class SQLExecutionResult implements ISQLExecutionResult {

    /**
     * Insert an Execution Result in the database (table RES_EXEC_CAMP)
     * @param idExec : id of the related execution
     * @param name of the Execution Result
     * @param description of the Execution Result
     * @param status (FAIT, A_FAIRE)
     * @param result (INCOMPLETE, STOPPEE, TERMINEE)
     * @param idUser
     * @return the id of the Execution Result in the table RES_EXEC_CAMP
     * @throws Exception canExecutCamp
     * need permission
     */
    @Override
    public int insert(int idExec, String name,  String description, String status, String result, int idUser) throws Exception {
        int idExecRes = -1;
        int transNumber = -1;
        if (idExec <1) {
            throw new Exception("[SQLExecutionResult->insert] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionResult : insert -> canExecutCamp]");
            }
        }
        if (SQLObjectFactory.getInstanceOfISQLExecution().getWrapper(idExec) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_EXECUTION_RESULT);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addResExecCamp"); //ok
            prep.setInt(1, idUser);
            prep.setInt(2, idExec);
            prep.setString(3, name);
            prep.setString(4, description);
            prep.setDate(5, Util.getCurrentDate());
            prep.setTime(6, Util.getCurrentTime());
            prep.setString(7, status);
            prep.setString(8, result);
            SQLEngine.runAddQuery(prep);

            idExecRes = getID(idExec, name);
            if (idExecRes <1) {
                throw new Exception("[SQLExecutionResult->insert] id are not valid");
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->insert]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idExecRes;
    }

    /**
     * Attach an Url to the  Execution Result : idExecRes (table ENV_ATTACHEMENT)
     * @param idExecRes
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    @Override
    public int addAttachUrl(int idExecRes, String url, String description) throws Exception {
        if (idExecRes <1 || url == null) {
            throw new Exception("[SQLExecutionResult->addAttachUrl] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;

        if (getWrapper(idExecRes) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLUrlAttachment().insert(url, description);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addUrlAttachToExecResult"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idExecRes);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->addAttachUrl]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }

        return idAttach;

    }

    /**
     * Attach a file to the  Execution Result : idExecRes (table ENV_ATTACHEMENT)
     * @param idExecRes
     * @param file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    @Override
    public int addAttachFile(int idExecRes, SalomeFileWrapper file, String description) throws Exception {
        if (idExecRes <1 || file == null) {
            throw new Exception("[SQLExecutionResult->addAttachFile] entry data are not valid");
        }
        int transNumber = -1;
        int idAttach = -1;
        if (getWrapper(idExecRes) == null){
            throw new DataUpToDateException();
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.INSERT_ATTACHMENT);
            idAttach = SQLObjectFactory.getInstanceOfISQLFileAttachment().insert(file,description);

            PreparedStatement prep = SQLEngine.getSQLAddQuery("addFileAttachToExecResult"); //ok
            prep.setInt(1,idAttach);
            prep.setInt(2,idExecRes);
            SQLEngine.runAddQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->addAttachFile]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
        return idAttach;
    }


    /**
     * Update the data of the  Execution Result identified by idExecRes
     * @param idResExec
     * @param description
     * @param status (FAIT, A_FAIRE)
     * @param result (INCOMPLETE, STOPPEE, TERMINEE)
     * @param idUser
     * @throws Exception
     * need permission canExecutCamp
     */
    @Override
    public void update(int idResExec, String description, String status, String result,int idUser) throws Exception{
        int transNumber = -1;
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->update] entry data are not valid");
        }
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionResult : insert -> canExecutCamp]");
            }
        }
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION_RESULT);

            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateExecResultStatusUsingID"); //ok
            prep.setInt(1,idUser);
            prep.setString(2, description);
            prep.setString(3, status);
            prep.setString(4, result);
            prep.setInt(5,idResExec);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->update]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * replace all reference of user oldIdUser by newIdUser in the table (RES_EXEC_CAMP) where execution is idExec
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    @Override
    public void updateUserRef(int idExec, int oldIdUser, int newIdUser)  throws Exception {
        if (idExec <1 || oldIdUser < 1 ||newIdUser <1 ) {
            throw new Exception("[SQLExecutionResult->updateUserRef] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.UPDATE_EXECUTION_RESULT);

            PreparedStatement prep = SQLEngine.getSQLUpdateQuery("updateResExecUser"); //ok
            prep.setInt(1, newIdUser);
            prep.setInt(2, oldIdUser);
            prep.setInt(3, idExec);
            SQLEngine.runUpdateQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->updateUserRef]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete the  Execution Result identified by idExecRes in table RES_EXEC_CAMP
     * Then delete all attachements, and related Test and Action execution result
     * @param idResExec
     * @throws Exception
     * @see ISQLExecutionTestResult.deleteAllFrom(int)
     * need permission canExecutCamp
     */
    @Override
    public void delete(int idResExec)  throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->delete] entry data are not valid");
        }
        int transNumber = -1;
        if (!SQLEngine.specialAllow) {
            if (!(Permission.canExecutCamp())){
                throw new SecurityException("[SQLExecutionResult : delete -> canExecutCamp]");
            }
        }

        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_EXECUTION_RESULT);

            //Delete All ExecutionTestResult
            SQLObjectFactory.getInstanceOfISQLExecutionTestResult().deleteAllFrom(idResExec);

            //Delete Attachs
            deleteAllAttach(idResExec);

            //Delete ref in table RES_EXEC_CAMP
            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteResExecCamp"); //ok
            prep.setInt(1,idResExec);
            SQLEngine.runDeleteQuery(prep);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->delete]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete all attchements of the  Execution Result identified by idExecRes
     * @param idResExec
     * @throws Exception
     * no permission needed
     */
    @Override
    public void deleteAllAttach(int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->deleteAllAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);

            AttachementWrapper[] attachList = getAttachs(idResExec);
            for (int i =0; i < attachList.length; i++){
                AttachementWrapper  pAttachementWrapper = attachList[i];
                deleteAttach(idResExec, pAttachementWrapper.getIdBDD());
            }

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->deleteAllAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Delete an attchement idAttach of the  Execution Result identified by idExecRes
     * @param idResExec
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    @Override
    public void deleteAttach(int idResExec, int  idAttach) throws Exception {
        if (idResExec <1 || idAttach < 1) {
            throw new Exception("[SQLExecutionResult->deleteAttach] entry data are not valid");
        }
        int transNumber = -1;
        try {
            transNumber = SQLEngine.beginTransaction(10, ApiConstants.DELETE_ATTACHMENT);

            PreparedStatement prep = SQLEngine.getSQLDeleteQuery("deleteAttachFromExecResult"); //ok
            prep.setInt(1, idAttach);
            prep.setInt(2, idResExec);
            SQLEngine.runDeleteQuery(prep);

            SQLObjectFactory.getInstanceOfISQLAttachment().delete(idAttach);

            SQLEngine.commitTrans(transNumber);
        } catch (Exception e ){
            Util.err("[SQLExecutionResult->deleteAttach]", e);
            SQLEngine.rollBackTrans(transNumber);
            throw e;
        }
    }

    /**
     * Get a vector of FileAttachementWrapper representing the files attachment of the
     * Execution Result identified by idExecRes
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public FileAttachementWrapper[] getAttachFiles(int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getAttachFiles] entry data are not valid");
        }
        Vector result = new Vector();

        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecResultAttachFiles"); //ok
        prep.setInt(1,idResExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while  (stmtRes.next()) {
            FileAttachementWrapper fileAttach = new FileAttachementWrapper();
            fileAttach.setName(stmtRes.getString("nom_attach"));
            fileAttach.setLocalisation("");
            fileAttach.setDate(stmtRes.getDate("date_attachement"));
            fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
            fileAttach.setDescription(stmtRes.getString("description_attach"));
            fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(fileAttach);
        }
        FileAttachementWrapper[] fawArray = new FileAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            fawArray[i] = (FileAttachementWrapper) result.get(i);
        }

        return fawArray;
    }

    /**
     * Get a vector of UrlAttachementWrapper representing the Urls attachment of the
     * Execution Result identified by idExecRes
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public UrlAttachementWrapper[] getAttachUrls(int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getAttachUrls] entry data are not valid");
        }
        Vector result = new Vector();

        PreparedStatement prep = SQLEngine.getSQLSelectQuery("selectExecResultAttachUrls"); //ok
        prep.setInt(1,idResExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while  (stmtRes.next()) {
            UrlAttachementWrapper pUrlAttachment = new UrlAttachementWrapper();
            String url = stmtRes.getString("url_attach");
            //                  pUrlAttachment.setUrl(url);
            pUrlAttachment.setName(url);
            pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
            pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            result.addElement(pUrlAttachment);
        }
        UrlAttachementWrapper[] uawArray = new UrlAttachementWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            uawArray[i] = (UrlAttachementWrapper) result.get(i);
        }
        return uawArray;
    }

    /**
     * Get a vector of all attachments (AttachementWrapper, File or Url) of the
     * Execution Result identified by idExecRes
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public AttachementWrapper[] getAttachs(int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getAttachs] entry data are not valid");
        }

        FileAttachementWrapper[] fileList =  getAttachFiles(idResExec);
        UrlAttachementWrapper[] urlList = getAttachUrls(idResExec);

        AttachementWrapper[] result = new AttachementWrapper[fileList.length + urlList.length];

        for(int i = 0; i < fileList.length; i++) {
            result[i] = fileList[i];
        }
        for(int i = 0; i < urlList.length; i++) {
            result[fileList.length + i] = urlList[i];
        }

        return result;
    }

    /**
     * Get the id of the Execution Result identified by name in the Excecution idExec
     * @param idExec
     * @param name
     * @return
     * @throws Exception
     */
    @Override
    public int getID(int idExec, String name) throws Exception {
        if (idExec <1 ) {
            throw new Exception("[SQLExecutionResult->getID] entry data are not valid");
        }



        int idResExec = -1;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecCampId"); //ok
        prep.setInt(1,idExec);
        prep.setString(2,name);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        if (stmtRes.next()){
            idResExec = stmtRes.getInt("id_res_exec_camp");
        }
        return idResExec;
    }

    int getNumberOfResult(int idResExec, String type) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getNumberOfResult] entry data are not valid");
        }
        int nbRes = 0;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecCampTestResults"); //ok

        prep.setInt(1, idResExec);
        prep.setString(2, type);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

        while (stmtRes.next()){
            nbRes ++;
        }
        return nbRes;
    }

    /**
     * @param   idResExec
     * @return  The number of test FAIL (ApiConstants.FAIL, and FAILED 
     *          in the database) in the execution result
     */
    @Override
    public int getNumberOfFail(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.FAIL);
    }

    /**
     * @param   idResExec
     * @return  The number of test SUCCESS (ApiConstants.SUCCESS, and PASSED 
     *          in the database) in the execution result
     */
    @Override
    public int getNumberOfPass(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.SUCCESS);
    }

    /**
     * @param   idResExec
     * @return  The number of test INCONCLUSIF (ApiConstants.UNKNOWN, and 
     *          INCONCLUSIF in the database) in the execution result
     */
    @Override
    public int getNumberOfInc(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.UNKNOWN);
    }

    /**
     * @param   idResExec
     * @return  The number of test NOTAPPLICABLE (ApiConstants.NOTAPPLICABLE, 
     *          and NOTAPPLICABLE in the database) in the execution result
     */
    @Override
    public int getNumberOfNotApplicable(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.NOTAPPLICABLE);
    }
    
    /**
     * @param   idResExec
     * @return  The number of test BLOCKED (ApiConstants.BLOCKED, 
     *          and BLOCKED in the database) in the execution result
     */
    @Override
    public int getNumberOfBlocked(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.NOTAPPLICABLE);
    }
    
    /**
     * @param   idResExec
     * @return  The number of test NONE (ApiConstants.NONE, 
     *          and NONE in the database) in the execution result
     */
    @Override
    public int getNumberOfNone(int idResExec) throws Exception {
        return getNumberOfResult(idResExec, ApiConstants.NOTAPPLICABLE);
    }
    
    /**
     * Get an ExecutionResultWrapper representing the ExecutionResult 
     * identified by idResExec
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionResultWrapper getWrapper (int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getWrapper] entry data are not valid");
        }
        ExecutionResultWrapper pExecResWrap = null;
        int transNuber = -1;
        try {
            transNuber = SQLEngine.beginTransaction(10, ApiConstants.LOADING);

            PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectExecutionResultsByID"); //ok
            prep.setInt(1, idResExec);
            ResultSet stmtRes = SQLEngine.runSelectQuery(prep);

            if (stmtRes.next()){
                pExecResWrap = new ExecutionResultWrapper();
                pExecResWrap.setName(stmtRes.getString("nom_res_exec_camp"));
                pExecResWrap.setExecutionDate(stmtRes.getDate("date_res_exec_camp"));
                pExecResWrap.setTime(stmtRes.getTime("heure_res_exec_camp").getTime());
                int  userId = stmtRes.getInt("PERSONNE_id_personne");
                pExecResWrap.setTester(SQLObjectFactory.getInstanceOfISQLPersonne().getLogin(userId));
                pExecResWrap.setExecutionStatus(stmtRes.getString("resultat_res_exec_camp"));
                pExecResWrap.setIdBDD(idResExec);
                pExecResWrap.setNumberOfFail(getNumberOfFail(idResExec));
                pExecResWrap.setNumberOfSuccess(getNumberOfPass(idResExec));
                pExecResWrap.setNumberOfUnknow(getNumberOfInc(idResExec));
            }

            SQLEngine.commitTrans(transNuber);
        } catch (Exception e){
            SQLEngine.rollBackTrans(transNuber);
            throw e;
        }
        return pExecResWrap;
    }

    /**
     * Get an Array of ExcutionResultTestWrapper included in the ExecutionResult identifed by idResExec
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public ExecutionResultTestWrapper[] getExecutionResultTestWrapper(int idResExec) throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getExecutionResultTestWrapper] entry data are not valid");
        }
        Vector res = new Vector();
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectExecutionResultsTestByID"); //OK
        prep.setInt(1, idResExec);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            ExecutionResultTestWrapper pExcutionResultTestWrapper = new ExecutionResultTestWrapper();
            int idTest = stmtRes.getInt("CAS_TEST_id_cas");
            TestWrapper pTest = SQLObjectFactory.getInstanceOfISQLTest().getTest(idTest);
            pExcutionResultTestWrapper.setExcutedTest(pTest);
            int idResExecTest = stmtRes.getInt("id_exec_cas");
            pExcutionResultTestWrapper.setIdBDD(idResExecTest);
            pExcutionResultTestWrapper.setOrder(stmtRes.getInt("ordre_exec_cas"));
            pExcutionResultTestWrapper.setIdResExec(stmtRes.getInt("RES_EXEC_CAMP_id_res_exec_camp"));
            pExcutionResultTestWrapper.setStatus(stmtRes.getString("res_exec_cas"));
            if (pTest.getType().equals(ApiConstants.MANUAL)){
                Vector listActionResult =  new Vector();
                PreparedStatement prep2  = SQLEngine.getSQLSelectQuery("selectActionResultForExecTestID"); //ok
                prep2.setInt(1, idResExecTest);
                ResultSet stmtRes2 = SQLEngine.runSelectQuery(prep2);
                while (stmtRes2.next()){
                    ExecutionActionWrapper pExecutionActionWrapper = new ExecutionActionWrapper();
                    pExecutionActionWrapper.setAwaitedResult(stmtRes2.getString("ACTION_TEST_res_attendu_action"));
                    pExecutionActionWrapper.setDescription(stmtRes2.getString("ACTION_TEST_description_action"));
                    pExecutionActionWrapper.setEffectivResult(stmtRes2.getString("effectiv_res_action"));
                    pExecutionActionWrapper.setIdAction(stmtRes2.getInt("ACTION_TEST_id_action"));
                    pExecutionActionWrapper.setIdBDD(stmtRes2.getInt("id_exec_action"));
                    pExecutionActionWrapper.setIdExecTest(stmtRes2.getInt("EXEC_CAS_id_exec_cas"));
                    pExecutionActionWrapper.setResult(stmtRes2.getString("res_exec_action"));
                    listActionResult.add(pExecutionActionWrapper);
                }
                ExecutionActionWrapper[] larArray = new ExecutionActionWrapper[listActionResult.size()];
                for(int i = 0; i < listActionResult.size(); i++) {
                    larArray[i] = (ExecutionActionWrapper) listActionResult.get(i);
                }
                pExcutionResultTestWrapper.setActionResult(larArray);
            }
            res.add(pExcutionResultTestWrapper);
        }
        ExecutionResultTestWrapper[] ertwArray = new ExecutionResultTestWrapper[res.size()];
        for(int i = 0; i < res.size(); i++) {
            ertwArray[i] = (ExecutionResultTestWrapper) res.get(i);
        }
        return ertwArray;
    }

    /**
     * Get the status of the execution of the tests idTest in the execution result idResExec
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    @Override
    public String getTestResultInExecution(int idResExec, int idTest) throws Exception {
        if (idResExec <1 || idTest < 1 ) {
            throw new Exception("[SQLExecutionResult->getTestResultInExecution] entry data are not valid");
        }
        String result = null;
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectResExecTest"); //ok
        prep.setInt(1, idResExec);
        prep.setInt(2, idTest);
        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        if (stmtRes.next()){
            result = stmtRes.getString("res_exec_cas");
        }
        return result;
    }

    /**
     * Get an Array of TestAttachmentWrapper
     * @param idResExec
     * @return
     * @throws Exception
     */
    @Override
    public TestAttachmentWrapper[] getAllAttachTestInExecutionResult(int idResExec)throws Exception {
        if (idResExec <1 ) {
            throw new Exception("[SQLExecutionResult->getAllAttachTestInExecutionResult] entry data are not valid");
        }
        Vector result = new Vector();
        PreparedStatement prep  = SQLEngine.getSQLSelectQuery("selectAllResExecTestAttach"); //ok
        prep.setInt(1, idResExec);

        ResultSet stmtRes = SQLEngine.runSelectQuery(prep);
        while (stmtRes.next()){
            int idTest = stmtRes.getInt("CAS_TEST_id_cas");
            FileAttachementWrapper fileAttach = null;
            UrlAttachementWrapper pUrlAttachment = null;
            //20100112 - D\ufffdbut modification Forge ORTF v1.0.0
            if (stmtRes.getString("nom_attach") != null){
                //20100112 - Fin modification Forge ORTF v1.0.0
                fileAttach = new FileAttachementWrapper();
                fileAttach.setName(stmtRes.getString("nom_attach"));
                fileAttach.setLocalisation("");
                fileAttach.setDate(stmtRes.getDate("date_attachement"));
                fileAttach.setSize(new Long(stmtRes.getLong("taille_attachement")));
                fileAttach.setDescription(stmtRes.getString("description_attach"));
                fileAttach.setIdBDD(stmtRes.getInt("id_attach"));
            } else {
                pUrlAttachment = new UrlAttachementWrapper();
                String url = stmtRes.getString("url_attach");
                //                              pUrlAttachment.setUrl(url);
                pUrlAttachment.setName(url);
                pUrlAttachment.setDescription(stmtRes.getString("description_attach"));;
                pUrlAttachment.setIdBDD(stmtRes.getInt("id_attach"));
            }
            TestAttachmentWrapper pTestAttachmentWrapper = new TestAttachmentWrapper();
            pTestAttachmentWrapper.setIdTest(idTest);
            pTestAttachmentWrapper.setFileAttachment(fileAttach);
            pTestAttachmentWrapper.setUrlAttachment(pUrlAttachment);

            result.add(pTestAttachmentWrapper);
        }
        TestAttachmentWrapper[] tawArray = new TestAttachmentWrapper[result.size()];
        for(int i = 0; i < result.size(); i++) {
            tawArray[i] = (TestAttachmentWrapper) result.get(i);
        }

        return tawArray;
    }
}
