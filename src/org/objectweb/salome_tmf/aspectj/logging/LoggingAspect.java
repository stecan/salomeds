package org.objectweb.salome_tmf.aspectj.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LoggingAspect extends AbstractLoggingAspect {

    @Override
    @Pointcut
    protected void logging() {
        // TODO Auto-generated method stub

    }

    //  @Override
    //  @Pointcut("("
    //                  +
    // IHM
    //                  "(" + "call(* org.objectweb.salome_tmf.ihm..*(..)) "
    //                  + "&& !call(* org.objectweb.salome_tmf.ihm.filtre..*(..)) "
    //                  + "&& !call(* org.objectweb.salome_tmf.ihm.languages..*(..)) "
    //                  + "&& !call(* org.objectweb.salome_tmf.ihm.models..*(..)) "
    //                  + "&& !call(* org.objectweb.salome_tmf.ihm.tools..*(..)) )"
    //                  +
    // Function useless
    //                  "&& !call(* *..*.set*(..)) " + "&& !call(* *..*.get*(..)) "
    //                  + "&& !call(* *..*.is*(..)) " + ")")
    //  protected void logging() {
    //
    //  }


    //  @Around("logging()")
    //  public Object Log(final ProceedingJoinPoint thisJoinPoint) throws Throwable {
    //
    //          Logger log;
    //          String className;
    //          String methodName;
    //
    //          try {
    //                  className = thisJoinPoint.getTarget().getClass().getName();
    //                  log = Logger.getLogger(className);
    //
    //                  methodName = thisJoinPoint.getSignature().getName();
    //          } catch (java.lang.NullPointerException nullException) {
    //                  className = thisJoinPoint.getStaticPart().getSignature()
    //                                  .getDeclaringTypeName();
    //                  log = Logger.getLogger(className);
    //                  methodName = thisJoinPoint.getStaticPart().toShortString();
    //          }
    //          try {
    //                  Object object1 = thisJoinPoint.getTarget();
    //
    //                  if (log.isDebugEnabled()) {
    //                          log.debug(methodName + "() - Entering");
    //                  }
    //                  Object object = thisJoinPoint.proceed();
    //                  return object;
    //          } finally {
    //                  if (log.isDebugEnabled()) {
    //                          log.debug(methodName + " - Leaving");
    //                  }
    //          }
    //
    //  }
    // @Around("logging()")
    // public Object LogObject(final ProceedingJoinPoint thisJoinPoint) throws
    // Throwable {
    // final String joinPointName = thisJoinPoint.getTarget().getClass()
    // .getName()
    // + "." + thisJoinPoint.getSignature().getName() + "()";
    // System.out.println("Entering [" + joinPointName + "]");
    // Object object = thisJoinPoint.proceed();
    // System.out.println("Leaving  [" + joinPointName + "]");
    // return object;
    // }
}
