/*
  2    * LoggingWithAspectJ - Logging with AspectJ
  3    * Copyright (C) 2007 Christian Schenk
  4    *
  5    * This program is free software; you can redistribute it and/or
  6    * modify it under the terms of the GNU General Public License
  7    * as published by the Free Software Foundation; either version 2
  8    * of the License, or (at your option) any later version.
  9    *
  10   * This program is distributed in the hope that it will be useful,
  11   * but WITHOUT ANY WARRANTY; without even the implied warranty of
  12   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  13   * GNU General Public License for more details.
  14   *
  15   * You should have received a copy of the GNU General Public License
  16   * along with this program; if not, write to the Free Software
  17   * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  18   */
package org.objectweb.salome_tmf.aspectj.logging;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * 27 * This indented logging aspect was inspired by <a 28 * href=
 * "http://www.bibsonomy.org/bibtex/2684fcd95b8bce37858bcc13753047a7e/cschenk"
 * >AspectJ 29 * in Action</a> (p. 171). 30 * 31 * @author Christian Schenk 32
 */
@Aspect
public abstract class AbstractLoggingAspect {

    /** Indent width */
    private final int width = 2;

    /** Current indent with counter */
    private int indent = 0;

    /**
     * This will be implemented by logging aspects.
     */
    @Pointcut
    protected abstract void logging();

    //  @Before("logging()")
    //  public void increaseIndent() {
    //          this.indent++;
    //  }
    //
    //  @After("logging()")
    //  public void decreaseIndent() {
    //          this.indent--;
    //  }

    //  @Before("call(* java.io.PrintStream.println(..))")
    //  public void print() {
    //          for (int spaces = 0, indent = this.indent * this.width; spaces < indent; spaces++) {
    //                  System.out.print(" ");
    //          }
    //  }
}
