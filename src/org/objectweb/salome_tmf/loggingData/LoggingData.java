/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.objectweb.salome_tmf.loggingData;

/**
 *
 * @author stefan
 */
public class LoggingData {
    
    private static String localUser;
    private static String localMachine;
    private static String login;
    private static String authentification;
    private static String data;
    private static String url;
    private static String project;
    private static String version;
    
    public static void setLocalUser(String name) {
        localUser = name;
        setData();
    }
    
    public static String getLocalUser () {
        return localUser;
    }
    
    public static void setLocalMachine(String name) {
        localMachine = name;
        setData();
    }
    
    public static String getLocalMachine () {
        return localMachine;
    }

    public static void setLogin(String name) {
        login = name;
        setData();
    }
    
    public static String getLogin () {
        return login;
    }
    
    public static void setAuthentification (String name) {
        authentification = name;
        setData();
    }
    
    public static String getAuthentification () {
        return authentification;
    }
    
    public static String getUrl () {
        return url;
    }

    public static void setUrl (String name) {
        url = name;
        setData();
    }
    
    public static String getProject () {
        return project;
    }
    
    public static void setProject (String name) {
        project = name;
        setData();
    }
    
    public static String getVersion () {
        return version;
    }

    public static void setVersion (String name) {
        version = name;
        setData();
    }

    public static String getData () {
        return data;
    }
 
    private static void setData() {
        data =  "URL = " + url + "\n" +
                "Version = " + version + "\n" +
                "User = " + localUser + "\n" + 
                "Machine = " + localMachine + "\n" + 
                "Login = " + login + "\n" + 
                "Authentification = " + authentification + "\n" + 
                "Project = " + project + "\n";
        
    }
    
}        
