package org.objectweb.salome_tmf.api;

import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.objectweb.salome_tmf.api.data.ConnectionWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLEngine;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;
import org.objectweb.salome_tmf.loggingData.LoggingData;
import org.objectweb.salome_tmf.tools.sql.DirectConnect;

public class Api {

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(Api.class);

    /* SQLENGINE */
    static ISQLObjectFactory pISQLObjectFactory = null;
    static ISQLEngine pISQLEngine = null;

    /* Config */
    static Config pConfig = new Config();

    static String cryptKey = "thesupersecretky";
    static boolean isLocked = false;
    static boolean isAlreadyConnected = false;
    static java.net.URL urlBase = null;
    static java.net.URL urlCodeBase = null;
    static String unixProjectName = null;

    /* Listener */
    static ChangeDispatcher changeDispatcher = null;
    // static PluginDispatcher pluginDispatcher = null;
    static Thread listenerThread = null;
    static ISafeThread pChangeListener = null;
    static int _pid = -1;

    static String mail_host = "";
    static String title;
    static String url;
    static String user;
    static String password;
    static String driver;
    static Object verrou = new Object();
    static int nbConnect = 0;
    // public final static String VERSION = "3.0";

    public final static String DEFAULT_LOGIN = "LoginSalome";
    public final static String DEFAULT_PASSWORD = "LoginSalome";
    static String strUsername = null;
    static String strMD5Password = null;

    static String validUserAuthentification = "";

    static String salomeHost = null;
    static String webdavLogin = null;
    static String webdavPasswd = null;
    static List<String> errorListConnections = new ArrayList<>();
    static List<String> errorListProperties = new ArrayList<>();

    public static String getStrUsername() {
        return strUsername;
    }

    public static String getStrMD5Password() {
        return strMD5Password;
    }

    public static void setStrUsername(String strUsername) {
        Api.strUsername = strUsername;
    }

    public static void setStrMD5Password(String strMD5Password) {
        Api.strMD5Password = strMD5Password;
    }

    public static String getUrl() {
        return url;
    }

    public static String getTitle() {
        return title;
    }

    public static String getUrlBase() {
        return urlBase.toString();
    }

    public static String getUser() {
        return user;
    }

    public static String getPassword() {
        return password;
    }

    public static String getDriver() {
        return driver;
    }

    public static String getUnixProjectName() {
        return unixProjectName;
    }

    static boolean isSOAP = false;

    public static boolean isSOAP() {
        return isSOAP;
    }

    public static String getCryptKey() {
        return cryptKey;
    }

    public static String getSalomeHost() {
        return salomeHost;
    }

    public static String getWebdavLogin() {
        return webdavLogin;
    }

    public static String getWebdavPasswd() {
        return webdavPasswd;
    }

    /**
     * Fonction qui etablit la connextion a la base de donnees SalomeTMF
     */
    public static boolean openConnection(java.net.URL _urlBase) {

        synchronized (verrou) {

            createTMP();
            if (isAlreadyConnected) {
                Util.log("[API : openConnection] Connection already exist");

                return isAlreadyConnected;
            }
            isAlreadyConnected = true;
            // nbConnect ++;
            try {
                String url_txt = _urlBase.toString();
                url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
                // boolean key = false;
                urlBase = new java.net.URL(url_txt);

                changeDispatcher = new ChangeDispatcher();
                // pluginDispatcher = new PluginDispatcher();
                _pid = (int) ((changeDispatcher.hashCode()) * (System.currentTimeMillis()));

                if (pISQLEngine == null) {
                    // Ouverture du fichier "properties" contenant les
                    // parametres de configuration pour la connexion a la BdD
                    Util.log("[Api] Applet base is  " + urlBase);
                    java.net.URL url_salome = new java.net.URL(urlBase
                            + ApiConstants.CONFIG_FILE_PATH + "/"
                            + ApiConstants.CONFIG_FILE_NAME);
                    LoggingData.setUrl(urlBase.toString());

                    java.net.URL url_log4j = new java.net.URL(urlBase
                            + ApiConstants.CONFIG_FILE_PATH + "/"
                            + ApiConstants.LOG4J_FILE_NAME);
                    
                    // Initialize log4j
                    Properties logProperties = null;
                    try
                    {
                        logProperties = Util.getPropertiesFile(url_log4j);
                        PropertyConfigurator.configure(logProperties);
                        Util.log("[Api] Properties from Url is : " + logProperties);
                    } catch (Exception e) {
                        Util.log("[Api] Couldn't read /cfg/log4j.properties");
                        // Do nothing! 
                        //
                        // Note: 
                        // If an error occures, then log4j issues the warning message
                        // "log4j: warn please initialize the log4j system properly" 
                        // to the java console!
                    }        
                    
                    Util.log("[Api] Try to find properties on " + url_salome);
                    Properties prop = null;
                    try {
                        prop = Util.getPropertiesFile(url_salome);
                        Util.log("[Api] Properties from Url is : " + prop);
                    } catch (Exception e) {
                        e.printStackTrace();

                        Util.log("[Api] WARNING JAR FILE PROPERTIES SELECTED : " + e);
                        prop = Util.getPropertiesFile(ApiConstants.CONFIG_FILE_PATH);
                        Util.log("[Api] Properties from jar is : " + prop);
                    }
                    String logLevel = "nolog";
                    try {
                        logLevel = prop.getProperty("Logging");
                        if (logLevel.equalsIgnoreCase("nolog")) {
                            pConfig.setLogLevel(Config.LOG_NOLOG_LEVEL);
                        } else if (logLevel.equalsIgnoreCase("info")) {
                            pConfig.setLogLevel(Config.LOG_INFO_LEVEL);
                        } else if (logLevel.equalsIgnoreCase("debug")) {
                            pConfig.setLogLevel(Config.LOG_DEBUG_LEVEL);
                        } else if (logLevel.equalsIgnoreCase("error")) {
                            pConfig.setLogLevel(Config.LOG_ERROR_LEVEL);
                        } else {
                            pConfig.setLogLevel(Config.LOG_NOLOG_LEVEL);
                        }
                    } catch (Exception e) {
                        logger.error("Error when getting the loglevel!", e);
                        logLevel = "nolog";
                        pConfig.setLogLevel(Config.LOG_NOLOG_LEVEL);
                        // Util.err(e);
                    }
                    Util.log("[Api] Logging value is  " + logLevel, true);

                    String user_authentification;
                    if (validUserAuthentification.length() > 0) {
                        user_authentification = validUserAuthentification;
                        pConfig.setUserAuthentification(user_authentification);
                    } else {
                        user_authentification = "Database";
                        try {
                            user_authentification = prop.getProperty("UserAuthentification");
                            if (user_authentification.equalsIgnoreCase("ldap")) {
                                pConfig.setUserAuthentification(user_authentification);
                            } else if (user_authentification.equalsIgnoreCase("both")) {
                                pConfig.setUserAuthentification(user_authentification);
                            } else if (user_authentification.equalsIgnoreCase("database")) {
                                pConfig.setUserAuthentification(user_authentification);
                            } else {
                                user_authentification = "Database";
                                pConfig.setUserAuthentification(user_authentification);
                            }
                        } catch (Exception e) {
                            logger.error("Error when getting the authentification method!", e);
                            user_authentification = "Database";
                            pConfig.setUserAuthentification(user_authentification);
                        }
                    }
                    Util.log("[Api] User Authentification value is  "
                             + user_authentification, true);

                    // pConfig.setDEBUG(true);
                    String allow_plugins = prop.getProperty("AllowPlugins");
                    if (allow_plugins.equals("true")) {
                        pConfig.setALLOW_PLUGINS(true);
                    } else {
                        pConfig.setALLOW_PLUGINS(false);
                    }

                    String ide_dev = prop.getProperty("IDE_Dev");
                    if (ide_dev.equals("true")) {
                        pConfig.setIDE_DEV(true);
                        Permission.setPermission(254);
                    } else {
                        pConfig.setIDE_DEV(false);
                    }
                    Util.log("[Api] IDE_DEV value is  " + ide_dev);

                    String net_change_track = prop.getProperty("NetChangeTrack");
                    if (net_change_track.equals("true")) {
                        pConfig.setNET_CHANGE_TRACK(true);
                    } else {
                        pConfig.setNET_CHANGE_TRACK(false);
                    }
                    Util.log("[Api] NetChangeTrack value is  " +
                             net_change_track);

                    try {
                        String with_ical = prop.getProperty("WithICAL");
                        if (with_ical.equals("true")) {
                            pConfig.setWITH_ICAL(true);
                        } else {
                            pConfig.setWITH_ICAL(false);
                        }
                        Util.log("[Api] WithICAL value is  " + with_ical);
                    } catch (Exception e) {
                        e.printStackTrace();

                        pConfig.setWITH_ICAL(false);
                    }

                    try {
                        String mail_server = prop.getProperty("MAIL_HOST");
                        mail_host = mail_server;
                        Util.log("[Api] MAIL_HOST value is  " + mail_server);
                    } catch (Exception e) {
                        e.printStackTrace();

                        mail_host = null;
                    }

                    try {
                        String lockOnExeC = prop.getProperty("LockOnTestExec");
                        if (lockOnExeC.equals("true")) {
                            pConfig.setLockOnExec(true);
                        } else {
                            pConfig.setLockOnExec(false);
                        }
                        Util.log("[Api] LockOnTestExec value is  " +
                                lockOnExeC);
                    } catch (Exception e) {
                        e.printStackTrace();

                        pConfig.setLockOnExec(true);
                    }

                    try {
                        int lock_meth = Integer.parseInt(prop.getProperty("DBLOCK"));
                        // 0 = NONE, 1 = mysql, 2 = salome
                        lock_meth--;
                        pConfig.setUSE_DB_LOCK(lock_meth);
                        Util.log("[Api] DBLOCK value is  " + lock_meth);
                    } catch (Exception e) {
                        e.printStackTrace();

                        pConfig.setUSE_DB_LOCK(1);
                    }

                    try {
                        String lockTestIfExecuted = prop
                            .getProperty("LockExecutedTest");
                        if (lockTestIfExecuted.equals("true")) {
                            pConfig.setLockExecutedTest(true);
                        } else {
                            pConfig.setLockExecutedTest(false);
                        }
                        Util.log("[Api] LockExecutedTest value is  "
                                 + lockTestIfExecuted);
                    } catch (Exception e) {
                        e.printStackTrace();

                        pConfig.setLockExecutedTest(true);
                    }

                    String bugVerification = prop.getProperty("BugVerification");
                    if ((bugVerification != null) && (bugVerification.equals("true"))) {
                        Config.setBugVerification(true);
                    } else {
                        Config.setBugVerification(false);
                    }

                    int i = 0;
                    String bugKey;
                    while (Config.getBugVerification()) {
                        i++;
                        bugKey = prop.getProperty("BugKey" + Integer.toString(i));
                        if (bugKey == null) {
                            break;
                        }
                        Config.addBugtrackingKey(bugKey);
                    }
                    
                    String polarionLinks = prop.getProperty("PolarionLinks");
                    if ((polarionLinks != null) && (polarionLinks.equals("true"))) {
                        Config.setPolarionLinks(true);
                    } else {
                        Config.setPolarionLinks(false);
                    }

                    i = 0;
                    String link;
                    String key;
                    while (Config.getPolarionLinks()) {
                        i++;
                        link = prop.getProperty("PolarionLink" + Integer.toString(i));
                        if (link == null) {
                            break;
                        }
                        Config.addPolarionHyperlink(link);
                    }

                    String lxrLinks = prop.getProperty("LxrLinks");
                    if ((lxrLinks != null) && (lxrLinks.equals("true"))) {
                        Config.setLxrLinks(true);
                    } else {
                        Config.setLxrLinks(false);
                    }

                    String LxrFirst = prop.getProperty("LxrFirst");
                    if (lxrLinks != null) {
                        Config.setLxrFirst(LxrFirst);
                    }

                    String LxrSecond = prop.getProperty("LxrSecond");
                    if (lxrLinks != null) {
                        Config.setLxrSecond(LxrSecond);
                    }

                    driver = prop.getProperty("DriverJDBC");
                    String engine = prop.getProperty("SQLEngine");

                    title = prop.getProperty("Title");
                    url = prop.getProperty("URL");
                    user = prop.getProperty("User");
                    password = prop.getProperty("Password");
                    if (!pConfig.isIDE_DEV()) {
                        Util.log("[Api] Decripte : " + password);
                        try {
                            MD5paswd.readkey(urlBase);
                            password = MD5paswd.decryptString(password);
                            Util.log("[Api] Password is crypted");
                        } catch (Exception e) {
                            logger.error("Password isn't crypted!", e);
                        }
                    }
                    Util.log("[Api] Open connexion on " + url + ", with pid : "
                             + _pid);

                    // Selection de la factory en fonction de la version (SQL ou
                    // SOAP)
                    /*
                      try {
                      pISQLObjectFactory = (ISQLObjectFactory) Class.forName(
                      engineSOAP).newInstance();
                      isSOAP = true;
                      Util.log("[Api] version : SOAP");
                      } catch (ClassNotFoundException e) {
                      logger.error(e);
                      pISQLObjectFactory = (ISQLObjectFactory) Class.forName(
                      engine).newInstance();
                      isSOAP = false;
                      Util.log("[Api] version : SQL");
                      }
                    */

                    String proj;
                    List<String> attachedProjects = new ArrayList<>();
                    List<String> attachedProjectsProperty = new ArrayList<>();
                    i = 0;
                    String propertyString;
                    while (true) {
                        if (i==0) {
                            propertyString = "AttachedProject";
                            proj = prop.getProperty(propertyString);
                        } else {
                            propertyString =  "AttachedProject" + Integer
                                    .toString(i);
                            proj = prop.getProperty(propertyString);
                        }
                        if (proj == null) {
                            if (i>=100) {
                                break;
                            } else {
                                // Do nothing: The first first 100 values 
                                // "AttachedProjectxxx" are read per default.
                            }
                        } else {
                            attachedProjects.add(proj);
                            attachedProjectsProperty.add(propertyString);
                        }
                        i++;
                    }

                    List<Connection> cons = new ArrayList<>();
                    Connection con;
                    int count = 0;
                    for (String str : attachedProjects) {
                        con = DirectConnect.DirectConnect(str);
                        if (con == null) {
                            errorListConnections.add(str);
                            errorListProperties.add(attachedProjectsProperty.get(count));
                        } else {
                            cons.add(con);
                        }
                        count++;
                    }
                    Config.setAttachedConnections(cons);

                    pISQLObjectFactory = (ISQLObjectFactory) Class.forName(engine).newInstance();
                    isSOAP = false;
                    Util.log("[Api] version : SQL");

                    Util.log("[Api] get ISQLObjectFactory = " +
                            pISQLObjectFactory);
                    pISQLEngine = pISQLObjectFactory.getInstanceOfSQLEngine(
                            url, user, password, changeDispatcher, _pid,
                            driver, pConfig.getUSE_DB_LOCK());
                    pConfig.init(pISQLObjectFactory);

                    try {
                        urlCodeBase = new URL(prop.getProperty("CodeBase"));
                    } catch (Exception e) {
                        // logger.error("No bootstrap found!", e);
                        urlCodeBase = _urlBase;
                    }

                } else {
                    pISQLEngine = pISQLObjectFactory.getInstanceOfSQLEngine(
                            url, user, password, changeDispatcher, _pid,
                            driver, pConfig.getUSE_DB_LOCK());
                    pConfig.init(pISQLObjectFactory);
                }
                nbConnect++;
            }

            catch (Exception E) {
                E.printStackTrace();
                Util.err(E);
                isAlreadyConnected = false;
            }
        }

        return isAlreadyConnected;
    }

    public static URL getCodeBase() {
        return urlCodeBase;
    }

    public static String getVersion() {
        return pISQLObjectFactory.getSalomeVersion();
    }

    public static boolean isConnected() {
        return isAlreadyConnected;
    }

    public static int getNbConnect() {
        synchronized (verrou) {
            return nbConnect;
        }
    }

    public static void closeConnection() {
        synchronized (verrou) {
            try {
                Util.log("[Api->closeConnection] Try to close connexion ");
                if (pChangeListener != null) {
                    pChangeListener.safe_stop();
                }
                Thread.sleep(2000);
                listenerThread = null;
                // pluginDispatcher = null;
                closeDB();
                isAlreadyConnected = false;
                nbConnect--;
            } catch (Exception e) {
                Util.err(e);
            }
        }
    }

    public static int beginTransaction(int lock_code, int type)
        throws Exception {
        if (pISQLEngine != null) {
            return pISQLEngine.beginTransDB(lock_code, type);
        }
        return -1;
    }

    public static void commitTrans(int code) throws Exception {
        if (pISQLEngine != null) {
            pISQLEngine.commitTransDB(code);
        }
    }

    public static void forceRollBackTrans(int code) {
        if (pISQLEngine != null) {
            try {
                pISQLEngine.rollForceBackTransDB(code);
            } catch (Exception e) {
                Util.err(e);
            }
        }
    }

    public static synchronized void lockTestPlan() throws Exception {
        if (isLocked == false) {
            if (pConfig.isLockOnExec()) {
                if (pISQLEngine != null) {
                    pISQLEngine.lockTPlan();
                }
            }
            isLocked = true;
        } else {
            throw new Exception("[Api->lockTestPlan] Database is already locked");
        }
    }

    public static synchronized void unLockTestPlan() throws Exception {
        if (isLocked == true) {
            if (pConfig.isLockOnExec()) {
                if (pISQLEngine != null) {
                    pISQLEngine.unLockTPlan();
                }
            }
            isLocked = false;
        } else {
            throw new Exception("[Api->lockTestPlan] Database is not locked");
        }
    }

    public static void closeDB() {
        if (!isSOAP()) {
            Util.log("[Api:closeDB] Try to close the database by SQLEngine");
            if (pISQLEngine != null) {
                try {
                    pISQLEngine.close();
                    Util.log("[Api:closeDB] Database is closed");
                } catch (Exception e) {
                    Util.log("[Api:closeDB] Error when close the database, database is not closed");
                }
            }
        }
        pISQLEngine = null;
        deleteTMP();
    }

    public static void setUrlBase(URL _urlBase) {
        try {
            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            urlBase = new java.net.URL(url_txt);
        } catch (Exception e) {
            Util.err(e);
        }
    }

    /* ISQLObjectFactory */

    public static void regiterSQLObjectFactory(ISQLObjectFactory pSQLObjectFactory) {
        pISQLObjectFactory = pSQLObjectFactory;
    }

    static public ISQLObjectFactory getISQLObjectFactory() {
        return pISQLObjectFactory;
    }

    static public void addWatchListener(java.util.Observer o, String projet) {
        changeDispatcher.addObserver(o);
        if (listenerThread == null) {
            if (pConfig.isNET_CHANGE_TRACK()) {
                pChangeListener = pISQLObjectFactory.getChangeListener(projet);
                listenerThread = new Thread(pChangeListener);
                listenerThread.start();
            } else {
                pISQLObjectFactory.changeListenerProject(projet);
            }
        }
    }

    static public void runListenerThread(String projet) {
        try {
            if (listenerThread == null) {
                pChangeListener = pISQLObjectFactory.getChangeListener(projet);
                listenerThread = new Thread(pChangeListener);
                listenerThread.start();
            } else {
                pChangeListener.safe_restart();
                listenerThread = new Thread(pChangeListener);
                listenerThread.start();
            }
        } catch (Exception e) {
        }
    }

    static public void stopListenerThread(String projet) {
        try {
            if (listenerThread != null) {
                pChangeListener.safe_stop();
            }
        } catch (Exception e) {

        }
    }

    /* Config */
    public static String getUsedLocale() {
        return Config.getUsedLocale();
    }

    public static void saveLocale(String locale) {
        pConfig.saveLocale(locale.toLowerCase().trim());
    }

    public static Vector getLocales() {
        return pConfig.getLocales();
    }

    public static int getLogLevel() {
        return pConfig.getLogLevel();
    }

    public static boolean isIDE_DEV() {
        return pConfig.isIDE_DEV();
    }

    public static boolean isALLOW_PLUGINS() {
        return pConfig.isALLOW_PLUGINS();
    }

    public static boolean isNET_CHANGE_TRACK() {
        return pConfig.isNET_CHANGE_TRACK();
    }

    public static boolean isWith_ICAL() {
        return pConfig.isWITH_ICAL();
    }

    public static boolean isLockExecutedTest() {
        return pConfig.isLockExecutedTest();
    }

    public static boolean isDYNAMIC_VALUE_DATASET() {
        return pConfig.isDYNAMIC_VALUE_DATASET();
    }

    // 20100112 - D�but modification Forge ORTF v1.0.0
    public static String getAttachementRepo() {
        return pConfig.getAttachementRepo();
    }

    // 20100112 - Fin modification Forge ORTF v1.0.0
    public static String getUserAuthentification() {
        return pConfig.getUserAuthentification();
    }

    public static void setUserAuthentification(String str) {
        pConfig.setUserAuthentification(str);
        validUserAuthentification = str;
    }

    /**
     * @return -1 = NONE, 0 = mysql, 1 = salome
     */
    public static int getLockMeth() {
        return pConfig.getUSE_DB_LOCK();
    }

    /* Session */
    public static void deleteConnection(int idConn) {
        try {
            pISQLObjectFactory.getISQLSession().deleteSession(idConn);
        } catch (Exception e) {
            Util.log("[Api->deleteConnection] Error on session " + idConn);
            Util.err(e);
        }
    }

    public static String getConnectionProject(int IdConnection) {
        String project = null;
        try {
            ConnectionWrapper pConnectionWrapper = pISQLObjectFactory
                .getISQLSession().getSession(IdConnection);
            project = pConnectionWrapper.getProjectConnected();
            String login = pConnectionWrapper.getLoginConnected();

            int permission = 0;
            if (!project.equals("ALL")) {
                int idProject = pISQLObjectFactory.getISQLProject().getProject(
                        project).getIdBDD();
                permission = pISQLObjectFactory.getISQLPersonne()
                    .getPermissionOfUser(idProject, login);

            } else {
                permission |= Permission.ALLOW_CREATE_TEST;
                permission |= Permission.ALLOW_CREATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_TEST;
                permission |= Permission.ALLOW_EXECUT_CAMP;
                permission |= Permission.ALLOW_DELETE_CAMP;
                permission |= Permission.ALLOW_DELETE_TEST;
            }
            Permission.setPermission(permission);

        } catch (Exception ex) {
            Util.log("[Api->getConnectionProject] Error on session " +
                    IdConnection);
            Util.err(ex);
        }
        return project;
    }

    public static void initConnectionUser(String project, String login) {
        try {

            int permission = 0;
            if (!project.equals("ALL")) {
                int idProject = pISQLObjectFactory.getISQLProject().getProject(
                        project).getIdBDD();
                permission = pISQLObjectFactory.getISQLPersonne()
                    .getPermissionOfUser(idProject, login);

            } else {
                permission |= Permission.ALLOW_CREATE_TEST;
                permission |= Permission.ALLOW_CREATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_TEST;
                permission |= Permission.ALLOW_EXECUT_CAMP;
                permission |= Permission.ALLOW_DELETE_CAMP;
                permission |= Permission.ALLOW_DELETE_TEST;
                // permission |= Permission.ALLOW_CREATE_REQ;
                // permission |= Permission.ALLOW_UPDATE_REQ;
                // permission |= Permission.ALLOW_DELETE_REQ;
            }
            Permission.setPermission(permission);

        } catch (Exception ex) {
            Util.log("[Api->getConnectionProject] Error on set Permission ");
            Util.err(ex);
        }
    }

    /**
     * Selection du login pour une connection
     *
     * @param idConnection
     */
    public static String getConnectionUser(int IdConnection) {
        String login = null;
        try {
            ConnectionWrapper pConnectionWrapper = pISQLObjectFactory
                .getISQLSession().getSession(IdConnection);
            login = pConnectionWrapper.getLoginConnected();
            String project = pConnectionWrapper.getProjectConnected();
            int permission = 0;
            if (!project.equals("ALL")) {
                int idProject = pISQLObjectFactory.getISQLProject().getProject(
                        project).getIdBDD();
                permission = pISQLObjectFactory.getISQLPersonne()
                    .getPermissionOfUser(idProject, login);

            } else {
                permission |= Permission.ALLOW_CREATE_TEST;
                permission |= Permission.ALLOW_CREATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_CAMP;
                permission |= Permission.ALLOW_UPDATE_TEST;
                permission |= Permission.ALLOW_EXECUT_CAMP;
                permission |= Permission.ALLOW_DELETE_CAMP;
                permission |= Permission.ALLOW_DELETE_TEST;
            }
            Permission.setPermission(permission);
        } catch (Exception ex) {
            Util.log("[Api->getConnectionUser] Error on session "
                     + IdConnection);
            Util.err(ex);
        }
        return login;
    }

    static void createTMP() {
        try {
            String tmpDir = System.getProperties()
                .getProperty("java.io.tmpdir");
            String fs = System.getProperties().getProperty("file.separator");
            File file = new File(tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP);
            if (!file.exists()) {
                file.mkdirs();
            } else {
                clearDir(file);
            }
        } catch (Exception e) {
            Util.err(e);
        }
    }

    static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // The directory is now empty so delete it
        return dir.delete();
    }

    static boolean clearDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return true;
    }

    static void deleteTMP() {
        try {
            String tmpDir = System.getProperties()
                .getProperty("java.io.tmpdir");
            String fs = System.getProperties().getProperty("file.separator");
            File file = new File(tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP);
            deleteDir(file);
        } catch (Exception e) {
            Util.err(e);
        }
    }

    static public List<String> getErrorListConnections () {
        return errorListConnections;
    }

    static public List<String> getErrorListProperties () {
        return errorListProperties;
    }

}
