package org.objectweb.salome_tmf.api;

import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.InputStream;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.swing.UIManager;


public class Util {
    /**
     * Recherche de l'heure courante
     * @return heure actuelle
     */
    public static Time getCurrentTime() {
        Calendar calendar = new GregorianCalendar();
        int heure = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int seconde = calendar.get(Calendar.SECOND);
        String chaineHeureActuelle = heure + ":" + minute + ":" + seconde;
        Time heureActuelle = Time.valueOf(chaineHeureActuelle);
        return heureActuelle;
    }
    /**
     * Recherche de la date actuelle
     * @return date actuelle
     */
    public static Date getCurrentDate() {
        return new Date(System.currentTimeMillis());
    }

    public static Properties getPropertiesFile(String file)  throws Exception {
        Properties prop = new Properties();
        InputStream in = Util.class.getResourceAsStream(file);
        prop.load(in);
        in.close();
        return prop;
    }

    public static Properties getPropertiesStream(InputStream in)  throws Exception {
        Properties prop = new Properties();
        prop.load(in);
        in.close();
        return prop;
    }

    public static Properties getPropertiesFile(java.net.URL url) throws Exception{
        Properties prop = new Properties();
        InputStream in = url.openStream();
        prop.load(in);
        in.close();
        return prop;
    }


    public static void adaptFont(){
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            GraphicsDevice gd = gs[0];
            GraphicsConfiguration[] gc = gd.getConfigurations();
            Rectangle dim = gc[0].getBounds();
            Util.debug("------------------------Resolution "+ dim.width + " x " + dim.height);
            if (800 >= dim.width ){
                setUIFont(new Font("Arial",Font.PLAIN,9));
            } else if (1024  >= dim.width ){
                setUIFont(new Font("Arial",Font.PLAIN,11));
            } else if (1280  >= dim.width ){
                setUIFont(new Font("Arial",Font.PLAIN,13));
            }
        } catch(Exception e){
            Util.err(e);
        }
    }

    public static void setUIFont (Font f){
        //
        // sets the default font for all Swing components.
        // ex.
        //  setUIFont (new javax.swing.plaf.FontUIResource("Serif",Font.ITALIC,12));
        //
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get (key);
            if (value instanceof javax.swing.plaf.FontUIResource)
                UIManager.put (key, f);
        }
    }

    static public void log () {
        log(null, false);
    }

    static public void log (Object o) {
        log(o, false);
    }

    static public void log (String str) {
        log((Object)str);
    }

    static public void log (Object o, boolean forceLogging) {
        if (Api.getLogLevel() <= Config.LOG_INFO_LEVEL || forceLogging)
            System.out.println("INFO - " + o);
    }

    static public void debug () {
        debug(null, false);
    }

    static public void debug (Object o) {
        debug(o, false);
    }

    static public void debug (Object o, boolean forceLogging) {
        int logLevel = Api.getLogLevel();
        if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL || forceLogging) {
            System.out.println("DEBUG - " + o);
        }
    }

    static public void err (String str) {
        err(str, null);
    }

    static public void err (Throwable t) {
        err(null, t);
    }

    static public void err (String str, Throwable t) {
        if (Api.getLogLevel() <= Config.LOG_ERROR_LEVEL) {
            if (str != null)
                System.err.println("ERROR - " + str);
            if (t != null)
                t.printStackTrace(System.err);
        }
    }
}
