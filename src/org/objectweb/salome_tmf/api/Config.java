package org.objectweb.salome_tmf.api;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.api.sql.ISQLObjectFactory;

public class Config {
    int LOG_LEVEL = 0;
    boolean ALLOW_PLUGINS = true;

    // If set, then a Bug-Number is mandatory.
    static boolean BUGVERIFICATION = false;

    // Polarion specific stuff
    static boolean POLARIONLINKS = false;
    static List<String> polarionHyperlink = new ArrayList<>();
    
    // Attached projects / databases
    static List<Connection> attachedConnections = new ArrayList<>();
    
    // List of keys for bug trackers
    static List<String> bugtrackingKeys = new ArrayList<>();

    // LXR specific stuff
    static boolean LXRLINKS = false;
    static String LXRFIRST = "";
    static String LXRSECOND = "";
    
    boolean IDE_DEV = false;
    boolean NET_CHANGE_TRACK = true;
    String ATTACH_REPO = null;
    Vector LOCALES_LIST = null;
    static String locale = null;
    boolean WITH_ICAL = false;
    boolean LOCK_ON_EXEC = true;
    int LOCK_METHODE = 0; //-1 = NONE, 0 = mysql, 1 = salome
    boolean LOCK_EXECUTED_TEST = true;
    boolean DYNAMIC_VALUE_DATASET = true;
    static ISQLConfig pISQLConfig = null;

    public static int LOG_INFO_LEVEL = 0;
    public static int LOG_DEBUG_LEVEL = 1;
    public static int LOG_ERROR_LEVEL = 2;
    public static int LOG_NOLOG_LEVEL = 3;

    public String userAuthentification;

    void init(ISQLObjectFactory pISQLObjectFactory){
        if (pISQLConfig == null){
            pISQLConfig = pISQLObjectFactory.getISQLConfig();
        }
    }

    public boolean isALLOW_PLUGINS() {
        return ALLOW_PLUGINS;
    }
    void setALLOW_PLUGINS(boolean allow_plugins) {
        ALLOW_PLUGINS = allow_plugins;
    }
    //20100112 - D\ufffdbut modification Forge ORTF v1.0.0
    public String getAttachementRepo() {
        return ATTACH_REPO;
    }
    void setAttachementRepo(String attach_repo) {
        ATTACH_REPO = attach_repo;
    }
    //20100112 - Fin modification Forge ORTF v1.0.0
    public int getLogLevel() {
        return LOG_LEVEL;
    }
    void setLogLevel(int debug) {
        LOG_LEVEL = debug;
    }

    public boolean isIDE_DEV() {
        return IDE_DEV;
    }
    void setIDE_DEV(boolean ide_dev) {
        IDE_DEV = ide_dev;
    }

    public static boolean getBugVerification() {
        return BUGVERIFICATION;
    }
    
    public static void setBugVerification(boolean verify) {
        BUGVERIFICATION = verify;
    }

    public static void addBugtrackingKey(String key) {
        bugtrackingKeys.add(key);
    }
    
    public static List<String> getBugtrackingKeys() {
        return bugtrackingKeys;
    }
            
    public static boolean getPolarionLinks() {
        return POLARIONLINKS;
    }
    
    public static void setPolarionLinks(boolean verify) {
        POLARIONLINKS = verify;
    }

    public static void addPolarionHyperlink(String link) {
        polarionHyperlink.add(link);
    }

    public static List<String> getPolarionHyperlinks() {
        return polarionHyperlink;
    }

    public static boolean getLxrLinks() {
        return LXRLINKS;
    }
    
    public static void setLxrLinks(boolean verify) {
        LXRLINKS = verify;
    }
    
    public static String getLxrFirst() {
        return LXRFIRST;
    }
    
    public static void setLxrFirst(String first) {
        LXRFIRST = first;
    }

    public static String getLxrSecond() {
        return LXRSECOND;
    }
    
    public static void setLxrSecond(String second) {
        LXRSECOND = second;
    }

    void setLockOnExec(boolean db_lock) {
        LOCK_ON_EXEC = db_lock;
    }

    public boolean isLockOnExec() {
        return LOCK_ON_EXEC;
    }

    public void setUserAuthentification(String str) {
        userAuthentification = str;
    }

    public String getUserAuthentification() {
        return userAuthentification;
    }

    void setLockExecutedTest(boolean lock_test) {
        LOCK_EXECUTED_TEST = lock_test;
    }

    public boolean isLockExecutedTest() {
        return LOCK_EXECUTED_TEST;
    }

    void setUSE_DB_LOCK(int db_lock) {
        LOCK_METHODE = db_lock;
    }

    public int getUSE_DB_LOCK() {
        return (LOCK_METHODE);
    }

    public boolean isNET_CHANGE_TRACK() {
        return NET_CHANGE_TRACK;
    }
    void setNET_CHANGE_TRACK(boolean net_change_track) {
        NET_CHANGE_TRACK = net_change_track;
    }

    public boolean isWITH_ICAL() {
        return WITH_ICAL;
    }
    void setWITH_ICAL(boolean with_ical) {
        WITH_ICAL = with_ical;
    }

    public boolean isDYNAMIC_VALUE_DATASET(){
        return DYNAMIC_VALUE_DATASET;
    }

    void setDYNAMIC_VALUE_DATASET(boolean dynamic) {
        DYNAMIC_VALUE_DATASET = dynamic;
    }

    public static String getUsedLocale() {
        if (locale != null){
            return locale;
        } else {
            if (pISQLConfig != null){
                try {
                    locale = pISQLConfig.getSalomeConf("Locale");
                } catch (Exception e){
                    Util.err("[Config->getUsedLocale]", e);
                }
            } else {
                //Util.log("[Config->getUsedLocale] return default local en");
                return "en";
            }
        }
        return locale;
    }

    public void saveLocale(String newLocale) {
        if (pISQLConfig != null){
            try {
                locale = newLocale;
                pISQLConfig.updateSalomeConf("Locale", newLocale);
            } catch (Exception e){
                Util.err("[Config->saveLocale]", e);
            }
        }
    }

    public void addLocale(String local){
        if (pISQLConfig != null){
            try {
                String list = pISQLConfig.getSalomeConf("LocalesList");
                list = list + ", " +local;
                pISQLConfig.updateSalomeConf("LocalesList", list);
            } catch (Exception e){
                Util.err("[Config->addLocale]", e);
            }
        }
    }

    public Vector getLocales() {
        if (LOCALES_LIST != null){
            return LOCALES_LIST;
        } else {
            if (pISQLConfig != null){
                try {
                    String list = pISQLConfig.getSalomeConf("LocalesList");
                    String tabList[] = list.split(",");
                    LOCALES_LIST = new Vector();
                    for (int i = 0 ; i < tabList.length; i++){
                        LOCALES_LIST.add(tabList[i]);
                    }
                    return LOCALES_LIST;
                } catch (Exception e){
                    LOCALES_LIST = null;
                    Util.err("[Config->getLocales]", e);
                }
            }
        }
        return null;
    }
    
    public static void setAttachedConnections(List<Connection> attachedConnections) {
        Config.attachedConnections = attachedConnections;
    }
    
    public static List<Connection> getAttachedConnections() {
        return attachedConnections;
    }

}
