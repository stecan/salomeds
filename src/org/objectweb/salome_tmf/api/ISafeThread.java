package org.objectweb.salome_tmf.api;

public interface ISafeThread extends Runnable {
    public void safe_stop();
        
    public void safe_restart();
}
