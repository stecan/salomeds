package org.objectweb.salome_tmf.api;

public class Permission {

    public static int ALLOW_CREATE_TEST = 2;
    public static int ALLOW_UPDATE_TEST = 4;
    public static int ALLOW_DELETE_TEST = 8;

    public static int ALLOW_CREATE_CAMP = 16;
    public static int ALLOW_UPDATE_CAMP = 32;
    public static int ALLOW_DELETE_CAMP = 64;
    public static int ALLOW_EXECUT_CAMP = 128;

    public static int ALLOW_CREATE_REQ = 256;
    public static int ALLOW_UPDATE_REQ = 512;
    public static int ALLOW_DELETE_REQ = 1024;

    private static int  permission = 0;


    public static void setPermission(int perm) {
        permission = perm;
        Util.log("[Permission->setPermission] : " + perm);
        Util.log("[Permission->setPermission] canDeleteTest : " + canDeleteTest());
        Util.log("[Permission->setPermission] canCreateTest : " + canCreateTest());
        Util.log("[Permission->setPermission] canUpdateTest : " + canUpdateTest());
        Util.log("[Permission->setPermission] canDeleteCamp : " + canDeleteCamp());
        Util.log("[Permission->setPermission] canCreateCamp : " + canCreateCamp());
        Util.log("[Permission->setPermission] canUpdateCamp : " + canUpdateCamp());
        Util.log("[Permission->setPermission] canExecutCamp : " + canExecutCamp());
    }
    static public boolean canCreateTest(){
        return (((permission & ALLOW_CREATE_TEST ) == ALLOW_CREATE_TEST) || canDeleteTest() );
    }

    static public boolean canUpdateTest(){
        return (((permission & ALLOW_UPDATE_TEST ) == ALLOW_UPDATE_TEST) ||  canCreateTest() );
    }

    static  public boolean canDeleteTest(){
        return ((permission & ALLOW_DELETE_TEST ) == ALLOW_DELETE_TEST);
    }

    static public boolean canCreateCamp(){
        return (((permission & ALLOW_CREATE_CAMP ) == ALLOW_CREATE_CAMP) || canDeleteCamp() );
    }

    static public boolean canUpdateCamp(){
        return (((permission & ALLOW_UPDATE_CAMP ) == ALLOW_UPDATE_CAMP)  || canCreateCamp() );
    }

    static public boolean canDeleteCamp(){
        int result = (permission &  ALLOW_DELETE_CAMP );
        return ((permission &  ALLOW_DELETE_CAMP ) ==  ALLOW_DELETE_CAMP);
    }

    static public boolean canExecutCamp(){
        return ((permission &  ALLOW_EXECUT_CAMP ) ==  ALLOW_EXECUT_CAMP);
    }

    static public boolean canCreateReq(){
        int result = (permission &  ALLOW_CREATE_REQ );
        return ((permission &  ALLOW_CREATE_REQ ) ==  ALLOW_CREATE_REQ);
    }

    static public boolean canUpdateReq(){
        return ((permission &  ALLOW_UPDATE_REQ ) ==  ALLOW_UPDATE_REQ);
    }

    static public boolean canDeleteReq(){
        return ((permission &  ALLOW_DELETE_REQ ) ==  ALLOW_DELETE_REQ);
    }

}
