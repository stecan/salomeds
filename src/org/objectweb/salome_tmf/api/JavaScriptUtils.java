package org.objectweb.salome_tmf.api;

import java.applet.Applet;
import java.lang.reflect.Method;
import java.net.URLDecoder;

import javax.swing.JApplet;

import org.jfree.util.Log;

public class JavaScriptUtils {

    final String SALOME_TMF = "salome_tmf";
    final String SALOME_TMF_MD5 = "salome_tmf_md5";
    final String SALOME_TMF_AUTHENTIFICATION = "salome_tmf_authentification";

    String strProject = null;
    String strUnixProject = null;
    String strLogin = null;
    String strMD5Password = null;
    JApplet parentApplet;
    boolean cookiesLoaded = false;
    boolean MD5CookiesLoaded = false;
    String userAuthentification = null;

    public JavaScriptUtils(JApplet parentApplet) {
        this.parentApplet = parentApplet;

        Api.setStrUsername(getLoginCookies());
        Api.setStrMD5Password(getMD5PasswordCookies());
    }

    private void loadingCookies() {
        try {
            String cookiesValue = getCookie(SALOME_TMF);
            Util
                .log("[SalomeTMF_AppJWS->loadingFromCookies] Cookie salome_tmf value is "
                     + cookiesValue);
            Util.debug("Cookie avant decode " + cookiesValue);
            if (cookiesValue != null && !cookiesValue.equals("")) {
                cookiesValue = URLDecoder.decode(cookiesValue, "ISO-8859-1");
                Util.debug("Cookie apr\ufffds decode " + cookiesValue);
                int indexDebut = cookiesValue.indexOf("=");
                if (indexDebut > 0) {
                    strProject = cookiesValue.substring(0, indexDebut);
                    strLogin = cookiesValue.substring(indexDebut + 1,
                                                      cookiesValue.length());
                    int indexMiddle = strLogin.indexOf("=");
                    if (indexMiddle > 0) {
                        cookiesValue = strLogin;
                        strLogin = cookiesValue.substring(0, indexMiddle);
                        strUnixProject = cookiesValue.substring(
                                                                indexMiddle + 1, cookiesValue.length());
                    }
                }
            }
            Util.debug("Cookies read is strProject = " + strProject
                       + ", strLogin = " + strLogin + ", unix name= "
                       + strUnixProject);
            String cookiesValue2 = getCookie(SALOME_TMF_AUTHENTIFICATION);
            Util
                .log("[SalomeTMF_AppJWS->loadingFromCookies] Cookie salome_tmf_Authentification value is "
                     + cookiesValue2);

            if (cookiesValue2 != null && !cookiesValue2.equals("")) {
                userAuthentification = cookiesValue2;
            }

            Util.debug("Cookies read is salome_tmf_Authentification = "
                       + userAuthentification);

        } catch (Exception e) {
            strProject = null;
            strLogin = null;
            Util.err(e);
        }
        cookiesLoaded = true;
    }

    private String getCookie(String name) {
        /*
         * * get a specific cookie by its name, parse the cookie.
         */
        String myCookie = getCookie();
        String search = name + "=";
        if (myCookie.length() > 0) {
            int offset = myCookie.indexOf(search);
            if (offset != -1) {
                offset += search.length();
                int end = myCookie.indexOf(";", offset);
                if (end == -1)
                    end = myCookie.length();
                return myCookie.substring(offset, end);
            } else {
                Util.debug("-----------> Did not find cookie: " + name);
            }
        }
        return "";
    }

    private String getCookie() {
        /*
         * * get all cookies for a document
         */
        try {

            Class classJSObject = Class.forName("netscape.javascript.JSObject");

            Method meth_getWindow = classJSObject.getMethod("getWindow",
                                                            new Class[] { Applet.class });
            Object myBrowser = meth_getWindow.invoke(null,
                                                     new Object[] { parentApplet });

            Method meth_getMember = classJSObject.getMethod("getMember",
                                                            new Class[] { String.class });
            Object myDocument = meth_getMember.invoke(myBrowser,
                                                      new Object[] { "document" });

            String myCookie = (String) meth_getMember.invoke(myDocument,
                                                             new Object[] { "cookie" });
            Util.debug("------------> getCookie = " + myCookie);
            /*
             * JSObject myBrowser = (JSObject) JSObject.getWindow(parentApplet);
             * JSObject myDocument = (JSObject) myBrowser.getMember("document");
             * String myCookie = (String)myDocument.getMember("cookie");
             */
            if (myCookie.length() > 0)
                return myCookie;
        } catch (Exception e) {
            Log.error(e);
        }
        return "?";
    }

    /********************* Public *******************/
    public void closeWindow() {
        callWindwowsMethode("close", null);
        // Util.debug(callWindwowsMethode("confirm",new Object[]
        // {"Confirmer"}));
        // confirm('Chaine de caracteres');
    }

    public Object callWindwowsMethode(String meth, Object[] arg) {
        Object ret = null;
        try {
            // Class classJSObject =
            // Class.forName("sun.plugin.javascript.JSObject");
            Class classJSObject = Class.forName("netscape.javascript.JSObject");
            Method meth_getWindow = classJSObject.getMethod("getWindow",
                                                            new Class[] { Applet.class });
            Object myBrowser = meth_getWindow.invoke(null,
                                                     new Object[] { parentApplet });
            // Util.debug("pJSObject = " + pJSObject);
            Method meth_call = classJSObject.getMethod("call", new Class[] {
                    String.class, Object[].class });
            ret = meth_call.invoke(myBrowser, new Object[] { meth, arg });
            /*
             * Method[] tabMeth = classJSObject.getMethods(); for (int i=0 ; i <
             * tabMeth.length; i++){ Method pMethod = tabMeth[i];
             * Util.debug("Methode disponible = " + pMethod); }
             */
        } catch (Exception e) {
            Util.err(e);
        }
        return ret;
    }

    public void setCookies(String project, String login, String MD5Password,
                           String UserAuthentification) throws Exception {
        setCookies(SALOME_TMF, project + "=" + login);
        setCookies(SALOME_TMF_MD5, MD5paswd.getEncodedPassword(MD5Password));
        setCookies(SALOME_TMF_AUTHENTIFICATION, UserAuthentification);
    }

    // public void setCookies(String project, String login, String MD5Password)
    // throws Exception {
    // setCookies(SALOME_TMF, project+ "=" + login);
    // setCookies(SALOME_TMF_MD5, MD5paswd.getEncodedPassword(MD5Password));
    // }

    public void setCookies(String name, String value) throws Exception {
        // String name = SALOME_TMF;
        // String value = project+ "=" + login;

        Class classJSObject = Class.forName("netscape.javascript.JSObject");
        Method meth_getWindow = classJSObject.getMethod("getWindow",
                                                        new Class[] { Applet.class });
        Object myBrowser = meth_getWindow.invoke(null,
                                                 new Object[] { parentApplet });

        Method meth_getMember = classJSObject.getMethod("getMember",
                                                        new Class[] { String.class });
        Object myDocument = meth_getMember.invoke(myBrowser,
                                                  new Object[] { "document" });

        if (myDocument == null) {
            Util.err("Can't set cookie: no document reference");
            throw new Exception("Can't set cookie: no document reference");
        }

        try {
            String cookieText = name + "=" + value;
            Util.debug("Setting document.cookie = \"" + cookieText + '"');
            Method meth_setMember = classJSObject.getMethod("setMember",
                                                            new Class[] { String.class, Object.class });
            meth_setMember.invoke(myDocument, new Object[] { "cookie",
                                                             cookieText });
        } catch (Exception ex) {
            Util.err(ex);
        }

    }

    public String getLoginCookies() {
        if (cookiesLoaded == false) {
            loadingCookies();
        }
        return strLogin;
    }

    public String getProjectCookies() {
        if (cookiesLoaded == false) {
            loadingCookies();
        }
        return strProject;
    }

    public String getUnixProjectCookies() {
        if (cookiesLoaded == false) {
            loadingCookies();
        }
        return strUnixProject;
    }

    public String getMD5PasswordCookies() {
        if (!MD5CookiesLoaded) {
            strMD5Password = getCookie(SALOME_TMF_MD5);
            MD5CookiesLoaded = true;
        }
        return strMD5Password;
    }

    public String getUserAuthentficationCookies() {
        if (cookiesLoaded == false) {
            loadingCookies();
        }
        return userAuthentification;
    }
}
