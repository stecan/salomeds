/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.ParameterWrapper;


public interface ISQLParameter extends Remote {
    /**
     * Add parameter  in a project
     * @param idProject  : id in Dabase of the project
     * @param name : name of the parameter 
     * @param description : description of the parameter
     * @return the id of the parameter in the dadabase
     * need permission canCreateTest
     */
    public int insert(int IdProject, String name, String description) throws Exception ;
        
    /**
     * get database id for a  parameter identified by name in the project IdProject
     * @param idProject
     * @param name
     * @return the database id of the parameter identified by name in the project IdProject
     * @throws Exception
     */
    public int selectID(int IdProject, String name) throws Exception ;
        
    /**
     * Update parameter (identifed by idParameter) description in DataBase 
     * @param idParameter
     * @param description : the new description of the parameter
     * @throws Exception
     * need permission canUpdateTest
     */
    public void updateDescription(int idParameter,  String description) throws Exception ;
        
    /**
     * Delete a parameter in Salome Database, include the suppresion of the parameter
     * in test, and in action
     * @param idParameter
     * @throws Exception
     * need permission canDeleteTest
     */
    public void delete(int idParameter) throws Exception ;
        
    /**
     * Get a ParameterWrapper representing the parameter idParameter in the database 
     * @param idParameter
     * @return
     * @throws Exception
     */
    public ParameterWrapper getParameterWrapper(int idParameter) throws Exception;
        
}
