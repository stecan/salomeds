package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

public interface ISQLEngine extends Remote {
        
    public boolean isClose() throws Exception;
    public void close() throws Exception;
    public boolean isLock() throws Exception;
    //public void lockAll() throws Exception;
    public void lockTPlan() throws Exception;
    //public void unLockAll() throws Exception;
    public void unLockTPlan() throws Exception;
    
    public  int beginTransDB(int lock_code, int type) throws Exception;
    public  void commitTransDB(int type) throws Exception ;
    public  void rollForceBackTransDB(int type) throws Exception;
        
    public void addSQLLoackRead(String table) throws Exception;
    public void addSQLLoackWrite(String table) throws Exception;
        
    public void setConnexionInfo(int _projectID, int _personneID) throws Exception;
        
    public int getProjectID() throws Exception;
        
    public int getPersonneID() throws Exception;
}
