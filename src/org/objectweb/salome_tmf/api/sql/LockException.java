package org.objectweb.salome_tmf.api.sql;

public class LockException extends Exception{

    static public final int DOLOCK = 1;
    static public final int INSERT_LOCK = 2;
    static public final int DELETE_LOCK = 3;
    public int code;
    public LockException(String msg, int _code){
        super(msg);
        code = _code;
    }
}
