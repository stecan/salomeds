package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLTest extends Remote {
    /**
     * Add in database the reference of use paramter paramId for test idTest in table CAS_PARAM_TEST
     * @param idTest  : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * need permission canUpdateTest
     */
    public void addUseParam(int idTest, int paramId) throws Exception ;
        
    /**
     * Attach a file attach to a test in table ATTACHEMENT and reference in table CAS_ATTACHEMENT
     * @param idTest : id of the test
     * @param f : the file
     * @param description of the file
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @See ISQLFileAttachment.insert(File, String);
     * no permission needed
     */
    public int addAttachFile(int idTest, SalomeFileWrapper f, String description) throws Exception ;
        
    /**
     * Attach a url to a test in table ATTACHEMENT and reference in table CAS_ATTACHEMENT
     * @param idTest : id of the test
     * @param url
     * @param description of the url
     * @return the id of the attachment in the table ATTACHEMENT
     * @see ISQLUrlAttachment.insert(String, String);
     * @throws Exception
     */
    public int addAttachUrl(int idTest, String url, String description) throws Exception ;
        
    /**
     * Update the test with new name and description
     * @param idTest : id of the test
     * @param newName : the new name
     * @param newDesc : the new description
     * @throws Exception
     * need permission canUpdateTest
     */
    public void update(int idTest, String newName, String newDesc) throws Exception ;
         
    /**
     * Update order of the test by incrementation if increment = true or decrementation
     * @param idTest : id of the test
     * @param increment
     * @return the new order of the test in database
     * @throws Exception
     */
    public int updateOrder(int idTest, boolean increment) throws Exception ;
         
    /**
     * replace all reference of user oldIdUser by newIdUser in the table (CAS_TEST) where suite = idSuite
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    public void updateUserRef(int idSuite, int oldIdUser, int newIdUser)  throws Exception ;
         
    /**
     * Delete the test in table CAS_TEST and all reference (Parameter, Campaign) and the test attachments
     * delete test actions (if manual test), delete test script (if automatic test)
     * @param idTest : id of the test
     * @throws Exception
     * @see deleteAllUseParam(int)
     * @see deleteAllAttach(int)
     * @see ISQLCampaign.deleteTestInAllCampaign(int)
     * need permission canDeleteTest
     */
    public void delete(int idTest)  throws Exception ;
         
    /**
     * Delete the test in table CAS_TEST and all reference (Parameter, Campaign) and the test attachments
     * delete test actions (if manual test), delete test script (if automatic test)
     * Update order of test in the suite if reorder = true
     * @param idTest : id of the test
     * @param reorder reorder the test in the suite
     * @throws Exception
     * @see deleteAllUseParam(int)
     * @see deleteAllAttach(int)
     * @see ISQLCampaign.deleteTestInAllCampaign(int)
     * need permission canDeleteTest
     * @TODO SOAP
     */
    public void delete(int idTest, boolean reorder)  throws Exception ;
                
                
    /**
     * Delete all attachment in table (CAS_ATTACHEMENT and ATTACHEMENT) for the test
     * @param idTest : id of the test
     * @throws Exception
     * @see deleteAttach(int, int)
     * no permission needed
     */
    public void deleteAllAttach(int idTest) throws Exception ;
         
    /**
     * Delete all reference about using parameter (table CAS_PARAM_TEST) for the test
     * if the test is manual, parameters are deleted in action  
     * @param idTest : id of the test
     * @throws Exception
     * need permission  canUpdateTest
     */
    public void deleteAllUseParam(int idTest) throws Exception ;
         
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test
     * if the test is manual, parameters are deleted in action  
     * @param idTest : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * @see ISQLAction.deleteParamUse(int, int)
     * need permission  canUpdateTest
     */
    public void deleteUseParam(int idTest, int paramId) throws Exception ;
         
    /**
     * Delete attachement for the test and the attachement (tables CAS_ATTACHEMENT, ATTACHEMENT)
     * @param idTest : id of the test
     * @param attachId : id of the attachment
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idTest, int attachId) throws Exception ;
         
        
    /**
     * Get a TestWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     * @see TestWrapper
     */
    public TestWrapper getTest(int testId) throws Exception ;
         
    /**
     * Get the id of the test identified by name in the testlist identified by idTestList
     * @param idTestList : id of the testlist
     * @param name : the test name
     * @return a test id
     * @throws Exception
     */
    public int getID(int idTestList, String name) throws Exception;
         
    /**
     * Get an Array of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int testId) throws Exception ;
         
    /**
     * Get an Array of FileAttachementWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int testId) throws Exception ;
         
    /**
     * Get an Array of UrlAttachementWrapper for the test identified by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int testId) throws Exception ;
       
    /**
     * Get an Array of ParameterWrapper (only id is set) using by the test identifed by testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    public ParameterWrapper[] getAllUseParams(int testId) throws Exception ;
}
