package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;

public interface ISQLPersonne extends Remote {
    /**
     * Insert a user to the datadase
     *
     * @param login
     * @param name
     * @param firstName
     * @param desc
     * @param email
     * @param tel
     * @param pwd
     * @return
     * @throws Exception
     *             no permission needed
     */
    public int insert(String login, String name, String firstName, String desc,
                      String email, String tel, String pwd, boolean crypth)
        throws Exception;

    /**
     * Update information about user identified by idUser
     *
     * @param idUser
     * @param newLogin
     * @param newName
     * @param newFirstName
     * @param newDesc
     * @param newEmail
     * @param newTel
     * @throws Exception
     *             no permission needed
     */
    public void update(int idUser, String newLogin, String newName,
                       String newFirstName, String newDesc, String newEmail, String newTel)
        throws Exception;

    /**
     * Update the password for User userLogin with newPassword
     *
     * @param userLogin
     * @param newPassword
     * @param crypt
     *            (true to crypte the password in MD5)
     * @return the new crypted password
     * @throws Exception
     *             no permission needed
     */
    public String updatePassword(String userLogin, String newPassword,
                                 boolean crypth) throws Exception;

    /**
     * Delete an user in the database the clean all reference about user in
     * project and group if user is the unique admin of an project, the project
     * is deleted
     *
     * @param userLogin
     * @throws Exception
     *             no permission needed
     */
    public void deleteByLogin(String userLogin) throws Exception;

    /**
     * Delete an user in the database the clean all reference about user in
     * project and group if user is the unique admin of an project, the project
     * is deleted
     *
     * @param idUser
     * @throws Exception
     *             no permission needed
     */
    public void deleteById(int idUser) throws Exception;

    /**
     * Delete user reference in project
     *
     * @param idUser
     * @param projectName
     * @throws Exception
     */
    public void deleteInProject(int idUser, String projectName)
        throws Exception;

    /**
     * Get the permission of an user in a projet idProject
     *
     * @param idProject
     * @param userLogin
     * @return
     * @throws Exception
     */
    public int getPermissionOfUser(int idProject, String userLogin)
        throws Exception;

    /**
     * Attach an Url to the User identified by idUser (Table
     * PERSONNE_ATTACHEMENT )
     *
     * @param idUser
     * @param url
     * @param description
     *            of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String) no permission needed
     */
    public int addAttachUrl(int idUser, String url, String description)
        throws Exception;

    /**
     * Delete an attchement idAttach of the user identified by idUser
     *
     * @param idUser
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int) no permission needed
     */
    public void deleteAttach(int idUser, int idAttach) throws Exception;

    /**
     * Get an Array of UrlAttachementWrapper for the user identified by idUser
     *
     * @param idUser
     *            : id of the user
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idUser)
        throws Exception;

    /**
     * Get The Id of a person using login
     *
     * @param login
     * @return
     * @throws Exception
     */
    public int getID(String login) throws Exception;

    /**
     * Get the login of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    public String getLogin(int idUser) throws Exception;

    /**
     * Get the name of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    public String getName(int idUser) throws Exception;

    /**
     * Get the last and the fist name of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    public String getTwoName(int idUser) throws Exception;

    /**
     * Get an UserWrapper of the user identified by login
     *
     * @param login
     * @return
     * @throws Exception
     */
    public UserWrapper getUserByLogin(String login) throws Exception;

    /**
     * Get an UserWrapper of the user identified by idUser
     *
     * @param idUser
     * @return
     * @throws Exception
     */
    public UserWrapper getUserById(int idUser) throws Exception;
}
