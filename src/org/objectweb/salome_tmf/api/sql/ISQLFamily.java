package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLFamily extends Remote {


    /**
     * Insert a Family in the database (table FAMILLE_TEST)
     * @param idProject: id of the project contening the family
     * @param name of the Family
     * @param description of the Family
     * @return the id of the family in the database
     * @throws Exception
     * need permission canCreateTest
     */
    public int insert(int idProject, String name, String description) throws Exception ;
        
    /**
     * Attach a file to the Family identified by idFamily (Table FAMILY_ATTACHEMENT)
     * @param idSuite
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idFamily, SalomeFileWrapper f, String description) throws Exception ;
        
    /**
     * Attach an Url to the Family identified by idFamily (Table FAMILY_ATTACHEMENT)
     * @param idFamily
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idFamily, String url, String description) throws Exception;
        
        
    /**
     * Update a Family identified by idFamily in the database
     * @param idFamily
     * @param name : new name of the family
     * @param description : new description of the family
     * @throws Exception
     * need permission canUpdateTest
     */
    public void update(int idFamily, String name, String description) throws Exception ;
        
    /**
     * Increment or decrement the order of the family identified by idFamily in the project
     * Then, reorder other Family to preserve a correct order 
     * @param idFamily
     * @param increment true for doing a decrementation (+1) or false (-1)
     * @return the new order of the family
     * @throws Exception
     */
    public int updateOrder(int idFamily, boolean increment)  throws Exception ;
        
    /**
     * Delete the Family identified by idProject in the database,
     * Then delete all TestList in the family, an reoder the families in the project
     * @param idFamily
     * @throws Exception
     * @see ISQLTestList.delete(int);
     * need permission canDeleteTest
     */
    public void delete(int idFamily) throws Exception ;
        
    /**
     * Delete the Family identified by idProject in the database,
     * Then delete all TestList in the family, an reoder the families in the project if reorder = true
     * @param idFamily
     * @param reorder re-order the family in the project
     * @throws Exception
     * @see ISQLTestList.delete(int);
     * need permission canDeleteTest
     * @TODO SOAP
     */
    public void delete(int idFamily, boolean reorder) throws Exception ;
        
    /**
     * Delete all attchements of the family identified by idFamily
     * @param idSuite
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idFamily) throws Exception;
        
        
    /**
     * Delete an attchement idAttach of the family identified by idFamily
     * @param idFamily
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idFamily, int idAttach) throws Exception ;
        
    /**
     * Get an Array of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the family identified by idFamily
     * @param idFamily : id of the family
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int idFamily) throws Exception;
        
    /**
     * Get an Array of FileAttachementWrapper for the family identified by idFamily
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int idFamily) throws Exception;
        
    /**
     * Get an Array of UrlAttachementWrapper for the family identified by idFamily
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idFamily) throws Exception ;
        
    /**
     * Get the Unique ID of the Family identified by name in the project idProject
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idProject, String name) throws Exception ;
        
    /**
     * Get the number of TestList in the Family identified by idFamily
     * @param idFamily
     * @return
     * @throws Exception
     */
    public int getNumberOfTestList(int idFamily) throws Exception ;
        
    /**
     * Get a SuiteWrapper representing a TestList at order in the Family identified by idFamily
     * @param idFamily
     * @param order
     * @return
     * @throws Exception
     */
    public SuiteWrapper getTestListByOrder(int idFamily, int order) throws Exception ;
        
    /**
     * Get a FamilyWrapper representing the family identified by idFamily
     * @param idFamily
     * @return
     * @throws Exception
     */
    public FamilyWrapper getFamily(int idFamily)throws Exception ;
        
    /**
     * Get an Array of SuiteWrappers representing all testList in the family
     * @param idFamily
     * @return 
     * @throws Exception 
     */
    public SuiteWrapper[] getTestList(int idFamily) throws Exception ;
        
}
