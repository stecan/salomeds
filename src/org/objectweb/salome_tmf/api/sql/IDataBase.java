package org.objectweb.salome_tmf.api.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface IDataBase {
        
    public void openPoolConnection(String url, String username, String password) throws Exception ;
        
    public void open(String url, String username, String password) throws Exception ;
        
    public void open(String url, String username, String password,
                     java.util.Properties prop_proxy) throws Exception ;
        
    public ResultSet executeQuery(String sql) throws Exception ;
    
    public void executeUpdate(String sql) throws Exception ;
        
    public PreparedStatement prepareStatement(String sql) throws Exception ;
        
    public void beginTrans() throws SQLException ;
        
    public void commit() throws SQLException;
        
    public void rollback() throws SQLException;
        
    public void close() throws Exception ;
        
        
    public Connection getCnt() throws Exception ;

    public void setCnt(Connection cnt) throws Exception ;
}
