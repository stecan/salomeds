/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;

public interface ISQLProject extends Remote {
    /**
     * Insert a project in the DB and create default group and permission for the project
     * @param name
     * @param description
     * @return
     * @throws Exception
     */
    public int insert(String name, String description, int idAdmin) throws Exception;
        
    /**
     * Create a project by copy information with an other project idFromProject
     * @param name
     * @param description : the description of the new project
     * @param idAdmin : the administator of the  new project
     * @param idFromProject : the project  where find the informations to create the new project
     * @param suite : true for copy tests information
     * @param campagne : true for copy campaigns information
     * @param users : true for copy users information
     * @param groupe : true for copy groups information
     * @return
     * @throws Exception
     */
    public int copyProject(String name, String description, int idAdmin, int idFromProject, boolean suite, boolean campagne, boolean users, boolean groupe) throws Exception;
        
    /**
     * Attach a file to the Project identified by idProject (Table  PROJET_VOICE_TESTING_ATTACHEMENT )
     * @param idProject
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idProject, SalomeFileWrapper f, String description) throws Exception;
        
    /**
     * Attach an Url to the Project identified by idProject (Table  PROJET_VOICE_TESTING_ATTACHEMENT )
     * @param idProject
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idProject, String url, String description) throws Exception;
        
    /**
     * Update project name and description in the database for the project idProject
     * @param idProject
     * @param newName
     * @param newDesc
     * @throws Exception
     * no permission needed
     */
    public void update(int idProject, String newName, String newDesc) throws Exception;
        
    /**
     * Delete a project in the database
     * the delete 
     * @param idProject
     * @param name
     * @throws Exception
     */
    public void delete(int idProject, String name) throws Exception ;
        
    /**
     * Delete all attchements of the project identified by idProject
     * @param idProject
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idProject) throws Exception;
        
    /**
     * Delete an attchement idAttach of the project identified by idProject
     * @param idProject
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idProject, int idAttach) throws Exception;
        
    /**
     * Get an Array of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the project identified by idProject
     * @param idProject : id of the project
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int idProject) throws Exception ;
        
    /**
     * Get an Array of FileAttachementWrapper for the project identified by idProject
     * @param idProject : id of the project
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int idProject) throws Exception;
        
    /**
     * Get an Array of UrlAttachementWrapper for the project identified by idProject
     * @param idProject : id of the project
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idProject) throws Exception ;
        
    /**
     * Get the number of family in the project identified by idProject
     * @param idProject
     * @throws Exception
     */
    public int getNumberOfFamily(int idProject) throws Exception ;
        
    /**
     * Get a FamilyWrapper representing a family at order in the project identified by idProject
     * @param idProject
     * @param order
     * @throws Exception
     */
    public FamilyWrapper getFamilyByOrder(int idProject, int order) throws Exception ;
        
        
    /**
     * Get an Array of FamilyWrapper representing all family defined in the project idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public FamilyWrapper[] getFamily(int idProject) throws Exception ;
        
    /**
     * Get a ProjectWrapper representing the project identified by name
     * @param name
     * @return
     * @throws Exception
     */
    public ProjectWrapper getProject(String name) throws Exception ;
        
    /**
     * Get an Array of ProjectWrapper representing all the project in the database
     * @throws Exception
     */
    public ProjectWrapper[] getAllProjects() throws Exception ;
        
    /**
     * Return an Array of UserWrapper representing all available User in Salome-TMF
     * @return
     * @throws Exception
     */
    public UserWrapper[] getAllUser()throws Exception ;
        
    /**
     * Get an Array of UserWrapper representing all the Users in the project projectName
     * @param projectName
     * @throws Exception
     */
    public UserWrapper[] getUsersOfProject(String projectName) throws Exception;
        
    /**
     * Get an Array of UserWrapper representing all admins in the project projectName
     * @param idProject
     * @throws Exception
     */
    public UserWrapper[] getAdminsOfProject(String projectName) throws Exception ;
        
    /**
     * Get an Array of UserWrapper representing all user in groupName in the project projectName
     * @param idProject
     * @throws Exception
     */
    public UserWrapper[] getUserOfGroupInProject(String projectName, String groupName) throws Exception ;
        
    /**
     * Get an Array of ParameterWrapper for the project idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public ParameterWrapper[] getProjectParams(int idProject) throws Exception ;
        
    /**
     * Get an Array of EnvironmentWrapper representing the environnment of the project idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public EnvironmentWrapper[] getProjectEnvs(int idProject) throws Exception ;
        
    /**
     * Get an Array of CampaignWrapper representing the campaigns of the project idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public CampaignWrapper[] getPrjectCampaigns(int idProject) throws Exception ;
        
    /**
     * Get an Array of GroupWrapper representing groups in the project idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public GroupWrapper[] getProjectGroups(int idProject) throws Exception ;
}
