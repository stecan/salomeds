package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.ConnectionWrapper;

public interface ISQLSession extends Remote {
        
    /**
     * Insert a session on table SESSION
     * @param project
     * @param login 
     * @throws Exception
     */
    public int addSession(String project, String login) throws Exception;
        
    /**
     * Delete a session id in table SESSION
     * @param id
     * @throws Exception
     */
    public void deleteSession(int id) throws Exception ;
        
    /**
     * Delete all session in table SESSION
     * @throws Exception
     */
    public void deleteAllSession() throws Exception ;
        
    /**
     * Get an Array of ConnectionWrapper representing all session in the databse  (table SESSION)
     * @return
     * @throws Exception
     */
    public ConnectionWrapper[] getAllSession() throws Exception ;
        
    /**
     *  Get a ConnectionWrapper representing a session idSession in the databse  (table SESSION)
     * @param idSession
     * @return
     * @throws Exception
     */
    public ConnectionWrapper getSession(int idSession)  throws Exception;
}
