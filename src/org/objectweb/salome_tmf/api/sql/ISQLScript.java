/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;
import java.sql.Date;

import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;

public interface ISQLScript extends Remote {
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped)
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    public int insert(String name, String arg1, String extension, String type) throws Exception ;
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped) for an Environnement
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * @param idEnv : id of the Environnement mapped with the script
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    public int insert(String name, String arg1, String extension, String type, int idEnv) throws Exception ;
        
    /**
     * Insert a new script in the table SCRIPT (No attachment are mapped) for an EXECUTION
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param extension : argument 2 of the script (plugin extension)
     * @param type : type of the script 
     * @param idEnv : id of the Environnement mapped with the script
     * (ApiConstants.TEST_SCRIPT, ApiConstants.INIT_SCRIPT, ApiConstants.PRE_SCRIPT, ApiConstants.POST_SCRIPT)
     * @return the id of the new Script
     * @throws Exception
     */
    public int insert(int idExec, String name, String arg1, String extension, String type) throws Exception ;
        
    /**
     * Map the attachment attachId to the script scriptId in the table SCRIPT_ATTACHEMENT
     * @param scriptId
     * @param attachId
     * @throws Exception
     */
    public void addAttach(int scriptId, int attachId) throws Exception;
        

    /**
     * Update the length of the script idScript in the database
     * @param idScript
     * @param length
     * @throws Exception
     * no permission needed
     */
    public void  updateLength(int idScript, long length)throws Exception ;
        
    /**
     * Update the date of the script idScript in the database
     * @param idScript
     * @param date
     * @throws Exception
     * no permission needed
     */
    public void updateDate(int idScript, Date date) throws Exception ;
        
    /**
     * Update the argument reserved for plugin in the script idScript in the database
     * @param idScript
     * @param arg
     * @throws Exception
     * no permission needed
     */
    public void updatePlugArg(int idScript, String arg) throws Exception ;
        
        
    /**
     * Update the conetent of the script id with the content of the file with path filePath
     * @param idScript
     * @param filePath
     * @throws Exception
     * @see ISQLFileAttachment.updateFileContent(int,BufferedInputStream);
     * no permission needed
     */
    public void updateContent(int idScript, String filePath) throws Exception ;
        
        
    /**
     * Update the script attachement (content, date size, and name) In Database
     * @param idScript
     * @param filePath
     * @throws Exception 
     * @see ISQLFileAttachment.updateFile(int, File);
     * no permission needed
     */
    public void update(int idScript, String filePath) throws Exception;
        
    /**
     * Update the script attachement (content, date size, and name) In Database
     * @param idScript
     * @param File
     * @throws Exception 
     * @see ISQLFileAttachment.updateFile(int, File);
     * no permission needed
     */
    public void update(int idScript, SalomeFileWrapper file) throws Exception ;
        
    /**
     * Update the Script name the table SCRIPT and ATTACHEMENT
     * @param idScript
     * @param name
     * @throws Exception
     * @see ISQLFileAttachment.updateFileName(int, String);
     * no permission needed
     */
    public void updateName(int idScript, String name) throws Exception;

    /**
     * Delete the script and mapped file in the tables ( SCRIPT, SCRIPT_ATTACHEMENT, ATTACHEMENT)
     * @param idScript
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment().delete(int)
     */
    public void delete(int idScript) throws Exception ;
        
    /**
     * @return the last id of an attachment in the table SCRIPT_ATTACHEMENT
     * @throws Exception
     */
    public int getLastIdAttach() throws Exception ;
        
    /**
     * @param idScript : id of the script in the database
     * @return the id of the attachment mapped to the script in the database
     * @throws Exception
     */
    public int getScriptIdAttach(int idScript) throws Exception;
        
    /**
     * @param idScript : id of the script in the database
     * @return a SalomeFileWrapper attached to the script
     * @throws Exception
     * @see ISQLFileAttachment.getFile(int)
     */
    public SalomeFileWrapper getFile(int idScript) throws Exception;
         

    /**
     * @param idScript : id of the script in the database
     * @return a SalomeFileWrapper attached to the script
     * @throws Exception
     * @see ISQLFileAttachment.getFile(int)
     */ 
    //public SalomeFileWrapper getFile(int idScript, String path) throws Exception ;
}
