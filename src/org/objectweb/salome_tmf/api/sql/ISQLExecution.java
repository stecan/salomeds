/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;
import java.sql.Date;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLExecution extends Remote {
    /**
     * Insert an Exceution in the campaign idCamp (table EXEC_CAMP)
     * @param idCamp
     * @param name of the execution
     * @param idEnv used by the execution
     * @param idDataSet used by the execution
     * @param idUser who created the execution
     * @param description of the execution
     * @return the id of the execution 
     * @throws Exception
     * need permission canExecutCamp
     */
    public int insert(int idCamp, String name, int idEnv, int idDataSet, int idUser, String description) throws Exception ;
        
    /**
     * Attach a file to the Exceution (table EXEC_CAMP_ATTACH)
     * @param idExec
     * @param file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idExec, SalomeFileWrapper file, String description) throws Exception ;
        
    /**
     * Attach an Url to the Exceution (table EXEC_CAMP_ATTACH)
     * @param idExec
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idExec, String url, String description) throws Exception ;
        
        
    /**
     * Insert a pre-scrit (type ApiConstants.PRE_SCRIPT) to the execution idExec
     * @param idExec
     * @param file of the script
     * @param description the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg2 : argument 2 of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    public int addPreScript(int idExec, SalomeFileWrapper file, String description,  String name, String extention, String arg2) throws Exception ;
        
    /**
     * Insert a pre-scrit (type ApiConstants.POST_SCRIPT) to the execution idExec 
     * @param idExec
     * @param file of the script
     * @param description the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg2 : argument 2 of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    public int addPostScript(int idExec, SalomeFileWrapper file, String description,  String name, String extention, String arg2) throws Exception ;
        
        
    /**
     * Update the name and the description of the execution idExec
     * @param idExec
     * @param name
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    public void updateName(int idExec, String name) throws Exception ;
        
    /**
     * Update the date of the execution idExec
     * @param idExec
     * @param date
     * @throws Exception
     * no permission needed
     */
    public void updateDate(int idExec, Date date) throws Exception ;
        
    /**
     * Update the environment mapped to the execution idExec
     * @param idExec
     * @param idEnv the new env to use
     * @throws Exception
     * need permission canExecutCamp
     */
    public void updateEnv(int idExec, int idEnv) throws Exception ;
        
    /**
     * Update the dataset mapped to the execution idExec
     * @param idExec
     * @param idDataset the new dataset to use
     * @throws Exception
     * need permission canExecutCamp
     */
    public void updateDataset(int idExec, int idDataset) throws Exception;
        
        
    /**
     * replace all reference of user oldIdUser by newIdUser in the table (EXEC_CAMP) where campagne = idCamp
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    public void updateUserRef(int idCamp, int oldIdUser, int newIdUser)  throws Exception ;
         
    /**
     * Delete all attchements of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idExec) throws Exception ;
        
    /**
     * Delete the execution idExec in the Database
     * Then delete all attachemnts, the script, and all related execution result
     * @param idExec
     * @see ISQLExecutionResult.delete(int)
     * need permission canExecutCamp
     */
    public void delete(int idExec) throws Exception ;
        
    /**
     * Delete all execution result for the execution idExec in the Database
     * @param idExec
     * @see ISQLExecutionResult.delete(int)
     * need permission canExecutCamp
     */
    public void deleteAllExecResult(int idExec) throws Exception ;
        
    /**
     * Delete an attchement idAttach of the execution idExec
     * @param idExec
     * @param idAttach
     * @throws Exception
     * no permission needed
     */
    public void deleteAttach(int idExec, int idAttach)throws Exception ;

        
    /**
     * Delete the pre-script of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    public void deletePreScript(int idExec) throws Exception;
                  
    /**
     * Delete the post-script of the execution idExec
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    public void deletePostScript(int idExec) throws Exception ;
          
    /**
     * Delete pre-script and post-script of the execution idEnv
     * @param idEnv
     * @throws Exception
     * no permission needed
     */
    public void deleteScripts(int idExec) throws Exception ;
          
          
    /**
     * Get an Array of FileAttachementWrapper representing the files attachment of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idExec)throws Exception ;
        
    /**
     *  Get an Array of UrlAttachementWrapper representing the Urls attachment of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idExec)throws Exception ;
        
    /**
     *  Get an Array of all attachments (AttachementWrapper, File or Url) of the execution
     * @param idExec
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAttachs(int idExec)throws Exception ;
        
        
    /**
     * Get an array (lenth 2) of ScriptWrapper representing the pre and post script of the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public ScriptWrapper[] getExecutionScripts(int idExec) throws Exception  ;
        
    /**
     * Get the SalomeFileWrapper of the pre-script in the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public SalomeFileWrapper getPreScript(int idExec) throws Exception ;
          
    /**
     * Get the SalomeFileWrapper of the post-script in the Execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public SalomeFileWrapper getPostScript(int idExec) throws Exception ;
          
    /**
     * Get an Array of ExecutionResultWrapper representing  the execution result of the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public ExecutionResultWrapper[] getExecResults(int idExec)throws Exception ;
        
    /**
     * Get the id of the execution name in the campaign idCamp
     * @param idCamp
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idCamp, String name) throws Exception;
        
    /**
     * Get a wrapper of the execution represented by idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public ExecutionWrapper getWrapper(int idExec) throws Exception ;
        
    /**
     * Get an Array of long reprenting all date (sorted) where the execution idExec was executed
     * @param idExec
     * @return
     * @throws Exception
     */
    public long[] getAllExecDate(int idExec) throws Exception ;
        
    /**
     * Get an EnvironmentWrapper representing the environnment used by the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public EnvironmentWrapper getEnvironmentWrapper(int idExec)throws Exception ;
        
    /**
     * Get an DataSetWrapper representing the dataset used by the execution idExec
     * @param idExec
     * @return
     * @throws Exception
     */
    public DataSetWrapper getDataSetWrapper(int idExec) throws Exception ;
}
