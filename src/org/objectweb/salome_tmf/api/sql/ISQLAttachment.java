/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

public interface ISQLAttachment extends Remote {
        
    /**
     * Delete attchment idetified by idAttach in database from table ATTACHEMENT
     * @param idAttach 
     * @throws Exception
     * no permission needed
     */
    public void delete(int idAttach) throws Exception;
        
    /**
     * Update the description of an attachment (identified by idAttach) in the database 
     * @param idAttach
     * @param description : the new description
     * @throws Exception
     * no permission needed
     */
    public void updateDescription(int idAttach,  String description) throws Exception;

    /**
     * @return The last id in the table  ATTACHEMENT
     * @throws Exception
     * no permission needed
     */
    public int getLastIdAttach() throws Exception;
}
