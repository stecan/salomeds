/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultTestWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLExecutionResult extends Remote {
        
    /**
     * Insert an Execution Result in the database (table RES_EXEC_CAMP)
     * 
     * @param idExec : id of the related execution
     * @param name of the Execution Result
     * @param description of the Execution Result
     * @param status (FAIT, A_FAIRE)
     * @param result (INCOMPLETE, STOPPEE, TERMINEE)
     * @param idUser
     * @return the id of the Execution Result in the table RES_EXEC_CAMP
     * @throws Exception canExecutCamp
     * need permission 
     */
    public int insert(int idExec, String name,  String description, String status, 
            String result, int idUser) throws Exception ;
        
    /**
     * Attach an URL to the  Execution Result : idExecRes (table ENV_ATTACHEMENT)
     * 
     * @param idExecRes
     * @param url
     * @param description of the URL
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception 
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idExecRes, String url, String description) 
            throws Exception ;

    /**
     * Attach a file to the  Execution Result : idExecRes (table ENV_ATTACHEMENT)
     * 
     * @param idExecRes
     * @param file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idExecRes, SalomeFileWrapper file, 
            String description) throws Exception ;
        
    /**
     * Update the data of the  Execution Result identified by idExecRes
     * 
     * @param idResExec
     * @param description 
     * @param status (FAIT, A_FAIRE)
     * @param result (INCOMPLETE, STOPPEE, TERMINEE)
     * @param idUser
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idResExec, String description, String status, 
            String result,int idUser) throws Exception ;
        
    /**
     * replace all reference of user oldIdUser by newIdUser in the table 
     * (RES_EXEC_CAMP) where execution is idExec
     * 
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    public void updateUserRef(int idExec, int oldIdUser, int newIdUser) throws Exception ;
        
    /**
     * Delete the  Execution Result identified by idExecRes in table RES_EXEC_CAMP
     * Then delete all attachments, and related Test and Action execution result
     * 
     * @param idResExec
     * @throws Exception
     * @see ISQLExecutionTestResult.deleteAllFrom(int)
     * need permission canExecutCamp
     */
    public void delete(int idResExec)  throws Exception ;
        
    /**
     * Delete all attachments of the  Execution Result identified by idExecRes
     * 
     * @param idResExec
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idResExec) throws Exception ;
        
    /**
     * Delete an attachment idAttach of the  Execution Result identified by 
     * idExecRes
     * 
     * @param idResExec
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idResExec, int  idAttach) throws Exception ;
        
    /**
     * Get an Array of FileAttachementWrapper representing the files attachment 
     * of the Execution Result identified by idExecRes
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idResExec) throws Exception ;
        
    /**
     * Get an Array of UrlAttachementWrapper representing the URLs attachment 
     * of the Execution Result identified by idExecRes
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idResExec) throws Exception ;
         
    /**
     * Get an Array of all attachments (AttachementWrapper, File or URL) of the
     * Execution Result identified by idExecRes
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAttachs(int idResExec) throws Exception ;
         
    /**
     * Get the id of the Execution Result identified by name in the 
     * Execution idExec
     * 
     * @param idExec
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idExec, String name) throws Exception ;
         
    /**
     * @param   idResExec
     * @return  The number of test FAIL (ApiConstants.FAIL, and FAILED in the 
     *          database) in the execution result
     */
    public int getNumberOfFail(int idResExec) throws Exception;
         
    /**
     * @param   idResExec
     * @return  The number of test SUCCESS (ApiConstants.SUCCESS, and PASSED 
     *          in the database) in the execution result
     */
    public int getNumberOfPass(int idResExec) throws Exception ;

    /**
     * @param   idResExec
     * @return  The number of test INCONCLUSIF (ApiConstants.UNKNOWN, and 
     *          INCONCLUSIF in the database) in the execution result
     */
    public int getNumberOfInc(int idResExec) throws Exception ;

    /**
     * @param   idResExec
     * @return  The number of test NOTAPPLICABLE (ApiConstants.NOTAPPLICABLE, 
     *          and NOTAPPLICABLE in the database) in the execution result
     */
    public int getNumberOfNotApplicable(int idResExec) throws Exception ;
    
    /**
     * @param   idResExec
     * @return  The number of test BLOCKED (ApiConstants.BLOCKED, and BLOCKED in 
     *          the database) in the execution result
     */
    public int getNumberOfBlocked(int idResExec) throws Exception ;
    
    /**
     * @param   idResExec
     * @return  The number of test NONE (ApiConstants.NONE, and NONE in the 
     *          database) in the execution result
     */
    public int getNumberOfNone(int idResExec) throws Exception ;
    
    /**
     * Get an ExecutionResultWrapper representing the ExecutionResult 
     * identified by idResExec
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    public ExecutionResultWrapper getWrapper (int idResExec) throws Exception ;
         
    /**
     * Get an Array of ExcutionResultTestWrapper included in the 
     * ExecutionResult identified by idResExec
     * 
     * @param idResExec
     * @return
     * @throws Exception
     */
    public ExecutionResultTestWrapper[] getExecutionResultTestWrapper(int idResExec) throws Exception ;
         
    /**
     * Get the status of the execution of the tests idTest in the execution 
     * result idResExec
     * 
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    public String getTestResultInExecution(int idResExec, int idTest) throws Exception ;
         
    /**
     * Get an Array of TestAttachementWrapper
     * @param idResExec
     * @return
     * @throws Exception
     */
    public TestAttachmentWrapper[] getAllAttachTestInExecutionResult(int idResExec)throws Exception ;
         
}
