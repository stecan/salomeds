/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;

public interface ISQLEnvironment extends Remote {
    /**
     * Insert a Environment in the database (table ENVIRONNEMENT)
     * @param idProject : id of the project wich contain the environment 
     * @param name of the environment
     * @param description of the environment
     * @return id of the environment in the table ENVIRONNEMENT
     * @throws Exception
     * need permission canCreateCamp or canExecutCamp
     */
    public int insert(int idProject, String name, String description) throws Exception ;
        
    /**
     * Attach a file to the environment (table ENV_ATTACHEMENT)
     * @param idEnv
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idEnv, SalomeFileWrapper f, String description ) throws Exception ;
        
    /**
     * Attach an Url to the environment (table ENV_ATTACHEMENT)
     * @param idEnv
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idEnv, String url,  String description ) throws Exception ;
        
    /**
     * Insert a script (type ApiConstants.INIT_SCRIPT) to the environment idEnv
     * If a script already exist, this previous script is deleted 
     * @param idExec
     * @param file of the script
     * @param description : the description of the script
     * @param name : the name of the script
     * @param extention : argument 1 of the script (plug-in extention)
     * @param arg2 : argument 2 of the script (free use for plug-in)
     * @return the Id of the script
     * @throws Exception
     * no permission needed
     */
    public int addScript(int idEnv, SalomeFileWrapper file, String description,  String name, String extention, String arg2) throws Exception ;
        
        
    /**
     * Map a value for the parameter idParam in the environment idEnv (table  VALEUR_PARAM)
     * @param idEnv
     * @param idParam
     * @param value
     * @param description
     * @throws Exception
     * need permission canCreateCamp or canExecutCamp
     */
    public void addParamValue(int idEnv, int idParam, String value, String description) throws Exception ;
        
    /**
     * Update the name and the description of the environment idEnv
     * @param idEnv
     * @param name
     * @param description
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    public void update(int idEnv, String name, String description) throws Exception ;
        
    /**
     * Update a mapped value for the parameter idParam in the environment idEnv
     * @param idEnv
     * @param idParam
     * @param value : the new value
     * @param description : the new description
     * @throws Exception
     * need permission canUpdateCamp or canExecutCamp
     */
    public void updateParamValue(int idEnv, int idParam, String value, String description) throws Exception ;
        
    /**
     * Delete the environment idEnv in the Database
     * then delete mapped parameters, mapped attachments and  related execution 
     * @param idEnv
     * @throws Exception
     * @see ISQLExecution.delete(int)
     * need permission canExecutCamp
     */
    public void delete(int idEnv ) throws Exception ;
        
    /**
     * Deleted mapped reference to the parameter paramId in the environment idEnv
     * @param idEnv
     * @param paramId
     * @throws Exception
     * need permission canExecutCamp
     */
    public void deleteDefParam(int idEnv, int paramId) throws Exception ;
        
    /**
     * Delete all attchements of the environments idEnv
     * @param idEnv
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idEnv) throws Exception ;
        
    /**
     * Delete an attchement idAttach of the environments idEnv
     * @param idEnv
     * @param attachId
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idEnv, int idAttach) throws Exception ;
          
        
         
        
    /**
     * Delete the script of the environnement idEnv
     * then delete reference in SCRIPT, SCRIPT_ATTACHEMENT, ATTACHEMENT
     * @param idExec
     * @throws Exception
     * no permission needed
     */
    public void deleteScript(int idEnv)throws Exception ;

          
    /**
     * Get an Array of FileAttachementWrapper representing the files attachment of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idEnv) throws Exception ;
          
    /**
     * Get an Array of UrlAttachementWrapper representing the Urls attachment of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idEnv) throws Exception ;
          
    /**
     * Get an Array of all attachments (AttachementWrapper, File or Url) of the environment
     * @param idEnv
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAttachs(int idEnv) throws Exception ;
          
         
    /**
     * Get the SalomeFileWrapper of the script in the Environnement idEnv
     * @param idExec
     * @return
     * @throws Exception
     */
    public SalomeFileWrapper getScript(int idEnv) throws Exception ;
                
    /**
     * Get a ScriptWrapper representing the script of the Environnement
     * @param idEnv
     * @return
     * @throws Exception
     */
    public ScriptWrapper getScriptWrapper(int idEnv)throws Exception ;
                
    /**
     * Get the id of the environment name in the project idProject
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idProject, String name) throws Exception ;
          
    /**
     * Get an EnvironmentWrapper reprensting the environnement idEnv
     * @param idEnv
     * @return
     * @throws Exception
     */
    public EnvironmentWrapper getWrapper(int idEnv) throws Exception ;
          
    /**
     * Get the value of the parameter idParameter in the environnement idEnv
     * @param idEnv
     * @param idParameter
     * @return the value or null if the parameter is not use by the environement
     * @throws Exception
     */
    public String getParameterValue(int idEnv, int idParameter)  throws Exception ;
          
    /**
     * Get an Array of ValuedParameterWrapper for the environnement idEnv
     * @param idEnv
     * @return
     * @throws Exception
     */
    public ValuedParameterWrapper[] getDefinedParameters(int idEnv)  throws Exception ;
}
