package org.objectweb.salome_tmf.api.sql;

import org.objectweb.salome_tmf.api.IChangeDispatcher;
import org.objectweb.salome_tmf.api.ISafeThread;

public interface ISQLObjectFactory {
        
    public ISQLEngine getInstanceOfSQLEngine(String url, String username, String password, IChangeDispatcher pIChangeDispatcher, int pid, String driverJDBC, int lockmeth) throws Exception ;
        
    public ISQLEngine getCurrentSQLEngine();
        
    public ISafeThread getChangeListener(String projet);
        
    public void changeListenerProject(String projet);
        
    public ISQLGroup getISQLGroup();
        
    public ISQLDataset getISQLDataset();
        
    public ISQLExecutionActionResult getISQLExecutionActionResult() ;
        
    public ISQLExecutionTestResult getISQLExecutionTestResult() ;
        
    public ISQLExecutionResult getISQLExecutionResult() ;
        
    public ISQLExecution getISQLExecution() ;
        
    public ISQLEnvironment getISQLEnvironment() ;
        
    public ISQLProject getISQLProject() ;
        
    public ISQLTest getISQLTest() ;
        
    public ISQLFamily getISQLFamily() ;
        
    public ISQLPersonne getISQLPersonne() ;
        
    public ISQLScript getISQLScript();
        
    public ISQLCampaign getISQLCampaign() ;
        
    public ISQLAutomaticTest getISQLAutomaticTest() ;
        
    public ISQLTestList getISQLTestList() ;
        
    public ISQLFileAttachment getISQLFileAttachment() ;
        
    public ISQLUrlAttachment getISQLUrlAttachment() ;
        
    public ISQLAttachment getISQLAttachment() ;
        
    public ISQLParameter getISQLParameter() ;
        
    public ISQLAction getISQLAction() ;
        
    public ISQLManualTest getISQLManualTest();
        
    public ISQLSession getISQLSession();
        
    public ISQLConfig getISQLConfig();

    public ISQLSalomeLock getISQLSalomeLock();
        
    public void setConnexionInfo(int _projectID, int _personneID);
        
    public int getProjectID();
        
    public int getPersonneID();
        
    public String getSalomeVersion();
        
    public IDataBase getInstanceOfDataBase(String driver) throws Exception;
        
    public IDataBase getInstanceOfSalomeDataBase() throws Exception;
}
