/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLExecutionTestResult extends Remote {
        
    /**
     * Insert an execution test result (table EXEC_CAS) for the execution result idExecRes and test idTest
     * @param idExecRes
     * @param idTest
     * @param result ('PASSED', 'FAILED', 'INCONCLUSIF'  @see ApiConstants)     
     * @return the id of the execution test result in the table EXEC_CAS
     * @throws Exception
     * need permission canExecutCamp
     */
    public int insert(int idExecRes, int idTest, String result) throws Exception ;
        
    /**
     * Attach an url to the execution test result  identified by idExecRes and idTest(table EXEC_CAS_ATTACH)
     * @param idExecRes
     * @param idTest
     * @param url
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idExecRes, int idTest, String url, String description) throws Exception ;
        
    /**
     * Attach an url to the execution test result identified by idRestestExec (table EXEC_CAS_ATTACH)
     * @param idRestestExec
     * @param url
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idRestestExec, String url, String description) throws Exception ;
        
    /**
     * Attach a file to the execution test result identified by idExecRes and idTest (table EXEC_CAS_ATTACH)
     * @param idExecRes
     * @param idTest
     * @param file
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idExecRes, int idTest, SalomeFileWrapper file, String description) throws Exception ;
        
    /**
     * Attach a file to the execution test result identified by idRestestExec (table EXEC_CAS_ATTACH)
     * @param idRestestExec
     * @param file
     * @param description
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idRestestExec, SalomeFileWrapper file, String description) throws Exception ;

        
    /**
     * Update the result of an execution test result identified by idExecRes and idTest 
     * @param idExecRes
     * @param idTest
     * @param result
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idExecRes, int idTest, String result) throws Exception ;
        
    /**
     * Update the result of an execution test result identified by idRestestExec
     * @param idRestestExec
     * @param result
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idRestestExec, String result) throws Exception ;

        
    /**
     * Delete the referenced execution test result identified by idExecRes and idTest
     * @param idResExec
     * @param idTest
     * @throws Exception
     * @see delete(int)
     * need permission canExecutCamp
     */
    public void delete(int idResExec, int idTest) throws Exception ;
        
    /**
     * Delete all referenced execution test result for the execution result idExecRes
     * @param idExecRes
     * @throws Exception
     * @see delete(int)
     * need permission canExecutCamp
     */
    public void deleteAllFrom(int idExecRes) throws Exception ;
        
    /**
     * Delete the execution test result identified by idRestestExec
     * Then delete all the attachements and Action results.
     * @param idRestestExec
     * @throws Exception
     * @see ISQLExecutionActionResult.deleteAll(int)
     * need permission canExecutCamp
     */
    public void delete(int idRestestExec) throws Exception ;
        
    /**
     * Delete all attchements of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idRestestExec) throws Exception ;
        
    /**
     * Delete an attchement idAttach of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @param idAttach
     * @throws Exception
     */
    public void deleteAttach(int idResExec, int idTest, int  idAttach) throws Exception ;
        
    /**
     * Delete an attchement idAttach of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @param idAttach
     * @throws Exception
     */
    public void deleteAttach(int idRestestExec, int  idAttach) throws Exception ;
        
    /**
     * Get an Array of FileAttachementWrapper representing the files attachment
     * execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idResExec, int idTest) throws Exception ;
        
    /**
     * Get an Array of FileAttachementWrapper representing the files attachment 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idRestestExec) throws Exception ;
        
    /**
     * Get an Array of UrlAttachementWrapper representing the Urls attachment 
     * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idResExec, int idTest) throws Exception ;
        
    /**
     * Get an Array of UrlAttachementWrapper representing the Urls attachment 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idRestestExec) throws Exception ;
        
    /**
     * Get an Array of all attachments (AttachementWrapper, File or Url) 
     * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAttachs(int idResExec, int idTest) throws Exception ;
        
    /**
     * Get an Array of all attachments (AttachementWrapper, File or Url) 
     * of the execution test result identified by idRestestExec
     * @param idRestestExec
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAttachs(int idRestestExec) throws Exception ;
        
    /**
     * Get the id * of the execution test result identified by idResExec and idTest
     * @param idResExec
     * @param idTest
     * @return
     * @throws Exception
     */
    public int getID(int idResExec, int idTest) throws Exception ;
}
