/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLAction extends Remote {
        
    /**
     * Insert an Action in the table ACTION_TEST for the test identified by idBddTest
     * @param idBddTest
     * @param name : name of the action
     * @param description : description of the action
     * @param awaitedResult : awaited result of the action
     * @return the id of the new action
     * @throws Exception
     * need canCreateTest permission
     */
    public int insert (int idBddTest, String name, String description, String awaitedResult) throws Exception;
        
    /**
     * insert in table ACTION_PARAM_TEST the use of the parameter identifed by idBddParam for the action idBddAction
     * WARNING This methode don't insert the reference of idBddParam int the table CAS_PARAM_TEST
     * @param idBddAction
     * @param idBddParam
     * @throws Exception
     * @see 
     */
    public void addUseParam(int idBddAction, int idBddParam) throws Exception;

    /**
     * Insert a file to the action identifeid by idBddAction
     * @param idBddAction
     * @param file
     * @param description
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * no permission needed
     * @see SQLFileAttachment.insert(File, String)
     */
    public int addFileAttach(int idBddAction, SalomeFileWrapper file, String description) throws Exception ;
        
    /**
     * Insert a UrlAttachment to the action identifeid by idBddAction
     * Table used are ATTACHEMENT and ACTION_ATTACHEMENT
     * @param idBddAction
     * @param strUrl
     * @param description
     * @return the id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * no permission needed
     * @see SQLUrlAttachment.insert(String, String)
     */
    public int addUrlAttach(int idBddAction, String strUrl,  String description) throws Exception ;
        
    //////////////////////////////////////UPDATE///////////////////////////////////////////
        
        
    /**
     * Update the information of an action identified by idBddAction in database (ACTION_TEST)
     * @param idBddAction
     * @param newActionName
     * @param newActionDesc
     * @param newActionResAttendu
     * @throws Exception
     * need permission canUpdateTest
     */
    public void update(int idBddAction, String newActionName, String newActionDesc, String newActionResAttendu) throws Exception;
        
    /**
     * Increment or decrement the order of the action identified by idBddAction in the test
     * Then, reorder other action to preserve a correct order   
     * @param idBddAction
     * @param inc true for doind a decrementation (+1) or false (-1)
     * @return the new order of the action
     * @throws Exception
     * need permission canUpdateTest
     */
    public int updateOrder(int idBddAction, boolean inc) throws Exception ;
        
        
    /////////////////////////////////DELETE/////////////////////////////////////////////////////
        
    /**
     * Delete in database the action identified by idBddAction
     * this delete all Attachemnts, reference of using parameters,
     * and update the order of the actions which are referenced in the same test
     * @param idBddAction
     * @throws Exception
     * @see deleteAllAttachment(int)
     * need permission canDeleteTest
     */
    public void delete(int idBddAction) throws Exception ;
        
    /**
     * Delete in database the action identified by idBddAction
     * this delete all Attachemnts, reference of using parameters,
     * and update the order of the actions which are referenced in the same test if reorder = true
     * @param idBddAction
     * @param reorder re-order the actions in the test
     * @throws Exception
     * @see deleteAllAttachment(int)
     * need permission canDeleteTest
     * @TODO SOAP
     */
    public void delete(int idBddAction, boolean reorder) throws Exception ;
        
    /**
     * Delete the reference of the use parameter idBddParam in action, but not delete the 
     * parameter to the database
     * @param idBddAction : unique identifier of the action in database
     * @param idBddParam : unique identifier of the parameter in database
     * @throws Exception
     * need permission canDeleteTest
     */
    public void deleteParamUse(int idBddAction, int idBddParam) throws Exception ;
        
    /**
     * Delete all Attachement of an action identied by idBddAction in database
     * Delete reference in table : ATTACHEMENT and ACTION_ATTACHEMENT
     * @param idBddAction
     * @throws Exception
     */
    public void deleteAllAttachment(int idBddAction) throws Exception ;
        
    /**
     * Delete Attchement identidied by idBddAttach int ATTACHEMNT table and  reference in ACTION_ATTACHEMENT
     * @param idBddAction : unique identifier of the action in database
     * @param idBddAttach : unique identifier of the attachment in database
     * @throws Exception
     * need permission canDeleteTest
     */
    public void deleteAttachment(int idBddAction, int idBddAttach) throws Exception ;
        
    ////////////////////////////////////GET/////////////////////////////////////////
        
    /**
     * Return a wrapped action represented by idBddAction in database
     * @param idBddAction : unique identifier of the action in database
     * @return Return a wrapped action represented by idBddAction in database
     * @throws Exception
     * no permission needed 
     */
    public ActionWrapper getActionWrapper(int idBddAction) throws Exception ;
        
    /**
     * Return an Array of ParameterWrapper used by an action identified by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector of java.lang.String contening all parameters names used by this action 
     * @throws Exception
     * no permission needed
     */
    public ParameterWrapper[] getParamsUses(int idBddAction) throws Exception ;
        
    /**
     * Get all FileAttachment of an Action idntifed by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector contening all FileAttachment (wrapped by FileAttachementWrapper)  of action idBddActio
     * @throws Exception
     * @see org.objectweb.salome_tmf.api.data.FileAttachementWrapper
     * no permission needed
     */
    public FileAttachementWrapper[] getAllAttachFile(int idBddAction) throws Exception ;
        
    /**
     * Get all UrlAttachment of an Action idntifed by idBddAction
     * @param idBddAction : unique identifier of the action in database
     * @return a Vector contening all UrlAttachment (wrapped by UrlAttachementWrapper)  of action idBddAction
     * @throws Exception
     * @see org.objectweb.salome_tmf.api.data.UrlAttachementWrapper
     * no permission needed
     */
    public UrlAttachementWrapper[] getAllAttachUrl(int idBddAction) throws Exception;
        
    /**
     * Return the Id of an action called name in the test identified by testId
     * @param testId
     * @param actionName
     * @return the Id of an action called name in the test identified by testId
     * @throws Exception
     * no permission needed
     */
    public int getID(int testId, String actionName) throws Exception;
}
