/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.ExecutionActionWrapper;

public interface ISQLExecutionActionResult extends Remote {
        
    /**
     * Insert a result of the execution of the action idAction in the execution result idExecRes
     * @param idExecRes id of the ExecutionResult (table RES_EXEC_CAMP)
     * @param idTest : id of the test
     * @param idAction : id of the action
     * @param description : description of the action
     * @param awaitedRes : awaited result of the action
     * @param effectiveRes : the effective result of the action
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @return the id of the row in the table EXEC_ACTION
     * @throws Exception
     * need permission canExecutCamp
     */
    public int insert(int idExecRes, int idTest, int idAction, String description, String awaitedRes,  String effectiveRes, String result) throws Exception ;
                
                
    /**
     * Insert a result of the execution of the action idAction in the execution result idExecRes
     * @param idRestestExec : id of the ExecutionTestResult (EXEC_CAS)
     * @param idAction : id of the action
     * @param description : description of the action
     * @param awaitedRes : awaited result of the action
     * @param effectiveRes : the effective result of the action
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @return the id of the row in the table EXEC_ACTION
     * @throws Exception
     * need permission canExecutCamp
     */
    public int insert(int idRestestExec, int idAction, String description, String awaitedRes, String effectiveRes,  String result) throws Exception ;
        
        
    /**
     * Update the effective result of the execution of the action
     * @param idResActionExec
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idResActionExec, String effectiveResult, String result) throws Exception ;
        
    /**
     * Update the effective result of the execution of the action
     * @param idExecRes
     * @param idTest
     * @param idAction
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idExecRes, int idTest, int idAction, String effectiveResult, String result) throws Exception ;
                
                
    /**
     * Update the effective result of the execution of the action
     * @param idRestestExec
     * @param idAction
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idRestestExec, int idAction, String effectiveResult, String result) throws Exception ;
        
    /**
     * Update the description, the awaited result and effective result of the execution of the action
     * @param idRestestExec
     * @param idAction
     * @param description
     * @param awaitedResult
     * @param effectiveResult
     * @param result : of the action ('PASSED', 'FAILED', 'INCONCLUSIF')
     * @throws Exception
     * need permission canExecutCamp
     */
    public void update(int idRestestExec, int idAction, String description, String awaitedResult, String effectiveResult, String result) throws Exception ;
        
    /**
     * Delete result of all actions in the ExecResulTest 
     * @param idRestestExec
     * need permission canExecutCamp
     */
    public void deleteAll(int idRestestExec) throws Exception ;
        
    /**
     * Delete result of the action in the ExecResulAction identified by idResActionExec   
     * @param idResActionExec
     * @throws Exception
     * need permission canExecutCamp
     */
    public void delete(int idResActionExec) throws Exception ;
        
    /**
     * Get the id of the ExecutionActionResult name in the project idProject
     * @param idRestestExec
     * @param idAction
     * @return
     * @throws Exception
     */
    public int getID(int idRestestExec, int idAction) throws Exception ;
        
    /**
     * Get an ExecutionActionWrapper representing the row idResActionExec in the table EXEC_ACTION 
     * @param idResActionExec
     * @return
     * @throws Exception
     */
    public ExecutionActionWrapper getExecAction(int idResActionExec) throws Exception ;
}
