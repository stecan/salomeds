package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;


public interface ISQLDataset extends Remote {
    /**
     * Insert a dataset (table JEU_DONNEES) for the campaign identified by idCamp
     * @param idCamp : id of the campaign wich will caontain the dataset
     * @param name of the dataset
     * @param description of the dataset
     * @return id of the dataset created in table JEU_DONNEES
     * need permission canExecutCamp or canCreateCamp
     * @throws Exception
     */
    public int insert(int idCamp, String name, String description) throws Exception ;
        
    /**
     * Map a value for the parameter idParam in the table VALEUR_PARAM
     * @param idDataset : id of the dataset in the table JEU_DONNEES
     * @param idParam : id of the parameter in the table PARAM_TEST
     * @param value 
     * @throws Exception
     * need permission canExecutCamp
     */
    public void addParamValue(int idDataset, int idParam, String value) throws Exception ;
         
    /**
     * Update the name and the description of the dataset idDataset
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @param name
     * @param description
     * need permission canExecutCamp
     * @throws Exception
     */
    public void update(int idDataset, String name, String description) throws Exception ;
        
    /**
     * Update a value maped to the parameter idParam for the dataset idDataset
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @param idParam
     * @param value
     * @param description
     * @throws Exception
     * need permission canExecutCamp or canUpdateCamp
     */
    public void updateParamValue(int idDataset, int idParam, String value, String description) throws Exception ;
        
    /**
     * Delete the dataset and all mapped values in the database
     * if the dataset is used by executions, the executions are deleted
     * @param idDataset : id of the dataset in table JEU_DONNEES
     * @throws Exception
     * @see ISQLExecution.delete(int)
     * need permission canExecutCamp
     */
    public void delete(int idDataset) throws Exception ;
        
    /**
     * Get the Id of the dataset identified by name for the campaign idCamp
     * @param idCamp
     * @param name
     * @return id of the dataset created in table JEU_DONNEES
     * @throws Exception
     */
    public int getID(int idCamp, String name) throws Exception ;
        
    /**
     * Get A DataSetWrapper representing the dataset idDataSet in database
     * @param idDataSet
     * @return
     * @throws Exception
     */
    public DataSetWrapper getWrapper(int idDataSet) throws Exception ;
        
    /**
     * Get an Array of ValuedParameterWrapper for the he dataset idDataSet
     * @param idEnv
     * @return
     * @throws Exception
     */
    public ValuedParameterWrapper[] getDefinedParameters(int idDataSet)  throws Exception ;
}
