package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLCampaign extends Remote {
    /**
     * Inset a campaign in table CAMPAGNE_TEST 
     * @param idProject : id of the projet where to insert the campaign 
     * @param name of the campaign
     * @param description of the campaign
     * @param idPers : id of the user who insert the campaign
     * @return the id (id_camp) of new campaign
     * @throws Exception
     * need permission canCreateCamp
     */
    public int insert(int idProject, String name, String description , int idPers) throws Exception;
        
    /**
     * Import a test identified by idTest (in table CAS_TEST) in the campaign identified by idCamp
     * in table CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @return the order of the test in the campaign
     * @throws Exception
     * need permission canCreateCamp
     */
    public int importTest(int idCamp, int idTest, int userID) throws Exception ; 
        
    /**
     * Attach a file to a campaign identified by idCamp (table CAMPAGNE_ATTACHEMENT)
     * @param idCamp
     * @param file : the file to insert in the database
     * @param description 
     * @return the id of the attachement in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idCamp, SalomeFileWrapper file, String description) throws Exception ;
        
    /**
     * Attach a URL to a campaign identified by idCamp (table CAMPAGNE_ATTACHEMENT)
     * @param idCamp
     * @param url : the url to insert in the database
     * @param description
     * @return the id of the attachement in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String String)
     * no permission needed
     */
    public int addAttachUrl(int idCamp, String url, String description) throws Exception ;
        
    /**
     * Update the name and the description of the campaign identified by idCamp
     * @param idCamp of the campaign
     * @param name : new name of the campaign
     * @param description : new description of the campaign
     * @throws Exception
     * need permission canUpdateCamp
     */
    public void update(int idCamp, String name, String description) throws Exception ;
        
        
    /**
     * Update the order of the campaign by incrementation (if increment = true) or decrementation
     * @param idCamp : id of the campaign 
     * @param increment
     * @return
     * @throws Exception
     * need permission canUpdateCamp
     */
    public int updateOrder(int idCamp, boolean increment) throws Exception ;
        
    /**
     * Update test Assignation (field assigned_user_id) in CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    public void updateTestAssignation(int idCamp, int idTest, int assignedID) throws Exception ;
        
    /**
     * Update test Assignation for all reference of old assignation (field assigned_user_id) in CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    public void updateTestAssignationRef(int idCamp, int assignedOldID,int assignedNewID ) throws Exception ;
        
    /**
     * Update All test Assignation (field assigned_user_id) in CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    public void updateCampagneAssignationRef(int idCamp, int assignedNewID ) throws Exception ;
        
    /**
     * Update Suite Assignation (field assigned_user_id for all tests in the suite) in CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    public void updateSuiteAssignation(int idCamp, int idSuite, int assignedID) throws Exception ;
        
    /**
     * Update Suite Assignation (field assigned_user_id for all tests in the suite) in CAMPAGNE_CAS
     * @param idCamp
     * @param idTest
     * @param assignedID
     * @return
     * @throws Exception
     */
    public void updateFamilyAssignation(int idCamp, int idFamille, int assignedID) throws Exception ;
        
    /**
     * replace all reference of user oldIdUser by newIdUser in the table (CAMPAGNE_CAS) whrerre campagne = idCamp
     * @param oldIdUser
     * @param newIdUser
     * @throws Exception
     * no permission needed
     */
    public void updateUserRef(int idCamp , int oldIdUser, int newIdUser)  throws Exception ;
         
    /**
     * Update test suite order in the campaign (+1 if increment = true) or -1
     * @param idCamp : id of the campaign
     * @param idSuite : id of the suite in table SUITE_TEST
     * @param increment
     * @throws Exception
     * need permission canUpdateCamp
     */
    public void updateTestSuiteOrder(int idCamp, int idSuite, boolean increment) throws Exception ;
        
    /**
     * Update a family order in the campaign (+1 if increment = true) or -1
     * @param idCamp
     * @param idFamily : id of the suite in table FAMILLE_TEST
     * @param increment
     * @throws Exception
     * need permission canUpdateCamp
     */
    public void updateTestFamilyOrder(int idCamp, int idFamily, boolean increment) throws Exception;
        
    /**
     *  Update a test order in the campaign (+1 if increment = true) or -1
     * @param idCamp
     * @param idTest : id of the test in the table CAS_TEST
     * @param increment
     * @return
     * @throws Exception
     * need permission canUpdateCamp
     */
    public int updateTestOrder(int idCamp, int idTest, boolean increment) throws Exception ;
        
    /**
     * Delete a campaign and it's related links in the database
     * include : delete test in campaign, dataset, execution (and result), and attachment
     * reorder the campaign in the project
     * @param idCamp
     * @throws Exception
     * need permission canDeleteCamp
     * @see deleteTest(int,int)
     * @see deleteAllExec(int)
     * @see deleteAllAttach(int)
     * @see deleteAllDataset(int)
     * need permission canDeleteCamp
     */
    public void delete(int idCamp) throws Exception ;
        
    /**
     * Delete a campaign and it's related links in the database
     * include : delete test in campaign, dataset, execution (and result), and attachment
     * reorder the campaign in the project if reorder = true
     * @param idCamp
     * @throws Exception
     * need permission canDeleteCamp
     * @see deleteTest(int,int)
     * @see deleteAllExec(int)
     * @see deleteAllAttach(int)
     * @see deleteAllDataset(int)
     * need permission canDeleteCamp
     * @TODO SOAP
     */
    public void delete(int idCamp, boolean reorder) throws Exception ;
        
    /**
     * Delete all executions reference (and it's result) for the campaign identified by idCamp
     * @param idCamp
     * @throws Exception
     * @see ISQLExecution.delete(int)
     * need permission canExecutCamp
     */
    public void deleteAllExec(int idCamp) throws Exception ;
        
    /**
     * Delete all datasets reference for the campaign identified by idCamp
     * @param idCamp
     * @throws Exception
     * @see ISQLDataset.delete(int)
     * need permission canExecutCamp
     */
    public void deleteAllDataset(int idCamp) throws Exception ;
        
    /**
     * Delete a test reference for the campaign identified by idCamp
     * then reoder the test in the campaign, and clean result on Excecution
     * then delete execution if empty and deleteExec = true
     * @param idCamp
     * @param testId
     * @param deleteExec
     * @throws Exception
     * need permission canDeleteCamp
     */
    public void deleteTest(int idCamp, int testId, boolean deleteExec) throws Exception ;
        
                
    /**
     * Delete an attachment idAttach in the campaign (table CAMPAGNE_ATTACHEMENT and ATTACHEMENT)
     * @param idCamp
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idCamp, int idAttach) throws Exception ;
        
    /**
     * Delete all attachments in the campaign (table CAMPAGNE_ATTACHEMENT and ATTACHEMENT)
     * @param idCamp
     * @see deleteAttach(int, int)
     * @throws Exception
     */
    public void deleteAllAttach(int idCamp) throws Exception ;
        
    /**
     * Delete a test reference in all campaign and then reorder the tests in the campaign
     * @param idTest
     * @see deleteTest(int, int)
     * need permission canDeleteCamp
     * @throws Exception
     */
    public void deleteTestInAllCampaign(int idTest) throws Exception;
        
        
    /**
     * Get an Array of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the campaign identified by idCamp
     * @param idCamp : id of the campaign
     * @return
     * @throws Exception
     */
         
    public AttachementWrapper[] getAttachs(int idCamp) throws Exception ;
        
    /**
     * Get an Array of FileAttachementWrapper for the campaign identified by idCamp
     * @param idCamp : id of the campaign
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAttachFiles(int idCamp) throws Exception ;
        
    /**
     * Get an Array of UrlAttachementWrapper for the campaign identified by idCamp
     * @param idCamp : id of the campaign
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAttachUrls(int idCamp)  throws Exception ;
         
    /**
     * Return the number of campaign in the project identified by idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public int getNumberOfCampaign(int idProject) throws Exception;
        
    /**
     * Return the number of tests in the campaign identified by idCamp
     * @param idCamp
     * @return
     * @throws Exception
     */
    public int getNumberOfTestInCampaign(int idCamp)  throws Exception;
        
    /**
     * Return an Array of CampaignWrapper representing the campaign of the project identified by idProject
     * @param idProject
     * @return
     * @throws Exception
     */
    public CampaignWrapper[] getAllCampaigns(int idProject) throws Exception;
        
    /**
     * Return a CampaignWrapper representing a campaign at order 'order' in the project identified by idProject
     * @param idProject
     * @param order
     * @return
     * @throws Exception
     */
    public CampaignWrapper getCampaignByOrder(int idProject, int order) throws Exception;
        
    /**
     * Return a CampaignWrapper for the campaign identified by idCamp
     * @param idCamp
     * @return
     * @throws Exception
     */
    public CampaignWrapper getCampaign(int idCamp) throws Exception ; 
        
    /**
     * Get a TestCampWrapper representing the test at defined order in the campagne idCamp
     * @param idCamp
     * @param order
     * @return
     * @throws Exception
     */
    public TestCampWrapper getTestCampByOrder(int idCamp, int order) throws Exception ;
        
    /**
     * Get a TestCampWrapper representing the test idTest in the campagne idCamp
     * @param idCamp
     * @param idTest
     * @return
     * @throws Exception
     */
    public TestCampWrapper getTestCampById(int idCamp, int idTest) throws Exception ;
        
    /**
     * Get an ordered Array of TestCampWrapper representing test available in the campaign idCamp 
     * @param idCamp
     * @return
     * @throws Exception
     */
    public TestCampWrapper[] getTestsByOrder(int idCamp)throws Exception ;
        
    /**
     * Get the number of tests in the family idFamily in the campaign idCamp
     * @param idCamp
     * @param idFamily
     * @return
     * @throws Exception
     */
    public int getSizeOfFamilyInCampaign(int idCamp,  int idFamily) throws Exception ;
        
    /**
     * Get the number of tests in the suite idSuite in the campaign idCamp
     * @param idCamp
     * @param idSuite
     * @return
     * @throws Exception
     */
    public int getSizeOfSuiteInCampaign(int idCamp,  int idSuite) throws Exception ;

    /**
     * Get the id of the campaign identified by name in the project identified by idProject
     * @param idProject
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idProject, String name) throws Exception ;
        
    /**
     * Get an Array of DataSetWrapper used by the campaign idCamp
     * @param idCamp
     * @return
     * @throws Exception
     */
    public DataSetWrapper[] getDatsets(int idCamp) throws Exception;
        
    /**
     * Get an Array of ExecutionWrapper used by the campaign idCamp
     * @param idCamp
     * @return
     * @throws Exception
     */
    public ExecutionWrapper[] getExecutions(int idCamp) throws Exception; 
        
    /**
     * Get an Array ExecutionAttachmentWrapper
     * representing all attachement of all execution result
     * @param idCamp
     * @return
     * @throws Exception
     */
    public ExecutionAttachmentWrapper[] getResExecutionsAttachment(int idCamp) throws Exception ;
}
