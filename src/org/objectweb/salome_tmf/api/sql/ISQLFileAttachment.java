/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;
import java.sql.Date;

import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;



public interface ISQLFileAttachment extends ISQLAttachment, Remote {
    /**
     * Insert an attachment as file in the table ATTACHEMENT
     * @param f
     * @param description
     * @throws Exception
     * @return the id of the new attached file
     * no permission needed
     */
    public int insert(SalomeFileWrapper f, String description) throws Exception ;
        
    /**
     * Update the file in database table ATTACHEMENT, this include the update of content, date and length 
     * @param idBdd : id of the file in the table ATTACHEMENT
     * @param file : the salome file representing the new content
     * @throws Exception
     * no permission needed
     */
    public void updateFile(int idBdd, SalomeFileWrapper file) throws Exception ;
        
        
    /**
     * Update the file name of the attachements identified by idBdd
     * @param idBdd
     * @param name : the new name
     * @throws Exception
     * no permission needed
     */
    public void updateFileName(int idBdd, String name)throws Exception ;
        
    /**
     * Change the content of an existing file in the database
     * @param idBdd : id of the file in the table ATTACHMENT
     * @param fileContent : the new content
     * @throws Exception
     * no permission needed
     */
    public void updateFileContent(int idBdd, byte[] fileContent) throws Exception ;
        
    /**
     * Change the date of the file in the database
     * @param idBdd : id of the file in the table ATTACHMENT
     * @param date : the new date
     * @throws Exception
     * no permission needed
     */
    public void updateFileDate(int idBdd, Date date) throws Exception ;
        
    /**
     * Change the length of the file in the database
     * @param idBdd : id of the file in the table ATTACHMENT
     * @param length : the new length
     * no permission needed
     */
    public void updateFileLength(int idBdd, long length) throws Exception ;
        
    /**
     * Get the file on local disk (at path specified) for the attachment identified by idBdd 
     * @param idBdd : id of the file in the table ATTACHMENT
     * @param path
     * @return the salome file object representing localy the file
     * @throws Exception
     */
    //public SalomeFileWrapper getFile(int idBdd) throws Exception ;
        
    /**
     * Get the file on local disk for the attachment identified by idBdd
     * @param idBdd : id of the file in the table ATTACHMENT
     * @return the salome file object representing localy the file
     * no permission needed
     */
    public SalomeFileWrapper getFile(int idBdd) throws Exception ;
        
    /**
     * Get the file on local disk (at path specified) for the attachment identified by idBdd 
     * @param idBdd : id of the file in the table ATTACHMENT
     * @param file;
     * @return the salome file object representing localy the file
     * @throws Exception
     */
    public SalomeFileWrapper getFileIn(int idBdd, SalomeFileWrapper file) throws Exception ;
}
