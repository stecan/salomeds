package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;

public interface ISQLAutomaticTest extends ISQLTest, Remote {
    /**
     * Insert an Automatic test in table CAS_TEST
     * @param idTestList : the id of the testlist which contain the inserted test
     * @param name : the name of the test
     * @param description : the description of the tests
     * @param conceptor : the conceptor of the test
     * @param extension : the plug-in extension of the test
     * @return the id of the test in table CAS_TEST
     * @throws Exception
     * need permission canCreateTest
     */
    public int insert(int idTestList, String name, String description, String conceptor, String extension) throws Exception ;
        
    /**
     * Insert a Script to the test
     * @param idTest : id of the test to insert the script
     * @param file : the file of the script
     * @param description : the description of the file
     * @param name : the name of the script
     * @param arg1 : argument 1 of the script (free use for plug-in)
     * @param arg2 : argument 1 of the script (free use for plug-in)
     * @return the id of script inserted in the table SCRIPT_ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * @see ISQLScript.addAttach(int, int);
     * need permission canUpdateTest
     */
    public int addScript(int idTest, SalomeFileWrapper file, String description,  String name, String arg1, String arg2) throws Exception ;
         
    /**
     * Delete the test in the database, this include :
     * delete test script, and all reference (Parameter, Campaign) and the test attachments
     * @param idTest
     * @throws Exception
     * need permission canDeleteTest (do a special allow)
     */
    @Override
    public void delete(int idTest)  throws Exception ;
         
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test
     * @param idTest
     * @param paramId
     * @throws Exception
     * need permission  canUpdateTest
     * @see deleteUseParamRef
     */
    @Override
    public void deleteUseParam(int idTest, int paramId) throws Exception ;
         
    /**
     * Delete the script used by the tests, this include the removing of Attachement and all reference
     * @param testId
     * @throws Exception
     * need permission  canUpdateTest
     * @see ISQLScript().delete(int, int)
     */
    public void deleteScript(int testId) throws Exception;
        
    /**
     * Get a ScriptWrapper representing the script for the tests testId
     * @param testId : id of the test
     * @return
     * @throws Exception
     * @see ScriptWrapper
     */
    public ScriptWrapper getTestScript(int testId) throws Exception ;
        
    /**
     * Get the Script File atached to the script for the tests testId
     * @param testId
     * @return
     * @throws Exception
     * @see ISQLScript.getFile(int)
     * no permission needed
     */
    public SalomeFileWrapper getTestScriptFile(int testId) throws Exception ;
}
