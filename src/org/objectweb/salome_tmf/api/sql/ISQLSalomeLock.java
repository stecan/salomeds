package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.LockInfoWrapper;

public interface ISQLSalomeLock extends Remote {

        
    public LockInfoWrapper isLock(int projectID) throws Exception ;
        
    public void insert(int projectID, int personneId, int lock_code, int action_code, String info, int pid) throws Exception ;
        
    public void delete(int projectID, int pid) throws Exception ;
        
    public void delete(int projectID) throws Exception ;
        
    public LockInfoWrapper[] getAllProjectLocks(int projectID) throws Exception ; 
}
