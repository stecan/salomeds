package org.objectweb.salome_tmf.api.sql;
/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;


public interface ISQLGroup extends Remote {
    /**
     * Insert a group for the project idProject
     * @param idProject
     * @param name of the group
     * @param description of the group
     * @param perm of the group in the project
     * @return the id of the group
     * @exception
     * no permission needed
     */
    public int insert(int idProject, String name, String description, int perm) throws Exception ;
        
    /**
     * Insert an user idUser in the group idGroup
     * @param
     * @param
     * @exception
     * no permission needed
     */
    public void insertUser(int idGroup, int idUser ) throws Exception ;
        
    /**
     * delete the group idGroup and all reference about user in group
     * @param idGroup
     * @exception
     * no permission needed
     */
    public void delete(int idGroup) throws Exception ;
        
    /**
     * update the permission of the group idGroup by perm
     * @param idGroup
     * @param perm
     * @exception
     * no permission needed
     */
    public void updatePermission(int idGroup, int perm) throws Exception ;
        
    /**
     * update the name and the description of the group idGroup
     * @param idGroup
     * @param name
     * @param description
     * @exception
     * no permission needed
     */
    public void updateGroup(int idGroup, String name, String description) throws Exception ;
        
    /**
     * Update the description of idUser in the group idGroup
     * @param idGroup
     * @param idUser
     * @param description
     * @exception
     * no permission needed
     */
    public void updateUserDescInGroup(int idGroup, int idUser, String description ) throws Exception ;
        
    /**
     * Delete idUser in the group idGroup
     * @param idGroup
     * @param idUser
     * @exception
     * no permission needed
     */
    public void deleteUserInGroup(int idGroup, int idUser) throws Exception ;
        
    /**
     * Delete all reference of group in user - group mapping
     * @param idGroup
     * @throws Exception
     */
    public void deleteUserGroup(int idGroup) throws Exception ;
        
    /**
     * get the id of an group  groupName in project idProject 
     * @param idProject
     * @param groupName
     * @exception
     * no permission needed
     */
    public int getID(int idProject, String groupName)  throws Exception ;

    /**
     * Get an Array of GroupWrapper representing all groups in project idProject 
     * @param idProject
     * @exception
     * no permission needed
     */
    public GroupWrapper[] getGroupWrapperInProject(int idProject) throws Exception ;
        
    /**
     * Get an Array of GroupWrapper representing all groups where  userLogin is 
     * @param idProject
     * @param userLogin
     * @exception
     * no permission needed
     */
    public GroupWrapper[] getGroupsForUser(int idProject, String userLogin) throws Exception ;
        
    /**
     * Get an Array of UserWrapper representing all users in group idGroup
     * @param idGroup
     * @exception
     * no permission needed
     */
    public UserWrapper[] getUserWrappersInGroup(int idGroup) throws Exception ;
}
