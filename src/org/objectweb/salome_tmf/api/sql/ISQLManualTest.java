/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.ActionWrapper;

public interface ISQLManualTest extends ISQLTest, Remote{
        
        
    /**
     * Insert a Manual test in table CAS_TEST
     * @param idTestList : the id of the testlist which contain the inserted test
     * @param name : the name of the test
     * @param description : the description of the tests
     * @param conceptor : the conceptor of the test
     * @param extension : the plug-in extension of the test
     * @return the id of the test in table CAS_TEST
     * @throws Exception
     * need permission canCreateTest
     */
    public int insert(int idTestList, String name, String description, String conceptor, String extension) throws Exception ;
        
    /**
     * Delete the test in the database, this include :
     * delete test actions, and all reference (Parameter, Campaign) and the test attachments
     * @param idTest : id of the test
     * @throws Exception
     * @see ISQLAction.delete(int)
     * need permission canDeleteTest (do a special allow)
     */
    @Override
    public void delete(int idTest)  throws Exception ;
        
    /**
     * Delete reference about using parameter paramId (table CAS_PARAM_TEST) for the test and his actions
     * @param idTest : id of the test
     * @param paramId : id of the parameter
     * @throws Exception
     * need permission  canUpdateTest
     * @see deleteUseParamRef
     * @see ISQLAction.deleteParamUse(int, int)
     */
    @Override
    public void deleteUseParam(int idTest, int paramId) throws Exception ;
        
        
    /**
     * Get an Array of ActionWrapper representing all Action of the test
     * @param idTest : id of the test
     * @return
     * @throws Exception
     */
    public ActionWrapper[] getTestActions(int idTest) throws Exception ;
        
    /**
     * Get the number of action contening in the test identified by idTest
     * @param idTest
     * @return
     * @throws Exception
     */
    public int getNumberOfAction(int idTest) throws Exception ;
        
    /**
     * Get a ActionWrapper representing an action in position order for the test idTest
     * @param idTest : id of the test
     * @param order : order of the action
     * @return a ActionWrapper
     * @throws Exception
     */
    public ActionWrapper getActionByOrder(int idTest, int order) throws Exception ;
}
