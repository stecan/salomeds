package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;

import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public interface ISQLTestList extends Remote {
        
    /**
     * Insert a TestList for the family idFamily
     * @param idFamily
     * @param name of the TestList
     * @param description of the TestList
     * @return the id of the new TestList
     * @throws Exception
     * need permission canCreateTest
     */
    public int insert (int idFamily, String name, String description) throws Exception ;
        
    /**
     * Attach a file to the TestList identified by idSuite (Table SUITE_ATTACHEMENT)
     * @param idSuite
     * @param f the file
     * @param description of the file
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLFileAttachment.insert(File, String)
     * no permission needed
     */
    public int addAttachFile(int idSuite, SalomeFileWrapper f, String description) throws Exception ;
        
    /**
     * Attach an Url to the TestList identified by idSuite (Table SUITE_ATTACHEMENT)
     * @param idSuite
     * @param url
     * @param description of the url
     * @return the Id of the attachment in the table ATTACHEMENT
     * @throws Exception
     * @see ISQLUrlAttachment.insert(String, String)
     * no permission needed
     */
    public int addAttachUrl(int idSuite, String url, String description) throws Exception ;
        
    /**
     * Update the name and the description of the TestList identified by idSuite
     * @param idSuite
     * @param name
     * @param description
     * @throws Exception
     * need permission canUpdateTest
     */
    public void update(int idSuite, String name, String description)throws Exception;
        
        
        
        
    /**
     * Increment or decrement the order of the TestList identified by idSuite in the Family
     * Then, reorder other estList to preserve a correct order   
     * @param idSuite
     * @param increment  true (+1) or false (-1)
     * @return the new order
     * @throws Exception
     * no permission needed
     */
    public int updateOrder(int idSuite, boolean increment) throws Exception ;
        
    /**
     * Delete the TestList identified by idSuite in the database
     * then all attachments and all tests in the testlist, and reoder the other testlist in the family 
     * @param idSuite
     * @throws Exception
     * @see ISQLTest.delete(int)
     * need permission canDeleteTest (do a special allow)
     */
    public void delete(int idSuite) throws Exception ;

    /**
     * Delete the TestList identified by idSuite in the database
     * then all attachments and all tests in the testlist, and reoder the other testlist in the family if reorder = true
     * @param idSuite
     * @param reorder re-order testlist in the family
     * @throws Exception
     * @see ISQLTest.delete(int)
     * need permission canDeleteTest (do a special allow)
     * @TDOD SOAP
     */
    public void delete(int idSuite, boolean reorder) throws Exception;
        
    /**
     * Delete all attchements of the TestList identified by idSuite
     * @param idSuite
     * @throws Exception
     * no permission needed
     */
    public void deleteAllAttach(int idSuite) throws Exception ;
        
    /**
     * Delete an attchement idAttach of the TestList identified by idSuite
     * @param idSuite
     * @param idAttach
     * @throws Exception
     * @see ISQLAttachment.delete(int)
     * no permission needed
     */
    public void deleteAttach(int idSuite, int idAttach) throws Exception ;
        
    /**
     * Get an Array of AttachementWrapper (FileAttachementWrapper, UrlAttachementWrapper)
     * for the testlist identified by idSuite
     * @param testId : id of the test
     * @return
     * @throws Exception
     */
    public AttachementWrapper[] getAllAttachemnt(int idSuite) throws Exception ;
         
    /**
     * Get an Array of FileAttachementWrapper for the testlist identified by idSuite
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    public FileAttachementWrapper[] getAllAttachFiles(int idSuite) throws Exception ;
         
    /**
     * Get an Array of UrlAttachementWrapper for the testlist identified by idSuite
     * @param idSuite : id of the testlist
     * @return
     * @throws Exception
     */
    public UrlAttachementWrapper[] getAllAttachUrls(int idSuite) throws Exception ;
         
    /**
     * Get the id of the TestList name in the family idFamily
     * @param idFamily
     * @param name
     * @return
     * @throws Exception
     */
    public int getID(int idFamily, String name) throws Exception ;
         
    /**
     * Get the number of test in the TestList identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    public int getNumberOfTest(int idSuite) throws Exception ;
        
    /**
     * Get TestWrapper representing the test at order in the TestList identified by idSuite
     * @param idSuite
     * @param order
     * @return
     * @throws Exception
     */
    public TestWrapper getTestByOrder(int idSuite, int order) throws Exception ;
        
    /**
     * Get SuiteWrapper  representing the TestList identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    public SuiteWrapper getTestList(int idSuite) throws Exception ;
        
    /**
     * Get an Array of TestWrapper (ManualTestWrapper or AutomaticTestWrapper) 
     * representing all tests in the suite identified by idSuite
     * @param idSuite
     * @return
     * @throws Exception
     */
    public TestWrapper[] getTestsWrapper(int idSuite) throws Exception ;
}
