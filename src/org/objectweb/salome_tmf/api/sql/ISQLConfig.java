package org.objectweb.salome_tmf.api.sql;

import java.rmi.Remote;
import java.util.Hashtable;

public interface ISQLConfig extends Remote {
    public void insertSalomeConf(String key, String value) throws Exception;
        
    public void insertUserConf(String key, String value, int idUser) throws Exception;
        
    public void insertProjectConf(String key, String value, int idProject) throws Exception;
        
        
    public void updateSalomeConf(String key, String value) throws Exception;
        
    public void updateUserConf(String key, String value, int idUser) throws Exception;
        
    public void updateProjectConf(String key, String value, int idProject) throws Exception;
        
        
    public void deleteSalomeConf(String key) throws Exception;
        
    public void deleteUserConf(String key, int idUser) throws Exception;
        
    public void deleteProjectConf(String key, int idProject) throws Exception;
        
    public void deleteAllUserConf(int idUser) throws Exception;
        
    public void deleteAllProjectConf(int idProject) throws Exception;
        
    public String getSalomeConf(String key) throws Exception;
        
    public String getUserConf(String key, int idUser) throws Exception;
        
    public String getProjectConf(String key, int idProject) throws Exception;
        
    public Hashtable getAllSalomeConf() throws Exception;
        
    public Hashtable getAllUserConf(int idUser) throws Exception;
        
    public Hashtable getAllProjectConf(int idProject) throws Exception;
}
