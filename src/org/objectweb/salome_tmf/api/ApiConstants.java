package org.objectweb.salome_tmf.api;

public interface ApiConstants {
        
    final static public int LOADING = 50;
        
    // Suppression
        
    final static public int DELETE_TEST = 100;
        
    final static public int DELETE_SUITE = 101;
        
    final static public int DELETE_FAMILY = 102;
        
    final static public int DELETE_PARAMETER_FROM_TEST = 103;
        
    final static public int DELETE_ACTION = 104;
    //  -------------------------
    final static public int DELETE_CAMPAIGN = 105;
        
    final static public int DELETE_TEST_FROM_CAMPAIGN = 106;
        
    final static public int DELETE_DATA_SET = 17;
        
    final static public int DELETE_EXECUTION = 108;
        
    final static public int DELETE_EXECUTION_RESULT = 109;
    //  -------------------------
    final static public int DELETE_PARAMETER = 110;

    final static public int DELETE_ENVIRONMENT = 111;
    //  -------------------------
    final static public int DELETE_ATTACHMENT = 112;  //nolock
    //  -------------------------
    final static public int DELETE_PROJECT = 113;
        
    final static public int DELETE_GROUP = 114;
        
    final static public int DELETE_USER = 115;
        
    final static public int DELETE_USER_FROM_GROUP = 116;
        
    final static public int DELETE_SCRIPT = 117;
        
    final static public int DELETE_PARAMETER_INTO_ENV = 118;
        
    final static public int DELETE_EXECUTION_TEST_RESULT = 119;
        
    final static public int DELETE_PROJECT_CONF = 120;
    // Insertion
        
    final static public int INSERT_TEST = 200;
        
    final static public int INSERT_SUITE = 201;
        
    final static public int INSERT_FAMILY = 202;
        
    final static public int INSERT_ACTION = 203;

    final static public int INSERT_PARAMETER_INTO_TEST = 204;
        
    final static public int INSERT_PARAMETER_INTO_ACTION = 219;
    //  -------------------------
    final static public int INSERT_CAMPAIGN = 205;
        
    final static public int INSERT_TEST_INTO_CAMPAIGN = 206;
        
    final static public int INSERT_DATA_SET = 207;
        
    final static public int INSERT_ENV_INTO_EXEC = 208;
        
    final static public int INSERT_EXECUTION_RESULT = 209;
        
    final static public int INSERT_EXECUTION = 210;
        
    //  -------------------------
    final static public int INSERT_PARAMETER = 211;  //nolock
        
    final static public int INSERT_ENVIRONMENT = 212;

    final static public int INSERT_PARAMETER_INTO_ENV = 213;

    //  -------------------------

    final static public int INSERT_ATTACHMENT = 214;  //nolock
    //  -------------------------

    final static public int INSERT_USER = 215;
        
    final static public int INSERT_PROJECT = 216;
        
    final static public int INSERT_GROUP = 217;
        
    final static public int INSERT_USER_INTO_GROUP = 218;

    final static public int INSERT_SCRIPT = 219;
        
    final static public int INSERT_EXECUTION_TEST_RESULT = 220;
    // Update

    final static public int UPDATE_TEST = 300;
        
    final static public int UPDATE_SUITE = 301;
        
    final static public int UPDATE_FAMILY = 302;

    final static public int UPDATE_ACTION = 303;
        
    //  -------------------------        
    final static public int UPDATE_CAMPAIGN = 304;

    final static public int UPDATE_EXECUTION = 305;
        
    final static public int UPDATE_EXECUTION_RESULT = 306;
        
    final static public int UPDATE_DATA_SET = 307;

    //  -------------------------               
    final static public int UPDATE_PARAMETER = 308;
        
    final static public int UPDATE_ENVIRONMENT = 309;
        
    //  -------------------------
        
    final static public int UPDATE_ATTACHMENT = 310;    
    //  -------------------------
    final static public int UPDATE_PASSWORD = 311;
        
    final static public int UPDATE_USER = 312;
        
    final static public int UPDATE_PERMISSION = 313;
        
    final static public int UPDATE_PROJECT = 314;
        
    final static public int UPDATE_GROUP = 315;
        
    final static public int UPDATE_PARAMETER_INTO_ENV = 316;
        
    final static public int UPDATE_EXECUTION_TEST_RESULT = 317;
        
    //  -------------------------
        
                
    final static public int COMMON_REQ = 318;
    final static public int INSERT_LOCK = 319;
    final static public int DELETE_LOCK = 320;
    final static public int READ_LOCK = 321;
        
    final static public int COPIE_TEST = 400;
    final static public int COPIE_TEST_LIST = 401;
    final static public int COPIE_FAMILY = 402;
    final static public int COPIE_CAMPAIGN = 403;

    // BDD Constants
    //   Constantes pour les groupes pr?d?finis
    
    /**
     * Titre du groupe des administrateurs
     */
    final static public String ADMINNAME = "Administrateur";
    final static public String ADMINDESC = "Groupe des administrateurs du projet";
    
    /**
     * Titre du groupe des devleoppeurs
     */
    final static public String DEVNAME = "Developpeur";
    final static public String DEVDESC = "Groupe des developpeurs";
    
    /**
     * Titre du groupe des invites
     */
    final static public String GUESTNAME = "Invite";
    final static public String GUESTDESC = "Groupe des invites";
    
    /**
     * R?sultat d'un test ou d'une action : succes
     */
    final static public String SUCCESS = "PASSED";
    
    /**
     * R?sultat d'un test ou d'une action : ?chec
     */
    final static public String FAIL = "FAILED";
    
    /**
     * R?sultat d'un test ou d'une action : non concluant
     */
    final static public String UNKNOWN = "INCONCLUSIF";

    /**
     * Results for Dentsplysirona
     */
    final static public String NOTAPPLICABLE = "NOTAPPLI";
    final static public String BLOCKED = "BLOCKED";
    final static public String NONE = "NONE";
    
    /**
     * R?sultat de l'ex?cution : interrompue
     */
    final static public String INTERRUPT = "STOPPEE";
    
    /**
     * R?sultat de l'ex?cution : incompl?te
     */
    final static public String INCOMPLETED = "INCOMPLETE";

    /**
     * Result for an execution: empty (e.g. no results available)
     */
    final static public String EMPTY = "EMPTY";
    
    /**
     * R?sultat de l'ex?cution : termin?e
     */
    final static public String FINISHED = "TERMINEE";
    
    /**
     * Test manuel
     */
    final static public String MANUAL = "MANUAL";
    
    /**
     * Test automatique
     */
    final static public String AUTOMATIC = "AUTOMATED";
    
    final static public String TEST_SCRIPT = "TEST_SCRIPT";
    
    final static public String INIT_SCRIPT = "INIT_SCRIPT";
    
    final static public String POST_SCRIPT = "POST_SCRIPT";
    
    final static public String PRE_SCRIPT = "PRE_SCRIPT";
    
    final static public String DEFAULT_FAMILY_NAME = "Famille par defaut";
    final static public String DEFAULT_FAMILY_DESC = "Famille de test par defaut";
    
    /**
     * Valeur par defaut des noms de suites
     */
    final static public String DEFAULT_TESTLIST_NAME = "Suite par defaut";
    final static public String DEFAULT_TESTLIST_DESC = "Suite de test par defaut";
    
    final static public String DEFAULT_DATA_SET = "default dataset";
    
    final static public String ADMIN_SALOME_NAME = "AdminSalome";
    final static public String EMPTY_NAME = "empty";
    
    /**
     * Config file path
     */
    final static public String CONFIG_FILE_PATH = "/cfg/";
    final static public String CONFIG_FILE_NAME = "DB_Connexion.properties";
    final static public String LOG4J_FILE_NAME = "log4j.properties";
    
    
    final static public String PATH_TO_ADD_TO_TEMP = "salome_data";
        
}
