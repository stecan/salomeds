package org.objectweb.salome_tmf.api;

public interface IChangeDispatcher {
        
    public void notifyObservers(Object o);
        
    public void setChange();
}
