package org.objectweb.salome_tmf.api.data;

public class ExecutionAttachmentWrapper {
    ExecutionWrapper execution;
    TestCampWrapper testCamp;
    FileAttachementWrapper fileAttachment;
    UrlAttachementWrapper urlAttachment;
        
    public ExecutionWrapper getExecution() {
        return execution;
    }
    public void setExecution(ExecutionWrapper execution) {
        this.execution = execution;
    }
    public FileAttachementWrapper getFileAttachment() {
        return fileAttachment;
    }
    public void setFileAttachment(FileAttachementWrapper fileAttachment) {
        this.fileAttachment = fileAttachment;
    }
    public TestCampWrapper getTestCamp() {
        return testCamp;
    }
    public void setTestCamp(TestCampWrapper testCamp) {
        this.testCamp = testCamp;
    }
    public UrlAttachementWrapper getUrlAttachment() {
        return urlAttachment;
    }
    public void setUrlAttachment(UrlAttachementWrapper urlAttachment) {
        this.urlAttachment = urlAttachment;
    }
}
