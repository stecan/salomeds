package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

public class UserWrapper extends DataWrapper {
    /* 
     * Data from DataWrapper
     * String name;
     * String description;
     * int idBDD;
     * */
    String login;
    String prenom;
    String email;
    String tel;
    Date createDate;
    long createTime;
    String password;
        
    public Date getCreateDate() {
        return createDate;
    }
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    public long getCreateTime() {
        return createTime;
    }
    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }
        
    @Override
    public String toString(){
        return login;
    }
        
}
