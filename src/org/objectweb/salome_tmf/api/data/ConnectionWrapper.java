package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

public class ConnectionWrapper {
    int id;
    String projectConnected;
    String loginConnected;
    String hostConnected;
    Date dateConnected;
    long timeConnected;
        
    public Date getDateConnected() {
        return dateConnected;
    }
    public void setDateConnected(Date dateConnected) {
        this.dateConnected = dateConnected;
    }
    public String getHostConnected() {
        return hostConnected;
    }
    public void setHostConnected(String hostConnected) {
        this.hostConnected = hostConnected;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getLoginConnected() {
        return loginConnected;
    }
    public void setLoginConnected(String loginConnected) {
        this.loginConnected = loginConnected;
    }
    public String getProjectConnected() {
        return projectConnected;
    }
    public void setProjectConnected(String projectConnected) {
        this.projectConnected = projectConnected;
    }
    public long getTimeConnected() {
        return timeConnected;
    }
    public void setTimeConnected(long timeConnected) {
        this.timeConnected = timeConnected;
    }
}
