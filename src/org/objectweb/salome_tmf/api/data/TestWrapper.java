/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

/**
 * @author marchemi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestWrapper extends DataWrapper {
    String conceptor;
    Date creationDate;
    int idSuite;
    String type;
    
    //    TODO [RNA]
    String testExtension;
    /**
     * @return Returns the testExtension.
     */
    public String getTestExtension() {
        return testExtension;
    }
    /**
     * @param testExtension The testExtension to set.
     */
    public void setTestExtension(String testExtension) {
        this.testExtension = testExtension;
    }

        
    /**
     * @return Returns the conceptor.
     */
    public String getConceptor() {
        return conceptor;
    }
    /**
     * @param conceptor The conceptor to set.
     */
    public void setConceptor(String conceptor) {
        this.conceptor = conceptor;
    }
    /**
     * @return Returns the creationDate.
     */
    public Date getCreationDate() {
        return creationDate;
    }
    /**
     * @param creationDate The creationDate to set.
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    /**
     * @return Returns the idSuite.
     */
    public int getIdSuite() {
        return idSuite;
    }
    /**
     * @param idSuite The idSuite to set.
     */
    public void setIdSuite(int idSuite) {
        this.idSuite = idSuite;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
