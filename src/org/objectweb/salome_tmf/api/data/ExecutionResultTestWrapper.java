package org.objectweb.salome_tmf.api.data;


public class ExecutionResultTestWrapper extends DataWrapper {

    TestWrapper excutedTest;
    int idResExec;
    String status;
    ExecutionActionWrapper[] ActionResult;
        
    public TestWrapper getExcutedTest() {
        return excutedTest;
    }
    public void setExcutedTest(TestWrapper excutedTest) {
        this.excutedTest = excutedTest;
    }
    public int getIdResExec() {
        return idResExec;
    }
    public void setIdResExec(int idResExec) {
        this.idResExec = idResExec;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public ExecutionActionWrapper[] getActionResult() {
        return ActionResult;
    }
    public void setActionResult(ExecutionActionWrapper[] actionResult) {
        ActionResult = actionResult;
    }
        
}
