/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

/**
 * @author marchemi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ManualTestWrapper extends TestWrapper{
    public ManualTestWrapper() {
                
    }
    public ManualTestWrapper(TestWrapper test) {
        //From DataWrapper
        setName(test.getName());
        setDescription(test.getDescription());
        setIdBDD(test.getIdBDD());
        setOrder(test.getOrder());
        
        setConceptor(test.getConceptor());
        setCreationDate(test.getCreationDate());
        setIdSuite(test.getIdSuite());
        setType(test.getType());
    }

}
