package org.objectweb.salome_tmf.api.data;

public class ValuedParameterWrapper  extends DataWrapper{

    ParameterWrapper pParameterWrapper;
    String value;
        
    public ParameterWrapper getParameterWrapper() {
        return pParameterWrapper;
    }
    public void setParameterWrapper(ParameterWrapper parameterWrapper) {
        pParameterWrapper = parameterWrapper;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
