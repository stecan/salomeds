/**
 * SalomeFile.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package org.objectweb.salome_tmf.api.data;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;

public class SalomeFileWrapper  implements java.io.Serializable {
        
    private byte[] content;
    private long lastModified;
    private int length;
    private String name;
    private String canonicalPath;
    private String absolutePath;
        
    // Pour construire l'Url
    private String protocol;
    private String host;
    private int port;
    private String fileName;

    public SalomeFileWrapper() {
    }

    public SalomeFileWrapper(File file) throws IOException {
        if ( file == null ) {
            return;
        }
        content = new byte[(int)file.length()];
        length = (int)file.length();
        lastModified = file.lastModified();
        name = file.getName();
        canonicalPath = file.getCanonicalPath().replace(File.separatorChar, '/');
        absolutePath = file.getAbsolutePath().replace(File.separatorChar, '/');
        protocol = file.toURL().getProtocol();
        host = file.toURL().getHost();
        port = file.toURL().getPort();
        fileName = file.toURL().getFile();
                
        if ( file.exists() ) {
            FileInputStream fis = new FileInputStream(file);
            fis.read(content);
        }
    }

        
    public File toFile() {
        return toFile(null);
        /*
          File f = null;
          f = new File(absolutePath);
          f.setLastModified(lastModified);
                
          try {
          FileOutputStream fos = new FileOutputStream(f);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
                        
          // Flux du fichier stocke dans la BdD 
          bos.write(content);
                        
          //    Fermeture des flux de donnees
          bos.flush();
          bos.close();
          } catch (Exception e) {
          // TODO Auto-generated catch block
          Util.err(e);
          }
                
          return f;
        */
    }
        
    public File toFile(String filePath) {
                
        String path = filePath;
        File file = null;
                
        String tmpDir;
        Properties sys = System.getProperties();
        String fs = sys.getProperty("file.separator");
                
        if (path == null || path.equals("")){
            // Repertoire temporaire systeme
            tmpDir = sys.getProperty("java.io.tmpdir");
            tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP + fs;
        } else {
            tmpDir = path;
            if (!path.endsWith(fs)){
                tmpDir = tmpDir + fs;
            }
                        
        }
        try {
            file = new File(tmpDir + getName());
            File fPath = new File(file.getCanonicalPath().substring(0,file.getCanonicalPath().lastIndexOf(fs)+1));
            if (!fPath.exists()){
                fPath.mkdirs();
            }
            file.createNewFile();
                        
            // Flux d'ecriture sur le fichier
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fos);
                        
            // Flux du fichier stocke dans la BdD 
            bos.write(getContent());
                        
            //  Fermeture des flux de donnees
            bos.flush();
            bos.close();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Util.err(e);
        }
        return file;
    }

    /**
     * Gets the content value for this SalomeFile.
     * 
     * @return content
     */
    public byte[] getContent() {
        return content;
    }



    /**
     * Gets the inputStream value for this SalomeFile.
     * 
     * @return inputStream
     */
    public InputStream openInputStream() {
        return new ByteArrayInputStream(content);
    }


    /**
     * Gets the lastModified value for this SalomeFile.
     * 
     * @return lastModified
     */
    public long getLastModified() {
        return lastModified;
    }


    /**
     * Gets the length value for this SalomeFile.
     * 
     * @return length
     */
    public int getLength() {
        return length;
    }


    /**
     * Gets the name value for this SalomeFile.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


        
    public boolean exists() {
        return (content != null);
    }

    public String getCanonicalPath() {
        return canonicalPath;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public String getFileName() {
        return fileName;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public void setCanonicalPath(String canonicalPath) {
        this.canonicalPath = canonicalPath;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

}
