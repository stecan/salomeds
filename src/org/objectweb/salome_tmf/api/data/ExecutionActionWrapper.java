package org.objectweb.salome_tmf.api.data;

public class ExecutionActionWrapper extends DataWrapper {

	int idExecTest;
	int idAction;
	String result;
	String effectivResult;
	String awaitedResult;
	
	public String getAwaitedResult() {
		return awaitedResult;
	}
	public void setAwaitedResult(String awaitedResult) {
		this.awaitedResult = awaitedResult;
	}
	public String getEffectivResult() {
		return effectivResult;
	}
	public void setEffectivResult(String effectivResult) {
		this.effectivResult = effectivResult;
	}
	public int getIdAction() {
		return idAction;
	}
	public void setIdAction(int idAction) {
		this.idAction = idAction;
	}
	public int getIdExecTest() {
		return idExecTest;
	}
	public void setIdExecTest(int idExecTest) {
		this.idExecTest = idExecTest;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
}
