package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

public class LockInfoWrapper {
    int project_id;
    int personne_id;
    int lock_code;
    int action_code;
    int pid;
    String info;
    Date lockdate;      

    public LockInfoWrapper(int _project_id, int _personne_id, int _pid, int _lock_code, int _action_code, String _info, Date _lockdate){
        project_id = _project_id;
        personne_id = _personne_id;
        lock_code = _lock_code;
        action_code = _action_code;
        info = _info;
        pid = _pid;
        lockdate = _lockdate;
    }
        
    public int getAction_code() {
        return action_code;
    }
    public void setAction_code(int action_code) {
        this.action_code = action_code;
    }
        
    public String getInfo() {
        return info;
    }
    public void setInfo(String info) {
        this.info = info;
    }
        
    public int getLock_code() {
        return lock_code;
    }
    public void setLock_code(int lock_code) {
        this.lock_code = lock_code;
    }
    public int getPersonne_id() {
        return personne_id;
    }
    public void setPersonne_id(int personne_id) {
        this.personne_id = personne_id;
    }
    public int getProject_id() {
        return project_id;
    }
    public void setProject_id(int project_id) {
        this.project_id = project_id;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public Date getLockdate() {
        return lockdate;
    }

    public void setLockdate(Date lockdate) {
        this.lockdate = lockdate;
    }
}
