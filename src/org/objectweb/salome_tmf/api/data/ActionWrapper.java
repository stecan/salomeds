/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

/**
 * @author marchemi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ActionWrapper extends DataWrapper{
    String awaitedResult;
    int orderIndex;
    int idTest;
    /**
     * @return Returns the orderIndex.
     */
    public int getOrderIndex() {
        return orderIndex;
    }
    /**
     * @param orderIndex The orderIndex to set.
     */
    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }
    /**
     * @return Returns the waitedResult.
     */
    public String getAwaitedResult() {
        return awaitedResult;
    }
    /**
     * @param waitedResult The waitedResult to set.
     */
    public void setAwaitedResult(String awaitedResult) {
        this.awaitedResult = awaitedResult;
    }
    /**
     * @return Returns the idTest.
     */
    public int getIdTest() {
        return idTest;
    }
    /**
     * @param idTest The idTest to set.
     */
    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }
}
