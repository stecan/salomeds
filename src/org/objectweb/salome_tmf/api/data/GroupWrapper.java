package org.objectweb.salome_tmf.api.data;

public class GroupWrapper extends DataWrapper {
    int permission;
    int idProject;
        
    public int getIdProject() {
        return idProject;
    }
    public void setIdProject(int idProject) {
        this.idProject = idProject;
    }
    public int getPermission() {
        return permission;
    }
    public void setPermission(int permission) {
        this.permission = permission;
    }
}
