package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

public class ProjectWrapper extends DataWrapper{
	Date createdDate;

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public String toString(){
		return name;
	}
}
