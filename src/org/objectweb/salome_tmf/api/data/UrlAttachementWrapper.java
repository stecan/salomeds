/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;


/**
 * @author marchemi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class UrlAttachementWrapper extends AttachementWrapper{
    //  String url;
    //  String urlProtocol;
    //  String urlHost;
    //  int urlPort;
    //  String urlFile;
        
    /**
     * @return Returns the url.
     */
    //  public URL getUrl() {
    //          return url;
    //  }
    /**
     * @param url The url to set.
     */
    //  public void setUrl(String url) {
    //          this.url = url;
    //          try {
    //                  URL tmpURL = new URL(url);
    //                  
    //                  this.urlProtocol = tmpURL.getProtocol();
    //                  this.urlHost = tmpURL.getHost();
    //                  this.urlPort = tmpURL.getPort();
    //                  this.urlFile = tmpURL.getFile();
    //                  } catch (MalformedURLException e) {
    //          }
    //  }
    //  
    //  public boolean equals(Object o){
    //          if (o instanceof UrlAttachementWrapper) {
    //                  UrlAttachementWrapper toTest =  (UrlAttachementWrapper)  o;
    //                  if (idBDD > 0) {
    //                          if (idBDD ==  toTest.getIdBDD()){
    //                                  return true;
    //                          }
    //                  } else {
    //                          return name.equals(toTest.getName());
    //                  }
    //          }
    //          return false;
    //  }
    //  public String getUrlFile() {
    //          return urlFile;
    //  }
    //  public String getUrlHost() {
    //          return urlHost;
    //  }
    //  public int getUrlPort() {
    //          return urlPort;
    //  }
    //  public String getUrlProtocol() {
    //          return urlProtocol;
    //  }
}
