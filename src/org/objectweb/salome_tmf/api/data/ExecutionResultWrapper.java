/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

import java.sql.Date;


public class ExecutionResultWrapper extends DataWrapper {
    Date executionDate;
    long time;
    String tester;
    String executionStatus;
    int numberOfFail;
    int numberOfSuccess;
    int numberOfUnknow;
    int numberOfNotApplicable;
    int numberOfBlocked;
    int numberOfNone;
    
    /**
     * @return Returns the executionDate.
     */
    public Date getExecutionDate() {
        return executionDate;
    }
    /**
     * @param executionDate The executionDate to set.
     */
    public void setExecutionDate(Date executionDate) {
        this.executionDate = executionDate;
    }
    /**
     * @return Returns the executionStatus.
     */
    public String getExecutionStatus() {
        return executionStatus;
    }
    /**
     * @param executionStatus The executionStatus to set.
     */
    public void setExecutionStatus(String executionStatus) {
        this.executionStatus = executionStatus;
    }
    /**
     * @return Returns the numberOfFail.
     */
    public int getNumberOfFail() {
        return numberOfFail;
    }
    /**
     * @param numberOfFail The numberOfFail to set.
     */
    public void setNumberOfFail(int numberOfFail) {
        this.numberOfFail = numberOfFail;
    }
    /**
     * @return Returns the numberOfSuccess.
     */
    public int getNumberOfSuccess() {
        return numberOfSuccess;
    }
    /**
     * @param numberOfSuccess The numberOfSuccess to set.
     */
    public void setNumberOfSuccess(int numberOfSuccess) {
        this.numberOfSuccess = numberOfSuccess;
    }
    /**
     * @return Returns the numberOfUnknow.
     */
    public int getNumberOfUnknow() {
        return numberOfUnknow;
    }
    /**
     * @param numberOfUnknow The numberOfUnknow to set.
     */
    public void setNumberOfUnknow(int numberOfUnknow) {
        this.numberOfUnknow = numberOfUnknow;
    }

    /**
     * @return Returns the numberOfNotApplicable.
     */
    public int getNumberOfNotApplicable() {
        return numberOfNotApplicable;
    }
    /**
     * @param numberOfUnknow The numberOfUnknow to set.
     */
    public void setNumberOfNotApplicable(int numberOfNotApplicable) {
        this.numberOfNotApplicable = numberOfNotApplicable;
    }

    /**
     * @return Returns the numberOfBlocked.
     */
    public int getNumberOfBlocked() {
        return numberOfBlocked;
    }
    /**
     * @param numberOfBlocked The numberOfBlocked to set.
     */
    public void setNumberOfBlocked(int numberOfBlocked) {
        this.numberOfBlocked = numberOfBlocked;
    }

    /**
     * @return Returns the numberOfNone.
     */
    public int getNumberOfNone() {
        return numberOfNone;
    }
    /**
     * @param numberOfBlocked The numberOfBlocked to set.
     */
    public void setNumberOfNone(int numberOfNone) {
        this.numberOfNone = numberOfNone;
    }

    /**
     * @return Returns the tester.
     */
    public String getTester() {
        return tester;
    }
    /**
     * @param tester The tester to set.
     */
    public void setTester(String tester) {
        this.tester = tester;
    }
    /**
     * @return Returns the time.
     */
    public long getTime() {
        return time;
    }
    /**
     * @param time The time to set.
     */
    public void setTime(long time) {
        this.time = time;
    }
}
