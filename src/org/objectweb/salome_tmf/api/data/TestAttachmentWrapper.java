package org.objectweb.salome_tmf.api.data;

public class TestAttachmentWrapper {
    int idTest;
    FileAttachementWrapper fileAttachment;
    UrlAttachementWrapper urlAttachment;
        
    public FileAttachementWrapper getFileAttachment() {
        return fileAttachment;
    }
    public void setFileAttachment(FileAttachementWrapper fileAttachment) {
        this.fileAttachment = fileAttachment;
    }
    public int getIdTest() {
        return idTest;
    }
    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }
    public UrlAttachementWrapper getUrlAttachment() {
        return urlAttachment;
    }
    public void setUrlAttachment(UrlAttachementWrapper urlAttachment) {
        this.urlAttachment = urlAttachment;
    }
}
