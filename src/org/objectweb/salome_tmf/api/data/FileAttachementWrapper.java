/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

import java.sql.Date;


public class FileAttachementWrapper extends AttachementWrapper{
    String localisation;
    Date date;
    Long size;
   
    /**
     * @return Returns the date.
     */
    public Date getDate() {
        return date;
    }
    /**
     * @param date The date to set.
     */
    public void setDate(Date date) {
        this.date = date;
    }
    /**
     * @return Returns the localisation.
     */
    public String getLocalisation() {
        return localisation;
    }
    /**
     * @param localisation The localisation to set.
     */
    public void setLocalisation(String localisation) {
        this.localisation = localisation;
    }
    /**
     * @return Returns the size.
     */
    public Long getSize() {
        return size;
    }
    /**
     * @param size The size to set.
     */
    public void setSize(Long size) {
        this.size = size;
    }
        
    @Override
    public boolean equals(Object o){
        if (o instanceof FileAttachementWrapper) {
            FileAttachementWrapper toTest =  (FileAttachementWrapper)  o;
            if (idBDD > 0) {
                if (idBDD ==  toTest.getIdBDD()){
                    return true;
                }
            } else {
                return name.equals(toTest.getName());
            }
        }
        return false;
    }
        
}
