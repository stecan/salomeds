/*
 * Created on 6 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.api.data;

import java.sql.Date;

public class ExecutionWrapper  extends DataWrapper {
    Date lastDate;
    Date creationDate;
    int campId;
    
    /**
     * @return Returns the creationDate.
     */
    public Date getCreationDate() {
        return creationDate;
    }
    /**
     * @param creationDate The creationDate to set.
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    /**
     * @return Returns the lastDate.
     */
    public Date getLastDate() {
        return lastDate;
    }
    /**
     * @param lastDate The lastDate to set.
     */
    public void setLastDate(Date lastDate) {
        this.lastDate = lastDate;
    }
    /**
     * @return Returns the campId.
     */
    public int getCampId() {
        return campId;
    }
    /**
     * @param campId The campId to set.
     */
    public void setCampId(int campId) {
        this.campId = campId;
    }
}
