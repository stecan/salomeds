package org.objectweb.salome_tmf.miniboot;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import org.objectweb.salome_tmf.api.Util;

public class SalomeTMFBoot extends JApplet  {
        
    String tmpDir = System.getProperties().getProperty("java.io.tmpdir");
    String fs = System.getProperties().getProperty("file.separator");
    String install_path ="SalomeV3_Boot";
    URL urlSalome;
        
    String commons_jars = "commons-logging.jar, jfreechart-1.0.1.jar, log4j-1.2.6.jar, driver.jar, jcommon-1.0.0.jar, jpf-0.3.jar, pg74.216.jdbc3.jar, dom4j-1.5.jar";
    String jar_salome = "salome_tmf_data.jar,salome_tmf_api.jar,salome_tmf_ihm.jar,salome_tmf_sqlengine.jar,salome_tmf_coreplug.jar";
        
    static boolean doInit = false; 
    ClassLoader pClassLoader;
        
    static JApplet pApplet;
    static Class classSalomeTMF_AppJWS;
    static String projectName = null;
    LoadingBar pLoadingBar;
        
    @Override
    public void init() {
        if (!doInit) {
            urlSalome = getCodeBase();
            Util.debug("Code base is " + urlSalome);
            //projectName = this.getParameter("projectname");
            String installDir = chooseProject(getDocumentBase());
            if (installDir!=null){
                install_path += fs + installDir;
            }
            pClassLoader = this.getClass().getClassLoader();
            pLoadingBar = new LoadingBar("Loading Salome-TMF", "Install Resources", 13);
            pLoadingBar.show();
            try {
                if (create_salomeInstall()){
                    installRessource();
                } else {
                    updateRessource();
                }
                create_salomeInstall();
            } catch(Exception e){
                                
            }
            pLoadingBar.doTask("Launch SalomeTMF");
            if (pApplet == null){
                try {
                    classSalomeTMF_AppJWS = Class.forName("org.objectweb.salome_tmf.ihm.main.SalomeTMF_AppJWS");
                    pApplet = (JApplet) classSalomeTMF_AppJWS.newInstance();
                    Method meth_setParentApplet = classSalomeTMF_AppJWS.getMethod("setParentApplet", new Class[] {JApplet.class});
                    meth_setParentApplet.invoke(pApplet ,new Object[] {this});
                } catch(Exception e){
                                
                }
            }
            pLoadingBar.dispose();
            pApplet.init();
            doInit = true;
        } else {
            Util.debug("Reload : " + pApplet.getContentPane());
            try {
                Method meth_reloadPanel = classSalomeTMF_AppJWS.getMethod("reloadPanel", new Class[] {JApplet.class});
                meth_reloadPanel.invoke(pApplet ,new Object[] {this});
            } catch(Exception e){
                Util.err(e);
            }
            //pApplet.getContentPane().validate();
            //pApplet.getContentPane().repaint();
            //setContentPane(pApplet.getContentPane());
            validate();
            repaint();  
        }
    }
    public  String chooseProject(URL url) {
        String urlString = url.toString();
        String[] tab = urlString.split("[?=]");
        if (tab.length == 3 && tab[1].equals("project")){
            return tab[2];
        }
        return null;            
    }
        
    @Override
    public void start() {
        Util.log("[SalomeTMFBoot]->start");
        /*if (!doInit) 
          pApplet.start();*/
    } 
        
    @Override
    public void destroy() {
        Util.log("[SalomeTMFBoot]->destroy");
        /*
          if (!doInit) 
          pApplet.destroy();
        */
    }
    @Override
    public void stop() {
        Util.log("[SalomeTMFBoot]->stop");
        /*
          if (!doInit) 
          pApplet.destroy();
        */
    }
        
    /******************************************************************************************/
        
    private void addJar(URL jar){
        try {
            Class pclC = pClassLoader.getClass();
            pclC.getMethod("addLocalJar", new Class[] {URL.class}).invoke(
                                                                          pClassLoader,new Object[] {jar}
                                                                          );
        }   catch (Exception e){
            Util.err(e);
        }
    }
        
    /******************************************************************************************/
        
    void updateRessource() throws Exception {
        String[] jar_list = commons_jars.split(",");
        for (int i=0; i< jar_list.length; i++){
            File fileLocal = new File(tmpDir + fs + install_path +  fs + jar_list[i].trim());
            pLoadingBar.doTask("Update : " + fileLocal);
            addJar(fileLocal.toURL());
        }
        jar_list = jar_salome.split(",");
        for (int i=0; i< jar_list.length; i++){
            String url =  urlSalome.toString()  + jar_list[i].trim();
            URL urlRemote = new URL(url);
            File fileLocal = new File(tmpDir + fs + install_path +  fs + jar_list[i].trim());
            pLoadingBar.doTask("Update : " + fileLocal);
            if(needUpdate(urlRemote, fileLocal)){
                installFile(urlRemote, fileLocal);
            }
            addJar(fileLocal.toURL());
        }
                
                
    }
        
        
    boolean needUpdate( URL pJarURL, File localFile){
        boolean ret = false;
        if (!localFile.exists()) {
            return true;
        }
        try {
            JarInputStream jarInput = new JarInputStream(pJarURL.openStream());
            Manifest remoteManifest = jarInput.getManifest();
                        
            JarFile localeJar = new JarFile(localFile);
            Manifest localManifest = localeJar.getManifest();
                        
            if (!localManifest.equals(remoteManifest)){
                ret = true;
            } else {
                if ( localManifest.getEntries().size() == 0){
                    ret = true;  
                    Util.debug("Need to Update : " + localFile);
                } 
            }
        }catch (Exception e){
            Util.err(e);
            ret = true;
        }
        return ret;
    }
        
    void installRessource( ) throws Exception {
        /* Les Jars*/
        String[] jar_list = commons_jars.split(",");
        for (int i=0; i< jar_list.length; i++){
            String urlJar = urlSalome.toString()  + jar_list[i].trim();
            URL url = new URL(urlJar);
            try {
                File file = new File(tmpDir + fs + install_path +fs + jar_list[i].trim());
                pLoadingBar.doTask("Install : " + file);
                installFile(url, file);
                addJar(file.toURL());
            } catch(Exception e){
                                
            }
        }
                
        jar_list = jar_salome.split(",");
        for (int i=0; i< jar_list.length; i++){
            String urlJar = urlSalome.toString()  + jar_list[i].trim();
            //Util.debug("Install JAR  = " + urlJar);
            URL url = new URL(urlJar);
            File file = new File(tmpDir + fs + install_path +fs + jar_list[i].trim());
            pLoadingBar.doTask("Install : " + file);
            installFile(url, file);
            addJar(file.toURL());
        }
                
    }
        
    void installFile(URL pUrl, File f) throws Exception{
        try {
            URLConnection urlconn = pUrl.openConnection();
            InputStream is = urlconn.getInputStream();
            writeStream(is, f);
        }catch (Exception  e){
            Util.err(e);
        }
    }
        
    void writeStream( InputStream is , File f) throws Exception{
        byte[] buf = new byte [102400];
        f.createNewFile();
        FileOutputStream fos = new FileOutputStream (f); 
        int len = 0;
        while ((len = is.read (buf)) != -1){
            fos.write (buf, 0, len);
        }
        fos.close();
        is.close();
    }
        
    boolean create_salomeInstall() throws Exception{
        boolean ret = false;
        File file = new File(tmpDir + fs + install_path);
        if (!file.exists()){
            file.mkdirs();
            ret = true;
        }
        return ret;
    }
        
        
    class LoadingBar extends    JFrame  {
        private JProgressBar    progress;
        private JLabel          label1;
        private JPanel          topPanel;
                
            
        int iCtr  = 0;
        int nbTask = 0;
        public LoadingBar(String title, String label, int nbTask)
        {
            setTitle(title);
            setSize( 310, 130 );
            setResizable(false);
            setBackground( Color.gray );
            this.nbTask = nbTask;
            topPanel = new JPanel();
            topPanel.setPreferredSize( new Dimension( 310, 130 ) );
            getContentPane().add( topPanel );

            // Create a label and progress bar
            label1 = new JLabel( label );
            label1.setPreferredSize( new Dimension( 280, 24 ) );
            topPanel.add( label1 );

            progress = new JProgressBar();
            progress.setPreferredSize( new Dimension( 300, 20 ) );
            progress.setMinimum( 0 );
            progress.setMaximum( nbTask );
            progress.setValue( 0 );
            progress.setBounds( 20, 35, 260, 20 );
            topPanel.add( progress );
            //this.setLocation(300,300);
            this.setLocationRelativeTo(this.getParent()); 
            //pack();
        }

        public void doTask(String task){
            iCtr++;
            // Update the progress indicator and label
            label1.setText(task);
            Rectangle labelRect = label1.getBounds();
            labelRect.x = 0;
            labelRect.y = 0;
            label1.paintImmediately( labelRect );

            progress.setValue( iCtr );
            Rectangle progressRect = progress.getBounds();
            progressRect.x = 0;
            progressRect.y = 0;
            progress.paintImmediately( progressRect );
        }
                
        /*public synchronized void show(){
          super.show();
          }*/
                
        public void finishAllTask(){
            dispose();
        }
    }
}
