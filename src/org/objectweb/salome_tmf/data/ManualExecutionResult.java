/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.util.HashMap;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionActionResult;

public class ManualExecutionResult extends ExecutionTestResult {
        
    static ISQLExecutionActionResult pISQLExecutionActionResult = null;
        
    protected HashMap actionsMap;
    protected HashMap effectivResultMap;
    protected HashMap descriptionResultMap;
    protected HashMap awaitedResultMap;
        
    public ManualExecutionResult(ManualTest pTest) {
        super(pTest);
        actionsMap  = new HashMap();
        effectivResultMap  = new HashMap();
        descriptionResultMap = new HashMap();
        awaitedResultMap = new HashMap();
        if (pISQLExecutionActionResult == null){
            pISQLExecutionActionResult = Api.getISQLObjectFactory().getISQLExecutionActionResult();
        }
    }
    public ManualExecutionResult(ManualTest pTest, String status) {
        super(pTest, status);
        actionsMap  = new HashMap();
        effectivResultMap  = new HashMap();
        descriptionResultMap = new HashMap();
        awaitedResultMap = new HashMap();
        if (pISQLExecutionActionResult == null){
            pISQLExecutionActionResult = Api.getISQLObjectFactory().getISQLExecutionActionResult();
        }
    }
                
    public void setActionsMapInModel(HashMap map) {
        actionsMap = map;
    }
        
    public void setEffectivResultMapInModel(HashMap map) {
        effectivResultMap = map;
    }
        
    public void setAwaitedResultMapInModel(HashMap map) {
        awaitedResultMap = map;
    }
        
    public void setDescriptionResultMapInModel(HashMap map) {
        descriptionResultMap = map;
    }
        
    public void addStatusForActionInModel(Action action, String status) {
        actionsMap.put(action, status);
    }
        
    public void addEffectivResultInModel(Action act, String result) {
        effectivResultMap.put(act, result);
    }
        
    public void addDescriptionResultInModel(Action act, String descr) {
        descriptionResultMap.put(act, descr);
    }
        
    public void addAwaitedResultInModel(Action act, String result) {
        awaitedResultMap.put(act, result);
    }
        
        
    public void removeActionStatusInModel(Action action) {
        actionsMap.remove(action);
    }
        
    public void removeEffectivResultInModel(Action act) {
        effectivResultMap.remove(act);
    }
        
    public void removeDescriptionResultInModel(Action act) {
        descriptionResultMap.remove(act);
    }
        
    public void removeAwaitedResultInModel(Action act) {
        awaitedResultMap.remove(act);
    }
        
        
    public HashMap getActionsMapInModel() {
        return actionsMap;
    }
        
    public HashMap getEffectivResultMapFromModel() {
        return effectivResultMap;
    }
        
    public HashMap getAwaitedResultMapFromModel() {
        return awaitedResultMap;
    }
        
    public HashMap getDescriptionResultMapFromModel() {
        return descriptionResultMap;
    }
        
    public String getEffectivResultFromModel(Action act) {
        return (String)effectivResultMap.get(act);
    }
        
    public String getDescriptionResultFromModel(Action act) {
        return (String)descriptionResultMap.get(act);
    }
        
    public String getAwaitedResultFromModel(Action act) {
        return (String)awaitedResultMap.get(act);
    }
        
    public String getActionStatusInModel(Action action) {
        String result = (String)actionsMap.get(action);
        if (result == null) {
            return ""; 
        }
        return result;
    }
        
        
    int addActionResultInDB(Action action) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        int id = pISQLExecutionActionResult.insert(idBdd, action.getIdBdd(), 
                                                   getDescriptionResultFromModel(action), getAwaitedResultFromModel(action),  getEffectivResultFromModel(action), getActionStatusInModel(action));
        return id;
    }
        
    public void updateActionEffectiveResInDB(Action action) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        pISQLExecutionActionResult.update(idBdd, action.getIdBdd(), getEffectivResultFromModel(action) , getActionStatusInModel(action));
    }
        
    public void updateActionResInDB(Action action) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        pISQLExecutionActionResult.update(idBdd, action.getIdBdd(), getDescriptionResultFromModel(action) , getAwaitedResultFromModel(action) ,getEffectivResultFromModel(action) , getActionStatusInModel(action));
    }
        
    @Override
    void deleteInModel() {
        test = null;
        actionsMap.clear();
        effectivResultMap.clear();
        descriptionResultMap.clear();
        awaitedResultMap.clear();
        clearAttachInModel();
    }
        
    @Override
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
}
