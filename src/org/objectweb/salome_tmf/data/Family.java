/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLFamily;

public class Family extends WithAttachment
{
    transient static ISQLFamily pISQLFamily = null;
        
    protected Project pProject = null;
    protected ArrayList suiteList;
    protected int order = -1;
        
    public Family(String name, String description) {
        super(name, description);
        suiteList = new ArrayList();
        if (pISQLFamily == null){
            pISQLFamily = Api.getISQLObjectFactory().getISQLFamily();
        }
    }
        
        
    public Family(FamilyWrapper pFamily) {
        super(pFamily.getName(), pFamily.getDescription());
        idBdd = pFamily.getIdBDD();
        suiteList = new ArrayList();
        order = pFamily.getOrder();
        if (pISQLFamily == null){
            pISQLFamily = Api.getISQLObjectFactory().getISQLFamily();
        }
    } 
        
    public Family(Project pProject, FamilyWrapper pFamily) {
        this(pFamily);
        this.pProject = pProject;
    } 
        
    public void reloadBaseFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Family " + name + " is not in BDD");
        }
        FamilyWrapper pFamilyWrapper = pISQLFamily.getFamily(idBdd);
        name =  pFamilyWrapper.getName();
        description = pFamilyWrapper.getDescription();  
        order = pFamilyWrapper.getOrder();
    }
        
        
    public void reloadFromDB()  throws Exception {
        reloadBaseFromDB();
        reloadAttachmentDataFromDB(false);
        reloadTestListFromDB();
    }
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
        
    public ArrayList getSuiteListFromModel() {
        return suiteList;
    }
        
        

    /***************************** Basic Operation *********************************/
        
    /* Used By Project */
    void addInDB(int idProject) throws Exception {
        if (isInBase()) {
            throw new Exception("Family " + name + " is already in BDD");
        }
        idBdd = pISQLFamily.insert(idProject, name, description);
        order = pISQLFamily.getFamily(idBdd).getOrder();
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_FAMILY ,this);
    }
        
    /* Used By Project */
    void addInModel(Project pProject) {
        this.pProject = pProject;
    }
        
    /* Used By Project */
    void addInDBAndModel(Project pProject) throws Exception {
        addInDB(pProject.getIdBdd());
        addInModel(pProject);
    }
        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Family " + name + " is not in BDD");
        }
        pISQLFamily.update(idBdd, newName, newDesc);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_FAMILY ,this, new String(name), newName);
    }
        
    public void updateInModel(String newName, String newDesc){
        name = newName;
        description = newDesc; 
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }
        
    public void updateOrderInBddAndModel(boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Family " + name + " is not in BDD");
        }
        order = pISQLFamily.updateOrder(idBdd, inc);
    }
        
    /* Used By Project */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Family " + name + " is not in BDD");
        }
        pISQLFamily.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_FAMILY ,this);
    }
        
    /* Used By Project */
    void deleteInModel() {
        pProject = null;
        for (int i=0; i < suiteList.size(); i++) {
            TestList pTestList = (TestList)suiteList.get(i);
            pTestList.deleteInModel();  
            Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_SUITE ,pTestList);
        }
        suiteList.clear();
    }
        
    void deleteInDBModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
    /************************** Suite/TestList **********************************/
        
    public void addTestListInModel(TestList list) {
        list.addInModel(this);
        suiteList.add(list);
    }
        
    public void addTestListInDB(TestList list) throws Exception{
        list.addInDB(idBdd);
    }
        
    public void addTestListInDBAndModel(TestList list) throws Exception{
        addTestListInDB(list);
        addTestListInModel(list);
    }
        
    public void deleteTestListInModel(TestList list){
        list.deleteInModel();
        suiteList.remove(list); 
    }
        
    public void deleteTestListInDB(TestList list) throws Exception{
        list.deleteInDB();
    }
        
    public void deleteTestListInDBAndModel(TestList list) throws Exception{
        deleteTestListInDB(list);
        deleteTestListInModel(list);
    }
        
        
        
    public TestList getTestListInModel(String testListName) {
        for (int i=0; i < suiteList.size(); i++) {
            if (((TestList)suiteList.get(i)).getNameFromModel().equals(testListName)) {
                return (TestList)suiteList.get(i);
            }
        }
        return null;
    } // Fin de la methode getTestList/1
        
        
    public TestList getTestListFromModel(int id) {
        for (int i=0; i < suiteList.size(); i++) {
            if (((TestList)suiteList.get(i)).getIdBdd() == id) {
                return (TestList)suiteList.get(i);
            }
        }
        return null;
    }
        
        
    public boolean isContainTestFromModel(int id){
        boolean res = false;
        for (int i=0; i < suiteList.size(); i++) {
            TestList pTestList = (TestList)suiteList.get(i);
            if (pTestList.getTestFromModel(id) != null){
                return true;
            }
        }
        return res;
    }
        
    public boolean isContainTestInModel(Test pTest){
        return isContainTestFromModel(pTest.getIdBdd());
    }
        
    public void reloadTestListFromDB()throws Exception{
        for (int i=0; i < suiteList.size(); i++) {
            ((TestList)suiteList.get(i)).deleteInModel();               
        }
        suiteList.clear();
        Vector testListVector = getSuitesWrapperFromDB();
        for (int j = 0; j < testListVector.size(); j++) {
            SuiteWrapper testListBdd = (SuiteWrapper) testListVector.get(j);
            TestList testList = new TestList(this, testListBdd);
            testList.reloadFromDB();
            addTestListInModel(testList);
        }
    }
        
    public Vector getSuitesWrapperFromDB() throws Exception{
        if (!isInBase()) {
            throw new Exception("Family " + name + " is not in BDD");
        }
        SuiteWrapper[] tmpArray = pISQLFamily.getTestList(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    /************************** ATTACHEMENTS **********************/
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }       
        File f = file.getLocalFile();
        int id = pISQLFamily.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        int id = pISQLFamily.addAttachUrl(idBdd, url.getNameFromModel(),url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
        
    @Override
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLFamily.deleteAttach(idBdd,attachId);           
    }
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach) throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLFamily.getAllAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLFamily.getAllAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    ////////////////////////////////////////////////////////////////////////////////
    public static boolean isInBase(Project pProject, Family family) {
        return isInBase(pProject, family.getNameFromModel()) ;
    } // Fin de la methode isInBase/1
        
    public static boolean isInBase(Project pProject, String familyName) {
        try  {
            int id = pISQLFamily.getID(pProject.getIdBdd(), familyName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLFamily.getID(pProject.getIdBdd(), name) == idBdd;
    }
        
    /**** COPIER/COLER ************/
    static Family getModelCopie(Family toCopie)throws Exception {
        Family pCopie = new Family(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
        SuiteWrapper[] allTestList = pISQLFamily.getTestList(toCopie.getIdBdd());
        int size = allTestList.length;
        for (int i = 0; i < size ; i++){
            SuiteWrapper pSuiteWrapper = allTestList[i];
            TestList copie = TestList.getModelCopie(new TestList(pSuiteWrapper));
            pCopie.addTestListInModel(copie);
        }
        return pCopie;
    }
        
    public static Family copieIn(Family toCopie) throws Exception {
        Family pCopie = new Family(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
        String familyName = toCopie.getNameFromModel();
        int i = 0;
        while (Family.isInBase(DataLoader.getCurrentProject(), familyName)){
            familyName = toCopie.getNameFromModel() + "_" + i;
            i++;
        }
        pCopie.updateInModel(familyName, toCopie.getDescriptionFromModel());
        DataLoader.getCurrentProject().addFamilyInDBAndModel(pCopie);
                
        SuiteWrapper[] allTestList = pISQLFamily.getTestList(toCopie.getIdBdd());
        int size = allTestList.length;
        for (i = 0; i < size ; i++){
            SuiteWrapper pSuiteWrapper = allTestList[i];
            TestList copie = TestList.copieIn(new TestList(pSuiteWrapper), pCopie);
            //pCopie.addTestListInModel(copie);
        }

        try {
            /* Ajout des attachements */
            HashMap<String,Attachment> attachs = toCopie.getAttachmentMapFromModel();
            for (Attachment attach : attachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pCopie.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pCopie.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }
        }catch (Exception e){
            //Util.err(e);
        }
        Vector<Family> argNotifier = new Vector<Family> ();
        argNotifier.add(toCopie);
        argNotifier.add(pCopie);
        Project.pCurrentProject.notifyChanged( ApiConstants.COPIE_FAMILY ,argNotifier);
                
        return pCopie;
    }
        
    /*************************************************************************/
        
    public void triTestListInModel(){
        Collections.sort(suiteList, new ComparateurTestList());
    }
        
    class ComparateurTestList implements Comparator {
        @Override
        public int compare(Object poTest1, Object poTest2){
            TestList pTest1 = (TestList) poTest1;
            TestList pTest2 = (TestList) poTest2;
            /*
              Util.debug(" ORDER1 = " + pTest1.getNameFromModel() + " " + pTest1.order);
              Util.debug(" ORDER2 = " + pTest2.getNameFromModel() + " " + pTest2.order);
            */
            if (pTest1.order > pTest2.order ) {
                return 1;
            } else  {
                return -1;              
            } 
        }      
    }
}
