/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLParameter;



public class Parameter extends SimpleData {
        
    static ISQLParameter pISQLParameter = null;
        
    public Parameter(String name, String description) {
        super(name, description);
        if (pISQLParameter == null){
            pISQLParameter = Api.getISQLObjectFactory().getISQLParameter();
        }
    }
        
    public Parameter(ParameterWrapper pParameterWrapper){
        super(pParameterWrapper.getName(), pParameterWrapper.getDescription());
        idBdd = pParameterWrapper.getIdBDD();
        if (pISQLParameter == null){
            pISQLParameter = Api.getISQLObjectFactory().getISQLParameter();
        }
    }
        
    @Override
    public void  clearCache(){
        /* NOTHING */
    }
        
    public void reloadFromDB() throws Exception {
        if (isInBase()) {
            throw new Exception("Parameter " + name + " is already in BDD");
        }
        ParameterWrapper pParameterWrapper = pISQLParameter.getParameterWrapper(idBdd);
        name = pParameterWrapper.getName();
        description = pParameterWrapper.getDescription();
    }
        
    @Override
    public boolean equals (Object o) {
        Parameter testedParameter;
        boolean test = false;
        if (!(o  instanceof Parameter)) {
            return  false;
        }
        testedParameter  = (Parameter) o;
        if (isInBase() && testedParameter.isInBase()){
            test = idBdd == testedParameter.idBdd;
        } else {
            test = name.equals(testedParameter.getNameFromModel());
        }
        return test;
    }
        
    void addInDB(int IdProject) throws Exception {
        if (isInBase()) {
            throw new Exception("Parameter " + name + " is already in BDD");
        }
        idBdd = pISQLParameter.insert(IdProject, name, description);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_PARAMETER ,this);
    }
        
    public void updateDescriptionInDB(String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Parameter " + name + " is not in BDD");
        }
        pISQLParameter.updateDescription(idBdd, newDesc);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_PARAMETER ,this);
    }
        
    /*public void updateDescriptionInModel(String newDesc) {
      description = newDesc;    
      }*/
        
    public void updateNameInModel(String name) {
        this.name = name;       
    }
        
        
    public void updateDescriptionInDBAndModel(String newDesc) throws Exception {
        //BDD
        updateDescriptionInDB(newDesc);
        
        //Model
        updateDescriptionInModel(newDesc);
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateDescriptionInDB(newDesc);
        updateDescriptionInModel(newDesc);
    }
        
    void deleteInDB() throws Exception {
        pISQLParameter.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_PARAMETER ,this);
    }
        
    public static boolean isInBase(Project pProject, String paramName) {
        try  {
            int id = pISQLParameter.selectID(pProject.getIdBdd(), paramName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLParameter.getParameterWrapper(idBdd) != null;
        //return pISQLParameter.selectID(DataLoader.getCurrentProject().getIdBdd(), name) == idBdd;
    }
}
