/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.util.HashMap;
import java.util.Hashtable;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLDataset;

public class DataSet extends SimpleData{
    static ISQLDataset pISQLDataset = null;
        
    protected HashMap parametersHashMap;
        
    public DataSet(String name, String decription){
        super(name, decription);
        parametersHashMap = new HashMap();
        if (pISQLDataset == null){
            pISQLDataset = Api.getISQLObjectFactory().getISQLDataset();
        }
    }
        
    public DataSet(DataSetWrapper pDataSet) {
        super(pDataSet.getName(), pDataSet.getDescription());
        idBdd = pDataSet.getIdBDD();
        parametersHashMap = new HashMap();
        if (pISQLDataset == null){
            pISQLDataset = Api.getISQLObjectFactory().getISQLDataset();
        }
    } 
        

    public void reloadBaseFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        DataSetWrapper pDataSet = pISQLDataset.getWrapper(idBdd);
        name = pDataSet.getName();
        description = pDataSet.getDescription();
    }
        
    public void reloadFromDB(boolean base, Hashtable paramsInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        if (base){
            reloadBaseFromDB();
        }
        reloadValuedParameters(paramsInModel);
                
    }
        
    @Override
    public void  clearCache(){
        /* NOTHING */
    }
    /***************************** Basic Operation *********************************/
        
    /* call from Campaign */
    void addInDB(int idCamp) throws Exception {
        if (isInBase()) {
            throw new Exception("Dataset " + name + " is already in BDD");
        }
        idBdd = pISQLDataset.insert(idCamp,name, description);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_DATA_SET ,this);
    }
        
    /* call from Campaign */
    void addInModel(Campaign pCampaign) {
        //pCampaign.addDataSet(this);
    }
        
    void addInDBAndModel(Campaign pCampaign) throws Exception {
        addInDB(pCampaign.getIdBdd());
        addInModel(pCampaign);
    }
        
        
        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        pISQLDataset.update(idBdd, newName, newDesc);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_DATA_SET ,this, new String(name), newName);
    }
        
    public void updateInModel(String newName, String newDesc)  {
        name = newName;
        description = newDesc;
    }
        
    /*public void updateInBddAndModel(String newName, String newDesc) throws Exception {
      updateInDB(newName, newDesc);
      updateInModel(newName, newDesc);
      }*/
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }
        
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        pISQLDataset.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_DATA_SET ,this);
    }
        
    /* call from Campaign */
    void deleteInModel(){
        parametersHashMap.clear();
    }
        
    /* call from Campaign */
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
    /****************************** Parameters ********************************/
        
    public void reloadValuedParameters(Hashtable paramsInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        parametersHashMap.clear();
        ValuedParameterWrapper[] definedParams = pISQLDataset.getDefinedParameters(idBdd);
        for (int i = 0 ; i < definedParams.length ; i ++) {
            Parameter param = null;
            ValuedParameterWrapper pValuedParameterWrapper = definedParams[i];
            if (paramsInModel == null){
                param = new Parameter(pValuedParameterWrapper.getParameterWrapper());
            } else {
                param = (Parameter) paramsInModel.get(pValuedParameterWrapper.getParameterWrapper().getName());
                if (param == null){
                    param = new Parameter(pValuedParameterWrapper.getParameterWrapper());
                    paramsInModel.put(pValuedParameterWrapper.getParameterWrapper().getName(), param);
                }
            }
            addParameterValueInModel(param.getNameFromModel(), pValuedParameterWrapper.getValue());
        }
    }
        
    public void addParameterValueInModel(String paramName, String value) {
        parametersHashMap.put(paramName, value);
    } 
    //public void addParamValue2DB(String paramName, String value) throws Exception {
    void addParameterValueInDB(String value, Parameter param) throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        pISQLDataset.addParamValue(idBdd, param.getIdBdd(),value);
    }
        
    public void addParamValueToDBAndModel(String value, Parameter param) throws Exception {
        addParameterValueInDB(value, param);
        addParameterValueInModel(param.getNameFromModel(), value);
    }
        
    void updateParamValueInDB(Parameter param, String value, String desc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Dataset " + name + " is not in BDD");
        }
        pISQLDataset.updateParamValue(idBdd,param.getIdBdd(), value, desc);
    }
        
        
    public void updateParamValueInModel(String paramName, String value)  {
        removeParameterInModel(paramName);
        addParameterValueInModel(paramName, value);
    }
        
    public void updateParamValueInDBAndModel(Parameter param, String value, String desc) throws Exception {
        updateParamValueInDB(param, value, desc);
        updateParamValueInModel(param.getNameFromModel(), value);
    }
        
        
    public void removeParameterInModel(String paramName) {
        parametersHashMap.remove(paramName); 
    } 
        
    public HashMap getParametersHashMapFromModel() {
        return parametersHashMap;
    }
        
    public String getParameterValueFromModel(String paramName) {
        return (String)parametersHashMap.get(paramName);
    }
        
    public void setParametersHashMapInModel(HashMap map) {
        parametersHashMap = map;
    }
        
    public static boolean isInBase(Campaign pCamp, String datasetName) {
        try  {
            int id = pISQLDataset.getID(pCamp.getIdBdd(), datasetName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLDataset.getWrapper(idBdd) != null;
    }
        
}
