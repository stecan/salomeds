/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ExecutionResultTestWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionActionWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionActionResult;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionResult;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionTestResult;

public class ExecutionResult extends WithAttachment {


    static ISQLExecutionResult pISQLExecutionResult = null;
    static ISQLExecutionTestResult pISQLExecutionTestResult = null;
    static ISQLExecutionActionResult pISQLExecutionActionResult = null;

    protected String status;
    protected Date executionDate;
    protected Time time;
    protected String testerLogin;
    protected HashMap testsResultMap;
    protected int[] statTab;
    protected Execution pExecution;

    private boolean resultLoaded = false;

    public ExecutionResult(String name, String description, Execution pExecution) {
        super(name,description );
        Util.log("[ExecutionResult] -------------> NEW ExecutionResult : " + name);
        testsResultMap = new HashMap();
        statTab = new int[6];
        statTab[0] = 0;
        statTab[1] = 0;
        statTab[2] = 0;
        statTab[3] = 0;
        statTab[4] = 0;
        statTab[5] = 0;
        this.pExecution = pExecution;
        if (pISQLExecutionResult == null){
            pISQLExecutionResult = Api.getISQLObjectFactory().getISQLExecutionResult();
        }
        if (pISQLExecutionTestResult == null){
            pISQLExecutionTestResult = Api.getISQLObjectFactory().getISQLExecutionTestResult();
        }
        if (pISQLExecutionActionResult == null){
            pISQLExecutionActionResult = Api.getISQLObjectFactory().getISQLExecutionActionResult();
        }
        //actionsMap  = new HashMap();
        //effectivResultMap  = new HashMap();
        //descriptionResultMap = new HashMap();
        //awaitedResultMap = new HashMap();
        resultLoaded = true; // true because new ExcetionResult not in DB
    }

    public ExecutionResult(ExecutionResultWrapper pExecRes, Execution pExecution) {
        super(pExecRes.getName(), pExecRes.getDescription());
        Util.log("[ExecutionResult] -------------> NEW ExecutionResult : " + pExecRes.getName());
        idBdd = pExecRes.getIdBDD();
        statTab = new int[6];
        statTab[0] = pExecRes.getNumberOfSuccess();
        statTab[1] = pExecRes.getNumberOfFail();
        statTab[2] = pExecRes.getNumberOfUnknow();
        statTab[3] = pExecRes.getNumberOfNotApplicable();
        statTab[4] = pExecRes.getNumberOfBlocked();
        statTab[5] = pExecRes.getNumberOfNone();
        testerLogin     = pExecRes.getTester();
        time = new Time(pExecRes.getTime());
        executionDate = pExecRes.getExecutionDate();
        status = pExecRes.getExecutionStatus();
        testsResultMap = new HashMap();
        this.pExecution = pExecution;

        if (pISQLExecutionResult == null){
            pISQLExecutionResult = Api.getISQLObjectFactory().getISQLExecutionResult();
        }
        if (pISQLExecutionTestResult == null){
            pISQLExecutionTestResult = Api.getISQLObjectFactory().getISQLExecutionTestResult();
        }
        if (pISQLExecutionActionResult == null){
            pISQLExecutionActionResult = Api.getISQLObjectFactory().getISQLExecutionActionResult();
        }

        //actionsMap  = new HashMap();
        //effectivResultMap  = new HashMap();
        //descriptionResultMap = new HashMap();
        //awaitedResultMap = new HashMap();
    }


    public ExecutionResult cloneInDB(boolean onlySucces, User pUser)  throws Exception {
        int transNuber = -1;
        ExecutionResult pExecutionResult = new ExecutionResult("re-" + name, "", pExecution);
        pExecutionResult.setExecutionDateInModel(new Date(Calendar.getInstance().getTimeInMillis()));
        pExecutionResult.setTesterInModel(pUser.getLastNameFromModel() + " " + pUser.getFirstNameFromModel());

        try {
            transNuber = Api.beginTransaction(10, ApiConstants.INSERT_EXECUTION_RESULT);


            pExecutionResult.addInDB(pExecution, pUser);
            if (onlySucces){
                pExecutionResult.setExecutionStatusInModel(ApiConstants.INCOMPLETED);
            }else {
                pExecutionResult.setExecutionStatusInModel(status);
            }

            int nbNotSucces = 0 ;
            int nbInconclusif = 0;
            int nbSucces = 0;
            int nbFail = 0;
            int nbNone = 0;
            int nbNotApplicable = 0;
            int nbBlocked = 0;

            loadTestReult();
            ArrayList testInCampaign = pExecution.getCampagneFromModel().getTestListFromModel();
            ExecutionResultTestWrapper[] listOfResExecTest = pISQLExecutionResult.getExecutionResultTestWrapper(idBdd);
            for (int i = 0; i < listOfResExecTest.length; i++) {
                ExecutionResultTestWrapper pResTest = listOfResExecTest[i];
                int orderInCamp =  getTestInModel(pResTest.getExcutedTest().getIdBDD(),testInCampaign);
                if (orderInCamp == -1){
                    // AIE QUE FAIRE
                    throw new Exception("[ExecutionResult->cloneInDB] No test in the campaign model are given for "+ pResTest.getExcutedTest().getIdBDD() + ", name =" + pResTest.getExcutedTest().getName() );
                }else {
                    Test pTest = (Test) testInCampaign.get(orderInCamp);
                    String status = pResTest.getStatus();
                    ExecutionTestResult pExecutionTestResult = null;
                    if (!status.equals(ApiConstants.SUCCESS) ){
                        nbNotSucces ++;
                        if (onlySucces){
                            status = "";
                        } else if (status.equals(ApiConstants.FAIL)){
                            nbFail ++;
                        } else if (status.equals(ApiConstants.UNKNOWN)){
                            nbInconclusif ++;
                        } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                            nbNotApplicable ++;
                        } else if (status.equals(ApiConstants.BLOCKED)){
                            nbBlocked ++;
                        } else if (status.equals(ApiConstants.NONE)){
                            nbNone ++;
                        }
                    } else {
                        nbSucces ++;
                    }
                    pExecutionTestResult = pExecutionResult.initTestResultStatusInModel(pTest, status, orderInCamp, null);
                    pExecutionResult.addExecTestResultInDB(pTest);

                    if (pExecutionTestResult instanceof ManualExecutionResult) {
                        //ExecutionActionWrapper pExecutionActionWrapper = pResTest.getActionResult();
                        ExecutionActionWrapper[] tmpArray = pResTest.getActionResult();
                        Vector actionsResult = new Vector();
                        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
                            actionsResult.add(tmpArray[tmpI]);
                        }
                        ArrayList actionListInModel = ((ManualTest)pTest).getActionListFromModel(false);
                        for (int n = 0; n < actionListInModel.size(); n++){
                            Action pAction = (Action)actionListInModel.get(n);
                            if (!status.equals(ApiConstants.SUCCESS) && onlySucces){
                                pExecutionResult.initActionStatus(pAction, (ManualExecutionResult) pExecutionTestResult, actionsResult);
                            } else {
                                pExecutionResult.setActionStatus(pAction, (ManualExecutionResult) pExecutionTestResult, actionsResult);
                            }
                            pExecutionResult.addExecActionResultInDB(pTest, pAction);
                        }
                    }
                    //Attachement
                    if ((!status.equals(ApiConstants.SUCCESS) && !onlySucces) || status.equals(ApiConstants.SUCCESS)){
                        Collection col = getExecutionTestResultFromModel(pTest).getAttachmentMapFromModel().values();
                        for (Iterator iterator = col.iterator(); iterator.hasNext();) {
                            Attachment attachToTest = (Attachment)iterator.next();
                            if (attachToTest instanceof UrlAttachment ){
                                UrlAttachment pUrlAttachment = new UrlAttachment(attachToTest.name, attachToTest.description);
                                pUrlAttachment.setUrl(((UrlAttachment)attachToTest).getUrl());
                                pExecutionResult.addAttachUrlForExecTestInDB(pTest.getIdBdd(), pUrlAttachment);
                            } else {
                                File f =  ((FileAttachment)attachToTest).getFileFromDB();
                                FileAttachment pFileAttachment = new FileAttachment(f, attachToTest.description) ;
                                pExecutionResult.addAttachFileForExecTestInDB(pTest.getIdBdd(), pFileAttachment);
                            }
                        }
                    }
                }
            }

            //Attachement
            Collection col = getAttachmentMapFromModel().values();
            for (Iterator iterAttach = col.iterator(); iterAttach.hasNext();) {
                Attachment attachToExecRes = (Attachment)iterAttach.next();
                if (attachToExecRes instanceof UrlAttachment ){
                    UrlAttachment pUrlAttachment = new UrlAttachment(attachToExecRes.name, attachToExecRes.description);
                    pUrlAttachment.setUrl(((UrlAttachment)attachToExecRes).getUrl());
                    pExecutionResult.addAttachementInDB(pUrlAttachment);
                } else {
                    File f =  ((FileAttachment)attachToExecRes).getFileFromDB();
                    FileAttachment pFileAttachment = new FileAttachment(f, attachToExecRes.description) ;
                    pExecutionResult.addAttachementInDB(pFileAttachment);
                }
            }

            //Si aucun test en echec et status == FAIT --> changer le status
            if (nbNotSucces == 0){
                pExecutionResult.setExecutionStatusInModel(ApiConstants.FINISHED);
            }
            pExecutionResult.updateInDB(pExecution, pUser);
            pExecutionResult.setNumberOfSuccessInModel(nbSucces);
            if (!onlySucces){
                pExecutionResult.setNumberOfFailInModel(nbFail);
                pExecutionResult.setNumberOfUnknowInModel(nbInconclusif);
                pExecutionResult.setNumberOfNotApplicableInModel(nbNotApplicable);
                pExecutionResult.setNumberOfBlockedInModel(nbBlocked);
            }
            pExecution.addExecutionResultInModel(pExecutionResult);
            Api.commitTrans(transNuber);
        } catch(Exception e){
            Api.forceRollBackTrans(transNuber);
        }
        return pExecutionResult;
    }

    protected void reloadBaseFromDB() throws Exception{
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        ExecutionResultWrapper pExecRes = pISQLExecutionResult.getWrapper(idBdd);
        name = pExecRes.getName();
        description = pExecRes.getDescription();
        statTab[0] = pExecRes.getNumberOfSuccess();
        statTab[1] = pExecRes.getNumberOfFail();
        statTab[2] = pExecRes.getNumberOfUnknow();
        statTab[3] = pExecRes.getNumberOfNotApplicable();
        statTab[4] = pExecRes.getNumberOfBlocked();
        statTab[5] = pExecRes.getNumberOfNone();
        testerLogin     = pExecRes.getTester();
        time = new Time(pExecRes.getTime());
        executionDate = pExecRes.getExecutionDate();
        status = pExecRes.getExecutionStatus();
    }

    public void reloadFromDB(boolean base) throws Exception{
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(10, ApiConstants.LOADING);
            if (base){
                reloadBaseFromDB();
            }
            if (!DataLoader.isDynamic){
                reloadTestResult();
            }
            reloadAttachmentDataFromDB(false);
            Api.commitTrans(transNuber);
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            Util.err(e);
        }
    }

    int getTestInModel (int id, ArrayList testInCampaign){
        for (int i = 0; i < testInCampaign.size(); i++) {
            Test testOfCampaignForStatus = (Test) testInCampaign.get(i);
            if (testOfCampaignForStatus.getIdBdd() == id){
                return i;
            }
        }
        return -1;
    }

    void setActionStatus(Action pAction, ManualExecutionResult pManualExecutionResult, Vector actionsResult ){
        for (int i = 0; i< actionsResult.size(); i++ ){
            ExecutionActionWrapper pExecutionActionWrapper = (ExecutionActionWrapper) actionsResult.elementAt(i);
            if (pExecutionActionWrapper.getIdAction() == pAction.getIdBdd()){
                pManualExecutionResult.addStatusForActionInModel(pAction, pExecutionActionWrapper.getResult());
                pManualExecutionResult.addEffectivResultInModel(pAction,pExecutionActionWrapper.getEffectivResult());
                pManualExecutionResult.addDescriptionResultInModel(pAction,pExecutionActionWrapper.getDescription());
                pManualExecutionResult.addAwaitedResultInModel(pAction,pExecutionActionWrapper.getAwaitedResult());
            }
        }
    }

    void initActionStatus(Action pAction, ManualExecutionResult pManualExecutionResult, Vector actionsResult ){
        for (int i = 0; i< actionsResult.size(); i++ ){
            ExecutionActionWrapper pExecutionActionWrapper = (ExecutionActionWrapper) actionsResult.elementAt(i);
            if (pExecutionActionWrapper.getIdAction() == pAction.getIdBdd()){
                pManualExecutionResult.addStatusForActionInModel(pAction, "");
                pManualExecutionResult.addEffectivResultInModel(pAction, "");
                pManualExecutionResult.addDescriptionResultInModel(pAction,pExecutionActionWrapper.getDescription());
                pManualExecutionResult.addAwaitedResultInModel(pAction,pExecutionActionWrapper.getAwaitedResult());
            }
        }
    }

    /*public void initStatus(){
      statTab[0] = 0;
      statTab[1] = 0;
      statTab[2] = 0;
      }*/

    protected void resetTestResult(){
        if (testsResultMap != null){
            Collection testResults = testsResultMap.values();
            for (Iterator iter = testResults.iterator(); iter.hasNext(); ) {
                ExecutionTestResult testRes = (ExecutionTestResult)iter.next();
                testRes.deleteInModel();
            }
            testsResultMap.clear();
            statTab[0] = 0;
            statTab[1] = 0;
            statTab[2] = 0;
            statTab[3] = 0;
            statTab[4] = 0;
            statTab[5] = 0;
        }
        resultLoaded = false;
    }

    public void reloadTestResult() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        if (pExecution == null){
            throw new Exception("[ExecutionResult->reloadTestResult] No test in the campaign are given");
        }

        /*Collection testResults = testsResultMap.values();
          for (Iterator iter = testResults.iterator(); iter.hasNext(); ) {
          ExecutionTestResult testRes = (ExecutionTestResult)iter.next();
          testRes.deleteInModel();
          }
          testsResultMap.clear();*/
        resetTestResult();
        resultLoaded = true;


        /*
          statTab[0] = 0;
          statTab[1] = 0;
          statTab[2] = 0;
        */
        int nbSuc = 0;
        int nbFail = 0;
        int nbUnk = 0;
        int nbNone = 0;
        int nbNotApplicable = 0;
        int nbBlocked = 0;
        ArrayList testInCampaign = pExecution.getCampagneFromModel().getTestListFromModel();
        ExecutionResultTestWrapper[] listOfResExecTest = pISQLExecutionResult.getExecutionResultTestWrapper(idBdd);
        for (int i = 0; i < listOfResExecTest.length; i++) {
            ExecutionResultTestWrapper pResTest = listOfResExecTest[i];
            int orderInCamp =  getTestInModel(pResTest.getExcutedTest().getIdBDD(),testInCampaign);
            if (orderInCamp == -1){
                // AIE QUE FAIRE
                throw new Exception("[ExecutionResult->reloadTestResult] No test in the campaign model are given for "+ pResTest.getExcutedTest().getIdBDD() + ", name =" + pResTest.getExcutedTest().getName() );
            }else {
                Test pTest = (Test) testInCampaign.get(orderInCamp);
                String status = pResTest.getStatus();
                if (status.equals(ApiConstants.SUCCESS)){
                    //addSuccessInModel(1);
                    nbSuc ++;
                } else if (status.equals(ApiConstants.FAIL)){
                    //addFailInModel(1);
                    nbFail ++;
                } else if (status.equals(ApiConstants.UNKNOWN)){
                    //addUnknowInModel(1);
                    nbUnk ++;
                } else if (status.equals(ApiConstants.NOTAPPLICABLE)){
                    //addUnknowInModel(1);
                    nbNotApplicable ++;
                } else if (status.equals(ApiConstants.BLOCKED)){
                    //addUnknowInModel(1);
                    nbBlocked ++;
                } else if (status.equals(ApiConstants.NONE)){
                    //addUnknowInModel(1);
                    nbNone ++;
                } else {
                    status = "";
                }
                ExecutionTestResult pExecutionTestResult = initTestResultStatusInModel(pTest, status, orderInCamp, null);
                pExecutionTestResult.setIdBdd(pResTest.getIdBDD());
                if (pExecutionTestResult instanceof ManualExecutionResult) {
                    //ExecutionActionWrapper pExecutionActionWrapper = pResTest.getActionResult();
                    ExecutionActionWrapper[] tmpArray = pResTest.getActionResult();
                    Vector actionsResult = new Vector();
                    for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
                        actionsResult.add(tmpArray[tmpI]);
                    }
                    ArrayList actionListInModel = ((ManualTest)pTest).getActionListFromModel(false);
                    for (int n = 0; n < actionListInModel.size(); n++){
                        Action pAction = (Action)actionListInModel.get(n);
                        setActionStatus(pAction, (ManualExecutionResult) pExecutionTestResult, actionsResult);
                    }
                }
                pExecutionTestResult.reloadAttachmentDataFromDB(false);
                statTab[0] = 0;
                statTab[1] = 0;
                statTab[2] = 0;
                statTab[3] = 0;
                statTab[4] = 0;
                statTab[5] = 0;
                addSuccessInModel(nbSuc);
                addFailInModel(nbFail);
                addUnknowInModel(nbUnk);
                addNotApplicableInModel(nbNotApplicable);
                addBlockedInModel(nbBlocked);
                addNoneInModel(nbNone);
            }

        }
    }
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
    protected void loadTestReult(){
        if (resultLoaded){
            correctOrder(false);
            return;
        }
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(10, ApiConstants.LOADING);
            if (!resultLoaded){
                Util.log("[ExecutionResult->loadTestReult] for "+ name);
                reloadTestResult();
            }
            resultLoaded = true;
            Api.commitTrans(transNuber);
        } catch(Exception e){
            Api.forceRollBackTrans(transNuber);
        }
        correctOrder(true);
    }



    private void correctOrder(boolean inBase){
        ArrayList<ExecutionTestResult> testResList = new ArrayList<ExecutionTestResult>();
        Collection pExecutionTestResultList = testsResultMap.values();
        for (Iterator iter = pExecutionTestResultList.iterator(); iter.hasNext(); ) {
            ExecutionTestResult pExecutionTestResult  =   (ExecutionTestResult)iter.next();
            testResList.add(pExecutionTestResult);
        }
        Collections.sort(testResList, new ComparateurTestExec());
        int size = testResList.size();
        for (int i = 0 ; i < size ; i++){
            ExecutionTestResult pExecutionTestResult  = testResList.get(i);
            pExecutionTestResult.setOrderInModel(i);
            if (inBase){
                /*@TODO*/
            }
        }
    }

    class ComparateurTestExec implements Comparator {
        @Override
        public int compare(Object poTestRes1, Object poTestRes2){
            ExecutionTestResult pExecutionTestResult1 = (ExecutionTestResult) poTestRes1;
            ExecutionTestResult pExecutionTestResult12 = (ExecutionTestResult) poTestRes2;
            if (pExecutionTestResult1.getOderFromModel() > pExecutionTestResult12.getOderFromModel() ) {
                return 1;
            } else  {
                return -1;
            }
        }
    }

    public Execution getExecution(){
        return pExecution;
    }

    public Date getExecutionDateFromModel() {
        return executionDate;
    }

    public HashMap getTestsResultMapFromModel() {
        loadTestReult();
        return testsResultMap;
    }

    public String getExecutionStatusFromModel() {
        return status;
    }

    public String getTesterFromModel() {
        return testerLogin;
    }

    public void setExecutionDateInModel(Date date) {
        executionDate = date;
    }

    public void setExecutionStatusInModel(String string) {
        status = string;
    }

    public void setTesterInModel(String user) {
        testerLogin = user;
    }


    protected void setNumberOfSuccessInModel(int nb) {
        statTab[0] = nb;
    }

    protected void addSuccessInModel(int nb) {
        statTab[0] = statTab[0] + nb;
    }
    /*public void addSuccessInModel2(int nb) {

      }*/


    protected void setNumberOfFailInModel(int nb) {
        statTab[1] = nb;
    }

    protected void addFailInModel(int nb) {
        statTab[1] = statTab[1] + nb;
    }
    /*public void addFailInModel2(int nb) {

      }*/

    protected void setNumberOfUnknowInModel(int nb) {
        statTab[2] = nb;
    }

    protected void addUnknowInModel(int nb) {
        statTab[2] = statTab[2] + nb;
    }

    protected void setNumberOfNotApplicableInModel(int nb) {
        statTab[3] = nb;
    }

    protected void addNotApplicableInModel(int nb) {
        statTab[3] = statTab[3] + nb;
    }

    protected void setNumberOfBlockedInModel(int nb) {
        statTab[4] = nb;
    }

    protected void addBlockedInModel(int nb) {
        statTab[4] = statTab[4] + nb;
    }

    protected void setNumberOfNoneInModel(int nb) {
        statTab[5] = nb;
    }

    protected void addNoneInModel(int nb) {
        statTab[5] = statTab[5] + nb;
    }
    /*public void addUnknowInModel2(int nb) {

      }*/
    public int[] getStatFromModel() {
        return statTab;
    }

    public int getNbOfTestWithStat(){
        return statTab[0] + statTab[1] + statTab[2] + statTab[3] + statTab[4];
    }

    public String getFormatedStatFromModel(String initialPass, String initialFail, 
            String initilalIncon, String initialNotAppli, String initialBlocked) {
        int nbTest = pExecution.getCampagneFromModel().getTestListFromModel().size();
        //int nbTest = testsResultMap.size() ;
        //reloadTestResult();
        String stat = "[" + statTab[0] + initialPass+ ", " + statTab[1] + 
                initialFail + ", " + statTab[2] + initilalIncon + ", " + statTab[3] + 
                initialNotAppli +", " + statTab[4] + initialBlocked + "] / " + nbTest + " tests";
        return stat;
        //return "[" + statTab[0] + initialPass+ ", " + statTab[1] + initialFail +", " + statTab[2] + initilalIncon + "] / " + nbTest + " tests";
    }

    public Time getTimeFromModel() {
        return time;
    }

    public void setTimeInModel(Time time) {
        this.time = time;
    }

    ExecutionTestResult makeExecutionTestResult(Test test, String status){
        ExecutionTestResult executionTestResult;
        if (test  instanceof ManualTest) {
            executionTestResult = new ManualExecutionResult((ManualTest)test, status);
        } else {
            executionTestResult = new ExecutionTestResult(test, status);
        }
        return executionTestResult;
    }

    /**
     * Ajoute un statut pour un test. Si le test est deja dans la table, son
     * statut est remplace.
     * @param test un test
     * @param status son statut (SUCCESS, FAIL, UNKNOWN)
     */
    public ExecutionTestResult initTestResultStatusInModel(Test test, String status, int i, Campaign c) {
        loadTestReult();

        ExecutionTestResult executionTestResult = makeExecutionTestResult(test, status);

        int order;
        if (c != null) {
            order = c.getTestOrderInModel(test);
        } else {
            order = i;
        }
        Util.log("[ExecutionTestResult->initTestResultStatusInModel]" +executionTestResult.getTestFromModel().getNameFromModel() + " set order " + order);
        executionTestResult.setOrderInModel(order);
        testsResultMap.put(test, executionTestResult);
        return executionTestResult;

    }

    /**
     * Ajoute un statut pour un test. Si le test est deja dans la table, son
     * statut est remplace.
     * @param test un test
     * @param status son statut (SUCCESS, FAIL, UNKNOWN)
     */
    public ExecutionTestResult addTestResultStatusInModel(Test test, String status) {
        loadTestReult();

        ExecutionTestResult pExecutionTestResult;
        if (testsResultMap.get(test) == null) {
            //ExecutionTestResult
            pExecutionTestResult = makeExecutionTestResult(test, status);
            testsResultMap.put(test, pExecutionTestResult);
        } else {
            pExecutionTestResult = (ExecutionTestResult)testsResultMap.get(test);
            pExecutionTestResult.setStatusInModel(status);
        }
        return pExecutionTestResult;
    }


    /**
     * Ajoute un statut pour un test. Si le test est deja dans la table, son
     * statut est remplace.
     * @param test un test
     * @param execTestResult un objet r?sultat d'ex?cution
     */
    public void setTestExecutionTestResultInModel(Test test, ExecutionTestResult execTestResult, int i) {
        loadTestReult();

        testsResultMap.put(test, execTestResult);
        execTestResult.setOrderInModel(i);
        //Util.log("!!-->!! " + test.getNameFromModel() + " set order " + i);
    }

    /*public ExecutionTestResult getExecutionTestResultFromModel(Test test) {
      if (!resultLoaded){
      loadTestReult();
      }
      return (ExecutionTestResult)testsResultMap.get(test);
      }*/

    public ExecutionTestResult getExecutionTestResultFromModel(Test test) {
        loadTestReult();
        ExecutionTestResult executionTestResult = (ExecutionTestResult)testsResultMap.get(test);
        return executionTestResult;
    }

    /**
     * Retourne le statut du test
     * @param test
     * @return
     */
    public String getTestResultStatusFromModel(Test test) {
        loadTestReult();
        return ((ExecutionTestResult)testsResultMap.get(test)).getStatusFromModel();
    }


    public boolean allTestsHaveStatusInModel() {
        boolean resul = true;
        /*if (!resultLoaded){
          loadTestReult();
          }*/
        Set keysSet = testsResultMap.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext(); ) {
            Test test = (Test)iter.next();
            if (getTestResultStatusFromModel(test) == null || getTestResultStatusFromModel(test).equals("")) {
                //resul = false;
                return false;
            }
            /*} else if (getTestResultStatusFromModel(test).equals(ApiConstants.SUCCESS)) {
              addSuccessInModel(1);
              } else if (getTestResultStatusFromModel(test).equals(ApiConstants.FAIL)) {
              addFailInModel(1);
              } else if (getTestResultStatusFromModel(test).equals(ApiConstants.UNKNOWN)) {
              addUnknowInModel(1);
              }*/
        }
        return resul;
    }


    public void addTestResultAttachMapInModel(Test test, HashMap attachMap) {
        loadTestReult();

        ExecutionTestResult execTestResult = (ExecutionTestResult)testsResultMap.get(test);
        execTestResult.setAttachmentMapInModel(attachMap);
    }

    public Test[] getTestOrderedFromModel(){
        loadTestReult();

        Test[] ptabTest = new Test[testsResultMap.size()];
        Collection pExecutionTestResultList = testsResultMap.values();
        for (Iterator iter = pExecutionTestResultList.iterator(); iter.hasNext(); ) {
            ExecutionTestResult pExecutionTestResult  =   (ExecutionTestResult)iter.next();
            //Util.log(pExecutionTestResult.getTestFromModel().getNameFromModel() + " order " + pExecutionTestResult.getOderFromModel());
            ptabTest[pExecutionTestResult.getOderFromModel()] = pExecutionTestResult.getTestFromModel();
        }
        return ptabTest;
    }


    /*public void setTestsResultMap(HashMap map) {
      testsResultMap = map;
      }*/

    public void removeTestResult(Test test) {
        loadTestReult();

        ExecutionTestResult testExec = (ExecutionTestResult)testsResultMap.get(test);
        if (testExec == null){
            return;
        }
        int order = testExec.getOderFromModel();

        Collection testResults = testsResultMap.values();
        for (Iterator iter = testResults.iterator(); iter.hasNext(); ) {
            ExecutionTestResult testRes = (ExecutionTestResult)iter.next();
            if (testRes.getOderFromModel() > order)
                testRes.setOrderInModel(testRes.getOderFromModel() - 1);
        }

        String status = testExec.getStatusFromModel();
        if (status.equals(ApiConstants.SUCCESS)) {
            statTab[0]--;
        } else if (status.equals(ApiConstants.FAIL)) {
            statTab[1]--;
        } else if (status.equals(ApiConstants.UNKNOWN)) {
            statTab[2]--;
        } else if (status.equals(ApiConstants.NOTAPPLICABLE)) {
            statTab[3]--;
        } else if (status.equals(ApiConstants.BLOCKED)) {
            statTab[4]--;
        } else if (status.equals(ApiConstants.NONE)) {
            statTab[5]--;
        }

        testsResultMap.remove(test);
    }


    /*public Attachment getTestResultAttachment(Test test, String attachName) {
      ExecutionTestResult execTestResult = (ExecutionTestResult)testsResultMap.get(test);
      return execTestResult.getAttachmentFromModel(attachName);
      }*/






    void addInDB(Execution execCamp, User pUser) throws Exception {
        if (isInBase()) {
            throw new Exception("ExecutionResult " + name + " already in BDD");
        }
        if (execCamp.getIdBdd() == -1){
            throw new Exception("Execution " + name + " is not in BDD");
        }
        if (pUser.getIdBdd() == -1){
            throw new Exception("User " + name + " is not in BDD");
        }
        getUniqueName(execCamp.getIdBdd());
        idBdd = pISQLExecutionResult.insert(execCamp.getIdBdd(), name, description, "FAIT", status,  pUser.getIdBdd() );
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_EXECUTION_RESULT ,this);
    }

    public void updateInDB(Execution execCamp, User pUser) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        if (pUser.getIdBdd() == -1){
            throw new Exception("User " + name + " is not in BDD");
        }
        pISQLExecutionResult.update(idBdd, description, "FAIT", status, pUser.getIdBdd());
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_EXECUTION_RESULT ,this);
    }


    public void addExecTestResultInDB(Test test) throws Exception {
        getExecutionTestResultFromModel(test).addInDB(idBdd, test.getIdBdd());
    }

    public void updateExecTestResultInDB(Test test) throws Exception {
        getExecutionTestResultFromModel(test).updateInDB();
    }

    public void addExecActionResultInDB(Test test,Action action) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        ((ManualExecutionResult)getExecutionTestResultFromModel(test)).addActionResultInDB(action);
    }

    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        throw new Exception("NOT IMPLEMENTED");
    }

    /*public void updateEffectiveResForActionInDB(int testId, Action action, String effective) throws Exception {
      if (!isInBase()) {
      throw new Exception("ExecutionResult " + name + " is not in BDD");
      }
      pISQLExecutionActionResult.update(idBdd, testId, action.getIdBdd(), effective);
      }*/

    public void updateEffectiveResForActionInDB(Test test, Action action) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        ((ManualExecutionResult)getExecutionTestResultFromModel(test)).updateActionEffectiveResInDB(action);
    }


    /* call from Execution */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        pISQLExecutionResult.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION_RESULT ,this);

    }
    /* call from Execution */
    void deleteInModel()  {
        clearAttachInModel();
        Collection testResults = testsResultMap.values();
        for (Iterator iter = testResults.iterator(); iter.hasNext(); ) {
            ExecutionTestResult testRes = (ExecutionTestResult)iter.next();
            testRes.deleteInModel();
            Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION_TEST_RESULT ,testRes);
        }
        testsResultMap.clear();
    }
    /* call from Execution */
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }





    ///////////////////// Attachement ///////////////////////

    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }

    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLExecutionResult.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }

    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        int id = pISQLExecutionResult.addAttachUrl(idBdd, url.getNameFromModel(), url.getDescriptionFromModel());
        url.setIdBdd(id);
    }


    public void addAttachFileForExecTestInDB(Test test, FileAttachment file) throws Exception {
        getExecutionTestResultFromModel(test).addAttachFileInDB(file);
    }

    public void addAttachFileForExecTestInDB(int idTest, FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLExecutionTestResult.addAttachFile(idBdd, idTest, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }


    public void addAttachUrlForExecTestInDB(Test test, UrlAttachment url) throws Exception {
        getExecutionTestResultFromModel(test).addAttachUrlInDB(url);
    }

    public void addAttachUrlForExecTestInDB(int idTest, UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        int id = pISQLExecutionTestResult.addAttachUrl(idBdd, idTest, url.getNameFromModel(), url.getDescriptionFromModel());;
        url.setIdBdd(id);
    }

    @Override
    public void deleteAttachementInDB(int idAttach) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        pISQLExecutionResult.deleteAttach(idBdd, idAttach);

    }

    @Override
    public void deleteAttachementInDBAndModel(Attachment attach) throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }


    public void deleteAttachFromExecResultInDB(int idTest, int idAttach) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        pISQLExecutionTestResult.deleteAttach(idBdd, idTest, idAttach );
    }

    public void deleteAttachFromExecResultInModel(Test test, Attachment attach) {
        getExecutionTestResultFromModel(test).deleteAttachmentInModel(attach);
    }

    public void deleteAttachFromExecResultInDBModel(Test test, Attachment attach) throws Exception {
        deleteAttachFromExecResultInDB(test.getIdBdd(), attach.getIdBdd());
        deleteAttachFromExecResultInModel(test, attach);
    }


    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLExecutionResult.getAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLExecutionResult.getAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public Vector getAttachFilesExecResultFromDB(int testId) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLExecutionTestResult.getAttachFiles(idBdd, testId);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public Vector getAttachUrlsExecResultFromDB(int testId) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLExecutionTestResult.getAttachUrls(idBdd, testId);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public Vector getAllResExecTestAttachFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionResult " + name + " is not in BDD");
        }
        Vector res = new Vector();
        TestAttachmentWrapper[] tmpRes = pISQLExecutionResult.getAllAttachTestInExecutionResult(idBdd);
        for (int i =0 ; i < tmpRes.length; i++){
            Vector data = new Vector();
            TestAttachmentWrapper tmpdata = tmpRes[i];
            data.add(new Integer(tmpdata.getIdTest()));
            if(tmpdata.getFileAttachment() != null) {
                data.add(new FileAttachment(tmpdata.getFileAttachment()));
            }else if(tmpdata.getUrlAttachment() != null) {
                data.add(new UrlAttachment(tmpdata.getUrlAttachment()));
            }
            res.add(data);
        }
        return res;
    }

    /********************************************************************************/
    public void getUniqueName(int idExec) {
        int nb = 0;
        try{
            while (pISQLExecutionResult.getID(idExec, name)>0){
                name += "_" + nb;
            }
        } catch (Exception e){
        }
    } // Fin de la methode isInBase/1

    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLExecutionResult.getID(pExecution.getIdBdd(), name) == idBdd;
    }



    /******************* TRI ********************************************/


}
