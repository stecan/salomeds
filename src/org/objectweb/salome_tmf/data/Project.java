/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Observer;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLProject;

public class Project extends WithAttachment {
    static ISQLProject pISQLProject = null;

    /* Parameter of this project */
    protected Hashtable parametersSet;
    protected ArrayList environmentList;
    protected User pUser;
    protected User administrator;
    protected ArrayList groupList;
    protected Date creationDate;
    transient protected Vector userWrapperOfProject;
    ObservableProject pObservableProject;
    boolean load = false;

    public static Project pCurrentProject = null;

    public Project(String name, String description) {
        super(name, description);
        campaignList = new ArrayList();
        familyList = new ArrayList();
        if (pISQLProject == null) {
            pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        }
        creationDate = Util.getCurrentDate();
        groupList = new ArrayList();
        environmentList = new ArrayList();
        familyList = new ArrayList();
        campaignList = new ArrayList();
        parametersSet = new Hashtable();
        pCurrentProject = this;
        pObservableProject = new ObservableProject();
    }

    public Project(String name) throws Exception {
        super(name, "");
        if (pISQLProject == null) {
            pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        }
        ProjectWrapper pProjectWrapper = pISQLProject.getProject(name);
        creationDate = pProjectWrapper.getCreatedDate();
        description = pProjectWrapper.getDescription();
        idBdd = pProjectWrapper.getIdBDD();
        groupList = new ArrayList();
        environmentList = new ArrayList();
        familyList = new ArrayList();
        campaignList = new ArrayList();
        parametersSet = new Hashtable();
        pCurrentProject = this;
        loadProjectInfo();
        pObservableProject = new ObservableProject();
    }

    public Project(ProjectWrapper pProjectWrapper) {
        super(pProjectWrapper.getName(), pProjectWrapper.getDescription());
        creationDate = pProjectWrapper.getCreatedDate();
        idBdd = pProjectWrapper.getIdBDD();
        if (pISQLProject == null) {
            pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        }
        groupList = new ArrayList();
        environmentList = new ArrayList();
        familyList = new ArrayList();
        campaignList = new ArrayList();
        parametersSet = new Hashtable();
        pCurrentProject = this;
        loadProjectInfo();
        pObservableProject = new ObservableProject();
    }

    public void setUserInModel(User pUser) {
        this.pUser = pUser;
    }

    public void setLoad(boolean isloading) {
        load = isloading;
        if (isloading == false) {
            notifyChanged(ApiConstants.LOADING, this);
        }
    }

    public void registerObserver(Observer o) {
        try {
            pObservableProject.addObserver(o);
        } catch (Exception ex) {
            Util.err(ex);
        }
    }

    public void unRegisterObserver(Observer o) {
        try {
            pObservableProject.deleteObserver(o);
        } catch (Exception ex) {
            Util.err(ex);
        }
    }

    public void notifyChanged(SalomeEvent e) {
        try {
            if (!load) {
                pObservableProject.notifyChanged(e);
            }
        } catch (Exception ex) {
            Util.err(ex);
        }
    }

    public void notifyChanged(int code, Object arg) {
        try {
            if (!load) {
                pObservableProject.notifyChanged(code, arg);
            }
        } catch (Exception ex) {
            Util.err(ex);
        }
    }

    public void notifyChanged(int code, Object arg, Object oldV, Object newV) {
        try {
            if (!load) {
                pObservableProject.notifyChanged(code, arg, oldV, newV);
            }
        } catch (Exception ex) {
            Util.err(ex);
        }
    }

    /****************************************************************************************/
    /*********************** PROJECT METHODES **********************************************/
    /****************************************************************************************/

    public User getAdministratorFromModel() {
        return administrator;
    }

    public void loadProjectInfo() {
        if (isInBase()) {
            try {
                UserWrapper[] tmpArray = pISQLProject.getUsersOfProject(name);
                Vector tmpVector = new Vector();
                for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
                    tmpVector.add(tmpArray[tmpI]);
                }
                userWrapperOfProject = tmpVector;
            } catch (Exception e) {
                Util.err(e);
                userWrapperOfProject = new Vector();
            }
        } else {
            userWrapperOfProject = new Vector();
        }
    }

    public Vector getAllUsersWrapper() {
        return userWrapperOfProject;
    }

    public int containUser(String login) {
        int id = -1;
        if (userWrapperOfProject == null) {
            return id;
        }
        int userSize = userWrapperOfProject.size();
        boolean trouve = false;
        int i = 0;
        while (i < userSize && !trouve) {
            UserWrapper pUserWrapper = (UserWrapper) userWrapperOfProject
                .elementAt(i);
            if (pUserWrapper.getLogin().equals(login)) {
                id = pUserWrapper.getIdBDD();
                trouve = true;
            }
            i++;
        }
        return id;
    }

    public Vector getALLAdministratorWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        UserWrapper[] tmpArray = pISQLProject.getAdminsOfProject(name);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public UserWrapper getAdministratorWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        Vector adminList = getALLAdministratorWrapperFromDB();
        UserWrapper pUserWrapper = null;
        if (adminList.size() > 0) {
            pUserWrapper = (UserWrapper) adminList.elementAt(0);
        } else {
            throw new Exception("Project " + name + " have no  administrator");
        }
        return pUserWrapper;
    }

    public Vector getUserOfGroupFromDB(String groupName) throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        UserWrapper[] tmpArray = pISQLProject.getUserOfGroupInProject(name,
                                                                      groupName);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public Date getCreationDateFromModel() {
        return creationDate;
    }

    public ArrayList getGroupListFromModel() {
        return groupList;
    } // Fin de la methode getGroupList/0

    /*
     * public int getStatus() { return status; }
     */

    public void setAdministratorInModel(User user) {
        administrator = user;
    }

    public void setCreationDateInModel(Date date) {
        creationDate = date;
    }

    public void setGroupListInModel(ArrayList list) {
        groupList = list;
    }

    public void addInDB() throws Exception {
        if (isInBase()) {
            throw new Exception("Project " + name + " is already in DB");
        }
        idBdd = pISQLProject
            .insert(name, description, administrator.getIdBdd());
        loadProjectInfo();
    }

    void addInDB(Project p, boolean suite, boolean campagne, boolean users,
                 boolean groupe) throws Exception {
        if (isInBase()) {
            throw new Exception("Project " + name + " is already in DB");
        }
        idBdd = pISQLProject.copyProject(name, description, administrator
                                         .getIdBdd(), p.getIdBdd(), suite, campagne, users, groupe);
    }

    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        pISQLProject.delete(idBdd, name);
    }

    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        pISQLProject.update(idBdd, newName, newDesc);
    }

    public void updateInModel(String newName, String newDesc) {
        name = newName;
        description = newDesc;
    }

    @Override
    public void updateInDBAndModel(String newName, String newDesc)
        throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }

    /******************************************************************************/
    /** METHODES PUBLIQUES ***/
    /******************************************************************************/
    /**
     * Ajoute un environnement au projet
     */
    public void addEnvironmentInModel(Environment env) {
        environmentList.add(env);
        env.addInModel(this);
    } // Fin de la methode addEnvironment/1

    public void addEnvironmentInDBAndModel(Environment env) throws Exception {
        //              SQLEngine.
        env.addInDB(idBdd);
        addEnvironmentInModel(env);
    } // Fin de la methode addEnvironment/1

    /**
     * Ajoute un groupe au projet
     */
    public void addGroupInModel(Group group) {
        groupList.add(group);
    }

    /**
     * Retourne la liste des environnements
     *
     * @return la liste des environnements
     */
    public ArrayList getEnvironmentListFromModel() {
        return environmentList;
    } // Fin de la methode getEnvironmentList/0

    /**
     * Retourne l'environnement dont le nom est passe en parametre, null si le
     * nom n'est pas celui d'un environnement
     *
     * @param name
     *            un nom
     * @return l'environnement dont le nom est passe en parametre, null si le
     *         nom n'est pas celui d'un environnement
     */
    public Environment getEnvironmentFromModel(String name) {
        for (int i = 0; i < environmentList.size(); i++) {
            if (((Environment) environmentList.get(i)).getNameFromModel()
                .equals(name)) {
                return (Environment) environmentList.get(i);
            }
        }
        return null;
    } // Fin de la methode getEnvironment/0

    /**
     * Retourne l'environnement dont l'id est passe en parametre, null si le nom
     * n'est pas celui d'un environnement
     *
     * @param name
     *            un nom
     * @return l'environnement dont le nom est passe en parametre, null si le
     *         nom n'est pas celui d'un environnement
     */
    public Environment getEnvironmentFromModel(int id) {
        for (int i = 0; i < environmentList.size(); i++) {
            if (((Environment) environmentList.get(i)).getIdBdd() == id) {
                return (Environment) environmentList.get(i);
            }
        }
        return null;
    } // Fin de la methode getEnvironment/0

    /**
     * Retourne le groupe dont le nom est passe en parametre, null si le nom
     * n'est pas celui d'un groupe
     *
     * @param name
     *            un nom
     * @return un groupe, ou <code>null</code> si le nom n'est pas celui d'un
     *         groupe
     */
    public Group getGroupFromModel(String name) {
        for (int i = 0; i < groupList.size(); i++) {
            String normalizeName = java.text.Normalizer.normalize(
                                                                  ((Group) groupList.get(i)).getNameFromModel(),
                                                                  java.text.Normalizer.Form.NFD);
            if (normalizeName.replaceAll("[^\\p{ASCII}]", "").equals(name)) {
                return (Group) groupList.get(i);
            }
        }
        return null;
    } // Fin de la methode getGroup/1

    /**
     * Supprime un environnement de la liste des environnments
     *
     * @param env
     *            un environnement
     */
    public void deleteEnvironmentInModel(Environment env) {

        for (int i = 0; i < campaignList.size(); i++) {
            Campaign pCampaign = (Campaign) campaignList.get(i);
            ArrayList pExcutionList = pCampaign.getExecutionListFromModel();
            for (int j = 0; j < pExcutionList.size(); j++) {
                Execution pExecution = (Execution) pExcutionList.get(j);
                Environment pEnv = pExecution.getEnvironmentFromModel();
                if (env.getNameFromModel().equals(pEnv.getNameFromModel())
                    && env.getIdBdd() == pEnv.getIdBdd()) {
                    pCampaign.deleteExecutionInModel(pExecution);
                }
            }
        }
        env.deleteInModel();
        environmentList.remove(env);
    } // Fin de la methode removeEnvironment/1

    /**
     * Supprime un environnement de la liste des environnments
     *
     * @param env
     *            un nom d'environnement
     */
    /*
     * void removeEnvironmentInModel(String envName) { for (int i = 0; i <
     * environmentList.size(); i ++) { if
     * (((Environment)environmentList.get(i)).
     * getNameFromModel().equals(envName)) { environmentList.remove(i); return;
     * } } } // Fin de la methode removeEnvironment/1
     */

    public void deleteEnvironmentInDBandModel(Environment env) throws Exception {
        env.deleteInDB();
        deleteEnvironmentInModel(env);
    }

    /**
     *
     * @param name
     * @return
     */
    public boolean containsEnvironment(String name) {
        for (int i = 0; i < environmentList.size(); i++) {
            if (((Environment) environmentList.get(i)).getNameFromModel()
                .equals(name)) {
                return true;
            }
        }
        return false;
    } // Fin de la methode containsEnvironment/1

    public Vector getEnvironmentWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        EnvironmentWrapper[] tmpArray = pISQLProject.getProjectEnvs(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public void loadEnvironnementsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        environmentList.clear();
        Vector environmentOfProject = getEnvironmentWrapperFromDB();
        for (int i = 0; i < environmentOfProject.size(); i++) {
            EnvironmentWrapper envDB = (EnvironmentWrapper) environmentOfProject
                .get(i);
            Environment env = new Environment(envDB);
            addEnvironmentInModel(env);
        }
    }

    /**
     *
     * @param group
     */
    public void removeGroup(Group group) {
        groupList.remove(group);
    } // Fin de la methode removeGroup/1

    public void addParameterToModel(Parameter param) {
        parametersSet.put(param.getNameFromModel(), param);
    }

    public void addParameterToDBAndModel(Parameter param) throws Exception {
        param.addInDB(idBdd);

        addParameterToModel(param);
    }

    /*
     * void removeParameter(Parameter param) { parametersSet.remove(param); }
     */

    /*
     * void removeParameterInModel(String paramName) { for (Iterator iter =
     * parametersSet.iterator(); iter.hasNext();) { Parameter element =
     * (Parameter)iter.next(); if (element.getNameFromModel().equals(paramName))
     * { parametersSet.remove(element); return; } } }
     */

    public boolean containsParameterInModel(String paramName) {
        return (parametersSet.get(paramName) != null);
        /*
         * for (Iterator iter = parametersSet.iterator(); iter.hasNext();) {
         * Parameter element = (Parameter)iter.next(); if
         * (element.getNameFromModel().equals(paramName)) { return true; } }
         * return false;
         */
    }

    public boolean containsParameterInModel(Parameter param) {
        return parametersSet.containsValue(param);
    }

    public Parameter getParameterFromModel(String paramName) {
        return (Parameter) parametersSet.get(paramName);
        /*
         * for (Iterator iter = parametersSet.iterator(); iter.hasNext();) {
         * Parameter element = (Parameter)iter.next(); if
         * (element.getNameFromModel().equals(paramName)) { return element; } }
         * return null;
         */
    }

    public Parameter getParameterFromModel(int id) {
        Enumeration listParams = parametersSet.elements();
        while (listParams.hasMoreElements()) {
            Parameter element = (Parameter) listParams.nextElement();
            if (element.getIdBdd() == id) {
                return element;
            }
        }
        return null;
        /*
         * for (Iterator iter = parametersSet.elements(); iter.hasNext();) {
         * Parameter element = (Parameter)iter.next(); if (element.getIdBdd() ==
         * id) { return element; } } return null;
         */
    }

    public Hashtable getParameterSetFromModel() {
        return parametersSet;
    }

    public Vector getParametersWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        ParameterWrapper[] tmpArray = pISQLProject.getProjectParams(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public void loadParamtersFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        Vector projectParam = getParametersWrapperFromDB();
        parametersSet.clear();
        for (int m = 0; m < projectParam.size(); m++) {
            ParameterWrapper paramBdd = (ParameterWrapper) projectParam.get(m);
            Parameter param = new Parameter(paramBdd);
            addParameterToModel(param);
        }
    }

    /****************************************************************************************/
    /*********************** TESTPLAN METHODES **********************************************/
    /****************************************************************************************/

    protected ArrayList familyList;

    void clearTestPlanData() {
        for (int i = 0; i < familyList.size(); i++) {
            Family pFamily = (Family) familyList.get(i);
            pFamily.deleteInModel();
        }
        familyList.clear();
        for (int i = 0; i < environmentList.size(); i++) {
            Environment pEnvironment = (Environment) environmentList.get(i);
            pEnvironment.deleteInModel();
        }
        environmentList.clear();
        parametersSet.clear();
    }

    /**
     * Retourne la liste ordonnee des tests d'une suite
     *
     * @param list
     *            une suite
     * @return liste ordonnee de la suite passee en parametre
     */
    public ArrayList getAllTestFromModel(TestList list) {
        return list.getTestListFromModel();
    } // Fin de la methode getAllTest/1

    /**
     * Retourne la liste ordonnee des objets "suites de tests"
     *
     * @return la liste ordonnee de tous les objets "suites de tests"
     */
    public ArrayList getAllTestListFromModel(Family family) {
        return family.getSuiteListFromModel();
    } // Fin de la methode getAllTestList/1

    /**
     * Retourne la liste ordonnee des familles
     */
    public ArrayList getFamilyListFromModel() {
        return familyList;
    }

    public Vector getProjectFamiliesWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        FamilyWrapper[] tmpArray = pISQLProject.getFamily(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    /**
     * Retourne la famille dont le nom est passe en parametre
     *
     * @param familyName
     *            un nom
     * @return Retourne la famille dont le nom est passe en parametre,
     *         <code>null</code> si le nom n'est pas celui d'une famille
     */
    public Family getFamilyFromModel(String familyName) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getNameFromModel().equals(
                                                                       familyName)) {
                return (Family) familyList.get(i);
            }
        }
        return null;
    }

    /**
     * Retourne la famille dont le nom est passe en parametre
     *
     * @param int id l'id d'une famille
     * @return Retourne la famille dont le nom est passe en parametre,
     *         <code>null</code> si le nom n'est pas celui d'une famille
     */
    public Family getFamilyFromModel(int id) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getIdBdd() == id) {
                return (Family) familyList.get(i);
            }
        }
        return null;
    }

    /**
     * Retourne la suite de tests dont le nom est passe en parametre
     *
     * @param familyName
     *            un nom de famille
     * @param testListName
     *            un nom
     * @return Retourne la suite de tests dont le nom est passe en parametre,
     *         <code>null</code> si le nom n'est pas celui d'une suite de tests
     */
    public TestList getTestListFromModel(String familyName, String testListName) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getNameFromModel().equals(
                                                                       familyName)) {
                ArrayList list = ((Family) familyList.get(i))
                    .getSuiteListFromModel();
                for (int j = 0; j < list.size(); j++) {
                    if (((TestList) list.get(j)).getNameFromModel().equals(
                                                                           testListName)) {
                        return (TestList) list.get(j);
                    }

                }
            }
        }
        return null;
    }

    /**
     * Retourne le test dont le nom est passe en parametre
     *
     * @param familyName
     *            un nom de famille
     * @param testListName
     *            un nom de suite
     * @param un
     *            nom de test
     * @return Retourne le test dont le nom est passe en parametre,
     *         <code>null</code> si le nom n'est pas celui d'un test
     */
    public Test getTestFromModel(String familyName, String testListName,
                                 String testName) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getNameFromModel().equals(
                                                                       familyName)) {
                ArrayList list = ((Family) familyList.get(i))
                    .getSuiteListFromModel();
                for (int j = 0; j < list.size(); j++) {
                    if (((TestList) list.get(j)).getNameFromModel().equals(
                                                                           testListName)) {
                        ArrayList tests = ((TestList) list.get(j))
                            .getTestListFromModel();
                        for (int k = 0; k < tests.size(); k++) {
                            if (((Test) tests.get(k)).getNameFromModel()
                                .equals(testName)) {
                                return (Test) tests.get(k);
                            }
                        }
                    }

                }
            }
        }
        return null;
    }

    /**
     * Retourne le test dont l' id est passe en parametre
     *
     * @param id
     *            du test
     * @return Retourne le test dont le nom est passe en parametre,
     *         <code>null</code> si le nom n'est pas celui d'un test
     */
    public Test getTestFromModel(int idTest) {
        for (int i = 0; i < familyList.size(); i++) {
            ArrayList list = ((Family) familyList.get(i))
                .getSuiteListFromModel();
            for (int j = 0; j < list.size(); j++) {
                ArrayList tests = ((TestList) list.get(j))
                    .getTestListFromModel();
                for (int k = 0; k < tests.size(); k++) {
                    if (((Test) tests.get(k)).getIdBdd() == idTest) {
                        return (Test) tests.get(k);
                    }
                }
            }

        }

        return null;
    }

    public ArrayList getTestOfParameterFromModel(String paramName) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < familyList.size(); i++) {
            ArrayList testListArrayList = ((Family) familyList.get(i))
                .getSuiteListFromModel();
            for (int j = 0; j < testListArrayList.size(); j++) {
                ArrayList testArrayList = ((TestList) testListArrayList.get(j))
                    .getTestListFromModel();
                for (int k = 0; k < testArrayList.size(); k++) {
                    if (((Test) testArrayList.get(k))
                        .getUsedParameterFromModel(paramName) != null) {
                        result.add(testArrayList.get(k));
                    }
                }
            }
        }
        return result;
    }

    public ArrayList getAlltestFromModel() {
        ArrayList testInModel = new ArrayList();
        ArrayList familyList = getFamilyListFromModel();
        int size = familyList.size();
        for (int i = 0; i < size; i++) {
            Family pFamily = (Family) familyList.get(i);
            ArrayList suiteList = pFamily.getSuiteListFromModel();
            int size2 = suiteList.size();
            for (int j = 0; j < size2; j++) {
                TestList pSuite = (TestList) suiteList.get(j);
                ArrayList testList = pSuite.getTestListFromModel();
                int size3 = testList.size();
                for (int k = 0; k < size3; k++) {
                    Test pTest = (Test) testList.get(k);
                    testInModel.add(pTest);
                }

            }
        }
        return testInModel;
    }

    /**************** Operations d'ajout dans le modele de donnees **********************************/
    /*
     * Ajoute une famille
     *
     * @param familyName le nom de la famille
     */
    public void addFamilyInModel(Family family) {
        family.addInModel(this);
        familyList.add(family);
    }

    public void addFamilyInDB(Family family) throws Exception {
        family.addInDB(idBdd);
    }

    public void addFamilyInDBAndModel(Family family) throws Exception {
        addFamilyInDB(family);
        addFamilyInModel(family);
    }

    /**
     * Ajoute une suite de tests dans une famille existante
     *
     * @param list
     * @param family
     */
    public void addTestListInFamilyInModel(TestList list, Family family) {
        family.addTestListInModel(list);
    }

    // Return True if IHM need refresh
    public void addTestListInFamilyInDBAndModel(TestList pList, Family pFamily)
        throws Exception {
        // BDD
        if (pFamily == null) {
            throw new Exception();
        }
        if (pFamily.getIdBdd() == -1) {
            throw new Exception();
        }
        pList.addInDB(pFamily.getIdBdd());

        addTestListInFamilyInModel(pList, pFamily);
    }

    public void addTestInListInModel(Test test, TestList list) {
        list.addTestInModel(test);
    }

    public void addTestInListInDBAndModel(Test pTest, TestList list)
        throws Exception {
        list.addTestInDB(pTest);
        addTestInListInModel(pTest, list);
    }

    /**************** Operations de suppression dans donnees **********************************/

    /**
     * Supprime un test
     *
     * @param test
     *            un test
     */
    public void deleteTestInModel(Test pTest) {
        TestList pList = pTest.getTestListFromModel();
        ArrayList campaignList = getCampaignOfTest(pTest);

        pList.deleteTestInModel(pTest);
        for (int i = 0; i < campaignList.size(); i++) {
            deleteTestFromCampaignInModel(pTest, (Campaign) campaignList.get(i));
        }

    }

    public void deleteTestInDBandModel(Test pTest) throws Exception {

        pTest.deleteInDB();
        // pTest.deleteInBddAndModel(true);
        // Model
        deleteTestInModel(pTest);
    } // Fin de la methode deleteTest/1

    /**
     * Supprime une suite de tests
     *
     * @param list
     *            une suite de tests
     */
    public void deleteTestListInModel(TestList pList) {
        ArrayList campaignList = getCampaignOfTestList(pList);

        Family pFamily = pList.getFamilyFromModel();
        pFamily.deleteTestListInModel(pList);

        for (int i = 0; i < campaignList.size(); i++) {
            deleteTestListFromCampaignInModel(pList, (Campaign) campaignList
                                              .get(i));
        }

    }

    public void deleteTestListInDBandModel(TestList pList) throws Exception {
        pList.deleteInDB();
        // Model
        deleteTestListInModel(pList);
    }

    /**
     * Supprime une famille du model en mettant a jour le model Suppression dans
     * les campagnes
     *
     * @param family
     *            une famille
     */
    public void deleteFamilyInModel(Family pFamily) {
        ArrayList campaignList = getCampaignOfFamily(pFamily);
        familyList.remove(pFamily);
        pFamily.deleteInModel();
        for (int i = 0; i < campaignList.size(); i++) {
            deleteFamilyFromCampaignFromModel(pFamily, (Campaign) campaignList
                                              .get(i));
        }
    } // Fin de la methode deleteFamily/1

    public void deleteFamilyInDBAndModel(Family pFamily) throws Exception {
        pFamily.deleteInDB();
        deleteFamilyInModel(pFamily);
    }

    /**********************************************************************************************************/

    /**
     * Methode qui rend vrai si la suite passee en parametre possede un test
     * dont le nom est passe en parametre, faux sinon.
     *
     * @param testListName
     *            le nom d'une suite
     * @param testName
     *            le nom d'un test
     * @return vrai si la suite dont le nom est passee en parametre possede un
     *         test dont le nom est passe en parametre, faux sinon.
     */
    public boolean containsTestInModel(TestList testList, String testName) {
        for (int i = 0; i < testList.getTestListFromModel().size(); i++) {
            Test test = (Test) testList.getTestListFromModel().get(i);
            if (test.getNameFromModel().equals(testName)) {
                return true;
            }
        }
        return false;
    } // Fin de la methode containsTest/2

    /**
     * Rend vrai si la famille passee en parametre contient une suite de tests
     * dont le nom est passe en parametre, faux sinon.
     *
     * @param family
     *            une famille
     * @param testListName
     *            une suite de tests
     * @return Rend vrai si la famille passee en parametre contient une suite de
     *         tests dont le nom est passe en parametre, faux sinon.
     */
    public boolean containsTestListInModel(Family family, String testListName) {
        for (int i = 0; i < family.getSuiteListFromModel().size(); i++) {
            if (((TestList) family.getSuiteListFromModel().get(i))
                .getNameFromModel().equals(testListName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Rend vrai si le nom passe en parametre correspond au nom d'une famille
     *
     * @param familyName
     *            le nom d'une famille
     * @return Rend vrai si le nom passe en parametre correspond au nom d'une
     *         famille, faux sinon.
     */
    public boolean containsFamilyInModel(String familyName) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getNameFromModel().equals(
                                                                       familyName)) {
                return true;
            }
        }
        return false;
    }

    /********************************** Selecteur *************************************************/

    public ArrayList getEnvironmentOfParameterFromModel(String paramName) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < getEnvironmentListFromModel().size(); i++) {
            if (((Environment) getEnvironmentListFromModel().get(i))
                .containsParameterInModel(paramName)) {
                result.add(getEnvironmentListFromModel().get(i));
            }
        }
        return result;
    }

    /************************************ Suppression *****************************/

    public void delParamInModel(Parameter pParam) {
        ArrayList envOfParam = getEnvironmentOfParameterFromModel(pParam
                                                                  .getNameFromModel());
        ArrayList testsOfParam = getTestOfParameterFromModel(pParam
                                                             .getNameFromModel());

        // Suppression du parametre des environnements
        for (int i = 0; i < envOfParam.size(); i++) {
            ((Environment) envOfParam.get(i)).deleteDefParameterInModel(pParam
                                                                        .getNameFromModel());
        }
        // Suppression du parametre des tests (et actions)
        for (int j = 0; j < testsOfParam.size(); j++) {

            Test currentTest = (Test) testsOfParam.get(j);
            currentTest.deleteUseParameterInModel(pParam);

            // On purge les campagnes concernees des tests utilisant le
            // parametre
            ArrayList campaignOfTest = getCampaignOfTest(currentTest);
            for (int k = 0; k < campaignOfTest.size(); k++) {
                Campaign campaign = (Campaign) campaignOfTest.get(k);
                deleteTestFromCampaignInModel((Test) testsOfParam.get(j),
                                              campaign);

                // Suppression du parametre des jeux de donnees des campagnes
                // concernees
                for (int l = 0; l < campaign.getDataSetListFromModel().size(); l++) {
                    DataSet dataSet = (DataSet) campaign
                        .getDataSetListFromModel().get(l);
                    dataSet.removeParameterInModel(pParam.getNameFromModel());
                }
            }

        }
        parametersSet.remove(pParam.getNameFromModel());
        // removeParameterInModel(pParam.getNameFromModel());
    }

    public void deleteParamInDBndModel(Parameter pParam) throws Exception {
        // BDD
        pParam.deleteInDB();

        // DATA
        delParamInModel(pParam);
    }

    /****************************************************************************************/
    /*********************** CAMPAIGN METHODES **********************************************/
    /****************************************************************************************/

    protected ArrayList campaignList;

    // static private ArrayList execResultList = new ArrayList();;

    /***********************************************************************************/
    void clearCampaignData() {
        for (int i = 0; i < campaignList.size(); i++) {
            Campaign pCampaign = (Campaign) campaignList.get(i);
            pCampaign.deleteInModel();
        }
        campaignList.clear();
    }

    /***********************************************************************************/
    public Vector getCampaignsWrapperListFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Project " + name + " is not in DB");
        }
        CampaignWrapper[] tmpArray = pISQLProject.getPrjectCampaigns(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    public ArrayList getCampaignListFromModel() {
        return campaignList;
    }

    public Campaign getCampaignFromModel(String campaignName) {
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i)).getNameFromModel().equals(
                                                                           campaignName)) {
                return (Campaign) campaignList.get(i);
            }
        }
        return null;
    }

    public Campaign getCampaignFromModel(int id) {
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i)).getIdBdd() == id) {
                return (Campaign) campaignList.get(i);
            }
        }
        return null;
    }

    public ArrayList getCampaignOfTest(Test test) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i)).containsTestInModel(test)) {
                result.add(campaignList.get(i));
            }
        }
        return result;
    }

    public ArrayList getCampaignOfTestList(TestList testList) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i))
                .containsTestListInModel(testList)) {
                result.add(campaignList.get(i));
            }
        }
        return result;
    }

    public ArrayList getCampaignOfFamily(Family family) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i)).containsFamilyInModel(family)) {
                result.add(campaignList.get(i));
            }
        }
        return result;
    }

    public ArrayList getExecutionOfEnvironmentInModel(String envName) {
        ArrayList result = new ArrayList();
        for (int i = 0; i < getCampaignListFromModel().size(); i++) {
            for (int j = 0; j < ((Campaign) campaignList.get(i))
                     .getExecutionListFromModel().size(); j++) {
                if (((Execution) ((Campaign) campaignList.get(i))
                     .getExecutionListFromModel().get(j))
                    .getEnvironmentFromModel().getNameFromModel().equals(
                                                                         envName)) {
                    result.add((((Campaign) campaignList.get(i))
                                .getExecutionListFromModel().get(j)));
                }
            }
        }
        return result;
    }

    public ArrayList getExecutionsOfDataSetInCamp(Campaign camp,
                                                  String dataSetName) {
        return camp.getExecutionsOfDataSetInCamp(dataSetName);
    }

    public String getCampaignWithExecResultWhereTestIsUse(Test test) {
        String message = "";
        for (int i = 0; i < campaignList.size(); i++) {
            Campaign camp = (Campaign) campaignList.get(i);
            if (camp.containsTestInModel(test)
                && camp.containsExecutionResultInModel())
                message = message + "* " + camp.getNameFromModel() + "\n";
        }
        return message;
    }

    /**************** Operations d'ajout dans le modele de donnees **********************************/

    void addTestInCampaignInModel(Test test, Campaign campaign, int userID) {
        campaign.addTestInModel(test, userID);
    }

    public ArrayList addTestInCampaignInDBAndModel(Test pTest,
                                                   Campaign pCampain, int userID) throws Exception {
        boolean newDataSetCreated = false;
        ArrayList datasetsCreated = new ArrayList();
        ArrayList dataSets = pCampain.getDataSetListFromModel();
        // BDD
        int transNumber = -1;
        try {
            // BDD
            transNumber = Api.beginTransaction(110,
                                               ApiConstants.INSERT_TEST_INTO_CAMPAIGN);

            pCampain.importTestInDB(pTest.getIdBdd(), userID);
            for (int i = 0; i < pTest.getParameterListFromModel().size(); i++) {
                if (dataSets != null) {
                    if ((dataSets.size() == 0) && (!newDataSetCreated)
                        && pCampain.getExecutionListFromModel().size() != 0) {
                        newDataSetCreated = true;
                        for (int k = 0; k < pCampain
                                 .getExecutionListFromModel().size(); k++) {
                            Execution pExecution = pCampain
                                .getExecutionFromModel(k);
                            DataSet newDataSet = new DataSet("dataSet_" + k,
                                                             ApiConstants.DEFAULT_DATA_SET);
                            newDataSet.addInDB(pCampain.getIdBdd());

                            Parameter param = (Parameter) pTest
                                .getParameterListFromModel().get(i);
                            Environment env = pExecution
                                .getEnvironmentFromModel();

                            if (env.getParameterFromModel(param
                                                          .getNameFromModel()) != null) {
                                newDataSet.addParameterValueInDB(env
                                                                 .getParameterValueFromModel(param),
                                                                 param);
                            } else {
                                newDataSet.addParameterValueInDB("", param);
                            }

                            pExecution.updateDatasetInDB(newDataSet.getIdBdd());
                            datasetsCreated.add(i, newDataSet);
                        }
                    } else {
                        for (int k = 0; k < dataSets.size(); k++) {
                            DataSet dataSet = (DataSet) dataSets.get(k);
                            if (!dataSet
                                .getParametersHashMapFromModel()
                                .containsKey(
                                             ((Parameter) pTest
                                              .getParameterListFromModel()
                                              .get(i)).getNameFromModel())) {
                                Parameter param = (Parameter) pTest
                                    .getParameterListFromModel().get(i);
                                if (Api.isDYNAMIC_VALUE_DATASET()) {
                                    dataSet.addParameterValueInDB(
                                                                  DataConstants.PARAM_VALUE_FROM_ENV,
                                                                  param);
                                } else {
                                    dataSet.addParameterValueInDB("", param);
                                }
                            }
                        }
                    }
                }
            }
            Api.commitTrans(transNumber);
        } catch (Exception e) {
            Api.forceRollBackTrans(transNumber);
        }
        // Model
        for (int i = 0; i < pTest.getParameterListFromModel().size(); i++) {
            Parameter param = (Parameter) pTest.getParameterListFromModel()
                .get(i);
            if (dataSets != null) {
                if (newDataSetCreated) {
                    for (int ind = 0; ind < datasetsCreated.size(); ind++) {
                        DataSet dataset = (DataSet) datasetsCreated.get(ind);
                        pCampain.addDataSetInModel(dataset);
                        // dataset.addParameter(param.getName(), "");
                    }
                    for (int k = 0; k < pCampain.getExecutionListFromModel()
                             .size(); k++) {
                        Execution pExecution = pCampain
                            .getExecutionFromModel(k);
                        Environment env = pExecution.getEnvironmentFromModel();
                        String paramValue = "";
                        if (env.getParameterFromModel(param.getNameFromModel()) != null) {
                            paramValue = env.getParameterValueFromModel(param);
                        }
                        ((DataSet) datasetsCreated.get(k))
                            .addParameterValueInModel(param
                                                      .getNameFromModel(), paramValue);
                        pExecution
                            .updateDatasetInModel((DataSet) datasetsCreated
                                                  .get(k));

                        // executionTableModel.setValueAt(((DataSet)datasetsCreated.get(k)).getName(),k,3);
                    }
                } else {
                    for (int k = 0; k < dataSets.size(); k++) {
                        DataSet dataSet = (DataSet) dataSets.get(k);
                        // String defParam = "\n";
                        // int add = 0;
                        if (!dataSet.getParametersHashMapFromModel()
                            .containsKey(
                                         ((Parameter) pTest
                                          .getParameterListFromModel()
                                          .get(i)).getNameFromModel())) {
                            // defParam += param.getNameFromModel() + " ";
                            // add++;
                            if (Api.isDYNAMIC_VALUE_DATASET()) {
                                dataSet.addParameterValueInModel(param
                                                                 .getNameFromModel(),
                                                                 DataConstants.PARAM_VALUE_FROM_ENV);
                            } else {
                                dataSet.addParameterValueInModel(param
                                                                 .getNameFromModel(), "");
                            }
                        }
                        /*
                         * if (add == 1){
                         * JOptionPane.showMessageDialog(AskNewExecution.this,
                         * Language.getInstance().getText("Le_parametre") +
                         * defParam +
                         * Language.getInstance().getText("Ajoute_aux_jeux"),
                         * Language.getInstance().getText("Attention_"),
                         * JOptionPane.WARNING_MESSAGE); } else if (add > 1 ){
                         * JOptionPane.showMessageDialog(AskNewExecution.this,
                         * Language.getInstance().getText("Les_parametres") +
                         * defParam +
                         * Language.getInstance().getText("Ajoutes_aux_jeux"),
                         * Language.getInstance().getText("Attention_"),
                         * JOptionPane.WARNING_MESSAGE); }
                         */
                    }
                }
            }
        }

        addTestInCampaignInModel(pTest, pCampain, userID);
        return datasetsCreated;
    }

    public void addCampaignInModel(Campaign campaign) {
        campaignList.add(campaign);
    } // Fin de la methode addcampaign/1

    public void addCampaignInDBandModel(Campaign campaign) throws Exception {
        campaign.addInDB(idBdd, pUser.getIdBdd());
        addCampaignInModel(campaign);
    }

    /**************** Operations de suppression dans donnees **********************************/

    /**
     * Supprime une campagne en mettant a jour le model
     *
     * @param campagne
     *            une campagne
     */
    public void deleteCampaignInModel(Campaign pCampaign) {
        campaignList.remove(pCampaign);
        pCampaign.deleteInModel();
        // Util.debug("campaigne size" + campaignList.size());
    }

    public void deleteCampaignInDBAndModel(Campaign pCampaign) throws Exception {
        pCampaign.deleteInDB();
        deleteCampaignInModel(pCampaign);
    }

    /**
     * Supprime un test d'une campagne en mettan a jour l'ensemble du model
     *
     * @param test
     *            un test
     * @param campagne
     *            une campagne
     * @return vrai si la campagne est vide
     */
    public boolean deleteTestFromCampaignInModel(Test test, Campaign campaign) {
        return campaign.deleteTestFromCampInModel(test, true);

    } // Fin de la methode deleteTestFromCampaign/2

    public boolean deleteTestFromCampaignInDBAndModel(Test pTest,
                                                      Campaign pCampaign) throws Exception {
        // BDD
        pCampaign.deleteTestFromCampInDB(pTest.getIdBdd(), true);
        // Model
        return deleteTestFromCampaignInModel(pTest, pCampaign);

    }

    /**
     * Supprime une suite de tests d'une campagne en mettan a jour l'ensemble du
     * model
     *
     * @param list
     *            une suite de tests
     * @param campagne
     *            une campagne
     * @return vrai si la campgne est vide
     */
    public boolean deleteTestListFromCampaignInModel(TestList pTestList,
                                                     Campaign campaign) {
        return campaign.removeTestListInModel(pTestList);
    }

    /*
     * @TODO Utiliser les donnees de la BDD
     */
    public boolean deleteTestListFromCampaignInDBAndModel(TestList pTestList,
                                                          Campaign campaign) throws Exception {
        // BDD
        int transNumber = -1;
        ArrayList pListofTest = campaign.getTestListFromModel();
        try {
            transNumber = Api.beginTransaction(10,
                                               ApiConstants.DELETE_TEST_FROM_CAMPAIGN);
            for (int i = 0; i < pListofTest.size(); i++) {
                Test pTest = (Test) pListofTest.get(i);
                if (pTestList.getTestFromModel(pTest.getIdBdd()) != null) {
                    campaign.deleteTestFromCampInDB(pTest.getIdBdd(), true);
                }
            }
            Api.commitTrans(transNumber);
        } catch (Exception e) {
            Api.forceRollBackTrans(transNumber);
            throw e;
        }
        // Model
        return deleteTestListFromCampaignInModel(pTestList, campaign);
    } // Fin de la methode deleteTestListFromCampaign/2

    /**
     * Supprime une famille d'une campagne en mettan a jour l'ensemble du model
     *
     * @param family
     *            une famille
     * @param campagne
     *            une campagne
     * @return vrai si la campgne est vide
     */
    public boolean deleteFamilyFromCampaignFromModel(Family family,
                                                     Campaign campaign) {
        return campaign.removeFamily(family);
    }

    /*
     * @TODO Utiliser les donnees de la BDD
     */
    public boolean deleteFamilyFromCampaignInDBAndModel(Family family,
                                                        Campaign campaign) throws Exception {
        // BDD
        int transNumber = -1;
        ArrayList pListofTest = campaign.getTestListFromModel();
        try {
            transNumber = Api.beginTransaction(10,
                                               ApiConstants.DELETE_TEST_FROM_CAMPAIGN);
            for (int i = 0; i < pListofTest.size(); i++) {
                Test pTest = (Test) pListofTest.get(i);
                if (family.isContainTestInModel(pTest)) {
                    campaign.deleteTestFromCampInDB(pTest.getIdBdd(), true);
                }
            }
            Api.commitTrans(transNumber);
        } catch (Exception e) {
            Api.forceRollBackTrans(transNumber);
            throw e;
        }

        // Model
        return deleteFamilyFromCampaignFromModel(family, campaign);
    }

    /************************** ATTACHEMENTS **********************/
    @Override
    public void addAttachementInDB(Attachment attach) throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }

    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLProject.addAttachFile(idBdd, new SalomeFileWrapper(f),
                                            file.getDescriptionFromModel());
        file.setIdBdd(id);
    }

    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        int id = pISQLProject.addAttachUrl(idBdd, url.getNameFromModel(), url
                                           .getDescriptionFromModel());
        url.setIdBdd(id);
    }

    @Override
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLProject.deleteAttach(idBdd, attachId);
    }

    @Override
    public void deleteAttachementInDBAndModel(Attachment attach)
        throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }

    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLProject
            .getAllAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLProject.getAllAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    /******************************************************************************************************************/

    /**
     * Rend vrai si le nom passe en parametre correspond au nom d'une campagne
     *
     * @param campagneName
     *            un nom
     * @return Rend vrai si le nom passe en parametre correspond au nom d'une
     *         campagne, faux sinon.
     */
    public boolean containsCampaignInModel(String campaignName) {
        for (int i = 0; i < campaignList.size(); i++) {
            if (((Campaign) campaignList.get(i)).getNameFromModel().equals(
                                                                           campaignName)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        ProjectWrapper pProject = pISQLProject.getProject(name);
        return pProject.getIdBDD() == idBdd;
    }

    public void triFamilleInModel() {
        Collections.sort(familyList, new ComparateurFamille());
    }

    class ComparateurFamille implements Comparator {
        @Override
        public int compare(Object poTest1, Object poTest2) {
            Family pFam1 = (Family) poTest1;
            Family pFam2 = (Family) poTest2;
            /*
             * Util.debug(" ORDER1 = " + pTest1.getNameFromModel() + " " +
             * pTest1.order); Util.debug(" ORDER2 = " +
             * pTest2.getNameFromModel() + " " + pTest2.order);
             */
            if (pFam1.order > pFam2.order) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public void triCampagneInModel() {
        Collections.sort(campaignList, new ComparateurCampagne());
    }

    class ComparateurCampagne implements Comparator {
        @Override
        public int compare(Object poTest1, Object poTest2) {
            Campaign pCamp1 = (Campaign) poTest1;
            Campaign pCamp2 = (Campaign) poTest2;
            /*
             * Util.debug(" ORDER1 = " + pTest1.getNameFromModel() + " " +
             * pTest1.order); Util.debug(" ORDER2 = " +
             * pTest2.getNameFromModel() + " " + pTest2.order);
             */
            if (pCamp1.order > pCamp2.order) {
                return 1;
            } else {
                return -1;
            }
        }
    }
}
