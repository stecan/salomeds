package org.objectweb.salome_tmf.data;

import java.util.ArrayList;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Permission;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;

public class Group extends SimpleData {
        
    static ISQLGroup pISQLGroup = null;
        
    private boolean[] testsPermissions;
    private boolean[] campaignPermissions;
        
    private ArrayList userList;
        
    /**************************************************************************/
    /**                                                         CONSTRUCTEUR                                                    ***/
    /**************************************************************************/
        
    /**
     * Constructeur du Groupe. le nom et la description sont instancies avec la 
     * chaine vide.
     */
    public Group(String name, String description) {
        super(name, description);
        testsPermissions = new boolean[3];
        campaignPermissions = new boolean[4]; 
        userList = new ArrayList();
        if (pISQLGroup == null){
            pISQLGroup = Api.getISQLObjectFactory().getISQLGroup();
        }
    } // Fin du constructeur Group/0
        
    public Group(GroupWrapper pGroupWrapper) {
        super(pGroupWrapper.getName(), pGroupWrapper.getDescription());
        int permissions = pGroupWrapper.getPermission();
        testsPermissions = new boolean[3];
        campaignPermissions = new boolean[4]; 
        changeTestPermissionInModel(0, ((permissions & Permission.ALLOW_CREATE_TEST) == Permission.ALLOW_CREATE_TEST));
        changeTestPermissionInModel(1, ((permissions & Permission.ALLOW_UPDATE_TEST) == Permission.ALLOW_UPDATE_TEST));
        changeTestPermissionInModel(2, ((permissions & Permission.ALLOW_DELETE_TEST) == Permission.ALLOW_DELETE_TEST));
        changeCampaignPermissionInModel(0, ((permissions & Permission.ALLOW_CREATE_CAMP) == Permission.ALLOW_CREATE_CAMP));
        changeCampaignPermissionInModel(1, ((permissions & Permission.ALLOW_UPDATE_CAMP) == Permission.ALLOW_UPDATE_CAMP));
        changeCampaignPermissionInModel(2, ((permissions & Permission.ALLOW_DELETE_CAMP) == Permission.ALLOW_DELETE_CAMP));
        changeCampaignPermissionInModel(3, ((permissions & Permission.ALLOW_EXECUT_CAMP) == Permission.ALLOW_EXECUT_CAMP));
        userList = new ArrayList();
        idBdd = pGroupWrapper.getIdBDD();
        if (pISQLGroup == null){
            pISQLGroup = Api.getISQLObjectFactory().getISQLGroup();
        }
    }
    /**
     * Constructeur d'un groupe. La description est instanciee avec une chaine 
     * vide, les permissions et la liste des utilisateurs sont creees vides.
     * @param n le nom du groupe
     */
    public Group(String n) {
        this(n, "");
    } 
        
    @Override
    public void  clearCache(){
        /* NOTHING */
    }
        
    /********************************** DB **********************************/
        
    void addInDB(int idProject) throws Exception {
        if (isInBase()) {
            throw new Exception("Group " + name + " already in BDD");
        }
        idBdd = pISQLGroup.insert(idProject, name, description, 0);
    }
        
    public void updatePermissionInDB(int perm) throws Exception {
        if (!isInBase()) {
            throw new Exception("Group " + name + " is not in BDD");
        }
        pISQLGroup.updatePermission(idBdd, perm);
    }
        
        
    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
    void delete() throws Exception  {
        if (!isInBase()) {
            throw new Exception("Group " + name + " is not in BDD");
        }
        pISQLGroup.delete(idBdd);
    }
        
    public void updateInDB(String neWname, String newDescription) throws Exception  {
        if (!isInBase()) {
            throw new Exception("Group " + name + " is not in BDD");
        }
        Util.log("[Group->updateInDB] with " + neWname + ", and " + newDescription);
        pISQLGroup.updateGroup(idBdd, neWname, newDescription);
    }
        
    @Override
    public void updateInDBAndModel(String name, String description) throws Exception  {
        updateInDB(name,description);
        updateInModel(name,description);
    }
        
    public void updateInModel(String name, String description)  {
        this.name = name;
        this.description = description;
    }
        
    public void updateNameInModel(String name){
        this.name = name;
    }
        
    /*public void updateDescriptionInModel(String des){
      description = des;
      }*/
        
    public void updateUserDescForGroupInDB(User pUser, String description)throws Exception  {
        if (!isInBase()) {
            throw new Exception("Group " + name + " is not in BDD");
        }
        if (!pUser.isInBase()) {
            throw new Exception("User  " + pUser.getLoginFromModel() + " is not in BDD");
        }
        pISQLGroup.updateUserDescInGroup(idBdd, pUser.getIdBdd(), description);
    }
    /**
     * Retourne la liste des utilisateurs
     * @return la liste des utilisateurs
     */
    public ArrayList getUserListFromModel() {
        return userList;
    } 
        
    /**setNameInModel
     * Change une permission du groupe
     * @param name nom de la permission
     * @param value valeur (vrai/faux)
     */
    public void changeTestPermissionInModel(int index, boolean value) {
        testsPermissions[index] = value;
    } 

    /**
     * Change une permission du groupe
     * @param name nom de la permission
     * @param value valeur (vrai/faux)
     */
    public void changeCampaignPermissionInModel(int index, boolean value) {
        campaignPermissions[index] = value;
    } 

        
    /**
     * Ajoute un utilisateur au groupe
     * @param user un utilisateur
     */
    public void addUserInModel(User user) {
        userList.add(user);
    } 
        
    /**
     * Supprime un utilisateur du groupe
     * @param user un utilisateur
     */
    public void removeUserInModel(User user) {
        userList.remove(user);
    } 
        
        
    public boolean[] getCampaignPermissionsFromModel() {
        return campaignPermissions;
    }
        
    public boolean[] getTestsPermissionsFromModel() {
        return testsPermissions;
    }
        
    public boolean containUserInModel(User user){
        for (int i = 0 ; i < userList.size(); i++){
            User pUser = (User) userList.get(i);
            if (pUser.getLoginFromModel().equals(user.getLoginFromModel())){
                return true;
            }
        }
        return false;
    }
        
    ////////////////////////////////////////EQUALS //////////////////////////////////
    @Override
    public boolean equals (Object o) {
        Group testedGroup;
        boolean test = false;
        if (!(o  instanceof Group)) {
            return  false;
        }
        testedGroup  = (Group) o;
        if (isInBase() && testedGroup.isInBase()){
            test = idBdd == testedGroup.idBdd;
        } else {
            test = name.equals(testedGroup.getNameFromModel());
        }
        return test;
    }
        
    @Override
    public boolean existeInBase() throws Exception {
        if( !isInBase()) {
            return false;
        }
        return pISQLGroup.getID(DataLoader.getCurrentProject().getIdBdd(), name) == idBdd;
    }
}
