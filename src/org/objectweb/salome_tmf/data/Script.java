/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.net.URL;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLScript;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.ScriptEngine;


public class Script extends Attachment {
        
    static ISQLScript pISQLScript = null;
        
    String plugArg;
    String scriptExtension = null;
        
    protected String type;
    protected ScriptEngine pEngine = null;
        
    /**************************************************************************/
    /**                                                         CONSTRUCTEUR                                                    ***/
    /**************************************************************************/
        
    /**
     * Le nom, le type et la localisation sont initialises a vide.
     */
    public Script(String name, String description) {
        super(name, description);
        type = "";
        //localisation = "";
        if (pISQLScript == null){
            pISQLScript = Api.getISQLObjectFactory().getISQLScript();
        }
        plugArg="";
    } 
        
    public Script(ScriptWrapper pScrip) {
        super( pScrip.getName(), pScrip.getDescription() );
        type = pScrip.getType();
        idBdd = pScrip.getIdBDD();
        plugArg = pScrip.getPlugArg();
        scriptExtension = pScrip.getScriptExtension();
        //localisation = "";
        if (pISQLScript == null){
            pISQLScript = Api.getISQLObjectFactory().getISQLScript();
        }
    } 
        
    @Override
    public void  clearCache(){
        /* NOTHING */
    }
    /**************************************************************************/
    /**                                         ACCESSEURS ET MUTATEURS                                                 ***/
    /**************************************************************************/
        
    public ScriptEngine getScriptEngine(Extension pExtension, URL urlSalome, JPFManager jpf){ //!!!! PENSER A INIT JPF
        if (scriptExtension != null && pEngine == null){
            if (pExtension != null){
                try {
                    pEngine = (ScriptEngine)jpf.activateExtension(pExtension);
                    pEngine.initScriptEngine(urlSalome);
                    Util.log("[Script] ScriptEngine ref : " +  pEngine);
                }catch (Exception e){
                    pEngine = null;
                    Util.err(e);
                }
            }
        }
        return pEngine;
    }
        
        
    public String getTypeFromModel() {
        return type;
    }
        
    public void setTypeInModel(String string) {
        type = string;
    } 
        
    public String getScriptExtensionFromModel() {
        return scriptExtension;
    }
        
        
    public void setScriptExtensionInModel(String extension) {
        scriptExtension = extension;
    }
        
    /********* Plug Arg **********/
        
    public void updatePlugArgInDB(String newPlugArg) throws Exception {
        if (!isInBase()) {
            throw new Exception("Script " + name + " is not in BDD");
        }
        pISQLScript.updatePlugArg(idBdd,newPlugArg);
    }
        
    public void updatePlugArgInModel(String string) {
        plugArg = string;
    }
        
    public void updatePlugArgInDBAndModel(String arg) throws Exception {
        updatePlugArgInDB(arg);
        updatePlugArgInModel(arg);
    }
        
    public String getPlugArgFromModel() {
        return plugArg;
    }
        
    /********** Basic operation on Script attachement ****************/
        
    public void updateInDB(String fileName) throws Exception {
        if (!isInBase()) {
            throw new Exception("Script " + name + " is not in BDD");
        }
        pISQLScript.update(idBdd, fileName);
    }
        
    public void updateInDB(File f) throws Exception {
        if (!isInBase()) {
            throw new Exception("Script " + name + " is not in BDD");
        }
        pISQLScript.update(idBdd, new SalomeFileWrapper(f));
    }
        
    //  public void updateInDB(SalomeFileWrapper f) throws Exception {
    //          if (!isInBase()) {
    //                  throw new Exception("Script " + name + " is not in BDD");
    //          }
    //          pISQLScript.update(idBdd, f);
    //  }
        
    public File getFileFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Script " + name + " is not in BDD");
        }
        return pISQLScript.getFile(idBdd).toFile();
    }
        
    public File getFileFromDB(String path) throws Exception {
        if (!isInBase()) {
            throw new Exception("Script " + name + " is not in BDD");
        }
        return pISQLScript.getFile(idBdd).toFile(path);
                
                
    }
        
    /*public void updateNameInDB(String fileName) throws Exception {
      if (!isInBase()) {
      throw new Exception("Script " + name + " is not in BDD");
      }
      pISQLScript.updateName(idBdd,fileName);
      }
        
      public void updateContentInDB(String filePath) throws Exception {
      if (!isInBase()) {
      throw new Exception("Script " + name + " is not in BDD");
      }
      pISQLScript.updateContent(idBdd, filePath);
      }
        
        
      public void updateDateInDB(Date date) throws Exception {
      if (!isInBase()) {
      throw new Exception("Script " + name + " is not in BDD");
      }
      pISQLScript.updateDate(idBdd,date);
      }
        
      public void updateLengthInDB(long length) throws Exception {
      if (!isInBase()) {
      throw new Exception("Script " + name + " is not in BDD");
      }
      pISQLScript.updateLength(idBdd, length);
      }
    */
        
        
}
