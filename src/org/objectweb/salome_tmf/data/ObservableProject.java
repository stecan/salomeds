package org.objectweb.salome_tmf.data;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import org.objectweb.salome_tmf.data.SalomeEvent;

public class ObservableProject extends Observable implements Serializable{
        
    @Override
    public void addObserver(Observer o){
        super.addObserver(o);
        setChanged();
    }
        
    public void notifyChanged(SalomeEvent e) {
        notifyObservers(e);
        setChanged();
    }
        
    public void notifyChanged(int code, Object arg) {
        notifyObservers(new SalomeEvent(code, arg));
        setChanged();
    }
        
    public void notifyChanged(int code, Object arg, Object oldV, Object newV) {
        notifyObservers(new SalomeEvent(code, arg, oldV, newV));
        setChanged();
    }
}
