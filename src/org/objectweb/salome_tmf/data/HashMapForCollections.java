/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Sous-classe de <code>HashMap</code> utilisee lorsque les valeurs de la table
 * de hachage sont des collections.<br>
 * Cette classe surcharge la methode {@link #put put(cle, valeur)} pour que
 * celle-ci permette de creer un couple (cle d'acces a liste) si la cle
 * n'existe pas dans la table de hachage, d'ajouter l'element dans un
 * {@link HashSet HashSet} existant si la cle existe.
 * @author teaml039
 * @version 0.1
 */
public class HashMapForCollections extends HashMap {


    /******************************************************************************/
    /**                                                         CONSTRUCTEUR                                                            ***/
    /******************************************************************************/
        
    /**
     * Constructeur d'une table de collections
     */
    public HashMapForCollections(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    } // Fin du constructeur HashMapforCollections/2

    /**
     * Constructeur d'une table de collections
     */
    public HashMapForCollections() {
        super(16, 0.75f);
    } // Fin du constructeur HashMapforCollections/2

    /******************************************************************************/
    /**                                                         METHODES PUBLIQUES                                                      ***/
    /******************************************************************************/
        
    /**
     * Cette methode surcharge celle definie dans {@link HashMap HashMap}.<br>
     * Si la cle n'existe pas deja, elle l'ajoute en plaçant l'objet
     * <code>value</code> dans un {@link HashSet HashSet} qui est cree en meme
     * temps.<br>
     * Si la cle existe deja, <code>value</code> est ajoutee dans le
     * {@link HashSet HashSet} correspondant a la cle <code>key</code>.
     * @param key cle de la table de hachage
     * @param value valeur a rajouter
     * @return l'objet HashSet ajoute si la cle existait deja, <code>null</code>
     *         si la cle n'existait pas
     */
    @Override
    public Object put(Object key, Object value) {
                
        if (this.containsKey(key) && value != null) {
            ((HashSet)this.get(key)).add(value);
            return this.get(key);
        } else {
            HashSet setValue = new HashSet(5,1);
            if (value != null) setValue.add(value);
            // Attention de bien rappeler le put/2 de la classe HashMap, sinon
            // ça boucle sur cette methode put
            super.put(key, setValue);
            return null;
        }
                
    } // Fin de la methode put/2
        
    /**
     * Calcule la taille cumulee de l'ensemble des elements qui
     * sont contenus dans les collections valeurs de la HashMap
     * @return la taille cumulee de l'ensemble des elements qui
     * sont contenus dans les collections valeurs de la HashMap
     */
    public int spreadedSize(){
                
        int spreadedSize = 0;
        for (Iterator it = values().iterator() ;it.hasNext() ;) {
            HashSet currentValue = (HashSet)it.next();
            spreadedSize += currentValue.size();
        }
        return spreadedSize;
    } // Fin de la methode speadedSize/0
} // Fin de la classe HashMapForCollections
