/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLAttachment;

public abstract class Attachment extends SimpleData {


    transient static ISQLAttachment pISQLAttachment = null;
    protected String localisation = null;
        
    public String getLocalisation() {
        return localisation;
    } 
        
        
    public void setLocalisation(String string) {
        Util.log("[Attachment->setLocalisation] change " + localisation + ", by " + string +", for " + name);
        localisation = string;
    }
    /**
     * Create instance of Attachment
     * @param name
     * @param description
     */
    public Attachment(String name, String description) {
        super(name, description);
        if (pISQLAttachment == null){
            pISQLAttachment = Api.getISQLObjectFactory().getISQLAttachment();
        }
    }
        
        
    /**
     * Create instance of Attachment with an AttachementWrapper
     * @param pAttach
     */
    public Attachment(AttachementWrapper pAttach) {
        super(pAttach.getName(),pAttach.getDescription() );
        setIdBdd(pAttach.getIdBDD()); 
        if (pISQLAttachment == null){
            pISQLAttachment = Api.getISQLObjectFactory().getISQLAttachment();
        }
    } 
    /*
     * Set id of this data in BDD
     */
    @Override
    public void setIdBdd(int id){
        super.setIdBdd(id);
    }
    /**
     * Update the description of the attachment in the model 
     * @param desc : the new description
     */
    /*public void updateDescriptionInModel(String desc){
      description = desc;
      }*/
        
    /**
     * Update the description of the attachment in the database 
     * @param desc : the new description
     * @throws Exception
     * no permission needed
     */    
    public void updateDescriptionInDB(String desc)  throws Exception {
        if (!isInBase()) {
            throw new Exception("Attachment " + name + " is not in BDD");
        }
        pISQLAttachment.updateDescription(idBdd, desc);
    }
        
    /**
     * Update the description of the attachment in the database and model 
     * @param desc : the new description
     * @throws Exception
     * no permission needed
     */
    public void updateDescriptionInDBdAndModel(String desc) throws Exception {
        //BDD
        updateDescriptionInDB(desc);
        //Model
        updateDescriptionInModel(desc);
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateDescriptionInDB(newDesc);
        updateDescriptionInModel(newDesc);
    }
        
    @Override
    public boolean existeInBase() throws Exception {    
        return  isInBase();
    }
}
