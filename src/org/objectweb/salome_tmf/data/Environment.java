/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ValuedParameterWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLConfig;
import org.objectweb.salome_tmf.api.sql.ISQLEnvironment;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;

public class Environment extends WithAttachment {
    static ISQLEnvironment pISQLEnvironment = null;
    static ISQLConfig pISQLConfig = null;
        
    protected Script initScript;
    protected Hashtable parametersHashTable; 
    protected Project pProject;
        
    public Environment(String name, String description) {
        super(name, description);
        initScript = null;
        parametersHashTable = new Hashtable();
        pProject = null;
        if (pISQLEnvironment == null){
            pISQLEnvironment = Api.getISQLObjectFactory().getISQLEnvironment();
        }
        if (pISQLConfig == null) {
            pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
        }
    }
        
    public Environment(EnvironmentWrapper pEnv) {
        super(pEnv.getName(), pEnv.getDescription() );
        idBdd = pEnv.getIdBDD();
        initScript = null;
        parametersHashTable = new Hashtable();
        pProject = null;
        if (pISQLEnvironment == null){
            pISQLEnvironment = Api.getISQLObjectFactory().getISQLEnvironment();
        }
        if (pISQLConfig == null) {
            pISQLConfig = Api.getISQLObjectFactory().getISQLConfig();
        }
    } 
        
    public void reloadFromDB(boolean base, Hashtable paramInModel) throws Exception {
        if (base){
            reloadBaseFromDB();
        }
        reloadScriptFromDB();
        reloadDefParameterFromDB(paramInModel);
        reloadAttachmentDataFromDB(true);
    }
        
    public void reloadBaseFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        EnvironmentWrapper pEnv = pISQLEnvironment.getWrapper(idBdd);
        name =pEnv.getName();
        description = pEnv.getDescription();
    }
        
        
        
        
    /***************************** Basic Operation *********************************/
        
        
    void addInDB(int idProject) throws Exception {
        if (isInBase()) {
            throw new Exception("Environment " + name + " is already in BDD");
        }
        idBdd = pISQLEnvironment.insert(idProject, name, description);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_ENVIRONMENT ,this);
    }
        
    void addInModel(Project pProject) {
        this.pProject = pProject;
        //pProject.addEnvironmentInModel(this); //NON
    }
                
    void addInBddAndModel(Project pProject)  throws Exception {
        addInDB(pProject.getIdBdd());   
        addInModel(pProject);
    }
        
        
        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.update(idBdd, newName, newDesc);
        //Util.log("[ENVIRONMENT-DB] old Name = " + name);
        //Util.log("[ENVIRONMENT-DB] new Name = " + newName);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_ENVIRONMENT ,this, new String(name), newName);
    }
        
    public void updateInModel(String newName, String newDesc)  {
        name = newName;
        description = newDesc;
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }
        
        
        
    public void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_ENVIRONMENT ,this);
                
        // Corresponding ICAL values
        if (pISQLConfig != null){
            String key = "Mantis_"+Language.getInstance().getText("ICAL")+"_"+idBdd;
            pISQLConfig.deleteProjectConf(key,DataModel.getCurrentProject().getIdBdd());
        
            // Corresponding QS Scoring values
            key = "Mantis_QsScore_"+idBdd;
            pISQLConfig.deleteProjectConf(key,DataModel.getCurrentProject().getIdBdd());
        }
                
    }
        
    public void deleteInModel()  {
        //pProject.removeEnvironmentInModel(name);
        pProject = null;
        parametersHashTable.clear();
        clearAttachInModel();
    }
        
    public void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
    /**************************** Script *************************************/
        
    public void reloadScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }       
        ScriptWrapper script =  getInitScriptFromDB();
        if (script != null){
            addScriptInModel(new Script(script));
        }
    }
        
    void addScriptInDB(Script script, File file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
                
        int id = pISQLEnvironment.addScript(idBdd,new SalomeFileWrapper(file), script.getDescriptionFromModel(), script.getNameFromModel(), script.getScriptExtensionFromModel(), script.getPlugArgFromModel());
        script.setIdBdd(id);
    }
        

    public void addScriptInModel(Script script){
        initScript = script;
    }
        
    public void addScriptInDBAndModel(Script script, File file) throws Exception {
        addScriptInDB(script, file);
        addScriptInModel(script);
    }
        

    public void deleteScriptInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.deleteScript(idBdd);
    }
        
    public void deleteScriptInModel(){
        initScript = null;
    }
        
    public void deleteScriptInDBAndModel() throws Exception {
        deleteScriptInDB();
        deleteScriptInModel();
    }
        

        
    public File getEnvScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        return pISQLEnvironment.getScript(idBdd).toFile();
    }
        
        
    public Script getInitScriptFromModel() {
        return initScript;
    }
        
    public File getInitScriptFileFromDB()  throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        return pISQLEnvironment.getScript(idBdd).toFile();
    }
        
        
    public ScriptWrapper getInitScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        return pISQLEnvironment.getScriptWrapper(idBdd);
    }
        
    /************************** Parameters ***************************/
    /**
     * paramInModel can be null if no object mapping with project params is need
     */
    public void reloadDefParameterFromDB(Hashtable paramInModel)throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        parametersHashTable.clear();
        ValuedParameterWrapper[] definedParams = pISQLEnvironment.getDefinedParameters(idBdd);
        for (int i = 0 ; i < definedParams.length ; i ++) {
            Parameter param = null;
            ValuedParameterWrapper pValuedParameterWrapper = definedParams[i];
            if (paramInModel == null){
                param = new Parameter(pValuedParameterWrapper.getParameterWrapper());
            } else {
                param = (Parameter) paramInModel.get(pValuedParameterWrapper.getParameterWrapper().getName());
                if (param == null){
                    param = new Parameter(pValuedParameterWrapper.getParameterWrapper());
                    paramInModel.put(pValuedParameterWrapper.getParameterWrapper().getName(), param);
                }
                                
            }
            addParameterValueInModel(param, pValuedParameterWrapper.getValue());
        }
    }
        
    public void setParametersHashTableInModel(Hashtable hashtable) {
        parametersHashTable = hashtable;
    }
        
    public void addParameterValueInModel(Parameter param, String value) {
        parametersHashTable.put(param, value);
    } 
        
    public void addParameterValueInDBModel(Parameter param, String value) throws Exception{
        addParameterValueInDB(param, value);
        addParameterValueInModel(param, value);
    } 
        
    public void addParameterValueInDB(Parameter param, String value) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.addParamValue(idBdd, param.getIdBdd(), value, param.getDescriptionFromModel());
    }
        
        
    public void addParamValueInDBAndModel(Parameter param, String value) throws Exception {
        addParameterValueInDB(param, value);
        addParameterValueInModel(param, value);
    }
        
    public void updateParamValueInModel(Parameter param, String value) {
        parametersHashTable.remove(param);
        addParameterValueInModel(param, value);
    }
        
    public void updateParamValueInDB(Parameter param, String value) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.updateParamValue(idBdd, param.getIdBdd(), value, param.getDescriptionFromModel());
    }
        
        
    public void updateParamValueInDBAndModel(Parameter param, String value) throws Exception {
        updateParamValueInDB(param,  value);
                
        updateParamValueInModel(param,  value);
    }
        
    public void deleteDefParameterInModel(Parameter param) {
        parametersHashTable.remove(param); 
    } 
        
        
    public void deleteDefParameterInModel(String paramName) {
        Set keysSet = parametersHashTable.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Parameter element = (Parameter)iter.next();
            if (element.getNameFromModel().equals(paramName)) {
                parametersHashTable.remove(element);
                return;
            }
        }
    }
        
    public void deleteDefParamInDB(int paramId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.deleteDefParam(idBdd, paramId);
    }
        
    public void deleteDefParamInDBAndModel(Parameter param) throws Exception {
        deleteDefParamInDB(param.getIdBdd());
        deleteDefParameterInModel(param);
    }

        
    public Parameter getParameterFromModel(String paramName) {
        Set keysSet = parametersHashTable.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Parameter element = (Parameter)iter.next();
            if (element.getNameFromModel().equals(paramName)) {
                return element;
            }
        }
        return null;
    } 
        
    public String getParameterValue(String paramName) {
        Set keysSet = parametersHashTable.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Parameter element = (Parameter)iter.next();
            if (element.getNameFromModel().equals(paramName)) {
                return (String)parametersHashTable.get(element);
            }
        }
        return null;
    } 
        
    public String getParameterValueFromModel(Parameter param) {
        return (String)parametersHashTable.get(param);
    }
        
    public Hashtable getCopyOfParameterHashTableFromModel(){
        Hashtable copyParameter = new Hashtable();
        Enumeration enumKey = parametersHashTable.keys();
        while (enumKey.hasMoreElements()){
            Object key = enumKey.nextElement();
            copyParameter.put(key, parametersHashTable.get(key));
        }
        return copyParameter;
    }
        
    public Hashtable getParametersHashTableFromModel() {
        return parametersHashTable;
    }
        
        
    public boolean containsParameterInModel(Parameter param) {
        return parametersHashTable.containsKey(param);
    }
        
    public boolean containsParameterInModel(String paramName) {
        Set keysSet = parametersHashTable.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Parameter element = (Parameter)iter.next();
            if (element.getNameFromModel().equals(paramName)) {
                return true;
            }
        }
        return false;
    }
        
        
    public Vector getValuedParametersWrapperFromDB()throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        ValuedParameterWrapper[] tmpArray = pISQLEnvironment.getDefinedParameters(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
        
    /************************** Attachements ***************************/
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLEnvironment.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        int id = pISQLEnvironment.addAttachUrl(idBdd, url.getNameFromModel(), url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
    public void addAttachInDBAndModel(Attachment attach)  throws Exception {
        addAttachementInDB(attach);
        addAttachementInModel(attach);
    }
        
        
    @Override
    public void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        pISQLEnvironment.deleteAttach(idBdd, attachId);
    }
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach) throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }
        
        
    /**
     * A vector of FileAttachementWrapper
     */
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLEnvironment.getAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector; 
    }
        
    public Vector getAttachFilesNameFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        Vector res= new Vector();
        FileAttachementWrapper[] listfilesWrapper = pISQLEnvironment.getAttachFiles(idBdd); 
        for (int i = 0; i < listfilesWrapper.length; i++){
            res.add(listfilesWrapper[i].getName());
        }
        return res;
    }
    /**
     * A vector of UrlAttachementWrapper
     */
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLEnvironment.getAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public Vector getAttachUrlsNameFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Environment " + name + " is not in BDD");
        }
        Vector res= new Vector();
        UrlAttachementWrapper[] listUrlsWrapper = pISQLEnvironment.getAttachUrls(idBdd); 
        for (int i = 0; i < listUrlsWrapper.length; i++){
            res.add(listUrlsWrapper[i].getName());
        }
        return res;
    }
    public static boolean isInBase(Project pProject, String envName) {
        try  {
            int id = pISQLEnvironment.getID(pProject.getIdBdd(), envName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLEnvironment.getID(pProject.getIdBdd(), name) == idBdd;
    }
}
