/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;


import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLProject;


public class AdminVTData {

    static ISQLProject pISQLProject = null;
    static ISQLPersonne pISQLPersonne = null;
    /**
     * Liste de tous les projets. Regroupe des elements de classe <code>Project</code>
     */
    ArrayList projectList;
        
    /**
     * Liste de tous les utilisateurs. Regroupe des elements de classe <code>User</code>
     */
    ArrayList userList;

    /**
     * Table contenant les administrateurs des projet. Pour chaque projet (la cle)
     * on associe un hashSet contenant les logins des admins (<code>String</code>)
     *  du projet;
     */
    protected Hashtable projectAdmin;
        
    User pAdmin;

    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/
         
    public void setUserAdminVTData(String login) throws Exception {
        pAdmin = new User(pISQLPersonne.getUserByLogin(login));
    }
        
    public User getUserAdminVTData(){
        return pAdmin;
    }
        
    /**
     * Ajoute un projet
     * @param project un projet
     */
    public void addProjectInModel(Project project) {
        if (project != null) {
            projectList.add(project);
            addAdminToProjectInModel(project.getAdministratorFromModel(), project);
        }
                
    } // Fin de la methode addProject/1
        
    public void addProjectInDB(Project project) throws Exception {
        if (project != null) {
            project.addInDB();
        }
    } // Fin de la methode addProject/1
        
    public void addProjectInDB(Project project, Project copy, boolean suite, boolean campagne, boolean users, boolean groupe) throws Exception {
        if (project != null) {
            project.addInDB(copy, suite, campagne, users, groupe);
        }
    } // Fin de la methode addProject/1
        
    public void addProjectInDBAndModel(Project project)  throws Exception{
        addProjectInDB(project);
        addProjectInModel(project);
    } // Fin de la methode addProject/1
        
        
    public void addProjectInDBAndModelByCopy(Project project, Project copy, boolean suite, boolean campagne, boolean users, boolean groupe)  throws Exception{
        addProjectInDB(project, copy, suite, campagne, users, groupe);
        addProjectInModel(project);
    } // Fin de la methode addProject/1
        
        
    /**
     * Permet de recuperer le projet associe au nom passe en parametre
     * @param name le nom du projet
     * @return un projet si le nom passe en parametre est un nom de projet, 
     * <code>null</code> sinon.
     */
    public Project getProjectFromModel(String name) {
        for (int i = 0 ; i < projectList.size(); i++) {
            if (((Project)projectList.get(i)).getNameFromModel().equals(name)) {
                return (Project)projectList.get(i);
            }
        }
        return null;
    } // Fin de la methode getProject/1


    void deleteProjectInDB(Project pProject) throws Exception {
        pProject.deleteInDB();
    }
        
    /**
     * Supprime un projet
     * @param project le projet a supprimer
     */
    public void deleteProjectInModel(Project project) {
        projectList.remove(project);
        projectAdmin.remove(project);
    } // Fin de la methode removeProject/1
        
    public void deleteProjectInDBAndModel(Project project) throws Exception {
        deleteProjectInDB(project);
        deleteProjectInModel(project);
    } // Fin de la methode removeProject/1
        
    /**
     * Supprime un projet
     * @param name le nom du projet a supprimer
     */
    /*public void deleteProjectInModel(String name) {
      Project project = getProjectFromModel(name);
      if (project != null) {
      removeProjectInModel(project);
      }
      } // Fin de la methode removeProject/1
    */
    /**
     * Retourne le nombre de projets dans SalomeTMF
     * @return le nombre de projets dans SalomeTMF
     */
    public int getProjectCountFromModel() {
        return projectList.size();
    } // Fin de la methode getProjectCount/0

    /**
     * Retourne la liste de tous les utilisateurs declares dans SalomeTMF
     * @return la liste de tous les utilisateurs declares dans SalomeTMF
     */
    public ArrayList getAllUsersFromModel() {
        return userList;
    } // Fin de la methode getAllUsers
        
    /**
     * Retourne le nombre d'utilisateurs declares dans SalomeTMF
     * @return le nombre d'utilisateurs declares dans SalomeTMF
     */
    public int getAllUsersCountFromModel() {
        return userList.size();
    } // Fin de la methode getAllUsersCount/0
        
    /**
     * Ajoute un nouvel utilisateur
     * @param user un utilisateur
     */
    public void addUserInModel(User user) {
        userList.add(user);
    } // Fin de la methode addUser/1
        
    /**
     * Supprime l'utilisateur dont le login est passe en parametre
     * @param userLogin le login de l'utilisateur
     */
    /*public void removeUserInModel(String userLogin) {
      for (int i = 0 ; i < userList.size(); i++) {
      if (((User)userList.get(i)).getLoginFromModel().equals(userLogin)) {
      userList.remove(i);
      return;
      }
      }
      } // Fin de la methode removeUser/1
    */
        
    public void delteUserInDB(User pUser) throws Exception {
        pUser.deleteInDB();
    } 
        
    public void delteUserInModel(User pUser) {
        for (int i = 0 ; i < userList.size(); i++) {
            if (((User)userList.get(i)).equals(pUser)) {
                userList.remove(i);
                return;
            }
        }
    } 
        
    public void delteUserInDBAndModel(User pUser) throws Exception {
        delteUserInDB(pUser);
        delteUserInModel(pUser);
    } 
        
    /**
     * Retourne vrai s'il existe dans la liste des utilisateurs, un utilisateur 
     * ayant le login passe en parametre, faux sinon.
     * @param login un login
     * @return vrai s'il existe dans la liste des utilisateurs, un utilisateur 
     * ayant le login passe en parametre, faux sinon.
     */
    public boolean containsLoginInModel(String login) {
        for (int i = 0; i < userList.size(); i ++) {
            if (((User)userList.get(i)).getLoginFromModel().equals(login)) {
                return true;
            }
        }
        return false;
    } // Fin de la methode containsLogin/1
        
    /**
     * Initialisation du modele de donnees
     */
    public void initData() {
        projectList = new ArrayList();
        userList = new ArrayList();
        projectAdmin = new Hashtable();
    } // Fin de la methode initData/0
        
    /**
     * Ajoute un administrateur au projet
     * @param userLogin le login d'un utilisateur
     * @param project un projet
     */
    public void addAdminToProjectInModel(User userAdmin, Project project) {
        Vector adminOfProject = (Vector) projectAdmin.get(project);
        if (adminOfProject == null){
            adminOfProject = new Vector();
        }
        if (!adminOfProject.contains(userAdmin)){
            adminOfProject.add(userAdmin);
        }
        projectAdmin.put(project, adminOfProject);
        //projectAdmin.put(project, userAdmin);
    } // Fin de la methode addAdminToProject/2
        
    /**
     * Retourne le nombre d'administrateurs du projet
     * @param project un projet
     * @return le nombre d'administrateurs du projet
     */
    public int getAdminCountFromModel(Project project) {
        Vector adminOfProject = (Vector) projectAdmin.get(project);
        if (adminOfProject == null){
            return 0;
        } else {
            return adminOfProject.size();
        }
        /*if (projectAdmin.containsKey(project)) {
          return 1;
          //return projectAdmin.get(project).size(); 
          }
          return 0;
        */
    } // Fin de la methode getAdminCount/1
        
    /**
     * Rend vrai si le projet passe en parametre a pour administrateur 
     * l'utilisateur dont le login est passe en parametre
     * @param userLogin un login d'utilisateur
     * @param project un projet
     * @return vrai si le projet passe en parametre a pour administrateur 
     * l'utilisateur dont le login est passe en parametre, faux sinon.
     */
    public boolean containsAdminInModel(String userLogin, Project project) {
        Vector adminOfProject = (Vector) projectAdmin.get(project);
        if (adminOfProject == null){
            return false;
        } else {
            for (int i = 0 ; i < adminOfProject.size(); i++) {
                User pUser = (User) adminOfProject.elementAt(i);
                if (pUser.getLoginFromModel().equals(userLogin)){
                    return true;
                }
            }
        }
        /*
          if (projectAdmin.containsKey(project)) {
          Enumeration enumP = projectAdmin.elements();
          while (enumP.hasMoreElements()){
          User pUser = (User) enumP.nextElement();
          if (pUser.getLoginFromModel().equals(userLogin)){
          return true;
          }
          }
          }*/
        return false;
    } // Fin de la methode containsAdmin/2
        
    public User containsAndGetAdminInModel(String userLogin, Project project) {
        Vector adminOfProject = (Vector) projectAdmin.get(project);
        if (adminOfProject == null){
            return null;
        } else {
            for (int i = 0 ; i < adminOfProject.size(); i++) {
                User pUser = (User) adminOfProject.elementAt(i);
                if (pUser.getLoginFromModel().equals(userLogin)){
                    return pUser;
                }
            }
        }
        /*
          if (projectAdmin.containsKey(project)) {
          Enumeration enumP = projectAdmin.elements();
          while (enumP.hasMoreElements()){
          User pUser = (User) enumP.nextElement();
          if (pUser.getLoginFromModel().equals(userLogin)){
          return true;
          }
          }
          }*/
        return null;
    } 
    /**
     * Retourne la liste des projets dont l'administrateur a pour login celui
     * passe en parametre
     * @param userLogin un login 
     * @return la liste des projets dont l'administrateur a pour login celui
     * passe en parametre
     */
    public ArrayList getProjectsWithThisUserAdminFromModel(String userLogin) {
        ArrayList result = new ArrayList();
        Enumeration enumA = projectAdmin.keys();
        while (enumA.hasMoreElements() ){
            Project project = (Project) enumA.nextElement();
            if (containsAdminInModel(userLogin, project)){
                result.add(project);
            }
            /*User pUser = (User) projectAdmin.get(project);
              if (pUser.getLoginFromModel().equals(userLogin)){
              result.add(project);
              }*/
        }
        return result;
    } // Fin de la methode getProjectsWithThisUserAdmin/1
        
    /**
     * Retourne la liste des projets
     * @return la liste des projets
     */
    public ArrayList getProjectListFromModel() {
        return projectList;
    } // Fin de la methode getProjectList/0

    /**
     * Retourne vrai si le projet dont le nom est passe en parametre exsite deja,
     * faux sinon.
     * @param projectName un nom de projet
     * @return vrai si le projet dont le nom est passe en parametre exsite deja,
     * faux sinon.
     */
    public boolean containsProjectInModel(String projectName) {
        for (int i = 0;  i < projectList.size(); i++) {
            if (((Project)projectList.get(i)).getNameFromModel().equals(projectName)) {
                return true;
            }
        }
        return false;
    } // Fin de la methode containsProject/1
        
        
    /**
     * Retourne l'utilisateur dont le login est passe en parametre. 
     * @param login un login d'utilisateur
     * @return l'utilisateur dont le login est passe en parametre, <code>null</code>
     * si le login n'est pas celui d'un utilisateur.
     */ 
    public User getUserFromModel(String login) {
        for (int i = 0 ; i < userList.size(); i++) {
            if (((User)userList.get(i)).getLoginFromModel().equals(login)) {
                return (User)userList.get(i);
            }
        }
        return null;
    } // Fin de la methode getUser/1
        
    /************************** Data Loading **********************************/
        
    public void loadData() throws Exception {
        if (pISQLProject == null){
            pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        }
        if (pISQLPersonne == null){
            pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
        }
                
        initData();
        ProjectWrapper[] listOfProjects= pISQLProject.getAllProjects();
        //adminProject.setUser(ADMIN_SALOME_NAME);
                
        for (int i = 0; i < listOfProjects.length; i++) {
            ProjectWrapper pProjectWrapper = listOfProjects[i];
            Project project = new Project(pProjectWrapper);
            try  {
                UserWrapper pUserWrapper = project.getAdministratorWrapperFromDB();
                User admin = new User(pUserWrapper);
                project.setAdministratorInModel(admin);
                addProjectInModel(project);
                        
                Vector usersAdmin = project.getUserOfGroupFromDB(ApiConstants.ADMINNAME);
                for (int j = 0; j < usersAdmin.size(); j++) {
                    pUserWrapper = (UserWrapper) usersAdmin.elementAt(j);
                    User pAdminProject = new User(pUserWrapper);
                    addAdminToProjectInModel(pAdminProject, project);
                }
            } catch(Exception e){
                Util.err(e);
            }
        }
                
        UserWrapper[] listOfUsers= pISQLProject.getAllUser();
        for (int i = 0; i < listOfUsers.length; i++) {
            UserWrapper pUserWrapper = listOfUsers[i];
            if (!pUserWrapper.getLogin().equals(ApiConstants.ADMIN_SALOME_NAME)){
                User pUser = new User(pUserWrapper);
                addUserInModel(pUser);
            }
        }
    }
        
} // Fin de la classe AdminVTData
