package org.objectweb.salome_tmf.data;

import java.util.ArrayList;

public interface IDataModelWrapper {
    public void addParameterToModel(ArrayList data);
    public void clearParameterToModel();
        
    public void addEnvToModel(ArrayList data);
    public void clearEnvModel();
        
        
    //public void initAttachmentModel(Collection col);
        
    public void addFamilyToModel(Family pFamily);
    public void updateFamilyToModel(Family pFamily);
    public void clearFamilyToModel(Family pFamily);
    public void refreshFamilyToModel(Family pFamily);
        
    public void addTestListToModel(Family pFamily, TestList pList);
    public void updateTestListToModel(TestList pList);
    public void clearTestListToModel(TestList pList);
    public void refreshTestListToModel(TestList pList);
        
    public void addTestToModel(TestList pList, Test pTest);
    public void updateTestToModel(TestList pList, Test pTest); //NOT USE
    public void clearTestToModel(Test pTest);
    public void refreshTestToModel(Test pTest);
        
    public void addCampaignToModel(Campaign campaign);
    public void upadateCampaignToModel(Campaign campaign);
    public void refreshCampaignToModel(Campaign campaign);
        
    public void addCampaignFamilyToModel(Campaign campaign, Family pFamily);
    public void upadateCampaignFamilyToModel(Campaign campaign, Family pFamily);
        
    public void addCampaignTestListToModel(Family pFamily, TestList pList);
    public void upadateCampaignTestListToModel(Campaign campaign, Family pFamily, TestList pList);
        
    public void addCampaignTestToModel(TestList pList, Test pTest);
    public void upadateCampaignTestToModel(TestList pList, Test pTest); //NOT USE
}
