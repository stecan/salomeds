
/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Mikael MARCHE
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.net.MalformedURLException;
import java.net.URL;

import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

public class UrlAttachment extends Attachment {
    URL url;

    public UrlAttachment(String name, String description){
        super(name, description);
    }

    public UrlAttachment(UrlAttachementWrapper pAttach){
        super(pAttach);
        try {
            url = new URL(pAttach.getName());
            //                  url = new URL(pAttach.getUrlProtocol(), pAttach.getUrlHost(), pAttach.getUrlPort(), pAttach.getUrlFile());
        } catch (MalformedURLException e) {
            //                  Util.err(e);
        }
        idBdd = pAttach.getIdBDD();
    }

    @Override
    public void  clearCache(){
        /* NOTHING */
    }

    /**
     * @return the Url of the attachment
     */
    public URL getUrl() {
        return url;
    }

    /**
     * Set the Url of the attachment
     * @param url
     */
    public void setUrl(URL url) {
        this.url = url;
    }


    @Override
    public String toString(){
        return "[URL] : "+name;
    }

    @Override
    public boolean equals(Object o){
        if (o instanceof UrlAttachment) {
            UrlAttachment toTest =  (UrlAttachment)  o;
            if (isInBase() && toTest.isInBase()) {
                if (getIdBdd() ==  toTest.getIdBdd()){
                    return true;
                }
            } else {
                return getUrl().equals(toTest.getUrl());
            }
        }
        return false;
    }

    /*public int hashCode(){
      if (isInBase()) {
      return getIdBdd();
      } else {
      return super.hashCode();
      }
      }*/
}
