/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.java.plugin.Extension;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AutomaticTestWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLAutomaticTest;
import org.objectweb.salome_tmf.plugins.JPFManager;
import org.objectweb.salome_tmf.plugins.core.TestDriver;

public class AutomaticTest extends Test{
        
    static ISQLAutomaticTest pISQLAutomaticTest = null;
        
    protected Script script;
    protected String testExtension = null;
    protected TestDriver driver = null; 
        
    public AutomaticTest(String name, String description, String testExtension) {
        super(name, description);
        script = null;
        this.testExtension = testExtension;
        if (pISQLAutomaticTest == null){
            pISQLAutomaticTest = Api.getISQLObjectFactory().getISQLAutomaticTest();
        }
                
    } 
        
    public AutomaticTest(AutomaticTestWrapper pTest) {
        super(pTest);
        script = null;
        this.testExtension = pTest.getTestExtension();
        if (pISQLAutomaticTest == null){
            pISQLAutomaticTest = Api.getISQLObjectFactory().getISQLAutomaticTest();
        }
    } 
        
    public AutomaticTest(TestList pList, AutomaticTestWrapper pTest) {
        super(pList, pTest);
        script = null;
        this.testExtension = pTest.getTestExtension();
        if (pISQLAutomaticTest == null){
            pISQLAutomaticTest = Api.getISQLObjectFactory().getISQLAutomaticTest();
        }
    } 
        
    @Override
    public void reloadFromDB(boolean base, Hashtable<String,Parameter> paramInModel, boolean attach)  throws Exception {
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(101, ApiConstants.LOADING);
                        
            commonReloadFromDB(base, paramInModel, attach);
            reloadScriptFromDB();
                        
            Api.commitTrans(transNuber);
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
    }
        
    public void reloadScriptFromDB()throws Exception {
        if (!isInBase()) {
            throw new Exception("AutomatedTest " + name + " is not in BDD");
        }
        ScriptWrapper pScriptWrapper = pISQLAutomaticTest.getTestScript(idBdd);
        if (pScriptWrapper != null){
            script = new Script(pScriptWrapper);
        } else {
            script = null;
        }
    }
        
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
        
        
    @Override
    public String getExtensionFromModel(){
        return testExtension;
    }
        
    public TestDriver getDriverFromModel(){
        return driver;
    }
        
    public TestDriver ActivateExtention(Extension pExtension, URL urlSalome, JPFManager jpf){ //!!! salome.ihm.SalomeTMF.jpf.activateExtension(pExtension);
        Util.log("[TestDriver]-> testExtension = " +testExtension + ", driver " + driver);
        if (testExtension != null && driver == null){
            // Create PluginExtention
            if (pExtension != null){
                // activation du plugin
                try {
                    driver = (TestDriver)jpf.activateExtension(pExtension);
                    driver.initTestDriver(urlSalome);
                    Util.log("[Automated Test] Driver ref : " +  driver);
                }catch (Exception e){
                    driver = null;
                    Util.err(e);
                }
            }
        }
        return driver;
    }
        
    /***************************** Basic Operation *********************************/

        
    @Override
    void addInDB(int idTestList) throws Exception {      
        if (isInBase()) {
            throw new Exception("AutomatedTest " + name + " is already in BDD");
        }
        idBdd = pISQLAutomaticTest.insert(idTestList, name, description, conceptorLogin, testExtension);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_TEST ,this);
    }
        
    /*   
     * used by TestList
     * WARNING This methode don't delete the test in campaign model
     * Use for that TestPlanData.deleteTestFromListFromModel()
     */
    @Override
    void deleteInModel() {
        script = null;
        clearAttachInModel();
        parameterList.clear();
    }   
        
        
    @Override
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
    /**************************** PARAMETERS ****************************************/
        
    @Override
    void deleteUseParameterInModel(Parameter pParm) {
        deleteUseParameterInModel(pParm.getNameFromModel());
    }
        
    /**************************** Script ****************************************/
        
    public void addScriptInDB(Script script, File file) throws Exception {
                
        if (!isInBase()) {
            throw new Exception("AutomatedTest " + name + " is not in BDD");
        }
        //Avant script.getLocalisation() a la pace de script.getNameFromModel()
        int id = pISQLAutomaticTest.addScript(idBdd, new SalomeFileWrapper(file), script.getDescriptionFromModel(), script.getNameFromModel(), script.getPlugArgFromModel(), script.getScriptExtensionFromModel() );
        script.setIdBdd(id);
    }
        
    public void addScriptInModel(Script newScript) {
        script = newScript;
        if (script != null){
            script.setScriptExtensionInModel(testExtension);
        } 
    } 
        
    public void addScriptInDBAndModel(Script script, File file) throws Exception {
        addScriptInDB(script, file);
        addScriptInModel(script);
    }
        
    public void deleteScriptInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("AutomatedTest " + name + " is not in BDD");
        }
        pISQLAutomaticTest.deleteScript(idBdd);
    }
        
    public void deleteScriptInModel(){
        script = null;
    }
        
    public void deleteScriptInDBAndModel() throws Exception{
        deleteScriptInDB();
        deleteScriptInModel();
    }
        
        
    public File getTestScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("AutomatedTest " + name + " is not in BDD");
        }
        return pISQLAutomaticTest.getTestScriptFile(idBdd).toFile();
                
    }
        
    public Script getScriptFromModel() {
        return script;
    }
        
    /**** COPIER/COLER ************/
        
    static AutomaticTest getModelCopie(AutomaticTest toCopie) throws Exception {
        AutomaticTest pCopie = new AutomaticTest(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel(), toCopie.testExtension);
        toCopie.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());
        ArrayList<Parameter> paramList = toCopie.getParameterListFromModel();
        for (Parameter param : paramList) {
            pCopie.setUseParamInModel(param);
        }
        //pCopie.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());
                
        ScriptWrapper pScriptWrapper = pISQLAutomaticTest.getTestScript(toCopie.getIdBdd());
        if (pScriptWrapper != null){
            File fScript = toCopie.getTestScriptFromDB();
            Script pScritp = new Script(pScriptWrapper);
            pScritp.setNameInModel(fScript.getName());
            pScritp.setIdBdd(-1);
            pCopie.script = pScritp;
        } else {
            pCopie.script = null;
        }                
        return pCopie;   
    }
        
    public static AutomaticTest copieIn(AutomaticTest toCopie, TestList pTestList) throws Exception {
        AutomaticTest pTest = AutomaticTest.getModelCopie(toCopie);
        String testName = toCopie.getNameFromModel();
        int i = 0;
        while (Test.isInBase(pTestList, testName)){
            testName = toCopie.getNameFromModel() + "_" + i;
            i++;
        }
        pTest.updateInModel(testName, pTest.getDescriptionFromModel());
        pTest.setConceptorLoginInModel(DataLoader.getCurrentUser().getLoginFromModel());
        /* 1 Ajout du test */
        pTestList.addTestInDBAndModel(pTest);
                
        /* 2 Ajout des paramtres */
        ArrayList<Parameter> paramList = pTest.getParameterListFromModel();
        for (Parameter param : paramList) {
            pTest.setUseParamInDB(param.getIdBdd());
        }
                
        try {
            /* Ajout des attachements */
            HashMap<String,Attachment> attachs = toCopie.getAttachmentMapFromModel();
            for (Attachment attach : attachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pTest.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pTest.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }
        }catch (Exception e){
            //Util.err(e);
        }
                
        /* 3 Ajout du Script de test*/
        //ScriptWrapper pScriptWrapper = pISQLAutomaticTest.getTestScript(toCopie.getIdBdd());
        try {
            File fScript = toCopie.getTestScriptFromDB();
            if (fScript != null){
                pTest.addScriptInDBAndModel(pTest.getScriptFromModel(), fScript);
            }
        }catch (Exception e){
            //Util.err(e);
        }
        Vector<Test> argNotifier = new Vector<Test> ();
        argNotifier.add(toCopie);
        argNotifier.add(pTest);
        Project.pCurrentProject.notifyChanged( ApiConstants.COPIE_TEST ,argNotifier);
                
        return pTest;
    }

    /**
     * Creates a copy of an automatic test in the same suite, the imported test takes
     * the parameter "oldName" as name
     * @param testDB
     * @param testName
     * @return
     */
    public static Test createCopyInDBAndModel(AutomaticTest toCopy,     String oldName) throws Exception {
        AutomaticTest pCopy = new AutomaticTest(oldName, toCopy.getDescriptionFromModel(), toCopy.testExtension);
        toCopy.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());             
        pCopy.setConceptorLoginInModel(DataLoader.getCurrentUser().getLoginFromModel());

        toCopy.getTestListFromModel().addTestInDBAndModel(pCopy);
                
        //add attachments
        HashMap<String,Attachment> attachs = toCopy.getAttachmentMapFromModel();
        for (Attachment attach : attachs.values()) {
            if (attach instanceof UrlAttachment) {
                pCopy.addAttachementInDBAndModel(attach);
            } else {
                FileAttachment fileAttach = (FileAttachment)attach;
                File attachFile = fileAttach.getFileFromDB(null);
                FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                pCopy.addAttachementInDBAndModel(toAdd);
                attachFile.delete();
            }
        }
                
        //parameters
        ArrayList<Parameter> paramList = toCopy.getParameterListFromModel();
        for (Parameter param : paramList) {
            pCopy.setUseParamInDBAndModel(param);
        }
                
        //script
        ScriptWrapper pScriptWrapper = pISQLAutomaticTest.getTestScript(toCopy.getIdBdd());
        File fScript = null;
        if (pScriptWrapper != null){
            fScript = toCopy.getTestScriptFromDB();
            Script pScript = new Script(pScriptWrapper);
            pScript.setNameInModel(fScript.getName());
            pScript.setIdBdd(-1);
            pCopy.script = pScript;
        } else {
            pCopy.script = null;
        }
        pCopy.addScriptInDBAndModel(pCopy.getScriptFromModel(), fScript);
                
                
        return pCopy;
    }
}
