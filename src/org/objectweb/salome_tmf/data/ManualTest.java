/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.ManualTestWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLManualTest;

public class ManualTest extends Test{
        
    static ISQLManualTest pISQLManualTest = null;
        
    private ArrayList<Action> actionList = new ArrayList<Action>();
        
    private boolean actionLoaded = false;
        
        
        
    public ManualTest(String name, String description) {
        super(name, description);
        //actionList = new ArrayList();
        if (pISQLManualTest == null){
            pISQLManualTest = Api.getISQLObjectFactory().getISQLManualTest();
        }
        //actionLoaded = false;
    }   
        
    public ManualTest(ManualTestWrapper pTest) {
        super(pTest);
        //actionList = new ArrayList();
        if (pISQLManualTest == null){
            pISQLManualTest = Api.getISQLObjectFactory().getISQLManualTest();
        }
    }   
        
    public ManualTest(TestList pList, ManualTestWrapper pTest) {
        super(pList, pTest);
        //actionList = new ArrayList();
        if (pISQLManualTest == null){
            pISQLManualTest = Api.getISQLObjectFactory().getISQLManualTest();
        }
    }
        
    @Override
    public void reloadFromDB(boolean base, Hashtable<String,Parameter> paramInModel, boolean attach)  throws Exception {
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(101, ApiConstants.LOADING);
            commonReloadFromDB(base, paramInModel, attach);
            if (!DataLoader.isDynamic){
                reloadActionFromDB(paramInModel, attach); //if dynamique
            }
            Api.commitTrans(transNuber);
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
    }
        
    @Override
    public void  clearCache(){
        if (doClearCache){
            actionLoaded = false;
            if (actionList != null){
                actionList.clear();
                actionList = null;
            }
            clearAttachCache();
        }
    }
        
    /**************************************************************************/
    /**                                         ACCESSEURS ET MUTATEURS                                                 ***/
    /**************************************************************************/
        
    void loadActionFromDB(boolean withAttach){
        try {
            reloadActionFromDB(Project.pCurrentProject.getParameterSetFromModel(), withAttach);
        } catch (Exception e){
                        
        }
    }
        
    public ArrayList<Action> getActionListFromModel(boolean withAttach) { //TO BE Dynamic
        if (!actionLoaded){
            loadActionFromDB(withAttach);
        }
        return actionList;
    }
        
    /*public void setActionListInModel(ArrayList list) { //TO BE Dynamic
      actionList = list;
      } */
        
    @Override
    public String getExtensionFromModel(){
        return "";
    }
        
    /********************************* Basic Operation ****************************/
    @Override
    /* used by TestList */
    void addInDB(int idTestList) throws Exception {
        if (isInBase()) {
            throw new Exception("Test " + name + " already in BDD");
        }
        if (idTestList == -1){
            idTestList = getTestListFromModel().getIdBdd();
            if (idTestList == -1){
                throw new Exception("TestList not defined");
            }
        }
        idBdd = pISQLManualTest.insert(idTestList, name, description, conceptorLogin, "");
        order = getOrderFromDB();
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_TEST ,this);
    }
        
        
    /* used by TestList */
    /*  
     * WARNING This methode don't delete the test in campaign model
     * Use for that TestPlanData.deleteTestFromListFromModel()
     */
    @Override
    void deleteInModel() {
        clearAttachInModel();
        parameterList.clear();
        parameterList = null;
        if (actionList != null){
            for (int i=0; i < actionList.size(); i++) {
                Action pAction = actionList.get(i);
                pAction.deleteInModel();
                Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_ACTION ,pAction);
            }
            actionList.clear();
            actionList = null;
        }
        actionLoaded = false;
    }
        
    /* used by TestList */
    /*  
     * WARNING This methode don't delete the test in campaign model
     * Use for that TestPlanData.deleteTestFromListFromModel()
     */
    @Override
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();        
    }
        
    /******************** ACTIONS *************************/
        
    public void reloadActionFromDB(Hashtable<String,Parameter> paramInModel, boolean attach) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        if (actionList != null){
            for (int i=0; i < actionList.size(); i++) {
                deleteActionInModel(actionList.get(i));
            }
            actionList.clear();
        } else { 
            actionList = new ArrayList<Action>();
        }
        actionLoaded = true;
        ActionWrapper[] actionVector = pISQLManualTest.getTestActions(idBdd);
        for (int i = 0; i < actionVector.length; i++) {
            Action action = new Action(actionVector[i], this);
            action.reloadFromDB(false, paramInModel, attach);
            addActionInModel(action);
        }        
    }
        
    public void addActionInModel(Action action) { //TO BE Dynamic
        if (!actionLoaded){
            loadActionFromDB(false);
        }
        actionList.add(action);
        Enumeration<Parameter> iterator = action.getParameterHashSetFromModel().elements();
        while (iterator.hasMoreElements()){
            Parameter param = iterator.nextElement();
            if (getUsedParameterFromModel(param.getNameFromModel()) == null){
                setUseParamInModel(param);
            }
        }
        action.addInModel(this); 
    }
        
        
    public void addActionInDB(Action action) throws Exception{
        action.addInDB(this);
    }
        
    public void addActionInDBAndModel(Action action) throws Exception {
        addActionInDB(action);
        addActionInModel(action);
    }
        
    public void deleteActionInDBModel(Action pAction)  throws Exception { 
        pAction.deleteInDB();
        deleteActionInModel(pAction);
    }
        
    public void deleteActionInModel(Action pAction) { //TO BE Dynamic
        if (actionLoaded){
            actionList.remove(pAction);
            pAction.deleteInModel();
            pAction.clearCache();
        }
        /*
          actionList.remove(pAction);
          pAction.deleteInModel();
        */
    }   
        
    public void deleteActionInDB(Action pAction) throws Exception{
        pAction.deleteInDB();
    }
        
    public Action getActionFromModel(String name) { //TO BE Dynamic
        if (!actionLoaded){
            loadActionFromDB(false);
        }
        for (int i=0; i < actionList.size(); i++) {
            if (actionList.get(i).getNameFromModel().equals(name)) {
                return actionList.get(i);
            }
        }
        return null;
    }
        
    public boolean hasActionNameInModel(String name) { //TO BE Dynamic
        if (!actionLoaded){
            loadActionFromDB(false);
        }
        for (int i=0; i < actionList.size(); i++) {
            if (actionList.get(i).getNameFromModel().equals(name)) {
                return true;
            }
        }
        return false;
    } 
        
    /******************** PARAMETERS **********************/
        
    @Override
    public void deleteUseParameterInModel(Parameter pParam) {
        Util.log("[ManualTest->deleteUseParameterInModel] Delete Use Parameter " + pParam + ", in test " + name);
        if (actionLoaded){
            for (int i = 0; i < getActionListFromModel(false).size(); i++) {
                Action action = (getActionListFromModel(false).get(i)); 
                action.deleteUseParamInModel(pParam);
            }
        }
        deleteUseParameterInModel(pParam.getNameFromModel());
    }
    /* + Heritage de Test */
        
    /**** COPIER/COLER ************/
        
    static ManualTest getModelCopie(ManualTest toCopie) throws Exception {
        ManualTest pCopie = new ManualTest(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
        toCopie.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());
        ArrayList<Parameter> paramList = toCopie.getParameterListFromModel();
        for (Parameter param : paramList) {
            pCopie.setUseParamInModel(param);
        }
        //pCopie.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());
                
        ActionWrapper[] actionVector = pISQLManualTest.getTestActions(toCopie.getIdBdd());
        for (int i = 0; i < actionVector.length; i++) {
            ActionWrapper pActionWrapper = actionVector[i];
                         
            //pActionWrapper.setIdBDD(-1);
            Action action = new Action(pActionWrapper, pCopie);
            action.reloadFromDB(false, DataLoader.getCurrentProject().getParameterSetFromModel(), false);
            action.setIdBdd(-1);
            pCopie.addActionInModel(action);
        }
        return pCopie;
    }
        
        
    public static ManualTest copieIn(ManualTest toCopie, TestList pTestList) throws Exception {
        ManualTest pTest = ManualTest.getModelCopie(toCopie);
        String testName = toCopie.getNameFromModel();
        int i = 0;
        while (Test.isInBase(pTestList, testName)){
            testName = toCopie.getNameFromModel() + "_" + i;
            i++;
        }
        pTest.updateInModel(testName, pTest.getDescriptionFromModel());
        pTest.setConceptorLoginInModel(DataLoader.getCurrentUser().getLoginFromModel());
        /* 1 Ajout du test */
        pTestList.addTestInDBAndModel(pTest);
                
                
        /* 2 Ajout des paramtres */
        ArrayList<Parameter> paramList = pTest.getParameterListFromModel();
        for (Parameter param : paramList) {
            pTest.setUseParamInDB(param.getIdBdd());
        }
                
        try {
            /* Ajout des attachements */
            HashMap<String,Attachment> attachs = toCopie.getAttachmentMapFromModel();
            for (Attachment attach : attachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pTest.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pTest.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }
        }catch (Exception e){
            //Util.err(e);
        }
                
        /* 3 Ajout des Actions */
        //pTest.loadActionFromDB(false);
        for (Action pAction : pTest.actionList) {
            pTest.addActionInDB(pAction);
            Hashtable<String,Parameter> parameterHashTable  = pAction.getParameterHashSetFromModel();
            Enumeration<Parameter> enumElement = parameterHashTable.elements();
            while (enumElement.hasMoreElements()){
                Parameter param = enumElement.nextElement();
                pAction.setUseParamInDB(param.getIdBdd());
            }
        }
        Vector<Test> argNotifier = new Vector<Test> ();
        argNotifier.add(toCopie);
        argNotifier.add(pTest);
        Project.pCurrentProject.notifyChanged( ApiConstants.COPIE_TEST ,argNotifier);
                
        return pTest;
    }
        
    /**
     * Creates a copy of a manual test in the same suite, the imported test takes
     * the parameter "oldName" as name
     * @param toCopy
     * @param newNameOfOldTest
     * @return
     */
    public static ManualTest createCopyInDBAndModel(ManualTest toCopy, String oldName) throws Exception {
        ManualTest pCopy = new ManualTest(oldName, toCopy.getDescriptionFromModel());           
        pCopy.setConceptorLoginInModel(DataLoader.getCurrentUser().getLoginFromModel());
                                
        //add to test list
        toCopy.getTestListFromModel().addTestInDBAndModel(pCopy);
                
        //add attachments
        HashMap<String,Attachment> attachs = toCopy.getAttachmentMapFromModel();
        for (Attachment attach : attachs.values()) {
            if (attach instanceof UrlAttachment) {
                pCopy.addAttachementInDBAndModel(attach);
            } else {
                FileAttachment fileAttach = (FileAttachment)attach;
                File attachFile = fileAttach.getFileFromDB(null);
                FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                pCopy.addAttachementInDBAndModel(toAdd);
                attachFile.delete();
            }
        }
                
        //used parameter
        toCopy.reloadUSeParameterFromDB(DataLoader.getCurrentProject().getParameterSetFromModel());
        ArrayList<Parameter> paramList = toCopy.getParameterListFromModel();
        for (Parameter param : paramList) {
            pCopy.setUseParamInDBAndModel(param);
        }
                
        //actions
        ActionWrapper[] actionVector = pISQLManualTest.getTestActions(toCopy.getIdBdd());
        for (int i = 0; i < actionVector.length; i++) {
            ActionWrapper pActionWrapper = actionVector[i];

            Action pAction = new Action(pActionWrapper, pCopy);
            pAction.reloadFromDB(false, DataLoader.getCurrentProject().getParameterSetFromModel(), false);
            pAction.setIdBdd(-1);
            pCopy.addActionInDBAndModel(pAction);
            Hashtable<String,Parameter> parameterHashTable  = pAction.getParameterHashSetFromModel();
            for (Parameter param : parameterHashTable.values()) {
                pAction.setUseParamInDB(param.getIdBdd());                              
            }                   
        }
                
        ArrayList<Action> actionList = pCopy.getActionListFromModel(true);
                
        for (Action action : actionList) {
            HashMap<String,Attachment> actionAttachs = action.getAttachmentMapFromModel();
            for (Attachment attach : actionAttachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pCopy.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pCopy.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }                   
        }
                
        return pCopy;
    }
}
