/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.ActionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLAction;

public class Action extends WithAttachment {
    static ISQLAction pISQLAction = null;
        
    private String awaitedResult;
    private int orderIndex;
    private Hashtable parameterHashTable;
    private Test pTest = null;
        
    public Action(Test pTest, String name, String description) {
        super(name, description);
        awaitedResult = "";
        orderIndex = 0;
        parameterHashTable = new Hashtable();
        this.pTest = pTest;
        if (pISQLAction == null){
            pISQLAction = Api.getISQLObjectFactory().getISQLAction();
        }
    }
        
    public Action(ActionWrapper pAction, Test pTest) {
        this(pTest, pAction.getName(), pAction.getDescription());
        awaitedResult = pAction.getAwaitedResult();
        orderIndex = pAction.getOrderIndex();
        idBdd = pAction.getIdBDD();
    } 
        
    public Action(Action pAction, Test pTest) {
        this(pTest,  pAction.getNameFromModel(), pAction.getDescriptionFromModel());
        awaitedResult = pAction.getAwaitedResultFromModel();
    } // Fin du constructeur Action/1
        
        
    protected void reloadBaseFromDB() throws Exception {
        if (isInBase()) {
            throw new Exception("Action " + name + " is already in BDD");
        }
        ActionWrapper pActionWrapper = pISQLAction.getActionWrapper(idBdd);
        awaitedResult = pActionWrapper.getAwaitedResult();
        orderIndex = pActionWrapper.getOrderIndex();
        name = pActionWrapper.getName();
        description = pActionWrapper.getDescription();
    }
        
        
    public void reloadFromDB(boolean base, Hashtable paramsInModel, boolean attach) throws Exception {
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(101, ApiConstants.LOADING);
            if (base){
                reloadBaseFromDB();
            }
            reloadUsedParameterFromDB(paramsInModel);
            if (attach){
                reloadAttachmentDataFromDB(false);
            }
            Api.commitTrans(transNuber);
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
    }
        
    @Override
    public void clearCache() {
        /* TODO ClearAttachement */
                
    }
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
        
    public String getAwaitedResultFromModel() {
        return awaitedResult;
    }
        
    public void setAwaitedResultInModel(String string) {
        awaitedResult = string;
    }
        
        
    public int getOrderIndexFromModel() {
        return orderIndex;
    }
        
    public void setOrderIndex(int i) {
        orderIndex = i;
    }
        
    public Test getTest(){
        return pTest;
    }
        
    /////////////////////////////// Basic Operation /////////////////////////
        
    /* Used by Manuel Test */
    void addInDB(Test pTest) throws Exception {
        //boolean needUpdate = false;
        if (isInBase()) {
            throw new Exception("Action " + name + " is already in BDD");
        }
        if (!pTest.isInBase()){
            throw new Exception("Test " + pTest.getNameFromModel() + " is not in BDD");
        }
        int id = pISQLAction.insert(pTest.getIdBdd(), name, description, awaitedResult);
        setIdBdd(id);
        orderIndex =  pISQLAction.getActionWrapper(id).getOrder();
        /*if (orderIndex != rowCount){
          needUpdate = true; 
          }
          return needUpdate;*/
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_ACTION ,this);
                
    }
        
    /* Used by Manuel Test */
    void addInModel(Test pTest){
        this.pTest = pTest;
    }
        
    /* Used by Manuel Test */
    void addInDBAndModel(Test pTest)throws Exception {
        //boolean res;
        addInDB(pTest);
        addInModel(pTest);
        //return res;
    }
        
        
        
    public void updateInDB(String newActionName, String newActionDesc, String newActionResAttendu) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        pISQLAction.update(idBdd, newActionName, newActionDesc,  newActionResAttendu );
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_ACTION ,this, new String(name), newActionName);
                
    }
        
    public void updateInModel(String newActionName, String newActionDesc, String newActionResAttendu) {
        setNameInModel(newActionName);
        updateDescriptionInModel(newActionDesc);
        setAwaitedResultInModel(newActionResAttendu);
    }
        
    public void updateInDBAndModel(String newActionName, String newActionDesc, String newActionResAttendu) throws Exception {
        updateInDB(newActionName, newActionDesc, newActionResAttendu);
        updateInModel(newActionName, newActionDesc, newActionResAttendu);
    } 
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc, awaitedResult);
        updateInModel(newName, newDesc, awaitedResult);
    }
        
    public void updateOrderInDBAndModel(boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        orderIndex = pISQLAction.updateOrder(idBdd, inc);     
    }
        
    /* Used by Manuel Test */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        pISQLAction.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_ACTION ,this);
    }
        
    /* Used by Manuel Test */
    void deleteInModel(){
        pTest=null;
        parameterHashTable.clear();
        clearAttachInModel();
    }
        
    /* Used by Manuel Test */
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
    //////////////// PARAMETERS ////////////////////////
        

    public void setParameterHashSetInModel(HashSet set) {
        parameterHashTable.clear();
        for (Iterator iter = set.iterator(); iter.hasNext();) {
            Parameter param = (Parameter)iter.next();
            parameterHashTable.put(param.getNameFromModel(), param);
        }
                
    }
    public void setParameterHashtableInModel(Hashtable table) {
        parameterHashTable.clear();
        parameterHashTable = table;
    }
        
    public Hashtable getCopyOfParameterHashTableFromModel(){
        Hashtable copyParameter = new Hashtable();
        Enumeration enumKey = parameterHashTable.keys();
        while (enumKey.hasMoreElements()){
            Object key = enumKey.nextElement();
            copyParameter.put(key, parameterHashTable.get(key));
        }
        return copyParameter;
    }
        
    public void reloadUsedParameterFromDB(Hashtable parametersInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        ParameterWrapper[] paramActionArray = pISQLAction.getParamsUses(idBdd);
        for (int i = 0; i < paramActionArray.length; i++) {
            Parameter param = null;
            ParameterWrapper pParameterWrapper = paramActionArray[i];
            if (parametersInModel != null){
                param = (Parameter)parametersInModel.get(pParameterWrapper.getName());
                if (param == null){
                    param = new Parameter(pParameterWrapper);
                }
            }
            setUseParamInModel(param);
        }
    }
        
    public void setUseParamInModel(Parameter pParam) {
        parameterHashTable.put(pParam.getNameFromModel(), pParam);
        if (pTest != null) {
            if (pTest.getUsedParameterFromModel(pParam.getNameFromModel()) == null){
                pTest.setUseParamInModel(pParam);
            }
        }
    }
        
    public void setUseParamInDB(int paramId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        pISQLAction.addUseParam(idBdd,paramId);
    }
        
    public void setUseParamInDBAndModel(Parameter pParam) throws Exception {
        //DB
        setUseParamInDB(pParam.getIdBdd());
        //model
        setUseParamInModel(pParam);
    }
        
    public void deleteUseParamInDB(int paramId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        pISQLAction.deleteParamUse(idBdd, paramId);
    }
        
    public void deleteUseParamInDBAndModel(Parameter pParam)  throws Exception {
        deleteUseParamInDB(pParam.getIdBdd());
        deleteUseParamInModel(pParam);
    }
        
    public void deleteUseParamInModel(Parameter pParam) {
        // Clean Action
        String newDesc = clearStringOfParameter(getDescriptionFromModel(), pParam);
        String newResult = clearStringOfParameter(getAwaitedResultFromModel(), pParam);
        description = newDesc;
        awaitedResult = newResult;
        Object o= parameterHashTable.remove(pParam.getNameFromModel());
        Util.log("[Action->deleteUseParamInModel] Delete Use Parameter " + pParam + ", in Action " + name + " res is " +o);
                
    }
        
        
        
    public Parameter getParameterFromModel(String name) {
        Enumeration paramList = parameterHashTable.elements();
        while (paramList.hasMoreElements()){
            Parameter param = (Parameter)paramList.nextElement();
            if (param.getNameFromModel().equals(name)) {
                return param;
            }
        }
        return null;
    }
        
    public Parameter getParameterFromDB(String name) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        HashSet result = getParameterHashSetFromDB();
        for (Iterator iter = result.iterator(); iter.hasNext(); ) {
            Parameter param = (Parameter)iter.next();
            if (param.getNameFromModel().equals(name)) {
                return param;
            }
        }
        return null;
    }
        
    public HashSet getParameterHashSetFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        HashSet result = new HashSet();
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        ParameterWrapper[] listParamWrapper =  pISQLAction.getParamsUses(idBdd);
        for (int i = 0; i <  listParamWrapper.length; i++){
            result.add(new Parameter(listParamWrapper[i]));
        }
        return result;
    }
        
    public Vector getParameterVectorFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        Vector result = new Vector();
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        ParameterWrapper[] listParamWrapper =  pISQLAction.getParamsUses(idBdd);
        for (int i = 0; i <  listParamWrapper.length; i++){
            result.add(new Parameter(listParamWrapper[i]));
        }
        return result;
    }
    public Hashtable getParameterHashSetFromModel() {
        return parameterHashTable;
    } 
        
    ////////////// ATTACHEMENT ///////////////////////
        
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLAction.addFileAttach(idBdd,new SalomeFileWrapper(f),file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        int id = pISQLAction.addUrlAttach(idBdd, url.getNameFromModel(),url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
        
    @Override
    public void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        pISQLAction.deleteAttachment(idBdd, attachId);
    }
        
        
        
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach)throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
                
    }
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        FileAttachementWrapper[] fawArray = pISQLAction.getAllAttachFile(idBdd);
        Vector result = new Vector();
        for(int i = 0; i < fawArray.length; i++) {
            result.add(fawArray[i]);
        }
        return result;
    }
        
        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Action " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] uawArray = pISQLAction.getAllAttachUrl(idBdd);
        Vector result = new Vector();
        for(int i = 0; i < uawArray.length; i++) {
            result.add(uawArray[i]);
        }
        return result;
    }
        
        
    //////// PROTECTED //////////////
    protected String clearStringOfParameter(String prtString, Parameter pParam) {
        String result = prtString;
        result = result.replaceAll("[$]" + pParam.getNameFromModel() + "[$]", "");
        return result;
    }
        
    public static boolean isInBase(Test pTest, String actionName) {
        try  {
            int id = pISQLAction.getID(pTest.getIdBdd(), actionName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return  pISQLAction.getID(pTest.getIdBdd(), name) == idBdd;
    }
}
