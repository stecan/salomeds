/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionAttachmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLCampaign;

public class Campaign extends WithAttachment {
        
    static ISQLCampaign pISQLCampaign = null;
    protected ArrayList suiteList;
    protected ArrayList testList;
    protected Hashtable testAssigned;
    protected ArrayList familyList;
    protected Date creationDate;
    protected String conceptor;
    protected ArrayList executionList;
    protected ArrayList dataSetList;
    protected int order;
        
    public Campaign(String name, String description) {
        super(name, description);
        suiteList = new ArrayList();
        testList = new ArrayList();
        familyList = new ArrayList();
        creationDate = new Date(Calendar.getInstance()
                                .getTimeInMillis());
        conceptor = "";
        executionList = new ArrayList();
        dataSetList = new ArrayList();
        testAssigned = new Hashtable();
        if (pISQLCampaign == null){
            pISQLCampaign = Api.getISQLObjectFactory().getISQLCampaign();
        }
                
    } 
        
    public Campaign(CampaignWrapper pCampaign) {
        super(pCampaign.getName(), pCampaign.getDescription());
        creationDate = pCampaign.getDate();
        conceptor = pCampaign.getConceptor();
        idBdd = pCampaign.getIdBDD();
        suiteList = new ArrayList();
        testList = new ArrayList();
        familyList = new ArrayList();
        executionList = new ArrayList();
        dataSetList = new ArrayList();
        testAssigned = new Hashtable();
        order = pCampaign.getOrder();
        if (pISQLCampaign == null){
            pISQLCampaign = Api.getISQLObjectFactory().getISQLCampaign();
        }
    } 
        
    public static Campaign copieIn(Campaign toCopie, User pUser) throws Exception {
        Campaign pCopie = new Campaign(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
        String campName = toCopie.getNameFromModel();
        int i = 0;
        while (Campaign.isInBase(DataLoader.getCurrentProject(), campName)){
            campName = toCopie.getNameFromModel() + "_" + i;
            i++;
        }
        pCopie.updateInModel(campName, toCopie.getDescriptionFromModel());
        DataLoader.getCurrentProject().addCampaignInDBandModel(pCopie);
        ArrayList testList = toCopie.getTestListFromModel();
        int size = testList.size();
        for (i = 0; i < size ; i++){
            Test pTest = (Test)testList.get(i);
            int userID = pUser.getIdBdd();
            pCopie.importTestInDBAndModel(pTest, userID);
        }
        pCopie.setConceptorInModel(pUser.getLoginFromModel());
                

        try {
            /* Ajout des attachements */
            HashMap<String,Attachment> attachs = toCopie.getAttachmentMapFromModel();
            for (Attachment attach : attachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pCopie.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pCopie.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }
        }catch (Exception e){
            //Util.err(e);
        }
                
        Vector<Campaign> argNotifier = new Vector<Campaign> ();
        argNotifier.add(toCopie);
        argNotifier.add(pCopie);
        Project.pCurrentProject.notifyChanged( ApiConstants.COPIE_CAMPAIGN ,argNotifier);
                
        return pCopie;
    }
        
    public Date getDateFromModel() {
        return creationDate;
    } 
        
    public String getConceptorFroModel() {
        return conceptor;
    }
        
    public void setConceptorInModel(String name) {
        conceptor = name;
    }
        
    public int getAssignedUserID(Test pTest) {
        int id = -1;
        Integer userID = (Integer) testAssigned.get(new Integer(pTest.getIdBdd()));
        if (userID != null){
            id = userID.intValue();
        }
        return id;
    }
        
    public void clearAssignedUserForTest(){
        testAssigned.clear();
    }
        
    public void setDateInModel(Date date) {
        creationDate = date;
    } 
        
        
    public Family containsFamilyInModel(String name) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).getNameFromModel().equals(name)) {
                return (Family) familyList.get(i);
            }
        }
        return null;
    }
        
    public boolean containsFamilyInModel(Family family) {
        for (int i = 0; i < familyList.size(); i++) {
            if (((Family) familyList.get(i)).equals(family)) {
                return true;
            }
        }
        return false;
    } 
        
        
    public boolean containsTestListInModel(TestList testList) {
        for (int i = 0; i < suiteList.size(); i++) {
            if (((TestList) suiteList.get(i)).equals(testList)) {
                return true;
            }
        }
        return false;
    } 
        
        
    public TestList containsTestListInModel(String name) {
        for (int i = 0; i < suiteList.size(); i++) {
            if (((TestList) suiteList.get(i)).getNameFromModel().equals(name)) {
                return (TestList) suiteList.get(i);
            }
        }
        return null;
    } 
        
        
    public boolean containsTestInModel(Test test) {
        for (int i = 0; i < testList.size(); i++) {
            if (((Test) testList.get(i)).equals(test)) {
                return true;
            }
        }
        return false;
    }
        
    public Test containsTestInModel(int testId) {
        for (int i = 0; i < testList.size(); i++) {
            Test pTest  = (Test) testList.get(i);
            if (pTest.getIdBdd() == testId) {
                return pTest;
            }
        }
        return null;
    }
        
    public boolean containsTestInModel(String testName) {
        for (int i = 0; i < testList.size(); i++) {
            if (((Test) testList.get(i)).getNameFromModel().equals(testName)) {
                return true;
            }
        }
        return false;
    } 
        
        
    public boolean containsExecutionResultInModel() {
        for (int i = 0; i < executionList.size(); i++) {
            if (((Execution) executionList.get(i)).getExecutionResultListFromModel()
                .size() > 0) {
                return true;
            }
        }
        return false;
    } 
        
    public boolean containsExecutionInModel(String execName) {
        for (int i = 0; i < executionList.size(); i++) {
            if (((Execution) executionList.get(i)).getNameFromModel().equals(execName)) {
                return true;
            }
        }
        return false;
    }
        
    /************* Basic Operation ****************/
        
        
    public void addInDB(int idProject, int idUser) throws Exception {
        if (isInBase()) {
            throw new Exception("Campaign " + name + " is already in BDD");
        }
        idBdd = pISQLCampaign.insert(idProject, name, description, idUser);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_CAMPAIGN ,this);
                
    }
        
    /**
     * 
     * @return true if model is inconistant
     * @throws Exception
     */
    public boolean reloadCampain(ArrayList testInModel, ArrayList envInModel,  Hashtable paramInModel) throws Exception {
        boolean ret = false;
        CampaignWrapper pCampaignWrapper = pISQLCampaign.getCampaign(idBdd);
        updateInModel(pCampaignWrapper.getName(), pCampaignWrapper.getDescription());
        ret = reloadTestInCampaign(testInModel);
        if (ret == true){
            return ret;
        }
        reloadDataSetFromBase(paramInModel);
        reloadExcecutionInModel(envInModel, paramInModel);
        return ret;
    }
        
    public void reloadTestCampain(ArrayList testInModel) throws Exception {
        reloadTestInCampaign(testInModel);
    }
        
    int importTestInDB(int testId, int userID) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
                
        return pISQLCampaign.importTest(idBdd, testId, userID);
    }
        
    boolean importTestInDB(int testId, int order, int userID) throws Exception {
        boolean needUpdate = false;
        if (isInBase()) {
            throw new Exception("Campaign " + name + " is already in BDD");
        }
        int maxOrder = importTestInDB (testId, userID);
        if (order != maxOrder) {
            needUpdate = true;
        }
        return needUpdate;
                
    }
        
    public void importTestInDBAndModel(Test pTest, int userID) throws Exception {
        importTestInDB(pTest.getIdBdd(), userID);
        addTestInModel(pTest, userID);
    }
        
        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.update(idBdd, newName, newDesc);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_CAMPAIGN ,this, new String(name), newName);
    }
        
    public void updateInModel(String newName, String newDesc){
        setNameInModel(newName);
        updateDescriptionInModel(newDesc);
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }
        
    public void updateOrderInDBAndModel(boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateOrder(idBdd, inc);
    }
        
        
    public void updateTestSuiteOrderInDB(int idSuite, boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateTestSuiteOrder(idBdd, idSuite, inc);
    }
        
    public void updateTestFamilyOrderInDB(int idFamily, boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateTestFamilyOrder(idBdd, idFamily, inc);
    }
        
    public void updateTestOrderInDB(int testId, boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateTestOrder(idBdd, testId, inc);
    }
        
    public void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_CAMPAIGN ,this);
    }
        
    void updateTestAssignationInDB(int idTest, int idUser) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateTestAssignation(idBdd, idTest, idUser);
    }
        
    void updateTestAssignationInModel(int idTest, int idUser)  {
        //if (containsTestInModel(idTest) != null){;
        testAssigned.put(new Integer(idTest), new Integer(idUser));
        //}
    }
        

    public void updateTestAssignationInDBAndModel(int idTest, int idUser) throws Exception {
        updateTestAssignationInDB(idTest, idUser);
        updateTestAssignationInModel(idTest, idUser);
    }
        
        
    void updateSuiteAssignationInDB(int idSuite, int idUser) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateSuiteAssignation(idBdd, idSuite, idUser);
    }
        
    void updateSuiteAssignationInModel(int idSuite, int idUser)  {
        int size = testList.size();
        for (int i = 0 ; i < size ; i++){
            Test pTest = (Test) testList.get(i);
            if (pTest.getTestListFromModel().getIdBdd() == idSuite){
                updateTestAssignationInModel(pTest.getIdBdd(), idUser);
            }
        }
        /*int size = suiteList.size();
          for (int i = 0 ; i < size ; i++){
          TestList pTestList = (TestList) suiteList.get(i);
          if (pTestList.getIdBdd() == idSuite){
          ArrayList listOfTest = pTestList.getTestListFromModel();
          int size2 = listOfTest.size();
          for (int j=0; j < size2 ; j++){
          Test pTest = (Test) listOfTest.get(j);
          if (containsTestInModel(pTest.getIdBdd()) != null){
          updateTestAssignationInModel(pTest.getIdBdd(), idUser);
          }
          }
          i = size; // Break IF 
          }
          }*/
    }
        

    public void updateSuiteAssignationInDBAndModel(int idSuite, int idUser) throws Exception {
        updateSuiteAssignationInDB(idSuite, idUser);
        updateSuiteAssignationInModel(idSuite, idUser);
    }
        
    void updateFamilyAssignationInDB(int idFamily, int idUser) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateFamilyAssignation(idBdd, idFamily, idUser);
    }
        
    void updateFamilyAssignationInModel(int idFamily, int idUser)  {
        int size = testList.size();
        for (int i = 0 ; i < size ; i++){
            Test pTest = (Test) testList.get(i);
            if (pTest.getTestListFromModel().getFamilyFromModel().getIdBdd() == idFamily){
                updateTestAssignationInModel(pTest.getIdBdd(), idUser);
            }
        }
        /*int size = suiteList.size();
          for (int i = 0 ; i < size ; i++){
          TestList pTestList = (TestList) suiteList.get(i);
          if (pTestList.getFamilyFromModel().getIdBdd() == idFamily){
          ArrayList listOfTest = pTestList.getTestListFromModel();
          int size2 = listOfTest.size();
          for (int j=0; j < size2 ; j++){
          Test pTest = (Test) listOfTest.get(j);
          if (containsTestInModel(pTest.getIdBdd()) != null){
          updateTestAssignationInModel(pTest.getIdBdd(), idUser);
          }
          }
          i = size; // Break IF 
          }
          }*/
    }
        

    public void updateFamilyAssignationInDBAndModel(int idFamily, int idUser) throws Exception {
        updateFamilyAssignationInDB(idFamily, idUser);
        updateFamilyAssignationInModel(idFamily, idUser);
    }
        
    void updateCampaignAssignationInDB(int idUser) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.updateCampagneAssignationRef(idBdd,  idUser);
    }
        
    void updateCampaignAssignationInModel(int idUser)  {
        int size = testList.size();
        for (int i = 0 ; i < size ; i++){
            Test pTest = (Test) testList.get(i);
            updateTestAssignationInModel(pTest.getIdBdd(), idUser);
        }
    }
        

    public void updateCampaignAssignationInDBAndModel(int idUser) throws Exception {
        updateCampaignAssignationInDB(idUser);
        updateCampaignAssignationInModel(idUser);
    }
        
        
        
    public void deleteInModel() {
        testList.clear();
        suiteList.clear();
        familyList.clear();             
        purgeExcecutionInModel(true);
        executionList.clear();
        purgeDataSetInModel();
        dataSetList.clear();
        clearAttachInModel();
    }
        
    public void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }
        
        
    /******************* About Family **********************/
    public void setFamilyListInModel(ArrayList list) {
        familyList = list;
    } 
        
        
    void addFamilyInModel(Family family) {
        if (!familyList.contains(family)) {
            familyList.add(family);
        }
    } 
        
    public void addFamilyTestListAndTest(Family family) {
        for (int i = 0; i < family.getSuiteListFromModel().size(); i++) {
            for (int j = 0; j < ((TestList) family.getSuiteListFromModel().get(i))
                     .getTestListFromModel().size(); j++) {
                if (!testList.contains(((TestList) family
                                        .getSuiteListFromModel().get(i)).getTestListFromModel().get(j))) {
                    this.testList.add(((TestList) family.getSuiteListFromModel().get(
                                                                                     i)).getTestListFromModel().get(j));
                }
            }
            if (!suiteList.contains((family.getSuiteListFromModel()
                                     .get(i)))) {
                this.suiteList.add((family.getSuiteListFromModel().get(
                                                                       i)));
            }
        }
        if (!familyList.contains(family)) {
            familyList.add(family);
        }
    } 

    public boolean removeFamily(Family family) {
        for (int j = suiteList.size() - 1; j >= 0; j--) {
            if (((TestList) suiteList.get(j)).getFamilyFromModel().getNameFromModel().equals(
                                                                                             family.getNameFromModel())) {
                TestList list = (TestList) suiteList.get(j);
                //removeTestList(list);
                suiteList.remove(j);
                for (int k = testList.size() - 1; k >= 0; k--) {
                    if (((Test) testList.get(k)).getTestListFromModel().equals(list)) {
                        for (int i = 0 ; i < executionList.size() ; i++) {
                            ((Execution)executionList.get(i)).deleteTestFromExecInModel(((Test)testList.get(k)));
                        }
                        testList.remove(k);
                    }
                }
            }
        }
        familyList.remove(family);
                
        purgeExcecutionInModel(true);
                
        return (testList.size() == 0);
    } 
        
        
    public ArrayList getFamilyListFromModel() {
        return familyList;
    }
        
    /******************* About Suite *************************/
        
    public void setSuiteListInModel(ArrayList list) {
        suiteList = list;
    }
        
    public void addTestList(TestList list) {
        for (int i = 0; i < list.getTestListFromModel().size(); i++) {
            if (!testList.contains(list.getTestListFromModel().get(i))) {
                this.testList.add(list.getTestListFromModel().get(i));
            }
        }
        if (!suiteList.contains(list)) {
            suiteList.add(list);
        }
        if (!familyList.contains(list.getFamilyFromModel())) {
            familyList.add(list.getFamilyFromModel());
        }
    } 

        
        
    public void addOnlyTestListInModel(TestList list) {
        if (!suiteList.contains(list)) {
            suiteList.add(list);
        }
    }
        
    public boolean removeTestListInModel(TestList list) {
        int j = 0;
        while (j < testList.size()) {
            if (((Test) testList.get(j)).getTestListFromModel().getNameFromModel().equals(
                                                                                          list.getNameFromModel())) {
                //removeTest(((Test) testList.get(j)));
                for (int k = 0 ; k < executionList.size() ; k++) {
                    ((Execution)executionList.get(k)).deleteTestFromExecInModel(((Test) testList.get(j)));
                }
                testList.remove(j);
            } else {
                j++;
            }
        }
        suiteList.remove(list);
        boolean otherListOfSameFamilyExist = false;
        int i = 0;
        while (!otherListOfSameFamilyExist && i < suiteList.size()) {
            if (((TestList) suiteList.get(i)).getFamilyFromModel().getNameFromModel().equals(
                                                                                             list.getFamilyFromModel().getNameFromModel())) {
                otherListOfSameFamilyExist = true;
            }
            i++;
        }
        if (!otherListOfSameFamilyExist) {
            familyList.remove(list.getFamilyFromModel());
        }
                
        purgeExcecutionInModel(true);
                
        return (testList.size() == 0);
    }
        
    public ArrayList getSuitListFromModel() {
        return suiteList;
    } 
        
    public int getTestOrderInModel(Test test) {
        for (int i = 0; i < testList.size(); i++) {
            if (((Test) testList.get(i)).equals(test)) {
                return i;
            }
        }
        return -1;
    } 
        
    /********************* About tests  **********************/
    Test findTestByID(int idTest, ArrayList testInModel){
        for (int i = 0 ; i < testInModel.size(); i++){
            Test pTest = (Test) testInModel.get(i);
            if (pTest.getIdBdd() == idTest){
                return pTest;
            }
        }
        return null;
    }
        
    /**
     * 
     * @param testInModel
     * @return true if model is not completed
     * @throws Exception
     */
    protected boolean reloadTestInCampaign(ArrayList testInModel) throws Exception {
        boolean ret = false;
        Vector testCampWrapperList = getTestsByOrderFromDB();
        for (int i = 0; i < testCampWrapperList.size() ; i++){
            TestCampWrapper pTestCampWrapper = (TestCampWrapper)testCampWrapperList.elementAt(i);
            int idTest = pTestCampWrapper.getIdBDD();
            Test pTest = findTestByID(idTest, testList);
            if (pTest == null) {
                // Try to Find Test In testPlan
                pTest = findTestByID(idTest, testInModel);
                if (pTest != null) {
                    addTestInModel(pTest, pTestCampWrapper.getIdUser());
                } else {
                    /* TODO Reload Testplan */
                    return true;
                }
            } else {
                //Update
                updateTestAssignationInModel(pTest.getIdBdd(), pTestCampWrapper.getIdUser());
            }
        }
        int size = testList.size();
        Vector test2Remove = new Vector();
        for (int i = 0; i < size; i++){
            Test pTest = (Test)testList.get(i);
            boolean trouve = false;
            int j = 0;
            int size2 = testCampWrapperList.size();
            while (j < size2 && !trouve){
                TestCampWrapper pTestCampWrapper = (TestCampWrapper)testCampWrapperList.elementAt(j);
                if (pTestCampWrapper.getIdBDD() == pTest.getIdBdd()){
                    trouve = true;
                }
                j++;
            }
            if (!trouve){
                test2Remove.add(pTest);
                                
            }
                        
        }
        size = test2Remove.size();
        for (int i = 0; i < size; i++){
            Test pTest = (Test)test2Remove.get(i);
            deleteTestFromCampInModel(pTest, true);
        }
                
        //Roerder 
        triTestInModel(testCampWrapperList);
        return ret ;
                
        /*suiteList.clear();
          testList.clear();
          familyList.clear();
          Vector testCampWrapperList = getTestsByOrderFromDB();
          for (int i = 0; i < testCampWrapperList.size() ; i++){
          TestCampWrapper pTestCampWrapper = (TestCampWrapper)testCampWrapperList.elementAt(i);
          int idTest = pTestCampWrapper.getIdBDD();
          Test pTest = findTestByID(idTest,testInModel);
          addTestInModel(pTest);
          }*/
    }

    public void setTestListInModel(ArrayList list) {
        testList = list;
                
    } 
        
    public void fillCampInModel(ArrayList list, User pUser) {
        suiteList.clear();
        testList.clear();
        familyList.clear();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Util.log("Fill test : " + ((Test) list.get(i)).getNameFromModel());
            addTestInModel((Test) list.get(i), pUser.getIdBdd());
        }
    }
        
    public void addTestInModel(Test test, int userID) {
        if (!testList.contains(test)) {
            testList.add(test);
        }
        if (!suiteList.contains(test.getTestListFromModel())) {
            suiteList.add(test.getTestListFromModel());
            //test.getTestList().
        }
        if (!familyList.contains(test.getTestListFromModel().getFamilyFromModel())) {
            familyList.add(test.getTestListFromModel().getFamilyFromModel());
        }
        testAssigned.put(new Integer(test.getIdBdd()), new Integer(userID) );
    } 
        
        
    public void deleteTestFromCampInDB(int testId, boolean deleteExec) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.deleteTest(idBdd, testId, deleteExec);
    }
        
    public boolean deleteTestFromCampInModel(Test test, boolean deleteExec) {
        testList.remove(test);
        boolean otherTestOfSameListExist = false;
        int i = 0;
        while (!otherTestOfSameListExist && i < testList.size()) {
            if (((Test) testList.get(i)).getTestListFromModel().getNameFromModel().equals(
                                                                                          test.getTestListFromModel().getNameFromModel())) {
                otherTestOfSameListExist = true;
            }
            i++;
        }
        if (!otherTestOfSameListExist) {
            suiteList.remove(test.getTestListFromModel());
        }
        boolean otherListOfSameFamilyExist = false;
        i = 0;
        while (!otherListOfSameFamilyExist && i < suiteList.size()) {
            if (((TestList) suiteList.get(i)).getFamilyFromModel().getNameFromModel().equals(
                                                                                             test.getTestListFromModel().getFamilyFromModel().getNameFromModel())) {
                otherListOfSameFamilyExist = true;
            }
            i++;
        }
        if (!otherListOfSameFamilyExist) {
            familyList.remove(test.getTestListFromModel().getFamilyFromModel());
        }
                
        for (int j = 0 ; j < executionList.size() ; j++) {
            ((Execution)executionList.get(j)).deleteTestFromExecInModel(test);
        }
                
        purgeExcecutionInModel(deleteExec);
        testAssigned.remove(new Integer(test.getIdBdd()));
        return (testList.size() == 0);
    } 
        
    public void deleteTestFromCampInDBAndModel(Test test, boolean deleteExec) throws Exception {
        deleteTestFromCampInDB(test.getIdBdd(), deleteExec);
        deleteTestFromCampInModel(test, deleteExec);
    }
        
    public ArrayList getTestListFromModel() {
        return testList;
    } 
        
        
    public Vector getTestsByOrderFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        TestCampWrapper[] tmpArray = pISQLCampaign.getTestsByOrder(idBdd);
        Vector tmpVector = new Vector();
        for(int i = 0; i < tmpArray.length; i++) {
            tmpVector.add(tmpArray[i]);
        }
        return tmpVector;
    }
        
        
    /**
     * Add test in campaign by synchronizing data in test plan (Family, Testlist) which is in the campaign 
     * by addind contening test in the campaign
     * @param data
     * @return true if the campaign change
     * @throws Exception
     */
    public boolean synchronizeWithTestPlanFromModel(SimpleData data, int userID) throws Exception {
        boolean res = false;
        ArrayList<Test> test2add = null;
                
        if (data instanceof TestList){
            TestList pTestList = (TestList) data;
            if (!pTestList.isInBase()){
                return false;
            }
            if (!containsTestListInModel(pTestList)){
                return false;
            }
            test2add = pTestList.getTestListFromModel();
        } else if (data instanceof Family){
            Family pFamily = (Family) data;
            if (!pFamily.isInBase()){
                return false;
            }
            if (!containsFamilyInModel(pFamily)){
                return false;
            }
            ArrayList<TestList> suite2add = pFamily.getSuiteListFromModel();
            int size = suite2add.size();
            test2add = new ArrayList<Test>() ;
            for (int i = 0 ; i < size ; i++){
                TestList pTestList = suite2add.get(i);
                ArrayList<Test> test2add_tmp = pTestList.getTestListFromModel();
                test2add.addAll(test2add_tmp);
            }
        } else {
            return false;
        }
        int size = test2add.size();
        for (int i = 0; i < size; i++){
            Test pTest = test2add.get(i);
            if (!containsTestInModel(pTest)){
                /** Ajout du test dans la campaigne **/
                importTestInDBAndModel(pTest, userID);
                res = true;
            }
        }
                
        return res;
    }
    /********************* About DataSet *********************/
        
    public void loadDataSetFromBase(Hashtable paramsInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        purgeDataSetInModel();
        Vector dataSetVector = getDatagetDataSetListWrapperFromDB();
        for (int i = 0; i < dataSetVector.size(); i++) {
            DataSet dataSet = new DataSet((DataSetWrapper) dataSetVector.get(i));
            dataSet.reloadFromDB(false, paramsInModel);
            addDataSetInModel(dataSet);
        }
                
    }
        
    protected void reloadDataSetFromBase(Hashtable paramsInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        int size =  dataSetList.size();
        Vector dataSetVector = getDatagetDataSetListWrapperFromDB();
        int size2 = dataSetVector.size();
        Vector dataSet2Remove = new Vector();
                
        for (int i = 0 ; i < size ; i++) {
            DataSet pDataSet = (DataSet) dataSetList.get(i);
            int j = 0;
            boolean trouve = false;
            while (j < size2 && !trouve){
                DataSet dataSet = new DataSet((DataSetWrapper) dataSetVector.get(j));
                if (pDataSet.getIdBdd() == dataSet.getIdBdd()){
                    trouve = true;
                    pDataSet.reloadFromDB(true, paramsInModel);
                }
                j++;
            }
            if (trouve == false){
                dataSet2Remove.add(pDataSet);
            }
        }
        
        for (int i = 0; i < size2; i++) {
            DataSet dataSet = new DataSet((DataSetWrapper) dataSetVector.get(i));
            boolean trouve = false;
            int j = 0;
            while (!trouve && j <  size){
                DataSet pDataSet = (DataSet) dataSetList.get(j);
                if (pDataSet.getIdBdd() == dataSet.getIdBdd()){
                    trouve = true;
                }
                j++;
            }
            if (trouve == false) {
                dataSet.reloadFromDB(false, paramsInModel);
                addDataSetInModel(dataSet);
            }
        }       
                
        int size3 = dataSet2Remove.size();
        for (int i = 0; i < size3; i++) {
            DataSet dataSet = (DataSet)dataSet2Remove.get(i);
            deleteDataSetInModel(dataSet);
        }
    }
        
    public void addDataSetInModel(DataSet dataSet) {
        dataSetList.add(dataSet);
        dataSet.addInModel(this);
    } 
        
    public void addDataSetInDB(DataSet dataSet) throws Exception {
        dataSet.addInDB(idBdd);
    } 
        
    public void addDataSetInDBAndModel(DataSet dataSet) throws Exception {
        addDataSetInDB(dataSet);
        addDataSetInModel(dataSet);
    }
        
    public ArrayList getExecutionsOfDataSetInCamp(String dataSetName) {
        ArrayList result = new ArrayList();
        for (int j = 0; j < getExecutionListFromModel().size(); j++) {
            if (((Execution)getExecutionListFromModel().get(j)).getDataSetFromModel().getNameFromModel().equals(dataSetName)) {
                result.add((getExecutionListFromModel().get(j)));
            }
        }
        return result;
    }
        
    public void deleteDataSetInModel(DataSet dataSet) {
        for (int j = 0; j < getExecutionListFromModel().size(); j++) {
            if (((Execution)getExecutionListFromModel().get(j)).getDataSetFromModel().getNameFromModel().equals(dataSet.getNameFromModel())) {
                deleteExecutionInModel((Execution)(getExecutionListFromModel().get(j)));
            }
        }
        dataSetList.remove(dataSet);
        dataSet.deleteInModel();
    }
        
    public void deleteDataSetInDB(DataSet dataset) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        dataset.deleteInDB();
    }
        
    public void deleteDataSetInDBAndModel(DataSet dataset) throws Exception {
        deleteDataSetInDB(dataset);
        deleteDataSetInModel(dataset);
    }
        
        
    /*public void removeDataSetInModel(String dataSetName) {
      for (int i = 0; i < dataSetList.size(); i++) {
      if (((DataSet) dataSetList.get(i)).getNameFromModel().equals(dataSetName)) {
      ((DataSet) dataSetList.get(i)).deleteInModel();
      dataSetList.remove(i);
      return;
      }
      }
      }*/
        
        
    public DataSet getDataSetFromModel(String dataSetName) {
        for (int i = 0; i < dataSetList.size(); i++) {
            if (((DataSet) dataSetList.get(i)).getNameFromModel().equals(dataSetName)) {
                return (DataSet) dataSetList.get(i);
            }
        }
        return null;
    } 
        
        
    public ArrayList getDataSetListFromModel() {
        return dataSetList;
    }
        
    public Vector getDatagetDataSetListWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        DataSetWrapper[] tmpArray = pISQLCampaign.getDatsets(idBdd);
        Vector tmpVector = new Vector();
        for(int i = 0; i < tmpArray.length; i++) {
            tmpVector.add(tmpArray[i]);
        }
        return tmpVector;
    }
        
    void purgeDataSetInModel(){
        for (int i = 0 ; i < dataSetList.size() ; i++) {
            DataSet pDataSet = (DataSet) dataSetList.get(i);
            pDataSet.deleteInModel();
        }
    }
        
    /********************* About Execution ********************/
        
    public void addExecutionInModel(Execution exec) {
        executionList.add(exec);
        exec.addInModel(this);
    }
        
    public void addExecutionInDBAndModel(Execution exec, User pUser, File preScript, File postScript) throws Exception {
        exec.addInDB(pUser, exec.getAttachmentMapFromModel(), preScript, postScript);
        addExecutionInModel(exec);
    }
        
    public void addExecutionInDBAndModel(Execution exec, User pUser) throws Exception {
        exec.addInModel(this);
        exec.addInDB(pUser);
        addExecutionInModel(exec);
    }
        
    public void updateExecutionInModel(Execution exec) {
        exec.addInModel(this);
    }
        
    public ArrayList getExecutionListFromModel() {
        return executionList;
    } 
        
    void purgeExcecutionInModel(boolean deleteExec){
        //Si la campagne est vide, suppression de ses resultats d'execution
        if (getTestListFromModel().size() == 0) {
            ArrayList execList = getExecutionListFromModel();
            //int execListSize = execList.size();
            for (int i = 0 ; i < execList.size(); i++) {
                ((Execution)execList.get(i)).getExecutionResultListFromModel().clear();
                if (deleteExec){
                    Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION ,execList.get(i));
                    deleteExecutionInModel((Execution)execList.get(i));
                }
            }
        }
    }
        
    protected void reloadExcecutionInModel(ArrayList envInModel, Hashtable paramInModel) throws Exception {
        int size = 0;
        ArrayList execList = null;
                
        execList = getExecutionListFromModel();
        Vector execVector = getExecutionWrapperListFromDB();
        Vector execToRemove = new Vector();
        int size2 = execVector.size();
                
        if (execList != null) {
            size = execList.size();
        }       
        for (int i = 0 ; i < size; i++) {
            Execution pExec = (Execution)execList.get(i);
            boolean trouve = false;
            int j = 0;
            while (!trouve && j <  size2){
                Execution exec = new Execution(this, (ExecutionWrapper) execVector.get(j));
                if (pExec.getIdBdd() == exec.getIdBdd()){
                    trouve = true;
                }
                j++;
            }
            if (trouve == true) {
                pExec.reloadEnvFromDB(envInModel, paramInModel);
                pExec.reloadDataSetFromDB(getDataSetListFromModel() ,paramInModel);
                pExec.reloadScriptFromDB();
                pExec.reloadExecResultFromDB();
                //exec.loadAttachmentDataFromDB();
                pExec.reloadAttachmentDataFromDB(false);        
            } else {
                execToRemove.add(pExec);
            }
                        
        }
                
        int size3 = execToRemove.size();
        for (int i = 0 ; i < size3; i++) {
            Execution pExec = (Execution)execToRemove.get(i);
            deleteExecutionInModel(pExec);
        }
        if (execList != null) {
            size = execList.size();
        }       
                
        for (int k = 0; k < size2; k++) {
            Execution exec = new Execution(this, (ExecutionWrapper) execVector.get(k));
                        
            boolean trouve = false;
            int j = 0;
            while (!trouve && j <  size){
                Execution pExec = (Execution)execList.get(j);
                if (pExec.getIdBdd() == exec.getIdBdd()){
                    trouve = true;
                }
                j++;
            }
            if (trouve == false) {
                exec.reloadEnvFromDB(envInModel, paramInModel);
                exec.reloadDataSetFromDB(envInModel ,paramInModel);
                exec.reloadScriptFromDB();
                exec.reloadExecResultFromDB();
                //exec.loadAttachmentDataFromDB();
                exec.reloadAttachmentDataFromDB(false);
                addExecutionInModel(exec);
            }
        }       
        
    }
        
        
    /*public void deleteExecutionInModel(int index) {
      Execution exec = (Execution) executionList.get(index);
      if (exec != null) {
      deleteExecutionInModel(exec);
      }
      } */
        
        
    public void deleteExecutionInModel(Execution exec) {
        executionList.remove(exec);
        exec.deleteInModel();
                
    } 
        
    public void deleteExecutionInDBAndModel(Execution exec) throws Exception {
        exec.deleteInDB();
        deleteExecutionInModel(exec);
    } 
        
    public void deleteExecutionInDB(Execution exec) throws Exception {
        exec.deleteInDB();
    }
        
    public Execution getExecutionFromModel(int index) {
        if (index >= executionList.size() || index < 0) {
            return null;
        }
        return (Execution) executionList.get(index);
    } 
        
        
    public Execution getExecutionFromModel(String execName) {
        for (int i = 0; i < executionList.size(); i++) {
            if (((Execution) executionList.get(i)).getNameFromModel().equals(execName)) {
                return (Execution) executionList.get(i);
            }
        }
        return null;
    }
        
    public Vector getExecutionWrapperListFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        ExecutionWrapper[] tmpArray = pISQLCampaign.getExecutions(idBdd);
        Vector tmpVector = new Vector();
        for(int i = 0; i < tmpArray.length; i++) {
            tmpVector.add(tmpArray[i]);
        }
        return tmpVector;
    }
        
    /********************** About Attachements *****************/
        
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        File f = file.getLocalFile();
        int id = pISQLCampaign.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        int id = pISQLCampaign.addAttachUrl(idBdd, url.getNameFromModel(), url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
    @Override
    public void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        pISQLCampaign.deleteAttach(idBdd, attachId);
    }
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach) throws Exception{
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }
        
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLCampaign.getAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int i = 0; i < tmpArray.length; i++) {
            tmpVector.add(tmpArray[i]);
        }
        return tmpVector;
    }

        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLCampaign.getAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int i = 0; i < tmpArray.length; i++) {
            tmpVector.add(tmpArray[i]);
        }
        return tmpVector;
    }
        
    /**
     * Get a Vector of Vector contening 3 data, a ExecutionWrapper, a  TestCampWrapper and  an Attachement
     * representing all attachement of all execution result
     * @return
     * @throws Exception
     */
    public Vector getAllResExecAttahFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Campaign " + name + " is not in BDD");
        }
        Vector res = new Vector();
                
        ExecutionAttachmentWrapper[] tmpArray = pISQLCampaign.getResExecutionsAttachment(idBdd);
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            Vector tmpData = new Vector();
            tmpData.add(0, tmpArray[tmpI].getExecution());
            tmpData.add(1, tmpArray[tmpI].getTestCamp());
            if(tmpArray[tmpI].getFileAttachment() != null) {
                tmpData.add(2, tmpArray[tmpI].getFileAttachment());
            }
            else if(tmpArray[tmpI].getUrlAttachment() != null) {
                tmpData.add(2, tmpArray[tmpI].getUrlAttachment());
            }
            res.add(tmpData);
        }
                
        return res;
    }
        
    /////////////////////////////////////////////////////////////////////////////
    public static boolean isInBase(Project pProject, Campaign campaign) {
        return isInBase(pProject, campaign.getNameFromModel());
    } // Fin de la methode isInBase/1
        
    public static boolean isInBase(Project pProject, String campaignName) {
        try  {
            int id = pISQLCampaign.getID(pProject.getIdBdd(), campaignName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLCampaign.getCampaign(idBdd) != null;
    }
        
    /**************************** Model Conformity operation *************************/
        
    /** 
     * Return true if the test in the campagn model are in base and in the campagn 
     */
    public boolean isValideModel() throws Exception{
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(111, ApiConstants.LOADING);
                        
            TestCampWrapper[] tmpArray = pISQLCampaign.getTestsByOrder(idBdd);
            Vector tmpVector = new Vector();
            for(int i = 0; i < tmpArray.length; i++) {
                tmpVector.add(tmpArray[i]);
            }
            Vector allTestDB = tmpVector;
                        
            int allTestDdSize = allTestDB.size();
            int alltestModelSize = testList.size();
            if (allTestDdSize != alltestModelSize){
                return false; //model are not coherant
            }
            for (int i = 0 ; i < allTestDdSize ;  i++){
                TestCampWrapper pTestCampWrapper = (TestCampWrapper)allTestDB.elementAt(i);
                Test pTest = containsTestInModel(pTestCampWrapper.getIdBDD());
                if (pTest == null){
                    return false; //model are not coherant
                }
                /*if (!pTest.existeInBase()){
                // Correct DB
                try {
                pISQLCampaign.deleteTest(idBdd, pTest.getIdBdd(), true);
                } catch (Exception e){
                                                
                }
                return false; //model are not coherant
                }*/
            }
                        
            Api.commitTrans(transNuber);
            return true;
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
    }
        
        
    /*************************************************************************/
        
    public void triTestInModel(Vector testWrapperOrdered){
        Collections.sort(testList, new ComparateurTestCamp(testWrapperOrdered));
    }
        
    class ComparateurTestCamp implements Comparator {
        Vector testWrapperOrdered;
        int size;
        ComparateurTestCamp(Vector testWrapperOrdered){
            this.testWrapperOrdered = testWrapperOrdered;
            size = testWrapperOrdered.size();
        }
        @Override
        public int compare(Object poTest1, Object poTest2){
            Test pTest1 = (Test) poTest1;
            Test pTest2 = (Test) poTest2;
            int order1 =-1;
            int order2 =-1;
            int i = 0;
            int trouve = 0;
            while (i < size && trouve < 2){
                TestCampWrapper pTestCampWrapper = (TestCampWrapper)testWrapperOrdered.elementAt(i);
                if (pTestCampWrapper.getIdBDD() == pTest1.getIdBdd()){
                    trouve ++;
                    order1 = i;
                } else if (pTestCampWrapper.getIdBDD() == pTest2.getIdBdd()){
                    trouve ++;
                    order2 = i;
                }
                i++;
            }
            if (order1 > order2 ) {
                return 1;
            } else  {
                return -1;              
            } 
        }      
    }
}
