package org.objectweb.salome_tmf.data;

public class SalomeEvent {

    int _code;
    Object _object;
        
    Object _oldValue;
    Object _newValue;
        
    public SalomeEvent(int c, Object arg){
        _code = c;
        _object = arg;
    }
        
    public SalomeEvent(int c, Object arg, Object oldValue, Object newValue){
        _code = c;
        _object = arg;
        _oldValue = oldValue;
        _newValue = newValue;
    }
        
    public int getCode() {
        return _code;
    }

    public Object getArg() {
        return _object;
    }

    public Object getNewValue() {
        return _newValue;
    }

    public Object getOldValue() {
        return _oldValue;
    }
        
        
}
