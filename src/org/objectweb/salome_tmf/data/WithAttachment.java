package org.objectweb.salome_tmf.data;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;

abstract public class WithAttachment extends SimpleData {

    /**
     * Table contenant les attachements. La cle est le nom de l'attachement et
     * la valeur l'attachement lui-meme.
     */
    private HashMap attachmentMap = new HashMap();
        
    private boolean attachLoaded = false;
        
    protected boolean doClearCache = false;
        
    @Override
    public void  clearCache(){
        if (doClearCache){
            if (attachmentMap != null){
                attachmentMap.clear();
                //attachmentMap = null;
            }
            attachLoaded = false;
        }
    }
        
    public void  clearAttachCache(){
        if (doClearCache){
            if (attachmentMap != null){
                attachmentMap.clear();
                //attachmentMap = null;
            }
            attachLoaded = false;
        }
    }
        
    void loadAttachment(){
        if (attachLoaded){
            return;
        }
        try  {
            loadAttachmentDataFromDB();
        } catch(Exception e){
            // Normally, no attachment is found if an execution is created!
            // Though, no StackTrace is printed.
            // e.printStackTrace();
            Util.err(e);
        }
    }
    public WithAttachment(String name, String description) {
        super(name, description);
        //attachmentMap = new HashMap(); 
    }
        
        
    public HashMap getAttachmentMapFromModel() {
        loadAttachment();
                
        return attachmentMap;
    }
        
    public HashMap getCopyOfAttachmentMapFromModel(){
        loadAttachment();
                
        HashMap copyAttachMap = new HashMap();
        Set keysSet = attachmentMap.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Object elem = iter.next();
            copyAttachMap.put(elem, attachmentMap.get(elem));
        }
        return copyAttachMap;
    }
        
    protected void clearAttachInModel(){
        if (attachmentMap != null){
            attachmentMap.clear();
            //attachmentMap = null;
        }
    }
        
    public void addAttchmentInModel(String fileName, String value){
        try {
            FileAttachment attach = makeFileAttachment(fileName, value);
            addAttachementInModel(attach);
        } catch(Exception e){
                        
        }
    }
        
    public FileAttachment takeScreenShot() throws Exception{
        String fileName = "screenshot.jpg";
        Properties sys = System.getProperties();
        String tempDir = sys.getProperty("java.io.tmpdir");
        String fs = sys.getProperty("file.separator");
        tempDir = tempDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP;
        File fDir = new File(tempDir );
        if (!fDir.exists()){
            fDir.mkdirs();
        }
        int num = 0;
        String pFileName = tempDir + fs +fileName;
        File file = new File(pFileName);
        while (file.exists() || containsAttachmentInModel(file.getName())) {
            pFileName = tempDir + fs + num +fileName;
            file = new File(pFileName);
            num++;
        }
        file.createNewFile();
                
        Robot robot = new Robot();
        robot.setAutoDelay(0);
        robot.setAutoWaitForIdle(false);
                
        Rectangle captureSize = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage bufferedImage = robot.createScreenCapture(captureSize);
       
        javax.imageio.ImageIO.write(bufferedImage, "jpg", file);
        return makeFileAttachment(file);
    }
        
        
    public FileAttachment makeFileAttachment(String fileName, String value) throws Exception{
        Properties sys = System.getProperties();
        String tempDir = sys.getProperty("java.io.tmpdir");
        String fs = sys.getProperty("file.separator");
        tempDir = tempDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP;
        int num = 0;
        String pFileName = tempDir + fs +fileName;
        File file = new File(pFileName);
        while (file.exists() || containsAttachmentInModel(file.getName())) {
            pFileName = tempDir + fs + num +fileName;
            file = new File(pFileName);
            num++;
            //errorFile.delete();
        }
        file.createNewFile();
        FileWriter fw = new FileWriter(pFileName, true);
        // le BufferedWriter output auquel on donne comme argument le FileWriter fw cree juste au dessus
        BufferedWriter output = new BufferedWriter(fw);
        //on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
        output.write(value);
        //on peut utiliser plusieurs fois methode write
        output.flush();
        //ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
        output.close();
        //et on le ferme
        return makeFileAttachment(file);
    }
        
    public void addAttchmentInModel(File file){
        FileAttachment pFileAttachment = makeFileAttachment(file);
        /*if  (attachmentMap == null)
          attachmentMap = new HashMap();
                
          attachmentMap.put(pFileAttachment.getNameFromModel(), pFileAttachment);*/
        addAttachementInModel(pFileAttachment);
    }
        
    public FileAttachment makeFileAttachment(File file){
        FileAttachment pFileAttachment = new FileAttachment(file, "");
        return pFileAttachment;
    }
        
    public void addAttchmentInModel(URL pUrl){
        UrlAttachment urlAttachment = makeUrlAttchment(pUrl);
        
        /*if  (attachmentMap == null)
          attachmentMap = new HashMap(); 
                
          attachmentMap.put(urlAttachment.getNameFromModel(), urlAttachment);
        */
        addAttachementInModel(urlAttachment);
    }
        
        
    public UrlAttachment makeUrlAttchment(URL pUrl){ 
        UrlAttachment urlAttachment = new UrlAttachment(pUrl.toString(), "");
        urlAttachment.setUrl(pUrl);
        return urlAttachment;
    }
        
    public void addAttachementInDBAndModel(Attachment attach )throws Exception {
        addAttachementInDB(attach);
        addAttachementInModel(attach);
    }
        
    abstract public void addAttachementInDB(Attachment attach)throws Exception;
        
        
    public void addAttachementInModel(Attachment attach) {
        if  (attachmentMap == null){
            attachmentMap = new HashMap();
        }
        attachmentMap.put(attach.getNameFromModel(), attach);
        //String uniqueName = getUniqueName(attach.getNameFromModel());
        //attachmentMap.put(uniqueName, attach);
    }
        
        
    abstract public void deleteAttachementInDBAndModel(Attachment attach) throws Exception;
        
    abstract protected void deleteAttachementInDB(int idAttach) throws Exception;
        
    public void deleteAttachmentInModel(Attachment attach) {
        attachmentMap.remove(attach.getNameFromModel());
        if (attachmentMap.containsValue(attach)){
            Set keysSet = attachmentMap.keySet();
            for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                Object attachName =  iter.next();
                Attachment at = (Attachment) attachmentMap.get(attachName);
                if (attach.equals(at)){
                    attachmentMap.remove(attachName);
                    return;
                }
            }
        } else {
            attachmentMap.remove(attach.getNameFromModel());
        }
    }
        
    /*
      public void deleteAttachmentInModel(String attachName) {
      attachmentMap.remove(attachName);
      }
    */
        
    public Attachment getAttachmentFromModel(String attachName) {
        loadAttachment();
        return (Attachment)attachmentMap.get(attachName);
                
    }
        
    public boolean containsAttachmentInModel(String attachName) {
        loadAttachment();
        return attachmentMap.containsKey(attachName); 
    }
        
    public boolean containsAttachmentInModel(Attachment attach) {
        loadAttachment();
        return attachmentMap.containsValue(attach); 
    }
        
    public void setAttachmentMapInModel(HashMap map) {
        //Util.log("[WithAttachemnt->SetAttachementMap]");
        attachmentMap = map;
        if (attachmentMap != null){
            attachLoaded = true;
        } else {
            attachLoaded = false;
        }
    }
        
    abstract public Vector getAttachFilesFromDB() throws Exception;
    abstract public Vector getAttachUrlsFromDB() throws Exception;
        
    public void reloadAttachmentDataFromDB(boolean force) throws Exception {
        clearAttachInModel();
        if (!DataLoader.isDynamic || force){
            int transNuber = -1;
            try {
                transNuber = Api.beginTransaction(0, ApiConstants.LOADING);
                loadAttachmentDataFromDB();
                Api.commitTrans(transNuber);
            } catch (Exception e){
                Api.forceRollBackTrans(transNuber);
                throw e;
            }
        }
    }
        
    void loadAttachmentDataFromDB() throws Exception {
        Vector fileNameAttachVector = getAttachFilesFromDB();
        Vector urlAttachVector = getAttachUrlsFromDB();
        if (attachmentMap == null){
            attachmentMap = new HashMap();
        }
        for (int i = 0; i < fileNameAttachVector.size(); i++) {
            FileAttachment fileAttach = new FileAttachment((FileAttachementWrapper) fileNameAttachVector.get(i));
            addAttachementInModel(fileAttach);
        }
                
        for (int i = 0; i < urlAttachVector.size(); i++) {
            UrlAttachment urlAttach = new UrlAttachment ((UrlAttachementWrapper) urlAttachVector.get(i));
            addAttachementInModel(urlAttach);
        }
        attachLoaded = true;
    }
        
    public void updateAttachement(HashMap oldAttachMap) throws Exception {
        /* Les attachement */
        //loadAttachment();
        HashMap newAttachMap  =  getAttachmentMapFromModel();
                
        Set keysSet = newAttachMap.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Object attachName =  iter.next();
            Attachment at = (Attachment) oldAttachMap.get(attachName);
            Attachment attach = (Attachment)newAttachMap.get(attachName);
            if (at == null){            
                /* New */       
                addAttachementInDB(attach);
            } else {
                /* Update */
                String oldDesc = at.getDescriptionFromModel();
                String newDesc = attach.getDescriptionFromModel();
                if (!oldDesc.equals(newDesc)){
                    attach.updateDescriptionInDB(attach.getDescriptionFromModel());
                }
            }
        }
                
        keysSet = oldAttachMap.keySet();
        for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
            Object attachName =  iter.next();
            Attachment at = (Attachment) newAttachMap.get(attachName);
            Attachment attach = (Attachment)oldAttachMap.get(attachName);
            if (at == null) {
                /* remove attach */
                deleteAttachementInDBAndModel(attach);
            }
        }
    }
    /*
      String getUniqueName(String name){
      if  (attachmentMap == null){
      return name;
      }
      boolean trouve = false;
      int copy = 0;
      String tmpName = name;
      while (!trouve){
      Object o = attachmentMap.get(tmpName);
      if (o != null){
      copy++;
      tmpName = name + copy;
      }else {
      trouve = true;
      }
      }
      return tmpName;
      }*/
}
