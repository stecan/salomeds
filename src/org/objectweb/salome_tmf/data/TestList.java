/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.AutomaticTestWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ManualTestWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLTestList;

public class TestList extends WithAttachment {
        
    transient static ISQLTestList pISQLTestList = null;
        
    protected ArrayList testList;       
    protected Family family;
    protected int order;
        
    public TestList (String name, String description) {
        super(name, description);
        testList = new ArrayList();
        family = null;
        order = -1;
        if (pISQLTestList == null){
            pISQLTestList = Api.getISQLObjectFactory().getISQLTestList();
        }
    }
        
    public TestList(SuiteWrapper pSuite) {
        super(pSuite.getName(), pSuite.getDescription());
        idBdd = pSuite.getIdBDD();
        testList = new ArrayList();
        family = null;
        order = pSuite.getOrder();
        if (pISQLTestList == null){
            pISQLTestList = Api.getISQLObjectFactory().getISQLTestList();
        }
    } 
        
    public TestList(Family family, SuiteWrapper pSuite) {
        this(pSuite);
        this.family = family;
    }
        
        
    public void reloadBaseFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        SuiteWrapper pSuiteWrapper =  pISQLTestList.getTestList(idBdd);
        name =  pSuiteWrapper.getName();
        order = pSuiteWrapper.getOrder();
        description = pSuiteWrapper.getDescription();
    }
        
    public void reloadFromDB() throws Exception {
        reloadBaseFromDB();
                
        reloadAttachmentDataFromDB(false);
    }
    /**************************************************************************/
    /**                                                 MUTATEURS ET ACCESSEURS                                         ***/
    /**************************************************************************/
        
    public Family getFamilyFromModel() {
        return family;
    } 
        
    /*
      public void setFamilyInModel(Family newFamily) {
      family = newFamily;
      } */
        
        
    /***************************** Basic Operation *********************************/
        
    /* Used By Family */
    void addInDB(int idFamily) throws Exception {
        if (isInBase()) {
            throw new Exception("TestList " + name + " already in BDD");
        }
        if (idFamily == -1 ){
            idFamily = family.getIdBdd();
            if (idFamily == -1 ){
                throw new Exception("Family is not set");
            }
        }
        idBdd = pISQLTestList.insert(idFamily, name, description);
        order = pISQLTestList.getTestList(idBdd).getOrder();
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_SUITE ,this);
    }
        
    /* Used By Family */
    void addInModel(Family newFamily){
        family = newFamily;
    }
    /* Used By Family */
    void addInDBAndModel(Family newFamily) throws Exception {
        addInDB(newFamily.getIdBdd());
        addInModel(newFamily);
    }

        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLTestList.update(idBdd, newName, newDesc );
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_SUITE ,this, new String(name), newName);
    }
        
        
    public void updateInModel(String newName, String newDesc) {
        name = newName;
        description = newDesc;
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateInDB(newName, newDesc);
        updateInModel(newName, newDesc);
    }
        
        
    public void updateOrderInDBAndModel(boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        order = pISQLTestList.updateOrder(idBdd, inc);        
    }
        
        
    /* Used By Family */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLTestList.delete(idBdd);    
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_SUITE ,this);
    }
        
    /* Used By Family */
    void deleteInModel(){
        clearAttachInModel();
        for (int i=0; i < testList.size(); i++) {
            Test pTest = (Test)testList.get(i);
            pTest.deleteInModel();
            Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_TEST ,pTest);
        }
        testList.clear();       
    }
        
    /* Used By Family */
    void deleteInDBAndModel() throws Exception {
        deleteInDB();
                
        deleteInModel();
    }
        
    /************************* TESTS ******************************/
        
    public void reloadTestsFromDB(Hashtable paramInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
                
        for (int i=0; i < testList.size(); i++) {
            ((Test)testList.get(i)).deleteInModel();
        }
        testList.clear();       
        Vector testVector =  getTestsWrapperFromDB();
        for (int i = 0; i < testVector.size(); i ++) {
            TestWrapper testBdd = (TestWrapper) testVector.get(i);
            Test test;
            if (testBdd.getType().equals(ApiConstants.MANUAL)) {
                test = new ManualTest(this, new ManualTestWrapper(testBdd));
            }else {
                test = new AutomaticTest(this, new AutomaticTestWrapper(testBdd));
            }
            test.reloadFromDB(false, paramInModel, true);
            addTestInModel(test);
        }
    }
        
    public ArrayList getTestListFromModel() {
        return testList;
    }
        
    public void setTestListInModel(ArrayList list) {
        testList = list;
    } 
        
    public void addTestInModel(Test test) {
        if (!hasTestInModel(test.getNameFromModel())){
            testList.add(test);
        }
        test.addInModel(this);
    }
        
    public void addTestInDB(Test test) throws Exception {
        test.addInDB(idBdd);
    }
        
    public void addTestInDBAndModel(Test test) throws Exception {
        addTestInDB(test);
        addTestInModel(test);
    }
        
        
    public boolean hasTestInModel(String name) {
        for (int i=0; i < testList.size(); i++) {
            if (((Test)testList.get(i)).getNameFromModel().equals(name)) {
                return true;
            }
        }
        return false;
    }
        
        
    public void deleteTestInModel(Test test) { //ancien removeTest
        testList.remove(test);
        test.deleteInModel();
        // Do reoder ????
        for (int i = 0; i < testList.size(); i++) {
            ((Test)(testList.get(i))).setOrderInModel(i);
        }
    }
        
    public void deleteTestInDB(Test test) throws Exception { //ancien removeTest
        test.deleteInDB();
    }
        
    public void deleteTestInDBAndModel(Test test) throws Exception { //ancien removeTest
        deleteTestInDB(test);
        deleteTestInModel(test);
    }
        
    public Test getTestFromModel(String testName) {
        for (int i=0; i < testList.size(); i++) {
            if (((Test)testList.get(i)).getNameFromModel().equals(testName)) {
                return (Test)testList.get(i);
            }
        }
        return null;
    } 
        
    public Test getTestFromModel(int id) {
        for (int i=0; i < testList.size(); i++) {
            if (((Test)testList.get(i)).getIdBdd() == id) {
                return (Test)testList.get(i);
            }
        }
        return null;
    }
        
    public Vector getTestsWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        TestWrapper[] tmpArray = pISQLTestList.getTestsWrapper(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    /************************** ATTACHEMENTS **********************/
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }       
        File f = file.getLocalFile();
        int id = pISQLTestList.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        int id = pISQLTestList.addAttachUrl(idBdd, url.getNameFromModel(),url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
        
    @Override
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLTestList.deleteAttach(idBdd,attachId);           
    }
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach) throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLTestList.getAllAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLTestList.getAllAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
    //////////////////////////////////////////////////////////////////////////////////////
    /**
     * Retourne vrai si la suite passee en parametre est presente dans la base
     * @param testList un suite de tests
     * @return vrai si la suite passee en parametre est presente dans la base, 
     * faux sinon
     */
    public static boolean isInBase(Family pFamily, TestList testList) {
        return isInBase(pFamily,  testList.getNameFromModel());
    } // Fin de la methode isInBase/1
        
    public static boolean isInBase(Family pFamily, String testListName) {
        try{
            int id = pISQLTestList.getID(pFamily.getIdBdd(), testListName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e){
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLTestList.getID(family.getIdBdd(),name) == idBdd;
    }
        
    /**** COPIER/COLER ************/
        
    static TestList getModelCopie(TestList toCopie)throws Exception {
        TestList pCopie = new TestList(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
                
        TestWrapper[] Alltests = pISQLTestList.getTestsWrapper(toCopie.getIdBdd());
        int size = Alltests.length;
        for (int i = 0; i < size; i++) {
            Test pTest;
            TestWrapper pTestWrapper = Alltests[i];
            if (pTestWrapper.getType().equals(ApiConstants.MANUAL)){
                pTest = ManualTest.getModelCopie(new ManualTest(new ManualTestWrapper(pTestWrapper)));
            } else {
                pTest = AutomaticTest.getModelCopie(new AutomaticTest(new AutomaticTestWrapper(pTestWrapper)));
            }
            pCopie.addTestInModel(pTest);
        }
        return pCopie;
    }
        
    public static TestList copieIn(TestList toCopie, Family pFamily) throws Exception {
        TestList pCopie = new TestList(toCopie.getNameFromModel(), toCopie.getDescriptionFromModel());
        String testListName = toCopie.getNameFromModel();
        int i = 0;
        while (TestList.isInBase(pFamily, testListName)){
            testListName = toCopie.getNameFromModel() + "_" + i;
            i++;
        }
        pCopie.updateInModel(testListName, toCopie.getDescriptionFromModel());
        pFamily.addTestListInDBAndModel(pCopie);
                
                
        TestWrapper[] Alltests = pISQLTestList.getTestsWrapper(toCopie.getIdBdd());
                
        int size = Alltests.length;
        for (i = 0; i < size; i++) {
            TestWrapper pTestWrapper = Alltests[i];
            if (pTestWrapper.getType().equals(ApiConstants.MANUAL)){
                ManualTest.copieIn(new ManualTest(new ManualTestWrapper(pTestWrapper)), pCopie);
            } else {
                AutomaticTest.copieIn(new AutomaticTest(new AutomaticTestWrapper(pTestWrapper)), pCopie);
            }
        }
                
        try {
            /* Ajout des attachements */
            HashMap<String,Attachment> attachs = toCopie.getAttachmentMapFromModel();
            for (Attachment attach : attachs.values()) {
                if (attach instanceof UrlAttachment) {
                    pCopie.addAttachementInDBAndModel(attach);
                } else {
                    FileAttachment fileAttach = (FileAttachment)attach;
                    File attachFile = fileAttach.getFileFromDB(null);
                    FileAttachment toAdd = new FileAttachment(attachFile, fileAttach.getDescriptionFromModel());
                    pCopie.addAttachementInDBAndModel(toAdd);
                    attachFile.delete();
                }
            }
        }catch (Exception e){
            //Util.err(e);
        }
        Vector<TestList> argNotifier = new Vector<TestList> ();
        argNotifier.add(toCopie);
        argNotifier.add(pCopie);
        Project.pCurrentProject.notifyChanged( ApiConstants.COPIE_TEST_LIST ,argNotifier);
        return pCopie;
    }
        
        
    /******************* TRI ********************************************/
        
    public void triTestInModel(){
        Collections.sort(testList, new ComparateurTest());
    }
        
    class ComparateurTest implements Comparator {
        @Override
        public int compare(Object poTest1, Object poTest2){
            Test pTest1 = (Test) poTest1;
            Test pTest2 = (Test) poTest2;
            if (pTest1.getOrderFromModel() > pTest2.getOrderFromModel() ) {
                return 1;
            } else  {
                return -1;              
            } 
        }      
    }
         
}
