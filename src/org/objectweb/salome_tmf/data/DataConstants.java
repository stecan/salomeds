/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fay\u00e7al SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

/**
 * Interface regroupant les constantes utilis?es dans Voice Testing
 * @covers SFG_ForgeORTF_TST_FIL_000010 - \ufffd2.4.9
 * Jira : FORTF-8
 */

public interface DataConstants {

    /**
     * Valeur relative au groupe
     */
    static final public int GROUP = 100;

    //20100105 - D\ufffdbut modification Forge ORTF v1.0.0
    static final public int CAMPAIGN_ALL = 6;
    static final public int CAMPAIGN_ENDED = 7;
    static final public int CAMPAIGN_INCOMPLETE = 8;
    static final public int CAMPAIGN_WITH_ANOMALY = 9;
    static final public int CAMPAIGN_NOT_EXECUTED = 10;
    static final public int CAMPAIGN = 5;
    //20100105 - Fin modification Forge ORTF v1.0.0
    /**
     * Valeur relative a une famille
     */
    static final public int FAMILY = 11;

    static final public int TESTLIST = 12;

    static final public int TEST = 13;

    static final public int  ACTION = 14;
    /**
     * Valeur relative a un parametre
     */
    static final public int PARAMETER = 15;

    /**
     * Valeur relative a une execution
     */
    static final public int EXECUTION = 16;

    /**
     * Valeur relative au resultat d'execution
     */
    static final public int EXECUTION_RESULT = 17;

    /**
     * Valeur relative au resultat d'execution d'un test
     */
    static final public int EXECUTION_RESULT_TEST = 18;

    static final public int MANUAL_TEST = 19;

    static final public int AUTOMATIC_TEST = 20;

    static final public int ENVIRONMENT = 21;

    static final public int PROJECT = 22;

    // Constantes pour les projets

    /**
     * Projet normal
     */
    static final public int NORMALSTATUS = 200;

    /**
     * Projet gel?
     */
    static final public int FREEZESTATUS = 201;

    /**
     * Projet utilis? en local
     */
    static final public int USEDINLOCALSTATUS = 202;


    final static public int SMALL_SIZE_FOR_ATTACH = 75;

    final static public int NORMAL_SIZE_FOR_ATTACH = 150;

    final static public String PARAM_VALUE_FROM_ENV = "$(env_value)$";
} // Fin de l'interface Constants
