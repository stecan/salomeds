/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.Serializable;


abstract public class SimpleData  implements Serializable {

    protected String name;
    protected String description="";
    protected int idBdd = -1;
        
    SimpleData(String name, String description){
        this.name = name;
        this.description = description;
        idBdd = -1;
    }
    /*
     * Set id of this data in BDD
     */
    protected void setIdBdd(int id){
        idBdd = id;
    }
        
    /*
     * Get id of this data in BDD
     */
    public int getIdBdd(){
        return idBdd;
    }
        
    /*
     * Return true if the data is in base
     */
    public boolean isInBase(){
        return idBdd != -1;
    }
        
    /* 
     * Return the name of the data
     */
    public String getNameFromModel(){
        return name;
    }
        
    /* 
     * Return the name of the data
     */
        
    public String getDescriptionFromModel(){
        return description;
    }
        
    abstract public void updateInDBAndModel(String name, String description) throws Exception;
        
    public void updateDescriptionInModel(String description) {
        this.description = description;
    }
        
    protected void setNameInModel(String name) {
        this.name = name;
    }
        
    @Override
    public String toString() {
        return name;
    }
        
    public abstract boolean existeInBase() throws Exception;
        
    public abstract void clearCache();
}
