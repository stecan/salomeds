/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecutionTestResult;

public class ExecutionTestResult extends WithAttachment {
    static ISQLExecutionTestResult pISQLExecutionTestResult = null;


    protected Test test;
    protected String status;
    protected int order = -1;

    public ExecutionTestResult(Test pTest) {
        super("", "");
        status="";
        test = pTest;
        if (pISQLExecutionTestResult == null){
            pISQLExecutionTestResult = Api.getISQLObjectFactory().getISQLExecutionTestResult();
        }
    }
    public ExecutionTestResult(Test pTest , String stat) {
        super("", "");
        test = pTest;
        status = stat;
        if (pISQLExecutionTestResult == null){
            pISQLExecutionTestResult = Api.getISQLObjectFactory().getISQLExecutionTestResult();
        }
    }

    public void setOrderInModel(int i){
        order = i;
    }

    public int getOderFromModel(){
        return order;
    }

    public String getStatusFromModel() {
        return status;
    }


    public void setStatusInModel(String string) {
        System.out.println("Set Status : "+string+" pour test : "+test.getNameFromModel());
        status = string;
    }


    public Test getTestFromModel() {
        return test;
    }

    void addInDB(int idExecRes, int idTest) throws Exception {
        if (isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " already in BDD");
        }
        idBdd = pISQLExecutionTestResult.insert(idExecRes, idTest, status);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_EXECUTION_TEST_RESULT ,this);
    }

    void updateInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        pISQLExecutionTestResult.update(idBdd, status);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_EXECUTION_TEST_RESULT ,this);
    }

    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        pISQLExecutionTestResult.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION_TEST_RESULT ,this);
    }

    void deleteInModel() {
        test = null;
        clearAttachInModel();
    }

    void deleteInDBAndModel() throws Exception {
        deleteInDB();
        deleteInModel();
    }

    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        throw new Exception ("NOT IMPLEMENTED");
    }
    /***************** About Attachements **********************/

    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }

    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }

        File f = file.getLocalFile();
        int id = pISQLExecutionTestResult.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }

    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }

        int id = pISQLExecutionTestResult.addAttachUrl(idBdd, url.getNameFromModel(), url.getDescriptionFromModel());
        url.setIdBdd(id);
    }


    @Override
    public void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        pISQLExecutionTestResult.deleteAttach(idBdd, attachId);
    }



    @Override
    public void deleteAttachementInDBAndModel(Attachment attach)  throws Exception  {
        deleteAttachementInDB(attach.getIdBdd());
        deleteAttachmentInModel(attach);
    }

    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        System.out.println("Id ds la BDD : "+idBdd);
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLExecutionTestResult.getAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("ExecutionTestResult " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLExecutionTestResult.getAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    @Override
    public boolean existeInBase() throws Exception {
        /* TODO */
        return isInBase() ;
    }

    @Override
    public String toString() {
        return getTestFromModel().getNameFromModel();
    }


}
