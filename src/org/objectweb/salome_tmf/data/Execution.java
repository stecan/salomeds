/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import static org.objectweb.salome_tmf.api.ApiConstants.EMPTY;
import org.objectweb.salome_tmf.api.data.DataSetWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionResultWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.ScriptWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLExecution;
import static org.objectweb.salome_tmf.ihm.tools.Tools.completeExecutionResult;

public class Execution extends WithAttachment {
        
    static ISQLExecution pISQLExecution = null; 
        
    protected Date creationDate = null;
    protected Environment environment;
    protected Campaign campagne;
    protected Script initScript;
    protected Script postScript;
    protected DataSet dataSet;
    protected ArrayList executionResultList;
    protected Date lastDate = null;
        
    public Execution(String name, String description) {
        super(name, description);
        executionResultList = new ArrayList();
        if (pISQLExecution == null){
            pISQLExecution = Api.getISQLObjectFactory().getISQLExecution();
        }
                
    }
        
    public Execution(ExecutionWrapper exec) {
        super(exec.getName(),  exec.getDescription());
        idBdd = exec.getIdBDD();
        lastDate = exec.getLastDate();
        creationDate = exec.getCreationDate();
        executionResultList = new ArrayList();
        if (pISQLExecution == null){
            pISQLExecution = Api.getISQLObjectFactory().getISQLExecution();
        }
    }
        
    public Execution(Campaign campagne, ExecutionWrapper exec) {
        this(exec);
        this.campagne =  campagne;
    }
    /******************************************************************************/
    /**                                                         ACCESSEURS ET MUTATEURS                                         ***/
    /******************************************************************************/
        
    public Campaign getCampagneFromModel() {
        return campagne;
    } 
        
    public Date getCreationDateFromModel() {
        return creationDate;
    } 
        
        
    public ArrayList getExecutionResultListFromModel() {
        return executionResultList;
    }
                
    public Date getLastDateFromModel() {
        return lastDate;
    } 
        
    public String getExecutionStatusFromModel() {
        if (executionResultList == null) {
            return ApiConstants.EMPTY;
        } else {
            return completeExecutionResult(this);
        }
    }
        
        
    /*public void setCampagneInModel(Campaign campagne) {
      this.campagne = campagne;
      } */
        
    public void setCreationDateInModel(Date date) {
        creationDate = date;
    } 
        
        
        
        
    void addInDB(User pUser) throws Exception {
        if (isInBase()) {
            throw new Exception("Execution " + name + " is already in BDD");
        } 
        int idDataSet = dataSet.getIdBdd();
        if (campagne.getIdBdd() == -1) {
            throw new Exception("Campagne " + campagne.getNameFromModel() + " is not in BDD");
        }
                
        if (environment.getIdBdd() == -1) {
            throw new Exception("Environment " + environment.getNameFromModel() + " is not in BDD");
        }
                
        if (idDataSet == -1 &&  dataSet.getNameFromModel().equals(ApiConstants.EMPTY_NAME)) {
            idDataSet = 0;
        } else if (idDataSet < 1  ){
            throw new Exception("DataSet " + dataSet.getNameFromModel() + " is not in BDD");
        }
                
        if (pUser.getIdBdd() == -1) {
            throw new Exception("User " + pUser.getNameFromModel() + " is not in BDD");
        }
                
        idBdd =  pISQLExecution.insert(campagne.getIdBdd(), name, environment.getIdBdd(), idDataSet, pUser.getIdBdd(), description);
        Project.pCurrentProject.notifyChanged( ApiConstants.INSERT_EXECUTION ,this);
    }
        
    void addInModel(Campaign campagne, Environment environment, DataSet dataSet)  {
        this.campagne = campagne;
        this.environment = environment;
        this.dataSet = dataSet;
    }
        
        
    void addInBDAndModel(Campaign campagne, Environment environment, DataSet dataSet, User pUser) throws Exception {
        addInDB(pUser);
        addInModel(campagne, environment, dataSet);
    }
        
        
        
    void addInModel(Campaign pCampaign){
        this.campagne = pCampaign;
    }
        
    void addInModel(Campaign pCampaign, HashMap pAttachmentMap){
        this.campagne = pCampaign;
        setAttachmentMapInModel(pAttachmentMap);
    }
        
    void addInDB(User pUser, HashMap pAttachmentMap, File fPre, File fPost) throws Exception{
        if (isInBase()) {
            throw new Exception("Execution " + name + " is already in BDD");
        }
        int transNumber = -1;
        try {
            transNumber = Api.beginTransaction(11, ApiConstants.INSERT_EXECUTION);
            addInDB(pUser);
            Set keysSet = pAttachmentMap.keySet();
                        
            if (keysSet != null){
                for (Iterator iter = keysSet.iterator(); iter.hasNext();) {
                    Attachment attach = getAttachmentFromModel((String)iter.next());
                    if (attach instanceof FileAttachment) {
                        addAttachFileInDB((FileAttachment)attach);
                    } else {
                        addAttachUrlInDB((UrlAttachment)attach);
                    }
                }
            }
            Script initScript = getPreScriptFromModel();
            Script postScript = getPostScriptFromModel();
            if (initScript != null) {
                addScriptInDB(initScript, fPre);
            }
            if (postScript != null) {
                addScriptInDB(postScript, fPost);
            }
                        
            Api.commitTrans(transNumber);
                        
        } catch (Exception exception) {
            Api.forceRollBackTrans(transNumber);
            throw exception;
        }
    }
        
        
    void addInDBAndModel(User pUser, Campaign pCampaign, HashMap pAttachmentMap, File fPre, File fPost) throws Exception{
        addInDB(pUser, pAttachmentMap,fPre, fPost);
        addInModel(pCampaign, pAttachmentMap);
    }
        
    void addInDBAndModel(User pUser, Campaign pCampaign, File fPre, File fPost) throws Exception{
        addInDB(pUser, getAttachmentMapFromModel(),fPre, fPost);
        addInModel(pCampaign);
    }
        
    void updateNameInDB(String newName) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        pISQLExecution.updateName(idBdd, newName);
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_EXECUTION ,this, new String(name), newName);
    }
        
    public void updateNameInModel(String newName) {
        name = newName;
    }
        
    public void updateNameInDBAndModel(String newName) throws Exception {
        updateNameInDB(newName);
        updateNameInModel(newName);
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc) throws Exception {
        updateNameInDB(newName);
        updateNameInModel(newName);
    }
        
    public void updateLastExecDateInDB(Date newDate) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        pISQLExecution.updateDate(idBdd, newDate);
                
    }
        
    public void updateLastExecDateInModel(Date newDate)  {
        lastDate = newDate;
    }
    public void updateLastExecDateInDBAndModel(Date newDate) throws Exception {
        updateLastExecDateInDB(newDate);
        updateLastExecDateInModel(newDate);
    }
        
        
    /* call from Camp */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        pISQLExecution.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION ,this);
    }
        
    /* call from Camp */
    void deleteInModel(){
        //campagne.removeExecution(this);
        for (int i = 0; i < executionResultList.size(); i ++) {
            ExecutionResult pExecutionResult = (ExecutionResult)executionResultList.get(i);
            pExecutionResult.deleteInModel();
            Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_EXECUTION_RESULT ,pExecutionResult);
        }
        clearAttachInModel();
        executionResultList = null;
    }
        
    /* call from Camp */
    void deleteInDBAndModel() throws Exception{
        deleteInDB();
        deleteInModel();
    }
        

    public Vector getResultsTimeFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        long[] tmpArray = pISQLExecution.getAllExecDate(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(new Time(tmpArray[tmpI]));
        }
        return tmpVector;
    }
        
    /*************************** About Environnement *******************************/
        
    public void reloadEnvFromDB(ArrayList envInModel, Hashtable paramInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        Environment environment = null;
        EnvironmentWrapper pEnvW = getEnvironmentWrapperFromDB();
        if (envInModel != null){
            boolean find = false;
            int i = 0;
            while (i < envInModel.size() && !find) {
                if (((Environment)envInModel.get(i)).getNameFromModel().equals(pEnvW.getName())) {
                    environment = (Environment)envInModel.get(i);
                    find = true;
                }
                i++;
            }
        }
        if (environment == null){
            environment = new Environment(pEnvW);
            environment.reloadFromDB(false, paramInModel);
            updateEnvInModel(environment);
            if (envInModel != null){
                envInModel.add(environment);
            }
        } else {
            updateEnvInModel(environment);
        }
                
    }
        
    void updateEnvInDB(int idEnv) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        pISQLExecution.updateEnv(idBdd, idEnv);
    }
        
    public void updateEnvInModel(Environment pEnv) {
        environment = pEnv;
    }
        
    public void updateEnvInDBAndModel(Environment pEnv) throws Exception {
        updateEnvInDB(pEnv.getIdBdd());
        updateEnvInModel(pEnv);
                
    }

    public Environment getEnvironmentFromModel() {
        return environment;
    }
        
    public EnvironmentWrapper getEnvironmentWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        return pISQLExecution.getEnvironmentWrapper(idBdd);
    }
        
    /*************************** About Datasets ************************************/
        
    public void reloadDataSetFromDB(ArrayList dataSetInModel, Hashtable paramInModel) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        DataSet dataSet = null;
        DataSetWrapper pDataSetWrapper = getDataSetWrapperFromDB();
                
        if (pDataSetWrapper == null){
            dataSet = new DataSet(ApiConstants.EMPTY_NAME,"");
            updateDatasetInModel(dataSet);
            return;
        }
                
        if (dataSetInModel != null){
            boolean find = false;
            int i = 0;
            while (i < dataSetInModel.size() && !find) {
                if (((DataSet)dataSetInModel.get(i)).getNameFromModel().equals(pDataSetWrapper.getName())) {
                    dataSet = (DataSet)dataSetInModel.get(i);
                    find = true;
                }
                i++;
            }
        }
        if (dataSet == null){
            dataSet = new DataSet(pDataSetWrapper);
            dataSet.reloadFromDB(false, paramInModel);
            updateDatasetInModel(dataSet);
            if (dataSetInModel != null){
                dataSetInModel.add(dataSet);
            }
        } else {
            updateDatasetInModel(dataSet);
        }
    }
        
    void updateDatasetInDB(int idDataset) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        pISQLExecution.updateDataset(idBdd, idDataset);
    }
        
    public void updateDatasetInModel(DataSet pDataSet)  {
        dataSet = pDataSet;
    }
        
    public void updateDatasetInDBAndModel(DataSet pDataSet) throws Exception {
        updateDatasetInDB(pDataSet.getIdBdd());
        updateDatasetInModel(pDataSet);
    }
        
    public DataSet getDataSetFromModel() {
        return dataSet;
    } 
        
    public DataSetWrapper getDataSetWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        return pISQLExecution.getDataSetWrapper(idBdd);
    }
        
        
    /*************************** About Attachements ******************************/
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        File f = file.getLocalFile();
        int id = pISQLExecution.addAttachFile(idBdd, new SalomeFileWrapper(f), file.getDescriptionFromModel());
        file.setIdBdd(id);
    }
        
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        int id = pISQLExecution.addAttachUrl(idBdd,url.getNameFromModel(), url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
    public void addAttachInModel(Attachment attach) {
        if (!containsAttachmentInModel(attach)){
            addAttachementInModel(attach);
        }
    }
        
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }

        
    public void addAttachInDBAndModel(Attachment attach) throws Exception {
        addAttachementInDB(attach);
        addAttachInModel(attach);
    }
        
    @Override
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        pISQLExecution.deleteAttach(idBdd, attachId);
                
    }
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment pAttach) throws Exception {
        //BDD
        deleteAttachementInDB(pAttach.getIdBdd());
                
        //Mode
        deleteAttachmentInModel(pAttach);
    }
        
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        FileAttachementWrapper[] tmpArray = pISQLExecution.getAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        UrlAttachementWrapper[] tmpArray = pISQLExecution.getAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
                
    }
        
    /*************************** About Scripts ************************************/
        
    public void reloadScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        ScriptWrapper[] pScripts = pISQLExecution.getExecutionScripts(idBdd);
        for (int i =0 ; i < 2 ; i++) {
            ScriptWrapper pScriptWrapper = pScripts[i];
            if (pScripts[i] != null) {
                Script pScript = new Script(pScriptWrapper);
                addScriptInModel(pScript);
            } 
        }
                
    }
        
    public void addScriptInDB(Script script, File file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        int id  = -1;
        if (script.getTypeFromModel().equals(ApiConstants.PRE_SCRIPT)){
            id = pISQLExecution.addPreScript(idBdd, new SalomeFileWrapper(file), script.getDescriptionFromModel(), script.getNameFromModel(), script.getScriptExtensionFromModel(), script.getPlugArgFromModel());
        } else  {
            id = pISQLExecution.addPostScript(idBdd, new SalomeFileWrapper(file), script.getDescriptionFromModel(), script.getNameFromModel(), script.getScriptExtensionFromModel(), script.getPlugArgFromModel());
        }
        script.setIdBdd(id);
    }
    public void addScriptInModel(Script script) {
        if (script.getTypeFromModel().equals(ApiConstants.PRE_SCRIPT)){
            initScript = script;
        } else {
            postScript = script;
        }
    }
        
    public void addPreScriptInModel(Script script) {
        initScript = script;
    }
        
    public void addPostScriptInModel(Script script) {
        postScript = script;
    }
        
    public void addPreScriptInDBAndModel(Script script, File file) throws Exception {
        //Bdd
        addScriptInDB(script, file);
        //Model
        addScriptInModel(script);
    }
        
    public void addPostScriptInDBAndModel(Script script, File file) throws Exception {
        //Bdd
        addScriptInDB(script, file);
                
        //Model
        addScriptInModel(script);
    }
        
    public void addScriptInDBAndModel(Script script, File file) throws Exception {
        //Bdd
        addScriptInDB(script, file);
        //Model
        addScriptInModel(script);
    }
        
    public void deleteScriptInDB(String type) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        if (type.equals(ApiConstants.PRE_SCRIPT)){
            pISQLExecution.deletePreScript(idBdd);
        }else {
            pISQLExecution.deletePostScript(idBdd);
        }
    }
        
    public void deleteScriptInModel(String type) {
        if (type.equals(ApiConstants.PRE_SCRIPT)){
            initScript = null;
        }else {
            postScript = null;
        }
    }
        
    public void deleteScriptInBDAndModel(String type)throws Exception {
        deleteScriptInDB(type);
        deleteScriptInModel(type);
    }
        
    public void deletePreScriptInDBAndModel()throws Exception {
        deleteScriptInDB(ApiConstants.PRE_SCRIPT);
        deleteScriptInModel(ApiConstants.PRE_SCRIPT);
    }
        
        
    public void deletePostScriptInDBAndModel()throws Exception {
        deleteScriptInDB(ApiConstants.POST_SCRIPT);
        deleteScriptInModel(ApiConstants.POST_SCRIPT);
    }
        
    public File getExecScriptFromDB(String type) throws Exception {
        if (type.equals(ApiConstants.PRE_SCRIPT)){
            return getPreExecScriptFromDB();
        }else {
            return getPostExecScriptFromDB();
        }
    }
    public File getPreExecScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        return pISQLExecution.getPreScript(idBdd).toFile();
    }
        
    public File getPostExecScriptFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        return pISQLExecution.getPostScript(idBdd).toFile();
    }
        
    public Script getPreScriptFromModel() { //Init
        return initScript;
    }
        
    public Script getPostScriptFromModel() {
        return postScript;
    }
        
    public ScriptWrapper getPreScriptWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        return getScriptWrapperFromDB(ApiConstants.PRE_SCRIPT);
    }
        
    public ScriptWrapper getPostScriptWrapperFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        return getScriptWrapperFromDB(ApiConstants.POST_SCRIPT);
    }
        
    ScriptWrapper getScriptWrapperFromDB(String type) throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        } 
        ScriptWrapper[] pScripts = pISQLExecution.getExecutionScripts(idBdd);
        int i = 0;
        while  (i < 2) {
            ScriptWrapper pScriptWrapper = pScripts[i];
            if (pScripts[i] != null) {
                if (pScriptWrapper.getType().equals(type)){
                    return pScriptWrapper;
                } 
            } 
            i++;
        }
        return null;
    }
        
    /************ About Execution Result ***************/
        
    public void reloadExecResultFromDB()throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        for (int i = 0; i < executionResultList.size(); i ++) {
            deleteExecutionResultInModel((ExecutionResult)executionResultList.get(i));
        }
        executionResultList.clear();
        Vector execResWrapList =  getExceutionsResultWrapperFromDB();
        for (int i = 0; i < execResWrapList.size(); i++) {
            ExecutionResult execResult = new ExecutionResult ((ExecutionResultWrapper) execResWrapList.get(i), this);
            //execResult.reloadFromDB(false, campagne.getTestListFromModel());
            execResult.reloadFromDB(false);
            addExecutionResultInModel(execResult);
        }
    }
        
    public void setExecutionResultListInModel(ArrayList list) {
        executionResultList = list;
    }
        
    public void addExecutionResultInModel(ExecutionResult result) {
        executionResultList.add(result);
    } 
        
    public void addExecutionResultInDB(ExecutionResult result,  User pUser) throws Exception{
        result.addInDB(this, pUser);
    } 
        
    public void addExecutionResultInDBAndModel(ExecutionResult result, User pUser) throws Exception{
        addExecutionResultInDB(result, pUser);
        addExecutionResultInModel(result);
    }
        
    public ExecutionResult getExecutionResultFromModel(String name) {
        for (int i = 0; i < executionResultList.size(); i ++) {
            if (((ExecutionResult)executionResultList.get(i)).getNameFromModel().equals(name)) {
                return (ExecutionResult)executionResultList.get(i);
            }
        }
        return null;
    } 
        
    public void deleteExecutionResultInModel(ExecutionResult execResult) {
        execResult.deleteInModel();
        executionResultList.remove(execResult);
    } 
        
    public void deleteExecutionResultInDBAndModel(ExecutionResult execResult) throws Exception {
        execResult.deleteInDB();
        deleteExecutionResultInModel(execResult);
    } 
        
    public void deleteTestFromExecInModel(Test test) {
        for (int i = 0 ; i < executionResultList.size() ; i++)
            ((ExecutionResult)executionResultList.get(i)).removeTestResult(test);
    }
        
    public Vector getExceutionsResultWrapperFromDB()throws Exception {
        if (!isInBase()) {
            throw new Exception("Execution " + name + " is not in BDD");
        }
        ExecutionResultWrapper[] tmpArray = pISQLExecution.getExecResults(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    public static boolean isInBase(Campaign pCamp, String execName) {
        try  {
            int id = pISQLExecution.getID(pCamp.getIdBdd(), execName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLExecution.getID(campagne.getIdBdd(), name) == idBdd;
    }
        
    /**************************** Model Conformity operation *************************/
        
    /** 
     * Return true if the excution and his component are in base 
     */
    public boolean isValideModel() throws Exception{
        //dataSet
                
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(11,ApiConstants.LOADING);
            if (!existeInBase()){
                return false;
            }
            if (!dataSet.getNameFromModel().equals(ApiConstants.EMPTY_NAME) && !dataSet.existeInBase()){
                return false;
            }
            if (!environment.existeInBase()){
                return false;
            }           
            Api.commitTrans(transNuber);
            return true;
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
    }
}
