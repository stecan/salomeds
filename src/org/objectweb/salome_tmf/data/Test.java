/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLTest;

abstract public class Test extends WithAttachment{
    static ISQLTest pISQLTest = null;
        
    // Ajour une refence au campagne ??? pour resoudre le Warning
        
    /**
     * Date de creation du test
     */
    protected Date creationDate;
        
    /**
     * Nom complet du concepteur du test
     */
    protected String conceptorName;
        
    /**
     * Login du concepteur du test
     */
    protected String conceptorLogin;
        
    /**
     * Nom de la suite de tests a laquelle appartient le test
     */
    protected TestList testList;
        
    protected int idSuite ;
    /**
     * Position du test dans l'arborescence a partir de la racine
     */
    protected int order;
        
        
    /**
     * Liste des parametres du test
     */
    protected ArrayList parameterList;
        
    public Test(String name, String description){
        super(name, description);
        creationDate = new Date(Calendar.getInstance().getTimeInMillis());
        conceptorName = "";
        conceptorLogin = "";
        description = "";
        testList = null;
        order = 0;
        parameterList = new ArrayList();
        if (pISQLTest == null){
            pISQLTest = Api.getISQLObjectFactory().getISQLTest();
        }
    }
        
    /**
     * Constructeur d'un test
     */
    public Test(TestWrapper pTest) {
        super(pTest.getName(),pTest.getDescription()) ;
        name = pTest.getName();
        creationDate = pTest.getCreationDate();
        conceptorName = pTest.getConceptor(); // modofier pour avoir Nom, Prenom
        idBdd = pTest.getIdBDD();
        conceptorLogin = pTest.getConceptor();
        testList = null;
        parameterList = new ArrayList();
        order = pTest.getOrder();
        idSuite = pTest.getIdSuite();
        if (pISQLTest == null){
            pISQLTest = Api.getISQLObjectFactory().getISQLTest();
        }
    } // Fin di constructeur Test/0
        
    public Test(TestList testList, TestWrapper pTest) {
        this(pTest) ;
        this.testList = testList;
                
    } // Fin di constructeur Test/0
        
        
    //public abstract void  clearCache();
        
    public void realoadBaseFromDB()throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        TestWrapper pTest= pISQLTest.getTest(idBdd);
        name = pTest.getName();
        description = pTest.getDescription();
        //creationDate = pTest.getCreationDate();
        //conceptorName = pTest.getConceptor(); // modofier pour avoir Nom, Prenom
        //conceptorLogin = pTest.getConceptor();
        order = pTest.getOrder();
        //idSuite = pTest.getIdSuite();
    }
        
    protected void commonReloadFromDB(boolean base, Hashtable<String,Parameter> paramInModel, boolean attach) throws Exception { 
        int transNuber = -1;
        try {
            transNuber = Api.beginTransaction(101, ApiConstants.LOADING);
                        
            if (base){
                realoadBaseFromDB();
            }
            reloadUSeParameterFromDB(paramInModel);     
            if (attach){
                reloadAttachmentDataFromDB(false);
            }
            Api.commitTrans(transNuber);
        } catch (Exception e){
            Api.forceRollBackTrans(transNuber);
            throw e;
        }
                
    }
        
    public abstract void reloadFromDB(boolean base, Hashtable<String,Parameter> paramInModel, boolean attach)  throws Exception ;
        
        
    /**************************************************************************/
    /**                                         ACCESSEURS ET MUTATEURS                                                 ***/
    /**************************************************************************/
        
    int getIdSuiteFromModel(){
        return idSuite;
    }
        
    public String getConceptorFromModel() {
        return conceptorName;
    } 
        
    public Date getCreationDateFromModel() {
        return creationDate;
    } 
        
        
    public TestList getTestListFromModel() {
        return testList;
    } 
        
    public int getOrderFromModel() {
        return order;
    } 
        
    public int getOrderFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
                
        return pISQLTest.getTest(idBdd).getOrder();
    } 
        
    public void setConceptorInModel(String name) {
        conceptorName = name;
    }
        
    public void setCreationDateInModel(Date date) {
        creationDate = date;
    } 
        
    public void setTestListInModel(TestList list) {
        testList = list;
        idSuite = list.getIdBdd();
    } 
        
    public void setOrderInModel(int i) {
        order = i;
    }
        
    public String getConceptorLoginFromModel() {
        return conceptorLogin;
    }
        
    public void setConceptorLoginInModel(String string) {
        conceptorLogin = string;
    }
        
        
    abstract public String getExtensionFromModel();
        
        
    /***************************** Basic Operation *********************************/
        
        
    /* used by TestList */
    abstract void addInDB(int idTestList) throws Exception;
        
    /* used by TestList */
    void addInModel(TestList pList){
        testList = pList;
        idSuite = pList.getIdBdd();
    }
        
    /* used by TestList */
    void addInDBAndModel(TestList pList) throws Exception{
        addInModel(pList);
        addInDB(pList.getIdBdd());
    }
        
    public void updateInDB(String newName, String newDesc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        pISQLTest.update(idBdd, newName, newDesc );
        Project.pCurrentProject.notifyChanged( ApiConstants.UPDATE_TEST ,this, new String(name), newName);
    }
        
    public void updateInModel(String newName, String newDesc) {
        name = newName;
        description = newDesc;
    }
        
    @Override
    public void updateInDBAndModel(String newName, String newDesc)throws Exception {
        //DB
        updateInDB(newName, newDesc);
        //Model
        updateInModel(newName, newDesc);
                
    }
        
    public void updateOrderInDBAndModel(boolean inc) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        order = pISQLTest.updateOrder(idBdd, inc);
    }
        
        
    /* used by TestList */
    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        pISQLTest.delete(idBdd);
        Project.pCurrentProject.notifyChanged( ApiConstants.DELETE_TEST ,this);
    }
        
    /*  
     * WARNING This methode don't delete the test in campaign model
     * Use for that TestPlanData.deleteTestFromListFromModel()
     */
    abstract void deleteInDBAndModel() throws Exception;
        
    /*   
     * used by TestList
     * WARNING This methode don't delete the test in campaign model
     * Use for that TestPlanData.deleteTestFromListFromModel()
     */
    abstract void deleteInModel() ;
        
    /************************** ATTACHEMENTS ******************************/
    @Override
    public void addAttachementInDB (Attachment attach )throws Exception {
        if (attach instanceof FileAttachment) {
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
    }
        
    void addAttachFileInDB(FileAttachment file) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        } 
        File f = file.getLocalFile();
        int id = pISQLTest.addAttachFile(idBdd,new SalomeFileWrapper(f),file.getDescriptionFromModel());
        file.setIdBdd(id);
                
                
    }
        
    void addAttachUrlInDB(UrlAttachment url) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        int id = pISQLTest.addAttachUrl(idBdd, url.getNameFromModel(),url.getDescriptionFromModel());
        url.setIdBdd(id);
    }
        
        
    public void addAttachInDBAndModel(Attachment attach)        throws Exception {
        if (attach instanceof FileAttachment){
            addAttachFileInDB((FileAttachment) attach);
        } else {
            addAttachUrlInDB((UrlAttachment) attach);
        }
        addAttachementInModel(attach);
    }
        
        
    @Override
    public void deleteAttachementInDBAndModel(Attachment attach)throws Exception {
        deleteAttachementInDB(attach.getIdBdd());
                
        deleteAttachmentInModel(attach);
    }
        
    @Override
    protected void deleteAttachementInDB(int attachId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        pISQLTest.deleteAttach(idBdd, attachId);
    }
        
    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        FileAttachementWrapper[] tmpArray = pISQLTest.getAllAttachFiles(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLTest.getAllAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
    /************************** PARAMETERS ********************************/
        
    public void reloadUSeParameterFromDB(Hashtable paramInModel) throws Exception {
        parameterList.clear();
        Vector paramList = getUsedParametersFromDB() ;
        for (int i = 0; i < paramList.size(); i++) {
            ParameterWrapper pParameterWrapper = (ParameterWrapper) paramList.elementAt(i);
            Parameter param = null;
            if (paramInModel != null){
                param = (Parameter) paramInModel.get(pParameterWrapper.getName());
                if (param == null){
                    param = new Parameter(pParameterWrapper);
                }
            } else {
                param = new Parameter(pParameterWrapper);
            }
            setUseParamInModel(param);
        }
    }
        
    public void setUseParamInDB(int paramId) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        } 
        if (paramId == -1) {
            throw new Exception("Parameter is not in BDD");
        }
        pISQLTest.addUseParam(idBdd, paramId);
    }
        
    public void setUseParamInModel(Parameter param) {
        parameterList.add(param);
    } // Fin de la methode addParamter/1
        
        
    public void setUseParamInDBAndModel(Parameter pParam) throws Exception {
        //DB
        setUseParamInDB(pParam.getIdBdd());
        //Model
        setUseParamInModel(pParam);
    }
        
    public boolean hasUsedParameterNameFromModel(String name) {
        for (int i=0; i < parameterList.size(); i++) {
            if (((Parameter)parameterList.get(i)).getNameFromModel().equals(name)) {
                return true;
            }
        }
        return false;
    }
        
    abstract void deleteUseParameterInModel(Parameter pParm);
        
    protected void deleteUseParameterInModel(String name) {
        for (int i=0; i < parameterList.size(); i++) {
            if (((Parameter)parameterList.get(i)).getNameFromModel().equals(name)) {
                parameterList.remove(i);
                return;
            }
        }
    }
        
    void deleteUseParamInDB(Parameter pParam) throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        if (!pParam.isInBase()){
            throw new Exception("Param " + pParam.getNameFromModel() + " is not in BDD");
        }
        pISQLTest.deleteUseParam(idBdd, pParam.getIdBdd());
    }
        
    public void deleteUseParamInDBAndModel(Parameter pParam) throws Exception {
        deleteUseParamInDB(pParam);
        deleteUseParameterInModel(pParam);
    }
        
        
    public Parameter getUsedParameterFromModel(String name) {
        for (int i=0; i < parameterList.size(); i++) {
            if (((Parameter)parameterList.get(i)).getNameFromModel().equals(name)) {
                return (Parameter)parameterList.get(i);
            }
        }
        return null;
    }
        
        
    public ArrayList getParameterListFromModel() {
        return parameterList;   
    } 

    public Vector getUsedParametersFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("Test " + name + " is not in BDD");
        }
        ParameterWrapper[] tmpArray = pISQLTest.getAllUseParams(idBdd);
        Vector tmpVector = new Vector();
        for(int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }
        
    ///////////////////////////////////////////////////////////////////////////////
    public static boolean isInBase( TestList testList, Test test) {
        return isInBase( testList, test.getNameFromModel());
    } // Fin de la methode isInBase/1
        
    public static boolean isInBase( TestList testList, String testName) {
        try  {
            int id = pISQLTest.getID(testList.getIdBdd(), testName);
            if (id > 0){
                return true;
            }
            return false;
        } catch (Exception e) {
                        
        }
        return false;
    } // Fin de la methode isInBase/1
        
    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLTest.getID(testList.getIdBdd(), name) == idBdd;
    }
}
