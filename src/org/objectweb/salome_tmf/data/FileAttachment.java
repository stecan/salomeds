/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.data.FileAttachementWrapper;
import org.objectweb.salome_tmf.api.data.SalomeFileWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLFileAttachment;

public class FileAttachment extends Attachment{
        
    transient static ISQLFileAttachment pISQLFileAttachment = null;
        
        
                
    private Long size;
    private Date date;
        
    /////////////////////// Selecteur //////////////////////////
    public Date getDate(){
        return date;
    }
        
    public Long getSize(){
        return size;
    }
        
    ///////////////////// Constructeur ///////////////////////////
        
    public FileAttachment(String name, String description) {
        super(name, description);
        if (pISQLFileAttachment == null){
            pISQLFileAttachment = Api.getISQLObjectFactory().getISQLFileAttachment();
        }
    }
        
    public FileAttachment(File f, String description) {
        super(f.getName(), description);
        size = new Long(f.length());
        date =  new Date(f.lastModified());
        localisation = f.getAbsolutePath();
        if (pISQLFileAttachment == null){
            pISQLFileAttachment = Api.getISQLObjectFactory().getISQLFileAttachment();
        }
    }
        
    @Override
    public void  clearCache(){
        /* NOTHING */
    }
        
    //  public FileAttachment(SalomeFileWrapper f, String description) {
    //          super(f.getName(), description);
    //          size = new Long(f.getLength());
    //          date =  new Date(f.getLastModified());
    //          localisation = f.getAbsolutePath();
    //          if (pISQLFileAttachment == null){
    //                  pISQLFileAttachment = Api.getISQLObjectFactory().getISQLFileAttachment();
    //          }
    //  }
        
    public FileAttachment(FileAttachementWrapper pFileAttachementWrapper) {
        super(pFileAttachementWrapper.getName(), pFileAttachementWrapper.getDescription());
        idBdd = pFileAttachementWrapper.getIdBDD();
        date = pFileAttachementWrapper.getDate();
        size = pFileAttachementWrapper.getSize();
        localisation = pFileAttachementWrapper.getLocalisation();
        if (pISQLFileAttachment == null){
            pISQLFileAttachment = Api.getISQLObjectFactory().getISQLFileAttachment();
        }
    }
        
    ///////////////////// Methode //////////////////////////
        
    public void updateInDB(File f) throws Exception  {
        if (!f.exists()) {
            throw new Exception ("[FileAttachment] File no exits");
        }
        pISQLFileAttachment.updateFile(idBdd, new SalomeFileWrapper(f));
    }
        
    public void updateInModel(File f){
        name = f.getName();
        size = new Long(f.length());
        date =  new Date(f.lastModified());
        localisation = f.getAbsolutePath();
    }
        
    public void updateInDBAndModel(File f) throws Exception {
        updateInDB(f);
        updateInModel(f);
    }
        
        
    public File getFileFromDB() throws Exception {
        File f = getFileFromDB(null);
        localisation = f.getAbsolutePath();
        return f;
                
        //              SalomeFileWrapper f =  pISQLFileAttachment.getFile(idBdd);
        //              
        //              String path = null;
        //              File file = null;
        //              InputStream is = null;
        //              String tmpDir;
        //              Properties sys = System.getProperties();
        //              String fs = sys.getProperty("file.separator");
        //              if (path == null || path.equals("")){
        //                      // Repertoire temporaire systeme
        //                      tmpDir = sys.getProperty("java.io.tmpdir");
        //                      tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP + fs;
        //              } else {
        //                      tmpDir = path;
        //                      if (!path.endsWith(fs)){
        //                              tmpDir = tmpDir + fs;
        //                      }
        //                      
        //              }
        //              Util.log("[SQLFileAttachment->getFile] at path : " + tmpDir);
        //              file = new File(tmpDir + f.getName());
        //              File fPath = new File(file.getCanonicalPath().substring(0,file.getCanonicalPath().lastIndexOf(fs)+1));
        //              if (!fPath.exists()){
        //                      fPath.mkdirs();
        //              }
        //              file.createNewFile();
        //              
        //              // Flux d'ecriture sur le fichier
        //              FileOutputStream fos = new FileOutputStream(file);
        //              BufferedOutputStream bos = new BufferedOutputStream(fos);
        //              
        //              // Flux du fichier stocke dans la BdD 
        //              bos.write(f.getContent());
        //              
        //              //      Fermeture des flux de donnees
        //              bos.flush();
        //              bos.close();
        //              
        //              localisation = file.getAbsolutePath();
        //              return new SalomeFileWrapper(file);
    }
        
    public File getFileFromDB(String filePath) throws Exception {
                
        //long debut = Calendar.getInstance().getTimeInMillis();
        return pISQLFileAttachment.getFile(idBdd).toFile(filePath);
                
        /*
          SalomeFileWrapper f = pISQLFileAttachment.getFile(idBdd);
                
          //String path = null;
          String path = filePath;
          File file = null;
          String tmpDir;
          Properties sys = System.getProperties();
          String fs = sys.getProperty("file.separator");
          if (path == null || path.equals("")){
          // Repertoire temporaire systeme
          tmpDir = sys.getProperty("java.io.tmpdir");
          tmpDir = tmpDir + fs + ApiConstants.PATH_TO_ADD_TO_TEMP + fs;
          } else {
          tmpDir = path;
          if (!path.endsWith(fs)){
          tmpDir = tmpDir + fs;
          }
                        
          }
          Util.log("[SQLFileAttachment->getFile] at path : " + tmpDir);
          file = new File(tmpDir + f.getName());
          File fPath = new File(file.getCanonicalPath().substring(0,file.getCanonicalPath().lastIndexOf(fs)+1));
          if (!fPath.exists()){
          fPath.mkdirs();
          }
          file.createNewFile();
                
          // Flux d'ecriture sur le fichier
          FileOutputStream fos = new FileOutputStream(file);
          BufferedOutputStream bos = new BufferedOutputStream(fos);
                
          // Flux du fichier stocke dans la BdD 
          bos.write(f.getContent());
                
          //    Fermeture des flux de donnees
          bos.flush();
          bos.close();
                
          return file;*/
    }
        
    public File getFileFromDBIn(File f) throws Exception {
        SalomeFileWrapper file = pISQLFileAttachment.getFileIn(idBdd, new SalomeFileWrapper(f));
        //localisation = f.getAbsolutePath();
        f.createNewFile();
                
        // Flux d'ecriture sur le fichier
        FileOutputStream fos = new FileOutputStream(f);
        BufferedOutputStream bos = new BufferedOutputStream(fos);
                
        // Flux du fichier stocke dans la BdD 
        bos.write(file.getContent());
                
        //      Fermeture des flux de donnees
        bos.flush();

        return file.toFile();
    }
        
    public void deleteFromDisk() throws Exception{
        if (localisation != null && !localisation.equals("")){
            File file = new File(localisation);
            if (file.exists()) {
                file.delete();
            }
        }
        localisation = null;
    }

    public File getLocalFile() throws Exception{
        File file = null;
        if (localisation != null && !localisation.equals("")){
            file = new File(localisation);
        }
        return file;
    }
        
    @Override
    public boolean equals(Object o){
        if (o instanceof FileAttachment) {
            FileAttachment toTest =  (FileAttachment)  o;
            if (isInBase() && toTest.isInBase()) {
                if (getIdBdd() ==  toTest.getIdBdd()){
                    return true;
                }
            } else {
                String loc2 = toTest.getLocalisation();
                if (localisation != null && !localisation.equals("") && loc2 != null && !loc2.equals("")){
                    return localisation.equals(loc2);
                }
                return super.equals(o);
            }
        }
        return false;
    }
}
