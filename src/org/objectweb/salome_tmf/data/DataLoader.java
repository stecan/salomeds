/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.net.URL;
import java.util.ArrayList;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.AutomaticTestWrapper;
import org.objectweb.salome_tmf.api.data.CampaignWrapper;
import org.objectweb.salome_tmf.api.data.EnvironmentWrapper;
import org.objectweb.salome_tmf.api.data.ExecutionWrapper;
import org.objectweb.salome_tmf.api.data.FamilyWrapper;
import org.objectweb.salome_tmf.api.data.ManualTestWrapper;
import org.objectweb.salome_tmf.api.data.ParameterWrapper;
import org.objectweb.salome_tmf.api.data.SuiteWrapper;
import org.objectweb.salome_tmf.api.data.TestCampWrapper;
import org.objectweb.salome_tmf.api.data.TestWrapper;

public class DataLoader {
    /* Environnement vide */
    static private Environment emptyEnvironment;

    /* Jeu de donnees vide */
    static private DataSet emptyDataSet;

    /* Wrapper to IHM */
    static private IDataModelWrapper pDataModelWrapper = null;
    static private Project currentProject;
    static private User currentUser;

    static boolean isDynamic = true;

    /* Constructeur */
    public DataLoader(IDataModelWrapper pDataModelWrapper){
        DataLoader.pDataModelWrapper = pDataModelWrapper;
    }


    /**************************************************************************/
    /**                                                 METHODES D'ACCES AUX DONNES                                 ***/
    /**************************************************************************/

    public static void setDataModelWrapper(IDataModelWrapper ptrDataModelWrapper){
        pDataModelWrapper = ptrDataModelWrapper;
    }


    public static DataSet getEmptyDataSet() {
        if (emptyDataSet == null){
            emptyDataSet = new DataSet(ApiConstants.EMPTY_NAME,"");
        }
        return emptyDataSet;
    }


    public static Environment getEmptyEnvironment() {
        if (emptyEnvironment == null){
            emptyEnvironment = new Environment(ApiConstants.EMPTY_NAME, "");
        }
        return emptyEnvironment;
    }


    public static User getCurrentUser() {
        return currentUser;
    }


    public static Project getCurrentProject() {
        return currentProject;
    }


    //****************** Open Connection  to BDD DATA ********************/

    public static boolean dataConnected(){
        return Api.isConnected();
    }


    /**************** Chargement des donnees ********************************/

    private static void clearData() {
        currentProject.clearCampaignData();
        currentProject.clearTestPlanData();
    }

    private static void clearTestPlanData() {
        currentProject.clearTestPlanData();
    }

    private static void clearCampainData() {
        currentProject.clearCampaignData();
    }



    public static void loadData(String projectName, String userLogin, URL url, boolean dynamicLoad) throws Exception {
        try {
            isDynamic = dynamicLoad;
            if (Api.isConnected()) {
                currentProject = new Project(projectName);
                currentUser = new User(userLogin);

                //20100310 - D\ufffdbut modification Forge ORTF v1.0.0
                try {
                    Api.getISQLObjectFactory().getISQLSalomeLock().delete(currentProject.getIdBdd());
                } catch (Exception e) {
                    Util.err(e);
                    Util.log("The database is read only. The purge of Salome-Locks is ignored!");
                }
                
                //20100310 - Fin modification Forge ORTF v1.0.0
                currentProject.setUserInModel(currentUser);
                Api.getISQLObjectFactory().setConnexionInfo(currentProject.getIdBdd(), currentUser.getIdBdd());
                int transNumber = -1;
                try {
                    transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
                    currentProject.setLoad(true);
                    loadParametersData();
                    loadEnvironmentData();
                    loadTestData();
                    loadCampaignData();
                    currentProject.setLoad(false);
                    Api.commitTrans(transNumber);
                } catch (Exception exception) {
                    Api.forceRollBackTrans(transNumber);
                    currentProject.setLoad(false);
                    throw exception;
                }
            }
        } catch (Exception e) {
            Util.err(e);
            throw e;
        }
    } // Fin de la methode initDataTests

    private static void loadParametersData() throws Exception{
        Vector projectParam  = currentProject.getParametersWrapperFromDB();
        if (pDataModelWrapper != null){
            Util.log("[DataLoader->loadParametersData] clearParameterToModel");
            pDataModelWrapper.clearParameterToModel();
        }
        for (int m = 0; m < projectParam.size(); m++) {
            ParameterWrapper paramBdd = (ParameterWrapper) projectParam.get(m);
            Parameter param =  new Parameter(paramBdd);
            currentProject.addParameterToModel(param);

            ArrayList data = new ArrayList();
            data.add(param.getNameFromModel());
            data.add(param.getDescriptionFromModel());

            if (pDataModelWrapper != null)
                pDataModelWrapper.addParameterToModel(data);

        }
    }



    public static void reloadData(boolean dynamicLoad) throws Exception {
        currentProject.setLoad(true);
        clearTestPlanData();
        clearCampainData();
        isDynamic = dynamicLoad;
        int transNumber = -1;
        if (Api.isConnected()) {
            transNumber = Api.beginTransaction(111, ApiConstants.LOADING);
            try {

                loadParametersData();
                loadEnvironmentData();
                loadTestData();
                loadCampaignData();

                Api.commitTrans(transNumber);
            } catch (Exception exception) {
                Api.forceRollBackTrans(transNumber);
                currentProject.setLoad(false);
                throw exception;
            }

        }
        currentProject.setLoad(false);
    }

    /* ancien refresh refreshAllTestData*/
    public static void realoadTestData(boolean dynamicLoad ) throws Exception {
        if (Api.isConnected()) {
            isDynamic = dynamicLoad;
            currentProject.setLoad(true);
            loadTestData();
            currentProject.setLoad(false);
        }
    }


    /* ancien refresh refreshAllCampaignData*/
    public static void reloadCampaignData(boolean dynamicLoad ) throws Exception {
        if (Api.isConnected()) {
            isDynamic = dynamicLoad;
            currentProject.setLoad(true);
            loadCampaignData();
            currentProject.setLoad(false);
        }
    }

    /* ancien refresh refreshEnvironmentAndParameter */
    public static void reloadEnvironmentAndParameterData(boolean dynamicLoad) throws Exception{
        if (Api.isConnected()) {
            isDynamic = dynamicLoad;
            currentProject.setLoad(true);
            loadParametersData();
            loadEnvironmentData();
            currentProject.setLoad(false);
        }
    }




    private static void loadEnvironmentData() throws Exception {
        Vector environmentOfProject = currentProject.getEnvironmentWrapperFromDB();
        for (int m = 0; m < environmentOfProject.size(); m++) {
            EnvironmentWrapper envDB = (EnvironmentWrapper) environmentOfProject.get(m);
            Environment env =  new Environment(envDB);
            currentProject.addEnvironmentInModel(env);

            env.reloadFromDB(false, currentProject.getParameterSetFromModel());

            ArrayList data = new ArrayList();
            data.add(env.getNameFromModel());
            if (env.getInitScriptFromModel() != null) {
                data.add(env.getInitScriptFromModel().getNameFromModel());
            } else {
                data.add("");
            }
            data.add(env.getParametersHashTableFromModel());
            data.add(env.getDescriptionFromModel());
            if (pDataModelWrapper != null)
                pDataModelWrapper.addEnvToModel(data);

        }

    }

    private static void loadFamilyData(Family family) throws Exception {
        Vector testListVector = family.getSuitesWrapperFromDB();
        for (int j = 0; j < testListVector.size(); j++) {
            SuiteWrapper testListBdd = (SuiteWrapper) testListVector.get(j);
            TestList testList = new TestList(family, testListBdd);
            family.addTestListInModel(testList);

            if (pDataModelWrapper != null) {
                pDataModelWrapper.addTestListToModel(family, testList);
            }
            //testList.loadAttachmentDataFromDB();
            testList.reloadAttachmentDataFromDB(false);
            //family.addTestList(testList);
            loadTestListData(testList);

            if (pDataModelWrapper != null)
                pDataModelWrapper.refreshTestListToModel(testList);

        }
    } // Fin de la methode initFamily/2


    private static void loadTestListData(TestList testList) throws Exception {
        Vector testVector =  testList.getTestsWrapperFromDB();

        for (int k = 0; k < testVector.size(); k ++) {
            TestWrapper testBdd = (TestWrapper) testVector.get(k);
            Test test = null;
            if (testBdd.getType().equals(ApiConstants.MANUAL)){
                test = new ManualTest(new ManualTestWrapper(testBdd));
                //              test = new ManualTest((ManualTestWrapper)testBdd);
            } else {
                test = new AutomaticTest(new AutomaticTestWrapper(testBdd));
                //                      test = new AutomaticTest((AutomaticTestWrapper)testBdd);
            }
            testList.addTestInModel(test);
            test.reloadFromDB(false,currentProject.getParameterSetFromModel(), true);

            if (pDataModelWrapper != null) {
                pDataModelWrapper.addTestToModel(testList, test);
            }
        }

    } // Fin de la methode refreshTestList/2

    private static void loadTestData() throws Exception {

        Vector familyVector = currentProject.getProjectFamiliesWrapperFromDB();
        for (int i = 0; i < familyVector.size(); i ++) {
            FamilyWrapper familyBdd = (FamilyWrapper) familyVector.get(i);
            Family family = new Family(currentProject, familyBdd);
            currentProject.addFamilyInModel(family);

            if (pDataModelWrapper != null) {
                pDataModelWrapper.addFamilyToModel(family);
            }

            loadFamilyData(family);

            if (pDataModelWrapper != null)
                pDataModelWrapper.refreshFamilyToModel(family);
        }
    }


    /**
     * @method loadCampaignData()
     * @throws Exception
     * @covers SFG_ForgeORTF_TST_CMP_000030 - \ufffd2.4.4
     * @covers EDF-2.4.4
     * @jira FORTF-6
     */
    private static void loadCampaignData() throws Exception {
        //boolean newCampaign = false;
        Vector campaignVector = currentProject.getCampaignsWrapperListFromDB();
        for (int i = 0; i < campaignVector.size(); i++) {
            CampaignWrapper campaignBdd = (CampaignWrapper) campaignVector.get(i);
            Campaign campaign =  new Campaign(campaignBdd);
            //currentProject.addCampaignInModel(campaign);
            if (pDataModelWrapper != null) {
                pDataModelWrapper.addCampaignToModel(campaign);
            }
            Vector testCampWrapperList = campaign.getTestsByOrderFromDB();
            TestList pCurrentTestList = null;
            for (int j = 0; j < testCampWrapperList.size() ; j++){
                TestCampWrapper pTestCampWrapper = (TestCampWrapper)testCampWrapperList.elementAt(j);
                int idTest = pTestCampWrapper.getIdBDD();
                int idUser = pTestCampWrapper.getIdUser();
                if (idUser == -1){
                    idUser = currentUser.getIdBdd();
                }
                Test pTest = currentProject.getTestFromModel(idTest);
                TestList pTestList = pTest.getTestListFromModel();
                Family pFamily = pTestList.getFamilyFromModel();

                if (campaign.containsFamilyInModel(pFamily)){
                    if (pDataModelWrapper != null) {
                        pDataModelWrapper.upadateCampaignFamilyToModel(campaign,pFamily);
                    }
                } else {
                    if (pDataModelWrapper != null) {
                        pDataModelWrapper.addCampaignFamilyToModel(campaign,pFamily);
                    }
                }
                //20100108 - D\ufffdbut modification Forge ORTF V1.0.0
                //                      if (campaign.containsTestListInModel(pTestList)){
                if(pTestList.equals(pCurrentTestList)){
                    if (pDataModelWrapper != null) {
                        pDataModelWrapper.upadateCampaignTestListToModel(campaign, pFamily, pTestList);
                    }
                } else {
                    if (pDataModelWrapper != null) {
                        pDataModelWrapper.addCampaignTestListToModel(pFamily, pTestList);
                    }
                    pCurrentTestList = pTestList;
                } if (!campaign.containsTestInModel(pTest)){
                    if (pDataModelWrapper != null) {
                        pDataModelWrapper.addCampaignTestToModel(pTestList, pTest);
                    }
                }
                //20100108 - Fin modification Forge ORTF V1.0.0

                campaign.addTestInModel(pTest, idUser);
            }

            campaign.loadDataSetFromBase(currentProject.getParameterSetFromModel());


            Vector execVector = campaign.getExecutionWrapperListFromDB();
            for (int k = 0; k < execVector.size(); k++) {
                Execution exec = new Execution(campaign, (ExecutionWrapper) execVector.get(k));
                exec.reloadEnvFromDB(currentProject.getEnvironmentListFromModel(), currentProject.getParameterSetFromModel());
                exec.reloadDataSetFromDB(campaign.getDataSetListFromModel(), currentProject.getParameterSetFromModel());
                exec.reloadScriptFromDB();
                exec.reloadExecResultFromDB();
                //exec.loadAttachmentDataFromDB();
                exec.reloadAttachmentDataFromDB(false);

                campaign.addExecutionInModel(exec);

                /*Vector execResults = ConnectionData.getCampTestSelect().getExecutionResults(exec.getIdBdd());
                  for (int h = 0; h < execResults.size(); h++) {
                  ExecutionResult execResult = new ExecutionResult ((ExecutionResultWrapper) execResults.get(h));
                  execResult.reloadTestResult(campaign.getTestListFromModel());
                  execResult.reloadAttachmentDataFromDB();
                  exec.addExecutionResultInModel(execResult);
                  }*/
            }
            //campaign.loadAttachmentDataFromDB();
            campaign.reloadAttachmentDataFromDB(false);
            currentProject.addCampaignInModel(campaign);
            if (pDataModelWrapper != null) {
                pDataModelWrapper.refreshCampaignToModel(campaign);
            }
        }
    }

}
