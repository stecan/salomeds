/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.sql.Date;
import java.util.Comparator;
import java.util.Vector;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.data.UrlAttachementWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;

public class User extends WithAttachment implements Comparable<User> {

    static ISQLPersonne pISQLPersonne = null;

    // private String login = name;
    private String lastName;
    protected String firstName;
    protected String email;
    protected String phoneNumber;
    protected Date creationDate;

    public User(String login, String description) {
        super(login, description);
        lastName = "";
        firstName = "";
        email = "";
        phoneNumber = "";
        if (pISQLPersonne == null) {
            pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
        }
    }

    public User(UserWrapper pUser) {
        super(pUser.getLogin(), pUser.getDescription());
        lastName = pUser.getName();
        firstName = pUser.getPrenom();
        email = pUser.getEmail();
        phoneNumber = pUser.getTel();
        creationDate = pUser.getCreateDate();
        idBdd = pUser.getIdBDD();
        if (pISQLPersonne == null) {
            pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
        }
    }

    public User(String login) throws Exception {
        super(login, "");
        if (pISQLPersonne == null) {
            pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
        }
        UserWrapper pUser = pISQLPersonne.getUserByLogin(login);
        description = pUser.getDescription();
        lastName = pUser.getName();
        firstName = pUser.getPrenom();
        email = pUser.getEmail();
        phoneNumber = pUser.getTel();
        creationDate = pUser.getCreateDate();
        idBdd = pUser.getIdBDD();
    }

    @Override
    public void clearCache() {
        /* NOTHING */
    }

    public Date getCreationDateFromModel() {
        return creationDate;
    }

    public String getEmailFromModel() {
        return email;
    }

    public String getFirstNameFromModel() {
        return firstName;
    }

    public String getLoginFromModel() {
        return name;
    }

    public String getPhoneNumberFromModel() {
        return phoneNumber;
    }

    public String getLastNameFromModel() {
        return lastName;
    }

    public String getTwoNameFromModel() {
        return lastName + " " + firstName;
    }

    public void setLastNameInModel(String string) {
        lastName = string;
    }

    public void setCreationDateInModel(Date date) {
        creationDate = date;
    }

    public void setEmailInModel(String string) {
        email = string;
    }

    public void setFirstNameInModel(String string) {
        firstName = string;
    }

    public void setLoginInModel(String string) {
        name = string;
    }

    public void setPhoneNumberInModel(String string) {
        phoneNumber = string;
    }

    /****************************** DB **********************************/
    public void addInDB(String pwd) throws Exception {
        // public void addInDB(String login, String name, String firstName,
        // String desc, String email, String tel, String pwd) throws Exception{
        if (isInBase()) {
            throw new Exception("User " + name + " is already in BDD");
        }
        int id = pISQLPersonne.insert(name, lastName, firstName, description,
                                      email, phoneNumber, pwd, true);
        setIdBdd(id);
    }

    public String getPasswordFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("User " + name + " is not in BDD");
        }
        UserWrapper pUserWrapper = pISQLPersonne.getUserByLogin(name);
        return pUserWrapper.getPassword();
    }

    public String updatePasswordInDB(String newPassword) throws Exception {
        if (!isInBase()) {
            throw new Exception("User " + name + " is not in BDD");
        }
        return pISQLPersonne.updatePassword(name, newPassword, true);
    }

    public void updateInDB(String newLogin, String newName,
                           String newFirstName, String newDesc, String newEmail, String newTel)
        throws Exception {
        if (!isInBase()) {
            throw new Exception("User " + name + " is not in BDD");
        }
        pISQLPersonne.update(idBdd, newLogin, newName, newFirstName, newDesc,
                             newEmail, newTel);
    }

    public void updateInDBModel(String newLogin, String newName,
                                String newFirstName, String newDesc, String newEmail, String newTel)
        throws Exception {
        updateInDB(newLogin, newName, newFirstName, newDesc, newEmail, newTel);
        updateInModel(newLogin, newName, newFirstName, newDesc, newEmail,
                      newTel);
    }

    public void updateInModel(String newLogin, String newName,
                              String newFirstName, String newDesc, String newEmail, String newTel) {
        name = newLogin;
        lastName = newName;
        firstName = newFirstName;
        description = newDesc;
        email = newEmail;
        phoneNumber = newTel;
    }

    @Override
    public void updateInDBAndModel(String newName, String newDesc)
        throws Exception {
        throw new Exception("NOT IMPLEMENTED");
    }

    void deleteInDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("User " + name + " is not in BDD");
        }
        // Util.log("[User->deleteInDB]  will delete " + name);
        pISQLPersonne.deleteById(idBdd);
    }

    void deleteInProjectInDB(String project) throws Exception {
        if (!isInBase()) {
            throw new Exception("User " + name + " is not in BDD");
        }
        // Util.log("[User->deleteInDB]  will delete " + name);
        pISQLPersonne.deleteInProject(idBdd, project);
    }

    @Override
    public boolean existeInBase() throws Exception {
        if (!isInBase()) {
            return false;
        }
        return pISQLPersonne.getID(name) == idBdd;
    }

    // ////////////////////////////////////// EQUALS
    // //////////////////////////////////
    @Override
    public boolean equals(Object o) {
        User testedUser;
        boolean test = false;
        if (!(o instanceof User)) {
            return false;
        }
        testedUser = (User) o;
        if (isInBase() && testedUser.isInBase()) {
            test = idBdd == testedUser.idBdd;
        } else {
            test = name.equals(testedUser.getNameFromModel());
        }
        return test;
    }

    @Override
    public int hashCode() {
        if (isInBase())
            return idBdd;
        else
            return super.hashCode();
    }

    /** WithAttachment Method */

    @Override
    public void addAttachementInDB(Attachment attach) throws Exception {
        if (attach instanceof UrlAttachment) {
            int id = pISQLPersonne.addAttachUrl(idBdd, attach
                                                .getNameFromModel(), attach.getDescriptionFromModel());
            attach.setIdBdd(id);
        }

    }

    @Override
    protected void deleteAttachementInDB(int idAttach) throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLPersonne.deleteAttach(idBdd, idAttach);

    }

    @Override
    public void deleteAttachementInDBAndModel(Attachment attach)
        throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        pISQLPersonne.deleteAttach(idBdd, attach.getIdBdd());

    }

    @Override
    public Vector getAttachFilesFromDB() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Vector getAttachUrlsFromDB() throws Exception {
        if (!isInBase()) {
            throw new Exception("TestList " + name + " is not in BDD");
        }
        UrlAttachementWrapper[] tmpArray = pISQLPersonne
            .getAllAttachUrls(idBdd);
        Vector tmpVector = new Vector();
        for (int tmpI = 0; tmpI < tmpArray.length; tmpI++) {
            tmpVector.add(tmpArray[tmpI]);
        }
        return tmpVector;
    }

    @Override
    public int compareTo(User o) {
        if (o.lastName == null && this.lastName == null) {
           return 0;
         }
         if (this.lastName == null) {
           return 1;
         }
         if (o.lastName == null) {
           return -1;
         }
         return this.lastName.compareTo(o.lastName);        
    }

    public static class Comparators {

        public static Comparator<User> FIRSTNAME = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.firstName.compareTo(o2.firstName);
            }
        };

         public static Comparator<User> LASTNAME = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.lastName.compareTo(o2.lastName);
            }
        };

         public static Comparator<User> LOGIN = new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                return o1.getLoginFromModel().compareTo(o2.getLoginFromModel());
            }
        };
    }

}
