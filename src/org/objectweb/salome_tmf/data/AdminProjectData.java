/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.ApiConstants;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.api.data.GroupWrapper;
import org.objectweb.salome_tmf.api.data.ProjectWrapper;
import org.objectweb.salome_tmf.api.data.UserWrapper;
import org.objectweb.salome_tmf.api.sql.ISQLGroup;
import org.objectweb.salome_tmf.api.sql.ISQLPersonne;
import org.objectweb.salome_tmf.api.sql.ISQLProject;

/**
 * Classe qui regroupe les donnees sur les utilisateurs et sur les groupes pour
 * l'administration des projets.
 * @author teaml039
 * @version : 0.1
 */
public class AdminProjectData {
    //implements DataConstants
        
    static ISQLProject pISQLProject = null;
    static ISQLPersonne pISQLPersonne = null;
    static ISQLGroup pISQLGroup = null;
    /**
     * Utilisateur courant
     */
    private User currentUser;
        
    /**
     * Projet courant
     */
    private Project currentProject;
        
    /**
     * Le groupe courant
     */
    private Group currentGroup;
        
    /**
     * Ensemble des utilisateurs du projet. Regroupe des elements de classe 
     * <code>User</code>.
     */
    private HashSet userSet = new HashSet();    
        
    /**
     * Ensemble des utilisateurs ne faisant pas partie du projet. Regroupe des 
     * elements de classe <code>User</code>.
     */
    private HashSet notInProjectUserSet = new HashSet();
        
    /**
     * Table qui associe aux utilisateurs les groupes. La cle est un objet User
     * et la value un ensemble de groupes
     */
    HashMapForCollections usersMap = new HashMapForCollections();
        
    User pAdmin;

    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/
                 
    public void setUserAdminVTData(String login){
        // TODO
    }
                
    public User getUserAdminVTData(){
        return pAdmin;
    }
                
    /**************************************************************************/
    /**                                                 METHODES PUBLIQUES                                                      ***/
    /**************************************************************************/
        
    /**
     * Ajout d'un nouvel utilisateur
     * @param user un utilisateur
     */
    public void addUserInModel(User user) {
        if (user != null) {
            usersMap.put(user, null);
            userSet.add(user);
            boolean in = notInProjectUserSet.remove(user);
            if (in = false){
                Util.debug("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn");
            }
        } 
    } // Fin de la methode addUser/1
        
    /**
     * Ajoute un utilisateur dans  la liste des utilisateurs ne faisant pas 
     * partie du projet
     * @param user un utilisateur
     */
    public void addNotInProjectUserInModel(User user) {
        notInProjectUserSet.add(user);
    } // Fin de la methode addNotInProjectUser/1
        
    /**
     * Ajoute a groupe au projet courant
     * @param group un groupe
     */
    public void addGroupInModel(Group group) {
        if (group != null) currentProject.addGroupInModel(group);
    } // Fin de la methode addGroup/1
        
    public void addGroupInDB(Group group) throws Exception {
        group.setIdBdd(pISQLGroup.insert(currentProject.getIdBdd(), group.getNameFromModel(), group.getDescriptionFromModel(), 0));
    } 
        
    public void addGroupInDBAndModel(Group group) throws Exception {
        addGroupInDB(group);
        addGroupInModel(group);
    }
    /**
     * Ajoute un utilisateur dans un groupe donne
     * @param user un utilisateur
     * @param group un groupe
     */
    public void addUserInGroupInModel(User user, Group group) {
        if (user !=null)  { 
            usersMap.put(user, group);
            userSet.add(user);
            notInProjectUserSet.remove(user);
        }
        if (group != null) group.addUserInModel(user);
    } // Fin de la methode addUserInGroup/2
        
    public void addUserInGroupInDB(User user, Group group) throws Exception{
        pISQLGroup.insertUser(group.getIdBdd(), user.getIdBdd());
    } // Fin de la methode addUserInGroup/2
        
        
    public void addUserInGroupInDBAndModel(User user, Group group) throws Exception{
        if (user !=null && group!=null){
            if (user.isInBase() && group.isInBase()){
                if (!group.containUserInModel(user)){
                    addUserInGroupInDB(user ,group);
                    addUserInGroupInModel(user ,group);
                }
            } else {
                throw new Exception ("group " + group.getNameFromModel() + " and user " + user.getLoginFromModel() + ", are not in DB");
            }
                                
        }
    } // Fin de la methode addUserInGroup/2
        
    /**
     * Supprime un utilisateur du projet courant
     * @param user un utilisateur
     */
    public void removeUserFromCurrentProjectInModel(User user) {
        if (user != null) {
            HashSet groupSet = (HashSet) usersMap.get(user);
            for (Iterator iter = groupSet.iterator(); iter.hasNext();) {
                Group group = (Group)iter.next();
                group.removeUserInModel(user);
            }
            usersMap.remove(user);
            userSet.remove(user);
            notInProjectUserSet.add(user);
        } 
    } // Fin de la methode removeUserFromCurrentProject/1
        
    public void removeUserFromCurrentProjectInDB(User user)  throws Exception{
        user.deleteInProjectInDB(currentProject.getNameFromModel());
    }
        
    public void removeUserFromCurrentProjectInDBAndModel(User user)  throws Exception{
        if (user != null) {
            removeUserFromCurrentProjectInDB(user);
            removeUserFromCurrentProjectInModel(user);
        } 
    } // Fin de la methode removeUserFromCurrentProject/1
        
    /**
     * Supprime un utilisateur d'un groupe donne
     * @param user un utilisateur
     * @param group un groupe
     */
    public void removeUserFromGroupInModel(User user, Group group) {
        HashSet groupSet = (HashSet) usersMap.get(user);
        groupSet.remove(group);
        group.removeUserInModel(user);          
    } // Fin de la methode removeUserFromGroup/2
        
    public void removeUserFromGroupInDB(User user, Group group) throws Exception  {
        pISQLGroup.deleteUserInGroup(group.getIdBdd(), user.getIdBdd());
    }
        
    public void removeUserFromGroupInDBAndModel(User user, Group group) throws Exception {
        if (user !=null && group!=null){
            if (user.isInBase() && group.isInBase()){
                removeUserFromGroupInDB(user, group);
                if (group.containUserInModel(user)){
                    removeUserFromGroupInModel(user, group);
                }
            } else {
                throw new Exception ("group " + group.getNameFromModel() + " and user " + user.getLoginFromModel() + ", are not in DB");
            }
                                
        }
                
                
    } //
    /**
     * Supprime un groupe du projet courant
     * @param group un groupe
     */
    public void removeGroupInModel(Group group) {
        ArrayList userSet = group.getUserListFromModel();
        for (Iterator iter = userSet.iterator(); iter.hasNext(); ) {
            HashSet groupSet = (HashSet)usersMap.get(iter.next());
            groupSet.remove(group);
        }
        currentProject.removeGroup(group);
    } // Fin de la methode removeGroup/1
        
    public void removeGroupInDB(Group group) throws Exception{
        group.delete();
    } 
        
    public void removeGroupInDBAndModel(Group group) throws Exception{
        removeGroupInDB(group);
        removeGroupInModel(group);
    } 
        
    /**
     * Retourne le nombre de groupe dans le projet
     * @return le nombre de groupe dans le projet
     */
    public int getGroupCountFromModel() {
        return currentProject.getGroupListFromModel().size();
    } // Fin de la methode getGroupCount/0
        
    /**
     * Retourne le nombre d'utilisateur dans le projet
     * @return le nombre d'utilisateur dans le projet
     */
    public int getUserCountFromModel() {
        return usersMap.size();
    } // Fin de la methode getUserCount/0
        
        
    /**
     * Retourne le ieme groupe
     * @param i indice du groupe
     * @return le groupe a l'indice i, <code>null</code> si l'indice passe en
     * parametre est superieur au nombre de groupe
     */
    public Group getGroupFromModel(int i) {
        if (i < currentProject.getGroupListFromModel().size()) {
            return (Group)currentProject.getGroupListFromModel().get(i);
        } else {
            return null;
        }
    } // Fin de la methode getGroup/1

    /**
     * Retourne le groupe dont le nom est passe en parametre s'il existe, 
     * <code>null</code> sinon
     * @param name un nom de groupe
     * @return le groupe dont le nom est passe en parametre s'il existe, 
     * <code>null</code> sinon
     */
    public Group getGroupFromModel(String name) {
        for (int i = 0; i < currentProject.getGroupListFromModel().size(); i++) {
            if (((Group)currentProject.getGroupListFromModel().get(i)).getNameFromModel().equals(name)) {
                return (Group)currentProject.getGroupListFromModel().get(i);
            }
        }
        return null;
    } // Fin de la methode getGroup/1
        
    /**
     * Retourne le groupe courant
     * @return le groupe courant
     */
    public Group getCurrentGroupFromModel() {
        return currentGroup;
    } // Fin de la methode getCurrentGroup/0
        
        
    /**
     * Retourne le projet courant
     * @return le projet courant
     */
    public Project getCurrentProjectFromModel() {
        return currentProject;
    } // Fin de la methode getCurrentProject/0

    /**
     * Retourne l'utilisateur courant
     * @return l'utilisateur courant
     */
    public User getCurrentUserFromModel() {
        return currentUser;
    } // Fin de la methode getCurrentUser/0
        
    /**
     * Mutateur du groupe courant
     * @param group un groupe
     */
    public void setCurrentGroupInModel(Group group) {
        //Util.debug("Set current group --------------> " + group);
        currentGroup = group;
    } // Fin de la methode setCurrentGroup/1

    /**
     * Mutateur du projet courant
     * @param project un projet
     */
    public void setCurrentProjectInModel(Project project) {
        currentProject = project;
    } // Fin de la methode setCurrentProject/1

    /**
     * Mutateur de l'utilisateur courant
     * @param user un utilisateur
     */
    public void setCurrentUserInModel(User user) {
        currentUser = user;
    } // Fin de la methode setCurrentUser/1
        
    /**
     * Retourne l'ensemble des utilisateurs du projet courant
     * @return l'ensemble des utilisateurs du projet courant
     */
    public Set getUsersFromModel() {
        return usersMap.keySet();
    } // Fin de la methode getUsers/0
        
        
        
    /**
     * Retourne l'ensemble des utilisateurs d'un groupe donne
     * @param group un groupe
     * @return l'ensemble des utilisateurs
     */
    public ArrayList getGroupUsersFromModel(Group group) {
        return group.getUserListFromModel();
    } // Fin de la methode getGroupUsers/1
        
    /**
     * Retourne l'ensemble des groupes d'un utilisateur donne
     * @param user un utilisateur
     * @return l'ensemble des groupes
     */
    public HashSet getUserGroupsFromModel(User user) {
        return (HashSet)usersMap.get(user);
    } // Fin de la methode getUserGroups/1

    /**
     * Retourne l'utilisateur dont le login est passe en parametre. Retourne 
     * <code>null</code> si le login n'est pas celui d'un utilisateur.
     * @param login le login de l'utilisateur
     * @return l'objet User correspondant, ou <code>null</code> si le nom n'est
     * pas celui d'un utilisateur
     */
    public User getUserFromModel(String login) {
        for (Iterator iter = usersMap.keySet().iterator(); iter.hasNext();) {
            User user = (User)iter.next();
            if (user.getLoginFromModel().equals(login)) return user;
        }
        return null;
    } // Fin de la methode getUser/1
        
    /**
     * Retourne l'ensemble des utilisateurs non presents dans le projet
     * @return un ensemble d'utilisateurs
     */
    public HashSet getNotInProjectUserSetFromModel() {
        return notInProjectUserSet;
    } // Fin de la methode getNotInProjectUserSet/0
        
        
        
    /**
     * Retourne l'ensemble des utilisateurs d'un projet
     * @return l'ensemble des utilisateurs d'un projet
     */
    public HashSet getUsersOfCurrentProjectFromModel() {
        return userSet;
    } // Fin de la methode getUsersOfCurrentProject/0
        
    /**
     * Retourne vrai si le nom passe en parametre est le nom d'un groupe de 
     * l'utilisateur passe en parametre.
     * @param user un utilisateur
     * @param name un nom de groupe
     * @return vrai si le nom passe en parametre est le nom d'un groupe de 
     * l'utilisateur passe en parametre, faux sinon.
     */
    public boolean containsGroupInModel(User user, String name) {
        HashSet groupSet = getUserGroupsFromModel(user);
        if (groupSet != null) {
            for(Iterator iter = groupSet.iterator(); iter.hasNext(); ) {
                if (((Group)iter.next()).getNameFromModel().equals(name)) return true;
            }
        }
        return false;
    } // Fin de la methode containsGroup/2
        
    /**
     * Retourne le groupe correspondant au nom passe en parametre si ce groupe
     * existe et qu'il correspond a un groupe de l'utilisateur, <code>null</code>
     * sinon. 
     * @param user un utilisateur
     * @param name un nom de groupe
     * @return le groupe correspondant au nom passe en parametre si ce groupe
     * existe et qu'il correspond a un groupe de l'utilisateur, <code>null</code>
     * sinon.
     */
    public Group getGroupFromModel(User user, String name) {
        HashSet groupSet = getUserGroupsFromModel(user);
        if (groupSet != null) {
            for(Iterator iter = groupSet.iterator(); iter.hasNext(); ) {
                Group group = (Group)iter.next();
                if (group.getNameFromModel().equals(name)) return group;
            }
        }
        return null;
    } // Fin de la methode getGroup/2
        
        
    /**
     * Methode pour afficher le contenu du modele de donnees
     * @param title le titre 
     */
    public void viewDataOfProject(String title) {
        Util.log("****** " + title + " ******");
        Util.log("Projet courant : " + currentProject);
        Util.log("Utilisateur courant : " + currentUser);
        Util.log("Groupe courant : " + currentGroup);
        Util.log("Liste utilisateurs : " + userSet);
        Util.log("Map utilisateurs : " + usersMap);
        Util.log("-------------------------------");
    } // Fin de la methode viewDataOfProject/1
        
    /**
     * Nettoyage du modele de donnees
     */
    public void clear() {
        currentProject = null;
        currentGroup = null;
        currentUser = null;
        userSet.clear();
        usersMap.clear();
        notInProjectUserSet.clear();
    } // Fin de la methode clear/0
        
    /**
     * Initialisation du modele de donnees
     *
     */
    public void loadData(String adminLogin, String projectName) throws Exception {
        Util.log("[AdminProjectData->loadData]");
        clear();
        Util.log("[AdminProjectData->loadData] clear");
        if (pISQLProject == null){
            pISQLProject = Api.getISQLObjectFactory().getISQLProject();
        }
        if (pISQLPersonne == null){
            pISQLPersonne = Api.getISQLObjectFactory().getISQLPersonne();
        }
        if (pISQLGroup == null){
            pISQLGroup = Api.getISQLObjectFactory().getISQLGroup();
        }
        Util.log("[AdminProjectData->loadData] getISQLObjectFactory");
        setUserAdminVTData(adminLogin);
                
        ProjectWrapper pProjectWrapper = pISQLProject.getProject(projectName);
        
        // init du projet courant
        Project project = new Project(pProjectWrapper);
        setCurrentProjectInModel(project);
        UserWrapper pUserWrapper = pISQLPersonne.getUserByLogin(adminLogin);
        User admin = new User(pUserWrapper);
        project.setAdministratorInModel(admin);
        
        // Initialisation des utilisateurs
        
        UserWrapper[] usersAll = pISQLProject.getAllUser();
        for (int i = 0 ; i< usersAll.length; i++){
            pUserWrapper = usersAll[i];
            if (!pUserWrapper.getLogin().equals(ApiConstants.ADMIN_SALOME_NAME)){
                User user = new User(pUserWrapper);
                addNotInProjectUserInModel(user);
            } 
        }
        
        UserWrapper[] usersProject = pISQLProject.getUsersOfProject(projectName);
        for (int i = 0 ; i< usersProject.length; i++){
            pUserWrapper = usersProject[i];
            User user = new User(pUserWrapper);
            addUserInModel(user); //then remove user from addNotInProjectUserInModel
        }
        
        
        //Init des groupes du projet
        GroupWrapper[] groupsProject = pISQLProject.getProjectGroups(project.getIdBdd());
        for (int i = 0 ; i< groupsProject.length; i++){
            GroupWrapper pGroupWrapper = groupsProject[i];
            Group group = new Group(pGroupWrapper);
            UserWrapper[] usersOfGroup = pISQLProject.getUserOfGroupInProject(projectName, group.getNameFromModel());
            addGroupInModel(group);
            for (int j=0 ; j < usersOfGroup.length; j++){
                pUserWrapper = usersOfGroup[j];
                User pUser = getUserFromModel(pUserWrapper.getLogin());
                addUserInGroupInModel(pUser, group);
            }
        }
        Util.log("[AdminProjectData->loadData] done");
                
    } // Fin de la methode initData/0
        
} // Fin de la classe AdminProjectData
