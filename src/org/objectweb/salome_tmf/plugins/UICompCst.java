package org.objectweb.salome_tmf.plugins;

import java.util.Vector;

public class UICompCst {
    /**
     * Corresponds to second buttons panel (variable "testPanel") for test lists
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TEST_SECOND_BUTTONS_PANEL = new Integer(1);

    /**
     * Corresponds to tools menu for tests (variable "testToolsMenu")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TEST_TOOLS_MENU = new Integer(2);

    /**
     * Corresponds to tools menu for campaigns (variable "campToolsMenu")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer CAMP_TOOLS_MENU = new Integer(3);

    /**
     * Corresponds to tools menu for data management (variable "dataToolsMenu")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer DATA_TOOLS_MENU = new Integer(4);

    /**
     * Corresponds to test lists tree (variable "testDynamicTree")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TEST_DYNAMIC_TREE = new Integer(5);

    /**
     * Corresponds to attachments panel (dynamique) (variable
     * "attachmentsPanel") for actions, executions, execution results and test
     * execution results
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer ATTACHMENTS_PANEL = new Integer(6);

    /**
     * Corresponds to attachments buttons panel (dynamique) (variable
     * "allButtons") for actions, executions, execution results and test
     * execution results
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer ATTACHMENTS_BUTTONS_PANEL = new Integer(7);

    /**
     * Corresponds to attachments tabel (dynamique) (variable "attachmentTable")
     * for actions, executions, execution results and test execution results
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer ATTACHMENTS_TABLE = new Integer(8);

    /**
     * Corresponds to script buttons panel (variable "allButtons") for automated
     * tests
     *
     * @see org.objectweb.salome_tmf.ihm.AutomaticTestScriptView
     */
    static final public Integer AUTOMATED_TEST_SCRIPT_BUTTONS_PANEL = new Integer(
                                                                                  9);

    /**
     * Corresponds to script file name panel (variable "fileNamePanel") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.AutomaticTestScriptView
     */
    static final public Integer AUTOMATED_TEST_SCRIPT_FILENAME_PANEL = new Integer(
                                                                                   10);

    /**
     * Corresponds to script class name panel (variable "classPanel") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.AutomaticTestScriptView
     */
    static final public Integer AUTOMATED_TEST_SCRIPT_CLASSNAME_PANEL = new Integer(
                                                                                    11);

    /**
     * Corresponds to script classpath panel (variable "extensionPanel") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.AutomaticTestScriptView
     */
    static final public Integer AUTOMATED_TEST_SCRIPT_EXTENSION_PANEL = new Integer(
                                                                                    12);

    /**
     * Corresponds to script panel (variable "scriptPanel") for automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer AUTOMATED_TEST_SCRIPT_PANEL = new Integer(13);

    /**
     * Corresponds to buttons panel in parameters view for Dynamic use (variable
     * "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer PARAMS_BUTTONS_PANEL = new Integer(14);

    /**
     * Corresponds to paramaters table in parameters view for for Dynamic use
     * (variable "parametersTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer PARAMS_TABLE = new Integer(15);

    /**
     * Corresponds to paramaters panel in parameters view for for Dynamic use
     * (variable "paramsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer PARAMS_PANEL = new Integer(16);

    /**
     * Corresponds to buttons panel (variable "buttonsPanel") for manual test
     * actions
     *
     * @see org.objectweb.salome_tmf.ihm.ManualActionView
     */
    static final public Integer MANUAL_TEST_ACTIONS_BUTTONS_PANEL = new Integer(
                                                                                17);

    /**
     * Corresponds to actions table (variable "actionTable") for manual test
     *
     * @see org.objectweb.salome_tmf.ihm.ManualActionView
     */
    static final public Integer MANUAL_TEST_ACTIONS_TABLE = new Integer(18);

    /**
     * Corresponds to actions panel (variable "actionsPanel") for manual test
     *
     * @see org.objectweb.salome_tmf.ihm.ManualActionView
     */
    static final public Integer MANUAL_TEST_ACTIONS_PANEL = new Integer(19);

    /**
     * Corresponds to second buttons panel (variable "listPanel") for campaigns
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer CAMP_SECOND_BUTTONS_PANEL = new Integer(20);

    /**
     * Corresponds to campaigns tree (variable "campaignDynamicTree")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer CAMPAIGN_DYNAMIC_TREE = new Integer(21);

    /**
     * Corresponds to buttons panel for campaign executions view (variable
     * "executionButtonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ExecutionView
     */
    static final public Integer CAMP_EXECUTIONS_BUTTONS_PANEL = new Integer(22);

    /**
     * Corresponds to executions table in campaign executions view (variable
     * "executionTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ExecutionView
     */
    static final public Integer CAMP_EXECUTIONS_TABLE = new Integer(23);

    /**
     * Corresponds to buttons panel for the execution results (variable
     * "executionResultButtonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ExecutionView
     */
    static final public Integer CAMP_EXECUTION_RESULTS_BUTTONS_PANEL = new Integer(
                                                                                   24);

    /**
     * Corresponds to the execution results table for campaigns (variable
     * "detailsExecutionTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ExecutionView
     */
    static final public Integer CAMP_EXECUTION_RESULTS_TABLE = new Integer(25);

    /**
     * Corresponds to buttons panel for the campaigns data set view (variable
     * "allButtons")
     *
     * @see org.objectweb.salome_tmf.ihm.DataSetView
     */
    static final public Integer CAMP_DATASET_BUTTONS_PANEL = new Integer(26);

    /**
     * Corresponds to data sets table for the campaigns data set view (variable
     * "dataSetTable")
     *
     * @see org.objectweb.salome_tmf.ihm.DataSetView
     */
    static final public Integer CAMP_DATASETS_TABLE = new Integer(27);

    /**
     * Corresponds to buttons panel for data management (variable
     * "upButtonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer DATA_MANAGEMENT_BUTTONS_PANEL = new Integer(28);

    /**
     * Corresponds to buttons panel for environments view in data management
     * (variable "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.EnvironmentView
     */
    static final public Integer DATA_MANAGEMENT_ENV_BUTTONS_PANEL = new Integer(
                                                                                29);

    /**
     * Corresponds to environments table for environement view in data
     * management (variable "environmentTable")
     *
     * @see org.objectweb.salome_tmf.ihm.EnvironmentView
     */
    static final public Integer DATA_MANAGEMENT_ENV_TABLE = new Integer(30);

    /**
     * Corresponds to buttons panel for the manual execution window (variable
     * "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_BUTTONS_PANEL = new Integer(31);

    /**
     * Corresponds to actions table for the manual execution window (variable
     * "actionsTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_ACTIONS_TABLE = new Integer(32);

    /**
     * Corresponds to the "JTextPane" that contains test description in the
     * manual execution window (variable "testDescription")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_TEST_DESCRIPTION_TEXTPANE = new Integer(
                                                                                         33);

    /**
     * Corresponds to the "JTextPane" that contains action description in the
     * manual execution window (variable "actionDescription")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_ACTION_DESCRIPTION_TEXTPANE = new Integer(
                                                                                           34);

    /**
     * Corresponds to the "JTextPane" that contains the waited result for action
     * in the manual execution window (variable "awaitedResult")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_ACTION_WAITED_RESULT_TEXTPANE = new Integer(
                                                                                             35);

    /**
     * Corresponds to the "JTextPane" that contains the effective result for
     * action in the manual execution window (variable "effectiveResult")
     *
     * @see org.objectweb.salome_tmf.ihm.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_ACTION_EFFECTIVE_RESULT_TEXTPANE = new Integer(
                                                                                                36);

    /**
     * Corresponds to tests results table in the execution result window
     * (variable "testResultTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ExecutionResultView
     */
    static final public Integer EXECUTION_RESULT_DETAILS_TESTS_TABLE = new Integer(
                                                                                   37);

    /**
     * Corresponds to actions execution results table in the test execution
     * result details window (variable "actionsResultTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ActionDetailsView
     */
    static final public Integer TEST_EXECUTION_RESULT_DETAILS_ACTIONS_TABLE = new Integer(
                                                                                          38);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * families (variable "familyWorkSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer FAMILY_WORKSPACE_PANEL_FOR_TABS = new Integer(
                                                                              39);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * test lists (variable "listWorkSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TESTLIST_WORKSPACE_PANEL_FOR_TABS = new Integer(
                                                                                40);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * automated tests (variable "automaticTest")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS = new Integer(
                                                                                      41);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * manual tests (variable "manualTest")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS = new Integer(
                                                                                   42);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * campaigns (variable "campagneSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer CAMPAIGN_WORKSPACE_PANEL_FOR_TABS = new Integer(
                                                                                43);

    /**
     * Corresponds to the description of selected action in the test execution
     * result details window (variable "descriptionArea")
     *
     * @see org.objectweb.salome_tmf.ihm.ActionDetailsView
     */
    static final public Integer TEST_EXECUTION_RESULT_DETAILS_ACTION_DESC = new Integer(
                                                                                        44);

    /**
     * Corresponds to the waited result of selected action in the test execution
     * result details window (variable "awaitedResultArea")
     *
     * @see org.objectweb.salome_tmf.ihm.ActionDetailsView
     */
    static final public Integer TEST_EXECUTION_RESULT_DETAILS_ACTION_WAITED_RES = new Integer(
                                                                                              45);

    /**
     * Corresponds to the effective result of selected action in the test
     * execution result details window (variable "effectiveResultArea")
     *
     * @see org.objectweb.salome_tmf.ihm.ActionDetailsView
     */
    static final public Integer TEST_EXECUTION_RESULT_DETAILS_ACTION_EFF_RES = new Integer(
                                                                                           46);

    /**
     * Corresponds to the JTabbedPane pJTabbedPane
     *
     * @see org.objectweb.salome_tmf.ihm.ActionDetailsView
     */
    static final public Integer MANUAL_TEST_EXECUTION_RESULT_DETAILS_TAB = new Integer(
                                                                                       47);

    /**
     * Corresponds to un instance of "AskNewEnvironment" class
     *
     * @see org.objectweb.salome_tmf.ihm.AskNewEnvironment
     */
    static final public Integer DATA_MANAGEMENT_NEW_ENV_WINDOW = new Integer(48);

    /**
     * Corresponds to refresh JMenuItem for data management (variable
     * "refreshItem")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer DATA_MANAGEMENT_REFRESH_ITEM = new Integer(49);

    /**
     * Corresponds to the JTabbedPane for the main panels (variable "tabs")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer MAIN_TABBED_PANE = new Integer(50);

    /**
     * Corresponds to first buttons panel (variable "createPanel") for campaigns
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer CAMP_FIRST_BUTTONS_PANEL = new Integer(51);

    /**
     * Corresponds to the buttons panel of project view in Salome administration
     * applet (variable "buttonSet")
     *
     * @see org.objectweb.salome_tmf.ihm.admin.Administration
     */
    static final public Integer ADMIN_PROJECT_MANAGEMENT_BUTTONS_PANEL = new Integer(
                                                                                     52);

    /**
     * Corresponds to the table model of project view in Salome administration
     * applet (variable "projectTable")
     *
     * @see org.objectweb.salome_tmf.ihm.admin.Administration
     */
    static final public Integer ADMIN_PROJECT_MANAGEMENT_TABLE = new Integer(53);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer AUTOMATED_TEST_ATTACHMENTS_PANEL = new Integer(
                                                                               54);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer AUTOMATED_TEST_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                                       55);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for
     * automated tests
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer AUTOMATED_TEST_ATTACHMENTS_TABLE = new Integer(
                                                                               56);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for Manual
     * tests
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer MANUAL_TEST_ATTACHMENTS_PANEL = new Integer(57);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * Manual tests
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer MANUAL_TEST_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                                    58);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for Manual
     * tests
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer MANUAL_TEST_ATTACHMENTS_TABLE = new Integer(59);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer TESTLIST_ATTACHMENTS_PANEL = new Integer(60);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer TESTLIST_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                                 61);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer TESTLIST_ATTACHMENTS_TABLE = new Integer(62);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer CAMP_ATTACHMENTS_PANEL = new Integer(63);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer CAMP_ATTACHMENTS_BUTTONS_PANEL = new Integer(64);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for
     * TestList
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer CAMP_ATTACHMENTS_TABLE = new Integer(65);
    // reste en dynamique actions, executions, execution results and execution
    // results

    /**
     * Corresponds to buttons panel in parameters view for Manual tests
     * (variable "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer MANUAL_TEST_PARAMS_BUTTONS_PANEL = new Integer(
                                                                               66);

    /**
     * Corresponds to paramaters table in parameters view for Manual tests
     * (variable "parametersTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer MANUAL_TEST_PARAMS_TABLE = new Integer(67);

    /**
     * Corresponds to paramaters panel in parameters view for Manuel tests
     * (variable "paramsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer MANUAL_TEST_PARAMS_PANEL = new Integer(68);

    /**
     * Corresponds to buttons panel in parameters view for Automated tests
     * (variable "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer AUTOMATED_TEST_PARAMS_BUTTONS_PANEL = new Integer(
                                                                                  69);

    /**
     * Corresponds to paramaters table in parameters view for Automated tests
     * (variable "parametersTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer AUTOMATED_TEST_PARAMS_TABLE = new Integer(70);

    /**
     * Corresponds to paramaters panel in parameters view for Automated tests
     * (variable "paramsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer AUTOMATED_TEST_PARAMS_PANEL = new Integer(71);

    /**
     * Corresponds to buttons panel in parameters view for data management
     * (variable "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer DATA_PARAMS_BUTTONS_PANEL = new Integer(72);

    /**
     * Corresponds to paramaters table in parameters view data management
     * (variable "parametersTable")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer DATA_PARAMS_TABLE = new Integer(73);

    /**
     * Corresponds to paramaters panel in parameters view for data management
     * (variable "paramsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.ParameterView
     */
    static final public Integer DATA_PARAMS_PANEL = new Integer(74);

    /**
     * Corresponds to buttons panel in PluginsView (variable "buttonsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.PluginsView
     */
    static final public Integer PLUGINS_BUTTONS_PANEL = new Integer(75);

    /**
     * Corresponds to plugins table in PluginsView (variable "parametersTable")
     *
     * @see org.objectweb.salome_tmf.ihm.PluginsView
     */
    static final public Integer PLUGINS_TABLE = new Integer(76);

    /**
     * Corresponds to the main panel in PluginsView (variable "paramsPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.PluginsView
     */
    static final public Integer PLUGINS_PANEL = new Integer(77);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * families (variable "familyWorkSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer FAMILY_CAMP_WORKSPACE_TABS_PANEL = new Integer(
                                                                               78);

    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * test lists (variable "listWorkSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TESTLIST_CAMP_WORKSPACE_TABS_PANEL = new Integer(
                                                                                 79);
    /**
     * Corresponds to main panel (right-hand side view) that contains tabs for
     * test lists (variable "listWorkSpace")
     *
     * @see org.objectweb.salome_tmf.ihm.SalomeTMF
     */
    static final public Integer TEST_CAMP_WORKSPACE_TABS_PANEL = new Integer(80);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for Family
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer FAMILY_ATTACHMENTS_PANEL = new Integer(81);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * Family
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer FAMILY_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                               82);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for Family
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer FAMILY_ATTACHMENTS_TABLE = new Integer(83);

    /**
     * Corresponds to attachments panel (variable "attachmentsPanel") for Family
     *
     * @see org.objectweb.salome_tmf.ihm.attachmentsView
     */
    static final public Integer PROJECT_ATTACHMENTS_PANEL = new Integer(84);

    /**
     * Corresponds to attachments buttons panel (variable "allButtons") for
     * Family
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer PROJECT_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                                85);

    /**
     * Corresponds to attachments tabel (variable "attachmentTable") for Family
     *
     * @see org.objectweb.salome_tmf.ihm.AttachmentView
     */
    static final public Integer PROJECT_ATTACHMENTS_TABLE = new Integer(86);

    /**
     * JTabbedPane
     *
     * @see org.objectweb.salome_tmf.ihm.main.ManualExecution
     */
    static final public Integer MANUAL_EXECUTION_TAB = new Integer(90);

    /**
     * JTabbedPane
     *
     * @see org.objectweb.salome_tmf.ihm.main.AskNewAction
     */
    static final public Integer ACTION_NEW_TAB = new Integer(87);

    public static final Integer ACTION_ATTACHMENTS_PANEL = new Integer(91);

    public static final Integer ACTION_ATTACHMENTS_TABLE = new Integer(92);

    public static final Integer ACTION_ATTACHMENTS_BUTTONS_PANEL = new Integer(
                                                                               93);

    /**
     *
     * Corresponds to the table model of administration Project applet in Salome
     * (variable "JPanel")
     *
     * @see org.objectweb.salome_tmf.ihm.admin.AdministrationProject
     */
    static final public Integer ADMINPROJECT_BUTTONS_PANEL = new Integer(94);

    static final public Integer ADMINPROJECT_BUTTONGROUP = new Integer(95);
    static final public Integer ADMINPROJECT_PANEL = new Integer(96);
    /**
     * List of static UI components Contains : CAMPAIGN_DYNAMIC_TREE,
     * DATA_MANAGEMENT_BUTTONS_PANEL, TEST_DYNAMIC_TREE,
     * TEST_SECOND_BUTTONS_PANEL, FAMILY_WORKSPACE_PANEL_FOR_TABS,
     * TESTLIST_WORKSPACE_PANEL_FOR_TABS,
     * AUTOMATED_TEST_WORKSPACE_PANEL_FOR_TABS, AUTOMATED_TEST_SCRIPT_PANEL,
     * AUTOMATED_TEST_SCRIPT_BUTTONS_PANEL,
     * AUTOMATED_TEST_SCRIPT_FILENAME_PANEL,
     * AUTOMATED_TEST_SCRIPT_CLASSNAME_PANEL,
     * AUTOMATED_TEST_SCRIPT_EXTENSION_PANEL,
     * MANUAL_TEST_WORKSPACE_PANEL_FOR_TABS, MANUAL_TEST_ACTIONS_BUTTONS_PANEL,
     * MANUAL_TEST_ACTIONS_TABLE, MANUAL_TEST_ACTIONS_PANEL,
     * CAMP_SECOND_BUTTONS_PANEL, CAMPAIGN_WORKSPACE_PANEL_FOR_TABS,
     * DATA_MANAGEMENT_ENV_TABLE, DATA_MANAGEMENT_ENV_BUTTONS_PANEL,
     * DATA_MANAGEMENT_REFRESH_ITEM, CAMP_EXECUTIONS_BUTTONS_PANEL,
     * CAMP_EXECUTIONS_TABLE, CAMP_EXECUTION_RESULTS_BUTTONS_PANEL,
     * CAMP_EXECUTION_RESULTS_TABLE, CAMP_DATASET_BUTTONS_PANEL,
     * CAMP_DATASETS_TABLE, MAIN_TABBED_PANE, CAMP_FIRST_BUTTONS_PANEL
     * CAMP_ATTACHMENTS_TABLE, CAMP_ATTACHMENTS_BUTTONS_PANEL,
     * CAMP_ATTACHMENTS_PANEL TESTLIST_ATTACHMENTS_TABLE,
     * TESTLIST_ATTACHMENTS_BUTTONS_PANEL, TESTLIST_ATTACHMENTS_PANEL
     * MANUAL_TEST_ATTACHMENTS_TABLE, MANUAL_TEST_ATTACHMENTS_BUTTONS_PANEL,
     * MANUAL_TEST_ATTACHMENTS_PANEL AUTOMATED_TEST_ATTACHMENTS_TABLE,
     * AUTOMATED_TEST_ATTACHMENTS_BUTTONS_PANEL,
     * AUTOMATED_TEST_ATTACHMENTS_PANEL AUTOMATED_TEST_PARAMS_BUTTONS_PANEL,
     * AUTOMATED_TEST_PARAMS_TABLE, AUTOMATED_TEST_PARAMS_PANEL,
     * MANUAL_TEST_PARAMS_BUTTONS_PANEL, MANUAL_TEST_PARAMS_TABLE,
     * MANUAL_TEST_PARAMS_PANEL, DATA_PARAMS_BUTTONS_PANEL, DATA_PARAMS_TABLE,
     * DATA_PARAMS_PANEL,MANUAL_EXECUTION_TAB
     */
    static public Vector staticUIComps = new Vector();

    /** Creates a new instance of UICompConstants */
    // public UICompCst() {
    // }

}
