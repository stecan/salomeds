/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.java.plugin.Extension;
import org.java.plugin.ExtensionPoint;
import org.java.plugin.IntegrityCheckReport;
import org.java.plugin.Library;
import org.java.plugin.Plugin;
import org.java.plugin.PluginDescriptor;
import org.java.plugin.PluginManager;
import org.java.plugin.IntegrityCheckReport.ReportItem;
import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Config;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.plugins.PluginsTools;
import org.objectweb.salome_tmf.plugins.core.Admin;
import org.objectweb.salome_tmf.plugins.core.AdminProject;
import org.objectweb.salome_tmf.plugins.core.BugTracker;
import org.objectweb.salome_tmf.plugins.core.Common;
import org.objectweb.salome_tmf.plugins.core.ReqManager;
import org.objectweb.salome_tmf.plugins.core.Statistiques;

//import org.objectweb.salome_tmf.data.TestData;

//import salome.ihm.UICompCst;
//import salome.ihm.SalomeTMF;

/**
 * Class for JPF management
 */

public class JPFManager implements PluginConstants {

    static java.net.URL urlBase = null;

    // Debug
    private Logger log = Logger.getLogger(this.getClass());

    private PluginManager pluginManager = null;

    private PluginClassLoader pPluginClassLoader;


    /** Creates a new instance of JPFManager */
    public JPFManager() {
        pPluginClassLoader = new PluginClassLoader(this.getClass()
                                                   .getClassLoader());
    }

    public PluginClassLoader getClassLoader() {
        return pPluginClassLoader;
    }

    /**
     * JPF initialisation
     *
     * @param _urlBase
     *            Applet's URL in the server
     */
    public void startJPF(IPlugObject salomeTMF, URL urlBase, Map UIComponentsMap) {
        // public void startJPF(SalomeTMF VT,Map UIComponentsMap) {
        // salomeContextTMF = salomeTMF;
        try {
            // URL _urlBase = VT.getDocumentBase();
            URL _urlBase = urlBase;

            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            urlBase = new java.net.URL(url_txt);

            // JPF's configuration file
            java.net.URL url_server = new java.net.URL(urlBase
                                                       + CONFIG_JPF_FILE_PATH);
            Properties props = null;

            try {
                props = Util.getPropertiesFile(url_server);
            } catch (Exception e) {
                System.out
                    .println("WARNING JAR FILE PLUGINS PROPERTIES SELECTED "
                             + e);
                props = Util.getPropertiesFile(CONFIG_JPF_FILE_PATH);
            }

            // SI DEBUG METTRE LE REPERTOIRE PHYSIQUE !!!
            String appliRoot = null;

            appliRoot = urlBase.toString() + "/";

            /*
             * if (Api.IDE_DEV) { //appliRoot = new
             * File("/org/objectweb/salome_tmf/").toURL().toString(); } else {
             * appliRoot = urlBase.toString() + "/"; }
             */

            // Logging system initialization
            // Logs will be stored in ${tmpDir}/logs where ${tmpDir} is the
            // system
            // temp directory
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                Properties sys = System.getProperties();
                // Temp directory for log system
                String tmpDir = sys.getProperty("java.io.tmpdir");
                props.setProperty("tmpDir", tmpDir);
                try {
                    File logFolder = new File(tmpDir + "logs");
                    logFolder.mkdir();
                } catch (Exception e) {
                    // Util.debug("Failed to create log folder \"tmpDir/logs\"");
                    Util.err(e);
                }
                PropertyConfigurator.configure(props);
                log = Logger.getLogger(JPFManager.class);
                log.info("logging system initialized");
            }
            Map pluginLocations = new HashMap();

            // Ajout du 09/04/2010 de Guillaume Favro pour ajouter des plugins
            // externe
            try {
                if (props.getProperty(PARAM_PLUGINS_ABSOLUTE_PATH) != null) {

                    StringTokenizer pluginsAbsoluteFolders = new StringTokenizer(
                                                                                 props.getProperty(PARAM_PLUGINS_ABSOLUTE_PATH),
                                                                                 ",", false);
                    for (; pluginsAbsoluteFolders.hasMoreTokens();) {
                        String currentPlg = pluginsAbsoluteFolders.nextToken()
                            .trim();
                        String plgManifest = "plugin.xml";
                        if (Api.isIDE_DEV())
                            plgManifest = "pluginDBG.xml";
                        try {
                            pluginLocations.put(new URL("File:///" + currentPlg
                                                        + "/" + plgManifest), new URL("File:///"
                                                                                      + currentPlg));
                        } catch (Exception e) {
                            e.printStackTrace();
                            Util.err(e);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Fin Ajout

            // Pamameters for PluginManager
            StringTokenizer PluginsFolders = new StringTokenizer(props
                                                                 .getProperty(PARAM_PLUGINS_FOLDERS), ",", false);

            for (; PluginsFolders.hasMoreTokens();) {
                String currentPlgFolder = appliRoot
                    + PluginsFolders.nextToken().trim();
                Util.log("plug-ins folder - " + currentPlgFolder);

                StringTokenizer PluginsList = new StringTokenizer(props
                                                                  .getProperty(PARAM_PLUGINS_LIST), ",", false);

                for (; PluginsList.hasMoreTokens();) {
                    String currentPlg = currentPlgFolder + "/"
                        + PluginsList.nextToken().trim();

                    String plgManifest = "plugin.xml";
                    if (Api.isIDE_DEV())
                        plgManifest = "pluginDBG.xml";
                    try {
                        pluginLocations.put(new URL(currentPlg + "/"
                                                    + plgManifest), new URL(currentPlg));

                    } catch (Exception e) {
                        Util.err(e);
                    }
                }
            }

            // New instance of PluginManager
            pluginManager = PluginManager
                .createStandardManager(pluginLocations);

            // Plugins integrity
            Util.log("[JPFManager] Check plugins integrity");
            IntegrityCheckReport integrityCheckReport = pluginManager
                .getRegistry().checkIntegrity(
                                              pluginManager.getPathResolver());
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                log.info("integrity check done: errors - "
                         + integrityCheckReport.countErrors() + ", warnings - "
                         + integrityCheckReport.countWarnings());
            }

            if (integrityCheckReport.countErrors() != 0) {
                // throw new Exception("plug-ins integrity check failed");
                Iterator it = integrityCheckReport.getItems().iterator();
                while (it.hasNext()) {
                    ReportItem ri = (ReportItem) it.next();
                    // if (ri.getCode() != 0) {
                    System.out.println("[JPFManager Error] " + ri.getMessage());
                    // }
                }
                // salomeTMF.showMessage("Probleme lors du chargement des plugins.\n"
                // + "Certains plugins ne fonctionneront pas normalement");
                salomeTMF.showMessage(Language.getInstance().getText(
                                                                     "Problem_while_loading_plugins"));
            }

            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL)
                log.trace(integrityCheckReport2str(integrityCheckReport));

            // Running the "core" plugin
            Util.log("[JPFManager] Running the core plugin");
            Plugin corePlugin = pluginManager.getPlugin("core");

            //
            // pPluginClassLoader =
            // pluginManager.getPluginClassLoader(corePlugin.getDescriptor());

            if (corePlugin == null) {
                // throw new Exception("can't get plug-in core");
                salomeTMF
                    .showMessage("Probleme lors du chargement du core plugin.\n"
                                 + "Les plugins ne pourront pas etre charges");
            }

            // Extension points initialization
            Util.log("[JPFManager] Extension points initialization");
            ExtensionPoint Common = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getCommonExtPoint", new Class[] {}).invoke(
                                                                       corePlugin, new Object[] {});

            ExtensionPoint testDriver = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getTestDriverExtPoint", new Class[] {}).invoke(
                                                                           corePlugin, new Object[] {});

            ExtensionPoint scriptEngine = (ExtensionPoint) corePlugin
                .getClass().getMethod("getScriptEngineExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            ExtensionPoint bugTracker = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getBugTrackerExtPoint", new Class[] {}).invoke(
                                                                           corePlugin, new Object[] {});

            ExtensionPoint statistiques = (ExtensionPoint) corePlugin
                .getClass().getMethod("getStatistiquesExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            ExtensionPoint reqManager = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getReqManagerExtPoint", new Class[] {}).invoke(
                                                                           corePlugin, new Object[] {});

            ExtensionPoint XMLPrinters = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getXMLPrinterExtPoint", new Class[] {}).invoke(
                                                                           corePlugin, new Object[] {});

            // Common plugins activation
            Util.log("[JPFManager] Common plugins activation");
            activateCommonsInStaticUIComps(salomeTMF, Common, UIComponentsMap);

            // Bug Tracking plugins activation
            Util.log("[JPFManager] Bug tracking plugins activation");
            activateBugTrackingPlugins(salomeTMF, bugTracker);

            // Statistiques plugins activation
            Util.log("[JPFManager] Statistiques plugins activation");
            activateStatistiquesPlugins(salomeTMF, statistiques);

            // ReqManager plugins activation
            Util.log("[JPFManager] Requirement plugins activation");
            activateRegManagerPlugins(salomeTMF, reqManager);

            // The application needs some JPF parameters
            Util.log("[JPFManager] The application needs some JPF parameters");
            salomeTMF.init_Component(pluginManager, Common, testDriver,
                                     scriptEngine, bugTracker, statistiques, reqManager);

            ExtensionPoint pExtsionTestDriver = (ExtensionPoint) corePlugin
                .getClass().getMethod("getTestDriverExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            ExtensionPoint pExtsionScriptEngine = (ExtensionPoint) corePlugin
                .getClass().getMethod("getScriptEngineExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            initExtsionTestDriver(salomeTMF, pExtsionTestDriver);
            initExtsionScriptEngine(salomeTMF, pExtsionScriptEngine);
            loadExtsionXMLPrinter(salomeTMF, XMLPrinters);

        } catch (Exception e) {
            Util.err(e);
        }
    }

    public void reActivatePlugin(IPlugObject salomeTMF, ExtensionPoint commonE,
                                 ExtensionPoint bugTracker, Map UIComponentsMap) {
        Util.log("[JPFManager] Common plugins activation");
        reActivateCommonsInStaticUIComps(salomeTMF, commonE, UIComponentsMap);
        reActivateBugTrackingPlugins(salomeTMF, bugTracker);
    }

    public void reActivatePlugin(IPlugObject salomeTMF, ExtensionPoint commonE,
                                 ExtensionPoint bugTracker, ExtensionPoint statistiques,
                                 Map UIComponentsMap) {
        Util.log("[JPFManager] Common plugins activation");
        reActivateCommonsInStaticUIComps(salomeTMF, commonE, UIComponentsMap);
        reActivateBugTrackingPlugins(salomeTMF, bugTracker);
        reActivateStatistiquesPlugins(salomeTMF, statistiques);
    }

    /*
     * public void suspendPlugin(IPlugObject salomeTMF, ExtensionPoint commonE,
     * ExtensionPoint bugTracker , Map UIComponentsMap) {
     * Util.log("[JPFManager] Common plugins suspend");
     * //suspendCommonsInStaticUIComps(salomeTMF, commonE, UIComponentsMap);
     * suspendBugTrackingPlugins(salomeTMF, bugTracker); }
     */

    /**
     * This method makes string presentation of given integrity check report.
     * sous forme de chaine de caracteres
     *
     * @param report
     *            IntegrityCheckReport
     */
    private static String integrityCheckReport2str(IntegrityCheckReport report) {
        StringBuffer buf = new StringBuffer();
        buf.append("integrity check report:\r\n");
        buf.append("-------------- REPORT BEGIN -----------------\r\n");
        for (Iterator it = report.getItems().iterator(); it.hasNext();) {
            IntegrityCheckReport.ReportItem item = (IntegrityCheckReport.ReportItem) it
                .next();
            buf.append("\tseverity=").append(item.getSeverity()).append(
                                                                        "; code=").append(item.getCode()).append("; message=")
                .append(item.getMessage()).append("; source=").append(
                                                                      item.getSource()).append("\r\n");
        }
        buf.append("-------------- REPORT END -----------------");
        return buf.toString();
    }

    /**
     * This method activates the plugins which implements the "Common" extension
     * point in static UI components
     *
     * @param VT
     * @param pluginManager
     * @param uiComps
     */
    private void activateCommonsInStaticUIComps(IPlugObject salomeTMF,
                                                ExtensionPoint Common, Map uiComps) {
        if (!Common.getConnectedExtensions().isEmpty()) {

            for (Iterator it = Common.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension commonExt = (Extension) it.next();

                try {
                    Common common = null;
                    common = (Common) activateExtension(commonExt);
                    // Plugin initialisation
                    common.init(salomeTMF);

                    // Activation of the plugin in the "Tools" menu for tests
                    if (common.isActivableInTestToolsMenu())
                        common.activatePluginInTestToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.TEST_TOOLS_MENU));

                    // Activation of the plugin in the "Tools" menu for
                    // campaigns
                    if (common.isActivableInCampToolsMenu())
                        common.activatePluginInCampToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.CAMP_TOOLS_MENU));

                    // Activation of the plugin in the "Tools" menu for data
                    // management
                    if (common.isActivableInDataToolsMenu())
                        common.activatePluginInDataToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.DATA_TOOLS_MENU));

                    // Ask if the plugin uses other UI components
                    if (common.usesOtherUIComponents()) {
                        // Getting UI components used by the plugin
                        Vector compCsts = common.getUsedUIComponents();

                        // We associate, for each dynamic UI component, the
                        // plugins that uses it
                        if (compCsts != null) {
                            for (int i = 0; i < compCsts.size(); i++) {
                                Integer uiCompCont = (Integer) compCsts
                                    .elementAt(i);
                                salomeTMF
                                    .addPlgToUICompList(uiCompCont, common);

                                // If it's a static UI component we activate the
                                // plugin in it
                                if (UICompCst.staticUIComps
                                    .contains(uiCompCont)) {
                                    common
                                        .activatePluginInStaticComponent(uiCompCont);
                                }// else we will activate plugins when dynamic
                                // UI components are instanciated
                            }
                        }
                    }

                } catch (Exception e) {
                    Util.err(e);
                }

            }
        }

    }

    private void reActivateCommonsInStaticUIComps(IPlugObject salomeTMF,
                                                  ExtensionPoint Common, Map uiComps) {
        if (!Common.getConnectedExtensions().isEmpty()) {

            for (Iterator it = Common.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension commonExt = (Extension) it.next();

                try {
                    Common common = null;
                    common = (Common) reActivateExtension(commonExt);
                    // Plugin initialisation
                    common.init(salomeTMF);

                    // Activation of the plugin in the "Tools" menu for tests
                    if (common.isActivableInTestToolsMenu())
                        common.activatePluginInTestToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.TEST_TOOLS_MENU));

                    // Activation of the plugin in the "Tools" menu for
                    // campaigns
                    if (common.isActivableInCampToolsMenu())
                        common.activatePluginInCampToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.CAMP_TOOLS_MENU));

                    // Activation of the plugin in the "Tools" menu for data
                    // management
                    if (common.isActivableInDataToolsMenu())
                        common.activatePluginInDataToolsMenu((JMenu) uiComps
                                                             .get(UICompCst.DATA_TOOLS_MENU));

                    // Ask if the plugin uses other UI components
                    if (common.usesOtherUIComponents()) {
                        // Getting UI components used by the plugin
                        Vector compCsts = common.getUsedUIComponents();

                        // We associate, for each dynamic UI component, the
                        // plugins that uses it
                        if (compCsts != null) {
                            for (int i = 0; i < compCsts.size(); i++) {
                                Integer uiCompCont = (Integer) compCsts
                                    .elementAt(i);

                                // salomeTMF.addPlgToUICompList(uiCompCont,
                                // common);

                                // If it's a static UI component we activate the
                                // plugin in it
                                if (UICompCst.staticUIComps
                                    .contains(uiCompCont)) {
                                    common
                                        .activatePluginInStaticComponent(uiCompCont);
                                }// else we will activate plugins when dynamic
                                // UI components are instanciated
                            }
                        }

                    }

                } catch (Exception e) {
                    Util.err(e);
                }

            }
        }

    }

    /*
     * private void suspendCommonsInStaticUIComps(IPlugObject salomeTMF,
     * ExtensionPoint Common, Map uiComps) { if
     * (!Common.getConnectedExtensions().isEmpty()) {
     *
     * for (Iterator it = Common.getConnectedExtensions().iterator();
     * it.hasNext();) { Extension commonExt = (Extension) it.next();
     *
     * try { Common common = null; common = (Common)
     * reActivateExtension(commonExt); // Plugin initialisation
     * common.init(salomeTMF);
     *
     * // Activation of the plugin in the "Tools" menu for tests if
     * (common.isActivableInTestToolsMenu())
     * common.activatePluginInTestToolsMenu((JMenu)
     * uiComps.get(UICompCst.TEST_TOOLS_MENU));
     *
     * // Activation of the plugin in the "Tools" menu for campaigns if
     * (common.isActivableInCampToolsMenu())
     * common.activatePluginInCampToolsMenu((JMenu)
     * uiComps.get(UICompCst.CAMP_TOOLS_MENU));
     *
     * // Activation of the plugin in the "Tools" menu for data management if
     * (common.isActivableInDataToolsMenu())
     * common.activatePluginInDataToolsMenu((JMenu)
     * uiComps.get(UICompCst.DATA_TOOLS_MENU));
     *
     * // Ask if the plugin uses other UI components if
     * (common.usesOtherUIComponents()) { // Getting UI components used by the
     * plugin Vector compCsts = common.getUsedUIComponents();
     *
     * // We associate, for each dynamic UI component, the plugins that uses it
     * if (compCsts != null) { for (int i = 0; i < compCsts.size(); i++) {
     * Integer uiCompCont = (Integer) compCsts.elementAt(i);
     *
     * //salomeTMF.addPlgToUICompList(uiCompCont, common);
     *
     * // If it's a static UI component we activate the plugin in it if
     * (UICompCst.staticUIComps.contains(uiCompCont)) {
     * common.activatePluginInStaticComponent(uiCompCont); }// else we will
     * activate plugins when dynamic UI components are instanciated } }
     *
     * }
     *
     * } catch (Exception e) { Util.err(e); }
     *
     * } }
     *
     * }
     */

    /**
     * This method activates the plugins which implements the "BugTracker"
     * extension point
     *
     * @param IPlugObject
     * @param BugTracker
     *            extension point
     */
    private void activateBugTrackingPlugins(IPlugObject salomeTMF,
                                            ExtensionPoint bugTracker) {
        Util.log("[JPFManager] Number of bug tracking plugins = "
                 + bugTracker.getConnectedExtensions().size());
        if (!bugTracker.getConnectedExtensions().isEmpty()) {

            for (Iterator it = bugTracker.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension bugTrackerExt = (Extension) it.next();

                try {
                    BugTracker bugTrack = null;
                    bugTrack = (BugTracker) activateExtension(bugTrackerExt);
                    // Plugins initialisation
                    bugTrack.initBugTracker(salomeTMF);
                    salomeTMF.addBugTrackers(bugTrack);
                    // Activation of bug tracking plugins in tools menus
                    if (bugTrack.isIndependantTracker()) {
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getTestToolsMenu(), bugTrack);
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getCampToolsMenu(), bugTrack);
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getDataToolsMenu(), bugTrack);
                    }
                } catch (Exception e) {
                    Util.err(e);
                }

            }
        }

    }

    private void reActivateBugTrackingPlugins(IPlugObject salomeTMF,
                                              ExtensionPoint bugTracker) {
        Util.log("[JPFManager] Number of bug tracking plugins = "
                 + bugTracker.getConnectedExtensions().size());
        if (!bugTracker.getConnectedExtensions().isEmpty()) {

            for (Iterator it = bugTracker.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension bugTrackerExt = (Extension) it.next();

                try {
                    BugTracker bugTrack = null;
                    bugTrack = (BugTracker) reActivateExtension(bugTrackerExt);
                    // Plugins initialisation
                    bugTrack.initBugTracker(salomeTMF);
                    // salomeTMF.addBugTrackers(bugTrack);

                    // Activation of bug tracking plugins in tools menus
                    if (bugTrack.isIndependantTracker()) {
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getTestToolsMenu(), bugTrack);
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getCampToolsMenu(), bugTrack);
                        PluginsTools.activateBugTrackingPlgsInToolsMenu(
                                                                        SalomeTMFPanels.getDataToolsMenu(), bugTrack);
                    }
                } catch (Exception e) {
                    Util.err(e);
                }

            }
        }

    }

    /**
     * This method activates the plugins which implements the "ReqManager"
     * extension point
     *
     * @param IPlugObject
     * @param ReqManager
     *            extension point
     */
    private void activateRegManagerPlugins(IPlugObject salomeTMF,
                                           ExtensionPoint reqManager) {
        Util.log("[JPFManager] Number of reqManager plugins = "
                 + reqManager.getConnectedExtensions().size());
        if (!reqManager.getConnectedExtensions().isEmpty()) {

            for (Iterator it = reqManager.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension reqManagerExt = (Extension) it.next();

                try {
                    ReqManager req = null;
                    req = (ReqManager) activateExtension(reqManagerExt);
                    // Plugins initialisation
                    salomeTMF.addReqManager(req);
                } catch (Exception e) {
                    Util.err(e);
                }

            }
        }

    }

    /**
     * This method activates the plugins which implements the "Statistiques"
     * extension point
     *
     * @param IPlugObject
     * @param Statistiques
     *            extension point
     */
    private void activateStatistiquesPlugins(IPlugObject salomeTMF,
                                             ExtensionPoint statistiques) {
        Util.log("[JPFManager] Number of statistiques plugins = "
                 + statistiques.getConnectedExtensions().size());
        if (!statistiques.getConnectedExtensions().isEmpty()) {
            for (Iterator it = statistiques.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension statistiquesExt = (Extension) it.next();
                try {
                    Statistiques stat = null;
                    stat = (Statistiques) activateExtension(statistiquesExt);
                    // Plugins initialisation
                    stat.initStatistiques(salomeTMF);
                    salomeTMF.addStatistiquess(stat);
                    // Activation of bug tracking plugins in tools menus
                    // if (stat.isIndependantStatistiques()){
                    // PluginsTools.activateStatistiquesPlgsInToolsMenu(SalomeTMFPanels.getTestToolsMenu(),stat);
                    // PluginsTools.activateStatistiquesPlgsInToolsMenu(SalomeTMFPanels.getCampToolsMenu(),stat);
                    // PluginsTools.activateStatistiquesPlgsInToolsMenu(SalomeTMFPanels.getDataToolsMenu(),stat);
                    // }
                } catch (Exception e) {
                    Util.err(e);
                }
            }
        }
    }

    private void reActivateStatistiquesPlugins(IPlugObject salomeTMF,
                                               ExtensionPoint statistiques) {
        Util.log("[JPFManager] Number of statistiques plugins = "
                 + statistiques.getConnectedExtensions().size());
        if (!statistiques.getConnectedExtensions().isEmpty()) {
            for (Iterator it = statistiques.getConnectedExtensions().iterator(); it
                     .hasNext();) {
                Extension statistiquesExt = (Extension) it.next();
                try {
                    Statistiques stat = null;
                    stat = (Statistiques) reActivateExtension(statistiquesExt);
                    // Plugins initialisation
                    stat.initStatistiques(salomeTMF);
                    // salomeTMF.addStatistiquess(stat);
                    // Activation of bug tracking plugins in tools menus
                    // if (stat.isIndependantTracker()){
                    // PluginsTools.activateBugTrackingPlgsInToolsMenu(SalomeTMFPanels.getTestToolsMenu(),stat);
                    // PluginsTools.activateBugTrackingPlgsInToolsMenu(SalomeTMFPanels.getCampToolsMenu(),stat);
                    // PluginsTools.activateBugTrackingPlgsInToolsMenu(SalomeTMFPanels.getDataToolsMenu(),stat);
                    // }
                } catch (Exception e) {
                    Util.err(e);
                }
            }
        }
    }

    /*
     * private void suspendBugTrackingPlugins(IPlugObject salomeTMF,
     * ExtensionPoint bugTracker) {
     * Util.log("[JPFManager] Number of bug tracking plugins = " +
     * bugTracker.getConnectedExtensions().size()); if
     * (!bugTracker.getConnectedExtensions().isEmpty()) {
     *
     * for (Iterator it = bugTracker.getConnectedExtensions().iterator();
     * it.hasNext();) { Extension bugTrackerExt = (Extension) it.next();
     *
     * try { BugTracker bugTrack = null;q bugTrack =
     * (BugTracker)reActivateExtension(bugTrackerExt); //Plugins initialisation
     * bugTrack.suspend();
     *
     * } catch (Exception e) { Util.err(e); }
     *
     * } }
     *
     * }
     */

    public Object activateExtension(Extension pExtension) throws Exception {
        PluginDescriptor pDesc = pExtension.getDeclaringPluginDescriptor();

        Object pluginRef = null;

        // This will invoke the method "addLocalJar()" of
        // sun.plugin.security.PluginClassLoader
        // which forces the SecurityManager to verify signatures for plugin's
        // libraries jar files
        Util.debug("Try to activate Extension : " + pExtension);

        if (!Api.isIDE_DEV()) {
            for (Iterator itl = pDesc.getLibraries().iterator(); itl.hasNext();) {
                Library lib = (Library) itl.next();
                Util.log("[activateExtension] Find lib : " + lib);
                if (lib.isCodeLibrary()) {
                    URL UrlLib = pluginManager.getPathResolver().resolvePath(
                                                                             lib, lib.getPath());
                    Util.log("[activateExtension] Try lo load lib : " + UrlLib);
                    pPluginClassLoader.AddJar(UrlLib);
                    /*
                     * try { Class pclC =
                     * (this.getClass().getClassLoader()).getClass();
                     * pclC.getMethod("addLocalJar", new Class[] { URL.class })
                     * .invoke(this.getClass().getClassLoader(), new Object[] {
                     * UrlLib }); } catch (Exception e){
                     * Util.log("[activateExtension] Error when load lib : " +
                     * e); Util.err(e); }
                     */
                }
            }
        }

        try {
            Util.log("[activateExtension] plugin to instance is   : " + pDesc);
            Plugin pPlugin = pluginManager.getPlugin(pDesc.getId());
            Util.log("[activateExtension] The plugin is  : " + pPlugin);
            Class pluginCls = pPlugin.getClass();
            Util.log("[activateExtension] The plugin class is  : " + pluginCls);
            Class extensionCls = pluginCls.getClassLoader().loadClass(
                                                                      pExtension.getParameter("class").valueAsString());
            Util.log("[activateExtension] The plugin extensionClass is  : "
                     + extensionCls);
            if (pluginCls.isAssignableFrom(extensionCls)) {
                Util.log("[activateExtension] The plugin is loaded");
                pluginRef = pPlugin;
            } else {
                Util
                    .log("[activateExtension] The plugin is not loaded, try to load");
                pluginRef = extensionCls.newInstance();
            }
        } catch (Exception e) {
            Util.err(e);

        }
        return pluginRef;
    }

    public Object reActivateExtension(Extension pExtension) throws Exception {
        PluginDescriptor pDesc = pExtension.getDeclaringPluginDescriptor();

        Object pluginRef = null;

        // This will invoke the method "addLocalJar()" of
        // sun.plugin.security.PluginClassLoader
        // which forces the SecurityManager to verify signatures for plugin's
        // libraries jar files
        Util.debug("Try to activate Extension : " + pExtension);

        try {
            Util.log("[activateExtension] plugin to instance is   : " + pDesc);
            Plugin pPlugin = pluginManager.getPlugin(pDesc.getId());
            Util.log("[activateExtension] The plugin is  : " + pPlugin);
            Class pluginCls = pPlugin.getClass();
            Util.log("[activateExtension] The plugin class is  : " + pluginCls);
            Class extensionCls = pluginCls.getClassLoader().loadClass(
                                                                      pExtension.getParameter("class").valueAsString());
            Util.log("[activateExtension] The plugin extensionClass is  : "
                     + extensionCls);
            if (pluginCls.isAssignableFrom(extensionCls)) {
                Util.log("[activateExtension] The plugin is loaded");
                pluginRef = pPlugin;
            } else {
                Util
                    .log("[activateExtension] The plugin is not loaded, try to load");
                pluginRef = extensionCls.newInstance();
            }
        } catch (Exception e) {
            Util.err(e);

        }
        return pluginRef;
    }

    private void initExtsionTestDriver(IPlugObject salomeTMF,
                                       ExtensionPoint pExtsionTestDriver) {
        if (!pExtsionTestDriver.getConnectedExtensions().isEmpty()) {
            for (Iterator it = pExtsionTestDriver.getConnectedExtensions()
                     .iterator(); it.hasNext();) {
                try {
                    Extension testDriverExt = (Extension) it.next();
                    String extList = testDriverExt.getParameter("extensions")
                        .valueAsString();
                    salomeTMF.associatedTestDriver(testDriverExt, extList);
                    salomeTMF.associatedExtension(testDriverExt.getId(),
                                                  testDriverExt);
                    Util.log("Add testDriverExt : " + testDriverExt.getId());
                } catch (Exception e) {
                    // TODO
                    Util.err(e);
                }
            }
        }
    }

    private void initExtsionScriptEngine(IPlugObject salomeTMF,
                                         ExtensionPoint pExtsionScriptEngine) {
        if (!pExtsionScriptEngine.getConnectedExtensions().isEmpty()) {
            for (Iterator it = pExtsionScriptEngine.getConnectedExtensions()
                     .iterator(); it.hasNext();) {
                try {
                    Extension scriptEngineExt = (Extension) it.next();
                    String extList = scriptEngineExt.getParameter("extensions")
                        .valueAsString();
                    salomeTMF.associatedScriptEngine(scriptEngineExt, extList);
                    salomeTMF.associatedExtension(scriptEngineExt.getId(),
                                                  scriptEngineExt);
                    Util.log("Add ExtsionScriptEngine : "
                             + scriptEngineExt.getId());
                } catch (Exception e) {
                    // TODO
                    Util.err(e);
                }
            }
        }
    }

    private void loadExtsionXMLPrinter(IPlugObject salomeTMF,
                                       ExtensionPoint pExtensionXMLPrinter) {
        if (!pExtensionXMLPrinter.getConnectedExtensions().isEmpty()) {

            for (Iterator it = pExtensionXMLPrinter.getConnectedExtensions()
                     .iterator(); it.hasNext();) {
                try {
                    Extension pXMLExt = (Extension) it.next();
                    salomeTMF.addXMLPrinterExtension(pXMLExt);
                } catch (Exception e) {
                    // TODO
                    Util.err(e);
                }
            }
        }
    }

    /**
     * JPF initialisation in Salome administration
     *
     * @param _urlBase
     *            Applet's URL in the server
     */
    public void startJPFInAdmin(URL urlBase, Map UIComponentsMap,
                                IPlugObject administration) {
        // salomeContextTMF = administration;
        System.out.println("startJPFInAdmin");
        try {
            // URL _urlBase = VT.getDocumentBase();
            URL _urlBase = urlBase;

            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            urlBase = new java.net.URL(url_txt);

            // JPF's configuration file
            java.net.URL url_server = new java.net.URL(urlBase
                                                       + CONFIG_JPF_FILE_PATH);
            Properties props = null;

            try {
                props = Util.getPropertiesFile(url_server);
            } catch (Exception e) {
                Util.debug("WARNING JAR FILE PLUGINS PROPERTIES SELECTED " + e);
                props = Util.getPropertiesFile(CONFIG_JPF_FILE_PATH);
            }

            // SI DEBUG METTRE LE REPERTOIRE PHYSIQUE !!!
            String appliRoot = null;

            appliRoot = urlBase.toString() + "/";

            // Logging system initialization
            // Logs will be stored in ${tmpDir}/logs where ${tmpDir} is the
            // system
            // temp directory
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                Properties sys = System.getProperties();
                // Temp directory for log system
                String tmpDir = sys.getProperty("java.io.tmpdir");

                props.setProperty("tmpDir", tmpDir);
                try {
                    File logFolder = new File(tmpDir + "logs");
                    logFolder.mkdir();
                } catch (Exception e) {
                    System.out
                        .println("Failed to create log folder \"tmpDir/logs\"");
                    Util.err(e);
                }

                PropertyConfigurator.configure(props);
                log = Logger.getLogger(JPFManager.class);
                log.info("logging system initialized");
            }

            // Pamameters for PluginManager
            StringTokenizer PluginsFolders = new StringTokenizer(props
                                                                 .getProperty(PARAM_PLUGINS_FOLDERS), ",", false);
            Map pluginLocations = new HashMap();

            // Ajout du 09/04/2010 de Guillaume Favro pour ajouter des plugins
            // externe
            try {
                StringTokenizer pluginsAbsoluteFolders = new StringTokenizer(
                                                                             props.getProperty(PARAM_PLUGINS_ABSOLUTE_PATH), ",",
                                                                             false);
                for (; pluginsAbsoluteFolders.hasMoreTokens();) {
                    String currentPlg = pluginsAbsoluteFolders.nextToken()
                        .trim();
                    String plgManifest = "plugin.xml";
                    if (Api.isIDE_DEV())
                        plgManifest = "pluginDBG.xml";
                    try {
                        pluginLocations.put(new URL("File:///" + currentPlg
                                                    + "/" + plgManifest), new URL("File:///"
                                                                                  + currentPlg));
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Fin Ajout

            for (; PluginsFolders.hasMoreTokens();) {
                String currentPlgFolder = appliRoot
                    + PluginsFolders.nextToken().trim();

                Util.log("plug-ins folder - " + currentPlgFolder);

                StringTokenizer PluginsList = new StringTokenizer(props
                                                                  .getProperty(PARAM_PLUGINS_LIST), ",", false);

                for (; PluginsList.hasMoreTokens();) {
                    String currentPlg = currentPlgFolder + "/"
                        + PluginsList.nextToken().trim();

                    String plgManifest = "plugin.xml";
                    if (Api.isIDE_DEV())
                        plgManifest = "pluginDBG.xml";
                    try {
                        pluginLocations.put(new URL(currentPlg + "/"
                                                    + plgManifest), new URL(currentPlg));
                    } catch (Exception e) {
                        Util.err(e);
                    }
                }
            }

            // New instance of PluginManager
            pluginManager = PluginManager
                .createStandardManager(pluginLocations);

            // Plugins integrity
            Util.log("[JPFManager] Check plugins integrity");

            IntegrityCheckReport integrityCheckReport = pluginManager
                .getRegistry().checkIntegrity(
                                              pluginManager.getPathResolver());
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                log.info("integrity check done: errors - "
                         + integrityCheckReport.countErrors() + ", warnings - "
                         + integrityCheckReport.countWarnings());
            }

            if (integrityCheckReport.countErrors() != 0) {
                administration
                    .showMessage("Probleme lors du chargement des plugins.\n"
                                 + "Certains plugins ne fonctionneront pas normalement");
            }

            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL)
                log.trace(integrityCheckReport2str(integrityCheckReport));

            // Running the "core" plugin
            Util.log("[JPFManager] Running the core plugin");
            Plugin corePlugin = pluginManager.getPlugin("core");

            if (corePlugin == null) {
                // throw new Exception("can't get plug-in core");
                administration
                    .showMessage("Probleme lors du chargement du core plugin.\n"
                                 + "Les plugins ne pourront pas etre charges");
            }

            // Extension points initialization
            Util.log("[JPFManager] Extension points initialization");
            ExtensionPoint Admin = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getAdminExtPoint", new Class[] {}).invoke(
                                                                      corePlugin, new Object[] {});

            ExtensionPoint XMLPrinters = (ExtensionPoint) corePlugin.getClass()
                .getMethod("getXMLPrinterExtPoint", new Class[] {}).invoke(
                                                                           corePlugin, new Object[] {});

            if (!Admin.getConnectedExtensions().isEmpty()) {

                for (Iterator it = Admin.getConnectedExtensions().iterator(); it
                         .hasNext();) {
                    Extension adminExt = (Extension) it.next();

                    Admin admin = null;

                    admin = (Admin) activateExtension(adminExt);

                    admin
                        .activateInSalomeAdmin(UIComponentsMap,
                                               administration);
                    administration.associatedExtension(adminExt.getId(),
                                                       adminExt);
                }
            }

            ExtensionPoint pExtsionTestDriver = (ExtensionPoint) corePlugin
                .getClass().getMethod("getTestDriverExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            ExtensionPoint pExtsionScriptEngine = (ExtensionPoint) corePlugin
                .getClass().getMethod("getScriptEngineExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            administration.initExtsionTestDriver(pExtsionTestDriver);
            administration.initExtsionScriptEngine(pExtsionScriptEngine);
            loadExtsionXMLPrinter(administration, XMLPrinters);

        } catch (Exception e) {
            Util.err(e);
        }
    }

    JPanel panelAdminProject;
    JPanel pluginAdminProject;

    public void startJPFInAdminProject(URL urlBase, Map UIComponentsMap,
                                       IPlugObject administration, String user, String projet) {
        // salomeContextTMF = administration;
        System.out.println("startJPFInAdmin");

        try {
            // URL _urlBase = VT.getDocumentBase();
            URL _urlBase = urlBase;

            String url_txt = _urlBase.toString();
            url_txt = url_txt.substring(0, url_txt.lastIndexOf("/"));
            urlBase = new java.net.URL(url_txt);

            // JPF's configuration file
            java.net.URL url_server = new java.net.URL(urlBase
                                                       + CONFIG_JPF_FILE_PATH);
            Properties props = null;

            try {
                props = Util.getPropertiesFile(url_server);
            } catch (Exception e) {
                Util.debug("WARNING JAR FILE PLUGINS PROPERTIES SELECTED " + e);
                props = Util.getPropertiesFile(CONFIG_JPF_FILE_PATH);
            }

            // SI DEBUG METTRE LE REPERTOIRE PHYSIQUE !!!
            String appliRoot = null;

            appliRoot = urlBase.toString() + "/";

            // Logging system initialization
            // Logs will be stored in ${tmpDir}/logs where ${tmpDir} is the
            // system
            // temp directory
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                Properties sys = System.getProperties();
                // Temp directory for log system
                String tmpDir = sys.getProperty("java.io.tmpdir");

                props.setProperty("tmpDir", tmpDir);
                try {
                    File logFolder = new File(tmpDir + "logs");
                    logFolder.mkdir();
                } catch (Exception e) {
                    System.out
                        .println("Failed to create log folder \"tmpDir/logs\"");
                    Util.err(e);
                }

                PropertyConfigurator.configure(props);
                log = Logger.getLogger(JPFManager.class);
                log.info("logging system initialized");
            }

            // Pamameters for PluginManager
            StringTokenizer PluginsFolders = new StringTokenizer(props
                                                                 .getProperty(PARAM_PLUGINS_FOLDERS), ",", false);
            Map pluginLocations = new HashMap();

            // Ajout du 09/04/2010 de Guillaume Favro pour ajouter des plugins
            // externe
            try {
                StringTokenizer pluginsAbsoluteFolders = new StringTokenizer(
                                                                             props.getProperty(PARAM_PLUGINS_ABSOLUTE_PATH), ",",
                                                                             false);
                for (; pluginsAbsoluteFolders.hasMoreTokens();) {
                    String currentPlg = pluginsAbsoluteFolders.nextToken()
                        .trim();
                    String plgManifest = "plugin.xml";
                    if (Api.isIDE_DEV())
                        plgManifest = "pluginDBG.xml";
                    try {
                        pluginLocations.put(new URL("File:///" + currentPlg
                                                    + "/" + plgManifest), new URL("File:///"
                                                                                  + currentPlg));
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
            } catch (Exception e) {
                log.error(e);
            }
            // Fin Ajout

            for (; PluginsFolders.hasMoreTokens();) {
                String currentPlgFolder = appliRoot
                    + PluginsFolders.nextToken().trim();

                Util.log("plug-ins folder - " + currentPlgFolder);

                StringTokenizer PluginsList = new StringTokenizer(props
                                                                  .getProperty(PARAM_PLUGINS_LIST), ",", false);

                for (; PluginsList.hasMoreTokens();) {
                    String currentPlg = currentPlgFolder + "/"
                        + PluginsList.nextToken().trim();

                    String plgManifest = "plugin.xml";
                    if (Api.isIDE_DEV())
                        plgManifest = "pluginDBG.xml";
                    try {
                        pluginLocations.put(new URL(currentPlg + "/"
                                                    + plgManifest), new URL(currentPlg));
                    } catch (Exception e) {
                        log.error(e);
                    }
                }
            }

            // New instance of PluginManager
            pluginManager = PluginManager
                .createStandardManager(pluginLocations);

            // Plugins integrity
            Util.log("[JPFManager] Check plugins integrity");

            IntegrityCheckReport integrityCheckReport = pluginManager
                .getRegistry().checkIntegrity(
                                              pluginManager.getPathResolver());
            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL) {
                log.info("integrity check done: errors - "
                         + integrityCheckReport.countErrors() + ", warnings - "
                         + integrityCheckReport.countWarnings());
            }

            if (integrityCheckReport.countErrors() != 0) {
                administration
                    .showMessage("Probleme lors du chargement des plugins.\n"
                                 + "Certains plugins ne fonctionneront pas normalement");
            }

            if (Api.getLogLevel() <= Config.LOG_DEBUG_LEVEL)
                log.trace(integrityCheckReport2str(integrityCheckReport));

            // Running the "core" plugin
            Util.log("[JPFManager] Running the core plugin");
            Plugin corePlugin = pluginManager.getPlugin("core");

            if (corePlugin == null) {
                // throw new Exception("can't get plug-in core");
                administration
                    .showMessage("Probleme lors du chargement du core plugin.\n"
                                 + "Les plugins ne pourront pas etre charges");
            }

            // Extension points initialization
            Util.log("[JPFManager] Extension points initialization");
            ExtensionPoint AdminProject = (ExtensionPoint) corePlugin
                .getClass().getMethod("getAdminProjectExtPoint",
                                      new Class[] {}).invoke(corePlugin, new Object[] {});

            if (!AdminProject.getConnectedExtensions().isEmpty()) {

                for (Iterator it = AdminProject.getConnectedExtensions()
                         .iterator(); it.hasNext();) {
                    Extension adminExt = (Extension) it.next();
                    AdminProject adminProject = null;
                    adminProject = (AdminProject) activateExtension(adminExt);

                    /**
                     * Initialize the plugin
                     */
                    adminProject.initAdminProject((Component) UIComponentsMap
                                                  .get(UICompCst.ADMINPROJECT_PANEL));

                    /**
                     * Get the Panel to add
                     */
                    pluginAdminProject = adminProject
                        .activateInSalomeAdminProject(UIComponentsMap,
                                                      administration, user, projet);

                    /**
                     * Get the name of the bouton
                     */
                    String buttonName = adminProject.buttonName();

                    /**
                     * Add the plugin Panel to the admin View
                     */
                    panelAdminProject = (JPanel) UIComponentsMap
                        .get(UICompCst.ADMINPROJECT_PANEL);
                    ButtonGroup buttonGroup = (ButtonGroup) UIComponentsMap
                        .get(UICompCst.ADMINPROJECT_BUTTONGROUP);
                    JPanel panelRadioButon = (JPanel) UIComponentsMap
                        .get(UICompCst.ADMINPROJECT_BUTTONS_PANEL);

                    JRadioButton radioButton = new JRadioButton(buttonName);
                    radioButton.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                panelAdminProject.removeAll();
                                // adminProject.updateData();
                                panelAdminProject.add(pluginAdminProject,
                                                      BorderLayout.CENTER);
                                panelAdminProject.validate();
                                panelAdminProject.repaint();
                            }
                        });
                    buttonGroup.add(radioButton);
                    panelRadioButon.add(Box
                                        .createRigidArea(new Dimension(1, 15)));
                    panelRadioButon.add(radioButton);

                    administration.associatedExtension(adminExt.getId(),
                                                       adminExt);
                }
            }

        } catch (Exception e) {
            log.error("", e);
        }
    }

    /*
     * private void initExtsionTestDriverForAdmin(IPlugObject administration,
     * ExtensionPoint pExtsionTestDriver) { if
     * (!pExtsionTestDriver.getConnectedExtensions().isEmpty()) { for (Iterator
     * it = pExtsionTestDriver.getConnectedExtensions() .iterator();
     * it.hasNext();) { try { Extension testDriverExt = (Extension) it.next();
     * String extList = testDriverExt.getParameter("extensions")
     * .valueAsString(); administration.associatedTestDriver(testDriverExt,
     * extList); administration.associatedExtension(testDriverExt.getId(),
     * testDriverExt); Util.log("Add testDriverExt : " + testDriverExt.getId());
     * } catch (Exception e) { // TODO Util.err(e); } } } }
     */

    /*
     * private void initExtsionScriptEngineForAdmin(IPlugObject administration,
     * ExtensionPoint pExtsionScriptEngine) { if
     * (!pExtsionScriptEngine.getConnectedExtensions().isEmpty()) { for
     * (Iterator it = pExtsionScriptEngine.getConnectedExtensions() .iterator();
     * it.hasNext();) { try { Extension scriptEngineExt = (Extension) it.next();
     * String extList = scriptEngineExt.getParameter("extensions")
     * .valueAsString(); administration.associatedScriptEngine(scriptEngineExt,
     * extList); administration.associatedExtension(scriptEngineExt.getId(),
     * scriptEngineExt); Util.log("Add ExtsionScriptEngine : " +
     * scriptEngineExt.getId()); } catch (Exception e) { // TODO Util.err(e); }
     * } } }
     */

}
