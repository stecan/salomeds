package org.objectweb.salome_tmf.plugins;

import java.net.URL;
import java.util.Vector;

import org.java.plugin.ExtensionPoint;
import org.java.plugin.PluginManager;
import org.objectweb.salome_tmf.plugins.core.Common;

public interface IPlugObject {
    public Object associatedExtension(Object key, Object value); // implementation
    // by
    // Hastable

    public Object associatedScriptEngine(Object key, Object value);// implementation
    // by
    // Hastable

    public Object associatedTestDriver(Object key, Object value); // implementation
    // by
    // Hastable

    public Object getAssociatedExtension(Object key); // implementation by
    // Hastable

    public Object getAssociatedScriptEngine(Object key);// implementation by
    // Hastable

    public Object get1ssociatedTestDriver(Object key); // implementation by
    // Hastable

    public void addBugTrackers(Object bugTracker);

    public Vector getBugTracker();

    public void addReqManager(Object reqManager);

    public Vector getReqManagers();

    public void addXMLPrinterExtension(Object xmlPrinter);

    public Vector getXMLPrintersExtension();

    public void initExtsionTestDriver(ExtensionPoint ext);

    public void initExtsionScriptEngine(ExtensionPoint ext);

    public void showMessage(String msg); // initExtsionScriptEngine

    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackerE,
                               ExtensionPoint reqMgr);

    public void addPlgToUICompList(Integer id, Common plugin);

    // jpf et url
    public URL getUrlBase();

    public JPFManager getPluginManager();

    public void addStatistiquess(Object statistiques);

    public Vector getStatistiques();

    public void init_Component(PluginManager pluginManager,
                               ExtensionPoint commonE, ExtensionPoint testE,
                               ExtensionPoint scriptE, ExtensionPoint bugTrackerE,
                               ExtensionPoint statistiquesE, ExtensionPoint reqMgr);

    // public void addJarToClassLoader(URL jar);
}
