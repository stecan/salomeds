package org.objectweb.salome_tmf.plugins.core;

import java.awt.Color;
import java.util.Vector;

import javax.swing.JPanel;

import org.objectweb.salome_tmf.data.Attachment;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.User;
import org.objectweb.salome_tmf.ihm.main.IBugJDialog;


public interface BugTracker {

    public final static int CRITICAL = 1;
    public final static int MAJOR = 0;


    /**
     * Bug Tracker initialisation
     * @param Applet instance
     */
    public void initBugTracker(Object ihm) throws Exception ;

    /**
     * @return true if projet, and user are stored in other DB (not in salome DB)
     */
    public boolean isIndependantTracker()  ;

    /**
     * @return true if the tracker support ICAL/QSCORE
     */
    public boolean isSuportIndicators() ;

    /**
     * Returns "true" if current user in Salome TMF exists in bug tracker's database, "false" if not
     * @return "true" if current user in Salome TMF exists in bug tracker's database, "false" if not
     */
    public boolean isUserExistsInBugDB();

    /**
     * Adds a user to the bug tracking database
     * @param user The user to add to the bug tracking database
     */
    public void addUser(User user) throws Exception;

    /**
     * Adds a project to the bug tracking database
     * @param project The project to add to the bug tracking database
     */
    public void addProject(Project project) throws Exception;

    /**
     * Adds an environment to the bug tracking database
     * @param environment The environment to add to the bug tracking database
     */
    public void addEnvironment(Environment environment) throws Exception;


    /**
     * Autonaume Defect view for adding defect IHM called by salome if isAutonaume return true
     *
     * @param actionName
     * @param actionDesc
     * @param actionAwatedRes
     * @param actionEffectiveRes
     */
    public JPanel getBugViewPanel( IBugJDialog pIBugJDialog, String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes );


    /**
     *
     * @return A panel representing state of defect by color or null
     */
    public Color getStateDefect(Attachment pAttachment);




    /**
     *To know if a Defect is open.
     *
     *Used to filtering Campagne with anomalie
     *
     * @param pAttachment
     * @return
     */
    public boolean isOpenDefect(Attachment pAttachment);

    public JPanel getDefectPanelForTests(Vector<Test> testSelected);

    /**
     * Adds a bug to the bug tracking database
     * @param long_desc
     * @param assigned_to
     * @param url_attach
     * @param bug_severity
     * @param short_desc
     * @param bug_OS
     * @param bug_priority
     * @param bug_platform
     * @param pExec
     * @param pExecRes
     * @param ptest
     * @return
     * @throws Exception
     */
    public Attachment addBug(String long_desc, String assigned_to, String url_attach, String bug_severity, String short_desc, String bug_OS,
                             String bug_priority, String bug_platform, String bug_reproductibility, Execution pExec, ExecutionResult pExecRes,
                             Test ptest) throws Exception;

    /**
     * Modify existing bug represented by pAttach with new values
     * @param pAttach
     * @param long_desc
     * @param assigned_to
     * @param url_attach
     * @param bug_severity
     * @param bug_satus
     * @param short_desc
     * @param bug_OS
     * @param bug_priority
     * @param bug_platform
     * @param bug_env
     * @throws Exception
     */
    public void modifyBug(Attachment pAttach, String long_desc, String assigned_to, String url_attach, String bug_severity, String bug_status, String short_desc, String bug_OS,
                          String bug_priority, String bug_platform, String bug_env, String bug_reproducibility, String bug_resolution) throws Exception ;

    /**
     * Return the bug ID of the defect representing by pAttachment
     * @param pAttachment
     * @return
     */
    public String getBugID(Attachment pAttachment);

    public String getBugKey(Attachment pAttachment);

    public String getName(Attachment pAttachment);

    public String getType(Attachment pAttachment);

    public String getPriority(Attachment pAttachment);

    public String getStatus(Attachment pAttachment);

    /**
     * Shows a bug from the bug (in bugtarker or using class AskNewBug)
     * @param The attachment which represents the bug
     */
    public void showBug(Attachment attach);

    /**
     * Shows a bug from the bug in the bug tracker
     * @param The attachment which represents the bug
     */
    public void showBugInBugTracker(Attachment attach);

    /**
     * Shows the bug list for an environment
     * @param Environment
     */
    public void showEnvironmentBugs(Environment env);

    /**
     * Calculates the number of bugs with specifier severity and correction
     * @param Environment
     * @param severity
     * @param onlyCorretedBugs
     * @return The number of critical bugs in current project
     */
    //public int getEnvironmentNbBugs(Environment env, int severity, boolean onlyCorretedBugs);
    public int getEnvNbBugs(Environment env,boolean onlyCorretedBugs);

    /**
     * Get bug tracker name
     * @return bug tracker name
     */
    public String getBugTrackerName();

    /**
     * Get bug tracker attachment description
     * @return bug tracker name
     */
    public String getBugTrackerAttachDesc();

    /**
     * Returns OS list
     * @return
     */
    public Vector getBugOSList();

    /**
     * Returns priority list
     * @return
     */
    public Vector getBugPriorityList();

    /**
     * Returns plateform list
     * @return
     */
    public Vector getBugPlateformList();

    /**
     * Returns severity list
     * @return
     */
    public Vector getBugSeverityList();

    /**
        * Returns the list of bugTracking users
        * @return
        */
    public Vector getBugTrackerAllUsers();


    public Vector getBugTrackerStatusList();

    public Vector getBugTrackerReproductibilityList();

    public Vector getBugTrackerResolutionList();

    public boolean isEditableOS();

    public boolean isEditablePlateForme();

    public boolean isUsesBugReproducibily();

    public boolean isUsesBugResolution();

    public String getAdditionalBugDesc();

    public void suspend();

    public int getQsScoreDefectForTest(Test test, Environment env);
}
