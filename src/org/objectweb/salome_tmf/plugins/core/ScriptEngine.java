/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins.core;

import java.net.URL;
import java.util.Hashtable;
import org.objectweb.salome_tmf.data.Script;
/**
 *
 * @author  marchemi
 */
public interface ScriptEngine {
   
    final static int ENV_SCRIPT = 1;
    final static int PRE_SCRIPT = 2;
    final static int POST_SCRIPT = 3;
    
    
   
    /* 
     * initialisation du plugin
     */
    public void initScriptEngine(URL urlSalome); 
    
    /**
     * Lancemement du script.
     * @param type de script ENV_SCRIPT, PRE_SCRIPT, POST_SCRIPT
     * @return Valeur de terminaison du script (0 : OK, <>0 code d'erreur)
     */
    public int runScript(int type, String urlScriptFile, Script pScript, Hashtable arg, String plugParam)  throws Exception;
    
    /**
     * Odre d'edition du script
     * @param type de script ENV_SCRIPT, PRE_SCRIPT, POST_SCRIPT
     */
    public void editScript(String urlScriptFile, int type, Script pScript, Hashtable arg, String plugParam) throws Exception;;
    
    /**
     * This method is called when user want to modify/set free arg of script
     * @return a string represented arg for test the testDriver 
     */
    public String modifyEngineAgument(Script pScript);
    
    public void stopScript()  throws Exception;
    
    public String getScriptLog();
}

