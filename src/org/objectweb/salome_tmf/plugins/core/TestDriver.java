
/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins.core;

import java.net.URL;
import java.util.Hashtable;

import org.objectweb.salome_tmf.data.AutomaticTest;


public interface TestDriver {
    
    /**
     * Init plugin
     * @param urlSalome
     */
    public void initTestDriver(URL urlSalome);
    
    /**
     * Display help/About on Plugin TestDriver
     */
    public void getHelp();
    
    /**
     * @return a string represented default arg for test the testDriver
     */
    public String getDefaultTestDiverAgument();
    
    /**
     * This method is called when user want to modify/set testDriver arg
     * @param oldArg are actually argumentset to the test driver
     * @return a string represented arg for test the testDriver
     */
    public String modifyTestDiverAgument(String oldArg);
    
    /**
     * This method is called when test is being to be executed
     * @return Valeur du verdict du test (: Erreur : -1 , Inconclusif : 2 , Fail : 1, Pass : 0)
     */
    public int runTest(String testScript, AutomaticTest pTest, Hashtable argTest, String plugArg) throws Exception;
    
    /**
     * Called by IHM to obtain log for the test execution, 
     * if log available, a fileattachement is iserted to the execution result
     * @return String representation of log or null if no log available
     */
    public String getTestLog();
    
    /**    
     * Stop the test execution
     */
    public void stopTest()  throws Exception;
    
    
    /**
     * This methode is called when testscrip is edited
     * @param testScript
     * @param pTest
     * @param arg
     * @param plugParam
     * @throws Exception
     */
    public void editTest(String testScript, AutomaticTest pTest, Hashtable arg, String plugParam) throws Exception;
    
    /**
     * This methode is called when choice a script for test
     * @return le test choisi
     */
    public java.io.File choiceTest(AutomaticTest pTest);
    
    /**
     * This methode is called when delete test script
     * @param pTest
     */
    public void onDeleteTestScript(AutomaticTest pTest);  
    
    /**
     * This methode is called when update test script
     * @param testScript : path to the file
     * @param pTest
     */
    public void updateTestScriptFromImport(String testScript, AutomaticTest pTest);
        
}
