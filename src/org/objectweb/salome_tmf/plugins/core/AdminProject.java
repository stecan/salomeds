package org.objectweb.salome_tmf.plugins.core;

import java.awt.Component;
import java.util.Map;

import javax.swing.JPanel;
import org.objectweb.salome_tmf.plugins.IPlugObject;

public interface AdminProject {

    /**
     * InitAdminProject
     */
    public void initAdminProject(Component comp);

    public JPanel activateInSalomeAdminProject(Map adminUIComps,
                                               IPlugObject pIPlugObject, String user, String projet);

    public String buttonName();

}
