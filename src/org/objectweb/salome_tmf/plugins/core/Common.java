/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins.core;

import java.util.Vector;

import javax.swing.JMenu;

import org.java.plugin.ExtensionPoint;


/**
 * "Home" interface for particular Common plugin. It is used by application core
 * to start up tool
 * application window.
 * @version $Id$
 */
public interface Common {

    /*
     * Plugin initialisation
     * @ param vt
     */
    void init(Object ihm);
    
    /**
     * Returns true if the plugin has menu items for "Tools" menu for tests
     * @return true if the plugin has menu items for "Tools" menu for tests
     */
    public boolean isActivableInTestToolsMenu();
    
    /**
     * Returns true if the plugin has menu items for "Tools" menu for campaigns
     * @return true if the plugin has menu items for "Tools" menu for campaigns
     */
    public boolean isActivableInCampToolsMenu();
    
    /**
     * Returns true if the plugin has menu items for "Tools" menu for data management
     * @return true if the plugin has menu items for "Tools" menu for data management
     */
    public boolean isActivableInDataToolsMenu();
    
    /**
     * Returns true if the plugin uses or is activable in other UI components than these 
     * three menus true if the plugin uses or is activable in other UI components than these 
     * @return
     */
    public boolean usesOtherUIComponents();
    
    /**
     * Activates the plugin in the "Tools" menu for tests if it's activable 
     * in this menu
     * @param
     */
    public void activatePluginInTestToolsMenu(JMenu testToolsMenu);
    
    /**
     * Activates the plugin in the "Tools" menu for campaigns if it's activable 
     * in this menu
     * @param
     */
    public void activatePluginInCampToolsMenu(JMenu campToolsMenu);
    
    /**
     * Activates the plugin in the "Tools" menu for data management if it's 
     * activable in this menu
     * @param
     */
    public void activatePluginInDataToolsMenu(JMenu dataToolsMenu);
    
    /*
     * Getting the UI components relative constants in which the plugin is activable
     * or which it uses different from these three menus
     * @see salome.ihm.UICompCst
     * @return A vector containing UI components constants
     */
    public Vector getUsedUIComponents();
    
    /*
     * Activates the plugin in static UI component 
     * This method will be called for each static UI component in the Vector returned by 
     * the previous method ( getUsedUIComponents() ).
     * @param UIComponents
     */
    public void activatePluginInStaticComponent(Integer uiCompCst);
    
    /*
     * Activates the plugin in dynamic UI component 
     * This method will be called for each dynamic UI component in the Vector returned by 
     * the previous method ( getUsedUIComponents() ).
     * @param UIComponents
     */
    public void activatePluginInDynamicComponent(Integer uiCompCst);
    
    /**
     * Retruns true if the plugin is freezable
     */
    public boolean isFreezable();
    
    /**
     * Freezes the plugin
     */
    public void freeze();
    
    /**
     * Unfreezes the plugin
     */
    public void unFreeze();
    
    /**
     * true if the plugin is freezed
     */
    public boolean isFreezed();
    
    /**
     * Called by Salome when all plugin are initialized and when Salome-tmf started
     * @param commonExtensions a ExtensionPoint of all plugin available of type common
     * @param testDriverExtensions a ExtensionPoint of all plugin available of type testDriver
     * @param scriptEngineExtensions a ExtensionPoint of all plugin available of type scriptEngine
     * @param bugTrackerExtensions a ExtensionPoint of all plugin available of type bugTrackerEngine
     */
    public void allPluginActived(ExtensionPoint commonExtensions, ExtensionPoint testDriverExtensions, 
                                 ExtensionPoint scriptEngineExtensions, ExtensionPoint bugTrackerExtensions);
    public void allPluginActived(ExtensionPoint commonExtensions, ExtensionPoint testDriverExtensions, ExtensionPoint scriptEngineExtensions, ExtensionPoint bugTrackerExtensions, ExtensionPoint statistiquesExtensions);
}
