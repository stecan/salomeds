package org.objectweb.salome_tmf.plugins.core;

import java.util.HashMap;

import org.dom4j.Element;
import org.objectweb.salome_tmf.data.Attachment;

public interface XMLWriterPlugin {
    public String formater(String s) throws Exception;
    public void addAttach(Element pElt, String dirName, HashMap<String,Attachment> attachs , String fileName) throws Exception;
}
