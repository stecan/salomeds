package org.objectweb.salome_tmf.plugins.core;

import java.io.File;

import javax.swing.JPanel;

import org.dom4j.Document;
import org.dom4j.Element;
import org.objectweb.salome_tmf.data.Action;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.DataSet;
import org.objectweb.salome_tmf.data.Environment;
import org.objectweb.salome_tmf.data.Execution;
import org.objectweb.salome_tmf.data.ExecutionResult;
import org.objectweb.salome_tmf.data.ExecutionTestResult;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.Parameter;
import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.data.Test;
import org.objectweb.salome_tmf.data.TestList;

public interface XMLPrinterPlugin {

    /************** Option Panel *************/

    JPanel getExportOptionPanel();
    JPanel getImportOptionPanel();

    /************** EXPORT *****************/
    void addXMLElement2Root(Element rootElement, XMLWriterPlugin pXMLWriter, String pathAttach);

    /* Test Plan */
    void addXMLElement2Project(Element projectElement, Project pProject, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2Family(Element familyElement, Family pFamily, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2Suite(Element suiteElement, TestList ptestList, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2Test(Element testElement, Test pTest, XMLWriterPlugin pXMLWriter, String pathAttach);
    //void addXMLElement2ValuedParameter(Element paramElement, Test pTest, Parameter pParam);
    void addXMLElement2Action(Element testElement, Action pAction, XMLWriterPlugin pXMLWriter, String pathAttach);

    /* Campaing plan */
    void addXMLElement2Campaign(Element campaignElement, Campaign pcampaign, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2Execution(Element execElement, Execution pExecution, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2ResExecution(Element resExecElement, ExecutionResult pExecRes, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2ResTestExecution(Element resTestElem, ExecutionTestResult pExecTestRes, Test pTest, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2DataSet(Element dataSetElement, DataSet pDataSet, XMLWriterPlugin pXMLWriter, String pathAttach);

    /* Data */
    void addXMLElement2Parameter(Element paramElement, Parameter pParam, XMLWriterPlugin pXMLWriter, String pathAttach);
    void addXMLElement2Environment(Element envElement, Environment pEnv, XMLWriterPlugin pXMLWriter, String pathAttach);


    /**************** IMPORT *****************/
    /**
     * Called first before updateMethode if isSupOption = true
     */
    void manageDelete(Document doc, XMLLoaderPlugin pXMLLoader) throws Exception ;

    /* Test Plan */
    void updateProjectFromXML(Document doc, boolean isSupOption, Project project, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateFamilyFromXML(Element familyElement, Family pFamily, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception  ;
    void updateSuiteFromXML(Element suiteElement, TestList pSuite, boolean isSupOption, XMLLoaderPlugin pXMLLoader)throws Exception  ;
    void updateTestFromXML(Element testElement, Test pTest, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateActionFromXML(Element actionElement, Action pAction, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;

    /* Campaing plan */
    void updateCampaignFromXML(Element campaignElement, Campaign pcampaign, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateExecutionFromXML(Element execElement, Execution pExecution, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateResExecutionFromXML(Element resExecElement, ExecutionResult pExecRes, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateDataSetFromXML(Element dataSetElement, DataSet pDataSet, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;

    /* Data */
    void updateParameterFromXML(Element paramElement, Parameter pParam, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;
    void updateEnvironmentFromXML(Element envElement, Environment pEnv, boolean isSupOption, XMLLoaderPlugin pXMLLoader) throws Exception ;

    /**
     *
     * Called last after import
     */
    void refreshNewData();

    /**************** XSLT ******************
     * @param dynamicMode true if generated HTML report is the results' report, false otherwise
     * @param multiFrame true if the generated HTML report is in multi-frame mode, false otherwise
     */
    File getXSLToImport(boolean dynamicMode, boolean multiFrame, boolean htmlFormat);

    /**
     * Get the translation file for the XSLT transformation
     * @return the XML file for translation
     */
    File getTranslationFile();
    void setExport(boolean b);
    void setImport(boolean b);
    /**
     * Gives the chapter title in the HTML report related to the concerned plugin.
     * @return the title
     */
    String getChapterTitleInReport();
    /**
     * Gives the name of the parameter for the XSLT transformation
     * return null if the given chapter name does not correspond to the plugin title.
     * @param chapterName
     * @return
     */
    String getParameterName(String chapterName);
}
