/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins.core;

import java.util.Map;

import org.objectweb.salome_tmf.data.Project;
import org.objectweb.salome_tmf.plugins.IPlugObject;


/**
 * "Home" interface for particular Common plugin. It is used by application core
 * to start up tool
 * application window.
 * @version $Id$
 */
public interface Admin {

    /*
     * Activates the plugin  
     * the previous method ( getUsedUIComponents() ).
     * @param UIComponents
     */
    public void activateInSalomeAdmin(Map adminUIComps, IPlugObject pIPlugObject);
    
    /**
     * Called when delete a project in Admin
     * @param p
     */
    public void onDeleteProject(Project p);
}
