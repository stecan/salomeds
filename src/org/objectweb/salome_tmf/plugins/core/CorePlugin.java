/*
 * SalomeTMF is a Test Management Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @author Fayçal SOUGRATI, Vincent Pautret, Marche Mikael
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */

package org.objectweb.salome_tmf.plugins.core;

//import java.net.URL;
//import java.io.File;

import org.java.plugin.ExtensionPoint;
import org.java.plugin.Plugin;
import org.java.plugin.PluginDescriptor;
import org.java.plugin.PluginManager;

//import salome.ihm.SalomeTMF;

/**
 *
 * @version $Id$
 */
public class CorePlugin extends Plugin {

    // private URL dataFolder;
    // private File dataFolderDBG;

    /**
     * @param manager
     * @param descr
     */
    public CorePlugin(PluginManager manager, PluginDescriptor descr) {
        super(manager, descr);
    }

    /**
     * @see org.java.plugin.Plugin#doStart()
     */
    @Override
    protected void doStart() throws Exception {
        // no-op
    }

    /**
     * @see org.java.plugin.Plugin#doStop()
     */
    @Override
    protected void doStop() throws Exception {
        // no-op
    }

    /**
     * Fonction qui retourne une instance du point d'extension "Common"
     */
    public ExtensionPoint getCommonExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "Common");
    }

    /**
     * Fonction qui retourne une instance du point d'extension "TestDriver"
     */
    public ExtensionPoint getTestDriverExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "TestDriver");
    }

    /**
     * Fonction qui retourne une instance du point d'extension "ScriptEngine"
     */
    public ExtensionPoint getScriptEngineExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "ScriptEngine");
    }

    /**
     * Fonction qui retourne une instance du point d'extension "Admin"
     */
    public ExtensionPoint getAdminExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "Admin");
    }

    /**
     * returns an instance of "BugTracker" extension point
     *
     * @return Instance of "BugTracker" extension point
     */
    public ExtensionPoint getBugTrackerExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "BugTracker");
    }

    public ExtensionPoint getXMLPrinterExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "XMLPrinterPlugin");
    }

    /**
     * returns an instance of "ReqManager" extension point
     *
     * @return Instance of "ReqManager" extension point
     */
    public ExtensionPoint getReqManagerExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "ReqManager");
    }

    /**
     * returns an instance of "Statistiques" extension point
     *
     * @return Instance of "Statistiques" extension point
     */
    public ExtensionPoint getStatistiquesExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "Statistiques");
    }

    /**
     * returns an instance of "Statistiques" extension point
     *
     * @return Instance of "Statistiques" extension point
     */
    public ExtensionPoint getAdminProjectExtPoint() {
        return getManager().getRegistry().getExtensionPoint(
                                                            getDescriptor().getId(), "AdminProject");
    }
}
