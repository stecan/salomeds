package org.objectweb.salome_tmf.plugins.core;

import java.util.Vector;

import javax.swing.JTree;

import org.objectweb.salome_tmf.data.Test;

public interface ReqManager {

    //20100408 - D\ufffdbut modification Forge ORTF v1.0.0
    public final static int PRIORITY = 0;

    public final static int CATEGORY = 1;

    public final static int COMPLEXE = 2;

    public final static int STATE = 3;

    public final static int VALID_BY = 4;

    public final static int CRITICAL = 5;

    public final static int UNDERSTANDING = 6;

    public final static int OWNER = 7;
    //20100408 - Fin modification Forge ORTF v1.0.0

    /**
     * Returns the number of requirements for a category
     * @param cat requirements category
     * @return
     */
    public int getNbReqForCat(int cat);

    /**
     * Returns true if all requierements with category cat and priority pri are covered by tests
     * @param cat
     * @param pri
     * @return
     */
    public boolean isAllReqCovered(int cat, int pri);

    /**
     * Returns true if all requierements with category cat and priority pri are covered by tests
     * and if all these tests are executed
     * @param cat
     * @param pri
     * @return
     */
    public boolean isAllReqCoveredExecuted(int cat, int pri);

    /**
     * Returns the list of tests linked with requirements with category cat
     * @param cat
     * @return
     */
    public Vector getAssociatedTestsForReqCat(int cat);

    /**
     * Returns req plugin name
     * @return
     */
    public String getReqManagerName();

    /**
     * Deletes requirements links with test
     * @param idTest
     */
    public void deleteReqLinkWithTest(int idTest);

    /**
     * Add a link between a requirement and a test
     */
    public void addReqLinkWithTest(Test wwtest, int idReq);


    //20100128 - D\ufffdbut modification Forge ORTF v1.0.0
    public JTree getTreeRequirement();

    /**
     * Update the value of requirement comboboxes.
     *
     * @param type The combox to update
     * @param value Value to add.
     */
    public void updateReqHMI(int type, String value);
    //20100128 - Fin modification Forge ORTF v1.0.0

}
