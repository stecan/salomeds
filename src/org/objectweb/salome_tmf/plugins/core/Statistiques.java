package org.objectweb.salome_tmf.plugins.core;

import java.util.Vector;


public interface Statistiques {
        
    public void initStatistiques(Object ihm) throws Exception ;
        
    //public JPanel getBugViewPanel( IBugJDialog pIBugJDialog, String actionName, String actionDesc, String actionAwatedRes, String actionEffectiveRes );
        
    public void suspend();
    public void init(Object ihm);
    public boolean usesOtherUIComponents();
    public Vector getUsedUIComponents();
    public void activatePluginInStaticComponent(Integer uiCompCst);
    public boolean isFreezable();
    public void freeze();
    public void unFreeze();
    public boolean isFreezed();
    public String getBusinessIntelligenceName();
    
}
