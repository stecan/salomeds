package org.objectweb.salome_tmf.plugins.core;

import org.dom4j.Element;
import org.objectweb.salome_tmf.data.WithAttachment;

public interface XMLLoaderPlugin {
    void updateElementAttachement(Element pElemnt, WithAttachment pData, boolean isNewElement);
}
