package org.objectweb.salome_tmf.plugins;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.jar.Manifest;

import org.objectweb.salome_tmf.api.Util;


public class PluginClassLoader extends URLClassLoader{
        
    String tmpDir = System.getProperties().getProperty("java.io.tmpdir");
    String fs = System.getProperties().getProperty("file.separator");
    String salome_cache = "salome_cache";

    boolean useCacheFile = true;
    boolean cacheAll = false;
    Hashtable loadedJar;
        
    ClassLoader pClassLoader;
        
    /*PluginClassLoader(ClassLoader parent) {
      super(new URL[0], parent); 
      init();
      pClassLoader  = parent; 
      }*/
        
    PluginClassLoader(ClassLoader parent ) {
        super(new URL[] { }, parent);
        //ClassLoader.getSystemClassLoader()
        init();
        pClassLoader  = parent; 
        loadedJar = new Hashtable();
    }

    void init(){
        if (useCacheFile){
            try {
                cacheAll = create_cache_dir();
            } catch (Exception e){
                useCacheFile = false;
            }
        }
                
    }
        
    public void AddJar(URL jar){
        File f = new File(jar.getFile());
        String jarName = f.getName();
        if (loadedJar.get(jarName) != null){
            return;
        }
        if (useCacheFile){
            jar = getCachedFile(jar);
        }
                
        try {
            Class pclC = pClassLoader.getClass();
            pclC.getMethod("addLocalJar", new Class[] {URL.class}).invoke(
                                                                          pClassLoader,new Object[] {jar}
                                                                          );
            Util.log("[PluginClassLoader->addJar] load" + jar + ", with classloader  " + pClassLoader);
            loadedJar.put(jarName, jar);
        }   catch (Exception e){
            try  {
                addURL(jar);
                //add(jar);
                loadedJar.put(jarName, jar);
            }catch (Exception e1){
                addURL(jar);
            }
                        
        }
    }
        
    /***************************** CORE **************************************/
    /*
      Hashtable urlsIn = new Hashtable();
      private boolean debug = false;

    
      private boolean alreadyIn(String jarurl){
      int slash = jarurl.lastIndexOf('/');// are urls always composed of / ?
      String key = jarurl.substring(slash+1);
      return (urlsIn.containsKey(key));
      }
    
    
      private void registerUrl(URL url){
      int slash = url.toString().lastIndexOf('/');// are urls always composed of / ?
      String key = url.toString().substring(slash+1);
      urlsIn.put(key,url);
      }

    
      public void add(URL url) {
      if (!alreadyIn(url.toString())){
      super.addURL(url);
      registerUrl(url);
      } 
      }

   
      public void add(URL[] urls) {
      for (int i=0;i<urls.length;i++){
      if (!alreadyIn(urls[i].toString())){
      super.addURL(urls[i]);
      registerUrl(urls[i]);
      }
      }
      }

   
      public void add(String jarurlname) {
      URL jarurl = null;
        
      try {
      if ( (jarurlname.startsWith("file:")) ||
      (jarurlname.startsWith("http:")) ||
      (jarurlname.startsWith("ftp:"))
      )
      jarurl = new URL(jarurlname);
      else
      jarurl = new URL("file:"+jarurlname);                       
      } catch (MalformedURLException eurl) {
      Util.debug("JarClassLoader Malformed URL "+eurl);
      }
      if (jarurl != null){
      if (!alreadyIn(jarurl.toString())){
      super.addURL(jarurl);
      registerUrl(jarurl);
      }
      }
      }

      public Class loadClassFromJar(String name) {
      Class c = null;
      c = getLoadedClass(name);
      if (c!=null)
      return c;
      try {
      c = findClass(name); 
      } catch (ClassNotFoundException e) {
      if (debug) System.err.println("JarClassLoader loadClassFromJar Exception "+e); 
      } catch (NullPointerException ne){
      if (debug) System.err.println("JarClassLoader loadClassFromJar Null Exception "+ne);
      }
      return c;
      }
 

      public Class loadMainClassFromJar(URL jarurl) {
      // find main class name in manifest
        
      // add and register url if not already in
      //resetClassLoader();
      add(jarurl);

      boolean empty = true;
      ManifestWrapper mf = null;
      byte buffer[] = new byte[1024];
      ZipInputStream zis = null;
      try {

      InputStream inf = jarurl.openStream();
      zis = new ZipInputStream(inf);
      ZipEntry ent = null;

      while ((ent = zis.getNextEntry()) != null) {
      empty = false;

      String name = ent.getName();
      String type = new String();

                
      ByteArrayOutputStream baos = new ByteArrayOutputStream();

                

      for (;;) {
      int len = zis.read(buffer);
      if (len < 0) {
      break;
      }
      baos.write(buffer, 0, len);
      }

      byte[] buf = baos.toByteArray();
      int size = buf.length;

      if (ManifestWrapper.isManifestName(name)) {
      type = "manifest/manifest";
      }

      if (type.equals("manifest/manifest")) {
      mf = new ManifestWrapper(buf);
      }
      }

      } catch (IOException ex) {
      Util.debug("IOException loading archive: " + ex);
      } catch (Throwable th) {
      Util.debug("Caught " + th + " in loadit()");
      } finally {
      if (zis != null) {
      try {
      zis.close();
      } catch (Exception ex) {
      // ignore
      }
      }
      }

      // Unfortunately ZipInputStream doesn't throw an exception if you hand
      // it a non-Zip file.  Our only way of spotting an invalid Jar file
      // is if there no ZIP entries.
      if (empty) {
      Util.debug("jar file empty");

      return null;
      }

      // find class name


      if (mf==null)
      return null;
      String res = null;
      //        Vector manifestHeaders = mf.;
      for (Enumeration e = mf.entries() ; e.hasMoreElements() ;) {
      MessageHeader mh = (MessageHeader)e.nextElement();


      res = mh.findValue("Main-Class");
      if (res==null) res = mh.findValue("Name");
      if (res!=null) break;
      }
         

      if (res==null)
      return null;

      String name = res;
      Class c = null;

      c = getLoadedClass(name);
      if (c!=null)
      return c;

      try {
      c = findClass(name);
      } catch (ClassNotFoundException e) {
      if (debug) System.err.println("JarClassLoader loadClassFromJar Exception "+e);
      } catch (NullPointerException ne){
      if (debug) System.err.println("JarClassLoader loadClassFromJar Null Exception "+ne);
      } catch (LinkageError le){
      if (debug) System.err.println("JarClassLoader loadClassFromJar Linkage Error "+le);
      }
      return c;
      }



      public Class getLoadedClass(String classname){
      //return super.findLoadedClass(classname);
      Class c = null;
      try {
      c = super.loadClass(classname);
      } catch (ClassNotFoundException e){ 
      if (debug) System.err.println("JarClassLoader getLoadedClass Exception "+e); 
      }
      return c;
      }

      public void resolvesClass(Class theClass){
      try {
      super.resolveClass(theClass);
      } catch (Exception e){
      if (debug) System.err.println("JarClassLoader resolvesClass Exception "+e);
      }
      }
    */
   
    /*************************** CACHE ************************************/
        
    URL getCachedFile(URL jarURL){
        URL fileURL = jarURL;
        if (jarURL.getProtocol().equals("file")){
            return jarURL;
        }
        //Util.log("[PluginClassLoader->getCachedFile] for   " +jarURL);
        try {
            File f = new File(jarURL.getFile());
            String jarName = f.getName();
                
            //String urlRemote =  "jar:" +   jarURL.toString() + "!/";
            File fileLocal = new File(tmpDir + fs + salome_cache +  fs + jarName);
            if(needUpdate(jarURL, fileLocal, jarName )){
                fileURL =  installFile(jarURL, jarName).toURL();
                        
            } else {
                Util.log("[PluginClassLoader->getCachedFile] Use cache for   " +jarURL);
                fileURL = fileLocal.toURL();
            } 
        }catch (Exception e){
                        
        }
        //Util.log("[PluginClassLoader->getCachedFile] set url to use  " +fileURL);
        return fileURL;
    }
        
    boolean needUpdate( URL pJarURL, File localFile, String name){
        boolean ret = false;
        if (!localFile.exists()) {
            //  log("[PluginClassLoader->needUpdate] Need to Update (not exist) : " + localFile);
            return true;
        }
        try {
            JarInputStream jarInput = new JarInputStream(pJarURL.openStream());
            Manifest remoteManifest = jarInput.getManifest();
                           
            JarFile localeJar = new JarFile(localFile);
            Manifest localManifest = localeJar.getManifest();
               
                          
            if (!localManifest.equals(remoteManifest)){
                //Util.log("[PluginClassLoader->needUpdate] Need to Update : " + localFile);
                ret = true;
            } else {
                // ZipEntry  getEntry(String name) META-INF/SALOME_T.SF
                if ( localManifest.getEntries().size() == 0){
                    // Util.log("[PluginClassLoader->needUpdate] Need to Update " + localFile + " No attribute in manifest") 
                    ret = true;  
                }
            }
                           
            //Util.debug("JarManifest  = " + manifest + " for " + jar);
        }catch (Exception e){
            Util.err(e);
            ret = true;
        }
        return ret;
    }
         
    boolean create_cache_dir() throws Exception{
        boolean ret = false;
        File file = new File(tmpDir + fs + salome_cache);
        if (!file.exists()){
            file.mkdirs();
            ret = true;
        }
        return ret;
    }
        
    File installFile(URL pUrl, String name) throws Exception{
        File file = null;
        try {
            URLConnection urlconn = pUrl.openConnection();
            InputStream is = urlconn.getInputStream();
            file = new File(tmpDir + fs + salome_cache + fs + name);
            writeStream(is, file);
                        
        }catch (Exception  e){
            Util.err(e);
            return null;
        }  
        return file;
    }
        
    void writeStream( InputStream is , File f) throws Exception{
        byte[] buf = new byte [102400];
        f.createNewFile();
        FileOutputStream fos = new FileOutputStream (f); 
        int len = 0;
        while ((len = is.read (buf)) != -1){
            fos.write (buf, 0, len);
        }
        fos.close();
        is.close();
    }
        
}


