package org.objectweb.salome_tmf.test.basic;

import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.data.Campaign;
import org.objectweb.salome_tmf.data.Family;
import org.objectweb.salome_tmf.data.TestList;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;
import org.objectweb.salome_tmf.test.common.SalomeProxy;

public class CopyPasteBehaviorTest {
        
    final static String URL = "http://salome-tmf.rd.francetelecom.fr/www/salome-demo/salome_tmf/";
    final static String USER = "marchemi";
    final static String PROJECT = "salome-demo";
        
    final static String FAMILY_NAME = "Famille par defaut";
    final static String TESTLIST_NAME = "Suite par defaut";
    final static String CAMPAIGN_NAME = "c";
        
    public CopyPasteBehaviorTest() {
    }
        
    public void run() {
        try {
            SalomeProxy pSalomeProxy = new SalomeProxy();
            pSalomeProxy.connect(URL,USER,PROJECT);
                        
            TestList tListCopy = copyPasteTestList(FAMILY_NAME,TESTLIST_NAME);
            Campaign campCopy = copyPasteCampaign(CAMPAIGN_NAME);
            deleteTestList(tListCopy);
            deleteCampaign(campCopy);
                        
            pSalomeProxy.disconnect();
        } catch(Exception  e){
            Util.err(e);
        }
    }
        
    public TestList copyPasteTestList(String familyName, String testListName){
        TestList tListCopy = null;
        try {
            Family pFamily = DataModel.getCurrentProject().getFamilyFromModel(familyName);
            TestList pSuite = pFamily.getTestListInModel(testListName);
            tListCopy = TestList.copieIn(pSuite,pFamily);
        } catch(Exception  e){
            Util.err(e);
        }
        return tListCopy;
    }
        
    public Campaign copyPasteCampaign(String campaignName){
        Campaign campCopy = null;
        try {
            Campaign camp = DataModel.getCurrentProject().getCampaignFromModel(campaignName);
            campCopy = Campaign.copieIn(camp,DataModel.getCurrentUser());
        } catch(Exception  e){
            Util.err(e);
        }
        return campCopy;
    }
        
    public void deleteTestList(TestList testList) {
        try {
            testList.getFamilyFromModel().deleteTestListInDBAndModel(testList);
        } catch(Exception  e){
            Util.err(e);
        }
    }
        
    public void deleteCampaign(Campaign camp) {
        try {
            DataModel.getCurrentProject().deleteCampaignInDBAndModel(camp);
        } catch(Exception  e){
            Util.err(e);
        }
    }
        
    public static void main(String[] args){
        CopyPasteBehaviorTest behavior = new CopyPasteBehaviorTest();
        behavior.run();
    }
        
}
