package org.objectweb.salome_tmf.test.common;

import java.awt.Frame;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;

import org.objectweb.salome_tmf.api.Api;
import org.objectweb.salome_tmf.api.Util;
import org.objectweb.salome_tmf.ihm.languages.Language;
import org.objectweb.salome_tmf.ihm.main.BaseIHM;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFContext;
import org.objectweb.salome_tmf.ihm.main.SalomeTMFPanels;
import org.objectweb.salome_tmf.ihm.main.datawrapper.DataModel;


public class SalomeProxy  implements  BaseIHM, WindowListener {

    private static String strProject = null;
    private static String strLogin = null;
    private static String urlSalome = null;

    Frame ptrFrame;

    /***************************************************************************************/
    SalomeTMFContext pSalomeTMFContext;
    SalomeTMFPanels pSalomeTMFPanels;
    /****************************************************************************************/


    public SalomeProxy(){
    }

    /*public SalomeProxy(String url, String user, String project){
      strLogin = user;
      strProject = project;
      urlSalome = url;
      onInit();
      quit(true, true);
      }*/


    /******************** CLIF BEHAVIOR ***********************************/

    public void connect(String url, String user, String project){
        strLogin = user;
        strProject = project;
        urlSalome = url;
        if (!onInit()) {
            System.out.println("CONNECTION PROBLEM!");
        }
    }


    /*public void behaviorRunExec(String campName, String nameExec){

      try {
      Campaign pCamp = DataModel.getCurrentProject().getCampaignFromModel(campName);
      Execution pExec = pCamp.getExecutionFromModel("Exec0");
      ArrayList arrayExec = new ArrayList();
      arrayExec.add(pExec);
      TestMethods.runExecution(arrayExec, ((Component) new Frame()), false, false);
      } catch(Exception  e){
      Util.err(e);
      }
      }*/

    public void disconnect(){
        quit(true, true);
    }


    /************************************************************************************/


    @Override
    public void quit(boolean do_recup, boolean doclose){
        if (Api.isConnected() && doclose) {
            Api.closeConnection();
        }
        System.exit(0);
    }

    /********************************************************/
    boolean connectToAPI(){
        boolean code = true;
        try {
            Api.openConnection(new URL(urlSalome));
            if (!Api.isConnected()){
                quit(false, false);
            }
            Api.initConnectionUser(strProject, strLogin);
        } catch (Exception e){
            Util.err(e);
            code = false;
        }
        return code;
    }


    boolean onInit(){
        boolean code = true;
        if (connectToAPI()){
            initComponent();
            loadModel();
            //loadPlugin();
        } else {
            code = false;
        }
        return code;
    }

    void loadPlugin(){
        pSalomeTMFContext.loadPlugin(pSalomeTMFPanels);
    }

    void loadModel(){
        DataModel.loadFromBase(strProject, strLogin, this);
    }

    void initComponent(){
        try {
            URL pUrlSalome = new URL(urlSalome);
            Api.setUrlBase(pUrlSalome);
            Language.getInstance().setLocale(new Locale(Api.getUsedLocale()));
            //ptrFrame = new Frame();
            ptrFrame = null;
            //ptrFrame.addWindowListener(this);
            //ptrFrame.setVisible(true);
            pSalomeTMFContext = new SalomeTMFContext(pUrlSalome,ptrFrame, this);
            //pSalomeTMFPanels = new SalomeTMFPanels(pSalomeTMFContext, this);
            //pSalomeTMFPanels.initComponentPanel();

        } catch (Exception e){
            Util.err(e);
        }
    }

    @Override
    public SalomeTMFContext getSalomeTMFContext() {
        return pSalomeTMFContext;
    }

    @Override
    public SalomeTMFPanels getSalomeTMFPanels() {
        return pSalomeTMFPanels;
    }

    @Override
    public void showDocument(URL toShow , String where) {
        switch (getPlatform()) {
        case (WIN_ID): {
            System.out.println("Try Windows Command Line");
            runCmdLine(replaceToken(WIN_CMDLINE, URLTOKEN, toShow.toString()));
        }
        case (MAC_ID): {
            System.out.println("Try Mac Command Line");
            runCmdLine(replaceToken(MAC_CMDLINE, URLTOKEN, toShow.toString()));

        }
        default:
            System.out.println("Try Unix Command Line");
            for (int i = 0; i < OTHER_CMDLINES.length; i++) {
                if (runCmdLine(replaceToken(OTHER_CMDLINES[i], URLTOKEN, toShow.toString()),
                               replaceToken(OTHER_FALLBACKS[i], URLTOKEN, toShow.toString())))
                    return;
            }
        }
    }

    @Override
    public boolean isGraphique() {
        return false;
    }
    /******************/
    /* This token is a placeholder for the actual URL */
    private final String URLTOKEN = "%URLTOKEN%";

    // Used to identify the windows platform.
    private final int WIN_ID = 1;

    // Used to discover the windows platform.
    private final String WIN_PREFIX = "Windows";

    // The default system browser under windows.
    // Once upon a time:
    //   for 'Windows 9' and 'Windows M': start
    //   for 'Windows': cmd /c start
    private final String[] WIN_CMDLINE = { "rundll32",
                                           "url.dll,FileProtocolHandler", URLTOKEN };

    // Used to identify the mac platform.
    private final int MAC_ID = 2;

    // Used to discover the mac platform.
    private final String MAC_PREFIX = "Mac";

    // The default system browser under mac.
    private  final String[] MAC_CMDLINE = { "open", URLTOKEN };

    // Used to identify the mac platform.
    private  final int OTHER_ID = -1;

    private  final String[][] OTHER_CMDLINES = {

        // The first guess for a browser under other systems (and unix):
        // Remote controlling firefox
        // (http://www.mozilla.org/unix/remote.html)
        { "firefox", "-remote", "openURL(" + URLTOKEN + ",new-window)" },

        //      Remote controlling mozilla
        // (http://www.mozilla.org/unix/remote.html)
        { "mozilla", "-remote", "openURL(" + URLTOKEN + ",new-window)" },
        // The second guess for a browser under other systems (and unix):
        // The RedHat skript htmlview
        { "htmlview", URLTOKEN },

        // The third guess for a browser under other systems (and unix):
        // Remote controlling netscape
        // (http://wp.netscape.com/newsref/std/x-remote.html)
        { "netscape", "-remote", "openURL(" + URLTOKEN + ")" }

    };

    private  final String[][] OTHER_FALLBACKS = {

        // Fallback for remote controlling mozilla:
        //Starting up a new mozilla
        { "firefox", URLTOKEN },

        // Starting up a new mozilla
        { "mozilla", URLTOKEN },

        // No fallback for htmlview
        null,

        // Fallback for remote controlling netscape:
        // Starting up a new netscape
        { "netscape", URLTOKEN }

    };


    private int getPlatform() {
        String os = System.getProperty("os.name");
        if (os != null && os.startsWith(WIN_PREFIX))
            return WIN_ID;
        if (os != null && os.startsWith(MAC_PREFIX))
            return MAC_ID;
        return OTHER_ID;
    }

    private String[] replaceToken(String[] target, String token,
                                  String replacement) {
        if (null == target)
            return null;
        String[] result = new String[target.length];

        for (int i = 0; i < target.length; i++)
            result[i] = target[i].replaceAll(token, replacement);

        return result;
    }

    private boolean runCmdLine(String[] cmdLine) {
        System.out.println("Try to execute commande line    = " + cmdLine);
        return runCmdLine(cmdLine, null);
    }

    private boolean runCmdLine(String[] cmdLine, String[] fallBackCmdLine) {
        try {
            System.out.println("Try to execute commande line    = " + cmdLine + " with " +fallBackCmdLine );
            /*
             * Util.debug( "Trying to invoke browser, cmd='" +
             * connectStringArray(cmdLine) + "' ... ");
             */
            Process p = Runtime.getRuntime().exec(cmdLine);

            if (null != fallBackCmdLine) {
                // wait for exit code -- if it's 0, command worked,
                // otherwise we need to start fallBackCmdLine.
                int exitCode = p.waitFor();
                if (exitCode != 0) {
                    /*
                     * Util.debug(exitCode); Util.debug();
                     */

                    /*
                     * Util.debug( "Trying to invoke browser, cmd='" +
                     * connectStringArray(fallBackCmdLine) + "' ...");
                     */
                    Runtime.getRuntime().exec(fallBackCmdLine);

                }
            }

            System.out.println();
            return true;

        } catch (InterruptedException e) {
            System.out.println("Caught: " + e);
        } catch (IOException e) {
            System.out.println("Caught: " + e);
        }

        return false;
    }
    /******************/

    /***************** Windows Listener ***************/
    @Override
    public void windowClosing(WindowEvent e) {
        quit(true, true);
    }

    @Override
    public void windowActivated(WindowEvent e) { }

    @Override
    public void windowClosed(WindowEvent e) { }


    @Override
    public void windowDeactivated(WindowEvent e) { }

    @Override
    public void windowDeiconified(WindowEvent e) { }

    @Override
    public void windowIconified(WindowEvent e) { }

    @Override
    public void windowOpened(WindowEvent e) { }



    public static void main(String[] args){
        SalomeProxy pSalomeTMF_Cmd = new SalomeProxy();
        pSalomeTMF_Cmd.connect("http://salome-tmf.rd.francetelecom.fr/www/salome-demo/salome_tmf/", "marchemi", "salome-demo");
        pSalomeTMF_Cmd.disconnect();
    }

}
