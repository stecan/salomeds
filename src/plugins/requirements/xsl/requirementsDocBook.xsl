<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
    Document   : requirements.xsl
    Created on : 9 mars 2007, 11:16
    Author     : vapu8214
    Description:
    Purpose of transformation follows.
-->

<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:my="http://rd.francetelecom.com/"
    xmlns:saxon="http://saxon.sf.net/"
    exclude-result-prefixes="saxon"
    extension-element-prefixes="saxon">
    <xsl:param name="requirements" />
    <xsl:template name="extRoot1">
    	<xsl:if test="$requirements = '1'">
    		<xsl:apply-templates select="Requirements" mode="Projet" />
    	</xsl:if>
    </xsl:template>
    <xsl:template match="Requirements" mode="Projet">
    	<section>
    		<title>
    			<xsl:variable name="fieldheader1"
    				select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
    			<xsl:value-of select="$fieldheader1" />
    		</title>
    		<section id="ReqRoot">
    			<title>
    				<xsl:variable name="fieldheader3"
    					select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
    				<xsl:value-of select="$fieldheader3" />
    				<emphasis>
              &#160;
              <xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
            </emphasis>
    			</title>
    			<xsl:choose>
    				<xsl:when test="Requirement">
    					<xsl:apply-templates mode="Projet"
    						select="Requirement" />
    				</xsl:when>
    				<xsl:otherwise>
    					<para />
    				</xsl:otherwise>
    			</xsl:choose>
    		</section>
    		<xsl:apply-templates mode="Projet" select="//RequirementFamily">
    			<xsl:sort select="count(ancestor::*)" data-type="number" order="ascending" />
    		</xsl:apply-templates>
    	</section>
    </xsl:template>
    <xsl:template match="Requirement" mode="Projet">
		<section id="{@id_req}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
          &#160;
          <xsl:value-of select="Nom/text()" />
        </emphasis>
			</title>
			<para>
				<variablelist>
					<xsl:apply-templates select="Description" mode="variablelist" />
					<varlistentry>
						<term>
							<emphasis>ID</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of select="substring-after(@id_req, 'Req_')" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@category" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@priority" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@complexity" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@criticity" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@understanding" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@owner" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of select="@state" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@checkedBy" />
							</para>
						</listitem>
					</varlistentry>
					<xsl:if test="@origine and @origine!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Origine']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@origine" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@reference and @reference!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Reference']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@reference" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@version and @version!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Version']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@version" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@verifway and @verifway!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Mode_de_verification']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@verifway" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
				</variablelist>
			</para>
			<xsl:apply-templates select="Attachements" mode="table">
			  <xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:variable name="idReq"><xsl:value-of select="@id_req" /></xsl:variable>
			<xsl:if test="//Test/LinkRequirement/RequirementRef[@ref=$idReq]">
				<informaltable>
					<tgroup cols="4">
						<colspec align="center" colwidth="1*" colname="col1" />
						<colspec colwidth="4*" colname="col2" />
						<colspec colwidth="4*" colname="col3" />
						<colspec colwidth="4*" colname="col4" />
						<thead>
						  <row>
						    <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
						    <entry namest="col1" nameend="col4">
						      <xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture']" />
                  <xsl:value-of select="$fieldheader7" />
						    </entry>
						  </row>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry align="center">N�</entry>
								<entry align="center">
									<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
									<xsl:value-of select="$fieldheader8" />
								</entry>
								<entry align="center">
									<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
									<xsl:value-of select="$fieldheader9" />
								</entry>
								<entry align="center">
									<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
									<xsl:value-of select="$fieldheader10" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:apply-templates select="//Test/LinkRequirement/RequirementRef[@ref=$idReq]" mode="Requirement" />
						</tbody>
					</tgroup>				
				</informaltable>
			</xsl:if>
		</section>		
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Requirement">
		<row>
			<entry align="center">
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="{../../../../../../@id_famille}">
					<xsl:value-of select="../../../../../../Nom/text()" />
				</link>
			</entry>
			<entry>
				<link linkend="{../../../../@id_suite}">
					<xsl:value-of select="../../../../Nom/text()" />
				</link>
			</entry>
			<entry>
				<link linkend="{../../@id_test}">
					<xsl:value-of select="../../Nom/text()" />
				</link>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Projet">
		<section id="{@id_req}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
          &#160;<xsl:value-of select="Nom/text()" />
         </emphasis>
			</title>		
			<para>
        <variablelist>
          <xsl:apply-templates select="Description" mode="variablelist" />
          <varlistentry>
            <term>
              <emphasis>
                <xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parent']" />
                <xsl:value-of select="$fieldheader3" />
              </emphasis>
            </term>
            <listitem>
              <para>
                <xsl:choose>
                  <xsl:when test="parent::RequirementFamily">
                    <link linkend="{parent::RequirementFamily/@id_req}">
                      <xsl:value-of select="parent::RequirementFamily/Nom/text()" />
                    </link>
                  </xsl:when>
                  <xsl:otherwise>
                    <link linkend="ReqRoot">
                      <xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
                    </link>
                  </xsl:otherwise>
                </xsl:choose>
              </para>
            </listitem>
          </varlistentry>
        </variablelist>
      </para>
			<xsl:apply-templates select="Attachements" mode="RequirementFamily">
				<xsl:with-param name="idReqFamilyParam">
					<xsl:value-of select="@id_req" />
				</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Requirement" mode="Projet" />
		</section>
	</xsl:template>
	<xsl:template match="Attachements" mode="RequirementFamily">
		<xsl:param name="idReqFamilyParam" />
		<section id="{$idReqFamilyParam}_Attachements">
			<title>
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader4" />
			</title>			
			<xsl:apply-templates select="." mode="table" />
			<xsl:apply-templates select="FileAttachement" mode="inclusion" />
		</section>
	</xsl:template>
	
	<xsl:template name="extTest">
		<xsl:apply-templates select="LinkRequirement" mode="Projet" />
	</xsl:template>
	<xsl:template match="LinkRequirement" mode="Projet">
		<informaltable>
      <tgroup cols="6">
        <colspec align="center" colwidth="1*" colname="col1" />
        <colspec colwidth="3*" colname="col2" />
        <colspec colwidth="3*" colname="col3" />
        <colspec colwidth="3*" colname="col4" />
        <colspec colwidth="3*" colname="col5" />
        <colspec colwidth="3*" colname="col6" />
        <thead>
          <row>
            <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <entry namest="col1" nameend="col6">
              <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
              <xsl:value-of select="$fieldheader1" />
            </entry>
          </row>
          <row>
            <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <entry align="center">ID</entry>
            <entry align="center">
              <xsl:variable name="fieldheader2"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
              <xsl:value-of select="$fieldheader2" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
          </row>
        </thead>
        <tbody>
          <xsl:apply-templates select="RequirementRef" mode="ProjetUn">
            <xsl:sort select="id(@ref)/@priority" data-type="number" order="descending" />
          </xsl:apply-templates>
        </tbody>
      </tgroup>
      <tgroup cols="6">
        <colspec align="center" colwidth="1*" colname="col1" />
        <colspec colwidth="3*" colname="col2" />
        <colspec colwidth="3*" colname="col3" />
        <colspec colwidth="3*" colname="col4" />
        <colspec colwidth="3*" colname="col5" />
        <colspec colwidth="3*" colname="col6" />
        <thead>
          <row>
            <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <entry namest="col1" nameend="col6">
              <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
              <xsl:value-of select="$fieldheader1" />
            </entry>
          </row>
          <row>
            <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <entry align="center">ID</entry>
            <entry align="center">
              <xsl:variable name="fieldheader2"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
              <xsl:value-of select="$fieldheader2" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
            <entry align="center">
              <xsl:variable name="fieldheader3"
                select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
              <xsl:value-of select="$fieldheader3" />
            </entry>
          </row>
        </thead>
        <tbody>
          <xsl:apply-templates select="RequirementRef" mode="ProjetDeux">
            <xsl:sort select="id(@ref)/@priority" data-type="number" order="descending" />
          </xsl:apply-templates>
        </tbody>
        </tgroup>
    </informaltable>
	</xsl:template>
	<xsl:template match="RequirementRef" mode="ProjetUn">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<row>
			<entry align="center">
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:variable name="category">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@category" />
				</xsl:variable>
				<xsl:value-of select="$category" />
			</entry>
			<entry>
				<xsl:variable name="priority">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@priority" />
				</xsl:variable>
				<xsl:value-of select="$priority" />
			</entry>
			<entry>
				<xsl:variable name="complexity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@complexity" />
				</xsl:variable>
				<xsl:value-of select="$complexity" />
			</entry>
				<entry>
				<xsl:variable name="criticity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@criticity" />
				</xsl:variable>
				<xsl:value-of select="$criticity" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="RequirementRef" mode="ProjetDeux">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<row>
			<entry align="center">
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:variable name="understanding">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@understanding" />
				</xsl:variable>
				<xsl:value-of select="$understanding" />
			</entry>
			<entry>
				<xsl:variable name="owner">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@owner" />
				</xsl:variable>
				<xsl:value-of select="$owner" />
			</entry>
			<entry>
				<xsl:variable name="state">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@state" />
				</xsl:variable>
				<xsl:value-of select="$state" />
			</entry>
				<entry>
				<xsl:variable name="checkedBy">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@checkedBy" />
				</xsl:variable>
				<xsl:value-of select="$checkedBy" />
			</entry>
		</row>
	</xsl:template>
</xsl:stylesheet>    