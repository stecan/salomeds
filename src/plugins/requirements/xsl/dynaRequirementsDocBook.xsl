<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
	Document   : dynaRequirements.xsl
	Created on : 20 mars 2007, 09:19
	Author     : vapu8214
	Description:
	Purpose of transformation follows.
-->

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	<xsl:param name="requirements" />

	<!-- TODO customize transformation rules
		syntax recommendation http://www.w3.org/TR/xslt
	-->

	<xsl:variable name="existRequirement">
		<xsl:value-of select="boolean(//Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref])" />
	</xsl:variable>
	<xsl:template name="extRoot1">
		<xsl:if test="$requirements = '1'">
			<xsl:apply-templates select="Requirements" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirements" mode="Projet">
		<xsl:if test="$existRequirement = 'true'">
			<section>
				<title>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
					<xsl:value-of select="$fieldheader1" />
				</title>
				<section id="ReqRoot">
					<title>
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
						<xsl:value-of select="$fieldheader3" />
						<emphasis>
							&#160;
							<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
						</emphasis>
					</title>
					<xsl:choose>
						<xsl:when test="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]">
							<xsl:apply-templates mode="Projet" select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" />
						</xsl:when>
						<xsl:otherwise>
							<para />
						</xsl:otherwise>
					</xsl:choose>
				</section>
				<xsl:apply-templates mode="Projet" select=".//RequirementFamily[.//Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]]">
					<xsl:sort select="count(ancestor::*)" data-type="number" order="ascending" />
				</xsl:apply-templates>
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="Projet">
		<section id="{@id_req}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<para>
				<variablelist>
					<xsl:apply-templates select="Description"
						mode="variablelist" />
					<varlistentry>
						<term>
							<emphasis>ID</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="substring-after(@id_req, 'Req_')" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@category" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@priority" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@complexity" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@criticity" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@understanding" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@owner" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of select="@state" />
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of
									select="@checkedBy" />
							</para>
						</listitem>
					</varlistentry>
					<xsl:if test="@origine and @origine!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Origine']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@origine" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@reference and @reference!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Reference']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@reference" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@version and @version!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Version']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@version" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:if test="@verifway and @verifway!=''">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Mode_de_verification']" />
									<xsl:value-of
										select="$fieldheader3" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of select="@verifway" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
				</variablelist>
			</para>
			<xsl:if test="Attachements">
				<xsl:apply-templates select="Attachements" mode="Projet">
					<xsl:with-param name="caption">true</xsl:with-param>
				</xsl:apply-templates>
				<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			</xsl:if>
			<!--  Couverture -->
			<xsl:variable name="idReq">
				<xsl:value-of select="@id_req" />
			</xsl:variable>
			<xsl:variable name="testLinkRequirement">
				<xsl:value-of select="boolean(//Test/LinkRequirement/RequirementRef[@ref=$idReq])" />
			</xsl:variable>
			<xsl:if test="$testLinkRequirement = 'true'">
			  <para>
			  	<informaltable>
			  		<tgroup cols="4">
			  			<colspec align="center" colwidth="1*" colname="col1" />
			  			<colspec colwidth="4*" colname="col2" />
			  			<colspec colwidth="4*" colname="col3" />
			  			<colspec colwidth="4*" colname="col4" />
			  			<thead>
			  				<row>
			  				  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
			  					<entry namest="col1" nameend="col4">
			  						<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture_de_test']" />
			  						<xsl:value-of select="$fieldheader7" />
			  					</entry>
			  				</row>
			  				<row>
			  				  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
			  					<entry>N�</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
			  						<xsl:value-of select="$fieldheader8" />
			  					</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
			  						<xsl:value-of select="$fieldheader9" />
			  					</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
			  						<xsl:value-of select="$fieldheader10" />
			  					</entry>
			  				</row>
			  			</thead>
			  			<tbody>
			  				<xsl:for-each select="//Test/LinkRequirement/RequirementRef[@ref=$idReq]">
			  					<row>
			  						<entry>
			  							<xsl:number value="position()" format="1" />
			  						</entry>
			  						<entry>
			  						  <xsl:variable name="id_famille" select="../../../../../../@id_famille" />
                      <xsl:choose>
                        <xsl:when test="//CampagneTests//FamilleRef[@ref=$id_famille]">
                          <link linkend="{$id_famille}">
                            <xsl:value-of select="../../../../../../Nom/text()" />
                          </link>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="../../../../../../Nom/text()" />
                        </xsl:otherwise>
                      </xsl:choose>
			  						</entry>
			  						<entry>
			  						  <xsl:variable name="id_suite" select="../../../../@id_suite" />
                      <xsl:choose>
                        <xsl:when test="//CampagneTests//SuiteTestRef[@ref=$id_suite]">
                          <link linkend="{$id_suite}">
                            <xsl:value-of select="../../../../Nom/text()" />
                          </link>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="../../../../Nom/text()" />
                        </xsl:otherwise>
                      </xsl:choose>
			  						</entry>
			  						<entry>
			  						  <xsl:variable name="id_test" select="../../@id_test" />
			  						  <xsl:choose>
			  						    <xsl:when test="//CampagneTests//TestRef[@ref=$id_test]">
			  						      <link linkend="{$id_test}">
                            <xsl:value-of select="../../Nom/text()" />
                          </link>
			  						    </xsl:when>
			  						    <xsl:otherwise>
			  						      <xsl:value-of select="../../Nom/text()" />
			  						    </xsl:otherwise>
			  						  </xsl:choose>
			  						</entry>
			  					</row>
			  				</xsl:for-each>
			  			</tbody>
			  		</tgroup>
			  	</informaltable>
			  </para>
			</xsl:if>
			<!--campagne de tests-->
			<xsl:if test="//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]/ExecCampTests/ExecCampTest">
			  <para>
			  	<informaltable>
			  		<tgroup cols="5">
			  			<colspec align="center" colwidth="1*" colname="col1" />
			  			<colspec colwidth="4*" colname="col2" />
			  			<colspec colwidth="4*" colname="col3" />
			  			<colspec colwidth="4*" colname="col4" />
			  			<colspec colwidth="4*" colname="col5" />
			  			<thead>
			  				<row>
			  				  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
			  					<entry namest="col1" nameend="col5">
			  						<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture_d_execution']" />
			  						<xsl:value-of select="$fieldheader11" />
			  						&#160;(
			  						<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pourcentage_de_couverture']" />
			  						<xsl:value-of select="$fieldheader12" />
			  						:
			  						<xsl:variable name="nbTestCamp">
			  							<xsl:value-of select="count(//Test[@id_test=//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]//TestRef/@ref and LinkRequirement/RequirementRef/@ref=$idReq])" />
			  						</xsl:variable>
			  						<xsl:variable name="nbTestReq">
			  							<xsl:value-of select="count(//Test[LinkRequirement/RequirementRef/@ref=$idReq])" />
			  						</xsl:variable>
			  						<xsl:variable name="couverture">
			  							<xsl:value-of select="$nbTestCamp div $nbTestReq" />
			  						</xsl:variable>
			  						<xsl:value-of select="format-number($couverture, '###.##%')" />
			  						)
			  					</entry>
			  				</row>
			  				<row>
			  				  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
			  					<entry>N�</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
			  						<xsl:value-of select="$fieldheader13" />
			  					</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader14" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
			  						<xsl:value-of select="$fieldheader14" />
			  					</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader15" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
			  						<xsl:value-of select="$fieldheader15" />
			  					</entry>
			  					<entry>
			  						<xsl:variable name="fieldheader16" select="$translations/allheadings/headings[lang($local)]/heading[@category='Tests_associes']" />
			  						<xsl:value-of select="$fieldheader16" />
			  					</entry>
			  				</row>
			  			</thead>
			  			<tbody>
			  				<xsl:for-each select="//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]/ExecCampTests/ExecCampTest">
			  					<xsl:variable name="id_env">
			  						<xsl:value-of select="EnvironnementEx/@ref" />
			  					</xsl:variable>
			  					<row>
			  						<entry>
			  							<xsl:number value="position()" format="1" />
			  						</entry>
			  						<entry>
			  							<link linkend="{../../@id_camp}">
			  								<xsl:value-of select="../../Nom/text()" />
			  							</link>
			  						</entry>
			  						<entry>
			  							<link linkend="{EnvironnementEx/@ref}">
			  								<xsl:value-of select="//Environnement[@idEnv=$id_env]/Nom/text()" />
			  							</link>
			  						</entry>
			  						<entry>
			  							<link linkend="{@id_exec_camp}">
			  								<xsl:value-of select="Nom/text()" />
			  							</link>
			  						</entry>
			  						<entry>
			  							<xsl:variable name="nodes" select="id(ancestor::CampagneTest/FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef/@ref)[LinkRequirement/RequirementRef/@ref=$idReq]" />
			  							<xsl:choose>
			  								<xsl:when test="count($nodes)>1">
			  									<simplelist>
			  										<xsl:for-each select="$nodes">
			  											<member>
			  												<link linkend="{@id_test}">
			  													<xsl:value-of select="Nom/text()" />
			  												</link>
			  											</member>
			  										</xsl:for-each>
			  									</simplelist>
			  								</xsl:when>
			  								<xsl:when test="count($nodes)=1">
			  									<link linkend="{$nodes/@id_test}">
			  										<xsl:value-of select="$nodes/Nom/text()" />
			  									</link>
			  								</xsl:when>
			  							</xsl:choose>
			  						</entry>
			  					</row>
			  				</xsl:for-each>
			  			</tbody>
			  		</tgroup>
			  	</informaltable>
			  </para>
			</xsl:if>
		</section>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Projet">
		<section id="{@id_req}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
				  &#160;<xsl:value-of select="Nom/text()" />
				 </emphasis>
			</title>
			<para>
				<variablelist>
				  <xsl:apply-templates select="Description" mode="variablelist" />
          <varlistentry>
            <term>
              <emphasis>
                <xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parent']" />
                <xsl:value-of select="$fieldheader3" />
              </emphasis>
            </term>
            <listitem>
              <para>
              	<xsl:choose>
              		<xsl:when test="parent::RequirementFamily">
              			<link linkend="{parent::RequirementFamily/@id_req}">
              				<xsl:value-of select="parent::RequirementFamily/Nom/text()" />
              			</link>
              		</xsl:when>
              		<xsl:otherwise>
              			<link linkend="ReqRoot">
              				<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
              			</link>
              		</xsl:otherwise>
              	</xsl:choose>
              </para>
            </listitem>
          </varlistentry>
				</variablelist>
			</para>
			<xsl:if test="Attachements">
				<section id="{@id_req}_Attachements">
					<title>
						<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
						<xsl:value-of select="$fieldheader4" />
					</title>
					<xsl:apply-templates select="Attachements" mode="Projet" />
					<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
				</section>
			</xsl:if>
			<xsl:apply-templates select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" mode="Projet" />
		</section>
	</xsl:template>
	
	<xsl:template name="extTest">
		<xsl:apply-templates select="LinkRequirement" mode="Projet">
			<xsl:with-param name="camp">false</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="LinkRequirement" mode="Projet">
		<xsl:param name="camp" />
		<informaltable>
			<tgroup cols="6">
				<colspec align="center" colwidth="1*" colname="col1" />
				<colspec colwidth="3*" colname="col2" />
				<colspec colwidth="3*" colname="col3" />
				<colspec colwidth="3*" colname="col4" />
				<colspec colwidth="3*" colname="col5" />
				<colspec colwidth="3*" colname="col6" />
				<thead>
				  <row>
				    <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
				    <entry namest="col1" nameend="col6">
				  	  <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
				  	  <xsl:value-of select="$fieldheader1" />
				  	  <xsl:if test="$camp = 'true'">
  				  		&#160;(
				  		  <xsl:variable name="fieldheader0" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pourcentage_de_couverture']" />
				  		  <xsl:value-of select="$fieldheader0" />
				  		  :
				  		  <xsl:variable name="couverture">
  				  			<xsl:value-of select="count(RequirementRef) div count(//Requirement)" />
				  		  </xsl:variable>
				  		  <xsl:value-of select="format-number($couverture, '###.##%')" />
				  		  )
				  	  </xsl:if>
				  	</entry>
				  </row>
				  <row>
				    <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
						<entry>ID</entry>
						<entry>
							<xsl:variable name="fieldheader2"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
							<xsl:value-of select="$fieldheader2" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
					</row>
				</thead>
				<tbody>
					<xsl:apply-templates select="RequirementRef" mode="Premier">
						<xsl:sort select="id(@ref)/@priority" data-type="number" order="descending" />
					</xsl:apply-templates>
				</tbody>
			</tgroup>
			<tgroup cols="6">
				<colspec align="center" colwidth="1*" colname="col1" />
				<colspec colwidth="3*" colname="col2" />
				<colspec colwidth="3*" colname="col3" />
				<colspec colwidth="3*" colname="col4" />
				<colspec colwidth="3*" colname="col5" />
				<colspec colwidth="3*" colname="col6" />
				<thead>
				  <row>
				    <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
				    <entry namest="col1" nameend="col6">
				  	  <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
				  	  <xsl:value-of select="$fieldheader1" />
				  	  <xsl:if test="$camp = 'true'">
  				  		&#160;(
				  		  <xsl:variable name="fieldheader0" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pourcentage_de_couverture']" />
				  		  <xsl:value-of select="$fieldheader0" />
				  		  :
				  		  <xsl:variable name="couverture">
  				  			<xsl:value-of select="count(RequirementRef) div count(//Requirement)" />
				  		  </xsl:variable>
				  		  <xsl:value-of select="format-number($couverture, '###.##%')" />
				  		  )
				  	  </xsl:if>
				  	</entry>
				  </row>
				  <row>
				    <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
						<entry>ID</entry>
						<entry>
							<xsl:variable name="fieldheader2"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
							<xsl:value-of select="$fieldheader2" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
						<entry>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
							<xsl:value-of select="$fieldheader3" />
						</entry>
					</row>
				</thead>
				<tbody>
					<xsl:apply-templates select="RequirementRef" mode="Second">
						<xsl:sort select="id(@ref)/@priority" data-type="number" order="descending" />
					</xsl:apply-templates>
				</tbody>
			</tgroup>
		</informaltable>
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Premier">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<row>
			<entry>
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:variable name="category">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@category" />
				</xsl:variable>
				<xsl:value-of select="$category" />
			</entry>
			<entry>
				<xsl:variable name="priority">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@priority" />
				</xsl:variable>
				<xsl:value-of select="$priority" />
			</entry>
			<entry>
				<xsl:variable name="complexity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@complexity" />
				</xsl:variable>
				<xsl:value-of select="$complexity" />
			</entry>
				<entry>
				<xsl:variable name="criticity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@criticity" />
				</xsl:variable>
				<xsl:value-of select="$criticity" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Second">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<row>
			<entry>
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:variable name="understanding">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@understanding" />
				</xsl:variable>
				<xsl:value-of select="$understanding" />
			</entry>
			<entry>
				<xsl:variable name="owner">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@owner" />
				</xsl:variable>
				<xsl:value-of select="$owner" />
			</entry>
			<entry>
				<xsl:variable name="state">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@state" />
				</xsl:variable>
				<xsl:value-of select="$state" />
			</entry>
				<entry>
				<xsl:variable name="checkedBy">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@checkedBy" />
				</xsl:variable>
				<xsl:value-of select="$checkedBy" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template name="extCampaign1">
		<xsl:apply-templates select="LinkRequirement" mode="Projet">
			<xsl:with-param name="camp">true</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)"
			mode="requirements" />
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="requirements">
		<xsl:variable name="lien_test">
			<xsl:value-of select="concat('testresult_', @id_exec_res)" />
		</xsl:variable>
		<xsl:variable name="idExecRes">
			<xsl:value-of select="@id_exec_res" />
		</xsl:variable>
		<xsl:variable name="exigence">
			<xsl:value-of
				select="boolean(ancestor::CampagneTest/LinkRequirement/RequirementRef[@ref=//Test[@id_test=//ResulExecCampTest[@id_exec_res=$idExecRes]/ResulExecs/ResulExec[@res='PASSED' or @res='FAILED' or @res='INCONCLUSIF']/@refTest]/LinkRequirement/RequirementRef/@ref])" />
		</xsl:variable>
		<xsl:if test="$exigence = 'true'">
		  <para>
		  	<informaltable>
		  		<tgroup cols="4">
		  			<colspec align="center" colwidth="1*" colname="col1" />
		  			<colspec colwidth="4*" colname="col2" />
		  			<colspec colwidth="4*" colname="col3" />
		  			<colspec colwidth="4*" colname="col4" />
		  			<thead>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
		  					<entry namest="col1" nameend="col4">
		  						<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_satisfaites']" />
		  						<xsl:value-of select="$fieldheader8" />
		  					</entry>
		  				</row>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
		  					<entry>ID</entry>
		  					<entry>
		  						<xsl:value-of select="$translatedName" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Satisfaction']" />
		  						<xsl:value-of select="$fieldheader9" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
		  						<xsl:value-of select="$fieldheader10" />
		  					</entry>
		  				</row>
		  			</thead>
		  			<tbody>
		  				<xsl:apply-templates select="id(ancestor::CampagneTest/LinkRequirement/RequirementRef/@ref)[@id_req=//Test[@id_test=//ResulExecCampTest[@id_exec_res=$idExecRes]/ResulExecs/ResulExec[@res='PASSED' or @res='FAILED' or @res='INCONCLUSIF']/@refTest]/LinkRequirement/RequirementRef/@ref]" mode="ResulExecCampTest">
		  					<xsl:with-param name="idExecRes">
		  						<xsl:value-of select="$idExecRes" />
		  					</xsl:with-param>
		  					<xsl:with-param name="lien_test">
		  						<xsl:value-of select="$lien_test" />
		  					</xsl:with-param>
		  				</xsl:apply-templates>
		  			</tbody>
		  		</tgroup>
		  	</informaltable>
		  </para>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="ResulExecCampTest">
		<xsl:param name="idExecRes" />
		<xsl:param name="lien_test" />
		<xsl:variable name="idReq">
			<xsl:value-of select="@id_req" />
		</xsl:variable>
		<row>
			<entry>
				<xsl:value-of select="substring-after($idReq, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@id_req}">
					<xsl:value-of select="./Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:call-template name="verdictRequirement">
					<xsl:with-param name="id_req">
						<xsl:value-of select="$idReq" />
					</xsl:with-param>
					<xsl:with-param name="id_resulExec">
						<xsl:value-of select="$idExecRes" />
					</xsl:with-param>
				</xsl:call-template>
			</entry>
			<entry>
			  <xsl:variable name="nodes" select="//Test[LinkRequirement/RequirementRef/@ref=$idReq and @id_test=id($idExecRes)/ResulExecs/ResulExec/@refTest]" />
        <xsl:choose>
          <xsl:when test="count($nodes)>1">
            <simplelist>
              <xsl:for-each select="$nodes">
                <member>
                  <xsl:value-of select="Nom/text()" />
                </member>
              </xsl:for-each>
            </simplelist>
          </xsl:when>
          <xsl:when test="count($nodes)=1">
            <xsl:value-of select="$nodes/Nom/text()" />
          </xsl:when>
        </xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)"
			mode="requirements" />
	</xsl:template>
	<xsl:template match="ResulExec" mode="requirements">
		<xsl:variable name="idTest">
			<xsl:value-of select="@refTest" />
		</xsl:variable>
		<xsl:variable name="exigenceTest">
			<xsl:value-of
				select="boolean(ancestor::CampagneTest/LinkRequirement/RequirementRef[@ref=//Test[@id_test=$idTest]/LinkRequirement/RequirementRef/@ref])" />
		</xsl:variable>
		<xsl:if test="$exigenceTest = 'true'">
		  <para>
		  	<informaltable>
		  		<tgroup cols="6">
		  			<colspec align="center" colwidth="1*" colname="col1" />
		  			<colspec colwidth="3*" colname="col2" />
		  			<colspec colwidth="3*" colname="col3" />
		  			<colspec colwidth="3*" colname="col4" />
		  			<colspec colwidth="3*" colname="col5" />
		  			<colspec colwidth="3*" colname="col6" />
		  			<thead>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
		  				  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
		  					<entry namest="col1" nameend="col6">
		  						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
		  						<xsl:value-of select="$fieldheader10" />
		  					</entry>
		  				</row>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
		  					<entry>ID</entry>
		  					<entry>
		  						<xsl:value-of select="$translatedName" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  				</row>
		  			</thead>
		  			<tbody>
		  				<xsl:apply-templates select="id(ancestor::CampagneTest/LinkRequirement/RequirementRef/@ref)[@id_req=//Test[@id_test=$idTest]/LinkRequirement/RequirementRef/@ref]" mode="detailExecPremier">
		  					<xsl:sort select="@priority" data-type="number" order="descending" />
		  				</xsl:apply-templates>
		  			</tbody>
		  		</tgroup>
		  		<tgroup cols="6">
		  			<colspec align="center" colwidth="1*" colname="col1" />
		  			<colspec colwidth="3*" colname="col2" />
		  			<colspec colwidth="3*" colname="col3" />
		  			<colspec colwidth="3*" colname="col4" />
		  			<colspec colwidth="3*" colname="col5" />
		  			<colspec colwidth="3*" colname="col6" />
		  			<thead>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
		  				  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
		  					<entry namest="col1" nameend="col6">
		  						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
		  						<xsl:value-of select="$fieldheader10" />
		  					</entry>
		  				</row>
		  				<row>
		  				  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
		  					<entry>ID</entry>
		  					<entry>
		  						<xsl:value-of select="$translatedName" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  					<entry>
		  						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
		  						<xsl:value-of select="$fieldheader3" />
		  					</entry>
		  				</row>
		  			</thead>
		  			<tbody>
		  				<xsl:apply-templates select="id(ancestor::CampagneTest/LinkRequirement/RequirementRef/@ref)[@id_req=//Test[@id_test=$idTest]/LinkRequirement/RequirementRef/@ref]" mode="detailExecSecond">
		  					<xsl:sort select="@priority" data-type="number" order="descending" />
		  				</xsl:apply-templates>
		  			</tbody>
		  		</tgroup>
		  	</informaltable>
		  </para>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="detailExecPremier">
		<row>
			<entry>
				<xsl:value-of select="substring-after(@id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@id_req}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:value-of select="@category" />
			</entry>
			<entry>
				<xsl:value-of select="@priority" />
			</entry>
			<entry>
				<xsl:value-of select="@complexity" />
			</entry>
			<entry>
				<xsl:value-of select="@criticity" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="Requirement" mode="detailExecSecond">
		<row>
			<entry>
				<xsl:value-of select="substring-after(@id_req, 'Req_')" />
			</entry>
			<entry>
				<link linkend="{@id_req}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:value-of select="@understanding" />
			</entry>
			<entry>
				<xsl:value-of select="@owner" />
			</entry>
			<entry>
				<xsl:value-of select="@state" />
			</entry>
			<entry>
				<xsl:value-of select="@checkedBy" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template name="verdictRequirement">
		<xsl:param name="id_req" />
		<xsl:param name="id_resulExec" />
		<xsl:choose>
			<xsl:when
				test="//ResulExecCampTest[@id_exec_res=$id_resulExec]/ResulExecs/ResulExec[@res='FAILED' and @refTest=//Test[LinkRequirement/RequirementRef/@ref=$id_req]/@id_test]">
				Fail
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when
						test="//ResulExecCampTest[@id_exec_res=$id_resulExec]/ResulExecs/ResulExec[@res='INCONCLUSIF' and @refTest=//Test[LinkRequirement/RequirementRef/@ref=$id_req]/@id_test]">
						<xsl:variable name="fieldheader1"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
						<xsl:value-of select="$fieldheader1" />
					</xsl:when>
					<xsl:otherwise>Pass</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>