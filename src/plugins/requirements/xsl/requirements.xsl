<?xml version="1.0" encoding="ISO-8859-1" ?>

	<!--
		Document : requirements.xsl Created on : 9 mars 2007, 11:16 Author :
		vapu8214 Description: Purpose of transformation follows.
	-->

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	<xsl:param name="requirements" />

	<!--
		TODO customize transformation rules syntax recommendation
		http://www.w3.org/TR/xslt
	-->
	<xsl:template name="extSummaryRoot1">
		<xsl:if test="$requirements = '1'">
			<xsl:apply-templates mode="Sommaire" select="Requirements" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirements" mode="Sommaire">
		<li>
			<a href="#Req">
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />
				&#160;
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
				<xsl:value-of select="$fieldheader1" />
			</a>
			<ol>
				<li>
					<a href="#ReqRoot">
						<saxon:assign name="h2" select="my:accu($h2)" />
						<xsl:value-of select="$h1" />
						.
						<xsl:value-of select="$h2" />
						&#160;
						<xsl:variable name="fieldheader2"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
						<xsl:value-of select="$fieldheader2" />
						&#160;:&#160;
						<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
					</a>
					<xsl:if test="Requirement">
						<ol>
							<xsl:apply-templates mode="Sommaire" select="Requirement" />
							<saxon:assign name="h3" select="0" />
						</ol>
					</xsl:if>
				</li>
				<xsl:apply-templates mode="Sommaire" select="//RequirementFamily">
					<xsl:sort select="count(ancestor::*)" data-type="number"
						order="ascending" />
				</xsl:apply-templates>
				<saxon:assign name="h2" select="0" />
			</ol>
		</li>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Sommaire">
		<li>
			<a href="#{@id_req}">
				<saxon:assign name="h2" select="my:accu($h2)" />
				<xsl:value-of select="$h1" />
				.
				<xsl:value-of select="$h2" />
				&#160;
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader1" />
				&#160;:&#160;
				<xsl:value-of select="Nom/text()" />
			</a>
			<xsl:if test="Attachements|Requirement">
				<ol>
					<xsl:if test="Attachements">
						<li>
							<a href="#{@id_req}_Attachements">
								<saxon:assign name="h3" select="my:accu($h3)" />
								<xsl:value-of select="$h1" />
								.
								<xsl:value-of select="$h2" />
								.
								<xsl:value-of select="$h3" />
								&#160;
								<xsl:variable name="fieldheader2"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
								<xsl:value-of select="$fieldheader2" />
							</a>
						</li>
					</xsl:if>
					<xsl:apply-templates mode="Sommaire" select="Requirement" />
					<saxon:assign name="h3" select="0" />
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="Requirement" mode="Sommaire">
		<li>
			<a href="#{@id_req}">
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />
				.
				<xsl:value-of select="$h2" />
				.
				<xsl:value-of select="$h3" />
				&#160;
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
				<xsl:value-of select="$fieldheader1" />
				&#160;:&#160;
				<xsl:value-of select="Nom/text()" />
			</a>
		</li>
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:if test="$requirements = '1'">
			<xsl:apply-templates select="Requirements" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirements" mode="Projet">
		<br />
		<hr />
		<h1>
			<a name="Req" />
			<saxon:assign name="h1" select="my:accu($h1)" />
			<xsl:value-of select="$h1" />
			&#160;
			<xsl:variable name="fieldheader1"
				select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
			<xsl:value-of select="$fieldheader1" />
			<span>
				<xsl:value-of select="$h1" />
				&#160;
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
				<xsl:value-of select="$fieldheader1" />
			</span>
		</h1>
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<h2>
			<a name="ReqRoot" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />
			.
			<xsl:value-of select="$h2" />
			&#160;
			<xsl:variable name="fieldheader3"
				select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
			<xsl:value-of select="$fieldheader3" />
			&#160;:&#160;
			<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
		</h2>
		<xsl:apply-templates mode="Projet" select="Requirement" />
		<xsl:apply-templates mode="Projet" select="//RequirementFamily">
			<xsl:sort select="count(ancestor::*)" data-type="number"
				order="ascending" />
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="Requirement" mode="Projet">
		<h3>
			<a name="{@id_req}" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />
			.
			<xsl:value-of select="$h2" />
			.
			<xsl:value-of select="$h3" />
			&#160;
			<xsl:variable name="fieldheader1"
				select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<p>
			<xsl:apply-templates select="Description" mode="Projet">
				<xsl:with-param name="withBr">
					true
				</xsl:with-param>
			</xsl:apply-templates>
			<b>ID :&#160;</b>
			<xsl:value-of select="substring-after(@id_req, 'Req_')" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@category" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@priority" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@complexity" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@criticity" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@understanding" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@owner" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@state" />
			<br />
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@checkedBy" />
			<xsl:if test="@origine and @origine!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Origine']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@origine" />
			</xsl:if>
			<xsl:if test="@reference and @reference!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Reference']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@reference" />
			</xsl:if>
			<xsl:if test="@version and @version!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Version']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@version" />
			</xsl:if>
			<xsl:if test="@verifway and @verifway!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Mode_de_verification']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@verifway" />
			</xsl:if>
		</p>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">
				true
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement"
			mode="inclusion" />
		<xsl:variable name="idReq">
			<xsl:value-of select="@id_req" />
		</xsl:variable>
		<xsl:if test="//Test/LinkRequirement/RequirementRef[@ref=$idReq]">
			<xsl:if test="Attachements">
				<br />
			</xsl:if>
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader7"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture']" />
					<xsl:value-of select="$fieldheader7" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader8"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
						<xsl:value-of select="$fieldheader8" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader9"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
						<xsl:value-of select="$fieldheader9" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader10"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
						<xsl:value-of select="$fieldheader10" />
					</th>
				</tr>
				<xsl:apply-templates
					select="//Test/LinkRequirement/RequirementRef[@ref=$idReq]" mode="Requirement" />
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Requirement">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{../../../../../../@id_famille}">
					<xsl:value-of select="../../../../../../Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<a href="#{../../../../@id_suite}">
					<xsl:value-of select="../../../../Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<a href="#{../../@id_test}">
					<xsl:value-of select="../../Nom/text()" />
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Projet">
		<h2>
			<a name="{@id_req}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />
			.
			<xsl:value-of select="$h2" />
			&#160;
			<xsl:variable name="fieldheader1"
				select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<p>
			<xsl:apply-templates select="Description" mode="Projet">
				<xsl:with-param name="withBr">
					true
				</xsl:with-param>
			</xsl:apply-templates>
			<b>
				<xsl:variable name="fieldheader3"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Parent']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="parent::RequirementFamily">
					<a href="#{parent::RequirementFamily/@id_req}">
						<xsl:value-of select="parent::RequirementFamily/Nom/text()" />
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a href="#ReqRoot">
						<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</p>
		<xsl:apply-templates select="Attachements" mode="RequirementFamily">
			<xsl:with-param name="idReqFamilyParam">
				<xsl:value-of select="@id_req" />
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Requirement" mode="Projet" />
	</xsl:template>
	<xsl:template match="Attachements" mode="RequirementFamily">
		<xsl:param name="idReqFamilyParam" />
		<h3>
			<a name="{$idReqFamilyParam}_Attachements" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />
			.
			<xsl:value-of select="$h2" />
			.
			<xsl:value-of select="$h3" />
			&#160;
			<xsl:variable name="fieldheader4"
				select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
			<xsl:value-of select="$fieldheader4" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="." mode="Projet" />
		<xsl:apply-templates select="FileAttachement"
			mode="inclusion" />
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:apply-templates select="LinkRequirement"
			mode="Projet" />
	</xsl:template>
	<xsl:template match="LinkRequirement" mode="Projet">
		<table class="tab">
			<caption>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
				<xsl:value-of select="$fieldheader1" />
			</caption>
			<tr>
				<th class="titre">ID</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader2"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader2" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
			</tr>
			<xsl:apply-templates select="RequirementRef"
				mode="Projet">
				<xsl:sort select="id(@ref)/@priority" data-type="number"
					order="descending" />
			</xsl:apply-templates>
		</table>
		<br />
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Projet">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</td>
			<td class="tab">
				<a href="#{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:variable name="category">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@category" />
				</xsl:variable>
				<xsl:value-of select="$category" />
			</td>
			<td class="tab">
				<xsl:variable name="priority">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@priority" />
				</xsl:variable>
				<xsl:value-of select="$priority" />
			</td>
			<td class="tab">
				<xsl:variable name="complexity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@complexity" />
				</xsl:variable>
				<xsl:value-of select="$complexity" />
			</td>
			<td class="tab">
				<xsl:variable name="criticity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@criticity" />
				</xsl:variable>
				<xsl:value-of select="$criticity" />
			</td>
			<td class="tab">
				<xsl:variable name="understanding">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@understanding" />
				</xsl:variable>
				<xsl:value-of select="$understanding" />
			</td>
			<td class="tab">
				<xsl:variable name="owner">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@owner" />
				</xsl:variable>
				<xsl:value-of select="$owner" />
			</td>
			<td class="tab">
				<xsl:variable name="state">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@state" />
				</xsl:variable>
				<xsl:value-of select="$state" />
			</td>
			<td class="tab">
				<xsl:variable name="checkedBy">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@checkedBy" />
				</xsl:variable>
				<xsl:value-of select="$checkedBy" />
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>
