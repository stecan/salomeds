<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
    Document   : dynaRequirements.xsl
    Created on : 22 mars 2007, 11:40
    Authors    : PENAULT Aurore, LEYRIE Jean-Michel
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	<xsl:param name="requirements" />

	<!-- TODO customize transformation rules
       syntax recommendation http://www.w3.org/TR/xslt
  -->
  <xsl:template name="extSummaryRoot1">
		<xsl:if test="$requirements = '1'">
	   	<xsl:apply-templates mode="Sommaire" select="Requirements" />
		</xsl:if>
	</xsl:template>
  <xsl:variable name="existRequirement">
		<xsl:value-of select="boolean(//Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref])" />
	</xsl:variable>
	<xsl:template match="Requirements" mode="Sommaire">
		<xsl:if test="$existRequirement = 'true'">
			<li class="rootDirectory">
			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('Req', this)"/>
				<a href="{$frame2}#Req" target="princ">
				  <span class="directoryNum">
					  <saxon:assign name="h1" select="my:accu($h1)" />
					  <xsl:value-of select="$h1" />&#160;
					</span>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
					<xsl:value-of select="$fieldheader1" />
				</a>
				<div id="Req" style="display: none;">
				  <ol>
					  <li class="directory">
					    <saxon:assign name="h2" select="my:accu($h2)" />
					    <xsl:variable name="reqRootContent">
					      <xsl:apply-templates mode="Sommaire" select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" />
					    </xsl:variable>
					    <xsl:choose>
					      <xsl:when test="count($reqRootContent//*)>0">
					        <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('ReqRoot', this)"/>
					      </xsl:when>
					      <xsl:otherwise>
					        <img class="directory" src="ftv2node.png" />
					      </xsl:otherwise>
					    </xsl:choose>
              <a href="{$frame2}#ReqRoot" target="princ">
                <span class="directoryNum">
                  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
                  <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
                  <xsl:value-of select="$fieldheader2" />
                  &#160;:&#160;
                </span>
                <xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
              </a>      
              <xsl:if test="count($reqRootContent//*)>0">
                <div id="ReqRoot" style="display: none;">
                  <ol>
                    <xsl:copy-of select="$reqRootContent" />
                  </ol>
                </div>
                <saxon:assign name="h3" select="0" />
              </xsl:if>
					  </li>
					  <xsl:apply-templates mode="Sommaire" select=".//RequirementFamily[.//Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]]">
  						<xsl:sort select="count(ancestor::*)" data-type="number" order="ascending"/>
					  </xsl:apply-templates>
				  </ol>
				</div>
				<saxon:assign name="h2" select="0" />
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Sommaire">
		<li class="directory">
      <saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:variable name="reqFamilyContent">
          <xsl:if test="Attachements">
            <li class="directory">
              <img class="directory" src="ftv2node.png" />
              <a href="{$frame2}#{@id_req}_Attachements" target="princ">
                <span class="directoryNum">
                  <saxon:assign name="h3" select="my:accu($h3)" />
                  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
                </span>
                <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
                <xsl:value-of select="$fieldheader2" />
              </a>
            </li>
          </xsl:if>
          <xsl:apply-templates mode="Sommaire" select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" />
			</xsl:variable>
			<xsl:choose>
			  <xsl:when test="count($reqFamilyContent//*)>0">
			    <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_req}', this)"/>
			  </xsl:when>
			  <xsl:otherwise>
			    <img class="directory" src="ftv2node.png" />
			  </xsl:otherwise>
			</xsl:choose>
      <a href="{$frame2}#{@id_req}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>			
			<xsl:if test="count($reqFamilyContent//*)>0">
			  <div id="{@id_req}" style="display: none;">
          <ol>
            <xsl:copy-of select="$reqFamilyContent" />
          </ol>
        </div>
        <saxon:assign name="h3" select="0" />
      </xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="Requirement" mode="Sommaire">
		<li class="directory">
		  <img class="directory" src="ftv2node.png" />
			<a href="{$frame2}#{@id_req}" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h3" select="my:accu($h3)" />
				  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				  <xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
				  <xsl:value-of select="$fieldheader" />
				  &#160;:&#160;
				</span>
				<xsl:value-of select="Nom/text()"/>
			</a>
		</li>
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:if test="$requirements = '1'">
			<xsl:apply-templates select="Requirements" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirements" mode="Projet">
		<xsl:if test="$existRequirement = 'true'">
			<br />
			<hr />
			<h1>
				<a name="Req" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
				<xsl:value-of select="$fieldheader1" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='EXIGENCES']" />
					<xsl:value-of select="$fieldheader2" />
				</span>
			</h1>
			<saxon:assign name="h2" select="0" />
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<h2>
				<a name="ReqRoot" />
				<saxon:assign name="h2" select="my:accu($h2)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
				<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
			</h2>
			<xsl:apply-templates mode="Projet" select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" />
			<xsl:apply-templates mode="Projet" select=".//RequirementFamily[.//Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]]">
				<xsl:sort select="count(ancestor::*)" data-type="number" order="ascending" />
			</xsl:apply-templates>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="Projet">
		<h3>
			<a name="{@id_req}" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigence']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<p>
			<xsl:apply-templates select="Description" mode="Projet">
				<xsl:with-param name="withBr">true</xsl:with-param>
			</xsl:apply-templates>
			<b>ID :&#160;</b>
			<xsl:value-of select="substring-after(@id_req, 'Req_')" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@category" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@priority" />			
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@complexity" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@criticity" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@understanding" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@owner" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@state" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@checkedBy" />
			<xsl:if test="@origine and @origine!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Origine']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@origine" />
			</xsl:if>
			<xsl:if test="@reference and @reference!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Reference']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@reference" />
			</xsl:if>
			<xsl:if test="@version and @version!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Version']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@version" />
			</xsl:if>
			<xsl:if test="@verifway and @verifway!=''">
				<br />
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Mode_de_verification']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@verifway" />
			</xsl:if>
		</p>
		<!-- Attachements -->
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<!--  Couverture -->
		<xsl:variable name="idReq"><xsl:value-of select="@id_req" /></xsl:variable>
		<xsl:variable name="testLinkRequirement">
			<xsl:value-of select="boolean(//Test/LinkRequirement/RequirementRef[@ref=$idReq])" />
		</xsl:variable>
		<xsl:if test="$testLinkRequirement = 'true'">
			<xsl:if test="Attachements">
				<br />
			</xsl:if>
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture_de_test']" />
					<xsl:value-of select="$fieldheader7" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
						<xsl:value-of select="$fieldheader8" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
						<xsl:value-of select="$fieldheader9" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
						<xsl:value-of select="$fieldheader10" />
					</th>
				</tr>
				<xsl:for-each select="//Test/LinkRequirement/RequirementRef[@ref=$idReq]">
					<tr>
						<td class="tab_center"><xsl:number value="position()" format="1" /></td>
						<td class="tab">
							<a href="#{../../../../../../@id_famille}">
								<xsl:value-of select="../../../../../../Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<a href="#{../../../../@id_suite}">
								<xsl:value-of select="../../../../Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<a href="#{../../@id_test}">
								<xsl:value-of select="../../Nom/text()" />
							</a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
		<xsl:if test="//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]/ExecCampTests/ExecCampTest">
			<xsl:if test="$testLinkRequirement = 'true' or Attachements">
				<br />
			</xsl:if>
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Couverture_d_execution']" />
					<xsl:value-of select="$fieldheader11" />
					&#160;
					(
					<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pourcentage_de_couverture']" />
					<xsl:value-of select="$fieldheader12" />
					&#160;:&#160;
					<xsl:variable name="nbTestCamp">
						<xsl:value-of select="count(//Test[@id_test=//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]//TestRef/@ref and LinkRequirement/RequirementRef/@ref=$idReq])" />
					</xsl:variable>
					<xsl:variable name="nbTestReq">
						<xsl:value-of select="count(//Test[LinkRequirement/RequirementRef/@ref=$idReq])" />
					</xsl:variable>
					<xsl:variable name="couverture">
						<xsl:value-of select="$nbTestCamp div $nbTestReq" />
					</xsl:variable>
					<xsl:value-of select="format-number($couverture, '###.##%')" />
					)
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="22%">
						<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
						<xsl:value-of select="$fieldheader13" />
					</th>
					<th class="titre" width="22%">
						<xsl:variable name="fieldheader14" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
						<xsl:value-of select="$fieldheader14" />
					</th>
					<th class="titre" width="22%">
						<xsl:variable name="fieldheader15" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
						<xsl:value-of select="$fieldheader15" />
					</th>
					<th class="titre" width="24%">
						<xsl:variable name="fieldheader16" select="$translations/allheadings/headings[lang($local)]/heading[@category='Tests_associes']" />
						<xsl:value-of select="$fieldheader16" />
					</th>
				</tr>
				<xsl:for-each select="//CampagneTest[LinkRequirement/RequirementRef/@ref=$idReq]/ExecCampTests/ExecCampTest">
					<xsl:variable name="id_env">
						<xsl:value-of select="EnvironnementEx/@ref" />
					</xsl:variable>
					<tr>
						<td class="tab_center"><xsl:number value="position()" format="1" /></td>
						<td class="tab">
							<a href="#{../../@id_camp}">
								<xsl:value-of select="../../Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<a href="#{EnvironnementEx/@ref}">
								<xsl:value-of select="//Environnement[@idEnv=$id_env]/Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<a href="#{@id_exec_camp}">
								<xsl:value-of select="Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<xsl:for-each select="id(ancestor::CampagneTest/FamillesCamp/FamilleRef/SuiteTestsCamp/SuiteTestRef/TestsCamp/TestRef/@ref)[LinkRequirement/RequirementRef/@ref=$idReq]">
								<a href="#{@id_test}">
									<xsl:value-of select="Nom/text()" />
								</a>
								<xsl:if test="position() != last()">
									<br />
								</xsl:if>
							</xsl:for-each>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="RequirementFamily" mode="Projet">
		<h2>
			<a name="{@id_req}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<p>
			<xsl:apply-templates select="Description" mode="Projet">
				<xsl:with-param name="withBr">true</xsl:with-param>
			</xsl:apply-templates>
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parent']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="parent::RequirementFamily">
					<a href="#{parent::RequirementFamily/@id_req}">
						<xsl:value-of select="parent::RequirementFamily/Nom/text()" />
					</a>
				</xsl:when>
				<xsl:otherwise>
					<a href="#ReqRoot">
						<xsl:value-of select="concat('Requirements_', //ProjetVT/Nom/text())" />
					</a>
				</xsl:otherwise>
			</xsl:choose>
		</p>
		<xsl:if test="Attachements">
			<h3>
				<a name="{@id_req}_Attachements" />
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader4" />
			</h3>
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:apply-templates select="Requirement[@id_req=//CampagneTest/LinkRequirement/RequirementRef/@ref]" mode="Projet" />
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:apply-templates select="LinkRequirement" mode="Projet">
			<xsl:with-param name="camp">false</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="LinkRequirement" mode="Projet">
		<xsl:param name="camp" />
		<table class="tab">
			<caption>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
				<xsl:value-of select="$fieldheader1" />
				<xsl:if test="$camp = 'true'">
					&#160;
					(
					<xsl:variable name="fieldheader0" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pourcentage_de_couverture']" />
					<xsl:value-of select="$fieldheader0" />
					&#160;:&#160;
					<xsl:variable name="couverture">
						<xsl:value-of select="count(RequirementRef) div count(//Requirement)" />
					</xsl:variable>
					<xsl:value-of select="format-number($couverture, '###.##%')" />
					)
				</xsl:if>
			</caption>
			<tr>
				<th class="titre">ID</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader2" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>		
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
					<xsl:value-of select="$fieldheader3" />
				</th>				
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
			</tr>
			<xsl:apply-templates select="RequirementRef" mode="Projet">
				<xsl:sort select="id(@ref)/@priority" data-type="number" order="descending" />
			</xsl:apply-templates>
		</table>
		<br />
	</xsl:template>
	<xsl:template match="RequirementRef" mode="Projet">
		<xsl:variable name="id_req">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:value-of select="substring-after($id_req, 'Req_')" />
			</td>
			<td class="tab">
				<a href="#{@ref}">
					<xsl:value-of select="./Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:variable name="category">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@category" />
				</xsl:variable>
				<xsl:value-of select="$category" />
			</td>
			<td class="tab">
				<xsl:variable name="priority">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@priority" />
				</xsl:variable>
				<xsl:value-of select="$priority" />
			</td>
			<td class="tab">
				<xsl:variable name="complexity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@complexity" />
				</xsl:variable>
				<xsl:value-of select="$complexity" />
			</td>
			<td class="tab">
				<xsl:variable name="criticity">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@criticity" />
				</xsl:variable>
				<xsl:value-of select="$criticity" />
			</td>
			<td class="tab">
				<xsl:variable name="understanding">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@understanding" />
				</xsl:variable>
				<xsl:value-of select="$understanding" />
			</td>
			<td class="tab">
				<xsl:variable name="owner">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@owner" />
				</xsl:variable>
				<xsl:value-of select="$owner" />
			</td>
			<td class="tab">
				<xsl:variable name="state">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@state" />
				</xsl:variable>
				<xsl:value-of select="$state" />
			</td>
			<td class="tab">
				<xsl:variable name="checkedBy">
					<xsl:value-of select="//Requirement[@id_req=$id_req]/@checkedBy" />
				</xsl:variable>
				<xsl:value-of select="$checkedBy" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="extCampaign1">
		<xsl:apply-templates select="LinkRequirement" mode="Projet">
			<xsl:with-param name="camp">true</xsl:with-param>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)" mode="requirements" />
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="requirements">
		<xsl:variable name="lien_test">
			<xsl:value-of select="concat('testresult_', @id_exec_res)" />
		</xsl:variable>
		<xsl:variable name="idExecRes">
			<xsl:value-of select="@id_exec_res" />
		</xsl:variable>
		<xsl:variable name="exigence">
			<xsl:value-of select="boolean(ancestor::CampagneTest/LinkRequirement/RequirementRef[@ref=//Test[@id_test=//ResulExecCampTest[@id_exec_res=$idExecRes]/ResulExecs/ResulExec[@res='PASSED' or @res='FAILED' or @res='INCONCLUSIF']/@refTest]/LinkRequirement/RequirementRef/@ref])" />
		</xsl:variable>
		<xsl:if test="$exigence = 'true'">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_satisfaites']" />
					<xsl:value-of select="$fieldheader8" />
				</caption>
				<tr>
					<th class="titre">ID</th>
					<th class="titre" width="30%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Satisfaction']" />
						<xsl:value-of select="$fieldheader9" />
					</th>
					<th class="titre" width="30%">
						<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
						<xsl:value-of select="$fieldheader10" />
					</th>
				</tr>
				<xsl:apply-templates select="id(ancestor::CampagneTest/LinkRequirement/RequirementRef/@ref)[@id_req=//Test[@id_test=//ResulExecCampTest[@id_exec_res=$idExecRes]/ResulExecs/ResulExec[@res='PASSED' or @res='FAILED' or @res='INCONCLUSIF']/@refTest]/LinkRequirement/RequirementRef/@ref]"
					mode="ResulExecCampTest">
					<xsl:with-param name="idExecRes">
						<xsl:value-of select="$idExecRes" />
					</xsl:with-param>
					<xsl:with-param name="lien_test">
						<xsl:value-of select="$lien_test" />
					</xsl:with-param>
				</xsl:apply-templates>
			</table>
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="ResulExecCampTest">
		<xsl:param name="idExecRes" />
		<xsl:param name="lien_test" />
		<xsl:variable name="idReq">
			<xsl:value-of select="@id_req" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:value-of select="substring-after($idReq, 'Req_')" />
			</td>
			<td class="tab">
				<a href="#{@id_req}">
					<xsl:value-of select="./Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:call-template name="verdictRequirement">
					<xsl:with-param name="id_req">
						<xsl:value-of select="$idReq" />
					</xsl:with-param>
					<xsl:with-param name="id_resulExec">
						<xsl:value-of select="$idExecRes" />
					</xsl:with-param>
				</xsl:call-template>
			</td>
			<td class="tab">
				<xsl:for-each select="//Test[LinkRequirement/RequirementRef/@ref=$idReq and @id_test=id($idExecRes)/ResulExecs/ResulExec/@refTest]">
					<a href="#{concat($lien_test, '_', @id_test)}">
						<xsl:value-of select="Nom/text()" />
						<xsl:if test="position()!=last()">
							<br />
						</xsl:if>
					</a>
				</xsl:for-each>
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)" mode="requirements" />
	</xsl:template>
	<xsl:template match="ResulExec" mode="requirements">
		<xsl:variable name="idTest">
			<xsl:value-of select="@refTest" />
		</xsl:variable>
		<xsl:variable name="exigenceTest">
			<xsl:value-of select="boolean(ancestor::CampagneTest/LinkRequirement/RequirementRef[@ref=//Test[@id_test=$idTest]/LinkRequirement/RequirementRef/@ref])" />
		</xsl:variable>
		<xsl:if test="$exigenceTest = 'true'">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Exigences_couvertes']" />
					<xsl:value-of select="$fieldheader10" />
				</caption>
				<tr>
					<th class="titre">ID</th>
					<th class="titre" width="18%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Categorie']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>		
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Complexite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Criticite']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Comprehension']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Proprietaire']" />
					<xsl:value-of select="$fieldheader3" />
				</th>				
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Etat']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				<th class="titre" width="18%">
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valide_par']" />
					<xsl:value-of select="$fieldheader3" />
				</th>
				</tr>
				<xsl:apply-templates select="id(ancestor::CampagneTest/LinkRequirement/RequirementRef/@ref)[@id_req=//Test[@id_test=$idTest]/LinkRequirement/RequirementRef/@ref]" mode="detailExec">
					<xsl:sort select="@priority" data-type="number" order="descending" />
				</xsl:apply-templates>
			</table>
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Requirement" mode="detailExec">
		<tr>
			<td class="tab_center">
				<xsl:value-of select="substring-after(@id_req, 'Req_')" />
			</td>
			<td class="tab">
				<a href="#{@id_req}">
					<xsl:value-of select="Nom/text()" />
		 		</a>
			</td>
			<td class="tab">
				<xsl:value-of select="@category" />
			</td>
			<td class="tab">
				<xsl:value-of select="@priority" />
			</td>
			<td class="tab">
				<xsl:value-of select="@complexity" />
			</td>
			<td class="tab">
				<xsl:value-of select="@criticity" />
			</td>
			<td class="tab">
				<xsl:value-of select="@understanding" />
			</td>
			<td class="tab">
				<xsl:value-of select="@owner" />
			</td>
			<td class="tab">
				<xsl:value-of select="@state" />
			</td>
			<td class="tab">
				<xsl:value-of select="@checkedBy" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template name="verdictRequirement">
		<xsl:param name="id_req" />
		<xsl:param name="id_resulExec" />
		<xsl:choose>
			<xsl:when test="//ResulExecCampTest[@id_exec_res=$id_resulExec]/ResulExecs/ResulExec[@res='FAILED' and @refTest=//Test[LinkRequirement/RequirementRef/@ref=$id_req]/@id_test]">
				<span class="failed">
					Fail
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="//ResulExecCampTest[@id_exec_res=$id_resulExec]/ResulExecs/ResulExec[@res='INCONCLUSIF' and @refTest=//Test[LinkRequirement/RequirementRef/@ref=$id_req]/@id_test]">
						<span class="inconclusif">
							<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
							<xsl:value-of select="$fieldheader1" />
						</span>
					</xsl:when>
					<xsl:otherwise>
						<span class="passed">
							Pass
						</span>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>