/*
 * Created on 20 juin 2005
 * SalomeTMF is a Test Managment Framework
 * Copyright (C) 2005 France Telecom R&D
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * Contact: mikael.marche@rd.francetelecom.com
 */
package plugins;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

/**
 * @author marchemi g.favro
 *
 *
 */

public class PluginInstaller {
    static String plgName;
    static String plgFile;
    static String propertiesFile = "CfgPlugins.properties";
    static String avaiblePugin = "";
    static Properties pluginProperties = new Properties();

    static private void printHelp() {
	System.out.println("PluginInstaller arguments are :");
	System.out
	    .println("\tpluginarchive.zip (compressed archive of the plugin)");
	System.out
	    .println("\tSalome-TMF plugin properties file (default CfgPlugins.properties)");

    }

    public static final void copyInputStream(InputStream in, OutputStream out)
	throws IOException {
	byte[] buffer = new byte[1024];
	int len;
	while ((len = in.read(buffer)) >= 0)
	    out.write(buffer, 0, len);

	in.close();
	out.close();
    }

    public static void main(String[] args) {
	if (args.length < 1 || args.length > 2) {
	    printHelp();
	    System.exit(1);
	}
	plgFile = args[0];
	if (args.length > 1) {
	    propertiesFile = args[1];
	}

	try {
	    loadPluginPropertie();
	    uncompressPlugin();
	    savePluginJnlp();
	    savePluginPropertie();
	} catch (Exception e) {
	    e.printStackTrace();
	    System.out.println("Plugin " + plgName
			       + " installation fail, try manualy");
	    System.exit(1);
	}
	System.out.println("Plugin " + plgName + " successfully installed");
	System.exit(0);
    }

    static private void loadPluginPropertie() throws Exception {
	FileInputStream ins = new FileInputStream(propertiesFile);
	pluginProperties.load(ins);
	ins.close();
	avaiblePugin = pluginProperties.getProperty("pluginsList", "core");
    }

    static private void savePluginPropertie() throws Exception {
	FileOutputStream out = new FileOutputStream(propertiesFile);
	pluginProperties.store(out, "");
	out.close();
    }

    static private void savePluginJnlp() throws Exception {
	File jnlpFile = new File("plugins.jnlp");

	SAXBuilder sxb = new SAXBuilder();

	/* je le construit */
	Document documentJNLP = sxb.build(jnlpFile);
	Element racineJNLP = documentJNLP.getRootElement();
	Element resources = racineJNLP.getChild("resources");

	List<Element> resourcesChildren = resources.getChildren();

	for (int i = 0; i < resourcesChildren.size(); i++) {/*
							     * Je parcours les
							     * plugins d�j�
							     * pr�sent
							     */
	    if (resourcesChildren.get(i).getAttribute("name").getValue()
		.equals(plgName))
		/*
		 * Si le plugin qu'on vient d'ajouter est d�j� pr�sent j'arrete
		 * le programme
		 */
		return;
	}
	/* Sinon je l'ajoute */
	Element extension = new Element("extension");
	List<Attribute> attributes = new ArrayList<Attribute>();
	attributes.add(new Attribute("name", plgName));
	attributes
	    .add(new Attribute("href", plgName + "/" + plgName + ".jnlp"));
	extension.setAttributes(attributes);

	Element extDownload = new Element("ext-download");
	extDownload.setAttribute("ext-part", plgName);
	extDownload.setAttribute("download", "lazy");

	extension.addContent(extDownload);
	resources.addContent(extension);

	XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());

	sortie.output(documentJNLP, new FileOutputStream(jnlpFile));
    }

    static private void uncompressPlugin() throws Exception {
	boolean firstDir = true;
	boolean toWrite = true;
	ZipFile pZipFile = new ZipFile(plgFile);
	Enumeration pZipEntries = pZipFile.entries();
	while (pZipEntries.hasMoreElements()) {
	    ZipEntry pEntry = (ZipEntry) pZipEntries.nextElement();
	    // System.out.println("Found entry : " + pEntry.getName());
	    if (pEntry.isDirectory()) {
		if (firstDir) {
		    firstDir = false;
		    plgName = pEntry.getName();
		    if (plgName.endsWith("/") || plgName.endsWith("\\")) {
			plgName = plgName.substring(0, plgName.length() - 1);
		    }
		    StringTokenizer st = new StringTokenizer(avaiblePugin, ",");
		    while (st.hasMoreTokens() && toWrite) {
			String token = st.nextToken().trim();
			if (token.equals(plgName)) {
			    toWrite = false;
			}
		    }
		    if (toWrite) {
			avaiblePugin += ", " + plgName;
			pluginProperties.setProperty("pluginsList",
						     avaiblePugin);
		    }
		}
		System.out.println("Extracting directory: " + pEntry.getName());
		(new File(pEntry.getName())).mkdir();
	    } else {
		System.out.println("Extracting file: " + pEntry.getName());
		copyInputStream(pZipFile.getInputStream(pEntry),
				new BufferedOutputStream(new FileOutputStream(pEntry
									      .getName())));

	    }
	}
	pZipFile.close();
    }
}
