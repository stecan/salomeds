<?xml version="1.0" encoding="ISO-8859-1" ?>
<!--
    Document   : framesStatiqueXmlToHtml.xsl
    Created on : 16 mars 2007, 16:52
    Authors    : PENAULT Aurore, LEYRIE Jean-Michel
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	<xsl:output method="xhtml"/>
	<xsl:output method="xhtml" name="xhtmlFormat"/>
	<xsl:param name="frame1" />
	<xsl:param name="frame2" />
	<xsl:param name="jpeg" />
	<xsl:param name="gif" />
	<xsl:param name="png" />
	<xsl:param name="project" />
	<xsl:param name="environments" />
	<xsl:param name="testplan" />
	<xsl:param name="local" />
	<xsl:param name="translate" />
	<xsl:variable name="translations" select="document($translate)" />
	<xsl:variable name="description">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Description']" />
	</xsl:variable>
	<xsl:variable name="voir">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Voir']" />
	</xsl:variable>
	<xsl:variable name="h1" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h2" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h3" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h4" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:template match="/">
		<html>
			<head>
				<title>
					<!--Tests du projet-->
					<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Dossier_de_tests']" />
					<xsl:value-of select="$fieldheader" /> : &#160;
					<xsl:value-of select="//ProjetVT/Nom/text()" />
				</title>
			</head>
			<frameset cols="*, 3*">
				<frame src="{$frame1}" scrolling="yes"/>
				<frame src="{$frame2}" name="princ"/>
			</frameset>
		</html>
		<xsl:result-document href="{$frame1}" format="xhtmlFormat">
			<html>
				<head>
					<title>
						<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Sommaire']"/>
						<xsl:value-of select="$fieldheader"/>
					</title>
					<script language="JavaScript">
            <![CDATA[
                function toggleFolder(id, imageNode) 
                {
                  var folder = document.getElementById(id);
                  var l = imageNode.src.length;
                  if (imageNode.src.substring(l-20,l)=="ftv2folderclosed.png" || 
                      imageNode.src.substring(l-18,l)=="ftv2folderopen.png")
                  {
                    imageNode = imageNode.previousSibling;
                    l = imageNode.src.length;
                  }
                  if (folder == null) 
                  {
                  } 
                  else if (folder.style.display == "block") 
                  {
                    if (imageNode != null) 
                    {
                      imageNode.nextSibling.src = "ftv2folderclosed.png";
                      if (imageNode.src.substring(l-13,l) == "ftv2mnode.png")
                      {
                        imageNode.src = "ftv2pnode.png";
                      }
                      else if (imageNode.src.substring(l-17,l) == "ftv2mlastnode.png")
                      {
                        imageNode.src = "ftv2plastnode.png";
                      }
                    }
                    folder.style.display = "none";
                  } 
                  else 
                  {
                    if (imageNode != null) 
                    {
                      imageNode.nextSibling.src = "ftv2folderopen.png";
                      if (imageNode.src.substring(l-13,l) == "ftv2pnode.png")
                      {
                        imageNode.src = "ftv2mnode.png";
                      }
                      else if (imageNode.src.substring(l-17,l) == "ftv2plastnode.png")
                      {
                        imageNode.src = "ftv2mlastnode.png";
                      }
                    }
                    folder.style.display = "block";
                  }
                }
              ]]>
          </script>
					<link href="salome.css" rel="stylesheet" type="text/css"/>
				</head>
				<body class="directory">
					<xsl:apply-templates mode="Titre" select="//ProjetVT"/>
					<xsl:apply-templates mode="Sommaire" select="//ProjetVT"/>
				</body>
			</html>
		</xsl:result-document>
		<xsl:result-document href="{$frame2}" format="xhtmlFormat">
			<html>
				<head>
					<title>
						<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Dossier_de_tests']" />
						<xsl:value-of select="$fieldheader" />
						:
						<xsl:value-of select="//ProjetVT/Nom/text()" />
					</title>
					<link href="salome.css" rel="stylesheet" type="text/css"/>
				</head>
				<body>
					<xsl:apply-templates mode="Projet" select="//ProjetVT" />
				</body>
			</html>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Titre">
		<h1 id="sommaire">
			<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='SOMMAIRE']" />
			<xsl:value-of select="$fieldheader" />
		</h1>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Sommaire">
		<ol>
			<xsl:if test="$project = '1'">
				<li class="rootDirectory">
				  <saxon:assign name="h1" select="my:accu($h1)" />
					<xsl:variable name="projectContent">
					  <xsl:if test="Attachements">
  					  <li class="directory">
  					    <img class="directory" src="ftv2node.png"/>
	 						  <a href="{$frame2}#AttachProj" target="princ">
							    <span class="directoryNum">
							  	  <saxon:assign name="h2" select="my:accu($h2)" />
							  	  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />
							  	  &#160;
							    </span>
							    <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
								  <xsl:value-of select="$fieldheader2" />
							  </a>
							</li>
						</xsl:if>
						<xsl:if test="Params">
  					  <li class="directory">
  					    <img class="directory" src="ftv2node.png"/>
							  <a href="{$frame2}#paramProj" target="princ">
								  <span class="directoryNum">
								    <saxon:assign name="h2" select="my:accu($h2)" />
									  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />
									  &#160;
									</span>
									<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
									<xsl:value-of select="$fieldheader3" />
								</a>
								<xsl:variable name="paramContent">
  								<xsl:call-template name="extSummaryParam">
									  <xsl:with-param name="current-node">
										  <xsl:value-of select="saxon:path()" />
									  </xsl:with-param>
								  </xsl:call-template>
								</xsl:variable>
								<xsl:if test="count($paramContent//*)>0">
								  <ol>
  									<xsl:copy-of select="$paramContent" />
									  <saxon:assign name="h3" select="0" />
								  </ol>
								</xsl:if>
							</li>
						</xsl:if>
						<xsl:call-template name="extSummaryProject">
						  <xsl:with-param name="current-node">
  							<xsl:value-of select="saxon:path()" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
					  <xsl:when test="count($projectContent//*)>0">
              <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('ProjetVT', this)"/>					  
					  </xsl:when>
					  <xsl:otherwise>
					    <img class="directory" src="ftv2node.png"/>
					  </xsl:otherwise>
					</xsl:choose>					
          <a href="{$frame2}#{Nom/text()}" target="princ">
            <span class="directoryNum">
              <xsl:value-of select="$h1" />
              &#160;
            </span>
            <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
            <xsl:value-of select="$fieldheader1" />
            &#160;:&#160;
            <xsl:value-of select="./Nom/text()" />
          </a>
					<xsl:if test="count($projectContent//*)>0">
					  <div id="ProjetVT" style="display: none;">
  						<ol>
							  <xsl:copy-of select="$projectContent" />
							  <saxon:assign name="h2" select="0" />
						  </ol>
					 </div>
					</xsl:if>
				</li>
			</xsl:if>
			<xsl:if test="$environments = '1'">
				<xsl:apply-templates mode="Sommaire" select="Environnements" />
			</xsl:if>
			<xsl:call-template name="extSummaryRoot1">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="$testplan = '1'">
				<xsl:apply-templates mode="Sommaire" select="Familles" />
			</xsl:if>
			<xsl:call-template name="extSummaryRoot2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</ol>
	</xsl:template>
	<xsl:template name="extSummaryParam">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryProject">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryRoot2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Environnements" mode="Sommaire">
		<li class="rootDirectory">
      <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('EnvsProjet', this)"/>
			<a href="{$frame2}#EnvsProjet" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h1" select="my:accu($h1)" />
				  <xsl:value-of select="$h1" />&#160;
				</span>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
				<xsl:value-of select="$fieldheader1" />
			</a>
			<div id="EnvsProjet" style="display: none;">
			  <xsl:if test="Environnement">			    
				  <ol>
					  <xsl:apply-templates select="Environnement" mode="Sommaire" />
					  <saxon:assign name="h2" select="0" />
				  </ol>
			  </xsl:if>
			</div>
		</li>
	</xsl:template>
	<xsl:template match="Environnement" mode="Sommaire">
		<li class="directory">
		  <saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:variable name="environmentContent">
				<xsl:if test="Attachements">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_DocEnv" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />
							  &#160;
							</span>
							<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
							<xsl:value-of select="$fieldheader3" />
						</a>
					</li>
				</xsl:if>
				<xsl:if test="ValeurParams">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_ParamEnv" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />
							  &#160;
							</span>
							<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
							<xsl:value-of select="$fieldheader4" />
						</a>
					</li>
				</xsl:if>
				<xsl:if test="Script">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_Script" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />
							  &#160;
							</span>
							<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script']" />
							<xsl:value-of select="$fieldheader5" />
						</a>
					</li>
				</xsl:if>
				<xsl:call-template name="extSummaryEnvironment">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
			  <xsl:when test="count($environmentContent//*)>0">
  			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@idEnv}', this)"/>
			  </xsl:when>     
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@idEnv}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;       
          <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
          <xsl:value-of select="$fieldheader2" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="./Nom/text()" />
      </a>
			<xsl:if test="count($environmentContent//*)>0">
			  <div id="{@idEnv}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$environmentContent" />
				    <saxon:assign name="h3" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryEnvironment">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Familles" mode="Sommaire">
		<li class="rootDirectory">
		  <xsl:choose>
		    <xsl:when test="Famille">
  		    <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('Familles', this)"/>
		    </xsl:when>     
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
			<a href="{$frame2}#DossierTests" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h1" select="my:accu($h1)" />
				  <xsl:value-of select="$h1" />&#160;
				</span>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
				<xsl:value-of select="$fieldheader1" />
			</a>
			<xsl:if test="Famille">
			  <div id="Familles" style="display: none;">
				  <ol>
					  <xsl:apply-templates mode="Sommaire" select="./Famille" />
					  <saxon:assign name="h2" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template match="Famille" mode="Sommaire">
		<li class="directory">
		  <saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:variable name="familyContent">
				<xsl:call-template name="extSummaryFamily1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:apply-templates mode="Sommaire" select="SuiteTests/SuiteTest" />
				<xsl:call-template name="extSummaryFamily2">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
			  <xsl:when test="count($familyContent//*)>0">
  			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_famille}', this)"/>
			  </xsl:when>			
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@id_famille}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />
          &#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($familyContent//*)>0">
			  <div id="{@id_famille}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$familyContent" />
					  <saxon:assign name="h3" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryFamily1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryFamily2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="SuiteTest" mode="Sommaire">
		<li class="directory">
      <saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:variable name="suiteContent">
				<xsl:call-template name="extSummarySuite1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:apply-templates mode="Sommaire" select="Tests/Test" />
				<xsl:call-template name="extSummarySuite2">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
			  <xsl:when test="count($suiteContent//*)>0">
  			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_suite}', this)"/>
			  </xsl:when>
			  <xsl:otherwise>
			    <img class="directory" src="ftv2node.png"/>
			  </xsl:otherwise>
			</xsl:choose>
      <a href="{$frame2}#{@id_suite}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($suiteContent//*)>0">
			  <div id="{@id_suite}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$suiteContent" />
					  <saxon:assign name="h4" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummarySuite1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummarySuite2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Test" mode="Sommaire">
		<li class="directory">
		  <img class="directory" src="ftv2node.png"/>
			<a href="{$frame2}#{@id_test}" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h4" select="my:accu($h4)" />
				  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
				  <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
				  <xsl:value-of select="$fieldheader1" />
				  &#160;:&#160;
				</span>
				<xsl:value-of select="Nom/text()" />
			</a>
			<xsl:variable name="extContent">
				<xsl:call-template name="extSummaryTest">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="count($extContent//*)>0">
				<ol>
					<xsl:copy-of select="$extContent" />
				</ol>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Projet">
		<saxon:assign name="h1" select="0" />
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<xsl:if test="$project = '1'">
			<h1>
				<a name="{Nom/text()}" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
				<xsl:value-of select="$fieldheader1" />
				&#160;:&#160;
				<xsl:value-of select="./Nom/text()" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
					<xsl:value-of select="$fieldheader1" />
					&#160;:&#160;
					<xsl:value-of select="./Nom/text()" />
				</span>
			</h1>
			<xsl:apply-templates select="Description" mode="p" />
			<xsl:apply-templates select="Attachements" mode="ProjetVT" />
			<xsl:apply-templates select="Params" mode="ProjetVT" />
			<xsl:call-template name="extProject">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$environments = '1'">
			<xsl:apply-templates select="Environnements" mode="ProjetVT" />
		</xsl:if>
		<xsl:call-template name="extRoot1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="$testplan = '1'">
			<xsl:apply-templates select="Familles" mode="Projet" />
		</xsl:if>
		<xsl:call-template name="extRoot2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extProject">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="ProjetVT">
		<h2>
			<a name="AttachProj" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
			<xsl:value-of select="$fieldheader4" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
 		<xsl:apply-templates select="." mode="Projet" />
		<xsl:apply-templates select="FileAttachement" mode="inclusion" />
	</xsl:template>
	<xsl:template match="Params" mode="ProjetVT">
		<h2>
			<a name="paramProj" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
			<xsl:value-of select="$fieldheader5" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<table class="tab">
			<tr>
				<th class="titre">N�</th>
				<th class="titre" width="45%">
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader6" />
				</th>
				<th class="titre" width="45%">
					<xsl:value-of select="$description" />
				</th>
			</tr>
			<xsl:apply-templates mode="Projet" select="Param" />
		</table>
		<xsl:call-template name="extParam">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extParam">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Environnements" mode="ProjetVT">
		<br />
		<hr />
		<h1>
			<a name="EnvsProjet" />
			<saxon:assign name="h1" select="my:accu($h1)" />
			<xsl:value-of select="$h1" />&#160;
			<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
			<xsl:value-of select="$fieldheader8" />
			<span>
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
				<xsl:value-of select="$fieldheader8" />
			</span>
		</h1>
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<table class="tab">
			<tr>
				<th class="titre">N�</th>
				<th class="titre" width="45%">
					<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader10" />
				</th>
				<th class="titre" width="45%">
					<xsl:value-of select="$description" />
				</th>
			</tr>
			<xsl:apply-templates select="Environnement" mode="ProjetVT" />
		</table>
		<xsl:apply-templates select="Environnement" mode="Projet" />
	</xsl:template>
	<xsl:template match="Environnement" mode="ProjetVT">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{@idEnv}">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:copy-of select="Description/* | Description/text()" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param	name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text	disable-output-escaping="no">
							&#160;
						</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Environnement" mode="Projet">
		<h2>
			<a name="{@idEnv}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:apply-templates select="Attachements" mode="Environnement">
			<xsl:with-param name="idEnvParam">
				<xsl:value-of select="@idEnv" />
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="ValeurParams" mode="Environnement">
			<xsl:with-param name="idEnvParam">
				<xsl:value-of select="@idEnv" />
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Script" mode="Environnement">
			<xsl:with-param name="idEnvParam">
				<xsl:value-of select="@idEnv" />
			</xsl:with-param>
		</xsl:apply-templates>
		<xsl:call-template name="extEnvironment">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extEnvironment">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="Environnement">
		<xsl:param name="idEnvParam" />
		<h3>
			<a name="{$idEnvParam}_DocEnv" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
			<xsl:value-of select="$fieldheader3" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="." mode="Projet" />
		<xsl:apply-templates select="FileAttachement" mode="inclusion" />
	</xsl:template>
	<xsl:template match="ValeurParams" mode="Environnement">
		<xsl:param name="idEnvParam" />
		<h3>
			<a name="{$idEnvParam}_ParamEnv" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
			<xsl:value-of select="$fieldheader4" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<table class="tab">
			<tr>
				<th class="titre">N�</th>
				<th class="titre" width="45%">
					<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader5" />
				</th>
				<th class="titre" width="45%">
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
					<xsl:value-of select="$fieldheader6" />
				</th>
			</tr>
			<xsl:apply-templates select="ValeurParam" mode="ValeurParams" />
		</table>
	</xsl:template>
	<xsl:template match="ValeurParam" mode="ValeurParams">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#paramProj">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:value-of select="@valeur" />
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Script" mode="Environnement">
		<xsl:param name="idEnvParam" />
		<h3>
			<a name="{$idEnvParam}_Script" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script']" />
			<xsl:value-of select="$fieldheader7" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<p>
			<b>
				<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_d_environnement']" />
				<xsl:value-of select="$fieldheader8" />
			</b>
			<br />
			<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
			<xsl:value-of select="$fieldheader9" />
			&#160;:&#160;
			<xsl:choose>
				<xsl:when test="Text">
					<a href="{@dir}" target="_blank">
						<xsl:value-of select="@nom" />
					</a>
					<span class="lien_inclus">
						[
					</span>
					<a href="#{@dir}" class="lien_inclus">
						<xsl:value-of select="$voir" />
					</a>
					<span class="lien_inclus">
					  ]
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{@dir}" target="_blank">
						<xsl:value-of select="@nom" />
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<br />
			<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
			<xsl:value-of select="$fieldheader11" />
			&#160;:&#160;
			<xsl:value-of select="Classpath" />
		</p>
		<xsl:apply-templates select="." mode="inclusion" />
	</xsl:template>
	<xsl:template match="Familles" mode="Projet">
		<br />
		<hr />
		<h1>
			<a name="DossierTests" />
			<saxon:assign name="h1" select="my:accu($h1)" />
			<xsl:value-of select="$h1" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
			<xsl:value-of select="$fieldheader1" />
			<span>
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
				<xsl:value-of select="$fieldheader1" />
			</span>
		</h1>
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="Famille" mode="Projet" />
	</xsl:template>
	<xsl:template match="Script" mode="inclusion">
		<xsl:if test="Text">
			<a name="{@dir}">
				<xsl:text disable-output-escaping="no">&#160;</xsl:text>
			</a>
			<table class="inclusTab">
				<tr>
					<th>
						<xsl:value-of select="@nom" />
					</th>
				</tr>
				<tr>
					<td class="inclusTab">
						<xsl:for-each select="Text/ligne">
							<xsl:value-of select="./text()" />
							<br />
						</xsl:for-each>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="inclusion">
		<xsl:if test="$gif = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'gif'">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$jpeg = '1'">
			<xsl:if
				test="(substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg')">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$png = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'png'">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="Text">
			<a name="{@dir}">
				<xsl:text disable-output-escaping="no">&#160;</xsl:text>
			</a>
			<table class="inclusTab">
				<tr>
					<th>
						<xsl:value-of select="@nom" />
					</th>
				</tr>
				<tr>
					<td class="inclusTab">
						<xsl:for-each select="Text/ligne">
							<xsl:value-of select="./text()" />
							<br />
						</xsl:for-each>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Param" mode="Projet">
		<tr>
			<td class="tab_center">
				<xsl:value-of select="count(preceding-sibling::*)+1" />
			</td>
			<td class="tab">
				<xsl:value-of select="Nom/text()" />
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:copy-of select="Description/* | Description/text()" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param	name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text	disable-output-escaping="no">
							&#160;
						</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Famille" mode="Projet">
		<h2>
			<a name="{@id_famille}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()"></xsl:value-of>
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:if test="Attachements">
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader3" />
			</b>
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:call-template name="extFamily1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="SuiteTests/SuiteTest" mode="Projet" />
		<xsl:call-template name="extFamily2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extFamily1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extFamily2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="SuiteTest" mode="Projet">
		<h3>
			<a name="{@id_suite}"/>
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:if test="Attachements">
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader3" />
			</b>
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:call-template name="extSuite1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="Tests/Test" mode="Projet" />
		<xsl:call-template name="extSuite2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extSuite1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSuite2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Test" mode="Projet">
		<h4>
			<a name="{@id_test}" />
			<saxon:assign name="h4" select="my:accu($h4)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h4>
		<p>
			<xsl:apply-templates select="Description" mode="Projet">
				<xsl:with-param name="withBr">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:if test="Concepteur">
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Concepteur_du_test']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Concepteur/Nom/text()" />
			</xsl:if>
			<xsl:if test="Description or Concepteur">
				<br />
			</xsl:if>
			<b>
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
				<xsl:value-of select="$fieldheader4" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="TestAuto">
					<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Automatique']" />
					<xsl:value-of select="$fieldheader5" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Manuel']" />
					<xsl:value-of select="$fieldheader6" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="TestAuto">
				<br />
				<b>
					<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
					<xsl:value-of select="$fieldheader7" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="TestAuto/@plug_ext" />
			</xsl:if>
		</p>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:call-template name="extTest">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="ParamsT">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
					<xsl:value-of select="$fieldheader8" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre">
						<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
						<xsl:value-of select="$fieldheader9" />
					</th>
				</tr>
				<xsl:apply-templates select="ParamsT/ParamT" mode="Projet" />
			</table>
			<br />
		</xsl:if>
		<xsl:if test="TestAuto">
			<xsl:apply-templates select="TestAuto" mode="Projet" />
			<xsl:apply-templates select="TestAuto/Script" mode="inclusion" />
		</xsl:if>
		<xsl:if test="TestManuel">
			<xsl:apply-templates select="TestManuel" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ParamT" mode="Projet">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" />
			</td>
			<td class="tab">
				<a href="#paramProj">
					<xsl:value-of select="id(@ref)/Nom/text()" />
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Attachements" mode="Projet">
		<xsl:param name="caption"/>
		<table class="tab">
			<xsl:if test="$caption = 'true'">
				<caption>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
					<xsl:value-of select="$fieldheader1" />
				</caption>
			</xsl:if>
			<tr>
				<th class="titre">N�</th>
				<th class="titre" width="45%">
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
					<xsl:value-of select="$fieldheader2" />
				</th>
				<th class="titre" width="45%">
					<xsl:value-of select="$description" />
				</th>
			</tr>
			<xsl:for-each select="UrlAttachement">
				<tr>
					<td class="tab_center">
						<xsl:value-of select="count(preceding-sibling::UrlAttachement)+1"/>
					</td>
					<td class="tab">
						<a href="{@url}" target="_blank">
							<xsl:value-of select="@url" />
						</a>
					</td>
					<td class="tab">
						<xsl:choose>
							<xsl:when test="Description[@isHTML='true']">
								<xsl:copy-of select="Description/* | Description/text()" />
							</xsl:when>
							<xsl:when test="Description">
								<xsl:call-template name="replaceByBr">
									<xsl:with-param	name="textToReplace" select="Description/text()" />
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text	disable-output-escaping="no">
									&#160;
								</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</xsl:for-each>
			<xsl:for-each select="FileAttachement">
				<tr>
					<td class="tab_center">
						<xsl:value-of select="count(../UrlAttachement)+count(preceding-sibling::FileAttachement)+1"/>
					</td>
					<td class="tab">
						<xsl:apply-templates select="." mode="Projet" />
					</td>
					<td class="tab">
						<xsl:choose>
							<xsl:when test="Description[@isHTML='true']">
								<xsl:copy-of select="Description/* | Description/text()" />
							</xsl:when>
							<xsl:when test="Description">
								<xsl:call-template name="replaceByBr">
									<xsl:with-param	name="textToReplace" select="Description/text()" />
								</xsl:call-template>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text	disable-output-escaping="no">
									&#160;
								</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</tr>
			</xsl:for-each>
		</table>
	</xsl:template>
	<xsl:template match="TestAuto" mode="Projet">
		<b>
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_de_test']" />
			<xsl:value-of select="$fieldheader1" />
		</b>
		<br />
		<b>
			<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
			<xsl:value-of select="$fieldheader2" />
			&#160;:&#160;
		</b>
		<xsl:choose>
			<xsl:when test="Script/Text">
				<a href="{Script/@dir}" target="_blank">
					<xsl:value-of select="Script/@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{Script/@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
				<br />
			</xsl:when>
			<xsl:otherwise>
				<a href="{Script/@dir}" target="_blank">
					<xsl:value-of select="Script/@nom" />
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="TestManuel" mode="Projet">
		<xsl:if test="ActionTest">
			<b>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Actions_du_test']" />
				<xsl:value-of select="$fieldheader1" />
			</b>
			<ul class="actions1">
				<xsl:apply-templates select="ActionTest" mode="Projet" />
			</ul>
			<xsl:apply-templates select="ActionTest/Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="ActionTest" mode="Projet">
		<li>
			<b>
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
				<xsl:value-of select="$fieldheader4" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="Nom/text()" />
			<xsl:variable name="actionContent">
				<xsl:apply-templates select="Description" mode="li" />
				<xsl:if test="ResultAttendu">
					<li>
						<b>
							<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
							<xsl:value-of select="$fieldheader6" />
							&#160;:&#160;
						</b>
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace"
								select="ResultAttendu/text()" />
						</xsl:call-template>
					</li>
				</xsl:if>
				<xsl:if test="Attachements">
					<li>
						<b>
							<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
							<xsl:value-of select="$fieldheader7" />
							&#160;:&#160;
						</b>
						<ul class="actions3">
							<xsl:for-each	select="Attachements/UrlAttachement">
								<li>
									<a href="{@url}" target="_blank">
										<xsl:value-of select="@url" />
									</a>
								</li>
							</xsl:for-each>
							<xsl:for-each	select="Attachements/FileAttachement">
								<li>
									<xsl:apply-templates select="." mode="Projet" />
								</li>
							</xsl:for-each>
						</ul>
					</li>
				</xsl:if>
				<xsl:call-template name="extAction">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="count($actionContent//*)>0">
				<ul class="actions2">
					<xsl:copy-of select="$actionContent" />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extAction">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="FileAttachement" mode="Projet">
		<xsl:choose>
			<xsl:when test="($gif = '1') and (substring-after(@nom, '.') = 'gif')">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when	test="($jpeg = '1') and ((substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg'))">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when	test="($png = '1') and (substring-after(@nom, '.') = 'png')">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when test="Text">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{@dir}" target="_blank">
					<xsl:value-of	select="@nom" />
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Description" mode="p">
		<p>
			<xsl:apply-templates select="." mode="Projet" />
		</p>
	</xsl:template>
	<xsl:template match="Description" mode="li">
		<li>
			<xsl:apply-templates select="." mode="Projet" />
		</li>
	</xsl:template>
	<xsl:template match="Description" mode="Projet">
		<xsl:param name="withBr" />
		<b>
			<xsl:value-of select="$description" />
			&#160;:&#160;
		</b>
		<xsl:choose>
			<xsl:when test="@isHTML='true'">
				<br />
				<xsl:copy-of select="* | text()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="replaceByBr">
					<xsl:with-param name="textToReplace" select="./text()" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$withBr = 'true'">
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template name="replaceByBr">
		<xsl:param name="textToReplace" />
		<xsl:choose>
			<xsl:when test="contains($textToReplace, '\n')">
				<xsl:value-of
					select="substring-before($textToReplace,'\n')" />
				<br />
				<xsl:call-template name="replaceByBr">
					<xsl:with-param name="textToReplace"
						select="substring-after($textToReplace,'\n')" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$textToReplace" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:function name="my:accu">
		<xsl:param name="accumulateur" />
		<xsl:value-of select="$accumulateur + 1" />
	</xsl:function>
</xsl:stylesheet>