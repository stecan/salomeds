<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
    Document   : frameDynaXmlToHtml.xsl
    Created on : 22 mars 2007, 13:48
    Authors    : PENAULT Aurore, LEYRIE Jean-Michel
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	<xsl:output method="xhtml"/>
	<xsl:output method="xhtml" name="xhtmlFormat"/>
	<xsl:param name="frame1" />
	<xsl:param name="frame2" />
	<xsl:param name="jpeg" />
	<xsl:param name="gif" />
	<xsl:param name="png" />
	<xsl:param name="project" />
	<xsl:param name="environments" />
	<xsl:param name="testplan" />
	<xsl:param name="campaigns" />
	<xsl:param name="executionreport" />
	<xsl:param name="local" />
	<xsl:param name="translate" />
	<xsl:variable name="translations" select="document($translate)" />
	<xsl:variable name="description">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Description']" />
	</xsl:variable>
	<xsl:variable name="voir">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Voir']" />
	</xsl:variable>
	<xsl:variable name="translatedName">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
	</xsl:variable>
	<xsl:variable name="h1" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h2" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h3" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h4" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="h5" saxon:assignable="yes">
		<xsl:value-of select="0" />
	</xsl:variable>
	<xsl:variable name="existParam">
		<xsl:call-template name="exist-param" />
	</xsl:variable>

	<!-- TODO customize transformation rules syntax recommendation http://www.w3.org/TR/xslt -->

	<xsl:template match="/">
		<html>
			<head>
				<title>
					<!--Campagnes du projet-->
					<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_des_tests']"/>
					<xsl:value-of select="$fieldheader"/>
					:
          <xsl:value-of select="//ProjetVT/Nom/text()"/>
				</title>
			</head>
			<frameset cols="*, 3*">
				<frame src="{$frame1}" scrolling="yes"/>
				<frame src="{$frame2}" name="princ"/>
			</frameset>
		</html>
		<xsl:result-document href="{$frame1}" format="xhtmlFormat">
			<html>
				<head>
					<link href="salome.css" rel="stylesheet" type="text/css"/>
					<link href="salome_print.css" rel="stylesheet" type="text/css" media="print" />
					<script language="JavaScript">
            <![CDATA[
                function toggleFolder(id, imageNode)
                {
                  var folder = document.getElementById(id);
                  var l = imageNode.src.length;
                  if (imageNode.src.substring(l-20,l)=="ftv2folderclosed.png" ||
                      imageNode.src.substring(l-18,l)=="ftv2folderopen.png")
                  {
                    imageNode = imageNode.previousSibling;
                    l = imageNode.src.length;
                  }
                  if (folder == null)
                  {
                  }
                  else if (folder.style.display == "block")
                  {
                    if (imageNode != null)
                    {
                      imageNode.nextSibling.src = "ftv2folderclosed.png";
                      if (imageNode.src.substring(l-13,l) == "ftv2mnode.png")
                      {
                        imageNode.src = "ftv2pnode.png";
                      }
                      else if (imageNode.src.substring(l-17,l) == "ftv2mlastnode.png")
                      {
                        imageNode.src = "ftv2plastnode.png";
                      }
                    }
                    folder.style.display = "none";
                  }
                  else
                  {
                    if (imageNode != null)
                    {
                      imageNode.nextSibling.src = "ftv2folderopen.png";
                      if (imageNode.src.substring(l-13,l) == "ftv2pnode.png")
                      {
                        imageNode.src = "ftv2mnode.png";
                      }
                      else if (imageNode.src.substring(l-17,l) == "ftv2plastnode.png")
                      {
                        imageNode.src = "ftv2mlastnode.png";
                      }
                    }
                    folder.style.display = "block";
                  }
                }
             ]]>
          </script>
					<title>
						<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Sommaire']"/>
						<xsl:value-of select="$fieldheader"/>
	        </title>
	      </head>
				<body class="directory">
					<xsl:apply-templates mode="Titre" select="SalomeDynamique/ProjetVT"/>
					<xsl:apply-templates mode="Sommaire" select="SalomeDynamique/ProjetVT"/>
				</body>
			</html>
		</xsl:result-document>
		<xsl:result-document href="{$frame2}" format="xhtmlFormat">
			<html>
				<head>
					<link href="salome.css" rel="stylesheet" type="text/css"/>
					<link href="salome_print.css" rel="stylesheet" type="text/css" media="print" />
					<title>
						<!--Campagnes du projet-->
						<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_des_tests']"/>
						<xsl:value-of select="$fieldheader"/>
						:
	          			<xsl:value-of select="//ProjetVT/Nom/text()"/>
	       			</title>
	      		</head>
				<body>
					<xsl:apply-templates mode="Projet" select="SalomeDynamique/ProjetVT"/>
				</body>
			</html>
		</xsl:result-document>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Titre">
		<h1 id="sommaire">
			<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='SOMMAIRE']" />
			<xsl:value-of select="$fieldheader" />
		</h1>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Sommaire">
		<ol>
			<xsl:if test="$project = '1'">
				<li class="rootDirectory">
          <saxon:assign name="h1" select="my:accu($h1)" />
					<xsl:variable name="projectContent">
						<xsl:if test="Attachements">
							<li class="directory">
							  <img class="directory" src="ftv2node.png"/>
								<a href="{$frame2}#AttachProj" target="princ">
								  <span class="directoryNum">
									  <saxon:assign name="h2" select="my:accu($h2)" />
									  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
									</span>
									<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
									<xsl:value-of select="$fieldheader2" />
								</a>
							</li>
						</xsl:if>
						<xsl:if test="$existParam = 'true' and Params">
							<li class="directory">
							  <img class="directory" src="ftv2node.png"/>
								<a href="{$frame2}#paramProj" target="princ">
								  <span class="directoryNum">
									  <saxon:assign name="h2" select="my:accu($h2)" />
									  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
									</span>
									<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
									<xsl:value-of select="$fieldheader3" />
								</a>
							</li>
							<xsl:variable name="paramContent">
								<xsl:call-template name="extSummaryParam">
									<xsl:with-param name="current-node">
										<xsl:value-of select="saxon:path()" />
									</xsl:with-param>
								</xsl:call-template>
							</xsl:variable>
							<xsl:if test="count($paramContent//*)>0">
								<ol>
									<xsl:copy-of select="$paramContent" />
									<saxon:assign name="h3" select="0" />
								</ol>
							</xsl:if>
						</xsl:if>
						<xsl:call-template name="extSummaryProject">
							<xsl:with-param name="current-node">
								<xsl:value-of select="saxon:path()" />
							</xsl:with-param>
						</xsl:call-template>
					</xsl:variable>
					<xsl:choose>
            <xsl:when test="count($projectContent//*)>0">
              <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('ProjetVT', this)"/>
            </xsl:when>
            <xsl:otherwise>
              <img class="directory" src="ftv2node.png"/>
            </xsl:otherwise>
          </xsl:choose>
          <a href="{$frame2}#{Nom/text()}" target="princ">
            <span class="directoryNum">
              <xsl:value-of select="$h1" />&#160;
            </span>
            <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
            <xsl:value-of select="$fieldheader1" />
            &#160;:&#160;
            <xsl:value-of select="./Nom/text()" />
          </a>
					<xsl:if test="count($projectContent//*)>0">
					  <div id="ProjetVT" style="display: none;">
						  <ol>
  							<xsl:copy-of select="$projectContent" />
							  <saxon:assign name="h2" select="0" />
						  </ol>
						</div>
					</xsl:if>
				</li>
			</xsl:if>
			<xsl:if test="$environments = '1'">
				<xsl:apply-templates mode="Sommaire" select="Environnements" />
			</xsl:if>
			<xsl:call-template name="extSummaryRoot1">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="$testplan = '1'">
				<xsl:apply-templates mode="Sommaire" select="Familles" />
			</xsl:if>
			<xsl:call-template name="extSummaryRoot2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates mode="Sommaire" select="CampagneTests" />
			<xsl:call-template name="extSummaryRoot3">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</ol>
	</xsl:template>
	<xsl:template name="extSummaryParam">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryProject">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryRoot2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryRoot3">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="exist-param">
		<xsl:choose>
			<xsl:when test="/SalomeDynamique/ProjetVT/CampagneTests/CampagneTest/JeuxDonnees/JeuDonnees/ValeurParams/ValeurParam">
				<xsl:value-of select="true()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="boolean(/SalomeDynamique/ProjetVT/Environnements/Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]/ValeurParams/ValeurParam)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:variable name="existEnvironnement">
		<xsl:value-of select="boolean(//Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref])" />
	</xsl:variable>
	<xsl:template match="Environnements" mode="Sommaire">
		<xsl:if test="$existEnvironnement = 'true'">
			<li class="rootDirectory">
			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('EnvsProjet', this)"/>
				<a href="{$frame2}#EnvsProjet" target="princ">
				  <span class="directoryNum">
					  <saxon:assign name="h1" select="my:accu($h1)" />
					  <xsl:value-of select="$h1" />&#160;
					</span>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
					<xsl:value-of select="$fieldheader1" />
				</a>
				<div id="EnvsProjet" style="display: none;">
          <xsl:if test="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]">
            <ol>
              <xsl:apply-templates select="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]" mode="Sommaire" />
              <saxon:assign name="h2" select="0" />
            </ol>
          </xsl:if>
        </div>
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Environnement" mode="Sommaire">
	  <li class="directory">
	    <saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:variable name="environmentContent">
				<xsl:if test="Attachements">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_DocEnv" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
							</span>
							<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
							<xsl:value-of select="$fieldheader3" />
						</a>
					</li>
				</xsl:if>
				<xsl:if test="ValeurParams">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_ParamEnv" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
							</span>
							<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
							<xsl:value-of select="$fieldheader4" />
						</a>
					</li>
				</xsl:if>
				<xsl:if test="Script">
					<li class="directory">
					  <img class="directory" src="ftv2node.png"/>
						<a href="{$frame2}#{@idEnv}_Script" target="princ">
						  <span class="directoryNum">
							  <saxon:assign name="h3" select="my:accu($h3)" />
							  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
							</span>
							<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script']" />
							<xsl:value-of select="$fieldheader5" />
						</a>
					</li>
				</xsl:if>
				<xsl:call-template name="extSummaryEnvironment">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:choose>
        <xsl:when test="count($environmentContent//*)>0">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@idEnv}', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@idEnv}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
          <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
          <xsl:value-of select="$fieldheader2" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="./Nom/text()" />
      </a>
			<xsl:if test="count($environmentContent//*)>0">
			  <div id="{@idEnv}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$environmentContent" />
					  <saxon:assign name="h3" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryEnvironment">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:variable name="existFamille">
		<xsl:value-of select="boolean(//Famille[@id_famille = //FamilleRef/@ref])" />
	</xsl:variable>
	<xsl:template match="Familles" mode="Sommaire">
		<xsl:if test="$existFamille = 'true'">
			<li class="rootDirectory">
			  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('Familles', this)"/>
				<a href="{$frame2}#DossierTests" target="princ">
				  <span class="directoryNum">
					  <saxon:assign name="h1" select="my:accu($h1)" />
					  <xsl:value-of select="$h1" />&#160;
					</span>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
					<xsl:value-of select="$fieldheader1" />
				</a>
				<div id="Familles" style="display: none;">
				  <ol>
  					<xsl:apply-templates mode="Sommaire" select="./Famille[@id_famille=ancestor::ProjetVT/CampagneTests/CampagneTest/FamillesCamp/FamilleRef/@ref]" />
				  </ol>
				</div>
				<saxon:assign name="h2" select="0" />
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Famille" mode="Sommaire">
		<li class="directory">
		  <saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:variable name="familyContent">
				<xsl:call-template name="extSummaryFamily1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:apply-templates mode="Sommaire" select="SuiteTests/SuiteTest[@id_suite=//SuiteTestsCamp/SuiteTestRef/@ref]" />
				<xsl:call-template name="extSummaryFamily2">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
      <xsl:choose>
        <xsl:when test="count($familyContent//*)>0">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_famille}', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@id_famille}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($familyContent//*)>0">
			  <div id="{@id_famille}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$familyContent" />
					  <saxon:assign name="h3" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryFamily1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummaryFamily2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="SuiteTest" mode="Sommaire">
		<li class="directory">
      <saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:variable name="suiteContent">
				<xsl:call-template name="extSummarySuite1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:apply-templates mode="Sommaire" select="Tests/Test[@id_test=//TestsCamp/TestRef/@ref]" />
				<xsl:call-template name="extSummarySuite2">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
      <xsl:choose>
        <xsl:when test="count($suiteContent//*)>0">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_suite}', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png"/>
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@id_suite}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($suiteContent//*)>0">
			  <div id="{@id_suite}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$suiteContent" />
					  <saxon:assign name="h4" select="0" />
					  <saxon:assign name="h5" select="0" />
				  </ol>
				</div>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummarySuite1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSummarySuite2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Test" mode="Sommaire">
		<li class="directory">
		  <img class="directory" src="ftv2node.png"/>
			<a href="{$frame2}#{@id_test}" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h4" select="my:accu($h4)" />
				  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
				  <xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
				  <xsl:value-of select="$fieldheader" />
				  &#160;:&#160;
				</span>
				<xsl:value-of select="Nom/text()" />
			</a>
			<xsl:variable name="extContent">
				<xsl:call-template name="extSummaryTest">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="count($extContent//*)>0">
				<ol>
					<xsl:copy-of select="$extContent" />
				</ol>
				<saxon:assign name="h5" select="0" />
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTests" mode="Sommaire">
		<xsl:if test="$campaigns = '1'">
			<li class="rootDirectory">
			  <xsl:choose>
			    <xsl:when test="CampagneTest">
			      <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('Campagnes', this)" />
			    </xsl:when>
			    <xsl:otherwise>
			      <img class="directory" src="ftv2node.png" />
			    </xsl:otherwise>
			  </xsl:choose>
				<a href="{$frame2}#CampTests" target="princ">
				  <span class="directoryNum">
					  <saxon:assign name="h1" select="my:accu($h1)" />
					  <xsl:value-of select="$h1" />&#160;
					</span>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNES_DU_PROJET']" />
					<xsl:value-of select="$fieldheader1" />
				</a>
				<xsl:if test="CampagneTest">
				  <div id="Campagnes" style="display: none;">
					  <ol>
  						<xsl:apply-templates mode="Sommaire" select="./CampagneTest" />
					  </ol>
					</div>
				</xsl:if>
				<saxon:assign name="h2" select="0" />
			</li>
		</xsl:if>
		<xsl:if test="$executionreport = '1'">
			<li class="rootDirectory">
			  <xsl:choose>
			    <xsl:when test="CampagneTest//ResulExecCampTest">
            <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('ExecutionReport', this)"/>
			    </xsl:when>
			    <xsl:otherwise>
            <img class="directory" src="ftv2node.png" />
			    </xsl:otherwise>
			  </xsl:choose>
				<a href="{$frame2}#resulExec" target="princ">
				  <span class="directoryNum">
					  <saxon:assign name="h1" select="my:accu($h1)" />
					  <xsl:value-of select="$h1" />&#160;
					</span>
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='RAPPORT_D_EXECUTION']" />
					<xsl:value-of select="$fieldheader2" />
				</a>
				<xsl:if test="CampagneTest//ResulExecCampTest">
				  <div id="ExecutionReport" style="display: none;">
					  <ol>
  						<xsl:apply-templates select="./CampagneTest[.//ResulExecCampTest]" mode="SommaireExecReport" />
					  </ol>
					</div>
				</xsl:if>
				<saxon:assign name="h2" select="0" />
			</li>
		</xsl:if>
	</xsl:template>
	<xsl:template match="CampagneTest" mode="Sommaire">
		<li class="directory">
		  <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_camp}', this)"/>
			<a href="{$frame2}#{@id_camp}" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h2" select="my:accu($h2)" />
				  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
				  <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
				  <xsl:value-of select="$fieldheader1" />
				  &#160;:&#160;
				</span>
				<xsl:value-of select="Nom/text()" />
			</a>
			<div id="{@id_camp}" style="display: none;">
			  <ol>
  				<li class="directory">
  				  <img class="directory" src="ftv2node.png" />
					  <a href="{$frame2}#{@id_camp}_details" target="princ">
					    <span class="directoryNum">
  						  <saxon:assign name="h3" select="my:accu($h3)" />
						    <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
						  </span>
						  <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Details']" />
						  <xsl:value-of select="$fieldheader2" />
					  </a>
				  </li>
				  <xsl:call-template name="extSummaryCamp">
  					<xsl:with-param name="current-node">
						  <xsl:value-of select="saxon:path()" />
					  </xsl:with-param>
				  </xsl:call-template>
				  <xsl:apply-templates select="ExecCampTests/ExecCampTest" mode="Sommaire" />
			  </ol>
			</div>
			<saxon:assign name="h3" select="0" />
		</li>
	</xsl:template>
	<xsl:template name="extSummaryCamp">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ExecCampTest" mode="Sommaire">
		<xsl:param name="levelCampParam" />
		<li class="directory">
      <saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:variable name="extContent">
				<xsl:call-template name="extSummaryExec">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
      <xsl:choose>
        <xsl:when test="count($extContent//*)>0">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_exec_camp}', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png" />
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@id_exec_camp}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
          <xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
          <xsl:value-of select="$fieldheader" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($extContent//*)>0">
			  <div id="{@id_exec_camp}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$extContent" />
				  </ol>
				</div>
				<saxon:assign name="h5" select="0" />
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryExec">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTest" mode="SommaireExecReport">
		<li class="directory">
		  <xsl:choose>
        <xsl:when test="//ResulExecCampTest">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_camp}_execReport', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png" />
        </xsl:otherwise>
      </xsl:choose>
			<a href="{$frame2}#{@id_camp}_execReport" target="princ">
			  <span class="directoryNum">
				  <saxon:assign name="h2" select="my:accu($h2)" />
				  <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
				  <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
				  <xsl:value-of select="$fieldheader1" />
				  &#160;:&#160;
				</span>
				<xsl:value-of select="Nom/text()" />
			</a>
			<xsl:if test="//ResulExecCampTest">
			  <div id="{@id_camp}_execReport" style="display: none;">
				  <ol>
  					<xsl:apply-templates select=".//ResulExecCampTest" mode="Sommaire" />
				  </ol>
				</div>
			</xsl:if>
			<saxon:assign name="h3" select="0" />
		</li>
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="Sommaire">
		<li class="directory">
			<xsl:variable name="extContent">
				<xsl:call-template name="extSummaryResExec">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
		  <xsl:choose>
        <xsl:when test="count($extContent//*)>0">
          <img class="directory" src="ftv2pnode.png" onclick="toggleFolder('{@id_exec_res}', this)"/>
        </xsl:when>
        <xsl:otherwise>
          <img class="directory" src="ftv2node.png" />
        </xsl:otherwise>
      </xsl:choose>
      <a href="{$frame2}#{@id_exec_res}" target="princ">
        <span class="directoryNum">
          <xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
          <xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
          <xsl:value-of select="$fieldheader1" />
          &#160;:&#160;
        </span>
        <xsl:value-of select="Nom/text()" />
      </a>
			<xsl:if test="count($extContent//*)>0">
			  <div id="{@id_exec_res}" style="display: none;">
				  <ol>
  					<xsl:copy-of select="$extContent" />
				  </ol>
				</div>
				<saxon:assign name="h5" select="0" />
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extSummaryResExec">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Projet">
		<saxon:assign name="h1" select="0" />
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:if test="$project = '1'">
			<h1>
				<a name="{Nom/text()}" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
				<xsl:value-of select="$fieldheader1" />
				&#160;:&#160;
				<xsl:value-of select="./Nom/text()" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
					<xsl:value-of select="$fieldheader2" />
					&#160;:&#160;
					<xsl:value-of select="./Nom/text()" />
				</span>
			</h1>
			<xsl:apply-templates select="Description" mode="p" />
			<xsl:apply-templates select="Attachements" mode="ProjetVT" />
			<xsl:apply-templates select="Params" mode="ProjetVT" />
			<xsl:call-template name="extProject">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<xsl:if test="$environments = '1'">
			<xsl:apply-templates mode="Projet" select="Environnements" />
		</xsl:if>
		<xsl:call-template name="extRoot1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="$testplan = '1'">
			<xsl:apply-templates mode="Projet" select="Familles" />
		</xsl:if>
		<xsl:call-template name="extRoot2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates mode="Projet" select="CampagneTests" />
		<xsl:call-template name="extRoot3">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extProject">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot3">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="ProjetVT">
		<h2>
			<a name="AttachProj" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
			<xsl:value-of select="$fieldheader4" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="." mode="Projet" />
		<xsl:apply-templates select="FileAttachement" mode="inclusion" />
	</xsl:template>
	<xsl:template match="Params" mode="ProjetVT">
		<xsl:if test="$existParam = 'true'">
			<h2>
				<a name="paramProj" />
				<saxon:assign name="h2" select="my:accu($h2)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
				<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
				<xsl:value-of select="$fieldheader5" />
			</h2>
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<table class="tab">
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$description" />
					</th>
				</tr>
				<xsl:apply-templates mode="Projet"
					select="Param[@id_param=//Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]/ValeurParams/ValeurParam/@ref] | Param[@id_param=//JeuDonnees/ValeurParams/ValeurParam/@ref]" />
			</table>
			<xsl:call-template name="extParam">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	<xsl:template name="extParam">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Param" mode="Projet">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<xsl:value-of select="Nom/text()" />
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:copy-of select="Description/* | Description/text()" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param	name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text	disable-output-escaping="no">
							&#160;
						</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Environnements" mode="Projet">
		<xsl:if test="$existEnvironnement = 'true'">
			<br />
			<hr />
			<h1>
				<a name="EnvsProjet" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
				<xsl:value-of select="$fieldheader1" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
					<xsl:value-of select="$fieldheader2" />
				</span>
			</h1>
			<saxon:assign name="h2" select="0" />
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<table class="tab">
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$description" />
					</th>
				</tr>
				<xsl:apply-templates select="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]" mode="Environnements" />
			</table>
			<xsl:apply-templates select="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Environnement" mode="Environnements">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{@idEnv}">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:copy-of select="Description/* | Description/text()" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param	name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text	disable-output-escaping="no">
							&#160;
						</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="Environnement" mode="Projet">
		<h2>
			<a name="{@idEnv}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:if test="Attachements">
			<h3>
				<a name="{@idEnv}_DocEnv" />
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader3" />
			</h3>
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:if test="ValeurParams">
			<h3>
				<a name="{@idEnv}_ParamEnv" />
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
				<xsl:value-of select="$fieldheader4" />
			</h3>
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<table class="tab">
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
						<xsl:value-of select="$fieldheader6" />
					</th>
				</tr>
				<xsl:for-each select="ValeurParams/ValeurParam">
					<tr>
						<td class="tab_center">
							<xsl:number value="position()" format="1" />
						</td>
						<td class="tab">
							<a href="#paramProj">
								<xsl:value-of select="Nom/text()" />
							</a>
						</td>
						<td class="tab">
							<xsl:value-of select="@valeur" />
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
		<xsl:if test="Script">
			<h3>
				<a name="{@idEnv}_Script" />
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script']" />
				<xsl:value-of select="$fieldheader7" />
			</h3>
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<p>
				<b>
					<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_d_environnement']" />
					<xsl:value-of select="$fieldheader8" />
				</b>
				<br />
				<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
				<xsl:value-of select="$fieldheader9" />
				&#160;:&#160;
				<xsl:choose>
					<xsl:when test="Script/Text">
						<a href="{Script/@dir}" target="_blank">
							<xsl:value-of select="Script/@nom" />
						</a>
						<span class="lien_inclus">
							[
						</span>
						<a href="#{Script/@dir}" class="lien_inclus">
							<xsl:value-of select="$voir" />
						</a>
						<span class="lien_inclus">
						  ]
						</span>
					</xsl:when>
					<xsl:otherwise>
						<a href="{Script/@dir}" target="_blank">
							<xsl:value-of select="Script/@nom" />
						</a>
					</xsl:otherwise>
				</xsl:choose>
				<br />
				<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
				<xsl:value-of select="$fieldheader11" />
				&#160;:&#160;
				<xsl:value-of select="Script/Classpath" />
			</p>
			<xsl:apply-templates select="Script" mode="inclusion" />
		</xsl:if>
		<xsl:call-template name="extEnvironment">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extEnvironment">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="Projet">
		<xsl:param name="caption"/>
		<xsl:param name="filtreAnomalie" />
		<xsl:if test="$filtreAnomalie != 'true' or FileAttachement or (UrlAttachement and count(UrlAttachement) != count(UrlAttachement[contains(Description/text(), '_ATTACH]')]))">
			<table class="tab">
				<xsl:if test="$caption = 'true'">
					<caption>
						<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
						<xsl:value-of select="$fieldheader1" />
					</caption>
				</xsl:if>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$description" />
					</th>
				</tr>
				<xsl:for-each select="UrlAttachement[not(contains(Description/text(), '_ATTACH]'))]">
					<tr>
						<td class="tab_center">
							<xsl:value-of select="count(preceding-sibling::UrlAttachement)+1"/>
						</td>
						<td class="tab">
							<a href="{@url}" target="_blank">
								<xsl:value-of select="@url" />
							</a>
						</td>
						<td class="tab">
							<xsl:choose>
								<xsl:when test="Description[@isHTML='true']">
									<xsl:copy-of select="Description/* | Description/text()" />
								</xsl:when>
								<xsl:when test="Description">
									<xsl:call-template name="replaceByBr">
										<xsl:with-param	name="textToReplace" select="Description/text()" />
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text	disable-output-escaping="no">
										&#160;
									</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
				<xsl:for-each select="FileAttachement">
					<tr>
						<td class="tab_center">
							<xsl:value-of select="count(../UrlAttachement)+count(preceding-sibling::FileAttachement)+1"/>
						</td>
						<td class="tab">
							<xsl:apply-templates select="." mode="Projet" />
						</td>
						<td class="tab">
							<xsl:choose>
								<xsl:when test="Description[@isHTML='true']">
									<xsl:copy-of select="Description/* | Description/text()" />
								</xsl:when>
								<xsl:when test="Description">
									<xsl:call-template name="replaceByBr">
										<xsl:with-param	name="textToReplace" select="Description/text()" />
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text	disable-output-escaping="no">
										&#160;
									</xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="inclusion">
		<xsl:if test="$gif = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'gif'">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$jpeg = '1'">
			<xsl:if
				test="(substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg')">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$png = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'png'">
				<a name="{@dir}">
					<xsl:text disable-output-escaping="no">
						&#160;
					</xsl:text>
				</a>
				<table class="inclusTab">
					<tr>
						<th>
							<xsl:value-of select="@nom" />
						</th>
					</tr>
					<tr>
						<td class="inclusTab" align="center">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of select="@dir" />
								</xsl:attribute>
							</img>
						</td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>
		<xsl:if test="Text">
			<a name="{@dir}">
				<xsl:text disable-output-escaping="no">&#160;</xsl:text>
			</a>
			<table class="inclusTab">
				<tr>
					<th>
						<xsl:value-of select="@nom" />
					</th>
				</tr>
				<tr>
					<td class="inclusTab">
						<xsl:for-each select="Text/ligne">
							<xsl:value-of select="./text()" />
							<br />
						</xsl:for-each>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Script" mode="inclusion">
		<xsl:if test="Text">
			<a name="{@dir}">
				<xsl:text disable-output-escaping="no">&#160;</xsl:text>
			</a>
			<table class="inclusTab">
				<tr>
					<th>
						<xsl:value-of select="@nom" />
					</th>
				</tr>
				<tr>
					<td class="inclusTab">
						<xsl:for-each select="Text/ligne">
							<xsl:value-of select="./text()" />
							<br />
						</xsl:for-each>
					</td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Familles" mode="Projet">
		<xsl:if test="$existFamille = 'true'">
			<br />
			<hr />
			<h1>
				<a name="DossierTests" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
				<xsl:value-of select="$fieldheader5" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
					<xsl:value-of select="$fieldheader6" />
				</span>
			</h1>
			<saxon:assign name="h2" select="0" />
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<xsl:apply-templates select="./Famille[@id_famille=//FamillesCamp/FamilleRef/@ref]" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Famille" mode="Projet">
		<h2>
			<a name="{@id_famille}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()"></xsl:value-of>
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:if test="Attachements">
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader3" />
			</b>
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:call-template name="extFamily1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="SuiteTests/SuiteTest[@id_suite=//SuiteTestsCamp/SuiteTestRef/@ref]" mode="Projet" />
		<xsl:call-template name="extFamily2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extFamily1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extFamily2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="SuiteTest" mode="Projet">
		<h3>
			<a name="{@id_suite}"/>
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:if test="Attachements">
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
				<xsl:value-of select="$fieldheader3" />
			</b>
			<xsl:apply-templates select="Attachements" mode="Projet" />
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
		<xsl:call-template name="extSuite1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="Tests/Test[@id_test=//TestsCamp/TestRef/@ref]" mode="Projet" />
		<xsl:call-template name="extSuite2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extSuite1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSuite2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Test" mode="Projet">
		<h4>
			<a name="{@id_test}" />
			<saxon:assign name="h4" select="my:accu($h4)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h4>
		<saxon:assign name="h5" select="0" />
		<p>
			<xsl:apply-templates select="Description" mode="Projet" />
			<xsl:if test="Concepteur">
				<xsl:if test="Description">
					<br />
				</xsl:if>
				<b>
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Concepteur_du_test']" />
					<xsl:value-of select="$fieldheader2" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Concepteur/Nom/text()" />
			</xsl:if>
			<xsl:if test="Description or Concepteur">
				<br />
			</xsl:if>
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="TestAuto">
					<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Automatique']" />
					<xsl:value-of select="$fieldheader4" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Manuel']" />
					<xsl:value-of select="$fieldheader5" />
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="TestAuto">
				<br />
				<b>
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
					<xsl:value-of select="$fieldheader6" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="TestAuto/@plug_ext" />
			</xsl:if>
		</p>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:call-template name="extTest">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:variable name="idTest">
			<xsl:value-of select="@id_test" />
		</xsl:variable>
		<xsl:variable name="existCamp">
			<xsl:value-of select="boolean(//TestRef[@ref=$idTest])" />
		</xsl:variable>
		<xsl:if test="$existCamp = 'true'">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagnes_du_test']" />
					<xsl:value-of select="$fieldheader7" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execute']" />
						<xsl:value-of select="$fieldheader8" />
					</th>
				</tr>
				<xsl:apply-templates select="//CampagneTest[.//TestRef/@ref=$idTest]" mode="Test">
					<xsl:with-param name="idTest">
						<xsl:value-of select="$idTest" />
					</xsl:with-param>
				</xsl:apply-templates>
			</table>
			<br />
		</xsl:if>
		<xsl:if test="ParamsT">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
					<xsl:value-of select="$fieldheader9" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre">
						<xsl:value-of select="$translatedName" />
					</th>
				</tr>
				<xsl:apply-templates select="ParamsT/ParamT" mode="Projet" />
			</table>
			<br />
		</xsl:if>
		<xsl:if test="TestAuto">
			<xsl:apply-templates select="TestAuto" mode="Projet" />
			<xsl:apply-templates select="TestAuto/Script" mode="inclusion" />
		</xsl:if>
		<xsl:if test="TestManuel">
			<xsl:apply-templates select="TestManuel" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTest" mode="Test">
		<xsl:param name="idTest" />
		<tr>
			<td class="tab_center"><xsl:number value="position()" /></td>
			<td class="tab">
				<a href="#{@id_camp}">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test=".//ResulExec[@refTest=$idTest and ( @res = 'PASSED' or @res = 'FAILED' or @res = 'INCONCLUSIF' )]">
						<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Oui']" />
						<xsl:value-of select="$fieldheader1" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Non']" />
						<xsl:value-of select="$fieldheader2" />
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="ParamT" mode="Projet">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" />
			</td>
			<td class="tab">
				<a href="#paramProj">
					<xsl:value-of select="id(@ref)/Nom/text()" />
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="TestAuto" mode="Projet">
		<b>
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_de_test']" />
			<xsl:value-of select="$fieldheader1" />
		</b>
		<br />
		<b>
			<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
			<xsl:value-of select="$fieldheader2" />
			&#160;:&#160;
		</b>
		<xsl:choose>
			<xsl:when test="Script/Text">
				<a href="{Script/@dir}" target="_blank">
					<xsl:value-of select="Script/@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{Script/@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
				<br />
			</xsl:when>
			<xsl:otherwise>
				<a href="{Script/@dir}" target="_blank">
					<xsl:value-of select="Script/@nom" />
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="TestManuel" mode="Projet">
		<xsl:if test="ActionTest">
			<b>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Actions_du_test']" />
				<xsl:value-of select="$fieldheader1" />
			</b>
			<ul class="actions1">
				<xsl:apply-templates select="ActionTest" mode="Projet" />
			</ul>
			<!-- TODO : Attachements des actions -->
			<xsl:apply-templates select="ActionTest/Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="ActionTest" mode="Projet">
		<li>
			<b>
				<xsl:value-of select="$translatedName" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="Nom/text()" />
			<xsl:variable name="actionContent">
				<xsl:apply-templates select="Description" mode="li" />
				<xsl:if test="ResultAttendu">
					<li>
						<b>
							<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
							<xsl:value-of select="$fieldheader4" />
							&#160;:&#160;
						</b>
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace"
								select="ResultAttendu/text()" />
						</xsl:call-template>
					</li>
				</xsl:if>
				<xsl:if test="Attachements">
					<li>
						<b>
							<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
							<xsl:value-of select="$fieldheader5" />
							&#160;:&#160;
						</b>
						<ul class="actions3">
							<xsl:for-each	select="Attachements/UrlAttachement">
								<li>
									<a href="{@url}" target="_blank">
										<xsl:value-of select="@url" />
									</a>
								</li>
							</xsl:for-each>
							<xsl:for-each	select="Attachements/FileAttachement">
								<li>
									<xsl:apply-templates select="." mode="Projet" />
								</li>
							</xsl:for-each>
						</ul>
					</li>
				</xsl:if>
				<xsl:call-template name="extAction">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:variable>
			<xsl:if test="count($actionContent//*)>0">
				<ul class="actions2">
					<xsl:copy-of select="$actionContent" />
				</ul>
			</xsl:if>
		</li>
	</xsl:template>
	<xsl:template name="extAction">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTests" mode="Projet">
		<xsl:if test="$campaigns = '1'">
			<br />
			<hr />
			<h1>
				<a name="CampTests" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNES_DU_PROJET']" />
				<xsl:value-of select="$fieldheader1" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNES_DU_PROJET']" />
					<xsl:value-of select="$fieldheader2" />
				</span>
			</h1>
			<saxon:assign name="h2" select="0" />
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<xsl:apply-templates mode="Projet" select="./CampagneTest" />
		</xsl:if>
		<xsl:if test="$executionreport = '1'">
			<br />
			<hr />
			<h1>
				<a name="resulExec" />
				<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='RAPPORT_D_EXECUTION']" />
				<xsl:value-of select="$fieldheader3" />
				<span>
					<xsl:value-of select="$h1" />&#160;
					<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='RAPPORT_D_EXECUTION']" />
					<xsl:value-of select="$fieldheader4" />
				</span>
			</h1>
			<saxon:assign name="h2" select="0" />
			<saxon:assign name="h3" select="0" />
			<saxon:assign name="h4" select="0" />
			<saxon:assign name="h5" select="0" />
			<xsl:apply-templates mode="ProjetExecReport" select="./CampagneTest[.//ResulExecCampTest]" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="CampagneTest" mode="Projet">
		<h2>
		<a name="{@id_camp}" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
			<xsl:value-of select="$fieldheader1" />
			:
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<h3>
			<a name="{@id_camp}_details" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Details']" />
			<xsl:value-of select="$fieldheader3" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:call-template name="extCampaign1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="FamillesCamp/FamilleRef">
			<b>
				<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Arborescence_de_test']" />
				<xsl:value-of select="$fieldheader4" />
			</b>
			<ul class="tests1">
				<xsl:for-each select="FamillesCamp/FamilleRef">
					<li>
						<a href="#{@ref}">
							<xsl:value-of select="Nom/text()" />
						</a>
						<ul class="tests2">
							<xsl:for-each select="SuiteTestsCamp/SuiteTestRef">
							<li>
								<a href="#{@ref}">
									<xsl:value-of select="Nom/text()" />
								</a>
								<ul class="tests3">
									<xsl:for-each select="TestsCamp/TestRef">
										<li>
											<a href="#{@ref}">
												<xsl:value-of select="Nom/text()" />
											</a>
										</li>
									</xsl:for-each>
								</ul>
							</li>
							</xsl:for-each>
						</ul>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
		<xsl:call-template name="extCampaign2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates select="ExecCampTests/ExecCampTest" mode="Projet" />
	</xsl:template>
	<xsl:template name="extCampaign1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extCampaign2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ExecCampTest" mode="Projet">
		<h3>
			<a name="{@id_exec_camp}" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
			<xsl:value-of select="$fieldheader1" />
			:
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select="Description" mode="p" />
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:if test="JeuDonneesEx">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Jeu_de_donnees']" />
					<xsl:value-of select="$fieldheader2" />
					&#160;:&#160;
					<xsl:value-of select="JeuDonneesEx/Nom/text()" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
				</tr>
				<xsl:variable name="idJeu">
					<xsl:value-of select="JeuDonneesEx/@ref" />
				</xsl:variable>
				<xsl:variable name="id_env">
					<xsl:value-of select="EnvironnementEx/@ref" />
				</xsl:variable>
				<xsl:apply-templates select="//JeuDonnees[@id_jeu=$idJeu]/ValeurParams/ValeurParam" mode="ExecCampTest">
					<xsl:with-param name="idJeu">
						<xsl:value-of select="$idJeu" />
					</xsl:with-param>
					<xsl:with-param name="id_env">
						<xsl:value-of select="$id_env" />
					</xsl:with-param>
				</xsl:apply-templates>
			</table>
			<br />
			<xsl:call-template name="extDataSet">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
		<p>
      <b>
        <xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
        <xsl:value-of select="$fieldheader3" />
        &#160;:&#160;
      </b>
      <a href="#{EnvironnementEx/@ref}">
        <xsl:value-of select="id(EnvironnementEx/@ref)/Nom/text()" />
      </a>
      <xsl:if test="Script[@type='PRE_SCRIPT']">
        <xsl:variable name="dir">
          <xsl:value-of select="Script[@type='PRE_SCRIPT']/@dir" />
        </xsl:variable>
        <xsl:variable name="nom">
          <xsl:value-of select="Script[@type='PRE_SCRIPT']/@nom" />
        </xsl:variable>
        <br/>
        <b>Pre-Script : </b>
        <xsl:choose>
          <xsl:when test="Script[@type='PRE_SCRIPT']/Text">
            <a href="{$dir}" target="_blank">
              <xsl:value-of select="$nom" />
            </a>
            &#160;[
            <a href="#{$dir}">
              <xsl:value-of select="$voir" />
            </a>
            ]
          </xsl:when>
          <xsl:otherwise>
            <a href="{$dir}" target="_blank">
              <xsl:value-of select="$nom" />
            </a>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:if test="Script[@type='POST_SCRIPT']">
        <xsl:variable name="dir">
          <xsl:value-of select="Script[@type='POST_SCRIPT']/@dir" />
        </xsl:variable>
        <xsl:variable name="nom">
          <xsl:value-of select="Script[@type='POST_SCRIPT']/@nom" />
        </xsl:variable>
        <br/>
        <b>Post-Script : </b>
        <xsl:choose>
          <xsl:when test="Script[@type='POST_SCRIPT']/Text">
            <a href="{$dir}" target="_blank">
              <xsl:value-of select="$nom" />
            </a>
            &#160;[
            <a href="#{$dir}">
              <xsl:value-of select="$voir" />
            </a>
            ]
          </xsl:when>
          <xsl:otherwise>
            <a href="{$dir}" target="_blank">
              <xsl:value-of select="$nom" />
            </a>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </p>
    <xsl:apply-templates select="Script[@type='PRE_SCRIPT']" mode="inclusion"/>
    <xsl:apply-templates select="Script[@type='POST_SCRIPT']" mode="inclusion"/>
		<xsl:if test="ResulExecCampTests">
      <br />
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
					<xsl:value-of select="$fieldheader4" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:value-of select="$translatedName" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statistique']" />
						<xsl:value-of select="$fieldheader5" />
					</th>
				</tr>
				<xsl:apply-templates select="ResulExecCampTests/ResulExecCampTest" mode="ExecCampTest" />
			</table>
		</xsl:if>
		<xsl:call-template name="extExec">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extDataSet">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extExec">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ValeurParam" mode="ExecCampTest">
		<xsl:param name="idJeu" />
		<xsl:param name="id_env" />
		<xsl:variable name="valeurParam">
			<xsl:value-of select="@valeur" />
		</xsl:variable>
		<xsl:variable name="idParam">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#paramProj">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="not($valeurParam = '$(env_value)$')">
						<xsl:value-of select="$valeurParam" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="//Environnement[@idEnv=$id_env]/ValeurParams/ValeurParam[@ref=$idParam]/@valeur" />
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="ExecCampTest">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{@id_exec_res}">
					<xsl:value-of select="Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<span class="passed">
					Pass :
					<xsl:value-of select="count(ResulExecs/ResulExec[@res='PASSED'])" />
				</span>
				,
				<span class="failed">
					Fail :
					<xsl:value-of select="count(ResulExecs/ResulExec[@res='FAILED'])" />
				</span>
				,
				<span class="inconclusif">
					<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
					<xsl:value-of select="$fieldheader" /> :
					<xsl:value-of select="count(ResulExecs/ResulExec[@res='INCONCLUSIF'])" />
				</span>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="CampagneTest" mode="ProjetExecReport">
		<h2>
			<a name="{@id_camp}_execReport" />
			<saxon:assign name="h2" select="my:accu($h2)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h2>
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates select=".//ResulExecCampTest" mode="Projet" />
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="Projet">
		<xsl:variable name="idCamp">
			<xsl:value-of select="../../../../@id_camp" />
		</xsl:variable>
		<xsl:variable name="idExec">
			<xsl:value-of select="../../@id_exec_camp" />
		</xsl:variable>
		<xsl:variable name="idExecRes">
			<xsl:value-of select="@id_exec_res" />
		</xsl:variable>
		<h3>
			<a name="{@id_exec_res}" />
			<saxon:assign name="h3" select="my:accu($h3)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="Nom/text()" />
		</h3>
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<p>
			<b>
				<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
				<xsl:value-of select="$fieldheader2" />
				&#160;:&#160;
			</b>
			<a href="#{$idCamp}">
				<xsl:value-of select="../../../../Nom/text()" />
			</a>
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<a href="#{$idExec}">
				<xsl:value-of select="../../Nom/text()" />
			</a>
		</p>
		<h4>
			<a name="{@id_exec_res}_resume" />
			<saxon:assign name="h4" select="my:accu($h4)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
			<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume_de_l_execution']" />
			<xsl:value-of select="$fieldheader4" />
		</h4>
		<saxon:assign name="h5" select="0" />
		<p>
			<xsl:if test="Date_crea">
				<b>
					<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Date_d_execution']" />
					<xsl:value-of select="$fieldheader5" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Date_crea/text()" />
				<br />
			</xsl:if>
			<b>
				<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statistique']" />
				<xsl:value-of select="$fieldheader6" />
				&#160;:&#160;
			</b>
			<span class="passed">
				Pass :
				<xsl:value-of select="count(ResulExecs/ResulExec[@res='PASSED'])" />
			</span>
			,
			<span class="failed">
				Fail :
				<xsl:value-of select="count(ResulExecs/ResulExec[@res='FAILED'])" />
			</span>
			,
			<span class="inconclusif">
				<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
				<xsl:value-of select="$fieldheader7" /> :
				<xsl:value-of select="count(ResulExecs/ResulExec[@res='INCONCLUSIF'])" />
			</span>
		</p>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:call-template name="extResExec1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:variable name="anomalie">
			<xsl:value-of select="boolean(.//ResulExec/Attachements/UrlAttachement[starts-with(Description/text(), '[')
						and substring(Description/text(), string-length(Description/text())) = ']'])" />
		</xsl:variable>
		<xsl:if test="$anomalie = 'true'">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
					<xsl:value-of select="$fieldheader11" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='Tracker']" />
						<xsl:value-of select="$fieldheader12" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Lien']" />
						<xsl:value-of select="$fieldheader13" />
					</th>
				</tr>
				<xsl:apply-templates select=".//ResulExec/Attachements/UrlAttachement[starts-with(Description/text(), '[')
						and substring(Description/text(), string-length(Description/text())) = ']']" mode="ficheAnomalie" />
			</table>
			<br />
		</xsl:if>
		<xsl:variable name="lien_test">
			<xsl:value-of select="concat('testresult_', @id_exec_res)" />
		</xsl:variable>
		<xsl:if test="ResulExecs/ResulExec">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader14" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultats_des_tests']" />
					<xsl:value-of select="$fieldheader14" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="23%">
						<xsl:variable name="fieldheader15" select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
						<xsl:value-of select="$fieldheader15" />
					</th>
					<th class="titre" width="23%">
						<xsl:variable name="fieldheader16" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
						<xsl:value-of select="$fieldheader16" />
					</th>
					<th class="titre" width="23%">
						<xsl:variable name="fieldheader17" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
						<xsl:value-of select="$fieldheader17" />
					</th>
					<th class="titre" width="21%">
						<xsl:variable name="fieldheader18" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
						<xsl:value-of select="$fieldheader18" />
					</th>
				</tr>
				<xsl:apply-templates select="ResulExecs/ResulExec" mode="resultatTests">
					<xsl:with-param name="lien_test">
						<xsl:value-of select="$lien_test" />
					</xsl:with-param>
				</xsl:apply-templates>
				</table>
		</xsl:if>
		<h4>
			<a name="{@id_exec_res}_detail" />
			<saxon:assign name="h4" select="my:accu($h4)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />&#160;
			<xsl:variable name="fieldheader19" select="$translations/allheadings/headings[lang($local)]/heading[@category='Details_de_l_execution']" />
			<xsl:value-of select="$fieldheader19" />
		</h4>
		<saxon:assign name="h5" select="0" />
		<xsl:apply-templates mode="Projet" select="ResulExecs/ResulExec" />
	</xsl:template>
	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ResulExec" mode="resultatTests">
		<xsl:param name="lien_test" />
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{id(@refTest)/ancestor::Famille/@id_famille}">
					<xsl:value-of select="RefTest/NomFamille/text()" />
				</a>
			</td>
			<td class="tab">
				<a href="#{id(@refTest)/ancestor::SuiteTest/@id_suite}">
					<xsl:value-of select="RefTest/NomSuite/text()" />
				</a>
			</td>
			<td class="tab">
				<a name="{concat($lien_test, '_', @refTest)}" />
				<a href="#{@refTest}">
					<xsl:value-of select="RefTest/NomTest/text()" />
				</a>
			</td>
			<td class="tab">
				<xsl:choose>
					<xsl:when test="@res = 'PASSED'">
						<span class="passed"> Pass </span>
					</xsl:when>
					<xsl:when test="@res = 'FAILED'">
						<span class="failed">	Fail </span>
					</xsl:when>
					<xsl:when test="@res = 'INCONCLUSIF'">
						<span class="inconclusif">
							<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
							<xsl:value-of select="$fieldheader1" />
						</span>
					</xsl:when>
				</xsl:choose>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="UrlAttachement" mode="ficheAnomalie">
		<xsl:variable name="desc">
			<xsl:value-of select="Description/text()" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<xsl:value-of select="substring(substring-before($desc, '_ATTACH'), 2)" />
			</td>
			<td class="tab">
				<a href="{@url}" target="_blank">
					<xsl:value-of select="@url" />
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="ResulExec" mode="Projet">
		<xsl:variable name="nomTest">
			<xsl:value-of select="id(@refTest)/Nom/text()" />
		</xsl:variable>
		<h5>
			<saxon:assign name="h5" select="my:accu($h5)" />
			<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />.<xsl:value-of select="$h4" />.<xsl:value-of select="$h5" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
			<xsl:value-of select="$fieldheader1" />
			&#160;:&#160;
			<xsl:value-of select="$nomTest" />
		</h5>
		<p>
			<b>
				<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
				<xsl:value-of select="$fieldheader2" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="@res = 'PASSED'">
					<span class="passed"> PASS </span>
				</xsl:when>
				<xsl:when test="@res = 'FAILED'">
					<span class="failed">	FAIL </span>
				</xsl:when>
				<xsl:when test="@res = 'INCONCLUSIF'">
					<span class="inconclusif">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='INCONCLUSIF']" />
						<xsl:value-of select="$fieldheader3" />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pas_execute']" />
					<xsl:value-of select="$fieldheader4" />
				</xsl:otherwise>
			</xsl:choose>
			<br />
			<b>
				<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
				<xsl:value-of select="$fieldheader5" />
				&#160;:&#160;
			</b>
			<xsl:choose>
				<xsl:when test="id(@refTest)/TestAuto">
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Automatique']" />
					<xsl:value-of select="$fieldheader6" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Manuel']" />
					<xsl:value-of select="$fieldheader7" />
				</xsl:otherwise>
			</xsl:choose>
			<br />
			<b>
				<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
				<xsl:value-of select="$fieldheader8" />
				&#160;:&#160;
			</b>
			<a href="#{id(@refTest)/ancestor::Famille/@id_famille}">
				<xsl:value-of select="id(@refTest)/ancestor::Famille/Nom/text()" />
			</a>
			<br />
			<b>
				<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
				<xsl:value-of select="$fieldheader9" />
				&#160;:&#160;
			</b>
			<a href="#{id(@refTest)/ancestor::SuiteTest/@id_suite}">
				<xsl:value-of select="id(@refTest)/ancestor::SuiteTest/Nom/text()" />
			</a>
		</p>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
			<xsl:with-param name="filtreAnomalie">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
		<xsl:variable name="idTest">
			<xsl:value-of select="@refTest" />
		</xsl:variable>
		<xsl:call-template name="extResExec2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:variable name="anomalieTest">
			<xsl:value-of select="boolean(Attachements/UrlAttachement[starts-with(Description/text(), '[')
					and contains(Description/text(), '_ATTACH]')])" />
		</xsl:variable>
		<xsl:if test="$anomalieTest = 'true'">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
					<xsl:value-of select="$fieldheader12" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Tracker']" />
						<xsl:value-of select="$fieldheader13" />
					</th>
					<th class="titre" width="45%">
						<xsl:variable name="fieldheader14" select="$translations/allheadings/headings[lang($local)]/heading[@category='Lien']" />
						<xsl:value-of select="$fieldheader14" />
					</th>
				</tr>
				<xsl:apply-templates select="Attachements/UrlAttachement[starts-with(Description/text(), '[')
					and contains(Description/text(), '_ATTACH]')]" mode="detailExec" />
			</table>
			<br />
		</xsl:if>
		<xsl:apply-templates select="id($idTest)/TestAuto" mode="detailExec" />
		<xsl:if test="boolean(ancestor::ResulExecCampTest/ResulActionTests/ResulActionTest[@refAction = id($idTest)/TestManuel/ActionTest/@id_action])">
			<b>
				<xsl:variable name="fieldheader15" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution_des_actions_de_test']" />
				<xsl:value-of select="$fieldheader15" />
			</b>
			<br />
			<ul class="actions1">
				<xsl:apply-templates select="ancestor::ResulExecCampTest/ResulActionTests/ResulActionTest[@refAction = id($idTest)/TestManuel/ActionTest/@id_action]" mode="detailExec" />
			</ul>
		</xsl:if>
	</xsl:template>
	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ResulActionTest" mode="detailExec">
		<li>
			<b>
				<xsl:value-of select="$translatedName" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="id(@refAction)/Nom/text()" />
			<ul class="actions2">
				<xsl:if test="Description and Description/text()!=''">
					<xsl:apply-templates select="Description" mode="li" />
				</xsl:if>
				<xsl:if test="ResultAttendu and ResultAttendu/text()!=''">
					<li>
						<b>
							<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
							<xsl:value-of select="$fieldheader1" />
							&#160;:&#160;
						</b>
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace" select="ResultAttendu/text()" />
						</xsl:call-template>
					</li>
					<li>
						<b>
							<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_effectif']" />
							<xsl:value-of select="$fieldheader2" />
							&#160;:&#160;
						</b>
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace" select="ResulEffectif/text()" />
						</xsl:call-template>
					</li>
				</xsl:if>
				<li>
					<b>
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
						<xsl:value-of select="$fieldheader3" />
						&#160;:&#160;
					</b>
					<xsl:choose>
						<xsl:when test="@res = 'PASSED'">
							<span class="passed"> PASS </span>
						</xsl:when>
						<xsl:when test="@res = 'FAILED'">
							<span class="failed">	FAIL </span>
						</xsl:when>
						<xsl:when test="@res = 'INCONCLUSIF'">
							<span class="inconclusif">
								<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='INCONCLUSIF']" />
								<xsl:value-of select="$fieldheader4" />
							</span>
						</xsl:when>
						<xsl:otherwise>
							<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Pas_execute']" />
							<xsl:value-of select="$fieldheader5" />
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</ul>
		</li>
	</xsl:template>
	<xsl:template match="TestAuto" mode="detailExec">
		<b>
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution_du_script_de_test']" />
			<xsl:value-of select="$fieldheader1" />
		</b>
		<br />
		<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
		<xsl:value-of select="$fieldheader2" />
		:
		<a href="{Script/@dir}" target="_blank">
			<xsl:value-of select="Script/@nom" />
		</a>
		<br />
		<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
		<xsl:value-of select="$fieldheader3" />
		:
		<xsl:value-of select="@plug_ext" />
	</xsl:template>
	<xsl:template match="UrlAttachement" mode="detailExec">
		<xsl:variable name="desc">
			<xsl:value-of select="Description/text()" />
		</xsl:variable>
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<xsl:value-of select="substring(substring-before($desc, '_ATTACH'), 2)" />
			</td>
			<td class="tab">
				<a href="{@url}" target="_blank">
					<xsl:value-of select="@url" />
				</a>
			</td>
		</tr>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="Projet">
		<xsl:choose>
			<xsl:when	test="($gif = '1') and (substring-after(@nom, '.') = 'gif')">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when	test="($jpeg = '1') and ((substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg'))">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when	test="($png = '1') and (substring-after(@nom, '.') = 'png')">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:when test="Text">
				<a href="{@dir}" target="_blank">
					<xsl:value-of select="@nom" />
				</a>
				<span class="lien_inclus">
					[
				</span>
				<a href="#{@dir}" class="lien_inclus">
					<xsl:value-of select="$voir" />
				</a>
				<span class="lien_inclus">
				  ]
				</span>
			</xsl:when>
			<xsl:otherwise>
				<a href="{@dir}" target="_blank">
					<xsl:value-of	select="@nom" />
				</a>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Description" mode="p">
		<p>
			<xsl:apply-templates select="." mode="Projet" />
		</p>
	</xsl:template>
	<xsl:template match="Description" mode="li">
		<li>
			<xsl:apply-templates select="." mode="Projet" />
		</li>
	</xsl:template>
	<xsl:template match="Description" mode="Projet">
		<xsl:param name="withBr" />
		<b>
			<xsl:value-of select="$description" />
			&#160;:&#160;
		</b>
		<xsl:choose>
			<xsl:when test="@isHTML='true'">
				<br />
				<xsl:copy-of select="* | text()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="replaceByBr">
					<xsl:with-param name="textToReplace" select="./text()" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$withBr = 'true'">
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template name="replaceByBr">
		<xsl:param name="textToReplace" />
		<xsl:choose>
			<xsl:when test="contains($textToReplace, '\n')">
				<xsl:value-of
					select="substring-before($textToReplace,'\n')" />
				<br />
				<xsl:call-template name="replaceByBr">
					<xsl:with-param name="textToReplace"
						select="substring-after($textToReplace,'\n')" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$textToReplace" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:function name="my:accu">
		<xsl:param name="accumulateur" />
		<xsl:value-of select="$accumulateur + 1" />
	</xsl:function>
</xsl:stylesheet>
