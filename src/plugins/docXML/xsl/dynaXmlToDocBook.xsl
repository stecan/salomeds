<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
	Document   : docbookdyna.xsl
	Created on : 17 ao\u00fbt 2007,10:38
	Author     : vapu8214
	Description:
	Purpose of transformation follows.
-->

<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/" exclude-result-prefixes="saxon my"
	extension-element-prefixes="saxon">
	<xsl:output method="xml"
		doctype-public="-//OASIS//DTD DocBook XML V4.4//EN"
		doctype-system="http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" />
	<xsl:param name="jpeg" />
	<xsl:param name="gif" />
	<xsl:param name="png" />
	<xsl:param name="project" />
	<xsl:param name="environments" />
	<xsl:param name="testplan" />
	<xsl:param name="campaigns" />
	<xsl:param name="executionreport" />
	<xsl:param name="local" />
	<xsl:param name="translate" />
	<xsl:param name="urlBase" />
	<xsl:variable name="translations" select="document($translate)" />
	<xsl:variable name="description">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Description']" />
	</xsl:variable>
	<xsl:variable name="voir">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Voir']" />
	</xsl:variable>
	<xsl:variable name="translatedName">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
	</xsl:variable>
	<xsl:variable name="existParam">
		<xsl:call-template name="exist-param" />
	</xsl:variable>

	<!-- TODO customize transformation rules syntax recommendation http://www.w3.org/TR/xslt -->

	<xsl:template match="/">
	  <article lang="{$local}">
		  <title>
        <!--Campagnes du projet-->
        <xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_des_tests']" />
        <xsl:value-of select="$fieldheader" />
        <emphasis>
          &#160;
          <xsl:value-of select="//ProjetVT/Nom/text()" />
        </emphasis>
      </title>
		  <xsl:apply-templates mode="Projet" select="/SalomeDynamique/ProjetVT" />
		</article>
	</xsl:template>
	<xsl:template name="exist-param">
		<xsl:choose>
			<xsl:when test="/SalomeDynamique/ProjetVT/CampagneTests/CampagneTest/JeuxDonnees/JeuDonnees/ValeurParams/ValeurParam">
				<xsl:value-of select="true()" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="boolean(/SalomeDynamique/ProjetVT/Environnements/Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]/ValeurParams/ValeurParam)" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Projet">
		<xsl:if test="$project = '1'">
			<section>
				<title>
					<xsl:variable name="fieldheader1"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
					<xsl:value-of select="$fieldheader1" />
					<emphasis>
						&#160;
						<xsl:value-of select="./Nom/text()" />
					</emphasis>
				</title>
				<xsl:apply-templates select="Description" mode="para" />
				<xsl:apply-templates select="Attachements" mode="ProjetVT" />
				<xsl:apply-templates select="Params" mode="ProjetVT" />
				<xsl:call-template name="extProject">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</section>
		</xsl:if>
		<xsl:if test="$environments = '1'">
			<xsl:apply-templates mode="Projet" select="Environnements" />
		</xsl:if>
		<xsl:call-template name="extRoot1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="$testplan = '1'">
			<xsl:apply-templates mode="Projet" select="Familles" />
		</xsl:if>
		<xsl:call-template name="extRoot2">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:apply-templates mode="Projet" select="CampagneTests" />
		<xsl:call-template name="extRoot3">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
	</xsl:template>
	<xsl:template name="extProject">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extRoot3">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="ProjetVT">
		<section>
			<title>
				<xsl:variable name="fieldheader4"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
				<xsl:value-of select="$fieldheader4" />
			</title>
			<xsl:apply-templates select="." mode="Projet" />
			<xsl:apply-templates select="FileAttachement" mode="inclusion" />
		</section>
	</xsl:template>
	<xsl:template match="Params" mode="ProjetVT">
		<xsl:if test="$existParam = 'true'">
			<section id="paramProj">
				<title>
					<xsl:variable name="fieldheader5"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
					<xsl:value-of select="$fieldheader5" />
				</title>
				<informaltable>
					<tgroup cols="3">
						<colspec align="center" colwidth="1*" />
						<colspec colwidth="4*" />
						<colspec colwidth="4*" />
						<thead>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry>N�</entry>
								<entry>
									<xsl:value-of
										select="$translatedName" />
								</entry>
								<entry>
									<xsl:value-of select="$description" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:apply-templates mode="Projet"
								select="Param[@id_param=//Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]/ValeurParams/ValeurParam/@ref] | Param[@id_param=//JeuDonnees/ValeurParams/ValeurParam/@ref]" />
							<xsl:call-template name="extParam">
								<xsl:with-param name="current-node">
									<xsl:value-of select="saxon:path()" />
								</xsl:with-param>
							</xsl:call-template>
						</tbody>
					</tgroup>
				</informaltable>
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template name="extParam">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Param" mode="Projet">
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<xsl:value-of select="Nom/text()" />
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:apply-templates
							select="Description/* | Description/text()" mode="html2docbook" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace"
								select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:variable name="existEnvironnement">
		<xsl:value-of
			select="boolean(//Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref])" />
	</xsl:variable>
	<xsl:template match="Environnements" mode="Projet">
		<xsl:if test="$existEnvironnement = 'true'">
			<section>
				<title>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
					<xsl:value-of select="$fieldheader1" />
				</title>
				<informaltable>
					<tgroup cols="3">
						<colspec align="center" colwidth="1*" />
						<colspec colwidth="4*" />
						<colspec colwidth="4*" />
						<thead>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry>N�</entry>
								<entry>
									<xsl:value-of select="$translatedName" />
								</entry>
								<entry>
									<xsl:value-of select="$description" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:apply-templates select="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]" mode="Environnements" />
						</tbody>
					</tgroup>
				</informaltable>
				<xsl:apply-templates select="Environnement[@idEnv=//ExecCampTest/EnvironnementEx/@ref]" mode="Projet" />
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Environnement" mode="Environnements">
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="{@idEnv}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:apply-templates select="Description/* | Description/text()" mode="html2docbook" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceByBr">
							<xsl:with-param name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="Environnement" mode="Projet">
		<section id="{@idEnv}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:if test="Description">
				<variablelist>
					<xsl:apply-templates select="Description" mode="variablelist" />
				</variablelist>
			</xsl:if>
			<xsl:if test="Attachements">
				<section id="{@idEnv}_DocEnv">
					<title>
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
						<xsl:value-of select="$fieldheader3" />
					</title>
					<xsl:apply-templates select="Attachements"
						mode="Projet" />
					<xsl:apply-templates
						select="Attachements/FileAttachement" mode="inclusion" />
				</section>
			</xsl:if>
			<xsl:if test="ValeurParams">
				<section id="{@idEnv}_ParamEnv">
					<title>
						<xsl:variable name="fieldheader4"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
						<xsl:value-of select="$fieldheader4" />
					</title>
					<informaltable>
						<tgroup cols="3">
							<colspec align="center" colwidth="1*" />
							<colspec colwidth="4*" />
							<colspec colwidth="4*" />
							<thead>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
									<entry>N�</entry>
									<entry>
										<xsl:value-of
											select="$translatedName" />
									</entry>
									<entry>
										<xsl:variable
											name="fieldheader6"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
										<xsl:value-of
											select="$fieldheader6" />
									</entry>
								</row>
							</thead>
							<tbody>
								<xsl:for-each
									select="ValeurParams/ValeurParam">
									<row>
										<entry>
											<xsl:number
												value="position()" format="1" />
										</entry>
										<entry>
											<link linkend="paramProj">
												<xsl:value-of
													select="Nom/text()" />
											</link>
										</entry>
										<entry>
											<xsl:value-of
												select="@valeur" />
										</entry>
									</row>
								</xsl:for-each>
							</tbody>
						</tgroup>
					</informaltable>
				</section>
			</xsl:if>
			<xsl:if test="Script">
				<section id="{@idEnv}_Script">
					<title>
						<xsl:variable name="fieldheader7"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_d_environnement']" />
						<xsl:value-of select="$fieldheader7" />
					</title>
					<para>
						<variablelist>
							<varlistentry>
								<term>
									<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
									<xsl:value-of	select="$fieldheader9" />
								</term>
								<listitem>
									<para>
										<xsl:choose>
											<xsl:when	test="Script/Text">
												<ulink url="{concat($urlBase,Script/@dir)}">
													<xsl:value-of	select="Script/@nom" />
												</ulink>
												[
												<link	linkend="{generate-id(Script/@dir)}">
													<xsl:value-of
														select="$voir" />
												</link>
												]
											</xsl:when>
											<xsl:otherwise>
												<ulink
													url="{concat($urlBase,Script/@dir)}">
													<xsl:value-of
														select="Script/@nom" />
												</ulink>
											</xsl:otherwise>
										</xsl:choose>
									</para>
								</listitem>
							</varlistentry>
							<varlistentry>
								<term>
									<xsl:variable name="fieldheader11"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
									<xsl:value-of
										select="$fieldheader11" />
								</term>
								<listitem>
									<para>
										<xsl:value-of
											select="Script/Classpath" />
									</para>
								</listitem>
							</varlistentry>
						</variablelist>
          </para>
					<xsl:apply-templates select="Script" mode="inclusion" />
				</section>
			</xsl:if>
			<xsl:call-template name="extEnvironment">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extEnvironment">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="Projet">
		<xsl:param name="caption" />
		<xsl:if test="FileAttachement or (UrlAttachement and count(UrlAttachement) != count(UrlAttachement[contains(Description/text(), '_ATTACH]')]))">
			<para>
				<informaltable>
					<tgroup cols="3">
						<colspec align="center" colwidth="1*"
							colname="col1" />
						<colspec colwidth="4*" colname="col2" />
						<colspec colwidth="4*" colname="col3" />
						<thead>
							<xsl:if test="$caption='true'">
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
									<entry namest="col1"
										nameend="col3">
										<xsl:variable
											name="fieldheader3"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
										<xsl:value-of
											select="$fieldheader3" />
									</entry>
								</row>
							</xsl:if>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry>N�</entry>
								<entry>
									<xsl:value-of
										select="$translatedName" />
								</entry>
								<entry>
									<xsl:variable name="fieldheader6"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Description']" />
									<xsl:value-of
										select="$fieldheader6" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:for-each
								select="UrlAttachement[not(contains(Description/text(), '_ATTACH]'))]">
								<row>
									<entry>
										<xsl:value-of
											select="count(preceding-sibling::UrlAttachement)+1" />
									</entry>
									<entry>
										<ulink url="{@url}">
											<xsl:value-of select="@url" />
										</ulink>
									</entry>
									<entry>
										<xsl:choose>
											<xsl:when
												test="Description[@isHTML='true']">
												<xsl:apply-templates
													select="Description/* | Description/text()"
													mode="html2docbook" />
											</xsl:when>
											<xsl:when
												test="Description">
												<xsl:call-template
													name="replaceByBr">
													<xsl:with-param
														name="textToReplace" select="Description/text()" />
												</xsl:call-template>
											</xsl:when>
										</xsl:choose>
									</entry>
								</row>
							</xsl:for-each>
							<xsl:for-each select="FileAttachement">
								<row>
									<entry>
										<xsl:value-of
											select="count(../UrlAttachement)+count(preceding-sibling::FileAttachement)+1" />
									</entry>
									<entry>
										<xsl:apply-templates select="."
											mode="Projet" />
									</entry>
									<entry>
										<xsl:choose>
											<xsl:when
												test="Description[@isHTML='true']">
												<xsl:apply-templates
													select="Description/* | Description/text()"
													mode="html2docbook" />
											</xsl:when>
											<xsl:when
												test="Description">
												<xsl:call-template
													name="replaceByBr">
													<xsl:with-param
														name="textToReplace" select="Description/text()" />
												</xsl:call-template>
											</xsl:when>
										</xsl:choose>
									</entry>
								</row>
							</xsl:for-each>
						</tbody>
					</tgroup>
				</informaltable>
			</para>
		</xsl:if>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="inclusion">
		<xsl:variable name="target">
			<xsl:value-of select="generate-id(@dir)" />
		</xsl:variable>
		<xsl:if test="$gif = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'gif'">
				<formalpara id="{$target}">
          <title>
            <xsl:value-of select="@nom" />
          </title>
          <para>
            <inlinegraphic
              scalefit="1"
              width="100%"
              contentdepth="100%"
              align="center"
              format="GIF"
              fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
          </para>
        </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$jpeg = '1'">
			<xsl:if test="(substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg')">
				<formalpara id="{$target}">
          <title>
            <xsl:value-of select="@nom" />
          </title>
          <para>
            <inlinegraphic
              scalefit="1"
              width="100%"
              contentdepth="100%"
              align="center"
              format="JPG"
              fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
          </para>
        </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$png = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'png'">
				<formalpara id="{$target}">
          <title>
            <xsl:value-of select="@nom" />
          </title>
          <para>
            <inlinegraphic
              scalefit="1"
              width="100%"
              contentdepth="100%"
              align="center"
              format="PNG"
              fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
          </para>
        </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="Text">
			<formalpara id="{$target}">
        <title>
          <xsl:value-of select="@nom" />
        </title>
        <para>
          <simplelist type="horiz" columns="1">
            <xsl:choose>
              <xsl:when test="Text/ligne">
                <xsl:for-each select="Text/ligne">
                  <member>
                    <xsl:value-of select="text()" />
                  </member>
                </xsl:for-each>
              </xsl:when>
              <xsl:otherwise>
                <member />
              </xsl:otherwise>
            </xsl:choose>
          </simplelist>
        </para>
      </formalpara>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Script" mode="inclusion">
	  <xsl:variable name="target">
      <xsl:value-of select="generate-id(@dir)" />
    </xsl:variable>
    <xsl:if test="Text">
      <formalpara id="{$target}">
        <title>
          <xsl:value-of select="@nom" />
        </title>
        <para>
          <programlisting><xsl:value-of select="Text/." />
          </programlisting>
        </para>
      </formalpara>
    </xsl:if>
	</xsl:template>
	<xsl:variable name="existFamille">
		<xsl:value-of
			select="boolean(//Famille[@id_famille = //FamilleRef/@ref])" />
	</xsl:variable>
	<xsl:template match="Familles" mode="Projet">
		<xsl:if test="$existFamille = 'true'">
			<section>
				<title>
					<xsl:variable name="fieldheader5"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
					<xsl:value-of select="$fieldheader5" />
				</title>
				<xsl:apply-templates select="./Famille[@id_famille=//FamillesCamp/FamilleRef/@ref]" mode="Projet" />
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Famille" mode="Projet">
		<section id="{@id_famille}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:if test="Description">
				<variablelist>
					<xsl:apply-templates select="Description"
						mode="variablelist" />
				</variablelist>
			</xsl:if>
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:call-template name="extFamily1">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates select="SuiteTests/SuiteTest[@id_suite=//SuiteTestsCamp/SuiteTestRef/@ref]" mode="Projet" />
			<xsl:call-template name="extFamily2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extFamily1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extFamily2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="SuiteTest" mode="Projet">
		<section id="{@id_suite}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:if test="Description">
				<variablelist>
					<xsl:apply-templates select="Description" mode="variablelist" />
				</variablelist>
			</xsl:if>
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:call-template name="extSuite1">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates select="Tests/Test[@id_test=//TestsCamp/TestRef/@ref]" mode="Projet" />
			<xsl:call-template name="extSuite2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extSuite1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extSuite2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Test" mode="Projet">
		<section id="{@id_test}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<para>
				<variablelist>
					<xsl:apply-templates select="Description" mode="variablelist" />
					<xsl:if test="Concepteur">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader2"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Concepteur_du_test']" />
									<xsl:value-of
										select="$fieldheader2" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of
										select="Concepteur/Nom/text()" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when test="TestAuto">
										<xsl:variable
											name="fieldheader4"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Automatique']" />
										<xsl:value-of
											select="$fieldheader4" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable
											name="fieldheader5"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Manuel']" />
										<xsl:value-of
											select="$fieldheader5" />
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
					<xsl:if test="TestAuto">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader6"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
									<xsl:value-of
										select="$fieldheader6" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of
										select="TestAuto/@plug_ext" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:apply-templates select="TestAuto" mode="Projet" />
				</variablelist>
			</para>
			<xsl:apply-templates select="TestAuto/Script" mode="inclusion" />
			<xsl:apply-templates select="TestManuel" mode="Projet" />
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:call-template name="extTest">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:variable name="idTest">
				<xsl:value-of select="@id_test" />
			</xsl:variable>
			<xsl:variable name="existCamp">
				<xsl:value-of select="boolean(//TestRef[@ref=$idTest])" />
			</xsl:variable>
			<xsl:if test="$existCamp = 'true'">
				<para>
					<informaltable>
						<tgroup cols="3">
							<colspec align="center" colwidth="1*"
								colname="col1" />
							<colspec colwidth="4*" colname="col2" />
							<colspec colwidth="4*" colname="col3" />
							<thead>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
									<entry namest="col1"
										nameend="col3">
										<xsl:variable
											name="fieldheader7"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagnes_du_test']" />
										<xsl:value-of
											select="$fieldheader7" />
									</entry>
								</row>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
									<entry>N�</entry>
									<entry>
										<xsl:value-of select="$translatedName" />
									</entry>
									<entry>
										<xsl:variable
											name="fieldheader8"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Execute']" />
										<xsl:value-of
											select="$fieldheader8" />
									</entry>
								</row>
							</thead>
							<tbody>
								<xsl:apply-templates
									select="//CampagneTest[.//TestRef/@ref=$idTest]" mode="Test">
									<xsl:with-param name="idTest">
										<xsl:value-of select="$idTest" />
									</xsl:with-param>
								</xsl:apply-templates>
							</tbody>
						</tgroup>
					</informaltable>
				</para>
			</xsl:if>
			<xsl:if test="ParamsT">
				<para>
					<informaltable>
						<tgroup cols="2">
							<colspec align="center" colwidth="1*"
								colname="col1" />
							<colspec colwidth="6*" colname="col2" />
							<thead>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
									<entry namest="col1"
										nameend="col2">
										<xsl:variable
											name="fieldheader9"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
										<xsl:value-of
											select="$fieldheader9" />
									</entry>
								</row>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
									<entry>N�</entry>
									<entry>
										<xsl:value-of
											select="$translatedName" />
									</entry>
								</row>
							</thead>
							<tbody>
								<xsl:apply-templates
									select="ParamsT/ParamT" mode="Projet" />
							</tbody>
						</tgroup>
					</informaltable>
				</para>
			</xsl:if>
		</section>
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTest" mode="Test">
		<xsl:param name="idTest" />
		<row>
			<entry class="tab_center">
				<xsl:number value="position()" />
			</entry>
			<entry class="tab">
				<link linkend="{@id_camp}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when
						test=".//ResulExec[@refTest=$idTest and ( @res = 'PASSED' or @res = 'FAILED' or @res = 'INCONCLUSIF' )]">
						<xsl:variable name="fieldheader1"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Oui']" />
						<xsl:value-of select="$fieldheader1" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:variable name="fieldheader2"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Non']" />
						<xsl:value-of select="$fieldheader2" />
					</xsl:otherwise>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="ParamT" mode="Projet">
		<row>
			<entry class="tab_center">
				<xsl:number value="position()" />
			</entry>
			<entry>
				<link linkend="paramProj">
					<xsl:value-of select="id(@ref)/Nom/text()" />
				</link>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="TestAuto" mode="Projet">
		<varlistentry>
			<term>
				<emphasis>
					<xsl:variable name="fieldheader1"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_de_test']" />
					<xsl:value-of select="$fieldheader1" />
				</emphasis>
			</term>
			<listitem>
				<para>
					<xsl:choose>
						<xsl:when test="Script/Text">
							<ulink
								url="{concat($urlBase,Script/@dir)}">
								<xsl:value-of select="Script/@nom" />
							</ulink>
							&#160;[
							<link linkend="{generate-id(Script/@dir)}">
								<xsl:value-of select="$voir" />
							</link>
							]
						</xsl:when>
						<xsl:otherwise>
							<ulink
								url="{concat($urlBase,Script/@dir)}">
								<xsl:value-of select="Script/@nom" />
							</ulink>
						</xsl:otherwise>
					</xsl:choose>
				</para>
			</listitem>
		</varlistentry>
	</xsl:template>
	<xsl:template match="TestManuel" mode="Projet">
		<xsl:if test="ActionTest">
			<para>
				<itemizedlist>
					<title>
						<xsl:variable name="fieldheader1"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Actions_du_test']" />
						<xsl:value-of select="$fieldheader1" />
					</title>
					<xsl:apply-templates select="ActionTest" mode="Projet" />
				</itemizedlist>
			</para>
			<xsl:apply-templates
				select="ActionTest/Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="ActionTest" mode="Projet">
		<listitem>
			<para>
				<emphasis>
					<xsl:value-of select="$translatedName" />
					:
				</emphasis>
				<xsl:value-of select="Nom/text()" />
				<xsl:variable name="actionContent">
					<xsl:apply-templates select="Description"
						mode="list" />
					<xsl:if test="ResultAttendu">
						<listitem>
							<para>
								<emphasis>
									<xsl:variable name="fieldheader4"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
									<xsl:value-of
										select="$fieldheader4" />
									:
								</emphasis>
								<xsl:call-template name="replaceByBr">
									<xsl:with-param name="textToReplace"
										select="ResultAttendu/text()" />
								</xsl:call-template>
							</para>
						</listitem>
					</xsl:if>
					<xsl:if test="Attachements">
						<listitem>
							<para>
								<emphasis>
									<xsl:variable name="fieldheader5"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
									<xsl:value-of
										select="$fieldheader5" />
									:
								</emphasis>
								<itemizedlist>
									<xsl:for-each
										select="Attachements/UrlAttachement">
										<listitem>
											<para>
												<ulink url="{@url}">
													<xsl:value-of
														select="@url" />
												</ulink>
											</para>
										</listitem>
									</xsl:for-each>
									<xsl:for-each
										select="Attachements/FileAttachement">
										<listitem>
											<para>
												<xsl:apply-templates
													select="." mode="Projet" />
											</para>
										</listitem>
									</xsl:for-each>
								</itemizedlist>
							</para>
						</listitem>
					</xsl:if>
					<xsl:call-template name="extAction">
						<xsl:with-param name="current-node">
							<xsl:value-of select="saxon:path()" />
						</xsl:with-param>
					</xsl:call-template>
				</xsl:variable>
				<xsl:if test="count($actionContent//*)>0">
					<itemizedlist>
						<xsl:copy-of select="$actionContent" />
					</itemizedlist>
				</xsl:if>
			</para>
		</listitem>
	</xsl:template>
	<xsl:template name="extAction">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="CampagneTests" mode="Projet">
		<xsl:if test="$campaigns = '1'">
			<section id="CampTests">
				<title>
					<xsl:variable name="fieldheader1"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNES_DU_PROJET']" />
					<xsl:value-of select="$fieldheader1" />
				</title>
				<xsl:apply-templates mode="Projet"
					select="./CampagneTest" />
			</section>
		</xsl:if>
		<xsl:if test="$executionreport = '1'">
			<section id="resulExec">
				<title>
					<xsl:variable name="fieldheader2"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='RAPPORT_D_EXECUTION']" />
					<xsl:value-of select="$fieldheader2" />
				</title>
				<xsl:apply-templates mode="ProjetExecReport"
					select="./CampagneTest[.//ResulExecCampTest]" />
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template match="CampagneTest" mode="Projet">
		<section id="{@id_camp}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:if test="Description">
				<variablelist>
					<xsl:apply-templates select="Description"
						mode="variablelist" />
				</variablelist>
			</xsl:if>
			<section id="{@id_camp}_details">
				<title>
					<xsl:variable name="fieldheader2"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Details']" />
					<xsl:value-of select="$fieldheader2" />
				</title>
				<xsl:apply-templates select="Attachements"
					mode="Projet">
					<xsl:with-param name="caption">true</xsl:with-param>
				</xsl:apply-templates>
				<xsl:apply-templates
					select="Attachements/FileAttachement" mode="inclusion" />
				<xsl:call-template name="extCampaign1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:if test="FamillesCamp/FamilleRef">
					<para>
						<itemizedlist>
							<title>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Arborescence_de_test']" />
								<xsl:value-of select="$fieldheader3" />
							</title>
							<xsl:for-each
								select="FamillesCamp/FamilleRef">
								<listitem>
									<para>
										<link linkend="{@ref}">
											<xsl:value-of
												select="Nom/text()" />
										</link>
										<itemizedlist>
											<xsl:for-each
												select="SuiteTestsCamp/SuiteTestRef">
												<listitem>
													<para>
														<link
															linkend="{@ref}">
															<xsl:value-of
																select="Nom/text()" />
														</link>
														<itemizedlist>
															<xsl:for-each
																select="TestsCamp/TestRef">
																<listitem>
																	<para>
																		<link
																			linkend="{@ref}">
																			<xsl:value-of
																				select="Nom/text()" />
																		</link>
																	</para>
																</listitem>
															</xsl:for-each>
														</itemizedlist>
													</para>
												</listitem>
											</xsl:for-each>
										</itemizedlist>
									</para>
								</listitem>
							</xsl:for-each>
						</itemizedlist>
					</para>
				</xsl:if>
			</section>
			<xsl:call-template name="extCampaign2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates select="ExecCampTests/ExecCampTest"
				mode="Projet" />
		</section>
	</xsl:template>
	<xsl:template name="extCampaign1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extCampaign2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ExecCampTest" mode="Projet">
		<section id="{@id_exec_camp}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:if test="Description">
				<variablelist>
					<xsl:apply-templates select="Description"
						mode="variablelist" />
				</variablelist>
			</xsl:if>
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement"
				mode="inclusion" />
			<xsl:if test="JeuDonneesEx">
				<para>
					<informaltable>
						<tgroup cols="3">
							<colspec align="center" colwidth="1*"
								colname="col1" />
							<colspec colwidth="4*" colname="col2" />
							<colspec colwidth="4*" colname="col3" />
							<thead>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
									<entry namest="col1"
										nameend="col3">
										<xsl:variable
											name="fieldheader2"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Jeu_de_donnees']" />
										<xsl:value-of
											select="$fieldheader2" />
										<emphasis>
											&#160;
											<xsl:value-of
												select="JeuDonneesEx/Nom/text()" />
										</emphasis>
									</entry>
								</row>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
									<entry>N�</entry>
									<entry>
										<xsl:value-of
											select="$translatedName" />
									</entry>
									<entry>
										<xsl:variable
											name="fieldheader3"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
										<xsl:value-of
											select="$fieldheader3" />
									</entry>
								</row>
							</thead>
							<tbody>
								<xsl:variable name="idJeu">
									<xsl:value-of
										select="JeuDonneesEx/@ref" />
								</xsl:variable>
								<xsl:variable name="id_env">
									<xsl:value-of
										select="EnvironnementEx/@ref" />
								</xsl:variable>
								<xsl:apply-templates
									select="//JeuDonnees[@id_jeu=$idJeu]/ValeurParams/ValeurParam"
									mode="ExecCampTest">
									<xsl:with-param name="idJeu">
										<xsl:value-of select="$idJeu" />
									</xsl:with-param>
									<xsl:with-param name="id_env">
										<xsl:value-of select="$id_env" />
									</xsl:with-param>
								</xsl:apply-templates>
							</tbody>
						</tgroup>
					</informaltable>
				</para>
				<xsl:call-template name="extDataSet">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
			</xsl:if>
			<para>
				<variablelist>
					<varlistentry>
						<term>
							<emphasis>
							  <xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
                <xsl:value-of select="$fieldheader3" />
              </emphasis>
						</term>
						<listitem>
							<para>
							  <link linkend="{EnvironnementEx/@ref}">
                  <xsl:value-of select="id(EnvironnementEx/@ref)/Nom/text()" />
                </link>
							</para>
						</listitem>
					</varlistentry>
					<xsl:if test="Script[@type='PRE_SCRIPT']">
					  <varlistentry>
              <term>
                <emphasis>Pre-Script</emphasis>
              </term>
              <listitem>
                <para>
                  <xsl:choose>
                    <xsl:when test="Script[@type='PRE_SCRIPT']/Text">
                        <ulink url="{concat($urlBase,Script[@type='PRE_SCRIPT']/@dir)}">
                          <xsl:value-of select="Script[@type='PRE_SCRIPT']/@nom" />
                        </ulink>
                        &#160;[
                        <link linkend="{generate-id(Script[@type='PRE_SCRIPT']/@dir)}">
                          <xsl:value-of select="$voir" />
                        </link>
                        ]
                      </xsl:when>
                      <xsl:otherwise>
                        <ulink url="{concat($urlBase,Script[@type='PRE_SCRIPT']/@dir)}">
                          <xsl:value-of select="Script[@type='PRE_SCRIPT']/@nom" />
                        </ulink>
                      </xsl:otherwise>
                    </xsl:choose>
                </para>
              </listitem>
            </varlistentry>
					</xsl:if>
					<xsl:if test="Script[@type='POST_SCRIPT']">
            <varlistentry>
              <term>
                <emphasis>Post-Script</emphasis>
              </term>
              <listitem>
                <para>
                  <xsl:choose>
                    <xsl:when test="Script[@type='POST_SCRIPT']/Text">
                        <ulink url="{concat($urlBase,Script[@type='POST_SCRIPT']/@dir)}">
                          <xsl:value-of select="Script[@type='POST_SCRIPT']/@nom" />
                        </ulink>
                        &#160;[
                        <link linkend="{generate-id(Script[@type='POST_SCRIPT']/@dir)}">
                          <xsl:value-of select="$voir" />
                        </link>
                        ]
                      </xsl:when>
                      <xsl:otherwise>
                        <ulink url="{concat($urlBase,Script[@type='POST_SCRIPT']/@dir)}">
                          <xsl:value-of select="Script[@type='POST_SCRIPT']/@nom" />
                        </ulink>
                      </xsl:otherwise>
                    </xsl:choose>
                </para>
              </listitem>
            </varlistentry>
          </xsl:if>
				</variablelist>
			</para>
			<xsl:apply-templates select="Script[@type='PRE_SCRIPT']" mode="inclusion"/>
			<xsl:apply-templates select="Script[@type='POST_SCRIPT']" mode="inclusion"/>
			<xsl:if test="ResulExecCampTests">
				<para>
					<informaltable>
						<tgroup cols="3">
							<colspec align="center" colwidth="1*"
								colname="col1" />
							<colspec colwidth="4*" colname="col2" />
							<colspec colwidth="4*" colname="col3" />
							<thead>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
									<entry namest="col1"
										nameend="col3">
										<xsl:variable
											name="fieldheader4"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
										<xsl:value-of
											select="$fieldheader4" />
									</entry>
								</row>
								<row>
								  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                  <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
									<entry>N�</entry>
									<entry>
										<xsl:value-of
											select="$translatedName" />
									</entry>
									<entry>
										<xsl:variable
											name="fieldheader5"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Statistique']" />
										<xsl:value-of
											select="$fieldheader5" />
									</entry>
								</row>
							</thead>
							<tbody>
								<xsl:apply-templates
									select="ResulExecCampTests/ResulExecCampTest"
									mode="ExecCampTest" />
							</tbody>
						</tgroup>
					</informaltable>
				</para>
			</xsl:if>
			<xsl:call-template name="extExec">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extDataSet">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template name="extExec">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ValeurParam" mode="ExecCampTest">
		<xsl:param name="idJeu" />
		<xsl:param name="id_env" />
		<xsl:variable name="valeurParam">
			<xsl:value-of select="@valeur" />
		</xsl:variable>
		<xsl:variable name="idParam">
			<xsl:value-of select="@ref" />
		</xsl:variable>
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="paramProj">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when
						test="not($valeurParam = '$(env_value)$')">
						<xsl:value-of select="$valeurParam" />
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of
							select="//Environnement[@idEnv=$id_env]/ValeurParams/ValeurParam[@ref=$idParam]/@valeur" />
					</xsl:otherwise>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="ExecCampTest">
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="{@id_exec_res}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				Pass :
				<xsl:value-of
					select="count(ResulExecs/ResulExec[@res='PASSED'])" />
				, Fail :
				<xsl:value-of
					select="count(ResulExecs/ResulExec[@res='FAILED'])" />
				,
				<xsl:variable name="fieldheader"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
				<xsl:value-of select="$fieldheader" />
				:
				<xsl:value-of
					select="count(ResulExecs/ResulExec[@res='INCONCLUSIF'])" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="CampagneTest" mode="ProjetExecReport">
		<section id="{@id_camp}_execReport">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='CAMPAGNE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<xsl:apply-templates select=".//ResulExecCampTest"
				mode="Projet" />
		</section>
	</xsl:template>
	<xsl:template match="ResulExecCampTest" mode="Projet">
		<xsl:variable name="idCamp">
			<xsl:value-of select="../../../../@id_camp" />
		</xsl:variable>
		<xsl:variable name="idExec">
			<xsl:value-of select="../../@id_exec_camp" />
		</xsl:variable>
		<xsl:variable name="idExecRes">
			<xsl:value-of select="@id_exec_res" />
		</xsl:variable>
		<section id="{@id_exec_res}">
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<para>
				<variablelist>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader2"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
								<xsl:value-of select="$fieldheader2" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<link linkend="{$idCamp}">
									<xsl:value-of
										select="../../../../Nom/text()" />
								</link>
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<link linkend="{$idExec}">
									<xsl:value-of
										select="../../Nom/text()" />
								</link>
							</para>
						</listitem>
					</varlistentry>
				</variablelist>
			</para>
			<section id="{@id_exec_res}_resume">
				<title>
					<xsl:variable name="fieldheader4"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume_de_l_execution']" />
					<xsl:value-of select="$fieldheader4" />
				</title>
				<para>
					<variablelist>
						<xsl:if test="Date_crea">
							<varlistentry>
								<term>
									<emphasis>
										<xsl:variable
											name="fieldheader5"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Date_d_execution']" />
										<xsl:value-of
											select="$fieldheader5" />
									</emphasis>
								</term>
								<listitem>
									<para>
										<xsl:value-of
											select="Date_crea/text()" />
									</para>
								</listitem>
							</varlistentry>
						</xsl:if>
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader6"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Statistique']" />
									<xsl:value-of
										select="$fieldheader6" />
								</emphasis>
							</term>
							<listitem>
								<para>
									Pass :
									<xsl:value-of
										select="count(ResulExecs/ResulExec[@res='PASSED'])" />
									, Fail :
									<xsl:value-of
										select="count(ResulExecs/ResulExec[@res='FAILED'])" />
									,
									<xsl:variable name="fieldheader"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
									<xsl:value-of select="$fieldheader" />
									:
									<xsl:value-of
										select="count(ResulExecs/ResulExec[@res='INCONCLUSIF'])" />
								</para>
							</listitem>
						</varlistentry>
					</variablelist>
				</para>
				<xsl:apply-templates select="Attachements"
					mode="Projet">
					<xsl:with-param name="caption">true</xsl:with-param>
				</xsl:apply-templates>
				<xsl:apply-templates
					select="Attachements/FileAttachement" mode="inclusion" />
				<xsl:call-template name="extResExec1">
					<xsl:with-param name="current-node">
						<xsl:value-of select="saxon:path()" />
					</xsl:with-param>
				</xsl:call-template>
				<xsl:variable name="lien_test">
					<xsl:value-of
						select="concat('testresult_', @id_exec_res)" />
				</xsl:variable>
				<xsl:if test="ResulExecs/ResulExec">
					<para>
						<informaltable>
							<tgroup cols="5">
								<colspec align="center" colwidth="1*"
									colname="col1" />
								<colspec colwidth="4*" colname="col2" />
								<colspec colwidth="4*" colname="col3" />
								<colspec colwidth="4*" colname="col4" />
								<colspec colwidth="4*" colname="col5" />
								<thead>
									<row>
									  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                    <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
										<entry namest="col1"
											nameend="col5">
											<xsl:variable
												name="fieldheader14"
												select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultats_des_tests']" />
											<xsl:value-of
												select="$fieldheader14" />
										</entry>
									</row>
									<row>
									  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                    <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
										<entry>N�</entry>
										<entry>
											<xsl:variable
												name="fieldheader15"
												select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
											<xsl:value-of
												select="$fieldheader15" />
										</entry>
										<entry>
											<xsl:variable
												name="fieldheader16"
												select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite']" />
											<xsl:value-of
												select="$fieldheader16" />
										</entry>
										<entry>
											<xsl:variable
												name="fieldheader17"
												select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
											<xsl:value-of
												select="$fieldheader17" />
										</entry>
										<entry>
											<xsl:variable
												name="fieldheader18"
												select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
											<xsl:value-of
												select="$fieldheader18" />
										</entry>
									</row>
								</thead>
								<tbody>
									<xsl:apply-templates
										select="ResulExecs/ResulExec" mode="resultatTests">
										<xsl:with-param
											name="lien_test">
											<xsl:value-of
												select="$lien_test" />
										</xsl:with-param>
									</xsl:apply-templates>
								</tbody>
							</tgroup>
						</informaltable>
					</para>
				</xsl:if>
			</section>
			<section id="{@id_exec_res}_detail">
				<title>
					<xsl:variable name="fieldheader19"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Details_de_l_execution']" />
					<xsl:value-of select="$fieldheader19" />
				</title>
				<xsl:apply-templates mode="Projet"
					select="ResulExecs/ResulExec" />
			</section>
		</section>
	</xsl:template>
	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ResulExec" mode="resultatTests">
		<xsl:param name="lien_test" />
		<row id="{concat($lien_test, '_', @refTest)}">
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link
					linkend="{id(@refTest)/ancestor::Famille/@id_famille}">
					<xsl:value-of select="RefTest/NomFamille/text()" />
				</link>
			</entry>
			<entry>
				<link
					linkend="{id(@refTest)/ancestor::SuiteTest/@id_suite}">
					<xsl:value-of select="RefTest/NomSuite/text()" />
				</link>
			</entry>
			<entry>
				<link linkend="{@refTest}">
					<xsl:value-of select="RefTest/NomTest/text()" />
				</link>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="@res = 'PASSED'">Pass</xsl:when>
					<xsl:when test="@res = 'FAILED'">Fail</xsl:when>
					<xsl:when test="@res = 'INCONCLUSIF'">
						<xsl:variable name="fieldheader1"
							select="$translations/allheadings/headings[lang($local)]/heading[@category='Inconclusif']" />
						<xsl:value-of select="$fieldheader1" />
					</xsl:when>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="ResulExec" mode="Projet">
		<xsl:variable name="nomTest">
			<xsl:value-of select="id(@refTest)/Nom/text()" />
		</xsl:variable>
		<section>
			<title>
				<xsl:variable name="fieldheader1"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="$nomTest" />
				</emphasis>
			</title>
			<para>
				<variablelist>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader2"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
								<xsl:value-of select="$fieldheader2" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when test="@res = 'PASSED'">
										PASS
									</xsl:when>
									<xsl:when test="@res = 'FAILED'">
										FAIL
									</xsl:when>
									<xsl:when
										test="@res = 'INCONCLUSIF'">
										<xsl:variable
											name="fieldheader3"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='INCONCLUSIF']" />
										<xsl:value-of
											select="$fieldheader3" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable
											name="fieldheader4"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Pas_execute']" />
										<xsl:value-of
											select="$fieldheader4" />
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader5"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
								<xsl:value-of select="$fieldheader5" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when
										test="id(@refTest)/TestAuto">
										<xsl:variable
											name="fieldheader6"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Automatique']" />
										<xsl:value-of
											select="$fieldheader6" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable
											name="fieldheader7"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Manuel']" />
										<xsl:value-of
											select="$fieldheader7" />
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader8"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Famille']" />
								<xsl:value-of select="$fieldheader8" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<link
									linkend="{id(@refTest)/ancestor::Famille/@id_famille}">
									<xsl:value-of
										select="id(@refTest)/ancestor::Famille/Nom/text()" />
								</link>
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader9"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
								<xsl:value-of select="$fieldheader9" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<link
									linkend="{id(@refTest)/ancestor::SuiteTest/@id_suite}">
									<xsl:value-of
										select="id(@refTest)/ancestor::SuiteTest/Nom/text()" />
								</link>
							</para>
						</listitem>
					</varlistentry>
				</variablelist>
			</para>
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement"
				mode="inclusion" />
			<xsl:variable name="idTest">
				<xsl:value-of select="@refTest" />
			</xsl:variable>
			<xsl:call-template name="extResExec2">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:apply-templates select="id($idTest)/TestAuto"
				mode="detailExec" />
			<xsl:if
				test="boolean(ancestor::ResulExecCampTest/ResulActionTests/ResulActionTest[@refAction = id($idTest)/TestManuel/ActionTest/@id_action])">
				<itemizedlist>
					<title>
						<emphasis>
							<xsl:variable name="fieldheader15"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution_des_actions_de_test']" />
							<xsl:value-of select="$fieldheader15" />
						</emphasis>
					</title>
					<xsl:apply-templates
						select="ancestor::ResulExecCampTest/ResulActionTests/ResulActionTest[@refAction = id($idTest)/TestManuel/ActionTest/@id_action]"
						mode="detailExec" />
				</itemizedlist>
			</xsl:if>
		</section>
	</xsl:template>
	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ResulActionTest" mode="detailExec">
		<listitem>
			<para>
				<emphasis>
					<xsl:value-of select="$translatedName" />
					:
				</emphasis>
				<xsl:value-of select="id(@refAction)/Nom/text()" />
				<itemizedlist>
					<xsl:if
						test="Description and Description/text()!=''">
						<xsl:apply-templates select="Description"
							mode="list" />
					</xsl:if>
					<xsl:if
						test="ResultAttendu and ResultAttendu/text()!=''">
						<listitem>
							<para>
								<emphasis>
									<xsl:variable name="fieldheader1"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
									<xsl:value-of
										select="$fieldheader1" />
									:
								</emphasis>
								<xsl:call-template name="replaceByBr">
									<xsl:with-param name="textToReplace"
										select="ResultAttendu/text()" />
								</xsl:call-template>
							</para>
						</listitem>
						<listitem>
							<para>
								<emphasis>
									<xsl:variable name="fieldheader2"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_effectif']" />
									<xsl:value-of
										select="$fieldheader2" />
									:
								</emphasis>
								<xsl:call-template name="replaceByBr">
									<xsl:with-param name="textToReplace"
										select="ResulEffectif/text()" />
								</xsl:call-template>
							</para>
						</listitem>
					</xsl:if>
					<listitem>
						<para>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat']" />
								<xsl:value-of select="$fieldheader3" />
								:
							</emphasis>
							<xsl:choose>
								<xsl:when test="@res = 'PASSED'">
									PASS
								</xsl:when>
								<xsl:when test="@res = 'FAILED'">
									FAIL
								</xsl:when>
								<xsl:when test="@res = 'INCONCLUSIF'">
									<xsl:variable name="fieldheader4"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='INCONCLUSIF']" />
									<xsl:value-of
										select="$fieldheader4" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:variable name="fieldheader5"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Pas_execute']" />
									<xsl:value-of
										select="$fieldheader5" />
								</xsl:otherwise>
							</xsl:choose>
						</para>
					</listitem>
				</itemizedlist>
			</para>
		</listitem>
	</xsl:template>
	<xsl:template match="TestAuto" mode="detailExec">
		<para>
			<variablelist>
				<title>
					<xsl:variable name="fieldheader1"
						select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution_du_script_de_test']" />
					<xsl:value-of select="$fieldheader1" />
				</title>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader2"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
							<xsl:value-of select="$fieldheader2" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<ulink
								url="{concat($urlBase,Script/@dir)}">
								<xsl:value-of select="Script/@nom" />
							</ulink>
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader3"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
							<xsl:value-of select="$fieldheader3" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="@plug_ext" />
						</para>
					</listitem>
				</varlistentry>
			</variablelist>
		</para>
	</xsl:template>
	<xsl:template match="UrlAttachement" mode="detailExec">
		<xsl:variable name="desc">
			<xsl:value-of select="Description/text()" />
		</xsl:variable>
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<xsl:value-of
					select="substring(substring-before($desc, '_ATTACH'), 2)" />
			</entry>
			<entry>
				<ulink url="{@url}">
					<xsl:value-of select="@url" />
				</ulink>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="Projet">
		<xsl:choose>
			<xsl:when
				test="($gif = '1') and (substring-after(@nom, '.') = 'gif')
				or ($jpeg = '1') and ((substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg'))
				or ($png = '1') and (substring-after(@nom, '.') = 'png')
				or Text">
				<ulink url="{concat($urlBase,@dir)}">
					<xsl:value-of select="@nom" />
				</ulink>
				<xsl:variable name="target">
					<xsl:value-of select="generate-id(@dir)" />
				</xsl:variable>
				&#160;[
				<link linkend="{$target}">
					<xsl:value-of select="$voir" />
				</link>
				]
			</xsl:when>
			<xsl:otherwise>
				<ulink url="{concat($urlBase,@dir)}">
					<xsl:value-of select="@nom" />
				</ulink>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="Description" mode="para">
		<para>
			<xsl:apply-templates select="." mode="Projet" />
		</para>
	</xsl:template>
	<xsl:template match="Description" mode="list">
		<listitem>
			<para>
				<xsl:apply-templates select="." mode="Projet" />
			</para>
		</listitem>
	</xsl:template>
	<xsl:template match="Description" mode="variablelist">
		<varlistentry>
			<term>
				<emphasis>
					<xsl:value-of select="$description" />
				</emphasis>
			</term>
			<listitem>
				<para>
					<xsl:choose>
						<xsl:when test="@isHTML='true'">
							&#160;
							<xsl:apply-templates select="* | text()"
								mode="html2docbook" />
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="replaceByBr">
								<xsl:with-param name="textToReplace"
									select="./text()" />
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</para>
			</listitem>
		</varlistentry>
	</xsl:template>
	<xsl:template match="Description" mode="Projet">
		<xsl:if test="normalize-space(.) != ''">
			<emphasis>
				<xsl:value-of select="$description" />
				:
			</emphasis>
			<xsl:choose>
				<xsl:when test="@isHTML='true'">
					&#160;
					<xsl:apply-templates select="* | text()"
						mode="html2docbook" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="replaceByBr">
						<xsl:with-param name="textToReplace"
							select="./text()" />
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="replaceByBr">
		<xsl:param name="textToReplace" />
		<xsl:choose>
			<xsl:when test="contains($textToReplace, '\n')">
				<xsl:value-of
					select="substring-before($textToReplace,'\n')" />
				&#160;
				<xsl:call-template name="replaceByBr">
					<xsl:with-param name="textToReplace"
						select="substring-after($textToReplace,'\n')" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$textToReplace" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="p" mode="html2docbook">
		<xsl:apply-templates select="* | text()" mode="html2docbook" />
	</xsl:template>

	<xsl:template match="ul" mode="html2docbook">
		<itemizedlist>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</itemizedlist>
	</xsl:template>

	<xsl:template match="ol" mode="html2docbook">
		<orderedlist>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</orderedlist>
	</xsl:template>

	<xsl:template match="li" mode="html2docbook">
		<listitem>
			<xsl:choose>
				<xsl:when test="count(p) = 0">
					<para>
						<xsl:apply-templates select="* | text()"
							mode="html2docbook" />
					</para>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="* | text()"
						mode="html2docbook" />
				</xsl:otherwise>
			</xsl:choose>
		</listitem>
	</xsl:template>

	<!--  tables -->

	<xsl:template match="table" mode="html2docbook">
		<informaltable>
			<tgroup cols="{count(tr)}">
				<tbody>
					<xsl:apply-templates select="* | text()"
						mode="html2docbook" />
				</tbody>
			</tgroup>
		</informaltable>
	</xsl:template>

	<xsl:template match="tr" mode="html2docbook">
		<row>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</row>
	</xsl:template>

	<xsl:template match="td" mode="html2docbook">
		<entry>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</entry>
	</xsl:template>

	<xsl:template match="*" mode="html2docbook">
		<xsl:message>
			No template for
			<xsl:value-of select="name()" />
		</xsl:message>
		<xsl:apply-templates select="* | text()" mode="html2docbook" />
	</xsl:template>

	<xsl:template match="@*" mode="html2docbook">
		<xsl:message>
			No template for
			<xsl:value-of select="name()" />
		</xsl:message>
	</xsl:template>

	<!-- inline formatting -->
	<xsl:template match="b" mode="html2docbook">
		<emphasis role="bold">
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</emphasis>
	</xsl:template>
	<xsl:template match="i" mode="html2docbook">
		<emphasis>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</emphasis>
	</xsl:template>
	<xsl:template match="u" mode="html2docbook">
		<citetitle>
			<xsl:apply-templates select="* | text()"
				mode="html2docbook" />
		</citetitle>
	</xsl:template>
	<xsl:template match="br" mode="html2docbook">
		<xsl:processing-instruction name="lb" />
	</xsl:template>

	<xsl:template match="text()" mode="html2docbook">
		<xsl:choose>
			<xsl:when test="normalize-space(.) = ''"></xsl:when>
			<xsl:otherwise>
				<xsl:copy />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
