<?xml version="1.0" encoding="ISO-8859-1" ?>
<!--
    Document   : docbookStatique.xsl
    Created on : 26 mars 2007, 17:46
    Author     : vapu8214
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon my"
	extension-element-prefixes="saxon">

	<xsl:output
		method="xml"
		doctype-public="-//OASIS//DTD DocBook XML V4.4//EN"
		doctype-system="http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" />
	<xsl:param name="jpeg" />
	<xsl:param name="gif" />
	<xsl:param name="png" />
	<xsl:param name="project" />
	<xsl:param name="environments" />
	<xsl:param name="testplan" />
	<xsl:param name="local" />
	<xsl:param name="translate" />
  <xsl:param name="urlBase" />
	<xsl:variable name="translations" select="document($translate)" />
	<xsl:variable name="nom">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
	</xsl:variable>
	<xsl:variable name="description">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Description']" />
	</xsl:variable>
	<xsl:variable name="voir">
		<xsl:value-of select="$translations/allheadings/headings[lang($local)]/heading[@category='Voir']" />
	</xsl:variable>

	<xsl:template match="/">
		<article lang="{$local}">
			<title>
				<xsl:variable name="fieldheader" select="$translations/allheadings/headings[lang($local)]/heading[@category='Dossier_de_tests']" />
				<xsl:value-of select="$fieldheader" />
				<emphasis>
					&#160;
					<xsl:value-of select="//ProjetVT/Nom/text()" />
				</emphasis>
			</title>
			<xsl:apply-templates mode="Projet" select="/SalomeStatique/ProjetVT" />
		</article>
	</xsl:template>
	<xsl:template match="ProjetVT" mode="Projet">
		<xsl:if test="$project = '1'">
			<section>
				<title>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PROJET']" />
					<xsl:value-of select="$fieldheader1" />
					<emphasis>
            &#160;
            <xsl:value-of select="./Nom/text()" />
          </emphasis>
				</title>
				<xsl:choose>
				  <xsl:when test="Description or Attachements or Params">
				    <xsl:apply-templates select="Description" mode="para" />
				    <xsl:apply-templates select="Attachements" mode="section" />
				    <xsl:apply-templates select="Params" mode="ProjetVT" />
				  </xsl:when>
				  <xsl:otherwise>
				    <para />
				  </xsl:otherwise>
				</xsl:choose>
			</section>
		</xsl:if>
		<xsl:if test="$environments = '1'">
			<xsl:apply-templates select="Environnements" mode="ProjetVT" />
		</xsl:if>
		<xsl:call-template name="extRoot1">
			<xsl:with-param name="current-node">
				<xsl:value-of select="saxon:path()" />
			</xsl:with-param>
		</xsl:call-template>
		<xsl:if test="$testplan = '1'">
			<xsl:apply-templates select="Familles" mode="ProjetVT" />
		</xsl:if>
	</xsl:template>
	<xsl:template name="extRoot1">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="Attachements" mode="section">
		<section>
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOCUMENTS']" />
				<xsl:value-of select="$fieldheader1" />
			</title>
			<xsl:apply-templates select="." mode="table" />
			<xsl:apply-templates select="FileAttachement" mode="inclusion" />
		</section>
	</xsl:template>
	<xsl:template match="Attachements" mode="table">
	  <xsl:param name="caption" />
		<informaltable>
			<tgroup cols="3">
				<colspec align="center" colwidth="1*" colname="col1" />
        <colspec colwidth="4*" colname="col2" />
        <colspec colwidth="4*" colname="col3" />
        <thead>
        	<xsl:if test="$caption='true'">
        		<row>
        		  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
        			<entry namest="col1" nameend="col3">
        				<xsl:variable name="fieldheader3"
        					select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
        				<xsl:value-of select="$fieldheader3" />
        			</entry>
        		</row>
        	</xsl:if>
        	<row>
        	  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
            <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
        		<entry align="center">N�</entry>
        		<entry align="center">
        			<xsl:value-of select="$nom" />
        		</entry>
        		<entry align="center">
        			<xsl:value-of select="$description" />
        		</entry>
        	</row>
        </thead>
        <tbody>
					<xsl:for-each select="UrlAttachement">
						<row>
							<entry align="center">
								<xsl:value-of select="count(preceding-sibling::UrlAttachement)+1"/>
							</entry>
							<entry>
								<ulink url="{@url}">
									<xsl:value-of select="@url" />
								</ulink>
							</entry>
							<entry>
								<xsl:choose>
                  <xsl:when test="Description[@isHTML='true']">
                    <xsl:apply-templates select="Description/* | Description/text()" mode="html2docbook" />
                  </xsl:when>
                  <xsl:when test="Description">
                    <xsl:call-template name="replaceCR">
                      <xsl:with-param name="textToReplace" select="Description/text()" />
                    </xsl:call-template>
                  </xsl:when>
                </xsl:choose>
							</entry>
						</row>
					</xsl:for-each>
					<xsl:for-each select="FileAttachement">
						<row>
							<entry align="center">
								<xsl:value-of
									select="count(../UrlAttachement)+count(preceding-sibling::FileAttachement)+1" />
							</entry>
							<entry>
								<xsl:apply-templates select="."
									mode="Projet" />
							</entry>
							<entry>
								<xsl:choose>
									<xsl:when	test="Description[@isHTML='true']">
										<xsl:apply-templates select="Description/* | Description/text()" mode="html2docbook" />
									</xsl:when>
									<xsl:when test="Description">
										<xsl:call-template name="replaceCR">
											<xsl:with-param name="textToReplace" select="Description/text()" />
										</xsl:call-template>
									</xsl:when>
								</xsl:choose>
							</entry>
						</row>
					</xsl:for-each>
				</tbody>
			</tgroup>
		</informaltable>
	</xsl:template>
	<xsl:template match="Params" mode="ProjetVT">
		<section id="paramProj">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='PARAMETRES']" />
				<xsl:value-of select="$fieldheader1" />
			</title>
			<informaltable>
				<tgroup cols="3">
					<colspec align="center" colwidth="1*" />
					<colspec colwidth="4*" />
					<colspec colwidth="4*" />
					<thead>
						<row>
						  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
							<entry align="center">N�</entry>
							<entry align="center">
								<xsl:value-of select="$nom" />
							</entry>
							<entry align="center">
								<xsl:value-of select="$description" />
							</entry>
						</row>
					</thead>
					<tbody>
						<xsl:apply-templates mode="Projet" select="Param" />
					</tbody>
				</tgroup>
			</informaltable>
		</section>
	</xsl:template>
	<xsl:template match="Param" mode="Projet">
		<row>
			<entry align="center">
				<xsl:value-of select="count(preceding-sibling::*)+1" />
			</entry>
			<entry>
				<xsl:value-of select="Nom/text()" />
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:apply-templates select="Description/* | Description/text()" mode="html2docbook" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceCR">
							<xsl:with-param name="textToReplace"
								select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="Environnements" mode="ProjetVT">
		<section>
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENTS']" />
				<xsl:value-of select="$fieldheader1" />
			</title>
			<informaltable>
				<tgroup cols="3">
					<colspec align="center" colwidth="1*" />
					<colspec colwidth="4*" />
					<colspec colwidth="4*" />
					<thead>
						<row>
						  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
							<entry align="center">N�</entry>
							<entry align="center">
								<xsl:value-of select="$nom" />
							</entry>
							<entry align="center">
								<xsl:value-of select="$description" />
							</entry>
						</row>
					</thead>
					<tbody>
						<xsl:apply-templates mode="ProjetVT" select="Environnement" />
					</tbody>
				</tgroup>
			</informaltable>
			<xsl:apply-templates select="Environnement" mode="Projet" />
		</section>
	</xsl:template>
	<xsl:template match="Environnement" mode="ProjetVT">
		<row>
			<entry align="center">
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="{@idEnv}">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:choose>
					<xsl:when test="Description[@isHTML='true']">
						<xsl:apply-templates select="Description/* | Description/text()" mode="html2docbook" />
					</xsl:when>
					<xsl:when test="Description">
						<xsl:call-template name="replaceCR">
							<xsl:with-param name="textToReplace" select="Description/text()" />
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="Environnement" mode="Projet">
		<section id="{@idEnv}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ENVIRONNEMENT']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
          &#160;
          <xsl:value-of select="Nom/text()" />
        </emphasis>
			</title>
			<xsl:if test="not(Description) and not(Attachements) and not(ValeurParams)">
				<para />
			</xsl:if>
			<xsl:apply-templates select="Description" mode="para" />
			<xsl:apply-templates select="Attachements" mode="section" />
			<xsl:apply-templates select="ValeurParams" mode="Environnement" />
			<xsl:apply-templates select="Script" mode="Environnement" />
		</section>
	</xsl:template>
	<xsl:template match="ValeurParams" mode="Environnement">
		<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
		<section>
			<title>
				<xsl:value-of select="$fieldheader2" />
			</title>
			<informaltable>
				<tgroup cols="3">
					<colspec align="center" colwidth="1*" />
					<colspec colwidth="4*" />
					<colspec colwidth="4*" />
					<thead>
						<row>
						  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
							<entry align="center">N�</entry>
							<entry align="center">
								<xsl:value-of select="$nom" />
							</entry>
							<entry align="center">
								<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Valeur']" />
								<xsl:value-of select="$fieldheader3" />
							</entry>
						</row>
					</thead>
					<tbody>
					<xsl:apply-templates select="ValeurParam" mode="Environnement" />
					</tbody>
				</tgroup>
			</informaltable>
		</section>
	</xsl:template>
	<xsl:template match="ValeurParam" mode="Environnement">
		<row>
			<entry align="center">
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="paramProj">
					<xsl:value-of select="Nom/text()" />
				</link>
			</entry>
			<entry>
				<xsl:value-of select="@valeur" />
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="Script" mode="Environnement">
		<section id="{@idEnv}_Script">
			<title>
				<xsl:variable name="fieldheader7"
					select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_d_environnement']" />
				<xsl:value-of select="$fieldheader7" />
			</title>
			<para>
				<variablelist>
					<varlistentry>
						<term>
							<xsl:variable name="fieldheader9"
								select="$translations/allheadings/headings[lang($local)]/heading[@category='Fichier']" />
							<xsl:value-of select="$fieldheader9" />
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when test="Text">
										<ulink url="{concat($urlBase,@dir)}">
											<xsl:value-of select="@nom" />
										</ulink>
										[
										<link linkend="{generate-id(@dir)}">
											<xsl:value-of select="$voir" />
										</link>
										]
									</xsl:when>
									<xsl:otherwise>
										<ulink url="{concat($urlBase,@dir)}">
											<xsl:value-of select="@nom" />
										</ulink>
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
					<varlistentry>
						<term>
							<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
							<xsl:value-of select="$fieldheader11" />
						</term>
						<listitem>
							<para>
								<xsl:value-of select="Classpath" />
							</para>
						</listitem>
					</varlistentry>
				</variablelist>
			</para>
		  <xsl:apply-templates select="." mode="inclusion" />
		</section>
	</xsl:template>
	<xsl:template match="Script" mode="inclusion">
    <xsl:variable name="target">
      <xsl:value-of select="generate-id(@dir)" />
    </xsl:variable>
    <xsl:if test="Text">
      <formalpara id="{$target}">
        <title>
          <xsl:value-of select="@nom" />
        </title>
        <para>
          <programlisting><xsl:value-of select="Text/." />
          </programlisting>
        </para>
      </formalpara>
    </xsl:if>
  </xsl:template>
	<xsl:template match="Familles" mode="ProjetVT">
		<section>
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='DOSSIER_DE_TESTS']" />
				<xsl:value-of select="$fieldheader1" />
			</title>
			<xsl:apply-templates select="Famille" mode="ProjetVT" />
		</section>
	</xsl:template>
	<xsl:template match="Famille" mode="ProjetVT">
		<section id="{@id_famille}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='FAMILLE']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
          &#160;
          <xsl:value-of select="Nom/text()" />
        </emphasis>
			</title>
			<xsl:if test="not(Description) and not(Attachements)">
				<para />
			</xsl:if>
			<xsl:apply-templates select="Description" mode="para" />
			<xsl:apply-templates select="Attachements" mode="table">
			  <xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:call-template name="extFamily1">
        <xsl:with-param name="current-node">
          <xsl:value-of select="saxon:path()" />
        </xsl:with-param>
      </xsl:call-template>
			<xsl:apply-templates select="SuiteTests/SuiteTest" mode="ProjetVT" />
			<xsl:call-template name="extFamily2">
        <xsl:with-param name="current-node">
          <xsl:value-of select="saxon:path()" />
        </xsl:with-param>
      </xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extFamily1">
    <xsl:param name="current-node" />
  </xsl:template>
  <xsl:template name="extFamily2">
    <xsl:param name="current-node" />
  </xsl:template>
	<xsl:template match="SuiteTest" mode="ProjetVT">
		<section id="{@id_suite}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Suite_de_tests']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
          &#160;
          <xsl:value-of select="Nom/text()" />
        </emphasis>
			</title>
			<xsl:if test="not(Description) and not(Attachements)">
				<para />
			</xsl:if>
			<xsl:apply-templates select="Description" mode="para" />
			<xsl:apply-templates select="Attachements" mode="table">
			  <xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
			<xsl:call-template name="extSuite1">
        <xsl:with-param name="current-node">
          <xsl:value-of select="saxon:path()" />
        </xsl:with-param>
      </xsl:call-template>
			<xsl:apply-templates select="Tests/Test" mode="ProjetVT" />
			<xsl:call-template name="extSuite2">
        <xsl:with-param name="current-node">
          <xsl:value-of select="saxon:path()" />
        </xsl:with-param>
      </xsl:call-template>
		</section>
	</xsl:template>
	<xsl:template name="extSuite1">
    <xsl:param name="current-node" />
  </xsl:template>
  <xsl:template name="extSuite2">
    <xsl:param name="current-node" />
  </xsl:template>
	<xsl:template match="Test" mode="ProjetVT">
		<section id="{@id_test}">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
				<xsl:value-of select="$fieldheader1" />
				<emphasis>
					&#160;
					<xsl:value-of select="Nom/text()" />
				</emphasis>
			</title>
			<para>
				<variablelist>
					<xsl:apply-templates select="Description" mode="variablelist" />
					<xsl:if test="Concepteur">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader2"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Concepteur_du_test']" />
									<xsl:value-of
										select="$fieldheader2" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of
										select="Concepteur/Nom/text()" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader3"
									select="$translations/allheadings/headings[lang($local)]/heading[@category='Type_de_test']" />
								<xsl:value-of select="$fieldheader3" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when test="TestAuto">
										<xsl:variable
											name="fieldheader4"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Automatique']" />
										<xsl:value-of
											select="$fieldheader4" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:variable
											name="fieldheader5"
											select="$translations/allheadings/headings[lang($local)]/heading[@category='Test_Manuel']" />
										<xsl:value-of
											select="$fieldheader5" />
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
					<xsl:if test="TestAuto">
						<varlistentry>
							<term>
								<emphasis>
									<xsl:variable name="fieldheader6"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Plugin']" />
									<xsl:value-of
										select="$fieldheader6" />
								</emphasis>
							</term>
							<listitem>
								<para>
									<xsl:value-of
										select="TestAuto/@plug_ext" />
								</para>
							</listitem>
						</varlistentry>
					</xsl:if>
					<xsl:apply-templates select="TestAuto"
						mode="Projet" />
				</variablelist>
			</para>
			<xsl:apply-templates select="TestAuto/Script"
				mode="inclusion" />
			<xsl:apply-templates select="TestManuel" mode="Projet" />
			<xsl:apply-templates select="Attachements" mode="table">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement"
				mode="inclusion" />
			<xsl:call-template name="extTest">
				<xsl:with-param name="current-node">
					<xsl:value-of select="saxon:path()" />
				</xsl:with-param>
			</xsl:call-template>
			<xsl:if test="ParamsT">
				<informaltable>
					<tgroup cols="2">
						<colspec align="center" colwidth="1*" colname="col1" />
						<colspec colwidth="8*" colname="col2" />
						<thead>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
								<entry namest="col1" nameend="col2">
									<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Parametres_utilises']" />
									<xsl:value-of select="$fieldheader9" />
								</entry>
							</row>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry align="center">N�</entry>
								<entry align="center">
									<xsl:value-of select="$nom" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:apply-templates select="ParamsT/ParamT" mode="Projet" />
						</tbody>
					</tgroup>
				</informaltable>
			</xsl:if>
		</section>
	</xsl:template>
	<xsl:template name="extTest">
		<xsl:param name="current-node" />
	</xsl:template>
	<xsl:template match="ParamT" mode="Projet">
		<row>
			<entry align="center">
				<xsl:number value="position()" />
			</entry>
			<entry>
				<link linkend="paramProj">
					<xsl:value-of select="id(@ref)/Nom/text()" />
				</link>
			</entry>
		</row>
	</xsl:template>
	<xsl:template match="TestAuto" mode="Projet">
	  <varlistentry>
      <term>
        <emphasis>
          <xsl:variable name="fieldheader1"
            select="$translations/allheadings/headings[lang($local)]/heading[@category='Script_de_test']" />
          <xsl:value-of select="$fieldheader1" />
        </emphasis>
      </term>
      <listitem>
        <para>
          <xsl:choose>
            <xsl:when test="Script/Text">
              <ulink
                url="{concat($urlBase,Script/@dir)}">
                <xsl:value-of select="Script/@nom" />
              </ulink>
              &#160;[
              <link linkend="{generate-id(Script/@dir)}">
                <xsl:value-of select="$voir" />
              </link>
              ]
            </xsl:when>
            <xsl:otherwise>
              <ulink
                url="{concat($urlBase,Script/@dir)}">
                <xsl:value-of select="Script/@nom" />
              </ulink>
            </xsl:otherwise>
          </xsl:choose>
        </para>
      </listitem>
    </varlistentry>
	</xsl:template>
	<xsl:template match="TestManuel" mode="Projet">
		<xsl:if test="ActionTest">
			<para>
				<itemizedlist>
					<title>
						<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Actions_du_test']" />
						<xsl:value-of select="$fieldheader1" />
					</title>
					<xsl:apply-templates select="ActionTest" mode="Projet" />
				</itemizedlist>
			</para>
			<xsl:apply-templates select="ActionTest/Attachements/FileAttachement" mode="inclusion" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="ActionTest" mode="Projet">
	  <listitem>
      <para>
        <emphasis>
          <xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Nom']" />
          <xsl:value-of select="$fieldheader4" />
          :
        </emphasis>
        <xsl:value-of select="Nom/text()" />
        <xsl:variable name="actionContent">
          <xsl:apply-templates select="Description" mode="list" />
          <xsl:if test="ResultAttendu">
            <listitem>
              <para>
                <emphasis>
                  <xsl:variable name="fieldheader4"
                    select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_attendu']" />
                  <xsl:value-of
                    select="$fieldheader4" />
                  :
                </emphasis>
                <xsl:call-template name="replaceCR">
                  <xsl:with-param name="textToReplace"
                    select="ResultAttendu/text()" />
                </xsl:call-template>
              </para>
            </listitem>
          </xsl:if>
          <xsl:if test="Attachements">
            <listitem>
              <para>
                <emphasis>
                  <xsl:variable name="fieldheader5"
                    select="$translations/allheadings/headings[lang($local)]/heading[@category='Documents']" />
                  <xsl:value-of
                    select="$fieldheader5" />
                  :
                </emphasis>
                <itemizedlist>
                  <xsl:for-each
                    select="Attachements/UrlAttachement">
                    <listitem>
                      <para>
                        <ulink url="{@url}">
                          <xsl:value-of
                            select="@url" />
                        </ulink>
                      </para>
                    </listitem>
                  </xsl:for-each>
                  <xsl:for-each
                    select="Attachements/FileAttachement">
                    <listitem>
                      <para>
                        <xsl:apply-templates select="." mode="Projet" />
                      </para>
                    </listitem>
                  </xsl:for-each>
                </itemizedlist>
              </para>
            </listitem>
          </xsl:if>
          <xsl:call-template name="extAction">
            <xsl:with-param name="current-node">
              <xsl:value-of select="saxon:path()" />
            </xsl:with-param>
          </xsl:call-template>
        </xsl:variable>
        <xsl:if test="count($actionContent//*)>0">
          <itemizedlist>
            <xsl:copy-of select="$actionContent" />
          </itemizedlist>
        </xsl:if>
      </para>
    </listitem>
	</xsl:template>
	<xsl:template name="extAction">
    <xsl:param name="current-node" />
  </xsl:template>
	<xsl:template match="FileAttachement" mode="Projet">
		<xsl:choose>
			<xsl:when test="($gif = '1') and (substring-after(@nom, '.') = 'gif')
							or ($jpeg = '1') and ((substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg'))
							or ($png = '1') and (substring-after(@nom, '.') = 'png')
							or Text">
				<ulink url="{concat($urlBase,@dir)}">
					<xsl:value-of select="@nom" />
				</ulink>
				<xsl:variable name="target">
						<xsl:value-of select="generate-id(@dir)"/>
				</xsl:variable>
				&#160;[<link linkend="{$target}">
						<xsl:value-of select="$voir" />
				</link>]
			</xsl:when>
			<xsl:otherwise>
				<ulink url="{concat($urlBase,@dir)}">
					<xsl:value-of select="@nom" />
				</ulink>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="FileAttachement" mode="inclusion">
		<xsl:variable name="target">
			<xsl:value-of select="generate-id(@dir)" />
		</xsl:variable>
		<xsl:if test="$gif = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'gif'">
			  <formalpara id="{$target}">
			    <title>
			      <xsl:value-of select="@nom" />
			    </title>
			    <para>
			      <inlinegraphic
			        scalefit="1"
			        width="100%"
			        contentdepth="100%"
			        align="center"
			        format="GIF"
			        fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
			    </para>
			  </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$jpeg = '1'">
			<xsl:if	test="(substring-after(@nom, '.') = 'jpg') or (substring-after(@nom, '.') = 'jpeg')">
			  <formalpara id="{$target}">
			    <title>
			      <xsl:value-of select="@nom" />
			    </title>
			    <para>
			      <inlinegraphic
			        scalefit="1"
			        width="100%"
			        contentdepth="100%"
			        align="center"
			        format="JPG"
			        fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
			    </para>
			  </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="$png = '1'">
			<xsl:if test="substring-after(@nom, '.') = 'png'">
			  <formalpara id="{$target}">
			    <title>
			      <xsl:value-of select="@nom" />
			    </title>
			    <para>
			      <inlinegraphic
			        scalefit="1"
			        width="100%"
			        contentdepth="100%"
			        align="center"
			        format="PNG"
			        fileref="{substring-after(concat($urlBase,@dir), 'file:')}" />
			    </para>
			  </formalpara>
			</xsl:if>
		</xsl:if>
		<xsl:if test="Text">
		  <formalpara id="{$target}">
		    <title>
		      <xsl:value-of select="@nom" />
		    </title>
		    <para>
		      <simplelist type="horiz" columns="1">
		        <xsl:choose>
		        	<xsl:when test="Text/Ligne">
		        		<xsl:for-each select="Text/ligne">
		        			<member>
		        				<xsl:value-of select="text()" />
		        			</member>
		        		</xsl:for-each>
		        	</xsl:when>
		        	<xsl:otherwise>
		        		<member />
		        	</xsl:otherwise>
		        </xsl:choose>
		      </simplelist>
		    </para>
		  </formalpara>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Description" mode="para">
		<xsl:if test="normalize-space(.) != ''">
			<para>
				<variablelist>
					<varlistentry>
						<term>
							<emphasis>
								<xsl:value-of select="$description" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:choose>
									<xsl:when test="@isHTML='true'">
										&#160;
										<xsl:apply-templates select="* | text()" mode="html2docbook" />
									</xsl:when>
									<xsl:otherwise>
										<xsl:call-template name="replaceCR">
											<xsl:with-param name="textToReplace" select="./text()" />
										</xsl:call-template>
									</xsl:otherwise>
								</xsl:choose>
							</para>
						</listitem>
					</varlistentry>
				</variablelist>
			</para>
		</xsl:if>
	</xsl:template>
	<xsl:template match="Description" mode="list">
    <listitem>
      <para>
        <xsl:apply-templates select="." mode="Projet" />
      </para>
    </listitem>
  </xsl:template>
  <xsl:template match="Description" mode="variablelist">
    <varlistentry>
      <term>
        <emphasis>
          <xsl:value-of select="$description" />
        </emphasis>
      </term>
      <listitem>
        <para>
          <xsl:choose>
            <xsl:when test="@isHTML='true'">
              &#160;
              <xsl:apply-templates select="* | text()" mode="html2docbook" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:call-template name="replaceCR">
                <xsl:with-param name="textToReplace" select="./text()" />
              </xsl:call-template>
            </xsl:otherwise>
          </xsl:choose>
        </para>
      </listitem>
    </varlistentry>
  </xsl:template>
  <xsl:template match="Description" mode="Projet">
    <xsl:if test="normalize-space(.) != ''">
      <emphasis>
        <xsl:value-of select="$description" />
        :
      </emphasis>
      <xsl:choose>
        <xsl:when test="@isHTML='true'">
          &#160;
          <xsl:apply-templates select="* | text()"
            mode="html2docbook" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="replaceCR">
            <xsl:with-param name="textToReplace" select="./text()" />
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
	<xsl:template name="replaceCR">
		<xsl:param name="textToReplace" />
		<xsl:choose>
			<xsl:when test="contains($textToReplace, '\n')">
				<xsl:value-of select="substring-before($textToReplace,'\n')" />
				&#160;
				<xsl:call-template name="replaceCR">
					<xsl:with-param name="textToReplace" select="substring-after($textToReplace,'\n')" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$textToReplace" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="p" mode="html2docbook">
    <xsl:apply-templates select="* | text()" mode="html2docbook" />
  </xsl:template>

  <xsl:template match="ul" mode="html2docbook">
    <itemizedlist>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </itemizedlist>
  </xsl:template>

  <xsl:template match="ol" mode="html2docbook">
    <orderedlist>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </orderedlist>
  </xsl:template>

  <xsl:template match="li" mode="html2docbook">
    <listitem>
      <xsl:choose>
        <xsl:when test="count(p) = 0">
          <para>
            <xsl:apply-templates select="* | text()"
              mode="html2docbook" />
          </para>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="* | text()"
            mode="html2docbook" />
        </xsl:otherwise>
      </xsl:choose>
    </listitem>
  </xsl:template>

  <!--  tables -->

  <xsl:template match="table" mode="html2docbook">
    <informaltable>
      <tgroup cols="{count(tr)}">
        <tbody>
          <xsl:apply-templates select="* | text()"
            mode="html2docbook" />
        </tbody>
      </tgroup>
    </informaltable>
  </xsl:template>

  <xsl:template match="tr" mode="html2docbook">
    <row>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </row>
  </xsl:template>

  <xsl:template match="td" mode="html2docbook">
    <entry>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </entry>
  </xsl:template>

  <xsl:template match="*" mode="html2docbook">
    <xsl:message>
      No template for
      <xsl:value-of select="name()" />
    </xsl:message>
    <xsl:apply-templates select="* | text()" mode="html2docbook" />
  </xsl:template>

  <xsl:template match="@*" mode="html2docbook">
    <xsl:message>
      No template for
      <xsl:value-of select="name()" />
    </xsl:message>
  </xsl:template>

  <!-- inline formatting -->
  <xsl:template match="b" mode="html2docbook">
    <emphasis role="bold">
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </emphasis>
  </xsl:template>
  <xsl:template match="i" mode="html2docbook">
    <emphasis>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </emphasis>
  </xsl:template>
  <xsl:template match="u" mode="html2docbook">
    <citetitle>
      <xsl:apply-templates select="* | text()"
        mode="html2docbook" />
    </citetitle>
  </xsl:template>
  <xsl:template match="br" mode="html2docbook">
    <xsl:processing-instruction name="lb" />
  </xsl:template>

  <xsl:template match="text()" mode="html2docbook">
    <xsl:choose>
      <xsl:when test="normalize-space(.) = ''"></xsl:when>
      <xsl:otherwise>
        <xsl:copy />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>