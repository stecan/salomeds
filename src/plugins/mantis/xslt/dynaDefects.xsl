<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
    Document   : dynaDefects.xsl
    Created on : 16 avril 2007, 13:24
    Author     : vapu8214
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	  <xsl:param name="anomalies" />

	<xsl:template name="extSummaryRoot1">
	  <xsl:if test="$anomalies = '1'">
  	  <xsl:apply-templates mode="Sommaire" select="Defects" />
  	</xsl:if>
  </xsl:template>
	<xsl:template match="Defects" mode="Sommaire">
  	<li>
    	<a href="#Defects">
    		<saxon:assign name="h1" select="my:accu($h1)" />
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ANOMALIES']" />
				<xsl:value-of select="$fieldheader1" />
			</a>
		</li>
	</xsl:template>

	<xsl:template name="extRoot1">
	  <xsl:if test="$anomalies = '1'">
		  <xsl:apply-templates select="Defects" mode="Projet" />
	  </xsl:if>
	</xsl:template>
	<xsl:template match="Defects" mode="Projet">
		<br />
		<hr />
		<h1>
			<a name="Defects" />
			<saxon:assign name="h1" select="my:accu($h1)" />
			<xsl:value-of select="$h1" />&#160;
			<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ANOMALIES']" />
			<xsl:value-of select="$fieldheader1" />
			<span>
				<xsl:value-of select="$h1" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ANOMALIES']" />
				<xsl:value-of select="$fieldheader1" />
			</span>
		</h1>
		<saxon:assign name="h2" select="0" />
		<saxon:assign name="h3" select="0" />
		<saxon:assign name="h4" select="0" />
		<saxon:assign name="h5" select="0" />
		<p>
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Gestionnaire_anomalie']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@defect_manager" />
			<br />
			<b>
				<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Host']" />
				<xsl:value-of select="$fieldheader3" />
				&#160;:&#160;
			</b>
			<xsl:value-of select="@host" />
			<br />
		</p>
		<xsl:apply-templates mode="Projet" select="Defect[@id=//CampagneTests//DefectsLink/@ref]">
		  <xsl:sort select="@id" order="ascending"/>
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="Defect" mode="Projet">
		<xsl:variable name="id_bug" select="@id" />
		<b>
			<font color="#005A9C">
				<a name="{@id}" />
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Anomalie']" />
				<xsl:value-of select="$fieldheader1" />
				&#160;ID =
				<xsl:value-of select="substring($id_bug,5)" />
			</font>
		</b>
		<ul>
			<li>
				<b>
					<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume']" />
					<xsl:value-of select="$fieldheader2" />
					&#160;:&#160;
				</b>
				<xsl:call-template name="replaceByBr">
					<xsl:with-param	name="textToReplace" select="@summary" />
				</xsl:call-template>
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Reporter']" />
					<xsl:value-of select="$fieldheader3" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@reporter" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Handler']" />
					<xsl:value-of select="$fieldheader4" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="@handler" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
					<xsl:value-of select="$fieldheader5" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Priority/@value" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Severite']" />
					<xsl:value-of select="$fieldheader6" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Severity/@value" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statut']" />
					<xsl:value-of select="$fieldheader7" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Status/@value" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Reproductibilite']" />
					<xsl:value-of select="$fieldheader8" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Reproducibility/@value" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resolution']" />
					<xsl:value-of select="$fieldheader9" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Resolution/@value" />
			</li>
			<li>
				<b>
					<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
					<xsl:value-of select="$fieldheader10" />
					&#160;:&#160;
				</b>
				<xsl:value-of select="Info/@environment" />
			</li>
			<xsl:if test="normalize-space(Info/@platform) != ''">
				<li>
					<b>
						<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='PlateForme']" />
						<xsl:value-of select="$fieldheader11" />
						&#160;:&#160;
					</b>
					<xsl:value-of select="Info/@platform" />
				</li>
			</xsl:if>
			<xsl:if test="normalize-space(Info/@os) != ''">
				<li>
					<b>
						<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='OS']" />
						<xsl:value-of select="$fieldheader12" />
						&#160;:&#160;
					</b>
					<xsl:value-of select="Info/@os" />
				</li>
			</xsl:if>
			<li>
				<b>
					<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Lien']" />
					<xsl:value-of select="$fieldheader13" />
					&#160;:&#160;
				</b>
				<a href="{MantisURL/@url}" target="_blank">
					<xsl:value-of select="MantisURL/@url" />
				</a>
			</li>
			<xsl:if test="DefectRelations/DefectRelation">
				<b>
					<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Relations']" />
					<xsl:value-of select="$fieldheader13" />
					&#160;:&#160;
				</b>
				<xsl:for-each select="DefectRelations/DefectRelation">
					<a href="#{@ref}">
						<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Anomalie']" />
						<xsl:value-of select="$fieldheader1" />
						&#160;ID =
						<xsl:value-of select="substring(@ref,5)" />
					</a>
					&#160;
				</xsl:for-each>
			</xsl:if>
		</ul>
		<xsl:if test="//ExecCampTest/DefectsLinks/DefectsLink[@ref=$id_bug]">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Liens']" />
					<xsl:value-of select="$fieldheader1" />
				</caption>
				<tr>
					<th class="titre">N�</th>
					<th class="titre" width="18%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
					<th class="titre" width="18%">
						<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
						<xsl:value-of select="$fieldheader2" />
					</th>
					<th class="titre" width="18%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
					<th class="titre" width="18%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
					<th class="titre" width="18%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
				</tr>
				<xsl:apply-templates select="//ExecCampTest/DefectsLinks/DefectsLink[@ref=$id_bug]" mode="defectGlobal" />
			</table>
			<br />
		</xsl:if>
		<xsl:apply-templates select="Attachements" mode="Projet">
			<xsl:with-param name="caption">true</xsl:with-param>
		</xsl:apply-templates>
		<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		<xsl:if test="Attachements">
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template match="DefectsLink" mode="defectGlobal">
		<tr>
			<td class="tab_center">
				<xsl:number value="position()" format="1" />
			</td>
			<td class="tab">
				<a href="#{EnvironmentRef/@id}">
				  <xsl:value-of select="EnvironmentRef/@name" />
				</a>
			</td>
			<td class="tab">
				<a href="#{TestRef/@ref}">
				  <xsl:value-of select="TestRef/Nom/text()" />
				</a>
			</td>
			<td class="tab">
				<a href="#{CampaignRef/@id}">
				  <xsl:value-of select="CampaignRef/@name" />
				</a>
			</td>
			<td class="tab">
				<a href="#{ExecutionRef/@id}">
				  <xsl:value-of select="ExecutionRef/@name" />
				</a>
			</td>
			<td class="tab">
				<a href="#{ExecutionResultRef/@id}">
				  <xsl:value-of select="ExecutionResultRef/@name" />
				</a>
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="extSummaryEnvironment">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)" mode="defectsSummary" />
	</xsl:template>
	<xsl:template match="Environnement" mode="defectsSummary">
		<xsl:if test="DefectsLinks/DefectsLink">
			<li>
				<a href="#{@idEnv}_Defect">
					<saxon:assign name="h3" select="my:accu($h3)" />
					<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
					<xsl:value-of select="$fieldheader1" />
				</a>
			</li>
		</xsl:if>
	</xsl:template>

	<xsl:template name="extEnvironment">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)" mode="defects" />
	</xsl:template>
	<xsl:template match="Environnement" mode="defects">
		<xsl:if test="DefectsLinks/DefectsLink[@ref=//CampagneTests//DefectsLink/@ref]">
			<h3>
				<a name="{@idEnv}_Defect" />
				<saxon:assign name="h3" select="my:accu($h3)" />
				<xsl:value-of select="$h1" />.<xsl:value-of select="$h2" />.<xsl:value-of select="$h3" />&#160;
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
				<xsl:value-of select="$fieldheader1" />
			</h3>
			<saxon:assign name="h4" select="0" />
			<xsl:apply-templates select="DefectsLinks" mode="defect" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="DefectsLinks" mode="defect">
		<xsl:if test="DefectsLink[@ref=//CampagneTests//DefectsLink/@ref]">
			<table class="tab">
				<caption>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
					<xsl:value-of select="$fieldheader1" />
				</caption>
				<tr>
					<th class="titre">ID</th>
					<th class="titre" width="15%">
						<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Gestionnaire_anomalie']" />
						<xsl:value-of select="$fieldheader2" />
					</th>
					<th class="titre" width="25%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
					<th class="titre" width="25%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
					<th class="titre" width="25%">
						<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statut']" />
						<xsl:value-of select="$fieldheader3" />
					</th>
				</tr>
				<xsl:apply-templates select="DefectsLink[@ref=//CampagneTests//DefectsLink/@ref]" mode="defect">
				  <xsl:sort select="@ref" order="ascending" /> 
				</xsl:apply-templates>
			</table>
			<br />
		</xsl:if>
	</xsl:template>
	<xsl:template match="DefectsLink" mode="defect">
		<tr>
			<td class="tab_center">
				<a href="#{@ref}">
					<xsl:value-of select="substring(@ref,5)" />
				</a>
			</td>
			<td class="tab">
				<xsl:value-of select="@defect_manager" />
			</td>
			<td class="tab">
				<xsl:call-template name="replaceByBr">
					<xsl:with-param	name="textToReplace" select="Defect/@summary" />
				</xsl:call-template>
			</td>
			<td class="tab">
				<xsl:value-of select="Defect/Priority/@value" />
			</td>
			<td class="tab">
				<xsl:value-of select="Defect/Status/@value" />
			</td>
		</tr>
	</xsl:template>

	<xsl:template name="extTest">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extExec">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>
</xsl:stylesheet>