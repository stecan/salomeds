<?xml version="1.0" encoding="ISO-8859-1" ?>

<!--
    Document   : dynaDefects.xsl
    Created on : 16 avril 2007, 13:24
    Author     : vapu8214
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:my="http://rd.francetelecom.com/"
	xmlns:saxon="http://saxon.sf.net/"
	exclude-result-prefixes="saxon"
	extension-element-prefixes="saxon">
	  <xsl:param name="anomalies" />

	<xsl:template name="extRoot1">
	  <xsl:if test="$anomalies = '1'">
		  <xsl:apply-templates select="Defects" mode="Projet" />
		</xsl:if>
	</xsl:template>
	<xsl:template match="Defects" mode="Projet">
		<section id="Defects">
			<title>
				<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='ANOMALIES']" />
				<xsl:value-of select="$fieldheader1" />
			</title>
			<para>
				<simplelist type="horiz" columns="1">
					<member>
						<emphasis>
							<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Gestionnaire_anomalie']" />
							<xsl:value-of select="$fieldheader3" />
							:
						</emphasis>
						<xsl:value-of select="@defect_manager" />
					</member>
					<member>
						<emphasis>
							<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Host']" />
							<xsl:value-of select="$fieldheader3" />
							:
						</emphasis>
						<xsl:value-of select="@host" />
					</member>
				</simplelist>
			</para>
			<xsl:apply-templates mode="Projet" select="Defect[@id=//CampagneTests//DefectsLink/@ref]">
				<xsl:sort select="@id" order="ascending"/>
			</xsl:apply-templates>
		</section>
	</xsl:template>
	<xsl:template match="Defect" mode="Projet">
		<xsl:variable name="id_bug" select="@id" />
		<para>
			<variablelist>
				<title>
					<emphasis id="{@id}">
						<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Anomalie']" />
						<xsl:value-of select="$fieldheader1" />
						&#160;ID =
						<xsl:value-of select="substring($id_bug,5)" />
					</emphasis>
				</title>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume']" />
							<xsl:value-of select="$fieldheader2" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:call-template name="replaceByBr">
								<xsl:with-param name="textToReplace" select="@summary" />
							</xsl:call-template>
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Reporter']" />
							<xsl:value-of select="$fieldheader3" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="@reporter" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Handler']" />
							<xsl:value-of select="$fieldheader4" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="@handler" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
							<xsl:value-of select="$fieldheader5" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Priority/@value" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader6" select="$translations/allheadings/headings[lang($local)]/heading[@category='Severite']" />
							<xsl:value-of select="$fieldheader6" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Severity/@value" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader7" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statut']" />
							<xsl:value-of select="$fieldheader7" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Status/@value" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader8" select="$translations/allheadings/headings[lang($local)]/heading[@category='Reproductibilite']" />
							<xsl:value-of select="$fieldheader8" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Reproducibility/@value" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader9" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resolution']" />
							<xsl:value-of select="$fieldheader9" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Resolution/@value" />
						</para>
					</listitem>
				</varlistentry>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader10" select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
							<xsl:value-of select="$fieldheader10" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<xsl:value-of select="Info/@environment" />
						</para>
					</listitem>
				</varlistentry>
				<xsl:if test="normalize-space(Info/@platform) != ''">
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader11" select="$translations/allheadings/headings[lang($local)]/heading[@category='PlateForme']" />
								<xsl:value-of select="$fieldheader11" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of select="Info/@platform" />
							</para>
						</listitem>
					</varlistentry>
				</xsl:if>
				<xsl:if test="normalize-space(Info/@os) != ''">
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader12" select="$translations/allheadings/headings[lang($local)]/heading[@category='OS']" />
								<xsl:value-of select="$fieldheader12" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:value-of select="Info/@os" />
							</para>
						</listitem>
					</varlistentry>
				</xsl:if>
				<varlistentry>
					<term>
						<emphasis>
							<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Lien']" />
							<xsl:value-of select="$fieldheader13" />
						</emphasis>
					</term>
					<listitem>
						<para>
							<ulink url="{MantisURL/@url}">
								<xsl:value-of select="MantisURL/@url" />
							</ulink>
						</para>
					</listitem>
				</varlistentry>
				<xsl:if test="DefectRelations/DefectRelation">
					<varlistentry>
						<term>
							<emphasis>
								<xsl:variable name="fieldheader13" select="$translations/allheadings/headings[lang($local)]/heading[@category='Relations']" />
								<xsl:value-of select="$fieldheader13" />
							</emphasis>
						</term>
						<listitem>
							<para>
								<xsl:for-each select="DefectRelations/DefectRelation">
									<link linkend="{@ref}">
										<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Anomalie']" />
										<xsl:value-of select="$fieldheader1" />
										ID =
										<xsl:value-of select="substring(@ref,5)" />
									</link>
									<xsl:if test="position() != last()">
										,&#160;
									</xsl:if>
								</xsl:for-each>
							</para>
						</listitem>
					</varlistentry>
				</xsl:if>
      </variablelist>
			<xsl:if test="//ExecCampTest/DefectsLinks/DefectsLink[@ref=$id_bug]">
				<informaltable>
					<tgroup cols="6">
						<colspec align="center" colwidth="1*"
							colname="col1" />
						<colspec colwidth="3*" colname="col2" />
						<colspec colwidth="3*" colname="col3" />
						<colspec colwidth="3*" colname="col4" />
						<colspec colwidth="3*" colname="col5" />
						<colspec colwidth="3*" colname="col6" />
						<thead>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
								<entry namest="col1" nameend="col6">
									<xsl:variable name="fieldheader1"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Liens']" />
									<xsl:value-of
										select="$fieldheader1" />
								</entry>
							</row>
							<row>
							  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
                <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
								<entry>N�</entry>
								<entry>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Environnement']" />
									<xsl:value-of
										select="$fieldheader3" />
								</entry>
								<entry>
									<xsl:variable name="fieldheader2"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Test']" />
									<xsl:value-of
										select="$fieldheader2" />
								</entry>
								<entry>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Campagne']" />
									<xsl:value-of
										select="$fieldheader3" />
								</entry>
								<entry>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Execution']" />
									<xsl:value-of
										select="$fieldheader3" />
								</entry>
								<entry>
									<xsl:variable name="fieldheader3"
										select="$translations/allheadings/headings[lang($local)]/heading[@category='Resultat_d_execution']" />
									<xsl:value-of
										select="$fieldheader3" />
								</entry>
							</row>
						</thead>
						<tbody>
							<xsl:apply-templates
								select="//ExecCampTest/DefectsLinks/DefectsLink[@ref=$id_bug]"
								mode="defectGlobal" />
						</tbody>
					</tgroup>
				</informaltable>
			</xsl:if>
			<xsl:apply-templates select="Attachements" mode="Projet">
				<xsl:with-param name="caption">true</xsl:with-param>
			</xsl:apply-templates>
			<xsl:apply-templates select="Attachements/FileAttachement" mode="inclusion" />
		</para>
	</xsl:template>
	<xsl:template match="DefectsLink" mode="defectGlobal">
		<row>
			<entry>
				<xsl:number value="position()" format="1" />
			</entry>
			<entry>
				<link linkend="{EnvironmentRef/@id}">
				  <xsl:value-of select="EnvironmentRef/@name" />
				</link>
			</entry>
			<entry>
				<link linkend="{TestRef/@ref}">
				  <xsl:value-of select="TestRef/Nom/text()" />
				</link>
			</entry>
			<entry>
				<link linkend="{CampaignRef/@id}">
				  <xsl:value-of select="CampaignRef/@name" />
				</link>
			</entry>
			<entry>
				<link linkend="{ExecutionRef/@id}">
				  <xsl:value-of select="ExecutionRef/@name" />
				</link>
			</entry>
			<entry>
				<link linkend="{ExecutionResultRef/@id}">
				  <xsl:value-of select="ExecutionResultRef/@name" />
				</link>
			</entry>
		</row>
	</xsl:template>

	<xsl:template name="extEnvironment">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="saxon:evaluate($current-node)" mode="defects" />
	</xsl:template>
	<xsl:template match="Environnement" mode="defects">
		<xsl:if test="DefectsLinks/DefectsLink">
			<section id="{@idEnv}_Defect">
				<title>
					<xsl:variable name="fieldheader1" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
					<xsl:value-of select="$fieldheader1" />
				</title>
				<xsl:apply-templates select="DefectsLinks" mode="defect" />
			</section>
		</xsl:if>
	</xsl:template>
	<xsl:template match="DefectsLinks" mode="defect">
		<xsl:if test="DefectsLink[@ref=//CampagneTests//DefectsLink/@ref]">
			<informaltable>
				<tgroup cols="5">
					<colspec align="center" colwidth="1*" colname="col1" />
					<colspec colwidth="4*" colname="col2" />
          <colspec colwidth="4*" colname="col3" />
          <colspec colwidth="4*" colname="col4" />
          <colspec colwidth="4*" colname="col5" />
					<thead>
					  <row>
					    <xsl:processing-instruction name="dbhtml">bgcolor="#B4CDCD"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#B4CDCD"</xsl:processing-instruction>
					    <entry namest="col1" nameend="col5">
					      <xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Fiche_d_anomalies']" />
                <xsl:value-of select="$fieldheader2" />
					    </entry>
					  </row>
						<row>
						  <xsl:processing-instruction name="dbhtml">bgcolor="#D1EEEE"</xsl:processing-instruction>
              <xsl:processing-instruction name="dbfo">bgcolor="#D1EEEE"</xsl:processing-instruction>
							<entry>ID</entry>
							<entry>
								<xsl:variable name="fieldheader2" select="$translations/allheadings/headings[lang($local)]/heading[@category='Gestionnaire_anomalie']" />
								<xsl:value-of select="$fieldheader2" />
							</entry>
							<entry>
								<xsl:variable name="fieldheader3" select="$translations/allheadings/headings[lang($local)]/heading[@category='Resume']" />
								<xsl:value-of select="$fieldheader3" />
							</entry>
							<entry>
								<xsl:variable name="fieldheader4" select="$translations/allheadings/headings[lang($local)]/heading[@category='Priorite']" />
								<xsl:value-of select="$fieldheader4" />
							</entry>
							<entry>
								<xsl:variable name="fieldheader5" select="$translations/allheadings/headings[lang($local)]/heading[@category='Statut']" />
								<xsl:value-of select="$fieldheader5" />
							</entry>
						</row>
					</thead>
					<tbody>	
						<xsl:apply-templates select="DefectsLink[@ref=//CampagneTests//DefectsLink/@ref]" mode="defect">
							<xsl:sort select="@ref" order="ascending"/>
						</xsl:apply-templates>
					</tbody>
				</tgroup>
			</informaltable>
		</xsl:if>
	</xsl:template>
	<xsl:template match="DefectsLink" mode="defect">
		<row>
			<entry>
				<link linkend="{@ref}">
					<xsl:value-of select="substring(@ref,5)" />
				</link>
			</entry>
			<entry>
				<xsl:value-of select="@defect_manager" />
			</entry>
			<entry>
				<xsl:call-template name="replaceByBr">
					<xsl:with-param	name="textToReplace" select="Defect/@summary" />
				</xsl:call-template>
			</entry>
			<entry>
				<xsl:value-of select="Defect/Priority/@value" />
			</entry>
			<entry>
				<xsl:value-of select="Defect/Status/@value" />
			</entry>
		</row>
	</xsl:template>

	<xsl:template name="extTest">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extExec">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extResExec1">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>

	<xsl:template name="extResExec2">
		<xsl:param name="current-node" />
		<xsl:apply-templates select="DefectsLinks" mode="defect" />
	</xsl:template>
</xsl:stylesheet>