<?php
	# Mantis - a php based bugtracking system
	# Copyright (C) 2000 - 2002  Kenzaburo Ito - kenito@300baud.org
	# Copyright (C) 2002 - 2004  Mantis Team   - mantisbt-dev@lists.sourceforge.net
	# This program is distributed under the terms and conditions of the GPL
	# See the README and LICENSE files for details

	# --------------------------------------------------------
	# $Revision$
	# $Author$
	# $Date$
	#
	# $Id$
	# --------------------------------------------------------
?>
<?php
	require_once( 'core.php' );
	
	$t_core_path = config_get( 'core_path' );
	require_once( $t_core_path.'current_user_api.php' );
	require_once( $t_core_path.'compress_api.php' );
	require_once( $t_core_path.'filter_api.php' );
	require_once( $t_core_path.'authentication_api.php' );

	function salome_save_filter( $t_setting_arr) {
		$tc_setting_arr = filter_ensure_valid_filter( $t_setting_arr );
		$t_cookie_version = config_get( 'cookie_version' );

		$t_settings_serialized = serialize( $tc_setting_arr );
		$t_settings_string = $t_cookie_version . '#' . $t_settings_serialized;

		# If only using a temporary filter, don't store it in the database
			# Store the filter string in the database: its the current filter, so some values won't change
			$t_project_id = helper_get_current_project();
			$t_project_id = ( $t_project_id * -1 );
			$t_row_id = filter_db_set_for_current_user( $t_project_id, false, '', $t_settings_string );

		# set cookie values
		gpc_set_cookie( config_get( 'view_all_cookie' ), $t_row_id, time()+config_get( 'cookie_time_length' ), config_get( 'cookie_path' ) );
	}
	
	function salome_banner(){
		PRINT '<table bgcolor="#ff9933" border="0" cellpadding="0" cellspacing="0" width="100%">';
    	PRINT '<tbody>';
      	PRINT '<tr>';
      	PRINT  	'<td>';
      	PRINT    '<font color="#ffffff" size="8">';
     	PRINT '<center>';
     	PRINT   ' Mantis - Salome-TMF&nbsp;</center>';
      	PRINT '</font>';
      	PRINT  	'</td>';
     	PRINT '</tr>';
  		PRINT '</tbody>';
		PRINT '</table>';
	}
	
	
	function auth_login_salome_user( $t_user_id) {
		global $g_script_login_cookie, $g_cache_current_user_id;
		
		$t_user = user_get_row( $t_user_id );

		# check for disabled account
		if ( OFF == $t_user['enabled'] ) {
			return false;
		}
		# ok, we're good to login now

		# increment login count
		user_increment_login_count( $t_user_id );

		# set the cookies
		$g_script_login_cookie = $t_user['cookie_string'];

		# cache user id for future reference
		$g_cache_current_user_id = $t_user_id;

		auth_set_cookies( $t_user_id, true);
		
	}
	
	
	function auth_login_salome_project($project_id){
		$f_make_default	= gpc_get_bool( 'make_default' );
		$f_ref			= gpc_get_string( 'ref', '' );

		$c_ref = string_prepare_header( $f_ref );

		if ( ALL_PROJECTS != $project_id  ) {
			project_ensure_exists( $project_id  );
		}

		# Set default project
		if ( $f_make_default ) {
			current_user_set_default_project( $project_id  );
		}
		
		helper_set_current_project( $project_id  );	
	}
	
?>
<?php
	// Copy project id and version name
	$project_id = $_REQUEST['project_id'];
	$version_name = $_REQUEST['version_name'];
	$user_id = $_REQUEST['user_id'];
	$bug_id = $_REQUEST['bug_id'];
	
	if ($user_id != null){ 	
		auth_login_salome_user($user_id );
		if (!auth_is_user_authenticated()){
			auth_login_salome_user($user_id );
		}
	}
	
	auth_login_salome_project($project_id);

	
	$f_page_number		= gpc_get_int( 'page_number', 1 );

	$t_per_page = null;
	$t_bug_count = null;
	$t_page_count = null; 
	
	if (!($version_name == null)){
		$my_filter = array();
		$my_filter['show_version'] = $version_name;
		salome_save_filter($my_filter);
		
		html_meta_redirect( 'view_all_bug_page.php',0);
	} else if (!($bug_id == null)){
		html_meta_redirect( 'view.php?id='.$bug_id,0);
	} else {
		html_meta_redirect( 'view_all_bug_page.php',0);
	}
	
?>
